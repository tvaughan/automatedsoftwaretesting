/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1275558455
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   volatile uint16_t  f0;
   volatile int16_t  f1;
   int8_t  f2;
};

union U1 {
   float  f0;
};

/* --- GLOBAL VARIABLES --- */
static volatile uint16_t g_2[6] = {0xCEAFL,0x6F1BL,0xCEAFL,0xCEAFL,0x6F1BL,0xCEAFL};
static uint8_t g_29 = 248UL;
static int32_t g_44[6][6][4] = {{{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL}},{{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL}},{{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL}},{{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL}},{{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL}},{{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL},{0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL,0xA6CA0CECL}}};
static int32_t g_74 = 0x6691CFE0L;
static uint16_t g_88 = 0x2191L;
static uint16_t g_92 = 0x4912L;
static int32_t g_94[4] = {0L,0L,0L,0L};
static float g_96 = 0xA.E6E92Ep-69;
static uint8_t g_152 = 0xABL;
static int16_t g_154 = (-1L);
static int64_t g_190 = 0xBAF640DE0EBD967FLL;
static union U0 g_220 = {0x5B54L};/* VOLATILE GLOBAL g_220 */
static uint8_t g_232 = 0xE7L;
static uint16_t *g_245 = &g_92;
static uint16_t **g_244 = &g_245;
static uint32_t g_252 = 4294967291UL;
static float g_285 = (-0x2.9p-1);
static volatile int32_t g_302[10] = {1L,1L,8L,1L,1L,8L,1L,1L,8L,1L};
static volatile int32_t *g_301 = &g_302[8];
static volatile int32_t **g_300 = &g_301;
static union U0 g_318 = {0x51B5L};/* VOLATILE GLOBAL g_318 */
static volatile union U0 * const g_325 = (void*)0;
static const volatile union U0 g_329 = {0xE5FEL};/* VOLATILE GLOBAL g_329 */
static const volatile union U0 *g_328 = &g_329;
static const volatile union U0 * volatile * volatile g_327 = &g_328;/* VOLATILE GLOBAL g_327 */
static const volatile union U0 * volatile * volatile *g_326 = &g_327;
static union U0 g_350 = {65534UL};/* VOLATILE GLOBAL g_350 */
static union U0 g_352 = {0xD9CCL};/* VOLATILE GLOBAL g_352 */
static int32_t g_359 = 0x965F8DA9L;
static int32_t g_376 = 0x9272B44AL;
static volatile int32_t g_415 = 0xCE86CE93L;/* VOLATILE GLOBAL g_415 */
static const union U1 g_437 = {-0x8.4p+1};
static uint64_t g_456[7][4][5] = {{{1UL,1UL,18446744073709551612UL,1UL,18446744073709551612UL},{0x9011A977A9AE8524LL,18446744073709551607UL,0x7549F4B528D20E4CLL,0x0FE1BC589DB039A9LL,0x7549F4B528D20E4CLL},{1UL,1UL,18446744073709551612UL,1UL,18446744073709551612UL},{0x9011A977A9AE8524LL,18446744073709551607UL,0x7549F4B528D20E4CLL,0x0FE1BC589DB039A9LL,0x7549F4B528D20E4CLL}},{{1UL,1UL,18446744073709551612UL,1UL,18446744073709551612UL},{0x9011A977A9AE8524LL,18446744073709551607UL,0x7549F4B528D20E4CLL,0x0FE1BC589DB039A9LL,0x7549F4B528D20E4CLL},{1UL,1UL,18446744073709551612UL,1UL,18446744073709551612UL},{0x9011A977A9AE8524LL,18446744073709551607UL,0x7549F4B528D20E4CLL,0x0FE1BC589DB039A9LL,0x7549F4B528D20E4CLL}},{{1UL,1UL,18446744073709551612UL,1UL,18446744073709551612UL},{0x9011A977A9AE8524LL,18446744073709551607UL,0x7549F4B528D20E4CLL,0x0FE1BC589DB039A9LL,0x7549F4B528D20E4CLL},{1UL,1UL,18446744073709551612UL,1UL,18446744073709551612UL},{0x9011A977A9AE8524LL,18446744073709551607UL,0x7549F4B528D20E4CLL,0x0FE1BC589DB039A9LL,0x7549F4B528D20E4CLL}},{{1UL,1UL,18446744073709551612UL,1UL,18446744073709551612UL},{0x9011A977A9AE8524LL,18446744073709551607UL,0x7549F4B528D20E4CLL,0x0FE1BC589DB039A9LL,0x7549F4B528D20E4CLL},{1UL,1UL,18446744073709551612UL,0x91FAFA0813F29533LL,0x265B78EF1150F9B0LL},{0x7549F4B528D20E4CLL,0xB2D53C59B4819535LL,0x15691E015A7075D5LL,18446744073709551615UL,0x15691E015A7075D5LL}},{{18446744073709551612UL,18446744073709551612UL,0x265B78EF1150F9B0LL,0x91FAFA0813F29533LL,0x265B78EF1150F9B0LL},{0x7549F4B528D20E4CLL,0xB2D53C59B4819535LL,0x15691E015A7075D5LL,18446744073709551615UL,0x15691E015A7075D5LL},{18446744073709551612UL,18446744073709551612UL,0x265B78EF1150F9B0LL,0x91FAFA0813F29533LL,0x265B78EF1150F9B0LL},{0x7549F4B528D20E4CLL,0xB2D53C59B4819535LL,0x15691E015A7075D5LL,18446744073709551615UL,0x15691E015A7075D5LL}},{{18446744073709551612UL,18446744073709551612UL,0x265B78EF1150F9B0LL,0x91FAFA0813F29533LL,0x265B78EF1150F9B0LL},{0x7549F4B528D20E4CLL,0xB2D53C59B4819535LL,0x15691E015A7075D5LL,18446744073709551615UL,0x15691E015A7075D5LL},{18446744073709551612UL,18446744073709551612UL,0x265B78EF1150F9B0LL,0x91FAFA0813F29533LL,0x265B78EF1150F9B0LL},{0x7549F4B528D20E4CLL,0xB2D53C59B4819535LL,0x15691E015A7075D5LL,18446744073709551615UL,0x15691E015A7075D5LL}},{{18446744073709551612UL,18446744073709551612UL,0x265B78EF1150F9B0LL,0x91FAFA0813F29533LL,0x265B78EF1150F9B0LL},{0x7549F4B528D20E4CLL,0xB2D53C59B4819535LL,0x15691E015A7075D5LL,18446744073709551615UL,0x15691E015A7075D5LL},{18446744073709551612UL,18446744073709551612UL,0x265B78EF1150F9B0LL,0x91FAFA0813F29533LL,0x265B78EF1150F9B0LL},{0x7549F4B528D20E4CLL,0xB2D53C59B4819535LL,0x15691E015A7075D5LL,18446744073709551615UL,0x15691E015A7075D5LL}}};
static union U0 g_481 = {0xFC91L};/* VOLATILE GLOBAL g_481 */
static uint32_t g_495 = 0x2D17689CL;
static union U0 g_511 = {0x9314L};/* VOLATILE GLOBAL g_511 */
static int32_t *g_608[3] = {(void*)0,(void*)0,(void*)0};
static uint16_t g_634 = 0xD476L;
static int32_t *g_654 = &g_94[3];
static int32_t g_657 = (-2L);
static int32_t *g_656 = &g_657;
static int32_t **g_655 = &g_656;
static float *g_673 = &g_96;
static float **g_672 = &g_673;
static union U0 g_684 = {0UL};/* VOLATILE GLOBAL g_684 */
static union U0 *g_683 = &g_684;
static union U0 g_687 = {2UL};/* VOLATILE GLOBAL g_687 */
static uint32_t g_732 = 0x2201C15AL;
static union U0 g_847[9] = {{9UL},{0xE018L},{9UL},{0xE018L},{9UL},{0xE018L},{9UL},{0xE018L},{9UL}};
static union U0 g_849 = {0x5A63L};/* VOLATILE GLOBAL g_849 */
static union U0 g_854 = {6UL};/* VOLATILE GLOBAL g_854 */
static uint8_t *g_860 = &g_232;
static uint8_t **g_859 = &g_860;
static uint8_t ***g_858 = &g_859;
static const int32_t *g_893 = &g_74;
static union U0 g_897[2][8] = {{{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}}};
static uint64_t g_990[2] = {2UL,2UL};
static uint16_t * const *g_1000[5][10] = {{&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245},{&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245},{&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245},{&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245},{&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245,&g_245}};
static uint16_t * const * volatile *g_999[5] = {&g_1000[0][4],&g_1000[0][4],&g_1000[0][4],&g_1000[0][4],&g_1000[0][4]};
static uint16_t * const * volatile **g_998 = &g_999[4];
static uint16_t *** const g_1002[8][7] = {{&g_244,&g_244,(void*)0,&g_244,&g_244,&g_244,&g_244},{&g_244,(void*)0,&g_244,&g_244,(void*)0,&g_244,&g_244},{&g_244,&g_244,(void*)0,&g_244,&g_244,(void*)0,&g_244},{&g_244,&g_244,&g_244,&g_244,&g_244,&g_244,&g_244},{(void*)0,&g_244,&g_244,&g_244,&g_244,&g_244,&g_244},{&g_244,&g_244,&g_244,&g_244,&g_244,&g_244,&g_244},{&g_244,(void*)0,(void*)0,&g_244,&g_244,&g_244,&g_244},{&g_244,&g_244,(void*)0,(void*)0,&g_244,&g_244,&g_244}};
static uint16_t *** const *g_1001[5] = {&g_1002[0][4],&g_1002[0][4],&g_1002[0][4],&g_1002[0][4],&g_1002[0][4]};
static union U1 g_1045 = {-0x7.Cp+1};
static union U1 *g_1046 = (void*)0;
static uint32_t g_1115[5] = {0UL,0UL,0UL,0UL,0UL};
static uint16_t g_1226[2][3] = {{0xCEC3L,0xCEC3L,0xCEC3L},{0xCEC3L,0xCEC3L,0xCEC3L}};
static uint64_t g_1281[1] = {18446744073709551615UL};
static volatile int64_t g_1359[6] = {0xFE9A3354CE422E04LL,0xFE9A3354CE422E04LL,0xFE9A3354CE422E04LL,0xFE9A3354CE422E04LL,0xFE9A3354CE422E04LL,0xFE9A3354CE422E04LL};
static volatile int64_t *g_1358 = &g_1359[1];
static union U0 **g_1362 = &g_683;
static uint32_t g_1390[9] = {7UL,7UL,7UL,7UL,7UL,7UL,7UL,7UL,7UL};
static int16_t *g_1394 = &g_154;
static int16_t **g_1393 = &g_1394;
static volatile int32_t g_1458[7] = {3L,3L,3L,3L,3L,3L,3L};
static uint8_t g_1476 = 0x7FL;
static uint8_t **g_1491 = &g_860;
static uint8_t **g_1492[1][1][10] = {{{&g_860,&g_860,&g_860,&g_860,&g_860,&g_860,&g_860,&g_860,&g_860,&g_860}}};
static uint8_t *** const g_1490[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t *** const *g_1489 = &g_1490[9];
static volatile union U1 g_1540 = {0x9.BE47D8p+82};/* VOLATILE GLOBAL g_1540 */
static volatile union U1 *g_1539[1] = {&g_1540};
static volatile union U1 * volatile *g_1538[8] = {&g_1539[0],&g_1539[0],&g_1539[0],&g_1539[0],&g_1539[0],&g_1539[0],&g_1539[0],&g_1539[0]};
static volatile union U1 * volatile **g_1537 = &g_1538[5];
static union U1 g_1562 = {0xF.8D66AEp-4};
static const uint32_t g_1581 = 0x57DE0551L;
static union U0 g_1617 = {0x58EEL};/* VOLATILE GLOBAL g_1617 */
static uint8_t ****g_1700 = &g_858;
static uint8_t ****g_1701 = &g_858;
static uint64_t * volatile * volatile g_1800 = (void*)0;/* VOLATILE GLOBAL g_1800 */
static union U1 **g_1879 = &g_1046;
static union U1 ***g_1878 = &g_1879;
static union U1 **** const g_1877 = &g_1878;
static union U1 **** const *g_1876 = &g_1877;
static union U1 g_2006 = {0x6.1p+1};
static int16_t * volatile *g_2014 = &g_1394;
static int16_t * volatile ** volatile g_2013 = &g_2014;/* VOLATILE GLOBAL g_2013 */
static int16_t * volatile ** volatile *g_2012 = &g_2013;
static uint32_t * volatile ** const  volatile g_2106 = (void*)0;/* VOLATILE GLOBAL g_2106 */
static uint32_t g_2123 = 0x56342EA0L;
static const int32_t ** volatile g_2129 = &g_893;/* VOLATILE GLOBAL g_2129 */
static int32_t * volatile g_2189 = &g_94[3];/* VOLATILE GLOBAL g_2189 */
static union U0 g_2211[8][8] = {{{0x9ADDL},{0x05DBL},{0x8D57L},{3UL},{65535UL},{1UL},{0UL},{0UL}},{{0x8D57L},{0x9ADDL},{0x9A0FL},{0x9A0FL},{0x9ADDL},{0x8D57L},{0UL},{1UL}},{{0xBF39L},{0x9A0FL},{0x8D57L},{1UL},{3UL},{65532UL},{3UL},{1UL}},{{3UL},{65532UL},{3UL},{1UL},{0x8D57L},{0x9A0FL},{0xBF39L},{1UL}},{{0UL},{0x8D57L},{0x9ADDL},{0x9A0FL},{0x9A0FL},{0x9ADDL},{0x8D57L},{0UL}},{{0UL},{1UL},{65535UL},{3UL},{0x8D57L},{0x05DBL},{0x9ADDL},{0x05DBL}},{{3UL},{0x537FL},{1UL},{0x537FL},{3UL},{0x05DBL},{65532UL},{0x8D57L}},{{0xBF39L},{1UL},{0x537FL},{0x32C4L},{0x9ADDL},{0x9ADDL},{0x32C4L},{0x537FL}}};
static int8_t g_2241 = 0xBEL;
static const int32_t ** volatile g_2247 = &g_893;/* VOLATILE GLOBAL g_2247 */
static const int32_t ** volatile g_2248 = &g_893;/* VOLATILE GLOBAL g_2248 */
static int8_t g_2273 = 0xCFL;
static const int32_t ** volatile g_2295 = (void*)0;/* VOLATILE GLOBAL g_2295 */
static int16_t ***g_2332[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int16_t ****g_2331 = &g_2332[4];
static int32_t **** const g_2385[2] = {(void*)0,(void*)0};
static int32_t **** const *g_2384 = &g_2385[1];
static int32_t g_2421 = 0L;
static union U0 g_2437 = {0xA0AEL};/* VOLATILE GLOBAL g_2437 */
static int16_t * volatile g_2467 = &g_154;/* VOLATILE GLOBAL g_2467 */
static const int32_t ** volatile g_2516[9] = {(void*)0,&g_893,&g_893,(void*)0,&g_893,&g_893,(void*)0,&g_893,&g_893};
static const int32_t ** volatile g_2517 = &g_893;/* VOLATILE GLOBAL g_2517 */
static uint8_t g_2545 = 255UL;
static int16_t g_2593 = (-1L);
static const uint64_t *g_2600 = &g_990[1];
static const uint64_t * const  volatile *g_2599 = &g_2600;
static const uint64_t * const  volatile * volatile *g_2598 = &g_2599;
static uint64_t ** const ** volatile g_2605 = (void*)0;/* VOLATILE GLOBAL g_2605 */
static uint64_t *g_2609 = &g_990[0];
static uint64_t ** const g_2608 = &g_2609;
static uint64_t ** const *g_2607[3][10][3] = {{{&g_2608,&g_2608,&g_2608},{(void*)0,&g_2608,&g_2608},{&g_2608,(void*)0,&g_2608},{(void*)0,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,(void*)0,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,(void*)0,&g_2608}},{{&g_2608,&g_2608,&g_2608},{&g_2608,(void*)0,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,(void*)0,(void*)0},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,(void*)0,(void*)0},{&g_2608,&g_2608,&g_2608}},{{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{(void*)0,(void*)0,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{(void*)0,(void*)0,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,&g_2608,&g_2608},{&g_2608,(void*)0,&g_2608}}};
static uint64_t ** const ** volatile g_2606 = &g_2607[1][7][2];/* VOLATILE GLOBAL g_2606 */
static uint16_t g_2612 = 0xC249L;
static volatile uint8_t g_2632 = 0xFDL;/* VOLATILE GLOBAL g_2632 */


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static float  func_5(uint32_t  p_6, union U1  p_7, int8_t  p_8);
static uint8_t  func_16(union U1  p_17);
static union U1  func_18(int8_t  p_19, uint32_t  p_20);
static int32_t  func_22(uint32_t  p_23, uint8_t  p_24, uint8_t  p_25, int32_t  p_26);
static uint64_t  func_30(int32_t  p_31, uint64_t  p_32);
static int32_t  func_39(int32_t  p_40, uint8_t  p_41);
static int16_t  func_51(uint16_t  p_52, int32_t  p_53, int16_t  p_54, int32_t ** p_55, int16_t  p_56);
static int8_t  func_71(const int32_t * p_72);
static const int32_t * func_77(int32_t * p_78);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_1700 g_858 g_859 g_1358 g_1359 g_300 g_301 g_318.f1 g_190 g_860 g_232 g_2384 g_672 g_673 g_96 g_1617.f2 g_732 g_655 g_456 g_252 g_990 g_1537 g_1538 g_1539 g_1540 g_2248 g_893 g_74 g_2421 g_1362 g_1115 g_654 g_94 g_854.f2 g_350.f1 g_2437 g_657 g_244 g_245 g_92 g_684.f2 g_1800 g_1281 g_2437.f2 g_656 g_2467 g_2211.f0 g_1393 g_1394 g_220.f2 g_2247 g_2129 g_1877 g_1878 g_1879 g_998 g_999 g_1000 g_2012 g_2013 g_2014 g_154 g_2593 g_2598 g_2545 g_29 g_2606 g_2517 g_849.f2 g_2612 g_2599 g_2600 g_2632 g_481.f2
 * writes: g_302 g_190 g_232 g_96 g_732 g_318.f2 g_656 g_154 g_673 g_456 g_252 g_990 g_1281 g_2211.f2 g_683 g_854.f2 g_92 g_684.f2 g_94 g_2421 g_1617.f2 g_301 g_1046 g_2593 g_2607 g_893 g_849.f2 g_481.f2 g_657
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    int32_t l_21 = 0x5B0E82CAL;
    union U1 l_2169 = {0x5.C2949Ep+94};
    int8_t l_2183 = 6L;
    int32_t l_2190 = 1L;
    int32_t l_2191 = 9L;
    int32_t l_2264 = 0xE1BBC81DL;
    int32_t l_2266[5][6][7] = {{{0xB3516FEBL,0x9C8A6174L,0xA2F57F1CL,(-1L),1L,(-1L),0xCCAA1A86L},{0L,0x9C8A6174L,0L,0xF1F48AE9L,(-1L),1L,0xCCAA1A86L},{0xB3516FEBL,1L,1L,(-1L),(-1L),(-1L),0L},{(-1L),(-8L),1L,0xF1F48AE9L,1L,(-8L),(-1L)},{(-1L),1L,0L,(-1L),(-10L),(-8L),0L},{0xB3516FEBL,0x9C8A6174L,0xA2F57F1CL,(-1L),1L,(-1L),0xCCAA1A86L}},{{0L,0x9C8A6174L,0L,0xF1F48AE9L,(-1L),1L,0xCCAA1A86L},{0xB3516FEBL,1L,1L,(-1L),(-1L),(-1L),0L},{(-1L),(-8L),1L,0xF1F48AE9L,1L,(-8L),(-1L)},{(-1L),1L,0L,(-1L),(-10L),(-8L),0L},{0xB3516FEBL,0x9C8A6174L,0xA2F57F1CL,(-1L),1L,(-1L),0xCCAA1A86L},{0L,0x9C8A6174L,0L,0xF1F48AE9L,(-1L),1L,0xCCAA1A86L}},{{0xB3516FEBL,1L,1L,(-1L),(-1L),(-1L),0L},{(-1L),(-8L),1L,0xF1F48AE9L,1L,(-8L),(-1L)},{(-1L),1L,0L,(-1L),(-10L),(-8L),0L},{0xB3516FEBL,0x9C8A6174L,0xA2F57F1CL,(-1L),1L,(-1L),0xCCAA1A86L},{0L,0x9C8A6174L,0L,0xF1F48AE9L,(-1L),1L,0xCCAA1A86L},{0xB3516FEBL,1L,1L,(-1L),(-1L),(-1L),0L}},{{(-1L),0L,(-3L),(-1L),(-3L),0L,(-1L)},{(-1L),0xB3516FEBL,0x851DE7CEL,0x0A53B43DL,0L,0L,(-1L)},{0xF1F48AE9L,(-1L),(-10L),0x0A53B43DL,(-3L),1L,1L},{(-1L),(-1L),0x851DE7CEL,(-1L),0L,0xB3516FEBL,1L},{0xF1F48AE9L,0xB3516FEBL,(-3L),0x44155227L,0L,1L,(-1L)},{(-1L),0L,(-3L),(-1L),(-3L),0L,(-1L)}},{{(-1L),0xB3516FEBL,0x851DE7CEL,0x0A53B43DL,0L,0L,(-1L)},{0xF1F48AE9L,(-1L),(-10L),0x0A53B43DL,(-3L),1L,1L},{(-1L),(-1L),0x851DE7CEL,(-1L),0L,0xB3516FEBL,1L},{0xF1F48AE9L,0xB3516FEBL,(-3L),0x44155227L,0L,1L,(-1L)},{(-1L),0L,(-3L),(-1L),(-3L),0L,(-1L)},{(-1L),0xB3516FEBL,0x851DE7CEL,0x0A53B43DL,0L,0L,(-1L)}}};
    int32_t l_2270 = 0xF80664B7L;
    const union U1 **l_2292 = (void*)0;
    uint64_t l_2346 = 0UL;
    int32_t *l_2387 = &l_2264;
    uint8_t l_2464 = 9UL;
    const union U1 *l_2469 = &g_1045;
    const union U1 **l_2468 = &l_2469;
    uint64_t l_2480[1][10][3] = {{{0x04953C472D6ACA7BLL,0UL,0x59F51D59E5908BEFLL},{0x0378A6B184C14C3ALL,0UL,0xE688B2FF17D062ADLL},{0x78658A4148A961F3LL,0UL,0UL},{0x04953C472D6ACA7BLL,0UL,0x59F51D59E5908BEFLL},{0x0378A6B184C14C3ALL,0UL,0xE688B2FF17D062ADLL},{0x78658A4148A961F3LL,0UL,0UL},{0x04953C472D6ACA7BLL,0UL,0x59F51D59E5908BEFLL},{0x0378A6B184C14C3ALL,0UL,0xE688B2FF17D062ADLL},{0x78658A4148A961F3LL,0UL,0UL},{0x04953C472D6ACA7BLL,0UL,0x59F51D59E5908BEFLL}}};
    float l_2546 = 0xF.6D3507p+68;
    const uint32_t l_2601[2][7][6] = {{{0x98DF5B64L,0xD59EA4EBL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551614UL,0xCC8533B4L,18446744073709551615UL,0x35AF17E0L,18446744073709551615UL,0xCC8533B4L},{18446744073709551615UL,0xD59EA4EBL,1UL,0x35AF17E0L,0xD59EA4EBL,18446744073709551615UL},{18446744073709551614UL,18446744073709551615UL,1UL,18446744073709551615UL,0xCC8533B4L,0xCC8533B4L},{0x98DF5B64L,18446744073709551615UL,18446744073709551615UL,0x98DF5B64L,0xD59EA4EBL,1UL},{0x98DF5B64L,0xD59EA4EBL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551614UL,0xCC8533B4L,18446744073709551615UL,0x35AF17E0L,18446744073709551615UL,0xCC8533B4L}},{{18446744073709551615UL,0xD59EA4EBL,1UL,0x35AF17E0L,0xD59EA4EBL,18446744073709551615UL},{18446744073709551614UL,18446744073709551615UL,1UL,18446744073709551615UL,0xCC8533B4L,0xCC8533B4L},{0x98DF5B64L,18446744073709551615UL,18446744073709551615UL,0x98DF5B64L,0xD59EA4EBL,1UL},{0x98DF5B64L,0xD59EA4EBL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551614UL,0xCC8533B4L,18446744073709551615UL,0x35AF17E0L,18446744073709551615UL,0xCC8533B4L},{18446744073709551615UL,0xD59EA4EBL,1UL,0x35AF17E0L,0xD59EA4EBL,0x545C7DD8L},{18446744073709551615UL,0x545C7DD8L,18446744073709551612UL,1UL,0x503AD83EL,0x503AD83EL}}};
    uint64_t *l_2604 = &l_2480[0][4][2];
    uint64_t **l_2603 = &l_2604;
    uint64_t ** const * const l_2602[1][9][6] = {{{(void*)0,&l_2603,&l_2603,(void*)0,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603,&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603,&l_2603,&l_2603,&l_2603},{(void*)0,&l_2603,&l_2603,(void*)0,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603,&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603,&l_2603,&l_2603,&l_2603},{(void*)0,&l_2603,&l_2603,(void*)0,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603,&l_2603,&l_2603,&l_2603},{&l_2603,&l_2603,&l_2603,&l_2603,&l_2603,&l_2603}}};
    union U1 l_2619 = {0x1.9p+1};
    uint64_t l_2634 = 0x41AC8A1858627F00LL;
    const int64_t l_2637 = 0xB45DF68B0571D79BLL;
    uint8_t l_2674 = 3UL;
    int64_t l_2675[10][6] = {{0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL,0x53DDF38A954B93C2LL,1L,0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL},{0xA36D6B8274297AA0LL,0x6D64D6B0D7B6BB72LL,0xEC4F91229C48321FLL,0x6D64D6B0D7B6BB72LL,0xA36D6B8274297AA0LL,0x6D64D6B0D7B6BB72LL},{0x53DDF38A954B93C2LL,1L,0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL,0x53DDF38A954B93C2LL,1L},{0xA36D6B8274297AA0LL,1L,0xEC4F91229C48321FLL,1L,0xA36D6B8274297AA0LL,1L},{0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL,0x53DDF38A954B93C2LL,1L,0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL},{0xA36D6B8274297AA0LL,0x6D64D6B0D7B6BB72LL,0xEC4F91229C48321FLL,0x6D64D6B0D7B6BB72LL,0xA36D6B8274297AA0LL,0x6D64D6B0D7B6BB72LL},{0x53DDF38A954B93C2LL,1L,0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL,0x53DDF38A954B93C2LL,1L},{0xA36D6B8274297AA0LL,1L,0xEC4F91229C48321FLL,1L,0xA36D6B8274297AA0LL,1L},{0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL,0x53DDF38A954B93C2LL,1L,0x53DDF38A954B93C2LL,0x6D64D6B0D7B6BB72LL},{0xA36D6B8274297AA0LL,0x6D64D6B0D7B6BB72LL,0xEC4F91229C48321FLL,0x6D64D6B0D7B6BB72LL,0xA36D6B8274297AA0LL,0x6D64D6B0D7B6BB72LL}};
    int64_t *l_2677 = &g_190;
    int64_t **l_2676 = &l_2677;
    int16_t l_2678 = 1L;
    int i, j, k;
    if (g_2[1])
    { /* block id: 1 */
        union U1 l_13 = {0x1.Ap-1};
        int32_t l_46 = (-9L);
        int32_t l_1284 = 0x0B6BCF2DL;
        union U1 *l_2162 = &g_2006;
        union U1 l_2163 = {0x6.072D9Cp-57};
        float l_2244 = 0x3.1441DAp-43;
        int32_t *l_2246 = &g_94[0];
        int32_t l_2253 = 0x8F4F4003L;
        int32_t l_2262 = 0x0EF7E1DDL;
        int32_t l_2263 = 4L;
        int32_t l_2268[10] = {0xC3922CDEL,0x084E7BC9L,9L,0x084E7BC9L,0xC3922CDEL,0xC3922CDEL,0x084E7BC9L,9L,0x084E7BC9L,0xC3922CDEL};
        float **l_2342[7][1][1];
        int i, j, k;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 1; k++)
                    l_2342[i][j][k] = &g_673;
            }
        }
    }
    else
    { /* block id: 1090 */
        uint8_t l_2363 = 0xE9L;
        union U1 l_2364 = {0x4.A55F07p+41};
        int32_t l_2383 = 0xF1FA6F63L;
        int32_t *****l_2386 = (void*)0;
        float *l_2399 = (void*)0;
        int32_t *l_2515 = &g_44[5][1][0];
        float l_2540 = 0x1.7p+1;
        const int32_t *l_2549[4][7];
        int32_t l_2571 = 0x1DACA569L;
        int32_t l_2572[3][1][4] = {{{(-8L),(-8L),(-8L),(-8L)}},{{(-8L),(-8L),(-8L),(-8L)}},{{(-8L),(-8L),(-8L),(-8L)}}};
        uint64_t *l_2618 = &l_2480[0][4][2];
        int64_t l_2631 = 0x5B066DA58FE9995BLL;
        int8_t *l_2633 = (void*)0;
        uint64_t l_2635 = 0x6BF432456AF5DE44LL;
        int64_t l_2636[8][5][6] = {{{(-1L),9L,0x42BF8CB437D6875BLL,0xFA219F341124B31ALL,0x9F108549FEF6FF9ALL,0x3ECC52C1734024CBLL},{(-7L),1L,0xC0F2A8A3E8C22AD4LL,0xE659536371DD7C6BLL,0x5CE5B989AAF909D7LL,0xE659536371DD7C6BLL},{0x210DF2F0760AC320LL,0x14176D2BC9011C2DLL,0x210DF2F0760AC320LL,0L,0x0E0F07E7360DF409LL,(-8L)},{3L,0x00120B9359FE1FDFLL,0L,(-1L),0xD69C6D1A99E75E47LL,0xD169B05E94A68FEALL},{0x5CE5B989AAF909D7LL,9L,0x3ECC52C1734024CBLL,(-1L),9L,0L}},{{3L,7L,(-4L),0L,7L,0xD69C6D1A99E75E47LL},{0x210DF2F0760AC320LL,(-1L),6L,0xE659536371DD7C6BLL,0L,(-9L)},{(-7L),0x42BF8CB437D6875BLL,0xDE127DB23629A008LL,0xFA219F341124B31ALL,0x14176D2BC9011C2DLL,(-4L)},{0xFE262E621D1AF20CLL,0xD448EBF453BB7679LL,1L,3L,2L,0x9F108549FEF6FF9ALL},{(-1L),0L,0x0E0F07E7360DF409LL,1L,0xE2D67D461E55B3C0LL,0L}},{{0xE41174D2FEC2D7AELL,(-1L),0x14176D2BC9011C2DLL,(-8L),0x9F108549FEF6FF9ALL,9L},{0xD169B05E94A68FEALL,(-1L),7L,1L,0x12D83D5187F818FFLL,9L},{0L,0xAA6D1430380A52FBLL,0xD448EBF453BB7679LL,0xBBF705CB8CD818A9LL,0x08C1004B7888E8DDLL,(-1L)},{0L,6L,0L,0L,7L,7L},{5L,0L,0L,5L,0xFA219F341124B31ALL,2L}},{{0x42BF8CB437D6875BLL,(-8L),0L,0xD169B05E94A68FEALL,0x5CE5B989AAF909D7LL,0x8C0C089D6F41877CLL},{(-1L),0x5F5E08FB86A83435LL,(-1L),(-3L),0x5CE5B989AAF909D7LL,3L},{0L,(-8L),1L,(-8L),0xFA219F341124B31ALL,0xC12C816ABCEAACACLL},{0x23BC560E697D69C0LL,0L,(-4L),0x0E0F07E7360DF409LL,7L,0x85748553BB460943LL},{(-4L),6L,0xC0F2A8A3E8C22AD4LL,0xFA219F341124B31ALL,0x08C1004B7888E8DDLL,0L}},{{0xEAB97431A8F85E5CLL,0xAA6D1430380A52FBLL,0xD169B05E94A68FEALL,3L,0x12D83D5187F818FFLL,0L},{0xDE127DB23629A008LL,(-1L),0x8C0C089D6F41877CLL,9L,0x9F108549FEF6FF9ALL,0xC0F2A8A3E8C22AD4LL},{(-8L),(-1L),0x08C1004B7888E8DDLL,6L,0xE2D67D461E55B3C0LL,0x0E0F07E7360DF409LL},{4L,0L,3L,0xE659536371DD7C6BLL,2L,(-1L)},{0L,0xD448EBF453BB7679LL,0L,(-1L),0L,0xE41174D2FEC2D7AELL}},{{0xCD78A68BF895B2C4LL,0x9F108549FEF6FF9ALL,(-1L),(-1L),(-8L),0L},{1L,0x7B91546A55DB8D7ALL,0xBBF705CB8CD818A9LL,0x7B91546A55DB8D7ALL,1L,(-8L)},{(-7L),(-4L),0x3ECC52C1734024CBLL,0x5F5E08FB86A83435LL,0xD4F73E1BCF0158A4LL,0x3A266CB6D5B29963LL},{2L,0x12D83D5187F818FFLL,0x7B91546A55DB8D7ALL,(-4L),(-1L),0x3A266CB6D5B29963LL},{0x85748553BB460943LL,0x0E0F07E7360DF409LL,0x3ECC52C1734024CBLL,(-1L),(-1L),(-8L)}},{{(-1L),0L,0xBBF705CB8CD818A9LL,0L,(-3L),0L},{(-1L),0xEAB97431A8F85E5CLL,(-1L),0xFE262E621D1AF20CLL,0L,0xE41174D2FEC2D7AELL},{0x0E0F07E7360DF409LL,0xD4F73E1BCF0158A4LL,0L,(-1L),0xBBF705CB8CD818A9LL,(-1L)},{9L,0xD69C6D1A99E75E47LL,3L,(-9L),0L,0x0E0F07E7360DF409LL},{0x3A266CB6D5B29963LL,0x00120B9359FE1FDFLL,0x08C1004B7888E8DDLL,(-1L),0x3ECC52C1734024CBLL,0xC0F2A8A3E8C22AD4LL}},{{(-4L),5L,0x8C0C089D6F41877CLL,1L,0x3A266CB6D5B29963LL,0L},{0L,0xC0F2A8A3E8C22AD4LL,0xD169B05E94A68FEALL,0x3A266CB6D5B29963LL,0L,0L},{(-1L),0x8C0C089D6F41877CLL,0xC0F2A8A3E8C22AD4LL,0x14176D2BC9011C2DLL,1L,0x85748553BB460943LL},{0xC12C816ABCEAACACLL,9L,(-4L),(-4L),9L,0xC12C816ABCEAACACLL},{(-9L),0xD169B05E94A68FEALL,1L,0x5CE5B989AAF909D7LL,0x85748553BB460943LL,0L}}};
        uint32_t l_2638 = 0x0B29BC3DL;
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 7; j++)
                l_2549[i][j] = &g_94[3];
        }
        if (((void*)0 != (**g_1700)))
        { /* block id: 1091 */
            int64_t l_2362 = 1L;
            (**g_300) = (safe_mod_func_int64_t_s_s((safe_mul_func_uint16_t_u_u((0L && (l_2364 , 0x27D373EE76373A4CLL)), l_2363)), (*g_1358)));
        }
        else
        { /* block id: 1094 */
            int64_t *l_2381 = &g_190;
            int32_t l_2382 = 0L;
            union U1 l_2458 = {0xB.8F49FFp-81};
            int32_t l_2570[7] = {0xD7A56A24L,2L,0xD7A56A24L,0xD7A56A24L,2L,0xD7A56A24L,0xD7A56A24L};
            uint32_t l_2594 = 0xE24ED3E9L;
            uint64_t *l_2597[5][10] = {{&g_456[5][3][2],&g_456[5][3][2],&g_1281[0],&g_456[5][3][2],&g_456[5][3][2],&g_1281[0],&g_456[5][3][2],&g_456[5][3][2],&g_1281[0],&g_456[5][3][2]},{&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2]},{&g_990[0],&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2],&g_990[0],&g_990[0]},{&g_456[5][3][2],&g_456[5][3][2],&g_1281[0],&g_456[5][3][2],&g_456[5][3][2],&g_1281[0],&g_456[5][3][2],&g_456[5][3][2],&g_1281[0],&g_456[5][3][2]},{&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2],&g_990[0],&g_990[0],&g_456[5][3][2]}};
            uint64_t **l_2596[4][9] = {{&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1]},{&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1]},{&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1]},{&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1],&l_2597[3][1],&l_2597[0][2],&l_2597[3][1]}};
            uint64_t ***l_2595[1][4][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}}};
            int i, j, k;
            if ((3UL & (g_318.f1 <= (((((*g_673) = (((!(((safe_lshift_func_int8_t_s_s(((l_2383 = (safe_div_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(((safe_mod_func_uint64_t_u_u(0x4FD52280335900B0LL, (safe_unary_minus_func_int16_t_s(0xDB88L)))) != (safe_div_func_uint8_t_u_u(((****g_1700) ^= ((((*l_2381) ^= (safe_mod_func_int16_t_s_s(l_2346, (-7L)))) , 0x0A11L) , l_2382)), 1L))), 0x71L)), 0x8E5161E3L))) ^ 1UL), 7)) , g_2384) == l_2386)) >= (**g_672)) > 0x3.3D73C1p-73)) , l_2364) , g_1617.f2) < l_2264))))
            { /* block id: 1099 */
                const uint32_t *l_2417[9][4] = {{&g_1581,&g_1581,&g_1581,&g_1581},{&g_1581,&g_1581,&g_1581,&g_1581},{&g_1581,&g_1581,&g_1115[2],&g_1581},{&g_1581,&g_1581,&g_1581,&g_1581},{&g_1581,&g_1581,&g_1581,&g_1581},{&g_1581,&g_1581,&g_1115[2],&g_1581},{&g_1581,&g_1581,&g_1581,&g_1581},{&g_1581,&g_1581,&g_1581,&g_1581},{&g_1581,&g_1581,&g_1115[2],&g_1581}};
                const uint32_t **l_2416 = &l_2417[7][0];
                const uint32_t ***l_2415 = &l_2416;
                int32_t l_2418 = 0L;
                int i, j;
                for (g_732 = 0; (g_732 <= 7); g_732 += 1)
                { /* block id: 1102 */
                    int64_t l_2390[9];
                    int i;
                    for (i = 0; i < 9; i++)
                        l_2390[i] = 0xA447D05199242039LL;
                    for (g_318.f2 = 7; (g_318.f2 >= 0); g_318.f2 -= 1)
                    { /* block id: 1105 */
                        l_2387 = ((*g_655) = &l_2382);
                    }
                    for (g_154 = 0; (g_154 <= 7); g_154 += 1)
                    { /* block id: 1111 */
                        uint64_t *l_2400 = &g_456[4][0][1];
                        uint32_t *l_2401 = &g_252;
                        uint64_t *l_2404 = &g_990[0];
                        uint64_t *l_2419 = &g_1281[0];
                        int8_t *l_2420 = &g_2211[7][2].f2;
                        const int32_t l_2422 = 0x24BD9F5BL;
                        int i, j;
                        g_96 = ((safe_mul_func_float_f_f((l_2390[7] , ((safe_lshift_func_int8_t_s_u(((safe_mul_func_uint8_t_u_u(((safe_div_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u(((*l_2400) |= (((*g_672) = l_2387) == l_2399)), (((*l_2401)--) , ((*l_2404)--)))), (safe_sub_func_int32_t_s_s((((safe_mul_func_int8_t_s_s((&g_2211[g_154][g_732] != ((*g_1362) = (((safe_sub_func_uint32_t_u_u(((***g_1537) , (((*l_2420) = (((*l_2419) = (safe_rshift_func_int16_t_s_u((((((**g_2248) , l_2415) != (void*)0) < ((l_2418 < l_2418) != 0UL)) < (-1L)), l_2418))) , 0L)) , g_2421)), (*g_893))) == l_2382) , (void*)0))), l_2422)) , g_1115[2]) != 0x8559CE79L), (*g_654))))) , 251UL), (****g_1700))) , 0x35L), 4)) , 0x2.7FD735p+9)), 0x9.AD701Fp+26)) > 0x6.0p+1);
                    }
                }
            }
            else
            { /* block id: 1122 */
                int64_t l_2441 = 0x1C378916A71534BBLL;
                uint64_t *l_2451 = &g_1281[0];
                uint64_t **l_2450 = &l_2451;
                const int16_t *l_2466 = &g_154;
                uint32_t l_2500 = 0x923BBB90L;
                union U1 l_2531 = {0x7.27DB1Dp+14};
                int32_t l_2564[8] = {0x04D15889L,0x3CAE3F3AL,0x3CAE3F3AL,0x04D15889L,0x3CAE3F3AL,0x3CAE3F3AL,0x04D15889L,0x3CAE3F3AL};
                int i;
                for (g_854.f2 = 0; (g_854.f2 > 28); g_854.f2++)
                { /* block id: 1125 */
                    const uint64_t l_2446 = 1UL;
                    uint8_t l_2447 = 0xE6L;
                    uint64_t ***l_2452 = &l_2450;
                    union U1 l_2453[8][4][8] = {{{{0x5.D866FAp-56},{0xC.85E55Dp-34},{-0x1.4p+1},{0x1.Fp+1},{0x7.380A6Ap+51},{0xE.ED364Fp-40},{0x8.Ap+1},{0x8.9p+1}},{{-0x1.7p+1},{0x1.Fp+1},{0xF.BAD294p+97},{0x4.B2AA81p-90},{0x4.9B816Ap+77},{0x2.CFE06Cp-19},{-0x8.Fp+1},{0x0.Ap+1}},{{0x4.9B816Ap+77},{0x2.CFE06Cp-19},{-0x8.Fp+1},{0x0.Ap+1},{0x5.Dp-1},{0x8.FC9099p-72},{0xD.32FAA6p+55},{0x5.EC10F0p-49}},{{0xF.8F1AF8p-89},{0x8.2p+1},{0x1.8FFE4Dp+59},{0x2.834994p-15},{0xE.61BCABp+13},{0xF.113D48p+62},{-0x6.2p-1},{0x5.Dp-1}}},{{{0xB.D03F0Cp+60},{0x4.6p-1},{0x8.9p+1},{0x0.6p+1},{0x1.Bp+1},{0x1.F153C6p+31},{0x3.69A1ECp+71},{-0x1.Ap-1}},{{0x8.E54492p-54},{0x3.6AE279p+71},{0x3.7p-1},{0x6.5p-1},{0x8.22E843p-39},{0x7.1E83BBp+18},{0xC.85E55Dp-34},{0xC.85E55Dp-34}},{{0xB.D03F0Cp+60},{-0x1.5p+1},{0x8.22E843p-39},{0x8.22E843p-39},{-0x1.5p+1},{0xB.D03F0Cp+60},{0x3.7p-1},{0x4.9B816Ap+77}},{{0x0.Ap+1},{0xF.113D48p+62},{0xE.ED364Fp-40},{0x4.B2AA81p-90},{0x3.0p-1},{0x9.E39778p+37},{0x7.380A6Ap+51},{0x1.8FFE4Dp+59}}},{{{0x0.9p-1},{0xA.1FF75Ap-94},{0xF.8F1AF8p-89},{0x4.B2AA81p-90},{0x9.85A9A9p-63},{0x3.Fp-1},{0x7.1E83BBp+18},{0x4.9B816Ap+77}},{{-0x6.Bp+1},{0x9.85A9A9p-63},{0xF.BAD294p+97},{0x8.22E843p-39},{-0x6.2p-1},{0x5.EC10F0p-49},{-0x3.3p+1},{0xC.85E55Dp-34}},{{0x3.69A1ECp+71},{-0x1.4p+1},{0x3.Fp-1},{0xB.D03F0Cp+60},{0x0.Ap+1},{0x9.85A9A9p-63},{0x0.6p-1},{0x5.Fp-1}},{{-0x3.3p+1},{0xC.8A6F19p-53},{0x1.Dp-1},{0x9.85A9A9p-63},{0x7.1E83BBp+18},{0x0.Ap+1},{0x2.834994p-15},{0x3.7p-1}}},{{{-0x8.Fp+1},{0xC.85E55Dp-34},{0x0.Ap+1},{-0x3.2p+1},{0x0.9p-1},{0x3.0p-1},{0x8.22E843p-39},{-0x6.Bp+1}},{{-0x1.5p+1},{0x5.EC10F0p-49},{0x4.6p-1},{0xF.8F1AF8p-89},{0x1.Bp-1},{0x3.69A1ECp+71},{0x1.Bp-1},{0xF.8F1AF8p-89}},{{0x7.1E83BBp+18},{-0x6.Bp+1},{0x7.1E83BBp+18},{0x1.Bp+1},{0x6.5p-1},{-0x8.Fp+1},{0x4.9B816Ap+77},{0x4.8C1513p+37}},{{0x5.EC10F0p-49},{0x8.22E843p-39},{0x9.95DD10p-65},{0x4.9B816Ap+77},{0x7.380A6Ap+51},{0x1.F153C6p+31},{0x6.5p-1},{-0x3.3p+1}}},{{{0x5.EC10F0p-49},{0x8.Ap+1},{0x2.834994p-15},{0xF.113D48p+62},{0x6.5p-1},{-0x3.2p+1},{0xA.A3FF9Cp-79},{0x1.Fp+1}},{{0x7.1E83BBp+18},{0x9.E39778p+37},{0xF.113D48p+62},{0x5.Dp-1},{0x1.Bp-1},{0x9.95DD10p-65},{0xF.8F1AF8p-89},{0x0.9p-1}},{{-0x1.5p+1},{0x7.380A6Ap+51},{-0x1.4p+1},{0x0.Ap+1},{0x0.9p-1},{0xC.85E55Dp-34},{0x3.Fp-1},{0xD.32FAA6p+55}},{{-0x8.Fp+1},{0xE.ED364Fp-40},{-0x1.Ap-1},{0x3.7p-1},{0x7.1E83BBp+18},{0x8.FC9099p-72},{0x4.8C1513p+37},{0x0.9p+1}}},{{{-0x3.3p+1},{0x9.95DD10p-65},{0x4.B2AA81p-90},{0x1.F153C6p+31},{0x0.Ap+1},{0x8.E54492p-54},{-0x1.Ap-1},{0xA.1FF75Ap-94}},{{0x3.69A1ECp+71},{0x5.Fp-1},{0xA.1FF75Ap-94},{-0x6.2p-1},{-0x6.2p-1},{0xA.1FF75Ap-94},{0x5.Fp-1},{0x3.69A1ECp+71}},{{-0x6.Bp+1},{0x4.B2AA81p-90},{-0x1.7p+1},{0x3.0p-1},{0x9.85A9A9p-63},{0x2.834994p-15},{0xF.BAD294p+97},{0x0.8p-1}},{{0x0.9p-1},{-0x6.2p-1},{0x3.69A1ECp+71},{0xA.A3FF9Cp-79},{0x3.0p-1},{0x2.834994p-15},{0x2.CFE06Cp-19},{0xB.D03F0Cp+60}}},{{{0x0.Ap+1},{0x4.B2AA81p-90},{0xD.32FAA6p+55},{0x1.Bp-1},{-0x1.5p+1},{0xA.1FF75Ap-94},{-0x6.Bp+1},{0x2.834994p-15}},{{0xB.D03F0Cp+60},{0x5.Fp-1},{0x1.Bp+1},{0x8.9p+1},{0x8.22E843p-39},{0x8.E54492p-54},{0x9.95DD10p-65},{0x4.6p-1}},{{0x1.Dp-1},{0x9.95DD10p-65},{0x2.CFE06Cp-19},{0x3.6AE279p+71},{-0x1.7p+1},{0x8.FC9099p-72},{-0x6.2p-1},{-0x8.Fp+1}},{{0x8.Ap+1},{0xE.ED364Fp-40},{0x7.380A6Ap+51},{0x1.Fp+1},{-0x1.4p+1},{0xC.85E55Dp-34},{0x5.D866FAp-56},{0x1.Bp-1}}},{{{0xF.113D48p+62},{0x7.380A6Ap+51},{-0x1.8p-1},{0xD.32FAA6p+55},{0x3.7p-1},{0x9.95DD10p-65},{0x8.Ap+1},{0x9.95DD10p-65}},{{0x3.6AE279p+71},{0x9.E39778p+37},{0x4.9B816Ap+77},{0x0.9p+1},{-0x1.8p-1},{0x8.9p+1},{0x1.Dp-1},{0x2.CFE06Cp-19}},{{-0x1.4p+1},{0x5.Dp-1},{0xD.32FAA6p+55},{-0x6.2p-1},{0xF.BAD294p+97},{0xE.ED364Fp-40},{0x6.5p-1},{0x0.9p+1}},{{0xE.ED364Fp-40},{-0x1.5p+1},{0xD.32FAA6p+55},{0x8.E54492p-54},{0x0.6p-1},{0x0.6p+1},{0x1.Dp-1},{0x8.22E843p-39}}}};
                    int32_t l_2463 = (-1L);
                    int8_t *l_2472 = &g_684.f2;
                    int i, j, k;
                    (**g_300) = ((safe_mod_func_uint64_t_u_u((*l_2387), (safe_mul_func_int8_t_s_s(g_350.f1, 6UL)))) && (((safe_lshift_func_int8_t_s_s((safe_mod_func_int32_t_s_s(((((safe_div_func_float_f_f((**g_672), ((safe_mul_func_float_f_f((g_2437 , (safe_sub_func_float_f_f(((**g_672) > (**g_672)), ((!l_2441) , (safe_add_func_float_f_f((((l_2382 &= ((**g_244) |= (safe_sub_func_uint8_t_u_u(0xF9L, g_657)))) , l_2446) < (*g_673)), l_2447)))))), l_2447)) < (*g_673)))) , 8UL) <= (-4L)) <= g_74), 4294967295UL)), 4)) >= (*l_2387)) <= g_684.f2));
                    if ((safe_add_func_uint64_t_u_u((((*l_2452) = l_2450) == g_1800), (l_2453[2][3][6] , (((*l_2387) , ((((*g_673) = (((((*l_2451)--) > (safe_rshift_func_uint16_t_u_u((l_2458 , (safe_sub_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((l_2463 > (l_2464 <= ((**g_244) == (((l_2446 , l_2382) || 0x7D319CEEL) & 0x7568L)))), (*l_2387))), 0xEDL))), 8))) < (*l_2387)) , (*g_673))) , 0L) || (*l_2387))) || g_2437.f2)))))
                    { /* block id: 1132 */
                        (*g_301) = (*g_656);
                    }
                    else
                    { /* block id: 1134 */
                        uint32_t l_2465 = 18446744073709551614UL;
                        (*l_2387) = (((l_2465 , l_2466) != g_2467) , 0xED04B031L);
                    }
                    l_2382 ^= (((void*)0 == l_2468) , ((0x632A01D1L > ((safe_rshift_func_int8_t_s_s(((*l_2472) = g_2211[7][2].f0), (safe_mod_func_uint16_t_u_u(l_2463, ((**g_1393) = (safe_lshift_func_int8_t_s_u((safe_add_func_uint64_t_u_u(0x5DF1CAE4519CD979LL, (l_2446 > (safe_unary_minus_func_uint64_t_u((&g_2273 != (void*)0)))))), 6))))))) ^ l_2480[0][4][2])) & g_220.f2));
                }
                (*g_654) &= (**g_2247);
lbl_2552:
                for (g_2421 = 0; (g_2421 > 27); g_2421 = safe_add_func_int8_t_s_s(g_2421, 1))
                { /* block id: 1144 */
                    float l_2511 = 0x9.9B1178p+68;
                    int32_t l_2512 = (-6L);
                    uint32_t *l_2513 = &g_252;
                    int8_t *l_2514 = &g_350.f2;
                    uint8_t *l_2547 = &g_1476;
                    int8_t *l_2548 = &g_2241;
                }
                for (g_1617.f2 = 22; (g_1617.f2 <= 24); g_1617.f2 = safe_add_func_int16_t_s_s(g_1617.f2, 4))
                { /* block id: 1164 */
                    int8_t l_2558 = 8L;
                    int32_t *l_2559 = &g_44[2][2][3];
                    int32_t *l_2560 = &g_657;
                    int32_t *l_2561 = &g_44[1][2][1];
                    int32_t *l_2562 = &g_94[3];
                    int32_t *l_2563 = &l_2264;
                    int32_t *l_2565 = &l_2266[2][5][4];
                    int32_t *l_2566 = (void*)0;
                    int32_t *l_2567 = (void*)0;
                    int32_t *l_2568 = &g_94[1];
                    int32_t *l_2569[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    uint64_t l_2573 = 0xAA7C0EDF71B5F48DLL;
                    int16_t *l_2592 = &g_2593;
                    int i;
                    if ((**g_2129))
                    { /* block id: 1165 */
                        if (g_2437.f2)
                            goto lbl_2552;
                        (*g_300) = (*g_300);
                    }
                    else
                    { /* block id: 1168 */
                        uint8_t l_2557 = 0xC7L;
                        (***g_1877) = &l_2458;
                        (*g_655) = ((safe_lshift_func_uint16_t_u_s(((****g_998) && ((safe_rshift_func_uint8_t_u_u((l_2557 & l_2557), 4)) , (****g_2012))), 2)) , (*g_655));
                    }
                    --l_2573;
                    (*g_2606) = ((((safe_div_func_int32_t_s_s(((*l_2562) = (l_2570[3] < ((safe_rshift_func_int8_t_s_u(((**g_672) , ((safe_mod_func_int16_t_s_s(((l_2564[4] = (safe_div_func_float_f_f((safe_div_func_float_f_f(((0x4.7C4C67p-25 >= ((((safe_div_func_float_f_f((((safe_add_func_int64_t_s_s((((safe_sub_func_int16_t_s_s(((*l_2592) |= (***g_2013)), l_2594)) , l_2595[0][1][0]) == g_2598), g_2545)) , l_2594) <= 0x3.869009p-53), 0x5.EF7269p+66)) >= (**g_672)) < l_2564[0]) > (-0x1.Ap-1))) >= (**g_672)), (*g_673))), 0xA.7B3351p-97))) , l_2564[0]), l_2570[6])) , 0L)), 7)) || l_2500))), g_29)) == l_2601[1][0][2]) , 0x6F131FA9AA87F732LL) , l_2602[0][2][2]);
                }
            }
            (*g_2129) = (*g_2517);
            for (g_849.f2 = 0; (g_849.f2 > 16); g_849.f2++)
            { /* block id: 1182 */
                if (g_2612)
                    break;
                (*g_654) = 0xB593A661L;
            }
            return (**g_2014);
        }
        if ((~((((***g_858) = (safe_div_func_uint16_t_u_u(8UL, ((((((*g_2599) == l_2618) | ((l_2364 = l_2619) , (((((****g_998) = (((((safe_lshift_func_uint16_t_u_s(65535UL, 5)) ^ ((*l_2604) &= (safe_add_func_int64_t_s_s(3L, (safe_lshift_func_int8_t_s_s((l_2634 ^= (safe_unary_minus_func_uint16_t_u((safe_div_func_uint32_t_u_u(0x2EF746FDL, (((*g_673) = 0x4.B4D117p+50) , (((safe_div_func_int64_t_s_s((*g_1358), l_2631)) ^ (-2L)) && g_2632))))))), (*l_2387))))))) >= g_456[4][0][1]) & (*l_2387)) , l_2635)) == l_2636[2][4][0]) , (void*)0) != (void*)0))) <= l_2637) | l_2638) & 18446744073709551611UL)))) < (*l_2387)) , (****g_2012))))
        { /* block id: 1194 */
            int64_t l_2642 = 0xFA5FF65FF97E0FD7LL;
            int32_t l_2643 = 1L;
            uint16_t l_2646[9][3][9] = {{{1UL,0x26D4L,0UL,0x5808L,0x93B1L,0xC6D5L,0x3899L,0x26D4L,0UL},{0xF2D4L,0x0F18L,1UL,0UL,1UL,0x13F8L,1UL,1UL,0x4EC2L},{0x3885L,0x1F7CL,65535UL,0x3885L,0x5808L,0xDC07L,0xBF00L,65535UL,0UL}},{{0xF2D4L,1UL,0xFFB7L,1UL,1UL,0xFFB7L,1UL,0xF2D4L,0xE6D1L},{1UL,0xB6D5L,0UL,0xA400L,0x5808L,0x3846L,65535UL,0x5808L,0xCF96L},{0UL,0x2E71L,65535UL,0x0F18L,1UL,0x6B76L,0xA71BL,0x6D7FL,0xE6D1L}},{{0x1F7CL,1UL,0UL,65535UL,0x93B1L,0UL,0x1F7CL,0xBF00L,0UL},{0xA71BL,1UL,0x4EC2L,0x0F18L,0UL,65535UL,0UL,0x0F18L,0x4EC2L},{65535UL,65535UL,0xC6D5L,0xA400L,9UL,0xCA6DL,1UL,0xBF00L,0UL}},{{1UL,1UL,0x6B76L,1UL,1UL,0x4EC2L,0xF2D4L,0x6D7FL,0x3D34L},{0xBF00L,0x1B09L,0xC6D5L,0x3885L,0x1F7CL,65535UL,0x3885L,0x5808L,0xDC07L},{1UL,1UL,0x4EC2L,0UL,65531UL,0x8F70L,0xF2D4L,0xF2D4L,0x8F70L}},{{0x3899L,0x5808L,0UL,0x5808L,0x3899L,65535UL,1UL,65535UL,0xDF9FL},{0x2E71L,1UL,65535UL,0UL,0x6D7FL,65535UL,0UL,1UL,65535UL},{0xA400L,0x1B09L,0UL,0x3899L,65535UL,65535UL,0x1F7CL,0x26D4L,0x3846L}},{{7UL,1UL,0xFFB7L,7UL,0UL,0x8F70L,0xA71BL,1UL,1UL},{0xA400L,65535UL,65535UL,0xB6D5L,0xB6D5L,65535UL,65535UL,0xA400L,0UL},{0x2E71L,1UL,1UL,0xF2D4L,0UL,0x4EC2L,1UL,0UL,0x0F18L}},{{0x31F8L,65531UL,0xB6D5L,0x62ADL,0x3F8AL,0x1F7CL,0xB432L,0xFEB1L,0xBF00L},{65535UL,0xB586L,0xA71BL,65526UL,2UL,0xF2D4L,65535UL,0x16F3L,0x6D7FL},{0xB432L,0x8EABL,0x5808L,0x62ADL,0x31F8L,0xA400L,0x31F8L,0x62ADL,0x5808L}},{{65526UL,65526UL,0x2E71L,0x74D7L,0x4119L,1UL,0xB586L,0x16F3L,0xF2D4L},{0x3F8AL,0xB892L,0x1F7CL,0x8EABL,0xB892L,0x5808L,65534UL,0xFEB1L,65535UL},{0x16F3L,65535UL,0x2E71L,0UL,65535UL,7UL,0UL,7UL,1UL}},{{0xB892L,0xA9CBL,0x5808L,0x31F8L,65535UL,0x26D4L,65534UL,65534UL,0x26D4L},{65534UL,7UL,0xA71BL,7UL,65534UL,65531UL,0xB586L,65526UL,1UL},{65531UL,0xA9CBL,0xB6D5L,8UL,0xFEB1L,1UL,0x31F8L,0xA9CBL,0xA400L}}};
            int i, j, k;
            for (g_481.f2 = 0; (g_481.f2 >= 16); g_481.f2 = safe_add_func_int16_t_s_s(g_481.f2, 4))
            { /* block id: 1197 */
                int32_t *l_2641 = &l_2264;
                int32_t *l_2644 = &g_44[2][5][1];
                int32_t *l_2645[6][1][10] = {{{&l_2191,&g_94[3],&l_2191,(void*)0,&l_2264,&g_94[2],&l_2266[0][5][5],&g_94[2],&l_2264,(void*)0}},{{&g_74,&l_2190,&g_74,(void*)0,&l_2266[0][5][5],&l_2572[1][0][0],&l_2191,&g_657,(void*)0,&g_657}},{{&g_74,&g_94[2],(void*)0,&g_94[3],(void*)0,&g_94[2],&g_74,&g_657,&l_2191,&l_2190}},{{&l_2191,&g_657,&g_74,&g_94[2],(void*)0,&g_94[3],(void*)0,&g_94[2],&g_74,&g_657}},{{(void*)0,&g_657,&l_2191,&l_2572[1][0][0],&l_2266[0][5][5],(void*)0,&g_74,&l_2190,&g_74,(void*)0}},{{&l_2264,&g_94[2],&l_2266[0][5][5],&g_94[2],&l_2264,(void*)0,&l_2191,&g_94[3],&l_2191,&l_2572[1][0][0]}}};
                int i, j, k;
                l_2646[2][1][2]--;
            }
        }
        else
        { /* block id: 1200 */
            uint16_t l_2653 = 0xE9F1L;
            for (l_2191 = 0; (l_2191 >= 18); l_2191++)
            { /* block id: 1203 */
                const int32_t *l_2651[8][4][6] = {{{&g_74,&l_2264,&g_94[3],&l_2264,&g_74,&g_94[3]},{&l_2264,&g_74,&g_94[3],&l_2571,&l_2571,&g_94[3]},{&l_2571,&l_2571,&g_94[3],&g_74,&l_2264,&g_94[3]},{&g_74,&l_2264,&g_94[3],&l_2264,&g_74,&g_94[3]}},{{&l_2264,&g_74,&g_94[3],&l_2571,&l_2571,&g_94[3]},{&l_2571,&l_2571,&g_94[3],&g_74,&l_2264,&g_94[3]},{&g_74,&l_2264,&g_94[3],&l_2264,&g_74,&g_94[3]},{&l_2264,&g_74,&g_94[3],&l_2571,&l_2571,&g_94[3]}},{{&l_2571,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571},{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571},{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571},{&l_2190,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571}},{{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571},{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571},{&l_2190,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571},{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571}},{{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571},{&l_2190,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571},{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571},{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571}},{{&l_2190,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571},{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571},{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571},{&l_2190,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571}},{{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571},{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571},{&l_2190,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571},{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571}},{{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571},{&l_2190,&l_2190,&l_2571,&l_2572[1][0][0],&l_2266[0][5][4],&l_2571},{&l_2572[1][0][0],&l_2266[0][5][4],&l_2571,&l_2266[0][5][4],&l_2572[1][0][0],&l_2571},{&l_2266[0][5][4],&l_2572[1][0][0],&l_2571,&l_2190,&l_2190,&l_2571}}};
                const int32_t **l_2652 = &l_2651[2][2][0];
                int i, j, k;
                (*l_2652) = l_2651[3][1][0];
                ++l_2653;
            }
        }
        (**g_655) &= 0xCE7F4204L;
    }
    (**g_672) = (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_sub_func_float_f_f(((safe_sub_func_float_f_f((+(+(safe_mul_func_float_f_f(((**g_672) != (safe_div_func_float_f_f((safe_sub_func_float_f_f(((((safe_add_func_float_f_f((0x8.1B11DBp+93 != 0x0.Ep+1), l_2674)) , l_2675[3][2]) , ((*l_2387) , &g_190)) != ((*l_2676) = &l_2675[3][2])), (**g_672))), (*g_673)))), 0x5.B35891p+82)))), l_2678)) != (*l_2387)), (**g_672))), (*l_2387))), 0x1.D79890p+60));
    return (*g_2467);
}


/* ------------------------------------------ */
/* 
 * reads : g_673 g_96
 * writes:
 */
static float  func_5(uint32_t  p_6, union U1  p_7, int8_t  p_8)
{ /* block id: 1026 */
    int8_t l_2172 = 0x78L;
    int32_t l_2173 = 0x1FFDC06DL;
    l_2172 |= (safe_lshift_func_uint16_t_u_s(p_8, p_8));
    l_2173 ^= (-6L);
    return (*g_673);
}


/* ------------------------------------------ */
/* 
 * reads : g_684.f2 g_859 g_860 g_232 g_88 g_655
 * writes: g_684.f2 g_88 g_654
 */
static uint8_t  func_16(union U1  p_17)
{ /* block id: 1007 */
    int32_t l_2164 = 5L;
    l_2164 = l_2164;
    for (g_684.f2 = 0; (g_684.f2 <= 4); g_684.f2 += 1)
    { /* block id: 1011 */
        return (**g_859);
    }
    for (g_88 = 18; (g_88 > 16); --g_88)
    { /* block id: 1016 */
        (*g_655) = &l_2164;
    }
    for (g_88 = 0; (g_88 != 32); g_88 = safe_add_func_uint32_t_u_u(g_88, 4))
    { /* block id: 1021 */
        (*g_655) = (void*)0;
        if (l_2164)
            continue;
    }
    return l_2164;
}


/* ------------------------------------------ */
/* 
 * reads : g_1362 g_683 g_634 g_244 g_44 g_1393 g_1394 g_154 g_1281 g_990 g_673 g_96 g_687.f0 g_1491 g_860 g_232 g_1115 g_359 g_1800 g_655 g_849.f2 g_1358 g_1359 g_350.f2 g_318.f2 g_376 g_302 g_88 g_94 g_657 g_859 g_456 g_1877 g_1878 g_2 g_1537 g_1538 g_1539 g_1540 g_998 g_999 g_1000 g_252 g_687.f2 g_300 g_301 g_190 g_897.f2 g_2006 g_2012 g_152 g_29 g_415 g_672 g_1226 g_2014 g_2106 g_2123 g_2129 g_329.f2 g_854.f2 g_352.f2 g_481.f2
 * writes: g_1700 g_1701 g_29 g_44 g_245 g_154 g_990 g_854.f2 g_252 g_352.f2 g_654 g_849.f2 g_481.f2 g_96 g_1045.f0 g_190 g_376 g_1394 g_1876 g_1226 g_1281 g_232 g_88 g_92 g_94 g_318.f2 g_657 g_1000 g_683 g_456 g_301 g_897.f2 g_152 g_1115 g_285 g_1617.f2 g_2123 g_656 g_893
 */
static union U1  func_18(int8_t  p_19, uint32_t  p_20)
{ /* block id: 775 */
    const uint16_t *l_1691[3][10][6] = {{{&g_634,&g_1226[0][1],(void*)0,(void*)0,&g_88,&g_92},{&g_92,(void*)0,(void*)0,&g_634,(void*)0,&g_92},{&g_634,(void*)0,&g_92,(void*)0,&g_1226[0][1],&g_92},{&g_92,&g_88,(void*)0,(void*)0,&g_634,&g_1226[0][1]},{&g_634,&g_1226[0][1],&g_634,&g_634,&g_1226[0][1],&g_1226[0][1]},{&g_92,&g_92,&g_92,(void*)0,&g_1226[0][1],(void*)0},{&g_634,(void*)0,&g_92,&g_634,&g_1226[0][1],&g_88},{&g_1226[0][1],&g_92,&g_1226[0][1],&g_92,&g_1226[0][1],&g_1226[0][1]},{&g_1226[1][2],&g_634,&g_634,&g_88,&g_634,&g_92},{&g_88,(void*)0,&g_92,&g_634,&g_88,&g_92}},{{&g_92,(void*)0,&g_634,&g_88,(void*)0,&g_1226[0][1]},{&g_88,&g_1226[1][2],&g_92,(void*)0,&g_1226[1][2],(void*)0},{&g_1226[1][2],&g_634,&g_634,&g_1226[0][1],&g_92,(void*)0},{&g_1226[1][2],&g_88,&g_1226[0][1],(void*)0,&g_1226[0][1],&g_92},{&g_88,&g_1226[1][2],&g_88,&g_88,&g_88,&g_92},{&g_92,(void*)0,&g_634,&g_634,&g_1226[1][2],&g_1226[0][1]},{&g_88,(void*)0,&g_634,&g_88,&g_88,&g_634},{&g_1226[1][2],&g_1226[1][2],&g_1226[0][1],&g_1226[1][1],&g_1226[0][1],&g_88},{&g_634,&g_88,(void*)0,&g_92,&g_92,&g_1226[0][1]},{&g_1226[1][2],&g_634,(void*)0,&g_1226[1][2],&g_1226[1][2],&g_88}},{{(void*)0,&g_1226[1][2],&g_1226[0][1],&g_634,(void*)0,&g_634},{&g_634,(void*)0,&g_634,&g_92,&g_88,&g_1226[0][1]},{&g_1226[1][1],(void*)0,&g_634,&g_92,&g_634,&g_92},{&g_634,&g_634,&g_88,&g_634,&g_634,&g_92},{(void*)0,&g_1226[1][1],&g_1226[0][1],&g_1226[1][2],(void*)0,(void*)0},{&g_1226[1][2],&g_92,&g_634,&g_92,(void*)0,(void*)0},{&g_634,&g_1226[1][1],&g_92,&g_1226[1][1],&g_634,&g_1226[0][1]},{&g_1226[1][2],&g_634,&g_634,&g_88,&g_634,&g_92},{&g_88,(void*)0,&g_92,&g_634,&g_88,&g_92},{&g_92,(void*)0,&g_634,&g_88,(void*)0,&g_1226[0][1]}}};
    uint8_t ****l_1697 = &g_858;
    uint8_t ****l_1699 = &g_858;
    uint8_t *****l_1698[8] = {(void*)0,(void*)0,&l_1699,(void*)0,(void*)0,&l_1699,(void*)0,(void*)0};
    union U0 * const l_1702 = &g_318;
    int32_t l_1703 = 0L;
    uint16_t *l_1708 = &g_1226[0][1];
    uint16_t ***l_1729 = &g_244;
    uint16_t ****l_1728 = &l_1729;
    int32_t l_1776 = 0L;
    int32_t l_1780 = 0xC8861C05L;
    int32_t l_1786 = 0xF5EF8831L;
    int32_t l_1788 = 0x09E46F65L;
    int32_t l_1789 = 0x734296F4L;
    int32_t l_1791 = 8L;
    int32_t l_1792 = 0xD4895408L;
    int32_t l_1793[2];
    uint8_t l_1830 = 0UL;
    int8_t l_1852 = 0x6BL;
    float l_1856 = 0x0.Fp+1;
    int32_t l_1857 = 0x0C1AE434L;
    uint64_t l_1863 = 0x620D8875AC38C8BELL;
    union U1 **l_1875 = &g_1046;
    union U1 ***l_1874 = &l_1875;
    union U1 **** const l_1873[10][6][4] = {{{&l_1874,(void*)0,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,(void*)0},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,(void*)0,&l_1874,(void*)0},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,(void*)0,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,(void*)0},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,(void*)0,&l_1874,(void*)0},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,(void*)0,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}},{{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874},{&l_1874,&l_1874,&l_1874,&l_1874}}};
    union U1 **** const *l_1872 = &l_1873[3][2][1];
    union U1 ****l_1883 = &l_1874;
    union U1 *****l_1882 = &l_1883;
    int8_t l_1890 = 9L;
    int32_t l_1960 = 0x3F37757AL;
    int8_t l_2046 = 1L;
    union U1 l_2161 = {0x0.80835Fp+80};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1793[i] = 7L;
lbl_2039:
    if ((safe_mod_func_int8_t_s_s((p_19 , (safe_add_func_int32_t_s_s(((safe_div_func_uint16_t_u_u(((((void*)0 == l_1691[0][1][5]) <= (((void*)0 != &l_1691[0][1][5]) > (safe_add_func_uint16_t_u_u((((+(safe_lshift_func_int16_t_s_u((l_1697 == (g_1701 = (g_1700 = l_1697))), 12))) , (*g_1362)) != l_1702), 0xECA5L)))) | 1L), l_1703)) | 0xD647723DL), l_1703))), p_19)))
    { /* block id: 778 */
        uint16_t * const **l_1726[10][1][3] = {{{&g_1000[0][4],(void*)0,&g_1000[0][4]}},{{&g_1000[0][4],&g_1000[1][4],&g_1000[0][4]}},{{(void*)0,&g_1000[0][4],(void*)0}},{{&g_1000[0][4],&g_1000[1][4],&g_1000[0][4]}},{{&g_1000[0][4],(void*)0,&g_1000[0][4]}},{{&g_1000[0][4],&g_1000[0][4],(void*)0}},{{&g_1000[0][4],&g_1000[0][4],&g_1000[0][4]}},{{&g_1000[0][4],&g_1000[0][4],&g_1000[0][4]}},{{&g_1000[0][4],&g_1000[0][4],&g_1000[0][4]}},{{&g_1000[0][4],&g_1000[0][4],&g_1000[0][4]}}};
        uint16_t * const ***l_1725 = &l_1726[0][0][0];
        uint16_t * const ****l_1724 = &l_1725;
        int32_t l_1742 = 0xBCA98B59L;
        int32_t l_1761 = 4L;
        int32_t l_1764 = 0x725FE877L;
        int32_t l_1782 = (-4L);
        int32_t l_1784 = 0L;
        int32_t l_1785 = 7L;
        int32_t l_1787 = (-4L);
        int32_t l_1790 = 1L;
        int32_t l_1794 = 0x0EF29E85L;
        int i, j, k;
        for (g_29 = 0; (g_29 <= 1); g_29 += 1)
        { /* block id: 781 */
            int32_t *l_1704 = &g_44[1][1][2];
            uint16_t ** const **l_1705 = (void*)0;
            uint64_t *l_1727 = &g_990[1];
            float l_1730 = 0x1.Fp+1;
            int32_t l_1777 = 0x1465327DL;
            int32_t l_1781[4];
            int i;
            for (i = 0; i < 4; i++)
                l_1781[i] = (-1L);
            (*l_1704) = (-4L);
            (*l_1704) = (((l_1705 == (((safe_add_func_int16_t_s_s((l_1703 && ((g_634 > p_19) || (l_1708 == ((*g_244) = (void*)0)))), (safe_mod_func_int8_t_s_s(((safe_div_func_float_f_f((((safe_sub_func_uint64_t_u_u(18446744073709551610UL, ((*l_1727) |= (safe_div_func_int8_t_s_s((safe_unary_minus_func_uint16_t_u(((safe_mul_func_uint8_t_u_u(((*l_1704) <= (safe_mul_func_int8_t_s_s(((safe_rshift_func_int16_t_s_u(((**g_1393) ^= (l_1724 == (void*)0)), p_19)) , g_1281[0]), p_19))), (-1L))) && 18446744073709551615UL))), 253UL))))) , l_1728) != (void*)0), (*g_673))) , p_20), p_20)))) & l_1703) , &g_1002[5][0])) , 65535UL) < (*l_1704));
            for (g_854.f2 = 1; (g_854.f2 >= 0); g_854.f2 -= 1)
            { /* block id: 789 */
                int16_t l_1778 = 9L;
                int32_t l_1779 = 9L;
                int32_t l_1783[7];
                uint32_t l_1795 = 0x7F7C53A6L;
                int i;
                for (i = 0; i < 7; i++)
                    l_1783[i] = 0L;
                for (g_252 = 0; (g_252 <= 1); g_252 += 1)
                { /* block id: 792 */
                    uint16_t l_1760 = 4UL;
                    int16_t l_1762[9][6] = {{0x4C5EL,0L,0x4C5EL,1L,0x4C5EL,0L},{0x5707L,0L,(-1L),0L,0x5707L,0L},{0x4C5EL,1L,0x4C5EL,0L,0x4C5EL,1L},{0x5707L,1L,(-1L),1L,0x5707L,1L},{0x4C5EL,0L,0x4C5EL,1L,0x4C5EL,0L},{0x5707L,0L,(-1L),0L,0x5707L,0L},{0x4C5EL,1L,0x4C5EL,0L,0x4C5EL,1L},{0x5707L,1L,(-1L),1L,0x5707L,1L},{0x4C5EL,0L,0x4C5EL,1L,0x4C5EL,0L}};
                    uint16_t ****l_1763 = (void*)0;
                    int32_t *l_1765 = &g_94[3];
                    int32_t *l_1766 = &g_94[3];
                    int32_t *l_1767 = &g_657;
                    int32_t *l_1768 = &l_1761;
                    int32_t *l_1769 = &g_44[0][0][3];
                    int32_t *l_1770 = &g_44[2][5][1];
                    int32_t *l_1771 = (void*)0;
                    int32_t *l_1772 = &g_94[3];
                    int32_t *l_1773 = &g_44[5][1][0];
                    int32_t l_1774 = 1L;
                    int32_t *l_1775[4][3][10] = {{{&l_1761,&l_1703,&l_1761,&g_657,&g_44[5][1][0],&g_94[3],&g_657,&g_94[0],&g_44[5][1][0],&g_94[3]},{&g_44[5][1][0],&g_657,(void*)0,&g_94[3],&g_44[5][0][2],(void*)0,&l_1703,&g_94[0],&g_44[5][1][0],(void*)0},{&g_44[0][4][1],(void*)0,&l_1761,&l_1703,&g_44[5][1][0],&l_1703,&l_1761,(void*)0,&g_44[0][4][1],(void*)0}},{{&g_44[0][4][1],&g_94[3],&g_657,&g_94[1],(void*)0,(void*)0,&g_44[5][1][0],&l_1703,&l_1703,(void*)0},{&g_44[5][1][0],(void*)0,(void*)0,&g_94[1],&g_657,&g_94[3],&g_44[0][4][1],&g_94[3],&g_44[0][4][1],&g_94[3]},{&l_1761,&l_1703,&g_44[5][1][0],&l_1703,&l_1761,(void*)0,&g_44[0][4][1],(void*)0,&g_44[5][1][0],&l_1761}},{{&l_1703,(void*)0,&g_44[5][0][2],&g_94[3],(void*)0,&g_657,&g_44[5][1][0],&g_94[1],&g_44[5][1][0],&l_1761},{&g_657,&g_94[3],&g_44[5][1][0],&g_657,&l_1761,&l_1703,&l_1761,&g_657,&g_44[5][1][0],&g_94[3]},{&g_44[5][0][2],(void*)0,&g_44[5][1][0],&g_657,&g_657,&g_657,&l_1703,&g_94[1],&g_74,(void*)0}},{{&g_44[5][1][0],&g_657,&g_44[5][0][2],&l_1761,(void*)0,&g_657,&g_657,(void*)0,&g_657,(void*)0},{&g_44[5][0][2],&l_1703,&g_44[5][1][0],(void*)0,&g_44[5][1][0],&l_1703,&g_44[5][0][2],&g_94[3],&g_44[5][1][0],(void*)0},{&l_1761,(void*)0,&g_44[0][4][1],(void*)0,&g_44[5][1][0],&l_1761,&g_44[5][1][0],&l_1703,&l_1742,&g_657}}};
                    int i, j, k;
                    l_1764 = (safe_add_func_int8_t_s_s((((*l_1704) == (+(65535UL ^ (((safe_mod_func_int16_t_s_s((safe_sub_func_uint8_t_u_u((l_1742 = (safe_rshift_func_uint8_t_u_s(p_19, p_19))), (((l_1704 == ((safe_add_func_int32_t_s_s(p_19, ((safe_add_func_uint32_t_u_u(((((((*l_1727) = ((safe_div_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u(1UL, ((safe_mul_func_int8_t_s_s(((safe_lshift_func_int16_t_s_u((safe_sub_func_uint64_t_u_u(p_20, (safe_mod_func_int32_t_s_s((!((*g_1394) = ((l_1760 >= 0x9DL) || 0x9CF63E65L))), p_19)))), l_1761)) < g_687.f0), (**g_1491))) , p_20))), p_20)) > p_19)) <= p_20) , (*l_1704)) , 0x8142690B57DABAD5LL) , 4294967295UL), l_1762[3][5])) >= (-8L)))) , (void*)0)) > 0x65EAL) == g_1115[3]))), l_1703)) , l_1763) == &g_1002[6][4])))) < g_359), (*g_860)));
                    l_1795--;
                }
                (*l_1704) ^= l_1779;
            }
        }
    }
    else
    { /* block id: 802 */
        uint64_t *l_1799[6][2] = {{&g_990[1],&g_990[1]},{&g_990[1],&g_1281[0]},{&g_1281[0],(void*)0},{&g_1281[0],(void*)0},{&g_1281[0],&g_1281[0]},{&g_990[1],&g_990[1]}};
        uint64_t **l_1798 = &l_1799[3][1];
        union U1 l_1801 = {0x2.Ap+1};
        int i, j;
        if ((l_1798 != g_1800))
        { /* block id: 803 */
            return l_1801;
        }
        else
        { /* block id: 805 */
            int32_t *l_1805 = &g_44[2][5][3];
            for (g_352.f2 = 6; (g_352.f2 != (-17)); --g_352.f2)
            { /* block id: 808 */
                int32_t *l_1804 = (void*)0;
                (*g_655) = l_1805;
            }
        }
        (*g_655) = (void*)0;
    }
    if (p_19)
    { /* block id: 814 */
        uint8_t l_1828[3];
        int32_t l_1829[5][5][7] = {{{0xE1FCC962L,9L,5L,9L,0xE1FCC962L,(-6L),0x190048E5L},{0xCA2238D6L,8L,0x9A022FE5L,(-9L),(-1L),0xCA2238D6L,0x458BDA61L},{0xB3C7B03DL,(-1L),0x853592DFL,0xB9E75436L,(-8L),1L,(-8L)},{0xCA2238D6L,(-9L),0xC5C1E2E0L,0xD151706FL,8L,0L,(-9L)},{0xE1FCC962L,0xE621CD00L,0x190048E5L,0L,(-5L),0xE621CD00L,(-7L)}},{{0xA23B7009L,(-1L),0x6938CD9AL,(-9L),0x0BF3633DL,0x0BF3633DL,(-9L)},{(-1L),3L,(-1L),1L,0xB3C7B03DL,1L,(-8L)},{(-1L),1L,0x6032188AL,0xF8994BF9L,1L,0xC5C1E2E0L,0x458BDA61L},{0x9FFE85A6L,9L,0x190048E5L,1L,0x567C4842L,1L,0x190048E5L},{0x0BF3633DL,0xA23B7009L,(-9L),0x6032188AL,(-1L),0x0BF3633DL,0x7C59AF75L}},{{(-5L),1L,0x853592DFL,9L,(-5L),0xE621CD00L,0xB3C7B03DL},{0xCA2238D6L,1L,0L,(-1L),(-1L),0L,1L},{(-5L),(-1L),5L,0L,0x567C4842L,1L,0x5FDE8300L},{0xD151706FL,(-1L),0xC5C1E2E0L,0x9A022FE5L,1L,0xCA2238D6L,(-9L)},{(-5L),(-6L),0xB3C7B03DL,0L,0xB3C7B03DL,(-6L),(-5L)}},{{0xA23B7009L,(-9L),0x6032188AL,(-1L),0x0BF3633DL,0x7C59AF75L,0xC5C1E2E0L},{0x9FFE85A6L,3L,0x4D935B97L,9L,(-5L),1L,0x5FDE8300L},{1L,8L,0x6032188AL,0x6032188AL,8L,1L,0x458BDA61L},{(-1L),1L,0xB3C7B03DL,1L,(-8L),0x008979ACL,0xB3C7B03DL},{0x0BF3633DL,0x5C115D09L,0xC5C1E2E0L,0xF8994BF9L,(-1L),0x7C59AF75L,0x5C115D09L}},{{0xE1FCC962L,1L,5L,1L,0xE1FCC962L,0xE621CD00L,0x190048E5L},{0xD151706FL,8L,0L,(-9L),1L,0xCA2238D6L,1L},{0xB3C7B03DL,3L,0x853592DFL,0L,(-8L),9L,(-8L)},{0xD151706FL,(-9L),(-9L),0xD151706FL,1L,0x6938CD9AL,0L},{(-5L),9L,0x4D935B97L,3L,0x9FFE85A6L,9L,0x190048E5L}}};
        uint64_t *l_1866[4][4][7] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_1281[0],(void*)0},{&g_990[0],&g_1281[0],&g_990[0],(void*)0,&g_990[1],&g_990[0],&g_1281[0]},{&g_990[1],&g_1281[0],&g_456[4][3][3],&g_456[4][3][3],&g_1281[0],&g_990[1],&g_1281[0]},{&g_990[0],(void*)0,&g_456[4][0][1],&g_990[0],&g_1281[0],&g_1281[0],(void*)0}},{{(void*)0,&g_990[0],&g_990[1],&g_456[2][2][0],&g_990[1],&g_990[0],(void*)0},{&g_990[0],(void*)0,&g_456[4][3][3],&g_990[1],(void*)0,&g_990[0],&g_990[1]},{&g_990[0],&g_1281[0],&g_1281[0],(void*)0,(void*)0,&g_1281[0],&g_1281[0]},{(void*)0,&g_1281[0],&g_456[4][3][3],&g_456[2][2][0],&g_1281[0],&g_990[1],&g_1281[0]}},{{&g_990[0],(void*)0,&g_990[1],&g_990[0],&g_1281[0],&g_990[0],&g_990[1]},{(void*)0,(void*)0,&g_456[4][0][1],&g_456[2][2][0],(void*)0,&g_1281[0],(void*)0},{(void*)0,&g_990[1],&g_456[4][3][3],(void*)0,&g_990[0],&g_990[0],(void*)0},{&g_990[0],&g_1281[0],&g_990[0],&g_990[1],(void*)0,&g_990[0],&g_1281[0]}},{{(void*)0,&g_1281[0],(void*)0,&g_456[2][2][0],&g_1281[0],&g_456[4][0][1],&g_1281[0]},{&g_990[0],&g_990[1],&g_990[1],&g_990[0],&g_1281[0],&g_990[0],(void*)0},{&g_990[0],(void*)0,&g_990[1],&g_456[4][3][3],(void*)0,&g_990[0],&g_990[0]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_1281[0],(void*)0}}};
        int32_t l_1867 = 0L;
        union U1 ****l_1881 = &l_1874;
        union U1 *****l_1880 = &l_1881;
        int32_t l_1891 = (-4L);
        const uint32_t *l_1900[3][1];
        const uint32_t **l_1899[4][5] = {{&l_1900[2][0],&l_1900[2][0],(void*)0,(void*)0,&l_1900[2][0]},{&l_1900[1][0],&l_1900[0][0],&l_1900[1][0],&l_1900[0][0],&l_1900[1][0]},{&l_1900[2][0],(void*)0,(void*)0,&l_1900[2][0],&l_1900[2][0]},{&l_1900[2][0],&l_1900[0][0],&l_1900[2][0],&l_1900[0][0],&l_1900[2][0]}};
        const int32_t *l_1915[4][4] = {{&l_1788,&l_1786,&l_1788,(void*)0},{&l_1788,(void*)0,(void*)0,&l_1788},{&g_44[5][1][0],(void*)0,&l_1703,(void*)0},{(void*)0,&l_1786,&l_1703,&l_1703}};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1828[i] = 1UL;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_1900[i][j] = &g_1581;
        }
        for (g_849.f2 = 1; (g_849.f2 >= 0); g_849.f2 -= 1)
        { /* block id: 817 */
            float l_1826 = 0x1.4p+1;
            int32_t *l_1827[5];
            uint32_t *l_1838 = &g_1390[6];
            float l_1849 = 0x0.BC7177p+87;
            uint32_t l_1860 = 0x69FDEB0AL;
            int i;
            for (i = 0; i < 5; i++)
                l_1827[i] = &g_44[5][1][0];
            for (g_481.f2 = 0; (g_481.f2 <= 8); g_481.f2 += 1)
            { /* block id: 820 */
                uint32_t l_1822 = 0xC9DA391CL;
                int32_t l_1824 = 0x7DB19163L;
                int32_t l_1845 = 0L;
                int32_t l_1846 = 0x4605FFDDL;
                float l_1847 = 0x8.8p+1;
                int32_t l_1848 = 0x8A7EAFC4L;
                int32_t l_1850 = 0x074BEB01L;
                int32_t l_1851 = 0L;
                int32_t l_1853 = 0xFA75E6E3L;
                int32_t l_1854 = 0L;
                int32_t l_1855[3];
                float l_1858 = (-0x1.1p-1);
                int8_t l_1859 = 0xE1L;
                int i;
                for (i = 0; i < 3; i++)
                    l_1855[i] = 1L;
                if ((safe_div_func_uint16_t_u_u(0UL, g_990[g_849.f2])))
                { /* block id: 821 */
                    float *l_1823 = &g_1045.f0;
                    int32_t l_1825 = 0x8CBF5BE7L;
                    if ((((((((safe_add_func_float_f_f((((*g_673) = (*g_673)) <= (safe_sub_func_float_f_f(0x1.9p-1, p_20))), (safe_mul_func_float_f_f((safe_mul_func_float_f_f(((safe_sub_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f((l_1824 = ((*l_1823) = l_1822)), 0x1.Ap+1)), (l_1825 > p_19))), 0x6.62A606p+39)) >= l_1780), l_1822)), l_1826)))) == g_687.f0) == p_19) > 0x2.892BC8p+14) > (-0x10.Ap-1)) == (-0x1.3p-1)) , 2L))
                    { /* block id: 825 */
                        (*g_655) = l_1827[3];
                    }
                    else
                    { /* block id: 827 */
                        if (p_20)
                            break;
                        if (l_1828[0])
                            break;
                        ++l_1830;
                    }
                }
                else
                { /* block id: 832 */
                    int64_t *l_1837 = &g_190;
                    int32_t l_1844 = 0x3A3C13D5L;
                    l_1786 = (((safe_add_func_int8_t_s_s(((l_1786 , (safe_add_func_int32_t_s_s((p_19 < ((((0xA6B554E8EB82CF32LL & ((*l_1837) = (*g_1358))) , &p_20) != (l_1838 = &g_1390[4])) == g_350.f2)), (safe_div_func_int64_t_s_s((((~(p_19 && ((((*g_1394) = g_990[g_849.f2]) < 0UL) < g_359))) ^ 18446744073709551615UL) < 0UL), 0x6837E3FB9167E3B0LL))))) && l_1793[1]), g_318.f2)) != l_1844) ^ 1UL);
                }
                if (p_19)
                    break;
                l_1860--;
                l_1829[2][4][6] |= p_19;
            }
            for (g_376 = 1; (g_376 <= 8); g_376 += 1)
            { /* block id: 844 */
                int i;
                l_1863 ^= g_302[(g_376 + 1)];
            }
        }
        l_1891 ^= ((((safe_mod_func_int16_t_s_s(((l_1703 = (((p_19 , (l_1829[3][2][1] = 0UL)) > p_19) & (l_1867 = l_1828[0]))) < ((((*g_1393) = l_1708) == ((safe_div_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_s(((g_1876 = l_1872) != (l_1882 = l_1880)), 10)) & (safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s((p_19 != ((*l_1708) = ((l_1828[1] , l_1857) ^ p_19))), 5L)), l_1890)), 0x24L))), 0xACF5L)) , l_1691[1][1][1])) >= p_19)), l_1863)) == p_20) != p_19) && 8L);
        for (l_1703 = 0; (l_1703 > 8); ++l_1703)
        { /* block id: 858 */
            const uint32_t ***l_1898[4];
            int32_t l_1910 = 0x0161E5EBL;
            int32_t *l_1934[9] = {&l_1891,&l_1891,&l_1891,&l_1891,&l_1891,&l_1891,&l_1891,&l_1891,&l_1891};
            int i;
            for (i = 0; i < 4; i++)
                l_1898[i] = (void*)0;
            if (p_20)
                break;
            if ((safe_mul_func_int8_t_s_s((g_1115[2] , (((0x78C83DE02E7D2C7DLL == ((safe_rshift_func_int8_t_s_s((((l_1899[3][3] = (void*)0) != (void*)0) & ((+(((safe_div_func_uint8_t_u_u(((*g_860) ^= ((safe_mod_func_uint32_t_u_u(((safe_rshift_func_uint16_t_u_s((3L <= (-3L)), 0)) <= (l_1793[0] <= ((((safe_mul_func_int8_t_s_s((0x9F08L & (((++g_1281[0]) != (safe_lshift_func_int16_t_s_s(0x8157L, p_20))) <= p_19)), p_20)) || 0xD61733DCBF953C62LL) , 0x097A65BEL) , 0L))), p_19)) < 0xAA93L)), l_1857)) > p_20) , (-1L))) == l_1830)), 1)) || l_1867)) <= 0xB2F68584L) != l_1910)), g_1359[1])))
            { /* block id: 863 */
                const uint32_t l_1925[1] = {18446744073709551607UL};
                int32_t l_1929[10] = {0xA8DDC083L,0x97BDF0E4L,0xA8DDC083L,0xA8DDC083L,0x97BDF0E4L,0xA8DDC083L,0xA8DDC083L,0x97BDF0E4L,0xA8DDC083L,0xA8DDC083L};
                union U1 l_1933 = {0x0.8F1EF4p+35};
                int i;
                l_1915[0][3] = func_77(&l_1829[2][4][6]);
                if (p_19)
                    continue;
                for (g_318.f2 = 7; (g_318.f2 >= 0); g_318.f2 -= 1)
                { /* block id: 868 */
                    union U1 ***l_1921[2];
                    int32_t l_1928 = 9L;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1921[i] = (void*)0;
                    for (g_657 = 0; (g_657 <= 3); g_657 += 1)
                    { /* block id: 871 */
                        int8_t *l_1918 = &g_352.f2;
                        union U1 ***l_1922 = &l_1875;
                        int32_t *l_1926 = &l_1891;
                        int32_t *l_1927[4];
                        uint8_t l_1930 = 0UL;
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                            l_1927[i] = &l_1788;
                        if (l_1829[(g_657 + 1)][(g_657 + 1)][(g_657 + 1)])
                            break;
                        l_1829[(g_657 + 1)][(g_657 + 1)][(g_657 + 3)] |= (((--(**g_859)) > (((*l_1918) = ((void*)0 != &g_1490[9])) || 0xD9L)) < ((((safe_div_func_float_f_f(g_456[(g_657 + 1)][g_657][(g_657 + 1)], ((*g_1877) == l_1921[1]))) , l_1922) == ((**l_1882) = (*g_1877))) == (safe_div_func_uint8_t_u_u(l_1925[0], p_19))));
                        --l_1930;
                    }
                    return l_1933;
                }
            }
            else
            { /* block id: 881 */
                l_1910 |= 7L;
            }
            if (p_19)
                continue;
            l_1788 ^= (g_2[1] , l_1793[0]);
        }
    }
    else
    { /* block id: 887 */
        union U0 *l_1937 = &g_897[1][4];
        int32_t l_1961 = 0x68CD6DE0L;
        uint16_t **l_1965 = &l_1708;
        uint32_t l_1985 = 0x23B97440L;
        uint32_t l_2005[10][1][8] = {{{0xAAED44BAL,0xAAED44BAL,4294967287UL,0UL,0x7C6807E8L,0x368F91F5L,0xAAED44BAL,0x7C6807E8L}},{{1UL,0x7C6807E8L,0x26DE07EAL,1UL,0x94B18B9BL,1UL,4294967295UL,1UL}},{{1UL,0x368F91F5L,0x6264E48BL,0UL,0x368F91F5L,4294967295UL,0xDCE00B3AL,0x6264E48BL}},{{0xB8D57935L,0xDCE00B3AL,0UL,1UL,1UL,0UL,0xDCE00B3AL,0xB8D57935L}},{{4294967287UL,1UL,0x6264E48BL,4294967295UL,0x26DE07EAL,4294967287UL,4294967295UL,0UL}},{{0x26DE07EAL,4294967287UL,4294967295UL,0UL,4294967295UL,4294967287UL,0x26DE07EAL,4294967295UL}},{{0xB8D57935L,1UL,0x7F55021DL,0xB8D57935L,0xDCE00B3AL,0UL,1UL,1UL}},{{4294967295UL,0xDCE00B3AL,0x6264E48BL,0x6264E48BL,0xDCE00B3AL,4294967295UL,0x368F91F5L,0UL}},{{0xB8D57935L,0x368F91F5L,0x4F734CF5L,1UL,4294967295UL,0x4F734CF5L,0xDCE00B3AL,0x4F734CF5L}},{{0x26DE07EAL,1UL,0x94B18B9BL,1UL,0x26DE07EAL,0x7C6807E8L,1UL,0UL}}};
        union U1 l_2060[2][5] = {{{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22}},{{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22},{0xD.AEAF8Bp+22}}};
        int32_t l_2118 = 0xB9723B93L;
        int32_t l_2120 = 0x4E3EAE64L;
        int32_t l_2121 = 0L;
        uint8_t l_2130 = 0xC5L;
        int i, j, k;
        for (g_154 = 0; (g_154 < (-1)); g_154--)
        { /* block id: 890 */
            l_1937 = l_1937;
            for (g_190 = (-8); (g_190 > (-20)); g_190 = safe_sub_func_uint16_t_u_u(g_190, 6))
            { /* block id: 894 */
                return (***g_1537);
            }
        }
        for (l_1780 = 1; (l_1780 <= 17); ++l_1780)
        { /* block id: 900 */
            union U0 *l_1947[5][5] = {{(void*)0,&g_687,&g_897[1][7],(void*)0,&g_897[1][7]},{&g_897[1][7],&g_897[1][7],&g_684,(void*)0,&g_687},{&g_687,(void*)0,(void*)0,&g_687,&g_897[1][7]},{&g_687,(void*)0,&g_350,&g_350,(void*)0},{&g_897[1][7],(void*)0,&g_350,&g_684,&g_684}};
            union U1 l_1948 = {-0x1.Dp+1};
            int32_t l_1949 = (-1L);
            union U0 ***l_2025 = &g_1362;
            const int16_t *l_2030 = &g_154;
            const int16_t **l_2029 = &l_2030;
            const int16_t ***l_2028[2];
            const int16_t ****l_2027 = &l_2028[1];
            int32_t l_2031[7][6][2] = {{{0xEAD6B962L,(-5L)},{(-5L),0x7A3C9496L},{0x01729BA5L,0xEAD6B962L},{1L,0xCA7911AAL},{(-5L),0xCA7911AAL},{1L,0xEAD6B962L}},{{0x01729BA5L,0x7A3C9496L},{(-5L),(-5L)},{0xEAD6B962L,(-5L)},{0xAE98EFA7L,0x66765294L},{6L,0x0B0237B7L},{0x47FAF62DL,6L}},{{0x530046D1L,1L},{0x530046D1L,6L},{0x47FAF62DL,0x0B0237B7L},{6L,0x66765294L},{0xAE98EFA7L,(-5L)},{0xEAD6B962L,(-5L)}},{{(-5L),0x7A3C9496L},{0x01729BA5L,0xEAD6B962L},{1L,0xCA7911AAL},{(-5L),0xCA7911AAL},{1L,0xEAD6B962L},{0x01729BA5L,0x7A3C9496L}},{{(-5L),(-5L)},{0xEAD6B962L,(-5L)},{0xAE98EFA7L,0x66765294L},{6L,0x0B0237B7L},{0x47FAF62DL,6L},{0x530046D1L,1L}},{{0x530046D1L,6L},{0x47FAF62DL,0x0B0237B7L},{6L,0x66765294L},{0xAE98EFA7L,(-5L)},{0xEAD6B962L,(-5L)},{(-5L),0x7A3C9496L}},{{0x01729BA5L,0xEAD6B962L},{1L,0xCA7911AAL},{(-5L),0xCA7911AAL},{1L,0xEAD6B962L},{0x01729BA5L,0x7A3C9496L},{(-5L),(-5L)}}};
            int32_t l_2059 = 1L;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_2028[i] = &l_2029;
            for (l_1890 = 2; (l_1890 <= 8); l_1890 += 1)
            { /* block id: 903 */
                for (g_481.f2 = 0; (g_481.f2 <= 7); g_481.f2 += 1)
                { /* block id: 906 */
                    int16_t ****l_1942 = (void*)0;
                    int16_t ***l_1944 = &g_1393;
                    int16_t ****l_1943 = &l_1944;
                    (*l_1943) = &g_1393;
                }
            }
            for (g_190 = 6; (g_190 < 3); g_190 = safe_sub_func_int8_t_s_s(g_190, 2))
            { /* block id: 912 */
                int16_t l_1986 = 0x87D0L;
                int32_t l_2032 = 0x3D660D82L;
                uint64_t *l_2035 = (void*)0;
                if (p_20)
                { /* block id: 913 */
                    (**g_998) = (**g_998);
                    (*g_1362) = l_1947[4][3];
                    if (l_1857)
                        break;
                }
                else
                { /* block id: 917 */
                    return l_1948;
                }
                for (g_352.f2 = 4; (g_352.f2 >= 1); g_352.f2 -= 1)
                { /* block id: 922 */
                    int32_t l_1950 = 0xE7C01311L;
                    int32_t *l_1962 = &l_1786;
                    uint64_t *l_2034[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_2034[i] = &g_1281[0];
                    if (((*l_1962) = (l_1949 | (l_1950 <= (safe_add_func_int64_t_s_s((((((safe_unary_minus_func_int32_t_s(0x99CA8373L)) > ((safe_mod_func_uint64_t_u_u(p_20, l_1949)) , (p_20 != ((0x00L & ((*g_1394) < (safe_sub_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((p_19 && p_19), l_1960)), p_19)))) == l_1961)))) || g_88) && 0x9EAB4E28E1BC3ABCLL) > 9UL), 1UL))))))
                    { /* block id: 924 */
                        uint32_t *l_1978 = &g_252;
                        uint64_t *l_1981 = (void*)0;
                        uint64_t *l_1982 = &g_456[4][0][1];
                        l_1986 &= (((safe_add_func_uint8_t_u_u(((void*)0 == l_1965), (safe_rshift_func_int16_t_s_s(((((((p_20 > ((safe_lshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(((safe_mod_func_int64_t_s_s((safe_add_func_int8_t_s_s((((*l_1982) |= (0xE2L & ((0x84E6L >= (safe_add_func_uint32_t_u_u(((*l_1978)++), ((void*)0 == g_860)))) <= g_687.f2))) & ((safe_add_func_uint16_t_u_u(0xBAA7L, l_1961)) , (*l_1962))), 2UL)), p_20)) || l_1961), 0x5C98L)), p_19)) == 0xAA7427170C20480ELL)) , p_20) <= g_657) , 0x61L) >= l_1776) & p_20), (*g_1394))))) <= (*l_1962)) | l_1985);
                    }
                    else
                    { /* block id: 928 */
                        int8_t *l_1999 = (void*)0;
                        int32_t l_2007 = 0L;
                        int8_t *l_2026[9][10][2] = {{{&g_220.f2,(void*)0},{(void*)0,&g_684.f2},{&l_1890,&g_318.f2},{&l_1890,&g_318.f2},{(void*)0,&g_318.f2},{(void*)0,&g_318.f2},{&l_1890,&g_220.f2},{&g_318.f2,&l_1890},{&g_687.f2,&g_847[6].f2},{&l_1852,&g_511.f2}},{{&g_511.f2,&g_352.f2},{(void*)0,&l_1852},{&g_318.f2,&g_350.f2},{&g_847[6].f2,&g_350.f2},{&g_318.f2,&l_1852},{(void*)0,&g_352.f2},{&g_511.f2,&g_511.f2},{&l_1852,&g_847[6].f2},{&g_687.f2,&l_1890},{&g_318.f2,&g_220.f2}},{{&l_1890,&g_318.f2},{(void*)0,&g_318.f2},{(void*)0,&g_318.f2},{&l_1890,&g_220.f2},{&g_318.f2,&l_1890},{&g_687.f2,&g_847[6].f2},{&l_1852,&g_511.f2},{&g_511.f2,&g_352.f2},{(void*)0,&l_1852},{&g_318.f2,&g_350.f2}},{{&g_847[6].f2,&g_350.f2},{&g_318.f2,&l_1852},{(void*)0,&g_352.f2},{&g_511.f2,&g_511.f2},{&l_1852,&g_847[6].f2},{&g_687.f2,&l_1890},{&g_318.f2,&g_220.f2},{&l_1890,&g_318.f2},{(void*)0,&g_318.f2},{(void*)0,&g_318.f2}},{{&l_1890,&g_220.f2},{&g_318.f2,&l_1890},{&g_687.f2,&g_847[6].f2},{&l_1852,&g_511.f2},{&g_511.f2,&g_352.f2},{(void*)0,&l_1852},{&g_318.f2,&g_350.f2},{&g_847[6].f2,&g_350.f2},{&g_318.f2,&l_1852},{(void*)0,&g_352.f2}},{{&g_511.f2,&g_511.f2},{&l_1852,&g_847[6].f2},{&g_687.f2,&l_1890},{&g_318.f2,&g_220.f2},{&l_1890,&g_318.f2},{(void*)0,&g_318.f2},{(void*)0,&g_318.f2},{&l_1890,&g_220.f2},{&g_318.f2,&l_1890},{&g_687.f2,&g_847[6].f2}},{{&l_1852,&g_511.f2},{&g_511.f2,&g_352.f2},{(void*)0,&l_1852},{&g_318.f2,&g_350.f2},{&g_847[6].f2,&g_350.f2},{&g_318.f2,&l_1852},{(void*)0,&g_352.f2},{&g_511.f2,&g_511.f2},{&l_1852,&g_847[6].f2},{&g_687.f2,&l_1890}},{{&g_318.f2,&g_220.f2},{&l_1890,&g_318.f2},{(void*)0,&g_318.f2},{(void*)0,&g_318.f2},{&l_1890,&g_220.f2},{&g_318.f2,&l_1890},{&g_687.f2,&g_847[6].f2},{&l_1852,&g_511.f2},{&g_511.f2,&g_352.f2},{(void*)0,&l_1852}},{{&g_318.f2,&g_350.f2},{&g_847[6].f2,&g_350.f2},{&g_318.f2,&l_1852},{(void*)0,&g_352.f2},{&g_511.f2,&g_511.f2},{&l_1852,&g_847[6].f2},{&g_687.f2,&l_1890},{&g_318.f2,&g_220.f2},{&l_1890,&g_318.f2},{(void*)0,&g_318.f2}}};
                        int32_t l_2033 = 8L;
                        int32_t *l_2036 = &l_1791;
                        int32_t *l_2037 = (void*)0;
                        int32_t *l_2038 = &l_1960;
                        int i, j, k;
                        (*l_1962) = l_1789;
                        (*g_300) = (*g_300);
                        (*l_2038) |= (((*l_2036) ^= ((((safe_rshift_func_int16_t_s_s(0x7790L, ((safe_lshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s(((((l_2032 = (safe_mod_func_uint8_t_u_u(((safe_add_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((g_897[1][4].f2 |= g_190), (safe_add_func_int32_t_s_s((+((safe_mul_func_uint8_t_u_u(l_2005[0][0][5], (g_1359[1] <= ((l_1786 ^= (g_2006 , (--(**g_1491)))) ^ (safe_mul_func_int16_t_s_s(p_20, (l_1949 ^= (g_2012 == ((safe_mod_func_int32_t_s_s(p_20, ((l_2007 |= ((((((safe_rshift_func_int8_t_s_s((((safe_rshift_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_s(1UL, 5)) ^ p_19), (-1L))), 13)) < 0xD2L) > 0x37L), 5)) >= (**g_1393)) < 1UL) & p_19) , l_2025) != &g_1362)) && 0x97L))) , l_2027))))))))) , p_20)), 4294967295UL)))), l_1788)) > 0x10A59EC5B8B3E057LL), l_2031[0][1][0]))) , 0x6A4405924C4C049BLL) , p_19) && l_1986), g_152)), l_2033)) , p_19))) && 0x4305L) , l_2034[4]) == l_2035)) != g_152);
                    }
                    if (l_1791)
                        goto lbl_2039;
                }
                for (g_854.f2 = (-6); (g_854.f2 >= 26); g_854.f2 = safe_add_func_uint8_t_u_u(g_854.f2, 1))
                { /* block id: 944 */
                    const int64_t l_2051 = 1L;
                    for (g_152 = (-4); (g_152 >= 16); g_152 = safe_add_func_uint8_t_u_u(g_152, 1))
                    { /* block id: 947 */
                        uint32_t *l_2055 = &l_2005[0][0][5];
                        int32_t *l_2056[6] = {&l_1791,&l_1791,&l_1791,&l_1791,&l_1791,&l_1791};
                        int i;
                        l_2032 = (safe_rshift_func_int8_t_s_u(g_29, ((l_1986 < (((p_20 || l_2046) | (((g_415 , (safe_rshift_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s(l_1789, l_2051)), (safe_lshift_func_uint16_t_u_u((+p_19), 15))))) != (((*l_2055) = g_990[0]) , p_20)) > l_2032)) & l_1985)) > p_19)));
                        l_1789 = 0xE6CC9615L;
                    }
                    l_1960 &= (safe_rshift_func_uint16_t_u_s(p_20, p_20));
                }
            }
            if (l_2059)
                continue;
        }
        if (((g_1115[3] = p_19) ^ l_1961))
        { /* block id: 958 */
            int16_t l_2082 = 0x7D2EL;
            union U0 **l_2093[3];
            int32_t l_2112[9][6][4] = {{{0xE410C120L,0L,1L,0L},{0x632D8BD4L,(-7L),7L,0x191F7904L},{0x06BFC5A8L,0xEC8E4ABEL,(-1L),(-1L)},{(-1L),0L,0x21CD56BAL,0x8F9964C5L},{0x6A5B343BL,(-3L),0L,(-6L)},{0x191F7904L,0L,0x52C7C266L,(-1L)}},{{0x06BFC5A8L,0xA65BA376L,0L,(-6L)},{0x974A3734L,0xEC240663L,(-1L),0x21CD56BAL},{0xA37B6947L,0x112716B3L,0x585E51ADL,0x0C17708EL},{0xEC240663L,1L,(-3L),0xD1717BABL},{0x52C7C266L,0x191F7904L,0L,5L},{(-6L),(-3L),0x06BFC5A8L,0x838BF04EL}},{{0L,(-6L),(-1L),0xDD5E3385L},{0L,(-9L),0L,0x44558BD0L},{0x974A3734L,0xBA4B9792L,0x21CD56BAL,(-6L)},{(-1L),0x70B4FB0DL,5L,0xBA4B9792L},{0x15B2499BL,0L,5L,0xD1717BABL},{(-1L),9L,0x21CD56BAL,(-7L)}},{{0x974A3734L,0L,0L,(-1L)},{0L,(-1L),(-1L),0x0C17708EL},{0L,0xA65BA376L,0x06BFC5A8L,(-1L)},{(-6L),0x15B2499BL,0L,0xC49D043EL},{0x52C7C266L,0x70B4FB0DL,(-3L),0x414FCB22L},{0xEC240663L,(-6L),0x585E51ADL,(-1L)}},{{0xA37B6947L,0xA6B53F91L,(-1L),0x632D8BD4L},{0x974A3734L,0L,0L,0x838BF04EL},{0x06BFC5A8L,1L,0x52C7C266L,0xBA4B9792L},{0x191F7904L,3L,0L,(-1L)},{0x6A5B343BL,(-1L),0x21CD56BAL,(-6L)},{(-1L),0x112716B3L,(-1L),0xEC240663L}},{{(-1L),(-1L),0x585E51ADL,1L},{0x112716B3L,9L,0L,(-1L)},{(-6L),0x191F7904L,0x191F7904L,(-6L)},{0L,1L,(-1L),0x414FCB22L},{0L,0x52C7C266L,(-6L),0x44558BD0L},{0xD1717BABL,0L,(-1L),0x44558BD0L}},{{(-1L),0x52C7C266L,0x019B4EDFL,0x414FCB22L},{1L,1L,5L,(-6L)},{3L,0x191F7904L,0xC49D043EL,(-1L)},{0x6A5B343BL,9L,0L,1L},{(-6L),(-1L),0xA37B6947L,0xEC240663L},{0L,0x112716B3L,(-6L),(-6L)}},{{(-1L),(-1L),0L,(-1L)},{0x52C7C266L,3L,0x563EA773L,0xBA4B9792L},{0xBA4B9792L,1L,(-3L),0x838BF04EL},{0x112716B3L,0L,1L,0x632D8BD4L},{0xD1717BABL,0xA6B53F91L,0L,(-1L)},{(-6L),(-6L),0x112716B3L,0x414FCB22L}},{{0x06BFC5A8L,0x70B4FB0DL,0L,0xC49D043EL},{0L,0x15B2499BL,0xC49D043EL,(-1L)},{(-1L),0L,(-9L),0xC49D043EL},{0L,0xA6B53F91L,0x70B4FB0DL,0xA6B53F91L},{0xE410C120L,0L,(-3L),0x52C7C266L},{0xA6B53F91L,1L,(-1L),(-3L)}}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2093[i] = &g_683;
            for (g_232 = 0; (g_232 <= 3); g_232 += 1)
            { /* block id: 961 */
                int32_t l_2087 = 0x28BAF8BCL;
                float *l_2088 = &l_1856;
                float *l_2089 = &g_285;
                int32_t l_2097 = 0xB6A7C8F7L;
                int32_t l_2122[9][2][6];
                int i, j, k;
                for (i = 0; i < 9; i++)
                {
                    for (j = 0; j < 2; j++)
                    {
                        for (k = 0; k < 6; k++)
                            l_2122[i][j][k] = 0L;
                    }
                }
                l_1793[0] = (((*g_673) = ((l_2060[0][2] , (safe_div_func_float_f_f((((safe_sub_func_float_f_f(((((*g_673) != 0x0.8p-1) != (safe_add_func_float_f_f((**g_672), (((*l_2089) = (safe_mul_func_float_f_f((safe_mul_func_float_f_f(((*l_2088) = (0x4.E70A84p-17 < (safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_add_func_float_f_f(((safe_mul_func_int8_t_s_s((safe_rshift_func_int8_t_s_s((~((l_2082 > (3L <= (safe_rshift_func_int8_t_s_s((safe_sub_func_int8_t_s_s(p_19, p_19)), g_1226[0][1])))) < 0UL)), p_20)), 1L)) , l_1852), l_2087)), 0x6.0p-1)), (**g_672))))), l_2087)), l_1703))) == l_1703)))) <= l_2087), p_20)) >= p_19) != 0x0.Ep-1), (-0x1.Dp+1)))) > p_20)) < p_20);
                for (g_1617.f2 = 0; (g_1617.f2 <= 2); g_1617.f2 += 1)
                { /* block id: 968 */
                    int32_t l_2090 = 0xC35E720DL;
                    union U0 **l_2092 = (void*)0;
                    union U0 ***l_2094 = &l_2093[1];
                    int32_t l_2111 = 0x3DE34008L;
                    int32_t l_2113 = 0xF1E47DBCL;
                    int32_t l_2114 = 0x1E8EF1F0L;
                    int32_t l_2115 = 9L;
                    int32_t l_2116 = 0x735703B9L;
                    int32_t l_2117 = 0L;
                    int32_t l_2119[7] = {(-2L),0xA002EA54L,0xA002EA54L,(-2L),0xA002EA54L,0xA002EA54L,(-2L)};
                    int i;
                    l_2097 = (((l_1961 , (((l_2090 == (+p_19)) , l_2092) == ((*l_2094) = (((**g_2014) = (0x864C9095A4AFCAB7LL && (-1L))) , l_2093[0])))) , p_20) , (safe_add_func_float_f_f((*g_673), p_19)));
                    for (l_1985 = 0; (l_1985 <= 4); l_1985 += 1)
                    { /* block id: 974 */
                        int32_t l_2101 = 0x4B9493D1L;
                        int32_t *l_2107 = (void*)0;
                        int32_t *l_2108 = &g_44[5][1][0];
                        int32_t *l_2109 = &l_1791;
                        int32_t *l_2110[8] = {&g_74,&g_74,&g_74,&g_74,&g_74,&g_74,&g_74,&g_74};
                        int i, j, k;
                        (*g_673) = ((safe_add_func_float_f_f((!(l_2101 = g_456[(g_232 + 1)][g_232][l_1985])), g_456[(g_232 + 1)][g_232][l_1985])) < (safe_add_func_float_f_f((safe_mul_func_float_f_f((p_20 < (*g_673)), ((l_1793[0] , p_20) != ((p_20 <= (g_2106 == (void*)0)) , l_1890)))), p_20)));
                        ++g_2123;
                        if (l_2111)
                            break;
                        g_656 = ((*g_655) = &l_1780);
                    }
                }
            }
        }
        else
        { /* block id: 984 */
            int32_t *l_2128 = &l_1788;
            int8_t * const l_2147 = (void*)0;
            int32_t l_2156 = 0xF2EDD858L;
            uint32_t l_2158 = 0x598D85E4L;
            l_1788 = 0xAA20E7B8L;
            for (l_1786 = (-5); (l_1786 > (-29)); l_1786 = safe_sub_func_uint64_t_u_u(l_1786, 6))
            { /* block id: 988 */
                int32_t l_2151[8][2][9] = {{{(-5L),0xE8D70E47L,0L,5L,(-1L),0x26ADE097L,(-1L),0x6EC7E4DBL,(-2L)},{0x15B83787L,0x3B3F980BL,(-10L),0xF511D9E7L,0x15B83787L,0x94086769L,4L,0x94086769L,0x15B83787L}},{{0xD217F435L,0x14150DE6L,0x14150DE6L,0xD217F435L,(-2L),0x6EC7E4DBL,(-1L),0x26ADE097L,(-1L)},{1L,0x94086769L,0x18D6033DL,0x677D1109L,(-1L),1L,(-1L),0x677D1109L,0x18D6033DL}},{{0xE8D70E47L,(-1L),1L,5L,(-2L),0x26ADE097L,5L,(-1L),0L},{0x75E31A22L,(-1L),(-5L),(-5L),0x15B83787L,(-5L),(-5L),(-1L),0x75E31A22L}},{{(-4L),5L,1L,0xD217F435L,(-1L),(-1L),0x14150DE6L,(-5L),0x26ADE097L},{1L,0x8A8A9047L,0x18D6033DL,0x3B3F980BL,0x18D6033DL,0x8A8A9047L,1L,0xF511D9E7L,(-1L)}},{{(-4L),(-1L),0x14150DE6L,5L,0L,(-5L),0x6EC7E4DBL,0x6EC7E4DBL,(-5L)},{0x75E31A22L,1L,(-10L),1L,0x75E31A22L,0x3B3F980BL,4L,0xF511D9E7L,0x75E31A22L}},{{0xE8D70E47L,0x14150DE6L,0L,0L,0x26ADE097L,0x6EC7E4DBL,1L,(-5L),0xD217F435L},{1L,0xF511D9E7L,1L,(-1L),(-1L),0x3B3F980BL,1L,(-1L),0x18D6033DL}},{{0xD217F435L,0xE8D70E47L,(-1L),5L,(-5L),(-5L),5L,(-1L),0xE8D70E47L},{0x15B83787L,(-5L),(-5L),(-1L),0x75E31A22L,0x8A8A9047L,(-5L),0x677D1109L,0x15B83787L}},{{(-5L),5L,0L,0L,0xD217F435L,(-1L),0L,0x26ADE097L,(-4L)},{1L,(-5L),1L,1L,0x18D6033DL,(-5L),(-1L),0x94086769L,(-1L)}}};
                int32_t l_2155 = 0xDAEE000BL;
                int32_t l_2157[5] = {0x1491E96CL,0x1491E96CL,0x1491E96CL,0x1491E96CL,0x1491E96CL};
                int i, j, k;
                (*g_2129) = func_77(l_2128);
                (*l_2128) |= l_2130;
                for (l_1960 = 0; (l_1960 > (-13)); l_1960 = safe_sub_func_uint8_t_u_u(l_1960, 9))
                { /* block id: 993 */
                    int64_t l_2136[5][2][10] = {{{0xE4CA010A69959E93LL,0xFA7098B6D297818FLL,1L,0L,1L,0x1BFDFAD73E7F58DCLL,0x1BFDFAD73E7F58DCLL,1L,0L,1L},{0L,0L,9L,0xFA7098B6D297818FLL,8L,0x1BFDFAD73E7F58DCLL,0xCBC91465FCF12BCBLL,(-8L),1L,(-1L)}},{{(-8L),0xCB2DC6627DC52983LL,0xCBC91465FCF12BCBLL,0xDC9E7B7AEA67E7E1LL,0L,0xDC9E7B7AEA67E7E1LL,0xCBC91465FCF12BCBLL,0xCB2DC6627DC52983LL,(-8L),0x6CC995331389D1BFLL},{0x866CBA5E9C41ADBFLL,0x6CC995331389D1BFLL,1L,8L,(-1L),1L,0x0A4DDAA4F107B284LL,0L,0L,0x0A4DDAA4F107B284LL}},{{0xCB2DC6627DC52983LL,0x866CBA5E9C41ADBFLL,8L,8L,0x866CBA5E9C41ADBFLL,0xCB2DC6627DC52983LL,0L,1L,(-8L),0x1BFDFAD73E7F58DCLL},{0x1BFDFAD73E7F58DCLL,0L,0x6CC995331389D1BFLL,0xDC9E7B7AEA67E7E1LL,1L,9L,0xCB2DC6627DC52983LL,9L,1L,0xDC9E7B7AEA67E7E1LL}},{{0x1BFDFAD73E7F58DCLL,9L,0x1BFDFAD73E7F58DCLL,0xFA7098B6D297818FLL,0x0A4DDAA4F107B284LL,0xCB2DC6627DC52983LL,0xDC9E7B7AEA67E7E1LL,(-1L),0x6CC995331389D1BFLL,1L},{0xCB2DC6627DC52983LL,0xDC9E7B7AEA67E7E1LL,(-1L),0x6CC995331389D1BFLL,1L,1L,0x6CC995331389D1BFLL,(-1L),0xDC9E7B7AEA67E7E1LL,0xCB2DC6627DC52983LL}},{{0x866CBA5E9C41ADBFLL,0xE4CA010A69959E93LL,0x1BFDFAD73E7F58DCLL,0L,0x6CC995331389D1BFLL,0xDC9E7B7AEA67E7E1LL,1L,9L,0xCB2DC6627DC52983LL,9L},{(-8L),0x1BFDFAD73E7F58DCLL,0x6CC995331389D1BFLL,0xE4CA010A69959E93LL,0x6CC995331389D1BFLL,0x1BFDFAD73E7F58DCLL,(-8L),1L,0L,0xCB2DC6627DC52983LL}}};
                    float *l_2148[3];
                    int32_t *l_2152 = (void*)0;
                    int32_t *l_2153 = &l_1703;
                    int32_t *l_2154[10];
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_2148[i] = &g_1562.f0;
                    for (i = 0; i < 10; i++)
                        l_2154[i] = &g_44[0][2][0];
                    l_2120 = ((~(safe_mod_func_int32_t_s_s(l_2136[2][1][7], p_19))) <= ((safe_sub_func_uint16_t_u_u(((safe_add_func_uint64_t_u_u((safe_div_func_uint16_t_u_u((0xB821A2A1L == (safe_rshift_func_uint8_t_u_s(((safe_div_func_float_f_f((**g_672), (((*l_2128) = (l_2147 != &l_2046)) <= (0x9.219F43p+10 == (l_2151[4][0][5] = ((((g_88 , (safe_sub_func_uint8_t_u_u(0xB9L, (*g_860)))) , 0xE.25F718p-19) != p_20) != 0x0.6p-1)))))) , 0x97L), 4))), 6UL)), g_329.f2)) <= p_20), 0UL)) , l_2130));
                    l_2158++;
                }
            }
            (*g_655) = (l_1703 , &l_1780);
            if (l_1780)
                goto lbl_2039;
        }
    }
    (*g_655) = &l_1780;
    return l_2161;
}


/* ------------------------------------------ */
/* 
 * reads : g_732 g_1226 g_998 g_999 g_1000 g_245 g_481.f2 g_860 g_232 g_672 g_673 g_96 g_154 g_1115 g_88 g_44 g_94 g_657 g_352.f2 g_854.f2 g_29 g_300 g_301 g_849.f2 g_893 g_190 g_859 g_1358 g_359 g_74 g_1390 g_1393 g_456 g_376 g_92 g_634 g_655 g_1394 g_318.f2 g_1281 g_1476 g_350.f2 g_990 g_244 g_897.f2 g_1491 g_152 g_1537
 * writes: g_732 g_92 g_634 g_655 g_481.f2 g_96 g_154 g_352.f2 g_88 g_94 g_893 g_854.f2 g_285 g_301 g_849.f2 g_152 g_190 g_657 g_1362 g_359 g_1045.f0 g_350.f2 g_74 g_1390 g_1393 g_1115 g_654 g_1281 g_232 g_1476 g_220.f2 g_1489 g_245 g_1537 g_1046
 */
static int32_t  func_22(uint32_t  p_23, uint8_t  p_24, uint8_t  p_25, int32_t  p_26)
{ /* block id: 552 */
    uint32_t l_1285 = 0x61E56766L;
    uint32_t *l_1294 = (void*)0;
    uint32_t *l_1295 = &g_732;
    int32_t ***l_1296 = &g_655;
    int8_t *l_1297 = &g_481.f2;
    uint8_t *l_1353 = &g_152;
    union U0 **l_1360[2];
    int32_t l_1389 = 0L;
    int16_t **l_1397 = (void*)0;
    int32_t l_1411 = 0xE72FDA80L;
    int32_t l_1412 = 0xB6E40D2BL;
    int32_t l_1414 = 0x5D9CB335L;
    int32_t l_1416[9][5][5] = {{{(-3L),1L,0x3D1B0737L,(-1L),1L},{1L,8L,(-5L),(-1L),0xED8EFB5CL},{(-10L),1L,3L,0L,8L},{0L,4L,0xED8EFB5CL,1L,0x86651D32L},{0xB29EDC92L,0x0DAE34CFL,0x10D1579CL,0L,1L}},{{8L,0x9B101DF3L,0xD2733FEAL,0x614B982BL,(-4L)},{0x95AD6501L,0x12AF0994L,0xD2733FEAL,0x0F8C674BL,0x48CE0CCCL},{0x29C52CA7L,2L,0x10D1579CL,0xB1EE73B1L,8L},{(-1L),(-6L),0xED8EFB5CL,0x0DAE34CFL,0x5685F609L},{4L,0xE95EF701L,3L,0xCE919C38L,0x0F8C674BL}},{{0L,1L,(-5L),0xE525AF6FL,(-10L)},{0xB147E73BL,0x76B99A7EL,0x3D1B0737L,0x76B99A7EL,0xB147E73BL},{0x954D368BL,2L,0x2C3011DAL,1L,0x3811AC09L},{0L,0x3D1B0737L,8L,0xD2733FEAL,(-5L)},{(-1L),1L,0x9B101DF3L,2L,0x3811AC09L}},{{0xCE919C38L,0xD2733FEAL,1L,0L,0xB147E73BL},{0x3811AC09L,(-4L),1L,0L,(-10L)},{0x0E1F9BDFL,0x121F4558L,0L,(-1L),0x0F8C674BL},{0x1C83683DL,0xB29EDC92L,0x95AD6501L,1L,0x5685F609L},{0x1CD6D045L,1L,0xB147E73BL,0x10D1579CL,8L}},{{1L,0x38C0F8F7L,3L,0x8A4EAA06L,(-4L)},{(-8L),0xE95EF701L,0L,1L,6L},{(-1L),0xE95EF701L,1L,0x89235B43L,(-5L)},{0x614B982BL,0xABDAF6C6L,(-4L),0x29C52CA7L,0x38C0F8F7L},{4L,0x86651D32L,0xB147E73BL,0x614B982BL,0x8A4EAA06L}},{{(-10L),1L,8L,1L,0x614B982BL},{(-4L),4L,1L,(-4L),(-10L)},{8L,0xCE919C38L,0xCE919C38L,8L,8L},{0x2C3011DAL,0x95AD6501L,0x3D1B0737L,0x1CD6D045L,0xED8EFB5CL},{0xB147E73BL,3L,0xF2C3CE12L,0x0E1F9BDFL,0xABDAF6C6L}},{{(-1L),0x89235B43L,0x5685F609L,0x1CD6D045L,0xB1EE73B1L},{0x0C3208FEL,(-1L),0x23F8708FL,8L,0xE525AF6FL},{(-6L),(-8L),0x76B99A7EL,(-4L),4L},{0x954D368BL,1L,1L,1L,1L},{1L,1L,(-5L),0x614B982BL,(-3L)}},{{0xABDAF6C6L,0x1CD6D045L,0L,0x29C52CA7L,0x89235B43L},{1L,(-1L),0x1CD6D045L,0x89235B43L,0x0C3208FEL},{1L,0L,0x2C3011DAL,1L,0x86651D32L},{1L,0xB1EE73B1L,0x0DAE34CFL,0x8A4EAA06L,1L},{1L,1L,1L,0xD2733FEAL,8L}},{{0xABDAF6C6L,(-6L),6L,0x3811AC09L,0x0F8C674BL},{1L,(-5L),0x0C3208FEL,0x6DE6EFCFL,3L},{0x954D368BL,0x8A4EAA06L,(-8L),0x48CE0CCCL,0x48CE0CCCL},{(-6L),1L,(-6L),(-3L),0xB29EDC92L},{0x0C3208FEL,(-4L),0x0E1F9BDFL,(-1L),1L}}};
    int64_t l_1431 = 0xEDAFE95E7EF293B8LL;
    uint8_t l_1501 = 0UL;
    int32_t l_1525 = 0xE4442305L;
    uint16_t ***l_1531 = &g_244;
    uint16_t ****l_1530 = &l_1531;
    uint32_t l_1573 = 0xCF9E91BCL;
    uint8_t l_1682 = 0x3EL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1360[i] = (void*)0;
lbl_1650:
    (**g_672) = ((((*l_1297) ^= (((*l_1296) = (((((0xB4L || (l_1285 == (safe_lshift_func_uint16_t_u_s(0xAC1AL, (safe_sub_func_uint64_t_u_u(0x6EDE24CA941BB042LL, (((****g_998) = (250UL || (safe_add_func_int32_t_s_s(((safe_mul_func_int8_t_s_s(((p_24 , 0UL) ^ ((((*l_1295) ^= p_26) || (((p_24 | g_1226[0][1]) | p_26) || 0xBA68L)) || p_26)), (-1L))) || l_1285), (-1L))))) == l_1285))))))) != l_1285) , (void*)0) == (void*)0) , &g_654)) != &g_608[2])) , (*g_860)) , (**g_672));
    p_26 &= p_23;
    for (g_154 = 0; (g_154 <= 3); g_154 += 1)
    { /* block id: 561 */
        int16_t l_1298 = 1L;
        uint8_t *l_1328 = &g_152;
        union U0 **l_1333[5][3][2] = {{{&g_683,&g_683},{&g_683,&g_683},{&g_683,&g_683}},{{&g_683,&g_683},{&g_683,&g_683},{&g_683,&g_683}},{{&g_683,&g_683},{&g_683,&g_683},{&g_683,&g_683}},{{&g_683,&g_683},{&g_683,&g_683},{&g_683,&g_683}},{{&g_683,&g_683},{&g_683,&g_683},{&g_683,&g_683}}};
        union U0 ***l_1332 = &l_1333[1][2][0];
        union U0 **** const l_1331[1] = {&l_1332};
        uint64_t l_1368 = 0x16637B184F364774LL;
        uint64_t l_1372 = 0UL;
        int32_t l_1375 = 0x9A482383L;
        int32_t ***l_1402 = &g_655;
        int32_t l_1415[9][9] = {{(-1L),0x1F6DAB30L,5L,0x1F6DAB30L,(-1L),0x1F6DAB30L,5L,0x1F6DAB30L,(-1L)},{(-1L),0xF97BB551L,0xF97BB551L,(-1L),0x4018C991L,(-3L),(-3L),0x4018C991L,(-1L)},{0xC93B6B43L,0x1F6DAB30L,0xC93B6B43L,(-1L),0xC93B6B43L,0x1F6DAB30L,0xC93B6B43L,(-1L),0xC93B6B43L},{(-1L),(-1L),(-3L),0xF97BB551L,0x4018C991L,0x4018C991L,0xF97BB551L,(-3L),(-1L)},{(-1L),(-1L),5L,(-1L),(-1L),(-1L),5L,(-1L),(-1L)},{0x4018C991L,0xF97BB551L,(-3L),(-1L),(-1L),(-3L),0xF97BB551L,0x4018C991L,0x4018C991L},{0xC93B6B43L,(-1L),0xC93B6B43L,0x1F6DAB30L,0xC93B6B43L,(-1L),0xC93B6B43L,0x1F6DAB30L,0xC93B6B43L},{0x4018C991L,(-1L),0xF97BB551L,0xF97BB551L,(-1L),0x4018C991L,(-3L),(-3L),0x4018C991L},{(-1L),0x1F6DAB30L,5L,0x1F6DAB30L,(-1L),0x1F6DAB30L,5L,0x1F6DAB30L,(-1L)}};
        uint32_t l_1418 = 0UL;
        const int16_t **l_1428 = (void*)0;
        const int16_t ***l_1427 = &l_1428;
        int i, j, k;
        p_26 = g_1115[g_154];
        for (g_352.f2 = 4; (g_352.f2 >= 0); g_352.f2 -= 1)
        { /* block id: 565 */
            int32_t *l_1299 = &g_74;
            int32_t *l_1300 = &g_657;
            int32_t *l_1301 = &g_94[3];
            int32_t *l_1302 = &g_44[2][3][2];
            int32_t *l_1303 = &g_74;
            int32_t *l_1304 = &g_74;
            int32_t *l_1305 = (void*)0;
            int32_t *l_1306 = &g_94[3];
            int32_t *l_1307 = &g_44[5][1][0];
            int32_t *l_1308 = &g_94[1];
            int32_t *l_1309 = &g_94[1];
            int32_t *l_1310 = &g_74;
            int32_t *l_1311 = &g_657;
            int32_t *l_1312 = &g_74;
            int32_t *l_1313 = &g_44[5][1][0];
            int32_t *l_1314 = &g_44[2][3][2];
            int32_t *l_1315 = &g_94[3];
            int32_t *l_1316 = &g_94[3];
            uint64_t l_1317[4][5][1] = {{{0x13FC4E20A42D3748LL},{0xED7F776AE61B9831LL},{3UL},{0x2AF2514057D1608ELL},{0xFCE2DE26A0E0D440LL}},{{0x2AF2514057D1608ELL},{3UL},{0xED7F776AE61B9831LL},{0x13FC4E20A42D3748LL},{0UL}},{{0x13FC4E20A42D3748LL},{0xED7F776AE61B9831LL},{3UL},{0x2AF2514057D1608ELL},{0xFCE2DE26A0E0D440LL}},{{0x2AF2514057D1608ELL},{3UL},{0xED7F776AE61B9831LL},{0x13FC4E20A42D3748LL},{0UL}}};
            const int32_t **l_1320[1];
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1320[i] = (void*)0;
            ++l_1317[2][0][0];
            g_893 = func_77(&p_26);
            for (g_732 = 1; (g_732 <= 4); g_732 += 1)
            { /* block id: 570 */
                union U1 **l_1334[8] = {&g_1046,(void*)0,(void*)0,&g_1046,(void*)0,(void*)0,&g_1046,(void*)0};
                const uint16_t *l_1335 = &g_634;
                int32_t l_1345 = (-7L);
                int i, j, k;
                g_893 = func_77(l_1311);
                for (g_854.f2 = 3; (g_854.f2 >= 0); g_854.f2 -= 1)
                { /* block id: 574 */
                    float *l_1336 = &g_285;
                    uint64_t l_1348 = 0UL;
                    int i, j, k;
                    (*g_300) = (g_44[(g_352.f2 + 1)][(g_154 + 1)][g_854.f2] , ((((*l_1336) = ((safe_div_func_float_f_f(0x1.40AF23p+68, (safe_mul_func_float_f_f(((*g_673) = ((safe_mul_func_float_f_f((-0x7.9p-1), (-0x5.Ap-1))) <= ((-((((l_1328 == &p_25) || (safe_rshift_func_uint16_t_u_u(((((void*)0 != l_1331[0]) , ((void*)0 == l_1334[6])) & g_44[g_732][g_854.f2][g_154]), 8))) , (void*)0) == l_1335)) > (**g_672)))), l_1298)))) <= p_26)) , g_29) , (void*)0));
                    (*g_300) = (*g_300);
                    for (g_849.f2 = 3; (g_849.f2 >= 0); g_849.f2 -= 1)
                    { /* block id: 581 */
                        int i, j, k;
                        (*l_1308) &= ((p_25 || g_44[g_732][g_854.f2][g_154]) , (safe_div_func_int32_t_s_s((((safe_lshift_func_int16_t_s_u(0xDBD2L, 7)) && l_1298) | (safe_mod_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(((*g_245) = l_1345), g_44[g_732][g_854.f2][g_154])), ((safe_sub_func_int32_t_s_s((p_24 >= (l_1298 == (((l_1348 < l_1345) >= g_849.f2) ^ (-1L)))), 0x47A39183L)) & l_1298)))), (*g_893))));
                        p_26 &= (safe_unary_minus_func_uint8_t_u(((*l_1328) = l_1298)));
                    }
                }
                if (g_44[(g_352.f2 + 1)][(g_732 + 1)][g_154])
                    continue;
                for (g_190 = 0; (g_190 <= 3); g_190 += 1)
                { /* block id: 591 */
                    int i, j, k;
                    (*l_1311) |= (safe_mod_func_int16_t_s_s(g_44[g_154][(g_190 + 2)][g_154], (safe_unary_minus_func_int8_t_s(g_44[(g_154 + 2)][g_352.f2][g_154]))));
                    return g_1115[g_154];
                }
            }
        }
        if (((*g_859) == (l_1353 = &p_24)))
        { /* block id: 598 */
            uint32_t l_1356[1][10] = {{18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL}};
            int64_t *l_1357 = &g_190;
            union U0 **l_1361 = &g_683;
            int32_t *l_1363 = (void*)0;
            int32_t *l_1364 = &g_359;
            float *l_1369 = (void*)0;
            float *l_1370 = &g_1045.f0;
            int32_t *l_1371 = &g_657;
            int i, j;
            for (g_88 = 0; (g_88 <= 3); g_88 += 1)
            { /* block id: 601 */
                return p_24;
            }
            (*l_1371) ^= ((((*l_1364) ^= (((*l_1332) = ((safe_sub_func_int16_t_s_s(l_1356[0][3], ((g_44[5][1][0] ^ ((*l_1357) |= 0x45F4E19877CAC379LL)) | (g_1358 == (void*)0)))) , l_1360[1])) == (g_1362 = l_1361))) , &g_683) != ((((*l_1370) = (((**g_672) = (!(safe_mul_func_float_f_f((**g_672), l_1368)))) != l_1356[0][7])) > l_1356[0][3]) , (void*)0));
        }
        else
        { /* block id: 611 */
            uint8_t l_1385[9] = {0xC7L,0xA5L,0xA5L,0xC7L,0xA5L,0xA5L,0xC7L,0xA5L,0xA5L};
            int32_t l_1404 = 0x3BB27C67L;
            int32_t l_1413 = 0xE9530305L;
            int32_t l_1417 = 1L;
            int i;
            if (l_1372)
                break;
            for (g_849.f2 = 4; (g_849.f2 >= 0); g_849.f2 -= 1)
            { /* block id: 615 */
                uint64_t l_1373 = 0x684EF00DA549479ALL;
                int32_t *l_1405 = (void*)0;
                int32_t *l_1406 = &l_1389;
                int32_t *l_1407 = &g_74;
                int32_t *l_1408 = (void*)0;
                int32_t *l_1409 = &g_44[5][1][0];
                int32_t *l_1410[8] = {(void*)0,&g_74,&g_74,(void*)0,&g_74,&g_74,(void*)0,&g_74};
                int i;
                if (l_1373)
                    break;
                for (g_350.f2 = 0; (g_350.f2 <= 4); g_350.f2 += 1)
                { /* block id: 619 */
                    int32_t l_1380 = 0L;
                    int16_t ***l_1395 = (void*)0;
                    int16_t ***l_1396 = (void*)0;
                    for (p_24 = 0; (p_24 <= 3); p_24 += 1)
                    { /* block id: 622 */
                        int32_t *l_1374 = &g_44[(g_849.f2 + 1)][(g_154 + 2)][p_24];
                        int32_t *l_1376 = &l_1375;
                        int32_t *l_1377 = &g_44[5][1][0];
                        int32_t *l_1378 = (void*)0;
                        int32_t *l_1379 = &l_1375;
                        int32_t *l_1381 = &l_1380;
                        int32_t *l_1382 = &g_94[1];
                        int32_t *l_1383 = &g_74;
                        int32_t *l_1384 = &g_44[4][3][0];
                        int32_t *l_1388[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_1388[i] = &g_44[5][0][3];
                        l_1385[2]++;
                        (*l_1383) ^= g_44[g_154][(p_24 + 2)][g_154];
                        ++g_1390[4];
                    }
                    g_1393 = (l_1397 = g_1393);
                    if (p_25)
                    { /* block id: 629 */
                        int32_t l_1403 = 0x9858296AL;
                        int i;
                        l_1404 ^= (((g_1115[g_154] = ((safe_add_func_uint64_t_u_u((0x3EFDC5BFL | 0L), ((((safe_rshift_func_int16_t_s_u(0L, (g_456[4][0][1] != 7UL))) , l_1402) != (void*)0) , (p_26 | (((((g_376 >= p_26) | 0L) == 3L) , (****g_998)) >= l_1380))))) & l_1403)) != l_1380) & 0x340FL);
                    }
                    else
                    { /* block id: 632 */
                        (*g_300) = (*g_300);
                        if (p_25)
                            break;
                    }
                }
                if (p_23)
                    break;
                l_1418--;
            }
        }
        for (g_849.f2 = 3; (g_849.f2 >= 0); g_849.f2 -= 1)
        { /* block id: 643 */
            float *l_1425[6];
            int32_t l_1426[1][5][8] = {{{(-1L),(-1L),0xB23D01DAL,0xE28F9419L,0x520A450DL,0xE28F9419L,0xB23D01DAL,(-1L)},{(-1L),0x0B72CF02L,0x18ECDA16L,0xB23D01DAL,0xB23D01DAL,0x18ECDA16L,0x0B72CF02L,(-1L)},{0x0B72CF02L,0x1ECD0A27L,(-1L),0xE28F9419L,(-1L),0x1ECD0A27L,0x0B72CF02L,0x0B72CF02L},{0x1ECD0A27L,0xE28F9419L,0x18ECDA16L,0x18ECDA16L,0xE28F9419L,0x1ECD0A27L,0xB23D01DAL,0x1ECD0A27L},{0xE28F9419L,0x1ECD0A27L,0xB23D01DAL,0x1ECD0A27L,0xE28F9419L,0x18ECDA16L,0x18ECDA16L,0xE28F9419L}}};
            int32_t l_1432 = 0xA7017F6EL;
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_1425[i] = &g_285;
            g_285 = (((**g_672) = (safe_sub_func_float_f_f((((**g_672) > 0x4.0p-1) <= 0xC.BFCAEBp+55), ((((safe_mul_func_float_f_f((l_1426[0][3][4] = 0x8.Ep+1), 0xF.D807D9p+89)) == ((&g_1393 == l_1427) != 0xA.BDDCF6p+54)) >= (!0x0.Dp-1)) <= (-0x1.1p-1))))) <= (-0x1.0p-1));
            for (p_25 = 0; (p_25 <= 8); p_25 += 1)
            { /* block id: 649 */
                int i, j, k;
                (**l_1296) = &g_44[(g_849.f2 + 1)][(g_154 + 1)][g_849.f2];
            }
            for (g_634 = 0; (g_634 <= 4); g_634 += 1)
            { /* block id: 654 */
                int32_t *l_1430[10][10] = {{&g_44[5][5][2],&l_1415[0][5],&g_44[4][1][2],&l_1389,(void*)0,&l_1411,(void*)0,(void*)0,&l_1416[8][4][4],&g_44[1][0][3]},{&l_1426[0][3][4],&l_1412,&l_1416[8][4][4],&g_44[g_849.f2][g_849.f2][g_849.f2],&l_1414,&l_1426[0][3][4],&l_1426[0][3][4],&g_44[5][5][2],&g_657,&l_1411},{&g_44[g_849.f2][g_849.f2][g_849.f2],&l_1416[8][4][4],&g_44[g_849.f2][g_849.f2][g_849.f2],(void*)0,(void*)0,(void*)0,&g_44[g_849.f2][g_849.f2][g_849.f2],&l_1416[8][4][4],&g_44[g_849.f2][g_849.f2][g_849.f2],&g_74},{&l_1426[0][3][4],(void*)0,&l_1414,(void*)0,&g_44[5][5][2],&l_1375,&l_1412,&g_74,&g_94[3],(void*)0},{&g_44[g_849.f2][g_849.f2][g_849.f2],&l_1411,&l_1375,(void*)0,&l_1415[0][5],&g_44[5][1][0],(void*)0,&l_1426[0][3][4],&g_44[g_849.f2][g_849.f2][g_849.f2],&l_1412},{(void*)0,&g_74,&l_1426[0][3][4],&g_44[g_849.f2][g_849.f2][g_849.f2],&l_1412,(void*)0,&g_44[1][0][3],&l_1426[0][3][4],&l_1426[0][3][4],&g_44[1][0][3]},{(void*)0,&l_1426[0][3][4],&l_1415[0][5],&l_1415[0][5],&l_1426[0][3][4],(void*)0,&g_44[4][1][2],&l_1426[0][2][6],&l_1426[0][3][4],(void*)0},{&l_1411,&l_1411,&g_74,&g_44[1][0][3],&l_1375,&l_1426[0][3][4],&g_94[3],&g_44[5][5][2],&g_44[g_849.f2][g_849.f2][g_849.f2],&l_1426[0][3][4]},{&l_1411,&l_1412,&l_1412,&l_1426[0][3][4],&l_1426[0][3][4],(void*)0,&l_1426[0][3][4],&l_1411,&l_1375,&l_1414},{(void*)0,&l_1426[0][3][4],&l_1411,&l_1375,&l_1414,(void*)0,(void*)0,(void*)0,&l_1414,&l_1375}};
                uint32_t l_1433 = 1UL;
                int i, j, k;
                --l_1433;
            }
        }
    }
    if ((((*g_1393) != (*g_1393)) > (p_24 && (l_1360[1] != (void*)0))))
    { /* block id: 659 */
        int16_t ** const *l_1454[4][3][6] = {{{&l_1397,&l_1397,&l_1397,&l_1397,(void*)0,&l_1397},{&g_1393,&g_1393,&g_1393,(void*)0,(void*)0,&l_1397},{&g_1393,&l_1397,&g_1393,&l_1397,&g_1393,(void*)0}},{{(void*)0,&l_1397,(void*)0,&l_1397,&l_1397,(void*)0},{&g_1393,&g_1393,&g_1393,(void*)0,&g_1393,&g_1393},{&g_1393,(void*)0,&g_1393,&l_1397,&l_1397,&g_1393}},{{&l_1397,&g_1393,&g_1393,(void*)0,&g_1393,&g_1393},{&g_1393,(void*)0,&g_1393,&l_1397,&l_1397,(void*)0},{&l_1397,&l_1397,(void*)0,&g_1393,&l_1397,(void*)0}},{{&l_1397,&l_1397,&g_1393,&g_1393,&l_1397,&l_1397},{&l_1397,(void*)0,&g_1393,(void*)0,&g_1393,&l_1397},{&g_1393,&g_1393,&l_1397,&l_1397,&l_1397,&l_1397}}};
        union U1 l_1471[1][3][5] = {{{{0x7.0B8843p+77},{-0x8.2p-1},{0x7.0B8843p+77},{0x3.6F78E2p+16},{0x3.6F78E2p+16}},{{0x7.0B8843p+77},{-0x8.2p-1},{0x7.0B8843p+77},{0x3.6F78E2p+16},{0x3.6F78E2p+16}},{{0x7.0B8843p+77},{-0x8.2p-1},{0x7.0B8843p+77},{0x3.6F78E2p+16},{0x3.6F78E2p+16}}}};
        const int32_t l_1478 = 0x0E352D88L;
        uint8_t *** const *l_1488 = (void*)0;
        const int32_t **l_1493 = &g_893;
        int i, j, k;
        for (l_1411 = 0; (l_1411 <= 2); l_1411 += 1)
        { /* block id: 662 */
            uint8_t l_1448 = 255UL;
            uint64_t *l_1453 = &g_1281[0];
            int16_t * const *l_1456[6][2][4] = {{{&g_1394,&g_1394,&g_1394,&g_1394},{&g_1394,&g_1394,&g_1394,&g_1394}},{{&g_1394,&g_1394,&g_1394,&g_1394},{&g_1394,&g_1394,&g_1394,&g_1394}},{{&g_1394,&g_1394,&g_1394,&g_1394},{&g_1394,&g_1394,&g_1394,&g_1394}},{{&g_1394,&g_1394,&g_1394,&g_1394},{&g_1394,&g_1394,&g_1394,&g_1394}},{{&g_1394,&g_1394,&g_1394,&g_1394},{&g_1394,&g_1394,&g_1394,&g_1394}},{{&g_1394,&g_1394,&g_1394,&g_1394},{&g_1394,&g_1394,&g_1394,&g_1394}}};
            int16_t * const ** const l_1455 = &l_1456[5][1][2];
            volatile int32_t *l_1457[1][10];
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 10; j++)
                    l_1457[i][j] = &g_1458[6];
            }
            if (p_24)
                break;
            l_1457[0][6] = ((safe_lshift_func_uint16_t_u_u(65528UL, (safe_mul_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_s((safe_rshift_func_uint16_t_u_s(((*g_1394) == l_1448), (safe_lshift_func_int8_t_s_s((safe_add_func_uint32_t_u_u(g_154, ((((*l_1453) = 1UL) , (((p_26 , l_1454[2][1][2]) != (((*g_672) != (void*)0) , l_1455)) >= 0UL)) , 4294967289UL))), l_1448)))), 14)), (-5L))), l_1448)))) , (*g_300));
            for (l_1414 = 2; (l_1414 >= 0); l_1414 -= 1)
            { /* block id: 668 */
                uint32_t l_1466 = 0xEF44207AL;
                union U1 *l_1472 = &l_1471[0][2][1];
                int32_t *l_1475 = &g_94[3];
                int16_t l_1477 = 0x64EBL;
                p_26 &= (((l_1448 > (safe_mod_func_uint32_t_u_u((((safe_add_func_int16_t_s_s(((*g_1394) = p_25), (!((0x4543FEDC14D0C716LL == (((((((safe_mod_func_uint16_t_u_u(l_1466, (g_1476 |= (safe_sub_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_s((0xFCL > p_24), (((((*l_1472) = l_1471[0][2][1]) , ((*g_860) = l_1466)) <= (safe_mod_func_int8_t_s_s((((*l_1475) = l_1466) == g_318.f2), 3L))) ^ g_1281[0]))), p_24))))) != g_350.f2) , (**g_859)) <= g_990[0]) ^ l_1477) || (-1L)) && (**g_244))) , (*l_1475))))) != l_1478) >= l_1478), g_359))) > g_190) ^ g_481.f2);
                for (g_220.f2 = 0; (g_220.f2 <= 2); g_220.f2 += 1)
                { /* block id: 677 */
                    return p_25;
                }
            }
        }
        p_26 = ((safe_add_func_int16_t_s_s((p_24 || p_23), (safe_rshift_func_int16_t_s_s(((**g_1393) = (+g_1115[4])), ((((((safe_add_func_uint64_t_u_u(g_897[1][4].f2, 0x24DED3514EEC2040LL)) , (safe_mod_func_int8_t_s_s(1L, (**g_859)))) == ((g_1489 = ((p_23 || 0x4594L) , l_1488)) == l_1488)) > (-8L)) | g_1390[7]) , (-1L)))))) | l_1478);
        (*l_1493) = func_77(&g_74);
    }
    else
    { /* block id: 686 */
        uint16_t *l_1508[5] = {&g_634,&g_634,&g_634,&g_634,&g_634};
        uint16_t *l_1509 = (void*)0;
        uint64_t l_1512 = 0xA81B145B2F55EE83LL;
        int32_t l_1521 = 4L;
        int32_t l_1522 = (-1L);
        int32_t l_1523 = 0xF7FF0C78L;
        int32_t l_1524 = 0x6F1E43FAL;
        int32_t **l_1584 = &g_654;
        uint8_t l_1594 = 0x19L;
        union U1 * const *l_1600[7][8] = {{&g_1046,(void*)0,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046},{&g_1046,(void*)0,&g_1046,&g_1046,&g_1046,&g_1046,(void*)0,&g_1046},{&g_1046,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046},{&g_1046,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046,&g_1046},{&g_1046,&g_1046,&g_1046,(void*)0,&g_1046,&g_1046,&g_1046,(void*)0},{&g_1046,(void*)0,&g_1046,&g_1046,(void*)0,&g_1046,&g_1046,(void*)0},{(void*)0,&g_1046,&g_1046,(void*)0,&g_1046,&g_1046,(void*)0,&g_1046}};
        union U1 * const **l_1599 = &l_1600[3][6];
        union U1 * const ***l_1598[1];
        union U1 * const ****l_1597 = &l_1598[0];
        union U0 *l_1616 = &g_1617;
        uint8_t l_1645 = 0x9EL;
        union U1 l_1659 = {0xB.DC8FA4p-56};
        int32_t l_1681[1];
        int i, j;
        for (i = 0; i < 1; i++)
            l_1598[i] = &l_1599;
        for (i = 0; i < 1; i++)
            l_1681[i] = (-2L);
        if ((l_1525 = (safe_mod_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((((!(p_25 || (safe_rshift_func_int8_t_s_u(((*l_1297) = l_1501), 6)))) || (safe_lshift_func_int16_t_s_s(((l_1524 ^= ((((*l_1353) = ((safe_lshift_func_int8_t_s_u(0x78L, 7)) & ((g_1390[4] || g_190) | ((((*l_1297) = ((((((l_1523 &= (safe_rshift_func_int16_t_s_s((l_1521 = ((((((*g_244) = l_1508[4]) == (l_1509 = l_1508[2])) <= (p_25 ^ (safe_mul_func_uint16_t_u_u((++l_1512), ((**g_1393) = (safe_mul_func_int8_t_s_s((safe_add_func_uint8_t_u_u((safe_div_func_int16_t_s_s(1L, 0xBEDCL)), g_1115[2])), p_26))))))) < 0x1AD55E74C70CE9C9LL) ^ l_1389)), l_1522))) | (**g_1491)) < p_26) <= l_1522) & g_74) , 0xD0L)) | p_23) || 0xDC2FD2E048CE540FLL)))) || 0x41L) < l_1431)) , p_25), 2))) ^ 2UL), p_25)), l_1522))))
        { /* block id: 698 */
            int32_t l_1532[5];
            int32_t l_1533 = 0x7E859B7BL;
            int32_t *l_1534 = &l_1524;
            union U1 *l_1549 = (void*)0;
            int32_t **l_1587 = &g_656;
            uint64_t *l_1654 = &g_456[4][0][1];
            int8_t *l_1658 = &g_350.f2;
            int i;
            for (i = 0; i < 5; i++)
                l_1532[i] = 1L;
lbl_1559:
            (*l_1534) |= ((g_152 <= ((((safe_lshift_func_uint8_t_u_u(0xE8L, 2)) & (((g_456[4][0][1] , 0xAF06L) < ((p_24 &= (((l_1533 = (((*g_860) = (safe_mod_func_int32_t_s_s((p_26 = (((((void*)0 != l_1530) , (p_26 , (*g_300))) == (void*)0) || l_1532[4])), 4294967293UL))) , 0L)) == p_25) ^ p_25)) , l_1512)) & p_25)) > p_25) , l_1523)) != g_94[2]);
            for (g_854.f2 = (-16); (g_854.f2 > 0); g_854.f2++)
            { /* block id: 706 */
                volatile union U1 * volatile ***l_1541 = &g_1537;
                uint16_t ***l_1544 = (void*)0;
                union U1 *l_1550 = &g_1045;
                union U1 **l_1551 = &g_1046;
                int32_t l_1554 = 1L;
                float *l_1557 = (void*)0;
                float *l_1558[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_1558[i] = &g_1045.f0;
                (*l_1541) = g_1537;
                (*l_1534) = ((safe_sub_func_float_f_f((l_1544 != ((safe_sub_func_float_f_f((*g_673), (*g_673))) , (*l_1530))), (0x1.D16B4Cp+23 == (safe_add_func_float_f_f((l_1549 != ((*l_1551) = l_1550)), (safe_add_func_float_f_f(((l_1554 >= ((safe_sub_func_float_f_f((l_1389 = (0x1.4p-1 >= 0xB.EBC0DCp-14)), (*l_1534))) <= (*g_673))) == (**g_672)), (-0x1.5p-1)))))))) <= 0x3.7F82C6p+82);
                (*g_300) = (*g_300);
                (*l_1534) = l_1524;
            }
            for (g_481.f2 = 1; (g_481.f2 >= 0); g_481.f2 -= 1)
            { /* block id: 716 */
                union U1 *l_1563 = &g_1045;
                int32_t l_1574[2][4][9] = {{{0L,0L,7L,0L,0L,7L,(-3L),(-3L),7L},{6L,0x3FCA66D3L,6L,0x3FCA66D3L,6L,6L,0xB39EE7B9L,0xB39EE7B9L,6L},{0L,0L,7L,0L,0L,7L,(-3L),(-3L),7L},{6L,0x3FCA66D3L,6L,0x3FCA66D3L,6L,6L,0xB39EE7B9L,0xB39EE7B9L,6L}},{{0L,0L,7L,0L,0L,7L,(-3L),(-3L),7L},{6L,0x3FCA66D3L,6L,0x3FCA66D3L,6L,6L,0xB39EE7B9L,0xB39EE7B9L,6L},{0L,0L,7L,0L,0L,7L,(-3L),(-3L),7L},{6L,0x3FCA66D3L,6L,0x3FCA66D3L,6L,6L,0xB39EE7B9L,0xB39EE7B9L,6L}}};
                union U1 * const ****l_1601[6][6];
                const float *l_1615[9][2][8] = {{{&g_437.f0,&g_437.f0,&g_285,&g_437.f0,&g_437.f0,&g_285,&g_437.f0,&g_437.f0},{(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0}},{{&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0},{&g_437.f0,&g_437.f0,&g_285,&g_437.f0,&g_437.f0,&g_285,&g_437.f0,&g_437.f0}},{{(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0},{&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0}},{{&g_437.f0,&g_437.f0,&g_285,&g_437.f0,&g_437.f0,&g_285,&g_437.f0,&g_437.f0},{(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0}},{{&g_437.f0,&g_285,&g_285,(void*)0,&g_285,&g_285,(void*)0,&g_285},{(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0}},{{&g_285,(void*)0,&g_285,&g_285,(void*)0,&g_285,&g_285,(void*)0},{(void*)0,&g_285,&g_285,(void*)0,&g_285,&g_285,(void*)0,&g_285}},{{(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0},{&g_285,(void*)0,&g_285,&g_285,(void*)0,&g_285,&g_285,(void*)0}},{{(void*)0,&g_285,&g_285,(void*)0,&g_285,&g_285,(void*)0,&g_285},{(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0,&g_437.f0,(void*)0,(void*)0}},{{&g_285,(void*)0,&g_285,&g_285,(void*)0,&g_285,&g_285,(void*)0},{(void*)0,&g_285,&g_285,(void*)0,&g_285,&g_285,(void*)0,&g_285}}};
                const float ** const l_1614 = &l_1615[5][1][6];
                uint32_t l_1668 = 0x4A8B9822L;
                int i, j, k;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 6; j++)
                        l_1601[i][j] = &l_1598[0];
                }
                if (g_232)
                    goto lbl_1559;
            }
        }
        else
        { /* block id: 767 */
            int32_t *l_1677 = &l_1524;
            int32_t *l_1678 = (void*)0;
            int32_t *l_1679 = (void*)0;
            int32_t *l_1680[2][7] = {{&g_74,&l_1411,&g_74,&l_1521,&g_94[3],&g_94[3],&l_1521},{&g_74,&l_1411,&g_74,&l_1521,&g_94[3],&g_94[3],&l_1521}};
            int i, j;
            p_26 &= ((*l_1677) ^= 0xEA52AB9DL);
            l_1682++;
            if (g_190)
                goto lbl_1650;
        }
    }
    return l_1416[2][4][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_44 g_74 g_88 g_94 g_92 g_244 g_245 g_634 g_456 g_29 g_481.f2 g_359 g_511.f2 g_154 g_495 g_152 g_655 g_672 g_654 g_656 g_657 g_376 g_252 g_328 g_683 g_300 g_190 g_732 g_220.f2 g_854.f2 g_684.f2 g_232 g_860 g_849.f2 g_893 g_673 g_96 g_350.f2 g_990 g_897.f2 g_352.f2 g_1226 g_1115 g_998 g_999 g_1000 g_859 g_608 g_1281
 * writes: g_74 g_88 g_92 g_94 g_96 g_154 g_44 g_608 g_654 g_252 g_190 g_657 g_481.f2 g_456 g_301 g_732 g_858 g_656 g_854.f2 g_684.f2 g_359 g_893 g_849.f2 g_350.f2 g_511.f2 g_232 g_990 g_897.f2 g_352.f2 g_1115 g_1281
 */
static uint64_t  func_30(int32_t  p_31, uint64_t  p_32)
{ /* block id: 6 */
    int8_t l_61 = 0x33L;
    const int32_t *l_73 = &g_44[3][1][3];
    uint16_t ***l_648 = &g_244;
    uint16_t **l_650 = &g_245;
    uint16_t ***l_649 = &l_650;
    int32_t l_651 = (-4L);
    uint32_t l_652 = 5UL;
    int32_t l_653 = 0xC57881B9L;
    int16_t *l_894[9] = {&g_154,&g_154,&g_154,&g_154,&g_154,&g_154,&g_154,&g_154,&g_154};
    int32_t l_895 = (-9L);
    union U0 *l_896 = &g_897[1][4];
    uint32_t l_921 = 0x0BE97DDCL;
    uint16_t l_1041[8][3] = {{0xC89CL,65529UL,0xE45FL},{0xC89CL,65535UL,9UL},{0xC89CL,65535UL,0xC89CL},{0xC89CL,65529UL,0xE45FL},{0xC89CL,65535UL,9UL},{0xC89CL,65535UL,0xC89CL},{0xC89CL,65529UL,0xE45FL},{0xC89CL,65535UL,9UL}};
    int64_t * const l_1047[7] = {&g_190,&g_190,&g_190,&g_190,&g_190,&g_190,&g_190};
    int32_t l_1059[1];
    int32_t l_1060[7];
    float ***l_1088[3][3][1];
    uint8_t *l_1096[6] = {&g_29,&g_29,(void*)0,&g_29,&g_29,(void*)0};
    union U1 l_1113 = {0x8.9E3E23p-72};
    int64_t l_1146 = 0x30A1E78E5E73443DLL;
    uint32_t l_1171[10][7] = {{0x13273CF0L,4294967290UL,0xF97F4EEDL,4294967290UL,0x13273CF0L,0xF370CCCBL,0x13273CF0L},{0UL,4294967288UL,4294967288UL,0UL,4294967288UL,4294967288UL,0UL},{5UL,4294967290UL,5UL,4294967295UL,0x13273CF0L,4294967295UL,5UL},{0UL,0UL,0xD1FA4736L,0UL,0UL,0xD1FA4736L,0UL},{0x13273CF0L,4294967295UL,5UL,4294967290UL,5UL,4294967295UL,0x13273CF0L},{4294967288UL,0UL,4294967288UL,4294967288UL,0UL,4294967288UL,4294967288UL},{0x13273CF0L,4294967290UL,0xF97F4EEDL,4294967290UL,0x13273CF0L,0xF370CCCBL,0x13273CF0L},{0UL,4294967288UL,0xD1FA4736L,4294967288UL,0xD1FA4736L,0xD1FA4736L,4294967288UL},{0xF97F4EEDL,4294967295UL,0xF97F4EEDL,0xF370CCCBL,5UL,0xF370CCCBL,0xF97F4EEDL},{4294967288UL,4294967288UL,0UL,4294967288UL,4294967288UL,0UL,4294967288UL}};
    uint16_t *** const **l_1182 = &g_1001[3];
    uint8_t ***l_1183 = &g_859;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1059[i] = 0xAF3842F6L;
    for (i = 0; i < 7; i++)
        l_1060[i] = 0x3A7BE8C1L;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 1; k++)
                l_1088[i][j][k] = &g_672;
        }
    }
    if (((safe_lshift_func_int8_t_s_u(((safe_mul_func_int16_t_s_s((l_895 = func_51((((g_654 = (g_608[2] = (((((safe_sub_func_uint16_t_u_u((l_653 |= (safe_div_func_int16_t_s_s(((l_61 <= ((!(l_651 = func_39(func_39(p_32, (l_61 , (safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u(g_44[2][4][2], 11)), (((safe_sub_func_int32_t_s_s((safe_sub_func_int8_t_s_s(func_71(l_73), ((((((((safe_add_func_int8_t_s_s(p_32, (safe_add_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((safe_rshift_func_uint16_t_u_s((l_648 != (l_649 = &g_244)), (*l_73))), 4294967295UL)), (-1L))))) <= g_481.f2) >= (*l_73)) , 0x8BB986C983C6F740LL) | p_31) , 0xC754L) == 6L) < g_359))), 0UL)) > 0x95L) & g_481.f2))))), g_456[4][0][1]))) , l_652)) & g_511.f2), p_31))), 1L)) ^ 65533UL) <= 0xFEL) && g_154) , &l_651))) != l_73) & g_495), g_456[4][0][1], g_152, g_655, g_481.f2)), p_31)) || p_32), 6)) || (*l_73)))
    { /* block id: 373 */
        union U0 **l_898 = &l_896;
        int8_t *l_899 = (void*)0;
        int8_t *l_900 = &g_854.f2;
        const int32_t l_913[9][7][2] = {{{(-3L),1L},{0xDC04C64EL,0x03AFBF9BL},{0xE0B3ECF1L,0x45AE223CL},{1L,0x03A364F5L},{0xC1C30738L,0L},{(-6L),4L},{0L,0x5791AEEDL}},{{(-6L),0x7F68B15AL},{0x1FB5FEC2L,0x52B84763L},{0x5BE546EAL,0L},{0xCC23AA77L,0L},{0x45AE223CL,0x7EC44BC7L},{(-1L),1L},{0xCF38516EL,9L}},{{0L,(-3L)},{(-6L),0x81CBF738L},{0x5791AEEDL,0xC1C30738L},{0L,1L},{0x4D288834L,0xCF38516EL},{0x1C56444DL,0xCF38516EL},{0x4D288834L,1L}},{{0L,0xC1C30738L},{0x5791AEEDL,0x81CBF738L},{(-6L),(-3L)},{0L,9L},{0xCF38516EL,1L},{(-1L),0x7EC44BC7L},{0x45AE223CL,0L}},{{0xCC23AA77L,0L},{0x5BE546EAL,0x52B84763L},{0x1FB5FEC2L,0x7F68B15AL},{(-6L),0x5791AEEDL},{0L,4L},{(-6L),0L},{0xC1C30738L,0x03A364F5L}},{{1L,0x45AE223CL},{0xE0B3ECF1L,0x03AFBF9BL},{0xDC04C64EL,1L},{(-3L),0x33D04CD1L},{0xC4ACC5E8L,0x6714DAADL},{0x0C2664BAL,0x7ECA1F99L},{(-2L),0x4CDA722FL}},{{0x0B071E8BL,0x5BE546EAL},{2L,8L},{(-1L),(-1L)},{9L,0xBFC3BF03L},{7L,0x1FB5FEC2L},{(-1L),0xBF6B3882L},{0x81CBF738L,(-1L)}},{{0x7F68B15AL,1L},{0x7F68B15AL,(-1L)},{0x81CBF738L,0xBF6B3882L},{(-1L),0x1FB5FEC2L},{7L,0xBFC3BF03L},{9L,(-1L)},{(-1L),8L}},{{2L,0x5BE546EAL},{0x0B071E8BL,0x4CDA722FL},{(-2L),0x7ECA1F99L},{0x0C2664BAL,0x6714DAADL},{0xC4ACC5E8L,0x33D04CD1L},{(-3L),1L},{0xDC04C64EL,0x03AFBF9BL}}};
        int32_t l_914[2][2] = {{(-1L),(-1L)},{(-1L),(-1L)}};
        int i, j, k;
        (*l_898) = l_896;
        l_914[1][1] |= ((*g_654) = ((((*l_900) = g_232) == (safe_sub_func_int32_t_s_s((~(safe_lshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s(((void*)0 != &l_61), ((g_154 = 0L) && g_634))), (g_732 | ((safe_mod_func_uint8_t_u_u((!((((safe_sub_func_uint32_t_u_u(((void*)0 == &l_898), p_32)) > 0x217EL) , (*g_860)) & l_913[3][2][1])), l_913[6][3][0])) == p_31))))), p_31))) <= 18446744073709551615UL));
        for (g_657 = 3; (g_657 >= 0); g_657 -= 1)
        { /* block id: 381 */
            uint32_t l_915 = 18446744073709551611UL;
            if (l_915)
                break;
            for (g_481.f2 = 3; (g_481.f2 >= 0); g_481.f2 -= 1)
            { /* block id: 385 */
                return g_854.f2;
            }
        }
    }
    else
    { /* block id: 389 */
        uint8_t ****l_933 = &g_858;
        int32_t l_934 = 0xCA1D6FA7L;
        int32_t l_935 = 0xBB7C9D0FL;
        int32_t l_967[5] = {0x329DDFCBL,0x329DDFCBL,0x329DDFCBL,0x329DDFCBL,0x329DDFCBL};
        uint8_t ***l_1005 = &g_859;
        int16_t l_1036[2][2][6] = {{{0xD93BL,9L,0xD93BL,0xD838L,0xD838L,0xD93BL},{(-7L),(-7L),0xD838L,0x1D48L,0xD838L,(-7L)}},{{0xD838L,9L,0x1D48L,0x1D48L,9L,0xD838L},{(-7L),0xD838L,0x1D48L,0xD838L,(-7L),(-7L)}}};
        union U0 **l_1114 = (void*)0;
        uint16_t **l_1161 = &g_245;
        union U1 *l_1170[7] = {&l_1113,(void*)0,(void*)0,&l_1113,(void*)0,(void*)0,&l_1113};
        int32_t *l_1172 = &g_74;
        uint16_t l_1208[4];
        uint16_t l_1219 = 65531UL;
        const uint16_t **l_1237 = (void*)0;
        const uint16_t *** const l_1236 = &l_1237;
        const uint16_t *** const *l_1235 = &l_1236;
        const uint16_t *** const **l_1234 = &l_1235;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1208[i] = 0xE5F0L;
        if ((*l_73))
        { /* block id: 390 */
            uint16_t l_918 = 0x50A6L;
            uint16_t *l_922 = &g_92;
            uint8_t ****l_932 = &g_858;
            int32_t l_936 = (-1L);
            for (g_849.f2 = 0; (g_849.f2 < 29); g_849.f2 = safe_add_func_uint16_t_u_u(g_849.f2, 1))
            { /* block id: 393 */
                l_918--;
            }
            if (((*g_654) = l_921))
            { /* block id: 397 */
                l_935 |= ((((void*)0 == &g_860) && (((p_32 , 65535UL) != (g_154 = ((void*)0 != l_922))) || (safe_div_func_int32_t_s_s((*g_654), (((~(safe_rshift_func_uint8_t_u_s(((safe_add_func_uint16_t_u_u(((**g_244) = (safe_div_func_int64_t_s_s((l_932 == l_933), 0x03F58CA92884A628LL))), 0x413AL)) < 7UL), (*l_73)))) || 0xE06CL) & l_934))))) , (*g_893));
                (**g_672) = (0xA.196695p-64 >= (**g_672));
            }
            else
            { /* block id: 402 */
                l_936 = p_32;
            }
            l_651 = (*l_73);
        }
        else
        { /* block id: 406 */
            int16_t l_965 = 0x0A52L;
            int32_t l_968 = 0xD3C65472L;
            uint8_t ***l_1004 = &g_859;
            uint16_t ****l_1010[7] = {&l_649,&l_649,&l_649,&l_649,&l_649,&l_649,&l_649};
            int32_t *l_1027 = &l_968;
            int32_t l_1037 = 0x7F9B145FL;
            int32_t l_1038 = 0xAD1575F0L;
            int32_t l_1039 = 0x8370E107L;
            union U1 * const l_1048[4][5] = {{&g_1045,&g_1045,&g_1045,&g_1045,&g_1045},{&g_1045,&g_1045,&g_1045,&g_1045,&g_1045},{&g_1045,&g_1045,&g_1045,&g_1045,&g_1045},{&g_1045,&g_1045,&g_1045,&g_1045,&g_1045}};
            int32_t l_1066 = 8L;
            int32_t l_1069 = 0L;
            int32_t l_1071[6] = {0x70002379L,0x70002379L,0x70002379L,0x70002379L,0x70002379L,0x70002379L};
            uint32_t l_1097 = 4294967288UL;
            uint32_t l_1102 = 18446744073709551609UL;
            float l_1117 = 0x9.Bp+1;
            uint8_t l_1145 = 0x7DL;
            int i, j;
            for (g_350.f2 = 0; (g_350.f2 >= (-3)); g_350.f2 = safe_sub_func_uint16_t_u_u(g_350.f2, 4))
            { /* block id: 409 */
                uint8_t l_960 = 0UL;
                uint8_t l_969[10] = {0xE0L,0xE0L,0xE0L,0xE0L,0xE0L,0xE0L,0xE0L,0xE0L,0xE0L,0xE0L};
                union U1 *l_996 = (void*)0;
                union U1 **l_997 = &l_996;
                uint16_t *** const **l_1003[5][5] = {{&g_1001[2],&g_1001[2],&g_1001[0],&g_1001[2],&g_1001[2]},{&g_1001[1],&g_1001[1],&g_1001[1],&g_1001[1],&g_1001[1]},{&g_1001[2],&g_1001[3],&g_1001[3],&g_1001[2],&g_1001[3]},{&g_1001[1],&g_1001[1],(void*)0,&g_1001[1],&g_1001[1]},{&g_1001[3],&g_1001[2],&g_1001[3],&g_1001[3],&g_1001[2]}};
                int32_t l_1034 = 0x4E4D2EF4L;
                int32_t l_1035[3][3][7] = {{{8L,(-1L),(-1L),8L,(-1L),(-1L),8L},{0L,0x70BE738AL,0L,0L,0x70BE738AL,0L,0L},{8L,8L,6L,8L,8L,6L,8L}},{{0x70BE738AL,0L,0L,0x70BE738AL,0L,0L,0x70BE738AL},{(-1L),8L,(-1L),(-1L),8L,(-1L),(-1L)},{0x70BE738AL,0x70BE738AL,2L,0x70BE738AL,0x70BE738AL,2L,0x70BE738AL}},{{8L,(-1L),(-1L),8L,(-1L),(-1L),8L},{0L,0x70BE738AL,0L,0L,0x70BE738AL,0L,0L},{8L,8L,6L,8L,8L,6L,8L}}};
                int i, j, k;
                for (p_31 = 0; (p_31 <= (-11)); --p_31)
                { /* block id: 412 */
                    int16_t l_943 = 0x6E31L;
                    uint8_t l_944 = 0x0BL;
                    int32_t l_959 = 0x63F359E8L;
                    int32_t l_966 = 3L;
                    for (l_921 = 0; (l_921 >= 59); ++l_921)
                    { /* block id: 415 */
                        (*g_654) = (*l_73);
                        if (l_943)
                            break;
                        if (l_944)
                            continue;
                        return p_32;
                    }
                    if (l_935)
                    { /* block id: 421 */
                        uint16_t l_945 = 65535UL;
                        if (l_945)
                            break;
                    }
                    else
                    { /* block id: 423 */
                        union U1 l_952[2] = {{0x1.8F2899p-40},{0x1.8F2899p-40}};
                        int8_t *l_953 = &g_511.f2;
                        int32_t l_958 = 0x452C567BL;
                        int32_t *l_961 = (void*)0;
                        int32_t *l_962 = (void*)0;
                        int32_t *l_963 = &l_958;
                        int32_t *l_964[6][4][4] = {{{&l_651,&g_44[5][1][0],(void*)0,(void*)0},{&l_653,&l_958,&l_935,&g_44[5][1][0]},{&g_657,(void*)0,&l_935,&g_44[3][5][2]},{&l_653,(void*)0,(void*)0,&g_94[3]}},{{&l_651,&g_44[4][2][3],(void*)0,&l_959},{(void*)0,&l_959,(void*)0,&l_651},{&l_653,&g_74,&g_74,&g_44[5][1][0]},{&g_74,&g_44[4][2][3],&g_44[3][5][2],&l_958}},{{&l_935,&l_653,&g_657,&g_44[3][5][2]},{&l_651,&l_935,&l_958,&g_74},{&l_653,&l_958,&l_958,&l_653},{&l_935,&g_74,&l_935,(void*)0}},{{&g_44[5][1][0],&l_958,&g_74,&g_94[3]},{(void*)0,(void*)0,&g_657,&g_94[3]},{(void*)0,&l_958,&l_651,(void*)0},{(void*)0,&g_74,(void*)0,&l_653}},{{&g_657,&l_958,(void*)0,(void*)0},{&l_958,(void*)0,&l_934,(void*)0},{(void*)0,&l_958,(void*)0,&l_895},{&l_935,&g_94[0],&g_74,&l_959}},{{&g_94[3],(void*)0,&l_958,&g_94[3]},{&l_958,&l_651,(void*)0,&l_651},{&l_651,&g_94[0],&l_934,&g_94[3]},{&g_94[0],(void*)0,&g_94[3],(void*)0}}};
                        int i, j, k;
                        l_959 |= (safe_sub_func_uint8_t_u_u((((((*g_860) = (safe_lshift_func_int8_t_s_u((safe_div_func_int16_t_s_s(g_190, (((&g_495 != &g_495) | (l_952[1] , (((*l_953) = ((p_31 <= g_732) , 1L)) >= (safe_mod_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((0L != 0UL), p_31)), l_958))))) ^ p_31))), 3))) <= g_152) & p_32) && l_944), 0xA9L));
                        (*g_654) &= (((*g_244) != (*g_244)) , (-1L));
                        l_960 = (*g_654);
                        l_969[4]++;
                    }
                    for (l_968 = 0; (l_968 < (-20)); l_968 = safe_sub_func_uint32_t_u_u(l_968, 1))
                    { /* block id: 433 */
                        int32_t *l_974 = &g_74;
                        int32_t *l_975 = (void*)0;
                        int32_t *l_976 = &g_94[2];
                        int32_t l_977 = (-10L);
                        int32_t l_978 = 3L;
                        int32_t l_979[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
                        int32_t *l_980 = &l_653;
                        int32_t *l_981 = &g_657;
                        int32_t *l_982 = &l_978;
                        int32_t *l_983 = (void*)0;
                        int32_t *l_984 = &l_967[1];
                        int32_t *l_985 = &g_657;
                        int32_t *l_986 = (void*)0;
                        int32_t *l_987 = &l_967[2];
                        int32_t *l_988 = &l_977;
                        int32_t *l_989[2];
                        union U0 ***l_993 = (void*)0;
                        union U0 **l_995 = (void*)0;
                        union U0 ***l_994 = &l_995;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_989[i] = &l_967[1];
                        (*g_654) = (p_31 | (&g_300 == (void*)0));
                        --g_990[0];
                        (*l_994) = &g_683;
                        if (p_31)
                            break;
                    }
                }
                (*l_997) = l_996;
            }
            l_73 = &l_934;
        }
        (*l_1172) ^= (safe_mod_func_int32_t_s_s(((*g_654) = (((safe_div_func_int16_t_s_s((l_934 = (safe_rshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_u((safe_div_func_int16_t_s_s(((((safe_div_func_int16_t_s_s((l_1161 == (*l_649)), ((safe_div_func_int64_t_s_s((((*g_860) && 0xFCL) & (((((safe_div_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((((l_1060[3] ^= (4294967294UL >= ((((safe_lshift_func_int16_t_s_s((*l_73), 9)) , func_77(&l_895)) != &l_967[0]) <= l_967[2]))) , 0x360446EF467C5CE6LL) & (*l_73)), (*l_73))), 1L)) & l_935) == (*l_73)) , l_1170[2]) != &l_1113)), p_31)) , 65535UL))) , p_31) && l_967[4]) != p_32), l_1171[1][4])), 3)), l_934)), 4))), p_31)) , l_1036[0][1][3]) != 18446744073709551615UL)), 0x02649154L));
        if ((safe_rshift_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s((*l_73), (safe_mul_func_int16_t_s_s((p_32 && p_32), (+(*l_1172)))))) ^ ((*g_654) ^= 9L)), (((safe_lshift_func_uint8_t_u_s(0x7DL, ((l_1182 != l_1182) ^ ((l_1183 != l_1183) >= 0xE372L)))) == 0x9C31FBA9L) & 0x53L))))
        { /* block id: 491 */
            l_73 = &l_967[1];
            (*g_654) ^= (*l_1172);
        }
        else
        { /* block id: 494 */
            float l_1194 = 0x8.0D6579p+97;
            int32_t l_1195 = 0x45C7B884L;
            int32_t l_1196 = 1L;
            int8_t *l_1197 = (void*)0;
            int8_t *l_1198 = (void*)0;
            int8_t *l_1199[10] = {(void*)0,(void*)0,&g_511.f2,(void*)0,(void*)0,&g_511.f2,(void*)0,(void*)0,&g_511.f2,(void*)0};
            int32_t *l_1204 = (void*)0;
            int32_t *l_1205 = &l_1059[0];
            uint16_t *l_1206 = (void*)0;
            int32_t *l_1207[10];
            union U0 **l_1222 = &l_896;
            int16_t l_1264 = 3L;
            uint16_t ** const *l_1274[3];
            uint16_t ** const **l_1273 = &l_1274[1];
            uint16_t ** const ***l_1272 = &l_1273;
            int i;
            for (i = 0; i < 10; i++)
                l_1207[i] = &l_935;
            for (i = 0; i < 3; i++)
                l_1274[i] = &l_650;
            l_1208[0] |= ((*g_654) = ((*l_1172) = (((safe_lshift_func_int8_t_s_s(p_32, 7)) <= (p_31 , (safe_div_func_uint16_t_u_u(((((*l_1161) != ((l_1060[0] |= (p_31 != (safe_mul_func_int8_t_s_s(0x85L, ((g_897[1][4].f2 ^= (safe_add_func_uint16_t_u_u(1UL, (safe_lshift_func_int16_t_s_u((l_1195 = g_684.f2), (l_1196 = ((***l_649) = 65526UL))))))) , (safe_mod_func_int16_t_s_s((safe_div_func_int16_t_s_s(((((*l_1205) = 0x4C6124A9L) , 0x43D29EF8L) , 0xEBFEL), 0x748DL)), p_31))))))) , l_1206)) >= p_32) == p_31), p_32)))) <= (*l_73))));
            for (l_1195 = 5; (l_1195 >= 1); l_1195 -= 1)
            { /* block id: 506 */
                int32_t **l_1211[9][7] = {{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172},{&l_1207[6],&l_1207[7],&g_654,&l_1207[7],&l_1207[6],&l_1172,&l_1172}};
                union U1 l_1227 = {0x8.8p+1};
                uint8_t * const *l_1240 = &g_860;
                int32_t l_1275 = 0L;
                int i, j;
                l_1060[l_1195] = (safe_lshift_func_uint8_t_u_s((*l_73), g_154));
                g_893 = ((*g_655) = &l_895);
                for (g_352.f2 = 1; (g_352.f2 >= 0); g_352.f2 -= 1)
                { /* block id: 512 */
                    int32_t *l_1212 = &l_653;
                    uint16_t l_1228 = 1UL;
                    union U1 l_1233 = {0x4.8FDF97p+3};
                    int i, j;
                    g_893 = ((*g_655) = l_1212);
                    if (((*g_656) = (l_1208[g_352.f2] & (safe_lshift_func_int16_t_s_u((~((~g_92) != (safe_rshift_func_int8_t_s_u(((void*)0 != &g_1000[(g_352.f2 + 2)][(g_352.f2 + 4)]), (0UL == ((l_1219 & (safe_sub_func_uint8_t_u_u((((((*l_1172) = ((l_1222 == ((safe_unary_minus_func_uint32_t_u(((safe_mod_func_uint16_t_u_u(g_1226[0][1], (l_1227 , p_32))) || 3UL))) , (void*)0)) & g_1115[2])) && p_32) , (*l_1212)) > (*l_73)), (*l_73)))) & 0UL)))))), l_1228)))))
                    { /* block id: 517 */
                        uint32_t *l_1229[4];
                        uint16_t * const **l_1232 = (void*)0;
                        uint16_t * const ***l_1231 = &l_1232;
                        uint16_t * const ****l_1230 = &l_1231;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_1229[i] = &l_1171[9][0];
                        (*g_654) = (((*l_1212) = p_31) > (l_1230 != (l_1233 , l_1234)));
                        return p_31;
                    }
                    else
                    { /* block id: 521 */
                        return p_32;
                    }
                }
                for (g_732 = 0; (g_732 <= 1); g_732 += 1)
                { /* block id: 527 */
                    uint8_t * const *l_1238 = &g_860;
                    uint8_t * const **l_1239 = (void*)0;
                    int32_t l_1241 = 0L;
                    int32_t **l_1270 = &g_608[2];
                    uint32_t *l_1271 = &g_1115[3];
                    int32_t l_1276 = (-1L);
                    int i, j;
                    (**g_655) |= p_31;
                    l_1240 = l_1238;
                    l_1241 = ((*g_654) = (&g_1045 == &g_1045));
                    (*g_656) |= (safe_rshift_func_uint8_t_u_s((((g_190 ^= (safe_add_func_int64_t_s_s((safe_mod_func_int16_t_s_s((g_154 |= ((((p_31 , (safe_rshift_func_uint16_t_u_u(((****g_998) = 0x3F73L), (safe_add_func_int8_t_s_s((safe_rshift_func_int8_t_s_u((safe_add_func_int64_t_s_s((((safe_add_func_int16_t_s_s(((((*l_1271) = (safe_lshift_func_uint8_t_u_s((safe_div_func_int8_t_s_s(((safe_rshift_func_uint8_t_u_s((l_1264 != (l_651 = ((*l_73) != (p_32 || (!(*g_654)))))), ((safe_mod_func_uint64_t_u_u((((void*)0 != l_1270) || (p_31 , 0UL)), p_31)) ^ 0x78B7L))) || g_152), p_32)), 0))) , &g_1001[1]) != l_1272), (*l_1172))) >= p_32) , (*l_1172)), p_32)), 2)), (**g_859)))))) && 1L) , p_32) != 0x73L)), p_32)), p_31))) == 1UL) | 3UL), 2));
                    for (g_350.f2 = 1; (g_350.f2 >= 0); g_350.f2 -= 1)
                    { /* block id: 540 */
                        int32_t l_1277 = 0x90738584L;
                        int32_t l_1278 = 0xEA82AF67L;
                        int32_t l_1279 = 1L;
                        int32_t l_1280 = (-6L);
                        int i, j, k;
                        (*g_673) = l_1036[g_732][g_350.f2][(g_732 + 4)];
                        (*l_1270) = (*l_1270);
                        g_1281[0]++;
                    }
                }
            }
            (*l_1172) ^= (g_376 && 18446744073709551614UL);
        }
    }
    return g_352.f2;
}


/* ------------------------------------------ */
/* 
 * reads : g_44
 * writes: g_44
 */
static int32_t  func_39(int32_t  p_40, uint8_t  p_41)
{ /* block id: 2 */
    float l_42 = (-0x1.Ap-1);
    int32_t *l_43 = &g_44[5][1][0];
    int32_t **l_45 = &l_43;
    (*l_43) &= p_41;
    (*l_45) = &p_40;
    return g_44[5][5][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_672 g_511.f2 g_654 g_656 g_657 g_376 g_252 g_328 g_683 g_92 g_244 g_245 g_300 g_190 g_154 g_732 g_655 g_456 g_220.f2 g_44 g_854.f2 g_684.f2 g_359 g_88 g_94
 * writes: g_252 g_190 g_657 g_481.f2 g_456 g_301 g_154 g_732 g_858 g_656 g_854.f2 g_684.f2 g_359 g_88 g_92 g_94 g_96 g_893
 */
static int16_t  func_51(uint16_t  p_52, int32_t  p_53, int16_t  p_54, int32_t ** p_55, int16_t  p_56)
{ /* block id: 225 */
    int8_t l_660 = 0x95L;
    uint8_t *l_667 = &g_152;
    float *l_671 = &g_96;
    float **l_670 = &l_671;
    float **l_674 = (void*)0;
    uint32_t *l_675 = (void*)0;
    uint32_t *l_676 = (void*)0;
    uint32_t *l_677 = &g_252;
    int64_t *l_678 = &g_190;
    int32_t l_698 = 0x8164F3FCL;
    int32_t l_709 = 0L;
    int32_t l_718 = 0x930B4BA5L;
    uint64_t l_719 = 0xB687A236A1439164LL;
    uint8_t l_746 = 0x89L;
    uint16_t l_773[7] = {0xDB7EL,1UL,1UL,0xDB7EL,1UL,1UL,0xDB7EL};
    int32_t l_794 = 0x90407232L;
    int32_t l_795[6][10][1] = {{{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L}},{{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L}},{{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L}},{{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L}},{{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L}},{{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L},{0x98221674L},{0x32C55B08L}}};
    uint16_t l_804 = 65535UL;
    int32_t *l_812[7] = {&g_657,&l_718,&g_657,&g_657,&l_718,&g_657,&g_657};
    const float *l_817 = &g_96;
    const float **l_816[9];
    uint32_t l_818 = 0x5ABF46CEL;
    int32_t ***l_821 = &g_655;
    uint16_t * const *l_826 = &g_245;
    uint16_t * const **l_825 = &l_826;
    uint16_t * const ***l_824[9][4] = {{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825},{&l_825,&l_825,&l_825,&l_825}};
    union U0 *l_853 = &g_854;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_816[i] = &l_817;
lbl_735:
    (*g_654) |= ((safe_mul_func_uint16_t_u_u(((0xD165L | ((l_660 && l_660) && (p_54 >= g_29))) < ((safe_div_func_int16_t_s_s((safe_mod_func_int64_t_s_s(((*l_678) = (safe_lshift_func_uint8_t_u_s(((void*)0 != l_667), ((safe_lshift_func_uint16_t_u_s((((((*l_677) = (l_670 == (l_674 = g_672))) == 0x12B49022L) && l_660) <= p_53), p_53)) <= p_54)))), 0x6AABB0B7A0F989D5LL)), 1L)) , 255UL)), 1L)) != g_511.f2);
    if ((*g_656))
    { /* block id: 230 */
        union U1 l_679 = {-0x1.3p+1};
        union U0 **l_685 = (void*)0;
        union U0 *l_686 = &g_687;
        int32_t l_690 = 5L;
        int32_t l_691 = 0x52BE63C3L;
        int8_t *l_692[6][6] = {{(void*)0,&l_660,(void*)0,&g_350.f2,&g_350.f2,(void*)0},{&g_350.f2,&g_350.f2,(void*)0,&l_660,(void*)0,(void*)0},{&l_660,(void*)0,(void*)0,(void*)0,&l_660,(void*)0},{(void*)0,&l_660,(void*)0,&g_350.f2,&g_350.f2,(void*)0},{&g_350.f2,&g_350.f2,(void*)0,&l_660,(void*)0,(void*)0},{&l_660,(void*)0,(void*)0,(void*)0,&l_660,(void*)0}};
        uint64_t *l_693[3];
        int i, j;
        for (i = 0; i < 3; i++)
            l_693[i] = (void*)0;
        if ((((l_679 , (((g_456[4][0][1] = (g_376 , (+((l_660 | (l_660 != ((*l_677)--))) || ((((g_328 != (l_686 = g_683)) | g_92) > (g_481.f2 = (safe_lshift_func_int16_t_s_u((l_691 |= (((**p_55) = l_690) <= ((*g_654) = 0xB2E47B1CL))), p_53)))) != (**g_244)))))) != l_660) == 0xFC30DF4AL)) , p_52) == 0x47F823EAL))
        { /* block id: 238 */
            (*g_300) = (void*)0;
            (*g_656) = (**p_55);
        }
        else
        { /* block id: 241 */
            int32_t *l_694 = &g_44[5][1][0];
            int32_t *l_695 = (void*)0;
            int32_t *l_696 = &l_690;
            int32_t *l_697 = &g_657;
            int32_t *l_699 = (void*)0;
            int32_t *l_700 = &l_690;
            int32_t *l_701 = &l_690;
            int32_t *l_702 = &l_698;
            int32_t *l_703 = &g_74;
            int32_t l_704[5][9];
            int32_t *l_705 = &g_44[4][5][0];
            int32_t *l_706 = &l_704[1][4];
            int32_t *l_707 = &l_704[4][0];
            int32_t *l_708 = (void*)0;
            int32_t *l_710 = (void*)0;
            int32_t *l_711 = (void*)0;
            int32_t *l_712 = &l_691;
            int32_t *l_713 = &l_690;
            int32_t *l_714 = &g_94[3];
            int32_t *l_715 = &g_94[2];
            int32_t *l_716 = &g_657;
            int32_t *l_717[3];
            int i, j;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 9; j++)
                    l_704[i][j] = 0xF462FEFAL;
            }
            for (i = 0; i < 3; i++)
                l_717[i] = &g_74;
            l_719++;
        }
        for (g_190 = 0; (g_190 != (-24)); --g_190)
        { /* block id: 246 */
            return p_53;
        }
    }
    else
    { /* block id: 249 */
        uint32_t l_733[1];
        int i;
        for (i = 0; i < 1; i++)
            l_733[i] = 0xB65666A6L;
        for (l_660 = 0; (l_660 > (-29)); l_660 = safe_sub_func_int64_t_s_s(l_660, 1))
        { /* block id: 252 */
            int32_t l_728 = 0xF589E81CL;
            for (g_154 = (-21); (g_154 < (-13)); ++g_154)
            { /* block id: 255 */
                uint32_t *l_731 = &g_732;
                int32_t l_734 = 0xB4F73D48L;
                l_734 |= ((**p_55) &= (0xC3L < (((*l_731) ^= ((*l_677)--)) & l_733[0])));
                if (p_54)
                    goto lbl_735;
            }
            if ((**p_55))
                break;
        }
    }
    if ((*g_654))
    { /* block id: 265 */
        const float l_745 = 0x2.7489F0p+78;
        int32_t l_765 = (-1L);
        int32_t l_767 = 0x270ECB92L;
        int32_t l_769[4][10][6] = {{{0x732999ECL,0x20B3BE1CL,0xFE906EA5L,0x20B3BE1CL,0x732999ECL,(-4L)},{0x0BDE3C9EL,0x20B3BE1CL,0x5D7399F5L,0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L},{0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L,0x20B3BE1CL,0x20B3BE1CL,0x04E18D10L},{0x732999ECL,0x732999ECL,0x5D7399F5L,1L,0x20B3BE1CL,(-4L)},{0x20B3BE1CL,0x5D6599A4L,0xFE906EA5L,1L,0x5D6599A4L,0x5D7399F5L},{0x732999ECL,0x20B3BE1CL,0xFE906EA5L,0x20B3BE1CL,0x732999ECL,(-4L)},{0x0BDE3C9EL,0x20B3BE1CL,0x5D7399F5L,0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L},{0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L,0x20B3BE1CL,0x20B3BE1CL,0x04E18D10L},{0x732999ECL,0x732999ECL,0x5D7399F5L,1L,0x20B3BE1CL,(-4L)},{0x20B3BE1CL,0x5D6599A4L,0xFE906EA5L,1L,0x5D6599A4L,0x5D7399F5L}},{{0x732999ECL,0x20B3BE1CL,0xFE906EA5L,0x20B3BE1CL,0x732999ECL,(-4L)},{0x0BDE3C9EL,0x20B3BE1CL,0x5D7399F5L,0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L},{0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L,0x20B3BE1CL,0x20B3BE1CL,0x04E18D10L},{0x732999ECL,0x732999ECL,0x5D7399F5L,1L,0x20B3BE1CL,(-4L)},{0x20B3BE1CL,0x5D6599A4L,0xFE906EA5L,1L,0x5D6599A4L,0x5D7399F5L},{0x732999ECL,0x20B3BE1CL,0xFE906EA5L,0x20B3BE1CL,0x732999ECL,(-4L)},{0x0BDE3C9EL,0x20B3BE1CL,0x5D7399F5L,0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L},{0x0BDE3C9EL,0x5D6599A4L,0x04E18D10L,0x20B3BE1CL,0x20B3BE1CL,0x04E18D10L},{0x732999ECL,0x732999ECL,0x5D7399F5L,1L,0x20B3BE1CL,(-4L)},{0x20B3BE1CL,0x5D6599A4L,0xFE906EA5L,1L,0x5D6599A4L,0x5D7399F5L}},{{0x732999ECL,0L,0x5D6599A4L,0L,0xF372545DL,0x20B3BE1CL},{0xE8127FF6L,0L,0x0BDE3C9EL,0xE8127FF6L,0xD7A59D0FL,1L},{0xE8127FF6L,0xD7A59D0FL,1L,0L,0L,1L},{0xF372545DL,0xF372545DL,0x0BDE3C9EL,0xBF6B5350L,0L,0x20B3BE1CL},{0L,0xD7A59D0FL,0x5D6599A4L,0xBF6B5350L,0xD7A59D0FL,0x0BDE3C9EL},{0xF372545DL,0L,0x5D6599A4L,0L,0xF372545DL,0x20B3BE1CL},{0xE8127FF6L,0L,0x0BDE3C9EL,0xE8127FF6L,0xD7A59D0FL,1L},{0xE8127FF6L,0xD7A59D0FL,1L,0L,0L,1L},{0xF372545DL,0xF372545DL,0x0BDE3C9EL,0xBF6B5350L,0L,0x20B3BE1CL},{0L,0xD7A59D0FL,0x5D6599A4L,0xBF6B5350L,0xD7A59D0FL,0x0BDE3C9EL}},{{0xF372545DL,0L,0x5D6599A4L,0L,0xF372545DL,0x20B3BE1CL},{0xE8127FF6L,0L,0x0BDE3C9EL,0xE8127FF6L,0xD7A59D0FL,1L},{0xE8127FF6L,0xD7A59D0FL,1L,0L,0L,1L},{0xF372545DL,0xF372545DL,0x0BDE3C9EL,0xBF6B5350L,0L,0x20B3BE1CL},{0L,0xD7A59D0FL,0x5D6599A4L,0xBF6B5350L,0xD7A59D0FL,0x0BDE3C9EL},{0xF372545DL,0L,0x5D6599A4L,0L,0xF372545DL,0x20B3BE1CL},{0xE8127FF6L,0L,0x0BDE3C9EL,0xE8127FF6L,0xD7A59D0FL,1L},{0xE8127FF6L,0xD7A59D0FL,1L,0L,0L,1L},{0xF372545DL,0xF372545DL,0x0BDE3C9EL,0xBF6B5350L,0L,0x20B3BE1CL},{0L,0xD7A59D0FL,0x5D6599A4L,0xBF6B5350L,0xD7A59D0FL,0x0BDE3C9EL}}};
        int i, j, k;
        for (g_657 = (-22); (g_657 < (-19)); g_657 = safe_add_func_int64_t_s_s(g_657, 2))
        { /* block id: 268 */
            uint32_t *l_743 = &g_732;
            int32_t l_749 = 5L;
            int32_t l_771 = 0xE1074751L;
            float l_777 = 0xC.F8B85Ep-42;
            const float *l_790 = &l_745;
            const float **l_789 = &l_790;
            const float ***l_788 = &l_789;
            uint32_t l_791[7] = {5UL,5UL,5UL,5UL,5UL,5UL,5UL};
            int32_t l_796 = (-10L);
            int32_t l_797 = 8L;
            int32_t l_798 = 0L;
            int32_t l_799 = 1L;
            int32_t l_800 = 0xA47D0882L;
            int32_t l_801 = (-1L);
            float l_802 = 0x5.BAC758p-67;
            int32_t l_803 = 0xC9055A70L;
            int32_t *l_815 = (void*)0;
            int i;
        }
    }
    else
    { /* block id: 328 */
        uint16_t l_835 = 0xC89CL;
        uint64_t *l_842 = (void*)0;
        int32_t l_843 = 0xC96C6EABL;
        union U0 *l_846 = &g_847[6];
        union U0 *l_848 = &g_849;
        int32_t l_852[6] = {0x6EABC549L,8L,8L,0x6EABC549L,8L,8L};
        int i;
        (***l_821) ^= ((void*)0 != l_821);
        (*g_654) &= (safe_sub_func_int8_t_s_s(((void*)0 != l_824[3][1]), (p_53 | (safe_sub_func_uint32_t_u_u((safe_sub_func_int16_t_s_s(0xEBA7L, 8UL)), ((g_732 , &g_608[0]) != (*l_821)))))));
        if ((safe_sub_func_int64_t_s_s((safe_sub_func_int8_t_s_s(g_456[4][0][1], l_835)), (safe_lshift_func_uint16_t_u_u((safe_add_func_int16_t_s_s((l_852[2] = (safe_rshift_func_int8_t_s_u(((l_843 = (l_835 <= g_252)) <= ((safe_add_func_uint8_t_u_u((((l_846 != (l_848 = g_683)) , (***l_821)) ^ (safe_div_func_int8_t_s_s((0xB6L <= 0UL), p_53))), 5L)) != 1L)), 3))), (*g_245))), 13)))))
        { /* block id: 334 */
            union U0 **l_855 = &l_853;
            int16_t *l_856 = (void*)0;
            int16_t *l_857 = &g_154;
            uint8_t ***l_862 = &g_859;
            uint8_t ****l_861 = &l_862;
            uint8_t ***l_864 = &g_859;
            uint8_t ****l_863 = &l_864;
            int32_t l_865 = 0x962E6124L;
            uint16_t **l_871 = &g_245;
            uint16_t **l_872 = &g_245;
            (*l_855) = l_853;
            (**p_55) |= ((p_54 ^= ((*l_857) &= g_220.f2)) < ((((g_858 = (void*)0) == ((*l_863) = ((*l_861) = (void*)0))) & l_865) || ((safe_rshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_s(((~((((*l_821) == &g_608[2]) > ((*l_678) |= (1L < ((l_871 == (l_872 = &g_245)) > 4294967291UL)))) & g_44[2][4][0])) , l_852[4]), 12)), 7)) <= 0xB36CL)));
            (**l_821) = (void*)0;
        }
        else
        { /* block id: 345 */
            const int32_t *l_892 = (void*)0;
            const int32_t **l_891[6] = {&l_892,&l_892,&l_892,&l_892,&l_892,&l_892};
            int i;
            for (l_804 = 0; l_804 < 9; l_804 += 1)
            {
                for (l_818 = 0; l_818 < 4; l_818 += 1)
                {
                    l_824[l_804][l_818] = &l_825;
                }
            }
            for (g_854.f2 = 0; (g_854.f2 < (-14)); g_854.f2 = safe_sub_func_int32_t_s_s(g_854.f2, 1))
            { /* block id: 349 */
                uint16_t l_877[7][8] = {{65535UL,0xE8AFL,0x3C42L,65535UL,0xE8AFL,0xC2BCL,7UL,0xC2BCL},{2UL,65535UL,65530UL,65535UL,2UL,0xC7D0L,65535UL,2UL},{0xC2BCL,0x59C9L,0x652EL,0xC2BCL,1UL,0xE8AFL,0xA011L,65535UL},{0x59C9L,0xA011L,0x652EL,65535UL,0x8FE4L,0x8FE4L,65535UL,0x652EL},{1UL,1UL,65530UL,2UL,0UL,5UL,7UL,1UL},{0xA011L,0x59C9L,0x3C42L,0x8FE4L,7UL,0x3C42L,0x652EL,1UL},{0x59C9L,0xC2BCL,0xA011L,2UL,0xA011L,65528UL,0xE8AFL,5UL}};
                int i, j;
                for (l_746 = 7; (l_746 <= 41); l_746 = safe_add_func_int8_t_s_s(l_746, 5))
                { /* block id: 352 */
                    uint8_t l_886 = 255UL;
                    l_877[2][2]--;
                    for (g_684.f2 = 1; (g_684.f2 <= 6); g_684.f2 += 1)
                    { /* block id: 356 */
                        int i;
                        if ((**p_55))
                            break;
                        (*g_654) ^= (safe_sub_func_int16_t_s_s((&p_56 == (void*)0), ((safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u((l_886 > p_56), ((l_812[g_684.f2] = (*g_655)) == (void*)0))), 2)) , 65527UL)));
                    }
                }
                for (g_359 = 0; (g_359 >= (-11)); g_359--)
                { /* block id: 364 */
                    const int32_t *l_890[1][4];
                    const int32_t **l_889 = &l_890[0][1];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 4; j++)
                            l_890[i][j] = &l_795[3][7][0];
                    }
                    (*l_889) = func_77(&l_843);
                }
            }
            g_893 = func_77((*g_655));
        }
    }
    return g_29;
}


/* ------------------------------------------ */
/* 
 * reads : g_74 g_44 g_88 g_94 g_92 g_244 g_245 g_634 g_456 g_29
 * writes: g_74 g_88 g_92 g_94 g_96 g_154
 */
static int8_t  func_71(const int32_t * p_72)
{ /* block id: 7 */
    int32_t l_499 = 0x2EB00099L;
    int32_t l_500 = 0L;
    int32_t l_501 = 0x30C5986FL;
    int32_t l_502 = 0x76DDEF46L;
    int32_t l_503 = 9L;
    int32_t l_504[2][2][2] = {{{(-1L),(-1L)},{(-1L),(-1L)}},{{(-1L),(-1L)},{(-1L),(-1L)}}};
    int8_t l_505 = 0x53L;
    uint32_t l_507[10][1] = {{0x2BB90EBDL},{4294967291UL},{0x2BB90EBDL},{4294967291UL},{0x2BB90EBDL},{4294967291UL},{0x2BB90EBDL},{4294967291UL},{0x2BB90EBDL},{4294967291UL}};
    union U0 *l_510 = &g_511;
    int i, j, k;
    for (g_74 = 16; (g_74 != (-9)); g_74--)
    { /* block id: 10 */
        const int32_t *l_497 = &g_74;
        const int32_t **l_496 = &l_497;
        int32_t *l_498[8][2][3] = {{{&g_44[3][3][3],&g_94[0],&g_44[3][3][3]},{&g_44[1][1][3],(void*)0,(void*)0}},{{&g_94[3],&g_94[0],&g_94[3]},{&g_44[1][1][3],&g_44[1][1][3],(void*)0}},{{&g_44[3][3][3],&g_94[0],&g_44[3][3][3]},{&g_44[1][1][3],(void*)0,(void*)0}},{{&g_94[3],&g_94[0],&g_94[3]},{&g_44[1][1][3],&g_44[1][1][3],(void*)0}},{{&g_44[3][3][3],&g_94[0],&g_44[3][3][3]},{&g_44[1][1][3],(void*)0,(void*)0}},{{&g_94[3],&g_94[0],&g_94[3]},{&g_44[1][1][3],&g_44[1][1][3],(void*)0}},{{&g_44[3][3][3],&g_94[0],&g_44[3][3][3]},{&g_44[1][1][3],(void*)0,(void*)0}},{{&g_94[3],&g_94[0],&g_94[3]},{&g_44[1][1][3],&g_44[1][1][3],(void*)0}}};
        int16_t l_506 = 1L;
        float l_575[2];
        float *l_625 = (void*)0;
        int8_t *l_633 = &g_511.f2;
        int16_t *l_639[7];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_575[i] = 0x0.Dp-1;
        for (i = 0; i < 7; i++)
            l_639[i] = (void*)0;
        (*l_496) = func_77(&g_44[1][1][2]);
        l_507[9][0]--;
        for (l_501 = 3; (l_501 >= 0); l_501 -= 1)
        { /* block id: 151 */
            union U0 **l_512 = &l_510;
            int32_t l_525 = 0x9C805CA0L;
            int16_t l_550 = (-3L);
            uint16_t **l_551 = &g_245;
            int32_t l_568 = 0x6B20CCCEL;
            int32_t l_571 = (-8L);
            l_510 = l_510;
            for (l_503 = 1; (l_503 >= 0); l_503 -= 1)
            { /* block id: 155 */
                union U0 **l_513[6] = {&l_510,&l_510,(void*)0,&l_510,&l_510,(void*)0};
                uint32_t *l_524 = &g_252;
                uint16_t ***l_555 = &l_551;
                uint16_t ****l_554 = &l_555;
                int32_t *l_558 = &l_504[l_503][l_503][l_503];
                uint32_t l_559 = 0x41EA5127L;
                int32_t *l_567[8] = {&l_502,&l_504[0][0][1],&l_502,&l_504[0][0][1],&l_502,&l_504[0][0][1],&l_502,&l_504[0][0][1]};
                uint8_t l_572 = 254UL;
                uint64_t l_576 = 18446744073709551609UL;
                int i, j, k;
            }
        }
        l_504[0][1][1] |= (((safe_rshift_func_int16_t_s_u((g_154 = ((((*l_497) < ((safe_rshift_func_int8_t_s_s(((l_633 == l_633) || ((**g_244) = l_507[4][0])), 3)) , ((**l_496) & g_634))) && (safe_mod_func_int64_t_s_s(0xB6F458647AEF45D9LL, ((safe_lshift_func_int16_t_s_u((*l_497), 10)) ^ l_507[9][0])))) & g_456[4][0][1])), l_503)) , g_94[3]) >= g_29);
    }
    return l_507[5][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_44 g_88 g_94 g_92 g_657 g_74
 * writes: g_88 g_92 g_94 g_96
 */
static const int32_t * func_77(int32_t * p_78)
{ /* block id: 11 */
    uint16_t l_83 = 0x64BBL;
    int32_t *l_84 = &g_44[4][1][2];
    uint16_t *l_87 = &g_88;
    uint16_t *l_91 = &g_92;
    int32_t *l_93 = &g_94[3];
    float *l_95 = &g_96;
    int32_t l_120 = 0xA4828161L;
    int32_t l_125 = 0xA28A71BBL;
    int32_t l_127 = 0xB94DD076L;
    int32_t l_128 = 0x32212A48L;
    int32_t l_130 = 1L;
    int32_t l_163[1];
    uint16_t l_199[1][1];
    uint64_t l_213 = 0x3621335F78ABD924LL;
    int32_t *l_218[8][4] = {{&l_130,&g_74,&l_128,&g_94[2]},{&g_74,(void*)0,(void*)0,&l_163[0]},{&l_125,&l_130,&g_94[2],&l_163[0]},{(void*)0,(void*)0,(void*)0,&g_94[2]},{&l_120,&g_74,&g_44[5][1][0],&l_120},{&l_125,&g_94[2],&l_128,&g_74},{&g_94[2],(void*)0,&l_128,&l_128},{&l_125,&l_125,&g_44[5][1][0],&l_163[0]}};
    union U1 l_248[5] = {{0xF.EA0DCDp+68},{0xF.EA0DCDp+68},{0xF.EA0DCDp+68},{0xF.EA0DCDp+68},{0xF.EA0DCDp+68}};
    int16_t l_289 = 7L;
    uint8_t l_354 = 0UL;
    int16_t l_357[5][10] = {{(-1L),(-1L),0xCD34L,(-1L),(-1L),0xCD34L,(-1L),(-1L),0xCD34L,(-1L)},{(-1L),3L,3L,(-1L),3L,3L,(-1L),3L,3L,(-1L)},{3L,(-1L),3L,3L,(-1L),3L,3L,(-1L),3L,3L},{(-1L),(-1L),0xCD34L,(-1L),(-1L),0xCD34L,(-1L),(-1L),0xCD34L,(-1L)},{(-1L),3L,3L,(-1L),3L,3L,(-1L),3L,3L,(-1L)}};
    int32_t l_412 = 0x4415F136L;
    int i, j;
    for (i = 0; i < 1; i++)
        l_163[i] = 1L;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
            l_199[i][j] = 65535UL;
    }
    (*l_93) ^= (safe_mod_func_int64_t_s_s((((safe_sub_func_uint64_t_u_u(l_83, (((l_84 == &g_44[5][1][0]) ^ (*p_78)) != (safe_sub_func_int16_t_s_s((((((*l_91) = (--(*l_87))) || (*l_84)) | g_44[4][4][2]) ^ (*l_84)), g_44[5][2][3]))))) == (0x3E233DF5L != (-1L))) < (*l_84)), 0x80D8AAF453288EE1LL));
    (*l_95) = (*l_84);
    for (g_92 = (-20); (g_92 > 7); ++g_92)
    { /* block id: 18 */
        float *l_101[6][9] = {{(void*)0,&g_96,&g_96,&g_96,&g_96,&g_96,(void*)0,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{(void*)0,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,(void*)0,&g_96},{&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,&g_96,(void*)0,&g_96}};
        int32_t l_106 = 0x770DDAD4L;
        int32_t l_129 = (-1L);
        uint16_t l_132[1];
        float l_214 = 0x1.349419p+10;
        int32_t **l_253 = &l_218[2][1];
        uint8_t l_255 = 247UL;
        int32_t l_272 = 1L;
        union U0 * const l_349[2][3][10] = {{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}},{{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350},{&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350,&g_350}}};
        uint32_t l_409[1];
        union U0 *l_484 = (void*)0;
        int16_t l_489 = (-1L);
        uint16_t l_491[10][9][2] = {{{0x2464L,0xCA07L},{1UL,0x945DL},{65534UL,65535UL},{0UL,65534UL},{65535UL,0xF82EL},{65535UL,65534UL},{0UL,65535UL},{65534UL,0x945DL},{1UL,0xCA07L}},{{0x2464L,0x341DL},{0x341DL,65535UL},{0UL,0xBDBEL},{65530UL,65527UL},{0UL,65527UL},{0xC7A9L,0UL},{65527UL,65535UL},{65535UL,3UL},{0x9FC5L,65535UL}},{{65526UL,0x311AL},{0xF82EL,0x9328L},{0x22CCL,65530UL},{3UL,0x5873L},{0x5BCEL,0UL},{0x945DL,65535UL},{0x3862L,65533UL},{65535UL,65533UL},{0x3862L,65535UL}},{{0x945DL,0UL},{0x5BCEL,0x5873L},{3UL,65530UL},{0x22CCL,0x9328L},{0xF82EL,0x311AL},{65526UL,65535UL},{0x9FC5L,3UL},{65535UL,65535UL},{65527UL,0UL}},{{0xC7A9L,65527UL},{0UL,65527UL},{65530UL,0xBDBEL},{0UL,65535UL},{0x341DL,0x341DL},{0x2464L,0xCA07L},{1UL,0x945DL},{65534UL,65535UL},{0UL,65534UL}},{{65535UL,0xF82EL},{65535UL,65534UL},{0UL,65535UL},{65534UL,65530UL},{0xBDBEL,0x1524L},{0xCA07L,0UL},{0UL,0UL},{0UL,0x945DL},{0x5A56L,1UL}},{{0x6EDFL,0xF82EL},{65535UL,3UL},{1UL,65527UL},{65535UL,0xE5CEL},{0x2285L,0x3862L},{0x5BCEL,65535UL},{65534UL,3UL},{65533UL,0x5A56L},{0xE5CEL,0xC7A9L}},{{0x2464L,0UL},{65530UL,0UL},{1UL,0x311AL},{0x3862L,0x311AL},{1UL,0UL},{65530UL,0UL},{0x2464L,0xC7A9L},{0xE5CEL,0x5A56L},{65533UL,3UL}},{{65534UL,65535UL},{0x5BCEL,0x3862L},{0x2285L,0xE5CEL},{65535UL,65527UL},{1UL,3UL},{65535UL,0xF82EL},{0x6EDFL,1UL},{0x5A56L,0x945DL},{0UL,0UL}},{{0UL,0UL},{0xCA07L,0x1524L},{0xBDBEL,65530UL},{0x5873L,65535UL},{65535UL,0x5873L},{0UL,65534UL},{0UL,0x5873L},{65535UL,65535UL},{0x5873L,65530UL}}};
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_132[i] = 0UL;
        for (i = 0; i < 1; i++)
            l_409[i] = 0x8279E18DL;
        for (l_83 = 0; (l_83 <= 1); ++l_83)
        { /* block id: 21 */
            int8_t l_104 = 0x7DL;
            int32_t **l_105 = &l_84;
            int32_t l_131 = 0x2B9DCA82L;
            int64_t *l_192 = (void*)0;
            uint16_t *l_383 = &l_199[0][0];
            uint8_t *l_388 = (void*)0;
            volatile int32_t *l_414 = &g_415;
            union U0 *l_480 = &g_481;
            float *l_490 = &l_248[1].f0;
            (*l_93) &= (((void*)0 != l_101[5][2]) || 0x0CL);
        }
    }
    return p_78;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_29, "g_29", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_44[i][j][k], "g_44[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_74, "g_74", print_hash_value);
    transparent_crc(g_88, "g_88", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_94[i], "g_94[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_96, sizeof(g_96), "g_96", print_hash_value);
    transparent_crc(g_152, "g_152", print_hash_value);
    transparent_crc(g_154, "g_154", print_hash_value);
    transparent_crc(g_190, "g_190", print_hash_value);
    transparent_crc(g_220.f0, "g_220.f0", print_hash_value);
    transparent_crc(g_220.f1, "g_220.f1", print_hash_value);
    transparent_crc(g_220.f2, "g_220.f2", print_hash_value);
    transparent_crc(g_232, "g_232", print_hash_value);
    transparent_crc(g_252, "g_252", print_hash_value);
    transparent_crc_bytes (&g_285, sizeof(g_285), "g_285", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_302[i], "g_302[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_318.f2, "g_318.f2", print_hash_value);
    transparent_crc(g_329.f0, "g_329.f0", print_hash_value);
    transparent_crc(g_329.f1, "g_329.f1", print_hash_value);
    transparent_crc(g_329.f2, "g_329.f2", print_hash_value);
    transparent_crc(g_350.f0, "g_350.f0", print_hash_value);
    transparent_crc(g_350.f1, "g_350.f1", print_hash_value);
    transparent_crc(g_350.f2, "g_350.f2", print_hash_value);
    transparent_crc(g_352.f0, "g_352.f0", print_hash_value);
    transparent_crc(g_352.f1, "g_352.f1", print_hash_value);
    transparent_crc(g_352.f2, "g_352.f2", print_hash_value);
    transparent_crc(g_359, "g_359", print_hash_value);
    transparent_crc(g_376, "g_376", print_hash_value);
    transparent_crc(g_415, "g_415", print_hash_value);
    transparent_crc_bytes (&g_437.f0, sizeof(g_437.f0), "g_437.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_456[i][j][k], "g_456[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_481.f2, "g_481.f2", print_hash_value);
    transparent_crc(g_495, "g_495", print_hash_value);
    transparent_crc(g_511.f0, "g_511.f0", print_hash_value);
    transparent_crc(g_511.f1, "g_511.f1", print_hash_value);
    transparent_crc(g_511.f2, "g_511.f2", print_hash_value);
    transparent_crc(g_634, "g_634", print_hash_value);
    transparent_crc(g_657, "g_657", print_hash_value);
    transparent_crc(g_684.f2, "g_684.f2", print_hash_value);
    transparent_crc(g_687.f0, "g_687.f0", print_hash_value);
    transparent_crc(g_687.f1, "g_687.f1", print_hash_value);
    transparent_crc(g_687.f2, "g_687.f2", print_hash_value);
    transparent_crc(g_732, "g_732", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_847[i].f0, "g_847[i].f0", print_hash_value);
        transparent_crc(g_847[i].f1, "g_847[i].f1", print_hash_value);
        transparent_crc(g_847[i].f2, "g_847[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_849.f2, "g_849.f2", print_hash_value);
    transparent_crc(g_854.f2, "g_854.f2", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_897[i][j].f0, "g_897[i][j].f0", print_hash_value);
            transparent_crc(g_897[i][j].f1, "g_897[i][j].f1", print_hash_value);
            transparent_crc(g_897[i][j].f2, "g_897[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_990[i], "g_990[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_1045.f0, sizeof(g_1045.f0), "g_1045.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1115[i], "g_1115[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_1226[i][j], "g_1226[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1281[i], "g_1281[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1359[i], "g_1359[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1390[i], "g_1390[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1458[i], "g_1458[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1476, "g_1476", print_hash_value);
    transparent_crc_bytes (&g_1540.f0, sizeof(g_1540.f0), "g_1540.f0", print_hash_value);
    transparent_crc_bytes (&g_1562.f0, sizeof(g_1562.f0), "g_1562.f0", print_hash_value);
    transparent_crc(g_1581, "g_1581", print_hash_value);
    transparent_crc(g_1617.f2, "g_1617.f2", print_hash_value);
    transparent_crc_bytes (&g_2006.f0, sizeof(g_2006.f0), "g_2006.f0", print_hash_value);
    transparent_crc(g_2123, "g_2123", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_2211[i][j].f2, "g_2211[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2241, "g_2241", print_hash_value);
    transparent_crc(g_2273, "g_2273", print_hash_value);
    transparent_crc(g_2421, "g_2421", print_hash_value);
    transparent_crc(g_2437.f0, "g_2437.f0", print_hash_value);
    transparent_crc(g_2437.f1, "g_2437.f1", print_hash_value);
    transparent_crc(g_2437.f2, "g_2437.f2", print_hash_value);
    transparent_crc(g_2545, "g_2545", print_hash_value);
    transparent_crc(g_2593, "g_2593", print_hash_value);
    transparent_crc(g_2612, "g_2612", print_hash_value);
    transparent_crc(g_2632, "g_2632", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 692
XXX total union variables: 29

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 51
breakdown:
   depth: 1, occurrence: 304
   depth: 2, occurrence: 106
   depth: 3, occurrence: 6
   depth: 4, occurrence: 1
   depth: 5, occurrence: 3
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 1
   depth: 14, occurrence: 3
   depth: 15, occurrence: 2
   depth: 17, occurrence: 5
   depth: 18, occurrence: 6
   depth: 19, occurrence: 3
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 22, occurrence: 3
   depth: 23, occurrence: 1
   depth: 24, occurrence: 4
   depth: 27, occurrence: 1
   depth: 28, occurrence: 3
   depth: 29, occurrence: 2
   depth: 30, occurrence: 2
   depth: 31, occurrence: 2
   depth: 35, occurrence: 2
   depth: 36, occurrence: 1
   depth: 38, occurrence: 1
   depth: 43, occurrence: 1
   depth: 46, occurrence: 1
   depth: 51, occurrence: 1

XXX total number of pointers: 627

XXX times a variable address is taken: 1354
XXX times a pointer is dereferenced on RHS: 298
breakdown:
   depth: 1, occurrence: 229
   depth: 2, occurrence: 57
   depth: 3, occurrence: 5
   depth: 4, occurrence: 7
XXX times a pointer is dereferenced on LHS: 320
breakdown:
   depth: 1, occurrence: 270
   depth: 2, occurrence: 40
   depth: 3, occurrence: 5
   depth: 4, occurrence: 5
XXX times a pointer is compared with null: 52
XXX times a pointer is compared with address of another variable: 10
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 8141

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1608
   level: 2, occurrence: 420
   level: 3, occurrence: 66
   level: 4, occurrence: 77
   level: 5, occurrence: 2
XXX number of pointers point to pointers: 238
XXX number of pointers point to scalars: 358
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28.1
XXX average alias set size: 1.57

XXX times a non-volatile is read: 1946
XXX times a non-volatile is write: 1045
XXX times a volatile is read: 55
XXX    times read thru a pointer: 27
XXX times a volatile is write: 17
XXX    times written thru a pointer: 10
XXX times a volatile is available for access: 2.76e+03
XXX percentage of non-volatile access: 97.6

XXX forward jumps: 0
XXX backward jumps: 14

XXX stmts: 319
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 42
   depth: 2, occurrence: 74
   depth: 3, occurrence: 65
   depth: 4, occurrence: 49
   depth: 5, occurrence: 54

XXX percentage a fresh-made variable is used: 20.3
XXX percentage an existing variable is used: 79.7
********************* end of statistics **********************/


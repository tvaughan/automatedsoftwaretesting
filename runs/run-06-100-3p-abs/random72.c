/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3572035029
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_5 = 1L;
static int32_t *g_4 = &g_5;
static int32_t g_20 = (-9L);
static int32_t g_40 = 1L;
static uint16_t g_47 = 6UL;
static uint16_t g_66 = 9UL;
static int32_t *g_75 = (void*)0;
static int32_t ** volatile g_74[3][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static int32_t g_80[4][10][6] = {{{0L,1L,(-1L),1L,(-1L),(-1L)},{1L,0x831C21D0L,0x831C21D0L,1L,0x32C3216FL,(-1L)},{0L,(-1L),(-1L),(-4L),(-1L),1L},{(-4L),1L,(-4L),(-1L),(-1L),0L},{0x16B841AEL,0L,0x32C3216FL,(-4L),(-4L),0x32C3216FL},{0x1FB9D331L,0x1FB9D331L,0x831C21D0L,(-4L),0L,(-1L)},{0x16B841AEL,0x831C21D0L,0L,(-1L),0L,0x831C21D0L},{(-4L),0x16B841AEL,0L,1L,0x1FB9D331L,(-1L)},{0x32C3216FL,1L,0x831C21D0L,0x831C21D0L,1L,0x32C3216FL},{0x831C21D0L,1L,0x32C3216FL,(-1L),0x1FB9D331L,0L}},{{0L,0x16B841AEL,(-4L),0x16B841AEL,0L,1L},{0L,0x831C21D0L,0x16B841AEL,(-1L),0L,0L},{0x831C21D0L,0x1FB9D331L,0x1FB9D331L,0x831C21D0L,(-4L),0L},{0x32C3216FL,0L,0x16B841AEL,1L,(-1L),1L},{(-4L),1L,(-4L),(-1L),(-1L),0L},{0x16B841AEL,0L,0x32C3216FL,(-4L),(-4L),0x32C3216FL},{0x1FB9D331L,0x1FB9D331L,0x831C21D0L,(-4L),0L,(-1L)},{0x16B841AEL,0x831C21D0L,0L,(-1L),0L,0x831C21D0L},{(-4L),0x16B841AEL,0L,1L,0x1FB9D331L,(-1L)},{0x32C3216FL,1L,0x831C21D0L,0x831C21D0L,1L,0x32C3216FL}},{{0x831C21D0L,1L,0x32C3216FL,(-1L),0x1FB9D331L,0L},{0L,0x16B841AEL,(-4L),0x16B841AEL,0L,1L},{0L,0x831C21D0L,0x16B841AEL,(-1L),0L,0L},{0x831C21D0L,0x1FB9D331L,0x1FB9D331L,0x831C21D0L,(-4L),0L},{0x32C3216FL,0L,0x16B841AEL,1L,(-1L),1L},{(-4L),1L,(-4L),(-1L),(-1L),0L},{0x16B841AEL,0L,0x32C3216FL,(-4L),(-4L),0x32C3216FL},{0x1FB9D331L,0x1FB9D331L,0x831C21D0L,(-4L),0L,(-1L)},{0x16B841AEL,0x831C21D0L,0L,(-1L),0L,0x831C21D0L},{(-4L),0x16B841AEL,0L,1L,0x1FB9D331L,(-1L)}},{{0x32C3216FL,1L,0x831C21D0L,0x831C21D0L,1L,0x32C3216FL},{0x831C21D0L,1L,0x32C3216FL,(-1L),0x1FB9D331L,0L},{0L,0x16B841AEL,(-4L),0x16B841AEL,0L,1L},{0L,0x831C21D0L,0x16B841AEL,(-1L),0L,0L},{0x831C21D0L,0x1FB9D331L,0x1FB9D331L,0x1FB9D331L,1L,0L},{(-4L),0L,1L,0x831C21D0L,0L,0x831C21D0L},{1L,(-1L),1L,0x16B841AEL,0L,0x32C3216FL},{1L,0L,(-4L),1L,1L,(-4L)},{(-1L),(-1L),0x1FB9D331L,1L,0L,0x16B841AEL},{1L,0x1FB9D331L,0x32C3216FL,0x16B841AEL,0x32C3216FL,0x1FB9D331L}}};
static int32_t g_88 = 0x74E5B049L;
static int32_t g_89 = 1L;
static uint32_t g_104 = 0x97C1FB1AL;
static uint16_t g_113 = 0x0A9AL;
static uint32_t g_126 = 1UL;
static float g_141 = 0x1.3p+1;
static int8_t g_153 = 0x4AL;
static int8_t * const  volatile g_152[5] = {&g_153,&g_153,&g_153,&g_153,&g_153};
static uint64_t g_156 = 7UL;
static int16_t g_162 = 0xF7A9L;
static uint16_t *g_169 = &g_66;
static uint16_t **g_168 = &g_169;
static uint16_t *** volatile g_167[4][8][1] = {{{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168}},{{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168}},{{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168}},{{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168},{&g_168}}};
static uint16_t *** volatile g_170 = &g_168;/* VOLATILE GLOBAL g_170 */
static int16_t g_185 = 0x1A80L;
static int64_t g_197 = 0L;
static int32_t g_199 = (-3L);
static uint32_t *g_270 = &g_104;
static volatile int64_t g_308 = 0L;/* VOLATILE GLOBAL g_308 */
static volatile int64_t *g_307 = &g_308;
static uint16_t g_373 = 0UL;
static int32_t *g_376 = &g_40;
static int32_t ** volatile g_375 = &g_376;/* VOLATILE GLOBAL g_375 */
static int32_t ** volatile g_380 = &g_75;/* VOLATILE GLOBAL g_380 */
static int32_t ** volatile g_382[8] = {&g_75,&g_75,&g_75,&g_75,&g_75,&g_75,&g_75,&g_75};
static uint16_t ***g_445 = &g_168;
static uint16_t ****g_444 = &g_445;
static float g_462 = 0xD.054940p-73;
static uint16_t *****g_514 = (void*)0;
static uint8_t g_571 = 255UL;
static uint8_t *g_570 = &g_571;
static float *g_579 = &g_462;
static float ** volatile g_578 = &g_579;/* VOLATILE GLOBAL g_578 */
static int16_t *g_582 = &g_162;
static int32_t ** volatile g_587 = (void*)0;/* VOLATILE GLOBAL g_587 */
static int32_t ** volatile g_588 = &g_376;/* VOLATILE GLOBAL g_588 */
static uint64_t *** volatile g_709 = (void*)0;/* VOLATILE GLOBAL g_709 */
static uint64_t *g_712 = &g_156;
static uint64_t **g_711 = &g_712;
static uint64_t *** const  volatile g_710 = &g_711;/* VOLATILE GLOBAL g_710 */
static int8_t * const g_732 = (void*)0;
static int8_t * const *g_731 = &g_732;
static int8_t g_735 = 0xD2L;
static int32_t ** const  volatile g_738 = (void*)0;/* VOLATILE GLOBAL g_738 */
static int32_t ** const  volatile g_739 = (void*)0;/* VOLATILE GLOBAL g_739 */
static int32_t ** volatile g_740 = &g_75;/* VOLATILE GLOBAL g_740 */
static int32_t ** volatile g_741[1] = {&g_376};
static int32_t ** volatile g_742 = &g_75;/* VOLATILE GLOBAL g_742 */
static volatile int8_t g_754 = 0L;/* VOLATILE GLOBAL g_754 */
static const uint16_t ***g_772 = (void*)0;
static const uint16_t ****g_771 = &g_772;
static float g_834[4][6] = {{0x1.0p+1,0x1.0p+1,0x8.77EF29p-64,0x1.0p+1,0x1.0p+1,0x8.77EF29p-64},{0x1.0p+1,0x1.0p+1,0x8.77EF29p-64,0x1.0p+1,0x1.0p+1,0x8.77EF29p-64},{0x1.0p+1,0x1.0p+1,0x8.77EF29p-64,0x1.0p+1,0x1.0p+1,0x8.77EF29p-64},{0x1.0p+1,0x1.0p+1,0x8.77EF29p-64,0x1.0p+1,0x1.0p+1,0x8.77EF29p-64}};
static volatile int8_t g_853 = 0L;/* VOLATILE GLOBAL g_853 */
static volatile uint8_t g_855 = 0x6DL;/* VOLATILE GLOBAL g_855 */
static volatile uint32_t *g_988 = (void*)0;
static volatile uint32_t * volatile * volatile g_987[4][4][5] = {{{&g_988,(void*)0,(void*)0,&g_988,&g_988},{(void*)0,&g_988,(void*)0,&g_988,(void*)0},{&g_988,&g_988,&g_988,(void*)0,&g_988},{&g_988,&g_988,&g_988,&g_988,&g_988}},{{&g_988,(void*)0,&g_988,&g_988,&g_988},{(void*)0,&g_988,(void*)0,&g_988,(void*)0},{&g_988,&g_988,(void*)0,(void*)0,&g_988},{&g_988,&g_988,&g_988,&g_988,&g_988}},{{&g_988,(void*)0,(void*)0,&g_988,&g_988},{(void*)0,&g_988,(void*)0,&g_988,(void*)0},{&g_988,&g_988,&g_988,(void*)0,&g_988},{&g_988,&g_988,&g_988,&g_988,&g_988}},{{&g_988,(void*)0,&g_988,&g_988,&g_988},{(void*)0,&g_988,(void*)0,&g_988,(void*)0},{&g_988,&g_988,(void*)0,(void*)0,&g_988},{&g_988,&g_988,&g_988,&g_988,&g_988}}};
static volatile uint32_t * volatile * volatile * const  volatile g_989[3] = {&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]};
static volatile uint32_t * volatile * volatile * volatile g_990[8][9][3] = {{{&g_987[0][2][1],(void*)0,&g_987[0][2][1]},{(void*)0,&g_987[0][2][1],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[3][0][3],(void*)0,&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][1][2],&g_987[0][3][0]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[0][0][2],&g_987[0][0][2],(void*)0}},{{&g_987[3][0][3],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],(void*)0,&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][2][1],(void*)0},{&g_987[3][0][3],&g_987[0][1][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][2][1],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0}},{{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[3][0][3],(void*)0,&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][1][2],&g_987[0][3][0]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[3][0][3],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],(void*)0,&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]}},{{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][2][1],(void*)0},{&g_987[3][0][3],&g_987[0][1][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][2][1],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[3][0][3],(void*)0,&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][1][2],&g_987[0][3][0]}},{{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[3][0][3],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],(void*)0,&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][2][1],(void*)0},{&g_987[3][0][3],&g_987[0][1][2],&g_987[0][0][2]}},{{&g_987[0][0][2],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][2][1],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[3][0][3],(void*)0,&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][1][2],&g_987[0][3][0]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[0][0][2],&g_987[0][0][2],(void*)0}},{{&g_987[3][0][3],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],(void*)0,&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][2][1],(void*)0},{&g_987[3][0][3],&g_987[0][1][2],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[1][0][3],&g_987[0][0][2]},{&g_987[0][0][2],&g_987[0][2][1],&g_987[3][0][3]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0}},{{&g_987[0][0][2],&g_987[0][0][2],&g_987[3][0][3]},{&g_987[3][0][3],(void*)0,&g_987[3][0][3]},{&g_987[1][3][4],&g_987[0][0][2],(void*)0},{&g_987[1][1][0],&g_987[0][0][2],(void*)0},{&g_987[1][1][0],&g_987[3][0][3],&g_987[0][0][1]},{&g_987[1][3][4],&g_987[0][3][0],&g_987[0][1][0]},{&g_987[0][0][2],&g_987[0][0][2],(void*)0},{&g_987[1][3][4],&g_987[0][0][2],&g_987[3][3][3]},{&g_987[1][1][0],&g_987[0][3][0],&g_987[3][3][4]}}};
static float *g_1015 = &g_834[0][0];
static int32_t **g_1035[9][8][3] = {{{&g_376,&g_376,&g_376},{(void*)0,&g_75,&g_75},{&g_376,&g_376,&g_376},{&g_75,(void*)0,&g_376},{&g_376,(void*)0,&g_376},{(void*)0,&g_376,&g_376},{&g_376,(void*)0,&g_75},{&g_75,&g_376,&g_376}},{{&g_376,(void*)0,&g_376},{&g_75,(void*)0,&g_376},{(void*)0,&g_376,&g_376},{&g_75,&g_75,&g_376},{(void*)0,&g_376,(void*)0},{&g_376,&g_75,&g_75},{&g_75,&g_376,&g_376},{&g_376,&g_376,(void*)0}},{{&g_75,&g_376,&g_75},{&g_376,&g_75,&g_75},{&g_75,&g_75,(void*)0},{&g_376,&g_376,(void*)0},{(void*)0,&g_75,&g_75},{&g_75,(void*)0,&g_75},{(void*)0,(void*)0,&g_376},{&g_75,&g_376,&g_75}},{{&g_376,&g_75,&g_376},{&g_75,&g_75,(void*)0},{&g_376,&g_376,&g_376},{(void*)0,&g_75,&g_75},{&g_376,&g_376,&g_376},{&g_75,&g_75,&g_75},{&g_376,&g_376,&g_75},{(void*)0,(void*)0,(void*)0}},{{&g_376,(void*)0,(void*)0},{(void*)0,&g_376,&g_75},{&g_75,&g_376,&g_75},{&g_376,&g_376,(void*)0},{&g_75,&g_376,&g_376},{&g_376,&g_376,&g_75},{&g_376,(void*)0,(void*)0},{&g_376,(void*)0,&g_376}},{{&g_376,&g_376,&g_376},{&g_376,&g_75,&g_376},{&g_376,&g_376,&g_376},{&g_75,&g_75,&g_376},{&g_376,&g_376,&g_75},{(void*)0,&g_75,&g_376},{&g_376,&g_75,&g_376},{&g_75,&g_376,&g_376}},{{&g_376,(void*)0,&g_376},{&g_376,(void*)0,&g_75},{&g_376,&g_75,&g_376},{&g_376,&g_376,&g_376},{&g_376,&g_75,&g_376},{&g_376,&g_75,(void*)0},{&g_75,&g_376,(void*)0},{&g_376,&g_376,(void*)0}},{{&g_75,&g_376,&g_376},{&g_75,&g_75,&g_75},{&g_376,&g_376,&g_376},{&g_75,&g_75,&g_376},{&g_75,&g_75,&g_75},{&g_75,&g_376,&g_376},{&g_75,&g_75,&g_376},{(void*)0,&g_75,(void*)0}},{{&g_376,(void*)0,&g_75},{&g_75,&g_75,(void*)0},{&g_376,&g_75,&g_75},{&g_376,&g_376,&g_376},{&g_376,&g_75,&g_376},{&g_75,&g_75,&g_376},{&g_75,&g_376,&g_75},{&g_75,&g_75,&g_376}}};
static const int32_t ***g_1058 = (void*)0;
static const volatile int32_t g_1087 = 0x0755872DL;/* VOLATILE GLOBAL g_1087 */
static int64_t g_1396 = (-1L);
static float * volatile g_1447 = &g_834[2][5];/* VOLATILE GLOBAL g_1447 */
static const int64_t g_1506 = 0xB512C7A36503E9EDLL;
static const int64_t *g_1567[2] = {(void*)0,(void*)0};
static int32_t g_1601 = (-6L);
static int32_t * volatile g_1686 = &g_80[3][1][3];/* VOLATILE GLOBAL g_1686 */
static int32_t g_1696 = 0x5ED80302L;
static volatile int64_t g_1707[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
static const volatile uint32_t g_1809 = 5UL;/* VOLATILE GLOBAL g_1809 */
static const volatile int16_t g_1853 = 0x2589L;/* VOLATILE GLOBAL g_1853 */
static int64_t *g_1877 = &g_1396;
static int64_t **g_1876 = &g_1877;
static int32_t g_1902 = 0x01151559L;
static uint32_t g_1945 = 1UL;
static int32_t ** volatile g_1964 = &g_75;/* VOLATILE GLOBAL g_1964 */
static const float ** const g_1970 = (void*)0;
static const float ** const *g_1969[9][5] = {{&g_1970,&g_1970,&g_1970,&g_1970,&g_1970},{&g_1970,&g_1970,&g_1970,&g_1970,&g_1970},{&g_1970,&g_1970,&g_1970,&g_1970,&g_1970},{&g_1970,&g_1970,&g_1970,&g_1970,(void*)0},{&g_1970,&g_1970,&g_1970,&g_1970,&g_1970},{&g_1970,&g_1970,&g_1970,&g_1970,&g_1970},{&g_1970,&g_1970,(void*)0,&g_1970,&g_1970},{&g_1970,&g_1970,&g_1970,&g_1970,&g_1970},{&g_1970,&g_1970,&g_1970,&g_1970,(void*)0}};
static const float ** const ** const  volatile g_1968 = &g_1969[0][1];/* VOLATILE GLOBAL g_1968 */
static int8_t g_2003 = (-1L);
static uint32_t g_2009 = 0x04996C04L;
static uint64_t g_2011[9][4] = {{0x37F80987AD01D8C0LL,18446744073709551611UL,0xAB0131A8E5AD708ELL,0x37F80987AD01D8C0LL},{0xB2BFEBB8B244587BLL,0UL,18446744073709551615UL,18446744073709551611UL},{0UL,0x3DB2488EAF87EC9BLL,18446744073709551615UL,18446744073709551615UL},{0xB2BFEBB8B244587BLL,0xB2BFEBB8B244587BLL,0xAB0131A8E5AD708ELL,7UL},{0x37F80987AD01D8C0LL,0x50A2C7BB6C0B82E8LL,18446744073709551615UL,18446744073709551611UL},{18446744073709551615UL,18446744073709551611UL,0UL,18446744073709551615UL},{0xB2BFEBB8B244587BLL,18446744073709551611UL,18446744073709551615UL,18446744073709551611UL},{18446744073709551611UL,0x50A2C7BB6C0B82E8LL,18446744073709551615UL,7UL},{4UL,0xB2BFEBB8B244587BLL,0UL,18446744073709551615UL}};
static uint32_t *g_2046 = (void*)0;
static uint32_t *g_2047 = &g_126;
static const int32_t *g_2073 = &g_1696;
static int32_t ** volatile g_2075 = (void*)0;/* VOLATILE GLOBAL g_2075 */
static volatile uint32_t * volatile *g_2095 = &g_988;
static volatile uint32_t * volatile **g_2094 = &g_2095;
static int32_t ** volatile g_2102 = &g_4;/* VOLATILE GLOBAL g_2102 */
static int32_t ** volatile g_2103 = &g_4;/* VOLATILE GLOBAL g_2103 */
static int64_t ***g_2159 = &g_1876;
static uint8_t **g_2179 = &g_570;
static int32_t ***g_2197 = (void*)0;
static int32_t ****g_2196 = &g_2197;
static uint16_t g_2396 = 65535UL;
static int32_t **g_2462 = (void*)0;
static int32_t ***g_2461 = &g_2462;
static volatile uint32_t g_2521 = 0x87461912L;/* VOLATILE GLOBAL g_2521 */
static const int32_t *g_2528 = &g_199;
static const int32_t ** volatile g_2527 = &g_2528;/* VOLATILE GLOBAL g_2527 */
static int32_t ** volatile g_2537 = &g_75;/* VOLATILE GLOBAL g_2537 */
static uint8_t ***g_2539 = &g_2179;
static uint8_t **** volatile g_2538[8][2][3] = {{{&g_2539,&g_2539,&g_2539},{&g_2539,(void*)0,(void*)0}},{{&g_2539,(void*)0,(void*)0},{&g_2539,&g_2539,(void*)0}},{{&g_2539,&g_2539,&g_2539},{&g_2539,&g_2539,(void*)0}},{{&g_2539,&g_2539,&g_2539},{(void*)0,&g_2539,&g_2539}},{{&g_2539,&g_2539,&g_2539},{(void*)0,&g_2539,&g_2539}},{{(void*)0,(void*)0,&g_2539},{(void*)0,(void*)0,&g_2539}},{{&g_2539,&g_2539,&g_2539},{(void*)0,&g_2539,&g_2539}},{{&g_2539,&g_2539,&g_2539},{&g_2539,(void*)0,&g_2539}}};
static int32_t g_2561 = 0x9CF60873L;
static float g_2586 = 0xB.5F4D99p-31;
static volatile int8_t g_2596[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint16_t g_2632 = 1UL;
static int8_t *g_2643 = &g_2003;
static int8_t *g_2644 = &g_735;
static int64_t * volatile **g_2653 = (void*)0;
static int64_t * volatile *** volatile g_2652 = &g_2653;/* VOLATILE GLOBAL g_2652 */
static int32_t ** const  volatile g_2714[9][7] = {{(void*)0,&g_4,&g_376,&g_75,&g_4,&g_4,&g_4},{&g_376,&g_4,&g_4,&g_376,&g_376,&g_75,&g_75},{&g_75,&g_75,&g_75,&g_75,&g_75,&g_75,(void*)0},{&g_4,&g_75,&g_4,&g_4,(void*)0,&g_4,&g_376},{&g_75,(void*)0,&g_4,&g_4,&g_75,(void*)0,&g_4},{&g_75,&g_75,&g_4,&g_376,&g_75,&g_4,&g_4},{&g_75,&g_75,&g_4,&g_75,&g_75,&g_376,(void*)0},{&g_4,&g_4,&g_376,(void*)0,&g_75,(void*)0,&g_4},{&g_4,&g_4,(void*)0,&g_75,(void*)0,&g_376,&g_4}};
static int32_t ** volatile g_2715 = &g_376;/* VOLATILE GLOBAL g_2715 */
static float **g_2733 = &g_1015;
static float ***g_2732 = &g_2733;
static uint16_t g_2835 = 65534UL;
static float g_2836 = 0x1.Ap+1;
static uint64_t g_2837[9] = {0x7FB43E90D5ED99DFLL,0x7FB43E90D5ED99DFLL,0x24FB545785BFC5EDLL,0x7FB43E90D5ED99DFLL,0x7FB43E90D5ED99DFLL,0x24FB545785BFC5EDLL,0x7FB43E90D5ED99DFLL,0x7FB43E90D5ED99DFLL,0x24FB545785BFC5EDLL};
static uint8_t g_2935[10][6][4] = {{{0x3DL,0x3DL,0x95L,1UL},{0UL,0x6AL,0xABL,0UL},{8UL,0xB4L,8UL,0xABL},{5UL,0xB4L,0x49L,0UL},{0xB4L,0x6AL,0xD5L,1UL},{0UL,0x3DL,4UL,0x5BL}},{{0xC6L,0x39L,0xDAL,1UL},{0x37L,1UL,0x3DL,246UL},{0x63L,255UL,0x37L,1UL},{0x6AL,0x82L,1UL,5UL},{0x09L,0xABL,0x8AL,0x16L},{1UL,1UL,1UL,1UL}},{{0x7BL,8UL,8UL,0x64L},{0UL,0UL,252UL,0UL},{0x13L,0x55L,0x46L,0x1DL},{0x82L,0x64L,3UL,0x39L},{255UL,0UL,1UL,0UL},{0x95L,0UL,0x4CL,0UL}},{{0UL,1UL,0x13L,252UL},{0x64L,0x4CL,0x09L,0x55L},{0x39L,0xD5L,252UL,0xABL},{0x39L,0x1DL,0x09L,4UL},{0x64L,0xABL,0x13L,255UL},{0UL,0x5BL,0x4CL,0xB4L}},{{0x95L,0xE7L,1UL,8UL},{255UL,0xC6L,3UL,0x14L},{0x82L,0x49L,0x46L,0UL},{0x13L,255UL,252UL,0x82L},{0UL,246UL,8UL,0xD5L},{0x7BL,255UL,0x49L,8UL}},{{0x63L,255UL,250UL,0x13L},{0x5EL,0x63L,0x95L,0UL},{0UL,0x7BL,0x65L,252UL},{1UL,0x6AL,1UL,0UL},{0x65L,0UL,252UL,246UL},{3UL,0UL,0UL,3UL}},{{5UL,0x37L,0x14L,0xB4L},{1UL,0x16L,252UL,1UL},{1UL,0x10L,0x64L,1UL},{0x5BL,0x16L,0x09L,0xB4L},{255UL,0x37L,8UL,3UL},{1UL,0UL,1UL,246UL}},{{8UL,0UL,255UL,0UL},{255UL,0x6AL,0x3DL,252UL},{4UL,0x7BL,246UL,0UL},{0UL,0x63L,1UL,0x13L},{0UL,255UL,246UL,8UL},{8UL,0x1DL,0UL,0x14L}},{{246UL,0x3DL,0x10L,0UL},{1UL,1UL,5UL,0x4CL},{0xABL,252UL,0xD5L,0x39L},{0x64L,3UL,0x39L,1UL},{1UL,1UL,3UL,1UL},{1UL,1UL,1UL,0xABL}},{{0UL,1UL,0x5BL,0UL},{0x55L,246UL,255UL,1UL},{1UL,0x14L,255UL,0xABL},{0x55L,0x13L,0x5BL,0xF5L},{0UL,0xD5L,1UL,255UL},{1UL,255UL,3UL,5UL}}};
static uint8_t g_2953 = 1UL;
static float g_2976 = 0x0.8p+1;
static const int8_t *g_3114 = &g_735;
static const int8_t **g_3113 = &g_3114;
static const int8_t ** volatile *g_3112 = &g_3113;
static const int8_t ** volatile ** volatile g_3111 = &g_3112;/* VOLATILE GLOBAL g_3111 */
static const int8_t ** volatile ** volatile * volatile g_3115 = &g_3111;/* VOLATILE GLOBAL g_3115 */
static uint32_t **g_3148 = &g_2046;
static uint32_t ***g_3147 = &g_3148;
static int16_t **** volatile g_3181[1] = {(void*)0};
static int16_t **g_3184 = (void*)0;
static int16_t ***g_3183[8] = {&g_3184,&g_3184,&g_3184,&g_3184,&g_3184,&g_3184,&g_3184,&g_3184};
static int16_t **** volatile g_3182 = &g_3183[0];/* VOLATILE GLOBAL g_3182 */
static int8_t **g_3218 = &g_2643;
static int8_t ***g_3217 = &g_3218;
static int32_t * const * const **g_3232 = (void*)0;
static int32_t * const * const ***g_3231 = &g_3232;
static int32_t ** volatile g_3237 = &g_75;/* VOLATILE GLOBAL g_3237 */
static int8_t *****g_3308 = (void*)0;
static volatile int8_t g_3314 = 1L;/* VOLATILE GLOBAL g_3314 */
static volatile int8_t * const  volatile g_3313 = &g_3314;/* VOLATILE GLOBAL g_3313 */
static volatile int8_t * const  volatile * volatile g_3312[2] = {&g_3313,&g_3313};
static volatile int8_t * const  volatile * const  volatile *g_3311 = &g_3312[0];
static volatile int8_t * const  volatile * const  volatile **g_3310 = &g_3311;
static volatile int8_t * const  volatile * const  volatile ***g_3309 = &g_3310;
static float g_3408 = 0xD.02185Dp-67;
static uint8_t **g_3490 = &g_570;
static uint64_t ***** volatile g_3510 = (void*)0;/* VOLATILE GLOBAL g_3510 */
static const int8_t g_3517 = (-1L);
static int32_t *g_3613 = &g_89;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int32_t * func_2(int32_t * p_3);
static int16_t  func_10(int32_t * p_11, int32_t * p_12);
static int32_t * func_13(uint64_t  p_14);
static uint64_t  func_15(int64_t  p_16);
static int32_t  func_25(int32_t * p_26, int16_t  p_27);
static int32_t * func_28(int32_t * p_29, int32_t * p_30, uint8_t  p_31);
static uint32_t  func_32(int32_t * const  p_33, int32_t * p_34, const int32_t  p_35, uint16_t  p_36);
static const int32_t  func_41(int16_t  p_42, int32_t  p_43);
static int64_t  func_57(int64_t  p_58, uint16_t * const  p_59, int64_t  p_60);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_5 g_2179 g_570 g_571 g_1696 g_1877 g_1396 g_582 g_270 g_104 g_376 g_40 g_444 g_445 g_168 g_169 g_66 g_380 g_75
 * writes: g_5 g_1396 g_162 g_104 g_40 g_1696 g_3613
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_3614 = 0UL;
    g_3613 = func_2(g_4);
    (*g_4) |= (1UL | l_3614);
    return l_3614;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_2179 g_570 g_571 g_1696 g_1877 g_1396 g_582 g_270 g_104 g_376 g_40 g_444 g_445 g_168 g_169 g_66 g_380 g_75
 * writes: g_5 g_1396 g_162 g_104 g_40 g_1696
 */
static int32_t * func_2(int32_t * p_3)
{ /* block id: 1 */
    uint16_t l_6[2][6] = {{1UL,1UL,1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL,1UL,1UL}};
    int32_t *l_3537 = &g_1696;
    int32_t l_3554 = 1L;
    int32_t l_3560 = (-1L);
    int8_t l_3584[10] = {0x7FL,(-1L),1L,1L,(-1L),0x7FL,(-1L),1L,1L,(-1L)};
    int32_t l_3588 = 0xACBE08A4L;
    int16_t l_3590 = 5L;
    int32_t l_3591 = 0xF566A854L;
    int16_t *** const l_3611[6] = {(void*)0,(void*)0,&g_3184,(void*)0,(void*)0,&g_3184};
    int16_t *l_3612[1];
    int i, j;
    for (i = 0; i < 1; i++)
        l_3612[i] = &g_185;
    for (g_5 = 1; (g_5 >= 0); g_5 -= 1)
    { /* block id: 4 */
        uint8_t l_7 = 0UL;
        int32_t *l_2077 = (void*)0;
        int32_t **l_3536[8][2] = {{&g_376,(void*)0},{(void*)0,&g_376},{(void*)0,&g_376},{(void*)0,(void*)0},{&g_376,&g_376},{&g_376,(void*)0},{(void*)0,&g_376},{(void*)0,&g_376}};
        uint16_t l_3545[7] = {0x323DL,0UL,0UL,0x323DL,0UL,0UL,0x323DL};
        const uint16_t *****l_3557 = (void*)0;
        int8_t l_3585 = 0x23L;
        int i, j;
    }
    (*l_3537) = (((safe_sub_func_int32_t_s_s(((*g_376) &= (safe_add_func_uint8_t_u_u((**g_2179), ((0xC2L & (safe_sub_func_int64_t_s_s(((*g_1877) ^= (*l_3537)), (safe_rshift_func_int16_t_s_u((*l_3537), 3))))) >= (((*g_582) = (-1L)) , (+(safe_div_func_int8_t_s_s((((*l_3537) == (safe_mod_func_uint16_t_u_u((&l_3590 != ((((((((*g_270) |= ((void*)0 != l_3611[4])) ^ (*p_3)) != 0xCC80B90EL) == (*l_3537)) , (*l_3537)) > (*l_3537)) , l_3612[0])), (*l_3537)))) == (*l_3537)), 0xE4L)))))))), (*l_3537))) & (*l_3537)) | (****g_444));
    return (*g_380);
}


/* ------------------------------------------ */
/* 
 * reads : g_571 g_710 g_711 g_712 g_156 g_2094 g_1015 g_834 g_579 g_40 g_2102 g_588 g_376 g_2103 g_582 g_162 g_2047 g_445 g_168 g_169 g_80 g_1877 g_1396 g_4 g_1876 g_66 g_2159 g_375 g_2396 g_270 g_104 g_570 g_731 g_732 g_2095 g_444 g_380 g_2521 g_75 g_2461 g_2462 g_2527 g_2537 g_2561 g_1686 g_2632 g_2652 g_2653 g_2073 g_1696 g_3111 g_3115 g_3114 g_735 g_307 g_308 g_1601 g_2643 g_3182 g_2009 g_3112 g_3113 g_3231 g_2715 g_3237 g_3218 g_2003 g_2644 g_1447 g_2732 g_2733 g_2179 g_462 g_578 g_3309 g_170 g_3313 g_3314 g_3310 g_3311 g_3312 g_3490 g_185 g_113
 * writes: g_571 g_462 g_40 g_4 g_185 g_199 g_2009 g_66 g_80 g_2159 g_1876 g_156 g_570 g_1396 g_104 g_834 g_2197 g_2461 g_75 g_2462 g_162 g_113 g_2539 g_3111 g_3147 g_2003 g_3183 g_3217 g_990 g_735 g_2632 g_1035 g_3308 g_2179 g_1601
 */
static int16_t  func_10(int32_t * p_11, int32_t * p_12)
{ /* block id: 991 */
    int8_t l_2084 = 0x13L;
    int8_t *l_2098 = &l_2084;
    int8_t **l_2097 = &l_2098;
    int8_t ***l_2096 = &l_2097;
    int32_t l_2101 = 0xBA6386C5L;
    uint8_t **l_2178 = &g_570;
    uint32_t l_2188[5];
    int16_t l_2223 = 4L;
    int32_t *l_2239 = &g_80[3][5][4];
    int32_t l_2240[5][5][7] = {{{1L,0xFEFA8C4DL,0x821FDCAAL,0x97E171BAL,0x1975C8F0L,0x1975C8F0L,0x97E171BAL},{0xDAB18E11L,1L,0xDAB18E11L,0xB58F18CAL,0xE153341CL,8L,0xF6EF7D18L},{0xFEFA8C4DL,(-7L),0xA5961B9AL,0xCFE8E9ACL,1L,0x09547896L,9L},{1L,6L,(-1L),6L,1L,8L,0L},{(-4L),(-1L),(-7L),7L,0xCFE8E9ACL,0x1975C8F0L,0x09547896L}},{{0xF6EF7D18L,0L,2L,8L,2L,0L,0xF6EF7D18L},{(-4L),7L,9L,1L,(-1L),0x821FDCAAL,(-7L)},{1L,0xB58F18CAL,0x00E36C3AL,0x28FF5528L,0x6F057967L,0x28FF5528L,0x00E36C3AL},{0xFEFA8C4DL,(-1L),9L,0x97E171BAL,1L,1L,7L},{0xDAB18E11L,1L,2L,0xE731193FL,0xE153341CL,6L,0xE153341CL}},{{1L,(-7L),(-7L),1L,1L,0x09547896L,0x821FDCAAL},{0x6F057967L,8L,(-1L),1L,0x6F057967L,1L,0L},{0x1975C8F0L,0xFEFA8C4DL,0xA5961B9AL,7L,(-1L),1L,0x821FDCAAL},{0xF6EF7D18L,0xE731193FL,0xDAB18E11L,1L,2L,0xE731193FL,0xE153341CL},{1L,7L,0x821FDCAAL,0xCFE8E9ACL,0xCFE8E9ACL,0x821FDCAAL,7L}},{{0x6F057967L,0xE731193FL,0x00E36C3AL,0L,1L,0xB58F18CAL,0x00E36C3AL},{0xCFE8E9ACL,0xFEFA8C4DL,3L,0x97E171BAL,1L,0x1975C8F0L,(-7L)},{0xDAB18E11L,8L,0xDAB18E11L,0L,0xE153341CL,1L,0xF6EF7D18L},{(-1L),(-7L),7L,0xCFE8E9ACL,0x1975C8F0L,0x09547896L,0x09547896L},{1L,1L,(-1L),1L,1L,1L,0L}},{{1L,(-1L),0x97E171BAL,7L,1L,0x1975C8F0L,9L},{0xF6EF7D18L,0xB58F18CAL,2L,1L,2L,0xB58F18CAL,0xF6EF7D18L},{1L,7L,0x09547896L,1L,0xFEFA8C4DL,0x821FDCAAL,0x97E171BAL},{1L,0L,0x00E36C3AL,0xE731193FL,0x6F057967L,0xE731193FL,0x00E36C3AL},{(-1L),(-1L),0x09547896L,0x97E171BAL,(-4L),1L,0xA5961B9AL}}};
    int32_t l_2250[5][1][1] = {{{1L}},{{0xBD5F4407L}},{{1L}},{{0xBD5F4407L}},{{1L}}};
    uint16_t *****l_2310 = &g_444;
    const int64_t *l_2311 = &g_197;
    int32_t *l_2314[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint64_t l_2315 = 0x51268E6D00B59880LL;
    uint16_t l_2379 = 65535UL;
    int32_t * const *l_2383 = &l_2314[1];
    int32_t * const **l_2382 = &l_2383;
    int32_t * const ***l_2381 = &l_2382;
    int64_t **l_2389 = &g_1877;
    uint8_t l_2415[4];
    int32_t *l_2459 = &l_2240[2][0][3];
    int32_t **l_2458 = &l_2459;
    int32_t ***l_2457 = &l_2458;
    int64_t l_2572 = 0x30CD328D9628E17ELL;
    int32_t l_2577[4][1][2];
    uint32_t l_2597[4][4] = {{1UL,1UL,5UL,4294967288UL},{0xBEFD0C13L,0x7B713EA9L,0xBEFD0C13L,5UL},{0xBEFD0C13L,5UL,5UL,0xBEFD0C13L},{1UL,0xBEFD0C13L,0x7B713EA9L,0xBEFD0C13L}};
    int32_t l_2601 = 8L;
    uint32_t l_2624 = 0xC5F31F38L;
    int64_t l_2631 = 0x7F54ED384396E8CDLL;
    int8_t l_2633 = (-1L);
    float * const *l_2645[7][7] = {{(void*)0,&g_579,(void*)0,(void*)0,&g_579,(void*)0,(void*)0},{&g_1015,&g_1015,&g_1015,&g_1015,&g_1015,&g_1015,&g_1015},{&g_579,(void*)0,(void*)0,&g_579,(void*)0,(void*)0,&g_579},{&g_1015,&g_1015,&g_1015,&g_1015,&g_1015,&g_1015,&g_1015},{&g_579,&g_579,(void*)0,&g_579,&g_579,(void*)0,&g_579},{&g_1015,&g_1015,&g_1015,&g_1015,&g_1015,&g_1015,&g_1015},{(void*)0,&g_579,(void*)0,(void*)0,&g_579,(void*)0,(void*)0}};
    int32_t ***l_2655 = (void*)0;
    uint64_t ***l_2851 = &g_711;
    uint32_t ***l_2881 = (void*)0;
    int16_t l_2975 = (-7L);
    int16_t l_2977[3][1][2] = {{{0x7D29L,0x7D29L}},{{0x7D29L,0x7D29L}},{{0x7D29L,0x7D29L}}};
    int32_t *l_3065 = &l_2250[3][0][0];
    uint64_t l_3176 = 18446744073709551615UL;
    int16_t **l_3180[5][8][2] = {{{&g_582,(void*)0},{(void*)0,(void*)0},{&g_582,(void*)0},{&g_582,&g_582},{&g_582,&g_582},{&g_582,(void*)0},{(void*)0,(void*)0},{&g_582,&g_582}},{{&g_582,&g_582},{&g_582,&g_582},{&g_582,&g_582},{&g_582,&g_582},{&g_582,(void*)0},{(void*)0,(void*)0},{&g_582,&g_582},{&g_582,&g_582}},{{&g_582,(void*)0},{&g_582,(void*)0},{(void*)0,(void*)0},{&g_582,&g_582},{&g_582,&g_582},{(void*)0,(void*)0},{&g_582,&g_582},{&g_582,&g_582}},{{&g_582,&g_582},{&g_582,&g_582},{&g_582,(void*)0},{&g_582,&g_582},{&g_582,&g_582},{&g_582,(void*)0},{&g_582,&g_582},{&g_582,&g_582}},{{&g_582,&g_582},{&g_582,&g_582},{&g_582,(void*)0},{(void*)0,&g_582},{&g_582,&g_582},{&g_582,(void*)0},{(void*)0,(void*)0},{&g_582,(void*)0}}};
    int16_t ***l_3179 = &l_3180[4][3][1];
    int32_t l_3197 = 1L;
    uint8_t ****l_3255 = &g_2539;
    const int64_t **l_3259 = &l_2311;
    const int64_t ***l_3258 = &l_3259;
    const int64_t ****l_3257 = &l_3258;
    const int64_t *****l_3256 = &l_3257;
    int32_t l_3403[1];
    float l_3461 = (-0x1.7p-1);
    float l_3483 = 0xB.70E302p+73;
    uint32_t l_3485[1][7];
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_2188[i] = 6UL;
    for (i = 0; i < 4; i++)
        l_2415[i] = 0x13L;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
                l_2577[i][j][k] = 0x106C3EFAL;
        }
    }
    for (i = 0; i < 1; i++)
        l_3403[i] = 9L;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
            l_3485[i][j] = 0UL;
    }
lbl_2571:
    (*p_11) |= ((++g_571) != (safe_rshift_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_s(l_2084, 7)) , (l_2084 > (safe_rshift_func_int8_t_s_u(0xE9L, 7)))), ((safe_mod_func_int16_t_s_s((safe_unary_minus_func_int64_t_s((safe_div_func_uint64_t_u_u((***g_710), (((*g_579) = ((safe_sub_func_float_f_f(((((void*)0 != g_2094) < ((l_2096 == (((safe_add_func_int32_t_s_s(2L, 0x26CDC94DL)) , 0xA.FD18C7p-88) , &g_731)) <= l_2084)) != l_2084), l_2084)) < (*g_1015))) , (*g_712)))))), 0xCB8FL)) && l_2084))));
    if ((l_2101 = ((*p_11) = l_2084)))
    { /* block id: 997 */
        int16_t l_2112 = 8L;
        int16_t **l_2124 = &g_582;
        int32_t l_2140 = (-3L);
        int64_t l_2158 = (-1L);
        uint8_t **l_2177 = &g_570;
        uint32_t l_2193 = 0x8AF16DE6L;
        uint64_t l_2200 = 1UL;
        int32_t l_2241[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int8_t l_2243 = 4L;
        uint32_t l_2247 = 0x1AC30298L;
        uint16_t *****l_2309[1];
        int32_t *l_2356[9] = {&l_2240[1][0][3],&l_2240[1][0][3],&l_2240[1][0][3],&l_2240[1][0][3],&l_2240[1][0][3],&l_2240[1][0][3],&l_2240[1][0][3],&l_2240[1][0][3],&l_2240[1][0][3]};
        const float *l_2361 = &g_834[3][1];
        const float **l_2360[1][1];
        const float ***l_2359 = &l_2360[0][0];
        uint64_t l_2388 = 0x310122C96B1DF589LL;
        uint16_t l_2438[3][10] = {{5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL},{5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL},{5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL,5UL}};
        uint16_t l_2502 = 0xE379L;
        uint8_t ***l_2541 = (void*)0;
        int32_t *l_2568 = (void*)0;
        int32_t l_2602 = 0x65F7D57FL;
        int32_t ****l_2682 = (void*)0;
        int64_t l_2696 = 0x9E70B9254E20B2C1LL;
        float **l_2730 = &g_579;
        float ***l_2729 = &l_2730;
        int8_t l_2784 = (-9L);
        uint32_t l_2792 = 0x04DE14B5L;
        int64_t l_2805[7][3] = {{3L,0xFA16A3CD7FE87C7ALL,0xFA16A3CD7FE87C7ALL},{0x5616C4764068D047LL,0L,0x2ED080BBCE3C472CLL},{0xFA16A3CD7FE87C7ALL,0xF07F60A55C4CBB6FLL,0xF07F60A55C4CBB6FLL},{0L,0x2ED080BBCE3C472CLL,0x2ED080BBCE3C472CLL},{0xFA16A3CD7FE87C7ALL,0xF07F60A55C4CBB6FLL,0xF07F60A55C4CBB6FLL},{0L,0x2ED080BBCE3C472CLL,0x2ED080BBCE3C472CLL},{0xFA16A3CD7FE87C7ALL,0xF07F60A55C4CBB6FLL,0xF07F60A55C4CBB6FLL}};
        int8_t l_2823 = 0xA2L;
        int i, j;
        for (i = 0; i < 1; i++)
            l_2309[i] = &g_444;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_2360[i][j] = &l_2361;
        }
        (*g_2102) = p_11;
        (*g_2103) = (*g_588);
        for (g_185 = (-24); (g_185 == (-29)); g_185 = safe_sub_func_int16_t_s_s(g_185, 5))
        { /* block id: 1002 */
            int32_t l_2111[4] = {0x2CA13D52L,0x2CA13D52L,0x2CA13D52L,0x2CA13D52L};
            uint8_t l_2113 = 0x72L;
            const int16_t **l_2136 = (void*)0;
            uint16_t *****l_2153 = (void*)0;
            int64_t ***l_2162 = &g_1876;
            int32_t **l_2192[3][3];
            uint32_t l_2226[8] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
            uint16_t l_2227[1];
            int8_t l_2275 = 1L;
            const float l_2289 = (-0x1.0p+1);
            int32_t l_2302 = (-1L);
            uint64_t l_2312 = 1UL;
            int i, j;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                    l_2192[i][j] = &g_376;
            }
            for (i = 0; i < 1; i++)
                l_2227[i] = 0UL;
            for (l_2101 = 4; (l_2101 >= 1); l_2101 -= 1)
            { /* block id: 1005 */
                return (*g_582);
            }
            for (g_199 = 0; (g_199 <= 2); g_199 += 1)
            { /* block id: 1010 */
                int32_t *l_2106 = &g_80[3][5][4];
                int32_t *l_2107 = &g_80[2][1][1];
                int32_t *l_2108 = &g_20;
                int32_t *l_2109 = &l_2101;
                int32_t *l_2110[4][2];
                int8_t l_2116 = (-1L);
                uint8_t **l_2117 = (void*)0;
                uint16_t ***l_2156 = &g_168;
                uint16_t **** const l_2155 = &l_2156;
                uint16_t **** const *l_2154 = &l_2155;
                int i, j;
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_2110[i][j] = &g_80[3][5][4];
                }
                l_2113++;
                if (l_2116)
                { /* block id: 1012 */
                    uint8_t ***l_2118 = &l_2117;
                    int16_t **l_2123[10] = {(void*)0,&g_582,&g_582,&g_582,&g_582,(void*)0,&g_582,&g_582,&g_582,&g_582};
                    int32_t l_2125[4][5] = {{1L,2L,2L,1L,1L},{2L,0x42F4CC5BL,2L,0x42F4CC5BL,2L},{1L,1L,2L,2L,1L},{0x477A90B1L,0x42F4CC5BL,0x477A90B1L,0x42F4CC5BL,0x477A90B1L}};
                    const int16_t ***l_2137 = &l_2136;
                    int i, j;
                    (*l_2118) = l_2117;
                    (*g_376) |= (((l_2140 ^= (safe_div_func_uint32_t_u_u(((((((*g_2047) = (l_2123[0] != l_2124)) , l_2125[0][2]) | (l_2125[0][2] == l_2111[0])) ^ (safe_add_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((((safe_add_func_uint64_t_u_u(((((*l_2098) = (((safe_div_func_uint32_t_u_u((safe_sub_func_uint16_t_u_u(((***g_445) = ((((*l_2137) = l_2136) != l_2124) , (((safe_mul_func_int16_t_s_s((l_2111[1] , (*g_582)), 3L)) && l_2112) < l_2112))), l_2101)), l_2125[2][4])) , (-10L)) > (*l_2106))) != 0xA0L) , l_2084), (*g_1877))) >= 1UL) || (*l_2106)), l_2125[3][1])), 0L))) , 0xB1202B78L), 0xC7666134L))) ^ l_2111[3]) , l_2101);
                    (**g_2103) = ((*l_2106) = ((-10L) <= l_2125[0][2]));
                }
                else
                { /* block id: 1022 */
                    int32_t l_2157 = (-1L);
                    int64_t ***l_2161 = &g_1876;
                    int64_t ****l_2160[7][10] = {{&l_2161,&l_2161,&l_2161,(void*)0,&l_2161,(void*)0,&l_2161,(void*)0,&l_2161,&l_2161},{(void*)0,(void*)0,&l_2161,&l_2161,&l_2161,&l_2161,(void*)0,&l_2161,&l_2161,&l_2161},{(void*)0,&l_2161,(void*)0,&l_2161,&l_2161,&l_2161,&l_2161,(void*)0,(void*)0,&l_2161},{(void*)0,&l_2161,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2161,&l_2161,&l_2161,(void*)0,(void*)0,&l_2161,&l_2161,&l_2161,&l_2161,(void*)0},{&l_2161,(void*)0,&l_2161,&l_2161,&l_2161,(void*)0,&l_2161,&l_2161,&l_2161,&l_2161},{&l_2161,&l_2161,&l_2161,&l_2161,(void*)0,&l_2161,(void*)0,&l_2161,(void*)0,&l_2161}};
                    int32_t l_2163 = 0L;
                    int i, j;
                    (*l_2107) = (l_2163 &= (safe_rshift_func_uint8_t_u_s((((safe_div_func_uint16_t_u_u(((-1L) > (safe_rshift_func_int16_t_s_s(((l_2101 == ((l_2162 = (g_2159 = (((safe_mod_func_int16_t_s_s(((safe_add_func_int32_t_s_s(((**g_1876) && ((((((((safe_lshift_func_uint16_t_u_u((0UL ^ 0L), 8)) == (l_2153 != (l_2154 = &g_444))) > (l_2136 != l_2124)) != (-3L)) > l_2157) , 0x0A72L) & (***g_445)) >= 6L)), l_2158)) , (-1L)), l_2101)) < (**g_2102)) , &g_1876))) == (void*)0)) , 0xF45FL), 11))), l_2101)) > 1UL) , l_2084), 0)));
                }
            }
        }
        if (((void*)0 == l_2359))
        { /* block id: 1105 */
            float l_2362 = 0x0.Bp+1;
            int32_t l_2363 = 1L;
            int32_t l_2364 = (-7L);
            int32_t l_2365[1];
            uint32_t l_2366 = 0xFC4BC694L;
            int32_t ****l_2380 = &g_2197;
            int32_t * const ****l_2384 = (void*)0;
            int32_t * const ****l_2385 = &l_2381;
            int8_t l_2433 = 0xC7L;
            int32_t l_2453 = (-1L);
            uint64_t l_2456 = 0UL;
            int32_t ***l_2463 = &g_2462;
            uint32_t **l_2467 = &g_2046;
            uint16_t l_2498 = 0x24DEL;
            int64_t *l_2564 = (void*)0;
            int64_t l_2600 = 0xDBCEA6EE2F7F8A9FLL;
            uint32_t l_2603[3];
            int i;
            for (i = 0; i < 1; i++)
                l_2365[i] = 0xD2ED5C31L;
            for (i = 0; i < 3; i++)
                l_2603[i] = 4294967295UL;
lbl_2390:
            l_2366--;
            if ((l_2241[2] = (0x4CL <= (safe_div_func_int64_t_s_s((safe_add_func_int32_t_s_s(((safe_mod_func_int32_t_s_s((safe_rshift_func_int8_t_s_u((((l_2379 || (l_2364 > ((l_2380 != ((*l_2385) = l_2381)) , (safe_add_func_uint16_t_u_u(((l_2388 |= g_66) & 0xE2L), l_2247))))) < (((*g_2159) = l_2389) != &l_2311)) | (*l_2239)), l_2140)), 0xFCBEC5CFL)) , (**g_588)), (*g_376))), 0x3BB0A06C9DDDC7D8LL)))))
            { /* block id: 1111 */
                uint32_t l_2399 = 0UL;
                uint8_t *l_2411 = &g_571;
                float ***l_2437 = (void*)0;
                int32_t ***l_2442 = (void*)0;
                for (l_2084 = 0; (l_2084 <= 0); l_2084 += 1)
                { /* block id: 1114 */
                    int16_t l_2414 = (-3L);
                    for (l_2243 = 0; (l_2243 <= 0); l_2243 += 1)
                    { /* block id: 1117 */
                        uint16_t l_2391 = 0x489AL;
                        if (g_40)
                            goto lbl_2390;
                        ++l_2391;
                    }
                    if ((**g_375))
                        continue;
                    for (l_2379 = 0; (l_2379 <= 0); l_2379 += 1)
                    { /* block id: 1124 */
                        float l_2405 = 0xF.A8B02Ap+18;
                        int8_t *l_2406 = &l_2243;
                        int32_t l_2416[6] = {0x7EAC62A0L,0xFE854735L,0x7EAC62A0L,0x7EAC62A0L,0xFE854735L,0x7EAC62A0L};
                        int32_t l_2417[2];
                        uint32_t l_2434 = 7UL;
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_2417[i] = 3L;
                        l_2417[0] ^= (safe_rshift_func_int16_t_s_u(g_2396, (2UL >= ((*g_270) |= (((((*g_712)--) == l_2399) & ((safe_mod_func_int16_t_s_s((((**l_2389) |= ((((safe_rshift_func_int8_t_s_u((+((*l_2406) = (*l_2239))), 2)) < (safe_div_func_int16_t_s_s((safe_mul_func_uint64_t_u_u(l_2247, (((*l_2177) = l_2411) == l_2406))), (safe_mul_func_int8_t_s_s(l_2414, l_2415[0]))))) ^ l_2416[4]) == 4294967286UL)) != 0xFB43F38FB1F77DC2LL), 0x04AAL)) < 0xD9DC3E89L)) >= l_2414)))));
                        (*p_11) = (((((**g_1876) = 0L) != ((safe_div_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(l_2247, (safe_mul_func_int8_t_s_s(((((*g_570) &= (safe_mul_func_uint16_t_u_u(0x0A1AL, 5L))) > (safe_div_func_int16_t_s_s((+((((((safe_mul_func_float_f_f(((-0x1.5p+1) >= 0x7.FAEE10p-28), (l_2417[0] , ((*g_579) = (((*l_2239) != ((*g_1015) = (safe_sub_func_float_f_f(l_2200, l_2399)))) != l_2399))))) != l_2193) <= l_2399) >= l_2414) , 8UL) <= l_2416[4])), (*g_582)))) <= 0x132CL), l_2414)))), 9UL)) , l_2399)) && l_2433) , l_2434);
                    }
                }
                (*p_11) = (((((l_2241[6] || ((l_2399 | ((((***l_2096) = (*l_2239)) ^ l_2399) , ((l_2437 != &l_2360[0][0]) <= l_2438[2][1]))) < (~(safe_rshift_func_int8_t_s_u((((*l_2380) = l_2442) != (**l_2385)), 6))))) >= 0x6DF4L) , (*g_712)) ^ (***g_710)) , (*p_11));
                if (((*g_376) = (((*l_2381) != ((*l_2380) = ((((safe_lshift_func_uint8_t_u_s(((((safe_sub_func_float_f_f(((safe_sub_func_int64_t_s_s(((safe_rshift_func_uint8_t_u_s((((l_2243 > 65535UL) || (1L == ((*g_1876) == (void*)0))) != ((safe_rshift_func_int16_t_s_s(((((l_2453 , (safe_rshift_func_int16_t_s_u(((void*)0 == (*g_731)), 3))) != l_2399) ^ 5UL) | 1UL), (*g_582))) , (**g_711))), 4)) & 4L), 1UL)) , 0xE.28A60Bp+48), (-0x1.7p-1))) <= 0x0.6p+1) , (*g_270)) != (-2L)), l_2200)) , 0L) > (*g_712)) , l_2442))) <= l_2456)))
                { /* block id: 1143 */
                    int32_t ****l_2460[3][6] = {{&l_2457,&l_2457,&l_2457,&l_2457,&l_2457,&l_2457},{&l_2457,&l_2457,&l_2457,&l_2457,&l_2457,&l_2457},{&l_2457,&l_2457,&l_2457,&l_2457,&l_2457,&l_2457}};
                    int i, j;
                    l_2463 = (g_2461 = l_2457);
                }
                else
                { /* block id: 1146 */
                    uint8_t l_2464 = 1UL;
                    uint32_t **l_2466[4][6] = {{&g_2047,&g_2046,&g_2047,&g_2047,&g_2046,&g_2047},{&g_2047,&g_2046,&g_2047,&g_2047,&g_2046,&g_2047},{&g_2047,&g_2046,&g_2047,&g_2047,&g_2046,&g_2047},{&g_2047,&g_2046,&g_2047,&g_2047,&g_2046,&g_2047}};
                    uint32_t ***l_2465[6][4][6] = {{{&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1],&l_2466[3][1],&l_2466[3][1]},{&l_2466[0][1],&l_2466[0][1],&l_2466[3][1],&l_2466[0][0],&l_2466[3][1],&l_2466[0][1]},{&l_2466[3][1],&l_2466[3][5],&l_2466[0][0],&l_2466[3][5],&l_2466[0][1],&l_2466[3][1]},{&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1],&l_2466[3][1]}},{{&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0],&l_2466[0][1],&l_2466[0][0]},{&l_2466[0][0],&l_2466[0][1],&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0]},{&l_2466[3][1],&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1]},{&l_2466[3][1],&l_2466[0][1],&l_2466[3][5],&l_2466[3][5],&l_2466[0][1],&l_2466[3][1]}},{{&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1],&l_2466[3][1]},{&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0],&l_2466[0][1],&l_2466[0][0]},{&l_2466[0][0],&l_2466[0][1],&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0]},{&l_2466[3][1],&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1]}},{{&l_2466[3][1],&l_2466[0][1],&l_2466[3][5],&l_2466[3][5],&l_2466[0][1],&l_2466[3][1]},{&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1],&l_2466[3][1]},{&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0],&l_2466[0][1],&l_2466[0][0]},{&l_2466[0][0],&l_2466[0][1],&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0]}},{{&l_2466[3][1],&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1]},{&l_2466[3][1],&l_2466[0][1],&l_2466[3][5],&l_2466[3][5],&l_2466[0][1],&l_2466[3][1]},{&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1],&l_2466[3][1]},{&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0],&l_2466[0][1],&l_2466[0][0]}},{{&l_2466[0][0],&l_2466[0][1],&l_2466[0][0],&l_2466[3][1],&l_2466[3][1],&l_2466[0][0]},{&l_2466[3][1],&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1]},{&l_2466[3][1],&l_2466[0][1],&l_2466[3][5],&l_2466[3][5],&l_2466[0][1],&l_2466[3][1]},{&l_2466[3][1],&l_2466[3][1],&l_2466[3][5],&l_2466[3][1],&l_2466[3][1],&l_2466[3][1]}}};
                    int32_t l_2468 = 0xCA9BB82EL;
                    int32_t l_2501 = 0x98669F6CL;
                    int i, j, k;
                    (*g_376) = l_2464;
                    l_2468 = ((*g_4) = (l_2464 < ((l_2467 = &g_2047) == (*g_2094))));
                    for (l_2379 = 21; (l_2379 >= 42); ++l_2379)
                    { /* block id: 1153 */
                        uint16_t *** const *l_2473[4][9][7] = {{{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,&g_445},{&g_445,&g_445,&g_445,&g_445,&g_445,&g_445,(void*)0}},{{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0},{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0},{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0}},{{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0},{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0},{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0}},{{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0},{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0},{&g_445,(void*)0,&g_445,(void*)0,&g_445,&g_445,&g_445},{&g_445,(void*)0,&g_445,&g_445,&g_445,&g_445,(void*)0},{&g_445,&g_445,&g_445,&g_445,(void*)0,&g_445,(void*)0}}};
                        int32_t l_2482 = 3L;
                        uint16_t l_2497[10] = {65535UL,0x9B1DL,65535UL,65535UL,0x9B1DL,65535UL,65535UL,0x9B1DL,65535UL,65535UL};
                        int i, j, k;
                        (*l_2239) = ((safe_rshift_func_int16_t_s_u(((void*)0 != l_2473[1][6][4]), 0)) < (safe_mul_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((g_571 &= (safe_add_func_uint32_t_u_u((l_2482 > ((*g_444) != (void*)0)), (safe_lshift_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((safe_mod_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((((-1L) >= ((l_2243 <= 0xF018L) > (safe_lshift_func_int8_t_s_s(((l_2497[3] <= 0UL) != l_2468), 2)))) || l_2438[2][1]), 1L)), 248UL)), l_2438[2][1])), 3)) , (*g_169)), 9)), 5))))), 0x53L)), l_2464)) , l_2468), l_2482)));
                        (*p_11) ^= 0x64F8CF74L;
                        l_2498++;
                    }
                    --l_2502;
                }
                (*l_2239) = (safe_rshift_func_uint16_t_u_u(l_2112, ((((safe_sub_func_int64_t_s_s(((**l_2389) |= l_2158), ((*g_712) = (((safe_mul_func_uint8_t_u_u(l_2438[0][5], (&l_2399 == (void*)0))) || ((l_2158 & 250UL) & (((*g_1015) == (safe_mul_func_float_f_f(((*g_1015) >= (-0x10.Cp-1)), 0xF.CB977Fp-87))) , l_2243))) >= (*g_270))))) == 0x97CFE254EAD715F0LL) < 0x9961B5EEL) > (****g_444))));
            }
            else
            { /* block id: 1164 */
                uint32_t l_2514[7] = {0x05EEF4D7L,0x05EEF4D7L,0xE087919DL,0x05EEF4D7L,0x05EEF4D7L,0xE087919DL,0x05EEF4D7L};
                int32_t *l_2518 = &g_80[1][9][2];
                int32_t ***l_2531 = &l_2458;
                uint32_t l_2562[2][1];
                int64_t *l_2563[3];
                int32_t l_2573 = 0x69573B8FL;
                int32_t l_2574 = 2L;
                int32_t l_2576 = (-1L);
                int32_t l_2583 = 0x651F1FFBL;
                int32_t l_2591 = 0x4543A18DL;
                int32_t l_2593 = (-9L);
                int32_t l_2594[1];
                int i, j;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_2562[i][j] = 4294967286UL;
                }
                for (i = 0; i < 3; i++)
                    l_2563[i] = &g_1396;
                for (i = 0; i < 1; i++)
                    l_2594[i] = 0x114C0396L;
                (*g_376) |= (((~(l_2514[5] ^ 0x5CL)) || (-4L)) == (**g_711));
                (*g_380) = p_12;
                if ((*p_11))
                { /* block id: 1167 */
                    float l_2517 = 0x4.A25B0Ap+18;
                    int32_t l_2536 = 0x460B1B89L;
                    for (l_2112 = 23; (l_2112 < 3); --l_2112)
                    { /* block id: 1170 */
                        (*g_376) = 0x6B87F0C3L;
                        l_2518 = p_11;
                        return l_2158;
                    }
                    for (g_571 = 1; (g_571 <= 4); g_571 += 1)
                    { /* block id: 1177 */
                        (*g_376) = 6L;
                        (*g_376) &= (safe_lshift_func_uint8_t_u_s(0xD2L, g_2521));
                        l_2314[1] = (*g_380);
                    }
                    if ((safe_unary_minus_func_uint32_t_u((((safe_sub_func_uint64_t_u_u((*l_2518), ((((*l_2463) = (*g_2461)) == g_2527) | (safe_sub_func_uint16_t_u_u((*g_169), ((*p_11) == (l_2531 == &l_2458))))))) & ((safe_unary_minus_func_uint16_t_u((safe_unary_minus_func_int8_t_s((*l_2518))))) < ((safe_rshift_func_int8_t_s_u(1L, 3)) != l_2536))) , 4294967293UL))))
                    { /* block id: 1183 */
                        uint8_t ****l_2540[2][9] = {{&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539},{&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539,&g_2539}};
                        int32_t l_2544[1][7][8] = {{{0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L)},{0xACEB19ADL,0xACEB19ADL,(-10L),0xACEB19ADL,0xACEB19ADL,(-10L),0xACEB19ADL,0xACEB19ADL},{(-6L),0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L),(-6L),0xACEB19ADL},{0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L)},{0xACEB19ADL,0xACEB19ADL,(-10L),0xACEB19ADL,0xACEB19ADL,(-10L),0xACEB19ADL,0xACEB19ADL},{(-6L),0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L),(-6L),0xACEB19ADL},{0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L),(-6L),0xACEB19ADL,(-6L)}}};
                        int i, j, k;
                        (*g_2537) = p_12;
                        l_2541 = &g_2179;
                        (*l_2239) = (safe_add_func_uint16_t_u_u(l_2544[0][3][5], (l_2243 & l_2544[0][3][5])));
                        (*l_2518) &= ((((void*)0 == &g_711) != (*g_1877)) , l_2536);
                    }
                    else
                    { /* block id: 1188 */
                        uint32_t l_2560 = 0xB1CC8058L;
                        uint32_t l_2565 = 0x0DBF80F2L;
                        int32_t *l_2566 = (void*)0;
                        int32_t **l_2567[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_2567[i] = &l_2314[1];
                        l_2565 &= (((*l_2239) > (safe_div_func_int64_t_s_s((*l_2518), (**g_1876)))) , (0x0FC1L != ((**l_2124) = (((((((!(0xB3A5L || (safe_mod_func_uint8_t_u_u(((!((safe_div_func_uint32_t_u_u(((((((safe_rshift_func_uint8_t_u_s((((*g_1877) = (safe_unary_minus_func_uint64_t_u(l_2536))) ^ (safe_rshift_func_int8_t_s_u(((l_2536 ^ 0xABL) , 0L), l_2560))), 4)) & l_2388) && 0x6531833A8C14D8FBLL) == g_2561) , l_2562[1][0]) && (*g_582)), l_2536)) , l_2158)) , l_2560), l_2112)))) != (-6L)) >= 0x64113C6AL) , (*p_11)) ^ (*g_1686)) , l_2563[0]) == l_2564))));
                        (*l_2518) ^= (*p_11);
                        l_2568 = l_2566;
                    }
                }
                else
                { /* block id: 1195 */
                    int32_t l_2579 = 0xE99F172AL;
                    int32_t l_2580 = 0x77342F73L;
                    int32_t l_2581 = 0x081F2127L;
                    int32_t l_2582 = 1L;
                    int32_t l_2584 = 1L;
                    int64_t l_2585 = 7L;
                    int32_t l_2587 = 1L;
                    int32_t l_2589[9][10][2] = {{{1L,0x79713DB2L},{0x4B6677BFL,0xD23004FDL},{0x3642A8F3L,0L},{(-1L),0xA83594A9L},{0x091FAB8DL,0x2FB8A222L},{0x091FAB8DL,0xA83594A9L},{(-1L),0L},{0x3642A8F3L,0xD23004FDL},{0x4B6677BFL,0x79713DB2L},{1L,(-5L)}},{{(-4L),(-1L)},{7L,(-1L)},{(-4L),(-5L)},{1L,0x79713DB2L},{0x4B6677BFL,0xD23004FDL},{0x3642A8F3L,0L},{(-1L),0xA83594A9L},{0x091FAB8DL,0x2FB8A222L},{0x091FAB8DL,0xA83594A9L},{(-1L),0L}},{{0x3642A8F3L,0xD23004FDL},{0x4B6677BFL,0x79713DB2L},{1L,(-5L)},{(-4L),(-1L)},{7L,(-1L)},{(-4L),(-5L)},{1L,0x79713DB2L},{0x4B6677BFL,0xD23004FDL},{0x3642A8F3L,0L},{(-1L),0xA83594A9L}},{{0x091FAB8DL,0x2FB8A222L},{0x091FAB8DL,0xA83594A9L},{(-1L),0L},{0x3642A8F3L,0xD23004FDL},{0x4B6677BFL,0x79713DB2L},{1L,(-5L)},{(-4L),(-1L)},{7L,(-1L)},{(-4L),(-5L)},{1L,0x79713DB2L}},{{0x4B6677BFL,0xD23004FDL},{0x3642A8F3L,0L},{(-1L),0xA83594A9L},{0x091FAB8DL,0x2FB8A222L},{0x091FAB8DL,0xA83594A9L},{(-1L),0L},{0x3642A8F3L,0xD23004FDL},{0x4B6677BFL,0x79713DB2L},{1L,(-5L)},{(-4L),(-1L)}},{{(-4L),(-4L)},{0xE392085BL,0x2FB8A222L},{0x1DABD2DEL,0L},{0x091FAB8DL,(-1L)},{1L,0L},{7L,0x10911D47L},{(-3L),0xA83594A9L},{(-3L),0x10911D47L},{7L,0L},{1L,(-1L)}},{{0x091FAB8DL,0L},{0x1DABD2DEL,0x2FB8A222L},{0xE392085BL,(-4L)},{(-4L),(-4L)},{0xE392085BL,0x2FB8A222L},{0x1DABD2DEL,0L},{0x091FAB8DL,(-1L)},{1L,0L},{7L,0x10911D47L},{(-3L),0xA83594A9L}},{{(-3L),0x10911D47L},{7L,0L},{1L,(-1L)},{0x091FAB8DL,0L},{0x1DABD2DEL,0x2FB8A222L},{0xE392085BL,(-4L)},{(-4L),(-4L)},{0xE392085BL,0x2FB8A222L},{0x1DABD2DEL,0L},{0x091FAB8DL,(-1L)}},{{1L,0L},{7L,0x10911D47L},{(-3L),0xA83594A9L},{(-3L),0x10911D47L},{7L,0L},{1L,(-1L)},{0x091FAB8DL,0L},{0x1DABD2DEL,0x2FB8A222L},{0xE392085BL,(-4L)},{(-4L),(-4L)}}};
                    int i, j, k;
                    for (l_2364 = (-22); (l_2364 > 15); ++l_2364)
                    { /* block id: 1198 */
                        int64_t l_2575[3];
                        int32_t l_2578 = 0x1CB64A0EL;
                        int32_t l_2588 = 2L;
                        int32_t l_2590 = 0xAA78B87FL;
                        int32_t l_2592 = 0x18E5D323L;
                        int32_t l_2595[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_2575[i] = (-1L);
                        for (i = 0; i < 3; i++)
                            l_2595[i] = 0x6E6D6BC9L;
                        if (l_2193)
                            goto lbl_2571;
                        l_2597[3][1]++;
                        return (*l_2518);
                    }
                }
            }
            l_2603[2]--;
        }
        else
        { /* block id: 1206 */
            uint32_t l_2610 = 0xFA260312L;
            int32_t *** const *l_2634 = (void*)0;
            int64_t ***l_2635 = (void*)0;
            int32_t l_2651 = 0L;
            uint64_t l_2663[1][3];
            uint8_t l_2664 = 0x13L;
            int32_t l_2690 = (-2L);
            int32_t l_2691 = 0x2ED97C8FL;
            int32_t l_2704[4] = {1L,1L,1L,1L};
            int32_t *l_2713 = &l_2704[1];
            float *** const l_2734 = &g_2733;
            uint16_t *l_2803 = &g_2632;
            int8_t l_2811 = 1L;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_2663[i][j] = 0x68C00933E713AF67LL;
            }
            if ((safe_add_func_uint8_t_u_u((((safe_rshift_func_uint8_t_u_u(l_2610, (+65531UL))) && (safe_sub_func_uint16_t_u_u((((((((((*g_579) = (safe_div_func_float_f_f((safe_add_func_float_f_f((((safe_div_func_int32_t_s_s(((((safe_div_func_uint16_t_u_u(l_2602, l_2624)) || ((((((255UL >= (safe_sub_func_int16_t_s_s(0xECFFL, (safe_mod_func_uint64_t_u_u((***g_710), 0x8E7C36F028CDBD0ALL))))) >= (safe_add_func_uint8_t_u_u((l_2610 ^ 0UL), l_2631))) > 18446744073709551609UL) , g_2632) | l_2610) <= l_2633)) < 0x4A8FL) , l_2610), 0xE95873EDL)) , l_2610) <= l_2610), 0x3.186694p+73)), l_2610))) < 0xB.95AD70p+73) , l_2634) == &l_2382) & (*g_582)) && l_2610) , 2UL) == l_2438[2][1]), 65531UL))) , l_2602), (*l_2239))))
            { /* block id: 1208 */
                int64_t ****l_2636 = &l_2635;
                (*l_2636) = l_2635;
            }
            else
            { /* block id: 1210 */
                for (g_113 = 0; (g_113 == 56); ++g_113)
                { /* block id: 1213 */
                    return (*g_582);
                }
                return (*g_582);
            }
            for (l_2601 = 0; (l_2601 <= 20); l_2601++)
            { /* block id: 1220 */
                int64_t l_2648 = 0L;
                uint32_t l_2670[8];
                int8_t l_2676 = 0x38L;
                int32_t l_2678 = (-9L);
                int32_t l_2693 = 0x3C27231EL;
                int32_t l_2694 = 3L;
                int32_t l_2697 = 0x2E656531L;
                int32_t l_2699 = 1L;
                int32_t l_2700 = 0x8BF4EE51L;
                int32_t l_2705 = 0L;
                int32_t l_2706 = 0x44CDBB85L;
                int32_t l_2708[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
                uint32_t l_2710 = 1UL;
                float ****l_2731[9][2] = {{&l_2729,&l_2729},{&l_2729,&l_2729},{&l_2729,&l_2729},{&l_2729,&l_2729},{&l_2729,&l_2729},{&l_2729,&l_2729},{&l_2729,&l_2729},{&l_2729,&l_2729},{&l_2729,&l_2729}};
                uint32_t l_2757 = 1UL;
                uint16_t *l_2804 = &g_2396;
                uint16_t ***l_2819 = &g_168;
                int i, j;
                for (i = 0; i < 8; i++)
                    l_2670[i] = 0x25D53A53L;
            }
        }
    }
    else
    { /* block id: 1293 */
        uint8_t ***l_2843 = &g_2179;
        int64_t ***l_2846 = &l_2389;
        uint64_t l_2852[3][5][1] = {{{0x16D0729193AC87F6LL},{2UL},{0x16D0729193AC87F6LL},{2UL},{0x16D0729193AC87F6LL}},{{2UL},{0x16D0729193AC87F6LL},{2UL},{0x16D0729193AC87F6LL},{2UL}},{{0x16D0729193AC87F6LL},{2UL},{0x16D0729193AC87F6LL},{2UL},{0x16D0729193AC87F6LL}}};
        float **l_2934 = &g_1015;
        int32_t l_2954 = 0x2E4BB208L;
        uint32_t l_2996 = 0xA883B78FL;
        const int32_t l_3001[6][10][4] = {{{(-6L),0x46529768L,1L,1L},{0L,0x46529768L,0x7F96AE05L,1L},{0x46529768L,0xC6B22283L,4L,0x07714ADFL},{(-1L),0x4E6C5745L,0xDEB5B2DFL,0L},{1L,0L,1L,0x46529768L},{5L,1L,1L,1L},{(-6L),1L,(-1L),1L},{0x46529768L,0x7F6A5B42L,(-1L),0x7F96AE05L},{(-6L),0x4E6C5745L,1L,4L},{5L,(-1L),1L,0xDEB5B2DFL}},{{1L,0xDEB5B2DFL,0xDEB5B2DFL,1L},{(-1L),1L,4L,1L},{0x46529768L,1L,0x7F96AE05L,(-1L)},{0L,0x4E6C5745L,1L,(-1L)},{(-6L),1L,1L,1L},{1L,1L,0x46529768L,1L},{0x4E6C5745L,0xDEB5B2DFL,0L,0xDEB5B2DFL},{0x46529768L,(-1L),1L,0x7F6A5B42L},{0xDEB5B2DFL,0x46529768L,5L,0xC6B22283L},{0x07714ADFL,0L,(-1L),1L}},{{0x07714ADFL,(-6L),5L,(-1L)},{0xDEB5B2DFL,1L,1L,9L},{9L,0x4E6C5745L,0L,0L},{0x46529768L,0x46529768L,9L,1L},{0x7F96AE05L,0x9A0AF9CCL,(-1L),5L},{0L,9L,1L,(-1L)},{1L,9L,0xC6B22283L,5L},{9L,0x9A0AF9CCL,0x7F6A5B42L,1L},{1L,0x46529768L,1L,0L},{(-1L),0x4E6C5745L,(-1L),9L}},{{4L,1L,(-6L),(-1L)},{1L,(-6L),(-1L),1L},{9L,0L,(-1L),0xC6B22283L},{1L,0x46529768L,(-6L),0x7F6A5B42L},{4L,(-1L),(-1L),1L},{(-1L),1L,1L,(-1L)},{1L,5L,0x7F6A5B42L,(-6L)},{9L,(-6L),0xC6B22283L,(-1L)},{1L,0x46529768L,1L,(-1L)},{0L,(-6L),(-1L),(-6L)}},{{0x7F96AE05L,5L,9L,(-1L)},{0x46529768L,1L,0L,1L},{9L,(-1L),1L,0x7F6A5B42L},{0xDEB5B2DFL,0x46529768L,5L,0xC6B22283L},{0x07714ADFL,0L,(-1L),1L},{0x07714ADFL,(-6L),5L,(-1L)},{0xDEB5B2DFL,1L,1L,9L},{9L,0x4E6C5745L,0L,0L},{0x46529768L,0x46529768L,9L,1L},{0x7F96AE05L,0x9A0AF9CCL,(-1L),5L}},{{0L,9L,1L,(-1L)},{1L,9L,0xC6B22283L,5L},{9L,0x9A0AF9CCL,0x7F6A5B42L,1L},{1L,0x46529768L,1L,0L},{(-1L),0x4E6C5745L,(-1L),9L},{4L,1L,(-6L),(-1L)},{1L,(-6L),(-1L),1L},{9L,0L,(-1L),0xC6B22283L},{1L,0x46529768L,(-6L),0x7F6A5B42L},{4L,(-1L),(-1L),1L}}};
        int32_t l_3005 = 0L;
        int32_t l_3006 = 8L;
        int64_t l_3105 = 0L;
        uint8_t ***l_3110 = &g_2179;
        uint32_t **l_3146 = &g_2047;
        uint32_t ***l_3145 = &l_3146;
        int32_t l_3173 = (-1L);
        int32_t l_3174 = 1L;
        int32_t l_3175[3];
        int32_t **** const *l_3191 = (void*)0;
        int8_t ***l_3215[9] = {&l_2097,&l_2097,&l_2097,&l_2097,&l_2097,&l_2097,&l_2097,&l_2097,&l_2097};
        int32_t * const *l_3238 = &l_2459;
        int16_t ***l_3260[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int16_t l_3295[2];
        int8_t ****l_3307 = &l_3215[2];
        int8_t *****l_3306 = &l_3307;
        uint16_t **l_3368[2];
        int32_t l_3406 = 0x7AE9D251L;
        int32_t l_3407[7][5][4] = {{{0xC849AEC0L,3L,0L,8L},{0x94D3293DL,0x05F205BFL,0xA13AEE3AL,0xB085442AL},{0x863C38D1L,(-7L),0x5B2F7228L,3L},{(-1L),0L,0L,0L},{1L,(-2L),0x182A7BD4L,0L}},{{8L,0L,0xAA116AFEL,0x5E3E28D3L},{(-7L),0xA0C4A4B9L,0L,0xFA993D5BL},{0L,1L,1L,0L},{0x863C38D1L,(-1L),(-2L),(-1L)},{0xA0C4A4B9L,(-7L),0L,0L}},{{8L,0xC73BC3C9L,(-2L),0L},{0xA13AEE3AL,(-7L),0x182A7BD4L,(-1L)},{0L,(-1L),1L,0L},{1L,1L,3L,0xFA993D5BL},{0x6CA1E405L,0xA0C4A4B9L,0x863C38D1L,0x5E3E28D3L}},{{0x94D3293DL,0L,0L,0L},{0xA0C4A4B9L,(-2L),1L,0L},{0x6CA1E405L,0L,0x73A82F7EL,3L},{0x4C54F6DFL,(-7L),1L,0xB085442AL},{0xB085442AL,0x05F205BFL,0xAA116AFEL,8L}},{{0xA13AEE3AL,0x94D3293DL,3L,0x182A7BD4L},{(-1L),1L,0L,0L},{1L,0x05F205BFL,(-5L),0L},{0x863C38D1L,0x4C54F6DFL,0x863C38D1L,3L},{8L,1L,0L,0x411E16B4L}},{{0x4C54F6DFL,(-2L),0xE7CE6CB9L,1L},{8L,(-1L),0xE7CE6CB9L,0x5E3E28D3L},{0x4C54F6DFL,0x94D3293DL,0L,0x136D309EL},{8L,1L,0x863C38D1L,8L},{0x863C38D1L,8L,(-5L),(-1L)}},{{1L,1L,0L,0L},{(-1L),0xC73BC3C9L,3L,0x411E16B4L},{0xA13AEE3AL,0x4C54F6DFL,0xAA116AFEL,(-1L)},{0xB085442AL,0L,1L,(-1L)},{0x4C54F6DFL,1L,0x73A82F7EL,(-1L)}}};
        uint16_t l_3455 = 0x0858L;
        uint32_t l_3462[9] = {0x18E89913L,0UL,0x18E89913L,0UL,0x18E89913L,0UL,0x18E89913L,0UL,0x18E89913L};
        int64_t l_3482 = 2L;
        uint16_t l_3509 = 65534UL;
        const int8_t *l_3515 = &l_2633;
        int32_t *l_3535[9] = {&l_3006,&l_3006,&l_3006,&l_3006,&l_3006,&l_3006,&l_3006,&l_3006,&l_3006};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_3175[i] = 0xD99F8655L;
        for (i = 0; i < 2; i++)
            l_3295[i] = 0x4D15L;
        for (i = 0; i < 2; i++)
            l_3368[i] = &g_169;
lbl_3340:
        if ((7L | (safe_sub_func_uint8_t_u_u((((*g_169) |= (safe_div_func_uint16_t_u_u(((((((*g_712) = (~((*p_11) | ((void*)0 != l_2843)))) ^ (((safe_add_func_uint16_t_u_u((((*g_2652) != l_2846) >= ((safe_mod_func_uint64_t_u_u((safe_add_func_int32_t_s_s((*g_2073), 0x08D152FBL)), ((&g_711 != l_2851) && (*l_2239)))) && (*g_2073))), (*g_582))) >= l_2852[1][2][0]) ^ l_2852[1][2][0])) , 5UL) && 65528UL) < l_2852[0][3][0]), (*l_2239)))) == (*g_582)), l_2852[0][3][0]))))
        { /* block id: 1296 */
            int8_t l_2877 = 0L;
            uint32_t l_2891 = 18446744073709551606UL;
            uint32_t **l_2941 = &g_2047;
            uint32_t l_3003 = 4294967295UL;
            int32_t l_3004[4][3] = {{(-1L),(-1L),(-1L)},{0x3054A767L,0x3054A767L,0x3054A767L},{(-1L),(-1L),(-1L)},{0x3054A767L,0x3054A767L,0x3054A767L}};
            int32_t **l_3022 = (void*)0;
            uint8_t ** const *l_3057[8] = {&g_2179,&g_2179,&g_2179,&g_2179,&g_2179,&g_2179,&g_2179,&g_2179};
            uint8_t ** const **l_3056 = &l_3057[2];
            uint64_t l_3099 = 4UL;
            const int32_t **l_3101 = &g_2528;
            const int32_t ***l_3100[8];
            int i, j;
            for (i = 0; i < 8; i++)
                l_3100[i] = &l_3101;
            if ((*p_11))
            { /* block id: 1297 */
                uint16_t l_2855 = 0xE766L;
                int32_t l_2880 = 0x339A4734L;
                int16_t *l_2890 = &l_2223;
                float l_3035 = (-0x5.5p-1);
                uint32_t l_3102[2][8] = {{4294967291UL,4294967289UL,0x317B2D8AL,0xF915662DL,0xF915662DL,0x317B2D8AL,4294967289UL,4294967291UL},{4294967289UL,0x621FD25EL,4294967291UL,6UL,4294967291UL,0x621FD25EL,6UL,6UL}};
                int i, j;
            }
            else
            { /* block id: 1414 */
                uint8_t ****l_3106 = &l_2843;
                uint8_t ****l_3107 = &g_2539;
                uint8_t ***l_3109 = (void*)0;
                uint8_t ****l_3108[3][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}};
                int32_t l_3120 = 0xF4AC97E2L;
                int32_t *l_3121[3][4][8] = {{{(void*)0,(void*)0,&l_3005,(void*)0,(void*)0,&l_3004[2][1],&l_2601,&l_3006},{(void*)0,&l_3004[2][1],&l_2601,&l_3006,&l_2601,&l_3004[2][1],(void*)0,(void*)0},{&g_80[3][3][4],(void*)0,&l_2601,(void*)0,&l_3120,(void*)0,&l_2601,(void*)0},{&l_2601,&g_20,&l_3005,&l_3006,&l_3120,(void*)0,&l_3120,&l_3006}},{{&g_80[3][3][4],&g_20,&g_80[3][3][4],(void*)0,&l_2601,(void*)0,&l_3120,(void*)0},{(void*)0,(void*)0,&l_3005,(void*)0,(void*)0,&l_3004[2][1],&l_2601,&l_3006},{(void*)0,&l_3004[2][1],&l_2601,&l_3006,&l_2601,&l_3004[2][1],(void*)0,(void*)0},{&g_80[3][3][4],(void*)0,&l_2601,(void*)0,&l_3120,(void*)0,&l_2601,(void*)0}},{{&l_2601,&g_20,&l_3005,&l_3006,&l_3120,(void*)0,&l_3120,&l_3006},{&g_80[3][3][4],&g_20,&g_80[3][3][4],(void*)0,&l_2601,(void*)0,&l_3120,(void*)0},{(void*)0,(void*)0,&l_3005,(void*)0,(void*)0,&l_3004[2][1],&l_2601,&l_3006},{(void*)0,&l_3004[2][1],&l_2601,&l_3006,&l_2601,&l_3004[2][1],(void*)0,(void*)0}}};
                int i, j, k;
                (*g_376) |= ((l_3110 = ((*l_3107) = ((*l_3106) = &l_2178))) != (void*)0);
                (*g_3115) = g_3111;
                l_3121[2][0][5] = p_12;
                (*p_11) |= 0x522B2B90L;
            }
        }
        else
        { /* block id: 1423 */
            int64_t l_3161 = 0xCFB6594228731359LL;
            const uint32_t *l_3166 = &g_126;
            const uint32_t **l_3165 = &l_3166;
            const uint32_t ***l_3164[3][6][8] = {{{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165},{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165},{&l_3165,&l_3165,(void*)0,&l_3165,&l_3165,(void*)0,&l_3165,&l_3165},{&l_3165,&l_3165,&l_3165,&l_3165,(void*)0,&l_3165,&l_3165,&l_3165},{(void*)0,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,(void*)0,&l_3165},{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,(void*)0,&l_3165,&l_3165}},{{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165},{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165},{(void*)0,&l_3165,(void*)0,&l_3165,(void*)0,&l_3165,&l_3165,&l_3165},{&l_3165,(void*)0,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165},{&l_3165,(void*)0,(void*)0,&l_3165,&l_3165,(void*)0,&l_3165,(void*)0},{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165}},{{(void*)0,&l_3165,(void*)0,(void*)0,&l_3165,(void*)0,&l_3165,&l_3165},{&l_3165,(void*)0,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165},{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165},{(void*)0,&l_3165,(void*)0,(void*)0,&l_3165,(void*)0,(void*)0,&l_3165},{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,(void*)0,&l_3165,&l_3165},{&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165,&l_3165}}};
            int32_t l_3167 = 1L;
            int32_t l_3168 = 0xA82D94C0L;
            int32_t l_3169 = 0L;
            int32_t l_3171[10] = {0x1F143CDCL,(-1L),0x1F143CDCL,0x1F143CDCL,(-1L),0x1F143CDCL,0x1F143CDCL,(-1L),0x1F143CDCL,0x1F143CDCL};
            float **l_3185[4][5] = {{&g_579,&g_1015,&g_579,&g_579,(void*)0},{&g_1015,&g_579,(void*)0,&g_579,&g_1015},{&g_579,&g_579,&g_1015,&g_1015,&g_1015},{&g_1015,&g_1015,(void*)0,&g_1015,&g_579}};
            int64_t ***l_3220 = &l_2389;
            int32_t **l_3236[6] = {&g_4,&g_4,&g_4,&g_4,&g_4,&g_4};
            int32_t * const *l_3239[3][9] = {{&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459},{&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459},{&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459,&l_2459}};
            uint16_t *****l_3244 = &g_444;
            int i, j, k;
            for (g_185 = 6; (g_185 <= (-16)); g_185--)
            { /* block id: 1426 */
                uint64_t l_3160 = 0xB1E89E480DC536A6LL;
                int64_t *l_3162[4];
                int32_t l_3163 = (-1L);
                int32_t l_3170 = 6L;
                int32_t l_3172[8][6] = {{1L,1L,0x1EDF98D9L,0xA61BFE0EL,(-2L),(-2L)},{1L,0xA61BFE0EL,0xA61BFE0EL,1L,(-2L),0x1EDF98D9L},{(-2L),1L,0xA61BFE0EL,0xA61BFE0EL,1L,(-2L)},{(-2L),0xA61BFE0EL,0x1EDF98D9L,1L,1L,0x1EDF98D9L},{1L,1L,0x1EDF98D9L,0xA61BFE0EL,(-2L),(-2L)},{1L,0xA61BFE0EL,0xA61BFE0EL,1L,(-2L),0x1EDF98D9L},{(-2L),1L,0xA61BFE0EL,0xA61BFE0EL,1L,(-2L)},{(-2L),0xA61BFE0EL,0x1EDF98D9L,1L,1L,0x1EDF98D9L}};
                float l_3211 = 0x8.2p-1;
                const uint32_t l_3213 = 0x159DA176L;
                int i, j;
                for (i = 0; i < 4; i++)
                    l_3162[i] = &g_197;
                (*l_2239) = (l_3167 = (safe_rshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(((**g_711)--), (safe_rshift_func_int8_t_s_u(((*g_2643) = ((safe_mod_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u(246UL, l_3105)), (safe_add_func_uint16_t_u_u(((*l_2239) > ((((!((((*l_2098) = (safe_sub_func_int16_t_s_s(((g_3147 = l_3145) == ((+((safe_mul_func_uint16_t_u_u((((safe_mod_func_int8_t_s_s((*g_3114), ((l_3163 &= ((**l_2389) = (safe_div_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(((8UL >= (((((*g_1877) <= (*g_307)) , (safe_add_func_int64_t_s_s(l_3160, 0L))) || l_3160) , (*g_270))) != l_3161), (**g_168))), l_3160)))) & g_1601))) >= (*g_270)) ^ (*g_376)), l_2996)) , l_3163)) , l_3164[2][1][0])), l_3161))) != 0x08L) & 0x411DCE4CL)) ^ 0xC6EFL) , l_3001[2][3][1]) , l_3160)), (**g_168))))) == (*g_376))), l_3161)))), l_3160)), (*l_3065))), 8)));
                l_3176--;
                (*g_3182) = l_3179;
                for (g_66 = 0; (g_66 <= 3); g_66 += 1)
                { /* block id: 1439 */
                    int32_t *****l_3190 = &g_2196;
                    int32_t l_3212[7];
                    uint8_t *l_3214[5][5][5] = {{{(void*)0,(void*)0,&l_2415[0],&l_2415[0],(void*)0},{&l_2415[2],&g_2935[6][0][3],(void*)0,&g_2935[6][0][3],(void*)0},{&g_2935[9][0][1],&g_2935[9][0][1],&g_571,&g_2935[0][4][1],(void*)0},{&g_2935[6][0][3],(void*)0,&l_2415[0],(void*)0,&g_2935[6][0][3]},{(void*)0,(void*)0,&g_571,&g_2935[6][0][3],(void*)0}},{{&g_2935[6][0][3],&g_571,&g_2935[6][0][3],&g_2935[6][0][3],&g_571},{&g_2935[6][0][3],&g_571,&g_2935[9][0][1],&g_2935[6][0][3],(void*)0},{&g_2935[0][4][1],(void*)0,&l_2415[2],&g_2935[6][0][3],(void*)0},{&l_2415[0],&l_2415[0],(void*)0,(void*)0,&g_571},{&g_2935[0][4][1],&l_2415[0],&g_571,&l_2415[0],&g_2935[0][4][1]}},{{&g_2935[6][0][3],(void*)0,&g_571,(void*)0,&g_571},{&g_2935[6][0][3],(void*)0,(void*)0,&g_2935[9][0][1],(void*)0},{(void*)0,&g_2935[0][4][1],&l_2415[2],(void*)0,&g_571},{&g_571,&g_2935[9][0][1],&g_2935[9][0][1],&g_571,&g_2935[0][4][1]},{&g_571,&g_2935[9][0][1],&g_2935[6][0][3],(void*)0,&g_571}},{{(void*)0,&g_2935[0][4][1],&g_571,&l_2415[0],(void*)0},{&g_2935[6][0][3],(void*)0,&l_2415[0],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_571,&g_571},{(void*)0,&l_2415[0],&l_2415[0],(void*)0,(void*)0},{&g_2935[6][0][3],&l_2415[0],&g_2935[0][4][1],&g_2935[9][0][1],&g_2935[6][0][3]}},{{(void*)0,(void*)0,&l_2415[0],(void*)0,&l_2415[0]},{&g_571,&g_571,(void*)0,&l_2415[0],&l_2415[0]},{&g_571,&g_571,&l_2415[0],(void*)0,&g_2935[6][0][3]},{(void*)0,(void*)0,&g_571,&g_2935[6][0][3],(void*)0},{&g_2935[6][0][3],&g_571,&g_2935[6][0][3],&g_2935[6][0][3],&g_571}}};
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_3212[i] = 0x9EE385EBL;
                    if (g_1396)
                        goto lbl_2571;
                    if ((*g_376))
                    { /* block id: 1441 */
                        (*p_11) = (*l_3065);
                    }
                    else
                    { /* block id: 1443 */
                        int i, j;
                        g_834[g_66][(g_66 + 2)] = 0x1.Ep+1;
                    }
                    if (((((*g_376) = ((void*)0 == l_3185[1][0])) != (safe_sub_func_uint64_t_u_u((**g_711), (l_3006 |= (safe_sub_func_uint8_t_u_u(((l_3190 != l_3191) , (l_3175[0] ^= ((((!(safe_add_func_int16_t_s_s((safe_div_func_uint16_t_u_u(((l_3197 & (((g_2009 ^= (*g_270)) ^ (safe_unary_minus_func_int16_t_s((safe_add_func_int32_t_s_s((safe_add_func_uint64_t_u_u(((safe_rshift_func_uint8_t_u_s((((safe_rshift_func_int8_t_s_u((safe_mod_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((((*l_3065) | 0x6E2CL) & 65535UL), 0x7569L)), (*l_2239))), (*l_2239))) , (*l_2239)) <= 0xEA5B5A65L), (*g_3114))) == 0x6BCFA92C26E24CA7LL), l_3169)), (*g_270)))))) == l_3160)) > l_3169), l_3212[4])), l_3213))) || (**g_1876)) <= 0x00ABEF56518E41DCLL) >= (***g_3112)))), l_3171[0])))))) < (*g_3114)))
                    { /* block id: 1450 */
                        int8_t ****l_3216 = (void*)0;
                        int i, j;
                        g_834[g_66][g_66] = ((g_3217 = l_3215[4]) == (void*)0);
                    }
                    else
                    { /* block id: 1453 */
                        volatile uint32_t * volatile ***l_3219[5] = {&g_2094,&g_2094,&g_2094,&g_2094,&g_2094};
                        int32_t * const * const ***l_3233[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int32_t l_3234 = 0x31751264L;
                        uint32_t l_3235 = 0xC6199BD2L;
                        int i;
                        if (l_3212[4])
                            break;
                        if ((*p_11))
                            break;
                        g_990[2][7][2] = &g_2095;
                        (*p_11) = (((void*)0 == l_3220) == (((((((safe_add_func_int32_t_s_s(((l_3167 = (((**g_1876) = ((safe_rshift_func_int8_t_s_s((safe_sub_func_uint16_t_u_u((((*l_3065) = (((((*g_270) |= (safe_lshift_func_int8_t_s_s((**g_3113), 1))) == ((*p_11) < (safe_sub_func_int8_t_s_s(((l_3233[3] = g_3231) == (void*)0), (-1L))))) > ((***g_2159) >= l_3169)) , (-1L))) > l_3234), 65528UL)), 0)) == 0x2FL)) >= l_3234)) || l_3172[2][3]), (**g_2715))) , l_3235) & 0L) || l_3174) == l_3235) , (*g_712)) != 0x70E95C8CE8346B31LL));
                    }
                }
            }
            (*g_3237) = (*l_2383);
            (*p_11) &= (((*g_582) = (l_3238 == l_3239[2][0])) , ((safe_add_func_uint32_t_u_u(((l_3244 != l_3244) != 0xBDL), ((safe_rshift_func_uint8_t_u_s(((-1L) != (((safe_rshift_func_uint8_t_u_s((safe_sub_func_uint64_t_u_u((safe_mul_func_int8_t_s_s(l_3175[0], ((*g_2644) = (((safe_mod_func_uint32_t_u_u((65526UL >= (*l_2239)), 0x1F53ED28L)) , l_2852[1][2][0]) | (**g_3218))))), (*l_2239))), 6)) != 0x61D8530EL) || 0xE7A0CDFEDD6E41B2LL)), l_3175[1])) || (*l_3065)))) || 0L));
            (***g_2732) = ((-0x1.Cp-1) == (*g_1447));
        }
        if (((**g_711) | (l_3255 == (((l_3256 != &g_2652) == ((void*)0 != l_3260[4])) , &g_2539))))
        { /* block id: 1472 */
            int32_t l_3276[2];
            uint8_t **l_3279[2][7] = {{&g_570,&g_570,&g_570,(void*)0,&g_570,(void*)0,&g_570},{&g_570,&g_570,&g_570,(void*)0,&g_570,(void*)0,&g_570}};
            int32_t l_3318 = 1L;
            int64_t l_3319[2];
            int32_t l_3321 = 0x79072AE4L;
            int32_t l_3322[3][10] = {{0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L},{0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L},{0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L,0x18D807E7L}};
            int16_t *l_3350 = &l_3295[0];
            int64_t l_3356 = 1L;
            int64_t l_3366 = 0x7C1A47E9787A589BLL;
            uint64_t ***l_3395 = &g_711;
            int64_t l_3402 = 1L;
            int64_t l_3405 = 0xCE93F45A19CE29BFLL;
            uint32_t * const **l_3464 = (void*)0;
            uint32_t * const ***l_3463 = &l_3464;
            const int32_t * const *l_3476 = &g_2073;
            const int32_t * const **l_3475[3];
            const int32_t * const ***l_3474 = &l_3475[2];
            const int32_t * const ****l_3473 = &l_3474;
            const int32_t * const ****l_3477 = &l_3474;
            int16_t l_3497[6];
            uint8_t **l_3518 = (void*)0;
            int32_t **l_3534[3][9] = {{&l_3065,&l_3065,&g_376,(void*)0,&g_376,&l_3065,&l_3065,&g_376,(void*)0},{&l_3065,&g_4,&l_3065,&g_376,&g_376,&l_3065,&g_4,&l_3065,&g_376},{&l_3065,&g_376,&g_376,&l_3065,&g_4,&l_3065,&g_376,&l_3065,(void*)0}};
            int i, j;
            for (i = 0; i < 2; i++)
                l_3276[i] = (-9L);
            for (i = 0; i < 2; i++)
                l_3319[i] = 1L;
            for (i = 0; i < 3; i++)
                l_3475[i] = &l_3476;
            for (i = 0; i < 6; i++)
                l_3497[i] = (-7L);
            for (l_3197 = 0; (l_3197 > 19); l_3197 = safe_add_func_int32_t_s_s(l_3197, 4))
            { /* block id: 1475 */
                (*p_11) ^= 0x32580EA7L;
            }
            for (g_2632 = 0; (g_2632 <= 3); g_2632 += 1)
            { /* block id: 1480 */
                int32_t ***l_3294[10][3][1] = {{{(void*)0},{&g_1035[2][4][1]},{(void*)0}},{{&g_1035[2][4][1]},{(void*)0},{&g_1035[2][4][1]}},{{(void*)0},{&g_1035[2][4][1]},{(void*)0}},{{&g_1035[2][4][1]},{(void*)0},{&g_1035[2][4][1]}},{{(void*)0},{&g_1035[2][4][1]},{(void*)0}},{{&g_1035[2][4][1]},{(void*)0},{&g_1035[2][4][1]}},{{(void*)0},{&g_1035[2][4][1]},{(void*)0}},{{&g_1035[2][4][1]},{(void*)0},{&g_1035[2][4][1]}},{{(void*)0},{&g_1035[2][4][1]},{(void*)0}},{{&g_1035[2][4][1]},{(void*)0},{&g_1035[2][4][1]}}};
                float l_3326 = 0xE.18AA7Dp-99;
                uint64_t l_3336 = 0x76E2E89ECBF54D7BLL;
                int32_t l_3360 = (-1L);
                int32_t *l_3370 = &g_89;
                int32_t l_3404[2];
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_3404[i] = 0xDF4C078BL;
                if ((safe_sub_func_int8_t_s_s(l_2415[g_2632], (((--(*g_270)) & (((((((safe_unary_minus_func_int8_t_s((safe_lshift_func_int16_t_s_u(((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f(l_3276[1], (safe_add_func_float_f_f((l_3279[1][0] != (*l_3110)), (safe_sub_func_float_f_f(l_2415[g_2632], ((safe_mul_func_float_f_f(0x7.8E36BDp-45, (((safe_sub_func_float_f_f(l_3276[1], ((**l_2934) = (safe_sub_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f(((safe_add_func_float_f_f(((l_3276[1] , (g_1035[2][4][1] = &p_12)) == (void*)0), 0x7.40F648p-88)) >= (***g_2732)), 0x9.F316ACp+88)), (*g_579))), l_3295[0]))))) != (**g_578)) > l_3276[1]))) == (*l_2239)))))))), 0xB.794539p-54)), l_3276[0])) , (*g_582)), 14)))) && (**g_3218)) != l_3276[1]) == 0x199CA5D84958BB2FLL) & 255UL) | (**g_711)) < (****g_444))) != l_3276[1]))))
                { /* block id: 1484 */
                    int16_t l_3303 = 0xB744L;
                    int32_t l_3320 = 0x2BCD1101L;
                    int32_t l_3323 = 0xA6A677DAL;
                    int32_t l_3324 = 0x497033B7L;
                    int32_t l_3325 = (-8L);
                    int32_t l_3327 = 8L;
                    int32_t l_3328 = 0x048B2EEEL;
                    int32_t l_3330 = 8L;
                    int32_t l_3332 = 0xF96C72EDL;
                    int32_t l_3333[8] = {9L,1L,1L,9L,1L,1L,9L,1L};
                    int16_t *l_3351 = &l_2977[1][0][0];
                    int32_t l_3359 = 1L;
                    int32_t l_3362[1][7];
                    uint16_t **l_3369 = &g_169;
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 7; j++)
                            l_3362[i][j] = 1L;
                    }
                    if (((safe_mod_func_uint64_t_u_u(((*p_11) < (safe_unary_minus_func_uint8_t_u((safe_div_func_int32_t_s_s(((-1L) > l_3276[1]), (safe_mod_func_uint64_t_u_u((((**g_2715) ^ l_3303) ^ (safe_mul_func_uint8_t_u_u(((g_3308 = l_3306) == g_3309), (safe_div_func_uint16_t_u_u((+(****g_444)), (*l_2239)))))), l_3303))))))), l_3276[0])) && (***g_170)))
                    { /* block id: 1486 */
                        int8_t l_3329 = 0x11L;
                        int32_t l_3331 = (-9L);
                        int32_t l_3334 = 0L;
                        int32_t l_3335 = 1L;
                        uint8_t *****l_3339[2][3] = {{&l_3255,&l_3255,&l_3255},{&l_3255,&l_3255,&l_3255}};
                        int i, j;
                        --l_3336;
                        if ((*p_11))
                            break;
                        l_3255 = &g_2539;
                    }
                    else
                    { /* block id: 1490 */
                        return l_3333[0];
                    }
                    if (l_3320)
                        break;
                    for (l_3327 = 0; (l_3327 <= 0); l_3327 += 1)
                    { /* block id: 1496 */
                        int32_t l_3345 = (-5L);
                        int32_t l_3357 = 5L;
                        int32_t l_3358 = 0x534E68C3L;
                        int32_t l_3361[4] = {0xF0866236L,0xF0866236L,0xF0866236L,0xF0866236L};
                        uint32_t l_3363 = 4UL;
                        uint8_t ***l_3367 = &l_2178;
                        int i;
                        if (g_162)
                            goto lbl_3340;
                        (*g_376) = (0x5B0CL | ((*g_3313) ^ ((*l_3065) >= ((safe_sub_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((l_3345 == ((safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u(((l_3351 = l_3350) != &l_3303), l_3332)), 4)) && 0x8DECE962D806A61FLL)), l_3333[0])), (*g_2644))) || l_3332))));
                        l_3370 = func_28(&l_3333[0], p_12, (l_3345 , ((safe_mul_func_uint16_t_u_u(((***g_445) = (safe_div_func_int16_t_s_s((((**l_2389) ^= l_3333[0]) > ((((l_3363--) , ((((l_3366 , l_3367) != &l_3279[1][0]) , l_3333[3]) > ((l_3357 , l_3368[0]) == l_3369))) , 1UL) > l_3366)), l_3319[0]))), l_3361[2])) || l_3345)));
                    }
                }
                else
                { /* block id: 1505 */
                    (**g_2715) &= (l_3006 &= 6L);
                    if (l_3319[0])
                        break;
                }
                for (l_3360 = 1; (l_3360 >= 0); l_3360 -= 1)
                { /* block id: 1512 */
                    int64_t l_3371[4][5] = {{0L,0L,0L,0L,0L},{0L,0L,1L,0L,0L},{0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L}};
                    int32_t **l_3397[6] = {&l_2314[1],&l_2314[1],&l_2314[1],&l_2314[1],&l_2314[1],&l_2314[1]};
                    uint64_t l_3409 = 0x5DC5B4171B501B71LL;
                    int32_t l_3412 = 0xD98075A3L;
                    int i, j;
                    (*p_11) = l_3371[2][4];
                    if (((safe_add_func_uint64_t_u_u(0xB65FB71BB5148BABLL, (((safe_mod_func_uint8_t_u_u(l_3319[1], (safe_sub_func_int64_t_s_s((safe_sub_func_int32_t_s_s((*p_11), ((l_3371[2][4] && (+(**g_1876))) <= (safe_unary_minus_func_int16_t_s((((((*g_582) = ((((!(safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s(l_3371[2][4])), 3)), ((safe_sub_func_uint16_t_u_u((*l_3065), ((((((+(safe_mod_func_uint16_t_u_u(((*****l_2310) = (****g_444)), (safe_mul_func_int8_t_s_s((*g_3313), l_3322[0][4]))))) < l_3371[2][4]) != 0L) , (-2L)) <= l_3318) > (***g_2159)))) & 0x8786DA59C1257394LL)))) & 0xD28EL) >= l_3321) , l_3319[0])) , (-9L)) , (void*)0) == l_3395)))))), (*g_712))))) >= l_3174) == l_3371[1][3]))) , 0x6EE8B92EL))
                    { /* block id: 1516 */
                        if (l_3371[1][1])
                            break;
                        if (g_104)
                            goto lbl_3340;
                        (*l_2382) = (((!8UL) >= 65535UL) , l_3397[1]);
                    }
                    else
                    { /* block id: 1520 */
                        int16_t l_3398 = 0x7EF4L;
                        int32_t l_3399 = (-10L);
                        int32_t l_3400 = 9L;
                        int32_t l_3401[1][2][9] = {{{4L,4L,0xF91F39C9L,(-9L),(-1L),0xF91F39C9L,(-1L),(-9L),0xF91F39C9L},{4L,4L,0xF91F39C9L,(-9L),(-1L),0xF91F39C9L,(-1L),(-9L),0xF91F39C9L}}};
                        int i, j, k;
                        (*g_376) = (-4L);
                        --l_3409;
                        return l_3412;
                    }
                }
            }
            for (l_3402 = 0; (l_3402 <= 1); l_3402 += 1)
            { /* block id: 1529 */
                uint8_t **l_3429 = &g_570;
                int32_t l_3438 = (-1L);
                int32_t l_3439 = 0xBA3057BFL;
                int64_t * const l_3442 = &g_1396;
                int16_t * const l_3460 = (void*)0;
                uint8_t ****l_3481[9];
                int i;
                for (i = 0; i < 9; i++)
                    l_3481[i] = (void*)0;
                if (l_3276[l_3402])
                { /* block id: 1530 */
                    int32_t l_3452 = 0x8B6E6B3DL;
                    uint16_t ****l_3484 = (void*)0;
                    uint8_t **l_3491 = &g_570;
                    const int8_t *l_3516 = &g_3517;
                    int i;
                    if ((((safe_add_func_int32_t_s_s((l_3295[l_3402] <= (safe_sub_func_int64_t_s_s((safe_add_func_int64_t_s_s((safe_mod_func_int64_t_s_s(((*p_11) | l_3276[l_3402]), (safe_sub_func_uint16_t_u_u(8UL, ((safe_mul_func_uint8_t_u_u((l_3439 = (l_3438 = (safe_rshift_func_int16_t_s_u(((safe_rshift_func_int16_t_s_s((((((*l_3110) = l_3279[1][0]) == l_3429) , (l_3276[l_3402] , (safe_div_func_uint8_t_u_u(4UL, (safe_rshift_func_int16_t_s_s((l_3175[0] = (safe_mod_func_int32_t_s_s(((safe_rshift_func_uint16_t_u_s((l_3319[0] , 0x2CECL), 3)) == 0x3CL), (*g_270)))), l_3295[l_3402])))))) > l_3295[l_3402]), 3)) != l_3295[l_3402]), (***g_445))))), 0x0FL)) != l_3295[l_3402]))))), l_3321)), (***g_2159)))), (*g_270))) | 1UL) || l_3295[l_3402]))
                    { /* block id: 1535 */
                        l_3438 = (((((safe_add_func_uint32_t_u_u((l_3442 == &l_3405), (safe_lshift_func_int16_t_s_u((safe_lshift_func_int16_t_s_u((l_3295[l_3402] , (((safe_div_func_int16_t_s_s((safe_mul_func_int8_t_s_s((*****g_3309), (((!((l_3452 &= 0x4CL) ^ (safe_sub_func_uint64_t_u_u((**g_711), l_3455)))) || (safe_div_func_int64_t_s_s((**g_1876), (safe_div_func_uint8_t_u_u(l_3006, l_3438))))) | 0xD97992BBC2554219LL))), l_3276[1])) , 18446744073709551609UL) , 1L)), l_3295[l_3402])), l_3295[l_3402])))) , l_3460) == l_3350) & l_3462[7]) & (*g_270));
                    }
                    else
                    { /* block id: 1538 */
                        uint32_t * const ****l_3465 = (void*)0;
                        uint32_t * const ****l_3466 = &l_3463;
                        int32_t l_3467 = 0x56BEA10FL;
                        int32_t * const ****l_3472[2][8] = {{&l_2381,&l_2381,&l_2381,&l_2381,&l_2381,&l_2381,&l_2381,&l_2381},{&l_2381,&l_2381,&l_2381,&l_2381,&l_2381,&l_2381,&l_2381,&l_2381}};
                        int i, j;
                        l_3467 = (l_3452 = (((*l_3466) = l_3463) != (((*g_579) = ((0x2.771213p-39 > (*g_579)) != ((l_3276[l_3402] , (((*g_1015) = (l_3467 , (*g_1447))) <= (safe_sub_func_float_f_f(0x4.Ep-1, (safe_add_func_float_f_f((((void*)0 != &g_2159) >= l_3295[0]), l_3276[l_3402])))))) <= l_3321))) , &l_2881)));
                        if ((*p_11))
                            continue;
                        (**g_2733) = (((*g_579) = (((g_1601 ^= ((((*p_11) , (((((*g_270) , (l_3472[1][7] == (l_3477 = l_3473))) | ((l_3276[l_3402] <= (((l_3484 = (((**l_3146) = ((((((!l_3438) ^ (safe_rshift_func_int8_t_s_u(((void*)0 != l_3481[5]), l_3452))) > l_3482) == (****l_3474)) | 65535UL) <= l_3276[l_3402])) , (void*)0)) == (*l_2310)) == 9UL)) <= (***g_3112))) || (*g_270)) & (*g_307))) , (*l_3065)) , l_3276[l_3402])) , (*g_270)) , (*g_1015))) == l_3485[0][0]);
                    }
                    (*l_3065) |= ((*****l_3473) && (safe_sub_func_uint16_t_u_u(((l_3452 , (*g_712)) || ((safe_sub_func_uint16_t_u_u(((****g_444) = (l_3295[l_3402] ^ (((*l_3110) = g_3490) == l_3491))), (safe_mul_func_int8_t_s_s((l_3318 = (18446744073709551615UL && l_3438)), (safe_unary_minus_func_uint64_t_u((safe_sub_func_int32_t_s_s((l_3497[5] < 0x9A848FDBL), l_3438)))))))) , (-1L))), 65535UL)));
                    if ((safe_mul_func_uint16_t_u_u(1UL, (safe_mul_func_int16_t_s_s(((safe_add_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u((***g_445), 0xB378L)), (((1UL == ((((***l_2851) = (0x8BL & (l_3439 <= (safe_unary_minus_func_int8_t_s((safe_div_func_uint32_t_u_u((l_3276[l_3402] == (((void*)0 == (*g_444)) > l_3276[l_3402])), (*l_2239)))))))) < 0UL) & 8UL)) && 3UL) || l_3509))) , l_3276[l_3402]), 5L)))))
                    { /* block id: 1557 */
                        (*g_376) &= (*g_1686);
                        return (*****l_3477);
                    }
                    else
                    { /* block id: 1560 */
                        uint64_t ****l_3512[7][3] = {{&l_3395,&l_3395,&l_3395},{&l_2851,&l_3395,&l_3395},{&l_3395,&l_2851,&l_3395},{&l_2851,&l_2851,&l_2851},{&l_3395,&l_3395,&l_3395},{&l_3395,&l_3395,&l_3395},{&l_2851,&l_3395,&l_3395}};
                        uint64_t *****l_3511 = &l_3512[4][2];
                        int i, j;
                        (*l_3511) = &l_2851;
                        if ((**l_3476))
                            break;
                        if ((*p_11))
                            continue;
                        if ((*p_11))
                            continue;
                    }
                    (*l_3065) = ((safe_add_func_int8_t_s_s(((*****l_3306) &= ((((l_3516 = l_3515) == (void*)0) && ((void*)0 == l_3518)) > ((*g_582) |= ((((((((**g_3218) |= ((+((((l_3321 = (((0x7803L ^ ((**l_3476) < (+(0x688BA639EE2E4D00LL >= (!((**l_3476) <= ((safe_sub_func_uint8_t_u_u(1UL, l_3276[l_3402])) == 1UL))))))) >= (***g_710)) , 0x3C0DL)) >= l_3295[l_3402]) , (-4L)) , (*g_1877))) == 0xD346L)) , 0xC73BF549L) == (*g_270)) <= (***g_445)) > l_3438) | l_3295[l_3402]) & (*g_712))))), 1UL)) & 0x39L);
                }
                else
                { /* block id: 1572 */
                    int32_t * const ****l_3533 = &l_2381;
                    (*g_376) = (*p_11);
                    l_3439 &= (safe_mod_func_uint16_t_u_u(((((*p_11) > (0xB5873FA70CC4A37BLL != l_3276[l_3402])) >= (*l_3065)) ^ (0L ^ (safe_add_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((~(((*l_3533) = &l_2382) != (void*)0)), 0x1DL)), 0xCB65L)))), 65528UL));
                }
            }
            l_3535[0] = p_11;
        }
        else
        { /* block id: 1579 */
            (*g_380) = p_12;
        }
    }
    return (*g_582);
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_20 g_47 g_40 g_66 g_74 g_80 g_88 g_89 g_570 g_571 g_270 g_104 g_579 g_588 g_376 g_1686 g_582 g_162 g_1447 g_834 g_445 g_168 g_169 g_1945 g_735 g_1876 g_1877 g_1396 g_1964 g_1968 g_462 g_578 g_731 g_732 g_711 g_712 g_156 g_1015 g_444 g_373 g_307 g_308 g_375
 * writes: g_20 g_4 g_47 g_88 g_89 g_80 g_104 g_40 g_167 g_462 g_162 g_571 g_66 g_1945 g_113 g_75 g_1969 g_834 g_141 g_2003 g_2009 g_2011 g_570 g_2046 g_2047 g_156 g_2073
 */
static int32_t * func_13(uint64_t  p_14)
{ /* block id: 5 */
    uint32_t *l_2008 = &g_2009;
    uint64_t *l_2010 = &g_2011[7][1];
    int32_t l_2016 = 4L;
    uint64_t *l_2017 = &g_156;
    int32_t l_2022 = 0x047F4CB3L;
    uint8_t *l_2024[10][1] = {{&g_571},{(void*)0},{&g_571},{&g_571},{(void*)0},{&g_571},{&g_571},{(void*)0},{&g_571},{&g_571}};
    uint8_t **l_2025 = &g_570;
    int8_t *l_2041[3][9][1] = {{{&g_2003},{&g_735},{&g_735},{&g_2003},{&g_735},{&g_735},{&g_2003},{&g_735},{&g_735}},{{&g_2003},{&g_735},{&g_735},{&g_2003},{&g_735},{&g_735},{&g_2003},{&g_735},{&g_735}},{{&g_2003},{&g_735},{&g_735},{&g_2003},{&g_735},{&g_735},{&g_2003},{&g_735},{&g_735}}};
    const uint32_t l_2042[8][4][8] = {{{0xE48304D2L,18446744073709551615UL,0xAF3DAEC7L,0x951282C0L,4UL,0x951282C0L,0xAF3DAEC7L,18446744073709551615UL},{0xAF3DAEC7L,0x421609FEL,0x8B4B5D7DL,18446744073709551608UL,4UL,18446744073709551615UL,4UL,18446744073709551608UL},{0xE48304D2L,0x421609FEL,0xE48304D2L,18446744073709551615UL,0xAF3DAEC7L,0x951282C0L,4UL,18446744073709551608UL},{0xAF3DAEC7L,0x951282C0L,4UL,0x951282C0L,0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL}},{{0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL,0xE48304D2L,18446744073709551615UL,0xAF3DAEC7L,0x951282C0L},{0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL,0xE48304D2L,0x951282C0L},{0xE48304D2L,3UL,4UL,0x421609FEL,0x34E51FFCL,0x951282C0L,0x34E51FFCL,0x421609FEL},{0x8B4B5D7DL,3UL,0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL}},{{0xAF3DAEC7L,0x951282C0L,4UL,0x951282C0L,0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL},{0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL,0xE48304D2L,18446744073709551615UL,0xAF3DAEC7L,0x951282C0L},{0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL,0xE48304D2L,0x951282C0L},{0xE48304D2L,3UL,4UL,0x421609FEL,0x34E51FFCL,0x951282C0L,0x34E51FFCL,0x421609FEL}},{{0x8B4B5D7DL,3UL,0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL},{0xAF3DAEC7L,0x951282C0L,4UL,0x951282C0L,0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL},{0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL,0xE48304D2L,18446744073709551615UL,0xAF3DAEC7L,0x951282C0L},{0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL,0xE48304D2L,0x951282C0L}},{{0xE48304D2L,3UL,4UL,0x421609FEL,0x34E51FFCL,0x951282C0L,0x34E51FFCL,0x421609FEL},{0x8B4B5D7DL,3UL,0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL},{0xAF3DAEC7L,0x951282C0L,4UL,0x951282C0L,0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL},{0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL,0xE48304D2L,18446744073709551615UL,0xAF3DAEC7L,0x951282C0L}},{{0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL,0xE48304D2L,0x951282C0L},{0xE48304D2L,3UL,4UL,0x421609FEL,0x34E51FFCL,0x951282C0L,0x34E51FFCL,0x421609FEL},{0x8B4B5D7DL,3UL,0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL},{0xAF3DAEC7L,0x951282C0L,4UL,0x951282C0L,0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL}},{{0xAF3DAEC7L,18446744073709551615UL,0xE48304D2L,0x421609FEL,0xE48304D2L,18446744073709551615UL,0xAF3DAEC7L,0x951282C0L},{0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL,0xE48304D2L,0x951282C0L},{0xE48304D2L,3UL,4UL,0x421609FEL,0x34E51FFCL,0x951282C0L,0xAF3DAEC7L,3UL},{4UL,18446744073709551615UL,4UL,18446744073709551608UL,0x8B4B5D7DL,0x421609FEL,0xAF3DAEC7L,0x421609FEL}},{{0xE48304D2L,18446744073709551608UL,0x34E51FFCL,18446744073709551608UL,0xE48304D2L,0x951282C0L,0x8B4B5D7DL,3UL},{0xE48304D2L,0x951282C0L,0x8B4B5D7DL,3UL,0x8B4B5D7DL,0x951282C0L,0xE48304D2L,18446744073709551608UL},{4UL,18446744073709551608UL,0x8B4B5D7DL,0x421609FEL,0xAF3DAEC7L,0x421609FEL,0x8B4B5D7DL,18446744073709551608UL},{0x8B4B5D7DL,18446744073709551615UL,0x34E51FFCL,3UL,0xAF3DAEC7L,18446744073709551608UL,0xAF3DAEC7L,3UL}}};
    int32_t l_2043 = (-1L);
    uint32_t *l_2045 = &g_126;
    uint32_t **l_2044[7] = {&l_2045,&l_2045,&l_2045,&l_2045,&l_2045,&l_2045,&l_2045};
    uint32_t *l_2048 = &g_126;
    uint32_t l_2071 = 1UL;
    int i, j, k;
    l_2022 &= (g_5 != (func_15(g_5) < (((*l_2008) = p_14) > (((*l_2010) = p_14) == (safe_lshift_func_int8_t_s_u(((safe_add_func_uint32_t_u_u(l_2016, ((void*)0 == l_2017))) || (safe_lshift_func_int16_t_s_u((safe_add_func_uint64_t_u_u((*g_712), (**g_711))), p_14))), p_14))))));
    (*g_376) ^= (safe_unary_minus_func_uint32_t_u((((p_14 , l_2016) & (l_2043 = (((((*l_2025) = l_2024[5][0]) != &g_855) == (safe_add_func_int32_t_s_s((!((--(*g_169)) != (safe_div_func_uint16_t_u_u((l_2016 , (((safe_sub_func_uint8_t_u_u(g_373, ((safe_unary_minus_func_int32_t_s(((safe_div_func_int32_t_s_s(p_14, (safe_mod_func_uint8_t_u_u(((p_14 != (l_2022 = ((~p_14) == l_2022))) || 8L), 0xD4L)))) >= 0xD38468D6ED341766LL))) ^ l_2042[6][0][2]))) , 1UL) < (**g_711))), (*g_582))))), 0xE583D562L))) ^ 18446744073709551607UL))) > (*g_307))));
    if (((g_2047 = (g_2046 = l_2008)) == l_2048))
    { /* block id: 972 */
        const uint32_t l_2049 = 0xCA97D3BFL;
        (*g_1015) = l_2049;
    }
    else
    { /* block id: 974 */
        int64_t l_2052[7];
        const int32_t *l_2072 = &g_80[3][5][4];
        int32_t *l_2074 = &l_2016;
        int32_t **l_2076 = &g_4;
        int i;
        for (i = 0; i < 7; i++)
            l_2052[i] = 2L;
        for (g_156 = 0; (g_156 > 53); g_156++)
        { /* block id: 977 */
            int32_t **l_2066[8][4] = {{&g_4,&g_376,&g_376,&g_4},{&g_4,&g_376,&g_376,&g_4},{&g_4,&g_376,&g_376,&g_4},{&g_4,&g_376,&g_376,&g_4},{&g_4,&g_376,&g_376,&g_4},{&g_4,&g_376,&g_376,&g_4},{&g_4,&g_376,&g_376,&g_4},{&g_4,&g_376,&g_376,&g_4}};
            int i, j;
            if (p_14)
                break;
            l_2052[5] = p_14;
            for (g_1945 = 0; (g_1945 != 18); ++g_1945)
            { /* block id: 982 */
                int32_t l_2056 = 0L;
                float **l_2061 = &g_579;
                float ***l_2060[3];
                float ****l_2059[5];
                int i;
                for (i = 0; i < 3; i++)
                    l_2060[i] = &l_2061;
                for (i = 0; i < 5; i++)
                    l_2059[i] = &l_2060[2];
                (*g_376) &= ((p_14 > ((+(l_2056 ^ 1UL)) == ((safe_mul_func_int8_t_s_s(p_14, p_14)) <= (((l_2059[0] != ((safe_add_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s(((**g_168) != ((l_2066[7][3] != (((safe_lshift_func_int8_t_s_s(((((safe_div_func_int64_t_s_s((-10L), p_14)) | p_14) > 0L) , l_2052[5]), l_2016)) || l_2043) , (void*)0)) , 65527UL)), p_14)), l_2071)) , (void*)0)) <= p_14) ^ p_14)))) <= l_2016);
            }
            g_2073 = (p_14 , l_2072);
        }
        (*g_1964) = func_28(l_2074, func_28(l_2074, l_2074, (p_14 > ((void*)0 == l_2072))), ((*g_270) , ((l_2022 ^ p_14) , 251UL)));
        (*l_2076) = &l_2016;
    }
    return (*g_375);
}


/* ------------------------------------------ */
/* 
 * reads : g_20 g_5 g_47 g_40 g_66 g_74 g_80 g_88 g_89 g_570 g_571 g_270 g_104 g_579 g_588 g_376 g_1686 g_582 g_162 g_1447 g_834 g_445 g_168 g_169 g_1945 g_735 g_1876 g_1877 g_1396 g_1964 g_1968 g_462 g_578 g_731 g_732 g_711 g_712 g_156 g_1015 g_444
 * writes: g_20 g_4 g_47 g_88 g_89 g_80 g_104 g_40 g_167 g_462 g_162 g_571 g_66 g_1945 g_113 g_75 g_1969 g_834 g_141 g_2003
 */
static uint64_t  func_15(int64_t  p_16)
{ /* block id: 6 */
    const int64_t l_23 = 0x2487223B16AAC895LL;
    uint32_t l_24[3];
    int32_t *l_37 = &g_20;
    const int8_t l_63 = 0x42L;
    uint32_t l_1703 = 4294967295UL;
    int32_t l_1711 = 0xDD96168CL;
    uint64_t ** const *l_1729[9] = {&g_711,&g_711,&g_711,&g_711,&g_711,&g_711,&g_711,&g_711,&g_711};
    int16_t **l_1760 = &g_582;
    int32_t ***l_1779 = (void*)0;
    int32_t ****l_1778 = &l_1779;
    int8_t * const *l_1810 = (void*)0;
    int32_t l_1911[10][3] = {{(-1L),0x033C454DL,(-1L)},{0xE6617BF1L,0xE6617BF1L,0xE6617BF1L},{(-1L),0x033C454DL,(-1L)},{0xE6617BF1L,0xE6617BF1L,0xE6617BF1L},{(-1L),0x033C454DL,(-1L)},{0xE6617BF1L,0xE6617BF1L,0xE6617BF1L},{(-1L),0x033C454DL,(-1L)},{0xE6617BF1L,0xE6617BF1L,0xE6617BF1L},{(-1L),0x033C454DL,(-1L)},{0xE6617BF1L,0xE6617BF1L,0xE6617BF1L}};
    int16_t l_1931 = (-1L);
    uint16_t l_2005[4];
    int i, j;
    for (i = 0; i < 3; i++)
        l_24[i] = 0x53413F08L;
    for (i = 0; i < 4; i++)
        l_2005[i] = 65530UL;
    for (p_16 = 0; (p_16 <= (-20)); p_16 = safe_sub_func_uint32_t_u_u(p_16, 6))
    { /* block id: 9 */
        int32_t *l_19 = &g_20;
        (*l_19) |= (-2L);
    }
    for (g_20 = (-16); (g_20 >= (-11)); g_20++)
    { /* block id: 14 */
        int32_t *l_39[7][8] = {{&g_40,&g_5,&g_5,&g_5,&g_40,&g_40,&g_40,&g_5},{&g_5,&g_20,&g_5,&g_5,&g_5,&g_5,&g_20,&g_5},{(void*)0,&g_40,&g_5,&g_5,&g_40,&g_40,&g_40,&g_5},{&g_5,(void*)0,&g_5,&g_5,&g_5,&g_40,&g_5,&g_5},{&g_5,&g_40,(void*)0,(void*)0,&g_40,&g_5,&g_5,&g_40},{&g_5,&g_20,&g_5,&g_40,&g_5,&g_40,&g_5,&g_20},{&g_5,&g_5,&g_40,&g_40,&g_40,&g_5,&g_5,&g_40}};
        int32_t l_64 = 0x79405A9AL;
        uint16_t * const l_65 = &g_66;
        int32_t l_1712 = 0xB722023CL;
        uint64_t ***l_1725[4] = {&g_711,&g_711,&g_711,&g_711};
        uint64_t l_1754[9][2];
        uint64_t l_1787 = 1UL;
        int64_t l_1855 = 0x0B9E95BA6BD49452LL;
        int64_t *l_1874 = &g_1396;
        int64_t **l_1873 = &l_1874;
        uint16_t ****l_1891 = &g_445;
        int i, j;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 2; j++)
                l_1754[i][j] = 0x70649B68E16B75B2LL;
        }
        if (l_23)
            break;
        for (p_16 = 0; (p_16 <= 2); p_16 += 1)
        { /* block id: 18 */
            int32_t **l_38[7][7][2] = {{{&l_37,(void*)0},{&l_37,(void*)0},{&l_37,&l_37},{&l_37,&l_37},{&l_37,&l_37},{&l_37,(void*)0},{&l_37,&l_37}},{{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37}},{{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0}},{{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37}},{{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37}},{{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0}},{{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37},{(void*)0,&l_37},{&l_37,(void*)0},{&l_37,&l_37}}};
            uint16_t *l_46[6][8] = {{(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47},{(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47},{(void*)0,&g_47,&g_47,&g_47,(void*)0,&g_47,(void*)0,&g_47},{(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47},{(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47,(void*)0,&g_47},{(void*)0,&g_47,&g_47,&g_47,(void*)0,&g_47,(void*)0,&g_47}};
            int64_t *l_1730 = &g_197;
            int16_t ***l_1793 = &l_1760;
            int8_t *l_1802[5];
            uint32_t *l_1819[5];
            uint32_t **l_1818[2][6][10] = {{{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]}},{{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]},{&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1],&l_1819[1]}}};
            int32_t l_1881 = 0L;
            float l_1912 = (-0x6.Cp+1);
            uint8_t l_1914 = 0UL;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_1802[i] = &g_153;
            for (i = 0; i < 5; i++)
                l_1819[i] = &g_126;
            l_1712 ^= (l_1711 = (0xBC21B07FL & func_25(func_28((g_4 = &g_20), (func_32(&g_20, (l_39[6][1] = l_37), func_41((l_24[p_16] , g_5), (safe_mul_func_uint16_t_u_u((g_47--), (+((((safe_mul_func_int16_t_s_s((safe_add_func_int8_t_s_s(((safe_mul_func_int8_t_s_s(g_40, (((func_57(((safe_rshift_func_uint8_t_u_s(((((((0x7A762AFEL > l_63) && (*l_37)) && 0L) ^ (-1L)) ^ l_64) && g_20), p_16)) > g_5), l_65, p_16) && (-1L)) > p_16) ^ p_16))) , p_16), (*g_570))), 1L)) == p_16) | p_16) == l_64))))), p_16) , l_37), l_1703), p_16)));
        }
        for (g_88 = 5; (g_88 > 28); g_88 = safe_add_func_uint8_t_u_u(g_88, 7))
        { /* block id: 917 */
            uint8_t l_1957 = 0x3CL;
            float *l_2002 = &g_141;
            int32_t l_2004 = (-8L);
            for (l_1703 = (-23); (l_1703 < 17); l_1703 = safe_add_func_int32_t_s_s(l_1703, 4))
            { /* block id: 920 */
                int16_t *l_1923 = &g_162;
                int32_t l_1924[9][8] = {{0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L},{0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L},{0x29A7AB23L,0x29A7AB23L,4L,0x29A7AB23L,0x29A7AB23L,4L,0x29A7AB23L,0x29A7AB23L},{0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L},{0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L},{0x29A7AB23L,0x29A7AB23L,4L,0x29A7AB23L,0x29A7AB23L,4L,0x29A7AB23L,0x29A7AB23L},{0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L},{0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L,0x4F68A1D9L,0x29A7AB23L,0x4F68A1D9L},{0x29A7AB23L,0x29A7AB23L,4L,0x29A7AB23L,0x29A7AB23L,4L,0x29A7AB23L,0x29A7AB23L}};
                int32_t *l_1963[1][8] = {{&l_1911[9][1],&l_1911[9][1],&l_1911[9][1],&l_1911[9][1],&l_1911[9][1],&l_1911[9][1],&l_1911[9][1],&l_1911[9][1]}};
                const float *l_1967[8];
                const float ** const l_1966 = &l_1967[4];
                const float ** const *l_1965 = &l_1966;
                int i, j;
                for (i = 0; i < 8; i++)
                    l_1967[i] = &g_141;
                for (g_162 = 0; (g_162 <= 18); g_162 = safe_add_func_uint32_t_u_u(g_162, 8))
                { /* block id: 923 */
                    uint32_t l_1958 = 0x23671918L;
                    int32_t *l_1962 = &l_1711;
                    if (((l_1923 == (void*)0) , p_16))
                    { /* block id: 924 */
                        int64_t *l_1925[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1925[i] = (void*)0;
                        l_1924[6][0] |= 0x67F5B6FEL;
                        (**g_588) |= (((void*)0 != l_1925[0]) != (safe_div_func_int16_t_s_s(p_16, (*l_37))));
                        if (l_1924[8][7])
                            break;
                    }
                    else
                    { /* block id: 928 */
                        int8_t *l_1930[10][4] = {{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735},{&g_735,&g_735,&g_735,&g_735}};
                        uint32_t *l_1944 = &g_1945;
                        int32_t l_1956[6] = {5L,0L,5L,5L,0L,5L};
                        uint16_t *l_1959 = &g_113;
                        int i, j;
                        if (p_16)
                            break;
                        (*g_376) = ((*g_1686) <= ((p_16 = ((safe_rshift_func_int8_t_s_u((l_1931 = p_16), 3)) <= (0UL != (*g_582)))) || 0UL));
                        (*g_376) = ((((*l_1959) = ((((0x4.1p-1 == ((*g_1447) <= ((((p_16 , (safe_lshift_func_uint16_t_u_s((((*g_570)--) == p_16), 8))) , ((*g_579) = ((safe_rshift_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s((-1L), (++(****l_1891)))) && (((*l_1944) |= (--(*g_270))) == ((l_1956[2] = (safe_mul_func_int16_t_s_s(p_16, ((safe_mod_func_uint32_t_u_u((safe_add_func_int32_t_s_s((((safe_lshift_func_uint16_t_u_s((p_16 && (((*g_570) != ((safe_div_func_int32_t_s_s(p_16, p_16)) , 0x70L)) , (*g_570))), 13)) != (*l_37)) > g_735), p_16)), p_16)) || l_1924[2][1])))) & (**g_1876)))), l_1957)) , 0x3.362074p+68))) < l_1958) != 0xF.23993Ap-70))) , l_1958) > p_16) != 0xC9L)) , (void*)0) != (void*)0);
                    }
                    if ((safe_rshift_func_int8_t_s_u(l_1957, 7)))
                    { /* block id: 942 */
                        return l_1958;
                    }
                    else
                    { /* block id: 944 */
                        l_1963[0][1] = l_1962;
                        (*g_1964) = &l_1924[6][0];
                        (*g_1968) = l_1965;
                    }
                    (*g_579) = ((-(safe_div_func_float_f_f((*g_579), (**g_578)))) >= ((((((((((void*)0 != (*g_731)) | (safe_mul_func_uint16_t_u_u(((**g_168) = 0xE20FL), (safe_sub_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s(((((p_16 , (**g_711)) < 0L) , (void*)0) != (void*)0), (*l_37))) | (-4L)), (*g_570)))))) && p_16) , (*g_712)) && 0x700EF86608E92405LL) > (*l_1962)) , 0xB.768480p-15) != p_16) != (*l_37)));
                }
            }
            if (p_16)
                break;
            l_2004 = (((safe_rshift_func_uint8_t_u_u(((((safe_add_func_float_f_f(((0x3.CC6DB9p-77 == ((safe_mul_func_float_f_f(((safe_mul_func_float_f_f(l_1957, 0x7.1p-1)) == (((**g_578) > ((*g_1015) = (+(*g_1015)))) != ((((g_2003 = (safe_add_func_float_f_f(((*g_570) , (safe_mul_func_float_f_f((safe_mul_func_float_f_f(((safe_mul_func_float_f_f(((*l_2002) = (safe_mul_func_float_f_f((+((((safe_add_func_uint16_t_u_u(((((****g_444) >= (**g_168)) , (p_16 <= (****g_444))) >= 0x495CB95CL), p_16)) , g_80[3][5][4]) , p_16) == 0x5.8p-1)), p_16))), 0x1.3p-1)) != 0x0.7p-1), l_1957)), p_16))), p_16))) < l_1957) <= l_1957) >= (-0x1.Fp+1)))), p_16)) , l_1957)) <= l_1957), p_16)) , (-7L)) || p_16) && (*g_570)), 7)) , (*g_444)) == (void*)0);
        }
        --l_2005[3];
    }
    return p_16;
}


/* ------------------------------------------ */
/* 
 * reads : g_579 g_20
 * writes: g_462
 */
static int32_t  func_25(int32_t * p_26, int16_t  p_27)
{ /* block id: 810 */
    int16_t l_1704 = 0xFA38L;
    int32_t *l_1705[10] = {&g_5,(void*)0,&g_5,(void*)0,&g_5,(void*)0,&g_5,(void*)0,&g_5,(void*)0};
    int32_t l_1706 = 0xBED3DF33L;
    uint64_t l_1708 = 0x8BBF8E2EF4CE0D8CLL;
    int i;
    (*g_579) = p_27;
    l_1704 &= (*p_26);
    l_1708++;
    return (*p_26);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_28(int32_t * p_29, int32_t * p_30, uint8_t  p_31)
{ /* block id: 808 */
    return p_29;
}


/* ------------------------------------------ */
/* 
 * reads : g_270 g_104
 * writes:
 */
static uint32_t  func_32(int32_t * const  p_33, int32_t * p_34, const int32_t  p_35, uint16_t  p_36)
{ /* block id: 805 */
    p_34 = p_34;
    return (*g_270);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_104 g_47 g_40 g_167
 */
static const int32_t  func_41(int16_t  p_42, int32_t  p_43)
{ /* block id: 316 */
    int32_t *l_600 = (void*)0;
    int16_t **l_615 = &g_582;
    int32_t l_643 = 3L;
    int32_t l_646 = 0xDA46D844L;
    int32_t l_648 = (-6L);
    int32_t l_656 = 0xBB85A99FL;
    int32_t l_657[5][7] = {{0x6C4837A1L,(-1L),0x17A1C3FCL,(-1L),0x6C4837A1L,0x17A1C3FCL,0xC9EDEB05L},{0xC9EDEB05L,6L,0xA64CCB73L,0xC9EDEB05L,0xA64CCB73L,6L,0xC9EDEB05L},{0xB61C3802L,0xC9EDEB05L,6L,0xA64CCB73L,0xC9EDEB05L,0xA64CCB73L,6L},{0xC9EDEB05L,0xC9EDEB05L,0x17A1C3FCL,0x6C4837A1L,(-1L),0x17A1C3FCL,(-1L)},{0x6C4837A1L,6L,6L,0x6C4837A1L,0xA64CCB73L,0xB61C3802L,0x6C4837A1L}};
    uint64_t * const l_675[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int16_t l_686 = (-1L);
    uint8_t l_695 = 255UL;
    int16_t l_698 = 0xF763L;
    uint16_t ****l_755 = &g_445;
    const uint16_t *l_769 = &g_113;
    const uint16_t **l_768 = &l_769;
    const uint16_t ***l_767 = &l_768;
    const uint16_t ****l_766 = &l_767;
    float l_773 = 0x1.2p-1;
    int32_t l_776 = (-6L);
    float l_777 = 0x2.6EB143p-5;
    int16_t l_778 = 1L;
    uint8_t *l_819 = &l_695;
    uint8_t l_820 = 0xF1L;
    uint16_t *l_821[3][6][7] = {{{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0},{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0},{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0}},{{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0},{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0},{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0}},{{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0},{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0},{&g_47,(void*)0,(void*)0,(void*)0,&g_47,(void*)0,&g_47},{&g_113,&g_373,(void*)0,&g_373,&g_113,(void*)0,(void*)0}}};
    int8_t *l_822 = &g_735;
    int64_t *l_823 = &g_197;
    float l_824[2][3][9] = {{{0x7.4p+1,0x4.42079Ap+8,0xB.B2FAA8p-57,0x4.42079Ap+8,0x7.4p+1,0x7.4p+1,0x4.42079Ap+8,0xB.B2FAA8p-57,0x4.42079Ap+8},{0x4.42079Ap+8,0x3.6p-1,0xB.B2FAA8p-57,0x3.6p-1,0x7.4p+1,(-0x3.3p+1),0x7.4p+1,0x3.6p-1,0x3.6p-1},{0x4.42079Ap+8,0x4.42079Ap+8,(-0x3.3p+1),0x3.6p-1,(-0x3.3p+1),0x4.42079Ap+8,0x4.42079Ap+8,(-0x3.3p+1),0x3.6p-1}},{{0xB.B2FAA8p-57,0x7.4p+1,0xB.B2FAA8p-57,(-0x3.3p+1),(-0x3.3p+1),0xB.B2FAA8p-57,0x7.4p+1,0xB.B2FAA8p-57,(-0x3.3p+1)},{0xB.B2FAA8p-57,(-0x3.3p+1),(-0x3.3p+1),0xB.B2FAA8p-57,0x7.4p+1,0xB.B2FAA8p-57,(-0x3.3p+1),(-0x3.3p+1),0xB.B2FAA8p-57},{0x4.42079Ap+8,(-0x3.3p+1),0x3.6p-1,(-0x3.3p+1),0x4.42079Ap+8,0x4.42079Ap+8,(-0x3.3p+1),0x3.6p-1,(-0x3.3p+1)}}};
    float **l_825 = &g_579;
    uint8_t l_868 = 0x7BL;
    int32_t ** const * const l_1060 = &g_1035[0][7][1];
    uint16_t *****l_1198 = (void*)0;
    uint32_t l_1273 = 0xB64D849DL;
    const int32_t *l_1323 = (void*)0;
    const int32_t **l_1322 = &l_1323;
    const int32_t ***l_1321 = &l_1322;
    int32_t ***l_1327 = &g_1035[2][4][1];
    uint16_t l_1353 = 3UL;
    int32_t l_1387 = 8L;
    uint64_t l_1427 = 2UL;
    uint64_t l_1445 = 1UL;
    uint16_t l_1446[6][2][5] = {{{0UL,0UL,65535UL,0UL,0UL},{4UL,0UL,4UL,4UL,0UL}},{{0UL,4UL,4UL,0UL,4UL},{0UL,0UL,65535UL,0UL,0UL}},{{4UL,0UL,4UL,4UL,0UL},{0UL,4UL,4UL,0UL,4UL}},{{0UL,0UL,65535UL,0UL,0UL},{4UL,0UL,4UL,4UL,0UL}},{{0UL,4UL,4UL,0UL,4UL},{0UL,0UL,65535UL,0UL,0UL}},{{4UL,0UL,4UL,4UL,0UL},{0UL,4UL,4UL,0UL,4UL}}};
    uint32_t l_1535 = 0xBFCDECE5L;
    uint32_t l_1548[7];
    int8_t **l_1627 = (void*)0;
    int8_t *** const l_1626 = &l_1627;
    uint64_t l_1660[8][2][3] = {{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}},{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}},{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}},{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}},{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}},{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}},{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}},{{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL},{0xA3601DD93F7147D4LL,0xA3601DD93F7147D4LL,0x9B81813C437900B0LL}}};
    uint64_t l_1685 = 0x75DF57E3E6B272B8LL;
    int16_t l_1687 = 0x581AL;
    int64_t l_1697 = (-3L);
    uint8_t l_1699 = 0xE2L;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1548[i] = 1UL;
    for (g_104 = 0; g_104 < 4; g_104 += 1)
    {
        for (g_47 = 0; g_47 < 8; g_47 += 1)
        {
            for (g_40 = 0; g_40 < 1; g_40 += 1)
            {
                g_167[g_104][g_47][g_40] = &g_168;
            }
        }
    }
    return p_42;
}


/* ------------------------------------------ */
/* 
 * reads : g_66 g_40 g_74 g_80 g_88 g_89
 * writes: g_88 g_89 g_80
 */
static int64_t  func_57(int64_t  p_58, uint16_t * const  p_59, int64_t  p_60)
{ /* block id: 22 */
    int32_t l_71[5] = {0x84D8BC89L,0x84D8BC89L,0x84D8BC89L,0x84D8BC89L,0x84D8BC89L};
    int32_t * const *l_76 = &g_75;
    int32_t * const **l_77[8];
    int32_t * const l_79 = &g_80[3][5][4];
    int32_t * const *l_78 = &l_79;
    int32_t *l_87 = &g_88;
    uint32_t l_90 = 18446744073709551614UL;
    uint8_t l_171 = 0UL;
    uint64_t *l_186[6] = {&g_156,&g_156,&g_156,&g_156,&g_156,&g_156};
    int32_t *l_189 = (void*)0;
    int64_t l_229 = 0x71473C660A7921E5LL;
    const int32_t l_279 = 0xC27CB549L;
    uint8_t l_352 = 255UL;
    int32_t l_361 = (-8L);
    int32_t *l_379 = &g_40;
    uint64_t l_471 = 0x4072D3E19B219649LL;
    uint32_t l_479 = 0x9F29BF2DL;
    uint64_t l_531 = 18446744073709551607UL;
    float l_590 = 0x7.Ep+1;
    int i;
    for (i = 0; i < 8; i++)
        l_77[i] = &l_76;
    g_89 |= (safe_add_func_uint16_t_u_u((*p_59), ((safe_sub_func_int64_t_s_s(((((*l_87) &= ((l_71[1] <= p_58) != (((safe_add_func_uint32_t_u_u(((g_40 , g_74[2][9]) != (l_78 = l_76)), ((0x55L >= g_40) < (safe_rshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s((safe_lshift_func_uint8_t_u_s(1UL, 6)), (*l_79))), (*p_59)))))) && 8L) || g_40))) , &g_75) == &g_75), 0x101DDD0F98E72CC6LL)) , p_58)));
    (*l_79) ^= 1L;
    return p_60;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_20, "g_20", print_hash_value);
    transparent_crc(g_40, "g_40", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_66, "g_66", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_80[i][j][k], "g_80[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_88, "g_88", print_hash_value);
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc(g_113, "g_113", print_hash_value);
    transparent_crc(g_126, "g_126", print_hash_value);
    transparent_crc_bytes (&g_141, sizeof(g_141), "g_141", print_hash_value);
    transparent_crc(g_153, "g_153", print_hash_value);
    transparent_crc(g_156, "g_156", print_hash_value);
    transparent_crc(g_162, "g_162", print_hash_value);
    transparent_crc(g_185, "g_185", print_hash_value);
    transparent_crc(g_197, "g_197", print_hash_value);
    transparent_crc(g_199, "g_199", print_hash_value);
    transparent_crc(g_308, "g_308", print_hash_value);
    transparent_crc(g_373, "g_373", print_hash_value);
    transparent_crc_bytes (&g_462, sizeof(g_462), "g_462", print_hash_value);
    transparent_crc(g_571, "g_571", print_hash_value);
    transparent_crc(g_735, "g_735", print_hash_value);
    transparent_crc(g_754, "g_754", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc_bytes(&g_834[i][j], sizeof(g_834[i][j]), "g_834[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_853, "g_853", print_hash_value);
    transparent_crc(g_855, "g_855", print_hash_value);
    transparent_crc(g_1087, "g_1087", print_hash_value);
    transparent_crc(g_1396, "g_1396", print_hash_value);
    transparent_crc(g_1506, "g_1506", print_hash_value);
    transparent_crc(g_1601, "g_1601", print_hash_value);
    transparent_crc(g_1696, "g_1696", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1707[i], "g_1707[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1809, "g_1809", print_hash_value);
    transparent_crc(g_1853, "g_1853", print_hash_value);
    transparent_crc(g_1902, "g_1902", print_hash_value);
    transparent_crc(g_1945, "g_1945", print_hash_value);
    transparent_crc(g_2003, "g_2003", print_hash_value);
    transparent_crc(g_2009, "g_2009", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2011[i][j], "g_2011[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2396, "g_2396", print_hash_value);
    transparent_crc(g_2521, "g_2521", print_hash_value);
    transparent_crc(g_2561, "g_2561", print_hash_value);
    transparent_crc_bytes (&g_2586, sizeof(g_2586), "g_2586", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2596[i], "g_2596[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2632, "g_2632", print_hash_value);
    transparent_crc(g_2835, "g_2835", print_hash_value);
    transparent_crc_bytes (&g_2836, sizeof(g_2836), "g_2836", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2837[i], "g_2837[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_2935[i][j][k], "g_2935[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2953, "g_2953", print_hash_value);
    transparent_crc_bytes (&g_2976, sizeof(g_2976), "g_2976", print_hash_value);
    transparent_crc(g_3314, "g_3314", print_hash_value);
    transparent_crc_bytes (&g_3408, sizeof(g_3408), "g_3408", print_hash_value);
    transparent_crc(g_3517, "g_3517", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 958
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 205
   depth: 2, occurrence: 37
   depth: 3, occurrence: 6
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 3
   depth: 14, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 1
   depth: 18, occurrence: 2
   depth: 20, occurrence: 2
   depth: 21, occurrence: 4
   depth: 22, occurrence: 1
   depth: 23, occurrence: 3
   depth: 25, occurrence: 1
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 4
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 32, occurrence: 2
   depth: 33, occurrence: 2
   depth: 37, occurrence: 1
   depth: 39, occurrence: 1
   depth: 41, occurrence: 2

XXX total number of pointers: 656

XXX times a variable address is taken: 1602
XXX times a pointer is dereferenced on RHS: 694
breakdown:
   depth: 1, occurrence: 542
   depth: 2, occurrence: 85
   depth: 3, occurrence: 49
   depth: 4, occurrence: 15
   depth: 5, occurrence: 3
XXX times a pointer is dereferenced on LHS: 509
breakdown:
   depth: 1, occurrence: 428
   depth: 2, occurrence: 54
   depth: 3, occurrence: 18
   depth: 4, occurrence: 6
   depth: 5, occurrence: 3
XXX times a pointer is compared with null: 78
XXX times a pointer is compared with address of another variable: 21
XXX times a pointer is compared with another pointer: 16
XXX times a pointer is qualified to be dereferenced: 14944

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2607
   level: 2, occurrence: 788
   level: 3, occurrence: 332
   level: 4, occurrence: 184
   level: 5, occurrence: 46
XXX number of pointers point to pointers: 360
XXX number of pointers point to scalars: 296
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.9
XXX average alias set size: 1.45

XXX times a non-volatile is read: 3348
XXX times a non-volatile is write: 1547
XXX times a volatile is read: 131
XXX    times read thru a pointer: 33
XXX times a volatile is write: 34
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 2.87e+03
XXX percentage of non-volatile access: 96.7

XXX forward jumps: 5
XXX backward jumps: 20

XXX stmts: 194
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 15
   depth: 2, occurrence: 25
   depth: 3, occurrence: 27
   depth: 4, occurrence: 36
   depth: 5, occurrence: 63

XXX percentage a fresh-made variable is used: 14.8
XXX percentage an existing variable is used: 85.2
********************* end of statistics **********************/


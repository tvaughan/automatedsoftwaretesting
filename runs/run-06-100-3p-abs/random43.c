/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      140607685
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint64_t g_12 = 0x27D2703D9B950315LL;
static uint16_t g_39[10][3][7] = {{{0x535BL,65534UL,0xF850L,1UL,65528UL,0UL,65528UL},{65535UL,65532UL,65532UL,65535UL,65535UL,0UL,1UL},{0x9AEAL,2UL,3UL,1UL,0xAC8EL,0x9A27L,0x854CL}},{{65528UL,65532UL,65535UL,0x506EL,0x9D69L,0x839BL,1UL},{65528UL,65534UL,65528UL,0x535BL,0xAC8EL,4UL,65528UL},{0x506EL,65535UL,65535UL,0x7C52L,65535UL,65528UL,65535UL}},{{65528UL,3UL,3UL,65528UL,0UL,0x82F7L,3UL},{65532UL,65528UL,0UL,65535UL,6UL,0x0F4FL,0xDC7EL},{3UL,4UL,0x64F2L,65528UL,0xB1AAL,1UL,3UL}},{{65535UL,0x839BL,0xBADBL,0xBC6AL,6UL,0UL,65533UL},{65528UL,0x9A27L,0x64F2L,65534UL,0UL,0x52B9L,0UL},{65535UL,0UL,0UL,65535UL,65533UL,0UL,6UL}},{{3UL,0UL,4UL,65534UL,3UL,1UL,0xB1AAL},{65532UL,0UL,0x4ED2L,0xBC6AL,0xDC7EL,0x0F4FL,6UL},{0xF850L,0x9A27L,0xC752L,65528UL,3UL,0x82F7L,0UL}},{{0xBC6AL,0x839BL,0x4ED2L,65535UL,65533UL,9UL,65533UL},{0xF850L,4UL,4UL,0xF850L,0UL,0x82F7L,3UL},{65532UL,65528UL,0UL,65535UL,6UL,0x0F4FL,0xDC7EL}},{{3UL,4UL,0x64F2L,65528UL,0xB1AAL,1UL,3UL},{65535UL,0x839BL,0xBADBL,0xBC6AL,6UL,0UL,65533UL},{65528UL,0x9A27L,0x64F2L,65534UL,0UL,0x52B9L,0UL}},{{65535UL,0UL,0UL,65535UL,65533UL,0UL,6UL},{3UL,0UL,4UL,65534UL,3UL,1UL,0xB1AAL},{65532UL,0UL,0x4ED2L,0xBC6AL,0xDC7EL,0x0F4FL,6UL}},{{0xF850L,0x9A27L,0xC752L,65528UL,3UL,0x82F7L,0UL},{0xBC6AL,0x839BL,0x4ED2L,65535UL,65533UL,9UL,65533UL},{0xF850L,4UL,4UL,0xF850L,0UL,0x82F7L,3UL}},{{65532UL,65528UL,0UL,65535UL,6UL,0x0F4FL,0xDC7EL},{3UL,4UL,0x64F2L,65528UL,0xB1AAL,1UL,3UL},{65535UL,0x839BL,0xBADBL,0xBC6AL,6UL,0UL,65533UL}}};
static uint32_t g_44 = 0x7B34174AL;
static volatile uint32_t g_67[6] = {0x66354922L,0x66354922L,0x66354922L,0x66354922L,0x66354922L,0x66354922L};
static volatile uint32_t *g_66[8] = {&g_67[5],&g_67[5],&g_67[5],&g_67[5],&g_67[5],&g_67[5],&g_67[5],&g_67[5]};
static uint16_t g_70 = 1UL;
static uint32_t g_73[9][3] = {{0xDFF541EDL,4294967295UL,0xDFF541EDL},{0UL,8UL,4294967293UL},{4294967290UL,7UL,4294967293UL},{0x339AA88DL,4294967290UL,0xDFF541EDL},{0xEAA3CE9DL,4294967295UL,0xEAA3CE9DL},{4294967293UL,0xEAA3CE9DL,4294967295UL},{4294967295UL,0xEAA3CE9DL,4294967293UL},{0xEAA3CE9DL,4294967295UL,4294967295UL},{8UL,4294967295UL,4294967293UL}};
static uint32_t *g_72 = &g_73[2][1];
static uint64_t g_79 = 0x6F4112691D145C15LL;
static float g_85 = 0x3.1D611Cp+30;
static uint32_t g_94 = 4294967287UL;
static uint32_t g_97 = 9UL;
static uint32_t * const g_96 = &g_97;
static uint32_t * const *g_95 = &g_96;
static float g_100 = (-0x1.3p-1);
static int32_t g_132 = 1L;
static int64_t g_189[4] = {0x0F154065CC1FFD37LL,0x0F154065CC1FFD37LL,0x0F154065CC1FFD37LL,0x0F154065CC1FFD37LL};
static uint32_t g_190 = 0xDA3525E9L;
static uint8_t g_216[2][3][8] = {{{251UL,0xB6L,0x3DL,0x3DL,0xB6L,251UL,0xB6L,0x3DL},{0x61L,0xB6L,0x61L,251UL,251UL,0x61L,0xB6L,0x61L},{0x4FL,251UL,0x3DL,251UL,0x4FL,0x4FL,251UL,0x3DL}},{{0x4FL,0x4FL,251UL,0x3DL,251UL,0x4FL,0x4FL,251UL},{0x61L,251UL,251UL,0x61L,0xB6L,0x61L,251UL,251UL},{251UL,0xB6L,0x3DL,0x3DL,0xB6L,251UL,0xB6L,0x3DL}}};
static const uint16_t g_223 = 0x48B3L;
static uint64_t g_236[5][9][5] = {{{0x38AD2CF8168FEBB8LL,0x051AD0993023009ELL,0x03E18173DE08B522LL,0x4F7079DBC9DE4289LL,0x383C10CCC691E9A4LL},{0x700C1A7E2D6D25A9LL,0xC511B62D161A44FBLL,0xC511B62D161A44FBLL,0x700C1A7E2D6D25A9LL,0x3BB4AA2B9E0A366ALL},{7UL,0x4F7079DBC9DE4289LL,1UL,1UL,0xE9AA08B820AD96D9LL},{0x3BB4AA2B9E0A366ALL,0xFF6E787A2B99ADBDLL,0UL,5UL,18446744073709551614UL},{0x7EC828289208D764LL,0x051AD0993023009ELL,0x7EC828289208D764LL,1UL,0x03E18173DE08B522LL},{0UL,18446744073709551614UL,5UL,0x700C1A7E2D6D25A9LL,0UL},{18446744073709551611UL,3UL,0xECE0AAE0DD45D5CCLL,0x4F7079DBC9DE4289LL,0xECE0AAE0DD45D5CCLL},{0x3BB4AA2B9E0A366ALL,0x3BB4AA2B9E0A366ALL,5UL,0UL,0x57ECFBF858BC3E6DLL},{0x6B115645DE02A35BLL,18446744073709551615UL,0x7EC828289208D764LL,3UL,0x38AD2CF8168FEBB8LL}},{{0x700C1A7E2D6D25A9LL,0UL,0UL,0UL,0UL},{0xECE0AAE0DD45D5CCLL,18446744073709551615UL,1UL,0x9A6307D7E8CB1020LL,0xA4E5EF4969E89804LL},{0xFF6E787A2B99ADBDLL,0x3BB4AA2B9E0A366ALL,0xC511B62D161A44FBLL,0UL,18446744073709551614UL},{0xE97096A69D1A7C04LL,3UL,0x03E18173DE08B522LL,3UL,0xE97096A69D1A7C04LL},{0xFF6E787A2B99ADBDLL,18446744073709551614UL,0xCC302F6452D96C67LL,0UL,0x3BB4AA2B9E0A366ALL},{0xECE0AAE0DD45D5CCLL,0x051AD0993023009ELL,0xA4E5EF4969E89804LL,0x4F7079DBC9DE4289LL,0x1553874B77E74460LL},{0x700C1A7E2D6D25A9LL,0xFF6E787A2B99ADBDLL,0xC511B62D161A44FBLL,18446744073709551614UL,0x3BB4AA2B9E0A366ALL},{0x6B115645DE02A35BLL,0x4F7079DBC9DE4289LL,1UL,1UL,0xE97096A69D1A7C04LL},{0x3BB4AA2B9E0A366ALL,0xC511B62D161A44FBLL,0UL,18446744073709551614UL,18446744073709551614UL}},{{18446744073709551611UL,0x051AD0993023009ELL,18446744073709551611UL,1UL,0xA4E5EF4969E89804LL},{0UL,0x700C1A7E2D6D25A9LL,5UL,18446744073709551614UL,0UL},{0x7EC828289208D764LL,3UL,0x38AD2CF8168FEBB8LL,0x4F7079DBC9DE4289LL,0x38AD2CF8168FEBB8LL},{0x3BB4AA2B9E0A366ALL,0xCC302F6452D96C67LL,5UL,0UL,0x57ECFBF858BC3E6DLL},{7UL,18446744073709551615UL,18446744073709551611UL,3UL,0xECE0AAE0DD45D5CCLL},{0x700C1A7E2D6D25A9LL,0UL,0UL,0UL,0UL},{0x38AD2CF8168FEBB8LL,18446744073709551615UL,1UL,0x9A6307D7E8CB1020LL,0x38AD2CF8168FEBB8LL},{0UL,0xA7F255F038DD4518LL,0UL,18446744073709551614UL,0x3BB4AA2B9E0A366ALL},{1UL,18446744073709551615UL,0xECE0AAE0DD45D5CCLL,18446744073709551615UL,1UL}},{{0UL,0xFF6E787A2B99ADBDLL,0xA7F255F038DD4518LL,0UL,0x57ECFBF858BC3E6DLL},{0x383C10CCC691E9A4LL,0x39DA6AA3D2066672LL,0x38AD2CF8168FEBB8LL,1UL,0x6B115645DE02A35BLL},{0xFF6E787A2B99ADBDLL,0UL,0UL,0xFF6E787A2B99ADBDLL,0x57ECFBF858BC3E6DLL},{18446744073709551611UL,1UL,0xA4E5EF4969E89804LL,0x9A6307D7E8CB1020LL,1UL},{0x57ECFBF858BC3E6DLL,0UL,5UL,0xCC302F6452D96C67LL,0x3BB4AA2B9E0A366ALL},{0xE97096A69D1A7C04LL,0x39DA6AA3D2066672LL,0xE97096A69D1A7C04LL,0x9A6307D7E8CB1020LL,0x38AD2CF8168FEBB8LL},{0UL,0xC511B62D161A44FBLL,0xCC302F6452D96C67LL,0xFF6E787A2B99ADBDLL,0UL},{0xE9AA08B820AD96D9LL,18446744073709551615UL,0x1553874B77E74460LL,1UL,0x1553874B77E74460LL},{0x57ECFBF858BC3E6DLL,0x57ECFBF858BC3E6DLL,0xCC302F6452D96C67LL,0UL,0x700C1A7E2D6D25A9LL}},{{0x7EC828289208D764LL,0x4F7079DBC9DE4289LL,0xE97096A69D1A7C04LL,18446744073709551615UL,0x383C10CCC691E9A4LL},{0xFF6E787A2B99ADBDLL,0UL,5UL,18446744073709551614UL,0UL},{0x1553874B77E74460LL,0x4F7079DBC9DE4289LL,0xA4E5EF4969E89804LL,0x051AD0993023009ELL,0xECE0AAE0DD45D5CCLL},{0UL,0x57ECFBF858BC3E6DLL,0UL,5UL,0x3BB4AA2B9E0A366ALL},{1UL,18446744073709551615UL,0x38AD2CF8168FEBB8LL,18446744073709551615UL,1UL},{0UL,0xC511B62D161A44FBLL,0xA7F255F038DD4518LL,0UL,0x57ECFBF858BC3E6DLL},{0x1553874B77E74460LL,0x39DA6AA3D2066672LL,0xECE0AAE0DD45D5CCLL,1UL,7UL},{0xFF6E787A2B99ADBDLL,0UL,0UL,0xC511B62D161A44FBLL,0x57ECFBF858BC3E6DLL},{0x7EC828289208D764LL,1UL,0x03E18173DE08B522LL,0x9A6307D7E8CB1020LL,1UL}}};
static int16_t g_249 = (-7L);
static uint64_t g_252 = 18446744073709551610UL;
static uint64_t g_268[2] = {0x8B5EEB1FCE9C3896LL,0x8B5EEB1FCE9C3896LL};
static const volatile uint64_t g_272 = 0UL;/* VOLATILE GLOBAL g_272 */
static const volatile uint64_t *g_271 = &g_272;
static const volatile uint64_t ** volatile g_270 = &g_271;/* VOLATILE GLOBAL g_270 */
static const volatile uint64_t ** volatile * volatile g_273 = (void*)0;/* VOLATILE GLOBAL g_273 */
static volatile int32_t g_279 = 1L;/* VOLATILE GLOBAL g_279 */
static volatile int32_t g_281[9][4] = {{0x22C59ECBL,0x114316DEL,0x114316DEL,0x22C59ECBL},{0x6784C590L,0x14190088L,0x114316DEL,0x2B007968L},{0x22C59ECBL,0x85D11EB6L,(-10L),0x85D11EB6L},{0x85D11EB6L,0x114316DEL,0x6784C590L,0x85D11EB6L},{0x6784C590L,0x85D11EB6L,0x2B007968L,0x2B007968L},{0x14190088L,0x14190088L,(-10L),0x22C59ECBL},{0x14190088L,0x114316DEL,0x2B007968L,0x14190088L},{0x6784C590L,0x22C59ECBL,0x6784C590L,0x2B007968L},{0x85D11EB6L,0x22C59ECBL,(-10L),0x14190088L}};
static int32_t g_283[2][3][10] = {{{(-5L),0x73A3FC10L,0x73A3FC10L,(-5L),3L,0xA031FE69L,(-5L),0xA031FE69L,3L,(-5L)},{0xA031FE69L,(-5L),0xA031FE69L,3L,(-5L),0x73A3FC10L,0x73A3FC10L,(-5L),3L,0xA031FE69L},{0xF1A96DB8L,0xF1A96DB8L,(-6L),(-5L),(-1L),(-6L),(-1L),(-5L),(-6L),0xF1A96DB8L}},{{(-1L),0x73A3FC10L,0xA031FE69L,(-1L),3L,3L,(-1L),0xA031FE69L,0x73A3FC10L,(-1L)},{0xA031FE69L,0xF1A96DB8L,0x73A3FC10L,3L,0xF1A96DB8L,3L,0x73A3FC10L,0xF1A96DB8L,0xA031FE69L,0xA031FE69L},{(-1L),(-5L),0xF1A96DB8L,0xA031FE69L,0xA031FE69L,0xF1A96DB8L,0x73A3FC10L,3L,0xF1A96DB8L,3L}}};
static volatile int64_t g_291 = 0x3FC6AC2D5D75FF6FLL;/* VOLATILE GLOBAL g_291 */
static int32_t g_293 = 0x54772FBBL;
static int8_t g_294 = 0xC2L;
static int32_t *g_311 = (void*)0;
static int32_t ** volatile g_313 = &g_311;/* VOLATILE GLOBAL g_313 */
static uint32_t ***g_329 = (void*)0;
static volatile uint64_t g_334 = 18446744073709551614UL;/* VOLATILE GLOBAL g_334 */
static volatile uint64_t *g_333 = &g_334;
static volatile uint64_t * volatile * volatile g_332 = &g_333;/* VOLATILE GLOBAL g_332 */
static volatile uint64_t * volatile * volatile *g_331 = &g_332;
static uint16_t *g_560 = (void*)0;
static int32_t * volatile g_571 = &g_283[1][2][8];/* VOLATILE GLOBAL g_571 */
static uint16_t **g_582 = &g_560;
static int32_t ** volatile g_653 = (void*)0;/* VOLATILE GLOBAL g_653 */
static int32_t ** volatile g_654 = &g_311;/* VOLATILE GLOBAL g_654 */
static uint64_t g_659[2] = {0xA0429D6158D1C4D1LL,0xA0429D6158D1C4D1LL};
static float *g_666 = &g_100;
static float * volatile *g_665 = &g_666;
static uint8_t g_691 = 0x9EL;
static int32_t g_785 = 1L;
static float g_788[10] = {0x1.7p+1,(-0x1.5p+1),0x1.7p+1,0x5.8p-1,0x5.8p-1,0x1.7p+1,(-0x1.5p+1),0x1.7p+1,0x5.8p-1,0x5.8p-1};
static int32_t ** volatile g_839[3][6][3] = {{{&g_311,&g_311,(void*)0},{&g_311,(void*)0,&g_311},{&g_311,&g_311,(void*)0},{(void*)0,&g_311,&g_311},{&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311}},{{(void*)0,&g_311,&g_311},{&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311},{&g_311,&g_311,&g_311},{(void*)0,&g_311,&g_311}},{{&g_311,&g_311,&g_311},{&g_311,(void*)0,&g_311},{&g_311,&g_311,&g_311},{(void*)0,(void*)0,&g_311},{&g_311,(void*)0,&g_311},{&g_311,&g_311,(void*)0}}};
static float g_887 = (-0x4.Cp-1);
static uint16_t g_910 = 0x42C0L;
static volatile uint32_t **g_928 = &g_66[3];
static uint32_t *g_975 = &g_190;
static uint32_t * volatile *g_974 = &g_975;
static uint32_t * volatile **g_973 = &g_974;
static float * const  volatile g_1097 = &g_85;/* VOLATILE GLOBAL g_1097 */
static uint32_t ****g_1108 = &g_329;
static uint32_t *****g_1107 = &g_1108;
static int8_t *g_1137 = (void*)0;
static int32_t ** volatile g_1171 = &g_311;/* VOLATILE GLOBAL g_1171 */
static uint64_t *g_1176 = &g_79;
static uint64_t * const *g_1175 = &g_1176;
static int32_t ** const  volatile g_1191 = &g_311;/* VOLATILE GLOBAL g_1191 */
static int32_t ** const  volatile g_1217 = &g_311;/* VOLATILE GLOBAL g_1217 */
static int32_t ** volatile g_1220 = &g_311;/* VOLATILE GLOBAL g_1220 */
static uint64_t **g_1247 = &g_1176;
static const int64_t g_1285 = (-1L);
static const int64_t *g_1284 = &g_1285;
static int32_t * volatile g_1318 = &g_132;/* VOLATILE GLOBAL g_1318 */
static int16_t g_1339 = 0xF320L;
static uint16_t g_1363[4][2] = {{0x4CE6L,3UL},{0x4CE6L,0x4CE6L},{3UL,0x4CE6L},{0x4CE6L,3UL}};
static uint16_t g_1366 = 0x51EEL;
static uint16_t g_1369[8] = {0x1596L,0x1596L,0x1596L,0x1596L,0x1596L,0x1596L,0x1596L,0x1596L};
static uint16_t g_1370 = 1UL;
static uint16_t g_1371 = 65534UL;
static uint16_t g_1372 = 0UL;
static uint16_t g_1373 = 0x9126L;
static uint16_t g_1374[2][5] = {{0x6A43L,0x6A43L,0x6A43L,0x6A43L,0x6A43L},{0x6AEDL,0x9D0EL,0x6AEDL,0x9D0EL,0x6AEDL}};
static uint16_t g_1375[6][9] = {{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL},{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL},{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL},{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL},{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL},{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL}};
static uint16_t g_1376 = 0x9CA1L;
static uint16_t * volatile * volatile g_1422[7] = {&g_560,&g_560,&g_560,&g_560,&g_560,&g_560,&g_560};
static uint16_t * volatile * volatile *g_1421[7] = {&g_1422[3],&g_1422[3],&g_1422[3],&g_1422[3],&g_1422[3],&g_1422[3],&g_1422[3]};
static uint16_t * volatile * volatile **g_1420 = &g_1421[2];
static int8_t g_1424 = 0xBCL;
static int64_t *g_1494 = &g_189[0];
static int64_t **g_1493 = &g_1494;
static int64_t *** const  volatile g_1492 = &g_1493;/* VOLATILE GLOBAL g_1492 */
static int8_t **g_1527 = &g_1137;
static int32_t * volatile g_1528 = &g_283[0][1][8];/* VOLATILE GLOBAL g_1528 */
static const uint16_t g_1572 = 0xB028L;
static uint32_t g_1636 = 8UL;
static const int32_t *g_1640 = &g_283[1][1][1];
static int32_t * volatile g_1662 = &g_293;/* VOLATILE GLOBAL g_1662 */
static int32_t * volatile g_1668 = (void*)0;/* VOLATILE GLOBAL g_1668 */
static uint32_t **g_1682[9][8][3] = {{{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,(void*)0},{(void*)0,&g_975,&g_975},{&g_975,&g_975,(void*)0},{&g_975,&g_975,&g_975},{&g_975,&g_975,(void*)0}},{{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,(void*)0},{&g_975,(void*)0,&g_975},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975}},{{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{(void*)0,&g_975,&g_975},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{(void*)0,&g_975,&g_975}},{{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,(void*)0},{(void*)0,&g_975,&g_975},{(void*)0,&g_975,(void*)0},{&g_975,&g_975,&g_975},{&g_975,(void*)0,(void*)0},{&g_975,&g_975,&g_975}},{{&g_975,&g_975,(void*)0},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,(void*)0,&g_975},{&g_975,&g_975,&g_975},{&g_975,(void*)0,&g_975},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975}},{{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{(void*)0,&g_975,&g_975},{&g_975,(void*)0,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,(void*)0}},{{(void*)0,(void*)0,&g_975},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,(void*)0},{&g_975,&g_975,&g_975},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,(void*)0,&g_975}},{{&g_975,&g_975,&g_975},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,(void*)0},{&g_975,(void*)0,&g_975},{(void*)0,&g_975,&g_975}},{{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,&g_975,&g_975},{(void*)0,&g_975,&g_975},{(void*)0,&g_975,&g_975},{&g_975,&g_975,&g_975},{&g_975,(void*)0,(void*)0}}};
static int32_t * const * const g_1693 = &g_311;
static int32_t * const * const *g_1692 = &g_1693;
static uint16_t ***g_1749 = &g_582;
static uint16_t ****g_1748 = &g_1749;
static uint16_t *****g_1747 = &g_1748;
static volatile uint8_t **g_1785 = (void*)0;
static volatile uint8_t ***g_1784 = &g_1785;
static int32_t ** volatile g_1831 = &g_311;/* VOLATILE GLOBAL g_1831 */
static uint16_t **g_1844[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t *g_1876 = (void*)0;
static uint8_t **g_1875 = &g_1876;
static uint8_t *** volatile g_1874 = &g_1875;/* VOLATILE GLOBAL g_1874 */
static uint16_t ***g_1931 = &g_1844[0];
static int16_t *g_1934 = &g_249;
static int16_t **g_1933 = &g_1934;
static volatile int32_t g_1966 = (-1L);/* VOLATILE GLOBAL g_1966 */
static volatile int32_t *g_1965[8] = {&g_1966,&g_1966,&g_1966,&g_1966,&g_1966,&g_1966,&g_1966,&g_1966};
static volatile int32_t **g_1964 = &g_1965[3];
static volatile int32_t *g_1969 = &g_281[7][0];
static float g_2002 = (-0x9.Cp+1);
static int16_t g_2082 = 1L;
static int32_t ** volatile g_2175 = &g_311;/* VOLATILE GLOBAL g_2175 */
static int16_t g_2232 = 0L;
static int64_t g_2282 = 8L;
static int16_t g_2289 = 0L;
static volatile uint32_t g_2296 = 0x45740F15L;/* VOLATILE GLOBAL g_2296 */
static int32_t ** volatile g_2304[7] = {&g_311,&g_311,&g_311,&g_311,&g_311,&g_311,&g_311};
static int32_t ** const  volatile g_2305 = &g_311;/* VOLATILE GLOBAL g_2305 */
static const uint16_t *g_2359 = &g_910;
static const uint16_t **g_2358 = &g_2359;
static const uint16_t g_2403 = 0x38A2L;
static int64_t * const **g_2494 = (void*)0;
static const uint32_t g_2609 = 9UL;
static int32_t *g_2627 = &g_132;
static int32_t ** volatile g_2626 = &g_2627;/* VOLATILE GLOBAL g_2626 */
static int32_t g_2634 = 1L;
static volatile float *g_2690 = (void*)0;
static volatile float ** volatile g_2689 = &g_2690;/* VOLATILE GLOBAL g_2689 */
static volatile float ** volatile *g_2688 = &g_2689;
static volatile float ** volatile **g_2687 = &g_2688;
static volatile float ** volatile ** volatile *g_2686 = &g_2687;
static uint64_t ***g_2892 = &g_1247;
static uint64_t *** const *g_2891 = &g_2892;
static uint64_t ****g_2895 = &g_2892;
static uint32_t g_2900 = 0xA29EC7C4L;
static int32_t ** volatile g_2907 = (void*)0;/* VOLATILE GLOBAL g_2907 */
static int32_t ** const  volatile g_2908 = (void*)0;/* VOLATILE GLOBAL g_2908 */
static int32_t *g_2983[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t * const *g_2982[4] = {&g_2983[1],&g_2983[1],&g_2983[1],&g_2983[1]};
static int32_t * const **g_2981 = &g_2982[1];
static int32_t * const **g_2985 = &g_2982[1];
static int32_t ** const  volatile g_2990[2][10] = {{&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627},{&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627,&g_2627}};
static int8_t g_3059 = 1L;
static int64_t ***g_3256 = &g_1493;
static int64_t ****g_3255[10] = {&g_3256,&g_3256,&g_3256,&g_3256,&g_3256,&g_3256,&g_3256,&g_3256,&g_3256,&g_3256};
static int16_t g_3281 = (-4L);
static uint64_t g_3349 = 18446744073709551615UL;
static uint16_t g_3362 = 0UL;
static uint16_t * const g_3361 = &g_3362;
static uint16_t * const *g_3360 = &g_3361;
static float *****g_3384 = (void*)0;
static int64_t *****g_3403[9][6][4] = {{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}},{{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]},{&g_3255[5],&g_3255[8],&g_3255[8],&g_3255[5]}}};


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static uint32_t  func_2(uint8_t  p_3, uint32_t  p_4, uint64_t  p_5, uint64_t  p_6, uint16_t  p_7);
static int16_t  func_10(int8_t  p_11);
static uint64_t  func_13(float  p_14, int16_t  p_15, int64_t  p_16, uint32_t  p_17);
static uint16_t  func_22(int16_t  p_23, int32_t  p_24, uint8_t  p_25, int8_t  p_26);
static int16_t  func_27(uint32_t  p_28);
static int16_t  func_30(uint32_t  p_31);
static int32_t  func_32(uint32_t  p_33, uint32_t  p_34, uint8_t  p_35, int64_t  p_36);
static uint8_t  func_46(uint16_t * p_47, int8_t  p_48, uint64_t  p_49);
static uint16_t * func_50(uint32_t  p_51, uint32_t  p_52, uint32_t * p_53);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_12 g_73 g_1284 g_1285 g_665 g_666 g_100 g_571 g_283 g_95 g_96 g_97 g_1176 g_79 g_910 g_72 g_249 g_1339 g_268 g_132 g_294 g_659 g_67 g_1376 g_252 g_271 g_272 g_293 g_1420 g_1366 g_279 g_216 g_1373 g_691 g_1247 g_236 g_1492 g_1318 g_1528 g_974 g_975 g_190 g_1107 g_44 g_334 g_1421 g_1422 g_560 g_582 g_270 g_1572 g_223 g_1636 g_70 g_1662 g_1371 g_1692 g_1097 g_85 g_311 g_1494 g_189 g_1747 g_1175 g_1874 g_1875 g_1933 g_1934 g_1372 g_1748 g_1749 g_1969 g_2082 g_2175 g_1191 g_2232 g_1693 g_281 g_928 g_66 g_654 g_1363 g_1640 g_1964 g_1965 g_1966 g_785 g_2891 g_2900 g_2282 g_332 g_333 g_2627 g_2634
 * writes: g_132 g_249 g_293 g_1339 g_283 g_294 g_1363 g_268 g_73 g_1424 g_97 g_1366 g_1373 g_691 g_1493 g_1527 g_1108 g_44 g_785 g_94 g_79 g_85 g_95 g_100 g_1640 g_1682 g_1371 g_1692 g_788 g_311 g_1372 g_189 g_1494 g_2082 g_1247 g_281 g_582 g_910 g_190 g_236 g_12 g_1376 g_2891 g_2895 g_2282 g_216 g_2634
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int16_t l_1324 = 0x2371L;
    int32_t *l_1329 = &g_132;
    uint32_t l_1330 = 0xCD214716L;
    float l_2902 = (-0x1.Fp-1);
    float l_2903 = 0x8.Ap+1;
    int32_t *l_2927 = &g_2634;
    uint32_t *l_2942 = &g_190;
    int32_t *l_2955[1];
    int32_t ** const l_2954[2][7] = {{&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0]},{&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0],&l_2955[0]}};
    int32_t l_2957 = 0L;
    const int16_t *l_2978 = &l_1324;
    const int16_t **l_2977 = &l_2978;
    uint32_t ****l_3006 = &g_329;
    int32_t l_3071 = (-1L);
    int32_t l_3075[4] = {(-1L),(-1L),(-1L),(-1L)};
    uint32_t l_3076 = 8UL;
    const int32_t l_3109 = 0x2B8DC2FEL;
    int8_t l_3275 = 0L;
    int64_t **l_3308 = &g_1494;
    uint64_t ****l_3346 = &g_2892;
    uint16_t * const *l_3357 = &g_560;
    uint64_t l_3394 = 0x19CFA8776F00B423LL;
    uint64_t l_3400 = 0xC5A3010C2413C156LL;
    const int32_t *l_3407 = (void*)0;
    uint16_t l_3418 = 0UL;
    float **l_3433 = &g_666;
    int64_t l_3442 = (-1L);
    int i, j;
    for (i = 0; i < 1; i++)
        l_2955[i] = &g_785;
    (*l_2927) ^= (((((func_2(((safe_mod_func_int16_t_s_s(func_10(((*l_1329) = (g_12 ^ func_13(g_12, g_12, (safe_lshift_func_uint8_t_u_s(((safe_div_func_uint16_t_u_u(func_22(func_27(g_12), ((*l_1329) = (safe_rshift_func_int8_t_s_s(l_1324, ((((*g_1284) | (((safe_mod_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((((l_1324 != ((((0x8.16F42Fp-17 > (**g_665)) == (*g_666)) , (*g_571)) || (**g_95))) >= l_1324) != (*g_1176)), 0xC2D3L)), g_73[2][2])) < g_73[0][2]) <= 5UL)) , &g_249) != &g_249)))), g_910, g_910), 0x9B6AL)) & l_1330), 3)), (*g_72))))), l_1324)) , (*l_1329)), l_1324, g_216[1][2][4], l_1324, l_1330) || (*l_1329)) , 0x3165L) | l_1324) & g_70) & l_1330);
    for (g_293 = 0; (g_293 > 3); ++g_293)
    { /* block id: 1257 */
        const uint32_t l_2938 = 0UL;
        uint32_t **l_2941[7] = {&g_975,&g_975,&g_975,&g_975,&g_975,&g_975,&g_975};
        uint64_t *l_2956 = &g_12;
        int32_t *l_2993 = &g_283[1][2][2];
        float l_3007[6][8] = {{0x1.Bp-1,0x4.199AF2p-63,(-0x10.5p+1),0x3.3CD16Ap-48,0x3.3CD16Ap-48,(-0x10.5p+1),0x4.199AF2p-63,0x1.Bp-1},{0x4.199AF2p-63,0x0.B0AEC5p+92,0x1.Bp-1,(-0x9.Bp-1),0x1.Bp-1,0x0.B0AEC5p+92,0x4.199AF2p-63,0x4.199AF2p-63},{0x0.B0AEC5p+92,(-0x9.Bp-1),(-0x10.5p+1),(-0x10.5p+1),(-0x9.Bp-1),0x0.B0AEC5p+92,0x3.3CD16Ap-48,0x0.B0AEC5p+92},{(-0x9.Bp-1),0x0.B0AEC5p+92,0x3.3CD16Ap-48,0x0.B0AEC5p+92,(-0x9.Bp-1),(-0x10.5p+1),(-0x10.5p+1),(-0x9.Bp-1)},{0x0.B0AEC5p+92,0x4.199AF2p-63,0x4.199AF2p-63,0x0.B0AEC5p+92,0x1.Bp-1,(-0x9.Bp-1),0x1.Bp-1,0x0.B0AEC5p+92},{0x4.199AF2p-63,0x1.Bp-1,0x4.199AF2p-63,(-0x10.5p+1),0x3.3CD16Ap-48,0x3.3CD16Ap-48,(-0x10.5p+1),0x4.199AF2p-63}};
        const int32_t l_3025 = 0L;
        uint32_t l_3057[7] = {0xAB8D3CE6L,0x159A02D0L,0xAB8D3CE6L,0xAB8D3CE6L,0x159A02D0L,0xAB8D3CE6L,0xAB8D3CE6L};
        int32_t l_3072 = 0x5C0F8874L;
        int32_t l_3073 = 1L;
        int32_t l_3074 = (-2L);
        int32_t l_3080[2];
        uint32_t l_3081[9][8] = {{0x166400F9L,7UL,0xEAD606B4L,0xEAD606B4L,7UL,0x166400F9L,7UL,0xEAD606B4L},{18446744073709551615UL,7UL,18446744073709551615UL,0x166400F9L,0x166400F9L,18446744073709551615UL,7UL,18446744073709551615UL},{0x1AE4008EL,0x166400F9L,0xEAD606B4L,0x166400F9L,0x1AE4008EL,0x166400F9L,18446744073709551615UL,7UL},{0x166400F9L,0x166400F9L,18446744073709551615UL,7UL,18446744073709551615UL,0x166400F9L,0x166400F9L,18446744073709551615UL},{0xEAD606B4L,18446744073709551615UL,18446744073709551615UL,0xEAD606B4L,0x1AE4008EL,0xEAD606B4L,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,0x1AE4008EL,7UL,7UL,0x1AE4008EL,18446744073709551615UL,0x1AE4008EL,7UL},{0xEAD606B4L,0x1AE4008EL,0xEAD606B4L,18446744073709551615UL,18446744073709551615UL,0xEAD606B4L,0x1AE4008EL,0xEAD606B4L},{0x166400F9L,18446744073709551615UL,7UL,18446744073709551615UL,0x166400F9L,0x166400F9L,18446744073709551615UL,7UL},{0x166400F9L,0x166400F9L,18446744073709551615UL,7UL,18446744073709551615UL,0x166400F9L,0x166400F9L,18446744073709551615UL}};
        uint32_t l_3103[9];
        const int32_t **l_3183 = &g_1640;
        const int32_t ***l_3182 = &l_3183;
        float l_3250[2][2][10];
        uint16_t l_3280[1];
        uint32_t **l_3352 = &g_72;
        uint16_t **l_3363[5][10] = {{&g_560,(void*)0,(void*)0,&g_560,(void*)0,&g_560,(void*)0,(void*)0,&g_560,(void*)0},{&g_560,&g_560,&g_560,&g_560,&g_560,(void*)0,(void*)0,&g_560,&g_560,&g_560},{&g_560,&g_560,(void*)0,&g_560,&g_560,&g_560,&g_560,&g_560,&g_560,&g_560},{&g_560,&g_560,&g_560,&g_560,&g_560,&g_560,&g_560,(void*)0,&g_560,&g_560},{(void*)0,(void*)0,&g_560,&g_560,&g_560,&g_560,&g_560,&g_560,&g_560,&g_560}};
        int32_t l_3395 = 0x5E7813F3L;
        const int32_t *l_3406[1];
        uint64_t l_3430[5][8] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}};
        float ***l_3434 = (void*)0;
        float ***l_3435 = &l_3433;
        float *l_3441[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_3080[i] = 9L;
        for (i = 0; i < 9; i++)
            l_3103[i] = 0UL;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 2; j++)
            {
                for (k = 0; k < 10; k++)
                    l_3250[i][j][k] = 0x2.6BED0Fp-77;
            }
        }
        for (i = 0; i < 1; i++)
            l_3280[i] = 65528UL;
        for (i = 0; i < 1; i++)
            l_3406[i] = &l_2957;
    }
    return (*g_1662);
}


/* ------------------------------------------ */
/* 
 * reads : g_2282 g_96 g_332 g_333 g_334 g_928 g_66 g_67 g_1933 g_1934 g_249 g_1247 g_1176 g_79 g_2627 g_132
 * writes: g_2282 g_97 g_216 g_132
 */
static uint32_t  func_2(uint8_t  p_3, uint32_t  p_4, uint64_t  p_5, uint64_t  p_6, uint16_t  p_7)
{ /* block id: 1243 */
    const int64_t l_2915 = 0x250E069E50F78311LL;
    uint16_t l_2925 = 0x6CF9L;
    int32_t l_2926 = (-1L);
    for (g_2282 = (-23); (g_2282 > (-11)); g_2282 = safe_add_func_int16_t_s_s(g_2282, 6))
    { /* block id: 1246 */
        int32_t *l_2906[6][4] = {{(void*)0,&g_132,(void*)0,&g_283[0][1][1]},{&g_293,&g_132,&g_293,&g_132},{&g_293,&g_283[0][1][1],(void*)0,&g_132},{(void*)0,&g_132,(void*)0,&g_283[0][1][1]},{&g_293,&g_132,&g_293,&g_132},{&g_293,&g_283[0][1][1],(void*)0,&g_132}};
        int32_t **l_2909 = &l_2906[1][2];
        uint8_t *l_2924 = &g_216[1][2][4];
        int i, j;
        (*l_2909) = l_2906[5][0];
        (*g_2627) &= (!((safe_div_func_int16_t_s_s(((safe_rshift_func_int8_t_s_s(((l_2926 = (l_2915 ^ ((safe_sub_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((safe_div_func_int64_t_s_s((safe_div_func_uint8_t_u_u((9L >= ((*g_96) = p_5)), ((p_3 < (l_2915 ^ p_7)) & (**g_332)))), p_4)), ((*l_2924) = l_2915))), p_5)) <= l_2925))) || (**g_928)), 1)) , (-1L)), (**g_1933))) , (**g_1247)));
    }
    return l_2926;
}


/* ------------------------------------------ */
/* 
 * reads : g_1494 g_189 g_1933 g_1934 g_249 g_72 g_73 g_96 g_97 g_1176 g_79 g_1748 g_1749 g_1662 g_691 g_1175 g_560 g_283 g_1528 g_1969 g_1284 g_1285 g_271 g_272 g_2082 g_2175 g_1191 g_311 g_190 g_910 g_236 g_2232 g_1692 g_1693 g_281 g_666 g_928 g_66 g_67 g_654 g_1376 g_1363 g_1640 g_1964 g_1965 g_1966 g_785 g_665 g_100 g_1097 g_2891 g_2900
 * writes: g_189 g_249 g_1494 g_73 g_97 g_79 g_293 g_1339 g_691 g_2082 g_283 g_1247 g_281 g_582 g_1527 g_311 g_910 g_190 g_236 g_1424 g_12 g_100 g_1376 g_785 g_85 g_2891 g_2895
 */
static int16_t  func_10(int8_t  p_11)
{ /* block id: 859 */
    const uint32_t l_2012 = 1UL;
    uint8_t **l_2015 = (void*)0;
    int8_t l_2044[7][5][7] = {{{0xB3L,0L,(-6L),1L,0x90L,0L,0L},{(-1L),1L,0xA0L,1L,0xA1L,0x8FL,(-6L)},{0xFAL,5L,(-5L),0x8FL,0x26L,1L,1L},{0xFAL,0x5EL,1L,0x5EL,0xFAL,0x8FL,(-1L)},{1L,0x1EL,0xA0L,0L,(-6L),0x22L,1L}},{{0x90L,0xFAL,0L,1L,0x8FL,0x3DL,(-6L)},{1L,0L,(-1L),0x26L,1L,(-8L),(-3L)},{0xFAL,1L,0x7DL,0x7DL,1L,0xFAL,0x3DL},{(-3L),1L,0xFAL,1L,0x8FL,(-6L),(-5L)},{1L,0xA1L,0x5EL,(-5L),(-6L),0x8FL,1L}},{{1L,1L,0xA1L,0x3DL,0xFAL,1L,0x7DL},{0x26L,1L,(-8L),(-3L),(-8L),1L,0x26L},{0xB3L,0L,(-3L),(-6L),0x3DL,0x8FL,1L},{0L,0xFAL,(-5L),1L,0x22L,(-6L),0L},{0x8FL,0x1EL,(-3L),(-1L),0x8FL,0xFAL,0x5EL}},{{0x1EL,0x5EL,(-8L),0L,0L,(-8L),0x5EL},{0x22L,(-5L),0xA1L,5L,0x5EL,0x3DL,0L},{(-1L),0x8FL,0x5EL,0xA0L,0x7DL,0x22L,1L},{0x5EL,(-1L),0xFAL,5L,(-5L),0x8FL,0x26L},{0x3DL,1L,0x7DL,0L,5L,0L,0x7DL}},{{0x3DL,0x3DL,(-1L),(-1L),0x90L,0x5EL,1L},{0x5EL,1L,0L,1L,(-1L),0x7DL,(-5L)},{(-1L),(-8L),0xA0L,(-6L),0x90L,(-5L),0x3DL},{0x22L,0L,1L,(-3L),5L,5L,(-3L)},{0x1EL,0L,0x1EL,0x3DL,(-5L),0x90L,(-6L)}},{{0x8FL,(-8L),0x26L,(-5L),0x7DL,(-1L),1L},{0L,1L,5L,1L,0x5EL,0x90L,(-1L)},{0xB3L,0x3DL,1L,0x7DL,0L,5L,0L},{0x26L,1L,1L,0x26L,0x8FL,(-5L),5L},{1L,(-1L),5L,1L,0x22L,0x7DL,0xA0L}},{{1L,(-1L),(-8L),0xA0L,(-6L),0x90L,(-5L)},{0L,0x1EL,1L,0x90L,0xA1L,(-1L),(-1L)},{(-3L),0x90L,0L,0x90L,(-3L),0x5EL,1L},{0L,1L,0x26L,0xA0L,5L,0L,0x8FL},{0x22L,(-3L),0x8FL,0L,(-1L),(-6L),5L}}};
    float ** const *l_2048 = (void*)0;
    float ** const ** const l_2047[9][6][4] = {{{(void*)0,&l_2048,&l_2048,&l_2048},{(void*)0,(void*)0,&l_2048,&l_2048},{(void*)0,(void*)0,&l_2048,&l_2048},{&l_2048,&l_2048,&l_2048,(void*)0},{&l_2048,&l_2048,&l_2048,&l_2048},{&l_2048,&l_2048,&l_2048,&l_2048}},{{(void*)0,&l_2048,&l_2048,(void*)0},{&l_2048,&l_2048,&l_2048,&l_2048},{(void*)0,&l_2048,&l_2048,&l_2048},{&l_2048,(void*)0,&l_2048,&l_2048},{&l_2048,&l_2048,(void*)0,&l_2048},{&l_2048,&l_2048,&l_2048,(void*)0}},{{(void*)0,&l_2048,&l_2048,&l_2048},{(void*)0,&l_2048,(void*)0,&l_2048},{&l_2048,&l_2048,&l_2048,(void*)0},{(void*)0,&l_2048,&l_2048,&l_2048},{&l_2048,(void*)0,(void*)0,&l_2048},{(void*)0,(void*)0,&l_2048,&l_2048}},{{&l_2048,&l_2048,&l_2048,&l_2048},{&l_2048,(void*)0,&l_2048,&l_2048},{&l_2048,(void*)0,&l_2048,(void*)0},{(void*)0,&l_2048,&l_2048,(void*)0},{&l_2048,&l_2048,&l_2048,(void*)0},{(void*)0,&l_2048,&l_2048,(void*)0}},{{&l_2048,&l_2048,&l_2048,&l_2048},{&l_2048,&l_2048,&l_2048,&l_2048},{&l_2048,&l_2048,&l_2048,&l_2048},{(void*)0,&l_2048,(void*)0,&l_2048},{&l_2048,&l_2048,&l_2048,&l_2048},{(void*)0,&l_2048,&l_2048,&l_2048}},{{&l_2048,&l_2048,(void*)0,(void*)0},{(void*)0,&l_2048,&l_2048,&l_2048},{(void*)0,(void*)0,&l_2048,&l_2048},{&l_2048,&l_2048,(void*)0,&l_2048},{&l_2048,(void*)0,&l_2048,(void*)0},{&l_2048,(void*)0,&l_2048,&l_2048}},{{(void*)0,&l_2048,&l_2048,&l_2048},{&l_2048,(void*)0,&l_2048,&l_2048},{(void*)0,&l_2048,&l_2048,(void*)0},{&l_2048,(void*)0,(void*)0,&l_2048},{(void*)0,&l_2048,&l_2048,(void*)0},{&l_2048,&l_2048,&l_2048,&l_2048}},{{(void*)0,&l_2048,(void*)0,(void*)0},{&l_2048,&l_2048,&l_2048,(void*)0},{&l_2048,&l_2048,&l_2048,&l_2048},{&l_2048,(void*)0,(void*)0,&l_2048},{(void*)0,&l_2048,&l_2048,&l_2048},{&l_2048,(void*)0,(void*)0,(void*)0}},{{&l_2048,&l_2048,&l_2048,&l_2048},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_2048,&l_2048,&l_2048,&l_2048},{&l_2048,&l_2048,&l_2048,&l_2048},{&l_2048,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2048,&l_2048,&l_2048}}};
    int32_t l_2078 = 0x3530EC82L;
    uint64_t **l_2100[2];
    uint16_t **l_2109 = &g_560;
    int8_t **l_2123 = &g_1137;
    int32_t l_2149 = 0xD6530E5EL;
    int32_t l_2150 = 1L;
    int32_t l_2151 = 0L;
    int32_t l_2154 = 0xB5FAB831L;
    int32_t l_2159 = 0x46C70E5CL;
    uint64_t l_2170 = 0xAF71C608E98C2110LL;
    int64_t * const l_2205 = &g_189[1];
    uint16_t l_2233 = 0UL;
    uint32_t * const ***l_2237 = (void*)0;
    uint32_t * const ****l_2236[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_2290 = 0x8F5644FEL;
    int32_t l_2291 = (-1L);
    int32_t l_2292 = 0L;
    int32_t l_2293 = (-1L);
    int32_t l_2294 = 4L;
    int32_t l_2295[4] = {(-1L),(-1L),(-1L),(-1L)};
    int32_t *l_2303 = &l_2154;
    uint16_t ***l_2388 = (void*)0;
    uint32_t *l_2458 = &g_44;
    uint32_t l_2565 = 0UL;
    int32_t ** const l_2696 = &l_2303;
    int32_t ** const *l_2695 = &l_2696;
    int16_t l_2755 = 1L;
    int32_t l_2761 = 0L;
    uint32_t l_2769 = 0UL;
    float l_2858[10] = {(-0x9.6p+1),0xA.B3E17Ep+96,0x3.9D47C7p-50,0xA.B3E17Ep+96,(-0x9.6p+1),(-0x9.6p+1),0xA.B3E17Ep+96,0x3.9D47C7p-50,0xA.B3E17Ep+96,(-0x9.6p+1)};
    uint32_t l_2863 = 0x35D42B3CL;
    int32_t *l_2867 = &g_2634;
    int32_t *l_2868 = &l_2295[0];
    int32_t *l_2869 = &l_2150;
    int32_t *l_2870 = &g_283[1][2][6];
    int32_t *l_2871 = &g_293;
    int32_t *l_2872 = &l_2150;
    int32_t *l_2873 = &l_2078;
    int32_t *l_2874[8][3][3] = {{{&l_2761,&l_2293,&l_2293},{&l_2159,(void*)0,&g_283[0][1][8]},{&l_2149,&l_2295[3],&l_2149}},{{&l_2159,&g_283[0][1][8],(void*)0},{&l_2761,&l_2295[3],&l_2293},{(void*)0,(void*)0,(void*)0}},{{&l_2149,&l_2293,&l_2149},{(void*)0,&g_283[0][1][8],&g_283[0][1][8]},{&l_2761,&l_2293,&l_2293}},{{&l_2159,(void*)0,&g_283[0][1][8]},{&l_2149,&l_2295[3],&l_2149},{&l_2159,&g_283[0][1][8],(void*)0}},{{&l_2761,&l_2295[3],&l_2293},{(void*)0,(void*)0,(void*)0},{&l_2149,&l_2293,&l_2149}},{{(void*)0,&g_283[0][1][8],&g_283[0][1][8]},{&l_2761,&l_2293,&l_2293},{&l_2159,(void*)0,&g_283[0][1][8]}},{{&l_2149,&l_2295[3],&l_2149},{&l_2159,&g_283[0][1][8],(void*)0},{&l_2761,&l_2295[3],&l_2293}},{{(void*)0,(void*)0,(void*)0},{&l_2149,&l_2293,&l_2149},{(void*)0,&g_283[0][1][8],&g_283[0][1][8]}}};
    int32_t l_2875 = 0x98A637ABL;
    int16_t l_2876 = (-3L);
    int8_t l_2877 = 0L;
    int8_t l_2878[5][7][2] = {{{(-1L),0x66L},{0x2FL,0xD3L},{0xD3L,1L},{0x75L,0x6EL},{7L,(-1L)},{2L,1L},{0x63L,1L}},{{2L,(-1L)},{7L,0x6EL},{0x75L,1L},{0xD3L,0xD3L},{0x2FL,0x66L},{(-1L),0x63L},{4L,0x8FL}},{{0x66L,4L},{1L,0x9BL},{1L,4L},{0x66L,0x8FL},{4L,0x63L},{(-1L),0x66L},{0x2FL,1L}},{{1L,0xD3L},{(-1L),0x9BL},{(-1L),0x2FL},{0x75L,7L},{1L,7L},{0x75L,0x2FL},{(-1L),0x9BL}},{{(-1L),0xD3L},{1L,1L},{0x8FL,1L},{0x2FL,1L},{0x63L,(-5L)},{1L,0x63L},{0xF3L,5L}}};
    int8_t l_2879 = 0xFEL;
    uint64_t l_2880 = 0x729E2B45F760F4F2LL;
    uint64_t ****l_2893 = (void*)0;
    uint64_t *****l_2894[9][6][2] = {{{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{(void*)0,&l_2893}},{{&l_2893,(void*)0},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,(void*)0},{&l_2893,&l_2893}},{{(void*)0,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893}},{{(void*)0,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,(void*)0},{&l_2893,(void*)0},{&l_2893,&l_2893}},{{&l_2893,&l_2893},{&l_2893,&l_2893},{(void*)0,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893}},{{&l_2893,&l_2893},{&l_2893,&l_2893},{(void*)0,&l_2893},{&l_2893,(void*)0},{&l_2893,&l_2893},{&l_2893,&l_2893}},{{&l_2893,&l_2893},{&l_2893,(void*)0},{&l_2893,&l_2893},{(void*)0,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893}},{{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{(void*)0,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893}},{{&l_2893,(void*)0},{&l_2893,(void*)0},{&l_2893,&l_2893},{&l_2893,&l_2893},{&l_2893,&l_2893},{(void*)0,&l_2893}}};
    uint32_t l_2901 = 0x1B78E7F3L;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2100[i] = (void*)0;
    if ((((*g_1494) &= ((safe_div_func_uint16_t_u_u(0xA16DL, (safe_lshift_func_uint8_t_u_s(l_2012, 0)))) ^ l_2012)) && ((safe_sub_func_int64_t_s_s(p_11, ((p_11 , (((((void*)0 != l_2015) , l_2012) & (0xA1FDD405L < 2UL)) | 0x587E75EAL)) != (**g_1933)))) , p_11)))
    { /* block id: 861 */
        int64_t *l_2031[5] = {&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0]};
        int64_t *l_2033 = &g_189[0];
        float l_2043 = 0x7.7A79FFp-85;
        uint16_t * const *l_2046 = &g_560;
        uint16_t * const **l_2045 = &l_2046;
        int32_t l_2063 = (-8L);
        uint32_t * const **l_2076[3][6] = {{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,(void*)0,&g_95,&g_95,(void*)0,&g_95},{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95}};
        uint32_t * const *** const l_2075[7] = {&l_2076[2][4],&l_2076[2][4],&l_2076[2][4],&l_2076[2][4],&l_2076[2][4],&l_2076[2][4],&l_2076[2][4]};
        uint32_t * const *** const *l_2074[1][9][2] = {{{&l_2075[1],&l_2075[1]},{&l_2075[4],&l_2075[1]},{&l_2075[1],&l_2075[1]},{&l_2075[4],&l_2075[1]},{&l_2075[1],&l_2075[1]},{&l_2075[4],&l_2075[1]},{&l_2075[1],&l_2075[1]},{&l_2075[4],&l_2075[1]},{&l_2075[1],&l_2075[1]}}};
        int32_t l_2084 = 0xE3300A19L;
        uint64_t **l_2098 = &g_1176;
        uint32_t ***l_2124 = &g_1682[7][0][0];
        const float l_2128 = 0x2.7p+1;
        int32_t l_2148 = 1L;
        int32_t l_2152 = 4L;
        int32_t l_2153 = 0x9591799AL;
        int32_t l_2155 = 0x4EFB5549L;
        int32_t l_2156 = 0x28B7FCE4L;
        int32_t l_2157 = 0x69CE6717L;
        int32_t l_2158 = 0x3A6309D3L;
        uint8_t l_2160 = 0x2EL;
        int32_t ** const **l_2222 = (void*)0;
        int i, j, k;
        for (g_249 = 0; (g_249 != 10); ++g_249)
        { /* block id: 864 */
            int64_t l_2022 = 0x36D6536FF94630E1LL;
            int64_t **l_2032 = &l_2031[3];
            int64_t **l_2034 = (void*)0;
            int64_t **l_2035 = &g_1494;
            float **l_2051 = &g_666;
            float ***l_2050[9] = {&l_2051,&l_2051,&l_2051,&l_2051,&l_2051,&l_2051,&l_2051,&l_2051,&l_2051};
            float ****l_2049 = &l_2050[3];
            int32_t l_2101[10][10] = {{0x6FD5677FL,0x2FE93D60L,1L,(-10L),0xFE489D95L,1L,0x182C38CDL,0x2FE93D60L,(-1L),1L},{0x6FD5677FL,0x2DF8D5B3L,1L,(-6L),0xAA0ED9E8L,0L,1L,0xAA0ED9E8L,0xAD2B247BL,0x806CB151L},{0L,1L,0xAA0ED9E8L,0xAD2B247BL,0x806CB151L,0x2DF8D5B3L,0L,0xD2EE4414L,0L,0x2DF8D5B3L},{0x182C38CDL,(-10L),0x0EC74D59L,(-10L),0x182C38CDL,1L,0x6FD5677FL,1L,1L,0x7C1D431DL},{(-6L),0L,(-1L),0x4608630DL,0x2DF8D5B3L,(-1L),1L,0xAD2B247BL,1L,0x7C1D431DL},{0xE531F3F2L,0x4608630DL,0L,0xA151E698L,0x182C38CDL,0L,0L,1L,0L,0x2DF8D5B3L},{0x94A8490DL,0x2FE93D60L,0x2AA50705L,0x182C38CDL,0x806CB151L,(-1L),(-6L),(-6L),(-1L),0x806CB151L},{0x7C1D431DL,(-1L),(-1L),0x7C1D431DL,0xAA0ED9E8L,0xA151E698L,0xD2EE4414L,0L,1L,1L},{(-1L),0x6FD5677FL,1L,0L,0xFE489D95L,0L,(-1L),0xD2EE4414L,1L,0x2D601391L},{0xD2EE4414L,1L,1L,0x7C1D431DL,0x94A8490DL,0xA00FEDC8L,0x7C1D431DL,(-10L),(-1L),0xD2EE4414L}};
            uint16_t **l_2110 = &g_560;
            int32_t *l_2120 = &g_785;
            int8_t **l_2133 = (void*)0;
            int64_t l_2147[7][9] = {{0x821642307186D089LL,0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL,0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL},{0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL},{0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL,0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL,0x821642307186D089LL},{7L,(-9L),7L,(-9L),7L,(-9L),7L,(-9L),7L},{0x821642307186D089LL,0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL,0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL},{0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL,(-9L),0x7B8B542F77B999D7LL},{0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL,0x821642307186D089LL,0x095B71E4B9989E1BLL,0x095B71E4B9989E1BLL,0x821642307186D089LL,0x821642307186D089LL}};
            int i, j;
            if ((safe_div_func_uint64_t_u_u(((*g_1176) = ((((safe_mod_func_uint8_t_u_u((l_2012 > (l_2022 > (safe_lshift_func_uint8_t_u_u((((safe_mod_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(0xA5L, (safe_add_func_uint32_t_u_u((l_2044[6][0][1] ^= (((((*l_2032) = l_2031[0]) != ((*l_2035) = l_2033)) , 0xB4L) == (p_11 >= (safe_add_func_uint32_t_u_u((--(*g_72)), (+((((l_2012 , ((*g_96)++)) , (4294967295UL ^ 0UL)) || (*g_1176)) || p_11))))))), l_2022)))), (**g_1933))) , 0UL) , 255UL), p_11)))), l_2012)) , (*g_1748)) != l_2045) < 1UL)), 0x760FE138AC029C19LL)))
            { /* block id: 871 */
                uint32_t ****l_2052 = &g_329;
                int16_t *l_2067 = &g_1339;
                uint8_t *l_2077 = &g_691;
                int32_t l_2079 = 0L;
                int32_t l_2080 = 0x93841949L;
                int16_t *l_2081 = &g_2082;
                int32_t *l_2083 = &g_283[0][1][8];
                (*g_1662) = (l_2047[4][2][1] == l_2049);
                if ((l_2084 &= ((*l_2083) = (l_2052 != ((((*l_2081) = (safe_sub_func_int32_t_s_s((l_2022 ^ (safe_div_func_int32_t_s_s(0x608340C1L, l_2022))), (safe_rshift_func_int8_t_s_s((((safe_div_func_int8_t_s_s((safe_mod_func_int16_t_s_s(((l_2063 && (~5L)) | (safe_add_func_int16_t_s_s(((*l_2067) = 3L), (safe_rshift_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_s((safe_mod_func_int8_t_s_s(7L, (((l_2079 = ((((((*l_2077) ^= (l_2074[0][4][1] != (void*)0)) == l_2022) && p_11) | l_2078) && (**g_1175))) , p_11) , (-1L)))), 4)) & (**g_1933)) | p_11), 2))))), 0xFC85L)), 0xE7L)) < l_2080) , 0x8CL), p_11))))) | (**g_1933)) , &g_329)))))
                { /* block id: 879 */
                    int64_t l_2087 = 0x48E29F3E0C959434LL;
                    uint64_t ***l_2099 = &g_1247;
                    l_2063 = (((safe_sub_func_uint32_t_u_u(l_2087, (safe_div_func_uint16_t_u_u((((((**l_2045) == (void*)0) >= p_11) < (l_2101[1][8] |= (safe_rshift_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((((safe_div_func_int64_t_s_s(0x2BDEA00969A4DA90LL, ((((safe_sub_func_uint8_t_u_u(((((((*l_2099) = l_2098) == (p_11 , l_2100[0])) == (-0x8.3p+1)) , (*l_2083)) > l_2012), p_11)) <= p_11) > p_11) || l_2063))) || 0x7DA60E64L) <= 0xFCL), l_2044[6][0][1])), 7)))) , (*l_2083)), p_11)))) , (*l_2083)) | p_11);
                }
                else
                { /* block id: 883 */
                    l_2063 |= (safe_unary_minus_func_int8_t_s((safe_rshift_func_int16_t_s_s((*g_1934), 13))));
                    if ((*g_1528))
                        break;
                }
                (*g_1969) = ((*l_2083) = (l_2063 &= p_11));
            }
            else
            { /* block id: 890 */
                int32_t **l_2121 = (void*)0;
                int32_t **l_2122 = &l_2120;
                uint32_t ***l_2125 = (void*)0;
                int16_t *l_2126 = &g_2082;
                int32_t l_2127 = (-7L);
                int8_t ***l_2134 = &g_1527;
                int32_t *l_2145 = &g_283[0][1][8];
                int32_t *l_2146[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_2146[i] = &g_283[1][2][7];
                l_2084 = (safe_mul_func_uint16_t_u_u(p_11, (*g_1934)));
                (*g_1969) = ((safe_div_func_int64_t_s_s(((((*g_1749) = l_2109) != l_2110) < (safe_rshift_func_uint16_t_u_s((((l_2101[1][7] = 0x9FD8L) < (+(safe_rshift_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((((((((safe_mod_func_int8_t_s_s(((((*l_2122) = l_2120) != ((((void*)0 == l_2123) | ((*l_2126) = (((-1L) || 0xA6043A3763303ACCLL) , (((l_2124 = (void*)0) != l_2125) > (*g_1284))))) , (void*)0)) || p_11), (-3L))) || p_11) == (*g_271)) || l_2127) , p_11) , p_11) < p_11), 0x71L)), l_2127)))) != 0xE3FDL), 7))), 0x463B5CFDC7E7D8ECLL)) | p_11);
                (*g_1969) = ((safe_add_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(((((((*l_2134) = l_2133) == (void*)0) , (p_11 < (safe_div_func_uint8_t_u_u(((void*)0 != (*l_2035)), ((safe_sub_func_uint16_t_u_u(((safe_add_func_int32_t_s_s(p_11, (safe_mul_func_uint16_t_u_u(p_11, 0UL)))) , (((*l_2126) ^= (((l_2101[1][8] = (safe_mod_func_int8_t_s_s((l_2127 ^ 0x11L), l_2044[6][1][5]))) ^ l_2022) != 0x1098L)) , 65528UL)), 65535UL)) , l_2127))))) , &g_1108) == (void*)0), l_2063)), p_11)) , 0xD85D7B98L);
                l_2160++;
            }
        }
        if (l_2044[6][0][1])
        { /* block id: 905 */
            int32_t *l_2163 = &l_2159;
            int32_t *l_2164 = &l_2155;
            int32_t *l_2165 = &l_2084;
            int32_t *l_2166 = &l_2152;
            int32_t *l_2167 = (void*)0;
            int32_t *l_2168[8][8][4] = {{{&l_2158,&l_2159,(void*)0,&l_2063},{(void*)0,&l_2078,(void*)0,(void*)0},{&l_2158,&l_2149,&l_2078,&l_2084},{(void*)0,&g_283[0][1][8],&g_293,&l_2063},{&g_293,&l_2063,(void*)0,(void*)0},{&l_2158,&l_2063,&l_2152,&l_2063},{&g_293,&l_2158,(void*)0,&g_283[0][1][8]},{&l_2078,&l_2159,&l_2151,&l_2063}},{{(void*)0,&l_2078,(void*)0,(void*)0},{(void*)0,&l_2155,&l_2151,(void*)0},{&l_2078,(void*)0,(void*)0,&l_2155},{&g_293,&l_2078,&l_2148,&g_283[0][1][8]},{(void*)0,&l_2158,&l_2151,&g_283[0][1][8]},{&l_2152,&l_2078,&l_2152,&l_2155},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2155,(void*)0,(void*)0}},{{&l_2151,&l_2078,(void*)0,&l_2063},{(void*)0,&l_2159,(void*)0,&g_283[0][1][8]},{(void*)0,&l_2158,&l_2152,(void*)0},{&l_2152,(void*)0,&l_2151,&l_2084},{(void*)0,(void*)0,&l_2148,(void*)0},{&g_293,&l_2158,(void*)0,&g_283[0][1][8]},{&l_2078,&l_2159,&l_2151,&l_2063},{(void*)0,&l_2078,(void*)0,(void*)0}},{{(void*)0,&l_2155,&l_2151,(void*)0},{&l_2078,(void*)0,(void*)0,&l_2155},{&g_293,&l_2078,&l_2148,&g_283[0][1][8]},{(void*)0,&l_2158,&l_2151,&g_283[0][1][8]},{&l_2152,&l_2078,&l_2152,&l_2155},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2155,(void*)0,(void*)0},{&l_2151,&l_2078,(void*)0,&l_2063}},{{(void*)0,&l_2159,(void*)0,&g_283[0][1][8]},{(void*)0,&l_2158,&l_2152,(void*)0},{&l_2152,(void*)0,&l_2151,&l_2084},{(void*)0,(void*)0,&l_2148,(void*)0},{&g_293,&l_2158,(void*)0,&g_283[0][1][8]},{&l_2078,&l_2159,&l_2151,&l_2063},{(void*)0,&l_2078,(void*)0,(void*)0},{(void*)0,&l_2155,&l_2151,(void*)0}},{{&l_2078,(void*)0,(void*)0,&l_2155},{&g_293,&l_2078,&l_2148,&g_283[0][1][8]},{(void*)0,&l_2158,&l_2151,&g_283[0][1][8]},{&l_2152,&l_2078,&l_2152,&l_2155},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2155,(void*)0,(void*)0},{&l_2151,&l_2078,(void*)0,&l_2063},{(void*)0,&l_2159,(void*)0,&g_283[0][1][8]}},{{(void*)0,&l_2158,&l_2152,(void*)0},{&l_2152,(void*)0,&l_2151,&l_2084},{(void*)0,(void*)0,&l_2148,(void*)0},{&g_293,&l_2158,(void*)0,&g_283[0][1][8]},{&l_2078,&l_2159,&l_2151,&l_2063},{(void*)0,&l_2078,(void*)0,&l_2084},{(void*)0,&l_2084,(void*)0,&l_2063},{(void*)0,&l_2084,&l_2158,&l_2084}},{{&l_2152,&l_2158,(void*)0,&l_2158},{&l_2151,&g_132,(void*)0,&l_2158},{&l_2148,&l_2158,&l_2148,&l_2084},{(void*)0,&l_2084,(void*)0,&l_2063},{&l_2151,&l_2084,&l_2158,&l_2084},{(void*)0,&l_2158,&l_2158,&l_2149},{&l_2151,&l_2155,(void*)0,&l_2158},{(void*)0,&l_2063,&l_2148,&l_2084}}};
            int32_t l_2169 = 0x04587B5DL;
            int i, j, k;
            l_2170--;
        }
        else
        { /* block id: 907 */
            uint8_t l_2234 = 0x5DL;
            int32_t l_2235 = 0xA0C6FB9BL;
            uint16_t *****l_2239 = &g_1748;
            const uint64_t *l_2246 = &g_252;
            uint64_t *l_2247 = &g_12;
            int32_t *l_2248 = &l_2158;
            for (g_691 = (-10); (g_691 >= 52); g_691 = safe_add_func_int8_t_s_s(g_691, 2))
            { /* block id: 910 */
                uint32_t l_2206 = 0xA61F7B0AL;
                int32_t * const **l_2221 = (void*)0;
                int32_t * const ***l_2220[6] = {(void*)0,&l_2221,(void*)0,(void*)0,&l_2221,(void*)0};
                int i;
                (*g_2175) = &l_2149;
                for (g_910 = 0; (g_910 <= 4); g_910 += 1)
                { /* block id: 914 */
                    uint8_t *l_2210 = (void*)0;
                    uint8_t *l_2211 = (void*)0;
                    uint8_t *l_2212 = (void*)0;
                    uint8_t *l_2213[6];
                    uint64_t l_2214 = 0x9107325BC8B35E32LL;
                    const int32_t l_2231 = 0x09B388D0L;
                    int16_t *l_2245 = &g_2232;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_2213[i] = &g_216[0][2][3];
                    for (g_190 = 0; (g_190 <= 4); g_190 += 1)
                    { /* block id: 917 */
                        const int32_t l_2204 = (-1L);
                        int32_t *l_2207[8];
                        int i, j, k;
                        for (i = 0; i < 8; i++)
                            l_2207[i] = &g_293;
                        g_283[0][1][8] |= (safe_mul_func_uint16_t_u_u((l_2206 &= (safe_lshift_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_u(((safe_add_func_uint64_t_u_u(((l_2154 == p_11) ^ (safe_mod_func_uint8_t_u_u((l_2159 ^ (safe_div_func_uint32_t_u_u((((*g_96) |= 0xA7C44D31L) <= ((safe_unary_minus_func_int16_t_s(((safe_add_func_uint8_t_u_u(((((safe_mul_func_int16_t_s_s((((*g_1934) , (**g_1191)) >= p_11), (safe_div_func_int8_t_s_s((g_1424 = (((0xB3L != ((((safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s((!(((((g_236[g_190][(g_910 + 4)][g_910]++) , (safe_mod_func_uint64_t_u_u(0x87CD7002EFE75E7DLL, 0x8AE483D519DAAAA0LL))) & l_2204) , l_2205) == &g_189[3])), p_11)), p_11)) ^ p_11) , 0xA2L) & p_11)) < (-1L)) != p_11)), p_11)))) && 0xD4FEEE9BL) == l_2158) ^ l_2157), p_11)) | (*g_311)))) != 252UL)), l_2044[4][0][3]))), l_2204))), p_11)) | 0x06L), 5)) < l_2204), 6))), p_11));
                    }
                    (*g_1969) &= ((***g_1692) &= (safe_lshift_func_int8_t_s_u(((l_2214--) & ((safe_mod_func_uint8_t_u_u(l_2150, p_11)) != (l_2235 = ((l_2234 = (((+(((p_11 , l_2220[4]) != l_2222) && ((safe_mod_func_int16_t_s_s((*g_1934), ((safe_sub_func_int32_t_s_s(p_11, (safe_div_func_uint64_t_u_u((safe_div_func_uint32_t_u_u((l_2231 , 0x4CAD5C12L), g_2232)), p_11)))) ^ (-1L)))) == l_2153))) & l_2233) , 249UL)) , l_2234)))), p_11)));
                    for (g_12 = 0; (g_12 <= 4); g_12 += 1)
                    { /* block id: 931 */
                        int32_t l_2238 = 0x2D8B4F10L;
                        l_2238 = ((*g_666) = (l_2236[4] == (void*)0));
                        return p_11;
                    }
                    l_2235 ^= (l_2239 != ((safe_mod_func_int32_t_s_s(((safe_rshift_func_uint16_t_u_u((safe_unary_minus_func_int16_t_s(0x4672L)), (l_2245 == (void*)0))) , ((p_11 | (l_2246 == (l_2247 = &l_2214))) | l_2012)), (**g_928))) , (void*)0));
                    for (g_1376 = 0; (g_1376 <= 4); g_1376 += 1)
                    { /* block id: 940 */
                        uint32_t *l_2249 = &g_73[2][1];
                        int32_t l_2253 = 0L;
                        int i, j, k;
                        l_2248 = (*g_654);
                        (**g_665) = (l_2249 == ((g_785 &= (((((((*g_96) = (safe_div_func_uint64_t_u_u((g_236[g_910][(g_910 + 1)][g_1376] |= (0x6A47CDEDFC3E8CE6LL && (!l_2253))), (safe_mul_func_int8_t_s_s(g_1363[0][1], l_2253))))) || (*g_1528)) == (safe_mod_func_uint32_t_u_u((safe_mod_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((*g_1640) > ((safe_div_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((-6L) <= ((safe_mul_func_uint16_t_u_u((((*l_2033) = (0L >= 0xAEABA4A2L)) < l_2253), l_2253)) == p_11)), p_11)), 0x8C6AL)) > l_2253)) || l_2214), 0x02D9L)), p_11)), l_2044[3][3][6]))) || p_11) < p_11) , (**g_1964))) , l_2248));
                        (*l_2248) = (**g_1191);
                    }
                }
            }
        }
        (*g_1969) = 0xE12CDFD7L;
    }
    else
    { /* block id: 953 */
        float l_2270 = (-0x1.0p+1);
        int32_t l_2275 = 4L;
        uint64_t **l_2283 = (void*)0;
        int32_t l_2286 = 0L;
        int32_t l_2287 = (-5L);
        int32_t l_2288[2];
        uint16_t l_2331 = 0x0975L;
        int32_t l_2340 = 0x044D412DL;
        int64_t l_2355 = (-1L);
        int32_t l_2397[4][8][5] = {{{0x95CD9103L,0xEBA22810L,0L,4L,(-5L)},{0L,0x3B462A51L,9L,1L,1L},{0x1481D232L,1L,0x1481D232L,4L,0xB7316A21L},{0x7CB5A7BDL,6L,(-1L),1L,(-5L)},{0x1481D232L,0x303D3086L,(-8L),(-5L),0x6C39933CL},{0L,0xB5AE0684L,(-1L),(-5L),0x8976AF41L},{0x95CD9103L,0xF82947E0L,0x1481D232L,0xC468759EL,0x6C39933CL},{0xD9A88CA5L,2L,9L,0x22B9E36AL,(-5L)}},{{(-8L),0xF82947E0L,0L,0xB7316A21L,0xB7316A21L},{0xFC259D0CL,0xB5AE0684L,0xFC259D0CL,0x22B9E36AL,1L},{0x4DD5B9C6L,0x303D3086L,(-1L),0xC468759EL,(-5L)},{0xFC259D0CL,6L,0L,(-5L),0L},{(-8L),1L,(-1L),(-5L),(-1L)},{0xD9A88CA5L,0x3B462A51L,0xFC259D0CL,1L,0L},{0x95CD9103L,0xEBA22810L,0L,4L,(-5L)},{0L,0x3B462A51L,9L,1L,1L}},{{0x1481D232L,1L,0x1481D232L,4L,0xB7316A21L},{0x7CB5A7BDL,6L,(-1L),1L,(-5L)},{0x1481D232L,0x303D3086L,(-8L),(-5L),0x6C39933CL},{0L,0xB5AE0684L,(-1L),(-5L),0x8976AF41L},{0x95CD9103L,0xF82947E0L,0x1481D232L,0xC468759EL,0x6C39933CL},{0xD9A88CA5L,2L,9L,0x22B9E36AL,(-5L)},{(-8L),0xF82947E0L,0L,0xB7316A21L,0xB7316A21L},{0xFC259D0CL,0xB5AE0684L,0xFC259D0CL,0x22B9E36AL,1L}},{{0x4DD5B9C6L,0x303D3086L,(-1L),0xC468759EL,(-5L)},{0xFC259D0CL,6L,0L,(-5L),0L},{(-8L),1L,(-1L),(-5L),(-1L)},{0xD9A88CA5L,0x3B462A51L,0xFC259D0CL,1L,0L},{0x95CD9103L,0xEBA22810L,0L,4L,0L},{0x7D0B48FFL,0x2D4CE48BL,6L,0L,0L},{1L,(-1L),1L,(-1L),(-8L)},{0L,0xD8EADAD0L,0xC8A6FC8CL,0xFC259D0CL,9L}}};
        int64_t *** const l_2493[7] = {&g_1493,&g_1493,&g_1493,&g_1493,&g_1493,&g_1493,&g_1493};
        const int32_t l_2503 = 1L;
        int32_t l_2518 = (-3L);
        int8_t l_2521 = 0xB1L;
        const uint32_t *l_2543 = &g_190;
        const uint32_t **l_2542[1][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        const uint32_t *** const l_2541 = &l_2542[0][8];
        int8_t l_2656 = 0x2AL;
        int64_t l_2658 = 0x926AE2DFA1F455B0LL;
        int32_t *l_2670[1];
        uint32_t **l_2722[9];
        const uint32_t l_2754 = 4294967295UL;
        uint32_t l_2764 = 1UL;
        uint32_t l_2770 = 0x931E081DL;
        int32_t l_2802 = 0xA3CB3916L;
        uint16_t ** const ***l_2831 = (void*)0;
        int32_t *l_2866 = (void*)0;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2288[i] = 1L;
        for (i = 0; i < 1; i++)
            l_2670[i] = &l_2294;
        for (i = 0; i < 9; i++)
            l_2722[i] = &l_2458;
    }
    l_2880++;
    l_2901 = (safe_div_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((**g_665), (safe_add_func_float_f_f(((*l_2870) = ((*g_1097) = (p_11 , (*l_2868)))), ((g_2891 = g_2891) != (g_2895 = l_2893)))))), (((safe_mod_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_s(0x9EL, 1)), 0xE9L)) ^ (0xB8L || g_2900)) , (*l_2303)))), 0x6.15A66Ap-5));
    (**l_2695) = (*l_2696);
    return p_11;
}


/* ------------------------------------------ */
/* 
 * reads : g_249 g_1339 g_268 g_132 g_294 g_659 g_67 g_95 g_96 g_97 g_1376 g_252 g_271 g_272 g_293 g_283 g_1420 g_571 g_72 g_1366 g_279 g_216 g_1373 g_691 g_1247 g_1176 g_79 g_236 g_1492 g_666 g_100 g_1318 g_1528 g_974 g_975 g_190 g_1107 g_44 g_334 g_73 g_1421 g_1422 g_560 g_582 g_270 g_1572 g_223 g_1285 g_1636 g_70 g_1662 g_1371 g_1692 g_1097 g_85 g_665 g_311 g_1494 g_189 g_1747 g_1175 g_1874 g_1875 g_1933 g_1934 g_1372
 * writes: g_249 g_293 g_1339 g_283 g_132 g_294 g_1363 g_268 g_73 g_1424 g_97 g_1366 g_1373 g_691 g_1493 g_1527 g_1108 g_44 g_785 g_94 g_79 g_85 g_95 g_100 g_1640 g_1682 g_1371 g_1692 g_788 g_311 g_1372
 */
static uint64_t  func_13(float  p_14, int16_t  p_15, int64_t  p_16, uint32_t  p_17)
{ /* block id: 563 */
    int64_t l_1341 = 2L;
    int32_t l_1345 = 0x8B2E9F6BL;
    int32_t l_1346 = 0x07E84430L;
    int32_t l_1347 = (-7L);
    int32_t l_1349[6];
    uint16_t * const l_1365 = &g_1366;
    uint16_t * const *l_1364 = &l_1365;
    uint16_t * const l_1368[5][6] = {{&g_1373,&g_1375[5][8],&g_1370,&g_1373,&g_1370,&g_1375[5][8]},{&g_1371,&g_1375[5][8],(void*)0,&g_1371,&g_1370,&g_1370},{&g_1376,&g_1375[5][8],&g_1375[5][8],&g_1376,&g_1370,(void*)0},{&g_1373,&g_1375[5][8],&g_1370,&g_1373,&g_1370,&g_1375[5][8]},{&g_1371,&g_1375[5][8],(void*)0,&g_1371,&g_1370,&g_1370}};
    uint16_t * const *l_1367 = &l_1368[3][3];
    int64_t **l_1491 = (void*)0;
    uint32_t ***l_1495 = (void*)0;
    int16_t l_1506 = (-7L);
    uint16_t l_1507 = 1UL;
    const uint16_t * const **l_1520 = (void*)0;
    const uint16_t * const ** const *l_1519 = &l_1520;
    uint32_t ****l_1533 = &l_1495;
    uint32_t l_1566 = 9UL;
    const int32_t *l_1619 = &g_283[1][0][0];
    const int32_t **l_1618 = &l_1619;
    const int32_t ***l_1617 = &l_1618;
    uint8_t *l_1648 = &g_691;
    int8_t l_1673 = 0xDBL;
    uint16_t *****l_1750 = &g_1748;
    int8_t l_1762 = (-1L);
    uint32_t l_1764 = 3UL;
    int32_t l_1786 = 0x45EA9C59L;
    uint8_t l_1893 = 0x31L;
    uint32_t l_1972[9][6] = {{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL},{0x9241D122L,18446744073709551614UL,0UL,18446744073709551609UL,0UL,18446744073709551614UL}};
    uint8_t l_1999 = 255UL;
    int i, j;
    for (i = 0; i < 6; i++)
        l_1349[i] = 0x9F791DAEL;
    for (g_249 = 1; (g_249 >= 0); g_249 -= 1)
    { /* block id: 566 */
        int32_t *l_1331 = &g_283[0][1][8];
        int32_t *l_1333 = &g_132;
        int32_t l_1348 = 0xEB3E0166L;
        int32_t l_1350 = (-7L);
        int32_t l_1351 = 0xA15AF235L;
        int32_t l_1352[4][2][2] = {{{0x4B421A3CL,0x4B421A3CL},{0x4B421A3CL,0x4B421A3CL}},{{0x4B421A3CL,0x4B421A3CL},{0x4B421A3CL,0x4B421A3CL}},{{0x4B421A3CL,0x4B421A3CL},{0x4B421A3CL,0x4B421A3CL}},{{0x4B421A3CL,0x4B421A3CL},{0x4B421A3CL,0x4B421A3CL}}};
        uint16_t **l_1358 = &g_560;
        const uint8_t *l_1394 = &g_216[1][2][4];
        const uint8_t * const *l_1393 = &l_1394;
        int64_t *l_1397[5] = {&l_1341,&l_1341,&l_1341,&l_1341,&l_1341};
        int16_t *l_1414 = (void*)0;
        int16_t ** const l_1413 = &l_1414;
        int32_t l_1426 = 1L;
        uint64_t **l_1488 = (void*)0;
        uint64_t **l_1490 = (void*)0;
        int i, j, k;
        for (g_293 = 1; (g_293 >= 0); g_293 -= 1)
        { /* block id: 569 */
            int32_t **l_1332 = (void*)0;
            int16_t *l_1338 = &g_1339;
            int32_t *l_1340 = &g_132;
            int i;
            l_1333 = l_1331;
            (*l_1340) ^= (safe_lshift_func_int16_t_s_s(((*l_1331) = ((safe_mul_func_int16_t_s_s((-6L), ((*l_1338) &= ((void*)0 != &g_216[0][1][6])))) , ((*l_1338) &= g_268[g_249]))), 3));
        }
        for (g_294 = 0; (g_294 <= 1); g_294 += 1)
        { /* block id: 578 */
            int32_t *l_1342 = &g_293;
            int32_t *l_1343 = &g_132;
            int32_t *l_1344[6];
            uint32_t l_1353[4][9][3] = {{{0x5A6CDF10L,0x7FE58159L,0x7FE58159L},{0x6594A2B1L,5UL,4294967286UL},{0x6594A2B1L,4294967286UL,0UL},{0x5A6CDF10L,1UL,1UL},{0x416935C8L,4294967292UL,1UL},{4294967295UL,1UL,3UL},{0x2E3CF0B7L,4294967286UL,3UL},{0UL,5UL,3UL},{8UL,0x7FE58159L,3UL}},{{1UL,3UL,1UL},{0x1541D803L,4294967290UL,1UL},{1UL,0xE65F57A5L,0UL},{8UL,6UL,4294967286UL},{0UL,6UL,0x7FE58159L},{0x2E3CF0B7L,0xE65F57A5L,6UL},{4294967295UL,4294967290UL,0UL},{0x416935C8L,3UL,6UL},{0x5A6CDF10L,0x7FE58159L,0xFF270C24L}},{{1UL,9UL,0x74519FC5L},{1UL,0x74519FC5L,0xDC0B1EFEL},{4294967295UL,0x5652E5D5L,0x5B372C76L},{5UL,0x78E962D5L,0xCF32D5CFL},{0xE65F57A5L,0x5652E5D5L,4294967295UL},{1UL,0x74519FC5L,4294967292UL},{0x7FE58159L,9UL,4294967292UL},{0UL,0xFF270C24L,4294967295UL},{3UL,4294967292UL,0xCF32D5CFL}},{{0UL,0x2D0F88FEL,0x5B372C76L},{3UL,0xABE136B4L,0xDC0B1EFEL},{0UL,0x26787B5AL,0x74519FC5L},{0x7FE58159L,0x26787B5AL,0xFF270C24L},{1UL,0xABE136B4L,0x26787B5AL},{0xE65F57A5L,0x2D0F88FEL,4294967290UL},{5UL,4294967292UL,0x26787B5AL},{4294967295UL,0xFF270C24L,0xFF270C24L},{1UL,9UL,0x74519FC5L}}};
            uint16_t * const **l_1359 = (void*)0;
            uint16_t * const l_1362 = &g_1363[2][1];
            uint16_t * const *l_1361 = &l_1362;
            uint16_t * const **l_1360[2][6];
            int16_t *l_1379 = (void*)0;
            int16_t *l_1380 = &g_1339;
            uint8_t **l_1392 = (void*)0;
            uint8_t ***l_1391 = &l_1392;
            uint64_t l_1419 = 0xF1C6CDB1C79C5B55LL;
            const float l_1425 = 0x8.69CD47p+9;
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_1344[i] = &g_293;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 6; j++)
                    l_1360[i][j] = &l_1361;
            }
            ++l_1353[3][5][0];
            l_1349[3] = (safe_sub_func_uint16_t_u_u(((&g_294 == (void*)0) , ((**l_1361) = (g_268[g_294] >= (l_1358 != (l_1367 = (l_1364 = l_1358)))))), (((safe_sub_func_uint16_t_u_u(((-1L) < ((*l_1380) = g_659[g_249])), (safe_sub_func_int16_t_s_s((g_67[(g_249 + 4)] && (safe_lshift_func_uint8_t_u_s((((((safe_lshift_func_uint16_t_u_s(((((*l_1343) >= 255UL) | (**g_95)) ^ p_17), g_1376)) & g_252) > p_15) > (*g_271)) , (*l_1342)), l_1349[3]))), 0xE6B4L)))) , l_1347) , p_16)));
            if ((safe_mul_func_uint8_t_u_u(((p_15 , p_17) | (safe_mod_func_int64_t_s_s((((*l_1391) = (void*)0) == l_1393), (g_268[g_294]--)))), ((&g_291 != (l_1397[4] = l_1397[4])) != (safe_sub_func_uint64_t_u_u((1UL != ((l_1349[3] ^ ((safe_lshift_func_int8_t_s_s(((p_17 >= l_1341) != g_252), (*l_1331))) || 0xD4L)) , p_15)), p_15))))))
            { /* block id: 588 */
                return (*l_1331);
            }
            else
            { /* block id: 590 */
                int8_t *l_1423[7] = {(void*)0,&g_1424,&g_1424,(void*)0,&g_1424,&g_1424,(void*)0};
                int32_t l_1428 = 0xDC1554F1L;
                int32_t l_1430 = 0L;
                int32_t l_1431 = 0xF64B5FADL;
                int32_t l_1432 = 0xB3A9FD77L;
                int32_t l_1434 = (-10L);
                int32_t l_1437 = 6L;
                int i;
                (*l_1342) = (safe_mod_func_uint64_t_u_u((safe_sub_func_int16_t_s_s((((253UL == (g_1424 = (!(safe_mod_func_int16_t_s_s((((*g_72) = (p_17 , (safe_add_func_int16_t_s_s(((((((((((l_1413 == &l_1380) || (((l_1347 | 4UL) & (safe_lshift_func_int8_t_s_u((l_1346 = ((safe_sub_func_float_f_f(l_1419, ((void*)0 == g_1420))) , p_16)), (*l_1333)))) , 1L)) , (*l_1331)) || 3L) == p_15) & (*l_1331)) > l_1426) == (*g_571)) , p_15) | 0x2BFE4E553D098E39LL), 8UL)))) > 0xAEADD054L), (*l_1342)))))) , g_97) , 0xACE8L), p_17)), l_1349[1]));
                for (g_97 = 0; (g_97 <= 1); g_97 += 1)
                { /* block id: 597 */
                    int16_t l_1427[4][4][2] = {{{1L,1L},{1L,5L},{0x083FL,5L},{1L,1L}},{{1L,1L},{5L,0x083FL},{5L,1L},{1L,1L}},{{1L,5L},{0x083FL,5L},{1L,1L},{1L,1L}},{{5L,0x083FL},{5L,1L},{1L,1L},{1L,5L}}};
                    int32_t l_1429 = 0L;
                    int32_t l_1433 = 0xB060EA00L;
                    int32_t l_1435 = (-1L);
                    int32_t l_1436 = 0L;
                    int i, j, k;
                    for (g_1366 = 0; (g_1366 <= 5); g_1366 += 1)
                    { /* block id: 600 */
                        uint64_t l_1438[9] = {18446744073709551615UL,0xF94DDAFFD59A45C0LL,18446744073709551615UL,0xF94DDAFFD59A45C0LL,18446744073709551615UL,0xF94DDAFFD59A45C0LL,18446744073709551615UL,0xF94DDAFFD59A45C0LL,18446744073709551615UL};
                        int i;
                        l_1438[5]++;
                    }
                    (*l_1333) = ((((((*g_271) | p_17) != (safe_mod_func_uint8_t_u_u(0x94L, l_1349[1]))) , (~((safe_mul_func_uint16_t_u_u((l_1436 = (safe_mul_func_uint8_t_u_u(((g_279 , (p_17 && (p_17 != (0x42L <= ((((safe_mul_func_int16_t_s_s(g_216[1][0][5], 1UL)) , (void*)0) != (void*)0) <= p_16))))) , p_17), p_15))), 8L)) , p_15))) , l_1343) != (void*)0);
                    for (g_1373 = 0; (g_1373 <= 1); g_1373 += 1)
                    { /* block id: 607 */
                        return p_16;
                    }
                }
            }
        }
        (*l_1333) |= 0L;
        for (l_1346 = 5; (l_1346 >= 0); l_1346 -= 1)
        { /* block id: 616 */
            int64_t ** const l_1458 = (void*)0;
            int32_t l_1470 = 0x80CCD44CL;
            int32_t l_1473 = 0xE5B07EB5L;
            int32_t l_1476 = (-1L);
            int32_t l_1480 = (-1L);
            int32_t l_1482 = 0x5355AFEAL;
            uint64_t l_1483 = 0x3BBC85B944C01F89LL;
            uint64_t ***l_1489[6][1][8] = {{{&g_1247,&l_1488,&l_1488,&l_1488,&l_1488,&l_1488,&l_1488,&l_1488}},{{&l_1488,&l_1488,&g_1247,&g_1247,&g_1247,&l_1488,(void*)0,&l_1488}},{{&g_1247,&g_1247,&l_1488,&l_1488,&g_1247,&l_1488,&l_1488,&l_1488}},{{&g_1247,&l_1488,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&l_1488}},{{(void*)0,&g_1247,&g_1247,&l_1488,&l_1488,&g_1247,&l_1488,&l_1488}},{{&l_1488,&l_1488,&l_1488,&g_1247,(void*)0,(void*)0,&l_1488,&g_1247}}};
            int i, j, k;
            for (l_1341 = 5; (l_1341 >= 1); l_1341 -= 1)
            { /* block id: 619 */
                const int32_t *l_1456 = (void*)0;
                int64_t ** const l_1457 = &l_1397[4];
                int32_t *l_1459 = (void*)0;
                int32_t l_1469 = 2L;
                int32_t l_1472 = (-1L);
                int32_t l_1474 = 0L;
                int32_t l_1475 = 0L;
                int32_t l_1478 = (-1L);
                int32_t l_1479 = 0xB1F60810L;
                int32_t l_1481 = 0x258099BBL;
                for (g_691 = 0; (g_691 <= 5); g_691 += 1)
                { /* block id: 622 */
                    const int32_t *l_1455 = (void*)0;
                    const int32_t **l_1454[8][6][1] = {{{&l_1455},{(void*)0},{(void*)0},{(void*)0},{&l_1455},{&l_1455}},{{&l_1455},{(void*)0},{(void*)0},{&l_1455},{&l_1455},{&l_1455}},{{(void*)0},{(void*)0},{(void*)0},{&l_1455},{&l_1455},{&l_1455}},{{(void*)0},{(void*)0},{&l_1455},{&l_1455},{&l_1455},{(void*)0}},{{(void*)0},{(void*)0},{&l_1455},{&l_1455},{&l_1455},{(void*)0}},{{(void*)0},{&l_1455},{&l_1455},{&l_1455},{(void*)0},{(void*)0}},{{(void*)0},{&l_1455},{&l_1455},{&l_1455},{&l_1455},{&l_1455}},{{(void*)0},{&l_1455},{(void*)0},{(void*)0},{&l_1455},{(void*)0}}};
                    int32_t *l_1471[7];
                    int16_t l_1477 = 0L;
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_1471[i] = (void*)0;
                    l_1349[l_1341] ^= ((safe_mod_func_int64_t_s_s(((l_1456 = (void*)0) != ((l_1457 != l_1458) , l_1459)), (safe_rshift_func_uint16_t_u_u(((safe_sub_func_uint32_t_u_u(g_268[g_249], ((g_659[g_249] & (safe_rshift_func_int8_t_s_s((g_67[g_691] <= g_283[g_249][(g_249 + 1)][(g_691 + 4)]), (+(safe_sub_func_int64_t_s_s(((p_17 || 9UL) & (*l_1331)), (**g_1247))))))) , p_16))) && l_1346), p_17)))) ^ g_236[0][4][2]);
                    if (l_1469)
                        continue;
                    for (p_16 = 4; (p_16 >= 0); p_16 -= 1)
                    { /* block id: 628 */
                        int i, j, k;
                        if (l_1352[g_249][g_249][g_249])
                            break;
                        return (*l_1331);
                    }
                    l_1483--;
                }
            }
            if (l_1482)
                break;
            (*l_1331) = (safe_add_func_float_f_f(0x7.03B3BAp-42, ((((-7L) > p_17) , l_1488) == (l_1490 = &g_1176))));
        }
    }
    (*g_1492) = l_1491;
    if (((l_1495 != l_1495) || ((safe_rshift_func_int8_t_s_s((0L != (((safe_lshift_func_int8_t_s_u((((*l_1365) ^= (((void*)0 != &g_1493) < ((((safe_mod_func_uint32_t_u_u(((*g_666) , (((1UL && (*g_1318)) <= (safe_add_func_int32_t_s_s((safe_mod_func_int64_t_s_s(p_17, p_17)), p_17))) & 0xFEL)), l_1345)) , l_1341) , (-1L)) == p_17))) ^ l_1349[0]), p_16)) | 250UL) < l_1506)), l_1507)) == l_1341)))
    { /* block id: 642 */
        float l_1510 = 0x4.6DFFB3p-18;
        int32_t l_1511[8][7] = {{(-1L),0x4E40D1E2L,4L,4L,0x4E40D1E2L,(-1L),9L},{(-1L),4L,0x7A7D88B1L,(-1L),0x0737D152L,(-7L),(-8L)},{4L,(-9L),(-8L),0x37C4C8C7L,0x42B5B277L,0xF781EA21L,0x0737D152L},{0x42B5B277L,4L,0x0E41006AL,0xBC48DC11L,0xBC48DC11L,0x0E41006AL,4L},{9L,0x4E40D1E2L,0x0E41006AL,0xF781EA21L,0xBA74BC55L,0xBC48DC11L,0x7A7D88B1L},{(-1L),(-1L),(-8L),0x48086298L,0x7F74E6B7L,1L,0x37C4C8C7L},{0x0E41006AL,0x48086298L,0x7A7D88B1L,0xF781EA21L,(-8L),(-8L),0xF781EA21L},{4L,0x37C4C8C7L,4L,0xBC48DC11L,(-8L),0x42B5B277L,9L}};
        int8_t l_1614 = 8L;
        uint32_t **l_1659 = &g_72;
        int16_t **l_1666 = (void*)0;
        int16_t ***l_1665 = &l_1666;
        uint32_t **l_1679 = &g_975;
        int32_t l_1684[1];
        int32_t **l_1696 = &g_311;
        int32_t ***l_1695 = &l_1696;
        int i, j;
        for (i = 0; i < 1; i++)
            l_1684[i] = 0xD3FA9016L;
        for (g_1339 = 0; (g_1339 <= 9); g_1339++)
        { /* block id: 645 */
            const uint16_t * const ** const **l_1521 = &l_1519;
            int32_t l_1524[2];
            int16_t *l_1525 = &l_1506;
            int8_t ** const l_1526 = (void*)0;
            int8_t *l_1556 = &g_294;
            int32_t l_1637 = 0x64E70C80L;
            uint32_t **l_1656[5][9][5] = {{{&g_72,(void*)0,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,(void*)0,&g_72,(void*)0},{(void*)0,&g_72,(void*)0,&g_72,&g_72},{&g_72,(void*)0,&g_72,&g_72,(void*)0},{&g_72,(void*)0,&g_72,&g_72,&g_72},{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72}},{{(void*)0,&g_72,&g_72,(void*)0,(void*)0},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,(void*)0,&g_72,&g_72,&g_72},{&g_72,(void*)0,&g_72,&g_72,(void*)0},{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,(void*)0,&g_72},{&g_72,(void*)0,&g_72,&g_72,(void*)0},{&g_72,(void*)0,(void*)0,&g_72,&g_72}},{{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,(void*)0},{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,(void*)0,(void*)0,&g_72,&g_72},{&g_72,(void*)0,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,(void*)0,&g_72,(void*)0},{(void*)0,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,(void*)0},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,(void*)0,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,(void*)0},{&g_72,&g_72,&g_72,(void*)0,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,&g_72,&g_72,(void*)0},{&g_72,&g_72,(void*)0,&g_72,&g_72},{(void*)0,&g_72,&g_72,(void*)0,&g_72},{&g_72,&g_72,&g_72,&g_72,(void*)0},{(void*)0,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,(void*)0}}};
            int16_t *** const l_1664 = (void*)0;
            uint64_t **l_1667 = (void*)0;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1524[i] = 0x7F119D8DL;
            if ((l_1511[4][1] && (safe_sub_func_uint16_t_u_u(((void*)0 != &p_16), ((g_236[2][0][4] > 0x25E6L) && ((*l_1525) = ((safe_mod_func_int8_t_s_s(l_1511[4][1], (~((&g_1421[4] == ((*l_1521) = l_1519)) | (((((safe_lshift_func_uint8_t_u_u((l_1511[7][0] >= l_1524[0]), p_16)) <= 1L) || l_1511[1][5]) >= 0x34L) < (*g_571)))))) || p_17)))))))
            { /* block id: 648 */
                uint32_t *** const *l_1534 = &g_329;
                uint64_t l_1539[5][9] = {{18446744073709551615UL,0UL,0UL,18446744073709551615UL,0x6420F2C7EB3038FCLL,18446744073709551615UL,0x6420F2C7EB3038FCLL,18446744073709551615UL,0UL},{0x6420F2C7EB3038FCLL,0x6420F2C7EB3038FCLL,0xD143BDB4652F03B5LL,0xFA7A968724A37879LL,18446744073709551611UL,18446744073709551615UL,0x5427CCF4AE37972FLL,0x6420F2C7EB3038FCLL,0x231589B181D1E926LL},{0UL,18446744073709551615UL,0x231589B181D1E926LL,0xFA7A968724A37879LL,0x6420F2C7EB3038FCLL,0x25FAAA2E148E4490LL,0UL,0UL,0x25FAAA2E148E4490LL},{0xFA7A968724A37879LL,18446744073709551615UL,0xD143BDB4652F03B5LL,18446744073709551615UL,0xFA7A968724A37879LL,0UL,18446744073709551615UL,0xFA7A968724A37879LL,0x25FAAA2E148E4490LL},{0UL,0x6420F2C7EB3038FCLL,0UL,18446744073709551615UL,0x5427CCF4AE37972FLL,0xD143BDB4652F03B5LL,18446744073709551615UL,0UL,0x231589B181D1E926LL}};
                int32_t l_1553[6][10][2] = {{{(-1L),(-1L)},{0x8E7D1EE2L,0x944F3BE2L},{(-1L),0x8E7D1EE2L},{(-1L),0x944F3BE2L},{0x8E7D1EE2L,(-1L)},{(-1L),0L},{(-1L),(-1L)},{0x8E7D1EE2L,0x944F3BE2L},{(-1L),0x8E7D1EE2L},{(-1L),0x944F3BE2L}},{{0x8E7D1EE2L,(-1L)},{(-1L),0L},{(-1L),(-1L)},{0x8E7D1EE2L,0x944F3BE2L},{(-1L),0x8E7D1EE2L},{(-1L),0x944F3BE2L},{0x8E7D1EE2L,(-1L)},{(-1L),0L},{(-1L),(-1L)},{0x8E7D1EE2L,0x944F3BE2L}},{{(-1L),0x8E7D1EE2L},{(-1L),0x944F3BE2L},{0x8E7D1EE2L,(-1L)},{(-1L),0x4E0E23B4L},{0x8E7D1EE2L,0x8E7D1EE2L},{0L,0L},{0x8E7D1EE2L,0L},{0x8E7D1EE2L,0L},{0L,0x8E7D1EE2L},{0x8E7D1EE2L,0x4E0E23B4L}},{{0x8E7D1EE2L,0x8E7D1EE2L},{0L,0L},{0x8E7D1EE2L,0L},{0x8E7D1EE2L,0L},{0L,0x8E7D1EE2L},{0x8E7D1EE2L,0x4E0E23B4L},{0x8E7D1EE2L,0x8E7D1EE2L},{0L,0L},{0x8E7D1EE2L,0L},{0x8E7D1EE2L,0L}},{{0L,0x8E7D1EE2L},{0x8E7D1EE2L,0x4E0E23B4L},{0x8E7D1EE2L,0x8E7D1EE2L},{0L,0L},{0x8E7D1EE2L,0L},{0x8E7D1EE2L,0L},{0L,0x8E7D1EE2L},{0x8E7D1EE2L,0x4E0E23B4L},{0x8E7D1EE2L,0x8E7D1EE2L},{0L,0L}},{{0x8E7D1EE2L,0L},{0x8E7D1EE2L,0L},{0L,0x8E7D1EE2L},{0x8E7D1EE2L,0x4E0E23B4L},{0x8E7D1EE2L,0x8E7D1EE2L},{0L,0L},{0x8E7D1EE2L,0L},{0x8E7D1EE2L,0L},{0L,0x8E7D1EE2L},{0x8E7D1EE2L,0x4E0E23B4L}}};
                int32_t *l_1554[9] = {&g_132,&g_132,&g_132,&g_132,&g_132,&g_132,&g_132,&g_132,&g_132};
                int i, j, k;
                g_1527 = l_1526;
                (*g_1528) = ((-1L) > l_1511[2][2]);
                l_1511[4][1] ^= (!((safe_rshift_func_int16_t_s_s((~(l_1524[0] = (((*g_1107) = ((**g_974) , l_1533)) != l_1534))), g_44)) != (safe_div_func_uint32_t_u_u(p_15, (safe_mul_func_uint16_t_u_u((g_216[1][1][2] >= (l_1539[0][2] , (safe_div_func_uint32_t_u_u(((safe_lshift_func_int8_t_s_u(((safe_add_func_int32_t_s_s((safe_div_func_uint8_t_u_u((+((p_16 , (safe_rshift_func_uint8_t_u_s(((safe_sub_func_uint8_t_u_u((0x2BL || 0x2DL), g_334)) == p_16), 4))) <= l_1349[3])), p_15)), l_1539[3][8])) < g_294), 5)) || 1L), p_15)))), l_1553[1][0][1]))))));
            }
            else
            { /* block id: 654 */
                int32_t *l_1555 = &l_1349[3];
                (*l_1555) ^= p_16;
            }
            if ((0UL || (((*l_1556) ^= l_1524[0]) < g_236[3][3][1])))
            { /* block id: 658 */
                int32_t l_1608 = (-10L);
                uint32_t **l_1661 = &g_72;
                for (l_1345 = 2; (l_1345 >= 0); l_1345 -= 1)
                { /* block id: 661 */
                    int32_t *l_1557 = (void*)0;
                    int32_t *l_1558 = &g_293;
                    int32_t *l_1559 = &g_283[1][1][7];
                    int32_t *l_1560 = &g_293;
                    int32_t *l_1561 = (void*)0;
                    int32_t *l_1562 = (void*)0;
                    int32_t *l_1563 = &l_1346;
                    int32_t *l_1564 = &g_283[1][1][1];
                    int32_t *l_1565[10][4] = {{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345},{&l_1349[4],&l_1345,&l_1349[4],&l_1345}};
                    const uint16_t *l_1571 = &g_1572;
                    int i, j;
                    ++l_1566;
                    if (g_73[(l_1345 + 5)][l_1345])
                        break;
                    for (g_294 = 0; (g_294 <= 2); g_294 += 1)
                    { /* block id: 666 */
                        const uint16_t *l_1570 = &g_223;
                        const uint16_t **l_1569[9] = {&l_1570,&l_1570,&l_1570,&l_1570,&l_1570,&l_1570,&l_1570,&l_1570,&l_1570};
                        int32_t l_1573 = 1L;
                        int i;
                        (*l_1559) ^= p_17;
                        l_1573 |= ((***g_1420) != (l_1571 = (*g_582)));
                        (*l_1563) &= (!(0x9F3BL <= l_1573));
                    }
                }
                for (g_44 = 0; (g_44 <= 1); g_44 += 1)
                { /* block id: 675 */
                    int16_t *l_1581 = &g_249;
                    int32_t *l_1583[3];
                    int32_t *l_1584 = &l_1345;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1583[i] = &g_785;
                    (*l_1584) = (safe_rshift_func_uint16_t_u_s(((((((l_1349[3] = g_659[g_44]) , p_15) , (safe_mod_func_int16_t_s_s(((*l_1525) &= (safe_div_func_uint64_t_u_u(0x868DBABF6E74C1A6LL, p_16))), ((*l_1581) = l_1511[3][5])))) , l_1347) , (safe_unary_minus_func_uint64_t_u(p_16))) & ((((g_785 = ((void*)0 != &p_16)) , 0L) , (-1L)) == 0xD5L)), p_15));
                }
                for (g_94 = 0; (g_94 > 39); g_94 = safe_add_func_uint32_t_u_u(g_94, 1))
                { /* block id: 684 */
                    int32_t l_1615 = 0xB368FAC6L;
                    uint32_t *l_1635 = &g_94;
                    int32_t l_1638[10][5] = {{0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L},{0xC43347FBL,0xDFAB6DE7L,0xC43347FBL,0xDFAB6DE7L,0xC43347FBL},{0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L},{0xC43347FBL,0xDFAB6DE7L,0xC43347FBL,0xDFAB6DE7L,0xC43347FBL},{0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L},{0xC43347FBL,0xDFAB6DE7L,0xC43347FBL,0xDFAB6DE7L,0xC43347FBL},{0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L},{0xC43347FBL,0xDFAB6DE7L,0xC43347FBL,0xDFAB6DE7L,0xC43347FBL},{0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L,0xB223D1D1L},{0xC43347FBL,0xDFAB6DE7L,0xC43347FBL,0xDFAB6DE7L,0xC43347FBL}};
                    uint32_t **l_1658 = &g_72;
                    uint32_t ***l_1657 = &l_1658;
                    int8_t *l_1660[3];
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1660[i] = &g_1424;
                    for (g_293 = 0; (g_293 > 18); g_293 = safe_add_func_int8_t_s_s(g_293, 4))
                    { /* block id: 687 */
                        int16_t l_1609 = 4L;
                        int32_t *l_1616 = &l_1345;
                        int16_t *l_1633 = (void*)0;
                        int16_t *l_1634 = &l_1609;
                        int32_t *l_1639 = &l_1511[0][5];
                        (*l_1616) |= ((safe_div_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s((safe_rshift_func_uint8_t_u_s(((((((l_1524[0] == (+(safe_add_func_uint16_t_u_u(l_1524[0], (p_15 < (((0x9F07612EL < (safe_sub_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u(((safe_add_func_uint16_t_u_u(((((((safe_mod_func_int64_t_s_s((0L > l_1608), l_1609)) < (1L != (safe_div_func_uint64_t_u_u((safe_mod_func_uint64_t_u_u(((p_16 , 18446744073709551608UL) , p_15), (**g_270))), p_17)))) & 0x3F4F5EE90A7D1B70LL) | g_1572) > 0x9A1EL) ^ 251UL), 0x7E31L)) == 0xD666L), l_1609)), 6)) , l_1511[4][1]), 0x0B9FL))) >= g_1572) ^ p_17)))))) <= l_1608) ^ 255UL) > l_1347) || l_1614) ^ 0xB92EEDA57F76A42BLL), g_1366)), 3)), l_1615)) ^ p_15);
                        (*l_1639) &= (l_1638[5][0] &= (((void*)0 == l_1617) | (safe_unary_minus_func_int8_t_s(((*l_1556) = ((((safe_rshift_func_int16_t_s_u((p_16 >= ((((((l_1524[0] = 0x6D291E83L) , func_50(((safe_rshift_func_uint16_t_u_s(((((*l_1616) = (safe_mul_func_int16_t_s_s(((*l_1525) = 6L), (g_249 = ((*l_1634) ^= ((1L | ((safe_lshift_func_int8_t_s_s((((*g_96) >= (((safe_lshift_func_uint8_t_u_u(((***l_1617) >= (1UL >= l_1524[1])), 0)) ^ l_1615) != 0x6C28BEADL)) <= g_223), 3)) > g_1285)) <= 0xEFL)))))) , g_334) , p_16), 10)) , (*l_1616)), p_17, l_1635)) != (void*)0) <= p_17) < g_1636) , 3UL)), l_1637)) ^ 255UL) , g_70) || 0xB6E7L))))));
                        if (p_16)
                            continue;
                        g_1640 = (*l_1618);
                    }
                    (*g_1662) ^= (((safe_sub_func_uint32_t_u_u((l_1511[4][4] == ((*l_1556) = p_17)), ((g_1424 = ((safe_mod_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u((safe_unary_minus_func_uint16_t_u(((l_1648 != (void*)0) , (l_1608 = (--(*l_1365)))))), (safe_sub_func_int16_t_s_s(p_17, ((~(safe_sub_func_int32_t_s_s(p_15, (((*l_1657) = (l_1656[1][4][0] = &l_1635)) == l_1659)))) >= l_1524[0]))))) ^ (**g_1247)), (**l_1618))) & p_15)) , p_17))) , l_1661) != (void*)0);
                }
            }
            else
            { /* block id: 708 */
                int32_t *l_1663 = (void*)0;
                l_1349[3] &= l_1524[0];
            }
            l_1665 = l_1664;
            (*g_1662) &= ((void*)0 == l_1667);
        }
        for (g_1373 = 5; (g_1373 > 7); g_1373 = safe_add_func_int8_t_s_s(g_1373, 7))
        { /* block id: 716 */
            return (***l_1617);
        }
        (*g_666) = (*g_666);
        for (g_1339 = 0; (g_1339 < (-1)); --g_1339)
        { /* block id: 722 */
            uint8_t **l_1674 = &l_1648;
            uint32_t ***l_1680 = (void*)0;
            uint32_t ***l_1681 = &l_1679;
            int16_t *l_1683[3][7][4] = {{{&l_1506,&g_1339,&l_1506,&l_1506},{&l_1506,&l_1506,(void*)0,&l_1506},{(void*)0,&g_1339,&l_1506,&g_1339},{&l_1506,(void*)0,&g_1339,&l_1506},{&l_1506,(void*)0,&g_249,&g_1339},{(void*)0,&l_1506,&g_249,&g_249},{&l_1506,&l_1506,&g_1339,&g_249}},{{&l_1506,&l_1506,&l_1506,&g_1339},{&l_1506,(void*)0,&g_1339,&l_1506},{&l_1506,(void*)0,&g_249,&g_1339},{(void*)0,&l_1506,&g_249,&g_249},{&l_1506,&l_1506,&g_1339,&g_249},{&l_1506,&l_1506,&l_1506,&g_1339},{&l_1506,(void*)0,&g_1339,&l_1506}},{{&l_1506,(void*)0,&g_249,&g_1339},{(void*)0,&l_1506,&g_249,&g_249},{&l_1506,&l_1506,&g_1339,&g_249},{&l_1506,&l_1506,&l_1506,&g_1339},{&l_1506,(void*)0,&g_1339,&l_1506},{&l_1506,(void*)0,&g_249,&g_1339},{(void*)0,&l_1506,&g_249,&g_249}}};
            int32_t l_1685 = 1L;
            int32_t l_1699 = 2L;
            int i, j, k;
            l_1685 &= (((l_1673 , &g_691) == ((*l_1674) = &g_216[1][2][4])) ^ ((l_1684[0] &= (((p_15 | (safe_sub_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((65535UL & ((((*l_1681) = l_1679) != (g_1682[6][3][0] = &g_975)) && (l_1511[4][1] || (*l_1619)))), g_249)), 1L))) > 0UL) == 0xB2BF49A2BC5E0ABALL)) > g_283[0][1][8]));
            for (g_1371 = (-4); (g_1371 == 18); g_1371 = safe_add_func_int8_t_s_s(g_1371, 1))
            { /* block id: 730 */
                int32_t * const * const **l_1694 = (void*)0;
                int32_t l_1706 = 1L;
                int32_t *l_1707 = (void*)0;
                int8_t * const l_1714 = &l_1614;
                float *l_1726 = &g_788[0];
                float *l_1727 = &g_85;
                l_1685 &= (((l_1511[4][1] > (safe_mod_func_int32_t_s_s((safe_mod_func_uint32_t_u_u(p_17, (((((g_1692 = g_1692) != l_1695) && ((safe_div_func_uint16_t_u_u((l_1699 >= ((**l_1618) , ((((((*g_96) = ((p_15 , p_16) >= (safe_mod_func_uint32_t_u_u(((*g_72) |= ((safe_sub_func_int8_t_s_s((safe_mul_func_int8_t_s_s(l_1706, p_15)), 0xC4L)) , l_1699)), p_17)))) , (***l_1617)) <= (*g_1097)) , &l_1519) == (void*)0))), 1UL)) && g_268[0])) && p_16) , p_17))), 4294967291UL))) && 9L) | 0x2A76FA2AL);
                (*l_1727) = ((((safe_div_func_float_f_f((safe_sub_func_float_f_f(((*l_1726) = ((+((!0x1.9p+1) >= (l_1714 == (p_16 , (void*)0)))) > ((((safe_mul_func_float_f_f(((*g_666) = (safe_div_func_float_f_f((!(*g_666)), (**g_665)))), (safe_div_func_float_f_f((***l_1617), (l_1685 = (safe_add_func_float_f_f((safe_sub_func_float_f_f(0xE.A6EFB0p-61, 0x1.3p+1)), 0x7.455907p-33))))))) > (***l_1617)) > 0x1.60CCC4p+19) > 0x1.6p-1))), 0xA.99F107p-80)), (-0x8.8p-1))) == (**l_1618)) == l_1699) == (-0x1.0p-1));
                l_1347 = 0L;
            }
            (**l_1695) = ((((**g_665) = 0x1.2p-1) <= (safe_add_func_float_f_f((safe_add_func_float_f_f(p_15, (**l_1618))), 0x7.C42C74p-73))) , (**l_1695));
        }
    }
    else
    { /* block id: 744 */
        int8_t l_1736 = 0xA6L;
        const uint16_t ***l_1745 = (void*)0;
        const uint16_t ****l_1744 = &l_1745;
        const uint16_t *****l_1743[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
        const uint16_t *****l_1746 = &l_1744;
        int32_t l_1753 = 1L;
        uint32_t l_1754 = 18446744073709551608UL;
        int32_t l_1761 = 0xAF0F3C8FL;
        int32_t l_1763[6][7][2] = {{{0xC46A3B8EL,0xE9726E5EL},{0x3540CBA3L,0xE9726E5EL},{0xC46A3B8EL,0x3F45CBFFL},{0x3F45CBFFL,0xC46A3B8EL},{0xE9726E5EL,0x3540CBA3L},{0xE9726E5EL,0xC46A3B8EL},{0x3F45CBFFL,0x3F45CBFFL}},{{0xC46A3B8EL,0xE9726E5EL},{0x3540CBA3L,0xE9726E5EL},{0xC46A3B8EL,0x3F45CBFFL},{0x3F45CBFFL,0xC46A3B8EL},{0xE9726E5EL,0x3540CBA3L},{0xE9726E5EL,0xC46A3B8EL},{0x3F45CBFFL,0x3F45CBFFL}},{{0xC46A3B8EL,0xE9726E5EL},{0x3540CBA3L,0xE9726E5EL},{0xC46A3B8EL,0x3F45CBFFL},{0x3F45CBFFL,0xC46A3B8EL},{0xE9726E5EL,0x3540CBA3L},{0xE9726E5EL,0xC46A3B8EL},{0x3F45CBFFL,0x3F45CBFFL}},{{0xC46A3B8EL,0xE9726E5EL},{0x3540CBA3L,0xE9726E5EL},{0xC46A3B8EL,0x3F45CBFFL},{0x3F45CBFFL,0xC46A3B8EL},{0xE9726E5EL,0x3540CBA3L},{0xE9726E5EL,0xC46A3B8EL},{0x3F45CBFFL,0x3F45CBFFL}},{{0xC46A3B8EL,0xE9726E5EL},{0x3540CBA3L,0xE9726E5EL},{0xC46A3B8EL,0x3F45CBFFL},{0x3F45CBFFL,0xC46A3B8EL},{0xE9726E5EL,0x3540CBA3L},{0xE9726E5EL,0xC46A3B8EL},{0x3F45CBFFL,0x3F45CBFFL}},{{0xC46A3B8EL,0xE9726E5EL},{0x3540CBA3L,0xE9726E5EL},{0xC46A3B8EL,0x3F45CBFFL},{0x3F45CBFFL,0xC46A3B8EL},{0xE9726E5EL,0x3540CBA3L},{0xE9726E5EL,0xC46A3B8EL},{0x3F45CBFFL,0x3F45CBFFL}}};
        int32_t l_1777 = (-1L);
        int64_t *l_1802 = &g_189[0];
        uint32_t l_1821 = 1UL;
        uint8_t **l_1873 = &l_1648;
        int16_t l_1890[5] = {0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L,0x1CA9L};
        int32_t l_1892 = 0L;
        float **l_1928 = (void*)0;
        float ***l_1927 = &l_1928;
        int32_t l_1998 = 0L;
        int i, j, k;
        if (((safe_mod_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u(((0xC3EE57AAL ^ (((((l_1736 , ((**g_665) = (safe_add_func_float_f_f((safe_add_func_float_f_f((((*g_1494) , (p_15 , 0x562D1F32L)) , ((((((((safe_mod_func_int16_t_s_s(((((l_1746 = l_1743[2]) == (l_1750 = g_1747)) & (l_1754 = ((safe_lshift_func_int16_t_s_s(g_268[1], ((((l_1753 = p_15) , 0xEE4DA421L) != p_16) && 1L))) , (-1L)))) , p_16), (-1L))) < (*l_1619)) || p_16) || (*g_1318)) , p_16) , 0x6.1490D3p+14) == (***l_1617)) != l_1736)), (**g_665))), (*g_666))))) < 0xD.C52368p-48) , p_15) , &l_1533) != &l_1533)) | p_15), p_15)), (**g_1175))) >= g_79))
        { /* block id: 750 */
            int32_t *l_1755 = &l_1349[3];
            int32_t *l_1756 = (void*)0;
            int32_t *l_1757 = &l_1753;
            int32_t *l_1758 = &l_1347;
            int32_t *l_1759 = &g_132;
            int32_t *l_1760[5] = {&g_132,&g_132,&g_132,&g_132,&g_132};
            uint16_t *****l_1794 = (void*)0;
            int32_t l_1827 = 6L;
            uint32_t l_1858 = 4294967289UL;
            uint64_t **l_1868 = (void*)0;
            float l_1885 = 0x0.9p-1;
            uint16_t **l_1914 = &g_560;
            int8_t *l_1924 = &l_1762;
            uint8_t **l_1930 = (void*)0;
            int i;
            ++l_1764;
            for (p_15 = 0; (p_15 <= 7); ++p_15)
            { /* block id: 754 */
                int8_t *l_1776[7] = {&g_294,&g_1424,&g_294,&g_294,&g_1424,&g_294,&g_294};
                int32_t l_1797 = 0x9EE13C03L;
                int64_t *l_1801[8] = {&g_189[1],&l_1341,&g_189[1],&l_1341,&g_189[1],&l_1341,&g_189[1],&l_1341};
                int32_t *l_1824[2];
                int32_t l_1840 = 0xF41949FAL;
                float l_1891 = 0xA.766F0Ep-39;
                uint16_t ****l_1901 = &g_1749;
                int i;
                for (i = 0; i < 2; i++)
                    l_1824[i] = &l_1761;
                (*l_1759) = 1L;
            }
            (**l_1617) = l_1757;
        }
        else
        { /* block id: 841 */
            int32_t l_1977[6][7][6] = {{{0x0619C7DBL,8L,0x4F853AECL,0x05B8A1D7L,0xECFBA858L,0x05B8A1D7L},{6L,(-10L),3L,0xDB42F1B2L,0xECFBA858L,1L},{0x0E85A070L,0x1BDE29FEL,0x0619C7DBL,(-8L),0x23A201CEL,0x1CC37996L},{7L,0L,(-9L),(-5L),0x0E85A070L,(-6L)},{1L,0xB84CA629L,7L,0x0E85A070L,0x0619C7DBL,0L},{(-6L),0xB32D8715L,0x1CC37996L,0xB32D8715L,(-6L),(-10L)},{8L,0x4C676E36L,(-5L),0x1BDE29FEL,6L,1L}},{{0x5E69E2C4L,(-8L),(-1L),0x4C676E36L,0xB320FF7CL,1L},{0xB9780F65L,0x77EA1799L,(-5L),(-1L),0xB84CA629L,(-10L)},{0xB320FF7CL,0xECFBA858L,0x1CC37996L,0xB9780F65L,0x4C676E36L,0L},{0x584B2E2EL,0x146C183BL,7L,1L,0x5BFE806BL,(-6L)},{0x1CC37996L,(-1L),(-9L),(-9L),(-1L),0x1CC37996L},{(-1L),(-6L),0x0619C7DBL,0xECFBA858L,7L,1L},{0x77EA1799L,0x0E85A070L,3L,(-1L),1L,0x05B8A1D7L}},{{0x77EA1799L,(-1L),(-1L),0xECFBA858L,0x584B2E2EL,0x5E69E2C4L},{(-1L),7L,0L,(-9L),(-5L),0x0E85A070L},{0x1CC37996L,0x05B8A1D7L,(-1L),1L,9L,(-1L)},{0x584B2E2EL,0xF9542E97L,0x05B8A1D7L,0xB9780F65L,0x05B8A1D7L,0xF9542E97L},{0xB320FF7CL,(-9L),0x584B2E2EL,(-1L),(-10L),0x4F853AECL},{0xB9780F65L,1L,(-6L),0x4C676E36L,(-8L),0x5BFE806BL},{0x5E69E2C4L,1L,1L,0x1BDE29FEL,(-10L),0xB320FF7CL}},{{8L,(-9L),0x4C676E36L,0xB32D8715L,0x05B8A1D7L,7L},{(-6L),0xF9542E97L,0x5E69E2C4L,0x0E85A070L,9L,0xB32D8715L},{1L,0x05B8A1D7L,7L,(-5L),(-5L),7L},{7L,7L,0x146C183BL,(-8L),0x584B2E2EL,9L},{0x0E85A070L,(-1L),0xB9780F65L,0xDB42F1B2L,1L,0x146C183BL},{6L,0x0E85A070L,0xB9780F65L,(-6L),7L,9L},{0xB84CA629L,(-6L),0x146C183BL,(-10L),(-1L),7L}},{{(-10L),(-1L),7L,0x23A201CEL,0x5BFE806BL,0xB32D8715L},{(-9L),0x146C183BL,0x5E69E2C4L,7L,0x4C676E36L,7L},{0x4C676E36L,0xECFBA858L,0x4C676E36L,0x4F853AECL,0xB84CA629L,0xB320FF7CL},{0xDB42F1B2L,0x77EA1799L,1L,0x5E69E2C4L,0xB320FF7CL,0x5BFE806BL},{(-1L),(-8L),0x5E69E2C4L,0x584B2E2EL,0xECFBA858L,(-1L)},{0x23A201CEL,0xB320FF7CL,8L,(-1L),0x5E69E2C4L,0L},{0xB320FF7CL,7L,(-6L),(-9L),0xB84CA629L,(-1L)}},{{(-10L),0xB32D8715L,1L,(-8L),0x05B8A1D7L,0x05B8A1D7L},{0x146C183BL,7L,7L,0x146C183BL,(-8L),0x584B2E2EL},{0xB32D8715L,9L,0x0E85A070L,0x5E69E2C4L,0xF9542E97L,(-6L)},{0xECFBA858L,0x146C183BL,6L,0x23A201CEL,0xF9542E97L,1L},{0x05B8A1D7L,9L,0xB84CA629L,0x5BFE806BL,(-8L),0x4C676E36L},{0x1CC37996L,7L,(-10L),0xDB42F1B2L,0x05B8A1D7L,0x5E69E2C4L},{1L,0xB32D8715L,(-9L),0x05B8A1D7L,0xB84CA629L,7L}}};
            uint8_t ***l_1986 = &l_1873;
            int32_t *l_1997[10] = {&l_1777,(void*)0,&l_1777,&l_1777,(void*)0,&l_1777,&l_1777,(void*)0,&l_1777,&l_1777};
            uint32_t l_2003 = 0xD347B671L;
            int i, j, k;
            l_1349[3] = (safe_sub_func_uint16_t_u_u(((((0L <= p_16) , ((((((safe_unary_minus_func_uint64_t_u(((~(l_1977[1][6][4] , (safe_div_func_uint8_t_u_u(((l_1763[5][0][1] |= (((safe_mul_func_int16_t_s_s(((0x3998L && ((safe_lshift_func_int16_t_s_u((safe_mul_func_int8_t_s_s((((*l_1986) = (*g_1874)) == (void*)0), (safe_mul_func_uint16_t_u_u((((safe_sub_func_int32_t_s_s(((((l_1347 ^= (safe_rshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_s(((0x4C5BL || ((g_293 | ((safe_mul_func_float_f_f(((l_1890[4] , (*g_666)) == (**g_665)), 0x5.419CFAp-35)) , p_17)) || (**g_1933))) > p_17), (*l_1619))), 2))) == (*g_1934)) <= l_1821) == p_17), 4294967290UL)) && l_1977[1][4][0]) , l_1977[2][0][3]), (*g_1934))))), p_16)) , l_1977[1][3][1])) != p_15), p_17)) >= (*g_1176)) ^ (*g_1934))) <= p_16), l_1753)))) && 65535UL))) | l_1890[1]) != g_189[0]) , (void*)0) != (void*)0) > 0xB938L)) >= (*g_1934)) == p_15), 0x2937L));
            l_1999--;
            l_2003++;
            (*g_666) = (*g_666);
        }
        for (g_1372 = 0; (g_1372 <= 22); ++g_1372)
        { /* block id: 852 */
            (**g_665) = (*g_666);
        }
        (**g_665) = (*g_1097);
    }
    return p_16;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t  func_22(int16_t  p_23, int32_t  p_24, uint8_t  p_25, int8_t  p_26)
{ /* block id: 561 */
    return p_26;
}


/* ------------------------------------------ */
/* 
 * reads : g_73
 * writes:
 */
static int16_t  func_27(uint32_t  p_28)
{ /* block id: 1 */
    int32_t l_29[9][9] = {{0L,0xA4CCFD66L,0xBDAE4F9DL,0xA4CCFD66L,0L,0xF906D8BCL,0L,0xA4CCFD66L,0xBDAE4F9DL},{1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L},{0L,0xA4CCFD66L,0xBDAE4F9DL,0xA4CCFD66L,0L,0xF906D8BCL,0L,0xA4CCFD66L,0xBDAE4F9DL},{1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L},{0L,0xA4CCFD66L,0xBDAE4F9DL,0xA4CCFD66L,0L,0xF906D8BCL,0L,0xA4CCFD66L,0xBDAE4F9DL},{1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L},{0L,0xA4CCFD66L,0xBDAE4F9DL,0xA4CCFD66L,0L,0xF906D8BCL,0L,0xA4CCFD66L,0xBDAE4F9DL},{1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L,1L,1L,0xC0FAAB39L},{0L,0xA4CCFD66L,0xBDAE4F9DL,0xA4CCFD66L,0L,0xF906D8BCL,0L,0xA4CCFD66L,0xBDAE4F9DL}};
    int32_t l_45 = 0L;
    uint32_t l_1100[5] = {0UL,0UL,0UL,0UL,0UL};
    int i, j;
    for (p_28 = 0; (p_28 <= 8); p_28 += 1)
    { /* block id: 4 */
        int16_t l_37 = 0x0E70L;
        uint16_t *l_38 = &g_39[8][2][4];
        uint32_t *l_42 = (void*)0;
        uint32_t *l_43[6][5] = {{(void*)0,&g_44,(void*)0,(void*)0,&g_44},{&g_44,(void*)0,(void*)0,&g_44,(void*)0},{&g_44,&g_44,(void*)0,&g_44,&g_44},{(void*)0,&g_44,(void*)0,(void*)0,&g_44},{&g_44,(void*)0,(void*)0,&g_44,(void*)0},{&g_44,&g_44,(void*)0,&g_44,&g_44}};
        uint32_t l_58 = 9UL;
        uint16_t *l_68 = (void*)0;
        uint16_t *l_69[4][9] = {{(void*)0,&g_70,(void*)0,(void*)0,&g_70,&g_70,(void*)0,&g_70,&g_70},{&g_70,&g_70,&g_70,(void*)0,(void*)0,&g_70,&g_70,&g_70,&g_70},{(void*)0,(void*)0,&g_70,&g_70,(void*)0,(void*)0,&g_70,(void*)0,(void*)0},{&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,&g_70}};
        int32_t l_71 = (-3L);
        uint16_t **l_557 = (void*)0;
        uint16_t **l_558 = &l_69[2][3];
        uint16_t **l_559[5];
        uint8_t *l_1098 = (void*)0;
        uint8_t *l_1099 = &g_691;
        int32_t l_1319 = (-1L);
        int32_t *l_1320 = &g_283[1][2][7];
        int i, j;
        for (i = 0; i < 5; i++)
            l_559[i] = &l_68;
    }
    return g_73[0][2];
}


/* ------------------------------------------ */
/* 
 * reads : g_334 g_691 g_560 g_70 g_1171 g_44 g_571 g_283 g_216 g_249 g_1175 g_132 g_311 g_279 g_1191 g_1176 g_1217 g_331 g_332 g_333 g_1220 g_666 g_654 g_190 g_293 g_94 g_236 g_1318 g_294
 * writes: g_294 g_691 g_189 g_311 g_249 g_94 g_79 g_100 g_190 g_132 g_293
 */
static int16_t  func_30(uint32_t  p_31)
{ /* block id: 437 */
    float l_1159 = 0xC.FB26B5p-93;
    int32_t l_1166 = (-1L);
    int32_t l_1169 = 9L;
    uint16_t l_1170 = 0x0E51L;
    uint64_t * const *l_1177 = (void*)0;
    uint16_t ***l_1202 = (void*)0;
    uint16_t ****l_1201[5] = {&l_1202,&l_1202,&l_1202,&l_1202,&l_1202};
    uint32_t l_1223 = 0x47EAABE4L;
    int64_t *l_1302 = &g_189[0];
    int64_t **l_1301 = &l_1302;
    int i;
lbl_1229:
    for (g_294 = 0; (g_294 < 13); g_294 = safe_add_func_int64_t_s_s(g_294, 2))
    { /* block id: 440 */
        uint32_t l_1156 = 0x0CF3F7A5L;
        uint8_t *l_1160 = &g_691;
        int32_t **l_1221[2][2][1];
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 2; j++)
            {
                for (k = 0; k < 1; k++)
                    l_1221[i][j][k] = &g_311;
            }
        }
        if (((-1L) ^ (g_189[1] = ((l_1156 ^ ((p_31 || 0L) & ((safe_lshift_func_uint8_t_u_u(((*l_1160) |= g_334), 1)) && ((~(l_1156 ^ (safe_rshift_func_uint16_t_u_u((l_1169 |= ((safe_sub_func_int8_t_s_s(l_1166, (safe_mod_func_uint8_t_u_u(0x4AL, 0x33L)))) , (*g_560))), p_31)))) || l_1170)))) , p_31))))
        { /* block id: 444 */
            int16_t *l_1174 = &g_249;
            uint64_t **l_1179[6][4][6] = {{{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176}},{{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176}},{{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176}},{{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176}},{{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176}},{{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176},{&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176}}};
            uint64_t ***l_1178 = &l_1179[5][3][0];
            int i, j, k;
            (*g_1171) = &l_1166;
            (*g_311) = (((1UL < (((0x6BF5L ^ g_44) , (*g_571)) < (safe_mod_func_uint16_t_u_u((((g_216[1][2][4] != ((*l_1174) |= (&g_665 == (void*)0))) && ((l_1177 = g_1175) == ((*l_1178) = &g_1176))) < g_132), l_1156)))) == 0x98FDCDBCL) , l_1156);
            return p_31;
        }
        else
        { /* block id: 451 */
            const uint32_t l_1187 = 5UL;
            if (((safe_lshift_func_uint16_t_u_u((*g_560), 12)) <= p_31))
            { /* block id: 452 */
                int16_t *l_1184 = &g_249;
                uint16_t ****l_1204 = &l_1202;
                int32_t *l_1219 = &g_293;
                if ((safe_lshift_func_uint16_t_u_u((((*l_1184) ^= g_279) > (safe_sub_func_uint8_t_u_u(l_1187, 255UL))), l_1169)))
                { /* block id: 454 */
                    for (g_94 = (-23); (g_94 > 54); g_94 = safe_add_func_int32_t_s_s(g_94, 4))
                    { /* block id: 457 */
                        int16_t l_1190 = 0xF400L;
                        return l_1190;
                    }
                    return p_31;
                }
                else
                { /* block id: 461 */
                    (*g_1191) = &l_1166;
                }
                if ((p_31 ^ (p_31 & (*g_571))))
                { /* block id: 464 */
                    int32_t l_1215 = 0x19BDD861L;
                    float **l_1218[2][7] = {{&g_666,&g_666,&g_666,&g_666,&g_666,&g_666,&g_666},{&g_666,&g_666,&g_666,&g_666,&g_666,&g_666,&g_666}};
                    int i, j;
                    if (((*g_311) |= (safe_rshift_func_uint16_t_u_s(p_31, 7))))
                    { /* block id: 466 */
                        if (p_31)
                            break;
                    }
                    else
                    { /* block id: 468 */
                        uint16_t *****l_1203[8][9] = {{&l_1201[1],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[1],&l_1201[3]},{&l_1201[3],&l_1201[3],&l_1201[2],&l_1201[1],&l_1201[2],&l_1201[3],&l_1201[3],&l_1201[1],&l_1201[3]},{&l_1201[1],&l_1201[0],&l_1201[3],&l_1201[3],&l_1201[0],&l_1201[1],&l_1201[3],&l_1201[3],&l_1201[3]},{&l_1201[3],(void*)0,&l_1201[3],(void*)0,&l_1201[3],&l_1201[1],&l_1201[3],&l_1201[1],&l_1201[3]},{&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[1],&l_1201[0]},{&l_1201[2],&l_1201[1],&l_1201[2],&l_1201[3],&l_1201[3],&l_1201[1],&l_1201[3],&l_1201[3],&l_1201[2]},{&l_1201[0],&l_1201[0],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[1],&l_1201[3],&l_1201[3],&l_1201[3]},{&l_1201[3],&l_1201[1],&l_1201[2],&l_1201[3],&l_1201[3],&l_1201[3],&l_1201[2],&l_1201[1],&l_1201[3]}};
                        int64_t *l_1216 = &g_189[3];
                        int i, j;
                        (*g_311) = ((+(((safe_div_func_uint64_t_u_u((((safe_sub_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_u((((l_1204 = l_1201[3]) == (void*)0) , p_31), 11)), (safe_div_func_uint64_t_u_u(0UL, ((**g_1175) = (safe_lshift_func_int8_t_s_u((+(((safe_lshift_func_uint16_t_u_s((*g_560), 8)) , (void*)0) == (void*)0)), 6))))))) ^ 0UL) != (safe_mod_func_int16_t_s_s((((*l_1216) = ((safe_unary_minus_func_int64_t_s(l_1215)) & p_31)) , 0x30CBL), p_31))), p_31)) & l_1187) != p_31)) <= l_1215);
                        (*g_1217) = (*g_1191);
                        (*g_311) |= (((***g_331) , &g_1097) != l_1218[0][1]);
                        (*g_1220) = l_1219;
                    }
                }
                else
                { /* block id: 477 */
                    int32_t ***l_1222 = &l_1221[0][0][0];
                    (*l_1222) = l_1221[0][1][0];
                }
            }
            else
            { /* block id: 480 */
                (*g_666) = 0x7.79A486p-9;
            }
        }
        l_1166 = ((*g_654) != &l_1166);
        return l_1223;
    }
    for (g_190 = 0; (g_190 <= 1); g_190 += 1)
    { /* block id: 489 */
        int32_t *l_1224 = &l_1166;
        int32_t **l_1225 = &l_1224;
        int64_t *l_1281 = &g_189[2];
        uint8_t *l_1306 = &g_216[1][2][4];
        uint8_t * const *l_1305 = &l_1306;
        uint32_t l_1310 = 4294967293UL;
        (*g_1220) = (g_190 , (*g_1217));
        (*l_1225) = l_1224;
        for (g_132 = 0; (g_132 <= 1); g_132 += 1)
        { /* block id: 494 */
            int16_t * const l_1226 = (void*)0;
            int16_t *l_1228 = &g_249;
            int16_t **l_1227 = &l_1228;
            int32_t l_1244 = (-2L);
            uint16_t l_1276 = 8UL;
            l_1169 = (l_1166 = ((p_31 , (l_1226 != ((*l_1227) = &g_249))) & 0xEC58F047L));
            for (g_293 = 0; (g_293 <= 1); g_293 += 1)
            { /* block id: 500 */
                if (g_132)
                    goto lbl_1229;
            }
            for (g_94 = 0; (g_94 <= 1); g_94 += 1)
            { /* block id: 505 */
                uint16_t l_1234 = 8UL;
                int64_t *l_1237[9][10] = {{&g_189[0],&g_189[0],&g_189[0],&g_189[0],(void*)0,&g_189[0],&g_189[0],&g_189[0],(void*)0,&g_189[0]},{&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],(void*)0,&g_189[0],&g_189[0],&g_189[0]},{&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0]},{&g_189[0],&g_189[0],&g_189[0],&g_189[2],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[2]},{&g_189[0],(void*)0,&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],(void*)0},{&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[2],&g_189[0],&g_189[0],&g_189[0],&g_189[2],&g_189[0]},{&g_189[2],&g_189[0],&g_189[0],(void*)0,&g_189[0],&g_189[0],(void*)0,&g_189[0],&g_189[0],&g_189[2]},{&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[1]},{&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[0],&g_189[2]}};
                uint64_t **l_1248[8] = {&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176,&g_1176};
                int32_t l_1249[10][7] = {{(-1L),0L,0x13083784L,(-9L),0x66897B9AL,7L,7L},{(-1L),0x8C4AF3E3L,0xBAC92FD7L,0x8C4AF3E3L,(-1L),7L,0x60EDB28AL},{(-10L),(-6L),0xBF3EF7A6L,0x8C4AF3E3L,0x66897B9AL,0xBAC92FD7L,0x098C1DD9L},{(-1L),0xE80EAE23L,0xF0650ACEL,0x48109974L,7L,0xBF3EF7A6L,0x098C1DD9L},{0xE1816AB7L,0xD3058288L,0x1396F4CBL,0xA9C08DA4L,7L,0xBAC92FD7L,0xBF3EF7A6L},{0x60EDB28AL,0x51A4C3ECL,0x1396F4CBL,0x48109974L,(-1L),0x13083784L,0x13083784L},{0x60EDB28AL,0xD3058288L,0xF0650ACEL,0xD3058288L,0x60EDB28AL,0x13083784L,0xBF3EF7A6L},{0xE1816AB7L,0xE80EAE23L,(-2L),0xD3058288L,(-1L),0xBAC92FD7L,0x098C1DD9L},{(-1L),0xE80EAE23L,0xF0650ACEL,0x48109974L,7L,0xBF3EF7A6L,0x098C1DD9L},{0xE1816AB7L,0xD3058288L,0x1396F4CBL,0xA9C08DA4L,7L,0xBAC92FD7L,0xBF3EF7A6L}};
                int i, j;
            }
        }
        (*g_1318) = (g_236[0][4][2] , ((*l_1224) = (+g_236[0][4][2])));
    }
    return p_31;
}


/* ------------------------------------------ */
/* 
 * reads : g_79 g_216 g_665 g_666 g_313 g_311 g_334 g_1097 g_85
 * writes: g_1107 g_100 g_1137 g_311
 */
static int32_t  func_32(uint32_t  p_33, uint32_t  p_34, uint8_t  p_35, int64_t  p_36)
{ /* block id: 416 */
    uint32_t ****l_1106 = &g_329;
    uint32_t *****l_1105 = &l_1106;
    int32_t * const *l_1109 = (void*)0;
    uint32_t *****l_1115 = &g_1108;
    int32_t l_1116[6] = {0xA817ECB1L,0xA817ECB1L,0xA817ECB1L,0xA817ECB1L,0xA817ECB1L,0xA817ECB1L};
    uint16_t ***l_1117 = (void*)0;
    uint64_t ***l_1132[3];
    int8_t *l_1138[10] = {&g_294,&g_294,&g_294,&g_294,&g_294,&g_294,&g_294,&g_294,&g_294,&g_294};
    uint32_t l_1150 = 0xA9A6E3E3L;
    uint64_t l_1153[1];
    int i;
    for (i = 0; i < 3; i++)
        l_1132[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_1153[i] = 0x2EDB25FF95A2A176LL;
    if ((g_79 && ((safe_mul_func_int8_t_s_s((((0x92L && (safe_mul_func_int8_t_s_s(((g_1107 = l_1105) == (l_1115 = ((((((l_1109 = (void*)0) != &g_311) && (+g_216[1][2][1])) >= ((safe_rshift_func_uint8_t_u_s(255UL, (safe_div_func_uint32_t_u_u((1UL && p_33), 6UL)))) != p_35)) <= 255UL) , (void*)0))), g_79))) == 248UL) || p_33), l_1116[4])) < 0xDE3F6DC2L)))
    { /* block id: 420 */
        uint16_t ****l_1118 = (void*)0;
        uint16_t ****l_1119 = &l_1117;
        int32_t l_1135 = 0L;
        int8_t **l_1136[3][2];
        int32_t l_1139[10];
        int i, j;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 2; j++)
                l_1136[i][j] = (void*)0;
        }
        for (i = 0; i < 10; i++)
            l_1139[i] = 0x9BC7577CL;
        l_1139[9] &= (((((l_1138[3] = (g_1137 = ((((*l_1119) = l_1117) != &g_582) , (((((**g_665) = ((-0x8.Fp+1) <= (safe_add_func_float_f_f((((safe_add_func_float_f_f((!0x0.9p+1), (safe_add_func_float_f_f(0x9.0p-1, (safe_mul_func_float_f_f((+(safe_div_func_float_f_f((l_1132[0] == (void*)0), 0x1.3A7697p-13))), (safe_div_func_float_f_f(l_1135, (-0x2.Bp+1))))))))) >= 0x1.Ap+1) > p_36), p_34)))) > p_36) < 0xD.77FE34p-31) , (void*)0)))) == (void*)0) != 0L) || p_35) != 249UL);
    }
    else
    { /* block id: 426 */
        int32_t **l_1140 = (void*)0;
        int32_t **l_1141 = &g_311;
        (*l_1141) = (*g_313);
    }
    for (p_34 = 0; (p_34 <= 2); p_34 += 1)
    { /* block id: 431 */
        int8_t l_1142 = 0x6FL;
        int32_t *l_1143 = &g_283[0][1][7];
        int32_t *l_1144 = (void*)0;
        int32_t *l_1145 = &l_1116[4];
        int32_t *l_1146 = &g_132;
        int32_t *l_1147 = (void*)0;
        int32_t *l_1148 = &g_283[0][1][8];
        int32_t *l_1149 = (void*)0;
        l_1150++;
        return g_334;
    }
    (*g_666) = (*g_1097);
    return l_1153[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_252 g_73 g_96 g_97 g_132 g_571 g_283 g_293 g_189 g_272 g_12 g_79 g_659 g_67 g_249 g_291 g_216 g_691 g_665 g_666 g_70 g_560 g_910 g_281 g_1097
 * writes: g_293 g_294 g_283 g_582 g_249 g_79 g_659 g_691 g_100 g_70 g_85
 */
static uint8_t  func_46(uint16_t * p_47, int8_t  p_48, uint64_t  p_49)
{ /* block id: 193 */
    int32_t *l_563 = &g_293;
    int32_t l_568 = 0x60392CB6L;
    int8_t *l_569 = &g_294;
    uint64_t l_570 = 0x5B6127C6A579B6DDLL;
    int8_t l_575 = 0x6DL;
    uint16_t **l_581 = &g_560;
    uint16_t ***l_580[2];
    float *l_584 = &g_100;
    float **l_583 = &l_584;
    int16_t *l_585 = &g_249;
    int64_t l_642 = 0xA1F4A9777A2724BELL;
    uint32_t ** const l_793 = (void*)0;
    uint16_t l_819 = 4UL;
    int8_t l_836 = 1L;
    int16_t l_847[7][2] = {{0x1A0FL,0x4EC6L},{0x9BEDL,0x1A0FL},{(-1L),(-1L)},{(-1L),0x1A0FL},{0x9BEDL,0x4EC6L},{0x1A0FL,0x4EC6L},{0x9BEDL,0x1A0FL}};
    int32_t l_852 = 0xF432A250L;
    int32_t l_856 = (-10L);
    int32_t l_858 = 0x0A7E742EL;
    int32_t l_859[6][1][5] = {{{0L,0L,4L,0L,0L}},{{1L,0xEEF29C57L,1L,1L,0xEEF29C57L}},{{0L,0x827AE8ACL,0x827AE8ACL,0L,0x827AE8ACL}},{{0xEEF29C57L,0xEEF29C57L,7L,0xEEF29C57L,0xEEF29C57L}},{{0x827AE8ACL,0L,0x827AE8ACL,0x827AE8ACL,0L}},{{0xEEF29C57L,1L,1L,0xEEF29C57L,1L}}};
    const uint32_t ***l_871 = (void*)0;
    const uint32_t ****l_870 = &l_871;
    float l_907 = 0x0.6B7D46p+3;
    uint32_t l_970 = 0xDE7CB4BDL;
    int32_t l_1060 = 0x1B7399C7L;
    uint32_t **l_1070 = &g_975;
    uint32_t ***l_1069[3][10][6] = {{{&l_1070,&l_1070,&l_1070,&l_1070,&l_1070,&l_1070},{&l_1070,&l_1070,(void*)0,&l_1070,&l_1070,&l_1070},{&l_1070,(void*)0,&l_1070,(void*)0,&l_1070,(void*)0},{(void*)0,&l_1070,&l_1070,&l_1070,(void*)0,&l_1070},{(void*)0,&l_1070,&l_1070,&l_1070,&l_1070,&l_1070},{&l_1070,&l_1070,&l_1070,(void*)0,&l_1070,(void*)0},{&l_1070,(void*)0,&l_1070,&l_1070,(void*)0,&l_1070},{(void*)0,&l_1070,(void*)0,&l_1070,&l_1070,&l_1070},{&l_1070,&l_1070,&l_1070,&l_1070,&l_1070,(void*)0},{&l_1070,(void*)0,&l_1070,&l_1070,&l_1070,(void*)0}},{{(void*)0,&l_1070,(void*)0,&l_1070,(void*)0,&l_1070},{&l_1070,&l_1070,&l_1070,(void*)0,&l_1070,&l_1070},{&l_1070,&l_1070,&l_1070,&l_1070,&l_1070,&l_1070},{(void*)0,&l_1070,(void*)0,&l_1070,(void*)0,&l_1070},{(void*)0,(void*)0,&l_1070,&l_1070,&l_1070,&l_1070},{&l_1070,&l_1070,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1070,&l_1070,&l_1070,&l_1070,(void*)0},{&l_1070,(void*)0,&l_1070,(void*)0,&l_1070,(void*)0},{&l_1070,(void*)0,(void*)0,&l_1070,&l_1070,&l_1070},{&l_1070,&l_1070,&l_1070,(void*)0,(void*)0,&l_1070}},{{(void*)0,&l_1070,(void*)0,&l_1070,(void*)0,&l_1070},{(void*)0,&l_1070,&l_1070,&l_1070,&l_1070,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1070,&l_1070},{&l_1070,&l_1070,&l_1070,&l_1070,(void*)0,(void*)0},{&l_1070,(void*)0,&l_1070,(void*)0,&l_1070,&l_1070},{&l_1070,&l_1070,(void*)0,&l_1070,&l_1070,&l_1070},{(void*)0,(void*)0,&l_1070,(void*)0,(void*)0,(void*)0},{&l_1070,&l_1070,(void*)0,&l_1070,&l_1070,&l_1070},{(void*)0,(void*)0,&l_1070,&l_1070,&l_1070,&l_1070},{&l_1070,&l_1070,&l_1070,(void*)0,(void*)0,&l_1070}}};
    int32_t l_1085 = 0L;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_580[i] = &l_581;
    (*g_571) &= (l_568 = ((0x41L && 0L) | (((((*l_563) = 0xBE40F4E4L) | (((safe_mul_func_uint8_t_u_u((0x3884114CL < (-1L)), ((safe_sub_func_int16_t_s_s(0x493FL, g_252)) < ((p_48 ^ (((*l_569) = l_568) < l_568)) , l_570)))) & g_73[7][2]) , (*g_96))) | g_132) >= p_49)));
    if (((safe_mul_func_uint16_t_u_u((((*l_563) = (!l_575)) , ((*l_563) == ((4L >= g_132) ^ (((*l_585) = ((safe_lshift_func_int8_t_s_u(((p_49 , (*l_563)) , (safe_mod_func_int8_t_s_s(((*l_569) = ((((g_582 = &p_47) == &g_560) >= (((*l_583) = l_563) != l_563)) < 0x6F80007FD4078CB9LL)), g_189[3]))), 4)) , g_272)) , (*g_571))))), g_12)) , p_48))
    { /* block id: 203 */
        uint32_t l_650 = 1UL;
        int32_t l_655 = 0x950F718FL;
        int32_t l_656[5][8] = {{0xBCB8DF37L,0x14664257L,0xBCB8DF37L,6L,(-1L),(-1L),6L,0xBCB8DF37L},{0x14664257L,0x14664257L,(-1L),0xEF63113EL,0x4C9B2738L,0xEF63113EL,(-1L),0x14664257L},{0x14664257L,0xBCB8DF37L,6L,(-1L),(-1L),6L,0xBCB8DF37L,0x14664257L},{0xBCB8DF37L,(-1L),0x14664257L,0xEF63113EL,0x14664257L,(-1L),0xBCB8DF37L,0xBCB8DF37L},{(-1L),0xEF63113EL,6L,6L,0xEF63113EL,(-1L),(-1L),(-1L)}};
        float l_739 = 0x1.86C75Cp-36;
        int32_t l_742 = 0xF3DE66B5L;
        int32_t *l_784 = &g_785;
        uint32_t **l_926[7] = {&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72};
        int64_t *l_952 = &l_642;
        const uint64_t l_967 = 18446744073709551609UL;
        int64_t l_1000 = (-5L);
        int8_t l_1001[9];
        int8_t l_1025 = (-5L);
        uint64_t *l_1035[5][5][3];
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_1001[i] = 0x8BL;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 5; j++)
            {
                for (k = 0; k < 3; k++)
                    l_1035[i][j][k] = (void*)0;
            }
        }
        for (g_79 = (-29); (g_79 == 9); g_79 = safe_add_func_uint32_t_u_u(g_79, 8))
        { /* block id: 206 */
            uint32_t l_592 = 18446744073709551615UL;
            uint64_t *l_601 = &g_268[1];
            uint64_t *l_602 = &l_570;
            uint16_t *l_641 = &g_70;
            int32_t l_649[1][5] = {{(-10L),(-10L),(-10L),(-10L),(-10L)}};
            int32_t l_662 = 0x45CFB493L;
            int32_t l_727 = 0xDBF5E881L;
            uint16_t l_769 = 0xCC0FL;
            int16_t l_862 = (-3L);
            int16_t l_865 = 0xE910L;
            int32_t **l_897 = &g_311;
            int64_t l_908 = 0x8C7C6CFC571F8E0CLL;
            uint32_t **l_927[8][5][6] = {{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,(void*)0,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,(void*)0}},{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{(void*)0,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,(void*)0,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,(void*)0,(void*)0}},{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{(void*)0,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,(void*)0,&g_72}},{{&g_72,&g_72,&g_72,(void*)0,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}}};
            uint8_t *l_929 = (void*)0;
            uint32_t * const **l_986 = &g_95;
            uint32_t * const ***l_985[2][7] = {{&l_986,&l_986,&l_986,&l_986,&l_986,&l_986,&l_986},{&l_986,&l_986,&l_986,&l_986,&l_986,&l_986,&l_986}};
            uint32_t * const l_999[2][5] = {{&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97}};
            int i, j, k;
        }
        l_656[1][4] ^= ((safe_rshift_func_uint16_t_u_s((*l_563), 2)) <= (safe_mod_func_uint32_t_u_u((safe_add_func_int16_t_s_s((0x9BDE93F66D5712ADLL || (--g_659[0])), (p_49 < (p_48 | p_48)))), (g_272 ^ 0x57L))));
        (*l_870) = (((*l_563) = p_49) , (void*)0);
        l_859[5][0][3] |= (safe_lshift_func_uint8_t_u_s((safe_div_func_int32_t_s_s((0xF7ECL ^ ((safe_rshift_func_int8_t_s_s((safe_rshift_func_int16_t_s_u((*l_563), 5)), 7)) ^ l_1000)), (0x1FL ^ p_49))), (((safe_mul_func_int8_t_s_s(0x91L, (p_49 < 65531UL))) != ((l_656[1][2] || g_67[5]) != l_742)) == l_967)));
    }
    else
    { /* block id: 394 */
        int32_t *l_1048 = &l_859[5][0][3];
        int32_t *l_1049 = &l_858;
        int64_t l_1050 = (-2L);
        int32_t *l_1051 = &l_858;
        int32_t *l_1052 = &g_132;
        int32_t *l_1053 = &l_568;
        int32_t *l_1054 = &l_859[5][0][3];
        int32_t *l_1055 = &l_859[5][0][3];
        int32_t *l_1056 = &l_856;
        int32_t l_1057 = (-6L);
        int32_t *l_1058 = &l_858;
        int32_t *l_1059[10] = {&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8],&g_283[0][1][8]};
        int32_t l_1061 = 0xEADA23D2L;
        uint32_t l_1062[1];
        int32_t l_1072 = 1L;
        uint32_t **l_1079 = &g_975;
        uint16_t ***l_1093[4];
        int8_t l_1096 = 0x1BL;
        int i;
        for (i = 0; i < 1; i++)
            l_1062[i] = 6UL;
        for (i = 0; i < 4; i++)
            l_1093[i] = (void*)0;
        l_1062[0]--;
        for (g_249 = 28; (g_249 > (-5)); g_249 = safe_sub_func_int16_t_s_s(g_249, 6))
        { /* block id: 398 */
            uint8_t *l_1071[10][2] = {{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691},{&g_691,&g_691}};
            int32_t l_1073 = 0L;
            int32_t l_1074[4];
            uint16_t l_1075 = 0x7862L;
            int64_t l_1078 = 0x6FC0D3B88E1AC056LL;
            int i, j;
            for (i = 0; i < 4; i++)
                l_1074[i] = 0xF6FA51BDL;
            (*l_1053) &= (g_291 & ((safe_lshift_func_int16_t_s_u(g_216[1][2][6], 9)) != ((*l_563) &= (g_691 &= ((void*)0 == l_1069[0][3][5])))));
            l_1075--;
            l_1074[0] ^= (g_659[1] <= ((*l_569) = (*l_1058)));
            if (l_1078)
                break;
        }
        (*g_1097) = ((l_1079 == l_1079) != (safe_div_func_float_f_f(p_48, ((safe_sub_func_float_f_f((((((**g_665) = 0x6.AADDECp+22) != (+(l_1085 < ((safe_div_func_float_f_f(((*l_584) = ((((*g_560) = (*p_47)) , (65531UL & ((*p_47) = (safe_unary_minus_func_uint8_t_u(((safe_mul_func_int8_t_s_s((((safe_div_func_int32_t_s_s(((l_1093[2] == ((safe_rshift_func_uint8_t_u_u((1L | l_1096), g_12)) , &l_581)) <= 18446744073709551615UL), (*l_563))) || (*p_47)) <= g_910), g_281[8][1])) <= p_48)))))) , 0xE.D95076p-99)), 0xF.AA7CA1p-94)) >= 0x5.1D23AAp-52)))) != p_49) >= (-0x2.5p-1)), (*l_1056))) , 0x3.EF4889p-21))));
    }
    return (*l_563);
}


/* ------------------------------------------ */
/* 
 * reads : g_79 g_73 g_67
 * writes: g_79 g_85 g_95 g_100
 */
static uint16_t * func_50(uint32_t  p_51, uint32_t  p_52, uint32_t * p_53)
{ /* block id: 9 */
    uint64_t *l_78 = &g_79;
    uint32_t **l_82[7][4] = {{&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,(void*)0},{&g_72,&g_72,(void*)0,&g_72},{(void*)0,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,(void*)0},{&g_72,&g_72,&g_72,&g_72},{&g_72,&g_72,&g_72,&g_72}};
    int8_t l_83 = 0x9EL;
    float *l_84 = &g_85;
    uint32_t * const l_93 = &g_94;
    uint32_t * const *l_92[7][3][1] = {{{(void*)0},{&l_93},{(void*)0}},{{&l_93},{(void*)0},{&l_93}},{{(void*)0},{&l_93},{(void*)0}},{{&l_93},{(void*)0},{&l_93}},{{(void*)0},{&l_93},{(void*)0}},{{&l_93},{(void*)0},{&l_93}},{{(void*)0},{&l_93},{(void*)0}}};
    uint32_t * const **l_91[3][5] = {{&l_92[3][2][0],&l_92[3][2][0],&l_92[3][2][0],&l_92[3][2][0],&l_92[3][2][0]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_92[3][2][0],&l_92[3][2][0],&l_92[3][2][0],&l_92[3][2][0],&l_92[3][2][0]}};
    float *l_98 = (void*)0;
    float *l_99 = &g_100;
    int32_t l_101 = 0L;
    int8_t *l_128 = &l_83;
    int32_t l_129 = 0x99B685FDL;
    int32_t l_130 = 0x4CC8AA12L;
    int32_t *l_131 = &g_132;
    int8_t l_168 = 0xCDL;
    float l_169 = 0x0.0p-1;
    int8_t l_286 = 6L;
    int32_t l_290[10][2][10] = {{{7L,(-1L),0xDCEB1211L,0x8D8B33F1L,(-10L),7L,0L,(-10L),(-1L),0L},{4L,(-1L),0x676DEBBEL,5L,0xB888BF66L,3L,0xB34B949FL,0xEEA6B039L,0x1E5C2C58L,0xB888BF66L}},{{4L,0L,0x637A08B2L,0x4EECCF07L,0xDCEB1211L,7L,4L,0L,0x8D8B33F1L,0xB888BF66L},{7L,4L,0L,0x8D8B33F1L,0xB888BF66L,0L,0L,0xB888BF66L,0x8D8B33F1L,0L}},{{0xB34B949FL,0xB34B949FL,0x676DEBBEL,0x4EECCF07L,(-10L),3L,(-1L),0xB888BF66L,0x1E5C2C58L,0xEEA6B039L},{(-1L),0L,0L,5L,0xDCEB1211L,(-1L),(-1L),0L,(-1L),(-10L)}},{{7L,0xB34B949FL,(-5L),(-1L),1L,0L,0x676DEBBEL,1L,7L,6L},{0xDCEB1211L,0x637A08B2L,0x1EEB307CL,0L,1L,0xEEA6B039L,0x637A08B2L,2L,(-5L),2L}},{{0L,0x676DEBBEL,1L,0L,1L,0x676DEBBEL,0L,6L,7L,1L},{0x6A70D89AL,0xDCEB1211L,1L,(-1L),2L,0x6A70D89AL,0x676DEBBEL,2L,5L,6L}},{{0x637A08B2L,0xDCEB1211L,0x1EEB307CL,0L,(-9L),0xEEA6B039L,0L,1L,(-5L),(-9L)},{0x637A08B2L,0x676DEBBEL,(-5L),0x3D58B1D7L,1L,0x6A70D89AL,0x637A08B2L,6L,(-1L),(-9L)}},{{0x6A70D89AL,0x637A08B2L,6L,(-1L),(-9L),0x676DEBBEL,0x676DEBBEL,(-9L),(-1L),6L},{0L,0L,0x1EEB307CL,0x3D58B1D7L,2L,0xEEA6B039L,0xDCEB1211L,(-9L),(-5L),1L}},{{0xDCEB1211L,0x676DEBBEL,6L,0L,1L,0L,0xDCEB1211L,6L,5L,2L},{0x6A70D89AL,0L,(-5L),(-1L),1L,0L,0x676DEBBEL,1L,7L,6L}},{{0xDCEB1211L,0x637A08B2L,0x1EEB307CL,0L,1L,0xEEA6B039L,0x637A08B2L,2L,(-5L),2L},{0L,0x676DEBBEL,1L,0L,1L,0x676DEBBEL,0L,6L,7L,1L}},{{0x6A70D89AL,0xDCEB1211L,1L,(-1L),2L,0x6A70D89AL,0x676DEBBEL,2L,5L,6L},{0x637A08B2L,0xDCEB1211L,0x1EEB307CL,0L,(-9L),0xEEA6B039L,0L,1L,(-5L),(-9L)}}};
    int16_t l_292 = 0xD97DL;
    int8_t l_295 = 0x53L;
    int8_t l_298 = 0xBEL;
    int32_t l_303 = 0L;
    int32_t l_305[1];
    int8_t l_306 = 0xF5L;
    volatile int32_t *l_357[6][7][6] = {{{&g_281[7][0],&g_281[7][0],&g_281[7][0],(void*)0,&g_279,&g_279},{(void*)0,&g_281[7][0],&g_279,&g_281[7][0],(void*)0,&g_281[7][0]},{(void*)0,(void*)0,&g_279,&g_281[5][2],&g_281[7][0],&g_279},{&g_279,&g_281[5][2],&g_281[7][0],&g_281[7][0],&g_281[7][0],&g_281[7][0]},{&g_281[7][0],&g_281[7][0],&g_281[7][0],&g_281[7][0],&g_279,&g_281[3][0]},{&g_281[7][0],&g_281[7][0],(void*)0,(void*)0,&g_281[7][0],&g_281[7][0]},{&g_281[7][0],&g_281[5][1],(void*)0,&g_281[7][0],&g_281[7][0],(void*)0}},{{&g_281[6][0],&g_279,&g_281[7][0],&g_279,(void*)0,&g_281[7][0]},{&g_279,&g_281[7][0],&g_281[0][3],&g_281[7][0],&g_279,&g_279},{&g_281[7][0],&g_281[7][0],&g_281[2][2],(void*)0,&g_281[7][0],&g_279},{&g_281[1][0],&g_281[0][1],&g_279,&g_279,&g_281[7][0],&g_279},{&g_279,&g_281[7][0],&g_279,&g_279,&g_281[4][3],&g_279},{&g_281[7][0],&g_281[6][3],&g_281[7][0],&g_281[0][3],&g_281[7][0],&g_279},{&g_281[2][2],&g_279,&g_281[7][0],&g_281[2][3],&g_279,(void*)0}},{{&g_281[3][2],&g_281[6][0],&g_281[7][0],&g_281[7][0],&g_281[7][0],&g_281[6][0]},{&g_279,&g_281[2][3],(void*)0,&g_279,&g_279,&g_281[7][0]},{&g_281[7][0],&g_279,&g_279,(void*)0,&g_281[4][2],(void*)0},{(void*)0,&g_279,&g_281[1][0],&g_281[7][0],&g_281[7][0],&g_281[3][0]},{&g_281[7][0],(void*)0,&g_281[7][0],&g_279,&g_281[2][2],&g_281[7][0]},{(void*)0,&g_281[4][3],&g_281[4][1],&g_281[6][0],(void*)0,&g_281[6][0]},{&g_281[5][2],&g_281[7][0],&g_281[5][2],&g_279,&g_281[7][0],(void*)0}},{{&g_279,&g_281[7][0],(void*)0,&g_281[7][0],&g_281[7][0],&g_281[3][2]},{&g_279,&g_281[7][0],&g_281[7][0],&g_281[7][0],&g_279,&g_279},{&g_279,&g_279,&g_279,&g_279,&g_279,&g_279},{&g_281[5][2],(void*)0,&g_279,&g_281[6][0],&g_281[7][1],&g_281[0][3]},{(void*)0,(void*)0,&g_279,&g_279,&g_281[7][0],(void*)0},{&g_281[7][0],&g_281[6][0],&g_281[1][3],&g_281[7][0],&g_281[7][0],&g_279},{(void*)0,&g_281[7][0],&g_281[7][0],(void*)0,&g_281[7][0],(void*)0}},{{&g_281[7][0],&g_281[7][0],&g_279,&g_279,&g_281[7][0],&g_279},{&g_279,&g_279,&g_281[7][0],&g_281[7][0],&g_281[7][0],(void*)0},{&g_281[3][2],&g_281[7][0],&g_279,&g_281[2][3],&g_279,&g_281[5][1]},{&g_281[2][2],&g_281[7][0],&g_281[7][0],&g_281[0][3],&g_281[7][0],&g_279},{&g_281[7][0],&g_279,&g_279,&g_279,&g_281[7][0],&g_281[1][3]},{&g_279,&g_281[7][0],&g_279,&g_279,(void*)0,(void*)0},{&g_281[1][0],&g_279,&g_279,(void*)0,&g_279,&g_279}},{{&g_281[7][0],&g_281[7][0],&g_281[4][2],&g_281[7][0],&g_281[7][0],&g_281[7][0]},{&g_281[6][0],&g_281[7][0],&g_281[7][0],&g_279,&g_281[7][0],(void*)0},{&g_281[6][3],&g_279,&g_281[6][0],&g_281[7][0],&g_279,&g_279},{&g_281[7][0],&g_279,(void*)0,(void*)0,(void*)0,&g_281[6][0]},{&g_279,&g_281[7][0],&g_281[7][0],&g_279,&g_281[7][0],&g_281[7][0]},{&g_281[7][0],&g_281[7][0],&g_281[7][0],&g_281[7][1],(void*)0,&g_279},{&g_279,&g_279,&g_281[7][0],&g_279,&g_281[7][0],&g_279}}};
    uint32_t l_423[9] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    uint8_t l_521 = 255UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_305[i] = (-1L);
    l_101 = (safe_add_func_float_f_f((safe_add_func_float_f_f((((*l_84) = ((((p_52 <= ((*l_78)++)) , &g_73[2][1]) != (p_53 = &p_52)) <= l_83)) < (g_73[2][1] , (+((0x1.75A398p+42 > ((safe_add_func_float_f_f(((*l_99) = (l_83 < (safe_sub_func_float_f_f((l_82[1][1] != (g_95 = l_82[0][0])), 0x8.8D280Dp+45)))), p_51)) == l_83)) != p_51)))), (-0x1.Ap-1))), g_67[5]));
    return &g_70;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_12, "g_12", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_39[i][j][k], "g_39[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_44, "g_44", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_67[i], "g_67[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_70, "g_70", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_73[i][j], "g_73[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_79, "g_79", print_hash_value);
    transparent_crc_bytes (&g_85, sizeof(g_85), "g_85", print_hash_value);
    transparent_crc(g_94, "g_94", print_hash_value);
    transparent_crc(g_97, "g_97", print_hash_value);
    transparent_crc_bytes (&g_100, sizeof(g_100), "g_100", print_hash_value);
    transparent_crc(g_132, "g_132", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_189[i], "g_189[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_190, "g_190", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_216[i][j][k], "g_216[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_223, "g_223", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_236[i][j][k], "g_236[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_249, "g_249", print_hash_value);
    transparent_crc(g_252, "g_252", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_268[i], "g_268[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_272, "g_272", print_hash_value);
    transparent_crc(g_279, "g_279", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_281[i][j], "g_281[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_283[i][j][k], "g_283[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_291, "g_291", print_hash_value);
    transparent_crc(g_293, "g_293", print_hash_value);
    transparent_crc(g_294, "g_294", print_hash_value);
    transparent_crc(g_334, "g_334", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_659[i], "g_659[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_691, "g_691", print_hash_value);
    transparent_crc(g_785, "g_785", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_788[i], sizeof(g_788[i]), "g_788[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_887, sizeof(g_887), "g_887", print_hash_value);
    transparent_crc(g_910, "g_910", print_hash_value);
    transparent_crc(g_1285, "g_1285", print_hash_value);
    transparent_crc(g_1339, "g_1339", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1363[i][j], "g_1363[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1366, "g_1366", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1369[i], "g_1369[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1370, "g_1370", print_hash_value);
    transparent_crc(g_1371, "g_1371", print_hash_value);
    transparent_crc(g_1372, "g_1372", print_hash_value);
    transparent_crc(g_1373, "g_1373", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1374[i][j], "g_1374[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1375[i][j], "g_1375[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1376, "g_1376", print_hash_value);
    transparent_crc(g_1424, "g_1424", print_hash_value);
    transparent_crc(g_1572, "g_1572", print_hash_value);
    transparent_crc(g_1636, "g_1636", print_hash_value);
    transparent_crc(g_1966, "g_1966", print_hash_value);
    transparent_crc_bytes (&g_2002, sizeof(g_2002), "g_2002", print_hash_value);
    transparent_crc(g_2082, "g_2082", print_hash_value);
    transparent_crc(g_2232, "g_2232", print_hash_value);
    transparent_crc(g_2282, "g_2282", print_hash_value);
    transparent_crc(g_2289, "g_2289", print_hash_value);
    transparent_crc(g_2296, "g_2296", print_hash_value);
    transparent_crc(g_2403, "g_2403", print_hash_value);
    transparent_crc(g_2609, "g_2609", print_hash_value);
    transparent_crc(g_2634, "g_2634", print_hash_value);
    transparent_crc(g_2900, "g_2900", print_hash_value);
    transparent_crc(g_3059, "g_3059", print_hash_value);
    transparent_crc(g_3281, "g_3281", print_hash_value);
    transparent_crc(g_3349, "g_3349", print_hash_value);
    transparent_crc(g_3362, "g_3362", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 790
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 44
breakdown:
   depth: 1, occurrence: 169
   depth: 2, occurrence: 47
   depth: 3, occurrence: 10
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
   depth: 6, occurrence: 3
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 3
   depth: 18, occurrence: 3
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 22, occurrence: 4
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 37, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 44, occurrence: 2

XXX total number of pointers: 806

XXX times a variable address is taken: 2139
XXX times a pointer is dereferenced on RHS: 489
breakdown:
   depth: 1, occurrence: 341
   depth: 2, occurrence: 116
   depth: 3, occurrence: 31
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 459
breakdown:
   depth: 1, occurrence: 399
   depth: 2, occurrence: 50
   depth: 3, occurrence: 10
XXX times a pointer is compared with null: 59
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 23
XXX times a pointer is qualified to be dereferenced: 12311

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1540
   level: 2, occurrence: 517
   level: 3, occurrence: 204
   level: 4, occurrence: 70
   level: 5, occurrence: 41
XXX number of pointers point to pointers: 376
XXX number of pointers point to scalars: 430
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.4
XXX average alias set size: 1.45

XXX times a non-volatile is read: 2838
XXX times a non-volatile is write: 1322
XXX times a volatile is read: 167
XXX    times read thru a pointer: 72
XXX times a volatile is write: 70
XXX    times written thru a pointer: 39
XXX times a volatile is available for access: 2.69e+03
XXX percentage of non-volatile access: 94.6

XXX forward jumps: 0
XXX backward jumps: 11

XXX stmts: 179
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 34
   depth: 2, occurrence: 38
   depth: 3, occurrence: 29
   depth: 4, occurrence: 26
   depth: 5, occurrence: 23

XXX percentage a fresh-made variable is used: 15.4
XXX percentage an existing variable is used: 84.6
********************* end of statistics **********************/


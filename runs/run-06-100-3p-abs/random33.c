/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3963500315
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   const uint64_t  f0;
};

union U1 {
   uint32_t  f0;
   uint64_t  f1;
   uint16_t  f2;
   const int32_t  f3;
};

union U2 {
   volatile int16_t  f0;
   volatile uint32_t  f1;
   uint64_t  f2;
   const int32_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2[6][3] = {{0L,(-1L),0x5A5EFA20L},{(-1L),(-1L),(-1L)},{0x5C5735E4L,0L,0x5A5EFA20L},{0x5C5735E4L,0x5C5735E4L,0L},{(-1L),0L,0L},{0L,(-1L),0x5A5EFA20L}};
static int32_t g_3 = 0xD8A94B2AL;
static volatile union U2 g_50 = {1L};/* VOLATILE GLOBAL g_50 */
static const int32_t g_53 = 1L;
static float g_67[7][7] = {{0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39},{0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1},{0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39},{0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1},{0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39},{0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1,0x0.9p+1},{0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39,0xE.90AECFp+39}};
static int32_t g_68 = 0xD05C4471L;
static union U0 g_70 = {0xEDE8B3EC6B9FE218LL};
static int16_t g_80 = 2L;
static int64_t g_92 = 0x4AADDA3982AEBACBLL;
static int64_t g_94 = 0xB25F501B3ADA1D09LL;
static float g_98[5][7] = {{0x6.750A3Bp+28,0x0.4B7FD3p-98,0x6.750A3Bp+28,0x0.4B7FD3p-98,0x6.750A3Bp+28,0x0.4B7FD3p-98,0x6.750A3Bp+28},{0x5.7p+1,0x5.7p+1,0xF.380447p-19,0xF.380447p-19,0x5.7p+1,0x5.7p+1,0xF.380447p-19},{0xD.171E9Dp-12,0x0.4B7FD3p-98,0xD.171E9Dp-12,0x0.4B7FD3p-98,0xD.171E9Dp-12,0x0.4B7FD3p-98,0xD.171E9Dp-12},{0x5.7p+1,0xF.380447p-19,0xF.380447p-19,0x5.7p+1,0x5.7p+1,0xF.380447p-19,0xF.380447p-19},{0x6.750A3Bp+28,0x0.4B7FD3p-98,0x6.750A3Bp+28,0x0.4B7FD3p-98,0x6.750A3Bp+28,0x0.4B7FD3p-98,0x6.750A3Bp+28}};
static uint8_t g_110 = 0x2CL;
static int64_t g_111 = 0L;
static float g_112[6] = {0xD.62C3E9p-96,0xD.62C3E9p-96,0xD.62C3E9p-96,0xD.62C3E9p-96,0xD.62C3E9p-96,0xD.62C3E9p-96};
static int32_t g_113[8] = {0xA8165F17L,0xA8165F17L,0xA8165F17L,0xA8165F17L,0xA8165F17L,0xA8165F17L,0xA8165F17L,0xA8165F17L};
static uint32_t g_116 = 0xCAE9C981L;
static int16_t *g_143 = &g_80;
static int8_t g_147 = 0x38L;
static int8_t g_149 = 1L;
static volatile union U2 g_153 = {0x4162L};/* VOLATILE GLOBAL g_153 */
static volatile union U1 g_173[10] = {{0xD941E280L},{0xD941E280L},{0xD941E280L},{0xD941E280L},{0xD941E280L},{0xD941E280L},{0xD941E280L},{0xD941E280L},{0xD941E280L},{0xD941E280L}};
static int32_t *g_177 = &g_68;
static volatile union U2 *g_201 = (void*)0;
static volatile union U2 ** volatile g_200 = &g_201;/* VOLATILE GLOBAL g_200 */
static int64_t *g_211 = &g_111;
static int64_t * const *g_210 = &g_211;
static int8_t g_240 = 0L;
static int16_t g_256 = (-5L);
static uint16_t g_300[5][2][5] = {{{65534UL,0x391CL,65534UL,65534UL,0x391CL},{0x7D4BL,5UL,5UL,0x7D4BL,5UL}},{{0x391CL,0x391CL,0UL,0x391CL,0x391CL},{5UL,0x7D4BL,5UL,5UL,0x7D4BL}},{{0x391CL,65534UL,65534UL,0x391CL,65534UL},{0x7D4BL,0x7D4BL,0x3B4EL,0x7D4BL,0x7D4BL}},{{65534UL,0x391CL,65534UL,65534UL,0x391CL},{0x7D4BL,5UL,5UL,0x7D4BL,5UL}},{{0x391CL,0x391CL,0UL,0x391CL,0x391CL},{5UL,0x7D4BL,5UL,5UL,0x7D4BL}}};
static int8_t g_320 = 0xC9L;
static int32_t g_325 = 0x1F46EE88L;
static int32_t * volatile g_324[2][8] = {{&g_325,&g_325,&g_325,&g_325,&g_325,&g_325,&g_325,&g_325},{&g_325,&g_325,&g_325,&g_325,&g_325,&g_325,&g_325,&g_325}};
static uint64_t g_338 = 0x3D8C45E4701F6066LL;
static union U2 g_355[5][1][3] = {{{{-9L},{0x6161L},{0x6161L}}},{{{0x6161L},{0xDEFFL},{9L}}},{{{-9L},{0xDEFFL},{-9L}}},{{{0L},{0x6161L},{9L}}},{{{0L},{0L},{0x6161L}}}};
static volatile int32_t ***g_356 = (void*)0;
static union U2 g_443 = {0x3B41L};/* VOLATILE GLOBAL g_443 */
static const int32_t *g_462 = &g_53;
static const int32_t **g_461[4][4][8] = {{{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,(void*)0,(void*)0,(void*)0,&g_462,&g_462,(void*)0,&g_462},{&g_462,(void*)0,&g_462,(void*)0,&g_462,&g_462,&g_462,(void*)0}},{{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462},{(void*)0,&g_462,&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{(void*)0,&g_462,(void*)0,(void*)0,(void*)0,&g_462,&g_462,(void*)0},{&g_462,(void*)0,(void*)0,&g_462,&g_462,&g_462,&g_462,&g_462}},{{&g_462,&g_462,(void*)0,(void*)0,&g_462,(void*)0,&g_462,&g_462},{(void*)0,&g_462,&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{&g_462,(void*)0,&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462,&g_462,&g_462}},{{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,(void*)0,&g_462,&g_462,(void*)0,&g_462,(void*)0},{&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462,(void*)0,&g_462},{&g_462,(void*)0,&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462}}};
static const int32_t ***g_460 = &g_461[3][3][3];
static volatile union U2 g_475[4] = {{2L},{2L},{2L},{2L}};
static int16_t **g_477 = &g_143;
static int16_t ***g_476 = &g_477;
static union U2 g_491 = {6L};/* VOLATILE GLOBAL g_491 */
static volatile uint64_t g_526[6][2] = {{18446744073709551608UL,18446744073709551615UL},{18446744073709551608UL,18446744073709551615UL},{18446744073709551608UL,18446744073709551615UL},{18446744073709551608UL,18446744073709551615UL},{18446744073709551608UL,18446744073709551615UL},{18446744073709551608UL,18446744073709551615UL}};
static int16_t ****g_556 = (void*)0;
static int16_t ***** volatile g_555[3][1][9] = {{{&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556}},{{&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556}},{{&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556,&g_556}}};
static int16_t ***** volatile g_557 = &g_556;/* VOLATILE GLOBAL g_557 */
static union U1 g_650 = {1UL};
static int32_t **g_652 = &g_177;
static int32_t ** volatile g_655 = &g_177;/* VOLATILE GLOBAL g_655 */
static uint8_t g_661 = 0x5AL;
static union U2 g_695[4][10] = {{{0xF897L},{1L},{0xF897L},{1L},{1L},{0x17C7L},{0x17C7L},{1L},{1L},{0xF897L}},{{0x5823L},{0x5823L},{0x7BB6L},{1L},{0xBC19L},{0x7BB6L},{0xBC19L},{1L},{0x7BB6L},{0x5823L}},{{0xBC19L},{0x17C7L},{0xF897L},{0xBC19L},{1L},{1L},{0xBC19L},{0xF897L},{0x17C7L},{0xBC19L}},{{0xF897L},{0x5823L},{0x17C7L},{1L},{0x5823L},{1L},{0x17C7L},{0x5823L},{0xF897L},{0xF897L}}};
static int32_t ** const  volatile g_717 = &g_177;/* VOLATILE GLOBAL g_717 */
static union U2 g_720[8][2][10] = {{{{0x3202L},{-1L},{0x10DDL},{0x8AD2L},{-9L},{7L},{0x2B3BL},{1L},{1L},{0x10DDL}},{{1L},{1L},{0xF898L},{0xFD4BL},{0xFD4BL},{0xF898L},{1L},{1L},{7L},{-1L}}},{{{0xF898L},{1L},{1L},{7L},{-1L},{-6L},{0L},{0L},{1L},{0xFD4BL}},{{-9L},{2L},{1L},{-1L},{0x10DDL},{0L},{0x0652L},{1L},{0x732EL},{0x83E1L}}},{{{6L},{0x1B57L},{0xF898L},{-1L},{0xF8E6L},{-9L},{0xF734L},{0xD1AFL},{1L},{7L}},{{0x327AL},{0L},{-9L},{7L},{0xF8E6L},{0x2430L},{0x408EL},{0x9385L},{7L},{0x83E1L}}},{{{0x9385L},{0xF734L},{6L},{0xFD4BL},{0x10DDL},{0x2430L},{0x0686L},{0x2430L},{0x10DDL},{0xFD4BL}},{{0x327AL},{0x53FCL},{0x327AL},{0x3202L},{-1L},{-9L},{0x0686L},{0xF898L},{0x83E1L},{-1L}}},{{{6L},{0xF734L},{0x9385L},{0x732EL},{0xFD4BL},{0L},{0x408EL},{0xF898L},{0x3202L},{0x10DDL}},{{-9L},{0L},{0x327AL},{0x6315L},{0x83E1L},{-6L},{0xF734L},{0x2430L},{0x3202L},{0xF8E6L}}},{{{0xF898L},{0x1B57L},{6L},{0x732EL},{7L},{0xF898L},{0x0652L},{0x9385L},{0x83E1L},{0xF8E6L}},{{1L},{2L},{-9L},{0x3202L},{0x83E1L},{6L},{0L},{0xD1AFL},{0x10DDL},{0x10DDL}}},{{{1L},{1L},{0xF898L},{0xFD4BL},{0xFD4BL},{0xF898L},{1L},{1L},{7L},{-1L}},{{0xF898L},{1L},{1L},{7L},{-1L},{-6L},{0L},{0L},{1L},{0xFD4BL}}},{{{-9L},{2L},{1L},{-1L},{0x10DDL},{0L},{0x0652L},{1L},{0x732EL},{0x83E1L}},{{6L},{0x1B57L},{0xF898L},{-1L},{0xF8E6L},{-9L},{0xF734L},{0xD1AFL},{1L},{7L}}}};
static union U2 *g_719 = &g_720[6][0][2];
static union U1 g_723 = {0xE1DDB358L};
static union U0 *g_750[6][8] = {{&g_70,&g_70,&g_70,(void*)0,&g_70,&g_70,(void*)0,&g_70},{&g_70,&g_70,&g_70,(void*)0,&g_70,(void*)0,(void*)0,(void*)0},{&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,&g_70,(void*)0},{&g_70,&g_70,&g_70,(void*)0,&g_70,&g_70,(void*)0,&g_70},{&g_70,&g_70,&g_70,(void*)0,&g_70,(void*)0,(void*)0,(void*)0},{(void*)0,&g_70,&g_70,&g_70,&g_70,(void*)0,&g_70,&g_70}};
static union U0 **g_749[4] = {&g_750[1][5],&g_750[1][5],&g_750[1][5],&g_750[1][5]};
static union U0 ***g_748 = &g_749[0];
static union U0 **** volatile g_747[3] = {&g_748,&g_748,&g_748};
static union U0 ****g_753[8][8] = {{&g_748,(void*)0,&g_748,&g_748,&g_748,&g_748,&g_748,(void*)0},{&g_748,(void*)0,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748},{&g_748,&g_748,(void*)0,&g_748,&g_748,(void*)0,&g_748,&g_748},{&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748},{&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748},{&g_748,&g_748,(void*)0,&g_748,&g_748,(void*)0,&g_748,&g_748},{&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,(void*)0,&g_748},{(void*)0,&g_748,&g_748,&g_748,&g_748,&g_748,(void*)0,&g_748}};
static union U0 *****g_752 = &g_753[1][5];
static int16_t g_790 = 0xF86CL;
static union U1 *g_816 = &g_723;
static union U1 ** volatile g_815 = &g_816;/* VOLATILE GLOBAL g_815 */
static int64_t **g_819[5][2][1] = {{{&g_211},{&g_211}},{{(void*)0},{(void*)0}},{{(void*)0},{&g_211}},{{&g_211},{(void*)0}},{{(void*)0},{(void*)0}}};
static int64_t *** const  volatile g_818[6][6] = {{(void*)0,(void*)0,&g_819[4][1][0],&g_819[4][1][0],&g_819[1][0][0],&g_819[4][1][0]},{&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0]},{&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0],(void*)0,&g_819[4][1][0]},{&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0],&g_819[1][0][0],&g_819[1][0][0]},{&g_819[4][1][0],&g_819[1][0][0],&g_819[1][0][0],&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0]},{&g_819[4][1][0],&g_819[4][1][0],(void*)0,&g_819[4][1][0],&g_819[4][1][0],&g_819[4][1][0]}};
static int64_t *** const  volatile g_820 = &g_819[3][0][0];/* VOLATILE GLOBAL g_820 */
static float *g_881 = &g_67[1][3];
static float ** const g_880[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile uint8_t g_901 = 255UL;/* VOLATILE GLOBAL g_901 */
static volatile uint32_t g_943 = 1UL;/* VOLATILE GLOBAL g_943 */
static float * volatile g_960[4][7][3] = {{{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0}},{{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]}},{{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0}},{{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]},{(void*)0,(void*)0,(void*)0},{&g_112[0],&g_112[5],&g_112[0]}}};
static float * const  volatile g_961 = &g_112[1];/* VOLATILE GLOBAL g_961 */
static int8_t *g_986 = &g_320;
static int8_t **g_985 = &g_986;
static volatile int64_t *g_991 = (void*)0;
static volatile int64_t * volatile *g_990 = &g_991;
static volatile int8_t g_1034 = (-2L);/* VOLATILE GLOBAL g_1034 */
static uint8_t g_1051 = 0UL;
static volatile union U2 ** volatile g_1057 = &g_201;/* VOLATILE GLOBAL g_1057 */
static volatile float g_1061 = 0x4.124CD2p-10;/* VOLATILE GLOBAL g_1061 */
static volatile int16_t g_1072 = 1L;/* VOLATILE GLOBAL g_1072 */
static int32_t ***g_1084[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t *** const *g_1083 = &g_1084[1];
static int32_t *** const **g_1082 = &g_1083;
static int32_t *** const **g_1085 = &g_1083;
static float **g_1101 = &g_881;
static const union U0 g_1155 = {0x6C45761EA3887EAFLL};
static uint8_t *g_1202 = &g_1051;
static uint8_t * volatile * volatile g_1201 = &g_1202;/* VOLATILE GLOBAL g_1201 */
static int64_t g_1253[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint8_t **g_1289 = &g_1202;
static union U2 g_1290 = {0x457DL};/* VOLATILE GLOBAL g_1290 */
static volatile union U2 g_1323 = {1L};/* VOLATILE GLOBAL g_1323 */
static const int32_t ** volatile g_1364[4][10][6] = {{{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{(void*)0,(void*)0,&g_462,(void*)0,(void*)0,&g_462},{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,(void*)0,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,(void*)0,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462}},{{&g_462,&g_462,(void*)0,(void*)0,&g_462,(void*)0},{(void*)0,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{(void*)0,(void*)0,&g_462,(void*)0,(void*)0,&g_462},{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,(void*)0,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462}},{{&g_462,&g_462,&g_462,(void*)0,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,&g_462,(void*)0,(void*)0,&g_462,(void*)0},{(void*)0,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{(void*)0,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,(void*)0}},{{(void*)0,&g_462,&g_462,&g_462,&g_462,(void*)0},{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{&g_462,&g_462,&g_462,(void*)0,&g_462,&g_462},{&g_462,&g_462,(void*)0,&g_462,&g_462,&g_462},{&g_462,(void*)0,&g_462,&g_462,&g_462,&g_462},{(void*)0,(void*)0,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,(void*)0,&g_462},{(void*)0,&g_462,&g_462,&g_462,&g_462,&g_462},{&g_462,&g_462,&g_462,&g_462,&g_462,&g_462}}};
static const int32_t ** volatile g_1365 = &g_462;/* VOLATILE GLOBAL g_1365 */
static int32_t *g_1398 = &g_325;
static int32_t ** volatile g_1397 = &g_1398;/* VOLATILE GLOBAL g_1397 */
static const union U0 *g_1419 = (void*)0;
static const union U0 **g_1418 = &g_1419;
static const union U0 ***g_1417 = &g_1418;
static const union U0 ****g_1416 = &g_1417;
static const union U0 *****g_1415 = &g_1416;
static float g_1466 = 0x5.D61E03p-67;
static volatile float g_1516 = (-0x1.Ap+1);/* VOLATILE GLOBAL g_1516 */
static volatile uint8_t g_1555 = 0x7AL;/* VOLATILE GLOBAL g_1555 */
static union U1 ** volatile g_1565 = &g_816;/* VOLATILE GLOBAL g_1565 */
static union U2 g_1622 = {1L};/* VOLATILE GLOBAL g_1622 */


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static int32_t * func_6(uint8_t  p_7, int32_t * p_8, uint16_t  p_9, int32_t  p_10);
static uint16_t  func_12(int32_t * p_13, const float  p_14);
static int32_t * func_15(int32_t  p_16, const int32_t * const  p_17);
static const int32_t * const  func_18(int32_t * p_19, const int16_t  p_20, float  p_21, int8_t  p_22);
static int16_t  func_26(float  p_27, uint64_t  p_28, int32_t  p_29, int32_t * p_30, int8_t  p_31);
static float  func_32(float  p_33, int8_t  p_34, int32_t * const  p_35);
static float  func_36(uint8_t  p_37, int32_t * p_38, int32_t * p_39, uint32_t  p_40, int16_t  p_41);
static int32_t * func_43(int32_t * p_44, int32_t * p_45, int32_t * p_46, uint64_t  p_47);
static int32_t * func_48(uint8_t  p_49);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_338 g_111 g_650.f1 g_476 g_477 g_143 g_80
 * writes: g_3 g_338 g_111 g_650.f1
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    int32_t l_11 = 4L;
    int32_t *l_23 = &g_3;
    int32_t l_1185[6][6] = {{6L,0x56BF749AL,0x56BF749AL,6L,0x56BF749AL,0x56BF749AL},{6L,0x56BF749AL,0x56BF749AL,6L,0x56BF749AL,0x56BF749AL},{6L,0x56BF749AL,0x56BF749AL,6L,0x56BF749AL,0x56BF749AL},{6L,0x56BF749AL,0x56BF749AL,6L,0x56BF749AL,0x56BF749AL},{6L,0x56BF749AL,0x56BF749AL,6L,0x56BF749AL,0x56BF749AL},{6L,0x56BF749AL,0x56BF749AL,6L,0x56BF749AL,0x56BF749AL}};
    int32_t l_1613 = 0L;
    int32_t l_1614 = 0L;
    uint8_t **l_1623 = &g_1202;
    uint32_t *l_1646 = (void*)0;
    uint32_t **l_1645 = &l_1646;
    int32_t *l_1653 = &l_1614;
    int32_t *l_1654 = &l_1613;
    int32_t *l_1655 = &l_1614;
    int32_t *l_1656 = &g_68;
    int32_t *l_1657 = &l_1614;
    int32_t *l_1658 = &g_68;
    int32_t *l_1659 = &g_3;
    int32_t *l_1660 = &l_1614;
    int32_t *l_1661 = &g_3;
    int32_t *l_1662 = (void*)0;
    int32_t *l_1663 = &g_325;
    int32_t *l_1664 = &g_3;
    int32_t *l_1665 = (void*)0;
    int32_t *l_1666 = &l_1613;
    int32_t *l_1667 = (void*)0;
    int32_t *l_1668[2];
    uint32_t l_1669 = 4294967288UL;
    int i, j;
    for (i = 0; i < 2; i++)
        l_1668[i] = &l_1613;
    for (g_3 = 0; (g_3 > 13); ++g_3)
    { /* block id: 3 */
        int32_t *l_42[6][10][4] = {{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0,&g_3},{(void*)0,&g_3,&g_3,&g_3},{(void*)0,&g_3,&g_3,(void*)0}},{{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3,&g_3},{(void*)0,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{(void*)0,(void*)0,&g_3,&g_3},{(void*)0,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,(void*)0},{&g_3,(void*)0,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,(void*)0,&g_3,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,(void*)0,(void*)0},{&g_3,&g_3,(void*)0,&g_3},{&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,&g_3},{(void*)0,&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3,&g_3},{&g_3,(void*)0,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3},{&g_3,&g_3,&g_3,&g_3}},{{&g_3,(void*)0,&g_3,&g_3},{&g_3,(void*)0,&g_3,&g_3},{(void*)0,&g_3,&g_3,&g_3},{&g_3,(void*)0,&g_3,&g_3},{&g_3,(void*)0,(void*)0,&g_3},{&g_3,&g_3,&g_3,(void*)0},{(void*)0,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,(void*)0},{&g_3,&g_3,&g_3,&g_3}}};
        const int32_t *l_52 = &g_53;
        const int32_t **l_51 = &l_52;
        int32_t * const l_1186 = &g_3;
        union U1 **l_1610 = &g_816;
        int32_t l_1612 = 0xEFE8CFA1L;
        uint16_t l_1628 = 1UL;
        float **l_1637 = &g_881;
        int i, j, k;
    }
    (*l_23) &= 5L;
    for (g_338 = 28; (g_338 > 21); --g_338)
    { /* block id: 749 */
        int8_t l_1647 = 0xE3L;
        for (g_111 = 0; (g_111 >= 21); g_111++)
        { /* block id: 752 */
            uint8_t l_1649 = 0x4EL;
            uint64_t l_1652 = 0x873634E1DB32FF53LL;
            for (g_650.f1 = 0; (g_650.f1 < 8); ++g_650.f1)
            { /* block id: 755 */
                int32_t *l_1648[6] = {&g_3,&g_3,&g_3,&g_3,&g_3,&g_3};
                int i;
                (*l_23) &= ((void*)0 == l_1645);
                l_1649++;
            }
            if (l_1652)
                break;
        }
    }
    --l_1669;
    return (***g_476);
}


/* ------------------------------------------ */
/* 
 * reads : g_717 g_177
 * writes:
 */
static int32_t * func_6(uint8_t  p_7, int32_t * p_8, uint16_t  p_9, int32_t  p_10)
{ /* block id: 719 */
    int32_t *l_1597 = &g_68;
    int32_t *l_1598 = (void*)0;
    int32_t *l_1599 = &g_325;
    int32_t *l_1600 = &g_68;
    int32_t *l_1601 = &g_325;
    int32_t *l_1602 = &g_325;
    int32_t l_1603[4][1][6] = {{{0xF16B15D4L,4L,0x37702552L,0x37702552L,4L,0xF16B15D4L}},{{1L,0xF16B15D4L,0x37702552L,0xF16B15D4L,1L,0xF16B15D4L}},{{0x37702552L,0xC331E50FL,0xC331E50FL,0x37702552L,1L,0x37702552L}},{{0x37702552L,1L,0x37702552L,0xC331E50FL,0xC331E50FL,0x37702552L}}};
    int32_t *l_1604 = &l_1603[1][0][4];
    int32_t *l_1605[4][1];
    int16_t l_1606 = 0xB89DL;
    uint32_t l_1607 = 18446744073709551609UL;
    int i, j, k;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
            l_1605[i][j] = &l_1603[1][0][4];
    }
    l_1607--;
    return (*g_717);
}


/* ------------------------------------------ */
/* 
 * reads : g_110 g_557 g_556 g_210 g_211 g_94 g_111 g_116 g_986 g_1415 g_320 g_300 g_526 g_477 g_143 g_256 g_80 g_1253 g_1202 g_1051 g_1289 g_462 g_53 g_325 g_476 g_1155.f0 g_1201 g_1365 g_240 g_3 g_652 g_650.f2 g_1034 g_661 g_113 g_881 g_650.f1 g_961 g_112 g_67 g_1555 g_723.f2 g_177 g_2 g_1397 g_655 g_1565
 * writes: g_116 g_320 g_1415 g_256 g_80 g_177 g_110 g_650.f2 g_67 g_650.f1 g_240 g_112 g_1466 g_1290.f2 g_723.f2 g_462 g_723.f1 g_752 g_1398 g_816 g_1051
 */
static uint16_t  func_12(int32_t * p_13, const float  p_14)
{ /* block id: 640 */
    int16_t ****l_1405 = &g_476;
    union U0 l_1406[7] = {{18446744073709551613UL},{18446744073709551613UL},{18446744073709551613UL},{18446744073709551613UL},{18446744073709551613UL},{18446744073709551613UL},{18446744073709551613UL}};
    int32_t l_1408 = 0x7B04D8E9L;
    int32_t l_1409 = 0x78412A1DL;
    int32_t l_1410 = 0xB64BCA8AL;
    union U0 *****l_1420 = &g_753[1][5];
    union U0 *****l_1421 = &g_753[3][2];
    int32_t ****l_1423 = &g_1084[1];
    int32_t **** const *l_1422[2][3][5] = {{{&l_1423,&l_1423,&l_1423,&l_1423,&l_1423},{&l_1423,&l_1423,&l_1423,&l_1423,&l_1423},{&l_1423,&l_1423,(void*)0,&l_1423,&l_1423}},{{&l_1423,&l_1423,&l_1423,&l_1423,&l_1423},{&l_1423,&l_1423,&l_1423,&l_1423,&l_1423},{&l_1423,&l_1423,(void*)0,&l_1423,&l_1423}}};
    int64_t l_1424[9] = {0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL,0x9FBAAC6AC594FFFELL};
    union U1 l_1426[4] = {{0xD8789717L},{0xD8789717L},{0xD8789717L},{0xD8789717L}};
    uint64_t *l_1427 = (void*)0;
    uint64_t **l_1428 = &l_1427;
    uint32_t *l_1429[3];
    int16_t l_1447[9][9][3] = {{{0xFE5AL,0L,0x288FL},{0x1294L,5L,0x8711L},{(-1L),2L,0x21C6L},{0L,0x46B4L,3L},{0L,5L,0x7778L},{9L,0xC118L,0x2CFBL},{(-2L),(-1L),0x1EDFL},{0xBFB2L,0x3B2FL,0x1EDFL},{(-1L),0L,0x2CFBL}},{{(-1L),1L,0x7778L},{3L,0xFE5AL,3L},{1L,0x050FL,0x21C6L},{5L,(-1L),0x8711L},{3L,(-2L),0x288FL},{4L,1L,0x8711L},{0xB260L,0x46B4L,0x21C6L},{0xBFB2L,0x8542L,3L},{0xC118L,1L,0x7778L}},{{0x38B1L,0L,0x2CFBL},{0L,0L,0x1EDFL},{(-1L),0x050FL,0x1EDFL},{0xB260L,3L,0x2CFBL},{0x1294L,0x85C2L,0x7778L},{0L,0L,3L},{5L,0x4018L,0x21C6L},{0x5D93L,0L,0x8711L},{0L,0xC118L,0x288FL}},{{(-1L),0x5D93L,0x8711L},{0x2C95L,0x8542L,0x21C6L},{(-1L),2L,3L},{(-2L),0x5D93L,0x7778L},{0x70E4L,(-2L),0x2CFBL},{0xC118L,0x0EDEL,0x1EDFL},{0L,0x4018L,0x1EDFL},{0x2C95L,0xFE5AL,0x2CFBL},{4L,0x08FCL,0x7778L}},{{0xFE5AL,3L,3L},{0x5D93L,0x3B2FL,0x21C6L},{1L,0x0EDEL,0x8711L},{0xFE5AL,0L,0x288FL},{0x1294L,5L,0x8711L},{(-1L),2L,0x21C6L},{0L,0x46B4L,1L},{4L,0L,(-1L)},{(-3L),1L,9L}},{{0x503BL,0x09C3L,0x08FCL},{0x97C0L,1L,0x08FCL},{0x53E7L,0xCA20L,9L},{(-9L),0x66D5L,(-1L)},{0x0FDDL,(-4L),1L},{(-8L),(-9L),(-1L)},{0L,0x09C3L,0x050FL},{0x0FDDL,0x503BL,0xC118L},{0xF3FEL,(-8L),0x050FL}},{{0xA30EL,0x40BAL,(-1L)},{0x97C0L,(-8L),1L},{1L,(-8L),(-1L)},{0xB774L,4L,9L},{4L,4L,0x08FCL},{0x3B96L,(-9L),0x08FCL},{0xA30EL,0x0FDDL,9L},{(-4L),0xA1E1L,(-1L)},{0xCA20L,0xCA20L,1L}},{{0L,5L,(-1L)},{0x6105L,4L,0x050FL},{0xCA20L,1L,0xC118L},{(-9L),0x6105L,0x050FL},{0x1387L,(-8L),(-1L)},{0x3B96L,0xFE22L,1L},{0x503BL,0x6105L,(-1L)},{0x19B7L,0x503BL,9L},{1L,0x63E6L,0x08FCL}},{{(-9L),5L,0x08FCL},{0x1387L,(-4L),9L},{0xF3FEL,(-2L),(-1L)},{(-4L),0x0FDDL,1L},{0x6105L,1L,(-1L)},{(-8L),0x63E6L,0x050FL},{(-4L),4L,0xC118L},{(-4L),0L,0x050FL},{0x53E7L,0xFE22L,(-1L)}}};
    uint8_t l_1448 = 0x6FL;
    int64_t l_1449 = 4L;
    const int32_t l_1459 = (-1L);
    uint32_t l_1462 = 18446744073709551611UL;
    uint8_t l_1465[10] = {0x90L,1UL,1UL,0x90L,3UL,0x90L,1UL,1UL,0x90L,3UL};
    int64_t **l_1479 = &g_211;
    int64_t l_1513[8] = {0x0DF7E0205ACE95CELL,0x2D78BBF7F991DDFALL,0x2D78BBF7F991DDFALL,0x0DF7E0205ACE95CELL,0x2D78BBF7F991DDFALL,0x2D78BBF7F991DDFALL,0x0DF7E0205ACE95CELL,0x2D78BBF7F991DDFALL};
    int64_t l_1519 = 0xBF8DBABE035BB01FLL;
    int32_t l_1556 = 0x08E559B3L;
    int32_t l_1570 = 0xBD5AAE93L;
    uint16_t l_1573 = 65535UL;
    int32_t l_1583 = 0xFBE35DF8L;
    float *l_1595[3];
    int32_t l_1596 = 0xACEF7C46L;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1429[i] = &g_650.f0;
    for (i = 0; i < 3; i++)
        l_1595[i] = (void*)0;
    l_1410 &= (((g_110 , (safe_unary_minus_func_uint32_t_u((safe_add_func_uint16_t_u_u((l_1409 |= (((-1L) < (l_1405 != (l_1406[0] , (*g_557)))) > ((0x50L & ((*g_986) = ((0x4FE0F116L & (g_116 ^= (((((~4L) < (l_1408 |= (**g_210))) | 0x3E287314L) != 1UL) <= 0UL))) < l_1406[0].f0))) < l_1406[0].f0))), l_1406[0].f0))))) , 0x2B127D11L) == 0xE38DCFDDL);
    l_1424[2] ^= ((safe_mod_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(((g_1415 = g_1415) != (l_1421 = (l_1420 = &g_753[1][5]))), (*g_986))), (*g_986))) , (l_1422[1][1][2] == &l_1423));
lbl_1492:
    l_1449 &= (((!((l_1426[1] , g_300[3][0][3]) != g_526[2][1])) & (((g_116 ^= (((*l_1428) = l_1427) != &g_338)) | (safe_div_func_uint64_t_u_u(((safe_add_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_s((((safe_sub_func_int32_t_s_s(((**g_477) , ((g_1253[3] ^ (((safe_div_func_int32_t_s_s((!((*g_1202) ^ (safe_add_func_int8_t_s_s((&l_1426[2] != &l_1426[3]), (**g_1289))))), (*g_462))) == (*g_1202)) == 8UL)) , l_1447[4][5][1])), (*g_462))) , (-9L)) || (*g_986)), 7)), (**g_1289))), (*g_986))), g_325)) ^ l_1448), (-2L)))) <= 18446744073709551615UL)) >= 1L);
    if (((safe_rshift_func_int8_t_s_s((((safe_rshift_func_uint16_t_u_u(((p_14 , ((&g_985 == (void*)0) ^ ((**g_210) != ((safe_lshift_func_uint8_t_u_u((0x871EL < ((**g_210) < (safe_mul_func_uint8_t_u_u((+l_1459), ((safe_mul_func_uint8_t_u_u((l_1462 != (safe_lshift_func_int16_t_s_u((***g_476), 6))), 0UL)) < g_1155.f0))))), (**g_1201))) & 3L)))) , 1UL), 15)) != (*g_986)) , l_1465[8]), 6)) , (**g_1365)))
    { /* block id: 653 */
        int32_t *l_1467 = &g_3;
        int16_t l_1471 = 0x751AL;
        int32_t l_1472 = (-8L);
        int32_t l_1473 = 0xFA163337L;
        int32_t l_1474[6] = {0x1D33CCF7L,0x1D33CCF7L,(-1L),0x1D33CCF7L,0x1D33CCF7L,(-1L)};
        int8_t ***l_1484 = &g_985;
        union U0 *l_1490[7][7][1] = {{{&l_1406[0]},{(void*)0},{&l_1406[0]},{&l_1406[0]},{(void*)0},{&l_1406[3]},{&l_1406[0]}},{{&l_1406[0]},{&l_1406[3]},{(void*)0},{&l_1406[0]},{&l_1406[0]},{(void*)0},{&l_1406[0]}},{{&l_1406[0]},{(void*)0},{(void*)0},{&l_1406[5]},{(void*)0},{(void*)0},{&l_1406[0]}},{{&l_1406[0]},{(void*)0},{&l_1406[0]},{&l_1406[0]},{(void*)0},{&l_1406[3]},{&l_1406[0]}},{{&l_1406[0]},{&l_1406[3]},{(void*)0},{&l_1406[0]},{&l_1406[0]},{(void*)0},{&l_1406[0]}},{{&l_1406[0]},{(void*)0},{(void*)0},{&l_1406[5]},{(void*)0},{(void*)0},{&l_1406[0]}},{{&l_1406[0]},{(void*)0},{&l_1406[0]},{&l_1406[0]},{(void*)0},{&l_1406[3]},{&l_1406[0]}}};
        float l_1517 = 0x2.B2DCACp-64;
        uint64_t l_1520 = 0x0AD2FF2371835898LL;
        int i, j, k;
        p_13 = l_1467;
        if ((safe_lshift_func_int8_t_s_u(((+((*g_143) && 0xA345L)) && g_526[0][1]), 6)))
        { /* block id: 655 */
            uint64_t l_1475 = 0x642E76C009E48628LL;
            int64_t l_1485 = 0x5B20AD26FB950A20LL;
            int32_t l_1486[8];
            int16_t *l_1487 = &l_1447[1][8][0];
            int32_t l_1491 = 0L;
            int i;
            for (i = 0; i < 8; i++)
                l_1486[i] = 1L;
            l_1475--;
            if ((+((l_1491 = (((void*)0 == l_1479) | (((safe_sub_func_int16_t_s_s(((*l_1487) = ((l_1486[2] |= (safe_add_func_int16_t_s_s(((*g_143) = ((void*)0 != l_1484)), l_1485))) && (l_1473 = l_1475))), (safe_div_func_int32_t_s_s((g_240 >= ((void*)0 != l_1490[6][3][0])), (**g_1365))))) < (*g_211)) , (*p_13)))) <= (*p_13))))
            { /* block id: 662 */
                (*g_652) = (void*)0;
            }
            else
            { /* block id: 664 */
                float l_1504 = 0x3.8F21C4p+95;
                int32_t l_1510[4][2] = {{(-6L),(-6L)},{(-6L),(-6L)},{(-6L),(-6L)},{(-6L),(-6L)}};
                int i, j;
                if (l_1475)
                    goto lbl_1492;
                for (g_110 = 0; (g_110 >= 26); g_110 = safe_add_func_uint8_t_u_u(g_110, 1))
                { /* block id: 668 */
                    int64_t l_1509[4][9] = {{0x17025A2D9673C99FLL,0x11871457C432B42CLL,0x11871457C432B42CLL,0x17025A2D9673C99FLL,0x17025A2D9673C99FLL,0x11871457C432B42CLL,0x11871457C432B42CLL,0x17025A2D9673C99FLL,0x17025A2D9673C99FLL},{(-6L),0xCB6EB141E34518EFLL,(-6L),0xCB6EB141E34518EFLL,(-6L),0xCB6EB141E34518EFLL,(-6L),0xCB6EB141E34518EFLL,(-6L)},{0x17025A2D9673C99FLL,0x17025A2D9673C99FLL,0x11871457C432B42CLL,0x11871457C432B42CLL,0x17025A2D9673C99FLL,0x17025A2D9673C99FLL,0x11871457C432B42CLL,0x11871457C432B42CLL,0x17025A2D9673C99FLL},{0xFA3190274878A5C3LL,0xCB6EB141E34518EFLL,0xFA3190274878A5C3LL,0xCB6EB141E34518EFLL,0xFA3190274878A5C3LL,0xCB6EB141E34518EFLL,0xFA3190274878A5C3LL,0xCB6EB141E34518EFLL,0xFA3190274878A5C3LL}};
                    int i, j;
                    for (g_650.f2 = 0; (g_650.f2 <= 13); g_650.f2++)
                    { /* block id: 671 */
                        int32_t l_1505[9][2][8] = {{{1L,0x5202EDBFL,6L,3L,1L,0xD2A1F66BL,1L,0x4AE2F1CAL},{0x7E5AFEB3L,0x5202EDBFL,0xE71FA5A8L,1L,0xA7670CA2L,(-1L),1L,1L}},{{0xE104E0A5L,0x4074F25AL,0x2B5913CAL,8L,(-1L),1L,0x88E504CAL,1L},{1L,1L,(-1L),1L,1L,(-2L),0xE104E0A5L,3L}},{{0xE71FA5A8L,0xA7670CA2L,0x7B49B314L,0xD6827F59L,8L,0x7B49B314L,(-1L),1L},{0x5202EDBFL,1L,0x7B49B314L,0x4AE2F1CAL,0x4074F25AL,0x7E5AFEB3L,0xE104E0A5L,1L}},{{8L,9L,(-1L),3L,0x4AE2F1CAL,6L,0x88E504CAL,0x88E504CAL},{0x7B49B314L,0x9F5C767EL,0x2B5913CAL,0x2B5913CAL,0x9F5C767EL,0x7B49B314L,1L,0xE104E0A5L}},{{0x88E504CAL,(-1L),0xE71FA5A8L,0x9F5C767EL,1L,1L,1L,(-1L)},{8L,1L,6L,0x9F5C767EL,0xE104E0A5L,(-1L),3L,0xE104E0A5L}},{{0x2B5913CAL,0xE104E0A5L,0x4074F25AL,0x2B5913CAL,8L,(-1L),1L,0x88E504CAL},{1L,0x2B5913CAL,0x7E5AFEB3L,3L,0x7E5AFEB3L,0x2B5913CAL,1L,1L}},{{1L,0x5202EDBFL,2L,0x4AE2F1CAL,0xA7670CA2L,0x20947303L,0x4AE2F1CAL,1L},{0x4074F25AL,0xE104E0A5L,0x2B5913CAL,0xD6827F59L,0xA7670CA2L,1L,0x7B49B314L,3L}},{{1L,0xE71FA5A8L,0xD6827F59L,1L,0x7E5AFEB3L,1L,0xE104E0A5L,1L},{1L,0xA7670CA2L,0xF32A6D56L,8L,8L,0xF32A6D56L,0xA7670CA2L,1L}},{{0x2B5913CAL,0x9F5C767EL,0x7B49B314L,1L,0xE104E0A5L,0x7E5AFEB3L,0x4074F25AL,0x4AE2F1CAL},{8L,(-9L),0xD6827F59L,3L,1L,0x7E5AFEB3L,0x88E504CAL,0x7B49B314L}}};
                        int i, j, k;
                        (*g_881) = ((((safe_rshift_func_uint16_t_u_u(g_1034, (l_1485 | g_661))) <= (g_113[7] | (safe_rshift_func_int16_t_s_s((((((safe_mod_func_int32_t_s_s(0L, (safe_unary_minus_func_int16_t_s(l_1505[5][1][6])))) | (((safe_mul_func_uint8_t_u_u((0x3DF1556952DFBE44LL ^ (((+((*l_1428) != &g_338)) && (*g_986)) | (-6L))), (*g_1202))) == (-4L)) || 0xEA6A94596FA5F48BLL)) < (**g_1289)) ^ l_1505[5][1][6]) , l_1509[2][2]), l_1510[0][1])))) , p_14) != p_14);
                    }
                }
            }
        }
        else
        { /* block id: 676 */
            int16_t l_1511 = 0xA962L;
            int32_t l_1512 = 0x1244D440L;
            int32_t l_1514 = 0x63B1D662L;
            int32_t l_1515[10] = {2L,2L,6L,2L,2L,2L,0x14A3CB2EL,0x14A3CB2EL,2L,0x14A3CB2EL};
            int16_t l_1518 = 4L;
            int64_t l_1553 = 0x71AB65ABFB6441D6LL;
            int i;
            l_1520--;
            for (g_650.f1 = 0; (g_650.f1 <= 1); g_650.f1 += 1)
            { /* block id: 680 */
                int16_t l_1539 = 0x8974L;
                for (g_240 = 1; (g_240 >= 0); g_240 -= 1)
                { /* block id: 683 */
                    float *l_1537 = (void*)0;
                    float *l_1538 = &g_112[1];
                    float *l_1540 = (void*)0;
                    float *l_1541 = &g_1466;
                    int32_t l_1546 = 0L;
                    uint64_t *l_1548[2][6] = {{&l_1520,&g_338,&g_338,&l_1520,&g_338,&g_338},{&l_1520,&g_338,&g_338,&l_1520,&g_338,&g_338}};
                    uint64_t **l_1547 = &l_1548[0][2];
                    uint32_t **l_1551[4][8][6] = {{{&l_1429[2],(void*)0,&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0]},{&l_1429[0],&l_1429[1],&l_1429[0],&l_1429[0],&l_1429[1],&l_1429[0]},{&l_1429[1],&l_1429[0],&l_1429[0],&l_1429[0],(void*)0,(void*)0},{&l_1429[2],&l_1429[0],&l_1429[1],(void*)0,(void*)0,&l_1429[1]},{&l_1429[2],&l_1429[2],&l_1429[1],&l_1429[0],&l_1429[0],&l_1429[0]},{&l_1429[1],&l_1429[2],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[1]},{&l_1429[0],&l_1429[1],&l_1429[0],&l_1429[1],&l_1429[2],&l_1429[0]},{&l_1429[0],&l_1429[1],&l_1429[1],&l_1429[0],&l_1429[0],&l_1429[1]}},{{&l_1429[0],&l_1429[0],&l_1429[1],&l_1429[2],&l_1429[1],(void*)0},{(void*)0,&l_1429[0],&l_1429[0],&l_1429[0],(void*)0,&l_1429[0]},{(void*)0,&l_1429[2],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0]},{&l_1429[0],(void*)0,&l_1429[0],&l_1429[2],&l_1429[0],&l_1429[2]},{&l_1429[0],(void*)0,(void*)0,&l_1429[0],&l_1429[0],&l_1429[0]},{&l_1429[2],(void*)0,&l_1429[0],(void*)0,&l_1429[0],&l_1429[0]},{&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[1],&l_1429[0],&l_1429[0]},{(void*)0,&l_1429[2],&l_1429[1],&l_1429[0],(void*)0,&l_1429[1]}},{{&l_1429[2],&l_1429[0],&l_1429[0],(void*)0,&l_1429[0],&l_1429[0]},{&l_1429[0],&l_1429[0],&l_1429[1],&l_1429[2],(void*)0,&l_1429[0]},{&l_1429[0],&l_1429[2],&l_1429[0],&l_1429[1],&l_1429[0],&l_1429[1]},{&l_1429[0],&l_1429[0],(void*)0,&l_1429[0],&l_1429[0],&l_1429[0]},{&l_1429[0],(void*)0,&l_1429[0],(void*)0,&l_1429[0],(void*)0},{&l_1429[1],(void*)0,&l_1429[0],&l_1429[2],&l_1429[0],&l_1429[0]},{&l_1429[0],(void*)0,&l_1429[0],(void*)0,&l_1429[0],&l_1429[0]},{&l_1429[0],&l_1429[2],(void*)0,&l_1429[1],(void*)0,&l_1429[0]}},{{&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[1],&l_1429[2]},{&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0]},{&l_1429[2],&l_1429[1],&l_1429[0],(void*)0,&l_1429[2],(void*)0},{&l_1429[1],&l_1429[1],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0]},{&l_1429[1],&l_1429[2],&l_1429[0],(void*)0,&l_1429[0],(void*)0},{&l_1429[1],&l_1429[1],&l_1429[0],&l_1429[0],&l_1429[1],&l_1429[2]},{&l_1429[1],&l_1429[0],&l_1429[0],(void*)0,&l_1429[1],&l_1429[1]},{&l_1429[2],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[0],&l_1429[1]}}};
                    int16_t l_1552 = 0x4618L;
                    int i, j, k;
                    (*l_1541) = ((safe_div_func_float_f_f(((*l_1538) = ((safe_sub_func_float_f_f((((void*)0 == p_13) > (safe_add_func_float_f_f(0xF.64E097p+59, (*l_1467)))), (*g_961))) != ((*g_881) = (((safe_sub_func_float_f_f(((void*)0 == &g_116), (safe_div_func_float_f_f(((safe_mul_func_float_f_f((*g_881), (safe_sub_func_float_f_f(0xC.93AF02p+53, 0x0.0p-1)))) < p_14), (-0x2.3p-1))))) < (*g_881)) > p_14)))), l_1539)) != (*l_1467));
                    l_1514 |= (safe_add_func_uint64_t_u_u((g_1290.f2 = (((((*l_1467) , ((-1L) < ((((safe_lshift_func_int16_t_s_u(0L, 5)) <= (l_1546 != (((((*l_1428) = &g_338) != ((*l_1547) = &g_338)) | (*g_1202)) > (safe_sub_func_int8_t_s_s((*g_986), l_1512))))) , (void*)0) == (void*)0))) >= (*p_13)) < l_1515[1]) | l_1539)), g_526[4][0]));
                    l_1552 ^= ((void*)0 == l_1551[0][5][2]);
                }
                return l_1511;
            }
            (*g_652) = p_13;
        }
        l_1472 ^= (*l_1467);
    }
    else
    { /* block id: 698 */
        union U1 *l_1564 = (void*)0;
        int32_t l_1567[3];
        int16_t l_1571[10][2];
        int32_t l_1576 = 1L;
        union U0 l_1589[7][3][1] = {{{{8UL}},{{18446744073709551606UL}},{{8UL}}},{{{0x925835A4EA60559BLL}},{{18446744073709551606UL}},{{0x4DA140C3A7280E5CLL}}},{{{0x4DA140C3A7280E5CLL}},{{18446744073709551606UL}},{{0x925835A4EA60559BLL}}},{{{8UL}},{{18446744073709551606UL}},{{8UL}}},{{{0x925835A4EA60559BLL}},{{18446744073709551606UL}},{{0x4DA140C3A7280E5CLL}}},{{{0x4DA140C3A7280E5CLL}},{{18446744073709551606UL}},{{0x925835A4EA60559BLL}}},{{{8UL}},{{18446744073709551606UL}},{{8UL}}}};
        int64_t l_1594 = 0x9D011FA7ECB50A7CLL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1567[i] = 0L;
        for (i = 0; i < 10; i++)
        {
            for (j = 0; j < 2; j++)
                l_1571[i][j] = 0L;
        }
        if ((~g_1555))
        { /* block id: 699 */
            uint32_t l_1557 = 0x40F63984L;
            int32_t l_1560 = 0x9724BFBDL;
            l_1557--;
            l_1560 = 0xB0961A24L;
            for (l_1462 = 11; (l_1462 != 31); ++l_1462)
            { /* block id: 704 */
                uint64_t l_1563[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_1563[i] = 0x5ADDEC9954F4D37BLL;
                (*g_655) = func_15(l_1563[1], p_13);
            }
        }
        else
        { /* block id: 707 */
            float *l_1566 = &g_67[3][0];
            int32_t l_1568[1][1][3];
            int32_t l_1569 = 0x938BE1C5L;
            int64_t l_1572 = 0xAEE174BDC88EC200LL;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 3; k++)
                        l_1568[i][j][k] = 0x86DE7990L;
                }
            }
            (*g_1565) = l_1564;
            (*l_1566) = ((void*)0 == l_1566);
            l_1573--;
        }
        l_1567[0] &= l_1576;
        l_1576 ^= ((safe_mul_func_uint8_t_u_u(((safe_add_func_uint8_t_u_u(((*g_1202) = 0UL), (safe_rshift_func_int16_t_s_s(((l_1583 <= ((l_1567[1] = (((****l_1405) = (safe_rshift_func_int8_t_s_u((-1L), 2))) < 0L)) != l_1571[6][1])) < ((!0x74008867L) , (safe_mul_func_int8_t_s_s((l_1589[2][2][0] , (((safe_add_func_uint32_t_u_u((safe_mul_func_int8_t_s_s((*g_986), l_1589[2][2][0].f0)), l_1594)) , l_1429[0]) != l_1595[1])), 0x05L)))), 5)))) , 255UL), l_1571[7][0])) < l_1596);
    }
    return g_1034;
}


/* ------------------------------------------ */
/* 
 * reads : g_986 g_320 g_723.f2 g_1365 g_723.f1 g_652 g_177 g_2 g_477 g_143 g_256 g_80 g_1289 g_1202 g_1051 g_1397
 * writes: g_723.f2 g_462 g_723.f1 g_752 g_177 g_1398
 */
static int32_t * func_15(int32_t  p_16, const int32_t * const  p_17)
{ /* block id: 620 */
    uint32_t l_1349 = 0xFFA8A66FL;
    int32_t l_1350 = 0xB12054DBL;
    uint16_t *l_1354 = &g_723.f2;
    int32_t l_1355 = (-9L);
    union U0 * const *l_1356[7] = {&g_750[2][1],&g_750[2][1],&g_750[2][1],&g_750[2][1],&g_750[2][1],&g_750[2][1],&g_750[2][1]};
    int16_t l_1357 = 0x65B1L;
    int32_t *l_1366 = &l_1355;
    int32_t *l_1367 = (void*)0;
    int32_t *l_1368 = &l_1350;
    int32_t *l_1369 = &l_1350;
    int32_t *l_1370 = &l_1355;
    int32_t *l_1371 = &l_1355;
    int32_t *l_1372 = &g_325;
    int32_t *l_1373 = (void*)0;
    uint64_t l_1374[3][9] = {{18446744073709551615UL,0x3AD9C17E981AD6EELL,0x43D960547FE89E1ALL,0x43D960547FE89E1ALL,0x3AD9C17E981AD6EELL,18446744073709551615UL,0x3AD9C17E981AD6EELL,0x43D960547FE89E1ALL,0x43D960547FE89E1ALL},{6UL,6UL,18446744073709551615UL,0x43D960547FE89E1ALL,18446744073709551615UL,6UL,6UL,18446744073709551615UL,0x43D960547FE89E1ALL},{0x3F226D633D392319LL,0x3AD9C17E981AD6EELL,0x3F226D633D392319LL,18446744073709551615UL,18446744073709551615UL,0x3F226D633D392319LL,0x3AD9C17E981AD6EELL,0x3F226D633D392319LL,18446744073709551615UL}};
    union U1 *l_1377 = (void*)0;
    const float l_1395 = 0x2.9C1537p-36;
    int32_t l_1396 = 4L;
    uint8_t l_1399[1];
    int i, j;
    for (i = 0; i < 1; i++)
        l_1399[i] = 0x6DL;
    l_1355 = (((l_1350 = l_1349) > 0x081AL) != ((*l_1354) &= (safe_rshift_func_uint8_t_u_s((~p_16), (*g_986)))));
    (*g_1365) = (((void*)0 != l_1356[6]) , p_17);
    l_1374[1][3]--;
    if ((l_1377 != (void*)0))
    { /* block id: 626 */
        int32_t *l_1378 = (void*)0;
        return l_1378;
    }
    else
    { /* block id: 628 */
        int32_t *l_1382 = &l_1355;
        union U0 *****l_1394 = &g_753[1][5];
        for (g_723.f1 = 0; (g_723.f1 == 6); ++g_723.f1)
        { /* block id: 631 */
            int32_t *l_1381[1][2][8] = {{{&l_1350,&g_3,&l_1350,&l_1350,&g_3,&l_1350,&l_1350,&g_3},{&g_3,&l_1350,&l_1350,&g_3,&l_1350,&l_1350,&g_3,&l_1350}}};
            int16_t *l_1391 = &l_1357;
            int i, j, k;
            (*g_1397) = ((*g_652) = func_43(func_43(l_1381[0][0][4], (*g_652), l_1382, ((p_16 | 0x17A4L) == ((safe_mul_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(((safe_add_func_int64_t_s_s((safe_add_func_uint16_t_u_u(g_2[0][0], (**g_477))), ((((*l_1391) |= p_16) , ((safe_mod_func_uint32_t_u_u((((l_1394 == &g_753[1][5]) , (*l_1371)) && (*l_1371)), 0x0FA85F9FL)) ^ (**g_1289))) && l_1396))) && (*g_986)), 0x64A5L)), p_16)) || (-1L)))), (*g_652), l_1381[0][0][4], (*l_1382)));
            ++l_1399[0];
            (*l_1382) &= 0x9A464727L;
        }
    }
    return (*g_652);
}


/* ------------------------------------------ */
/* 
 * reads : g_655 g_177 g_110 g_143 g_3 g_652
 * writes: g_325 g_256 g_80 g_752 g_177
 */
static const int32_t * const  func_18(int32_t * p_19, const int16_t  p_20, float  p_21, int8_t  p_22)
{ /* block id: 611 */
    uint8_t l_1339 = 1UL;
    float *l_1343[2];
    int32_t **l_1344[4][8][6] = {{{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177}},{{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177}},{{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177}},{{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177},{&g_177,&g_177,&g_177,&g_177,&g_177,&g_177}}};
    int32_t *l_1347 = &g_68;
    const int32_t * const l_1348 = &g_53;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1343[i] = &g_98[3][3];
    if ((safe_lshift_func_int8_t_s_s(1L, 4)))
    { /* block id: 612 */
        uint8_t ** const *l_1337 = &g_1289;
        int32_t *l_1338 = &g_325;
        l_1339 |= ((*l_1338) = ((void*)0 != l_1337));
    }
    else
    { /* block id: 615 */
        int32_t **l_1345 = &g_177;
        union U1 l_1346 = {0x77E87844L};
        (*g_652) = func_43((*g_655), (((4L <= ((*g_143) = ((p_20 >= (~l_1339)) | ((safe_rshift_func_uint16_t_u_s(((void*)0 != l_1343[1]), (l_1344[3][1][2] == l_1345))) , ((p_22 , l_1346) , g_110))))) | (**l_1345)) , (*l_1345)), l_1347, p_22);
    }
    return l_1348;
}


/* ------------------------------------------ */
/* 
 * reads : g_652 g_476 g_477 g_143 g_256 g_80
 * writes: g_177
 */
static int16_t  func_26(float  p_27, uint64_t  p_28, int32_t  p_29, int32_t * p_30, int8_t  p_31)
{ /* block id: 603 */
    int64_t l_1320 = 0x71F82F7B47305F3CLL;
    int32_t l_1326[4][2] = {{8L,0xD0BFD5D3L},{8L,0xD0BFD5D3L},{8L,0xD0BFD5D3L},{8L,0xD0BFD5D3L}};
    uint8_t *l_1327 = &g_661;
    int32_t *l_1328 = &g_68;
    int32_t *l_1329 = (void*)0;
    int32_t *l_1330[10][6][3] = {{{(void*)0,&g_3,(void*)0},{&g_3,&l_1326[1][0],(void*)0},{&g_3,&g_3,&l_1326[2][1]},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&l_1326[2][1],&g_3,&g_68},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]}},{{&g_3,&l_1326[2][1],&g_68},{&g_3,&g_3,&l_1326[1][0]},{(void*)0,&l_1326[2][1],&l_1326[2][1]},{&l_1326[1][0],&l_1326[1][0],(void*)0},{(void*)0,&g_3,(void*)0},{&g_3,&l_1326[1][0],(void*)0}},{{&g_3,&g_3,&l_1326[2][1]},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&l_1326[2][1],&g_3,&g_68},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&g_3,&l_1326[2][1],&g_68},{&g_3,&g_3,&l_1326[1][0]}},{{(void*)0,&l_1326[2][1],&l_1326[2][1]},{&l_1326[1][0],&l_1326[1][0],(void*)0},{(void*)0,&g_3,(void*)0},{&g_3,&l_1326[1][0],(void*)0},{&g_3,&g_3,&l_1326[2][1]},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]}},{{&l_1326[2][1],&g_3,&g_68},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&g_3,&l_1326[2][1],&g_68},{&g_3,&g_3,&l_1326[1][0]},{(void*)0,&l_1326[2][1],&l_1326[2][1]},{&l_1326[1][0],&l_1326[1][0],(void*)0}},{{(void*)0,&g_3,&g_68},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&l_1326[2][1],&l_1326[2][1],(void*)0},{(void*)0,&l_1326[1][0],&l_1326[1][0]},{(void*)0,&g_3,&g_3},{(void*)0,&g_3,(void*)0}},{{&l_1326[2][1],(void*)0,&g_3},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&g_68,(void*)0,(void*)0},{&l_1326[1][0],&g_3,&l_1326[1][0]},{&g_68,&g_3,&g_68},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]}},{{&l_1326[2][1],&l_1326[2][1],(void*)0},{(void*)0,&l_1326[1][0],&l_1326[1][0]},{(void*)0,&g_3,&g_3},{(void*)0,&g_3,(void*)0},{&l_1326[2][1],(void*)0,&g_3},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]}},{{&g_68,(void*)0,(void*)0},{&l_1326[1][0],&g_3,&l_1326[1][0]},{&g_68,&g_3,&g_68},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&l_1326[2][1],&l_1326[2][1],(void*)0},{(void*)0,&l_1326[1][0],&l_1326[1][0]}},{{(void*)0,&g_3,&g_3},{(void*)0,&g_3,(void*)0},{&l_1326[2][1],(void*)0,&g_3},{&l_1326[1][0],&l_1326[1][0],&l_1326[1][0]},{&g_68,(void*)0,(void*)0},{&l_1326[1][0],&g_3,&l_1326[1][0]}}};
    uint64_t l_1331 = 0xD5F78A40BD45EC17LL;
    int i, j, k;
lbl_1334:
    (*g_652) = p_30;
    ++l_1331;
    if (l_1331)
        goto lbl_1334;
    return (***g_476);
}


/* ------------------------------------------ */
/* 
 * reads : g_338 g_1201 g_961 g_3 g_476 g_477 g_143 g_256 g_80 g_986 g_320 g_526 g_211 g_557 g_556 g_881 g_652 g_723.f0 g_655 g_1253 g_723.f2 g_210 g_111 g_200 g_201 g_300 g_990 g_991 g_1290 g_1202 g_1051 g_325 g_177
 * writes: g_338 g_819 g_650.f2 g_112 g_256 g_80 g_320 g_111 g_67 g_723.f0 g_177 g_723.f2 g_201 g_211 g_1289 g_325 g_1201
 */
static float  func_32(float  p_33, int8_t  p_34, int32_t * const  p_35)
{ /* block id: 526 */
    int64_t **l_1187 = (void*)0;
    int32_t l_1196 = 1L;
    union U0 l_1250[9][2] = {{{0xE40D718EEA2CD266LL},{0x75045B3FE2BD88F4LL}},{{0xC9D9381388BC7F55LL},{0x75045B3FE2BD88F4LL}},{{0xE40D718EEA2CD266LL},{0x75045B3FE2BD88F4LL}},{{0xC9D9381388BC7F55LL},{0x75045B3FE2BD88F4LL}},{{0xE40D718EEA2CD266LL},{0x75045B3FE2BD88F4LL}},{{0xC9D9381388BC7F55LL},{0x75045B3FE2BD88F4LL}},{{0xE40D718EEA2CD266LL},{0x75045B3FE2BD88F4LL}},{{0xC9D9381388BC7F55LL},{0x75045B3FE2BD88F4LL}},{{0xE40D718EEA2CD266LL},{0x75045B3FE2BD88F4LL}}};
    int32_t l_1259 = 1L;
    int32_t l_1265 = 0L;
    int32_t l_1270 = 1L;
    int32_t l_1272 = 0x8D27CCF2L;
    int i, j;
    for (g_338 = 0; (g_338 <= 2); g_338 += 1)
    { /* block id: 529 */
        int64_t **l_1188[7];
        int64_t ***l_1189 = &g_819[3][1][0];
        int32_t l_1190 = 0xD77D028BL;
        int32_t l_1191 = 0x8EF8CF4EL;
        int8_t *l_1209[3];
        uint8_t **l_1238[5][6] = {{&g_1202,&g_1202,&g_1202,&g_1202,&g_1202,&g_1202},{&g_1202,&g_1202,&g_1202,&g_1202,&g_1202,&g_1202},{&g_1202,&g_1202,&g_1202,&g_1202,&g_1202,&g_1202},{&g_1202,&g_1202,&g_1202,&g_1202,&g_1202,&g_1202},{&g_1202,&g_1202,&g_1202,&g_1202,&g_1202,&g_1202}};
        int32_t l_1269 = 0L;
        int i, j;
        for (i = 0; i < 7; i++)
            l_1188[i] = &g_211;
        for (i = 0; i < 3; i++)
            l_1209[i] = &g_147;
        l_1191 &= (l_1190 = (((*l_1189) = (l_1188[4] = l_1187)) == l_1187));
        for (g_650.f2 = 0; (g_650.f2 <= 2); g_650.f2 += 1)
        { /* block id: 536 */
            union U1 l_1194 = {0xE9DD3A36L};
            const union U0 l_1195 = {0x18C73CED2B37C8EELL};
            int32_t ****l_1200 = (void*)0;
            int32_t *****l_1199[5][9][5] = {{{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{(void*)0,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,(void*)0,&l_1200,&l_1200,&l_1200},{(void*)0,&l_1200,(void*)0,&l_1200,(void*)0},{(void*)0,&l_1200,&l_1200,&l_1200,(void*)0},{(void*)0,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,(void*)0}},{{&l_1200,&l_1200,&l_1200,&l_1200,(void*)0},{(void*)0,&l_1200,(void*)0,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,(void*)0},{(void*)0,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200}},{{&l_1200,(void*)0,&l_1200,&l_1200,(void*)0},{&l_1200,&l_1200,(void*)0,&l_1200,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,(void*)0,&l_1200,(void*)0,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200}},{{(void*)0,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,(void*)0},{&l_1200,&l_1200,(void*)0,(void*)0,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,&l_1200},{(void*)0,&l_1200,&l_1200,&l_1200,(void*)0},{&l_1200,&l_1200,(void*)0,&l_1200,&l_1200}},{{&l_1200,&l_1200,&l_1200,&l_1200,(void*)0},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,(void*)0,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,&l_1200,&l_1200},{&l_1200,&l_1200,&l_1200,(void*)0,(void*)0}}};
            const int16_t *l_1211 = &g_790;
            uint32_t l_1213 = 0UL;
            int32_t *l_1256[6];
            int8_t l_1273[5][6] = {{0L,0L,0xDEL,1L,(-5L),1L},{0L,0L,0L,1L,0xDEL,0xDEL},{0x5BL,0L,0L,0x5BL,0L,1L},{1L,0x5BL,0xDEL,0L,1L,0xDEL},{0L,1L,0xDEL,0xDEL,1L,0L}};
            int64_t *l_1280 = &g_94;
            uint8_t **l_1288 = &g_1202;
            int64_t l_1312 = 0x9B8BBD6A379B175ALL;
            int16_t ***l_1313 = &g_477;
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_1256[i] = &g_68;
            (*g_961) = (safe_mul_func_float_f_f((p_34 > (l_1194 , ((l_1195 , (l_1196 == (safe_mul_func_float_f_f((p_33 == l_1196), (&g_1083 != l_1199[3][6][0]))))) != (((void*)0 != g_1201) <= l_1191)))), 0x6.E60EF4p+17));
            if ((*p_35))
            { /* block id: 538 */
                int8_t **l_1210 = &l_1209[2];
                uint64_t *l_1212[3];
                int64_t **l_1214 = &g_211;
                int16_t *l_1222 = &g_80;
                int32_t l_1239 = 0xD335A943L;
                uint32_t *l_1240 = &g_723.f0;
                uint16_t *l_1249 = &l_1194.f2;
                int32_t l_1261 = 1L;
                int32_t l_1262 = 0L;
                int32_t l_1263[6];
                int i;
                for (i = 0; i < 3; i++)
                    l_1212[i] = &g_695[3][9].f2;
                for (i = 0; i < 6; i++)
                    l_1263[i] = 0L;
                l_1213 &= (p_34 || (((l_1190 = ((l_1190 || ((*g_986) &= (safe_add_func_int32_t_s_s(((safe_rshift_func_uint8_t_u_u(p_34, (((((8UL & p_34) != ((((*l_1210) = l_1209[2]) != (void*)0) | 0x1CF9F221F3A33275LL)) > ((***g_476) &= p_34)) , (void*)0) != l_1211))) | 0x81C6FFC3L), (-1L))))) || 255UL)) , g_526[0][0]) <= l_1196));
                (*l_1189) = l_1214;
                (*g_652) = ((((p_34 & 0UL) & ((safe_lshift_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((!(l_1190 = 0x0E2E1AF8L)), ((*l_1240) = ((l_1222 != ((((((safe_add_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s(((safe_sub_func_int32_t_s_s((1UL | ((safe_unary_minus_func_int64_t_s(((*g_211) = p_34))) || (safe_sub_func_int16_t_s_s(((safe_unary_minus_func_uint8_t_u(255UL)) < ((safe_div_func_float_f_f(((0xC.8EA0F2p-37 != (-((*g_881) = (safe_div_func_float_f_f(((void*)0 != (*g_557)), l_1191))))) , 0x5.593C84p+21), p_34)) , l_1191)), 0x6915L)))), p_34)) <= (*p_35)), 9)), l_1191)) | p_34) , (void*)0) == l_1238[2][4]) , (-3L)) , l_1211)) ^ l_1239)))), 14)) <= 0x5D27L)) ^ g_338) , &l_1196);
                if ((l_1190 = (safe_mul_func_uint8_t_u_u(((safe_div_func_uint32_t_u_u(l_1190, ((*l_1240)++))) , (((l_1191 = (safe_rshift_func_uint16_t_u_u((((*l_1249) |= l_1191) | (l_1250[0][0] , ((((safe_lshift_func_uint8_t_u_u(((((*l_1222) = (-1L)) , ((&l_1196 != ((*g_655) = &l_1196)) & (*g_143))) >= l_1250[0][0].f0), 6)) & l_1250[0][0].f0) == g_1253[3]) || l_1250[0][0].f0))), 7))) , 255UL) | l_1190)), 0x37L))))
                { /* block id: 556 */
                    int32_t *l_1255 = &g_325;
                    int32_t l_1260 = (-9L);
                    int32_t l_1264 = (-4L);
                    int32_t l_1266 = 0x9DB7081EL;
                    int32_t l_1267 = 0L;
                    int32_t l_1268 = 0x7EE1C8A1L;
                    int32_t l_1271[3][9] = {{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{0x2D4559A0L,0x2D4559A0L,0x2D4559A0L,0x2D4559A0L,0x2D4559A0L,0x2D4559A0L,0x2D4559A0L,0x2D4559A0L,0x2D4559A0L},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}};
                    int i, j;
                    for (g_723.f2 = 0; (g_723.f2 <= 2); g_723.f2 += 1)
                    { /* block id: 559 */
                        uint32_t l_1254 = 0x8C2CCA2FL;
                        l_1256[4] = ((*g_652) = ((l_1254 , (**g_210)) , l_1255));
                    }
                    (*g_881) = p_34;
                    if ((*p_35))
                    { /* block id: 564 */
                        volatile union U2 **l_1257 = (void*)0;
                        int32_t l_1258[8] = {(-7L),0x3BF2CB7FL,(-7L),(-7L),0x3BF2CB7FL,(-7L),(-7L),0x3BF2CB7FL};
                        uint64_t l_1274 = 18446744073709551608UL;
                        int i;
                        (*g_200) = (*g_200);
                        l_1274++;
                    }
                    else
                    { /* block id: 567 */
                        uint16_t *l_1279 = &g_723.f2;
                        uint8_t ***l_1287[1];
                        uint32_t l_1291 = 0xC5C6E3CDL;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1287[i] = &l_1238[2][4];
                        (*g_881) = ((((g_300[0][1][1] , ((((*l_1279) = (++(*l_1249))) , (*g_990)) != ((*l_1214) = ((p_34 && p_34) , l_1280)))) >= ((safe_add_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((((l_1288 = (void*)0) != (g_1289 = &g_1202)) < (((l_1261 == (g_1290 , l_1272)) >= p_34) & (*g_1202))) && l_1270), p_34)), 7L)), l_1291)) > p_34)) | l_1270) , 0xA.04B9A6p-23);
                        l_1255 = (void*)0;
                        return l_1265;
                    }
                    if (((0x9FD8FA74L & (p_34 <= (p_34 > (18446744073709551615UL | (~(l_1239 |= ((*l_1255) |= (safe_lshift_func_int8_t_s_s(l_1265, 4))))))))) <= g_320))
                    { /* block id: 579 */
                        (*g_881) = (p_33 = 0x0.Ap-1);
                    }
                    else
                    { /* block id: 582 */
                        uint64_t l_1295 = 0x497F08ADBEFE8466LL;
                        ++l_1295;
                        return l_1239;
                    }
                }
                else
                { /* block id: 586 */
                    uint8_t **l_1304 = &g_1202;
                    uint8_t ***l_1305[4][9] = {{&l_1288,&l_1238[2][4],&l_1288,&l_1238[2][4],&l_1288,&l_1238[2][4],&l_1288,&l_1238[2][4],&l_1288},{&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288},{&l_1288,&l_1238[2][4],&l_1288,&l_1238[2][4],&l_1288,&l_1238[2][4],&l_1288,&l_1238[2][4],&l_1288},{&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288,&l_1288}};
                    int i, j;
                    for (g_256 = 0; (g_256 <= 2); g_256 += 1)
                    { /* block id: 589 */
                        int8_t l_1298 = 0x5FL;
                        int32_t l_1299 = 1L;
                        int32_t l_1300[3][7][8] = {{{1L,1L,0x15641F36L,(-1L),0L,(-6L),0L,(-1L)},{0x613D3875L,(-1L),0x613D3875L,1L,0xAA45E2E2L,0L,0xE533C10FL,1L},{0x8A34B600L,0xA293A5C6L,(-10L),0x30DEE330L,0xBF9F1D51L,(-1L),0xAA45E2E2L,0x6346587BL},{0x8A34B600L,0x2EDBCF9AL,(-6L),(-6L),0xAA45E2E2L,0xE82593D0L,0x6346587BL,0x15641F36L},{0x613D3875L,0x8A34B600L,0x8CB3F99EL,0L,0L,0x8CB3F99EL,0x8A34B600L,0x613D3875L},{1L,(-5L),1L,0xE533C10FL,1L,0x20F698AFL,0x15641F36L,(-10L)},{0xA293A5C6L,0x89B2F110L,0x8A34B600L,0xAA45E2E2L,0xE533C10FL,0x20F698AFL,(-4L),(-6L)}},{{0x15641F36L,(-5L),0xBF9F1D51L,0x6346587BL,(-3L),0x8CB3F99EL,1L,0x8CB3F99EL},{(-6L),0x8A34B600L,(-3L),0x8A34B600L,(-6L),0xE82593D0L,0x2EDBCF9AL,1L},{(-4L),0x2EDBCF9AL,(-6L),0x15641F36L,0x7CBDDC6BL,(-1L),1L,0x8A34B600L},{0L,0xA293A5C6L,(-6L),(-4L),0x8A34B600L,0L,0x2EDBCF9AL,0xBF9F1D51L},{0x7CBDDC6BL,(-1L),(-3L),1L,(-6L),(-6L),1L,(-3L)},{1L,1L,0xBF9F1D51L,0x2EDBCF9AL,0L,0x8A34B600L,(-4L),(-6L)},{0xBF9F1D51L,0x20F698AFL,0x8A34B600L,1L,(-1L),0x7CBDDC6BL,0L,0xAA45E2E2L}},{{0xBF9F1D51L,0x30DEE330L,(-10L),0xA293A5C6L,0x8A34B600L,1L,0L,(-5L)},{0x8CB3F99EL,(-5L),0x89B2F110L,(-10L),0x89B2F110L,(-5L),0x8CB3F99EL,(-6L)},{(-10L),0x20F698AFL,1L,0x15641F36L,0xBF9F1D51L,(-6L),0x613D3875L,0L},{0x7CBDDC6BL,1L,0xE82593D0L,0L,0xBF9F1D51L,0xE533C10FL,(-6L),(-10L)},{(-10L),0x613D3875L,0x30DEE330L,0L,0x89B2F110L,0x2EDBCF9AL,0x2EDBCF9AL,0x89B2F110L},{0x8CB3F99EL,0L,0L,0x8CB3F99EL,0x8A34B600L,0x613D3875L,0xAA45E2E2L,1L},{0xBF9F1D51L,0x89B2F110L,0x8CB3F99EL,0x613D3875L,1L,(-6L),0x20F698AFL,0xE82593D0L}}};
                        uint32_t l_1301 = 0UL;
                        int i, j, k;
                        l_1301--;
                    }
                    if ((*g_177))
                        continue;
                    g_1201 = l_1304;
                }
            }
            else
            { /* block id: 595 */
                float l_1319 = 0x1.2p-1;
                (*g_881) = (safe_mul_func_float_f_f(((((safe_sub_func_float_f_f(p_34, 0x4.CF250Bp-20)) >= 0x6.9988A2p+93) == ((-0x10.5p+1) != (safe_mul_func_float_f_f(l_1312, 0x5.8D2BCBp+30)))) <= (l_1313 != (void*)0)), (-(safe_sub_func_float_f_f((((safe_mul_func_float_f_f(l_1196, p_34)) == 0x1.FADB29p+43) > l_1319), l_1269)))));
                if ((*p_35))
                    continue;
            }
            return p_33;
        }
    }
    return p_33;
}


/* ------------------------------------------ */
/* 
 * reads : g_695.f0 g_92 g_211 g_300 g_70.f0 g_477 g_143 g_3 g_652 g_650.f2 g_256 g_80 g_173.f2 g_815 g_820 g_325 g_210 g_476 g_880 g_816 g_723 g_717 g_177 g_901 g_943 g_111 g_881 g_961 g_990 g_986 g_320 g_240 g_443.f1 g_655 g_661 g_526 g_1034 g_1051 g_200 g_201 g_1057 g_491 g_695 g_462 g_53 g_94 g_1082 g_113 g_723.f0 g_50.f0
 * writes: g_240 g_110 g_111 g_256 g_80 g_320 g_790 g_177 g_443.f2 g_68 g_94 g_816 g_819 g_116 g_147 g_901 g_943 g_67 g_98 g_112 g_985 g_650.f0 g_661 g_723.f2 g_1051 g_201 g_723.f0 g_300 g_1082 g_1085 g_1101 g_650.f1 g_324
 */
static float  func_36(uint8_t  p_37, int32_t * p_38, int32_t * p_39, uint32_t  p_40, int16_t  p_41)
{ /* block id: 318 */
    uint64_t l_784[10];
    uint32_t l_786 = 0x86BE0B0DL;
    union U0 l_787[4] = {{0xB013DBC574229E7CLL},{0xB013DBC574229E7CLL},{0xB013DBC574229E7CLL},{0xB013DBC574229E7CLL}};
    int32_t l_792 = 0x3F86CBA6L;
    int8_t *l_850 = &g_240;
    int8_t **l_849 = &l_850;
    float **l_882 = (void*)0;
    int32_t l_913 = 0x6089FA30L;
    int32_t l_919 = 0x4A4AA099L;
    int32_t l_923 = 0x99EE778FL;
    int32_t l_924[2][3];
    int64_t l_933 = 0L;
    int32_t l_942 = 0x961330E6L;
    int32_t l_981 = 0x7DF98D71L;
    uint32_t l_995 = 0x8296E334L;
    int32_t ***l_1004 = &g_652;
    float l_1009[1][1][5];
    uint32_t l_1069 = 9UL;
    const union U0 *l_1154[3][9][3] = {{{&g_1155,&g_1155,&g_1155},{&g_1155,(void*)0,&g_1155},{&g_1155,&g_1155,(void*)0},{(void*)0,&g_1155,&g_1155},{&g_1155,(void*)0,&g_1155},{&g_1155,&g_1155,(void*)0},{&g_1155,&g_1155,&g_1155},{(void*)0,&g_1155,&g_1155},{&g_1155,&g_1155,(void*)0}},{{&g_1155,&g_1155,&g_1155},{(void*)0,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155},{(void*)0,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155}},{{&g_1155,(void*)0,&g_1155},{&g_1155,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155},{&g_1155,(void*)0,&g_1155},{(void*)0,&g_1155,(void*)0},{&g_1155,&g_1155,&g_1155},{(void*)0,&g_1155,&g_1155},{&g_1155,&g_1155,&g_1155},{&g_1155,(void*)0,&g_1155}}};
    const union U0 **l_1153 = &l_1154[1][5][0];
    int32_t *l_1178 = &g_68;
    int32_t *l_1179 = &l_924[1][0];
    int32_t *l_1180 = &l_919;
    int32_t *l_1181[8] = {&g_68,(void*)0,&g_68,&g_68,(void*)0,&g_68,&g_68,(void*)0};
    uint16_t l_1182 = 1UL;
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_784[i] = 1UL;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
            l_924[i][j] = 0xD6FFE242L;
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 5; k++)
                l_1009[i][j][k] = (-0x2.Cp-1);
        }
    }
    for (p_37 = 0; (p_37 == 6); p_37 = safe_add_func_uint8_t_u_u(p_37, 4))
    { /* block id: 321 */
        int16_t ** const *l_765 = &g_477;
        int16_t ** const **l_764 = &l_765;
        int32_t l_777[9] = {9L,9L,9L,9L,9L,9L,9L,9L,9L};
        int64_t l_857 = 1L;
        const float l_871 = 0x1.Fp+1;
        float l_886 = 0x7.965EFAp+23;
        union U2 **l_958 = &g_719;
        union U1 l_972 = {0xC9997671L};
        int8_t **l_1047 = &l_850;
        union U0 *****l_1056 = &g_753[6][6];
        int16_t ***l_1090 = &g_477;
        float * const *l_1166 = &g_881;
        int i;
        for (g_240 = (-5); (g_240 < (-7)); --g_240)
        { /* block id: 324 */
            uint8_t *l_776 = &g_110;
            int32_t l_785[10];
            int16_t *l_788 = (void*)0;
            int16_t *l_789 = &g_790;
            int32_t *l_791[6] = {&l_785[1],&l_785[1],&l_785[1],&l_785[1],&l_785[1],&l_785[1]};
            uint64_t *l_836 = &l_784[0];
            int32_t ***l_872 = &g_652;
            const union U2 *l_957[7] = {&g_491,&g_491,&g_491,&g_491,&g_491,&g_491,&g_491};
            const union U2 **l_956 = &l_957[1];
            const uint8_t l_971 = 0x0BL;
            int64_t l_983 = 0x2AC6BE2B8584D196LL;
            int64_t **l_992[1];
            union U0 ***** const l_1055 = &g_753[4][7];
            uint8_t l_1068 = 255UL;
            int32_t l_1103[6][7] = {{0x7592FE6CL,0x4C63AA90L,0x846D5705L,0x846D5705L,0x4C63AA90L,0x7592FE6CL,(-1L)},{0x4569F22EL,(-1L),7L,0x4569F22EL,0x4C63AA90L,0x899307D4L,(-1L)},{0xA45AF957L,0xA833340EL,0x7592FE6CL,0xACBFDAE4L,0x899307D4L,7L,0x7592FE6CL},{7L,0x820BAA9DL,0x33E36632L,0x899307D4L,0x7592FE6CL,7L,0x899307D4L},{0x846D5705L,0xA833340EL,0xACBFDAE4L,0x820BAA9DL,0x820BAA9DL,0xACBFDAE4L,0xA833340EL},{0x820BAA9DL,0xE3AF5FEAL,0x33E36632L,(-1L),0xE3AF5FEAL,0x899307D4L,0xA833340EL}};
            int16_t ***l_1105 = &g_477;
            union U0 l_1123 = {0x209655EB1A3D44D4LL};
            int64_t **l_1124 = &g_211;
            uint8_t l_1143 = 0x47L;
            union U0 **l_1159 = &g_750[1][5];
            uint64_t l_1168 = 8UL;
            int i, j;
            for (i = 0; i < 10; i++)
                l_785[i] = 7L;
            for (i = 0; i < 1; i++)
                l_992[i] = &g_211;
            (*g_652) = (((l_792 = (((*l_789) = (safe_sub_func_int16_t_s_s((safe_div_func_int16_t_s_s((safe_unary_minus_func_int32_t_s((l_764 == (((g_320 = (((safe_mul_func_uint8_t_u_u((((safe_lshift_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_u(1L, 7)) && ((*g_211) = (safe_mul_func_uint8_t_u_u(((*l_776) = g_695[3][9].f0), g_92)))), l_777[4])), (safe_div_func_int32_t_s_s(((safe_mul_func_int16_t_s_s(p_37, ((***l_765) = (g_300[2][0][4] | ((((safe_add_func_uint16_t_u_u(((((((&g_116 != ((p_41 <= l_784[0]) , &g_116)) & l_785[1]) >= l_785[1]) & 9UL) | p_40) > p_40), l_785[9])) || l_785[1]) , l_786) == g_70.f0))))) & 1L), p_41)))) < p_37) != l_784[0]), p_40)) , g_3) , 1L)) , l_787[1]) , (void*)0)))), 0x9710L)), p_40))) < l_785[7])) <= 0x0F7B81F4L) , p_38);
            for (g_443.f2 = 28; (g_443.f2 <= 47); ++g_443.f2)
            { /* block id: 334 */
                int64_t **l_817[9][9] = {{(void*)0,&g_211,&g_211,(void*)0,(void*)0,&g_211,&g_211,(void*)0,(void*)0},{&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211},{(void*)0,(void*)0,&g_211,&g_211,(void*)0,(void*)0,&g_211,&g_211,(void*)0},{(void*)0,&g_211,&g_211,&g_211,(void*)0,&g_211,&g_211,&g_211,(void*)0},{(void*)0,&g_211,&g_211,(void*)0,(void*)0,&g_211,&g_211,(void*)0,(void*)0},{&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211,&g_211},{(void*)0,(void*)0,&g_211,&g_211,(void*)0,(void*)0,&g_211,&g_211,(void*)0},{(void*)0,&g_211,&g_211,&g_211,(void*)0,&g_211,&g_211,&g_211,(void*)0},{(void*)0,&g_211,&g_211,(void*)0,(void*)0,&g_211,&g_211,(void*)0,(void*)0}};
                uint32_t l_830 = 0xC2B74EBDL;
                uint64_t *l_837 = (void*)0;
                uint16_t *l_842 = &g_300[4][0][0];
                uint32_t *l_855 = (void*)0;
                int8_t *l_873 = &g_147;
                int32_t l_885 = 0x1093E961L;
                uint16_t l_890 = 0xBB3EL;
                int32_t l_908 = 0x84337157L;
                int32_t l_915 = 0x282A304DL;
                int32_t l_916 = 0L;
                int32_t l_917 = (-8L);
                int32_t l_939 = (-9L);
                int32_t l_940[5];
                int32_t ***l_1006 = &g_652;
                int i, j;
                for (i = 0; i < 5; i++)
                    l_940[i] = (-1L);
                for (g_68 = 0; (g_68 > 10); g_68++)
                { /* block id: 337 */
                    int32_t ***l_806 = &g_652;
                    int32_t ****l_805 = &l_806;
                    union U0 **l_812 = &g_750[1][5];
                    int32_t l_821 = (-1L);
                    for (g_94 = 2; (g_94 != (-13)); g_94 = safe_sub_func_uint8_t_u_u(g_94, 3))
                    { /* block id: 340 */
                        l_792 = (safe_mod_func_int32_t_s_s((safe_mul_func_uint8_t_u_u(((safe_mod_func_uint8_t_u_u((l_805 == (void*)0), (p_41 & ((l_777[4] |= ((safe_mod_func_uint32_t_u_u(p_37, (safe_unary_minus_func_uint8_t_u((safe_mul_func_uint16_t_u_u((((1UL < g_650.f2) <= p_40) != ((void*)0 == l_812)), (**g_477))))))) < g_173[1].f2)) || 1UL)))) && p_41), 0x67L)), 0x6CFAAA36L));
                    }
                    for (g_94 = 20; (g_94 >= 5); g_94--)
                    { /* block id: 346 */
                        uint32_t l_822 = 0x290BB22BL;
                        (*g_815) = &g_723;
                        (*g_820) = l_817[1][8];
                        l_822--;
                    }
                    for (g_94 = 15; (g_94 != 7); g_94 = safe_sub_func_uint32_t_u_u(g_94, 8))
                    { /* block id: 353 */
                        uint32_t l_827 = 4294967295UL;
                        ++l_827;
                    }
                }
                if (l_830)
                    continue;
                for (g_116 = 0; (g_116 > 25); g_116 = safe_add_func_int32_t_s_s(g_116, 2))
                { /* block id: 360 */
                    uint64_t l_833[7][8][1] = {{{0x47FF71D3F0D72EB7LL},{0x3BBF2E91FB2D82BALL},{0x47FF71D3F0D72EB7LL},{0x3381357E075FBDD4LL},{0x9D410E4B0B7AE9E5LL},{1UL},{0xB72E2B69CDFD5A28LL},{18446744073709551613UL}},{{1UL},{18446744073709551613UL},{0xB72E2B69CDFD5A28LL},{1UL},{0x9D410E4B0B7AE9E5LL},{0x3381357E075FBDD4LL},{0x47FF71D3F0D72EB7LL},{0x3BBF2E91FB2D82BALL}},{{0x47FF71D3F0D72EB7LL},{0x3381357E075FBDD4LL},{0x9D410E4B0B7AE9E5LL},{1UL},{0xB72E2B69CDFD5A28LL},{18446744073709551613UL},{1UL},{18446744073709551613UL}},{{0xB72E2B69CDFD5A28LL},{1UL},{0x9D410E4B0B7AE9E5LL},{0x3381357E075FBDD4LL},{0x47FF71D3F0D72EB7LL},{0x3BBF2E91FB2D82BALL},{0x47FF71D3F0D72EB7LL},{0x3381357E075FBDD4LL}},{{0x9D410E4B0B7AE9E5LL},{1UL},{0xB72E2B69CDFD5A28LL},{18446744073709551613UL},{1UL},{18446744073709551613UL},{0xB72E2B69CDFD5A28LL},{1UL}},{{0x9D410E4B0B7AE9E5LL},{0x3381357E075FBDD4LL},{0x47FF71D3F0D72EB7LL},{0x3BBF2E91FB2D82BALL},{0x47FF71D3F0D72EB7LL},{0x3381357E075FBDD4LL},{0x9D410E4B0B7AE9E5LL},{1UL}},{{0xB72E2B69CDFD5A28LL},{18446744073709551613UL},{1UL},{18446744073709551613UL},{0xB72E2B69CDFD5A28LL},{1UL},{0x9D410E4B0B7AE9E5LL},{0x3381357E075FBDD4LL}}};
                    uint8_t l_843[10][2] = {{0x6BL,0UL},{0x6BL,255UL},{255UL,255UL},{255UL,0x6BL},{0UL,0x6BL},{255UL,255UL},{255UL,255UL},{0x6BL,0UL},{0x6BL,255UL},{255UL,255UL}};
                    int64_t *l_844 = (void*)0;
                    uint32_t *l_854[3];
                    uint32_t **l_856 = &l_854[2];
                    int32_t l_858[9];
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_854[i] = &g_650.f0;
                    for (i = 0; i < 9; i++)
                        l_858[i] = 0xFC67666AL;
                    if ((l_833[4][4][0] |= l_787[1].f0))
                    { /* block id: 362 */
                        return p_37;
                    }
                    else
                    { /* block id: 364 */
                        const int32_t l_838[9] = {3L,3L,3L,3L,3L,3L,3L,3L,3L};
                        int8_t *l_839 = &g_320;
                        int i;
                        l_792 &= (+(((*l_839) = (safe_unary_minus_func_uint8_t_u(((l_836 == l_837) >= l_838[6])))) >= ((safe_rshift_func_int16_t_s_u((*g_143), 3)) || (g_325 , (l_843[4][0] = (l_777[4] |= (l_842 == (void*)0)))))));
                    }
                    l_858[5] = (((l_844 = (void*)0) == (*g_210)) > ((safe_mul_func_uint8_t_u_u(p_40, (safe_add_func_uint16_t_u_u((l_849 != (void*)0), (!(p_41 == ((l_792 = (safe_rshift_func_int16_t_s_u(((*l_789) = ((***g_476) = p_41)), 15))) != (((*l_856) = l_855) != &g_116)))))))) == l_857));
                }
                if ((safe_div_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s((safe_add_func_int8_t_s_s(((*l_873) = (safe_add_func_int64_t_s_s(((((((safe_mod_func_uint8_t_u_u(((safe_add_func_uint64_t_u_u(p_37, 0UL)) || p_41), l_786)) & ((*g_143) < 2L)) > (p_37 >= ((0x93L >= (18446744073709551608UL | p_37)) != p_37))) , (void*)0) == l_872) , (-2L)), 0xCDD716C133813302LL))), 0x26L)), p_40)), p_37)))
                { /* block id: 378 */
                    uint16_t l_887 = 0xABF6L;
                    int16_t *l_888 = &g_256;
                    uint32_t *l_889[9][1][3] = {{{&g_116,&g_116,&g_116}},{{&g_116,(void*)0,&g_116}},{{&g_116,&g_116,&g_116}},{{&g_116,(void*)0,&g_116}},{{&g_116,&g_116,&g_116}},{{&g_116,(void*)0,&g_116}},{{&g_116,&g_116,&g_116}},{{&g_116,(void*)0,&g_116}},{{&g_116,&g_116,&g_116}}};
                    int32_t l_900 = 0x7CC7522BL;
                    int32_t l_906 = 7L;
                    int32_t l_910 = 1L;
                    int32_t l_912 = (-1L);
                    int32_t l_914 = 0x2BFE08D9L;
                    int32_t l_918[5] = {0x26BFD3A5L,0x26BFD3A5L,0x26BFD3A5L,0x26BFD3A5L,0x26BFD3A5L};
                    uint32_t l_920 = 18446744073709551612UL;
                    uint8_t l_925[3][1][6] = {{{0xA4L,255UL,0xA4L,0xA4L,255UL,0xA4L}},{{0xA4L,255UL,0xA4L,0xA4L,255UL,0xA4L}},{{0xA4L,255UL,0xA4L,0xA4L,255UL,0xA4L}}};
                    float *l_959 = &g_98[4][4];
                    int i, j, k;
                    if (l_787[1].f0)
                        break;
                    if ((safe_sub_func_int32_t_s_s((p_40 , (safe_div_func_uint32_t_u_u(((18446744073709551615UL < 0x5C390D9C31A15440LL) ^ ((safe_rshift_func_int8_t_s_s(((g_173[1].f2 > (g_880[1] != ((*g_816) , l_882))) , (((l_777[4] = (((safe_div_func_int32_t_s_s((((l_885 = ((*l_776) = (&l_817[1][8] != (void*)0))) >= 0L) != p_37), l_887)) , &p_41) != l_888)) | p_41) >= l_830)), l_830)) && p_40)), l_857))), l_857)))
                    { /* block id: 383 */
                        const uint32_t l_899 = 0x62252C11L;
                        --l_890;
                        p_39 = (*g_717);
                        g_901--;
                    }
                    else
                    { /* block id: 388 */
                        int32_t l_904 = 0x9FDE8915L;
                        int32_t l_905 = (-1L);
                        int32_t l_907 = 0x717848DBL;
                        int32_t l_909 = (-1L);
                        int32_t l_911[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_911[i] = (-5L);
                        ++l_920;
                        l_925[2][0][4]++;
                        if (l_917)
                            break;
                    }
                    for (g_80 = 0; (g_80 >= 1); ++g_80)
                    { /* block id: 395 */
                        uint64_t l_930 = 0x350FF5A990257EAELL;
                        int32_t l_934 = 9L;
                        int32_t l_935 = (-7L);
                        int32_t l_936 = 1L;
                        int32_t l_937 = (-8L);
                        int32_t l_938 = 0x00745413L;
                        int32_t l_941 = 0xCC565436L;
                        l_930++;
                        l_910 |= l_857;
                        ++g_943;
                    }
                    (*g_961) = (safe_sub_func_float_f_f((l_940[4] = p_40), (l_792 = (safe_mul_func_float_f_f(((safe_div_func_float_f_f(((l_924[1][2] ^= (**g_210)) , p_40), ((safe_div_func_float_f_f((l_777[4] = (safe_add_func_float_f_f(((*g_881) = (l_956 != l_958)), ((-0x1.Cp-1) <= ((*l_959) = ((p_37 == l_923) > 0x1.B25AA6p-82)))))), l_915)) >= 0x0.Ap+1))) > 0x7.2490EDp-97), p_41)))));
                }
                else
                { /* block id: 407 */
                    uint8_t l_982 = 246UL;
                    int32_t l_1018 = 2L;
                    int32_t l_1019 = 0x2769BF29L;
                    if ((safe_add_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(((p_40 == 0x21L) & (safe_rshift_func_uint16_t_u_u((~((&g_661 == ((safe_add_func_uint32_t_u_u(l_971, l_784[2])) , &g_661)) & (l_972 , (safe_lshift_func_uint16_t_u_u(((safe_div_func_uint32_t_u_u((safe_add_func_int32_t_s_s(((!(+(p_41 ^ (p_41 & l_981)))) , l_982), g_300[4][0][4])), l_972.f3)) <= 0x0254L), 2))))), p_41))), p_37)), l_857)))
                    { /* block id: 408 */
                        int8_t ***l_984[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_984[i] = &l_849;
                        if (l_983)
                            break;
                        l_940[4] |= ((g_985 = &l_873) != &g_986);
                        (**l_872) = p_38;
                    }
                    else
                    { /* block id: 413 */
                        uint32_t l_989[2][2][2] = {{{0x1D85FECCL,0x1D85FECCL},{0x1D85FECCL,0x1D85FECCL}},{{0x1D85FECCL,0x1D85FECCL},{0x1D85FECCL,0x1D85FECCL}}};
                        int64_t ***l_993 = &l_817[5][1];
                        int32_t l_996 = 0x02180A75L;
                        union U0 **** const *l_999[5][9] = {{(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5]},{(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5]},{(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5]},{(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5]},{(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5],(void*)0,&g_753[1][5],&g_753[1][5]}};
                        int32_t ****l_1005[6][7][2] = {{{&l_1004,(void*)0},{&l_1004,&l_872},{(void*)0,&l_872},{&l_1004,(void*)0},{&l_1004,&l_1004},{&l_1004,(void*)0},{&l_1004,&l_872}},{{(void*)0,&l_872},{&l_1004,(void*)0},{&l_1004,&l_1004},{&l_1004,(void*)0},{&l_1004,&l_872},{(void*)0,&l_872},{&l_1004,(void*)0}},{{&l_1004,&l_1004},{&l_1004,(void*)0},{&l_1004,&l_872},{(void*)0,&l_872},{&l_1004,(void*)0},{&l_1004,&l_1004},{&l_1004,(void*)0}},{{&l_1004,&l_872},{(void*)0,&l_872},{&l_1004,(void*)0},{&l_1004,&l_1004},{&l_1004,(void*)0},{&l_1004,&l_872},{(void*)0,&l_872}},{{&l_1004,(void*)0},{&l_1004,&l_1004},{&l_1004,(void*)0},{&l_1004,&l_872},{(void*)0,&l_872},{&l_1004,(void*)0},{&l_1004,&l_1004}},{{&l_1004,(void*)0},{&l_1004,&l_872},{(void*)0,&l_872},{&l_1004,(void*)0},{&l_1004,&l_1004},{&l_1004,(void*)0},{&l_1004,&l_872}}};
                        uint32_t *l_1017[5];
                        uint16_t l_1020 = 0UL;
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                            l_1017[i] = &l_989[0][1][1];
                        l_981 = (safe_rshift_func_int8_t_s_s(2L, 7));
                        l_996 &= (((l_989[1][0][0] < (g_990 == ((*l_993) = l_992[0]))) < l_830) ^ ((l_777[5] |= (~(0xAA907C97L >= l_989[1][0][0]))) ^ l_995));
                        l_924[1][1] |= ((((l_942 && ((safe_sub_func_int16_t_s_s((l_999[0][1] != (((~(safe_rshift_func_int8_t_s_u((~((l_872 != (l_1006 = l_1004)) & (l_857 ^ ((g_650.f0 = (((safe_mod_func_int16_t_s_s((l_982 == (((safe_add_func_uint32_t_u_u(p_37, (+(safe_sub_func_uint8_t_u_u(((((p_41 , (safe_mul_func_int8_t_s_s((*g_986), g_92))) <= 2L) & l_777[2]) && p_40), p_37))))) , 0xAFF31F98L) < l_787[1].f0)), g_320)) , g_240) & g_443.f1)) | g_240)))), 2))) && p_37) , &g_747[1])), (-8L))) >= g_300[0][0][0])) , (*g_211)) == 0xDDFBE6F93B5C9DE9LL) | 0xA99BL);
                        --l_1020;
                    }
                    (**l_872) = (*g_655);
                }
            }
            if ((p_41 > l_972.f0))
            { /* block id: 426 */
                uint64_t l_1030[10][7][1] = {{{1UL},{0xD52B90F3113069E0LL},{0x812DDD615D8B3AEFLL},{18446744073709551615UL},{0xBCC46FBCA6021DC8LL},{18446744073709551607UL},{18446744073709551615UL}},{{0x01BED04B29CF7A1ALL},{8UL},{18446744073709551613UL},{18446744073709551607UL},{18446744073709551613UL},{8UL},{0x01BED04B29CF7A1ALL}},{{18446744073709551615UL},{18446744073709551607UL},{0xBCC46FBCA6021DC8LL},{2UL},{0x64DDC56F66849FABLL},{18446744073709551609UL},{18446744073709551607UL}},{{1UL},{0xC7077078AFA585E4LL},{0xC7077078AFA585E4LL},{1UL},{18446744073709551607UL},{18446744073709551609UL},{0x64DDC56F66849FABLL}},{{2UL},{0xBCC46FBCA6021DC8LL},{18446744073709551607UL},{18446744073709551615UL},{0x01BED04B29CF7A1ALL},{8UL},{18446744073709551613UL}},{{18446744073709551607UL},{18446744073709551613UL},{8UL},{0x01BED04B29CF7A1ALL},{18446744073709551615UL},{18446744073709551607UL},{0xBCC46FBCA6021DC8LL}},{{2UL},{0x64DDC56F66849FABLL},{18446744073709551609UL},{18446744073709551607UL},{1UL},{0xC7077078AFA585E4LL},{0xC7077078AFA585E4LL}},{{1UL},{18446744073709551607UL},{18446744073709551609UL},{0x64DDC56F66849FABLL},{2UL},{0xBCC46FBCA6021DC8LL},{18446744073709551607UL}},{{18446744073709551615UL},{0x01BED04B29CF7A1ALL},{8UL},{18446744073709551613UL},{18446744073709551607UL},{18446744073709551613UL},{8UL}},{{0x01BED04B29CF7A1ALL},{18446744073709551615UL},{18446744073709551607UL},{0xBCC46FBCA6021DC8LL},{2UL},{0x64DDC56F66849FABLL},{18446744073709551609UL}}};
                int32_t l_1033 = 0x7E351E0AL;
                int i, j, k;
                for (g_661 = 0; (g_661 <= 1); g_661 += 1)
                { /* block id: 429 */
                    int32_t l_1023 = 0xD9BA6C08L;
                    uint32_t *l_1024 = &g_650.f0;
                    uint32_t *l_1025 = &l_972.f0;
                    uint16_t *l_1031 = (void*)0;
                    uint16_t *l_1032 = &g_723.f2;
                    int i, j;
                    l_1023 = (-1L);
                    l_1033 = (((*l_1032) = (((*l_1025)++) <= (((g_526[g_661][g_661] <= ((0x86CCAA6D3B9AEF46LL > 0x71D45E5F80A4E0F3LL) || 0x7141408CL)) || (((l_1030[5][4][0] && (p_37 , (&g_324[g_661][g_661] != (void*)0))) <= p_40) != g_173[1].f2)) < p_41))) <= 0xDB7AL);
                    if (g_1034)
                        continue;
                }
                if (l_924[0][1])
                    continue;
                (**l_872) = (*g_655);
            }
            else
            { /* block id: 438 */
                float l_1039 = 0x3.3p+1;
                int32_t l_1058 = 1L;
                uint32_t *l_1062[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                uint16_t *l_1065 = &g_300[2][0][4];
                union U2 *l_1100 = &g_720[6][0][2];
                int32_t l_1130 = (-3L);
                int32_t l_1132 = 9L;
                int32_t l_1133 = 0x51BFE8A5L;
                int32_t l_1136 = 0L;
                int32_t l_1137 = (-1L);
                int32_t l_1138 = (-1L);
                int32_t l_1139[9];
                uint32_t l_1140 = 18446744073709551615UL;
                int i;
                for (i = 0; i < 9; i++)
                    l_1139[i] = 0L;
                for (l_942 = 22; (l_942 <= 26); l_942 = safe_add_func_int16_t_s_s(l_942, 7))
                { /* block id: 441 */
                    int8_t ***l_1046 = &l_849;
                    int8_t ***l_1048 = &g_985;
                    int8_t ***l_1049 = &l_1047;
                    uint32_t *l_1050[7][6][3] = {{{(void*)0,&l_786,&g_116},{&l_786,&g_116,&g_723.f0},{&g_723.f0,&g_116,&g_116},{&g_116,&l_786,&g_723.f0},{&l_786,(void*)0,&g_116},{&l_786,&l_786,&g_723.f0}},{{(void*)0,&l_786,&g_116},{&l_786,&g_116,&g_723.f0},{&g_723.f0,&g_116,&g_116},{&g_116,&l_786,&g_723.f0},{&l_786,(void*)0,&g_116},{&l_786,&l_786,&g_723.f0}},{{(void*)0,&l_786,&g_116},{&l_786,&g_116,&g_723.f0},{&g_723.f0,&g_116,&g_116},{&g_116,&l_786,&g_723.f0},{&l_786,(void*)0,&g_116},{&l_786,&l_786,&g_723.f0}},{{(void*)0,&l_786,&g_116},{&l_786,&g_116,&g_723.f0},{&g_723.f0,&g_116,&g_116},{&g_116,&l_786,&g_723.f0},{&l_786,(void*)0,&g_116},{&l_786,&l_786,&g_723.f0}},{{(void*)0,&l_786,&g_116},{&l_786,&g_116,&g_723.f0},{&g_723.f0,&g_116,&g_116},{&g_116,&l_786,&g_723.f0},{&l_786,(void*)0,&g_116},{&l_786,&l_786,&g_723.f0}},{{(void*)0,&l_786,&g_116},{&l_786,&g_116,&g_723.f0},{&g_723.f0,&g_116,&g_116},{&g_116,&l_786,&g_723.f0},{&l_786,(void*)0,&g_116},{&l_786,&l_786,&g_723.f0}},{{(void*)0,&l_786,&g_116},{&l_786,&g_116,&g_723.f0},{&g_723.f0,&g_116,&g_116},{&g_116,&l_786,&g_723.f0},{&l_786,&l_786,&g_723.f0},{&g_723.f0,&g_723.f0,&l_786}}};
                    int32_t l_1054 = 0L;
                    int i, j, k;
                    for (g_80 = 7; (g_80 >= (-21)); --g_80)
                    { /* block id: 444 */
                        l_913 &= l_972.f3;
                    }
                    l_1054 = (l_857 != ((*l_776) = (g_300[2][0][4] < ((safe_div_func_int64_t_s_s((safe_mul_func_uint8_t_u_u(((((*l_1046) = (void*)0) != ((*l_1049) = l_1047)) & (--g_1051)), (p_40 | (l_1054 ^ (g_92 | (l_1055 != l_1056)))))), g_526[2][0])) & g_80))));
                    (*g_1057) = (*g_200);
                }
                l_1058 &= 0L;
                l_1069 = (safe_mod_func_int32_t_s_s((((p_41 == (g_723.f0 = 0UL)) | ((p_40 ^ (safe_rshift_func_uint16_t_u_s(((*l_1065) = 0x3532L), 7))) , ((((((l_777[4] |= l_1058) >= (((*l_776) = (((l_1068 = (safe_sub_func_int16_t_s_s((-1L), p_41))) <= p_37) > ((**l_956) , 0UL))) <= p_40)) >= p_37) , p_40) || p_40) , p_37))) | (*g_462)), g_94));
                if (l_857)
                { /* block id: 461 */
                    uint64_t l_1076 = 0x6F29AA8F92F8CE5DLL;
                    for (l_983 = 1; (l_983 <= 18); l_983 = safe_add_func_uint32_t_u_u(l_983, 5))
                    { /* block id: 464 */
                        int32_t l_1073 = 0xC5BD1B9AL;
                        int32_t l_1074 = 0xDA39E92DL;
                        int32_t l_1075 = (-10L);
                        l_1076--;
                    }
                }
                else
                { /* block id: 467 */
                    union U2 *l_1099 = (void*)0;
                    int32_t l_1109 = 0xAD93578BL;
                    uint8_t l_1111[7][5] = {{0x40L,250UL,0xDAL,1UL,250UL},{253UL,250UL,255UL,255UL,250UL},{0x40L,1UL,255UL,1UL,1UL},{0x40L,250UL,0xDAL,1UL,250UL},{253UL,250UL,255UL,255UL,250UL},{0x40L,1UL,255UL,1UL,1UL},{0x40L,250UL,0xDAL,1UL,250UL}};
                    int32_t l_1128 = 0L;
                    int32_t l_1131 = 0x028D83AEL;
                    int32_t l_1135 = (-7L);
                    int i, j;
                    for (g_110 = (-19); (g_110 >= 52); g_110++)
                    { /* block id: 470 */
                        int16_t ****l_1091 = &l_1090;
                        float **l_1102[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int32_t l_1104 = (-7L);
                        int32_t l_1106 = 0x723EB5FEL;
                        int i;
                        g_1085 = (g_1082 = ((~4294967289UL) , g_1082));
                        l_1106 = (safe_add_func_float_f_f(((((*g_881) = (p_37 < (safe_sub_func_float_f_f((((*l_1091) = l_1090) == ((0xC2A0FB16L == (((*l_836) = (~(((p_37 , (((l_1104 = (((safe_sub_func_int8_t_s_s((l_972.f0 > p_40), g_113[5])) > (safe_lshift_func_uint16_t_u_s((((g_1101 = (((safe_sub_func_uint32_t_u_u(0x7A3D8A65L, (l_1099 == l_1100))) != p_40) , (void*)0)) == l_1102[2]) || 0x74L), l_1103[4][3]))) ^ p_37)) != 0x0D7EF865L) || g_723.f0)) <= l_1058) , g_320))) == g_300[4][0][4])) , l_1105)), l_1058)))) != l_972.f2) != p_40), 0x6.8380FCp-74));
                    }
                    l_777[4] &= (((*g_986) <= (safe_sub_func_int16_t_s_s(l_1109, (((void*)0 != (**l_872)) != (+((l_1111[5][2] ^ ((g_650.f1 = 1UL) > (l_1058 = (safe_div_func_uint16_t_u_u(((*l_1065) ^= (safe_rshift_func_int8_t_s_u((p_41 > (safe_mod_func_uint32_t_u_u((((*l_836) = ((!(((+(l_972.f0 && (safe_lshift_func_int16_t_s_s((+((l_1123 , l_1124) != (void*)0)), 1)))) , (void*)0) != &g_816)) , 1UL)) | 0xB134B773FBC95975LL), l_1111[5][2]))), 1))), p_40))))) && p_40)))))) , l_1058);
                    for (l_1109 = 17; (l_1109 > (-16)); l_1109 = safe_sub_func_int16_t_s_s(l_1109, 9))
                    { /* block id: 487 */
                        float l_1127[8] = {0xF.0AB8A2p-9,0xF.0AB8A2p-9,(-0x1.5p+1),0xF.0AB8A2p-9,0xF.0AB8A2p-9,(-0x1.5p+1),0xF.0AB8A2p-9,0xF.0AB8A2p-9};
                        int32_t l_1129[4][4][8] = {{{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL}},{{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL}},{{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL}},{{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL},{0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL,0x52D58428L,0xB1C9B54CL}}};
                        int32_t l_1134 = 0x268FB484L;
                        int i, j, k;
                        ++l_1140;
                        if (l_1137)
                            break;
                        l_1143 ^= 0xEB47DF69L;
                    }
                    for (l_972.f1 = 0; l_972.f1 < 2; l_972.f1 += 1)
                    {
                        for (l_995 = 0; l_995 < 8; l_995 += 1)
                        {
                            g_324[l_972.f1][l_995] = &l_924[1][1];
                        }
                    }
                }
            }
            for (g_320 = 0; (g_320 == 26); g_320++)
            { /* block id: 497 */
                const union U0 **l_1156 = &l_1154[0][3][2];
                int32_t l_1158 = 0x5BD3072BL;
                if (l_972.f2)
                { /* block id: 498 */
                    return p_41;
                }
                else
                { /* block id: 500 */
                    int8_t l_1175 = 0x2EL;
                    int32_t l_1176 = (-9L);
                    for (l_981 = 0; (l_981 < (-28)); --l_981)
                    { /* block id: 503 */
                        uint16_t l_1157 = 0x0CEFL;
                        uint8_t **l_1160 = &l_776;
                        float **l_1165 = &g_881;
                        int32_t l_1167 = 0x6A1F73E9L;
                        l_1167 ^= (safe_rshift_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s((+((((l_1156 = (l_1153 = (void*)0)) != ((l_1158 ^= l_1157) , l_1159)) , &g_1051) == ((*l_1160) = &p_37))), ((p_37 & p_40) <= (safe_add_func_uint32_t_u_u((((((p_37 | ((p_37 , l_1165) == l_1166)) ^ 0UL) | 0x280AA60BL) < p_41) != 8L), l_777[4]))))) && 0x804D1B768D3503D3LL), g_526[0][1]));
                        --l_1168;
                        return p_37;
                    }
                    if ((((g_50.f0 || (&g_338 == &g_526[0][0])) , (l_1176 = (safe_mod_func_int64_t_s_s((((*l_776) = ((0xE6F61B3DL && (*g_462)) | p_40)) < (((((6UL > 0xEB746C83L) <= (safe_add_func_int64_t_s_s(0xBE12662D19325669LL, p_37))) > 0x81D26B01L) ^ l_1175) > (*g_986))), 0xF526507A81576164LL)))) && 18446744073709551610UL))
                    { /* block id: 514 */
                        return p_40;
                    }
                    else
                    { /* block id: 516 */
                        int32_t l_1177 = (-5L);
                        l_1177 &= l_1176;
                        return p_37;
                    }
                }
            }
        }
    }
    l_1182--;
    return p_37;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_752
 */
static int32_t * func_43(int32_t * p_44, int32_t * p_45, int32_t * p_46, uint64_t  p_47)
{ /* block id: 314 */
    union U0 *l_746 = &g_70;
    union U0 **l_745 = &l_746;
    union U0 ***l_744 = &l_745;
    union U0 ****l_751 = &l_744;
    int32_t *l_754 = (void*)0;
    (*l_751) = l_744;
    g_752 = &l_751;
    return l_754;
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_68 g_173.f0 g_143 g_80 g_113 g_210 g_211 g_111 g_300 g_240 g_256 g_320 g_149 g_92 g_338 g_116 g_110 g_355 g_356 g_177 g_70.f0 g_53 g_443 g_475 g_476 g_173.f2 g_477 g_70 g_325 g_526 g_475.f0 g_557 g_173 g_2 g_462 g_173.f3 g_650 g_650.f2 g_655 g_147 g_661 g_650.f0 g_153.f0 g_723.f2 g_94 g_717 g_723.f0 g_652
 * writes: g_67 g_300 g_110 g_149 g_338 g_116 g_68 g_147 g_112 g_143 g_111 g_80 g_460 g_476 g_98 g_526 g_556 g_256 g_177 g_325 g_652 g_661 g_650.f2 g_723.f0
 */
static int32_t * func_48(uint8_t  p_49)
{ /* block id: 5 */
    uint32_t l_54 = 3UL;
    int32_t *l_72 = (void*)0;
    int32_t l_213 = (-1L);
    int32_t l_215 = 0x3FD60A17L;
    int32_t l_261 = 0L;
    int32_t l_262 = 0xF9C94B45L;
    int32_t l_272 = 0xB93A7BC0L;
    int32_t l_273[7] = {6L,6L,6L,6L,6L,6L,6L};
    int64_t l_274 = (-9L);
    uint16_t l_275 = 0x3FAAL;
    uint16_t l_301 = 0x4872L;
    int8_t l_377 = 0xECL;
    uint32_t *l_442 = &l_54;
    uint32_t **l_441 = &l_442;
    uint16_t l_457 = 5UL;
    uint8_t *l_471 = &g_110;
    int16_t * const l_474[1][6] = {{&g_80,&g_80,&g_80,&g_80,&g_80,&g_80}};
    union U1 l_486[4] = {{6UL},{6UL},{6UL},{6UL}};
    union U2 *l_490 = &g_491;
    union U2 **l_489 = &l_490;
    int64_t l_498[3][2][1] = {{{(-1L)},{0x4F22A72F8DAB3FDELL}},{{(-1L)},{0x4F22A72F8DAB3FDELL}},{{(-1L)},{0x4F22A72F8DAB3FDELL}}};
    float l_542 = (-0x3.Cp+1);
    float *l_550[2];
    const float *l_551 = &l_542;
    const float **l_552 = &l_551;
    const uint64_t *l_563 = &g_70.f0;
    uint64_t l_574 = 8UL;
    union U0 l_656 = {18446744073709551606UL};
    int32_t l_673 = 0xE8B96711L;
    int64_t l_715 = 0x961DAF3005D9EDD9LL;
    union U1 *l_722 = &g_723;
    int16_t *l_736[5][2][5] = {{{&g_80,&g_256,&g_256,&g_256,&g_80},{&g_80,&g_256,&g_256,&g_256,&g_80}},{{&g_80,&g_256,&g_256,&g_256,&g_80},{&g_80,&g_256,&g_256,&g_256,&g_80}},{{&g_80,&g_256,&g_256,&g_256,&g_80},{&g_80,&g_256,&g_256,&g_256,&g_80}},{{&g_80,&g_256,&g_256,&g_256,&g_80},{&g_80,&g_256,&g_256,&g_256,&g_80}},{{&g_80,&g_256,&g_256,&g_256,&g_80},{&g_80,&g_256,&g_256,&g_256,&g_80}}};
    int16_t *l_738 = (void*)0;
    int16_t **l_737 = &l_738;
    int8_t *l_739 = &g_147;
    int32_t *l_741 = &l_272;
    int32_t **l_740[9][10][2] = {{{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0}},{{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741},{(void*)0,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0},{&l_741,&l_741},{&l_741,&l_741}},{{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741},{&l_741,&l_741}},{{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0},{&l_741,&l_741}},{{&l_741,&l_741},{(void*)0,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0},{&l_741,(void*)0},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741}},{{&l_741,&l_741},{&l_741,(void*)0},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741}},{{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741},{&l_741,&l_741}},{{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,(void*)0},{&l_741,&l_741}},{{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{(void*)0,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741},{&l_741,&l_741}}};
    int32_t *l_742 = &l_262;
    int32_t *l_743 = &g_68;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_550[i] = &l_542;
lbl_577:
    if (((l_54 , (safe_add_func_int64_t_s_s(g_3, l_54))) , g_3))
    { /* block id: 6 */
        int32_t *l_65 = &g_3;
        int32_t **l_64 = &l_65;
        int32_t l_216 = (-1L);
        int32_t l_217 = 0L;
        uint32_t l_267 = 1UL;
        int32_t *l_270 = &l_262;
        int32_t *l_271[3][10][2] = {{{&l_213,&l_262},{&l_262,(void*)0},{&l_215,&l_215},{&l_213,&l_215},{&l_215,(void*)0},{&l_215,&l_215},{&l_213,&l_215},{&l_215,(void*)0},{&l_262,&l_262},{&l_213,&l_262}},{{&l_262,(void*)0},{&l_215,&l_215},{&l_213,&l_215},{&l_215,(void*)0},{&l_215,&l_215},{&l_213,&l_215},{&l_215,(void*)0},{&l_262,&l_262},{&l_213,&l_262},{&l_262,(void*)0}},{{&l_215,&l_215},{&l_213,&l_215},{&l_215,(void*)0},{&l_215,&l_215},{&l_213,&l_215},{&l_215,(void*)0},{&l_262,&l_262},{&l_213,&l_262},{&l_262,(void*)0},{&l_215,&l_215}}};
        union U0 **l_292 = (void*)0;
        int i, j, k;
        for (p_49 = 0; (p_49 < 6); p_49++)
        { /* block id: 9 */
            int32_t *l_63 = &g_3;
            int32_t **l_62[3][10][2] = {{{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63}},{{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63}},{{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63},{&l_63,&l_63}}};
            float *l_101 = &g_67[4][3];
            float *l_154 = &g_98[3][6];
            int64_t * const l_168[7][8] = {{&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94},{&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94},{&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94},{&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94},{&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94},{&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94},{&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94,&g_94}};
            int32_t l_241 = 1L;
            int i, j, k;
            for (l_54 = 5; (l_54 == 49); l_54 = safe_add_func_int64_t_s_s(l_54, 1))
            { /* block id: 12 */
                union U0 l_61[4] = {{0x2C2674D9070A6219LL},{0x2C2674D9070A6219LL},{0x2C2674D9070A6219LL},{0x2C2674D9070A6219LL}};
                float *l_66 = &g_67[5][6];
                int i;
                (*l_66) = ((l_61[0] , (void*)0) != ((l_62[1][0][1] != &l_63) , l_64));
            }
            for (l_54 = 0; (l_54 <= 2); l_54 += 1)
            { /* block id: 17 */
                int32_t l_114 = (-4L);
                union U1 l_137 = {0x53D42119L};
                float *l_176[6][10][1] = {{{&g_67[5][6]},{&g_112[1]},{&g_98[0][3]},{(void*)0},{&g_67[5][6]},{&g_67[5][6]},{&g_67[5][6]},{&g_98[0][3]},{(void*)0},{(void*)0}},{{(void*)0},{&g_98[0][3]},{&g_67[5][6]},{&g_67[5][6]},{&g_67[5][6]},{(void*)0},{&g_98[0][3]},{&g_112[1]},{&g_67[5][6]},{&g_112[1]}},{{&g_98[0][3]},{(void*)0},{&g_67[5][6]},{&g_67[5][6]},{&g_67[5][6]},{&g_98[0][3]},{(void*)0},{(void*)0},{(void*)0},{&g_98[0][3]}},{{&g_67[5][6]},{&g_67[5][6]},{&g_67[5][6]},{(void*)0},{&g_98[0][3]},{&g_112[1]},{&g_67[5][6]},{&g_112[1]},{&g_98[0][3]},{(void*)0}},{{&g_67[5][6]},{&g_67[5][6]},{&g_67[5][6]},{&g_98[0][3]},{(void*)0},{(void*)0},{(void*)0},{&g_98[0][3]},{&g_67[5][6]},{&g_67[5][6]}},{{&g_67[5][6]},{(void*)0},{&g_98[0][3]},{&g_112[1]},{&g_67[5][6]},{&g_112[1]},{&g_98[0][3]},{(void*)0},{&g_67[5][6]},{&g_67[5][6]}}};
                int32_t *l_178 = &g_68;
                int64_t * const *l_212 = &g_211;
                int32_t l_266[6];
                int i, j, k;
                for (i = 0; i < 6; i++)
                    l_266[i] = 0xA09C7167L;
            }
        }
        ++l_275;
        for (l_217 = (-2); (l_217 < (-13)); l_217 = safe_sub_func_uint32_t_u_u(l_217, 3))
        { /* block id: 104 */
            uint32_t l_280 = 0x580BC1E8L;
            union U0 *l_291 = (void*)0;
            union U0 **l_290 = &l_291;
            int16_t **l_293[7] = {(void*)0,&g_143,(void*)0,(void*)0,&g_143,(void*)0,(void*)0};
            int16_t **l_294 = &g_143;
            int16_t **l_296 = &g_143;
            int16_t ***l_295 = &l_296;
            int16_t **l_298 = &g_143;
            int16_t ***l_297 = &l_298;
            uint16_t *l_299 = &l_275;
            int i;
            for (l_272 = 5; (l_272 >= 0); l_272 -= 1)
            { /* block id: 107 */
                uint32_t l_283 = 0xFBB93CB3L;
                l_280++;
                l_283++;
            }
            g_300[2][0][4] |= (g_68 , (safe_rshift_func_uint8_t_u_s(((((*l_299) = (((safe_mul_func_int16_t_s_s(((g_173[1].f0 | ((l_290 != l_292) > 0x4CE26FF6L)) || (0x6927871A6DCD7AABLL & (l_293[2] != ((*l_297) = ((*l_295) = (l_294 = l_293[2])))))), ((*g_143) != p_49))) | g_113[3]) <= (*g_143))) && (*g_143)) <= (**g_210)), g_68)));
        }
        l_301--;
    }
    else
    { /* block id: 118 */
        int32_t l_313 = 0x339C9CAAL;
        int8_t *l_321 = (void*)0;
        int8_t *l_322 = &g_149;
        int16_t ***l_323 = (void*)0;
        int32_t l_326 = 0x541714DEL;
        int8_t l_342 = 0xF5L;
        union U2 *l_359 = (void*)0;
        int32_t l_392[2][6][6] = {{{0x90920A86L,(-1L),0x4E3B226DL,0x90920A86L,0x2761FFCBL,7L},{5L,0x90920A86L,0xA67218FBL,0x90920A86L,5L,0x18AE3B0FL},{0x90920A86L,5L,0x18AE3B0FL,0L,5L,0L},{(-1L),0x90920A86L,0x9729E21BL,5L,0x2761FFCBL,0L},{0x2761FFCBL,(-1L),0x18AE3B0FL,0L,0L,0x18AE3B0FL},{0x2761FFCBL,0x2761FFCBL,0xA67218FBL,5L,0xF02128ABL,7L}},{{(-1L),0x2761FFCBL,0x4E3B226DL,0L,0L,0xA67218FBL},{0x90920A86L,(-1L),0x4E3B226DL,0x90920A86L,0x2761FFCBL,7L},{5L,0x90920A86L,0xA67218FBL,0x90920A86L,5L,0x18AE3B0FL},{0x90920A86L,5L,0x18AE3B0FL,0L,5L,0L},{(-1L),0x90920A86L,0x9729E21BL,5L,0x2761FFCBL,0L},{0x2761FFCBL,(-1L),0x18AE3B0FL,0L,0L,0x18AE3B0FL}}};
        uint16_t l_402 = 1UL;
        int16_t *l_440 = &g_256;
        int64_t *l_519[3];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_519[i] = &g_94;
        l_326 ^= ((((safe_sub_func_int32_t_s_s((safe_lshift_func_uint8_t_u_s((((safe_rshift_func_int8_t_s_u(((*l_322) &= (!(((safe_add_func_int64_t_s_s(l_313, 0x9437D5B3B05B4049LL)) > ((g_110 = g_240) >= l_54)) >= ((((g_240 | ((safe_mul_func_int8_t_s_s((((((((((safe_lshift_func_uint8_t_u_u(((void*)0 != &g_70), 6)) | ((void*)0 == &l_275)) & p_49) != g_256) != g_68) > p_49) ^ 0xC2FB002AL) > g_320) & 0xA6F0L), p_49)) < 0xDA34E3CBL)) , (void*)0) == (void*)0) ^ l_313)))), p_49)) , l_323) != l_323), g_92)), 0x2917DDB6L)) != 18446744073709551615UL) > 0L) || p_49);
        for (l_301 = 0; (l_301 <= 51); l_301 = safe_add_func_int16_t_s_s(l_301, 5))
        { /* block id: 124 */
            uint64_t *l_337 = &g_338;
            union U1 l_341 = {0xA27B1B8DL};
            int32_t l_352 = (-1L);
            uint32_t *l_353[3];
            uint8_t *l_354[3];
            union U2 *l_358[3][1][1];
            int32_t l_379 = 0L;
            int32_t l_383 = 0xCF33A7FFL;
            int32_t l_387 = (-1L);
            int32_t l_395 = 9L;
            int32_t l_397 = 0xFB11BB9BL;
            int32_t l_398 = 0xA02A9C73L;
            int32_t l_400 = 0xCA6F1A25L;
            float *l_487 = &g_98[3][2];
            float *l_488 = &g_112[1];
            uint16_t l_534 = 65532UL;
            uint8_t l_543 = 255UL;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_353[i] = &g_116;
            for (i = 0; i < 3; i++)
                l_354[i] = &g_110;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_358[i][j][k] = &g_355[2][0][0];
                }
            }
            if ((((((safe_rshift_func_uint8_t_u_s(0UL, (((((safe_div_func_int8_t_s_s(0x72L, (((safe_mod_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(((*l_337) |= (l_326 &= 1UL)), ((safe_add_func_int32_t_s_s(p_49, ((l_341 , l_342) != ((g_110 &= ((((g_116 &= (((safe_sub_func_int32_t_s_s((((((~((safe_rshift_func_uint16_t_u_u((l_352 = (safe_rshift_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(6UL, ((g_240 > 0xB84FCDD09516DBD0LL) | p_49))), l_342))), 8)) < (-8L))) != l_274) < l_313) | p_49) > l_313), 0x460A1502L)) & 0x01L) | p_49)) > (-9L)) ^ l_313) < 1UL)) && p_49)))) && 1UL))), g_113[7])) , g_355[2][0][0]) , p_49))) == (-8L)) < 0x732AL) , g_356) != (void*)0))) & p_49) == p_49) | p_49) && 9UL))
            { /* block id: 130 */
                int32_t *l_357 = (void*)0;
                return l_357;
            }
            else
            { /* block id: 132 */
                int32_t l_376 = 0x3D2CF8F0L;
                int32_t l_382 = 1L;
                int32_t l_385 = 0x374226B9L;
                int32_t l_389 = 0x755289B1L;
                int32_t l_390 = 0L;
                int32_t l_394 = 0x6979698AL;
                int32_t l_396 = 0x6640F26BL;
                int32_t l_399 = 2L;
                int32_t l_401[1];
                uint32_t **l_444 = &l_353[1];
                union U0 l_472 = {1UL};
                int16_t ****l_478 = (void*)0;
                int16_t ****l_479 = &g_476;
                int i;
                for (i = 0; i < 1; i++)
                    l_401[i] = (-2L);
                if ((p_49 | l_341.f3))
                { /* block id: 133 */
                    uint64_t l_369 = 1UL;
                    int32_t l_378 = 0L;
                    int32_t l_380 = 0x9BD2A156L;
                    int16_t l_381 = 0x5D69L;
                    int32_t l_384 = 0x351D8BECL;
                    int32_t l_386 = 0xF4E722B1L;
                    int32_t l_388 = 0x2E359C76L;
                    int32_t l_391 = 0x854ECEBCL;
                    int32_t l_393[6] = {(-3L),(-3L),0xAABB8F62L,(-3L),(-3L),0xAABB8F62L};
                    uint16_t *l_422 = &g_300[2][1][1];
                    int32_t **l_426 = &l_72;
                    int i;
                    (*g_177) |= (l_358[1][0][0] == l_359);
                    for (l_262 = 0; (l_262 > (-23)); l_262--)
                    { /* block id: 137 */
                        int32_t *l_362 = &l_272;
                        int32_t *l_363 = &l_326;
                        int32_t *l_364 = &l_215;
                        int32_t *l_365 = &l_273[3];
                        int32_t *l_366 = (void*)0;
                        int32_t *l_367 = &l_326;
                        int32_t *l_368[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_368[i] = &l_215;
                        --l_369;
                    }
                    for (g_147 = (-21); (g_147 > 13); g_147 = safe_add_func_uint64_t_u_u(g_147, 6))
                    { /* block id: 142 */
                        int32_t *l_374 = &l_326;
                        int32_t *l_375[5];
                        const int32_t *l_406 = &l_326;
                        const int32_t **l_405 = &l_406;
                        float *l_423 = (void*)0;
                        float *l_424 = (void*)0;
                        float *l_425[6] = {(void*)0,(void*)0,(void*)0,&g_98[0][3],&g_98[0][3],(void*)0};
                        int i;
                        for (i = 0; i < 5; i++)
                            l_375[i] = (void*)0;
                        ++l_402;
                        (*l_405) = &g_53;
                        if (l_341.f0)
                            continue;
                        (*g_177) = (!((g_173[1].f0 > g_70.f0) & (((*l_374) ^= (0xB253L != ((*l_422) = (((p_49 != (safe_sub_func_int8_t_s_s(0xC3L, (((l_392[1][1][0] = (safe_div_func_float_f_f(((-0x9.Dp+1) < 0x8.412345p-73), (safe_mul_func_float_f_f((g_112[4] = (((safe_mod_func_int8_t_s_s(((safe_div_func_int64_t_s_s((safe_mul_func_int8_t_s_s(1L, (((safe_mod_func_uint8_t_u_u((l_422 != &g_300[2][0][4]), p_49)) || 1L) | l_402))), 2L)) ^ p_49), 0x82L)) , 0x9.FE3ABDp-20) > g_113[4])), p_49))))) , l_352) > (**l_405))))) && p_49) || g_111)))) < l_393[4])));
                    }
                    (*l_426) = (void*)0;
                }
                else
                { /* block id: 153 */
                    int16_t *l_437 = &g_80;
                    int32_t l_450 = (-1L);
                    const int32_t ****l_458 = (void*)0;
                    const int32_t ****l_459 = (void*)0;
                    for (g_149 = 0; (g_149 < 28); g_149++)
                    { /* block id: 156 */
                        int16_t **l_438 = (void*)0;
                        int16_t **l_439 = &g_143;
                        uint32_t **l_445 = &l_353[1];
                        (*g_177) = (safe_sub_func_uint64_t_u_u((safe_mod_func_int64_t_s_s((((safe_add_func_int64_t_s_s(p_49, ((*g_211) = (((*l_439) = l_437) != l_440)))) < (l_441 == (l_445 = (g_443 , l_444)))) ^ ((safe_rshift_func_uint8_t_u_s(g_113[7], g_80)) , 0xF4A5L)), p_49)), p_49));
                    }
                    l_376 |= (((l_353[1] == (void*)0) > l_389) & ((g_460 = (((safe_div_func_int16_t_s_s(((*l_437) |= (l_450 , ((safe_lshift_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s(l_382, l_394)), (l_399 || 0x14A00012ADA95FE3LL))) > (safe_mod_func_uint32_t_u_u(((**l_444) = p_49), l_457))))), l_450)) || 0xCC85L) , (void*)0)) != &g_461[1][0][4]));
                }
                for (l_274 = (-25); (l_274 != (-19)); ++l_274)
                { /* block id: 169 */
                    int32_t *l_465 = &l_396;
                    int32_t **l_466 = &l_72;
                    (*l_466) = l_465;
                }
                (*g_177) ^= ((((safe_add_func_float_f_f((((((3UL || (safe_mul_func_uint8_t_u_u((l_471 == (void*)0), ((((l_472 , (+(((p_49 != p_49) , (&g_256 != l_474[0][2])) , 65533UL))) ^ g_113[7]) , g_475[0]) , 2L)))) == 0x366AFFA9L) , 0x4.A58457p-0) <= (-0x4.8p-1)) >= 0xB.083667p+46), p_49)) , l_399) | (*g_143)) & p_49);
                l_398 &= (((*l_479) = g_476) == &g_477);
            }
            if ((safe_div_func_int64_t_s_s((safe_add_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(0x107EL, 1)), (((0x2.6551D7p+30 == l_383) >= ((l_486[1] , ((((*l_488) = ((*l_487) = p_49)) != 0xA.18C79Dp+80) != g_173[1].f2)) <= (l_489 == ((((safe_div_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(255UL, p_49)), g_338)) & (***g_476)) && 1UL) , &l_359)))) , p_49))), l_397)))
            { /* block id: 178 */
                int32_t *l_524 = &l_272;
                int32_t *l_525[7][4] = {{&l_398,&l_383,&l_383,&l_398},{&l_272,&l_383,&l_326,&l_383},{&l_383,&l_383,&l_326,&l_326},{&l_272,&l_272,&l_383,&l_326},{&l_398,&l_383,&l_398,&l_383},{&l_398,&l_383,&l_383,&l_398},{&l_272,&l_383,&l_326,&l_383}};
                int i, j;
                for (l_326 = 0; (l_326 == 7); l_326 = safe_add_func_int32_t_s_s(l_326, 5))
                { /* block id: 181 */
                    uint16_t *l_510[5];
                    int32_t l_511 = 0x80428AD7L;
                    int32_t l_512 = 0L;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_510[i] = &l_457;
                    (*g_177) = ((l_498[0][1][0] > ((g_70 , (safe_rshift_func_int8_t_s_s((-6L), 4))) || ((g_325 != (0L | ((((((~(((safe_mod_func_uint8_t_u_u((((((((5L & (safe_mod_func_uint16_t_u_u((l_511 = (safe_div_func_uint32_t_u_u((1UL || (-1L)), (safe_sub_func_int32_t_s_s(((0xE501L && p_49) < 0xCB41L), 0x7AC861C3L))))), l_392[0][2][1]))) ^ g_53) , (*g_143)) > 0xF21AL) , (**g_477)) , 0x624369D6595C8F11LL) & l_383), p_49)) & p_49) | g_80)) , l_342) | 0x4A13L) > l_512) > p_49) , g_173[1].f2))) && (-6L)))) & p_49);
                }
                (*g_177) ^= (safe_rshift_func_uint8_t_u_s((((0xB.EB611Fp+72 == (safe_sub_func_float_f_f(0x0.2p-1, ((safe_sub_func_float_f_f((p_49 > (((*l_487) = ((void*)0 != l_519[0])) <= l_387)), p_49)) >= (l_342 > (safe_mul_func_float_f_f(((((safe_sub_func_float_f_f((((g_113[7] , g_475[0]) , (void*)0) == (void*)0), 0x4.E4E3C2p-91)) >= (-0x1.0p-1)) < g_113[4]) >= p_49), 0x0.0E2839p-20))))))) , 0x3F85L) > 1UL), 4));
                --g_526[0][1];
            }
            else
            { /* block id: 188 */
                int64_t l_533 = 0x956ED5D91861E68FLL;
                int32_t *l_539 = &l_272;
                int32_t *l_540 = &l_213;
                int32_t *l_541[3][4][9] = {{{&l_383,(void*)0,&l_262,&l_400,&l_398,&l_398,&l_400,&l_262,(void*)0},{&l_261,(void*)0,&g_3,&l_398,(void*)0,&l_326,&l_398,&l_387,(void*)0},{&l_400,(void*)0,&l_379,&l_272,&l_398,&l_383,&l_272,&l_262,&l_392[1][0][1]},{&l_398,(void*)0,&l_387,&g_3,(void*)0,(void*)0,&g_3,&l_387,(void*)0}},{{&l_272,(void*)0,&l_352,&l_383,&l_398,(void*)0,&l_383,&l_262,&l_272},{&g_3,(void*)0,&l_395,&l_261,(void*)0,&l_392[0][2][1],&l_261,&l_387,&g_325},{&l_383,(void*)0,&l_262,&l_400,&l_398,&l_398,&l_400,&l_262,(void*)0},{&l_261,(void*)0,&g_3,&l_398,(void*)0,&l_326,&l_398,&l_387,(void*)0}},{{&l_400,(void*)0,&l_379,&l_272,&l_398,&l_383,&l_272,&l_262,&l_392[1][0][1]},{&l_398,(void*)0,&l_387,&g_3,(void*)0,(void*)0,&g_3,&l_387,(void*)0},{&l_272,(void*)0,&l_352,&l_383,&l_398,(void*)0,&l_383,&l_262,&l_272},{&g_3,(void*)0,&l_395,&l_261,(void*)0,&l_392[0][2][1],&l_261,&l_387,&g_325}}};
                int i, j, k;
                for (l_341.f2 = 0; (l_341.f2 < 21); l_341.f2++)
                { /* block id: 191 */
                    int32_t l_531[10][6] = {{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x36B65BF1L,(-1L),0x36B65BF1L,0x36B65BF1L},{(-1L),0x36B65BF1L,0x65CEFFC2L,0x36B65BF1L,0x65CEFFC2L,0x65CEFFC2L},{0x36B65BF1L,0x65CEFFC2L,0x65CEFFC2L,0x36B65BF1L,0x65CEFFC2L,0x65CEFFC2L}};
                    int32_t *l_532[5][1][4] = {{{&l_273[3],&l_273[3],&l_273[3],&l_262}},{{&l_273[3],&l_262,&l_262,&l_273[3]}},{{&l_326,&l_262,&l_272,&l_262}},{{&l_262,&l_273[3],&l_272,&l_272}},{{&l_326,&l_326,&l_262,&l_272}}};
                    int i, j, k;
                    l_534++;
                    for (l_342 = 6; (l_342 >= 0); l_342 -= 1)
                    { /* block id: 195 */
                        int16_t l_537 = 0x5B11L;
                        int32_t *l_538 = &l_213;
                        if (l_537)
                            break;
                        return &g_3;
                    }
                }
                l_543++;
            }
            for (l_262 = 0; (l_262 != (-9)); l_262 = safe_sub_func_uint16_t_u_u(l_262, 1))
            { /* block id: 204 */
                if (l_392[0][2][1])
                    break;
                l_72 = &l_326;
                (*l_72) = 0L;
            }
        }
    }
    if ((((*l_471) = g_475[0].f0) >= (l_550[0] != ((*l_552) = l_551))))
    { /* block id: 213 */
        int32_t *l_553 = &l_213;
        int16_t ****l_554 = &g_476;
        union U0 * const l_570 = &g_70;
        int32_t **l_641[10][4] = {{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177},{&l_72,&g_177,&l_72,&g_177}};
        int32_t ***l_640[5][6] = {{&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3]},{&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3]},{&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3]},{&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3]},{&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3],&l_641[1][3]}};
        int32_t l_714[4] = {4L,4L,4L,4L};
        int i, j;
        l_72 = l_553;
        (*g_557) = l_554;
        for (g_256 = 0; (g_256 <= 1); g_256 += 1)
        { /* block id: 218 */
            int32_t *l_558 = &l_273[0];
            return &g_3;
        }
        if (((safe_rshift_func_uint16_t_u_u(((((p_49 , (*l_72)) >= (safe_div_func_float_f_f((l_563 == (void*)0), ((safe_add_func_float_f_f((((g_173[1] , (safe_mul_func_float_f_f((safe_add_func_float_f_f((g_112[2] = ((void*)0 == l_570)), (g_67[5][6] = (+(safe_sub_func_float_f_f((l_574 = p_49), p_49)))))), (*l_72)))) == p_49) > p_49), g_70.f0)) <= (-0x1.Bp+1))))) >= p_49) , 65535UL), 7)) >= (-1L)))
        { /* block id: 224 */
            for (l_274 = 9; (l_274 > (-27)); l_274 = safe_sub_func_uint64_t_u_u(l_274, 1))
            { /* block id: 227 */
                if (l_54)
                    goto lbl_577;
            }
            for (g_338 = 17; (g_338 >= 35); g_338++)
            { /* block id: 232 */
                if ((*l_72))
                    break;
            }
        }
        else
        { /* block id: 235 */
            uint32_t l_580 = 0x9D361CC9L;
            int32_t l_595 = 0L;
            int32_t l_643[10][10] = {{0x78D78CF0L,0xC446676CL,0x39D2C6BEL,1L,(-10L),1L,0x39D2C6BEL,0xC446676CL,0x78D78CF0L,0L},{0x286445DFL,1L,0xF6645A1AL,0xB7D5F09CL,(-10L),5L,1L,0xA2A2D49AL,5L,1L},{0xB7D5F09CL,0x39D2C6BEL,(-5L),0xB7D5F09CL,0x0DC3AE14L,6L,0xC446676CL,1L,0x78D78CF0L,(-10L)},{0x78D78CF0L,0xA2A2D49AL,(-5L),1L,1L,0x78D78CF0L,0x39D2C6BEL,0xA2A2D49AL,6L,0L},{0x7F11FB76L,0xA2A2D49AL,0xF6645A1AL,0x286445DFL,1L,5L,0xC446676CL,0xC446676CL,5L,1L},{0x7F11FB76L,0x39D2C6BEL,0x39D2C6BEL,0x7F11FB76L,0x0DC3AE14L,0x78D78CF0L,1L,1L,1L,1L},{0x78D78CF0L,1L,1L,1L,1L,6L,0x39D2C6BEL,1L,1L,0L},{0xB7D5F09CL,0xC446676CL,0xF6645A1AL,0x7F11FB76L,1L,5L,0xA2A2D49AL,1L,5L,(-10L)},{0x286445DFL,0x39D2C6BEL,1L,0x286445DFL,0x0DC3AE14L,1L,0xA2A2D49AL,5L,0x2B4C8294L,0x70938C81L},{0x7443E0CEL,0x78D78CF0L,5L,9L,0x3C7BF6A8L,9L,5L,0x78D78CF0L,0x7443E0CEL,0xB18C377AL}};
            uint32_t l_645[9];
            int32_t **l_651 = &g_177;
            float **l_684 = &l_550[0];
            union U2 *l_718[4];
            int i, j;
            for (i = 0; i < 9; i++)
                l_645[i] = 0xCFE06B0EL;
            for (i = 0; i < 4; i++)
                l_718[i] = (void*)0;
            ++l_580;
            if ((*g_177))
            { /* block id: 237 */
                int8_t *l_593 = (void*)0;
                int8_t *l_594[3][2];
                uint64_t l_609 = 0xA420812CA0F87947LL;
                int32_t l_644[2][9] = {{0x72B5A0BAL,0x72B5A0BAL,0x72B5A0BAL,0x72B5A0BAL,0x72B5A0BAL,0x72B5A0BAL,0x72B5A0BAL,0x72B5A0BAL,0x72B5A0BAL},{1L,1L,1L,1L,1L,1L,1L,1L,1L}};
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_594[i][j] = &g_149;
                }
                if (((l_595 = ((g_2[1][2] ^ (((safe_sub_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(((void*)0 != &g_70), ((safe_rshift_func_uint16_t_u_u((safe_div_func_int64_t_s_s(((((l_580 , g_110) & ((g_240 == (((((*l_72) , (((*l_72) = (((safe_sub_func_uint32_t_u_u(((**l_441) = ((((((void*)0 != l_553) & p_49) | p_49) <= (*l_72)) || (*l_72))), g_111)) > 0xEB44B0E1L) | p_49)) , 0xCE087EEEBDD9C765LL)) && 0x2F998F98C51E94A2LL) | 6L) , (-1L))) | 1L)) != 0x2B097369L) , (*g_211)), 18446744073709551615UL)), 4)) != (*g_462)))), g_149)) , 0xD5BDL) <= p_49)) ^ 0x28D4L)) == g_113[7]))
                { /* block id: 241 */
                    int32_t **l_596 = &g_177;
                    (*l_596) = &l_273[3];
                }
                else
                { /* block id: 243 */
                    uint8_t l_605 = 254UL;
                    int8_t **l_608 = &l_593;
                    (*g_177) ^= (safe_rshift_func_int8_t_s_u((safe_lshift_func_uint16_t_u_s(((l_553 == (void*)0) ^ (((**g_477) &= 8L) < ((((safe_rshift_func_uint8_t_u_s(255UL, (l_605 = g_53))) && p_49) , (safe_div_func_uint16_t_u_u((((*l_608) = &g_320) != &g_240), (-1L)))) == 0x89L))), l_609)), p_49));
                    return &g_325;
                }
                if (((safe_lshift_func_uint8_t_u_s(l_609, (((p_49 , (safe_mod_func_uint64_t_u_u((((*l_471)++) || ((safe_div_func_int8_t_s_s(((safe_mod_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((-8L), (*l_72))), (safe_sub_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u((safe_add_func_int64_t_s_s((((((safe_rshift_func_uint8_t_u_u((safe_mod_func_int16_t_s_s(((void*)0 != l_474[0][2]), 0x8970L)), 6)) || (l_609 <= ((safe_sub_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(((*l_72) & g_240), p_49)), (*l_553))) <= 0x00L))) , &g_461[2][1][4]) == l_640[1][5]) < 0x7A93L), 18446744073709551611UL)), p_49)) ^ 0xB152L), p_49)), g_256)))) , p_49), 0xEFL)) || p_49)), 1UL))) >= 0UL) > l_609))) , p_49))
                { /* block id: 251 */
                    int32_t l_642[8][3][8] = {{{0x24968D74L,(-2L),0x627B9C38L,0L,0x7D790D50L,(-1L),(-2L),0x604E4BC1L},{0xD680AB06L,0x7D790D50L,0xA48F8546L,(-1L),0x9F2352A4L,0L,0xD1B55382L,9L},{0xDE2EDCB7L,0x6A897D87L,(-6L),0x24968D74L,0x7E08E54CL,0x7E08E54CL,0x24968D74L,(-6L)}},{{0L,0L,(-1L),(-7L),0x24968D74L,0x0FA42E86L,0xDE2EDCB7L,(-1L)},{0xD680AB06L,(-1L),0L,0L,(-2L),0xA48F8546L,0x6A897D87L,(-1L)},{(-1L),0xD1B55382L,0x37AD1B5AL,(-7L),1L,0L,(-2L),(-6L)}},{{1L,0x78255209L,6L,0x24968D74L,(-1L),6L,0x7D790D50L,9L},{0x7C95539EL,0x24968D74L,0xCEDFAB11L,(-1L),0xDE2EDCB7L,0x7C95539EL,0x6A897D87L,0x604E4BC1L},{0L,4L,(-1L),0L,(-1L),4L,0L,0L}},{{8L,(-1L),(-1L),9L,0x7D790D50L,6L,(-1L),0x24968D74L},{0L,0x7D790D50L,0x37AD1B5AL,0xCEDFAB11L,0x7D790D50L,(-1L),0xD1B55382L,0L},{8L,0xD1B55382L,0xD680AB06L,0x24968D74L,(-1L),0L,0x78255209L,0xD680AB06L}},{{0L,0x9F2352A4L,0x627B9C38L,(-1L),0xDE2EDCB7L,0x0FA42E86L,0x24968D74L,(-7L)},{0x7C95539EL,0xDB908DC2L,0L,0xCEDFAB11L,(-1L),4L,4L,(-1L)},{1L,0x6A897D87L,0x6A897D87L,1L,1L,0xD680AB06L,(-1L),0L}},{{(-1L),0x78255209L,0x627B9C38L,0x604E4BC1L,(-2L),(-1L),0x7D790D50L,0L},{0xD680AB06L,0x78255209L,0L,(-1L),0x24968D74L,0xD680AB06L,0xD1B55382L,8L},{0L,0x6A897D87L,0xCEDFAB11L,0x9F2352A4L,0x7E08E54CL,4L,0x9F2352A4L,(-6L)}},{{0xDE2EDCB7L,0xDB908DC2L,(-1L),0L,0x9F2352A4L,0x0FA42E86L,0xDB908DC2L,0x24968D74L},{0xD680AB06L,0x9F2352A4L,0x6A897D87L,0L,0x7D790D50L,0L,0x6A897D87L,0xD680AB06L},{0L,0x0FA42E86L,0xA7868D26L,1L,0x29578432L,4L,0x7E08E54CL,0xA7868D26L}},{{(-6L),0x7C95539EL,0L,0xA48F8546L,0L,0L,0x7E08E54CL,0x37AD1B5AL},{1L,0xA48F8546L,0xA7868D26L,4L,0x6A897D87L,(-1L),6L,0xD1B55382L},{0x6A897D87L,(-1L),6L,0xD1B55382L,4L,1L,0x6A897D87L,0xB958F9A3L}}};
                    int i, j, k;
                    l_645[1]--;
                    for (g_325 = 4; (g_325 >= 0); g_325 -= 1)
                    { /* block id: 255 */
                        int i;
                        (*l_553) = (g_113[(g_325 + 2)] | 0x94ADL);
                        if (l_643[5][5])
                            continue;
                    }
                }
                else
                { /* block id: 259 */
                    const uint32_t l_670 = 0x782D809EL;
                    if ((safe_div_func_uint8_t_u_u(g_173[1].f3, (g_650 , g_650.f2))))
                    { /* block id: 260 */
                        g_652 = l_651;
                    }
                    else
                    { /* block id: 262 */
                        int32_t * const l_653 = (void*)0;
                        int32_t **l_654[9] = {&g_177,&l_553,&g_177,&l_553,&g_177,&l_553,&g_177,&l_553,&g_177};
                        int i;
                        (*g_655) = l_653;
                    }
                    (*l_553) |= (l_656 , (254UL & ((safe_rshift_func_int8_t_s_u(g_147, (g_661 ^= ((*l_471)++)))) | (l_609 <= ((safe_mul_func_int8_t_s_s((((((++p_49) == (safe_mul_func_uint16_t_u_u((g_68 ^ ((g_173[1].f3 | (((safe_div_func_int8_t_s_s(l_670, (safe_mod_func_uint16_t_u_u(g_650.f0, (**g_477))))) ^ 0x5F3CL) < l_673)) , (-1L))), 0xB9E8L))) <= 0xF9L) <= 0xC6C5CB6BL) || l_670), g_300[2][0][4])) != (-10L))))));
                }
            }
            else
            { /* block id: 270 */
                uint32_t l_691 = 3UL;
                int32_t * const l_716 = (void*)0;
                (**l_651) ^= ((safe_div_func_int32_t_s_s(((p_49 | ((safe_mul_func_int16_t_s_s((0xB80CDB0FL == p_49), ((*l_72) , (safe_div_func_uint16_t_u_u(p_49, (safe_sub_func_int32_t_s_s(((((p_49 <= (safe_mod_func_uint16_t_u_u((l_684 == (void*)0), (g_300[2][0][4] | (*l_72))))) != p_49) || p_49) && g_111), 4294967291UL))))))) & g_300[4][1][0])) < p_49), p_49)) != 0x02L);
                for (g_256 = 0; (g_256 == 18); g_256 = safe_add_func_int16_t_s_s(g_256, 8))
                { /* block id: 274 */
                    float *l_687 = &g_67[5][0];
                    int32_t l_690 = 0x3F3CD90CL;
                }
            }
        }
    }
    else
    { /* block id: 299 */
        uint16_t *l_728[3][4] = {{&l_486[1].f2,&l_486[1].f2,&l_301,&l_486[1].f2},{&l_486[1].f2,&l_457,&l_457,&l_486[1].f2},{&l_457,&l_486[1].f2,&l_457,&l_457}};
        union U0 l_730[4] = {{0x507D2200904C0CE8LL},{0x507D2200904C0CE8LL},{0x507D2200904C0CE8LL},{0x507D2200904C0CE8LL}};
        int32_t *l_733[3][10][7] = {{{(void*)0,&l_215,&l_273[3],&l_261,&l_273[6],&l_273[6],&l_273[0]},{&l_272,&l_272,&l_272,&l_215,&l_273[6],&l_273[3],&g_68},{&l_215,(void*)0,&g_68,&l_215,&l_273[6],&l_215,&g_68},{&l_273[0],&l_273[0],&g_68,&l_273[3],&l_273[6],&l_215,&l_272},{(void*)0,&l_215,&l_273[0],&l_273[6],&l_273[6],&l_261,&l_273[3]},{&l_272,&l_272,&l_273[3],(void*)0,&l_273[6],&l_215,&l_273[3]},{&l_215,(void*)0,&l_273[3],&l_215,&l_273[6],(void*)0,&l_273[3]},{(void*)0,&l_215,&l_273[3],&l_261,&l_273[6],&l_273[6],&l_273[0]},{&l_272,&l_272,&l_272,&l_215,&l_273[6],&l_273[3],&g_68},{&l_215,(void*)0,&g_68,&l_215,&l_273[6],&l_215,&g_68}},{{&l_273[0],&l_273[0],&g_68,&l_273[3],&l_273[6],&l_215,&l_272},{(void*)0,&l_215,&l_273[0],&l_273[6],&l_273[6],&l_261,&l_273[3]},{&l_272,&l_272,&l_273[3],(void*)0,&l_273[6],&l_215,&l_273[3]},{&l_215,(void*)0,&l_273[3],&l_215,&l_273[6],(void*)0,&l_273[3]},{(void*)0,&l_215,&l_273[3],&l_261,&l_273[6],&l_273[6],&l_273[0]},{&l_272,&l_272,&l_272,&l_215,&l_273[6],&l_273[3],&g_68},{&l_215,(void*)0,&g_68,&l_215,&l_273[6],&l_215,&g_68},{&l_273[0],&l_273[0],&g_68,&l_273[3],&l_273[6],&l_215,&l_272},{(void*)0,&l_215,&l_273[0],&l_273[6],&l_273[6],&l_261,&l_273[3]},{&l_273[0],&l_273[6],&l_215,&l_273[5],&l_213,&g_68,&l_215}},{{&l_215,&l_213,&l_215,&g_68,&l_213,&l_273[5],&l_215},{&l_213,&l_215,&l_273[0],&g_68,&l_213,&g_325,&l_272},{&l_273[6],&l_273[0],(void*)0,&g_325,&l_213,&g_3,&l_272},{&l_215,&l_215,(void*)0,&l_273[5],&l_213,&l_273[5],(void*)0},{&g_68,&g_68,&l_272,&g_3,&l_213,&g_325,(void*)0},{&l_215,&l_215,&l_272,&g_325,&l_213,&g_68,&l_273[0]},{&l_273[0],&l_273[6],&l_215,&l_273[5],&l_213,&g_68,&l_215},{&l_215,&l_213,&l_215,&g_68,&l_213,&l_273[5],&l_215},{&l_213,&l_215,&l_273[0],&g_68,&l_213,&g_325,&l_272},{&l_273[6],&l_273[0],(void*)0,&g_325,&l_213,&g_3,&l_272}}};
        int i, j, k;
        g_98[3][4] = (safe_sub_func_float_f_f(((p_49 | (safe_mod_func_uint8_t_u_u(((g_650.f2 ^= 0xBB3DL) , ((*g_143) , (~(((l_730[3] , ((g_153.f0 != ((((((((g_723.f2 , 0xDEL) | ((*l_471) = (0L != (safe_mod_func_uint64_t_u_u((g_650 , p_49), (**g_210)))))) && l_730[3].f0) , l_301) | (*g_211)) , g_300[3][1][3]) >= p_49) & 0x06F22D1FA6C342AELL)) , (**g_210))) | g_300[2][0][4]) | (-7L))))), g_94))) , p_49), 0x7.89C1E0p+15));
        return (*g_717);
    }
    l_742 = ((((*l_739) = (safe_rshift_func_uint16_t_u_u((((*l_72) = (*l_72)) < (g_723.f0 ^= (((**g_476) = l_736[0][1][0]) != ((*l_737) = l_736[4][1][0])))), g_300[0][1][3]))) < g_173[1].f0) , (l_72 = ((*g_652) = (*g_717))));
    return l_743;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_2[i][j], "g_2[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_50.f0, "g_50.f0", print_hash_value);
    transparent_crc(g_53, "g_53", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc_bytes(&g_67[i][j], sizeof(g_67[i][j]), "g_67[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_68, "g_68", print_hash_value);
    transparent_crc(g_70.f0, "g_70.f0", print_hash_value);
    transparent_crc(g_80, "g_80", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    transparent_crc(g_94, "g_94", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc_bytes(&g_98[i][j], sizeof(g_98[i][j]), "g_98[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_112[i], sizeof(g_112[i]), "g_112[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_113[i], "g_113[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_147, "g_147", print_hash_value);
    transparent_crc(g_149, "g_149", print_hash_value);
    transparent_crc(g_153.f0, "g_153.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_173[i].f0, "g_173[i].f0", print_hash_value);
        transparent_crc(g_173[i].f2, "g_173[i].f2", print_hash_value);
        transparent_crc(g_173[i].f3, "g_173[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_240, "g_240", print_hash_value);
    transparent_crc(g_256, "g_256", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_300[i][j][k], "g_300[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_320, "g_320", print_hash_value);
    transparent_crc(g_325, "g_325", print_hash_value);
    transparent_crc(g_338, "g_338", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_355[i][j][k].f0, "g_355[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_443.f0, "g_443.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_475[i].f0, "g_475[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_491.f0, "g_491.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_526[i][j], "g_526[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_650.f0, "g_650.f0", print_hash_value);
    transparent_crc(g_650.f2, "g_650.f2", print_hash_value);
    transparent_crc(g_650.f3, "g_650.f3", print_hash_value);
    transparent_crc(g_661, "g_661", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_695[i][j].f0, "g_695[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_720[i][j][k].f0, "g_720[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_723.f0, "g_723.f0", print_hash_value);
    transparent_crc(g_723.f2, "g_723.f2", print_hash_value);
    transparent_crc(g_723.f3, "g_723.f3", print_hash_value);
    transparent_crc(g_790, "g_790", print_hash_value);
    transparent_crc(g_901, "g_901", print_hash_value);
    transparent_crc(g_943, "g_943", print_hash_value);
    transparent_crc(g_1034, "g_1034", print_hash_value);
    transparent_crc(g_1051, "g_1051", print_hash_value);
    transparent_crc_bytes (&g_1061, sizeof(g_1061), "g_1061", print_hash_value);
    transparent_crc(g_1072, "g_1072", print_hash_value);
    transparent_crc(g_1155.f0, "g_1155.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1253[i], "g_1253[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1290.f0, "g_1290.f0", print_hash_value);
    transparent_crc(g_1323.f0, "g_1323.f0", print_hash_value);
    transparent_crc_bytes (&g_1466, sizeof(g_1466), "g_1466", print_hash_value);
    transparent_crc_bytes (&g_1516, sizeof(g_1516), "g_1516", print_hash_value);
    transparent_crc(g_1555, "g_1555", print_hash_value);
    transparent_crc(g_1622.f0, "g_1622.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 494
XXX total union variables: 31

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 261
   depth: 2, occurrence: 62
   depth: 3, occurrence: 7
   depth: 4, occurrence: 3
   depth: 5, occurrence: 3
   depth: 6, occurrence: 1
   depth: 9, occurrence: 3
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 3
   depth: 17, occurrence: 3
   depth: 18, occurrence: 4
   depth: 19, occurrence: 5
   depth: 20, occurrence: 4
   depth: 21, occurrence: 4
   depth: 22, occurrence: 1
   depth: 23, occurrence: 4
   depth: 25, occurrence: 3
   depth: 26, occurrence: 1
   depth: 28, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 3
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 41, occurrence: 2

XXX total number of pointers: 383

XXX times a variable address is taken: 1011
XXX times a pointer is dereferenced on RHS: 155
breakdown:
   depth: 1, occurrence: 114
   depth: 2, occurrence: 36
   depth: 3, occurrence: 5
XXX times a pointer is dereferenced on LHS: 172
breakdown:
   depth: 1, occurrence: 159
   depth: 2, occurrence: 9
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 45
XXX times a pointer is compared with address of another variable: 8
XXX times a pointer is compared with another pointer: 11
XXX times a pointer is qualified to be dereferenced: 7709

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 707
   level: 2, occurrence: 190
   level: 3, occurrence: 81
   level: 4, occurrence: 76
   level: 5, occurrence: 51
XXX number of pointers point to pointers: 155
XXX number of pointers point to scalars: 207
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.4
XXX average alias set size: 1.47

XXX times a non-volatile is read: 1184
XXX times a non-volatile is write: 602
XXX times a volatile is read: 68
XXX    times read thru a pointer: 2
XXX times a volatile is write: 20
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 5.77e+03
XXX percentage of non-volatile access: 95.3

XXX forward jumps: 0
XXX backward jumps: 4

XXX stmts: 256
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 26
   depth: 2, occurrence: 35
   depth: 3, occurrence: 48
   depth: 4, occurrence: 53
   depth: 5, occurrence: 59

XXX percentage a fresh-made variable is used: 16.5
XXX percentage an existing variable is used: 83.5
********************* end of statistics **********************/


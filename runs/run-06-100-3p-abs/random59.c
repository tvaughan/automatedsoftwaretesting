/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3250344733
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   volatile unsigned f0 : 13;
   const unsigned f1 : 16;
   unsigned f2 : 19;
   volatile signed f3 : 18;
   volatile signed f4 : 27;
   unsigned f5 : 8;
   volatile unsigned f6 : 7;
   const signed f7 : 30;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int16_t g_11 = 0L;
static volatile struct S0 g_55 = {76,171,303,-87,5351,15,7,-1979};/* VOLATILE GLOBAL g_55 */
static struct S0 g_58 = {21,91,334,-74,7213,1,1,24460};/* VOLATILE GLOBAL g_58 */
static uint16_t g_74 = 65535UL;
static uint32_t g_87 = 0xB773B032L;
static uint16_t g_103 = 0x5664L;
static int8_t g_109 = 1L;
static int8_t g_111 = 0xD8L;
static float g_114 = (-0x9.Ap+1);
static float * volatile g_113 = &g_114;/* VOLATILE GLOBAL g_113 */
static int64_t g_125 = 0xC64A1B41FDD29F2DLL;
static uint16_t *g_130 = &g_74;
static uint16_t * volatile *g_129 = &g_130;
static uint8_t g_134 = 0xE1L;
static float g_141 = 0x4.Ep-1;
static int32_t g_143 = 0x9FCE93EFL;
static struct S0 g_147 = {17,6,521,328,-8393,11,1,26276};/* VOLATILE GLOBAL g_147 */
static volatile uint32_t g_192 = 0xF0B0E5E2L;/* VOLATILE GLOBAL g_192 */
static int16_t g_213 = (-1L);
static float g_224 = 0xD.11F430p-67;
static int8_t g_229 = (-9L);
static uint64_t g_233 = 0UL;
static int8_t *g_235[8][2] = {{&g_111,&g_229},{&g_111,&g_111},{&g_111,&g_229},{&g_111,&g_111},{&g_111,&g_229},{&g_111,&g_111},{&g_111,&g_229},{&g_111,&g_111}};
static int16_t g_249[2][9][6] = {{{(-1L),(-9L),(-1L),0xDE3EL,(-1L),(-1L)},{0x749CL,0L,5L,1L,8L,(-1L)},{0x749CL,(-10L),1L,0xDE3EL,5L,0xFD2DL},{(-1L),(-1L),9L,(-1L),9L,(-1L)},{0L,0xE849L,(-1L),(-1L),(-6L),0L},{5L,0x749CL,0xE849L,1L,0L,0xDE3EL},{(-1L),0x749CL,0xFD2DL,7L,0x1E73L,0L},{(-1L),(-1L),7L,1L,1L,7L},{0L,0L,(-1L),(-10L),1L,9L}},{{(-1L),(-1L),(-6L),0xE849L,1L,(-1L)},{0xDE3EL,(-1L),(-6L),0xFD2DL,0L,9L},{0L,0xFD2DL,(-1L),7L,(-1L),7L},{7L,(-1L),7L,(-1L),0xFD2DL,0L},{9L,0L,0xFD2DL,(-6L),(-1L),0xDE3EL},{(-1L),1L,0xE849L,(-6L),(-1L),(-1L)},{9L,1L,(-10L),(-1L),0L,0L},{7L,1L,1L,7L,(-1L),(-1L)},{0L,0x1E73L,7L,0xFD2DL,0x749CL,(-1L)}}};
static int32_t *g_255[6][5][7] = {{{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,(void*)0,&g_143,&g_143}},{{&g_143,&g_143,&g_143,(void*)0,&g_143,&g_143,&g_143},{(void*)0,&g_143,(void*)0,&g_143,&g_143,&g_143,&g_143},{&g_143,(void*)0,&g_143,&g_143,&g_143,&g_143,&g_143},{(void*)0,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,(void*)0,&g_143,&g_143,&g_143,&g_143}},{{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,(void*)0},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,(void*)0,(void*)0,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143}},{{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,(void*)0,&g_143,&g_143},{&g_143,&g_143,(void*)0,(void*)0,&g_143,&g_143,&g_143},{(void*)0,&g_143,(void*)0,(void*)0,(void*)0,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143}},{{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,(void*)0,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,(void*)0,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,&g_143}},{{(void*)0,&g_143,&g_143,(void*)0,(void*)0,&g_143,&g_143},{&g_143,&g_143,(void*)0,(void*)0,&g_143,&g_143,&g_143},{&g_143,&g_143,&g_143,&g_143,(void*)0,&g_143,&g_143},{&g_143,(void*)0,&g_143,&g_143,&g_143,(void*)0,&g_143},{&g_143,&g_143,&g_143,&g_143,&g_143,&g_143,(void*)0}}};
static const float g_266 = 0xA.D00658p-64;
static int64_t *g_278[9] = {&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125};
static int64_t **g_277 = &g_278[3];
static float * volatile *g_284 = &g_113;
static uint32_t g_314 = 9UL;
static uint8_t g_342[6][3] = {{0xDBL,0x97L,0xDBL},{2UL,2UL,0xF2L},{4UL,0x97L,4UL},{2UL,0xF2L,0xF2L},{0xDBL,0x97L,0xDBL},{2UL,2UL,0xF2L}};
static volatile struct S0 g_392 = {82,40,582,30,4879,7,0,-25880};/* VOLATILE GLOBAL g_392 */
static int32_t ** volatile g_396 = &g_255[0][1][0];/* VOLATILE GLOBAL g_396 */
static struct S0 g_400 = {42,41,287,-336,7483,11,5,10946};/* VOLATILE GLOBAL g_400 */
static int32_t ** volatile g_449 = &g_255[3][3][2];/* VOLATILE GLOBAL g_449 */
static const struct S0 g_452 = {77,186,723,-56,1208,6,5,-25327};/* VOLATILE GLOBAL g_452 */
static uint16_t * const *g_462 = &g_130;
static uint16_t * const **g_461 = &g_462;
static int32_t g_481[8] = {0x7723F62DL,(-1L),0x7723F62DL,(-1L),0x7723F62DL,(-1L),0x7723F62DL,(-1L)};
static uint64_t g_485 = 1UL;
static int32_t **g_503 = &g_255[3][2][6];
static uint16_t **g_584 = &g_130;
static float g_590 = 0xA.2BCB8Ep+17;
static float *g_610 = &g_590;
static float **g_609 = &g_610;
static struct S0 g_646 = {3,69,223,293,-2562,10,7,1205};/* VOLATILE GLOBAL g_646 */
static volatile uint64_t g_662 = 0x6749D2B42FEAA83BLL;/* VOLATILE GLOBAL g_662 */
static int32_t g_663 = 0x0AEC6F41L;
static uint32_t g_688 = 4294967295UL;
static int32_t g_690 = 1L;
static int32_t g_692 = (-10L);
static struct S0 ** volatile g_696 = (void*)0;/* VOLATILE GLOBAL g_696 */
static struct S0 g_699[8][1][4] = {{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}},{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}},{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}},{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}},{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}},{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}},{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}},{{{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279},{18,16,42,-442,-303,11,2,24310},{35,245,721,442,895,2,3,12279}}}};
static struct S0 *g_698 = &g_699[1][0][1];
static struct S0 **g_697 = &g_698;
static const uint16_t g_761 = 65533UL;
static const int32_t *g_775 = &g_143;
static const int32_t ** volatile g_774 = &g_775;/* VOLATILE GLOBAL g_774 */
static int8_t **g_866 = (void*)0;
static int8_t ***g_865[5][9] = {{(void*)0,&g_866,&g_866,&g_866,&g_866,&g_866,(void*)0,&g_866,&g_866},{&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866},{(void*)0,&g_866,&g_866,&g_866,&g_866,&g_866,(void*)0,&g_866,&g_866},{&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866},{&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866,&g_866}};
static volatile struct S0 g_882 = {8,180,394,-344,-5462,13,4,14678};/* VOLATILE GLOBAL g_882 */
static volatile int64_t g_914 = 0x808491FF8692A6DFLL;/* VOLATILE GLOBAL g_914 */
static const int32_t ** const  volatile g_951[7][4] = {{&g_775,&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775,&g_775}};
static const int32_t ** volatile g_952[8][10][3] = {{{&g_775,&g_775,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775},{(void*)0,&g_775,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775}},{{&g_775,(void*)0,(void*)0},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,(void*)0},{&g_775,(void*)0,&g_775},{&g_775,(void*)0,&g_775},{&g_775,(void*)0,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775}},{{(void*)0,(void*)0,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775},{&g_775,(void*)0,&g_775},{&g_775,&g_775,&g_775},{(void*)0,&g_775,&g_775},{&g_775,&g_775,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775}},{{(void*)0,&g_775,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775},{&g_775,(void*)0,(void*)0},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775},{&g_775,&g_775,&g_775}},{{&g_775,&g_775,(void*)0},{&g_775,(void*)0,&g_775},{&g_775,(void*)0,&g_775},{&g_775,(void*)0,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{(void*)0,(void*)0,&g_775},{&g_775,&g_775,&g_775},{(void*)0,&g_775,(void*)0},{&g_775,&g_775,&g_775}},{{&g_775,&g_775,&g_775},{&g_775,(void*)0,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{(void*)0,(void*)0,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,&g_775}},{{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{(void*)0,&g_775,&g_775},{&g_775,&g_775,&g_775},{(void*)0,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775}},{{&g_775,&g_775,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{(void*)0,&g_775,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775},{&g_775,(void*)0,(void*)0},{&g_775,&g_775,&g_775},{&g_775,&g_775,&g_775}}};
static struct S0 ***g_1038 = &g_697;
static int32_t g_1060 = (-6L);
static volatile uint16_t g_1069 = 65535UL;/* VOLATILE GLOBAL g_1069 */
static volatile int32_t g_1102 = 0xF055F042L;/* VOLATILE GLOBAL g_1102 */
static int16_t g_1134[5] = {0x2FFBL,0x2FFBL,0x2FFBL,0x2FFBL,0x2FFBL};
static uint16_t * const * const *g_1182 = &g_462;
static uint16_t * const * const **g_1181 = &g_1182;
static uint16_t * const * const ** volatile * volatile g_1180 = &g_1181;/* VOLATILE GLOBAL g_1180 */
static int16_t g_1226 = 0x6901L;
static volatile int32_t g_1332 = 0L;/* VOLATILE GLOBAL g_1332 */
static volatile struct S0 g_1334 = {50,180,286,-99,8493,15,10,-654};/* VOLATILE GLOBAL g_1334 */
static volatile struct S0 g_1338 = {72,94,102,-336,2000,5,8,28907};/* VOLATILE GLOBAL g_1338 */
static volatile struct S0 g_1350[7] = {{84,76,515,31,1302,8,4,26},{84,76,515,31,1302,8,4,26},{84,76,515,31,1302,8,4,26},{84,76,515,31,1302,8,4,26},{84,76,515,31,1302,8,4,26},{84,76,515,31,1302,8,4,26},{84,76,515,31,1302,8,4,26}};
static volatile struct S0 g_1400 = {44,235,613,-433,1120,8,7,-3818};/* VOLATILE GLOBAL g_1400 */
static int32_t * volatile g_1414 = &g_481[2];/* VOLATILE GLOBAL g_1414 */
static volatile int32_t *g_1415[10] = {&g_1332,&g_1332,&g_1332,&g_1332,&g_1332,&g_1332,&g_1332,&g_1332,&g_1332,&g_1332};
static volatile int32_t ** volatile g_1416 = (void*)0;/* VOLATILE GLOBAL g_1416 */
static int8_t ****g_1420 = &g_865[0][7];
static int8_t ***** volatile g_1419[9] = {&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420};
static int8_t ***** volatile g_1421 = &g_1420;/* VOLATILE GLOBAL g_1421 */
static struct S0 g_1430 = {49,137,412,301,-5123,11,7,-12876};/* VOLATILE GLOBAL g_1430 */
static int32_t g_1490 = (-6L);
static uint8_t g_1512 = 0x15L;
static int8_t * volatile **g_1559 = (void*)0;
static const int32_t ** volatile g_1582[9] = {(void*)0,&g_775,(void*)0,&g_775,(void*)0,&g_775,(void*)0,&g_775,(void*)0};
static const int32_t ** volatile g_1583 = &g_775;/* VOLATILE GLOBAL g_1583 */
static volatile int16_t g_1733[1][7] = {{(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)}};
static volatile int32_t g_1800 = (-5L);/* VOLATILE GLOBAL g_1800 */
static volatile struct S0 g_1812 = {34,26,514,-380,-7383,11,8,31797};/* VOLATILE GLOBAL g_1812 */
static struct S0 g_1880 = {42,198,33,426,6980,12,3,13602};/* VOLATILE GLOBAL g_1880 */
static const volatile float g_1896 = 0x0.2p-1;/* VOLATILE GLOBAL g_1896 */
static uint16_t **g_1967[6][4] = {{&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130}};
static volatile uint8_t g_2000[8][8] = {{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L},{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L},{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L},{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L},{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L},{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L},{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L},{0x3EL,0xF6L,0x3EL,0x78L,0x3EL,0xF6L,0x3EL,0x78L}};
static uint16_t g_2016 = 0xAFBEL;
static volatile struct S0 *g_2188 = &g_392;
static volatile struct S0 ** volatile g_2187 = &g_2188;/* VOLATILE GLOBAL g_2187 */
static volatile struct S0 ** volatile * volatile g_2186 = &g_2187;/* VOLATILE GLOBAL g_2186 */
static volatile struct S0 ** volatile * volatile * volatile g_2185[4][9] = {{&g_2186,(void*)0,&g_2186,(void*)0,&g_2186,&g_2186,&g_2186,&g_2186,&g_2186},{&g_2186,(void*)0,&g_2186,&g_2186,&g_2186,(void*)0,&g_2186,&g_2186,&g_2186},{&g_2186,&g_2186,&g_2186,(void*)0,&g_2186,(void*)0,&g_2186,(void*)0,&g_2186},{&g_2186,&g_2186,&g_2186,&g_2186,&g_2186,&g_2186,(void*)0,&g_2186,(void*)0}};
static volatile struct S0 g_2221 = {79,67,101,488,7167,0,1,29475};/* VOLATILE GLOBAL g_2221 */
static int32_t ** volatile g_2292 = &g_255[4][0][4];/* VOLATILE GLOBAL g_2292 */
static const int8_t *g_2332 = &g_109;
static const int8_t **g_2331 = &g_2332;
static volatile int32_t * volatile *g_2340 = &g_1415[0];
static volatile int32_t * volatile **g_2339 = &g_2340;
static volatile int32_t * volatile ***g_2338 = &g_2339;
static volatile int32_t * volatile **** volatile g_2341 = &g_2338;/* VOLATILE GLOBAL g_2341 */
static volatile uint8_t * const g_2344 = &g_2000[1][5];
static volatile uint8_t * const *g_2343 = &g_2344;
static volatile uint8_t * const **g_2342[5] = {&g_2343,&g_2343,&g_2343,&g_2343,&g_2343};
static volatile uint8_t * const *** volatile g_2345 = &g_2342[1];/* VOLATILE GLOBAL g_2345 */
static struct S0 g_2354[3] = {{3,103,452,470,-2959,1,9,20271},{3,103,452,470,-2959,1,9,20271},{3,103,452,470,-2959,1,9,20271}};
static volatile struct S0 g_2394 = {19,150,66,-379,6227,4,6,-11512};/* VOLATILE GLOBAL g_2394 */
static uint32_t g_2401[7] = {0x75987125L,0x75987125L,0x75987125L,0x75987125L,0x75987125L,0x75987125L,0x75987125L};
static struct S0 **** volatile g_2426[1][2][1] = {{{&g_1038},{&g_1038}}};
static float ***** volatile g_2429 = (void*)0;/* VOLATILE GLOBAL g_2429 */
static float ***g_2432 = &g_609;
static float ****g_2431 = &g_2432;
static float ***** volatile g_2430 = &g_2431;/* VOLATILE GLOBAL g_2430 */
static int32_t ** volatile g_2519 = &g_255[5][2][0];/* VOLATILE GLOBAL g_2519 */
static const int32_t ** volatile g_2537 = (void*)0;/* VOLATILE GLOBAL g_2537 */


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static float  func_3(int64_t  p_4, int32_t  p_5, const int8_t  p_6, uint16_t  p_7, uint16_t  p_8);
static int64_t  func_12(int32_t  p_13, int8_t  p_14, uint16_t  p_15, uint16_t  p_16);
static uint32_t  func_17(uint64_t  p_18, uint64_t  p_19, int64_t  p_20, uint64_t  p_21);
static uint64_t  func_22(int64_t  p_23);
static uint8_t  func_26(int64_t  p_27, int8_t  p_28, int32_t  p_29, uint32_t  p_30, int8_t  p_31);
static int32_t  func_37(int16_t  p_38);
static int64_t  func_45(int32_t  p_46, uint64_t  p_47);
static float  func_50(uint32_t  p_51);
static float  func_52(int16_t  p_53, uint32_t  p_54);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_663
 * writes:
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint8_t l_2 = 1UL;
    uint64_t *l_482 = (void*)0;
    uint64_t *l_483 = (void*)0;
    uint64_t *l_484 = &g_485;
    uint32_t *l_687 = &g_688;
    int32_t *l_689 = &g_690;
    int32_t *l_691[1];
    int32_t l_693 = (-1L);
    int32_t l_694[2][8] = {{0xD022DC92L,1L,1L,0xD022DC92L,0x17ACEC71L,0xD022DC92L,1L,1L},{1L,0x17ACEC71L,(-1L),(-1L),0x17ACEC71L,1L,0x17ACEC71L,(-1L)}};
    int8_t l_695 = (-2L);
    uint64_t l_1335 = 0x96A86E4A31C0FB86LL;
    const uint8_t l_1336[5][3][10] = {{{5UL,0x27L,5UL,0xA1L,0x19L,5UL,0x1FL,0UL,0xA1L,0xA1L},{0UL,0x27L,0x6BL,0x19L,0x19L,0x6BL,0x27L,0UL,0x43L,0x19L},{0UL,0x1FL,5UL,0x19L,0xA1L,5UL,0x27L,5UL,0xA1L,0x19L}},{{5UL,0x27L,5UL,0xA1L,0x19L,5UL,0x1FL,0UL,0xA1L,0xA1L},{0UL,0x27L,0x6BL,0x19L,0x19L,0x6BL,0x27L,0UL,0x43L,0x19L},{0UL,0x1FL,5UL,0x19L,0xA1L,5UL,0x27L,5UL,0xA1L,0x19L}},{{5UL,0x27L,5UL,0xA1L,0x19L,5UL,0x1FL,0UL,0xA1L,0xA1L},{0UL,0x27L,0x6BL,0x19L,0x19L,0x6BL,0x27L,0UL,0x43L,0x19L},{0UL,0x1FL,0UL,5UL,0x6BL,0UL,1UL,0UL,0x6BL,5UL}},{{0UL,1UL,0UL,0x6BL,5UL,0UL,0xA9L,1UL,0x6BL,0x6BL},{1UL,1UL,1UL,5UL,5UL,1UL,1UL,1UL,0UL,5UL},{1UL,0xA9L,0UL,5UL,0x6BL,0UL,1UL,0UL,0x6BL,5UL}},{{0UL,1UL,0UL,0x6BL,5UL,0UL,0xA9L,1UL,0x6BL,0x6BL},{1UL,1UL,1UL,5UL,5UL,1UL,1UL,1UL,0UL,5UL},{1UL,0xA9L,0UL,5UL,0x6BL,0UL,1UL,0UL,0x6BL,5UL}}};
    int32_t l_1337 = 1L;
    int8_t ** const *l_2224[3];
    int32_t l_2228[9];
    uint32_t l_2230 = 0x9287674DL;
    uint8_t *l_2242 = &g_342[3][2];
    uint8_t **l_2241 = &l_2242;
    uint32_t l_2326 = 4294967295UL;
    uint16_t ****l_2363 = (void*)0;
    uint8_t l_2465 = 0x07L;
    struct S0 ****l_2487 = &g_1038;
    const int8_t l_2488 = 4L;
    int32_t *l_2500[3][10][3] = {{{&l_2228[8],(void*)0,&g_481[7]},{(void*)0,&l_2228[8],&l_2228[8]},{&g_481[7],&l_2228[8],&g_143},{&l_694[1][1],(void*)0,&l_2228[8]},{&g_481[7],&g_481[7],&l_2228[8]},{(void*)0,&l_694[1][1],&g_143},{&l_2228[8],&g_481[7],&l_2228[8]},{&l_2228[8],(void*)0,&g_481[7]},{(void*)0,&l_2228[8],&l_2228[8]},{&g_481[7],&l_2228[8],&g_143}},{{&l_694[1][1],(void*)0,&l_2228[8]},{&g_481[7],&g_481[7],&l_2228[8]},{(void*)0,&l_694[1][1],&g_143},{&l_2228[8],&g_481[7],&l_2228[8]},{&l_2228[8],(void*)0,&g_481[7]},{(void*)0,&l_2228[8],&l_2228[8]},{&g_481[7],&l_2228[8],&g_143},{&l_694[1][1],(void*)0,&l_2228[8]},{&g_481[7],&g_481[7],&l_2228[8]},{(void*)0,&l_694[1][1],&g_143}},{{&l_2228[8],&g_481[7],&l_2228[8]},{&l_2228[8],(void*)0,&g_481[7]},{(void*)0,&l_2228[8],&l_2228[8]},{&g_481[7],&l_2228[8],&g_143},{&l_694[1][1],(void*)0,&l_2228[8]},{&g_481[7],&g_481[7],&l_2228[8]},{(void*)0,&l_694[1][1],&g_143},{&l_2228[8],&g_481[7],&l_2228[8]},{&l_2228[8],(void*)0,&g_481[7]},{(void*)0,&l_2228[8],&l_2228[8]}}};
    int32_t *l_2518 = &g_663;
    uint8_t l_2539 = 0UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_691[i] = &g_692;
    for (i = 0; i < 3; i++)
        l_2224[i] = &g_866;
    for (i = 0; i < 9; i++)
        l_2228[i] = (-4L);
    return (*l_2518);
}


/* ------------------------------------------ */
/* 
 * reads : g_1338 g_130 g_74 g_111 g_1350 g_143 g_1226 g_485 g_1490 g_481 g_452.f0 g_1060 g_342 g_1180 g_1181 g_1182 g_462 g_1102 g_400.f5 g_1134 g_775 g_2000 g_2016 g_646 g_58.f1 g_129 g_400.f2 g_147.f7 g_1334.f1 g_1420 g_865 g_1038 g_697 g_698 g_609 g_610 g_1880.f3 g_688 g_58.f0 g_400.f7 g_1400.f0 g_233 g_1733 g_249 g_663 g_134 g_1334.f7 g_1421 g_392.f1 g_699.f7 g_584 g_284 g_113 g_692 g_58.f2 g_114 g_590 g_2185 g_400.f1 g_1880.f5 g_1430.f7 g_11
 * writes: g_143 g_74 g_692 g_1226 g_1060 g_1490 g_485 g_87 g_1102 g_690 g_342 g_249 g_1967 g_2000 g_2016 g_688 g_865 g_590 g_109 g_233 g_663 g_134 g_130 g_114 g_314 g_11
 */
static float  func_3(int64_t  p_4, int32_t  p_5, const int8_t  p_6, uint16_t  p_7, uint16_t  p_8)
{ /* block id: 652 */
    const int32_t *l_1347[4][1][7] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_690,&g_690,&g_690,&g_690,&g_690,&g_690,&g_690}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_690,&g_690,&g_690,&g_690,&g_690,&g_690,&g_690}}};
    const int32_t ** const l_1346 = &l_1347[3][0][6];
    int32_t l_1351 = (-4L);
    int32_t *l_1352 = &g_143;
    int32_t l_1355 = 0x3A5AFE37L;
    uint32_t l_1360 = 0UL;
    int8_t l_1392[5];
    int32_t l_1424 = 1L;
    volatile int32_t *l_1425 = &g_1102;
    int32_t l_1493 = (-3L);
    int32_t l_1494 = 0xAEE85A7BL;
    int32_t l_1495 = 0x63078110L;
    int32_t l_1500 = 0x64FFE135L;
    int32_t l_1504 = 0xCBC3CBB7L;
    int32_t l_1507 = 0L;
    int64_t l_1552 = 0xE055E8C7D073A667LL;
    const float *l_1569 = &g_224;
    const float **l_1568 = &l_1569;
    float l_1577 = (-0x1.Ap-1);
    uint32_t l_1698[6][4] = {{4294967290UL,4294967294UL,0UL,7UL},{4294967290UL,0x5DE772E4L,7UL,0x5DE772E4L},{7UL,0x5DE772E4L,4294967290UL,0xF78A5AEDL},{0x5DE772E4L,0xE599030AL,7UL,7UL},{0xAD6E28E7L,0xBF899531L,0xBF899531L,0xAD6E28E7L},{0xAD6E28E7L,0xF78A5AEDL,7UL,4294967294UL}};
    int8_t l_1731[2];
    struct S0 *l_1739 = &g_699[1][0][1];
    struct S0 **l_1831 = &g_698;
    int8_t *****l_1873[10] = {&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420,&g_1420};
    struct S0 *** const *l_1874 = (void*)0;
    int32_t l_1902[9] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
    uint32_t l_1908 = 0xFF8F8E8AL;
    uint16_t ***l_1976 = (void*)0;
    int8_t l_2013 = 0x4EL;
    int16_t l_2015 = 0x46D1L;
    int8_t ***l_2065 = &g_866;
    uint16_t *l_2148 = &g_2016;
    uint64_t l_2206 = 1UL;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_1392[i] = (-1L);
    for (i = 0; i < 2; i++)
        l_1731[i] = 0x99L;
    if ((((g_1338 , ((*g_130) < (safe_rshift_func_uint8_t_u_u(g_111, 5)))) && (safe_rshift_func_int8_t_s_s(((safe_unary_minus_func_int8_t_s((6UL & ((void*)0 != l_1346)))) | (safe_add_func_int8_t_s_s((((*l_1352) |= (g_1350[3] , l_1351)) && (safe_sub_func_int64_t_s_s(p_6, l_1355))), p_5))), p_4))) != 0x4E21506E7DE98C7DLL))
    { /* block id: 654 */
        uint32_t l_1391 = 0x1DD5E650L;
        uint32_t l_1407 = 18446744073709551606UL;
        uint16_t ***l_1409 = &g_584;
        uint16_t ****l_1408 = &l_1409;
        int32_t l_1412 = 8L;
        const uint32_t l_1465[10] = {4294967295UL,0xBF9E3FF8L,0xBF9E3FF8L,4294967295UL,0xBF9E3FF8L,0xBF9E3FF8L,4294967295UL,0xBF9E3FF8L,0xBF9E3FF8L,4294967295UL};
        int32_t *l_1483 = &l_1424;
        int32_t l_1496 = 0x24A85610L;
        int32_t l_1503 = 0x5DFD8EFDL;
        int32_t l_1508[7] = {0xE9618ACFL,0xE9618ACFL,0xE9618ACFL,0xE9618ACFL,0xE9618ACFL,0xE9618ACFL,0xE9618ACFL};
        uint64_t l_1516 = 0UL;
        uint32_t l_1593 = 0x1D980CF4L;
        int8_t ***l_1643 = &g_866;
        const uint16_t **l_1695 = (void*)0;
        const uint16_t ***l_1694 = &l_1695;
        int32_t ***l_1713 = &g_503;
        int8_t *****l_1774 = &g_1420;
        uint64_t l_1777 = 0UL;
        uint64_t l_1801 = 0UL;
        struct S0 **l_1830 = &l_1739;
        int i;
        for (g_74 = 0; (g_74 <= 1); g_74 += 1)
        { /* block id: 657 */
            int64_t l_1378 = 0xA2690538CA38E150LL;
            int16_t l_1390 = 1L;
            int32_t l_1411 = 1L;
            int32_t **l_1433[2][5][9] = {{{(void*)0,&g_255[3][3][2],&g_255[3][3][2],&l_1352,(void*)0,&g_255[3][1][5],&g_255[1][2][4],&l_1352,&g_255[1][1][4]},{&g_255[1][1][4],&g_255[0][1][5],&g_255[3][1][5],&g_255[5][2][3],&g_255[3][3][2],(void*)0,&g_255[3][3][2],(void*)0,&g_255[3][4][6]},{&l_1352,&g_255[1][2][4],&l_1352,&g_255[0][1][5],(void*)0,&g_255[1][3][3],&g_255[3][1][5],(void*)0,&g_255[5][1][2]},{&g_255[1][3][3],&g_255[4][3][0],&l_1352,&g_255[0][1][5],&l_1352,&g_255[3][4][6],&l_1352,&g_255[0][1][5],&l_1352},{&g_255[1][4][3],&g_255[1][4][3],&g_255[3][3][2],&g_255[5][2][3],(void*)0,(void*)0,&g_255[3][3][2],(void*)0,(void*)0}},{{&g_255[5][2][3],&g_255[3][3][2],&l_1352,&l_1352,&g_255[0][1][5],&g_255[3][3][2],&g_255[3][0][6],&l_1352,&l_1352},{&g_255[3][4][6],&l_1352,&g_255[3][3][2],&g_255[3][1][5],&l_1352,&l_1352,&l_1352,(void*)0,&g_255[3][3][2]},{(void*)0,&g_255[3][3][2],&l_1352,&g_255[3][0][6],&g_255[3][3][2],&g_255[3][1][5],&g_255[4][3][0],&g_255[3][3][2],&l_1352},{&g_255[3][3][2],&g_255[3][3][2],&l_1352,&l_1352,&g_255[3][3][2],&l_1352,&l_1352,&g_255[3][3][2],&g_255[3][3][2]},{&g_255[4][3][0],&l_1352,&g_255[3][1][5],&g_255[3][3][2],&g_255[1][4][3],&g_255[3][3][2],&l_1352,&g_255[1][3][3],(void*)0}}};
            struct S0 ***l_1446 = &g_697;
            int8_t l_1491 = 0x06L;
            int32_t l_1499 = 0x402A4030L;
            float **l_1571[5][5] = {{&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610}};
            int32_t l_1592 = (-3L);
            uint8_t l_1603[8][10] = {{4UL,0x04L,0x04L,4UL,4UL,0x8DL,0x26L,0xDCL,247UL,251UL},{1UL,4UL,253UL,0x66L,247UL,0x26L,0x04L,0x26L,247UL,0x66L},{0x89L,1UL,0x89L,251UL,247UL,0xDCL,0x26L,0x8DL,4UL,4UL},{4UL,0xDCL,0UL,253UL,0x8DL,0UL,0UL,0x8DL,253UL,0UL},{0x71L,0x71L,0x89L,0x04L,4UL,1UL,1UL,1UL,0x8DL,0x66L},{0UL,0x26L,0x71L,0xFEL,1UL,247UL,1UL,0xFEL,0x71L,0x26L},{0x8DL,0x71L,253UL,4UL,0x66L,251UL,0UL,247UL,0x04L,1UL},{0x04L,0xDCL,1UL,0x26L,251UL,251UL,0x26L,1UL,0xDCL,0x04L}};
            uint32_t l_1619[3][3][3] = {{{0x194702D5L,18446744073709551608UL,0x8B4C3D32L},{0x194702D5L,0x194702D5L,18446744073709551608UL},{0x6B7289E7L,18446744073709551608UL,18446744073709551608UL}},{{18446744073709551608UL,18446744073709551611UL,0x8B4C3D32L},{0x6B7289E7L,18446744073709551611UL,0x6B7289E7L},{0x194702D5L,18446744073709551608UL,0x8B4C3D32L}},{{0x194702D5L,0x194702D5L,18446744073709551608UL},{0x6B7289E7L,18446744073709551608UL,18446744073709551608UL},{18446744073709551608UL,18446744073709551611UL,0x8B4C3D32L}}};
            float l_1626 = 0x6.8B2519p+49;
            int32_t **l_1659 = &g_255[3][3][2];
            int8_t * const **l_1685 = (void*)0;
            int8_t * const ***l_1684 = &l_1685;
            int8_t * const ****l_1683 = &l_1684;
            uint64_t l_1797 = 1UL;
            int i, j, k;
            for (g_692 = 0; (g_692 >= 0); g_692 -= 1)
            { /* block id: 660 */
                int8_t l_1363 = 0xB1L;
                int32_t l_1370[5] = {0x44FF1754L,0x44FF1754L,0x44FF1754L,0x44FF1754L,0x44FF1754L};
                uint16_t *l_1371 = &g_103;
                uint64_t *l_1398 = (void*)0;
                uint64_t *l_1399 = &g_233;
                uint32_t *l_1410[5][6];
                int16_t *l_1413 = &g_1134[4];
                volatile int32_t **l_1417 = &g_1415[0];
                int8_t ****l_1418 = &g_865[3][5];
                int i, j;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 6; j++)
                        l_1410[i][j] = &g_688;
                }
            }
        }
        for (l_1507 = 8; (l_1507 != 24); ++l_1507)
        { /* block id: 896 */
            int32_t l_1850 = (-1L);
            int32_t **l_1853 = &l_1483;
            uint64_t *l_1854 = &l_1777;
            int16_t l_1862 = 2L;
            float l_1877 = (-0x3.2p+1);
            int32_t l_1903 = 0xC4F0C8D2L;
            int32_t l_1904 = 0x9A8191E4L;
            int32_t l_1905 = (-1L);
            int32_t l_1906 = 1L;
            int32_t l_1907 = (-5L);
        }
        return p_8;
    }
    else
    { /* block id: 934 */
        struct S0 *l_1922 = &g_646;
        int32_t l_1942 = (-2L);
        int32_t l_1963 = 0L;
        uint16_t ****l_1980 = &l_1976;
        uint16_t *****l_1979 = &l_1980;
        int32_t l_1993 = (-10L);
        int32_t l_1996 = 4L;
        int32_t l_1997 = 1L;
        int32_t l_1998 = 0xD6E6F1BAL;
        int32_t l_1999[3];
        int32_t *l_2012[3][2][9] = {{{(void*)0,(void*)0,&l_1998,&l_1963,(void*)0,&g_481[6],(void*)0,&l_1963,&l_1998},{&l_1998,&l_1998,&l_1507,(void*)0,(void*)0,(void*)0,&l_1998,&l_1963,(void*)0}},{{&l_1494,&l_1351,&g_481[6],&l_1998,&l_1351,&l_1351,&l_1998,&g_481[6],&l_1351},{&l_1351,(void*)0,&l_1507,&l_1504,&l_1504,(void*)0,&l_1998,&l_1998,(void*)0}},{{&l_1963,&l_1494,&l_1998,&l_1351,&l_1351,&l_1351,&l_1998,&l_1494,&l_1963},{&l_1504,(void*)0,(void*)0,&l_1351,&l_1998,&l_1351,(void*)0,&l_1507,&l_1494}}};
        int64_t l_2014 = 0x5040678986B1ECDBLL;
        uint8_t *l_2040 = &g_342[4][0];
        uint64_t *l_2052 = &g_233;
        uint8_t l_2074[2][10][5] = {{{0x4EL,246UL,255UL,0x4EL,0xBDL},{0x58L,8UL,8UL,0x58L,0x32L},{1UL,246UL,0UL,1UL,0xBDL},{0xAEL,0x2EL,8UL,0xAEL,0UL},{1UL,255UL,255UL,1UL,255UL},{0x58L,0x2EL,0x2DL,0x58L,0UL},{0x4EL,246UL,255UL,0x4EL,0xBDL},{0x58L,8UL,8UL,0x58L,0x32L},{1UL,246UL,0UL,1UL,0xBDL},{0x70L,0xAEL,0x68L,0x70L,0xE7L}},{{0xB8L,248UL,248UL,0xB8L,251UL},{0xB7L,0xAEL,0x58L,0xB7L,0xE7L},{0x21L,0x4EL,248UL,0x21L,3UL},{0xB7L,0x68L,0x68L,0xB7L,0UL},{0xB8L,0x4EL,1UL,0xB8L,3UL},{0x70L,0xAEL,0x68L,0x70L,0xE7L},{0xB8L,248UL,248UL,0xB8L,251UL},{0xB7L,0xAEL,0x58L,0xB7L,0xE7L},{0x21L,0x4EL,248UL,0x21L,3UL},{0xB7L,0x68L,0x68L,0xB7L,0UL}}};
        uint64_t l_2116 = 0x6EAE44536B6895E1LL;
        int16_t l_2190 = 0x0E92L;
        int32_t ***l_2194 = &g_503;
        uint32_t *l_2204 = &g_314;
        int16_t *l_2205 = &g_11;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1999[i] = 0x9336EFE4L;
        for (g_1226 = 0; (g_1226 <= 4); g_1226 += 1)
        { /* block id: 937 */
            struct S0 *l_1923 = &g_646;
            int32_t l_1944 = 0x12975570L;
            int32_t l_1945 = 0xD352C0B7L;
            int32_t l_1986 = 0L;
            int32_t l_1987 = 0xFEF5353EL;
            int32_t l_1988 = (-8L);
            int32_t l_1989 = 0x5DF14F81L;
            int32_t l_1990 = 0x60E57196L;
            int32_t l_1991 = 1L;
            int32_t l_1992 = (-1L);
            int32_t l_1994[3][7][1] = {{{(-3L)},{0x8480F090L},{(-3L)},{0x8480F090L},{(-3L)},{0x8480F090L},{(-3L)}},{{0x8480F090L},{(-3L)},{0x8480F090L},{(-3L)},{0x8480F090L},{(-3L)},{0x8480F090L}},{{(-3L)},{0x8480F090L},{(-3L)},{0x8480F090L},{(-3L)},{0x8480F090L},{(-3L)}}};
            int16_t l_1995 = 8L;
            int i, j, k;
            for (g_1060 = 0; (g_1060 <= 7); g_1060 += 1)
            { /* block id: 940 */
                uint16_t l_1946[4][4][6] = {{{0UL,0xB61FL,0x354EL,0x38BDL,0x38BDL,0x354EL},{0UL,0UL,7UL,0xB941L,0x43A0L,0x282CL},{0xB61FL,0UL,65535UL,0UL,0x38BDL,7UL},{0xCD39L,0xB61FL,65535UL,0xCD39L,0UL,0x282CL}},{{0xB941L,0xCD39L,7UL,0xCD39L,0xB941L,0x354EL},{0xCD39L,0xB941L,0x354EL,0UL,0xB941L,0x2C8DL},{0xB61FL,0xCD39L,0xE1B3L,0xB941L,0UL,0x2C8DL},{0UL,0xB61FL,0x354EL,0x38BDL,0x38BDL,0x354EL}},{{0UL,0UL,7UL,0xB941L,0x43A0L,0x282CL},{0xB61FL,0UL,65535UL,0UL,0x38BDL,7UL},{0xCD39L,0xB61FL,65535UL,0xCD39L,0UL,0x282CL},{0xB941L,0xCD39L,7UL,0xCD39L,0xB941L,0x354EL}},{{0xCD39L,0xB941L,0x354EL,0UL,0xB941L,0x2C8DL},{0xB61FL,0xCD39L,0xE1B3L,0xB941L,0UL,0x2C8DL},{0UL,0xB61FL,0x354EL,0x38BDL,0x38BDL,0x354EL},{0UL,0UL,7UL,0xB941L,0x43A0L,0x282CL}}};
                uint32_t l_1969 = 0xB25C5F72L;
                int i, j, k;
                for (g_1490 = 6; (g_1490 >= 3); g_1490 -= 1)
                { /* block id: 943 */
                    int16_t l_1926[6];
                    uint32_t l_1934 = 0xAD007B3CL;
                    int32_t *l_1943[4] = {&g_663,&g_663,&g_663,&g_663};
                    int i;
                    for (i = 0; i < 6; i++)
                        l_1926[i] = (-2L);
                    for (g_485 = 0; (g_485 <= 4); g_485 += 1)
                    { /* block id: 946 */
                        uint32_t *l_1925 = &l_1698[2][2];
                        uint32_t *l_1929 = &g_87;
                        int i;
                        (*l_1352) = l_1392[g_485];
                        (*l_1352) |= (l_1922 == l_1923);
                        (*l_1425) = (~(((((*l_1925) = g_481[g_1490]) != ((((l_1926[4] > ((*l_1929) = ((safe_mod_func_int64_t_s_s(((void*)0 != &g_485), l_1392[g_485])) <= g_481[g_1490]))) > 0xC253L) || ((*l_1352) = (l_1392[g_485] = ((safe_div_func_int8_t_s_s((safe_mul_func_int8_t_s_s((l_1934 == (safe_lshift_func_int8_t_s_u(((((!((safe_rshift_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((0UL || (*l_1352)), 0x8312L)), p_5)) >= 0x68CC2E3CB20DF74ELL)) || 0xA8310B1D3B4B470DLL) || g_452.f0) & p_6), p_7))), 1UL)), g_481[0])) || l_1942)))) | 246UL)) == g_481[g_1490]) , 0xF809CAC9L));
                    }
                    l_1946[3][2][1]++;
                    return l_1946[2][0][5];
                }
                for (g_690 = 0; (g_690 <= 8); g_690 += 1)
                { /* block id: 960 */
                    int32_t *l_1968[7][8][4] = {{{(void*)0,&g_481[7],&l_1495,(void*)0},{&g_481[5],(void*)0,&l_1945,&g_481[7]},{&l_1945,&l_1504,&l_1942,(void*)0},{&l_1945,&g_663,&l_1424,&g_481[g_1060]},{&l_1355,&g_481[7],&l_1902[8],&l_1902[8]},{&g_481[7],(void*)0,&l_1945,&l_1495},{(void*)0,&l_1355,&l_1355,(void*)0},{&l_1902[8],&g_481[5],&l_1942,(void*)0}},{{&g_663,&g_143,&l_1945,&g_481[7]},{&l_1945,(void*)0,&l_1963,&g_481[7]},{&l_1355,&g_143,&l_1902[8],(void*)0},{&l_1495,&g_481[5],&l_1504,(void*)0},{&g_481[g_1060],&l_1355,(void*)0,&l_1495},{&l_1355,(void*)0,&l_1355,&l_1902[8]},{&g_143,&g_481[7],&l_1945,&g_481[g_1060]},{&g_481[7],&l_1355,(void*)0,&g_481[7]}},{{&l_1902[8],&l_1495,(void*)0,(void*)0},{&g_481[7],&g_481[7],&l_1945,&g_143},{&g_143,(void*)0,&l_1355,&l_1945},{&l_1355,&l_1945,(void*)0,(void*)0},{&g_481[g_1060],&g_481[g_1060],&l_1504,&g_663},{&l_1495,&l_1355,&l_1902[8],&g_481[5]},{&l_1355,&g_663,&l_1963,&l_1902[8]},{&l_1945,&g_663,&l_1945,&g_481[5]}},{{&g_663,&l_1355,&l_1942,&g_663},{&l_1902[8],&g_481[g_1060],&l_1355,(void*)0},{(void*)0,&l_1945,&l_1945,&l_1945},{&g_481[7],(void*)0,&l_1902[8],&g_143},{&l_1355,&g_481[7],&l_1424,(void*)0},{&g_481[5],&l_1495,&l_1504,&g_481[7]},{&g_481[5],&l_1355,&l_1424,&g_481[g_1060]},{&l_1355,&g_481[7],&l_1902[8],&l_1902[8]}},{{&g_481[7],(void*)0,&l_1945,&l_1495},{(void*)0,&l_1355,&l_1355,(void*)0},{&l_1902[8],&g_481[5],&l_1942,(void*)0},{&g_663,&g_143,&l_1945,&g_481[7]},{&l_1945,(void*)0,&l_1963,&g_481[7]},{&l_1355,&g_143,&l_1902[8],(void*)0},{&l_1495,&g_481[5],&l_1504,(void*)0},{&g_481[g_1060],&l_1355,(void*)0,&l_1495}},{{&l_1355,(void*)0,&l_1355,&l_1902[8]},{&g_143,&g_481[7],&l_1945,&g_481[g_1060]},{&g_481[7],&l_1355,(void*)0,&g_481[7]},{&l_1902[8],&l_1495,(void*)0,(void*)0},{&g_481[7],&g_481[7],&l_1945,&g_143},{&g_143,(void*)0,&l_1355,&l_1945},{&l_1355,&l_1945,(void*)0,(void*)0},{&g_481[g_1060],&g_481[g_1060],&l_1504,&g_663}},{{&l_1495,&l_1355,&l_1902[8],&g_481[5]},{&l_1355,&g_663,&l_1963,&l_1902[8]},{&l_1945,&g_663,&l_1945,&g_481[5]},{&g_663,&l_1944,&l_1504,&l_1355},{(void*)0,&l_1963,&l_1944,&l_1493},{&l_1942,&l_1424,&g_481[5],&l_1424},{(void*)0,&l_1945,(void*)0,&l_1902[8]},{(void*)0,(void*)0,(void*)0,&l_1493}}};
                    int i, j, k;
                    if ((l_1392[g_1226] & g_481[g_1060]))
                    { /* block id: 961 */
                        uint8_t *l_1951 = (void*)0;
                        uint8_t *l_1952 = (void*)0;
                        uint8_t *l_1953 = &g_342[4][2];
                        int64_t *l_1962 = &l_1552;
                        int16_t *l_1964 = &g_249[1][3][5];
                        uint16_t **l_1965 = (void*)0;
                        uint16_t ***l_1966 = (void*)0;
                        int i;
                        l_1945 ^= ((((safe_sub_func_uint8_t_u_u(p_6, ((*l_1953) |= p_8))) <= (l_1942 ^ (((((0xEFC3L ^ ((0x4710EB3375559377LL && (p_7 , (0L != l_1946[3][2][1]))) && ((*l_1964) = (safe_lshift_func_int16_t_s_s((+(l_1392[g_1226] = ((((l_1963 = ((*l_1352) |= ((***g_1182) = ((p_5 = ((safe_mul_func_uint8_t_u_u(((((*l_1962) = (safe_unary_minus_func_uint16_t_u((*****g_1180)))) ^ 18446744073709551615UL) <= l_1942), 1L)) | 0xABBA1E35L)) , p_8)))) , (*l_1425)) > p_7) || g_400.f5))), 2))))) > l_1942) ^ g_1134[3]) , g_1350[3].f3) || 1UL))) <= 0x35B993CCL) >= 0xD4L);
                        g_1967[4][0] = l_1965;
                    }
                    else
                    { /* block id: 972 */
                        l_1968[3][5][3] = &l_1944;
                        if ((*g_775))
                            break;
                    }
                    l_1969 = 8L;
                }
            }
            (*l_1352) &= ((safe_div_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((((((safe_sub_func_uint16_t_u_u((0x2E91L < ((void*)0 == &g_11)), l_1392[g_1226])) , ((void*)0 != l_1976)) <= ((g_481[(g_1226 + 2)] != ((safe_rshift_func_int8_t_s_s(g_481[(g_1226 + 3)], 5)) >= p_4)) || 1UL)) <= 0x1904L) != l_1942), 3L)), p_6)) && 0xA4BFF20588EC3E98LL);
            for (l_1355 = 4; (l_1355 >= 0); l_1355 -= 1)
            { /* block id: 982 */
                int32_t *l_1981 = (void*)0;
                int32_t *l_1982 = &l_1902[2];
                int32_t *l_1983 = &l_1495;
                int32_t *l_1984 = &l_1500;
                int32_t *l_1985[1][1];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1985[i][j] = &l_1902[8];
                }
                if (p_5)
                    break;
                (*l_1352) |= ((void*)0 == l_1979);
                --g_2000[1][5];
            }
            for (l_1991 = 0; (l_1991 <= 4); l_1991 += 1)
            { /* block id: 989 */
                int32_t *l_2003 = (void*)0;
                int32_t *l_2004 = &l_1990;
                int32_t *l_2005 = &l_1986;
                int32_t *l_2006 = &l_1993;
                int32_t *l_2007 = (void*)0;
                int32_t *l_2008[5];
                uint32_t l_2009[4];
                int i;
                for (i = 0; i < 5; i++)
                    l_2008[i] = &l_1494;
                for (i = 0; i < 4; i++)
                    l_2009[i] = 0x32B2934BL;
                l_2009[0]--;
                return p_5;
            }
        }
        ++g_2016;
        if (((safe_add_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u((((((safe_mod_func_uint8_t_u_u(((safe_add_func_uint8_t_u_u(((~(safe_lshift_func_int16_t_s_s((g_1338.f1 , (safe_add_func_uint64_t_u_u(((safe_add_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s((p_4 | (((void*)0 == &l_1980) ^ ((((*l_1922) , (((*l_1352) = (0x4B8EAE7AL < ((safe_rshift_func_uint8_t_u_s(((*l_2040) &= (1UL < p_8)), 5)) > (safe_add_func_uint32_t_u_u(4UL, p_8))))) == p_8)) , (void*)0) == &l_2015))), g_58.f1)), p_7)), p_8)) != 6L), g_1226))), p_6))) & (**g_129)), 8UL)) < 4294967291UL), g_400.f2)) , p_7) == p_4) > g_58.f1) == 0xD119L), 1UL)), 0x2F5982E8L)) <= 5L))
        { /* block id: 997 */
            return p_6;
        }
        else
        { /* block id: 999 */
            uint16_t l_2055[7] = {3UL,3UL,0xF9EEL,3UL,3UL,0xF9EEL,3UL};
            int32_t l_2056 = (-3L);
            int32_t l_2113 = 0xE94A6969L;
            int32_t l_2114 = 0xA9D8E908L;
            int8_t ****l_2131 = &l_2065;
            struct S0 ** const *l_2156 = &l_1831;
            struct S0 ** const **l_2155 = &l_2156;
            uint64_t l_2189 = 0x8A0C366FA33A6990LL;
            int16_t l_2192 = 0xCCA9L;
            int i;
            for (l_1355 = 19; (l_1355 == (-15)); l_1355 = safe_sub_func_uint64_t_u_u(l_1355, 2))
            { /* block id: 1002 */
                uint32_t *l_2049 = &g_688;
                int32_t l_2057[7][7][5] = {{{1L,0L,0xCF833B27L,(-7L),0xEF93B096L},{(-3L),0L,9L,0xB2917DE5L,9L},{0L,0L,9L,5L,0L},{0x6DC531F2L,0L,0xEF93B096L,(-1L),9L},{0xF4DCFDADL,0L,0L,0x91ADE1D7L,0xCF833B27L},{1L,0L,0xCF833B27L,(-7L),0xEF93B096L},{(-3L),0L,9L,0xB2917DE5L,9L}},{{0L,0L,9L,5L,0L},{0x6DC531F2L,0L,0xEF93B096L,(-1L),9L},{0xF4DCFDADL,0L,0L,0x91ADE1D7L,0xCF833B27L},{1L,0L,0xCF833B27L,(-7L),0xEF93B096L},{(-3L),0L,9L,0xB2917DE5L,9L},{0L,0L,9L,5L,0L},{0x6DC531F2L,0L,0xEF93B096L,(-1L),9L}},{{0xF4DCFDADL,0L,0L,0x91ADE1D7L,0xCF833B27L},{1L,0L,0xCF833B27L,(-7L),0xEF93B096L},{(-3L),0L,9L,0xB2917DE5L,9L},{0L,0L,9L,5L,0L},{0x6DC531F2L,0L,0xEF93B096L,(-1L),9L},{0xF4DCFDADL,0L,0L,0x91ADE1D7L,0xCF833B27L},{1L,0L,0xCF833B27L,(-7L),0xEF93B096L}},{{(-3L),0L,9L,0xB2917DE5L,9L},{0L,0L,9L,5L,0L},{0x6DC531F2L,0L,0xEF93B096L,(-1L),9L},{0xF4DCFDADL,0L,0L,0x91ADE1D7L,0xCF833B27L},{1L,0L,0xCF833B27L,(-7L),0xEF93B096L},{(-3L),0L,9L,0xB2917DE5L,9L},{0L,0L,9L,5L,0L}},{{0x6DC531F2L,0L,0xEF93B096L,(-1L),9L},{0xF4DCFDADL,0L,0L,0x91ADE1D7L,0xCF833B27L},{1L,0L,0xCF833B27L,(-7L),0xEF93B096L},{(-3L),0L,9L,0xB2917DE5L,9L},{0L,0L,9L,5L,0L},{9L,9L,1L,0x4C43CD88L,1L},{0xCF833B27L,9L,9L,(-1L),0x04C0B57BL}},{{0L,9L,0x04C0B57BL,0x6DC9F68CL,1L},{0xEF93B096L,9L,(-4L),5L,(-4L)},{9L,9L,1L,0xBA64CE0CL,9L},{9L,9L,1L,0x4C43CD88L,1L},{0xCF833B27L,9L,9L,(-1L),0x04C0B57BL},{0L,9L,0x04C0B57BL,0x6DC9F68CL,1L},{0xEF93B096L,9L,(-4L),5L,(-4L)}},{{9L,9L,1L,0xBA64CE0CL,9L},{9L,9L,1L,0x4C43CD88L,1L},{0xCF833B27L,9L,9L,(-1L),0x04C0B57BL},{0L,9L,0x04C0B57BL,0x6DC9F68CL,1L},{0xEF93B096L,9L,(-4L),5L,(-4L)},{9L,9L,1L,0xBA64CE0CL,9L},{9L,9L,1L,0x4C43CD88L,1L}}};
                int8_t ***l_2066 = &g_866;
                struct S0 *** const *l_2071[8][5][2] = {{{(void*)0,&g_1038},{(void*)0,&g_1038},{&g_1038,&g_1038},{&g_1038,&g_1038},{(void*)0,&g_1038}},{{(void*)0,&g_1038},{&g_1038,&g_1038},{&g_1038,&g_1038},{(void*)0,&g_1038},{(void*)0,&g_1038}},{{&g_1038,&g_1038},{&g_1038,&g_1038},{(void*)0,&g_1038},{(void*)0,&g_1038},{&g_1038,&g_1038}},{{&g_1038,&g_1038},{(void*)0,&g_1038},{(void*)0,&g_1038},{&g_1038,&g_1038},{&g_1038,&g_1038}},{{(void*)0,&g_1038},{(void*)0,&g_1038},{&g_1038,&g_1038},{&g_1038,&g_1038},{(void*)0,&g_1038}},{{(void*)0,&g_1038},{&g_1038,&g_1038},{&g_1038,&g_1038},{(void*)0,&g_1038},{(void*)0,&g_1038}},{{&g_1038,&g_1038},{&g_1038,&g_1038},{(void*)0,&g_1038},{(void*)0,&g_1038},{&g_1038,&g_1038}},{{&g_1038,&g_1038},{(void*)0,&g_1038},{(void*)0,&g_1038},{&g_1038,&g_1038},{&g_1038,&g_1038}}};
                uint16_t l_2105 = 1UL;
                int8_t ****l_2132 = &l_2066;
                uint64_t * const l_2135 = &g_233;
                const uint8_t l_2191 = 1UL;
                int i, j, k;
                if ((g_147.f7 & (safe_sub_func_int8_t_s_s(((p_8 >= (p_8 >= (safe_mod_func_int32_t_s_s((((*l_2049) = g_1334.f1) & (((safe_sub_func_int64_t_s_s((((l_2052 != l_2052) & ((((safe_sub_func_uint16_t_u_u(((****g_1181) || (l_2056 ^= ((p_4 , (((void*)0 != &g_697) == l_2055[5])) & g_481[4]))), 0L)) , (*l_1352)) == p_7) >= 0x3539L)) , p_7), p_8)) , p_7) && l_2057[2][1][3])), g_646.f1)))) >= (*l_1352)), (*l_1352)))))
                { /* block id: 1005 */
                    uint32_t l_2058 = 0xEFC347C8L;
                    ++l_2058;
                }
                else
                { /* block id: 1007 */
                    struct S0 *** const **l_2072 = (void*)0;
                    struct S0 *** const **l_2073 = &l_1874;
                    int32_t l_2079[9] = {0L,0x43311D11L,0L,0L,0x43311D11L,0L,0L,0x43311D11L,0L};
                    const uint16_t l_2106 = 65534UL;
                    int i;
                    (**g_609) = ((((*l_2040) = (safe_mod_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u((g_1350[3].f4 != (((*l_1352) == (((*g_1420) = (*g_1420)) == (l_2066 = l_2065))) < ((0x962AL ^ ((safe_mul_func_uint8_t_u_u(((safe_mod_func_int8_t_s_s((((*l_2073) = l_2071[3][3][1]) != &g_1038), l_2056)) , l_2057[3][5][2]), l_2074[0][4][2])) > 0UL)) && l_2057[5][1][1]))), l_2057[2][1][3])), (*l_1352)))) , (void*)0) != (**g_1038));
                    for (g_109 = 0; (g_109 <= 4); g_109 += 1)
                    { /* block id: 1015 */
                        int8_t *****l_2084 = &g_1420;
                        int32_t l_2103 = 0x4EE2F412L;
                        int16_t *l_2104 = &l_2015;
                        uint16_t *****l_2107 = &l_1980;
                        int32_t l_2108 = 0L;
                        int32_t l_2109 = (-8L);
                        int32_t l_2110 = 0x16EFBBEDL;
                        int32_t l_2111 = 0x6CA3F744L;
                        int32_t l_2112 = 4L;
                        int32_t l_2115 = 0x9075D6D8L;
                        (*l_1352) = (((g_1880.f3 || ((0UL > (safe_mod_func_uint16_t_u_u((((safe_div_func_int8_t_s_s(l_2079[7], (safe_add_func_uint32_t_u_u((safe_add_func_uint32_t_u_u(((*l_2049) &= (((*l_2040) |= p_8) >= p_4)), ((void*)0 == &g_449))), (((void*)0 == l_2084) || l_2079[1]))))) || g_2016) == g_58.f0), g_400.f7))) , p_7)) < 1L) || p_7);
                        l_2107 = (((safe_rshift_func_uint8_t_u_s(((*l_2040) = (safe_rshift_func_int8_t_s_s(((((safe_div_func_uint16_t_u_u((l_1874 != (((l_2079[7] != (g_1400.f0 , ((+((((*l_2052) ^= l_2057[2][1][3]) , p_8) , (g_1733[0][0] ^ g_249[1][4][4]))) || (l_2057[1][3][4] && ((l_2105 ^= (safe_rshift_func_uint8_t_u_s((((+((((((*l_1352) ^= (safe_div_func_int16_t_s_s((safe_mul_func_int8_t_s_s((((safe_add_func_float_f_f((((*l_1425) < p_6) > p_8), l_2103)) == p_8) , p_8), 0x18L)), p_8))) , l_2104) != &l_2015) ^ p_4) < 0x4B80A73095CEBA1ALL)) || l_2055[5]) | g_663), 4))) , 0UL))))) == l_2106) , &g_1038)), 0x747FL)) != 65535UL) , 0x50CFL) && p_6), 6))), p_7)) , l_2106) , (void*)0);
                        l_2116--;
                    }
                }
                for (g_663 = 0; (g_663 < (-24)); g_663 = safe_sub_func_int16_t_s_s(g_663, 2))
                { /* block id: 1029 */
                    int64_t l_2136 = (-3L);
                    uint32_t l_2143 = 18446744073709551613UL;
                    (*l_1352) &= ((((p_8 & (g_134 &= g_342[3][2])) & (((safe_mod_func_uint16_t_u_u((l_2105 <= (&p_6 == &p_6)), (p_4 || (0x94528E02L <= (safe_lshift_func_int16_t_s_u((safe_mul_func_uint8_t_u_u(((l_2132 = l_2131) == ((safe_rshift_func_int16_t_s_u(g_1334.f7, (*g_130))) , (*g_1421))), p_4)), 9)))))) || l_2055[2]) != 0xB92BA8E144FA07D2LL)) && 0xB3L) != p_7);
                    if (((void*)0 != l_2135))
                    { /* block id: 1033 */
                        int16_t l_2139[1];
                        uint16_t * const ***l_2142 = &g_461;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_2139[i] = (-3L);
                        (*l_1425) = l_2057[2][3][4];
                        (*g_610) = (((l_2136 , (((p_4 < (l_2055[3] , (safe_lshift_func_uint8_t_u_s(g_392.f1, 5)))) > (l_2139[0] >= (((safe_mul_func_uint8_t_u_u(0x09L, ((*l_1352) ^= ((((-1L) | (l_2142 != (void*)0)) && p_6) >= 0xAA089002L)))) , p_8) > 0xEF5246273A6DB630LL))) & g_699[1][0][1].f7)) && l_2055[0]) , l_2143);
                    }
                    else
                    { /* block id: 1037 */
                        float *l_2157 = (void*)0;
                        (*l_1425) = ((safe_mul_func_float_f_f((((*g_610) = ((void*)0 == (**g_1038))) > (safe_mul_func_float_f_f((((**g_284) = (((*g_584) = &p_8) == (l_2148 = &p_8))) , (safe_mul_func_float_f_f((*l_1425), (((((safe_div_func_float_f_f((p_5 != (safe_sub_func_float_f_f(((((((void*)0 == l_2155) == (g_692 | 0xFFD8L)) , 0x0.8F0AD8p-15) <= l_2114) > p_8), l_2114))), p_5)) < l_2105) >= 0xA.7A2C9Fp-31) > l_2055[6]) >= g_58.f2)))), p_6))), (-0x1.4p-1))) <= 0x0.4p-1);
                    }
                    (**g_609) = (safe_add_func_float_f_f((safe_mul_func_float_f_f(((-0x5.5880D6p+29) >= (((-10L) < p_4) , ((**g_284) = ((*l_1425) , (*g_113))))), (*g_610))), (*g_610)));
                }
                (*g_610) = (-0x3.9p+1);
                (*l_1352) = (safe_sub_func_int64_t_s_s((safe_div_func_uint8_t_u_u(((((safe_rshift_func_uint8_t_u_u(p_8, 7)) > (safe_sub_func_int32_t_s_s((safe_unary_minus_func_uint16_t_u((safe_rshift_func_uint16_t_u_u((((!p_7) != (p_7 || (((safe_mul_func_int16_t_s_s(((safe_mul_func_int8_t_s_s(l_2113, ((((((l_2057[1][0][4] = (safe_unary_minus_func_uint16_t_u((((65535UL <= ((safe_rshift_func_int8_t_s_u((safe_sub_func_int8_t_s_s(p_7, (l_2055[5] < ((safe_unary_minus_func_uint64_t_u((&l_2156 == g_2185[0][8]))) || p_8)))), p_5)) < g_400.f1)) >= g_663) == (*l_1352))))) ^ l_2189) && (-1L)) == l_2190) || (*g_130)) == l_2191))) <= l_2192), p_7)) & 0xC00EDCAEL) != l_2113))) < p_7), 4)))), 0x2C98B792L))) <= l_2105) < g_1880.f5), l_2191)), 0x7C3F53BB106D1848LL));
            }
        }
        l_1902[5] ^= ((+((void*)0 == l_2194)) , ((safe_add_func_int16_t_s_s(5L, (((*l_2205) |= (safe_rshift_func_int8_t_s_u((&l_2015 != &l_2190), (safe_add_func_uint16_t_u_u(0xD780L, ((((((safe_mod_func_uint32_t_u_u(((((*l_2204) = (g_249[0][4][3] & (!((*l_1922) , p_5)))) == g_1430.f7) <= g_233), 0x57AF7ECDL)) , p_7) != p_5) != (*l_1352)) > p_7) != (*l_1352))))))) > (*l_1352)))) <= 0x8A07E256L));
    }
    return l_2206;
}


/* ------------------------------------------ */
/* 
 * reads : g_696 g_697 g_584 g_130 g_74 g_688 g_143 g_452.f6 g_111 g_775 g_774 g_699.f5 g_147 g_699 g_481 g_58.f6 g_284 g_113 g_114 g_610 g_609 g_452.f0 g_761 g_11 g_213 g_109 g_590 g_698 g_663 g_1060 g_233 g_1069 g_400.f7 g_1102 g_646.f1 g_58.f3 g_485 g_646.f5 g_314 g_229 g_342 g_1180 g_55.f0 g_1182 g_462 g_134 g_1181 g_1332
 * writes: g_697 g_74 g_688 g_229 g_111 g_213 g_87 g_775 g_590 g_143 g_1038 g_690 g_663 g_114 g_342 g_314 g_249 g_233 g_485 g_1226 g_109 g_698
 */
static int64_t  func_12(int32_t  p_13, int8_t  p_14, uint16_t  p_15, uint16_t  p_16)
{ /* block id: 390 */
    struct S0 ***l_700 = &g_697;
    int32_t l_708 = 1L;
    const int8_t l_711 = (-2L);
    uint32_t *l_712 = &g_688;
    float **l_713 = &g_610;
    int8_t *l_714 = &g_229;
    int32_t l_798 = 0xBBC70D5CL;
    int32_t l_800 = 2L;
    int32_t l_801 = 0L;
    int32_t l_802 = 0xA966F9F4L;
    int32_t l_806[9][10][2] = {{{0x0A4E7CBFL,0x7DED3F7BL},{0xB406D74BL,0xD236ADB1L},{0x0A4E7CBFL,0x70EFF403L},{0x4C0AB180L,(-1L)},{0x61171084L,1L},{1L,0xD236ADB1L},{0x4B288DB5L,0xD236ADB1L},{1L,1L},{0x61171084L,(-1L)},{0x4C0AB180L,0x70EFF403L}},{{0x0A4E7CBFL,0x2466E795L},{1L,(-1L)},{0x2A533F0FL,0x0497179EL},{0xB406D74BL,0x70EFF403L},{0x4B288DB5L,0xA1F3692DL},{0x980789ABL,(-1L)},{0x0A4E7CBFL,(-1L)},{0x980789ABL,0xA1F3692DL},{0x4B288DB5L,0x70EFF403L},{0xB406D74BL,0x0497179EL}},{{0x2A533F0FL,(-1L)},{1L,0x2466E795L},{0x2A533F0FL,0xA1F3692DL},{0xB406D74BL,1L},{0x4B288DB5L,0x0497179EL},{0x980789ABL,0x2466E795L},{0x0A4E7CBFL,0x2466E795L},{0x980789ABL,0x0497179EL},{0x4B288DB5L,1L},{0xB406D74BL,0xA1F3692DL}},{{0x2A533F0FL,0x2466E795L},{1L,(-1L)},{0x2A533F0FL,0x0497179EL},{0xB406D74BL,0x70EFF403L},{0x4B288DB5L,0xA1F3692DL},{0x980789ABL,(-1L)},{0x0A4E7CBFL,(-1L)},{0x980789ABL,0xA1F3692DL},{0x4B288DB5L,0x70EFF403L},{0xB406D74BL,0x0497179EL}},{{0x2A533F0FL,(-1L)},{1L,0x2466E795L},{0x2A533F0FL,0xA1F3692DL},{0xB406D74BL,1L},{0x4B288DB5L,0x0497179EL},{0x980789ABL,0x2466E795L},{0x0A4E7CBFL,0x2466E795L},{0x980789ABL,0x0497179EL},{0x4B288DB5L,1L},{0xB406D74BL,0xA1F3692DL}},{{0x2A533F0FL,0x2466E795L},{1L,(-1L)},{0x2A533F0FL,0x0497179EL},{0xB406D74BL,0x70EFF403L},{0x4B288DB5L,0xA1F3692DL},{0x980789ABL,(-1L)},{0x0A4E7CBFL,(-1L)},{0x980789ABL,0xA1F3692DL},{0x4B288DB5L,0x70EFF403L},{0xB406D74BL,0x0497179EL}},{{0x2A533F0FL,(-1L)},{1L,0x2466E795L},{0x2A533F0FL,0xA1F3692DL},{0xB406D74BL,1L},{0x4B288DB5L,0x0497179EL},{0x980789ABL,0x2466E795L},{0x0A4E7CBFL,0x2466E795L},{0x980789ABL,0x0497179EL},{0x4B288DB5L,1L},{0xB406D74BL,0xA1F3692DL}},{{0x2A533F0FL,0x2466E795L},{1L,(-1L)},{0x2A533F0FL,0x0497179EL},{0xB406D74BL,0x70EFF403L},{0x4B288DB5L,0xA1F3692DL},{0x980789ABL,(-1L)},{0x0A4E7CBFL,(-1L)},{0x980789ABL,0xA1F3692DL},{0x4B288DB5L,0x70EFF403L},{0xB406D74BL,0x0497179EL}},{{0x2A533F0FL,(-1L)},{1L,0x2466E795L},{0x2A533F0FL,0xA1F3692DL},{0xB406D74BL,1L},{0x4B288DB5L,0x0497179EL},{0x980789ABL,0x2466E795L},{0x0A4E7CBFL,0x2466E795L},{0x980789ABL,0x0497179EL},{0x4B288DB5L,1L},{0xB406D74BL,0xA1F3692DL}}};
    int64_t l_843 = 0x45409BEA6BB43309LL;
    int32_t *l_858 = &l_800;
    int32_t l_913 = 4L;
    int16_t l_948 = (-1L);
    int64_t **l_976[7];
    int32_t l_1031[8] = {(-1L),0L,(-1L),0L,(-1L),0L,(-1L),0L};
    int64_t l_1072[8][10][1] = {{{3L},{0L},{0L},{0x3D3FA83DD3213360LL},{0x971F1B06B3DF67D0LL},{0x271CEC3281D72B22LL},{(-4L)},{1L},{0L},{0x184C671FD55AFF36LL}},{{0xD6A279231DC68C72LL},{0x1EF33508000C8BEDLL},{(-7L)},{0x1EF33508000C8BEDLL},{0xD6A279231DC68C72LL},{0x184C671FD55AFF36LL},{0L},{1L},{(-4L)},{0x271CEC3281D72B22LL}},{{0x971F1B06B3DF67D0LL},{0x3D3FA83DD3213360LL},{0L},{0L},{2L},{0x271CEC3281D72B22LL},{3L},{5L},{(-7L)},{0x84FBFBD99CA13ED4LL}},{{(-7L)},{5L},{3L},{0x271CEC3281D72B22LL},{2L},{0x1EF33508000C8BEDLL},{0L},{1L},{1L},{(-7L)}},{{(-1L)},{(-6L)},{0L},{1L},{0L},{0x6AB4F44F5048987CLL},{0L},{0x6AB4F44F5048987CLL},{0L},{1L}},{{0L},{(-6L)},{(-1L)},{(-7L)},{1L},{1L},{0L},{0x1EF33508000C8BEDLL},{2L},{0x271CEC3281D72B22LL}},{{3L},{5L},{(-7L)},{0x84FBFBD99CA13ED4LL},{(-7L)},{5L},{3L},{0x271CEC3281D72B22LL},{2L},{0x1EF33508000C8BEDLL}},{{0L},{1L},{1L},{(-7L)},{(-1L)},{(-6L)},{0L},{1L},{0L},{0x6AB4F44F5048987CLL}}};
    uint16_t **l_1093 = (void*)0;
    uint32_t l_1127 = 0x9C6FB836L;
    uint16_t l_1129[9][8] = {{1UL,0xA138L,0UL,0xA138L,1UL,65535UL,0xA138L,0UL},{2UL,1UL,0x0B29L,0x6A35L,0xA138L,2UL,2UL,0xA138L},{0xDF63L,0x0B29L,0x0B29L,0xDF63L,65532UL,65530UL,0xA138L,0x0B29L},{0xA138L,0UL,0UL,0UL,0UL,0xA03FL,0UL,0UL},{65530UL,0UL,65530UL,0x0B29L,0xA138L,65530UL,65532UL,0xDF63L},{1UL,0x0B29L,0x6A35L,0xA138L,2UL,2UL,0xA138L,0x6A35L},{1UL,1UL,0xA03FL,0UL,0xA138L,65535UL,1UL,0xA138L},{65530UL,0xA138L,0x0B29L,65530UL,0UL,65530UL,0x0B29L,0xA138L},{0xA138L,65532UL,0x6A35L,0UL,65532UL,0x0B29L,0UL,0x6A35L}};
    int16_t l_1132 = 0x6457L;
    uint16_t l_1135 = 0xB713L;
    int8_t * const *l_1157 = &l_714;
    int8_t * const **l_1156 = &l_1157;
    int8_t * const ***l_1155 = &l_1156;
    uint16_t l_1213 = 0xA72BL;
    int64_t l_1290 = 0L;
    uint16_t l_1329 = 0xBA7FL;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_976[i] = &g_278[3];
    if (((g_696 == ((*l_700) = g_697)) && ((safe_div_func_uint16_t_u_u((248UL <= ((!(p_14 < (p_13 & ((*l_714) = (safe_sub_func_uint64_t_u_u(((((*l_712) ^= (safe_div_func_uint16_t_u_u(((((**g_584) &= p_15) | (l_708 , 0x81C9L)) , (safe_div_func_uint8_t_u_u(l_711, l_708))), 0x07B4L))) , l_713) != (void*)0), p_16)))))) ^ g_143)), g_452.f6)) ^ p_16)))
    { /* block id: 395 */
        uint32_t l_720[3];
        int32_t l_767 = 0xF318CB9DL;
        uint32_t l_768 = 5UL;
        int32_t l_808 = 0x856BFEE5L;
        int32_t l_810 = 0x742EF130L;
        int32_t l_811[1][4];
        int8_t **l_823 = &g_235[4][1];
        int8_t ***l_822 = &l_823;
        int64_t l_915 = 0x4C8AB0275675FF8ELL;
        uint32_t l_917 = 0x354F32AFL;
        const struct S0 *l_929 = &g_147;
        const struct S0 **l_928 = &l_929;
        uint32_t l_946 = 0x05B1B097L;
        struct S0 *l_1091 = (void*)0;
        float l_1128[7][8][4] = {{{0xD.73F272p+7,0x1.Cp-1,0x1.3p+1,(-0x1.Ap-1)},{0xF.4AD9E1p+74,(-0x3.5p+1),0xF.4AD9E1p+74,0xC.C1CE5Ep-70},{0xC.C1CE5Ep-70,0x1.6p+1,0x6.0C76E9p-73,(-0x4.9p-1)},{0x6.50B1A3p-41,0xB.EA524Ap-4,0x1.5p-1,0x1.6p+1},{0x1.8p+1,0x8.6p-1,0x1.5p-1,0xC.7C2B67p+40},{0x6.50B1A3p-41,0xF.140ADAp-18,0x6.0C76E9p-73,0xA.5667B4p+80},{0xC.C1CE5Ep-70,0x6.50B1A3p-41,0xF.4AD9E1p+74,0x5.6C3F60p-21},{0xF.4AD9E1p+74,0x5.6C3F60p-21,0x1.3p+1,0xD.BE5CFCp+59}},{{0xD.73F272p+7,0x5.A5CA35p-56,0xA.C9A78Ap+30,0x1.8p+1},{(-0x3.5p+1),0xA.5667B4p+80,0x1.BD2CD5p+33,0x8.C75E9Dp+94},{0x1.Fp-1,(-0x1.5p+1),0xA.5667B4p+80,0xA.C9A78Ap+30},{0x1.B16DC8p-91,0x5.A9F13Dp+2,0xD.73F272p+7,0xD.73F272p+7},{0xC.7C2B67p+40,0xC.7C2B67p+40,0x8.C75E9Dp+94,0xF.8F7756p-20},{(-0x1.4p-1),0x0.C4DB65p+93,(-0x1.9p+1),(-0x8.Fp+1)},{0x5.A5CA35p-56,0x1.3p+1,0x5.A9F13Dp+2,(-0x1.9p+1)},{(-0x1.5p+1),0x1.3p+1,0x1.B16DC8p-91,(-0x1.9p+1)}},{{0x5.A5CA35p-56,0xD.73F272p+7,0x8.1F342Ap+67,0x1.Cp-1},{0x8.6p-1,(-0x7.9p-1),(-0x1.7p+1),0x1.3p+1},{0xA.C9A78Ap+30,0x8.A25822p-16,0xD.2F76DEp-88,(-0x4.9p-1)},{0xF.4AD9E1p+74,0x0.38503Fp-26,0x5.A9F13Dp+2,(-0x1.Ap-1)},{0xD.2F76DEp-88,0x6.50B1A3p-41,0xD.73F272p+7,0x1.6p+1},{0x8.42E90Fp-71,0xD.BE5CFCp+59,(-0x1.9p+1),(-0x1.7p+1)},{(-0x4.9p-1),0x5.A9F13Dp+2,0x1.8p+1,0x5.A9F13Dp+2},{0xD.73F272p+7,0x1.8p+1,(-0x1.4p-1),0x6.50B1A3p-41}},{{(-0x1.7p+1),0xB.EA524Ap-4,(-0x1.5p+1),(-0x7.9p-1)},{0x1.Cp-1,(-0x1.4p-1),0x0.38503Fp-26,0x4.Fp-1},{0x1.Cp-1,0x0.C4DB65p+93,(-0x1.5p+1),0xC.7C2B67p+40},{(-0x1.7p+1),0x4.Fp-1,(-0x1.4p-1),0x1.5p-1},{0xD.73F272p+7,0xF.7187EFp+89,0x1.8p+1,0x8.42E90Fp-71},{(-0x4.9p-1),0xF.4AD9E1p+74,(-0x1.9p+1),0xD.73F272p+7},{0x8.42E90Fp-71,0x8.6p-1,0xD.73F272p+7,0xC.C1CE5Ep-70},{0xD.2F76DEp-88,0x5.6C3F60p-21,0x5.A9F13Dp+2,0x5.B06485p-74}},{{0xF.4AD9E1p+74,0xF.140ADAp-18,0xD.2F76DEp-88,0x8.1F342Ap+67},{0xA.C9A78Ap+30,(-0x1.7p+1),(-0x1.7p+1),0xA.C9A78Ap+30},{0x8.6p-1,(-0x1.9p+1),0x8.1F342Ap+67,0xF.140ADAp-18},{0x5.A5CA35p-56,0xC.C1CE5Ep-70,0x6.0C76E9p-73,0x0.C4DB65p+93},{0x0.38503Fp-26,0x1.5p-1,0x8.A25822p-16,0x0.C4DB65p+93},{0xD.BE5CFCp+59,0xC.C1CE5Ep-70,0x1.B16DC8p-91,0xF.140ADAp-18},{0x5.B06485p-74,(-0x1.9p+1),(-0x1.Ap-1),0xA.C9A78Ap+30},{(-0x7.9p-1),(-0x1.7p+1),0x1.3p+1,0x8.1F342Ap+67}},{{0x6.0C76E9p-73,0xF.140ADAp-18,0x6.50B1A3p-41,0x5.B06485p-74},{(-0x1.5p+1),0x5.6C3F60p-21,0x8.C75E9Dp+94,0xC.C1CE5Ep-70},{0xF.7187EFp+89,0x8.6p-1,(-0x4.9p-1),0xD.73F272p+7},{0x1.3p+1,0xF.4AD9E1p+74,0x5.A5CA35p-56,0x8.42E90Fp-71},{(-0x8.Fp+1),0xF.7187EFp+89,(-0x8.Fp+1),0x1.5p-1},{0x1.5p-1,0x4.Fp-1,0xC.C1CE5Ep-70,0xC.7C2B67p+40},{0x1.8p+1,0x0.C4DB65p+93,0xA.5667B4p+80,0x4.Fp-1},{0x1.6p+1,(-0x1.4p-1),0xA.5667B4p+80,(-0x7.9p-1)}},{{0x1.8p+1,0xB.EA524Ap-4,0xC.C1CE5Ep-70,0x6.50B1A3p-41},{0x1.5p-1,0x1.8p+1,(-0x8.Fp+1),0x5.A9F13Dp+2},{(-0x8.Fp+1),0x5.A9F13Dp+2,0x5.A5CA35p-56,(-0x1.7p+1)},{0x1.3p+1,0xD.BE5CFCp+59,(-0x4.9p-1),0x1.6p+1},{0xF.7187EFp+89,0x6.50B1A3p-41,0x8.C75E9Dp+94,(-0x1.Ap-1)},{(-0x1.5p+1),0x0.38503Fp-26,0x6.50B1A3p-41,(-0x4.9p-1)},{0x6.0C76E9p-73,0x8.A25822p-16,0x1.3p+1,0x1.3p+1},{(-0x7.9p-1),(-0x7.9p-1),(-0x1.Ap-1),0x1.Cp-1}}};
        int32_t *l_1130 = &l_811[0][0];
        int32_t *l_1131[8] = {&g_663,&g_663,&g_663,&g_663,&g_663,&g_663,&g_663,&g_663};
        int32_t l_1133 = 0x1706D0B7L;
        float ** const *l_1194 = &l_713;
        uint16_t ***l_1196 = &l_1093;
        uint16_t ****l_1195 = &l_1196;
        uint8_t l_1225 = 1UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_720[i] = 0x5630D580L;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 4; j++)
                l_811[i][j] = 1L;
        }
        for (g_111 = 0; (g_111 != (-6)); g_111--)
        { /* block id: 398 */
            int32_t *l_717 = &g_143;
            int32_t *l_718 = &g_663;
            int32_t *l_719[6][1][1] = {{{&g_481[7]}},{{&l_708}},{{&g_481[7]}},{{&g_481[7]}},{{&l_708}},{{&g_481[7]}}};
            int64_t l_750 = 0x5FBE0D60856C9985LL;
            const uint16_t *l_760 = &g_761;
            const uint16_t **l_759 = &l_760;
            const uint16_t ***l_758[9][8][3] = {{{&l_759,&l_759,&l_759},{&l_759,(void*)0,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759}},{{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,&l_759,&l_759},{&l_759,(void*)0,&l_759},{&l_759,&l_759,(void*)0}}};
            const uint16_t ****l_757 = &l_758[0][6][2];
            float l_799 = (-0x1.7p+1);
            int32_t l_847 = 1L;
            float l_848 = 0x0.Bp-1;
            uint32_t l_849 = 0x3CE33FBCL;
            uint32_t l_856[1][9][3] = {{{4294967295UL,0xEEC4C69AL,0x451C384EL},{0x842A2123L,0x842A2123L,0x451C384EL},{0xEEC4C69AL,4294967295UL,0xDBE13E18L},{0xB7B6DFF6L,0x842A2123L,0xB7B6DFF6L},{0xB7B6DFF6L,0xEEC4C69AL,0x842A2123L},{0xEEC4C69AL,0xB7B6DFF6L,0xB7B6DFF6L},{0x842A2123L,0xB7B6DFF6L,0xDBE13E18L},{4294967295UL,0xEEC4C69AL,0x451C384EL},{0x842A2123L,0x842A2123L,0x451C384EL}}};
            int8_t ****l_1039[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t l_1048 = 4UL;
            struct S0 *l_1090 = &g_400;
            uint64_t l_1095[1];
            uint8_t l_1114 = 0xB2L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1095[i] = 0xC3B699F90B722775LL;
            --l_720[2];
            for (g_213 = 4; (g_213 == (-8)); g_213 = safe_sub_func_uint16_t_u_u(g_213, 1))
            { /* block id: 402 */
                uint16_t ***l_753 = &g_584;
                uint16_t ****l_752 = &l_753;
                float l_766 = 0x3.79629Ap+76;
                int32_t l_785 = 8L;
                uint32_t l_795 = 4294967294UL;
                int32_t l_804 = 0xCC983134L;
                int32_t l_809 = 0x96E70789L;
                int32_t l_844 = 0x4A605273L;
                int32_t l_845 = 2L;
                int32_t l_846[6][5] = {{(-1L),0xD3CB295DL,(-1L),0xD3CB295DL,(-1L)},{0xABEED837L,0xABEED837L,0xABEED837L,0xABEED837L,0xABEED837L},{(-1L),0xD3CB295DL,(-1L),0xD3CB295DL,(-1L)},{0xABEED837L,0xABEED837L,0xABEED837L,0xABEED837L,0xABEED837L},{(-1L),0xD3CB295DL,(-1L),0xD3CB295DL,(-1L)},{0xABEED837L,0xABEED837L,0xABEED837L,0xABEED837L,0xABEED837L}};
                int16_t *l_855 = (void*)0;
                int32_t l_877 = 0L;
                uint8_t *l_939 = (void*)0;
                uint8_t *l_940 = &g_134;
                int32_t **l_945 = &l_718;
                uint8_t *l_947 = &g_342[0][0];
                int i, j;
            }
            if ((*g_775))
            { /* block id: 475 */
                int32_t *l_963 = &l_767;
                int32_t l_965 = (-3L);
                struct S0 *l_969 = &g_699[1][0][1];
                uint64_t *l_1022[7][8][1] = {{{&g_233},{&g_233},{&g_485},{&g_485},{&g_233},{&g_233},{&g_233},{&g_233}},{{&g_485},{&g_485},{&g_233},{&g_233},{&g_233},{&g_485},{&g_485},{&g_233}},{{&g_233},{&g_233},{&g_233},{&g_485},{&g_485},{&g_233},{&g_233},{&g_233}},{{&g_485},{&g_485},{&g_233},{&g_233},{&g_233},{&g_233},{&g_485},{&g_485}},{{&g_233},{&g_233},{&g_233},{&g_485},{&g_485},{&g_233},{&g_233},{&g_233}},{{&g_233},{&g_485},{&g_485},{&g_233},{&g_233},{&g_233},{&g_485},{&g_485}},{{&g_233},{&g_233},{&g_233},{&g_233},{&g_485},{&g_485},{&g_233},{&g_233}}};
                uint16_t * const * const ***l_1035 = (void*)0;
                int i, j, k;
                for (g_87 = 0; (g_87 == 27); ++g_87)
                { /* block id: 478 */
                    const int32_t **l_953 = &g_775;
                    (*l_953) = (*g_774);
                }
                if (((*g_775) | g_699[1][0][1].f5))
                { /* block id: 481 */
                    int32_t l_962 = 9L;
                    int64_t l_964 = 0x400D3A6DED8933AALL;
                    int64_t **l_975 = &g_278[3];
                    if ((255UL > (safe_sub_func_uint64_t_u_u((safe_mod_func_int32_t_s_s((!(safe_mul_func_int8_t_s_s(p_13, (+(-1L))))), l_946)), ((void*)0 != &g_485)))))
                    { /* block id: 482 */
                        uint32_t l_966 = 0x826743DBL;
                        l_718 = (l_962 , l_963);
                        --l_966;
                        (*l_928) = l_969;
                    }
                    else
                    { /* block id: 486 */
                        const int32_t l_979 = 1L;
                        (**l_713) = (p_16 > (((safe_add_func_uint16_t_u_u(65530UL, (safe_mod_func_int64_t_s_s((((l_964 || ((safe_unary_minus_func_int8_t_s(((p_15 && (((((((p_14 > ((((**l_928) , l_975) != l_976[0]) || (safe_mul_func_int8_t_s_s(0xB3L, l_979)))) != p_15) >= p_16) >= 1L) >= g_481[1]) ^ l_964) && g_58.f6)) > 0x7AL))) != l_917)) | l_811[0][0]) != p_16), p_13)))) , (**g_284)) > p_15));
                        (**g_609) = (safe_div_func_float_f_f((safe_mul_func_float_f_f(((void*)0 != l_719[1][0][0]), (-0x7.2p-1))), (!(*l_963))));
                    }
                }
                else
                { /* block id: 490 */
                    uint8_t l_1004 = 251UL;
                    for (g_74 = 0; (g_74 <= 1); g_74 += 1)
                    { /* block id: 493 */
                        struct S0 **l_1009 = &g_698;
                        const uint32_t l_1010 = 1UL;
                        l_810 = (((void*)0 != &g_249[0][6][5]) && (safe_lshift_func_int8_t_s_u((l_811[0][3] &= (safe_rshift_func_uint16_t_u_s((p_14 ^ ((safe_mod_func_int64_t_s_s(p_16, (safe_lshift_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((safe_sub_func_int16_t_s_s(((safe_div_func_uint64_t_u_u(((((safe_mul_func_int16_t_s_s(p_14, ((safe_mul_func_int16_t_s_s(((!(0x43F9L & l_1004)) > (safe_sub_func_int32_t_s_s(((((*l_700) = &g_698) != ((safe_div_func_int32_t_s_s(((((((p_13 ^ 0x8C4AL) <= 0x1F460B3AL) || 1UL) | p_13) > 0xDEL) | p_14), l_1004)) , l_1009)) != p_16), g_452.f0))), g_761)) && 0UL))) && l_767) , l_1010) >= (*l_858)), l_1010)) && l_1004), p_13)), 7)), l_1010)))) && 0x7D9883F0D06C8C01LL)), 5))), g_11)));
                    }
                }
                if (((((safe_rshift_func_int8_t_s_s((((safe_div_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((0x6DC5B83A9F2094D2LL > (((safe_div_func_int8_t_s_s(p_15, (((((*l_963) = ((!((((0x21DBA937L <= ((*l_717) = (0x64F7L == l_808))) & (safe_sub_func_int32_t_s_s(((((*g_609) != (*l_713)) , (1L > (((*l_712) = (*l_858)) || 4294967290UL))) > 0x1BD93F7AL), g_213))) != p_16) , (-6L))) != l_917)) || p_14) != g_699[1][0][1].f3) && p_15))) < g_109) < (*l_858))), 0x6DC0L)), 0xA7CFL)) < p_13) & p_14), 4)) >= p_13) <= p_16) , 0xB90428D1L))
                { /* block id: 502 */
                    uint16_t ***l_1034[7];
                    uint16_t ****l_1033 = &l_1034[1];
                    uint16_t *****l_1032 = &l_1033;
                    struct S0 ***l_1037[4][6][8] = {{{&g_697,&g_697,(void*)0,(void*)0,&g_697,(void*)0,(void*)0,&g_697},{&g_697,&g_697,&g_697,(void*)0,(void*)0,&g_697,(void*)0,&g_697},{(void*)0,&g_697,&g_697,(void*)0,&g_697,&g_697,(void*)0,&g_697},{&g_697,(void*)0,&g_697,&g_697,(void*)0,&g_697,(void*)0,&g_697},{(void*)0,&g_697,(void*)0,&g_697,(void*)0,&g_697,&g_697,(void*)0},{&g_697,&g_697,(void*)0,&g_697,&g_697,(void*)0,&g_697,&g_697}},{{(void*)0,&g_697,(void*)0,&g_697,(void*)0,(void*)0,&g_697,&g_697},{&g_697,&g_697,(void*)0,&g_697,&g_697,&g_697,&g_697,&g_697},{&g_697,(void*)0,(void*)0,&g_697,&g_697,(void*)0,(void*)0,&g_697},{(void*)0,&g_697,&g_697,&g_697,&g_697,&g_697,(void*)0,(void*)0},{(void*)0,&g_697,(void*)0,(void*)0,&g_697,&g_697,&g_697,&g_697},{&g_697,&g_697,&g_697,&g_697,&g_697,(void*)0,(void*)0,&g_697}},{{(void*)0,(void*)0,(void*)0,&g_697,&g_697,&g_697,&g_697,&g_697},{(void*)0,&g_697,(void*)0,(void*)0,&g_697,&g_697,(void*)0,(void*)0},{&g_697,&g_697,&g_697,&g_697,&g_697,&g_697,&g_697,&g_697},{&g_697,(void*)0,(void*)0,&g_697,&g_697,(void*)0,(void*)0,&g_697},{(void*)0,&g_697,&g_697,&g_697,&g_697,&g_697,(void*)0,(void*)0},{(void*)0,&g_697,(void*)0,(void*)0,&g_697,&g_697,&g_697,&g_697}},{{&g_697,&g_697,&g_697,&g_697,&g_697,(void*)0,(void*)0,&g_697},{(void*)0,(void*)0,(void*)0,&g_697,&g_697,&g_697,&g_697,&g_697},{(void*)0,&g_697,(void*)0,(void*)0,&g_697,&g_697,(void*)0,(void*)0},{&g_697,&g_697,&g_697,&g_697,&g_697,&g_697,&g_697,&g_697},{&g_697,(void*)0,(void*)0,&g_697,&g_697,(void*)0,(void*)0,&g_697},{(void*)0,&g_697,&g_697,&g_697,&g_697,&g_697,(void*)0,(void*)0}}};
                    struct S0 ****l_1036[10] = {&l_700,(void*)0,&l_1037[0][0][3],(void*)0,&l_700,&l_700,(void*)0,&l_1037[0][0][3],(void*)0,&l_700};
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_1034[i] = &g_584;
                    (*g_610) = (safe_sub_func_float_f_f(((p_15 > (**g_609)) == ((safe_add_func_float_f_f(0x7.9p-1, (safe_div_func_float_f_f(((((((safe_sub_func_uint32_t_u_u(0xFDC9597CL, ((l_1031[4] & 1UL) ^ 4UL))) != ((l_1032 == l_1035) > (*l_858))) && (*l_858)) , (*g_698)) , p_16) , p_13), p_15)))) >= p_15)), 0x7.ECB87Fp-25));
                    (*l_858) = (((l_700 == (g_1038 = &g_697)) , l_1039[0]) != (void*)0);
                }
                else
                { /* block id: 506 */
                    (*l_717) &= (*l_963);
                }
                for (g_690 = 0; (g_690 == 5); g_690 = safe_add_func_uint16_t_u_u(g_690, 5))
                { /* block id: 511 */
                    int32_t **l_1045 = &l_719[5][0][0];
                    int32_t **l_1046 = &l_717;
                    int32_t l_1047 = 0x9A05DDC8L;
                    (*l_718) &= (-3L);
                    for (l_800 = (-1); (l_800 == 27); l_800 = safe_add_func_int64_t_s_s(l_800, 7))
                    { /* block id: 515 */
                        int32_t **l_1044 = &l_719[0][0][0];
                        if (p_16)
                            break;
                        (*l_718) |= l_811[0][0];
                        (*l_1044) = &l_806[6][2][1];
                    }
                    (*l_1046) = ((*l_1045) = &l_965);
                    ++l_1048;
                }
            }
            else
            { /* block id: 524 */
                int32_t l_1055 = 0xA383FEA7L;
                int32_t l_1061 = (-5L);
                int16_t *l_1064[7] = {&g_11,&g_11,&g_11,&g_11,&g_11,&g_11,&g_11};
                int32_t *l_1073[7][3][8] = {{{&g_663,&g_143,&l_708,(void*)0,&l_801,&l_798,&l_800,(void*)0},{&l_811[0][0],&l_811[0][3],&l_798,(void*)0,&g_663,(void*)0,&l_798,&l_811[0][3]},{&l_800,&g_663,&l_806[8][3][1],&l_811[0][0],(void*)0,(void*)0,&l_811[0][0],&l_811[0][0]}},{{&l_811[0][0],&g_663,&l_811[0][3],&l_806[8][3][1],&l_800,&l_811[0][0],&l_811[0][0],&l_800},{(void*)0,&l_806[8][3][1],&l_806[8][3][1],(void*)0,&l_811[0][3],(void*)0,&l_798,&l_810},{&l_811[0][3],&l_708,(void*)0,&l_801,&l_798,&l_800,(void*)0,&l_810}},{{&g_663,&l_708,&l_811[0][3],&l_811[0][0],&l_811[0][3],&l_708,&g_663,(void*)0},{&l_811[0][0],&g_663,(void*)0,&l_708,&g_663,(void*)0,(void*)0,&l_806[8][3][1]},{&l_806[8][3][1],&l_810,&l_811[0][0],&l_798,&g_663,&g_663,&l_798,&l_811[0][0]}},{{&l_811[0][0],&l_811[0][0],&l_800,&l_806[8][3][1],&l_811[0][3],&g_663,&l_811[0][0],(void*)0},{&g_663,&g_143,&l_811[0][0],&l_811[0][3],&l_798,(void*)0,&g_663,(void*)0},{&g_143,&l_806[8][3][1],&g_663,&l_806[8][3][1],&g_143,&l_801,&l_800,&l_811[0][0]}},{{&g_663,(void*)0,&l_810,&l_798,(void*)0,&l_811[0][3],(void*)0,&l_806[8][3][1]},{(void*)0,&l_800,&l_810,&l_708,&l_708,&l_810,&l_800,(void*)0},{(void*)0,&g_663,&g_663,&l_811[0][0],&l_811[0][0],(void*)0,&g_663,&l_810}},{{(void*)0,&l_798,&l_811[0][0],&l_801,&l_811[0][0],(void*)0,&l_811[0][0],&l_801},{&l_800,&g_663,&l_800,&g_663,(void*)0,&l_810,&l_798,(void*)0},{&l_801,&l_800,&l_811[0][0],&g_663,(void*)0,&l_811[0][3],(void*)0,(void*)0}},{{&l_801,(void*)0,(void*)0,(void*)0,(void*)0,&l_801,&g_663,&g_143},{&l_800,&l_806[8][3][1],&l_811[0][3],&g_663,&l_811[0][0],(void*)0,(void*)0,&l_708},{(void*)0,&g_143,(void*)0,&g_663,&l_811[0][0],&g_663,(void*)0,&g_143}}};
                struct S0 *l_1089 = &g_400;
                int i, j, k;
                l_1061 = (((0x4.2C36A0p+22 == 0x7.Cp-1) != 0xD.D8F0A7p-50) >= ((p_14 , (safe_add_func_float_f_f(((**l_713) = (-0x3.4p+1)), ((safe_add_func_float_f_f(l_1055, ((**g_284) = ((p_16 != ((*g_113) > (safe_sub_func_float_f_f((safe_mul_func_float_f_f(((p_16 , 0x9.2009F6p-58) == p_16), p_15)), p_13)))) != (*l_858))))) != g_1060)))) < l_768));
                if (((safe_sub_func_uint8_t_u_u(0UL, (((g_213 |= (-1L)) || ((safe_add_func_uint16_t_u_u((g_233 == g_1069), ((*l_717) = ((safe_add_func_uint64_t_u_u(1UL, p_13)) ^ (*l_858))))) < ((0x24D9CB689FD2EF6FLL >= (p_13 || 18446744073709551606UL)) != l_1072[3][3][0]))) <= 0xC995L))) != p_13))
                { /* block id: 530 */
                    uint16_t l_1092 = 1UL;
                    uint8_t *l_1094 = &g_342[3][2];
                    uint64_t *l_1109 = &l_1095[0];
                    l_1073[4][0][3] = &l_1061;
                    (*l_858) = (safe_sub_func_uint32_t_u_u(((*l_712) &= ((safe_unary_minus_func_int64_t_s((*l_858))) | (safe_add_func_uint8_t_u_u(p_14, (((((((((*l_929) , ((0x1F74EE21L <= 4294967295UL) <= (((safe_sub_func_uint8_t_u_u((((*l_1094) = (safe_add_func_int8_t_s_s(((safe_div_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_s((l_1089 == (l_1091 = l_1090)), 12)) && ((((l_1092 , l_1093) != (void*)0) <= p_16) != p_13)), p_14)), l_768)) <= p_15), p_15))) > g_699[1][0][1].f5), p_13)) , g_400.f7) < p_16))) < 0x13D11F0507D30956LL) , l_1092) , 0x94L) >= l_1095[0]) , (void*)0) != (void*)0) , p_13))))), p_14));
                    l_810 &= (((g_213 = (safe_mod_func_int32_t_s_s(((((safe_add_func_int8_t_s_s((((safe_div_func_uint64_t_u_u(g_1102, (safe_mul_func_uint8_t_u_u(g_646.f1, p_14)))) || (!(((((*l_858) = (*l_718)) ^ p_15) >= (safe_mul_func_uint8_t_u_u((((((254UL != ((+((*l_1109)--)) >= (safe_lshift_func_uint8_t_u_s(l_1114, 7)))) | (safe_unary_minus_func_int64_t_s(((safe_add_func_int32_t_s_s((((p_15 , ((safe_mul_func_uint16_t_u_u((safe_add_func_int64_t_s_s((safe_mod_func_int64_t_s_s((!(safe_lshift_func_int16_t_s_s(0L, p_14))), 18446744073709551615UL)), p_15)), (*l_717))) != 0UL)) & p_16) || 1L), l_1127)) != p_16)))) >= g_58.f3) , g_485) , p_15), 1L))) ^ g_646.f5))) || p_14), p_13)) ^ l_1129[6][7]) || 1UL) >= (-6L)), p_14))) && l_1092) ^ (-3L));
                }
                else
                { /* block id: 540 */
                    return p_14;
                }
            }
        }
lbl_1229:
        l_1135++;
        for (g_314 = 0; (g_314 <= 60); g_314++)
        { /* block id: 548 */
            int8_t * const ***l_1159 = &l_1156;
            int8_t ****l_1165 = &g_865[0][8];
            uint16_t l_1179 = 0x0ABCL;
            int32_t l_1228 = (-1L);
            for (g_143 = 0; g_143 < 2; g_143 += 1)
            {
                for (g_87 = 0; g_87 < 9; g_87 += 1)
                {
                    for (g_229 = 0; g_229 < 6; g_229 += 1)
                    {
                        g_249[g_143][g_87][g_229] = 0xFD23L;
                    }
                }
            }
            for (g_229 = 29; (g_229 == (-4)); --g_229)
            { /* block id: 552 */
                int8_t * const ****l_1158 = (void*)0;
                int32_t l_1162 = 8L;
                uint8_t *l_1163 = &g_342[5][2];
                int8_t *****l_1166 = (void*)0;
                int8_t *****l_1167 = (void*)0;
                int8_t *****l_1168 = &l_1165;
                struct S0 *l_1209[9][7][4] = {{{&g_699[7][0][2],&g_147,(void*)0,&g_646},{&g_58,&g_699[7][0][0],&g_400,(void*)0},{(void*)0,&g_699[1][0][1],&g_699[0][0][0],&g_58},{&g_646,(void*)0,&g_699[3][0][3],&g_147},{&g_699[1][0][1],&g_147,&g_58,&g_699[1][0][1]},{&g_58,&g_58,&g_699[1][0][1],&g_699[1][0][1]},{&g_699[0][0][0],&g_699[1][0][1],&g_699[1][0][1],&g_699[3][0][3]}},{{&g_646,&g_699[2][0][1],&g_646,(void*)0},{&g_400,&g_646,(void*)0,&g_699[1][0][1]},{&g_147,&g_58,&g_400,(void*)0},{&g_699[3][0][3],&g_699[1][0][1],(void*)0,&g_699[1][0][1]},{(void*)0,(void*)0,&g_147,&g_147},{(void*)0,(void*)0,(void*)0,&g_147},{&g_699[4][0][3],&g_58,&g_699[0][0][2],&g_400}},{{&g_699[7][0][0],(void*)0,&g_699[7][0][3],&g_699[0][0][2]},{&g_58,(void*)0,&g_147,(void*)0},{&g_699[5][0][3],&g_699[5][0][1],&g_58,&g_699[1][0][1]},{&g_400,(void*)0,&g_400,(void*)0},{&g_58,(void*)0,&g_699[5][0][3],&g_400},{&g_699[4][0][3],&g_646,&g_400,&g_646},{&g_699[1][0][1],&g_699[1][0][1],(void*)0,(void*)0}},{{&g_699[1][0][1],&g_147,&g_699[7][0][3],(void*)0},{&g_699[1][0][1],&g_699[1][0][1],&g_646,(void*)0},{&g_699[3][0][3],&g_699[1][0][1],&g_147,&g_147},{&g_646,&g_699[0][0][0],&g_646,&g_646},{(void*)0,&g_699[1][0][1],&g_699[2][0][1],&g_58},{&g_147,&g_699[2][0][1],&g_699[5][0][1],(void*)0},{&g_646,&g_400,&g_646,(void*)0}},{{&g_147,&g_646,&g_699[1][0][1],&g_147},{&g_699[1][0][1],(void*)0,&g_699[1][0][1],&g_646},{&g_699[7][0][2],&g_646,&g_699[1][0][1],&g_699[1][0][1]},{&g_699[1][0][1],&g_699[1][0][1],&g_699[1][0][1],&g_699[5][0][1]},{&g_147,&g_699[7][0][3],&g_646,&g_147},{&g_646,&g_147,&g_699[5][0][1],&g_646},{&g_147,&g_699[7][0][0],&g_699[2][0][1],&g_147}},{{(void*)0,(void*)0,&g_646,&g_147},{&g_646,&g_699[1][0][1],&g_147,&g_699[7][0][0]},{&g_699[3][0][3],&g_147,&g_646,&g_400},{&g_699[1][0][1],&g_147,&g_699[7][0][3],&g_58},{&g_699[1][0][1],&g_699[1][0][1],(void*)0,&g_699[0][0][2]},{&g_699[1][0][1],(void*)0,&g_400,&g_646},{&g_699[4][0][3],&g_147,&g_699[5][0][3],(void*)0}},{{&g_58,&g_400,&g_400,&g_58},{&g_400,(void*)0,&g_58,&g_58},{&g_699[5][0][3],&g_147,&g_58,&g_147},{&g_699[0][0][0],(void*)0,&g_699[4][0][3],&g_147},{&g_646,&g_147,&g_147,&g_58},{&g_147,(void*)0,&g_147,&g_58},{(void*)0,&g_400,(void*)0,(void*)0}},{{&g_646,&g_147,(void*)0,&g_646},{(void*)0,(void*)0,&g_58,&g_699[0][0][2]},{&g_699[7][0][3],&g_699[1][0][1],(void*)0,&g_58},{&g_58,&g_147,&g_699[1][0][1],&g_400},{&g_699[1][0][1],&g_147,(void*)0,&g_699[7][0][0]},{&g_699[7][0][0],&g_699[1][0][1],&g_147,&g_147},{&g_699[5][0][1],(void*)0,&g_699[1][0][1],&g_147}},{{&g_699[1][0][1],&g_699[7][0][0],(void*)0,&g_646},{&g_58,&g_147,&g_699[7][0][0],&g_147},{&g_400,&g_699[7][0][3],&g_58,&g_699[5][0][1]},{&g_699[1][0][1],&g_699[1][0][1],(void*)0,&g_699[1][0][1]},{&g_699[0][0][2],&g_646,&g_58,&g_646},{&g_699[0][0][2],(void*)0,(void*)0,&g_147},{&g_699[1][0][1],&g_646,&g_58,(void*)0}}};
                int i, j, k;
                for (l_915 = 7; (l_915 <= 16); l_915 = safe_add_func_int16_t_s_s(l_915, 2))
                { /* block id: 555 */
                    (*l_858) &= 0xF019273AL;
                }
                if ((((!(g_74 , p_13)) ^ (safe_add_func_int16_t_s_s((safe_add_func_uint8_t_u_u((safe_lshift_func_int8_t_s_u(((safe_mod_func_int32_t_s_s((*l_858), p_13)) , ((safe_mod_func_uint32_t_u_u(((l_1159 = l_1155) == ((*l_1168) = (((safe_lshift_func_int16_t_s_u(l_1162, 0)) ^ (((*l_1163) ^= p_14) > (!(*l_1130)))) , l_1165))), (*g_775))) != p_13)), 6)), p_14)), (*l_1130)))) & 1L))
                { /* block id: 561 */
                    float l_1183 = 0xD.1A213Cp+82;
                    uint16_t l_1198 = 0x0FA6L;
                    int32_t * const l_1210[5][10] = {{&l_801,&g_481[7],(void*)0,&l_801,&l_801,(void*)0,(void*)0,(void*)0,&l_801,&l_801},{(void*)0,(void*)0,(void*)0,&l_801,&l_801,(void*)0,(void*)0,(void*)0,&l_801,&l_801},{(void*)0,(void*)0,(void*)0,&l_801,&l_801,(void*)0,(void*)0,(void*)0,&l_801,&l_801},{(void*)0,(void*)0,(void*)0,&l_801,&l_801,(void*)0,(void*)0,(void*)0,&l_801,&l_801},{(void*)0,(void*)0,(void*)0,&l_801,&l_801,(void*)0,(void*)0,(void*)0,&l_801,&l_801}};
                    int i, j;
                    if ((8L < (((safe_rshift_func_uint16_t_u_u(p_16, 9)) < ((safe_sub_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s((g_342[0][0] &= ((safe_sub_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u((((p_16 , ((*l_1130) > p_14)) & l_1179) , ((void*)0 != g_1180)), p_13)), p_13)) <= l_1179)), p_13)), 0x2A6DL)) <= p_16)) , (-1L))))
                    { /* block id: 563 */
                        int16_t *l_1197 = &g_213;
                        struct S0 *l_1208[8][1][2];
                        int i, j, k;
                        for (i = 0; i < 8; i++)
                        {
                            for (j = 0; j < 1; j++)
                            {
                                for (k = 0; k < 2; k++)
                                    l_1208[i][j][k] = &g_147;
                            }
                        }
                        (*l_1130) = ((safe_div_func_int8_t_s_s((l_1198 = ((safe_sub_func_uint8_t_u_u((((*l_1163) = (safe_mul_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((safe_add_func_int64_t_s_s((p_14 , (l_1194 == ((0xA9E9L == (((l_1195 != &l_1196) & (((*l_858) > ((*l_1197) = 0x6981L)) <= (g_74 != ((*l_712) = (0UL == 0xF9L))))) > p_15)) , &g_609))), p_16)), l_1162)) , p_16), 0xBB89L))) || 0UL), 255UL)) , (-1L))), 0x8EL)) >= 0x2DB5039AL);
                        if ((**g_774))
                            continue;
                        (*l_858) = (!((p_13 != p_13) < (safe_mul_func_int16_t_s_s(((((safe_rshift_func_uint8_t_u_s(((void*)0 != &g_690), 7)) , (((l_1162 , 0UL) > ((*l_858) | (((safe_mod_func_int32_t_s_s((((safe_add_func_int32_t_s_s(0xDFFE55C8L, 0UL)) && (*l_1130)) > (*l_858)), 0x4176B8A2L)) , l_1208[4][0][1]) == l_1209[6][4][3]))) , (-1L))) != p_15) <= g_55.f0), (***g_1182)))));
                    }
                    else
                    { /* block id: 571 */
                        int32_t **l_1211 = &l_1130;
                        (*l_1211) = l_1210[3][4];
                        return p_14;
                    }
                }
                else
                { /* block id: 575 */
                    const int32_t l_1212 = 1L;
                    uint64_t *l_1223 = &g_233;
                    uint64_t *l_1224 = &g_485;
                    uint16_t *l_1227 = (void*)0;
                    l_1228 = ((l_1212 < ((0xA2DE093BL | l_1213) , (safe_div_func_uint32_t_u_u(l_1212, (safe_div_func_uint32_t_u_u((((g_1226 = ((-7L) >= ((+((safe_div_func_uint64_t_u_u(((***l_700) , (safe_add_func_int8_t_s_s((((void*)0 != &g_696) > ((((*l_1224) = ((*l_1223) = ((&g_1181 != &g_1181) ^ 0x9D63L))) & l_1225) | 0L)), l_1162))), p_13)) == 0x19EAL)) | 8UL))) , l_1227) != (void*)0), l_1162)))))) && g_134);
                }
            }
        }
        if (g_147.f1)
            goto lbl_1229;
    }
    else
    { /* block id: 584 */
        uint64_t *l_1230 = &g_485;
        int32_t l_1231 = 0x57920442L;
        int32_t *l_1232[3][5] = {{&g_481[7],&g_481[7],&g_481[1],&l_802,(void*)0},{(void*)0,&l_806[5][4][0],&l_806[5][4][0],(void*)0,&g_481[7]},{(void*)0,&l_802,&l_802,&l_802,&l_802}};
        int32_t l_1233[7] = {0xEFD4806BL,0xEFD4806BL,0xEFD4806BL,0xEFD4806BL,0xEFD4806BL,0xEFD4806BL,0xEFD4806BL};
        uint64_t l_1234 = 1UL;
        int i, j;
        l_1231 = (((-4L) == ((*l_858) &= (65531UL || (((*l_1230) &= ((void*)0 == &g_610)) | 0x5E95865486F2E033LL)))) && p_16);
        l_1234++;
        return p_16;
    }
    for (g_109 = 0; (g_109 <= 1); g_109 += 1)
    { /* block id: 593 */
        uint32_t l_1241 = 4294967287UL;
        uint32_t l_1257 = 0x2C382DE8L;
        int32_t l_1263[9][4] = {{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL},{0x0B48291CL,0x0B48291CL,0x0B48291CL,0x0B48291CL}};
        float l_1322 = 0x4.6F0CC0p+52;
        int i, j;
        for (g_229 = 0; (g_229 <= 1); g_229 += 1)
        { /* block id: 596 */
            struct S0 *l_1237[4][8] = {{&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147,&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147},{&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147,&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147},{&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147,&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147},{&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147,&g_699[3][0][0],&g_699[3][0][0],&g_147,&g_147}};
            int32_t l_1238 = (-5L);
            int32_t *l_1239 = &g_481[7];
            int32_t *l_1240[9];
            uint64_t l_1273 = 0x43EA9DCE422C9948LL;
            int i, j;
            for (i = 0; i < 9; i++)
                l_1240[i] = &g_663;
            (**l_700) = l_1237[1][7];
            l_1241--;
            for (l_1127 = 0; (l_1127 <= 1); l_1127 += 1)
            { /* block id: 601 */
                uint16_t **l_1254 = &g_130;
                uint64_t *l_1255 = (void*)0;
                uint64_t *l_1256[4][1] = {{&g_233},{&g_485},{&g_233},{&g_485}};
                int16_t *l_1262 = &l_948;
                int32_t l_1264 = 0x020DAA53L;
                int32_t l_1272[3];
                int i, j;
                for (i = 0; i < 3; i++)
                    l_1272[i] = 0xDAFF7718L;
            }
        }
        for (g_314 = 0; (g_314 <= 1); g_314 += 1)
        { /* block id: 644 */
            uint32_t l_1326 = 1UL;
            int32_t *l_1333 = &g_663;
            (*l_1333) &= ((((*l_858) = ((l_1326 = (((((p_15 & p_14) > (*l_858)) & (65534UL >= 0xB26CL)) > p_16) != p_13)) <= (safe_mod_func_int32_t_s_s(l_1329, (safe_mul_func_int16_t_s_s(0x4CD9L, (****g_1181))))))) == l_711) == g_1332);
        }
    }
    return p_14;
}


/* ------------------------------------------ */
/* 
 * reads : g_452.f3 g_314 g_58.f3 g_58.f2 g_400.f1 g_111 g_129 g_130 g_74 g_342 g_249 g_481 g_125 g_58.f4 g_584 g_58.f7 g_609 g_400.f0 g_392.f4 g_58.f0 g_284 g_113 g_114 g_610 g_646 g_11 g_662 g_663 g_233 g_452.f4 g_147.f1 g_109
 * writes: g_255 g_125 g_74 g_134 g_249 g_503 g_111 g_229 g_481 g_109 g_609 g_342 g_590 g_233
 */
static uint32_t  func_17(uint64_t  p_18, uint64_t  p_19, int64_t  p_20, uint64_t  p_21)
{ /* block id: 272 */
    int32_t **l_486 = &g_255[3][3][2];
    int32_t l_507 = 8L;
    float l_508[1][3];
    int32_t l_510 = (-1L);
    int32_t l_513 = 0xF0E37AB3L;
    int32_t l_516[6];
    uint8_t l_561 = 0x84L;
    uint8_t l_596 = 0x7EL;
    uint64_t l_631 = 0x77C744603B487E6CLL;
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
            l_508[i][j] = (-0x1.9p+1);
    }
    for (i = 0; i < 6; i++)
        l_516[i] = 0xA26F7BC8L;
lbl_588:
    (*l_486) = (void*)0;
    for (g_125 = 17; (g_125 > 11); --g_125)
    { /* block id: 276 */
        int16_t l_497 = 0x41CFL;
        int32_t l_504 = (-5L);
        int32_t l_509 = 0xC1698E56L;
        int32_t l_514[9][1][6] = {{{0L,0xCDF65E29L,(-6L),(-10L),0x9632C4CCL,(-10L)}},{{(-6L),0L,(-6L),0x68C1AAC7L,(-1L),0xE88373F0L}},{{(-4L),0x68C1AAC7L,(-10L),0xA36B9FD4L,0xBE9ACBB1L,0xBE9ACBB1L}},{{0xA36B9FD4L,0xBE9ACBB1L,0xBE9ACBB1L,0xA36B9FD4L,(-10L),0x68C1AAC7L}},{{(-4L),0xE88373F0L,(-1L),0x68C1AAC7L,(-6L),0L}},{{(-6L),(-10L),0x9632C4CCL,(-10L),(-6L),0xCDF65E29L}},{{0L,0xE88373F0L,(-1L),2L,(-10L),(-1L)}},{{(-1L),0xBE9ACBB1L,0xE88373F0L,0xE88373F0L,0xBE9ACBB1L,(-1L)}},{{2L,0xCDF65E29L,0x9632C4CCL,0xE88373F0L,(-10L),(-1L)}}};
        uint16_t l_560 = 5UL;
        int32_t *l_565[1][1];
        int64_t l_620 = 0xCD2618373BDAFC27LL;
        int8_t *l_636 = &g_229;
        uint32_t l_676 = 1UL;
        float l_679 = 0x1.Cp+1;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_565[i][j] = &l_509;
        }
        for (g_74 = 0; (g_74 != 12); ++g_74)
        { /* block id: 279 */
            int64_t l_498[9][6][4] = {{{0x8D9DD1932A829275LL,0xDEAD5DF2E0254A88LL,0xEBBC58088B17C481LL,0L},{0L,0x14F2DA64149788E6LL,0x58BD7F8BFDA9A8CDLL,0x58BD7F8BFDA9A8CDLL},{1L,1L,0L,0xEBBC58088B17C481LL},{0xD2AA3C124428605ELL,1L,0xD69C94B6C471C457LL,1L},{0xFA2DB4E2088B78B5LL,0L,0xDEAD5DF2E0254A88LL,0xD69C94B6C471C457LL},{0x296D78CCDA2D8EEELL,0L,0x6D0E1C11DC48B89DLL,1L}},{{0L,1L,0L,0xEBBC58088B17C481LL},{0xE84D8DF74B69C448LL,1L,0x81EBB9CAFF69778CLL,0x58BD7F8BFDA9A8CDLL},{0xD69C94B6C471C457LL,0x14F2DA64149788E6LL,0xD69C94B6C471C457LL,0L},{0xC8FD3134440B08DBLL,0xDEAD5DF2E0254A88LL,0xCAB6361537A81A6CLL,0xD69C94B6C471C457LL},{0x7B945B797B65C4D5LL,0xCAB6361537A81A6CLL,8L,0xDEAD5DF2E0254A88LL},{0L,1L,8L,0x6D0E1C11DC48B89DLL}},{{0x7B945B797B65C4D5LL,1L,0xCAB6361537A81A6CLL,0L},{0xC8FD3134440B08DBLL,0x7B7078473D50FF0DLL,0xD69C94B6C471C457LL,0x81EBB9CAFF69778CLL},{0xD69C94B6C471C457LL,0x81EBB9CAFF69778CLL,0x81EBB9CAFF69778CLL,0xD69C94B6C471C457LL},{0xE84D8DF74B69C448LL,1L,0L,0xCAB6361537A81A6CLL},{0L,0xFD62610EB170FAA9LL,0x6D0E1C11DC48B89DLL,8L},{0x296D78CCDA2D8EEELL,1L,0xDEAD5DF2E0254A88LL,8L}},{{0xFA2DB4E2088B78B5LL,0xFD62610EB170FAA9LL,0xD69C94B6C471C457LL,0xCAB6361537A81A6CLL},{0xD2AA3C124428605ELL,1L,0L,0xD69C94B6C471C457LL},{1L,0x81EBB9CAFF69778CLL,0x58BD7F8BFDA9A8CDLL,0x81EBB9CAFF69778CLL},{0L,0x7B7078473D50FF0DLL,0xEBBC58088B17C481LL,0L},{0x8D9DD1932A829275LL,1L,1L,0x6D0E1C11DC48B89DLL},{(-8L),1L,0xD69C94B6C471C457LL,0xDEAD5DF2E0254A88LL}},{{(-8L),0xCAB6361537A81A6CLL,1L,0xD69C94B6C471C457LL},{0x8D9DD1932A829275LL,0xDEAD5DF2E0254A88LL,0xEBBC58088B17C481LL,0L},{0L,0x14F2DA64149788E6LL,0x58BD7F8BFDA9A8CDLL,0x58BD7F8BFDA9A8CDLL},{1L,1L,0L,0xEBBC58088B17C481LL},{0xD2AA3C124428605ELL,1L,0xD69C94B6C471C457LL,1L},{0xFA2DB4E2088B78B5LL,0L,0xDEAD5DF2E0254A88LL,0xD69C94B6C471C457LL}},{{0x296D78CCDA2D8EEELL,0L,0x6D0E1C11DC48B89DLL,1L},{0L,1L,0L,0xEBBC58088B17C481LL},{0xE84D8DF74B69C448LL,1L,0x81EBB9CAFF69778CLL,0x58BD7F8BFDA9A8CDLL},{0xD69C94B6C471C457LL,0x14F2DA64149788E6LL,0xD69C94B6C471C457LL,0L},{0xC8FD3134440B08DBLL,0xDEAD5DF2E0254A88LL,0xCAB6361537A81A6CLL,0xD69C94B6C471C457LL},{0x7B945B797B65C4D5LL,0xCAB6361537A81A6CLL,8L,0xDEAD5DF2E0254A88LL}},{{0L,1L,8L,0x6D0E1C11DC48B89DLL},{0x7B945B797B65C4D5LL,1L,0xCAB6361537A81A6CLL,0L},{0xC8FD3134440B08DBLL,0x7B7078473D50FF0DLL,0xD69C94B6C471C457LL,0x81EBB9CAFF69778CLL},{0xD69C94B6C471C457LL,0x81EBB9CAFF69778CLL,0x81EBB9CAFF69778CLL,0xD69C94B6C471C457LL},{0xE84D8DF74B69C448LL,1L,0L,0xCAB6361537A81A6CLL},{0L,0xFD62610EB170FAA9LL,0x6D0E1C11DC48B89DLL,8L}},{{0x296D78CCDA2D8EEELL,1L,0xDEAD5DF2E0254A88LL,8L},{0xFA2DB4E2088B78B5LL,0xFD62610EB170FAA9LL,0xD69C94B6C471C457LL,0xCAB6361537A81A6CLL},{0xD2AA3C124428605ELL,1L,0L,0xD69C94B6C471C457LL},{1L,0x81EBB9CAFF69778CLL,0x58BD7F8BFDA9A8CDLL,0x81EBB9CAFF69778CLL},{0L,0xE84D8DF74B69C448LL,0xFD62610EB170FAA9LL,1L},{0x81EBB9CAFF69778CLL,0L,0xC8FD3134440B08DBLL,1L}},{{0xEBBC58088B17C481LL,0x296D78CCDA2D8EEELL,8L,0xD2AA3C124428605ELL},{0xEBBC58088B17C481LL,0xFA2DB4E2088B78B5LL,0xC8FD3134440B08DBLL,8L},{0x81EBB9CAFF69778CLL,0xD2AA3C124428605ELL,0xFD62610EB170FAA9LL,(-8L)},{(-8L),1L,0x14F2DA64149788E6LL,0x14F2DA64149788E6LL},{0L,0L,(-8L),0xFD62610EB170FAA9LL},{0x6D0E1C11DC48B89DLL,0x8D9DD1932A829275LL,8L,0xC8FD3134440B08DBLL}}};
            uint8_t *l_500 = &g_134;
            int16_t *l_501 = (void*)0;
            int16_t *l_502 = &g_249[1][6][3];
            int32_t l_511 = 0L;
            int32_t l_512 = 0x46CE4A80L;
            int32_t l_515 = 0xAAC8D55AL;
            int32_t l_517 = 0x71B8AF01L;
            uint32_t l_518[7] = {0x13B7AA85L,0x13B7AA85L,0x13B7AA85L,0x13B7AA85L,0x13B7AA85L,0x13B7AA85L,0x13B7AA85L};
            int32_t *l_553 = (void*)0;
            uint16_t **l_577 = &g_130;
            int i, j, k;
            if (((0x6DL && (((~((safe_add_func_uint64_t_u_u(6UL, (p_20 = (((safe_unary_minus_func_uint16_t_u((safe_lshift_func_uint8_t_u_u(l_497, g_452.f3)))) || (((((((l_498[7][5][0] ^ (((*l_500) = (+l_498[2][4][3])) == (((*l_502) = p_18) == (((((g_503 = ((l_498[7][5][0] | 0x0D7B228BFD3ABA04LL) , (void*)0)) != l_486) > l_504) && 0xF22048DDL) , l_497)))) && p_18) , g_314) != l_497) && 0x15L) < p_21) , g_58.f3)) & 0UL)))) , p_20)) | l_504) >= g_58.f2)) <= l_498[7][5][0]))
            { /* block id: 284 */
                int32_t *l_505 = &g_481[2];
                int32_t *l_506[10][8] = {{&g_143,&g_143,&g_481[7],&g_143,&g_143,&g_481[7],&g_143,&g_143},{&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143},{&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7]},{&g_143,&g_143,&g_481[7],&g_143,&g_143,&g_481[7],&g_143,&g_143},{&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143},{&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7]},{&g_143,&g_143,&g_481[7],&g_143,&g_143,&g_481[7],&g_143,&g_143},{&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143},{&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7],&g_481[7],&g_143,&g_481[7]},{&g_143,&g_143,&g_481[7],&g_143,&g_143,&g_481[7],&g_143,&g_143}};
                const int32_t l_525 = (-1L);
                int i, j;
                if (p_18)
                    break;
                --l_518[1];
                for (g_111 = 0; (g_111 == (-28)); g_111 = safe_sub_func_int16_t_s_s(g_111, 7))
                { /* block id: 289 */
                    uint16_t l_536 = 0xBEE5L;
                    uint8_t *l_541 = &g_134;
                    int64_t *l_554 = (void*)0;
                    int64_t *l_555 = &l_498[7][5][0];
                    int8_t *l_556 = &g_229;
                    int8_t *l_557[1];
                    uint64_t *l_558[10] = {&g_485,(void*)0,&g_485,(void*)0,&g_485,(void*)0,&g_485,(void*)0,&g_485,(void*)0};
                    const int32_t l_559 = 1L;
                    int32_t l_562 = 1L;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_557[i] = &g_109;
                    if ((p_18 ^ ((safe_rshift_func_int8_t_s_u(l_525, 1)) , (safe_div_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((l_562 = ((g_400.f1 <= (safe_add_func_uint8_t_u_u((safe_div_func_uint64_t_u_u((++l_536), (((safe_add_func_uint32_t_u_u((((void*)0 == l_541) >= (safe_div_func_int32_t_s_s((safe_lshift_func_uint16_t_u_s(((safe_mod_func_int8_t_s_s((l_560 = ((*l_505) = (g_111 ^ (p_19 & (+((p_21 = (((safe_sub_func_int64_t_s_s(((((l_509 = ((*l_556) = (safe_rshift_func_int16_t_s_u((((*l_555) = (((void*)0 == l_553) >= p_18)) , p_20), p_19)))) & p_18) > (**g_129)) <= 0x3DL), l_504)) && 0xBA64L) == p_19)) == l_559)))))), l_561)) <= l_511), g_342[1][2])), p_19))), p_19)) , (-1L)) , l_509))), p_20))) > l_497)), p_18)), g_58.f3)), g_249[0][4][3])))))
                    { /* block id: 298 */
                        int32_t **l_563 = (void*)0;
                        int32_t **l_564[8][5][6] = {{{&l_553,&l_553,&l_553,&l_553,&l_553,&l_506[4][4]},{&l_506[7][5],&l_505,&l_553,&l_506[4][4],&l_505,&l_505},{(void*)0,&l_553,&l_553,&l_506[4][4],&l_505,&l_553},{&l_506[6][5],&l_505,(void*)0,&l_506[4][4],&l_553,(void*)0},{&l_506[4][4],&l_553,&l_505,&l_553,&l_506[4][4],&l_506[4][4]}},{{&l_553,&l_506[4][7],&l_553,&l_506[4][7],&l_553,&l_553},{&l_553,(void*)0,&l_506[7][5],(void*)0,(void*)0,&l_553},{&l_506[4][7],&l_553,&l_553,(void*)0,&l_553,&l_553},{&l_506[4][4],&l_505,&l_506[7][5],&l_506[6][5],&l_505,&l_553},{&l_553,&l_506[7][5],&l_553,&l_553,&l_553,&l_506[4][4]}},{{(void*)0,(void*)0,&l_505,&l_506[4][4],&l_505,&l_506[4][7]},{&l_505,(void*)0,&l_505,(void*)0,&l_505,&l_553},{&l_505,&l_553,&l_553,&l_506[4][4],(void*)0,(void*)0},{&l_505,&l_553,&l_506[4][4],(void*)0,&l_553,&l_505},{&l_505,&l_505,&l_506[4][4],&l_506[4][4],&l_553,&l_553}},{{&l_506[4][7],&l_506[6][5],(void*)0,&l_553,(void*)0,&l_506[6][5]},{&l_506[4][4],&l_505,&l_506[4][4],&l_553,&l_506[4][7],&l_553},{&l_505,&l_505,&l_553,&l_506[4][7],&l_505,&l_506[3][3]},{(void*)0,&l_505,&l_506[6][5],&l_506[4][4],&l_506[4][7],&l_505},{&l_506[3][3],&l_505,&l_506[4][4],(void*)0,(void*)0,&l_506[4][4]}},{{&l_506[4][4],&l_506[6][5],&l_553,&l_553,&l_553,&l_553},{&l_505,&l_505,&l_506[4][4],&l_505,&l_553,&l_553},{&l_553,&l_553,&l_506[7][5],&l_553,(void*)0,&l_506[4][4]},{&l_506[4][4],&l_553,&l_506[7][5],(void*)0,&l_505,&l_553},{&l_553,(void*)0,&l_506[4][4],&l_553,&l_505,&l_553}},{{&l_553,&l_505,&l_553,&l_506[4][4],&l_506[4][4],&l_506[4][4]},{&l_506[4][4],&l_553,&l_506[4][4],&l_553,(void*)0,&l_505},{(void*)0,&l_506[4][4],&l_506[6][5],&l_505,&l_506[4][4],&l_506[3][3]},{&l_505,&l_506[7][5],&l_553,&l_505,&l_505,&l_553},{(void*)0,&l_506[4][7],&l_506[4][4],&l_553,&l_506[4][4],&l_506[6][5]}},{{&l_506[4][4],(void*)0,(void*)0,&l_506[4][4],&l_553,&l_553},{&l_553,&l_506[4][4],&l_506[4][4],&l_553,&l_506[7][5],&l_505},{&l_553,&l_553,&l_506[4][4],(void*)0,(void*)0,(void*)0},{&l_506[4][4],&l_553,&l_553,&l_553,(void*)0,&l_553},{&l_553,&l_553,&l_505,&l_505,&l_506[7][5],&l_506[4][7]}},{{&l_505,&l_506[4][4],&l_506[4][4],&l_553,&l_553,&l_505},{&l_506[4][4],(void*)0,&l_506[3][3],(void*)0,&l_506[4][4],&l_506[4][4]},{&l_506[3][3],&l_506[4][7],&l_553,&l_506[4][4],&l_505,&l_506[4][4]},{(void*)0,&l_506[7][5],&l_506[4][4],&l_506[4][7],&l_506[4][4],&l_506[4][4]},{&l_505,&l_506[4][4],&l_553,&l_553,(void*)0,&l_506[4][4]}}};
                        int i, j, k;
                        l_565[0][0] = ((*l_486) = &l_562);
                    }
                    else
                    { /* block id: 301 */
                        (*l_505) |= ((!((*l_502) ^= (p_21 > 0x36FCAFE4L))) != 0x9E43E32CL);
                    }
                }
                return g_125;
            }
            else
            { /* block id: 307 */
                uint32_t l_570 = 0x2265ED66L;
                const int32_t l_575[9] = {(-1L),0x507A25CDL,(-1L),0x507A25CDL,(-1L),0x507A25CDL,(-1L),0x507A25CDL,(-1L)};
                int i;
                for (l_513 = 0; (l_513 > 22); l_513 = safe_add_func_int32_t_s_s(l_513, 8))
                { /* block id: 310 */
                    int64_t l_569 = 0x5A0DF20E0BD1B28ALL;
                    uint32_t *l_582 = &l_570;
                    int32_t l_583 = 0x45BA979BL;
                    int32_t l_585 = 0x760AA9BEL;
                    ++l_570;
                    l_515 &= (safe_mod_func_int64_t_s_s(1L, (((l_575[7] || ((0x76361E857C9BE4EBLL > ((~(p_21 || g_58.f4)) , ((l_585 = (l_577 != ((l_570 ^ (((-6L) & (safe_rshift_func_uint16_t_u_u(((safe_unary_minus_func_int32_t_s(((+(l_583 = (l_582 != (void*)0))) < 0x109A71CEL))) & 0x91F0L), p_21))) == p_21)) , g_584))) , l_585))) > p_19)) && p_18) || p_20)));
                }
            }
            l_516[5] = (safe_rshift_func_int16_t_s_s(0L, 0));
            return g_58.f7;
        }
        for (l_509 = 5; (l_509 >= 0); l_509 -= 1)
        { /* block id: 322 */
            int32_t l_589 = 2L;
            int32_t l_592 = (-1L);
            int32_t l_595[3][1];
            float **l_618 = (void*)0;
            int i, j;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_595[i][j] = 0xB95F644EL;
            }
            if (g_58.f7)
                goto lbl_588;
            if (l_516[l_509])
            { /* block id: 324 */
                int i;
                l_516[l_509] = l_516[l_509];
            }
            else
            { /* block id: 326 */
                int32_t l_591 = 1L;
                int32_t l_593 = 0x8AB85FEBL;
                int32_t l_594 = 6L;
                l_596--;
                (*l_486) = &l_509;
                if (p_19)
                    break;
            }
            for (g_111 = 0; (g_111 <= 1); g_111 += 1)
            { /* block id: 333 */
                int32_t l_675 = 1L;
                int i;
                (*l_486) = &g_481[l_509];
                (*l_486) = &l_595[2][0];
                for (g_109 = 2; (g_109 >= 0); g_109 -= 1)
                { /* block id: 338 */
                    float ***l_611 = &g_609;
                    int32_t **l_619 = &l_565[0][0];
                    int32_t *l_621 = &l_516[5];
                    uint8_t l_624 = 0x3CL;
                    int i, j, k;
                    if (((((*l_621) = ((((g_249[g_111][(g_111 + 3)][(g_111 + 1)] ^ (((safe_lshift_func_int8_t_s_s(((safe_mul_func_int16_t_s_s(0xFE18L, ((g_481[(g_111 + 6)] , (((l_592 <= p_21) & (safe_lshift_func_int16_t_s_u((-1L), 5))) != ((safe_lshift_func_int16_t_s_u((safe_div_func_int16_t_s_s(((((*l_619) = ((((*l_611) = g_609) != (l_618 = ((g_249[g_111][(g_111 + 3)][(g_111 + 1)] == ((((safe_mod_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((((safe_rshift_func_uint16_t_u_s(1UL, p_21)) && (-10L)) , 1L), p_20)), (**g_129))) , g_481[l_509]) ^ (-8L)) , 1L)) , (void*)0))) , (void*)0)) != &g_481[l_509]) || 0x9629L), l_592)), 8)) > g_400.f0))) ^ l_620))) != p_20), g_481[l_509])) || 0UL) || p_19)) != 0x6D94L) , 1UL) & (-1L))) != p_19) == 0x6B2AL))
                    { /* block id: 343 */
                        int8_t l_622 = (-8L);
                        int32_t l_623 = 0x1310600CL;
                        int64_t *l_645[7];
                        int i, j;
                        for (i = 0; i < 7; i++)
                            l_645[i] = &l_620;
                        l_624++;
                        (*l_619) = &l_514[0][0][2];
                        (***l_611) = (safe_div_func_float_f_f(((((((safe_mul_func_uint16_t_u_u((((((((l_631 != p_21) != ((l_592 |= (((safe_lshift_func_uint16_t_u_u((**g_129), ((safe_rshift_func_int8_t_s_s(0x94L, ((void*)0 == l_636))) > 0xB0A312520063B204LL))) == (safe_div_func_uint8_t_u_u((g_342[(g_111 + 2)][g_111] = (safe_sub_func_int64_t_s_s(l_595[2][0], ((safe_div_func_uint16_t_u_u((((((safe_div_func_int32_t_s_s(p_21, g_392.f4)) < p_18) & l_622) && g_58.f0) & l_622), p_21)) < 9UL)))), 0xC3L))) != 18446744073709551615UL)) > (*l_621))) , 0x878596ECL) , l_516[l_509]) , &g_284) == (void*)0) & p_20), g_58.f7)) & 0x84L) , g_481[l_509]) != p_18) , (**g_284)) >= 0x2.D5D224p+16), p_18));
                        (*l_621) = (g_646 , (safe_div_func_uint16_t_u_u((safe_add_func_int8_t_s_s((l_595[1][0] <= (safe_add_func_int8_t_s_s(((p_19 < (g_11 < (!((((l_622 || ((p_18 , (safe_div_func_int64_t_s_s(((safe_sub_func_int32_t_s_s(p_18, (safe_add_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(g_74, 11)), (*l_621))))) , g_662), g_646.f7))) ^ 0xE0L)) < (-1L)) & g_663) , 1L)))) == p_19), p_20))), 0L)), p_20)));
                    }
                    else
                    { /* block id: 350 */
                        const uint16_t ** const * const **l_664 = (void*)0;
                        const uint16_t *l_669 = &g_74;
                        const uint16_t **l_668 = &l_669;
                        const uint16_t ***l_667 = &l_668;
                        const uint16_t ****l_666 = &l_667;
                        const uint16_t *****l_665 = &l_666;
                        int32_t *l_670 = (void*)0;
                        (*l_665) = (void*)0;
                        (*l_619) = (void*)0;
                        (*l_486) = l_670;
                    }
                }
                for (l_561 = 0; (l_561 <= 1); l_561 += 1)
                { /* block id: 358 */
                    int32_t *l_671 = &g_663;
                    int32_t l_673[7];
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_673[i] = 0xFAAEEE3CL;
                    if ((g_249[l_561][(g_111 + 5)][(g_111 + 4)] ^ 1L))
                    { /* block id: 359 */
                        (*l_486) = &g_663;
                        (*l_486) = l_671;
                        l_516[l_509] = l_595[2][0];
                        return l_595[2][0];
                    }
                    else
                    { /* block id: 364 */
                        int64_t l_672 = 0x3324E8C4494CDB9DLL;
                        int32_t l_674[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_674[i] = 0xFD219CBCL;
                        if (g_111)
                            goto lbl_588;
                        l_676++;
                        if (p_18)
                            continue;
                        if ((*l_671))
                            continue;
                    }
                    for (g_233 = (-21); (g_233 <= 3); g_233++)
                    { /* block id: 372 */
                        uint8_t l_682 = 1UL;
                        ++l_682;
                    }
                    (*l_486) = (void*)0;
                }
            }
        }
        for (l_513 = 0; (l_513 > 24); l_513 = safe_add_func_uint32_t_u_u(l_513, 8))
        { /* block id: 381 */
            return g_452.f4;
        }
        return g_147.f1;
    }
    return g_452.f3;
}


/* ------------------------------------------ */
/* 
 * reads : g_11 g_55 g_58 g_74 g_87 g_103 g_109 g_113 g_129 g_114 g_143 g_125 g_284 g_192 g_130 g_147.f2 g_314 g_233 g_147.f1 g_392 g_396 g_400 g_255 g_449 g_111 g_229 g_147.f7 g_452.f1 g_249 g_481
 * writes: g_74 g_87 g_103 g_109 g_111 g_114 g_134 g_143 g_125 g_255 g_342 g_314 g_233 g_229 g_249 g_461 g_141 g_481
 */
static uint64_t  func_22(int64_t  p_23)
{ /* block id: 1 */
    uint32_t l_34 = 4294967294UL;
    uint32_t l_456[9][10][2] = {{{6UL,0x99C70E43L},{18446744073709551615UL,18446744073709551607UL},{0x1189C198L,0xAD38FA3FL},{18446744073709551612UL,1UL},{18446744073709551606UL,0UL},{0xCC18F221L,18446744073709551606UL},{0x6C674AB2L,18446744073709551610UL},{4UL,18446744073709551615UL},{0xCC18F221L,0x55ED1355L},{18446744073709551615UL,1UL}},{{0x1BA145CBL,0xF538693BL},{0x7AF55E32L,0xA59098D1L},{18446744073709551611UL,0xCC18F221L},{0x812651C3L,0xCC18F221L},{18446744073709551611UL,0xA59098D1L},{0x7AF55E32L,0xF538693BL},{0x1BA145CBL,1UL},{18446744073709551615UL,0x55ED1355L},{0xCC18F221L,18446744073709551615UL},{4UL,18446744073709551610UL}},{{0x6C674AB2L,18446744073709551606UL},{0xCC18F221L,0UL},{18446744073709551606UL,1UL},{18446744073709551612UL,0xAD38FA3FL},{0x7AF55E32L,18446744073709551611UL},{0xA59098D1L,0xCC18F221L},{18446744073709551615UL,5UL},{18446744073709551611UL,18446744073709551611UL},{0x83704C71L,0xF538693BL},{18446744073709551612UL,1UL}},{{18446744073709551615UL,0UL},{5UL,18446744073709551615UL},{0x6C674AB2L,0x24C3D9C2L},{0x6C674AB2L,18446744073709551615UL},{5UL,0UL},{18446744073709551615UL,1UL},{18446744073709551612UL,0xF538693BL},{0x83704C71L,18446744073709551611UL},{18446744073709551611UL,5UL},{18446744073709551615UL,0xCC18F221L}},{{0xA59098D1L,18446744073709551611UL},{0x7AF55E32L,0xAD38FA3FL},{18446744073709551612UL,1UL},{18446744073709551606UL,0UL},{0xCC18F221L,18446744073709551606UL},{0x6C674AB2L,18446744073709551610UL},{4UL,18446744073709551615UL},{0xCC18F221L,0x55ED1355L},{18446744073709551615UL,1UL},{0x1BA145CBL,0xF538693BL}},{{0x7AF55E32L,0xA59098D1L},{18446744073709551611UL,0xCC18F221L},{0x812651C3L,0xCC18F221L},{18446744073709551611UL,0xA59098D1L},{0x7AF55E32L,0xF538693BL},{0x1BA145CBL,1UL},{18446744073709551615UL,0x55ED1355L},{0xCC18F221L,18446744073709551615UL},{4UL,18446744073709551610UL},{0x6C674AB2L,18446744073709551606UL}},{{0xCC18F221L,0UL},{18446744073709551606UL,1UL},{18446744073709551612UL,0xAD38FA3FL},{0x7AF55E32L,18446744073709551611UL},{0xA59098D1L,0xCC18F221L},{18446744073709551615UL,5UL},{18446744073709551611UL,18446744073709551611UL},{0x83704C71L,0xF538693BL},{18446744073709551612UL,1UL},{18446744073709551615UL,0UL}},{{5UL,18446744073709551615UL},{0x6C674AB2L,0x24C3D9C2L},{0x6C674AB2L,18446744073709551615UL},{5UL,0UL},{18446744073709551615UL,1UL},{18446744073709551612UL,0xF538693BL},{0x83704C71L,18446744073709551611UL},{18446744073709551611UL,5UL},{18446744073709551615UL,0xCC18F221L},{0xA59098D1L,18446744073709551611UL}},{{0x7AF55E32L,0xAD38FA3FL},{18446744073709551612UL,1UL},{18446744073709551606UL,0UL},{0xCC18F221L,18446744073709551606UL},{0x6C674AB2L,18446744073709551610UL},{4UL,18446744073709551615UL},{0xCC18F221L,0x55ED1355L},{18446744073709551615UL,1UL},{0x1BA145CBL,0xF538693BL},{0x7AF55E32L,0xA59098D1L}}};
    int32_t *l_479 = (void*)0;
    int32_t *l_480 = &g_481[7];
    int i, j, k;
    (*l_480) &= (g_11 == ((safe_div_func_uint8_t_u_u(func_26((safe_add_func_uint16_t_u_u((l_34 == g_11), (((0x005DL && (((l_34 > (((safe_sub_func_int32_t_s_s(func_37((255UL && (safe_mod_func_int16_t_s_s((p_23 , (safe_div_func_int8_t_s_s((safe_add_func_int32_t_s_s((func_45(g_11, (safe_mod_func_int16_t_s_s(((func_50(p_23) , p_23) > g_400.f2), g_147.f1))) != 0x08372FF3A9094DB9LL), l_456[4][9][1])), p_23))), 0xA4A9L)))), g_147.f7)) < 0x12L) && p_23)) ^ 0UL) , (*g_130))) && 4294967295UL) == p_23))), p_23, l_34, l_456[4][9][1], p_23), p_23)) <= l_456[2][2][1]));
    return g_400.f2;
}


/* ------------------------------------------ */
/* 
 * reads : g_113 g_114 g_452.f1 g_249 g_143 g_130 g_284
 * writes: g_461 g_249 g_74 g_141 g_114
 */
static uint8_t  func_26(int64_t  p_27, int8_t  p_28, int32_t  p_29, uint32_t  p_30, int8_t  p_31)
{ /* block id: 259 */
    uint16_t * const **l_460 = (void*)0;
    const struct S0 *l_464[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const struct S0 **l_463 = &l_464[5];
    int32_t l_469 = (-6L);
    int32_t l_472[6] = {1L,0x1CB94732L,0x1CB94732L,1L,0x1CB94732L,1L};
    int16_t *l_473 = &g_249[0][4][3];
    float l_474 = (-0x10.3p-1);
    float *l_477 = &g_141;
    uint32_t l_478 = 4294967287UL;
    int i;
    g_461 = l_460;
    (*l_463) = &g_452;
    l_469 = (safe_div_func_float_f_f((((**g_284) = (safe_add_func_float_f_f((l_469 >= 0x2.5614E2p+22), (safe_div_func_float_f_f((*g_113), ((((*l_477) = (((253UL == (0xBF6021AA025E1621LL ^ ((l_472[2] = 1L) || ((*l_473) |= g_452.f1)))) , ((*g_130) = (((l_472[5] & ((safe_rshift_func_uint8_t_u_s(p_27, l_472[1])) ^ p_29)) != 3L) & g_143))) , p_31)) >= l_478) < l_478)))))) != l_469), 0x1.1p-1));
    return l_469;
}


/* ------------------------------------------ */
/* 
 * reads : g_111 g_229
 * writes: g_111 g_229
 */
static int32_t  func_37(int16_t  p_38)
{ /* block id: 248 */
    uint32_t l_459[10][5][5] = {{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL}},{{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}},{{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{4294967288UL,4294967288UL,4294967288UL,4294967288UL,4294967288UL}}};
    int i, j, k;
    for (g_111 = 0; (g_111 <= (-23)); g_111--)
    { /* block id: 251 */
        for (g_229 = 0; (g_229 <= 8); g_229 += 1)
        { /* block id: 254 */
            return p_38;
        }
    }
    return l_459[2][0][2];
}


/* ------------------------------------------ */
/* 
 * reads : g_143
 * writes: g_143
 */
static int64_t  func_45(int32_t  p_46, uint64_t  p_47)
{ /* block id: 245 */
    uint16_t l_453 = 0x30C4L;
    int32_t *l_454 = (void*)0;
    int32_t *l_455 = &g_143;
    (*l_455) = l_453;
    return (*l_455);
}


/* ------------------------------------------ */
/* 
 * reads : g_11 g_55 g_58 g_74 g_87 g_103 g_109 g_113 g_129 g_114 g_143 g_125 g_284 g_192 g_130 g_147.f2 g_314 g_233 g_147.f1 g_392 g_396 g_400 g_255 g_449
 * writes: g_74 g_87 g_103 g_109 g_111 g_114 g_134 g_143 g_125 g_255 g_342 g_314 g_233 g_229 g_249
 */
static float  func_50(uint32_t  p_51)
{ /* block id: 2 */
    uint32_t l_140 = 4294967286UL;
    int32_t *l_142 = &g_143;
    int64_t *l_146 = &g_125;
    int32_t l_161 = 0L;
    int32_t l_164 = 0L;
    int8_t *l_236 = &g_109;
    uint16_t l_242[2];
    int64_t **l_276[6];
    uint8_t *l_312 = &g_134;
    int64_t l_340 = 0x19EDC76D682609BDLL;
    int32_t l_368 = 0x155007D6L;
    int32_t l_370 = 7L;
    int32_t l_371 = 1L;
    int32_t l_373 = (-1L);
    int32_t l_376[3];
    uint16_t l_377 = 0x6E56L;
    uint16_t * const *l_389 = &g_130;
    const uint8_t l_445 = 0x45L;
    int i;
    for (i = 0; i < 2; i++)
        l_242[i] = 0UL;
    for (i = 0; i < 6; i++)
        l_276[i] = &l_146;
    for (i = 0; i < 3; i++)
        l_376[i] = 0x519FD23FL;
    (*g_113) = func_52(g_11, g_11);
    if (((*l_142) |= ((safe_unary_minus_func_int16_t_s(p_51)) ^ (!l_140))))
    { /* block id: 25 */
        int64_t *l_148[7][1] = {{&g_125},{&g_125},{&g_125},{&g_125},{&g_125},{&g_125},{&g_125}};
        int32_t l_163 = 0xA5162D90L;
        uint32_t l_186 = 0x5778D356L;
        int32_t l_189 = 0L;
        int32_t l_190[10][5] = {{0L,0L,0x69B01DD7L,2L,1L},{1L,0xA954584BL,(-1L),(-1L),0x34FE3EFBL},{0x2A8A950BL,0L,0xEC640611L,0L,1L},{(-1L),0xA954584BL,0L,0x5DEFB49FL,0xEC640611L},{1L,0L,0L,0L,1L},{1L,(-1L),0x2A8A950BL,1L,0L},{(-1L),1L,0x9EEFC36FL,0x2A8A950BL,0L},{0x2A8A950BL,0x9EEFC36FL,1L,(-1L),0L},{1L,0x2A8A950BL,(-1L),1L,1L},{0L,0L,0L,1L,0xEC640611L}};
        int8_t *l_237 = &g_229;
        int32_t *l_256 = &l_190[1][2];
        float *l_269 = &g_141;
        int32_t *l_317 = (void*)0;
        int32_t *l_318[4];
        uint8_t l_319[4];
        int i, j;
        for (i = 0; i < 4; i++)
            l_318[i] = &l_190[7][1];
        for (i = 0; i < 4; i++)
            l_319[i] = 255UL;
        for (g_125 = 18; (g_125 <= (-29)); g_125--)
        { /* block id: 28 */
            int32_t l_149 = 1L;
            uint8_t *l_150 = &g_134;
            int32_t l_191[8];
            int16_t *l_247 = &g_213;
            int16_t *l_248[5][9] = {{&g_249[0][4][3],(void*)0,&g_249[1][2][2],(void*)0,&g_249[0][4][3],&g_249[0][4][3],(void*)0,&g_249[1][2][2],(void*)0},{(void*)0,&g_249[0][4][3],&g_249[1][2][2],&g_249[1][2][2],&g_249[0][4][3],(void*)0,&g_249[0][4][3],&g_249[1][2][2],&g_249[1][2][2]},{&g_249[0][4][3],&g_249[0][4][3],(void*)0,&g_249[1][2][2],(void*)0,&g_249[0][4][3],&g_249[0][4][3],(void*)0,&g_249[1][2][2]},{&g_249[0][1][1],&g_249[0][4][3],&g_249[0][1][1],(void*)0,(void*)0,&g_249[0][1][1],&g_249[0][4][3],&g_249[0][1][1],(void*)0},{&g_249[0][1][1],(void*)0,(void*)0,&g_249[0][1][1],&g_249[0][4][3],&g_249[0][1][1],(void*)0,(void*)0,&g_249[0][1][1]}};
            int8_t **l_250 = (void*)0;
            int i, j;
            for (i = 0; i < 8; i++)
                l_191[i] = 0xAB99CF72L;
        }
        if (l_140)
            goto lbl_316;
lbl_316:
        for (l_189 = 0; (l_189 > 5); l_189++)
        { /* block id: 94 */
            int32_t *l_261 = &l_161;
            int32_t **l_262 = &l_256;
            const float **l_263 = (void*)0;
            const float *l_265 = &g_266;
            const float **l_264 = &l_265;
            float *l_268 = (void*)0;
            float **l_267[5][6] = {{&l_268,&l_268,&l_268,&l_268,(void*)0,&l_268},{&l_268,&l_268,&l_268,&l_268,&l_268,&l_268},{&l_268,&l_268,&l_268,&l_268,&l_268,&l_268},{&l_268,&l_268,(void*)0,&l_268,(void*)0,&l_268},{(void*)0,&l_268,&l_268,&l_268,(void*)0,&l_268}};
            int16_t *l_270 = (void*)0;
            int16_t *l_271[8][6] = {{&g_249[0][4][3],&g_249[0][4][3],&g_11,&g_11,&g_249[0][4][3],&g_249[0][4][3]},{(void*)0,&g_249[0][4][3],&g_11,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_11,&g_249[0][4][3],(void*)0},{&g_249[0][4][3],&g_249[0][4][3],&g_11,&g_11,&g_249[0][4][3],&g_249[0][4][3]},{&g_11,(void*)0,&g_11,&g_11,(void*)0,&g_11},{&g_249[0][4][3],&g_249[0][4][3],&g_11,&g_11,&g_249[0][4][3],&g_249[0][4][3]},{(void*)0,&g_249[0][4][3],&g_11,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_11,&g_249[0][4][3],(void*)0}};
            uint32_t *l_279 = &l_140;
            uint32_t *l_280 = &g_87;
            uint64_t *l_281 = &g_233;
            int32_t *l_282 = &l_190[3][4];
            int32_t l_283 = 0x1A6DB7C8L;
            int i, j;
        }
        g_255[3][3][2] = &l_190[1][3];
        l_319[0]--;
    }
    else
    { /* block id: 134 */
        float l_324 = 0x9.297DA3p-35;
        int32_t l_344 = 0x203A046EL;
        int32_t l_364 = 0xFCF0B019L;
        int32_t l_366 = 1L;
        int32_t l_369 = 0x8463D822L;
        uint32_t *l_399 = &l_140;
        int32_t l_429 = 0x7C658BECL;
        uint8_t *l_430 = &g_134;
        (**g_284) = (*g_113);
        if (p_51)
        { /* block id: 136 */
            int16_t l_343 = 0x8B6BL;
            int32_t l_346 = 1L;
            for (p_51 = 0; (p_51 <= 1); p_51 += 1)
            { /* block id: 139 */
                int32_t l_325 = (-1L);
                float *l_338[10] = {&g_141,&g_141,&g_141,&g_141,&g_141,&g_141,&g_141,&g_141,&g_141,&g_141};
                float **l_339 = &l_338[4];
                uint8_t *l_341 = &g_342[3][2];
                int32_t *l_345 = &l_344;
                int i;
                l_346 ^= ((*l_345) = (safe_sub_func_uint8_t_u_u(l_325, (safe_sub_func_uint8_t_u_u(((((*l_142) = ((safe_rshift_func_uint16_t_u_s(((safe_lshift_func_int16_t_s_u(g_192, 11)) < ((*l_341) = (((safe_rshift_func_uint16_t_u_u(1UL, (l_325 , (*l_142)))) , ((safe_lshift_func_int8_t_s_s((safe_sub_func_int16_t_s_s(((p_51 , (((*l_339) = l_338[4]) != (void*)0)) || ((*l_312) = (*l_142))), (*g_130))), 5)) && l_340)) > 18446744073709551608UL))), g_147.f2)) >= l_343)) | l_344) , l_343), l_343)))));
                for (g_314 = 0; (g_314 <= 1); g_314 += 1)
                { /* block id: 148 */
                    int8_t l_356 = 0x9CL;
                    for (g_233 = 0; (g_233 <= 1); g_233 += 1)
                    { /* block id: 151 */
                        int32_t l_347[8][4] = {{(-2L),(-2L),(-1L),(-2L)},{(-2L),0xF4E3D528L,0xF4E3D528L,(-2L)},{0xF4E3D528L,(-2L),0xF4E3D528L,0xF4E3D528L},{(-2L),(-2L),(-1L),(-2L)},{(-2L),0xF4E3D528L,0xF4E3D528L,(-2L)},{0xF4E3D528L,(-2L),0xF4E3D528L,0xF4E3D528L},{(-2L),(-2L),(-1L),(-2L)},{(-2L),0xF4E3D528L,0xF4E3D528L,(-2L)}};
                        int32_t *l_357 = &l_346;
                        int i, j;
                        (*l_357) |= (l_347[3][2] & (safe_mul_func_int8_t_s_s((safe_rshift_func_int16_t_s_u((safe_sub_func_uint64_t_u_u(((safe_div_func_uint8_t_u_u(((*l_341) = (((((*l_146) = (1L > l_356)) & 8UL) == l_344) , 0x21L)), p_51)) <= (0xF089A8FEE83EF03DLL != (((g_58 , p_51) > (*l_142)) && 0x4447B229L))), 0x4E5084947FFBDC57LL)), 13)), (*l_345))));
                        return p_51;
                    }
                    (*l_142) = (p_51 && ((*l_146) |= 0xFD7C6794A24C8CDCLL));
                }
                for (g_103 = 0; (g_103 <= 1); g_103 += 1)
                { /* block id: 162 */
                    for (l_340 = 0; (l_340 <= 1); l_340 += 1)
                    { /* block id: 165 */
                        (*l_345) &= (~g_147.f1);
                    }
                }
            }
        }
        else
        { /* block id: 170 */
            int32_t **l_359 = &g_255[3][3][2];
            (*l_359) = &l_344;
        }
        for (g_125 = 0; (g_125 <= 18); g_125++)
        { /* block id: 175 */
            int8_t l_365[10][6] = {{0xB9L,0xD1L,0xB9L,0L,0xB0L,0x84L},{(-1L),1L,0x27L,1L,0x81L,(-1L)},{0x84L,0x0AL,(-1L),1L,0xF5L,0L},{(-1L),0xC3L,0x0BL,0L,0x0AL,0x46L},{0xB9L,0xE1L,0L,0xC7L,0xC7L,0L},{0L,0L,(-7L),1L,0x84L,0xE1L},{0xF5L,0L,0xCBL,0xB0L,(-1L),(-7L)},{2L,0xF5L,0xCBL,0x90L,0L,0xE1L},{1L,0x90L,(-7L),0xCBL,0x46L,0L},{0xCBL,0x46L,0L,0xB0L,0L,0x46L}};
            int32_t l_367 = 0x4346611AL;
            int32_t l_372 = 0L;
            int32_t l_374 = 0xFEBCA4E2L;
            int32_t l_375 = 0L;
            uint64_t *l_393[10][10] = {{&g_233,(void*)0,(void*)0,(void*)0,(void*)0,&g_233,&g_233,&g_233,&g_233,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_233,&g_233,&g_233,&g_233,(void*)0,(void*)0},{(void*)0,(void*)0,&g_233,(void*)0,&g_233,&g_233,(void*)0,&g_233,(void*)0,(void*)0},{&g_233,(void*)0,&g_233,(void*)0,(void*)0,&g_233,(void*)0,&g_233,&g_233,(void*)0},{&g_233,(void*)0,(void*)0,(void*)0,(void*)0,&g_233,&g_233,&g_233,&g_233,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_233,&g_233,&g_233,&g_233,(void*)0,(void*)0},{(void*)0,(void*)0,&g_233,(void*)0,&g_233,&g_233,(void*)0,&g_233,(void*)0,(void*)0},{&g_233,(void*)0,&g_233,(void*)0,(void*)0,&g_233,(void*)0,&g_233,&g_233,(void*)0},{&g_233,(void*)0,(void*)0,(void*)0,(void*)0,&g_233,&g_233,&g_233,&g_233,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_233,&g_233,&g_233,&g_233,(void*)0,(void*)0}};
            int i, j;
            for (g_229 = 3; (g_229 >= 0); g_229 -= 1)
            { /* block id: 178 */
                int32_t *l_362 = &l_164;
                int32_t *l_363[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_363[i] = &g_143;
                --l_377;
                if (l_374)
                    continue;
                for (g_103 = 0; (g_103 <= 4); g_103 += 1)
                { /* block id: 183 */
                    uint16_t l_388 = 65535UL;
                    int i, j, k;
                    for (l_140 = 0; (l_140 <= 4); l_140 += 1)
                    { /* block id: 186 */
                        const int32_t *l_381[2];
                        const int32_t **l_380 = &l_381[0];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_381[i] = &l_366;
                        (*l_380) = (void*)0;
                        (**g_284) = (-0x1.9p-1);
                    }
                    (*l_142) |= (safe_add_func_uint64_t_u_u(0x1A090B4C5DE5531ALL, (safe_sub_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u(l_365[0][1], l_388)), p_51))));
                }
                for (l_370 = 5; (l_370 >= 1); l_370 -= 1)
                { /* block id: 194 */
                    uint16_t * const **l_390 = (void*)0;
                    uint16_t * const **l_391 = &l_389;
                    (*l_391) = (g_58 , l_389);
                }
            }
            for (l_369 = 1; (l_369 >= 0); l_369 -= 1)
            { /* block id: 200 */
                uint64_t *l_394 = &g_233;
                int16_t *l_395[7][9][4] = {{{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]}},{{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]}},{{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]}},{{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]}},{{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]}},{{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]}},{{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]},{&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3],&g_249[0][4][3]}}};
                int i, j, k;
                (*g_396) = ((1UL || (p_51 < (g_249[0][4][3] = (g_392 , (((l_393[1][1] != (l_394 = l_146)) > 0xD4A6L) || 0xE2L))))) , &g_143);
            }
            if ((safe_lshift_func_uint8_t_u_u(((void*)0 == l_399), (g_400 , ((*l_142) != ((*l_312) = (((safe_rshift_func_uint16_t_u_u((+((*l_142) || (((safe_add_func_float_f_f(((p_51 ^ (0x1AD9B4CEL <= ((((((*l_236) = ((g_400.f1 <= 65527UL) , l_369)) >= l_372) ^ (*l_142)) || l_372) > 0x4945L))) , (*g_113)), (-0x7.8p+1))) , &g_255[4][0][6]) == &g_255[3][2][6]))), 14)) | 0x00268C01L) , 254UL)))))))
            { /* block id: 207 */
                for (g_74 = 0; (g_74 <= 5); g_74 += 1)
                { /* block id: 210 */
                    for (l_377 = 0; (l_377 <= 5); l_377 += 1)
                    { /* block id: 213 */
                        int i, j;
                        return l_365[(l_377 + 1)][l_377];
                    }
                }
            }
            else
            { /* block id: 217 */
                int8_t ** const l_409 = &l_236;
                int8_t ** const *l_408[6][1][5];
                int32_t l_417 = 0xB6F7C992L;
                int32_t *l_418 = &l_374;
                int8_t ** const l_446 = (void*)0;
                int i, j, k;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 1; j++)
                    {
                        for (k = 0; k < 5; k++)
                            l_408[i][j][k] = &l_409;
                    }
                }
                if (((l_408[3][0][3] == ((*l_142) , (void*)0)) ^ ((*l_418) = ((!((((l_364 = (safe_rshift_func_int16_t_s_u(((safe_div_func_int16_t_s_s(((safe_sub_func_uint64_t_u_u(p_51, ((void*)0 == (*g_396)))) || (((l_417 ^ l_375) == ((p_51 && 0x3EL) , 0xA8ACL)) ^ p_51)), 65535UL)) && g_392.f0), l_374))) , p_51) , p_51) | l_372)) >= p_51))))
                { /* block id: 220 */
                    int32_t l_426 = 0x201DBA6FL;
                    int32_t l_431 = 0xF556798BL;
                    int64_t *l_432 = &l_340;
                    if (((safe_unary_minus_func_int32_t_s((*l_142))) < (safe_mod_func_int32_t_s_s(((l_369 |= ((safe_mod_func_uint32_t_u_u(((l_426 = 0x8ED2F359L) || (safe_mul_func_uint16_t_u_u(l_429, (l_365[0][1] || (0x68249719CCD13215LL || ((void*)0 == l_430)))))), l_431)) == (l_432 == l_432))) >= g_147.f2), g_55.f3))))
                    { /* block id: 223 */
                        (*l_142) &= 1L;
                    }
                    else
                    { /* block id: 225 */
                        uint32_t l_437 = 1UL;
                        (*l_418) |= ((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u((*l_142), 2)), l_437)) > p_51);
                        (**g_284) = (&g_111 != &g_109);
                    }
                    if ((*l_418))
                        continue;
                    (**g_284) = ((l_375 ^ p_51) , ((((safe_div_func_int16_t_s_s(((void*)0 != l_399), ((*g_130)--))) , 0x0.3p-1) > (**g_284)) == ((*l_142) = ((safe_sub_func_float_f_f(((+0x2.5p-1) > 0xF.A09181p+13), ((l_445 , l_446) != (void*)0))) <= p_51))));
                }
                else
                { /* block id: 233 */
                    for (l_429 = (-6); (l_429 >= (-10)); l_429--)
                    { /* block id: 236 */
                        const struct S0 *l_450 = (void*)0;
                        const struct S0 *l_451 = &g_452;
                        (*g_449) = &l_376[1];
                        l_451 = l_450;
                    }
                }
            }
        }
    }
    return p_51;
}


/* ------------------------------------------ */
/* 
 * reads : g_55 g_58 g_74 g_87 g_103 g_109 g_113 g_129 g_114
 * writes: g_74 g_87 g_103 g_109 g_111 g_114 g_134
 */
static float  func_52(int16_t  p_53, uint32_t  p_54)
{ /* block id: 3 */
    int32_t l_69 = 9L;
    uint16_t *l_73 = &g_74;
    int8_t l_77 = (-4L);
    int32_t l_106 = 0x732E7CE9L;
    int16_t l_112 = (-1L);
    int32_t l_137 = 0L;
    l_77 |= (g_55 , (7L < (safe_mod_func_uint32_t_u_u((((((g_58 , (((safe_mod_func_uint32_t_u_u((safe_sub_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_s(((*l_73) = (safe_sub_func_int32_t_s_s((safe_mod_func_int8_t_s_s(l_69, (safe_unary_minus_func_int8_t_s(0x1AL)))), (safe_rshift_func_int16_t_s_u(g_58.f2, 0))))), 5)) , g_55.f0), (safe_mod_func_int8_t_s_s((((g_58.f2 != l_69) | l_69) || g_58.f7), g_58.f1)))), g_58.f1)) >= 0xF201L) , 1L)) & 6UL) & p_53) || g_55.f5) ^ l_69), p_53))));
    for (g_74 = (-28); (g_74 >= 47); g_74 = safe_add_func_int32_t_s_s(g_74, 1))
    { /* block id: 8 */
        uint16_t *l_82[3][9][2];
        uint16_t **l_83 = &l_82[0][4][0];
        int32_t *l_86[8] = {&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69};
        uint16_t *l_102[8][10][1] = {{{(void*)0},{&g_103},{(void*)0},{(void*)0},{&g_74},{&g_74},{&g_103},{&g_74},{&g_103},{&g_74}},{{(void*)0},{&g_74},{&g_74},{&g_103},{&g_74},{&g_74},{(void*)0},{&g_74},{&g_103},{&g_74}},{{&g_103},{&g_74},{&g_74},{(void*)0},{(void*)0},{&g_103},{(void*)0},{&g_103},{&g_103},{(void*)0}},{{&g_74},{&g_74},{&g_74},{&g_74},{(void*)0},{&g_103},{&g_103},{(void*)0},{&g_103},{(void*)0}},{{(void*)0},{&g_74},{&g_74},{&g_103},{&g_74},{&g_103},{&g_74},{(void*)0},{&g_74},{&g_74}},{{&g_103},{&g_74},{&g_74},{(void*)0},{&g_74},{&g_103},{&g_74},{&g_103},{&g_74},{&g_74}},{{&g_103},{&g_103},{(void*)0},{&g_74},{&g_74},{&g_103},{&g_74},{&g_74},{(void*)0},{(void*)0}},{{&g_74},{&g_74},{&g_103},{&g_74},{&g_74},{(void*)0},{&g_103},{&g_103},{&g_103},{&g_103}}};
        int64_t *l_123 = (void*)0;
        int64_t *l_124[9] = {&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125,&g_125};
        uint16_t ***l_128 = &l_83;
        uint8_t *l_133 = &g_134;
        int16_t *l_135 = (void*)0;
        int16_t *l_136 = &l_112;
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 9; j++)
            {
                for (k = 0; k < 2; k++)
                    l_82[i][j][k] = &g_74;
            }
        }
        (*g_113) = (safe_div_func_float_f_f((((*l_83) = l_82[0][4][0]) != (void*)0), ((safe_div_func_float_f_f(g_55.f3, ((g_87 |= (-3L)) , ((safe_div_func_float_f_f(((safe_sub_func_uint64_t_u_u(p_54, (((safe_mod_func_int32_t_s_s((safe_div_func_int64_t_s_s((~(((safe_unary_minus_func_uint8_t_u((safe_lshift_func_uint16_t_u_s((safe_mod_func_int16_t_s_s((g_58.f1 , ((l_106 = (g_103--)) <= (((safe_sub_func_int32_t_s_s(g_58.f2, (g_109 |= 0xE65A1B20L))) != (((((g_111 = (+(&g_74 == (void*)0))) <= 0x829F48D0L) > g_58.f2) && l_69) , g_58.f2)) != 6UL))), p_54)), g_58.f5)))) != (-4L)) , 0xA6FF1AB1L)), l_112)), p_54)) | g_58.f1) < p_54))) , g_74), 0x0.87A7CDp+39)) > 0x1.Ap-1)))) >= 0x0.0p-1)));
        l_137 = (safe_add_func_uint32_t_u_u((((*l_136) = (((1L | ((*l_133) = (safe_lshift_func_int8_t_s_s(p_53, (p_53 != ((safe_add_func_uint16_t_u_u(((l_69 |= (g_55.f7 != (safe_rshift_func_uint8_t_u_s(1UL, 1)))) ^ (safe_mul_func_int16_t_s_s((p_54 > ((((((((((*l_128) = &l_73) == g_129) < (safe_lshift_func_int16_t_s_u(((l_106 , 0x01A4A5B4L) , p_53), 8))) , (-7L)) > l_106) > 0L) , &g_74) != &g_74) , g_58.f2)), p_53))), 0x5715L)) != p_53)))))) , l_77) >= 0x953B525EL)) < p_53), g_58.f2));
    }
    return (*g_113);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_55.f0, "g_55.f0", print_hash_value);
    transparent_crc(g_55.f1, "g_55.f1", print_hash_value);
    transparent_crc(g_55.f2, "g_55.f2", print_hash_value);
    transparent_crc(g_55.f3, "g_55.f3", print_hash_value);
    transparent_crc(g_55.f4, "g_55.f4", print_hash_value);
    transparent_crc(g_55.f5, "g_55.f5", print_hash_value);
    transparent_crc(g_55.f6, "g_55.f6", print_hash_value);
    transparent_crc(g_55.f7, "g_55.f7", print_hash_value);
    transparent_crc(g_58.f0, "g_58.f0", print_hash_value);
    transparent_crc(g_58.f1, "g_58.f1", print_hash_value);
    transparent_crc(g_58.f2, "g_58.f2", print_hash_value);
    transparent_crc(g_58.f3, "g_58.f3", print_hash_value);
    transparent_crc(g_58.f4, "g_58.f4", print_hash_value);
    transparent_crc(g_58.f5, "g_58.f5", print_hash_value);
    transparent_crc(g_58.f6, "g_58.f6", print_hash_value);
    transparent_crc(g_58.f7, "g_58.f7", print_hash_value);
    transparent_crc(g_74, "g_74", print_hash_value);
    transparent_crc(g_87, "g_87", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_109, "g_109", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc_bytes (&g_114, sizeof(g_114), "g_114", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_134, "g_134", print_hash_value);
    transparent_crc_bytes (&g_141, sizeof(g_141), "g_141", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    transparent_crc(g_147.f0, "g_147.f0", print_hash_value);
    transparent_crc(g_147.f1, "g_147.f1", print_hash_value);
    transparent_crc(g_147.f2, "g_147.f2", print_hash_value);
    transparent_crc(g_147.f3, "g_147.f3", print_hash_value);
    transparent_crc(g_147.f4, "g_147.f4", print_hash_value);
    transparent_crc(g_147.f5, "g_147.f5", print_hash_value);
    transparent_crc(g_147.f6, "g_147.f6", print_hash_value);
    transparent_crc(g_147.f7, "g_147.f7", print_hash_value);
    transparent_crc(g_192, "g_192", print_hash_value);
    transparent_crc(g_213, "g_213", print_hash_value);
    transparent_crc_bytes (&g_224, sizeof(g_224), "g_224", print_hash_value);
    transparent_crc(g_229, "g_229", print_hash_value);
    transparent_crc(g_233, "g_233", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_249[i][j][k], "g_249[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_266, sizeof(g_266), "g_266", print_hash_value);
    transparent_crc(g_314, "g_314", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_342[i][j], "g_342[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_392.f0, "g_392.f0", print_hash_value);
    transparent_crc(g_392.f1, "g_392.f1", print_hash_value);
    transparent_crc(g_392.f2, "g_392.f2", print_hash_value);
    transparent_crc(g_392.f3, "g_392.f3", print_hash_value);
    transparent_crc(g_392.f4, "g_392.f4", print_hash_value);
    transparent_crc(g_392.f5, "g_392.f5", print_hash_value);
    transparent_crc(g_392.f6, "g_392.f6", print_hash_value);
    transparent_crc(g_392.f7, "g_392.f7", print_hash_value);
    transparent_crc(g_400.f0, "g_400.f0", print_hash_value);
    transparent_crc(g_400.f1, "g_400.f1", print_hash_value);
    transparent_crc(g_400.f2, "g_400.f2", print_hash_value);
    transparent_crc(g_400.f3, "g_400.f3", print_hash_value);
    transparent_crc(g_400.f4, "g_400.f4", print_hash_value);
    transparent_crc(g_400.f5, "g_400.f5", print_hash_value);
    transparent_crc(g_400.f6, "g_400.f6", print_hash_value);
    transparent_crc(g_400.f7, "g_400.f7", print_hash_value);
    transparent_crc(g_452.f0, "g_452.f0", print_hash_value);
    transparent_crc(g_452.f1, "g_452.f1", print_hash_value);
    transparent_crc(g_452.f2, "g_452.f2", print_hash_value);
    transparent_crc(g_452.f3, "g_452.f3", print_hash_value);
    transparent_crc(g_452.f4, "g_452.f4", print_hash_value);
    transparent_crc(g_452.f5, "g_452.f5", print_hash_value);
    transparent_crc(g_452.f6, "g_452.f6", print_hash_value);
    transparent_crc(g_452.f7, "g_452.f7", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_481[i], "g_481[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_485, "g_485", print_hash_value);
    transparent_crc_bytes (&g_590, sizeof(g_590), "g_590", print_hash_value);
    transparent_crc(g_646.f0, "g_646.f0", print_hash_value);
    transparent_crc(g_646.f1, "g_646.f1", print_hash_value);
    transparent_crc(g_646.f2, "g_646.f2", print_hash_value);
    transparent_crc(g_646.f3, "g_646.f3", print_hash_value);
    transparent_crc(g_646.f4, "g_646.f4", print_hash_value);
    transparent_crc(g_646.f5, "g_646.f5", print_hash_value);
    transparent_crc(g_646.f6, "g_646.f6", print_hash_value);
    transparent_crc(g_646.f7, "g_646.f7", print_hash_value);
    transparent_crc(g_662, "g_662", print_hash_value);
    transparent_crc(g_663, "g_663", print_hash_value);
    transparent_crc(g_688, "g_688", print_hash_value);
    transparent_crc(g_690, "g_690", print_hash_value);
    transparent_crc(g_692, "g_692", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_699[i][j][k].f0, "g_699[i][j][k].f0", print_hash_value);
                transparent_crc(g_699[i][j][k].f1, "g_699[i][j][k].f1", print_hash_value);
                transparent_crc(g_699[i][j][k].f2, "g_699[i][j][k].f2", print_hash_value);
                transparent_crc(g_699[i][j][k].f3, "g_699[i][j][k].f3", print_hash_value);
                transparent_crc(g_699[i][j][k].f4, "g_699[i][j][k].f4", print_hash_value);
                transparent_crc(g_699[i][j][k].f5, "g_699[i][j][k].f5", print_hash_value);
                transparent_crc(g_699[i][j][k].f6, "g_699[i][j][k].f6", print_hash_value);
                transparent_crc(g_699[i][j][k].f7, "g_699[i][j][k].f7", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_761, "g_761", print_hash_value);
    transparent_crc(g_882.f0, "g_882.f0", print_hash_value);
    transparent_crc(g_882.f1, "g_882.f1", print_hash_value);
    transparent_crc(g_882.f2, "g_882.f2", print_hash_value);
    transparent_crc(g_882.f3, "g_882.f3", print_hash_value);
    transparent_crc(g_882.f4, "g_882.f4", print_hash_value);
    transparent_crc(g_882.f5, "g_882.f5", print_hash_value);
    transparent_crc(g_882.f6, "g_882.f6", print_hash_value);
    transparent_crc(g_882.f7, "g_882.f7", print_hash_value);
    transparent_crc(g_914, "g_914", print_hash_value);
    transparent_crc(g_1060, "g_1060", print_hash_value);
    transparent_crc(g_1069, "g_1069", print_hash_value);
    transparent_crc(g_1102, "g_1102", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1134[i], "g_1134[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1226, "g_1226", print_hash_value);
    transparent_crc(g_1332, "g_1332", print_hash_value);
    transparent_crc(g_1334.f0, "g_1334.f0", print_hash_value);
    transparent_crc(g_1334.f1, "g_1334.f1", print_hash_value);
    transparent_crc(g_1334.f2, "g_1334.f2", print_hash_value);
    transparent_crc(g_1334.f3, "g_1334.f3", print_hash_value);
    transparent_crc(g_1334.f4, "g_1334.f4", print_hash_value);
    transparent_crc(g_1334.f5, "g_1334.f5", print_hash_value);
    transparent_crc(g_1334.f6, "g_1334.f6", print_hash_value);
    transparent_crc(g_1334.f7, "g_1334.f7", print_hash_value);
    transparent_crc(g_1338.f0, "g_1338.f0", print_hash_value);
    transparent_crc(g_1338.f1, "g_1338.f1", print_hash_value);
    transparent_crc(g_1338.f2, "g_1338.f2", print_hash_value);
    transparent_crc(g_1338.f3, "g_1338.f3", print_hash_value);
    transparent_crc(g_1338.f4, "g_1338.f4", print_hash_value);
    transparent_crc(g_1338.f5, "g_1338.f5", print_hash_value);
    transparent_crc(g_1338.f6, "g_1338.f6", print_hash_value);
    transparent_crc(g_1338.f7, "g_1338.f7", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1350[i].f0, "g_1350[i].f0", print_hash_value);
        transparent_crc(g_1350[i].f1, "g_1350[i].f1", print_hash_value);
        transparent_crc(g_1350[i].f2, "g_1350[i].f2", print_hash_value);
        transparent_crc(g_1350[i].f3, "g_1350[i].f3", print_hash_value);
        transparent_crc(g_1350[i].f4, "g_1350[i].f4", print_hash_value);
        transparent_crc(g_1350[i].f5, "g_1350[i].f5", print_hash_value);
        transparent_crc(g_1350[i].f6, "g_1350[i].f6", print_hash_value);
        transparent_crc(g_1350[i].f7, "g_1350[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1400.f0, "g_1400.f0", print_hash_value);
    transparent_crc(g_1400.f1, "g_1400.f1", print_hash_value);
    transparent_crc(g_1400.f2, "g_1400.f2", print_hash_value);
    transparent_crc(g_1400.f3, "g_1400.f3", print_hash_value);
    transparent_crc(g_1400.f4, "g_1400.f4", print_hash_value);
    transparent_crc(g_1400.f5, "g_1400.f5", print_hash_value);
    transparent_crc(g_1400.f6, "g_1400.f6", print_hash_value);
    transparent_crc(g_1400.f7, "g_1400.f7", print_hash_value);
    transparent_crc(g_1430.f0, "g_1430.f0", print_hash_value);
    transparent_crc(g_1430.f1, "g_1430.f1", print_hash_value);
    transparent_crc(g_1430.f2, "g_1430.f2", print_hash_value);
    transparent_crc(g_1430.f3, "g_1430.f3", print_hash_value);
    transparent_crc(g_1430.f4, "g_1430.f4", print_hash_value);
    transparent_crc(g_1430.f5, "g_1430.f5", print_hash_value);
    transparent_crc(g_1430.f6, "g_1430.f6", print_hash_value);
    transparent_crc(g_1430.f7, "g_1430.f7", print_hash_value);
    transparent_crc(g_1490, "g_1490", print_hash_value);
    transparent_crc(g_1512, "g_1512", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_1733[i][j], "g_1733[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1800, "g_1800", print_hash_value);
    transparent_crc(g_1812.f0, "g_1812.f0", print_hash_value);
    transparent_crc(g_1812.f1, "g_1812.f1", print_hash_value);
    transparent_crc(g_1812.f2, "g_1812.f2", print_hash_value);
    transparent_crc(g_1812.f3, "g_1812.f3", print_hash_value);
    transparent_crc(g_1812.f4, "g_1812.f4", print_hash_value);
    transparent_crc(g_1812.f5, "g_1812.f5", print_hash_value);
    transparent_crc(g_1812.f6, "g_1812.f6", print_hash_value);
    transparent_crc(g_1812.f7, "g_1812.f7", print_hash_value);
    transparent_crc(g_1880.f0, "g_1880.f0", print_hash_value);
    transparent_crc(g_1880.f1, "g_1880.f1", print_hash_value);
    transparent_crc(g_1880.f2, "g_1880.f2", print_hash_value);
    transparent_crc(g_1880.f3, "g_1880.f3", print_hash_value);
    transparent_crc(g_1880.f4, "g_1880.f4", print_hash_value);
    transparent_crc(g_1880.f5, "g_1880.f5", print_hash_value);
    transparent_crc(g_1880.f6, "g_1880.f6", print_hash_value);
    transparent_crc(g_1880.f7, "g_1880.f7", print_hash_value);
    transparent_crc_bytes (&g_1896, sizeof(g_1896), "g_1896", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_2000[i][j], "g_2000[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2016, "g_2016", print_hash_value);
    transparent_crc(g_2221.f0, "g_2221.f0", print_hash_value);
    transparent_crc(g_2221.f1, "g_2221.f1", print_hash_value);
    transparent_crc(g_2221.f2, "g_2221.f2", print_hash_value);
    transparent_crc(g_2221.f3, "g_2221.f3", print_hash_value);
    transparent_crc(g_2221.f4, "g_2221.f4", print_hash_value);
    transparent_crc(g_2221.f5, "g_2221.f5", print_hash_value);
    transparent_crc(g_2221.f6, "g_2221.f6", print_hash_value);
    transparent_crc(g_2221.f7, "g_2221.f7", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2354[i].f0, "g_2354[i].f0", print_hash_value);
        transparent_crc(g_2354[i].f1, "g_2354[i].f1", print_hash_value);
        transparent_crc(g_2354[i].f2, "g_2354[i].f2", print_hash_value);
        transparent_crc(g_2354[i].f3, "g_2354[i].f3", print_hash_value);
        transparent_crc(g_2354[i].f4, "g_2354[i].f4", print_hash_value);
        transparent_crc(g_2354[i].f5, "g_2354[i].f5", print_hash_value);
        transparent_crc(g_2354[i].f6, "g_2354[i].f6", print_hash_value);
        transparent_crc(g_2354[i].f7, "g_2354[i].f7", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2394.f0, "g_2394.f0", print_hash_value);
    transparent_crc(g_2394.f1, "g_2394.f1", print_hash_value);
    transparent_crc(g_2394.f2, "g_2394.f2", print_hash_value);
    transparent_crc(g_2394.f3, "g_2394.f3", print_hash_value);
    transparent_crc(g_2394.f4, "g_2394.f4", print_hash_value);
    transparent_crc(g_2394.f5, "g_2394.f5", print_hash_value);
    transparent_crc(g_2394.f6, "g_2394.f6", print_hash_value);
    transparent_crc(g_2394.f7, "g_2394.f7", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2401[i], "g_2401[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 635
   depth: 1, occurrence: 17
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 8
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 2
XXX volatile bitfields defined in structs: 4
XXX structs with bitfields in the program: 47
breakdown:
   indirect level: 0, occurrence: 17
   indirect level: 1, occurrence: 12
   indirect level: 2, occurrence: 8
   indirect level: 3, occurrence: 3
   indirect level: 4, occurrence: 6
   indirect level: 5, occurrence: 1
XXX full-bitfields structs in the program: 17
breakdown:
   indirect level: 0, occurrence: 17
XXX times a bitfields struct's address is taken: 120
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 21
XXX times a single bitfield on LHS: 2
XXX times a single bitfield on RHS: 156

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 215
   depth: 2, occurrence: 67
   depth: 3, occurrence: 3
   depth: 4, occurrence: 4
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 8, occurrence: 2
   depth: 10, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 3
   depth: 16, occurrence: 1
   depth: 17, occurrence: 3
   depth: 18, occurrence: 3
   depth: 19, occurrence: 3
   depth: 20, occurrence: 2
   depth: 21, occurrence: 4
   depth: 23, occurrence: 4
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 3
   depth: 33, occurrence: 3
   depth: 34, occurrence: 2
   depth: 36, occurrence: 1
   depth: 38, occurrence: 1
   depth: 39, occurrence: 1
   depth: 41, occurrence: 1

XXX total number of pointers: 496

XXX times a variable address is taken: 1381
XXX times a pointer is dereferenced on RHS: 221
breakdown:
   depth: 1, occurrence: 168
   depth: 2, occurrence: 37
   depth: 3, occurrence: 8
   depth: 4, occurrence: 5
   depth: 5, occurrence: 3
XXX times a pointer is dereferenced on LHS: 344
breakdown:
   depth: 1, occurrence: 301
   depth: 2, occurrence: 37
   depth: 3, occurrence: 3
   depth: 4, occurrence: 3
XXX times a pointer is compared with null: 51
XXX times a pointer is compared with address of another variable: 4
XXX times a pointer is compared with another pointer: 14
XXX times a pointer is qualified to be dereferenced: 8787

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1020
   level: 2, occurrence: 327
   level: 3, occurrence: 74
   level: 4, occurrence: 56
   level: 5, occurrence: 21
XXX number of pointers point to pointers: 213
XXX number of pointers point to scalars: 267
XXX number of pointers point to structs: 16
XXX percent of pointers has null in alias set: 28.6
XXX average alias set size: 1.55

XXX times a non-volatile is read: 1882
XXX times a non-volatile is write: 1024
XXX times a volatile is read: 141
XXX    times read thru a pointer: 30
XXX times a volatile is write: 61
XXX    times written thru a pointer: 35
XXX times a volatile is available for access: 7.02e+03
XXX percentage of non-volatile access: 93.5

XXX forward jumps: 1
XXX backward jumps: 8

XXX stmts: 229
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 25
   depth: 1, occurrence: 31
   depth: 2, occurrence: 29
   depth: 3, occurrence: 42
   depth: 4, occurrence: 46
   depth: 5, occurrence: 56

XXX percentage a fresh-made variable is used: 16.9
XXX percentage an existing variable is used: 83.1
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


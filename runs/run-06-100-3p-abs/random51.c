/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3054312114
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   int32_t  f0;
   volatile int8_t  f1;
   uint32_t  f2;
   uint64_t  f3;
   volatile int64_t  f4;
   volatile uint32_t  f5;
   float  f6;
   int16_t  f7;
   uint64_t  f8;
   uint32_t  f9;
};

union U1 {
   uint16_t  f0;
   struct S0  f1;
   uint32_t  f2;
};

/* --- GLOBAL VARIABLES --- */
static uint32_t g_10[4][8][6] = {{{0xA43DEE6EL,0x7218670FL,0x8E487928L,0xEC2184E5L,0x9141A9C2L,0xEC2184E5L},{0UL,0xC593C869L,0UL,0x9141A9C2L,8UL,0x945CB81EL},{0UL,0xD91F06E8L,0x33C8A0C0L,6UL,0x4E2B79DCL,1UL},{0xC593C869L,4294967295UL,4294967286UL,6UL,5UL,0x9141A9C2L},{0UL,7UL,4294967295UL,0x9141A9C2L,0xA6C84902L,0xF57A5575L},{0UL,4294967286UL,0xA0F0F5EBL,0xEC2184E5L,0xFCB44481L,0x6B9C2D62L},{0xA43DEE6EL,0xA0F0F5EBL,8UL,8UL,4294967292UL,0x4C3F4F88L},{0x9141A9C2L,6UL,0xEC2184E5L,2UL,4294967294UL,5UL}},{{0xA445A8CCL,0xAC8E4C12L,0x7073E70DL,8UL,0UL,4294967295UL},{4294967293UL,0x945CB81EL,0x6B9C2D62L,4294967294UL,0xA6C84902L,4294967295UL},{0x6695CD1DL,0x4C3F4F88L,0xA6C84902L,0x7218670FL,0UL,0xE072305AL},{0xE8EA4716L,7UL,0xAC8E4C12L,5UL,4294967295UL,4294967293UL},{4294967291UL,1UL,3UL,0x2DD8607FL,0x2DD8607FL,3UL},{5UL,5UL,0x37280DDAL,0xE072305AL,0x7073E70DL,0xEC2184E5L},{0x8E487928L,0x60C437FFL,0UL,4294967295UL,2UL,0x37280DDAL},{4294967295UL,0x8E487928L,0UL,0x14AF12A8L,5UL,0xEC2184E5L}},{{0xA43DEE6EL,0x14AF12A8L,0x37280DDAL,8UL,0x8A52B4B5L,3UL},{8UL,0x8A52B4B5L,3UL,0x945CB81EL,0xE67E784BL,4294967293UL},{0xFCB44481L,0xF0842170L,0xAC8E4C12L,3UL,4294967286UL,0xE072305AL},{4294967286UL,0xC5EC62BBL,0xA6C84902L,4294967295UL,0x14AF12A8L,4294967295UL},{0x4E2B79DCL,4294967295UL,0x6B9C2D62L,4294967293UL,5UL,4294967295UL},{0x02002449L,0UL,0x7073E70DL,0xA0F0F5EBL,4294967295UL,5UL},{0x7073E70DL,4294967293UL,0x69A0165DL,0xD91F06E8L,1UL,0xDE769035L},{0xC5EC62BBL,0xA0F0F5EBL,4294967293UL,0x8A52B4B5L,0xCCA69919L,0xAC8E4C12L}},{{5UL,1UL,8UL,0x8E487928L,8UL,1UL},{0xEA0D018DL,4294967295UL,0x60C437FFL,4294967295UL,4294967295UL,7UL},{6UL,0xE67E784BL,0xA445A8CCL,0xDB7758B3L,0x8E487928L,0xD91F06E8L},{0x8A52B4B5L,0xE67E784BL,0x6695CD1DL,4294967293UL,4294967295UL,0xE8EA4716L},{1UL,4294967295UL,4294967295UL,0xA445A8CCL,8UL,0x69A0165DL},{0x37280DDAL,1UL,6UL,0x7073E70DL,0xCCA69919L,0x6695CD1DL},{0UL,0xA0F0F5EBL,4294967295UL,0xE67E784BL,1UL,4294967293UL},{0x60C437FFL,4294967293UL,4294967295UL,0xC5EC62BBL,4294967295UL,4294967292UL}}};
static uint16_t g_40[4][7][6] = {{{65529UL,0UL,65529UL,0x4B80L,65535UL,65529UL},{65529UL,0xCAFAL,0x4B80L,0x03BAL,0x7B88L,0x03BAL},{0x3645L,0xCAFAL,0x3645L,65529UL,65535UL,0x4B80L},{0x09CEL,0UL,0x3645L,0x09CEL,0xCAFAL,0x03BAL},{0x03BAL,0x0F91L,0x4B80L,0x09CEL,0x35CAL,65529UL},{0x09CEL,0x35CAL,65529UL,65529UL,0x35CAL,0x09CEL},{0x3645L,0x0F91L,65529UL,0x03BAL,0xCAFAL,0x09CEL}},{{65529UL,0UL,65529UL,0x8ECAL,65529UL,65535UL},{0x3153L,0x63F7L,0x8ECAL,0UL,0x09CEL,0UL},{0xD727L,0x63F7L,0xD727L,65535UL,65529UL,0x8ECAL},{0x18BAL,0x03BAL,0xD727L,0x18BAL,0x63F7L,0UL},{0UL,0x3645L,0x8ECAL,0x18BAL,65529UL,65535UL},{0x18BAL,65529UL,65535UL,65535UL,65529UL,0x18BAL},{0xD727L,0x3645L,0x3153L,0UL,0x63F7L,0x18BAL}},{{0x3153L,0x03BAL,65535UL,0x8ECAL,65529UL,65535UL},{0x3153L,0x63F7L,0x8ECAL,0UL,0x09CEL,0UL},{0xD727L,0x63F7L,0xD727L,65535UL,65529UL,0x8ECAL},{0x18BAL,0x03BAL,0xD727L,0x18BAL,0x63F7L,0UL},{0UL,0x3645L,0x8ECAL,0x18BAL,65529UL,65535UL},{0x18BAL,65529UL,65535UL,65535UL,65529UL,0x18BAL},{0xD727L,0x3645L,0x3153L,0UL,0x63F7L,0x18BAL}},{{0x3153L,0x03BAL,65535UL,0x8ECAL,65529UL,65535UL},{0x3153L,0x63F7L,0x8ECAL,0UL,0x09CEL,0UL},{0xD727L,0x63F7L,0xD727L,65535UL,65529UL,0x8ECAL},{0x18BAL,0x03BAL,0xD727L,0x18BAL,0x63F7L,0UL},{0UL,0x3645L,0x8ECAL,0x18BAL,65529UL,65535UL},{0x18BAL,65529UL,65535UL,65535UL,65529UL,0x18BAL},{0xD727L,0x3645L,0x3153L,0UL,0x63F7L,0x18BAL}}};
static float g_62 = (-0x1.5p-1);
static int32_t * volatile g_70 = (void*)0;/* VOLATILE GLOBAL g_70 */
static int32_t g_72 = 1L;
static int32_t * volatile g_71 = &g_72;/* VOLATILE GLOBAL g_71 */
static union U1 g_73[1][1][6] = {{{{0x2AFEL},{0x2AFEL},{0x2AFEL},{0x2AFEL},{0x2AFEL},{0x2AFEL}}}};
static int32_t *g_82 = &g_72;
static int32_t ** volatile g_81 = &g_82;/* VOLATILE GLOBAL g_81 */
static struct S0 g_83 = {-1L,0xFFL,0x5597B072L,1UL,4L,1UL,0xE.C64C76p+76,0x1E79L,0x7B2D6F4160CA180BLL,0UL};/* VOLATILE GLOBAL g_83 */
static uint16_t *g_98 = &g_40[1][4][2];
static uint16_t **g_97 = &g_98;
static int32_t g_101[9][7] = {{(-8L),(-8L),1L,0L,(-1L),0L,1L},{0xC029035EL,(-1L),0x57036DBDL,0xA53E6A3FL,0x8FB3CF24L,0xA53E6A3FL,0x57036DBDL},{(-8L),(-8L),1L,0L,(-1L),0L,1L},{0xC029035EL,(-1L),0x57036DBDL,0xA53E6A3FL,0x8FB3CF24L,0xA53E6A3FL,0x57036DBDL},{(-8L),(-8L),1L,0L,(-1L),0L,1L},{0xC029035EL,(-1L),0x57036DBDL,0xA53E6A3FL,0x8FB3CF24L,0xA53E6A3FL,0x57036DBDL},{(-8L),(-8L),1L,0L,(-1L),0L,1L},{0xC029035EL,(-1L),0x57036DBDL,0xA53E6A3FL,0x8FB3CF24L,0xA53E6A3FL,0x57036DBDL},{(-8L),(-8L),1L,0L,(-1L),0x8E4F1F3AL,(-8L)}};
static uint64_t g_102 = 18446744073709551612UL;
static uint16_t *** const  volatile g_106 = &g_97;/* VOLATILE GLOBAL g_106 */
static int64_t g_111 = 0L;
static int64_t *g_151 = (void*)0;
static int64_t **g_150 = &g_151;
static int32_t ** volatile g_159 = &g_82;/* VOLATILE GLOBAL g_159 */
static struct S0 g_175 = {-1L,-8L,0UL,0x60FE60D97CCDA527LL,0x6C95E86A8CA0577ELL,0UL,0x1.Bp-1,0xE392L,18446744073709551615UL,0x8496B461L};/* VOLATILE GLOBAL g_175 */
static volatile int32_t g_185 = 0xADFB77FFL;/* VOLATILE GLOBAL g_185 */
static uint32_t g_187 = 9UL;
static int32_t ** volatile g_196 = &g_82;/* VOLATILE GLOBAL g_196 */
static int8_t g_215 = 0x46L;
static const int16_t **g_220 = (void*)0;
static volatile struct S0 g_270 = {0x8742192EL,0L,0UL,18446744073709551615UL,0x4D3F08EF28FBD838LL,18446744073709551614UL,-0x7.Ep-1,0x2CC2L,0x1256004775444CAALL,0x2A4950D6L};/* VOLATILE GLOBAL g_270 */
static uint8_t g_280 = 0UL;
static int32_t ** volatile g_300 = &g_82;/* VOLATILE GLOBAL g_300 */
static int8_t *g_373 = &g_215;
static volatile union U1 g_399[5] = {{1UL},{1UL},{1UL},{1UL},{1UL}};
static int64_t g_418 = 0xB32F4127E0A07396LL;
static volatile uint32_t *g_447[10][7][3] = {{{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,&g_270.f2}},{{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,(void*)0,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2}},{{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,&g_270.f2}},{{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2}},{{&g_270.f2,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,(void*)0,(void*)0}},{{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2}},{{&g_270.f2,(void*)0,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,&g_270.f2,&g_270.f2}},{{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2}},{{(void*)0,&g_270.f2,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,(void*)0,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2},{(void*)0,(void*)0,&g_270.f2}},{{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,&g_270.f2},{&g_270.f2,&g_270.f2,&g_270.f2},{&g_270.f2,(void*)0,(void*)0},{&g_270.f2,&g_270.f2,&g_270.f2}}};
static volatile uint32_t **g_446[10] = {&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0],&g_447[2][6][0]};
static uint16_t ***g_448 = &g_97;
static float * volatile g_454 = &g_83.f6;/* VOLATILE GLOBAL g_454 */
static float * volatile g_472[4] = {&g_83.f6,&g_83.f6,&g_83.f6,&g_83.f6};
static int32_t ** volatile g_474 = &g_82;/* VOLATILE GLOBAL g_474 */
static int16_t *g_490[4][7][5] = {{{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7}},{{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0}},{{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7}},{{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0},{&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7,&g_83.f7},{(void*)0,&g_83.f7,(void*)0,&g_83.f7,(void*)0}}};
static int16_t **g_489[8][8] = {{&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],(void*)0},{&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][6][0],&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0]},{&g_490[0][0][0],&g_490[0][0][0],(void*)0,&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0]},{&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0]},{&g_490[0][0][0],&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][6][0],&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0]},{&g_490[0][0][0],&g_490[0][0][0],(void*)0,&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0]},{&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0]},{&g_490[0][0][0],&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0],&g_490[0][6][0],&g_490[0][6][0],&g_490[0][0][0],&g_490[0][0][0]}};
static volatile struct S0 g_494 = {0x6C197C7AL,-8L,0x72066F9AL,18446744073709551615UL,-5L,0UL,0x2.F7EDB5p-83,-4L,0xC58335E80B8834E9LL,0xDA3FBF01L};/* VOLATILE GLOBAL g_494 */
static volatile union U1 g_495 = {65528UL};/* VOLATILE GLOBAL g_495 */
static const float g_509[5] = {0x0.ADAB81p-52,0x0.ADAB81p-52,0x0.ADAB81p-52,0x0.ADAB81p-52,0x0.ADAB81p-52};
static struct S0 g_536 = {0x9322263EL,0x51L,4294967287UL,0xA72FA405E61A48D7LL,-8L,0x2332D144L,0xF.8DA135p-51,0x1015L,0x647245270478B1D8LL,0x45B99B50L};/* VOLATILE GLOBAL g_536 */
static struct S0 * volatile g_537 = &g_536;/* VOLATILE GLOBAL g_537 */
static struct S0 * volatile g_538 = &g_73[0][0][1].f1;/* VOLATILE GLOBAL g_538 */
static int32_t g_541[3][2][10] = {{{0x1403C766L,9L,0x8A7EA64CL,9L,0x1403C766L,0xDF2D228FL,7L,0x646DAE79L,0x15E6D43FL,(-6L)},{1L,0x295B4CC5L,0x8A7EA64CL,0x646DAE79L,0x9B56A82AL,0x646DAE79L,0x8A7EA64CL,0x295B4CC5L,1L,(-6L)}},{{0x15E6D43FL,0x646DAE79L,7L,0xDF2D228FL,0x1403C766L,9L,0x8A7EA64CL,9L,0x1403C766L,0xDF2D228FL},{0L,0x295B4CC5L,0L,0xDF2D228FL,0x8A7EA64CL,(-6L),7L,0xAFCC6C0EL,1L,0xAFCC6C0EL}},{{0L,9L,1L,0x646DAE79L,1L,9L,0L,0xAFCC6C0EL,0x15E6D43FL,0x295B4CC5L},{0x15E6D43FL,0xAFCC6C0EL,0L,9L,1L,0x646DAE79L,1L,9L,0L,0xAFCC6C0EL}}};
static const struct S0 g_547 = {0L,0L,0xCE6332B8L,0x4FED21FE613C0B62LL,1L,1UL,0x0.Dp+1,0x5F47L,18446744073709551615UL,0x24DCE0FCL};/* VOLATILE GLOBAL g_547 */
static struct S0 g_548 = {0x1F315753L,-1L,0x1D5DA4B8L,0xA37BEC88CB0C1F3DLL,0x2645FC40BD5225C5LL,1UL,0x3.C01E43p+26,-1L,0x6633D5E6A82CC034LL,9UL};/* VOLATILE GLOBAL g_548 */
static volatile int32_t g_561 = 0xC30C153AL;/* VOLATILE GLOBAL g_561 */
static volatile union U1 g_572 = {0xEEAEL};/* VOLATILE GLOBAL g_572 */
static struct S0 * volatile g_607 = &g_548;/* VOLATILE GLOBAL g_607 */
static struct S0 g_619[1] = {{0xA23BB4DFL,0x2FL,0UL,1UL,4L,0UL,0x0.9101AAp+74,0x37DAL,1UL,0UL}};
static union U1 g_626 = {0xF00CL};/* VOLATILE GLOBAL g_626 */
static const uint8_t **g_646 = (void*)0;
static const uint8_t *** volatile g_647 = &g_646;/* VOLATILE GLOBAL g_647 */
static const union U1 g_672 = {0UL};/* VOLATILE GLOBAL g_672 */
static uint8_t *g_675 = &g_280;
static int32_t ** volatile g_681[5][2] = {{&g_82,&g_82},{&g_82,&g_82},{&g_82,&g_82},{&g_82,&g_82},{&g_82,&g_82}};
static int32_t ** volatile g_682 = &g_82;/* VOLATILE GLOBAL g_682 */
static volatile union U1 g_696 = {65535UL};/* VOLATILE GLOBAL g_696 */
static struct S0 g_766 = {0xDDCD6216L,-1L,0xD1E52554L,9UL,-1L,18446744073709551613UL,0x3.C6DCF4p-55,0xDB66L,18446744073709551615UL,0x30F7C476L};/* VOLATILE GLOBAL g_766 */
static volatile int64_t g_800 = 0xBA866DF81D321637LL;/* VOLATILE GLOBAL g_800 */
static volatile float g_806 = 0x1.9p+1;/* VOLATILE GLOBAL g_806 */
static volatile float *g_805 = &g_806;
static volatile float * volatile * const g_804 = &g_805;
static struct S0 g_839 = {0x16C2B2BCL,1L,4294967295UL,0x522D854650BC1107LL,0x7304C8CCFB5BE479LL,18446744073709551606UL,0x1.59C7EFp-1,3L,0x6EB74E64BAC3C388LL,0x08860954L};/* VOLATILE GLOBAL g_839 */
static int32_t ** const g_877 = (void*)0;
static int32_t ** const *g_876[8][2][1] = {{{&g_877},{&g_877}},{{&g_877},{&g_877}},{{&g_877},{&g_877}},{{&g_877},{&g_877}},{{&g_877},{&g_877}},{{&g_877},{&g_877}},{{&g_877},{&g_877}},{{&g_877},{&g_877}}};
static volatile struct S0 g_937[4] = {{1L,0xC6L,0x380CB9F1L,0x70ABDF2016FE0414LL,0x8758CD77F642B3EFLL,0xD0524F96L,-0x10.8p-1,-1L,0xBA51159CFD1783D1LL,18446744073709551615UL},{1L,0xC6L,0x380CB9F1L,0x70ABDF2016FE0414LL,0x8758CD77F642B3EFLL,0xD0524F96L,-0x10.8p-1,-1L,0xBA51159CFD1783D1LL,18446744073709551615UL},{1L,0xC6L,0x380CB9F1L,0x70ABDF2016FE0414LL,0x8758CD77F642B3EFLL,0xD0524F96L,-0x10.8p-1,-1L,0xBA51159CFD1783D1LL,18446744073709551615UL},{1L,0xC6L,0x380CB9F1L,0x70ABDF2016FE0414LL,0x8758CD77F642B3EFLL,0xD0524F96L,-0x10.8p-1,-1L,0xBA51159CFD1783D1LL,18446744073709551615UL}};
static struct S0 g_989 = {-1L,0xAEL,0xEB58B4A0L,0x1323FF190FDAAFD3LL,0x0C073A5932B90C5BLL,18446744073709551615UL,0xC.034834p-27,0x121CL,1UL,0x9A30A1DFL};/* VOLATILE GLOBAL g_989 */
static struct S0 * volatile g_990 = &g_619[0];/* VOLATILE GLOBAL g_990 */
static int16_t * volatile *g_997 = &g_490[0][0][0];
static int16_t * volatile * volatile *g_996 = &g_997;
static int16_t * volatile * volatile ** const  volatile g_995 = &g_996;/* VOLATILE GLOBAL g_995 */
static int16_t * volatile * volatile ** volatile g_999 = &g_996;/* VOLATILE GLOBAL g_999 */
static int16_t * volatile * volatile ** volatile * volatile g_998 = &g_999;/* VOLATILE GLOBAL g_998 */
static uint8_t g_1022 = 0x5FL;
static uint8_t **g_1061 = (void*)0;
static int64_t g_1065 = 1L;
static int32_t * volatile g_1066 = &g_72;/* VOLATILE GLOBAL g_1066 */
static union U1 g_1072[10] = {{0UL},{0UL},{0UL},{0UL},{0UL},{0UL},{0UL},{0UL},{0UL},{0UL}};
static int32_t ** volatile g_1099 = &g_82;/* VOLATILE GLOBAL g_1099 */
static volatile struct S0 g_1113[9] = {{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL},{-2L,1L,4294967288UL,6UL,8L,1UL,0xC.168E83p-14,0xAE83L,0xB456D8E6AC9EA6F0LL,18446744073709551615UL}};
static int32_t * volatile g_1125 = (void*)0;/* VOLATILE GLOBAL g_1125 */
static volatile uint64_t g_1150 = 6UL;/* VOLATILE GLOBAL g_1150 */
static uint16_t * const g_1180 = (void*)0;
static uint16_t * const *g_1179[9][2][6] = {{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}},{{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180},{&g_1180,&g_1180,&g_1180,&g_1180,&g_1180,&g_1180}}};
static uint16_t * const **g_1178 = &g_1179[6][1][3];
static int32_t * volatile g_1207 = &g_1072[4].f1.f0;/* VOLATILE GLOBAL g_1207 */
static int32_t * volatile g_1208[7][10] = {{&g_536.f0,&g_83.f0,&g_83.f0,&g_83.f0,&g_83.f0,&g_536.f0,&g_83.f0,&g_766.f0,&g_175.f0,&g_619[0].f0},{&g_536.f0,&g_83.f0,&g_766.f0,&g_175.f0,&g_619[0].f0,&g_536.f0,&g_619[0].f0,&g_619[0].f0,(void*)0,&g_541[0][1][3]},{&g_536.f0,&g_619[0].f0,&g_619[0].f0,(void*)0,&g_541[0][1][3],&g_536.f0,&g_541[0][1][3],(void*)0,&g_619[0].f0,&g_619[0].f0},{&g_536.f0,&g_541[0][1][3],(void*)0,&g_619[0].f0,&g_619[0].f0,&g_536.f0,&g_619[0].f0,&g_175.f0,&g_766.f0,&g_83.f0},{&g_536.f0,&g_619[0].f0,&g_175.f0,&g_766.f0,&g_83.f0,&g_536.f0,&g_83.f0,&g_83.f0,&g_83.f0,&g_83.f0},{&g_536.f0,&g_83.f0,&g_83.f0,&g_83.f0,&g_83.f0,&g_536.f0,&g_83.f0,&g_766.f0,&g_175.f0,&g_619[0].f0},{&g_536.f0,&g_83.f0,&g_766.f0,&g_175.f0,&g_619[0].f0,&g_536.f0,&g_619[0].f0,&g_619[0].f0,(void*)0,&g_541[0][1][3]}};
static const struct S0 g_1212[8] = {{1L,9L,4294967290UL,18446744073709551613UL,0xB182EE12E582DEEALL,1UL,0xE.AE585Dp-57,1L,0x0E2E69594ECD6E00LL,0UL},{0x52AD8AC2L,0x6FL,4294967295UL,18446744073709551615UL,-2L,0x18E8A64CL,0xA.53DBEFp+64,0x29B6L,0x7ED35BC2DB24258ELL,0x83DE6534L},{1L,9L,4294967290UL,18446744073709551613UL,0xB182EE12E582DEEALL,1UL,0xE.AE585Dp-57,1L,0x0E2E69594ECD6E00LL,0UL},{1L,9L,4294967290UL,18446744073709551613UL,0xB182EE12E582DEEALL,1UL,0xE.AE585Dp-57,1L,0x0E2E69594ECD6E00LL,0UL},{0x52AD8AC2L,0x6FL,4294967295UL,18446744073709551615UL,-2L,0x18E8A64CL,0xA.53DBEFp+64,0x29B6L,0x7ED35BC2DB24258ELL,0x83DE6534L},{1L,9L,4294967290UL,18446744073709551613UL,0xB182EE12E582DEEALL,1UL,0xE.AE585Dp-57,1L,0x0E2E69594ECD6E00LL,0UL},{1L,9L,4294967290UL,18446744073709551613UL,0xB182EE12E582DEEALL,1UL,0xE.AE585Dp-57,1L,0x0E2E69594ECD6E00LL,0UL},{0x52AD8AC2L,0x6FL,4294967295UL,18446744073709551615UL,-2L,0x18E8A64CL,0xA.53DBEFp+64,0x29B6L,0x7ED35BC2DB24258ELL,0x83DE6534L}};
static struct S0 g_1213[10] = {{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL},{9L,0x2BL,4294967295UL,0xB52060F8CCC02CF6LL,0L,0x925EA1AAL,0x1.Ap-1,-1L,5UL,18446744073709551615UL}};
static volatile int8_t g_1344 = 0xCFL;/* VOLATILE GLOBAL g_1344 */
static volatile int8_t * volatile g_1343 = &g_1344;/* VOLATILE GLOBAL g_1343 */
static volatile int8_t * volatile * volatile g_1342 = &g_1343;/* VOLATILE GLOBAL g_1342 */
static volatile int8_t * volatile * volatile *g_1341 = &g_1342;
static const int8_t *g_1347 = (void*)0;
static const int8_t * const *g_1346 = &g_1347;
static const int8_t * const **g_1345 = &g_1346;
static int16_t g_1388 = 0x78FFL;
static volatile struct S0 g_1397[1] = {{0L,0x33L,4294967295UL,0UL,-1L,18446744073709551610UL,0x1.2p-1,0L,0xA27A3C24009ACB66LL,2UL}};
static volatile struct S0 * volatile g_1398 = (void*)0;/* VOLATILE GLOBAL g_1398 */
static int16_t * const  volatile ** volatile g_1417 = (void*)0;/* VOLATILE GLOBAL g_1417 */
static int8_t g_1440[1] = {0x3DL};
static struct S0 *g_1466[4][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_619[0],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_619[0],&g_619[0],(void*)0,&g_619[0]}};
static int16_t **g_1489 = &g_490[1][6][1];
static int16_t *** const g_1488 = &g_1489;
static int16_t *** const *g_1487 = &g_1488;
static volatile struct S0 g_1492 = {0L,5L,0xFC9AF8D6L,0x53168AE2F84A6F9FLL,0L,1UL,0x4.79B2E7p-15,0xD0D8L,0x095485CF768C1572LL,4UL};/* VOLATILE GLOBAL g_1492 */
static volatile struct S0 g_1495 = {0x56F25DD3L,0x44L,1UL,0x6C02F66A640ECE44LL,-1L,0x4958CAC4L,-0x4.1p-1,0x342CL,0xAA32E7BD4A7035B0LL,0x64D5F5D0L};/* VOLATILE GLOBAL g_1495 */
static union U1 g_1499 = {0xF656L};/* VOLATILE GLOBAL g_1499 */


/* --- FORWARD DECLARATIONS --- */
static union U1  func_1(void);
static int32_t  func_4(uint32_t  p_5, float  p_6, uint32_t  p_7, uint16_t  p_8, int32_t  p_9);
static uint8_t  func_14(uint16_t  p_15, int8_t  p_16, int16_t  p_17);
static uint32_t  func_22(uint32_t  p_23, uint8_t  p_24, uint64_t  p_25, int8_t  p_26, int8_t  p_27);
static uint8_t  func_28(int8_t  p_29, uint64_t  p_30, int32_t  p_31, uint8_t  p_32);
static int8_t  func_33(int8_t  p_34, uint32_t  p_35, float  p_36, uint32_t  p_37);
static uint16_t ** func_44(uint16_t  p_45, uint32_t  p_46, uint16_t * const  p_47, uint16_t ** const  p_48);
static int16_t  func_49(uint16_t * p_50, uint16_t ** p_51, uint16_t ** p_52, uint16_t * p_53);
static int8_t  func_58(float  p_59, uint16_t ** p_60);
static union U1  func_63(uint16_t ** const  p_64, int8_t  p_65);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_10 g_40 g_71 g_72 g_73 g_81 g_83 g_97 g_98 g_82 g_102 g_106 g_159 g_989 g_990 g_766.f8 g_995 g_998 g_675 g_280 g_373 g_215 g_454 g_805 g_300 g_619 g_804 g_111 g_1065 g_1066 g_626.f0 g_541 g_1072 g_1099 g_1113 g_1150 g_999 g_996 g_448 g_175.f8 g_1212 g_1213 g_536.f9 g_536.f0 g_175.f0 g_937.f9 g_937.f1 g_766.f0 g_547.f5 g_839.f0 g_1341 g_1345 g_175.f9 g_647 g_646 g_1397 g_1342 g_1343 g_1344 g_766.f3 g_1417 g_1346 g_1492 g_1347 g_270.f5 g_1495 g_1499
 * writes: g_40 g_72 g_62 g_82 g_83 g_97 g_101 g_102 g_73.f1.f7 g_150 g_619 g_766.f8 g_999 g_175.f7 g_280 g_1022 g_806 g_626.f0 g_111 g_1061 g_215 g_1072.f1 g_989.f3 g_839.f2 g_548.f0 g_1150 g_1178 g_839.f9 g_175.f8 g_175 g_536.f9 g_989.f9 g_989.f0 g_536.f2 g_1065 g_418 g_839.f7 g_536.f7 g_1345 g_626.f1.f8 g_495.f1 g_626.f1.f6 g_1466 g_1487
 */
static union U1  func_1(void)
{ /* block id: 0 */
    uint8_t l_11 = 0UL;
    uint16_t *l_39 = &g_40[0][5][0];
    int16_t l_988 = (-1L);
    uint64_t *l_1202 = &g_83.f3;
    uint64_t *l_1203 = &g_175.f8;
    int16_t *l_1204 = &g_175.f7;
    int16_t l_1211 = (-1L);
    int16_t l_1247 = 0x9A73L;
    int32_t *l_1252 = &g_175.f0;
    int64_t l_1271 = 0x9E18B48FFE0E43E0LL;
    int32_t l_1301 = 1L;
    int32_t l_1309 = 1L;
    int32_t l_1311 = 0x878AE418L;
    int32_t l_1313 = (-1L);
    int32_t l_1315 = 0L;
    int32_t l_1316 = 0xBD97A62DL;
    float l_1381 = 0x3.FED499p-38;
    int16_t l_1387[10][3];
    int32_t l_1389 = 3L;
    int32_t l_1391 = 0xDF98B728L;
    float l_1414 = 0x9.F59200p+15;
    uint16_t ** const *l_1425[1][9][8] = {{{&g_97,&g_97,&g_97,(void*)0,&g_97,&g_97,(void*)0,&g_97},{&g_97,&g_97,&g_97,&g_97,(void*)0,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,(void*)0,&g_97,(void*)0,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97},{&g_97,(void*)0,(void*)0,&g_97,&g_97,&g_97,&g_97,&g_97}}};
    int8_t **l_1437 = &g_373;
    int32_t *l_1442 = &l_1311;
    int32_t *l_1443 = &g_839.f0;
    int32_t *l_1444 = &l_1391;
    int32_t *l_1445[2];
    int64_t l_1446 = 0L;
    uint32_t l_1447 = 0xFF326D3BL;
    int32_t *l_1450 = &g_83.f0;
    uint16_t *l_1455 = (void*)0;
    uint16_t *l_1456[4][1][9] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3]}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3],&g_40[0][5][3]}}};
    struct S0 *l_1463[10];
    struct S0 **l_1464 = (void*)0;
    struct S0 **l_1465[1];
    struct S0 *l_1471 = &g_175;
    uint32_t l_1472 = 0UL;
    const int16_t ***l_1491 = (void*)0;
    const int16_t ****l_1490[10][10][2] = {{{&l_1491,&l_1491},{(void*)0,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0},{&l_1491,&l_1491}},{{(void*)0,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,(void*)0},{&l_1491,&l_1491},{(void*)0,(void*)0},{&l_1491,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491}},{{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0},{&l_1491,(void*)0},{(void*)0,&l_1491},{&l_1491,(void*)0},{(void*)0,&l_1491},{&l_1491,&l_1491}},{{&l_1491,&l_1491},{(void*)0,&l_1491},{&l_1491,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,&l_1491}},{{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0}},{{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491}},{{&l_1491,(void*)0},{(void*)0,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0},{(void*)0,&l_1491},{&l_1491,&l_1491},{(void*)0,(void*)0},{&l_1491,&l_1491}},{{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491}},{{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,&l_1491},{&l_1491,&l_1491}},{{&l_1491,&l_1491},{&l_1491,(void*)0},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{&l_1491,&l_1491},{(void*)0,&l_1491}}};
    int i, j, k;
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 3; j++)
            l_1387[i][j] = 1L;
    }
    for (i = 0; i < 2; i++)
        l_1445[i] = &l_1301;
    for (i = 0; i < 10; i++)
        l_1463[i] = &g_1213[8];
    for (i = 0; i < 1; i++)
        l_1465[i] = &l_1463[3];
    if ((safe_add_func_int32_t_s_s(func_4(g_10[0][7][0], ((((l_11 != (safe_mod_func_int64_t_s_s((g_10[0][7][0] | ((*l_1204) = (func_14((g_10[0][7][0] & ((safe_mod_func_int16_t_s_s(4L, ((*l_1204) = (((*l_1203) &= ((*l_1202) = (safe_div_func_int16_t_s_s((func_22((func_28(l_11, g_10[0][5][3], ((((func_33((((+((*l_39) |= l_11)) <= (1UL < l_11)) <= 0x41E226996DCEBFDCLL), l_11, g_10[0][2][3], g_10[0][7][0]) ^ l_11) >= l_11) && 1UL) <= l_11), l_11) , l_988), l_988, g_10[1][5][4], l_988, l_988) || l_11), 0x4912L)))) || l_11)))) , 7UL)), l_11, l_11) || l_11))), l_988))) < l_1211) >= 18446744073709551615UL) , 0x1.5p-1), l_1211, l_1211, l_1211), 0x38D3953EL)))
    { /* block id: 591 */
        int32_t *l_1241 = (void*)0;
        int32_t *l_1242 = &g_989.f0;
        int8_t *l_1280 = &g_215;
        int32_t l_1306 = 0xB677A558L;
        int32_t l_1307 = 5L;
        int32_t l_1308 = (-1L);
        int32_t l_1310[7] = {0x265A8BE1L,(-7L),0x265A8BE1L,0x265A8BE1L,(-7L),0x265A8BE1L,0x265A8BE1L};
        int32_t l_1312 = 0xB212407EL;
        uint8_t l_1365 = 255UL;
        int16_t l_1380[6][4] = {{0L,(-1L),(-6L),(-1L)},{(-1L),(-4L),0x5539L,(-1L)},{0x5539L,(-1L),(-1L),(-1L)},{0x6E83L,0x6E83L,(-6L),0L},{0x6E83L,(-4L),(-1L),0x6E83L},{0x5539L,0L,0x5539L,(-1L)}};
        int16_t l_1390 = (-1L);
        float l_1409 = 0x3.DCE286p+95;
        int16_t ***l_1416[1][2];
        const int64_t l_1418 = (-6L);
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
                l_1416[i][j] = &g_489[7][1];
        }
lbl_1292:
        (*l_1242) &= 0x7DB14B86L;
        for (g_536.f2 = (-7); (g_536.f2 < 29); g_536.f2 = safe_add_func_uint64_t_u_u(g_536.f2, 2))
        { /* block id: 595 */
            int32_t l_1260 = 0x8EF48E4BL;
            int32_t **l_1262 = &g_82;
            int32_t **l_1263 = &g_82;
            int32_t **l_1264 = &g_82;
            int32_t **l_1265 = (void*)0;
            int32_t **l_1266 = &g_82;
            int32_t **l_1267 = &g_82;
            int32_t **l_1268 = &g_82;
            int32_t **l_1269 = &l_1242;
            int32_t l_1270 = 0x58E5D45EL;
            const struct S0 * const l_1299 = &g_83;
            const struct S0 * const *l_1298 = &l_1299;
            int32_t l_1314 = 8L;
            int16_t l_1332 = 1L;
            const float *l_1352[6][3] = {{&g_509[3],&g_509[3],&g_1213[8].f6},{&g_536.f6,&g_1213[8].f6,&g_1213[8].f6},{&g_1213[8].f6,&g_509[1],&g_1212[7].f6},{&g_536.f6,&g_509[1],&g_536.f6},{&g_509[3],&g_1213[8].f6,&g_1212[7].f6},{&g_509[3],&g_509[3],&g_1213[8].f6}};
            int32_t l_1361 = 0x7230AC6FL;
            int32_t l_1362 = 0xD66C481CL;
            int32_t l_1364[9][5][5] = {{{0x82F72B8AL,0x5A64ECD7L,4L,1L,(-1L)},{0xB4394C21L,1L,4L,0L,0xCE351731L},{0x67AB432CL,0x71B39E23L,0x71B39E23L,0x67AB432CL,(-3L)},{0x815630DEL,0xCE351731L,0xD55AB4AFL,(-1L),6L},{0xEC257D70L,8L,6L,1L,(-6L)}},{{(-5L),0xF43877D9L,(-6L),(-1L),1L},{0x43B4B5F2L,4L,0xD5DB5DD7L,1L,1L},{0xD6E12A7FL,0L,0x82F72B8AL,0xB4394C21L,0xA9314392L},{0xF43877D9L,0x43B4B5F2L,0xB4394C21L,8L,0xA9314392L},{0xCE351731L,1L,0x67AB432CL,0x67AB432CL,1L}},{{1L,0x67AB432CL,0x815630DEL,0xDEB68A86L,1L},{8L,1L,0xFA028F45L,(-3L),(-6L)},{1L,0x60C7CE78L,4L,(-5L),5L},{8L,(-1L),0xA9314392L,0x60C7CE78L,0xC4F19C7EL},{1L,0x5A64ECD7L,0xCE351731L,0xFA028F45L,0xD6E12A7FL}},{{0xCE351731L,0x82F72B8AL,1L,(-6L),0xD55AB4AFL},{0xF43877D9L,0x82F72B8AL,0xE6D38ED7L,0L,0L},{0xD6E12A7FL,0x5A64ECD7L,0xD6E12A7FL,1L,0x43B4B5F2L},{0x43B4B5F2L,(-1L),1L,4L,0x5A64ECD7L},{(-5L),0x60C7CE78L,0xDEB68A86L,0xD6E12A7FL,1L}},{{0xEC257D70L,1L,1L,0x5A64ECD7L,0x815630DEL},{(-1L),0x67AB432CL,0xD6E12A7FL,0L,0x82F72B8AL},{1L,1L,0xE6D38ED7L,0xC4F19C7EL,(-4L)},{0x67AB432CL,0x43B4B5F2L,1L,0xC4F19C7EL,8L},{4L,0L,0xCE351731L,0L,4L}},{{0xDEB68A86L,4L,0xA9314392L,0x5A64ECD7L,0xE6D38ED7L},{0xFA028F45L,0xF43877D9L,4L,0xD6E12A7FL,0x60C7CE78L},{5L,8L,0xFA028F45L,4L,0xE6D38ED7L},{(-3L),0xD6E12A7FL,0x815630DEL,1L,4L},{0xE6D38ED7L,0x659FCC28L,0x67AB432CL,0L,8L}},{{0x815630DEL,0xD5DB5DD7L,0xB4394C21L,(-6L),(-4L)},{0x815630DEL,0x71B39E23L,0x82F72B8AL,0xFA028F45L,0x82F72B8AL},{0xE6D38ED7L,0xE6D38ED7L,0xD5DB5DD7L,0x60C7CE78L,0x815630DEL},{(-3L),0xEC257D70L,(-6L),(-5L),1L},{5L,1L,6L,(-3L),0x5A64ECD7L}},{{0xFA028F45L,0xEC257D70L,0L,0xDEB68A86L,0x43B4B5F2L},{0xDEB68A86L,0xE6D38ED7L,0x659FCC28L,0x67AB432CL,0L},{4L,0x71B39E23L,(-5L),8L,0xD55AB4AFL},{0x67AB432CL,0xD5DB5DD7L,(-5L),0xB4394C21L,0xD6E12A7FL},{1L,0x659FCC28L,0x659FCC28L,1L,0xC4F19C7EL}},{{(-1L),0xD6E12A7FL,0L,(-1L),5L},{0xEC257D70L,8L,6L,1L,(-6L)},{0x815630DEL,(-6L),0xFA028F45L,0xD55AB4AFL,0xD5DB5DD7L},{0L,(-5L),0xDEB68A86L,0xD5DB5DD7L,8L},{1L,0xB4394C21L,4L,0x67AB432CL,0x43B4B5F2L}}};
            uint32_t l_1394 = 0xB652967EL;
            int i, j, k;
            (*l_1242) = (-1L);
            if (l_988)
                continue;
            if ((safe_sub_func_int64_t_s_s((l_1247 == (0x2AL <= (safe_mul_func_int8_t_s_s(((((safe_div_func_float_f_f((&g_215 != (void*)0), (l_1252 == ((*l_1269) = ((safe_add_func_int16_t_s_s((+(safe_rshift_func_uint8_t_u_s((*l_1242), (*l_1242)))), ((l_1260 && (!l_1260)) || 0x5237L))) , l_1252))))) <= (*l_1252)) < 0x1.8p+1) , l_1270), 255UL)))), (*l_1252))))
            { /* block id: 599 */
                int8_t *l_1279 = &g_215;
                int32_t l_1281 = 0xDBE496C8L;
                int32_t l_1291[10][4] = {{0L,0L,0x53E87788L,0L},{0L,0xA03C3925L,0xA03C3925L,0L},{0xA03C3925L,0L,0xA03C3925L,0xA03C3925L},{0L,0L,0x53E87788L,0L},{0L,0xA03C3925L,0xA03C3925L,0L},{0xA03C3925L,0L,0xA03C3925L,0xA03C3925L},{0L,0L,0x53E87788L,0L},{0L,0xA03C3925L,0xA03C3925L,0L},{0xA03C3925L,0L,0xA03C3925L,0xA03C3925L},{0L,0L,0x53E87788L,0L}};
                const uint64_t l_1297 = 0xD9CE60B7A5D9E602LL;
                int64_t l_1302 = 0x7602B97DF5742DFBLL;
                uint8_t l_1317 = 0xE8L;
                int64_t *l_1328 = &g_418;
                int64_t *l_1329 = &l_1271;
                int i, j;
                for (g_175.f7 = 4; (g_175.f7 >= 1); g_175.f7 -= 1)
                { /* block id: 602 */
                    float l_1288[2];
                    uint32_t l_1289 = 0x679633B5L;
                    int32_t l_1290[7][4][9] = {{{1L,0x51EC9D7AL,0x9D2A1CBEL,0xAE91B0BCL,1L,6L,0x4A5B1E11L,0xF73EC64EL,(-8L)},{1L,0x81292818L,1L,5L,6L,1L,0xD2F37EFDL,0xD2F37EFDL,1L},{0x57146B66L,0x8204BCA1L,(-1L),0x8204BCA1L,0x57146B66L,0x9D2A1CBEL,0xE4EEF0FAL,0x2F3E9105L,0L},{4L,1L,0xDD2C3544L,8L,(-1L),(-1L),1L,(-1L),0x67644FF0L}},{{0xFD46BBC7L,1L,0x51EC9D7AL,0x79A6410EL,0x3F0E7E72L,0x9D2A1CBEL,(-10L),0xF7E6D040L,1L},{0x65E1DBBBL,0xDD2C3544L,(-1L),0x1DD4E7C2L,0L,1L,4L,(-1L),0xCFF728BBL},{0x2F3E9105L,0xAD26A269L,1L,0L,6L,6L,0L,1L,0xAD26A269L},{5L,0xA5713CA4L,0L,0xA9E5418FL,0xCFF728BBL,1L,(-8L),1L,(-1L)}},{{0x9D2A1CBEL,(-8L),(-1L),0x57146B66L,0xF7E6D040L,6L,0xAD26A269L,(-9L),(-1L)},{(-1L),0xA5713CA4L,2L,0L,0xD2F37EFDL,2L,0x8E6BFAF9L,(-1L),(-1L)},{0L,0xAD26A269L,0x79A6410EL,6L,(-1L),0xF73EC64EL,(-1L),(-1L),(-1L)},{0xA5713CA4L,0xDD2C3544L,(-8L),(-8L),0xDD2C3544L,0xA5713CA4L,8L,2L,1L}},{{0x79A6410EL,1L,1L,0x3F0E7E72L,6L,4L,0x2F3E9105L,0x8204BCA1L,0xF7E6D040L},{0xCFF728BBL,1L,0x67644FF0L,(-1L),0x97FA6803L,4L,8L,(-1L),2L},{(-8L),0x8204BCA1L,0xF73EC64EL,0xDB705F65L,(-1L),(-1L),(-1L),0xAD26A269L,(-8L)},{0xB36FC92BL,2L,0x793FFFB3L,1L,0x67644FF0L,0L,0x67644FF0L,1L,0x793FFFB3L}},{{0L,0L,0xD71A2574L,0L,0x8204BCA1L,0x9D2A1CBEL,(-8L),(-1L),0x57146B66L},{(-8L),0L,2L,0x7A9FE638L,0x97FA6803L,(-1L),0xDD2C3544L,0x81292818L,5L},{0x4A5B1E11L,0xDB705F65L,0xD71A2574L,(-8L),0xFD46BBC7L,(-1L),0x9D2A1CBEL,(-9L),0L},{0x8C15B0DDL,1L,0x793FFFB3L,0x8E6BFAF9L,(-8L),1L,(-1L),4L,4L}},{{0L,(-1L),0x51EC9D7AL,0x3F0E7E72L,0x51EC9D7AL,(-1L),0L,(-1L),1L},{0xDD2C3544L,0xA5713CA4L,8L,2L,1L,9L,0x1DD4E7C2L,(-1L),0x8E6BFAF9L},{0xAE91B0BCL,6L,0xF7E6D040L,0xFD46BBC7L,(-1L),0L,1L,(-1L),0L},{(-1L),0x65E1DBBBL,0xDD2C3544L,(-1L),0x1DD4E7C2L,0L,1L,4L,(-1L)}},{{0xF73EC64EL,0xE4EEF0FAL,1L,(-1L),(-9L),0xFD46BBC7L,0xFD46BBC7L,(-9L),(-1L)},{4L,(-1L),4L,0xB36FC92BL,0x15D8A50AL,0x8C15B0DDL,8L,0x81292818L,1L},{(-1L),(-1L),0xAE91B0BCL,0x57146B66L,1L,(-10L),1L,(-1L),0xDB705F65L},{0xA52E150AL,0x7A9FE638L,0x6B462F47L,0xB36FC92BL,0xA9E5418FL,(-1L),2L,1L,0x8C15B0DDL}}};
                    uint32_t *l_1293 = &l_1289;
                    const struct S0 * const **l_1300 = &l_1298;
                    int32_t *l_1303 = (void*)0;
                    int32_t *l_1304 = (void*)0;
                    int32_t *l_1305[3][6];
                    uint32_t l_1320 = 0xEC2A7E5DL;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_1288[i] = 0x1.Bp+1;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 6; j++)
                            l_1305[i][j] = &g_626.f1.f0;
                    }
                    for (g_280 = 0; (g_280 <= 4); g_280 += 1)
                    { /* block id: 605 */
                        uint32_t l_1276[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1276[i] = 5UL;
                        (*l_1269) = l_1252;
                        l_1291[6][3] |= (l_1271 == (((*l_1280) = (safe_mod_func_uint32_t_u_u((((((((void*)0 != &g_1212[7]) , (safe_sub_func_uint64_t_u_u((((**l_1269) = ((l_1289 = (l_1276[0] < (safe_mul_func_int8_t_s_s(((((l_1279 == l_1280) | (l_1281 || (safe_add_func_uint16_t_u_u(((*l_39) = (((safe_rshift_func_uint8_t_u_s((18446744073709551610UL ^ (safe_sub_func_int32_t_s_s((l_1280 == (void*)0), l_1281))), (*g_373))) ^ g_937[3].f9) && 0xDC2EL)), l_1281)))) , (*l_1252)) < 0x96L), (*l_1252))))) != (*l_1252))) | g_536.f9), l_1290[5][1][3]))) && l_1290[5][1][3]) ^ l_1276[0]) <= l_1290[5][1][3]) > l_1290[5][1][3]), g_1212[7].f0))) , g_937[3].f1));
                        if (g_989.f9)
                            goto lbl_1292;
                    }
                    (*g_71) ^= (l_1290[3][1][7] && ((((*l_1293) = ((*l_1252) | (l_1291[6][3] && ((-10L) >= l_1290[6][2][6])))) && ((*l_1293) = (&g_106 != ((safe_sub_func_uint8_t_u_u((!(*g_373)), (l_1297 , ((((((*l_1300) = l_1298) == &g_990) >= (**l_1269)) | (*l_1242)) > (*l_1252))))) , &g_106)))) ^ (*l_1252)));
                    l_1317--;
                    for (g_1065 = 0; (g_1065 <= 4); g_1065 += 1)
                    { /* block id: 621 */
                        if (l_1320)
                            break;
                    }
                }
                (*l_1242) = (l_1310[0] |= ((safe_mul_func_int16_t_s_s((*l_1252), (+((*l_1204) = 0xDE2AL)))) | ((safe_mul_func_int16_t_s_s(((safe_mod_func_int64_t_s_s((*l_1242), ((*l_1328) = g_1113[5].f9))) > ((((*g_675) = 255UL) , l_1329) != &l_1302)), (g_766.f0 & (*l_1252)))) && 0x5D1ABB76L)));
            }
            else
            { /* block id: 630 */
                float *l_1339 = (void*)0;
                int32_t l_1349 = 0xE58931C1L;
                int32_t l_1357 = 0x9BB102C5L;
                int32_t l_1358[1][9] = {{(-1L),0L,0L,(-1L),0L,0L,(-1L),0L,0L}};
                int32_t l_1359 = 0x67ED8291L;
                int32_t l_1360[4][1];
                int8_t l_1363 = 0x22L;
                int i, j;
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1360[i][j] = 9L;
                }
                for (g_839.f7 = 0; (g_839.f7 == 0); g_839.f7++)
                { /* block id: 633 */
                    if (l_1332)
                        break;
                }
                for (g_536.f7 = 0; (g_536.f7 > 26); ++g_536.f7)
                { /* block id: 638 */
                    int16_t l_1340 = 0x563CL;
                    int32_t l_1348 = (-1L);
                    int32_t *l_1353 = &l_1301;
                    int32_t *l_1354 = &g_72;
                    int32_t *l_1355 = (void*)0;
                    int32_t *l_1356[9] = {&g_766.f0,&g_1213[8].f0,&g_1213[8].f0,&g_766.f0,&g_1213[8].f0,&g_1213[8].f0,&g_766.f0,&g_1213[8].f0,&g_1213[8].f0};
                    int i;
                    l_1310[5] ^= ((safe_lshift_func_int8_t_s_s((((l_1340 = (safe_unary_minus_func_int32_t_s((+(l_1339 != (*g_804)))))) || (g_547.f5 | ((g_839.f0 || (l_1348 |= (g_1341 == (g_1345 = g_1345)))) < l_1349))) <= ((safe_sub_func_uint64_t_u_u(((((void*)0 == l_1352[3][0]) , g_175.f9) >= 0x7220C362C10D54F6LL), (-1L))) < (*g_373))), (*l_1252))) ^ (*l_1252));
                    for (g_626.f1.f8 = 0; (g_626.f1.f8 <= 6); g_626.f1.f8 += 1)
                    { /* block id: 645 */
                        int i, j;
                        (*l_1242) &= 0x4B0721D9L;
                        l_1348 |= ((*g_647) == (void*)0);
                        if ((*l_1252))
                            continue;
                    }
                    l_1365--;
                    (*g_1066) |= ((*l_1242) = (*l_1252));
                }
                for (l_11 = 0; (l_11 < 25); ++l_11)
                { /* block id: 656 */
                    uint32_t l_1376 = 4294967287UL;
                    int8_t l_1377 = 1L;
                    int32_t l_1379 = 0xAAEA149AL;
                    int32_t l_1382 = 0x3EC90EB6L;
                    int32_t l_1383 = 0xB02C7E05L;
                    int32_t l_1384 = 5L;
                    int32_t l_1385 = 1L;
                    int32_t l_1386 = 0xBBF6C783L;
                    int32_t l_1392 = 0xC030D2B3L;
                    int32_t l_1393 = (-4L);
                    int32_t ***l_1413 = &l_1267;
                    int32_t l_1415 = 1L;
                    if ((safe_mul_func_uint8_t_u_u(((*g_675) = 0x99L), (safe_lshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_s(65535UL, l_1376)), 3)))))
                    { /* block id: 658 */
                        int32_t *l_1378[1][4][4] = {{{(void*)0,&l_1260,(void*)0,&l_1260},{(void*)0,&l_1260,(void*)0,&l_1260},{(void*)0,&l_1260,(void*)0,&l_1260},{(void*)0,&l_1260,(void*)0,&l_1260}}};
                        int i, j, k;
                        ++l_1394;
                    }
                    else
                    { /* block id: 660 */
                        volatile struct S0 *l_1399 = (void*)0;
                        volatile struct S0 *l_1400 = (void*)0;
                        volatile struct S0 *l_1401 = &g_495.f1;
                        int32_t *l_1402 = &l_1364[1][1][2];
                        float *l_1410 = &g_626.f1.f6;
                        (*l_1401) = g_1397[0];
                        (*l_1252) |= 0x33D999A6L;
                        (*l_1268) = l_1402;
                        (**l_1266) = (((*l_1204) = ((safe_mul_func_int8_t_s_s((((*g_373) = (-1L)) <= (***g_1341)), ((0x68D3L ^ (((((((((safe_add_func_float_f_f((safe_add_func_float_f_f(0x0.Dp-1, (((l_1349 , ((*l_1410) = (*l_1242))) <= ((((*l_1402) >= (((safe_rshift_func_uint8_t_u_u((*g_675), 3)) , (((&l_1410 == &g_805) , l_1413) == &g_877)) <= g_766.f0)) >= 0xD.5F2BDFp-71) < g_989.f9)) >= (***l_1413)))), g_175.f9)) , (***l_1413)) ^ l_1360[1][0]) < 0x77L) , &g_1179[6][1][3]) == &g_97) != 0xB775L) , g_766.f3) , (*l_1252))) == (*g_675)))) , l_1360[3][0])) != l_1363);
                    }
                    l_1415 ^= ((*l_1252) > (*l_1252));
                    l_1313 ^= ((**l_1269) |= (l_1416[0][1] == g_1417));
                }
            }
            if (l_1418)
                break;
        }
    }
    else
    { /* block id: 676 */
        uint32_t *l_1427 = (void*)0;
        uint32_t *l_1428 = &g_73[0][0][1].f1.f2;
        uint32_t *l_1429 = &g_83.f2;
        int32_t l_1436 = 0x8792F32DL;
        int32_t *l_1438 = &g_101[5][5];
        int8_t *l_1439[7] = {&g_1440[0],&g_1440[0],&g_1440[0],&g_1440[0],&g_1440[0],&g_1440[0],&g_1440[0]};
        int32_t l_1441[7][9][4] = {{{0L,0x1E91A299L,0L,1L},{0x88F02717L,0x6735789FL,(-1L),1L},{0xF1658486L,0x1E91A299L,0L,(-1L)},{1L,0L,(-6L),(-5L)},{0x88F02717L,0x04278991L,0x79647F27L,0xE6FEC6F2L},{0x04278991L,0x1E91A299L,0x79647F27L,0x79647F27L},{0x88F02717L,0x88F02717L,(-6L),1L},{1L,0xDED36FD3L,0L,0xE6FEC6F2L},{0xF1658486L,0L,(-9L),0L}},{{0x04278991L,0xF1658486L,1L,0L},{0xF1658486L,0xEFF7BD77L,0L,0x3D891CADL},{7L,0x04278991L,(-9L),0L},{0xBA96409EL,0x6735789FL,1L,0L},{0xBA96409EL,(-8L),(-9L),0x79647F27L},{7L,0xF1658486L,0L,(-9L)},{0xF1658486L,0x6735789FL,1L,0x3D891CADL},{0x04278991L,7L,(-9L),0x3D891CADL},{0x47ECB917L,0x6735789FL,0L,(-9L)}},{{0xBA96409EL,0xF1658486L,(-5L),0x79647F27L},{0x04278991L,(-8L),0L,0L},{(-8L),0x6735789FL,0L,0L},{0x04278991L,0x04278991L,(-5L),0x3D891CADL},{0xBA96409EL,0xEFF7BD77L,0L,0L},{0x47ECB917L,0xF1658486L,(-9L),0L},{0x04278991L,0xF1658486L,1L,0L},{0xF1658486L,0xEFF7BD77L,0L,0x3D891CADL},{7L,0x04278991L,(-9L),0L}},{{0xBA96409EL,0x6735789FL,1L,0L},{0xBA96409EL,(-8L),(-9L),0x79647F27L},{7L,0xF1658486L,0L,(-9L)},{0xF1658486L,0x6735789FL,1L,0x3D891CADL},{0x04278991L,7L,(-9L),0x3D891CADL},{0x47ECB917L,0x6735789FL,0L,(-9L)},{0xBA96409EL,0xF1658486L,(-5L),0x79647F27L},{0x04278991L,(-8L),0L,0L},{(-8L),0x6735789FL,0L,0L}},{{0x04278991L,0x04278991L,(-5L),0x3D891CADL},{0xBA96409EL,0xEFF7BD77L,0L,0L},{0x47ECB917L,0xF1658486L,(-9L),0L},{0x04278991L,0xF1658486L,1L,0L},{0xF1658486L,0xEFF7BD77L,0L,0x3D891CADL},{7L,0x04278991L,(-9L),0L},{0xBA96409EL,0x6735789FL,1L,0L},{0xBA96409EL,(-8L),(-9L),0x79647F27L},{7L,0xF1658486L,0L,(-9L)}},{{0xF1658486L,0x6735789FL,1L,0x3D891CADL},{0x04278991L,7L,(-9L),0x3D891CADL},{0x47ECB917L,0x6735789FL,0L,(-9L)},{0xBA96409EL,0xF1658486L,(-5L),0x79647F27L},{0x04278991L,(-8L),0L,0L},{(-8L),0x6735789FL,0L,0L},{0x04278991L,0x04278991L,(-5L),0x3D891CADL},{0xBA96409EL,0xEFF7BD77L,0L,0L},{0x47ECB917L,0xF1658486L,(-9L),0L}},{{0x04278991L,0xF1658486L,1L,0L},{0xF1658486L,0xEFF7BD77L,0L,0x3D891CADL},{1L,(-8L),1L,(-9L)},{0L,7L,0x3D891CADL,0L},{0L,0x121B1D6BL,1L,0L},{1L,0x47ECB917L,(-9L),1L},{0x47ECB917L,7L,0L,0xEB5607B7L},{(-8L),1L,1L,0xEB5607B7L},{0x1E91A299L,7L,1L,1L}}};
        int i, j, k;
        (*l_1252) = (safe_sub_func_uint32_t_u_u((safe_sub_func_uint16_t_u_u(((*l_1252) ^ ((**g_1342) <= (l_1441[4][1][1] &= (safe_mul_func_uint16_t_u_u((((*l_1438) = ((l_1425[0][6][1] == (void*)0) | (safe_unary_minus_func_uint64_t_u(((*l_1252) >= (((--(*l_1429)) | (((safe_mod_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((((((l_1436 != ((*g_1345) != l_1437)) & (((*g_373) = l_1436) && (*g_373))) ^ l_1436) , (void*)0) == (void*)0), (*l_1252))), (*l_1252))) , (void*)0) == (void*)0)) > 0x909F7865L)))))) , (*l_1252)), 0xB56DL))))), 0x6A40L)), g_1212[7].f7));
    }
    ++l_1447;
lbl_1493:
    l_1445[0] = l_1450;
    if (((safe_add_func_int32_t_s_s((safe_rshift_func_int16_t_s_u((l_1455 == l_1456[0][0][3]), (safe_mul_func_uint16_t_u_u((0x3961C2A0512D32F4LL > (safe_mod_func_int16_t_s_s((safe_div_func_uint16_t_u_u((((*l_1252) > ((g_1466[0][4] = l_1463[3]) != &g_1212[4])) < ((*l_1202) = ((safe_lshift_func_int16_t_s_u((0x68L != ((void*)0 == l_1471)), 0)) >= 1L))), (*l_1442))), l_1472))), (*l_1443))))), (*l_1252))) , (*l_1444)))
    { /* block id: 687 */
        uint8_t l_1476 = 253UL;
        int32_t l_1477 = 0x75E4135AL;
        int16_t **l_1484 = &l_1204;
        int16_t *** const l_1483 = &l_1484;
        int16_t *** const *l_1482 = &l_1483;
        int16_t *** const **l_1481 = &l_1482;
        int16_t *** const *l_1486[1];
        int16_t *** const **l_1485[10] = {&l_1486[0],&l_1486[0],&l_1486[0],&l_1486[0],&l_1486[0],&l_1486[0],&l_1486[0],&l_1486[0],&l_1486[0],&l_1486[0]};
        int i;
        for (i = 0; i < 1; i++)
            l_1486[i] = (void*)0;
        (*l_1442) ^= (safe_mod_func_int16_t_s_s((+(l_1477 = ((***g_1341) == ((l_1476 & l_1476) || 0x601EL)))), (safe_div_func_uint32_t_u_u(0xB76AB5C6L, ((((*l_1252) | (~((g_1487 = ((*l_1481) = (void*)0)) == l_1490[3][7][1]))) != 255UL) ^ (*l_1450))))));
        (*l_1471) = g_1492;
    }
    else
    { /* block id: 693 */
        uint64_t l_1494 = 0x7E417688D9320C9ALL;
        int32_t * const l_1496 = &l_1315;
        int32_t **l_1497 = (void*)0;
        int32_t **l_1498 = &l_1445[1];
        if (l_11)
            goto lbl_1493;
        (*l_1498) = ((((*g_1346) == ((0xB.D8EB95p-30 < l_1494) , (**g_1345))) <= g_270.f5) , (g_1495 , (l_1494 , l_1496)));
    }
    return g_1499;
}


/* ------------------------------------------ */
/* 
 * reads : g_1212 g_1213 g_536.f9 g_83.f3 g_989.f9 g_82 g_675 g_280 g_373 g_536.f0
 * writes: g_536.f9 g_1178 g_83.f3 g_82 g_989.f9 g_215
 */
static int32_t  func_4(uint32_t  p_5, float  p_6, uint32_t  p_7, uint16_t  p_8, int32_t  p_9)
{ /* block id: 570 */
    uint64_t l_1214 = 5UL;
    const int32_t l_1240 = 0x17B60C7AL;
lbl_1217:
    l_1214 &= ((g_1212[7] , g_1213[8]) , p_7);
    for (g_536.f9 = (-26); (g_536.f9 == 21); g_536.f9 = safe_add_func_uint32_t_u_u(g_536.f9, 3))
    { /* block id: 574 */
        uint16_t * const ***l_1218 = &g_1178;
        int32_t l_1235 = 0xD32E508FL;
        if (g_536.f9)
            goto lbl_1217;
        (*l_1218) = &g_1179[8][1][0];
        for (g_83.f3 = 0; (g_83.f3 != 24); g_83.f3 = safe_add_func_int8_t_s_s(g_83.f3, 6))
        { /* block id: 579 */
            int32_t **l_1221 = &g_82;
            (*l_1221) = &p_9;
            for (g_989.f9 = (-6); (g_989.f9 != 10); ++g_989.f9)
            { /* block id: 583 */
                (**l_1221) = (safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(0x88L, (safe_add_func_uint16_t_u_u(((**l_1221) > (safe_rshift_func_uint8_t_u_s((((safe_unary_minus_func_uint64_t_u((safe_lshift_func_uint8_t_u_s(((((p_8 ^ (0x49A9L < (((*g_675) , (l_1235 || (((safe_mul_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((l_1214 , p_9), (((*g_373) = (**l_1221)) <= p_7))), 0x26L)) < 1L) || l_1235))) | 0x1BL))) & p_5) > p_5) && (**l_1221)), g_536.f0)))) <= p_5) , (**l_1221)), 0))), 1UL)))), l_1240));
            }
            (*l_1221) = &l_1235;
        }
    }
    return l_1214;
}


/* ------------------------------------------ */
/* 
 * reads : g_72 g_990 g_619
 * writes: g_72 g_175
 */
static uint8_t  func_14(uint16_t  p_15, int8_t  p_16, int16_t  p_17)
{ /* block id: 565 */
    int32_t *l_1209 = &g_72;
    struct S0 *l_1210 = &g_175;
    (*l_1209) ^= (safe_rshift_func_uint16_t_u_s(p_17, 2));
    (*l_1210) = (*g_990);
    return p_15;
}


/* ------------------------------------------ */
/* 
 * reads : g_989 g_990 g_766.f8 g_995 g_998 g_83.f0 g_675 g_280 g_373 g_215 g_454 g_83.f6 g_805 g_300 g_619 g_804 g_111 g_1065 g_1066 g_72 g_626.f0 g_541 g_1072 g_81 g_1099 g_1113 g_71 g_73 g_1150 g_82 g_999 g_996 g_98 g_448
 * writes: g_619 g_766.f8 g_82 g_999 g_83.f0 g_175.f7 g_280 g_1022 g_806 g_626.f0 g_111 g_1061 g_72 g_215 g_1072.f1 g_989.f3 g_839.f2 g_548.f0 g_1150 g_1178 g_839.f9 g_97 g_83.f6
 */
static uint32_t  func_22(uint32_t  p_23, uint8_t  p_24, uint64_t  p_25, int8_t  p_26, int8_t  p_27)
{ /* block id: 470 */
    uint32_t l_994[7][1][2] = {{{4294967294UL,0UL}},{{4294967294UL,4294967294UL}},{{0UL,4294967294UL}},{{4294967294UL,0UL}},{{4294967294UL,4294967294UL}},{{0UL,4294967294UL}},{{4294967294UL,0UL}}};
    int16_t **l_1000 = &g_490[3][3][2];
    int16_t * const *l_1020 = &g_490[2][4][1];
    int16_t * const **l_1019 = &l_1020;
    int16_t * const ***l_1018 = &l_1019;
    int16_t * const ****l_1017 = &l_1018;
    int64_t **l_1044 = &g_151;
    int32_t l_1069 = 0x2409B6B4L;
    uint32_t l_1091 = 3UL;
    uint8_t l_1092 = 0x3FL;
    const int8_t *l_1102 = &g_215;
    const int8_t **l_1101[8][5][1] = {{{&l_1102},{&l_1102},{&l_1102},{&l_1102},{&l_1102}},{{&l_1102},{&l_1102},{(void*)0},{(void*)0},{(void*)0}},{{(void*)0},{(void*)0},{&l_1102},{&l_1102},{&l_1102}},{{&l_1102},{&l_1102},{&l_1102},{&l_1102},{&l_1102}},{{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}},{{&l_1102},{&l_1102},{&l_1102},{&l_1102},{&l_1102}},{{&l_1102},{&l_1102},{&l_1102},{(void*)0},{(void*)0}},{{(void*)0},{(void*)0},{(void*)0},{&l_1102},{&l_1102}}};
    int64_t l_1116 = 0x27315D0EE7697180LL;
    int32_t l_1132 = 1L;
    int32_t l_1136 = 1L;
    int32_t l_1140 = 0xECC2EAA2L;
    int32_t l_1143[6];
    int32_t *l_1157 = &l_1069;
    uint64_t l_1167 = 6UL;
    uint16_t * const *l_1176 = &g_98;
    uint16_t * const **l_1175[8][8][4] = {{{&l_1176,&l_1176,(void*)0,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,(void*)0,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,(void*)0,&l_1176},{&l_1176,&l_1176,&l_1176,(void*)0},{&l_1176,(void*)0,&l_1176,(void*)0},{&l_1176,&l_1176,(void*)0,(void*)0}},{{(void*)0,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,(void*)0},{&l_1176,&l_1176,&l_1176,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,(void*)0,&l_1176},{&l_1176,(void*)0,&l_1176,&l_1176}},{{&l_1176,(void*)0,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,(void*)0,&l_1176},{&l_1176,&l_1176,&l_1176,(void*)0},{(void*)0,&l_1176,(void*)0,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{(void*)0,&l_1176,(void*)0,&l_1176},{&l_1176,(void*)0,&l_1176,&l_1176}},{{&l_1176,(void*)0,(void*)0,&l_1176},{(void*)0,(void*)0,&l_1176,(void*)0},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,(void*)0,&l_1176,&l_1176},{&l_1176,(void*)0,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,(void*)0}},{{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_1176,(void*)0,&l_1176,&l_1176},{&l_1176,&l_1176,&l_1176,&l_1176},{&l_1176,(void*)0,(void*)0,&l_1176},{(void*)0,&l_1176,&l_1176,(void*)0}},{{&l_1176,&l_1176,&l_1176,(void*)0},{&l_1176,&l_1176,&l_1176,&l_1176},{(void*)0,(void*)0,&l_1176,&l_1176},{&l_1176,&l_1176,(void*)0,&l_1176},{&l_1176,(void*)0,&l_1176,(void*)0},{&l_1176,(void*)0,&l_1176,&l_1176},{&l_1176,&l_1176,(void*)0,&l_1176},{(void*)0,&l_1176,(void*)0,&l_1176}},{{&l_1176,&l_1176,&l_1176,(void*)0},{(void*)0,&l_1176,&l_1176,&l_1176},{&l_1176,(void*)0,(void*)0,&l_1176},{&l_1176,(void*)0,&l_1176,&l_1176},{(void*)0,&l_1176,&l_1176,&l_1176},{&l_1176,&l_1176,(void*)0,&l_1176},{(void*)0,&l_1176,(void*)0,(void*)0},{&l_1176,(void*)0,&l_1176,&l_1176}},{{&l_1176,&l_1176,&l_1176,(void*)0},{&l_1176,&l_1176,(void*)0,(void*)0},{&l_1176,&l_1176,&l_1176,&l_1176},{(void*)0,&l_1176,&l_1176,(void*)0},{&l_1176,(void*)0,&l_1176,&l_1176},{&l_1176,(void*)0,&l_1176,(void*)0},{(void*)0,&l_1176,(void*)0,&l_1176},{&l_1176,&l_1176,&l_1176,(void*)0}}};
    uint16_t ** const l_1190[8][10][3] = {{{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,(void*)0,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98}},{{&g_98,&g_98,&g_98},{&g_98,(void*)0,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0},{&g_98,&g_98,(void*)0},{&g_98,&g_98,&g_98},{&g_98,(void*)0,(void*)0},{&g_98,(void*)0,&g_98},{&g_98,&g_98,&g_98}},{{&g_98,&g_98,(void*)0},{(void*)0,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0}},{{&g_98,(void*)0,&g_98},{(void*)0,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0}},{{&g_98,&g_98,&g_98},{(void*)0,(void*)0,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0},{&g_98,&g_98,(void*)0},{&g_98,&g_98,&g_98},{&g_98,(void*)0,(void*)0},{&g_98,(void*)0,&g_98},{&g_98,&g_98,&g_98}},{{&g_98,&g_98,(void*)0},{(void*)0,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0}},{{&g_98,(void*)0,&g_98},{(void*)0,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0}},{{&g_98,&g_98,&g_98},{(void*)0,(void*)0,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0},{&g_98,&g_98,&g_98},{(void*)0,&g_98,(void*)0},{&g_98,&g_98,&g_98},{(void*)0,&g_98,&g_98},{&g_98,(void*)0,&g_98}}};
    uint64_t l_1199 = 0x0DCD7012960D806BLL;
    int32_t **l_1201 = &g_82;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_1143[i] = 0x0FF09B79L;
    (*g_990) = g_989;
    for (g_766.f8 = 0; (g_766.f8 <= 25); g_766.f8 = safe_add_func_uint64_t_u_u(g_766.f8, 4))
    { /* block id: 474 */
        int32_t **l_993 = &g_82;
        (*l_993) = (void*)0;
    }
    if (l_994[5][0][1])
    { /* block id: 477 */
        int32_t l_1023 = 4L;
        int16_t *l_1024 = &g_548.f7;
        const int32_t l_1027 = 0x2CFB093AL;
        int32_t l_1131 = 0xC54C606FL;
        int32_t l_1133 = 0xFDA4C657L;
        int32_t l_1137 = 0xAD45CB7BL;
        int32_t l_1139 = (-1L);
        int32_t l_1141 = 0xCFC40C57L;
        int32_t l_1145 = 0xAB355F61L;
        int32_t l_1148 = 0xEC7741B6L;
        int32_t l_1149 = 0x272369CBL;
        int32_t *l_1158 = &l_1148;
        int32_t *l_1159 = &g_548.f0;
        int32_t *l_1160 = &g_626.f1.f0;
        int32_t *l_1161 = &g_541[0][1][3];
        int32_t *l_1162 = &l_1069;
        int32_t *l_1163 = (void*)0;
        int32_t *l_1164 = &g_83.f0;
        int32_t *l_1165 = &g_619[0].f0;
        int32_t *l_1166[4][4] = {{&l_1143[5],&g_839.f0,&g_83.f0,&g_541[0][1][8]},{(void*)0,&g_839.f0,&g_839.f0,(void*)0},{&g_839.f0,(void*)0,&l_1143[5],&l_1132},{&g_839.f0,&l_1143[5],&g_839.f0,&g_83.f0}};
        int i, j;
        (*g_998) = g_995;
        for (g_83.f0 = 0; (g_83.f0 <= 0); g_83.f0 += 1)
        { /* block id: 481 */
            float l_1001 = 0x7.51D554p+77;
            int16_t *l_1004 = &g_175.f7;
            uint8_t *l_1021[4][7] = {{&g_1022,&g_1022,(void*)0,(void*)0,&g_1022,&g_1022,(void*)0},{&g_1022,&g_1022,&g_1022,&g_1022,&g_1022,&g_1022,&g_1022},{&g_1022,(void*)0,(void*)0,&g_1022,&g_1022,(void*)0,(void*)0},{(void*)0,&g_1022,(void*)0,&g_1022,(void*)0,&g_1022,(void*)0}};
            int32_t l_1025 = 0x9280A976L;
            int32_t l_1026 = (-8L);
            int8_t ** const l_1033 = &g_373;
            int64_t **l_1046 = &g_151;
            struct S0 *l_1096 = &g_1072[4].f1;
            uint16_t ** const l_1118 = (void*)0;
            int32_t **l_1120 = &g_82;
            int32_t ***l_1119 = &l_1120;
            int i, j;
            if ((((void*)0 != l_1000) <= (p_27 , (g_1022 = (safe_mul_func_uint16_t_u_u(((-1L) & (p_23 & ((*l_1004) = 1L))), ((safe_add_func_uint8_t_u_u(((*g_675)++), (((void*)0 == &g_537) , (safe_mul_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u(((safe_add_func_int64_t_s_s((safe_lshift_func_uint8_t_u_s((((l_1025 |= ((((l_1023 = (&g_995 == l_1017)) <= (-1L)) , (void*)0) == l_1024)) & p_24) != 0xB0L), (*g_373))), l_1026)) >= p_25), l_1027)), p_23))))) <= l_994[1][0][0])))))))
            { /* block id: 487 */
                uint64_t l_1060 = 0x452D973197BBFADFLL;
                uint8_t **l_1062 = (void*)0;
                int32_t l_1093 = 0xA64D7340L;
                int32_t **l_1094 = (void*)0;
                int32_t **l_1095 = &g_82;
                (*g_805) = (*g_454);
                for (g_626.f0 = 0; (g_626.f0 <= 7); g_626.f0 += 1)
                { /* block id: 491 */
                    int32_t l_1030 = 0x65965C78L;
                    int32_t *l_1031 = &l_1025;
                    int64_t **l_1063[1][3];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_1063[i][j] = &g_151;
                    }
                    if ((safe_add_func_int8_t_s_s((p_26 = l_1030), (p_23 <= l_1030))))
                    { /* block id: 493 */
                        int32_t *l_1032 = &l_1026;
                        int i;
                        (*g_300) = (l_1032 = (l_1031 = &l_1030));
                        (**g_804) = (g_619[g_83.f0] , (l_1025 <= (l_1033 == &g_373)));
                    }
                    else
                    { /* block id: 498 */
                        int64_t ***l_1045[9][2] = {{&g_150,&g_150},{&g_150,&g_150},{&g_150,&g_150},{&g_150,&g_150},{&g_150,&g_150},{&g_150,&g_150},{&g_150,&g_150},{&g_150,&g_150},{&g_150,&g_150}};
                        int32_t l_1059 = 0L;
                        uint32_t l_1064 = 1UL;
                        int i, j;
                        (*l_1031) &= 0xCCA4206BL;
                        (*g_805) = p_24;
                        (*g_1066) &= ((*l_1031) = (safe_div_func_uint8_t_u_u(p_27, (((((3UL || 8UL) | (((safe_mod_func_uint8_t_u_u((p_26 < (safe_lshift_func_int8_t_s_u((safe_mod_func_int32_t_s_s(((l_1046 = l_1044) == (((g_111 |= (-1L)) <= (safe_mul_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(1UL, 6)), (safe_lshift_func_int16_t_s_u((0x944EFB4F97585F5CLL || ((g_1061 = (((l_1023 = (safe_mod_func_int32_t_s_s((((((safe_div_func_int32_t_s_s(((safe_mul_func_uint16_t_u_u(65530UL, p_27)) | l_1059), 0xB3119433L)) || p_23) & p_25) , l_1031) != (void*)0), 0x724EFD36L))) < l_1060) , &g_675)) != l_1062)), l_1027))))) , l_1063[0][1])), l_1064)), 4))), 1L)) , 0xCB1A94E415EA2C98LL) == p_27)) <= l_1064) != l_1064) | g_1065))));
                        (**g_804) = p_27;
                    }
                    l_1069 = (safe_rshift_func_int8_t_s_s((0L <= ((*g_373) = 0xBCL)), l_1060));
                    for (p_26 = 7; (p_26 >= 0); p_26 -= 1)
                    { /* block id: 513 */
                        int i, j, k;
                        if (g_541[(g_83.f0 + 1)][(g_83.f0 + 1)][(g_626.f0 + 1)])
                            break;
                        return l_994[(g_83.f0 + 6)][g_83.f0][(g_83.f0 + 1)];
                    }
                }
                l_1093 ^= ((((safe_add_func_uint64_t_u_u((((g_1072[4] , 8L) ^ (p_26 | (((safe_add_func_uint32_t_u_u(((safe_mul_func_uint8_t_u_u(l_1023, 0x36L)) > (safe_rshift_func_int8_t_s_u(((safe_sub_func_int16_t_s_s(l_1060, ((safe_div_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_s((safe_div_func_uint32_t_u_u((((((p_24 >= ((safe_mod_func_uint16_t_u_u((((safe_div_func_uint8_t_u_u(l_1060, (((*g_373) > l_1027) ^ l_1027))) , &p_24) == (void*)0), p_26)) && l_1060)) & p_24) > l_1091) & p_23) , p_27), 4294967286UL)), p_26)), p_27)) , p_26))) ^ l_1060), 4))), 0xC57B1941L)) | p_27) | l_1091))) & (*g_675)), p_26)) <= (-1L)) > l_1092) && l_1026);
                (*l_1095) = (void*)0;
            }
            else
            { /* block id: 520 */
                int i;
                g_619[g_83.f0] = g_619[g_83.f0];
            }
            (*l_1096) = g_619[g_83.f0];
            for (g_989.f3 = 0; (g_989.f3 <= 1); g_989.f3 += 1)
            { /* block id: 526 */
                int8_t **l_1100[9] = {&g_373,&g_373,&g_373,&g_373,&g_373,&g_373,&g_373,&g_373,&g_373};
                int32_t l_1142 = 1L;
                int32_t l_1147[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_1147[i] = (-1L);
                for (p_26 = 1; (p_26 >= 0); p_26 -= 1)
                { /* block id: 529 */
                    uint32_t l_1117[1];
                    uint32_t l_1123 = 1UL;
                    int32_t l_1134 = (-1L);
                    int32_t l_1135 = 0x65D3A66AL;
                    int32_t l_1138 = 0x03D01CDDL;
                    int32_t l_1144[2][7] = {{1L,1L,(-2L),0xA42179B6L,(-2L),1L,1L},{1L,1L,(-2L),0xA42179B6L,(-2L),1L,1L}};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_1117[i] = 0UL;
                    if (g_541[(g_83.f0 + 1)][g_989.f3][(g_989.f3 + 1)])
                    { /* block id: 530 */
                        int32_t *l_1097 = &l_1069;
                        int32_t **l_1098 = (void*)0;
                        const int8_t ***l_1103 = &l_1101[1][4][0];
                        (*l_1097) = p_23;
                        (*g_81) = &l_1025;
                        (*g_1099) = l_1097;
                        (*l_1097) ^= (l_1100[8] == ((*l_1103) = l_1101[1][4][0]));
                    }
                    else
                    { /* block id: 536 */
                        int32_t ****l_1121 = &l_1119;
                        const int8_t l_1122 = 0x40L;
                        uint32_t *l_1124 = &g_839.f2;
                        int32_t *l_1126 = &g_548.f0;
                        int32_t *l_1127 = &g_536.f0;
                        int32_t *l_1128 = (void*)0;
                        int32_t *l_1129 = &g_73[0][0][1].f1.f0;
                        int32_t *l_1130[8];
                        int64_t l_1146[9] = {0x422C2D244D3AD2BALL,0x422C2D244D3AD2BALL,(-6L),0x422C2D244D3AD2BALL,0x422C2D244D3AD2BALL,(-6L),0x422C2D244D3AD2BALL,0x422C2D244D3AD2BALL,(-6L)};
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1130[i] = &g_1072[4].f1.f0;
                        (*l_1126) = (safe_lshift_func_int16_t_s_u((!(safe_mod_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((((safe_div_func_uint32_t_u_u((l_1023 = ((*l_1124) = (g_1113[5] , (safe_mul_func_int32_t_s_s((g_541[(g_83.f0 + 1)][g_989.f3][(g_989.f3 + 1)] | ((l_1117[0] = l_1116) > p_27)), (((func_63(l_1118, (((*l_1121) = l_1119) != &g_1099)) , l_1122) ^ l_1123) >= g_541[(g_83.f0 + 1)][g_989.f3][(g_989.f3 + 1)])))))), p_26)) | 0x7993967BED4943ABLL) > l_1069), 6L)), (-1L)))), 14));
                        ++g_1150;
                    }
                    l_1023 ^= (l_1137 = ((&g_1061 != &g_646) != (safe_unary_minus_func_int16_t_s(((safe_unary_minus_func_int16_t_s((safe_mod_func_uint8_t_u_u(l_1147[0], 0x94L)))) >= p_26)))));
                }
            }
        }
        l_1157 = (*g_300);
        ++l_1167;
    }
    else
    { /* block id: 551 */
        uint16_t ***l_1174[9][9][3] = {{{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,(void*)0},{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,&g_97},{&g_97,(void*)0,&g_97}},{{&g_97,&g_97,(void*)0},{&g_97,(void*)0,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,(void*)0},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{(void*)0,&g_97,(void*)0}},{{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,(void*)0},{&g_97,&g_97,&g_97},{&g_97,(void*)0,&g_97},{&g_97,&g_97,&g_97},{(void*)0,(void*)0,&g_97},{(void*)0,(void*)0,&g_97},{&g_97,&g_97,&g_97}},{{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,(void*)0},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,&g_97},{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97}},{{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,(void*)0},{(void*)0,&g_97,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,(void*)0}},{{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{(void*)0,(void*)0,&g_97},{&g_97,&g_97,&g_97}},{{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97}},{{(void*)0,&g_97,&g_97},{(void*)0,&g_97,&g_97},{&g_97,(void*)0,&g_97},{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97},{(void*)0,(void*)0,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97}},{{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{(void*)0,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,&g_97}}};
        uint16_t * const ***l_1177[6] = {&l_1175[1][3][0],&l_1175[1][3][0],&l_1175[1][3][0],&l_1175[1][3][0],&l_1175[1][3][0],&l_1175[1][3][0]};
        int32_t l_1183 = (-1L);
        int16_t ***l_1188 = &l_1000;
        uint32_t *l_1189 = &g_839.f9;
        int64_t l_1197 = (-5L);
        const float l_1198 = 0x0.F975C3p-0;
        int i, j, k;
        (*g_454) = (((*g_448) = func_44((*l_1157), ((*l_1189) = (p_27 && (safe_mul_func_int16_t_s_s((safe_sub_func_int32_t_s_s((l_1174[3][5][0] == (g_1178 = l_1175[1][3][0])), (safe_lshift_func_int16_t_s_u(l_1183, 13)))), ((((safe_lshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_s(((*g_373) ^= ((**g_998) != (*l_1018))), 5)), ((((l_1188 != (((void*)0 != &l_1157) , (void*)0)) >= 0x63L) < l_1183) > (*l_1157)))) , l_1183) == p_26) , (*l_1157)))))), (*l_1176), l_1190[5][9][2])) == (void*)0);
        (*l_1157) = p_23;
        (*l_1157) ^= ((safe_mod_func_int16_t_s_s(p_26, p_23)) , (((safe_rshift_func_int8_t_s_s((safe_add_func_int16_t_s_s((l_1197 == l_1183), l_1199)), ((void*)0 != &p_23))) == (+((void*)0 == &g_448))) || 0x3AD0L));
    }
    (*l_1201) = &l_1136;
    return p_27;
}


/* ------------------------------------------ */
/* 
 * reads : g_159
 * writes: g_82
 */
static uint8_t  func_28(int8_t  p_29, uint64_t  p_30, int32_t  p_31, uint8_t  p_32)
{ /* block id: 54 */
    int32_t * const l_158 = &g_72;
    int32_t l_165[10][1] = {{(-1L)},{0x895D7F4EL},{(-1L)},{0x895D7F4EL},{(-1L)},{0x895D7F4EL},{(-1L)},{0x895D7F4EL},{(-1L)},{0x895D7F4EL}};
    uint16_t l_201 = 65528UL;
    const int16_t *l_219 = &g_83.f7;
    const int16_t ** const l_218[10][5] = {{&l_219,(void*)0,&l_219,&l_219,&l_219},{&l_219,&l_219,&l_219,&l_219,(void*)0},{&l_219,&l_219,(void*)0,&l_219,&l_219},{&l_219,&l_219,&l_219,(void*)0,&l_219},{&l_219,&l_219,&l_219,(void*)0,&l_219},{&l_219,&l_219,(void*)0,&l_219,&l_219},{&l_219,&l_219,(void*)0,&l_219,&l_219},{&l_219,&l_219,&l_219,&l_219,&l_219},{&l_219,&l_219,&l_219,(void*)0,&l_219},{&l_219,&l_219,&l_219,(void*)0,&l_219}};
    int64_t l_263 = 1L;
    uint64_t l_315 = 1UL;
    int32_t *l_344[3];
    uint32_t l_585 = 0x45FC4218L;
    int32_t l_587[4] = {0xBC19BBE3L,0xBC19BBE3L,0xBC19BBE3L,0xBC19BBE3L};
    int32_t l_590 = 7L;
    uint32_t l_616 = 0x2433FF1CL;
    uint8_t *l_653 = &g_280;
    uint8_t **l_652 = &l_653;
    uint32_t l_668[8][2] = {{0x4E1D1C0DL,0x0956B0C3L},{0x4E1D1C0DL,0x0956B0C3L},{0x4E1D1C0DL,0x0956B0C3L},{0x4E1D1C0DL,0x0956B0C3L},{0x4E1D1C0DL,0x0956B0C3L},{0x4E1D1C0DL,0x0956B0C3L},{0x4E1D1C0DL,0x0956B0C3L},{0x4E1D1C0DL,0x0956B0C3L}};
    uint32_t l_671 = 0x147266C5L;
    uint8_t *l_676 = &g_280;
    int64_t **l_677 = &g_151;
    const int8_t l_686 = 0xB9L;
    uint16_t l_700 = 9UL;
    int8_t **l_709[4] = {&g_373,&g_373,&g_373,&g_373};
    uint32_t l_736 = 0xF151739EL;
    struct S0 *l_765 = &g_766;
    uint16_t l_767[1][5] = {{65535UL,65535UL,65535UL,65535UL,65535UL}};
    int64_t l_790 = 0x5521ABD1DDF05AE7LL;
    uint8_t l_848 = 0x04L;
    int16_t l_871 = (-1L);
    int32_t ** const *l_878 = (void*)0;
    int32_t **l_880[4][1][2] = {{{&l_344[0],(void*)0}},{{(void*)0,&l_344[0]}},{{(void*)0,(void*)0}},{{&l_344[0],(void*)0}}};
    int32_t *** const l_879 = &l_880[3][0][0];
    int64_t l_945 = (-7L);
    int16_t l_972 = 1L;
    int16_t *l_975 = &g_766.f7;
    uint64_t *l_987 = &g_536.f3;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_344[i] = &g_73[0][0][1].f1.f0;
    (*g_159) = l_158;
    return p_32;
}


/* ------------------------------------------ */
/* 
 * reads : g_71 g_72 g_73 g_10 g_81 g_83 g_97 g_98 g_40 g_82 g_102 g_106
 * writes: g_72 g_62 g_82 g_83 g_97 g_101 g_102 g_40 g_73.f1.f7 g_150
 */
static int8_t  func_33(int8_t  p_34, uint32_t  p_35, float  p_36, uint32_t  p_37)
{ /* block id: 2 */
    uint32_t l_41 = 18446744073709551607UL;
    float *l_61[1][9][1] = {{{(void*)0},{&g_62},{(void*)0},{&g_62},{(void*)0},{&g_62},{(void*)0},{&g_62},{(void*)0}}};
    uint16_t *l_67 = (void*)0;
    uint16_t ** const l_66 = &l_67;
    uint16_t **l_68 = &l_67;
    int32_t l_79 = 0xCA842DFAL;
    float l_147 = (-0x5.1p+1);
    int i, j, k;
    ++l_41;
lbl_156:
    (*g_106) = func_44(((*g_98) = ((func_49(((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(l_41, 5)), func_58((((p_36 = l_41) , func_63((l_68 = l_66), l_41)) , (safe_mul_func_float_f_f(((g_62 = (-0x1.Dp+1)) < (((safe_div_func_float_f_f(p_36, ((p_35 < (l_79 = ((((safe_unary_minus_func_uint32_t_u((p_35 >= 0x65L))) < l_41) , p_35) <= g_10[0][7][0]))) <= 0x8.Dp+1))) != l_41) == g_72)), l_41))), &l_67))) , (void*)0), &l_67, &l_67, (*l_66)) == l_41) == 0x10A2F695L)), g_10[0][7][0], (*l_66), &l_67);
    for (g_83.f7 = (-1); (g_83.f7 == (-30)); g_83.f7--)
    { /* block id: 28 */
        int32_t l_109 = (-1L);
        int64_t *l_110[6] = {(void*)0,&g_111,&g_111,(void*)0,&g_111,&g_111};
        int32_t l_112 = 0x42942228L;
        int32_t l_142[7];
        int16_t *l_143 = &g_73[0][0][1].f1.f7;
        int16_t *l_145 = &g_83.f7;
        int16_t **l_144 = &l_145;
        int64_t l_146[1];
        int i;
        for (i = 0; i < 7; i++)
            l_142[i] = 0xFA74B5D4L;
        for (i = 0; i < 1; i++)
            l_146[i] = 0x33D11F6D7AAF5FA4LL;
        (*g_82) = ((((l_109 | (l_112 |= g_83.f1)) > (safe_sub_func_uint8_t_u_u((safe_lshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_s(1L, 7)), 2)), l_41))) != l_79) , ((safe_lshift_func_uint16_t_u_u(((safe_add_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((l_79 != 1UL), (safe_mod_func_int64_t_s_s((safe_add_func_uint64_t_u_u(p_35, ((safe_div_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((--(*g_98)), (safe_rshift_func_int16_t_s_s(((*g_81) == &g_72), 1)))), p_35)) & l_109))), g_83.f0)))), p_34)) && p_37), 9)) != g_10[0][1][0]));
        l_142[0] = (((safe_sub_func_float_f_f(g_83.f2, (safe_add_func_float_f_f(p_37, (((((!p_35) | (p_37 && ((*l_143) = (l_79 = l_142[1])))) < ((((*l_144) = (void*)0) != (void*)0) > ((2UL != (((0x20L & l_146[0]) & 0x3DBCAAF734E6BA9FLL) > g_10[0][7][0])) | g_10[0][7][0]))) > l_41) , 0x1.2p-1))))) != g_40[1][3][2]) < l_147);
        for (g_72 = 0; (g_72 <= (-25)); g_72 = safe_sub_func_uint32_t_u_u(g_72, 5))
        { /* block id: 38 */
            g_150 = &l_110[1];
            for (l_41 = (-1); (l_41 != 17); l_41 = safe_add_func_int16_t_s_s(l_41, 6))
            { /* block id: 42 */
                if (p_34)
                    break;
            }
        }
        for (l_109 = 20; (l_109 >= (-20)); l_109 = safe_sub_func_int8_t_s_s(l_109, 1))
        { /* block id: 48 */
            int32_t l_157 = 0x68449CD7L;
            if (g_83.f2)
                goto lbl_156;
            return l_157;
        }
    }
    return g_40[0][3][3];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t ** func_44(uint16_t  p_45, uint32_t  p_46, uint16_t * const  p_47, uint16_t ** const  p_48)
{ /* block id: 23 */
    uint16_t **l_105 = (void*)0;
    return l_105;
}


/* ------------------------------------------ */
/* 
 * reads : g_83 g_97 g_98 g_40 g_82 g_102
 * writes: g_83 g_97 g_72 g_101 g_102
 */
static int16_t  func_49(uint16_t * p_50, uint16_t ** p_51, uint16_t ** p_52, uint16_t * p_53)
{ /* block id: 14 */
    int64_t l_85 = 0x4D78F30ED53B0E2DLL;
    int8_t l_86[2];
    int16_t *l_87 = &g_83.f7;
    uint16_t ***l_99 = &g_97;
    int32_t *l_100[2];
    int i;
    for (i = 0; i < 2; i++)
        l_86[i] = 0xAAL;
    for (i = 0; i < 2; i++)
        l_100[i] = (void*)0;
    g_83 = g_83;
    g_101[5][5] = ((0x97D506DCL | (((!(l_85 < 0xB144L)) > (l_86[1] && ((*l_87) = l_86[0]))) , (safe_rshift_func_int8_t_s_s((safe_div_func_uint8_t_u_u((65526UL < (((((safe_unary_minus_func_int32_t_s(((*g_82) = (safe_div_func_int16_t_s_s((0x3695F895E34458F4LL & (safe_sub_func_int64_t_s_s(((((*l_99) = g_97) != &g_98) , l_85), l_85))), (*g_98)))))) < l_86[1]) <= 9L) & 0x04EFFBF5L) , 0x6E94L)), g_83.f0)), 0)))) ^ l_85);
    --g_102;
    return g_83.f4;
}


/* ------------------------------------------ */
/* 
 * reads : g_81 g_72
 * writes: g_82
 */
static int8_t  func_58(float  p_59, uint16_t ** p_60)
{ /* block id: 11 */
    int32_t *l_80 = &g_72;
    (*g_81) = l_80;
    return (*l_80);
}


/* ------------------------------------------ */
/* 
 * reads : g_71 g_72 g_73
 * writes: g_72
 */
static union U1  func_63(uint16_t ** const  p_64, int8_t  p_65)
{ /* block id: 6 */
    uint64_t l_69 = 0x9679FF52682FBA83LL;
    (*g_71) |= l_69;
    return g_73[0][0][1];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_10[i][j][k], "g_10[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_40[i][j][k], "g_40[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_62, sizeof(g_62), "g_62", print_hash_value);
    transparent_crc(g_72, "g_72", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_73[i][j][k].f0, "g_73[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_83.f0, "g_83.f0", print_hash_value);
    transparent_crc(g_83.f1, "g_83.f1", print_hash_value);
    transparent_crc(g_83.f2, "g_83.f2", print_hash_value);
    transparent_crc(g_83.f3, "g_83.f3", print_hash_value);
    transparent_crc(g_83.f4, "g_83.f4", print_hash_value);
    transparent_crc(g_83.f5, "g_83.f5", print_hash_value);
    transparent_crc_bytes (&g_83.f6, sizeof(g_83.f6), "g_83.f6", print_hash_value);
    transparent_crc(g_83.f7, "g_83.f7", print_hash_value);
    transparent_crc(g_83.f8, "g_83.f8", print_hash_value);
    transparent_crc(g_83.f9, "g_83.f9", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_101[i][j], "g_101[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_102, "g_102", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc(g_175.f0, "g_175.f0", print_hash_value);
    transparent_crc(g_175.f1, "g_175.f1", print_hash_value);
    transparent_crc(g_175.f2, "g_175.f2", print_hash_value);
    transparent_crc(g_175.f3, "g_175.f3", print_hash_value);
    transparent_crc(g_175.f4, "g_175.f4", print_hash_value);
    transparent_crc(g_175.f5, "g_175.f5", print_hash_value);
    transparent_crc_bytes (&g_175.f6, sizeof(g_175.f6), "g_175.f6", print_hash_value);
    transparent_crc(g_175.f7, "g_175.f7", print_hash_value);
    transparent_crc(g_175.f8, "g_175.f8", print_hash_value);
    transparent_crc(g_175.f9, "g_175.f9", print_hash_value);
    transparent_crc(g_185, "g_185", print_hash_value);
    transparent_crc(g_187, "g_187", print_hash_value);
    transparent_crc(g_215, "g_215", print_hash_value);
    transparent_crc(g_270.f0, "g_270.f0", print_hash_value);
    transparent_crc(g_270.f1, "g_270.f1", print_hash_value);
    transparent_crc(g_270.f2, "g_270.f2", print_hash_value);
    transparent_crc(g_270.f3, "g_270.f3", print_hash_value);
    transparent_crc(g_270.f4, "g_270.f4", print_hash_value);
    transparent_crc(g_270.f5, "g_270.f5", print_hash_value);
    transparent_crc_bytes (&g_270.f6, sizeof(g_270.f6), "g_270.f6", print_hash_value);
    transparent_crc(g_270.f7, "g_270.f7", print_hash_value);
    transparent_crc(g_270.f8, "g_270.f8", print_hash_value);
    transparent_crc(g_270.f9, "g_270.f9", print_hash_value);
    transparent_crc(g_280, "g_280", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_399[i].f0, "g_399[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_418, "g_418", print_hash_value);
    transparent_crc(g_494.f0, "g_494.f0", print_hash_value);
    transparent_crc(g_494.f1, "g_494.f1", print_hash_value);
    transparent_crc(g_494.f2, "g_494.f2", print_hash_value);
    transparent_crc(g_494.f3, "g_494.f3", print_hash_value);
    transparent_crc(g_494.f4, "g_494.f4", print_hash_value);
    transparent_crc(g_494.f5, "g_494.f5", print_hash_value);
    transparent_crc_bytes (&g_494.f6, sizeof(g_494.f6), "g_494.f6", print_hash_value);
    transparent_crc(g_494.f7, "g_494.f7", print_hash_value);
    transparent_crc(g_494.f8, "g_494.f8", print_hash_value);
    transparent_crc(g_494.f9, "g_494.f9", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc_bytes(&g_509[i], sizeof(g_509[i]), "g_509[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_536.f0, "g_536.f0", print_hash_value);
    transparent_crc(g_536.f1, "g_536.f1", print_hash_value);
    transparent_crc(g_536.f2, "g_536.f2", print_hash_value);
    transparent_crc(g_536.f3, "g_536.f3", print_hash_value);
    transparent_crc(g_536.f4, "g_536.f4", print_hash_value);
    transparent_crc(g_536.f5, "g_536.f5", print_hash_value);
    transparent_crc_bytes (&g_536.f6, sizeof(g_536.f6), "g_536.f6", print_hash_value);
    transparent_crc(g_536.f7, "g_536.f7", print_hash_value);
    transparent_crc(g_536.f8, "g_536.f8", print_hash_value);
    transparent_crc(g_536.f9, "g_536.f9", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_541[i][j][k], "g_541[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_547.f0, "g_547.f0", print_hash_value);
    transparent_crc(g_547.f1, "g_547.f1", print_hash_value);
    transparent_crc(g_547.f2, "g_547.f2", print_hash_value);
    transparent_crc(g_547.f3, "g_547.f3", print_hash_value);
    transparent_crc(g_547.f4, "g_547.f4", print_hash_value);
    transparent_crc(g_547.f5, "g_547.f5", print_hash_value);
    transparent_crc_bytes (&g_547.f6, sizeof(g_547.f6), "g_547.f6", print_hash_value);
    transparent_crc(g_547.f7, "g_547.f7", print_hash_value);
    transparent_crc(g_547.f8, "g_547.f8", print_hash_value);
    transparent_crc(g_547.f9, "g_547.f9", print_hash_value);
    transparent_crc(g_548.f0, "g_548.f0", print_hash_value);
    transparent_crc(g_548.f1, "g_548.f1", print_hash_value);
    transparent_crc(g_548.f2, "g_548.f2", print_hash_value);
    transparent_crc(g_548.f3, "g_548.f3", print_hash_value);
    transparent_crc(g_548.f4, "g_548.f4", print_hash_value);
    transparent_crc(g_548.f5, "g_548.f5", print_hash_value);
    transparent_crc_bytes (&g_548.f6, sizeof(g_548.f6), "g_548.f6", print_hash_value);
    transparent_crc(g_548.f7, "g_548.f7", print_hash_value);
    transparent_crc(g_548.f8, "g_548.f8", print_hash_value);
    transparent_crc(g_548.f9, "g_548.f9", print_hash_value);
    transparent_crc(g_561, "g_561", print_hash_value);
    transparent_crc(g_572.f0, "g_572.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_619[i].f0, "g_619[i].f0", print_hash_value);
        transparent_crc(g_619[i].f1, "g_619[i].f1", print_hash_value);
        transparent_crc(g_619[i].f2, "g_619[i].f2", print_hash_value);
        transparent_crc(g_619[i].f3, "g_619[i].f3", print_hash_value);
        transparent_crc(g_619[i].f4, "g_619[i].f4", print_hash_value);
        transparent_crc(g_619[i].f5, "g_619[i].f5", print_hash_value);
        transparent_crc_bytes(&g_619[i].f6, sizeof(g_619[i].f6), "g_619[i].f6", print_hash_value);
        transparent_crc(g_619[i].f7, "g_619[i].f7", print_hash_value);
        transparent_crc(g_619[i].f8, "g_619[i].f8", print_hash_value);
        transparent_crc(g_619[i].f9, "g_619[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_626.f0, "g_626.f0", print_hash_value);
    transparent_crc(g_672.f0, "g_672.f0", print_hash_value);
    transparent_crc(g_696.f0, "g_696.f0", print_hash_value);
    transparent_crc(g_766.f0, "g_766.f0", print_hash_value);
    transparent_crc(g_766.f1, "g_766.f1", print_hash_value);
    transparent_crc(g_766.f2, "g_766.f2", print_hash_value);
    transparent_crc(g_766.f3, "g_766.f3", print_hash_value);
    transparent_crc(g_766.f4, "g_766.f4", print_hash_value);
    transparent_crc(g_766.f5, "g_766.f5", print_hash_value);
    transparent_crc_bytes (&g_766.f6, sizeof(g_766.f6), "g_766.f6", print_hash_value);
    transparent_crc(g_766.f7, "g_766.f7", print_hash_value);
    transparent_crc(g_766.f8, "g_766.f8", print_hash_value);
    transparent_crc(g_766.f9, "g_766.f9", print_hash_value);
    transparent_crc(g_800, "g_800", print_hash_value);
    transparent_crc_bytes (&g_806, sizeof(g_806), "g_806", print_hash_value);
    transparent_crc(g_839.f0, "g_839.f0", print_hash_value);
    transparent_crc(g_839.f1, "g_839.f1", print_hash_value);
    transparent_crc(g_839.f2, "g_839.f2", print_hash_value);
    transparent_crc(g_839.f3, "g_839.f3", print_hash_value);
    transparent_crc(g_839.f4, "g_839.f4", print_hash_value);
    transparent_crc(g_839.f5, "g_839.f5", print_hash_value);
    transparent_crc_bytes (&g_839.f6, sizeof(g_839.f6), "g_839.f6", print_hash_value);
    transparent_crc(g_839.f7, "g_839.f7", print_hash_value);
    transparent_crc(g_839.f8, "g_839.f8", print_hash_value);
    transparent_crc(g_839.f9, "g_839.f9", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_937[i].f0, "g_937[i].f0", print_hash_value);
        transparent_crc(g_937[i].f1, "g_937[i].f1", print_hash_value);
        transparent_crc(g_937[i].f2, "g_937[i].f2", print_hash_value);
        transparent_crc(g_937[i].f3, "g_937[i].f3", print_hash_value);
        transparent_crc(g_937[i].f4, "g_937[i].f4", print_hash_value);
        transparent_crc(g_937[i].f5, "g_937[i].f5", print_hash_value);
        transparent_crc_bytes(&g_937[i].f6, sizeof(g_937[i].f6), "g_937[i].f6", print_hash_value);
        transparent_crc(g_937[i].f7, "g_937[i].f7", print_hash_value);
        transparent_crc(g_937[i].f8, "g_937[i].f8", print_hash_value);
        transparent_crc(g_937[i].f9, "g_937[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_989.f0, "g_989.f0", print_hash_value);
    transparent_crc(g_989.f1, "g_989.f1", print_hash_value);
    transparent_crc(g_989.f2, "g_989.f2", print_hash_value);
    transparent_crc(g_989.f3, "g_989.f3", print_hash_value);
    transparent_crc(g_989.f4, "g_989.f4", print_hash_value);
    transparent_crc(g_989.f5, "g_989.f5", print_hash_value);
    transparent_crc_bytes (&g_989.f6, sizeof(g_989.f6), "g_989.f6", print_hash_value);
    transparent_crc(g_989.f7, "g_989.f7", print_hash_value);
    transparent_crc(g_989.f8, "g_989.f8", print_hash_value);
    transparent_crc(g_989.f9, "g_989.f9", print_hash_value);
    transparent_crc(g_1022, "g_1022", print_hash_value);
    transparent_crc(g_1065, "g_1065", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1113[i].f0, "g_1113[i].f0", print_hash_value);
        transparent_crc(g_1113[i].f1, "g_1113[i].f1", print_hash_value);
        transparent_crc(g_1113[i].f2, "g_1113[i].f2", print_hash_value);
        transparent_crc(g_1113[i].f3, "g_1113[i].f3", print_hash_value);
        transparent_crc(g_1113[i].f4, "g_1113[i].f4", print_hash_value);
        transparent_crc(g_1113[i].f5, "g_1113[i].f5", print_hash_value);
        transparent_crc_bytes(&g_1113[i].f6, sizeof(g_1113[i].f6), "g_1113[i].f6", print_hash_value);
        transparent_crc(g_1113[i].f7, "g_1113[i].f7", print_hash_value);
        transparent_crc(g_1113[i].f8, "g_1113[i].f8", print_hash_value);
        transparent_crc(g_1113[i].f9, "g_1113[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1150, "g_1150", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1212[i].f0, "g_1212[i].f0", print_hash_value);
        transparent_crc(g_1212[i].f1, "g_1212[i].f1", print_hash_value);
        transparent_crc(g_1212[i].f2, "g_1212[i].f2", print_hash_value);
        transparent_crc(g_1212[i].f3, "g_1212[i].f3", print_hash_value);
        transparent_crc(g_1212[i].f4, "g_1212[i].f4", print_hash_value);
        transparent_crc(g_1212[i].f5, "g_1212[i].f5", print_hash_value);
        transparent_crc_bytes(&g_1212[i].f6, sizeof(g_1212[i].f6), "g_1212[i].f6", print_hash_value);
        transparent_crc(g_1212[i].f7, "g_1212[i].f7", print_hash_value);
        transparent_crc(g_1212[i].f8, "g_1212[i].f8", print_hash_value);
        transparent_crc(g_1212[i].f9, "g_1212[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1213[i].f0, "g_1213[i].f0", print_hash_value);
        transparent_crc(g_1213[i].f1, "g_1213[i].f1", print_hash_value);
        transparent_crc(g_1213[i].f2, "g_1213[i].f2", print_hash_value);
        transparent_crc(g_1213[i].f3, "g_1213[i].f3", print_hash_value);
        transparent_crc(g_1213[i].f4, "g_1213[i].f4", print_hash_value);
        transparent_crc(g_1213[i].f5, "g_1213[i].f5", print_hash_value);
        transparent_crc_bytes(&g_1213[i].f6, sizeof(g_1213[i].f6), "g_1213[i].f6", print_hash_value);
        transparent_crc(g_1213[i].f7, "g_1213[i].f7", print_hash_value);
        transparent_crc(g_1213[i].f8, "g_1213[i].f8", print_hash_value);
        transparent_crc(g_1213[i].f9, "g_1213[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1344, "g_1344", print_hash_value);
    transparent_crc(g_1388, "g_1388", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1397[i].f0, "g_1397[i].f0", print_hash_value);
        transparent_crc(g_1397[i].f1, "g_1397[i].f1", print_hash_value);
        transparent_crc(g_1397[i].f2, "g_1397[i].f2", print_hash_value);
        transparent_crc(g_1397[i].f3, "g_1397[i].f3", print_hash_value);
        transparent_crc(g_1397[i].f4, "g_1397[i].f4", print_hash_value);
        transparent_crc(g_1397[i].f5, "g_1397[i].f5", print_hash_value);
        transparent_crc_bytes(&g_1397[i].f6, sizeof(g_1397[i].f6), "g_1397[i].f6", print_hash_value);
        transparent_crc(g_1397[i].f7, "g_1397[i].f7", print_hash_value);
        transparent_crc(g_1397[i].f8, "g_1397[i].f8", print_hash_value);
        transparent_crc(g_1397[i].f9, "g_1397[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1440[i], "g_1440[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1492.f0, "g_1492.f0", print_hash_value);
    transparent_crc(g_1492.f1, "g_1492.f1", print_hash_value);
    transparent_crc(g_1492.f2, "g_1492.f2", print_hash_value);
    transparent_crc(g_1492.f3, "g_1492.f3", print_hash_value);
    transparent_crc(g_1492.f4, "g_1492.f4", print_hash_value);
    transparent_crc(g_1492.f5, "g_1492.f5", print_hash_value);
    transparent_crc_bytes (&g_1492.f6, sizeof(g_1492.f6), "g_1492.f6", print_hash_value);
    transparent_crc(g_1492.f7, "g_1492.f7", print_hash_value);
    transparent_crc(g_1492.f8, "g_1492.f8", print_hash_value);
    transparent_crc(g_1492.f9, "g_1492.f9", print_hash_value);
    transparent_crc(g_1495.f0, "g_1495.f0", print_hash_value);
    transparent_crc(g_1495.f1, "g_1495.f1", print_hash_value);
    transparent_crc(g_1495.f2, "g_1495.f2", print_hash_value);
    transparent_crc(g_1495.f3, "g_1495.f3", print_hash_value);
    transparent_crc(g_1495.f4, "g_1495.f4", print_hash_value);
    transparent_crc(g_1495.f5, "g_1495.f5", print_hash_value);
    transparent_crc_bytes (&g_1495.f6, sizeof(g_1495.f6), "g_1495.f6", print_hash_value);
    transparent_crc(g_1495.f7, "g_1495.f7", print_hash_value);
    transparent_crc(g_1495.f8, "g_1495.f8", print_hash_value);
    transparent_crc(g_1495.f9, "g_1495.f9", print_hash_value);
    transparent_crc(g_1499.f0, "g_1499.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 416
   depth: 1, occurrence: 17
XXX total union variables: 9

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 49
breakdown:
   depth: 1, occurrence: 138
   depth: 2, occurrence: 25
   depth: 3, occurrence: 4
   depth: 4, occurrence: 3
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 8, occurrence: 1
   depth: 10, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 3
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 1
   depth: 27, occurrence: 3
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 36, occurrence: 2
   depth: 49, occurrence: 1

XXX total number of pointers: 343

XXX times a variable address is taken: 912
XXX times a pointer is dereferenced on RHS: 152
breakdown:
   depth: 1, occurrence: 135
   depth: 2, occurrence: 13
   depth: 3, occurrence: 4
XXX times a pointer is dereferenced on LHS: 197
breakdown:
   depth: 1, occurrence: 188
   depth: 2, occurrence: 9
XXX times a pointer is compared with null: 26
XXX times a pointer is compared with address of another variable: 9
XXX times a pointer is compared with another pointer: 9
XXX times a pointer is qualified to be dereferenced: 5051

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 752
   level: 2, occurrence: 145
   level: 3, occurrence: 35
   level: 4, occurrence: 5
   level: 5, occurrence: 2
XXX number of pointers point to pointers: 160
XXX number of pointers point to scalars: 165
XXX number of pointers point to structs: 18
XXX percent of pointers has null in alias set: 29.7
XXX average alias set size: 1.44

XXX times a non-volatile is read: 1062
XXX times a non-volatile is write: 554
XXX times a volatile is read: 73
XXX    times read thru a pointer: 11
XXX times a volatile is write: 38
XXX    times written thru a pointer: 10
XXX times a volatile is available for access: 3.85e+03
XXX percentage of non-volatile access: 93.6

XXX forward jumps: 0
XXX backward jumps: 9

XXX stmts: 123
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 22
   depth: 2, occurrence: 14
   depth: 3, occurrence: 13
   depth: 4, occurrence: 17
   depth: 5, occurrence: 26

XXX percentage a fresh-made variable is used: 19.8
XXX percentage an existing variable is used: 80.2
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3772561085
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int64_t g_5 = 0xFE1CA2961C0F8067LL;/* VOLATILE GLOBAL g_5 */
static uint32_t g_31 = 0x6A0B5978L;
static int32_t g_35[7] = {4L,4L,4L,4L,4L,4L,4L};
static float g_84[9] = {(-0x6.3p-1),(-0x6.3p-1),(-0x6.3p-1),(-0x6.3p-1),(-0x6.3p-1),(-0x6.3p-1),(-0x6.3p-1),(-0x6.3p-1),(-0x6.3p-1)};
static int32_t g_86 = 0x0D9FD67DL;
static int64_t g_90 = 2L;
static uint16_t g_91[1][4] = {{0UL,0UL,0UL,0UL}};
static float g_96 = 0xD.99DD6Bp+65;
static float g_98 = 0x1.Fp-1;
static int32_t g_154 = 1L;
static int16_t g_161 = 0x3DBFL;
static int16_t *g_176 = (void*)0;
static uint8_t g_178 = 255UL;
static uint32_t g_199 = 1UL;
static uint64_t g_201 = 0x54707D7012D8CDCCLL;
static uint8_t *g_271 = (void*)0;
static float g_278 = 0x9.D68E3Bp+83;
static uint32_t g_279[3] = {18446744073709551608UL,18446744073709551608UL,18446744073709551608UL};
static int32_t g_291 = (-1L);
static volatile int32_t *g_299 = (void*)0;
static volatile int32_t * const *g_298 = &g_299;
static int32_t *g_301 = &g_35[2];
static int32_t **g_300[10][6] = {{(void*)0,&g_301,&g_301,&g_301,&g_301,&g_301},{&g_301,&g_301,(void*)0,(void*)0,&g_301,&g_301},{&g_301,&g_301,(void*)0,&g_301,&g_301,&g_301},{&g_301,&g_301,&g_301,(void*)0,&g_301,&g_301},{&g_301,(void*)0,&g_301,(void*)0,&g_301,(void*)0},{&g_301,&g_301,&g_301,(void*)0,(void*)0,(void*)0},{&g_301,&g_301,(void*)0,&g_301,&g_301,&g_301},{(void*)0,&g_301,&g_301,(void*)0,(void*)0,&g_301},{&g_301,&g_301,&g_301,&g_301,&g_301,&g_301},{(void*)0,(void*)0,&g_301,&g_301,&g_301,&g_301}};
static int8_t g_304[6][6][4] = {{{0xABL,(-1L),4L,0xF3L},{(-1L),1L,(-1L),0x5AL},{(-1L),0x68L,0x19L,0xABL},{1L,(-1L),1L,0xFAL},{(-1L),1L,6L,0x35L},{0xF3L,0xABL,(-5L),1L}},{{0x1DL,0xD2L,0xD2L,0x1DL},{0x35L,0xFAL,6L,1L},{0x68L,(-1L),(-1L),0xC4L},{1L,4L,0x88L,0xC4L},{0xD2L,(-1L),(-1L),1L},{0xE4L,0xFAL,0x7CL,0x1DL}},{{0xABL,0xD2L,1L,1L},{(-1L),0xABL,4L,0x35L},{0xE4L,1L,(-1L),0xFAL},{(-1L),(-1L),0x88L,0xABL},{1L,0x68L,1L,0x5AL},{0x68L,1L,(-1L),0xF3L}},{{0xF3L,(-1L),0xD2L,1L},{3L,(-1L),0xD2L,3L},{0xF3L,0xFAL,(-1L),1L},{0x68L,0x4AL,1L,0xC4L},{1L,0xC4L,0x88L,4L},{(-1L),(-1L),(-1L),1L}},{{0xE4L,0x5AL,4L,0x1DL},{(-1L),(-1L),1L,1L},{0xABL,0xABL,0x7CL,0xF3L},{0xE4L,1L,(-1L),0xFAL},{0xD2L,0x68L,0x88L,(-1L)},{1L,0x68L,(-1L),0xFAL}},{{0x68L,1L,6L,0xF3L},{0x35L,0xABL,0xD2L,1L},{0x1DL,(-1L),(-5L),0x1DL},{0xF3L,0x5AL,6L,1L},{(-1L),(-1L),1L,4L},{1L,0xC4L,0x19L,0xC4L}}};
static int8_t g_320 = 0xC4L;
static const uint8_t g_321[2] = {252UL,252UL};
static volatile uint64_t g_331[3] = {0UL,0UL,0UL};
static volatile uint64_t *g_330 = &g_331[0];
static volatile uint64_t * const * const g_329 = &g_330;
static int32_t g_357 = 0xD50A5C85L;
static uint64_t g_394[8][5][6] = {{{0x59321D68B107CD17LL,0x575CA1BAF0D8E3BELL,0xC93012EB3200A3AFLL,0x575CA1BAF0D8E3BELL,0x59321D68B107CD17LL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL}},{{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL}},{{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL}},{{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL}},{{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL}},{{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL},{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL}},{{0xD1A03054315FED4ALL,0xA985E607DF8C5710LL,1UL,0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL},{0xA985E607DF8C5710LL,0xD1A03054315FED4ALL,1UL,18446744073709551615UL,18446744073709551615UL,1UL},{18446744073709551615UL,18446744073709551615UL,1UL,0xC93012EB3200A3AFLL,2UL,18446744073709551615UL},{0xC93012EB3200A3AFLL,2UL,18446744073709551615UL,2UL,0xC93012EB3200A3AFLL,18446744073709551615UL},{2UL,0xC93012EB3200A3AFLL,18446744073709551615UL,0x70E1E20A6ADBB826LL,0x70E1E20A6ADBB826LL,18446744073709551615UL}},{{0x70E1E20A6ADBB826LL,0x70E1E20A6ADBB826LL,18446744073709551615UL,0xC93012EB3200A3AFLL,2UL,18446744073709551615UL},{0xC93012EB3200A3AFLL,2UL,18446744073709551615UL,2UL,0xC93012EB3200A3AFLL,18446744073709551615UL},{2UL,0xC93012EB3200A3AFLL,18446744073709551615UL,0x70E1E20A6ADBB826LL,0x70E1E20A6ADBB826LL,18446744073709551615UL},{0x70E1E20A6ADBB826LL,0x70E1E20A6ADBB826LL,18446744073709551615UL,0xC93012EB3200A3AFLL,2UL,18446744073709551615UL},{0xC93012EB3200A3AFLL,2UL,18446744073709551615UL,2UL,0xC93012EB3200A3AFLL,18446744073709551615UL}}};
static int16_t g_417 = (-1L);
static const int16_t g_423 = (-3L);
static uint16_t g_453 = 0xF65DL;
static int32_t g_511 = 0xEEB4EAD1L;
static volatile uint16_t g_585 = 65535UL;/* VOLATILE GLOBAL g_585 */
static volatile uint16_t * volatile g_584 = &g_585;/* VOLATILE GLOBAL g_584 */
static volatile uint16_t * volatile *g_583 = &g_584;
static volatile uint16_t * volatile **g_582 = &g_583;
static uint32_t g_595[5] = {0x52EFF2DBL,0x52EFF2DBL,0x52EFF2DBL,0x52EFF2DBL,0x52EFF2DBL};
static int32_t *g_670 = (void*)0;
static int16_t g_675 = 9L;


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static int32_t  func_18(int8_t  p_19, uint64_t  p_20, int64_t  p_21, uint64_t  p_22, uint32_t  p_23);
static int64_t  func_47(const float  p_48);
static const uint16_t  func_52(const int32_t * p_53);
static const int32_t * func_54(int32_t * p_55, int32_t  p_56, int32_t * p_57);
static int32_t  func_58(int32_t * p_59, uint64_t  p_60, int32_t * p_61, uint64_t  p_62, int32_t * p_63);
static int32_t * func_64(float  p_65, int32_t * p_66, int16_t  p_67, int32_t * const  p_68);
static float  func_77(uint8_t  p_78, uint16_t  p_79, int16_t  p_80, int32_t * p_81);
static uint16_t  func_106(float * p_107, int8_t  p_108, float  p_109, uint32_t  p_110, uint32_t  p_111);
static int32_t  func_113(uint32_t  p_114, float  p_115);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_31 g_35 g_91 g_90 g_98 g_86 g_161 g_178 g_154 g_199 g_201 g_176 g_271 g_279 g_291 g_298 g_300 g_304 g_301 g_321 g_329 g_84 g_278 g_320 g_394 g_96 g_675
 * writes: g_91 g_84 g_96 g_98 g_86 g_154 g_161 g_176 g_178 g_199 g_201 g_279 g_291 g_304 g_320 g_357 g_394 g_278 g_35 g_670 g_90
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    uint8_t l_17 = 0xE6L;
    int32_t l_28 = 1L;
    uint16_t l_32 = 0x9BA5L;
    float *l_673[9][5] = {{&g_84[1],&g_278,(void*)0,&g_278,&g_96},{(void*)0,(void*)0,&g_278,&g_278,(void*)0},{(void*)0,(void*)0,(void*)0,&g_96,&g_84[2]},{&g_84[2],(void*)0,&g_96,(void*)0,&g_84[2]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_84[2],&g_278,&g_84[2],&g_278,(void*)0},{(void*)0,&g_84[2],&g_84[2],(void*)0,&g_84[2]},{(void*)0,(void*)0,(void*)0,&g_278,&g_84[2]},{&g_84[1],&g_278,(void*)0,(void*)0,(void*)0}};
    int32_t l_674 = 0xBA356732L;
    int i, j;
    g_84[2] = (0x9.B642C7p-95 <= (safe_sub_func_float_f_f(((((safe_unary_minus_func_int32_t_s(g_5)) , 0x7.B5CA98p+32) <= (safe_mul_func_float_f_f((safe_div_func_float_f_f(((+(safe_add_func_float_f_f((safe_sub_func_float_f_f((l_674 = (((l_17 != ((((func_18(((l_17 && (safe_add_func_uint8_t_u_u((l_28 = (safe_rshift_func_uint8_t_u_u(0x3BL, 2))), ((((-8L) == (l_17 < g_31)) != l_17) || l_17)))) ^ 1UL), l_17, l_32, l_32, l_17) != l_32) , l_32) < 1UL) > g_31)) ^ l_17) , 0x5.2E4900p-23)), g_321[0])), l_32))) <= 0x1.3p+1), g_321[0])), 0xD.1DB00Fp+41))) <= l_32), l_32)));
    return g_675;
}


/* ------------------------------------------ */
/* 
 * reads : g_35 g_31 g_91 g_90 g_98 g_86 g_161 g_178 g_154 g_199 g_201 g_176 g_271 g_279 g_291 g_298 g_300 g_304 g_301 g_321 g_329 g_84 g_278 g_320 g_394 g_96
 * writes: g_91 g_84 g_96 g_98 g_86 g_154 g_161 g_176 g_178 g_199 g_201 g_279 g_291 g_304 g_320 g_357 g_394 g_278 g_35 g_670 g_90
 */
static int32_t  func_18(int8_t  p_19, uint64_t  p_20, int64_t  p_21, uint64_t  p_22, uint32_t  p_23)
{ /* block id: 2 */
    int32_t l_33[7][7][4] = {{{0x4CE10B49L,1L,0xE094BF7DL,0xE094BF7DL},{0xFDF20BE8L,0xFDF20BE8L,7L,(-9L)},{0xE094BF7DL,0x4ABF0A64L,0xE3F0E26AL,1L},{0x1300CA3CL,(-1L),0L,0xE3F0E26AL},{0x97D93CBFL,(-1L),1L,1L},{(-1L),0x4ABF0A64L,(-1L),(-9L)},{1L,0xFDF20BE8L,0x97D93CBFL,0xE094BF7DL}},{{7L,1L,0xEB300A9EL,(-1L)},{7L,0x8FA7AD11L,0x9C7279C9L,(-5L)},{(-1L),0L,1L,0L},{(-9L),1L,(-1L),0x7AA29676L},{(-1L),(-3L),(-1L),0x826243A5L},{0xEB300A9EL,0x17C264F8L,0xFDF20BE8L,1L},{0x4ABF0A64L,0L,0x7AB83DE3L,0x17C264F8L}},{{1L,0L,0x7AB83DE3L,0x18158FB5L},{0x4ABF0A64L,7L,0xFDF20BE8L,0x9C7279C9L},{0xEB300A9EL,0x4CE10B49L,(-1L),0x32955F1BL},{(-1L),0x32955F1BL,(-1L),0x7AB83DE3L},{(-9L),0xEB300A9EL,1L,0x4CE10B49L},{(-1L),(-1L),0x9C7279C9L,8L},{7L,(-1L),0xEB300A9EL,(-3L)}},{{7L,0x97D93CBFL,0x97D93CBFL,7L},{1L,1L,(-1L),0xEB300A9EL},{(-1L),0x18158FB5L,1L,(-3L)},{7L,(-1L),0xE094BF7DL,0x9C7279C9L},{0L,1L,(-3L),0x826243A5L},{1L,0x7AB83DE3L,(-10L),0x1300CA3CL},{7L,7L,1L,0xEB300A9EL}},{{8L,(-1L),0x1300CA3CL,0xE3F0E26AL},{0x18158FB5L,(-6L),(-1L),8L},{1L,0x826243A5L,0x8FA7AD11L,(-1L)},{0x9C7279C9L,0L,0xE3F0E26AL,0L},{0L,8L,0L,1L},{1L,0x1300CA3CL,0xEB300A9EL,1L},{0xE094BF7DL,0x32955F1BL,0x7AB83DE3L,0x7AA29676L}},{{0xE094BF7DL,0L,0xEB300A9EL,0x4ABF0A64L},{1L,0x7AA29676L,0L,(-1L)},{0L,0xEB300A9EL,0xE3F0E26AL,0x18158FB5L},{0x9C7279C9L,0L,0x8FA7AD11L,0L},{1L,0x8FA7AD11L,(-1L),0L},{0x18158FB5L,(-1L),0x1300CA3CL,0x97D93CBFL},{8L,0x4ABF0A64L,1L,1L}},{{7L,7L,(-10L),0L},{1L,0x4CE10B49L,(-3L),0x7AB83DE3L},{0L,0x17C264F8L,0xE094BF7DL,(-3L)},{7L,0x17C264F8L,0x4ABF0A64L,0x7AB83DE3L},{0x17C264F8L,0x4CE10B49L,(-6L),0L},{1L,7L,7L,1L},{0x1300CA3CL,0x4ABF0A64L,0x826243A5L,0x97D93CBFL}}};
    int32_t *l_34 = &g_35[6];
    int32_t *l_36 = &g_35[1];
    int32_t *l_37 = (void*)0;
    int32_t *l_38 = &g_35[5];
    int32_t *l_39 = &g_35[6];
    int32_t *l_40[1];
    int8_t l_41 = 0x47L;
    uint32_t l_42 = 18446744073709551615UL;
    int32_t *l_666 = &l_33[5][0][1];
    int32_t **l_665 = &l_666;
    int32_t **l_667 = (void*)0;
    int32_t *l_669 = &l_33[6][5][2];
    int32_t **l_668 = &l_669;
    int64_t *l_671 = &g_90;
    int64_t l_672[3][8] = {{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)},{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)},{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_40[i] = &g_35[2];
    l_42++;
    l_672[2][5] &= (((*l_671) = ((safe_sub_func_int64_t_s_s(func_47(p_21), 1L)) == (((*l_665) = g_301) != (g_670 = ((*l_668) = (void*)0))))) ^ g_31);
    return p_21;
}


/* ------------------------------------------ */
/* 
 * reads : g_35 g_31 g_91 g_90 g_98 g_86 g_161 g_178 g_154 g_199 g_201 g_176 g_271 g_279 g_291 g_298 g_300 g_304 g_301 g_321 g_329 g_84 g_278 g_320 g_394 g_96
 * writes: g_91 g_84 g_96 g_98 g_86 g_154 g_161 g_176 g_178 g_199 g_201 g_279 g_291 g_304 g_320 g_357 g_394 g_278 g_35
 */
static int64_t  func_47(const float  p_48)
{ /* block id: 4 */
    int32_t * const l_51 = (void*)0;
    int32_t l_83 = 1L;
    float *l_94 = &g_84[7];
    float *l_95 = &g_96;
    float *l_97 = &g_98;
    uint32_t *l_430 = &g_199;
    uint32_t l_432 = 0xC4BDB0D1L;
    int8_t *l_437 = &g_304[1][4][1];
    uint64_t l_494 = 18446744073709551611UL;
    int32_t l_506 = 0x7B1E7B70L;
    int32_t l_508[10] = {6L,6L,6L,6L,6L,6L,6L,6L,6L,6L};
    uint64_t l_516 = 0xE7159D5EC1698E56LL;
    int32_t l_564 = 0L;
    int16_t *l_630 = (void*)0;
    float l_635 = (-0x1.9p-1);
    int32_t *l_648 = &g_35[3];
    int i;
    if ((l_83 = (safe_sub_func_uint16_t_u_u((g_35[3] < ((void*)0 != l_51)), func_52(func_54(&g_35[6], func_58(func_64(((safe_mul_func_float_f_f((g_35[3] == (safe_mul_func_float_f_f((((*l_97) = ((l_51 != l_51) <= (safe_sub_func_float_f_f((safe_mul_func_float_f_f((((*l_95) = ((*l_94) = (func_77((!(((void*)0 != l_51) & g_35[6])), g_31, l_83, &l_83) < 0x1.8DB56Ep+46))) != p_48), 0x8.7A7CD7p+60)), g_35[0])))) <= p_48), g_35[2]))), 0x5.Ep+1)) >= 0xB.8A93D2p+95), &g_35[6], g_90, &g_35[0]), g_35[6], &l_83, g_321[0], &l_83), &l_83))))))
    { /* block id: 148 */
        int8_t l_411[3][9][3] = {{{5L,1L,5L},{3L,(-2L),3L},{0x34L,1L,0x10L},{7L,(-1L),7L},{(-1L),(-1L),0x57L},{7L,3L,(-1L)},{0x34L,0x57L,0x4BL},{3L,0L,0L},{5L,0x34L,0x4BL}},{{3L,(-1L),(-1L)},{0x10L,(-1L),0x57L},{7L,1L,7L},{0x57L,(-1L),0x10L},{(-1L),(-1L),3L},{0x4BL,0x34L,5L},{0L,0L,3L},{0x4BL,0x57L,0x34L},{(-1L),3L,7L}},{{0x57L,(-1L),(-1L)},{(-2L),1L,3L},{5L,0x4BL,1L},{7L,3L,0L},{(-1L),0x29L,(-1L)},{0L,3L,7L},{1L,0x4BL,5L},{3L,1L,(-2L)},{0xFDL,0xFDL,(-1L)}}};
        int i, j, k;
        (*g_301) = 0x49719CCDL;
        return l_411[0][5][1];
    }
    else
    { /* block id: 151 */
        uint64_t l_431 = 0xA4263F55E92D356ALL;
        int32_t ** const l_465 = &g_301;
        uint8_t ** const l_485[8][8][2] = {{{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271}},{{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271}},{{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0}},{{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0}},{{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0}},{{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271}},{{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271}},{{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,&g_271},{&g_271,(void*)0},{&g_271,&g_271},{&g_271,&g_271}}};
        int32_t l_491 = 0x0DFD8631L;
        uint16_t *l_498[4][5] = {{&g_453,&g_453,&g_453,&g_453,&g_453},{&g_91[0][3],&g_91[0][3],&g_91[0][3],&g_91[0][3],&g_91[0][3]},{&g_453,&g_453,&g_453,&g_453,&g_453},{&g_91[0][3],&g_91[0][3],&g_91[0][3],&g_91[0][3],&g_91[0][3]}};
        uint16_t **l_497 = &l_498[3][4];
        int32_t l_512 = 0xBEA14B6CL;
        int32_t l_513 = 1L;
        int32_t l_515[10] = {0xA075C438L,0x8FFB4F1BL,0xA075C438L,0xA075C438L,0x8FFB4F1BL,0xA075C438L,0xA075C438L,0x8FFB4F1BL,0xA075C438L,0xA075C438L};
        uint16_t l_547 = 0xCC91L;
        int16_t l_561 = (-8L);
        uint32_t l_603 = 0x8A21B464L;
        int16_t **l_611 = &g_176;
        int16_t ***l_610 = &l_611;
        int32_t l_623 = (-1L);
        int32_t l_634 = 9L;
        int i, j, k;
        for (g_161 = 23; (g_161 != 15); --g_161)
        { /* block id: 154 */
            uint32_t *l_424 = &g_199;
            int32_t l_435 = 1L;
            int32_t **l_466 = &g_301;
            uint16_t l_481 = 9UL;
            int32_t l_507 = 0L;
            int32_t l_509 = 0xDA641497L;
            int32_t l_510 = 0x4D590C7DL;
            int32_t l_514[3][2] = {{0xDA0D2B2AL,0xDA0D2B2AL},{0xDA0D2B2AL,0xDA0D2B2AL},{0xDA0D2B2AL,0xDA0D2B2AL}};
            uint32_t l_566 = 18446744073709551606UL;
            int64_t l_592 = 0xD7F67F8F1EB24045LL;
            uint32_t l_616 = 0xEB4F59AEL;
            int16_t *l_631[6] = {&g_161,&g_161,&g_161,(void*)0,(void*)0,&g_161};
            int32_t l_652 = (-10L);
            uint32_t l_654[2][4][3] = {{{0x8939E05AL,4UL,0x8939E05AL},{0UL,0UL,0x00123F26L},{0x3157A591L,4UL,0x3157A591L},{0UL,0x00123F26L,0x00123F26L}},{{0x8939E05AL,4UL,0x8939E05AL},{0UL,0UL,0x00123F26L},{0x3157A591L,4UL,0x3157A591L},{0UL,0x00123F26L,0x00123F26L}}};
            int i, j, k;
            for (g_291 = 0; (g_291 != 24); g_291++)
            { /* block id: 157 */
                int16_t *l_416 = &g_417;
                const int16_t *l_422 = &g_423;
                const int16_t **l_421 = &l_422;
                const int16_t ***l_420 = &l_421;
                uint32_t **l_425 = (void*)0;
                uint32_t *l_427[3][7][1] = {{{&g_199},{&g_199},{&g_199},{&g_199},{&g_199},{&g_199},{&g_199}},{{&g_199},{&g_199},{&g_199},{&g_199},{&g_199},{&g_199},{&g_199}},{{&g_199},{&g_199},{&g_199},{&g_199},{&g_199},{&g_199},{&g_199}}};
                uint32_t **l_426 = &l_427[0][2][0];
                uint32_t *l_429[7][6] = {{&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0},{&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0},{&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0},{&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0},{&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0},{&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0},{&g_199,&g_199,(void*)0,&g_199,&g_199,(void*)0}};
                uint32_t **l_428 = &l_429[5][5];
                int64_t *l_433 = (void*)0;
                int64_t *l_434 = &g_90;
                int8_t *l_436[9][3][4] = {{{&g_304[1][2][0],&g_320,&g_320,&g_320},{&g_304[4][1][3],&g_320,(void*)0,(void*)0},{(void*)0,&g_304[4][1][3],&g_304[4][1][3],&g_304[1][2][0]}},{{(void*)0,&g_304[4][1][3],&g_304[4][1][3],&g_304[4][1][3]},{&g_304[4][1][3],&g_320,&g_320,&g_320},{&g_320,&g_304[4][1][3],(void*)0,&g_304[4][1][3]}},{{&g_304[4][2][0],&g_320,(void*)0,(void*)0},{(void*)0,(void*)0,&g_320,&g_320},{(void*)0,&g_304[0][3][3],(void*)0,&g_320}},{{&g_304[4][2][0],&g_320,(void*)0,(void*)0},{&g_320,(void*)0,&g_320,(void*)0},{&g_304[4][1][3],(void*)0,&g_304[4][1][3],&g_320}},{{(void*)0,&g_320,&g_304[4][1][3],&g_320},{(void*)0,&g_320,(void*)0,&g_304[4][2][0]},{&g_304[4][1][3],&g_320,&g_320,(void*)0}},{{&g_304[1][2][0],(void*)0,(void*)0,&g_304[0][3][3]},{&g_304[4][1][3],&g_304[4][1][3],&g_304[4][1][3],&g_304[4][1][3]},{(void*)0,&g_320,&g_320,(void*)0}},{{(void*)0,&g_320,&g_320,&g_304[4][1][3]},{(void*)0,&g_304[4][1][3],&g_320,&g_304[4][1][3]},{(void*)0,&g_320,(void*)0,(void*)0}},{{&g_320,&g_320,(void*)0,&g_304[4][1][3]},{&g_320,&g_304[4][1][3],(void*)0,&g_304[0][3][3]},{&g_320,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_320,&g_304[4][1][2],&g_304[4][2][0]},{(void*)0,&g_320,&g_320,&g_320},{&g_320,(void*)0,&g_320,(void*)0}}};
                int32_t l_438[7];
                uint64_t *l_501 = &g_394[5][3][2];
                uint64_t **l_500 = &l_501;
                int32_t l_539 = 0x72146CE4L;
                int32_t l_540 = 0x6225E94BL;
                uint16_t **l_552 = &l_498[3][4];
                int32_t **l_591 = &g_301;
                int16_t *l_593 = (void*)0;
                int16_t *l_594[8] = {&l_561,&l_561,&l_561,&l_561,&l_561,&l_561,&l_561,&l_561};
                int64_t l_645 = 0x878596ECC18A5D37LL;
                int16_t l_653 = 0x9494L;
                int i, j, k;
                for (i = 0; i < 7; i++)
                    l_438[i] = 7L;
            }
            return (**l_465);
        }
    }
    (*g_301) = (*g_301);
    return g_321[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_86
 * writes: g_86
 */
static const uint16_t  func_52(const int32_t * p_53)
{ /* block id: 144 */
    uint32_t l_409 = 4294967289UL;
    int32_t *l_410 = &g_86;
    (*l_410) = l_409;
    return (*l_410);
}


/* ------------------------------------------ */
/* 
 * reads : g_278 g_291 g_394 g_96
 * writes: g_394 g_278
 */
static const int32_t * func_54(int32_t * p_55, int32_t  p_56, int32_t * p_57)
{ /* block id: 140 */
    uint64_t *l_404 = &g_394[4][4][2];
    int32_t l_407 = 0L;
    float *l_408 = &g_278;
    (*l_408) = (safe_add_func_float_f_f(((((safe_lshift_func_uint8_t_u_s(p_56, 6)) , (((p_56 , (safe_sub_func_float_f_f((-(0x9.Bp-1 < ((g_278 == 0x5.FC86FDp-76) < (0xA.76B645p-91 < (-g_291))))), ((((((*l_404)--) , l_407) ^ (0xC30A2C6EL & 0x5C2CDBFBL)) , l_407) != l_407)))) != l_407) > 0x6.8ED2F3p-57)) != p_56) < p_56), g_96));
    return p_55;
}


/* ------------------------------------------ */
/* 
 * reads : g_86 g_84 g_278 g_320 g_178 g_91 g_291 g_201
 * writes: g_357 g_161 g_291 g_201
 */
static int32_t  func_58(int32_t * p_59, uint64_t  p_60, int32_t * p_61, uint64_t  p_62, int32_t * p_63)
{ /* block id: 110 */
    float *l_356[8] = {&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2],&g_84[2]};
    int32_t l_358[10];
    int32_t l_359 = 0L;
    uint8_t *l_370 = &g_178;
    int16_t *l_371[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const uint8_t l_372 = 255UL;
    int i;
    for (i = 0; i < 10; i++)
        l_358[i] = 0xA88BC39DL;
    l_359 = (safe_div_func_float_f_f((l_358[8] = (g_357 = 0x0.D8113Cp-59)), g_86));
    l_358[4] = ((g_84[0] != ((safe_div_func_float_f_f(g_278, ((((g_320 , (safe_mod_func_int32_t_s_s((safe_add_func_uint8_t_u_u(((safe_div_func_int8_t_s_s((-1L), 251UL)) < ((0x554CL < (safe_add_func_int64_t_s_s(g_178, ((g_161 = ((0x79L == (((void*)0 == l_370) ^ p_60)) <= l_359)) , l_358[6])))) & p_62)), 0x70L)), l_358[1]))) , 1L) && l_372) , l_372))) >= g_91[0][3])) < 0xA.23C349p-42);
    for (g_291 = 0; (g_291 <= 0); g_291 += 1)
    { /* block id: 118 */
        return g_201;
    }
    for (g_201 = 0; (g_201 <= 5); g_201 += 1)
    { /* block id: 123 */
        int32_t **l_373[2];
        int16_t *l_376 = &g_161;
        int i;
        for (i = 0; i < 2; i++)
            l_373[i] = &g_301;
    }
    return p_60;
}


/* ------------------------------------------ */
/* 
 * reads : g_90 g_31 g_91 g_35 g_98 g_86 g_161 g_178 g_154 g_199 g_201 g_176 g_271 g_279 g_291 g_298 g_300 g_304 g_301 g_321 g_329
 * writes: g_86 g_154 g_161 g_176 g_178 g_199 g_201 g_84 g_279 g_291 g_304 g_320
 */
static int32_t * func_64(float  p_65, int32_t * p_66, int16_t  p_67, int32_t * const  p_68)
{ /* block id: 11 */
    int16_t l_105 = 0xD9E4L;
    float *l_112 = (void*)0;
    uint8_t l_116 = 0x23L;
    float *l_119 = &g_84[0];
    float *l_120 = &g_84[0];
    int32_t *l_139 = &g_86;
    int32_t *l_289 = (void*)0;
    int32_t *l_290 = &g_291;
    int32_t l_337 = 0L;
    int32_t l_338 = 0xBF5C7C5DL;
    int32_t l_339 = 0x2A9A8A20L;
    int32_t l_340 = 0x513AD5C0L;
    int32_t l_341 = 0xBDBB9429L;
    int32_t l_342 = 0xEDA2E692L;
    int32_t l_343 = (-4L);
    int32_t l_344 = (-3L);
    int32_t l_345 = 0L;
    int32_t l_346 = 0L;
    int32_t l_347 = 0L;
    int32_t l_348 = 0xDEF9147DL;
    int32_t l_349[8] = {0x3D974655L,0L,0x3D974655L,0L,0x3D974655L,0L,0x3D974655L,0L};
    int i;
    (*l_290) ^= ((*l_139) = (safe_sub_func_uint16_t_u_u((safe_sub_func_int16_t_s_s((safe_div_func_int16_t_s_s(l_105, func_106(l_112, (g_90 , (p_67 , (g_31 ^ ((*l_139) = func_113(l_116, (safe_add_func_float_f_f((l_119 == (l_105 , (l_120 = l_112))), p_67))))))), g_35[5], g_35[5], g_91[0][2]))), 0xD19DL)), 0UL)));
    for (g_154 = 0; (g_154 > (-4)); --g_154)
    { /* block id: 86 */
        int32_t **l_302[3];
        int32_t l_350 = 7L;
        uint32_t l_351 = 0xDE143D4CL;
        int i;
        for (i = 0; i < 3; i++)
            l_302[i] = &g_301;
        for (g_291 = 3; (g_291 <= 8); g_291 += 1)
        { /* block id: 89 */
            int8_t *l_303 = &g_304[4][1][3];
            int32_t l_318 = 0xAA7E5032L;
            uint64_t *l_324[3][4][3];
            uint64_t **l_323 = &l_324[1][0][0];
            uint64_t l_334 = 18446744073709551609UL;
            int i, j, k;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    for (k = 0; k < 3; k++)
                        l_324[i][j][k] = &g_201;
                }
            }
            (*l_139) ^= (0UL > (safe_div_func_int8_t_s_s(((*l_303) |= ((safe_sub_func_int64_t_s_s((g_298 != (l_302[2] = g_300[0][1])), 0UL)) ^ (g_199 = g_31))), p_67)));
            for (l_116 = 1; (l_116 <= 8); l_116 += 1)
            { /* block id: 96 */
                float l_307 = 0x0.Cp-1;
                int32_t l_314 = 0xDCC1F51FL;
                const int16_t **l_315 = (void*)0;
                const int16_t *l_317 = (void*)0;
                const int16_t **l_316 = &l_317;
                int8_t *l_319[3];
                uint64_t **l_322 = (void*)0;
                int i;
                for (i = 0; i < 3; i++)
                    l_319[i] = &g_320;
                (*l_139) |= ((((safe_sub_func_uint16_t_u_u(p_67, g_178)) != ((safe_mod_func_int64_t_s_s((-6L), p_67)) == p_67)) >= ((safe_rshift_func_uint8_t_u_s(((g_320 = (g_199 == (safe_sub_func_uint32_t_u_u(((((*l_316) = (((((*g_301) , 0x74L) < ((((*l_303) |= l_314) | 9UL) | (*p_68))) & l_314) , &l_105)) == &g_161) || 1UL), l_318)))) , 0x41L), 6)) <= g_154)) & g_321[0]);
                p_66 = (l_314 , p_66);
                l_323 = l_322;
            }
            (*l_139) = (0UL == (safe_sub_func_int32_t_s_s((safe_mul_func_uint8_t_u_u(p_67, (((void*)0 == g_329) != 0x96C4F1D6L))), (safe_lshift_func_uint16_t_u_s(0x02B2L, 11)))));
            --l_334;
        }
        l_351++;
    }
    return p_66;
}


/* ------------------------------------------ */
/* 
 * reads : g_91
 * writes: g_91
 */
static float  func_77(uint8_t  p_78, uint16_t  p_79, int16_t  p_80, int32_t * p_81)
{ /* block id: 5 */
    int32_t *l_85 = &g_86;
    int32_t *l_87 = (void*)0;
    int32_t *l_88 = &g_86;
    int32_t *l_89[9][9] = {{&g_35[5],(void*)0,&g_86,&g_35[6],&g_35[6],&g_86,(void*)0,&g_35[5],&g_35[6]},{(void*)0,&g_86,&g_35[5],&g_86,(void*)0,(void*)0,&g_86,&g_35[5],&g_86},{&g_35[6],&g_35[5],(void*)0,&g_35[6],&g_35[6],&g_86,&g_86,&g_35[6],&g_35[6]},{&g_35[6],&g_35[4],&g_35[6],&g_86,&g_86,(void*)0,&g_86,&g_86,(void*)0},{(void*)0,&g_35[5],&g_35[6],&g_35[5],(void*)0,&g_35[6],&g_35[6],&g_86,&g_86},{&g_35[5],&g_86,(void*)0,&g_86,(void*)0,&g_86,&g_35[5],&g_86,(void*)0},{&g_86,(void*)0,&g_35[5],&g_35[6],&g_86,&g_35[6],&g_35[5],(void*)0,&g_86},{&g_86,&g_35[6],&g_86,&g_86,&g_35[6],(void*)0,&g_35[6],&g_86,&g_86},{&g_35[6],&g_35[6],&g_86,&g_35[6],(void*)0,&g_86,&g_86,&g_86,(void*)0}};
    int i, j;
    --g_91[0][3];
    return p_78;
}


/* ------------------------------------------ */
/* 
 * reads : g_86 g_91 g_161 g_178 g_154 g_199 g_35 g_201 g_176 g_90 g_31 g_271 g_279
 * writes: g_86 g_154 g_161 g_176 g_178 g_199 g_201 g_84 g_279
 */
static uint16_t  func_106(float * p_107, int8_t  p_108, float  p_109, uint32_t  p_110, uint32_t  p_111)
{ /* block id: 17 */
    int32_t l_159 = 0xAE464C47L;
    int16_t *l_170 = &g_161;
    float *l_185 = (void*)0;
    uint16_t l_197 = 0x2E9DL;
    int8_t l_237[8] = {(-3L),(-3L),(-2L),(-3L),(-3L),(-2L),(-3L),(-3L)};
    uint64_t *l_259 = &g_201;
    uint8_t *l_270 = &g_178;
    int32_t l_277 = 0xDAC85E21L;
    int32_t **l_284 = (void*)0;
    int32_t *l_285 = &g_86;
    int32_t **l_288 = &l_285;
    int i;
    for (p_108 = 19; (p_108 > 26); p_108 = safe_add_func_int16_t_s_s(p_108, 2))
    { /* block id: 20 */
        int32_t *l_143 = &g_35[1];
        int32_t **l_142 = &l_143;
        int32_t *l_144 = &g_86;
        uint16_t l_175 = 65526UL;
        uint64_t *l_258 = (void*)0;
        uint8_t l_269 = 0x19L;
        g_86 |= p_110;
        if (((((*l_142) = &g_86) != (l_144 = p_107)) < p_111))
        { /* block id: 24 */
            int16_t *l_160 = &g_161;
            (**l_142) = (((safe_mul_func_uint8_t_u_u(((((*l_160) = ((safe_sub_func_uint16_t_u_u((g_91[0][0] <= (safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint8_t_u_s((&g_86 != (g_86 , p_107)), 5)), ((((-3L) != (((+(g_154 = (**l_142))) <= (safe_add_func_int8_t_s_s(p_110, g_91[0][2]))) || ((safe_div_func_uint16_t_u_u(g_91[0][1], l_159)) <= 255UL))) & 0xC1B6DC2EE892D9D7LL) < p_111)))), p_111)) >= l_159)) ^ 0x9A8EL) > 0x9CF343CDL), g_91[0][0])) < 4294967292UL) & p_111);
            (*l_142) = &l_159;
        }
        else
        { /* block id: 29 */
            int32_t *l_162 = &g_86;
            int32_t *l_163[10] = {&g_35[6],&l_159,&g_35[6],&g_35[6],&l_159,&g_35[6],&g_35[6],&l_159,&g_35[6],&g_35[6]};
            int32_t l_164 = 0L;
            uint32_t l_165 = 4294967295UL;
            uint8_t *l_177 = &g_178;
            int8_t l_251 = 1L;
            int i;
            l_165--;
            if ((safe_div_func_uint8_t_u_u(((*l_177) ^= (l_170 == (g_176 = ((l_175 ^= (safe_rshift_func_int8_t_s_s((((*l_170) |= g_86) < 0x778DL), ((safe_add_func_int8_t_s_s(((l_159 , &l_159) == (*l_142)), ((void*)0 != l_163[2]))) < p_111)))) , &g_161)))), g_154)))
            { /* block id: 35 */
                uint32_t l_198 = 0UL;
                uint64_t *l_200 = &g_201;
                int32_t *l_207 = &l_159;
                int32_t l_226 = 0x8FA292BBL;
                float *l_272[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_272[i] = &g_96;
                (*l_143) ^= (((safe_lshift_func_int8_t_s_s(((safe_div_func_int8_t_s_s((-3L), (--(*l_177)))) ^ p_110), ((l_185 != l_143) < ((*l_200) ^= (p_111 >= (safe_lshift_func_int16_t_s_u((((safe_mod_func_uint64_t_u_u(((1L ^ ((p_108 <= (((safe_lshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u(((g_199 ^= ((safe_sub_func_uint64_t_u_u(((((!(1L | (g_154 < 18446744073709551615UL))) & l_197) , l_198) || 0x97A8B776L), 0xFC9525464414D6DALL)) , p_111)) | g_161), p_110)), 0)) ^ p_110) || p_111)) >= (-1L))) ^ 1L), p_111)) | g_161) > p_111), g_35[1]))))))) | p_110) == 0xEAL);
                for (l_175 = 10; (l_175 > 27); l_175 = safe_add_func_int16_t_s_s(l_175, 1))
                { /* block id: 42 */
                    uint8_t ** const l_204 = &l_177;
                    uint8_t **l_206[4][10];
                    uint8_t ***l_205 = &l_206[2][1];
                    int32_t l_223 = 0x38824DB7L;
                    float *l_224 = &g_96;
                    uint32_t l_225 = 0x697A0A95L;
                    int32_t *l_238 = &g_86;
                    int i, j;
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 10; j++)
                            l_206[i][j] = (void*)0;
                    }
                    (*l_205) = l_204;
                    (**l_142) = g_86;
                    l_207 = &g_35[6];
                    if ((safe_mod_func_int64_t_s_s((safe_rshift_func_uint16_t_u_u((((safe_mod_func_uint32_t_u_u(((safe_rshift_func_int8_t_s_u(((((safe_div_func_uint64_t_u_u(p_110, 0x225F419C157CCD4ELL)) & (((safe_mul_func_uint8_t_u_u(((p_111 , l_144) != l_162), (safe_unary_minus_func_int16_t_s(0x5632L)))) >= (l_207 != ((safe_mul_func_uint16_t_u_u(l_223, g_91[0][0])) , l_224))) && (*g_176))) && (-1L)) , (-7L)), 1)) >= l_225), g_91[0][3])) & p_110) || l_226), 1)), g_91[0][0])))
                    { /* block id: 46 */
                        return l_223;
                    }
                    else
                    { /* block id: 48 */
                        uint8_t l_239 = 0x28L;
                        (*l_143) |= ((safe_mod_func_uint32_t_u_u(p_108, (((safe_rshift_func_int16_t_s_s(((((((((((1UL < ((safe_add_func_int16_t_s_s((safe_add_func_uint32_t_u_u(l_225, (g_35[6] == p_108))), ((*g_176) = (g_90 == (safe_rshift_func_uint16_t_u_s(p_111, 2)))))) >= 18446744073709551607UL)) , 0x4A8BL) || 1L) != l_237[3]) , (void*)0) != (void*)0) == 0xACE6L) & (*l_207)) != 0x901DL) , 0xC1D6L), 12)) < (*l_207)) || 0xF589615DL))) != 0xE1203BA3660699C9LL);
                        l_238 = &l_159;
                        (*l_238) = ((*l_162) ^= l_239);
                    }
                }
                (*l_142) = (void*)0;
                (*l_142) = ((safe_sub_func_float_f_f(((g_84[4] = ((((((safe_mul_func_uint8_t_u_u(g_35[4], ((((safe_lshift_func_uint8_t_u_u((safe_add_func_int16_t_s_s((safe_unary_minus_func_uint8_t_u(((safe_mul_func_int16_t_s_s((l_251 , (-10L)), ((((safe_lshift_func_int8_t_s_u((safe_add_func_int16_t_s_s((g_199 && ((safe_add_func_uint8_t_u_u(((l_258 == l_259) ^ (safe_mul_func_int8_t_s_s((((*l_177) |= (g_91[0][1] != (safe_div_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((g_31 , (safe_div_func_uint32_t_u_u((!(*l_207)), g_91[0][3]))), p_110)) | 0x988937E8L), p_108)))) , p_108), 9L))), (*l_162))) ^ 255UL)), l_269)), 0)) > (*l_207)) != p_110) > g_91[0][3]))) >= g_31))), l_197)), p_111)) >= p_111) || p_111) || p_110))) && g_90) == p_111) <= g_90) , l_270) == g_271)) == 0xA.01AF70p-31), 0x8.4F8C2Ap-82)) , &g_86);
            }
            else
            { /* block id: 60 */
                (*l_162) = g_35[4];
                (*l_143) = ((p_108 , (safe_rshift_func_int16_t_s_u(((*l_170) ^= (g_199 , (safe_mul_func_uint8_t_u_u(0x89L, g_91[0][2])))), 4))) == p_111);
            }
            g_279[2]--;
        }
        if ((**l_142))
            continue;
        if ((~(p_111 & g_86)))
        { /* block id: 68 */
            return p_111;
        }
        else
        { /* block id: 70 */
            float l_283 = 0x3.80ACD6p+88;
            return p_108;
        }
    }
    l_285 = &l_159;
    for (l_159 = 6; (l_159 >= 0); l_159 -= 1)
    { /* block id: 77 */
        float l_286[1][9][10] = {{{0x5.Ap+1,0x4.00BED7p-14,0x8.97EB01p+99,(-0x6.6p+1),(-0x3.0p+1),0x1.E9FAB2p+10,(-0x1.3p-1),(-0x7.5p-1),0x0.1p+1,0x0.1p+1},{0x4.00BED7p-14,0xA.5D3EA8p+34,0x1.E9FAB2p+10,0x5.8p+1,0x5.8p+1,0x1.E9FAB2p+10,0xA.5D3EA8p+34,0x4.00BED7p-14,(-0x6.6p+1),(-0x1.3p-1)},{0x5.Ap+1,0x8.97EB01p+99,0x6.0DB634p+7,0x4.00BED7p-14,0x7.Cp-1,0x5.8p+1,0xA.CB3B04p+40,(-0x1.3p-1),0xA.CB3B04p+40,0x5.8p+1},{(-0x5.Dp+1),0x7.Cp-1,0x6.0DB634p+7,0x7.Cp-1,(-0x5.Dp+1),(-0x1.3p-1),(-0x6.6p+1),0x4.00BED7p-14,0xA.5D3EA8p+34,0x1.E9FAB2p+10},{(-0x6.6p+1),(-0x3.0p+1),0x1.E9FAB2p+10,(-0x1.3p-1),(-0x7.5p-1),0x0.1p+1,0x0.1p+1,(-0x7.5p-1),(-0x1.3p-1),0x1.E9FAB2p+10},{(-0x1.3p-1),0x6.0DB634p+7,0xA.5D3EA8p+34,(-0x3.0p+1),0x2.Ep+1,0x0.1p+1,0x5.8p+1,0x4.00BED7p-14,(-0x7.5p-1),(-0x5.Dp+1)},{0x4.00BED7p-14,0x7.Cp-1,0x5.8p+1,0xA.CB3B04p+40,(-0x1.3p-1),0xA.CB3B04p+40,0x5.8p+1,0x7.Cp-1,0x4.00BED7p-14,0x6.0DB634p+7},{(-0x8.Ep+1),0x6.0DB634p+7,(-0x7.5p-1),0x2.Ep+1,(-0x5.Dp+1),0x1.E9FAB2p+10,0x8.97EB01p+99,(-0x1.3p-1),(-0x1.3p-1),0x8.97EB01p+99},{0x7.Cp-1,(-0x8.Ep+1),0x2.Ep+1,0x2.Ep+1,(-0x8.Ep+1),0x7.Cp-1,0x5.Ap+1,0x1.E9FAB2p+10,0x4.00BED7p-14,0x0.1p+1}}};
        int32_t l_287 = 0x8633E49BL;
        int i, j, k;
        return l_287;
    }
    (*l_288) = &g_86;
    return p_111;
}


/* ------------------------------------------ */
/* 
 * reads : g_91 g_35 g_98
 * writes:
 */
static int32_t  func_113(uint32_t  p_114, float  p_115)
{ /* block id: 13 */
    float *l_127[10][5][5] = {{{&g_84[7],&g_98,(void*)0,&g_98,&g_84[7]},{&g_96,&g_98,&g_84[2],(void*)0,&g_84[2]},{(void*)0,(void*)0,(void*)0,&g_84[7],&g_84[7]},{&g_98,&g_96,&g_96,&g_98,&g_84[2]},{&g_98,&g_84[7],&g_84[2],&g_84[2],&g_84[7]}},{{&g_84[2],&g_96,&g_98,&g_98,&g_98},{&g_96,(void*)0,&g_96,&g_84[2],(void*)0},{(void*)0,&g_98,&g_98,&g_98,(void*)0},{&g_96,&g_98,(void*)0,&g_84[7],(void*)0},{&g_84[2],&g_84[2],&g_98,(void*)0,&g_84[2]}},{{&g_98,&g_96,&g_96,&g_98,(void*)0},{&g_98,(void*)0,&g_98,&g_98,(void*)0},{(void*)0,&g_96,&g_84[2],(void*)0,(void*)0},{&g_96,&g_84[2],&g_96,&g_98,&g_98},{&g_84[7],&g_98,(void*)0,&g_98,&g_84[7]}},{{&g_96,&g_98,&g_84[2],(void*)0,&g_84[2]},{(void*)0,(void*)0,(void*)0,&g_84[7],&g_84[7]},{&g_98,&g_96,&g_96,&g_98,&g_84[2]},{&g_98,&g_84[7],&g_84[2],&g_84[2],&g_84[7]},{&g_84[2],&g_96,&g_98,&g_98,&g_98}},{{&g_96,(void*)0,&g_96,&g_84[2],(void*)0},{(void*)0,&g_98,&g_98,&g_98,(void*)0},{&g_96,&g_98,(void*)0,&g_84[7],(void*)0},{&g_84[2],&g_84[2],&g_98,(void*)0,&g_84[2]},{&g_98,&g_96,&g_96,&g_98,(void*)0}},{{&g_98,(void*)0,&g_98,&g_98,(void*)0},{(void*)0,&g_96,&g_84[2],(void*)0,(void*)0},{&g_96,&g_84[2],&g_96,&g_98,&g_98},{&g_84[7],&g_98,(void*)0,&g_98,&g_84[7]},{&g_96,&g_98,&g_84[2],(void*)0,&g_84[2]}},{{(void*)0,(void*)0,(void*)0,&g_84[2],&g_98},{&g_96,(void*)0,(void*)0,&g_96,&g_84[2]},{&g_96,&g_84[2],(void*)0,(void*)0,&g_84[2]},{&g_84[2],(void*)0,&g_98,&g_84[2],&g_84[2]},{&g_84[7],&g_84[7],&g_84[7],(void*)0,(void*)0}},{{&g_98,&g_96,&g_84[2],&g_96,&g_98},{&g_84[7],&g_96,&g_84[7],&g_84[2],&g_84[7]},{&g_84[2],&g_84[2],&g_84[2],&g_98,&g_98},{&g_96,&g_84[7],&g_84[7],&g_96,&g_84[7]},{&g_96,&g_98,&g_98,&g_98,&g_98}},{{&g_84[7],&g_84[7],(void*)0,(void*)0,(void*)0},{(void*)0,&g_84[2],(void*)0,&g_98,&g_84[2]},{&g_84[2],&g_96,(void*)0,&g_96,&g_84[2]},{(void*)0,&g_96,&g_84[2],&g_98,&g_84[2]},{&g_84[7],&g_84[7],(void*)0,&g_84[2],&g_98}},{{&g_96,(void*)0,(void*)0,&g_96,&g_84[2]},{&g_96,&g_84[2],(void*)0,(void*)0,&g_84[2]},{&g_84[2],(void*)0,&g_98,&g_84[2],&g_84[2]},{&g_84[7],&g_84[7],&g_84[7],(void*)0,(void*)0},{&g_98,&g_96,&g_84[2],&g_96,&g_98}}};
    int32_t l_132 = (-6L);
    uint8_t l_137 = 0x58L;
    int64_t l_138 = 0x5BE42EE2B1DE1D06LL;
    int i, j, k;
    l_138 = (safe_add_func_float_f_f(((((safe_add_func_float_f_f(0x4.E36C85p-7, ((safe_sub_func_float_f_f(((void*)0 == l_127[5][1][4]), (safe_sub_func_float_f_f(p_114, (safe_sub_func_float_f_f(((p_114 > (((0x0830044FL ^ l_132) || (((p_114 > ((safe_sub_func_int8_t_s_s((safe_mod_func_uint64_t_u_u((p_114 >= g_91[0][1]), p_114)), l_132)) == 0x59FCL)) , g_35[6]) && 0UL)) , 0x6.B0D488p+23)) < l_132), p_114)))))) < g_98))) <= 0xB.140366p-98) == 0x0.6p+1) >= g_35[3]), l_137));
    return p_114;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_31, "g_31", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_35[i], "g_35[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_84[i], sizeof(g_84[i]), "g_84[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_86, "g_86", print_hash_value);
    transparent_crc(g_90, "g_90", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_91[i][j], "g_91[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_96, sizeof(g_96), "g_96", print_hash_value);
    transparent_crc_bytes (&g_98, sizeof(g_98), "g_98", print_hash_value);
    transparent_crc(g_154, "g_154", print_hash_value);
    transparent_crc(g_161, "g_161", print_hash_value);
    transparent_crc(g_178, "g_178", print_hash_value);
    transparent_crc(g_199, "g_199", print_hash_value);
    transparent_crc(g_201, "g_201", print_hash_value);
    transparent_crc_bytes (&g_278, sizeof(g_278), "g_278", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_279[i], "g_279[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_291, "g_291", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_304[i][j][k], "g_304[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_320, "g_320", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_321[i], "g_321[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_331[i], "g_331[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_357, "g_357", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_394[i][j][k], "g_394[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_417, "g_417", print_hash_value);
    transparent_crc(g_423, "g_423", print_hash_value);
    transparent_crc(g_453, "g_453", print_hash_value);
    transparent_crc(g_511, "g_511", print_hash_value);
    transparent_crc(g_585, "g_585", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_595[i], "g_595[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_675, "g_675", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 169
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 38
breakdown:
   depth: 1, occurrence: 75
   depth: 2, occurrence: 13
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
   depth: 7, occurrence: 2
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 15, occurrence: 1
   depth: 20, occurrence: 3
   depth: 22, occurrence: 1
   depth: 23, occurrence: 4
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 38, occurrence: 2

XXX total number of pointers: 155

XXX times a variable address is taken: 349
XXX times a pointer is dereferenced on RHS: 50
breakdown:
   depth: 1, occurrence: 20
   depth: 2, occurrence: 30
XXX times a pointer is dereferenced on LHS: 82
breakdown:
   depth: 1, occurrence: 74
   depth: 2, occurrence: 8
XXX times a pointer is compared with null: 10
XXX times a pointer is compared with address of another variable: 1
XXX times a pointer is compared with another pointer: 5
XXX times a pointer is qualified to be dereferenced: 2257

XXX max dereference level: 2
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 208
   level: 2, occurrence: 79
XXX number of pointers point to pointers: 43
XXX number of pointers point to scalars: 112
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 30.3
XXX average alias set size: 1.48

XXX times a non-volatile is read: 513
XXX times a non-volatile is write: 251
XXX times a volatile is read: 1
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 2
XXX percentage of non-volatile access: 99.9

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 70
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 11
   depth: 2, occurrence: 13
   depth: 3, occurrence: 9
   depth: 4, occurrence: 4
   depth: 5, occurrence: 4

XXX percentage a fresh-made variable is used: 17
XXX percentage an existing variable is used: 83
********************* end of statistics **********************/


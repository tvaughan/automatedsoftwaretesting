/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2704196400
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint16_t g_2 = 65532UL;
static int32_t g_6[3] = {0x4C3C69AEL,0x4C3C69AEL,0x4C3C69AEL};
static volatile int32_t g_51 = 0x833AED5EL;/* VOLATILE GLOBAL g_51 */
static int32_t g_52 = 0x88B1495DL;
static int32_t *g_57 = &g_6[1];
static int32_t ** volatile g_56[5] = {&g_57,&g_57,&g_57,&g_57,&g_57};
static volatile int32_t g_58[2] = {0xCDCBD5F5L,0xCDCBD5F5L};
static int32_t g_59[5] = {2L,2L,2L,2L,2L};
static volatile int32_t g_62 = 3L;/* VOLATILE GLOBAL g_62 */
static volatile int32_t g_63[7][8][2] = {{{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L}},{{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L}},{{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L}},{{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L}},{{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L}},{{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L}},{{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L},{0x0A862252L,0x0A862252L}}};
static int32_t g_64 = (-4L);
static float g_108 = 0x0.Dp+1;
static uint8_t g_109 = 0x4EL;
static int16_t g_122 = 5L;
static int64_t g_134 = 0xB3E8CBBEE733D09BLL;
static volatile int32_t * volatile g_141 = &g_58[1];/* VOLATILE GLOBAL g_141 */
static volatile int32_t * volatile *g_140 = &g_141;
static volatile int32_t * volatile * volatile *g_139 = &g_140;
static volatile int32_t * volatile * volatile * volatile *g_138[7] = {&g_139,&g_139,&g_139,&g_139,&g_139,&g_139,&g_139};
static volatile int32_t * volatile * volatile * volatile ** volatile g_142 = &g_138[1];/* VOLATILE GLOBAL g_142 */
static int16_t **** volatile g_145 = (void*)0;/* VOLATILE GLOBAL g_145 */
static uint32_t g_196 = 0UL;
static int8_t g_200 = 0x18L;
static uint32_t g_204 = 0UL;
static float g_217 = 0xD.F6C7F8p+32;
static float g_221 = 0x4.BF00CBp+55;
static float g_229[2][8] = {{0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43},{0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43,0x7.7F4DF6p+43}};
static float g_231 = 0x0.Ep+1;
static float * volatile g_230 = &g_231;/* VOLATILE GLOBAL g_230 */
static int64_t * const g_236[2][1][2] = {{{&g_134,&g_134}},{{&g_134,&g_134}}};
static int64_t * const *g_235 = &g_236[1][0][0];
static int64_t * const ** const  volatile g_234 = &g_235;/* VOLATILE GLOBAL g_234 */
static int32_t *g_245 = (void*)0;
static volatile float **g_291 = (void*)0;
static int32_t * volatile g_320[5][9][5] = {{{&g_6[2],&g_6[2],&g_59[0],&g_6[0],&g_6[1]},{&g_59[0],&g_52,&g_6[1],&g_59[3],&g_52},{&g_52,(void*)0,&g_6[1],&g_52,&g_59[3]},{(void*)0,&g_52,&g_64,&g_6[1],&g_6[0]},{(void*)0,&g_6[2],&g_52,&g_59[3],&g_59[2]},{&g_59[3],&g_52,&g_52,&g_59[3],&g_59[0]},{&g_6[1],(void*)0,&g_64,&g_6[2],&g_64},{&g_59[0],&g_6[0],&g_6[1],&g_59[0],(void*)0},{&g_64,&g_52,&g_6[1],&g_6[2],&g_59[3]}},{{&g_59[2],&g_64,&g_59[0],&g_59[3],&g_52},{&g_59[0],&g_52,&g_59[3],&g_59[3],&g_52},{&g_52,(void*)0,(void*)0,&g_6[1],&g_59[3]},{&g_6[0],&g_6[1],&g_64,&g_52,(void*)0},{(void*)0,&g_59[2],(void*)0,&g_59[3],&g_64},{&g_6[0],&g_52,(void*)0,&g_6[0],&g_59[0]},{&g_52,&g_59[3],&g_64,&g_59[2],&g_59[2]},{&g_59[0],&g_59[3],&g_59[0],&g_52,&g_52},{(void*)0,(void*)0,&g_52,&g_59[0],&g_59[2]}},{{&g_59[0],(void*)0,&g_59[2],(void*)0,&g_59[3]},{&g_6[1],&g_59[0],&g_52,&g_59[2],&g_59[0]},{&g_59[0],&g_64,&g_6[1],&g_6[1],&g_59[2]},{(void*)0,&g_6[1],&g_64,&g_6[1],(void*)0},{&g_59[0],&g_59[0],&g_59[0],&g_59[2],&g_6[1]},{(void*)0,(void*)0,&g_64,(void*)0,&g_52},{&g_6[1],&g_52,&g_64,&g_59[0],&g_6[1]},{&g_6[1],(void*)0,&g_59[0],&g_52,(void*)0},{&g_6[1],(void*)0,&g_59[2],(void*)0,&g_59[2]}},{{&g_6[1],&g_6[1],&g_59[2],&g_52,&g_59[0]},{&g_6[1],&g_59[3],&g_6[0],&g_59[2],&g_59[3]},{&g_6[1],&g_64,&g_6[1],&g_59[3],&g_59[2]},{(void*)0,&g_59[3],&g_64,&g_59[0],&g_52},{&g_59[0],&g_6[1],(void*)0,&g_59[2],(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_52},{&g_59[0],(void*)0,&g_64,&g_6[1],&g_59[0]},{&g_6[1],&g_52,&g_6[1],&g_52,(void*)0},{&g_59[0],(void*)0,&g_6[0],&g_6[1],&g_59[2]}},{{(void*)0,&g_59[0],&g_59[2],(void*)0,&g_6[1]},{&g_6[1],&g_6[1],&g_59[2],&g_59[2],&g_6[1]},{&g_59[3],&g_64,&g_59[0],&g_59[0],&g_59[2]},{&g_52,&g_59[0],&g_64,&g_59[3],(void*)0},{&g_59[0],(void*)0,&g_64,&g_59[2],&g_59[0]},{&g_52,(void*)0,&g_59[0],&g_52,&g_52},{&g_59[3],(void*)0,&g_64,(void*)0,(void*)0},{&g_6[1],(void*)0,&g_6[1],&g_52,&g_52},{(void*)0,(void*)0,&g_52,&g_59[0],&g_59[2]}}};
static int32_t * volatile * volatile g_321 = &g_245;/* VOLATILE GLOBAL g_321 */
static int16_t * const g_341 = (void*)0;
static int16_t * const *g_340 = &g_341;
static int16_t * const **g_339 = &g_340;
static float *g_354 = (void*)0;
static float **g_353 = &g_354;
static float *** volatile g_352 = &g_353;/* VOLATILE GLOBAL g_352 */
static int32_t g_377 = 0xF7B21165L;
static int32_t ** volatile g_503 = &g_57;/* VOLATILE GLOBAL g_503 */
static const int8_t g_517 = 0xD6L;
static uint16_t *g_630 = &g_2;
static uint16_t **g_629 = &g_630;
static uint16_t ***g_628 = &g_629;
static uint16_t *** volatile * volatile g_627 = &g_628;/* VOLATILE GLOBAL g_627 */
static uint32_t g_656 = 0UL;
static int32_t ** volatile g_666[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int64_t g_676 = (-2L);
static uint64_t g_692[10] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
static float g_805 = 0xA.8A5528p+19;
static uint64_t *g_809 = &g_692[4];
static uint64_t ** const g_808 = &g_809;
static uint64_t *** volatile g_810[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint64_t **g_812 = &g_809;
static uint64_t *** volatile g_811 = &g_812;/* VOLATILE GLOBAL g_811 */
static int64_t g_965 = (-1L);
static int8_t g_967 = 0x42L;
static float * volatile g_1075 = &g_229[1][1];/* VOLATILE GLOBAL g_1075 */
static int32_t ** volatile g_1077 = (void*)0;/* VOLATILE GLOBAL g_1077 */
static int32_t *g_1128 = &g_377;
static int32_t **g_1127 = &g_1128;
static const uint64_t **g_1149[1] = {(void*)0};
static const uint64_t ***g_1148 = &g_1149[0];
static uint8_t *g_1255 = &g_109;
static uint8_t **g_1254 = &g_1255;
static uint8_t *** volatile g_1253 = &g_1254;/* VOLATILE GLOBAL g_1253 */
static float ***g_1332 = &g_353;
static uint16_t ****g_1358 = &g_628;
static uint16_t *****g_1357 = &g_1358;
static int8_t g_1404 = 0L;
static int64_t g_1410 = 0xE7494E95BF962444LL;
static uint32_t g_1413 = 1UL;
static uint64_t g_1423[4][9][4] = {{{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL}},{{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL}},{{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL}},{{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL},{0xD0E940D405F84916LL,0x8D13DF61B082FECALL,0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL},{0xD0E940D405F84916LL,0xF3E25FD9F9D2CDF9LL,0xD0E940D405F84916LL,0x8D13DF61B082FECALL}}};
static volatile uint64_t g_1437 = 0xBB553E4CF7E1B65BLL;/* VOLATILE GLOBAL g_1437 */
static int8_t ****g_1505 = (void*)0;
static int8_t ***** volatile g_1504 = &g_1505;/* VOLATILE GLOBAL g_1504 */
static const int16_t * const ****g_1511 = (void*)0;
static volatile float g_1529 = 0x1.0A45C0p-51;/* VOLATILE GLOBAL g_1529 */
static uint16_t g_1604 = 1UL;
static int8_t g_1764 = 0x73L;
static int32_t g_1816 = (-1L);
static int32_t ** volatile g_1822[6] = {&g_57,&g_57,&g_57,&g_57,&g_57,&g_57};
static int32_t ***g_1848[5][4] = {{&g_1127,&g_1127,&g_1127,&g_1127},{&g_1127,&g_1127,&g_1127,&g_1127},{&g_1127,&g_1127,&g_1127,&g_1127},{&g_1127,&g_1127,&g_1127,&g_1127},{&g_1127,&g_1127,&g_1127,&g_1127}};
static uint64_t g_1855[1][2][6] = {{{0UL,0xBC80023ADBE20048LL,0UL,0xBC80023ADBE20048LL,0UL,0xBC80023ADBE20048LL},{0UL,0xBC80023ADBE20048LL,0UL,0xBC80023ADBE20048LL,0UL,0xBC80023ADBE20048LL}}};
static float g_1889 = 0xC.7289B2p-5;
static volatile int32_t g_1895[3][4] = {{(-9L),(-9L),(-9L),(-9L)},{(-9L),(-9L),(-9L),(-9L)},{(-9L),(-9L),(-9L),(-9L)}};
static int8_t ** const ** const  volatile g_1916 = (void*)0;/* VOLATILE GLOBAL g_1916 */
static int8_t *g_1920 = &g_1404;
static int8_t **g_1919 = &g_1920;
static int8_t ** const *g_1918 = &g_1919;
static int8_t ** const ** volatile g_1917 = &g_1918;/* VOLATILE GLOBAL g_1917 */
static int32_t g_1926 = 0x6441BB88L;
static float g_1951 = (-0x1.Ap+1);
static int32_t * const *g_2088 = &g_245;
static int32_t * const **g_2087 = &g_2088;
static int32_t * const ***g_2086 = &g_2087;
static int16_t g_2101 = 0x3FC3L;
static volatile int8_t g_2242[6] = {0x9BL,0xBDL,0xBDL,0x9BL,0xBDL,0xBDL};
static const int16_t g_2263[8] = {0xA034L,0xA034L,0xA034L,0xA034L,0xA034L,0xA034L,0xA034L,0xA034L};
static const int16_t g_2265 = 0x1A64L;
static float g_2267 = 0x1.Dp+1;
static int32_t ** volatile g_2268 = (void*)0;/* VOLATILE GLOBAL g_2268 */
static int32_t g_2289 = 0xBFA74735L;
static uint64_t *g_2355 = &g_1855[0][1][2];
static int32_t **g_2417 = (void*)0;
static int32_t ***g_2416 = &g_2417;
static int32_t ****g_2415 = &g_2416;
static int32_t *****g_2414 = &g_2415;
static const uint8_t * volatile ** const  volatile *g_2479 = (void*)0;
static int32_t g_2499[7][2] = {{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L}};
static uint64_t * const g_2522 = &g_1423[0][7][3];
static const uint32_t g_2582 = 0xEC015678L;
static const uint32_t g_2586 = 4294967287UL;
static const uint32_t *g_2585 = &g_2586;
static volatile float g_2595 = 0x0.Ap-1;/* VOLATILE GLOBAL g_2595 */
static uint32_t g_2735 = 0UL;
static int16_t *g_2827 = (void*)0;
static int16_t **g_2826 = &g_2827;
static int16_t ***g_2825 = &g_2826;
static int16_t **** volatile g_2824[8] = {&g_2825,&g_2825,&g_2825,&g_2825,&g_2825,&g_2825,&g_2825,&g_2825};
static int16_t **** volatile g_2828 = &g_2825;/* VOLATILE GLOBAL g_2828 */
static volatile float * volatile * volatile *g_2959 = (void*)0;
static volatile float * volatile * volatile **g_2958 = &g_2959;
static volatile float * volatile * volatile ***g_2957 = &g_2958;
static const int32_t *g_3078 = &g_6[0];
static const int32_t ** volatile g_3077[10] = {&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078};
static int32_t g_3176 = 0xBCD18B31L;
static const int32_t ** volatile g_3242 = &g_3078;/* VOLATILE GLOBAL g_3242 */


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static int32_t  func_7(const int32_t * p_8, uint64_t  p_9, const float  p_10, const int32_t  p_11);
static int32_t * func_12(int32_t * p_13, uint16_t  p_14, int32_t * p_15, int32_t * p_16);
static int32_t * func_17(int32_t * p_18);
static int32_t * func_19(uint32_t  p_20, int32_t * p_21, uint32_t  p_22, int32_t * p_23, uint8_t  p_24);
static int32_t * func_25(int32_t  p_26);
static int32_t * func_27(int64_t  p_28, float  p_29, int32_t  p_30);
static float  func_33(const int64_t  p_34, uint32_t  p_35, int32_t * const  p_36, const uint32_t  p_37, int64_t  p_38);
static int64_t  func_42(int32_t  p_43);
static int32_t  func_44(int32_t * p_45, int32_t * p_46);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_6 g_52 g_59 g_64 g_58 g_57 g_51 g_109 g_230 g_231 g_245 g_200 g_234 g_235 g_236 g_134 g_321 g_196 g_142 g_138 g_139 g_140 g_141 g_630 g_377 g_629 g_676 g_204 g_656 g_692 g_808 g_811 g_517 g_628 g_809 g_320 g_503 g_63 g_1254 g_1255 g_1357 g_627 g_353 g_1332 g_1413 g_1423 g_122 g_1816 g_812 g_1604 g_1358 g_1253 g_1855 g_1917 g_1404 g_352 g_1926 g_967 g_1919 g_1920 g_1127 g_1128 g_1918 g_2086 g_2101 g_2828 g_1504 g_1505 g_2087 g_2088 g_2355 g_1075 g_229 g_2585 g_2586 g_2957 g_2735 g_1764 g_2499
 * writes: g_2 g_6 g_52 g_59 g_64 g_108 g_109 g_245 g_231 g_58 g_320 g_676 g_692 g_134 g_200 g_812 g_805 g_229 g_217 g_204 g_1332 g_656 g_1127 g_353 g_63 g_1404 g_1413 g_1423 g_122 g_1604 g_1848 g_141 g_666 g_1918 g_1926 g_377 g_628 g_57 g_2086 g_196 g_2825 g_630 g_1764 g_2499
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_5 = &g_6[1];
    int32_t l_1411 = 1L;
    uint32_t *l_1412 = &g_1413;
    uint64_t *l_1422[4][3];
    int32_t l_1424 = 8L;
    int32_t **l_2100 = &g_245;
    const int64_t l_2983 = 0xD2EB6356CCCB29BDLL;
    const float l_2984 = 0x9.1AC7B3p+97;
    int32_t *l_3306[3][6] = {{&g_2499[4][0],(void*)0,&g_2499[4][0],(void*)0,&g_2499[4][0],(void*)0},{&g_2499[4][0],(void*)0,&g_2499[4][0],(void*)0,&g_2499[4][0],(void*)0},{&g_2499[4][0],(void*)0,&g_2499[4][0],(void*)0,&g_2499[4][0],(void*)0}};
    int64_t l_3307[3];
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
            l_1422[i][j] = &g_1423[0][7][3];
    }
    for (i = 0; i < 3; i++)
        l_3307[i] = 0xEACFE029D6C44439LL;
    ++g_2;
    (*l_5) |= 0x15252CB1L;
    g_2499[1][0] ^= func_7(((l_5 == (void*)0) , func_12(((*l_2100) = func_17(func_19(g_6[2], func_25(g_6[1]), (--(*l_1412)), (((safe_mul_func_uint8_t_u_u(g_517, ((safe_rshift_func_int16_t_s_u((*l_5), 1)) == ((safe_mul_func_uint8_t_u_u(((--g_1423[3][1][1]) < ((0UL >= ((*l_5) > (*l_5))) <= (*l_5))), (-8L))) & (*l_5))))) & 1UL) , (void*)0), g_122))), g_2101, &l_1424, g_1128)), (*g_2355), l_2983, g_2735);
    return l_3307[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_59 g_1255 g_109 g_1254 g_1920 g_1253 g_812 g_809 g_692 g_2087 g_2088 g_245 g_630 g_2 g_1128 g_377 g_1604 g_628 g_629 g_1764 g_230
 * writes: g_1404 g_109 g_59 g_1604 g_1764 g_377 g_231
 */
static int32_t  func_7(const int32_t * p_8, uint64_t  p_9, const float  p_10, const int32_t  p_11)
{ /* block id: 1286 */
    int32_t ** const **l_2996 = (void*)0;
    int32_t ** const ***l_2995 = &l_2996;
    uint64_t l_3003 = 0x94BF49D698AEF2F9LL;
    int32_t l_3004 = 6L;
    float ****l_3006 = (void*)0;
    float *****l_3005 = &l_3006;
    const int16_t ****l_3011 = (void*)0;
    const uint8_t **l_3014 = (void*)0;
    const uint8_t ***l_3013 = &l_3014;
    const uint8_t ****l_3012 = &l_3013;
    uint8_t l_3030 = 0x40L;
    float l_3031 = 0x0.4p-1;
    int32_t l_3033 = 0x097A1D36L;
    const int8_t * const ** const **l_3041 = (void*)0;
    int32_t l_3053 = 1L;
    int32_t l_3056 = 1L;
    int32_t l_3066 = 5L;
    int16_t l_3090 = 1L;
    int32_t l_3095[3][9] = {{0x4C7E5654L,0xC4078F08L,0x4C7E5654L,0x65106F46L,0x87E6ECF7L,0x65106F46L,0x4C7E5654L,0xC4078F08L,0x4C7E5654L},{9L,0xADD8E8E2L,9L,9L,0xADD8E8E2L,9L,9L,0xADD8E8E2L,9L},{0x4C7E5654L,0xC4078F08L,0x4C7E5654L,0x65106F46L,0x87E6ECF7L,0x65106F46L,0x4C7E5654L,0xC4078F08L,0x4C7E5654L}};
    int16_t l_3100[3];
    float l_3103 = 0x3.EDEFB5p-41;
    int64_t l_3105[8][10][1] = {{{0xD4E9BF80914F5AEFLL},{0xE5CF633356115D85LL},{0x7A4DDACAA398CBF1LL},{0xD49F481947DF420BLL},{0xC40F11099A938DC6LL},{0xE5CF633356115D85LL},{0x3A9AE3B414C5F00BLL},{0x726B7900A20FAC97LL},{0x3A9AE3B414C5F00BLL},{0xE5CF633356115D85LL}},{{0xC40F11099A938DC6LL},{0xD49F481947DF420BLL},{0x7A4DDACAA398CBF1LL},{0xE5CF633356115D85LL},{0xD4E9BF80914F5AEFLL},{0x726B7900A20FAC97LL},{0xD4E9BF80914F5AEFLL},{0xE5CF633356115D85LL},{0x7A4DDACAA398CBF1LL},{0xD49F481947DF420BLL}},{{0xC40F11099A938DC6LL},{0xE5CF633356115D85LL},{0x3A9AE3B414C5F00BLL},{0x726B7900A20FAC97LL},{0x3A9AE3B414C5F00BLL},{0xE5CF633356115D85LL},{0xC40F11099A938DC6LL},{0xD49F481947DF420BLL},{0x7A4DDACAA398CBF1LL},{0xE5CF633356115D85LL}},{{0xD4E9BF80914F5AEFLL},{0x726B7900A20FAC97LL},{0xD4E9BF80914F5AEFLL},{0xE5CF633356115D85LL},{0x7A4DDACAA398CBF1LL},{0xD49F481947DF420BLL},{0xC40F11099A938DC6LL},{0xE5CF633356115D85LL},{0x3A9AE3B414C5F00BLL},{0x726B7900A20FAC97LL}},{{0x3A9AE3B414C5F00BLL},{0xE5CF633356115D85LL},{0xC40F11099A938DC6LL},{0xD49F481947DF420BLL},{0x7A4DDACAA398CBF1LL},{0xE5CF633356115D85LL},{0xD4E9BF80914F5AEFLL},{0x726B7900A20FAC97LL},{0xD4E9BF80914F5AEFLL},{0xE5CF633356115D85LL}},{{0x7A4DDACAA398CBF1LL},{0xD49F481947DF420BLL},{0xC40F11099A938DC6LL},{0xE5CF633356115D85LL},{0x3A9AE3B414C5F00BLL},{0x726B7900A20FAC97LL},{0x3A9AE3B414C5F00BLL},{0xE5CF633356115D85LL},{0xC40F11099A938DC6LL},{0xD49F481947DF420BLL}},{{0x7A4DDACAA398CBF1LL},{0xE5CF633356115D85LL},{0xD4E9BF80914F5AEFLL},{0x726B7900A20FAC97LL},{0xD4E9BF80914F5AEFLL},{0xE5CF633356115D85LL},{0x7A4DDACAA398CBF1LL},{0xD49F481947DF420BLL},{0xC40F11099A938DC6LL},{0xE5CF633356115D85LL}},{{0x3A9AE3B414C5F00BLL},{0x726B7900A20FAC97LL},{0x3A9AE3B414C5F00BLL},{0xE5CF633356115D85LL},{0xC40F11099A938DC6LL},{0xD49F481947DF420BLL},{0x7A4DDACAA398CBF1LL},{0xE5CF633356115D85LL},{0xD4E9BF80914F5AEFLL},{0x726B7900A20FAC97LL}}};
    int16_t l_3107 = 0x362EL;
    uint8_t l_3108 = 0x5CL;
    uint64_t l_3202[9] = {0x2C07E66584457F49LL,0x2C07E66584457F49LL,0x2C07E66584457F49LL,0x2C07E66584457F49LL,0x2C07E66584457F49LL,0x2C07E66584457F49LL,0x2C07E66584457F49LL,0x2C07E66584457F49LL,0x2C07E66584457F49LL};
    int32_t **l_3218 = &g_1128;
    uint32_t l_3287 = 1UL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_3100[i] = (-1L);
    if (((***g_2087) = (((safe_sub_func_uint8_t_u_u(((**g_1254) = ((safe_rshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s((((0xFB0953A5FADAC12DLL < (0x06L ^ (((*p_8) <= (safe_sub_func_uint32_t_u_u((18446744073709551609UL >= ((safe_rshift_func_uint16_t_u_u(((((((void*)0 != l_2995) ^ ((safe_mul_func_int8_t_s_s((safe_div_func_uint8_t_u_u((*g_1255), (**g_1254))), ((*g_1920) = ((safe_lshift_func_uint16_t_u_s(p_11, 10)) | 0x8D3CL)))) != 0x57579238L)) , &g_235) != (void*)0) < 0x7A08L), l_3003)) > (***g_1253))), 2UL))) <= (**g_1254)))) < (**g_812)) != l_3004), 0x47FBL)), 1)) & 4L)), p_11)) , l_3005) == &g_2958)))
    { /* block id: 1290 */
        int32_t l_3018 = 4L;
        uint16_t *l_3027 = &g_1604;
        uint16_t *** const *l_3032 = (void*)0;
        int32_t *l_3034[9][5][4] = {{{&g_1816,&g_2289,&g_1816,&g_1816},{&g_1816,&g_1816,&g_2289,&g_1816},{(void*)0,&g_1816,&g_377,&g_1816},{&g_2289,&g_1816,&g_2289,&g_377},{(void*)0,(void*)0,(void*)0,&g_2289}},{{&g_2289,&g_1816,&g_1816,&g_1816},{&g_2289,&g_377,(void*)0,(void*)0},{&g_2289,&g_1816,&g_377,&g_2289},{&g_1816,&g_2289,&g_2289,(void*)0},{&g_1816,&g_377,&g_377,(void*)0}},{{&g_1816,(void*)0,&g_2289,(void*)0},{&g_1816,&g_2289,&g_1816,&g_1816},{(void*)0,&g_1816,&g_1816,&g_377},{&g_377,&g_377,&g_1816,(void*)0},{&g_377,&g_2289,&g_1816,&g_1816}},{{&g_2289,&g_1816,&g_377,(void*)0},{&g_1816,&g_2289,(void*)0,&g_1816},{&g_1816,&g_2289,(void*)0,&g_377},{&g_2289,&g_377,&g_377,&g_2289},{&g_377,&g_1816,&g_1816,&g_1816}},{{&g_377,&g_2289,&g_1816,&g_2289},{&g_377,(void*)0,&g_1816,&g_2289},{&g_377,&g_2289,&g_377,&g_1816},{&g_1816,&g_1816,&g_377,&g_2289},{(void*)0,&g_377,&g_377,&g_377}},{{(void*)0,&g_2289,&g_2289,&g_1816},{&g_1816,&g_2289,&g_2289,(void*)0},{(void*)0,&g_1816,&g_2289,&g_1816},{&g_2289,&g_2289,&g_377,(void*)0},{(void*)0,&g_377,&g_1816,&g_377}},{{&g_377,&g_1816,&g_1816,&g_1816},{&g_1816,&g_2289,&g_1816,(void*)0},{&g_2289,(void*)0,&g_2289,(void*)0},{&g_2289,&g_377,(void*)0,(void*)0},{(void*)0,&g_2289,(void*)0,&g_2289}},{{&g_2289,(void*)0,&g_2289,&g_2289},{&g_2289,(void*)0,&g_1816,&g_2289},{&g_1816,&g_2289,&g_1816,&g_1816},{&g_377,&g_1816,&g_1816,&g_2289},{(void*)0,&g_1816,&g_377,&g_2289}},{{&g_2289,&g_1816,&g_2289,&g_1816},{(void*)0,(void*)0,&g_2289,&g_377},{&g_1816,&g_1816,&g_2289,(void*)0},{(void*)0,&g_2289,&g_377,&g_377},{(void*)0,(void*)0,&g_377,&g_1816}}};
        int32_t l_3052 = (-1L);
        int32_t l_3054 = 0xC26E0854L;
        int32_t l_3058 = (-4L);
        int32_t l_3059 = 0L;
        int32_t l_3060 = (-1L);
        int32_t l_3062 = 0xE7AFA81AL;
        int32_t l_3063 = 0L;
        int32_t l_3065 = 0xE5DBA4CDL;
        int32_t l_3093 = 0xD9B41E3EL;
        int32_t l_3096 = 9L;
        int32_t l_3097 = 0x09E43CD2L;
        int32_t l_3098 = 0xB19B1B5AL;
        int32_t l_3099 = (-5L);
        int32_t l_3101[7];
        uint16_t l_3174[5];
        uint32_t l_3180 = 18446744073709551615UL;
        int32_t l_3203 = 8L;
        uint16_t l_3260 = 0UL;
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_3101[i] = 0xA3795797L;
        for (i = 0; i < 5; i++)
            l_3174[i] = 0x4447L;
        (*g_245) = (((safe_mul_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(p_9, ((*g_630) <= ((void*)0 != l_3011)))), ((l_3012 != (((l_3033 = (safe_div_func_uint64_t_u_u((((((~l_3018) && (safe_mod_func_int8_t_s_s(((*g_1128) , ((safe_div_func_int16_t_s_s((safe_add_func_int16_t_s_s(0x291AL, (((safe_mod_func_uint16_t_u_u(((*l_3027)--), l_3030)) | l_3018) , 0L))), p_9)) > (***g_628))), p_11))) , l_3032) == l_3032) ^ 0x7D1CF31BL), 0x1C2312C8D5EFAAA2LL))) != p_9) , (void*)0)) && l_3018))) == 3UL) && 4L);
        for (l_3018 = 1; (l_3018 >= 0); l_3018 -= 1)
        { /* block id: 1296 */
            int32_t l_3035 = 0x68C7340AL;
            const int8_t *l_3040 = (void*)0;
            const int8_t * const *l_3039[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            const int8_t * const ** const l_3038[1][6][1] = {{{(void*)0},{&l_3039[1]},{&l_3039[1]},{(void*)0},{&l_3039[1]},{&l_3039[1]}}};
            const int8_t * const ** const *l_3037 = &l_3038[0][3][0];
            const int8_t * const ** const **l_3036[10] = {&l_3037,&l_3037,&l_3037,&l_3037,&l_3037,&l_3037,&l_3037,&l_3037,&l_3037,&l_3037};
            int32_t l_3055 = 1L;
            int64_t l_3057 = 0x4D65C08A14A3AD4DLL;
            int32_t l_3061 = 1L;
            int32_t l_3064 = 0xCD814C39L;
            uint64_t l_3067[4][2];
            int32_t l_3092 = 0L;
            int32_t l_3102[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
            float l_3106 = 0x6.3p-1;
            int64_t l_3115 = 0x27428905217F29A4LL;
            const int16_t l_3133 = 0xE411L;
            int32_t **l_3219 = &g_1128;
            int8_t *****l_3238 = &g_1505;
            int i, j, k;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 2; j++)
                    l_3067[i][j] = 1UL;
            }
            l_3041 = (((((void*)0 != l_3034[2][4][3]) < l_3035) , 0xC7L) , l_3036[0]);
            p_8 = &l_3018;
            for (g_1764 = 1; (g_1764 >= 0); g_1764 -= 1)
            { /* block id: 1302 */
                int32_t *l_3046 = &l_3033;
                int32_t *l_3047 = &g_2499[1][1];
                int32_t *l_3048 = &l_3004;
                int32_t *l_3049 = &g_52;
                int32_t *l_3050 = &l_3035;
                int32_t *l_3051[7] = {&g_6[1],&g_6[1],&g_59[0],&g_6[1],&g_6[1],&g_59[0],&g_6[1]};
                int32_t l_3087[8][7] = {{0xDA3FE59EL,0x8FF4A706L,0x7235B268L,0L,0xD80F0B4AL,0xD80F0B4AL,0L},{0x488A88AEL,0x37613B25L,0x488A88AEL,(-10L),5L,(-1L),(-8L)},{(-1L),(-9L),(-4L),0x7235B268L,0xDA3FE59EL,0x7235B268L,(-4L)},{5L,5L,0xED0314FFL,0xD9DB83B3L,0xF0B637EFL,(-1L),0x37613B25L},{0x8FF4A706L,(-1L),0xD80F0B4AL,0x846648F4L,0x846648F4L,0xD80F0B4AL,(-1L)},{1L,0x1EA9536EL,5L,0x488A88AEL,0xF0B637EFL,(-10L),0xD9DB83B3L},{0xD80F0B4AL,(-1L),0xDA3FE59EL,(-1L),0xDA3FE59EL,(-1L),0xD80F0B4AL},{0xD9DB83B3L,(-10L),0xF0B637EFL,0x488A88AEL,5L,0x1EA9536EL,1L}};
                int32_t *l_3129[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                int8_t ** const ** const  volatile *l_3130[8][4] = {{&g_1917,&g_1917,&g_1917,&g_1917},{&g_1917,&g_1917,&g_1917,&g_1917},{&g_1917,&g_1917,&g_1917,&g_1917},{&g_1917,&g_1917,&g_1917,&g_1917},{&g_1917,&g_1917,&g_1917,&g_1917},{&g_1917,&g_1917,&g_1917,&g_1917},{&g_1917,&g_1917,&g_1917,&g_1917},{&g_1917,&g_1917,&g_1917,&g_1917}};
                uint64_t ***l_3187 = &g_812;
                int32_t ****l_3216 = &g_2416;
                int64_t *l_3267 = &g_676;
                int i, j;
                ++l_3067[1][0];
            }
        }
    }
    else
    { /* block id: 1409 */
        for (g_1764 = 0; (g_1764 != (-27)); g_1764--)
        { /* block id: 1412 */
            return (*p_8);
        }
        for (g_377 = 0; (g_377 > 21); g_377++)
        { /* block id: 1417 */
            (*g_230) = 0x5.BC9C08p+12;
        }
    }
    return p_11;
}


/* ------------------------------------------ */
/* 
 * reads : g_52 g_59 g_377 g_122 g_2828 g_1358 g_628 g_629 g_630 g_58 g_627 g_2 g_235 g_236 g_134 g_1504 g_1505 g_2087 g_2088 g_245 g_234 g_1919 g_1920 g_1404 g_1127 g_1128 g_1253 g_1254 g_1255 g_109 g_2355 g_1855 g_1075 g_229 g_2585 g_2586 g_230 g_231 g_2086 g_200 g_2957 g_1357
 * writes: g_52 g_196 g_377 g_122 g_2825 g_59 g_2 g_630 g_1404 g_229 g_200
 */
static int32_t * func_12(int32_t * p_13, uint16_t  p_14, int32_t * p_15, int32_t * p_16)
{ /* block id: 906 */
    uint8_t l_2113 = 0x93L;
    int32_t l_2126 = 0x708FA136L;
    int32_t l_2128[10] = {(-1L),1L,0x8A082115L,0x8A082115L,1L,(-1L),1L,0x8A082115L,0x8A082115L,1L};
    uint16_t l_2153 = 65535UL;
    uint64_t ***l_2188 = &g_812;
    uint64_t ****l_2187 = &l_2188;
    uint32_t l_2207 = 0x73B456E7L;
    int32_t *l_2272 = &g_59[3];
    uint8_t l_2303[8][6] = {{255UL,0xD8L,1UL,0UL,1UL,1UL},{0x28L,1UL,1UL,0x28L,0xD8L,248UL},{247UL,255UL,1UL,0x28L,248UL,0x7CL},{0x28L,248UL,0x7CL,0UL,248UL,255UL},{255UL,255UL,0xD8L,247UL,0xD8L,255UL},{0x9BL,1UL,0x7CL,0xEFL,1UL,0x7CL},{0x9BL,0xD8L,1UL,247UL,0x7DL,248UL},{255UL,0xD8L,1UL,0UL,1UL,1UL}};
    float **l_2313 = (void*)0;
    uint16_t *** const l_2364 = &g_629;
    int8_t l_2396[9][10][2] = {{{(-7L),3L},{0x70L,(-7L)},{(-1L),(-2L)},{(-1L),(-7L)},{0x70L,3L},{(-7L),9L},{(-1L),7L},{(-6L),0x2FL},{0x2FL,0x30L},{1L,(-6L)}},{{(-2L),0L},{7L,0L},{(-2L),(-6L)},{1L,0x30L},{0x2FL,0x2FL},{(-6L),7L},{(-1L),9L},{(-7L),3L},{0x70L,(-7L)},{(-1L),(-2L)}},{{(-1L),(-7L)},{(-7L),0x2FL},{0x53L,0L},{0x4BL,1L},{0x30L,(-1L)},{(-1L),3L},{(-1L),0x30L},{3L,(-2L)},{1L,(-2L)},{3L,0x30L}},{{(-1L),3L},{(-1L),(-1L)},{0x30L,1L},{0x4BL,0L},{0x53L,0x2FL},{(-7L),0x53L},{7L,3L},{7L,0x53L},{(-7L),0x2FL},{0x53L,0L}},{{0x4BL,1L},{0x30L,(-1L)},{(-1L),3L},{(-1L),0x30L},{3L,(-2L)},{1L,(-2L)},{3L,0x30L},{(-1L),3L},{(-1L),(-1L)},{0x30L,1L}},{{0x4BL,0L},{0x53L,0x2FL},{(-7L),0x53L},{7L,3L},{7L,0x53L},{(-7L),0x2FL},{0x53L,0L},{0x4BL,1L},{0x30L,(-1L)},{(-1L),3L}},{{(-1L),0x30L},{3L,(-2L)},{1L,(-2L)},{3L,0x30L},{(-1L),3L},{(-1L),(-1L)},{0x30L,1L},{0x4BL,0L},{0x53L,0x2FL},{(-7L),0x53L}},{{7L,3L},{7L,0x53L},{(-7L),0x2FL},{0x53L,0L},{0x4BL,1L},{0x30L,(-1L)},{(-1L),3L},{(-1L),0x30L},{3L,(-2L)},{1L,(-2L)}},{{3L,0x30L},{(-1L),3L},{(-1L),(-1L)},{0x30L,1L},{0x4BL,0L},{0x53L,0x2FL},{(-7L),0x53L},{7L,3L},{7L,0x53L},{(-7L),0x2FL}}};
    int32_t *****l_2418 = &g_2415;
    int16_t l_2440 = 0xD14CL;
    uint16_t l_2455 = 0x3FFAL;
    uint64_t l_2480[5][10][5] = {{{18446744073709551615UL,0x53BB76C20FF91ACELL,1UL,0x82662E9B0DF80920LL,0x29475DD43497A2C0LL},{0UL,18446744073709551615UL,1UL,0x29475DD43497A2C0LL,0xB7BC0DF7CA7DD8B5LL},{0xF97F34642A39D9DBLL,0x93470783F1F96D50LL,0UL,0x82662E9B0DF80920LL,0x4CE05AA594F096DBLL},{0xC7DCC0B8755CC271LL,18446744073709551613UL,1UL,0xBD69047A4A5EB7F8LL,1UL},{0UL,18446744073709551612UL,18446744073709551615UL,18446744073709551615UL,0x82662E9B0DF80920LL},{0UL,0xBBC89B616008F7DELL,18446744073709551612UL,1UL,18446744073709551615UL},{0xB7BC0DF7CA7DD8B5LL,0x91F5200145009E98LL,18446744073709551612UL,0xC7DCC0B8755CC271LL,18446744073709551611UL},{18446744073709551611UL,0UL,18446744073709551615UL,0xE19F505F084DF159LL,18446744073709551615UL},{0UL,0UL,1UL,0xF97F34642A39D9DBLL,0x55F295617FB0CFCELL},{18446744073709551615UL,0x4CE05AA594F096DBLL,0UL,0UL,0x0FBC659382D7834FLL}},{{0xBBC89B616008F7DELL,18446744073709551615UL,1UL,0x0FBC659382D7834FLL,0x4CE05AA594F096DBLL},{0xD3B4A9A238138CDELL,0x93470783F1F96D50LL,18446744073709551615UL,0x55F295617FB0CFCELL,0UL},{0xF82D6808FDDB679ELL,0xF5DB494499F1835BLL,0x25A28C18800512C7LL,18446744073709551615UL,0x91F5200145009E98LL},{0x82662E9B0DF80920LL,0UL,0x29475DD43497A2C0LL,18446744073709551611UL,0UL},{0xBBC89B616008F7DELL,18446744073709551612UL,1UL,18446744073709551615UL,0x55551E0B93D828C6LL},{0xBBC89B616008F7DELL,0x7445B87510CFF7A9LL,0x55F295617FB0CFCELL,0x82662E9B0DF80920LL,0xF97F34642A39D9DBLL},{0x82662E9B0DF80920LL,1UL,0xD3B4A9A238138CDELL,1UL,0x82662E9B0DF80920LL},{0xF82D6808FDDB679ELL,0x29475DD43497A2C0LL,0x91F5200145009E98LL,0x4CE05AA594F096DBLL,0xF5DB494499F1835BLL},{0xD3B4A9A238138CDELL,0x0FBC659382D7834FLL,8UL,0xB7BC0DF7CA7DD8B5LL,1UL},{0x7445B87510CFF7A9LL,0UL,1UL,0x29475DD43497A2C0LL,0xF5DB494499F1835BLL}},{{0x25A28C18800512C7LL,0xB7BC0DF7CA7DD8B5LL,0xC7DCC0B8755CC271LL,0xA02F63689CEE7057LL,0x82662E9B0DF80920LL},{0xF5DB494499F1835BLL,0x25A28C18800512C7LL,18446744073709551615UL,0x91F5200145009E98LL,0xF97F34642A39D9DBLL},{18446744073709551615UL,18446744073709551615UL,0x82662E9B0DF80920LL,0x53BB76C20FF91ACELL,0x55551E0B93D828C6LL},{0x885F04D7A7BF9A3DLL,0x55F295617FB0CFCELL,18446744073709551611UL,0x53BB76C20FF91ACELL,0UL},{0UL,0x4CE05AA594F096DBLL,0x885F04D7A7BF9A3DLL,0x91F5200145009E98LL,0x91F5200145009E98LL},{18446744073709551613UL,0xBD69047A4A5EB7F8LL,18446744073709551613UL,0xA02F63689CEE7057LL,0UL},{18446744073709551609UL,18446744073709551611UL,0xE470CAB81CEA97E8LL,0x29475DD43497A2C0LL,0x4CE05AA594F096DBLL},{0UL,0xC7DCC0B8755CC271LL,0x7445B87510CFF7A9LL,0xB7BC0DF7CA7DD8B5LL,18446744073709551611UL},{18446744073709551615UL,18446744073709551615UL,0xE470CAB81CEA97E8LL,0x4CE05AA594F096DBLL,0xD3B4A9A238138CDELL},{0xA02F63689CEE7057LL,0xD3B4A9A238138CDELL,18446744073709551613UL,1UL,0UL}},{{1UL,0x16FA25234D0659DDLL,0x885F04D7A7BF9A3DLL,0x82662E9B0DF80920LL,18446744073709551615UL},{0x55551E0B93D828C6LL,0xF82D6808FDDB679ELL,18446744073709551611UL,18446744073709551615UL,0xA02F63689CEE7057LL},{0xBD69047A4A5EB7F8LL,0xF82D6808FDDB679ELL,0x82662E9B0DF80920LL,18446744073709551611UL,0UL},{1UL,0x16FA25234D0659DDLL,18446744073709551615UL,18446744073709551615UL,0x16FA25234D0659DDLL},{18446744073709551611UL,0xD3B4A9A238138CDELL,0xC7DCC0B8755CC271LL,0x55F295617FB0CFCELL,0x93470783F1F96D50LL},{0UL,18446744073709551615UL,1UL,0x0FBC659382D7834FLL,0x885F04D7A7BF9A3DLL},{0x53BB76C20FF91ACELL,0xC7DCC0B8755CC271LL,8UL,0xF5DB494499F1835BLL,0xE470CAB81CEA97E8LL},{0UL,18446744073709551611UL,0x91F5200145009E98LL,0UL,8UL},{18446744073709551611UL,0xBD69047A4A5EB7F8LL,0xD3B4A9A238138CDELL,0x55551E0B93D828C6LL,18446744073709551612UL},{1UL,0x4CE05AA594F096DBLL,0x55F295617FB0CFCELL,18446744073709551609UL,0xB7BC0DF7CA7DD8B5LL}},{{0xBD69047A4A5EB7F8LL,0x55F295617FB0CFCELL,1UL,0xE19F505F084DF159LL,0xB7BC0DF7CA7DD8B5LL},{0x55551E0B93D828C6LL,18446744073709551615UL,0x29475DD43497A2C0LL,0x25A28C18800512C7LL,18446744073709551612UL},{1UL,0x25A28C18800512C7LL,0x25A28C18800512C7LL,1UL,8UL},{0xA02F63689CEE7057LL,0xB7BC0DF7CA7DD8B5LL,18446744073709551615UL,0UL,0xE470CAB81CEA97E8LL},{18446744073709551615UL,0UL,0x16FA25234D0659DDLL,0xE470CAB81CEA97E8LL,0x885F04D7A7BF9A3DLL},{0UL,0x0FBC659382D7834FLL,0xBBC89B616008F7DELL,0UL,0x93470783F1F96D50LL},{18446744073709551609UL,0x29475DD43497A2C0LL,0xE19F505F084DF159LL,1UL,0x16FA25234D0659DDLL},{18446744073709551613UL,1UL,0UL,0x25A28C18800512C7LL,0UL},{0UL,0x7445B87510CFF7A9LL,1UL,0xE19F505F084DF159LL,8UL},{0xF82D6808FDDB679ELL,1UL,0x16FA25234D0659DDLL,0UL,0UL}}};
    int16_t l_2493 = (-1L);
    int8_t l_2501 = (-1L);
    int32_t l_2502 = 0L;
    uint32_t l_2542 = 0UL;
    uint8_t l_2695 = 0UL;
    uint8_t ***l_2737 = (void*)0;
    int32_t **l_2762 = &g_1128;
    float * const *l_2766 = &g_354;
    float * const **l_2765 = &l_2766;
    float * const ***l_2764 = &l_2765;
    uint64_t l_2815[3];
    uint32_t l_2902 = 4294967295UL;
    int16_t l_2920 = 0L;
    const int16_t *l_2947 = (void*)0;
    const int16_t **l_2946 = &l_2947;
    const int16_t ***l_2945[5][5] = {{&l_2946,&l_2946,&l_2946,(void*)0,&l_2946},{&l_2946,&l_2946,&l_2946,(void*)0,(void*)0},{(void*)0,&l_2946,(void*)0,&l_2946,&l_2946},{&l_2946,&l_2946,&l_2946,(void*)0,&l_2946},{&l_2946,&l_2946,&l_2946,&l_2946,(void*)0}};
    const int16_t ****l_2944 = &l_2945[1][3];
    const int16_t *****l_2943 = &l_2944;
    int64_t l_2968 = (-4L);
    uint64_t l_2980 = 0x3A2612E287BD513DLL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_2815[i] = 1UL;
    for (g_52 = 0; (g_52 != (-8)); --g_52)
    { /* block id: 909 */
        int64_t l_2147 = 7L;
        int32_t l_2159 = 0x28613E13L;
        int32_t l_2160 = (-1L);
        int32_t l_2161 = (-10L);
        int32_t l_2162 = 0L;
        int32_t l_2165[6] = {0x344E0248L,0x344E0248L,0x344E0248L,0x344E0248L,0x344E0248L,0x344E0248L};
        uint8_t ***l_2183[10][6] = {{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0},{&g_1254,&g_1254,(void*)0,&g_1254,&g_1254,(void*)0}};
        uint8_t *** const *l_2182[5];
        uint16_t **l_2190 = &g_630;
        uint8_t l_2201 = 0xFDL;
        int16_t *l_2222[10][5] = {{&g_122,&g_122,&g_122,&g_122,&g_122},{&g_122,(void*)0,&g_122,&g_2101,&g_2101},{&g_122,&g_122,&g_122,&g_122,&g_2101},{&g_2101,(void*)0,(void*)0,&g_2101,&g_2101},{(void*)0,&g_122,(void*)0,&g_122,&g_2101},{&g_122,&g_122,&g_122,&g_122,(void*)0},{&g_122,(void*)0,&g_122,&g_122,(void*)0},{&g_2101,&g_122,&g_122,&g_2101,(void*)0},{&g_122,&g_2101,&g_122,&g_122,(void*)0},{&g_122,&g_122,&g_122,&g_2101,&g_2101}};
        int16_t **l_2221 = &l_2222[0][2];
        int16_t ***l_2220 = &l_2221;
        const int16_t *l_2262 = &g_2263[1];
        int64_t l_2314 = 5L;
        uint64_t l_2346[6][1] = {{0UL},{1UL},{0UL},{1UL},{0UL},{1UL}};
        uint64_t *l_2356 = &g_692[1];
        uint64_t l_2394 = 1UL;
        int8_t ****l_2425 = (void*)0;
        float ****l_2436 = &g_1332;
        float *****l_2435[1][7][4] = {{{&l_2436,&l_2436,&l_2436,&l_2436},{&l_2436,&l_2436,&l_2436,&l_2436},{&l_2436,&l_2436,&l_2436,&l_2436},{&l_2436,&l_2436,&l_2436,&l_2436},{&l_2436,&l_2436,&l_2436,&l_2436},{&l_2436,&l_2436,&l_2436,&l_2436},{&l_2436,&l_2436,&l_2436,&l_2436}}};
        int8_t * const l_2447 = &g_200;
        uint16_t *****l_2473 = &g_1358;
        int32_t l_2594[1][3][1];
        uint64_t l_2642 = 0x1F0B9F5AB171C250LL;
        uint16_t l_2649 = 65535UL;
        uint32_t l_2650 = 0xD8D4885CL;
        int32_t *l_2713 = &l_2594[0][1][0];
        int64_t *l_2761 = (void*)0;
        int64_t **l_2760 = &l_2761;
        int64_t ***l_2759 = &l_2760;
        int64_t ****l_2758 = &l_2759;
        int16_t l_2763[4][5][8] = {{{1L,7L,1L,0x9AF4L,3L,0xE1F8L,0xD4CAL,2L},{(-1L),0x84CDL,0xA3C1L,0x2229L,2L,7L,7L,2L},{0xFA4CL,0x9455L,0x9455L,0xFA4CL,7L,0x2229L,0x03F3L,0x6B67L},{3L,1L,0x2229L,0xE1F8L,0x9455L,0xD4CAL,0x6B67L,0xD4CAL},{1L,1L,0x03F3L,1L,1L,0x2229L,0xFA4CL,(-1L)}},{{0x2229L,0x9455L,1L,1L,0x9AF4L,7L,1L,1L},{0x6B67L,0x84CDL,1L,1L,0x84CDL,0x6B67L,0xFA4CL,0x9AF4L},{0x9AF4L,0xD4CAL,0x03F3L,(-1L),1L,1L,0x6B67L,0x2229L},{0x03F3L,3L,0x2229L,(-1L),0x2229L,3L,0x03F3L,0x9AF4L},{0x84CDL,0x2229L,0x9455L,1L,1L,0x9AF4L,7L,1L}},{{(-1L),0xE1F8L,0xA3C1L,1L,1L,0xA3C1L,0xE1F8L,(-1L)},{0x84CDL,(-1L),3L,1L,0x2229L,0xE1F8L,0x9455L,0xD4CAL},{0x03F3L,0x6B67L,2L,0xE1F8L,1L,0xE1F8L,2L,0x6B67L},{0x9AF4L,(-1L),0xD4CAL,0xFA4CL,0x84CDL,0xA3C1L,0x2229L,2L},{0x6B67L,0xE1F8L,1L,0x2229L,0x9AF4L,0x9AF4L,0x2229L,1L}},{{0x2229L,0x2229L,0xD4CAL,0xA3C1L,1L,3L,2L,0x03F3L},{1L,3L,2L,0x03F3L,0x9455L,1L,0x9455L,0x03F3L},{3L,0xD4CAL,3L,0xA3C1L,7L,0x6B67L,0xE1F8L,1L},{0xFA4CL,0x84CDL,0xA3C1L,0x2229L,2L,7L,7L,2L},{0xFA4CL,0x9455L,0x9455L,0xFA4CL,7L,0x2229L,0x03F3L,0x6B67L}}};
        uint16_t l_2805 = 65527UL;
        uint64_t l_2812 = 0UL;
        int32_t l_2874 = 0xD65BF24FL;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_2182[i] = &l_2183[0][5];
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 3; j++)
            {
                for (k = 0; k < 1; k++)
                    l_2594[i][j][k] = 0x55635EE8L;
            }
        }
        for (g_196 = 23; (g_196 == 48); g_196 = safe_add_func_int16_t_s_s(g_196, 9))
        { /* block id: 912 */
            uint64_t ***l_2116 = &g_812;
            int32_t l_2127 = (-1L);
            int64_t *l_2132 = &g_1410;
            int64_t **l_2131 = &l_2132;
            int64_t ***l_2130[2][9][2] = {{{&l_2131,&l_2131},{&l_2131,&l_2131},{(void*)0,&l_2131},{(void*)0,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131}},{{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131},{&l_2131,&l_2131}}};
            int64_t *** const *l_2129 = &l_2130[1][0][1];
            uint32_t *l_2144 = &g_204;
            int32_t l_2163 = 0x7145CE8CL;
            int32_t l_2167 = 0x1E590B85L;
            uint16_t **l_2191 = &g_630;
            uint32_t l_2237 = 8UL;
            uint8_t *l_2287 = (void*)0;
            uint8_t l_2294 = 0x0AL;
            float l_2382 = 0x0.0p+1;
            int32_t * const **l_2391 = (void*)0;
            uint32_t l_2420 = 0x2F405960L;
            uint32_t l_2421 = 1UL;
            int32_t l_2498[9] = {(-7L),(-10L),(-10L),(-7L),0x5867C1BAL,0x5867C1BAL,(-10L),0x5867C1BAL,0x5867C1BAL};
            uint64_t l_2504[3];
            uint32_t l_2600 = 0x7F6122C4L;
            int32_t l_2604 = 1L;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2504[i] = 9UL;
        }
        for (g_377 = 3; (g_377 >= 0); g_377 -= 1)
        { /* block id: 1206 */
            uint16_t l_2820 = 0UL;
            float *l_2860 = &g_229[1][7];
            int32_t l_2868 = 0xF07C1DF6L;
            int32_t l_2899 = (-2L);
            int32_t **l_2915 = &g_1128;
            int8_t l_2916 = 1L;
            uint64_t l_2973 = 0x84D1A5969EDB4086LL;
            if ((*p_13))
                break;
            for (p_14 = 0; (p_14 <= 3); p_14 += 1)
            { /* block id: 1210 */
                int64_t l_2819 = 0L;
                int32_t *l_2867 = (void*)0;
                int32_t l_2898 = (-5L);
                int32_t l_2931 = 0x9D5BB680L;
                float *****l_2956 = (void*)0;
                uint8_t ****l_2967 = &l_2183[0][5];
                for (g_122 = 0; (g_122 <= 1); g_122 += 1)
                { /* block id: 1213 */
                    int32_t l_2818 = 0x8EFD38AEL;
                    uint64_t ***l_2851 = &g_812;
                    int i, j, k;
                    l_2820++;
                    if (l_2763[p_14][g_377][(g_122 + 2)])
                    { /* block id: 1215 */
                        int16_t ***l_2823 = &l_2221;
                        int i, j, k;
                        (*g_2828) = l_2823;
                        (*p_13) = l_2763[p_14][(g_122 + 1)][(g_377 + 3)];
                    }
                    else
                    { /* block id: 1218 */
                        const uint16_t * const l_2844 = &g_1604;
                        if (l_2819)
                            break;
                        (*p_13) = (safe_add_func_uint16_t_u_u(((*****l_2473) = 0x3A2DL), g_58[g_122]));
                        (*l_2713) ^= ((((safe_div_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((safe_add_func_int16_t_s_s(0x5FFFL, ((safe_sub_func_uint16_t_u_u(((p_14 == ((safe_div_func_uint64_t_u_u(p_14, (safe_lshift_func_int16_t_s_u((0L <= (~(((***g_1358) = (*l_2190)) != l_2844))), p_14)))) , (safe_mul_func_uint16_t_u_u((****g_627), (0x7B83316EL | 0x2D0D2D7DL))))) ^ 1L), p_14)) >= (*l_2272)))), p_14)), p_14)) | l_2818) , 0x87L) & 1L);
                        (*l_2713) = ((*p_13) = (safe_mul_func_int16_t_s_s((l_2763[p_14][g_377][(g_122 + 2)] || (((((9UL > (safe_rshift_func_int8_t_s_u(((&g_808 != ((*l_2187) = l_2851)) == p_14), 6))) & (**g_235)) & (safe_add_func_uint8_t_u_u(((void*)0 != (*g_1504)), ((safe_rshift_func_int8_t_s_s(l_2819, 3)) > 0xC185L)))) | l_2820) > 0UL)), p_14)));
                    }
                }
                (*p_15) |= (safe_lshift_func_uint16_t_u_s(((p_14 != ((void*)0 != l_2860)) != (*l_2272)), 10));
                if ((safe_div_func_uint32_t_u_u(((***g_2087) != (*p_16)), (safe_sub_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u(l_2819, 9)), (((*g_1920) = ((((l_2867 != l_2867) | (*l_2272)) & (l_2868 = (***g_234))) >= (safe_rshift_func_int8_t_s_u((**g_1919), ((safe_mod_func_int16_t_s_s(p_14, p_14)) & p_14))))) && (*g_1920)))))))
                { /* block id: 1232 */
                    int32_t **l_2873 = &l_2867;
                    (*l_2713) ^= (*p_13);
                    (*l_2873) = func_17(p_15);
                    if ((*l_2867))
                    { /* block id: 1235 */
                        (*l_2713) = p_14;
                        l_2874 |= ((*g_1127) != (void*)0);
                    }
                    else
                    { /* block id: 1238 */
                        (*l_2873) = ((safe_mul_func_int8_t_s_s(p_14, (((**g_235) >= (3UL ^ (safe_rshift_func_uint16_t_u_u((((**g_629) &= ((void*)0 == (*g_234))) | (p_14 <= (safe_mul_func_int16_t_s_s((**l_2873), ((**l_2873) || ((((p_14 < 0x9961L) , (-1L)) || 0UL) , (*l_2867))))))), p_14)))) , (***g_1253)))) , p_15);
                    }
                }
                else
                { /* block id: 1242 */
                    float l_2897 = 0x5.85538Bp+99;
                    int32_t l_2900 = 7L;
                    int32_t l_2901 = (-3L);
                    if ((*p_16))
                    { /* block id: 1243 */
                        int32_t *l_2883 = &l_2159;
                        int32_t *l_2884 = (void*)0;
                        int32_t *l_2885 = &l_2128[4];
                        int32_t l_2886 = (-7L);
                        int32_t *l_2887 = &g_2499[6][0];
                        int32_t *l_2888 = (void*)0;
                        int32_t *l_2889 = &l_2165[5];
                        int32_t *l_2890 = &l_2159;
                        int32_t *l_2891 = &l_2162;
                        int32_t *l_2892 = &l_2160;
                        int32_t *l_2893 = (void*)0;
                        int32_t *l_2894 = &l_2126;
                        int32_t *l_2895 = (void*)0;
                        int32_t *l_2896[10][3] = {{&g_1926,&g_2499[6][0],&g_2499[6][0]},{&l_2165[1],(void*)0,&l_2165[1]},{&g_1926,&g_2499[6][0],&g_2499[6][0]},{&l_2165[1],(void*)0,&l_2165[1]},{&g_1926,&g_2499[6][0],&g_2499[6][0]},{&l_2165[1],(void*)0,&l_2165[1]},{&g_1926,&g_2499[6][0],&g_2499[6][0]},{&l_2165[1],(void*)0,&l_2165[1]},{&g_1926,&g_2499[6][0],&g_2499[6][0]},{&l_2165[1],(void*)0,&l_2165[1]}};
                        int i, j;
                        ++l_2902;
                    }
                    else
                    { /* block id: 1245 */
                        int8_t l_2917 = 0xB0L;
                        uint8_t l_2932[1][7][2] = {{{0xEEL,0x50L},{0UL,0UL},{0x50L,0xEEL},{0x8AL,0xEEL},{0x50L,0UL},{0UL,0x50L},{0xEEL,0x8AL}}};
                        int i, j, k;
                        (*g_245) |= (((((safe_add_func_int32_t_s_s(((0x030FL ^ (((*l_2713) |= ((0xF18FEC0FL && (safe_add_func_uint8_t_u_u((safe_div_func_int16_t_s_s(0xBC64L, (safe_add_func_uint64_t_u_u((l_2915 == l_2762), (l_2916 , l_2917))))), ((safe_sub_func_uint32_t_u_u(l_2917, 4294967295UL)) == p_14)))) >= (*g_2355))) , 9UL)) , 0x94B2788AL), 0x2EF9B219L)) < l_2920) || l_2917) >= 0x96A5C97EL) >= 0UL);
                        (*p_15) ^= ((0x37E8L || ((safe_mul_func_uint8_t_u_u(((((****g_1358) = 0x5660L) < 0x14BBL) || ((*g_245) = (((((safe_add_func_float_f_f((((((-0x5.2p+1) <= ((l_2899 = ((safe_mul_func_int16_t_s_s((*l_2713), (3UL & ((((p_14 , (safe_lshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_u((*l_2713), p_14)), p_14))) && (*p_16)) , (-1L)) && 0xC82EA3C633B4A128LL)))) , l_2901)) >= (*g_1075))) <= l_2917) != p_14) != l_2931), l_2932[0][3][1])) , 0x0.2A6962p-60) >= 0xF.F5E756p-82) > p_14) , (-1L)))), (**g_1919))) | (*g_2585))) , 0x6F958B15L);
                        (*l_2713) |= (*p_13);
                    }
                }
                for (l_2642 = 0; (l_2642 <= 3); l_2642 += 1)
                { /* block id: 1257 */
                    int16_t l_2960[1][6][6] = {{{1L,0x4E7AL,(-1L),3L,3L,(-1L)},{3L,3L,(-1L),0x4E7AL,1L,(-1L)},{0x4E7AL,1L,(-1L),1L,0x4E7AL,(-1L)},{1L,0x4E7AL,(-1L),3L,3L,(-1L)},{3L,3L,(-1L),0x4E7AL,1L,(-1L)},{0x4E7AL,1L,(-1L),1L,0x4E7AL,(-1L)}}};
                    int32_t l_2961 = 0x4CB150C1L;
                    int i, j, k;
                    l_2961 = (!(safe_sub_func_float_f_f((((((safe_mul_func_float_f_f((*l_2272), ((safe_sub_func_float_f_f((safe_mul_func_float_f_f((!(l_2943 != ((p_14 , ((*l_2860) = p_14)) , &l_2944))), ((((*l_2447) ^= ((*g_1920) = (((0x9.7p-1 == (*g_230)) , ((((safe_rshift_func_uint16_t_u_u((****g_627), 15)) , ((safe_sub_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint16_t_u_s(1UL, (*l_2713))), l_2899)), 0x7A4FL)) , p_14)) ^ (****g_2086)) , (*l_2272))) != 0UL))) , l_2956) == g_2957))), 0x0.4p-1)) == (*l_2713)))) >= 0x1.47AE82p+75) >= 0xD.BAB8B7p-64) > l_2960[0][2][1]) == p_14), 0xF.9DAF10p+17)));
                    for (l_2207 = 0; (l_2207 <= 3); l_2207 += 1)
                    { /* block id: 1264 */
                        int32_t l_2962 = (-1L);
                        if ((****g_2086))
                            break;
                        (*p_13) = ((l_2962 != (4L | ((*****g_1357) = 1UL))) | (safe_mul_func_int16_t_s_s(l_2960[0][4][3], l_2962)));
                        (*p_15) = (*p_16);
                        if ((*p_16))
                            break;
                    }
                    if ((safe_rshift_func_int16_t_s_u(((void*)0 == l_2967), 13)))
                    { /* block id: 1271 */
                        int32_t *l_2969 = (void*)0;
                        int32_t *l_2970 = (void*)0;
                        int32_t *l_2971 = &g_2499[0][0];
                        int32_t *l_2972[4][3][8] = {{{(void*)0,&l_2899,&l_2961,&g_1926,&g_59[3],&g_2499[5][0],(void*)0,&g_59[0]},{&g_59[0],(void*)0,&g_1926,(void*)0,&g_59[3],&l_2128[5],&g_52,&g_52},{(void*)0,&l_2159,&g_59[0],&g_59[0],&l_2159,(void*)0,&l_2126,&g_59[0]}},{{(void*)0,&l_2961,&g_59[0],&g_2499[4][1],&l_2594[0][1][0],&g_6[1],&g_2499[5][0],&l_2128[8]},{&l_2899,&g_1926,&l_2126,&g_2499[4][1],&g_59[0],&g_59[0],&g_2499[6][0],&g_59[0]},{&l_2128[5],&g_59[0],&g_64,&g_59[0],&l_2128[5],(void*)0,&l_2594[0][1][0],&g_52}},{{&l_2594[0][1][0],&g_59[0],&g_6[1],(void*)0,&g_52,&l_2961,(void*)0,&g_59[0]},{&g_64,&l_2126,&g_6[1],&g_1926,&l_2868,&l_2594[0][1][0],&l_2594[0][1][0],&l_2868},{&g_52,&g_64,&g_64,&g_52,&g_2499[4][1],&l_2128[8],&g_2499[6][0],(void*)0}},{{&g_1926,&g_6[1],&l_2126,&g_64,&l_2128[8],&l_2159,&g_2499[5][0],(void*)0},{(void*)0,&g_6[1],&g_59[0],&l_2594[0][1][0],&l_2126,&l_2128[8],&l_2126,&l_2594[0][1][0]},{&g_59[0],&g_64,&g_59[0],&l_2128[5],(void*)0,&l_2594[0][1][0],&g_52,&l_2126}}};
                        int i, j, k;
                        l_2973++;
                    }
                    else
                    { /* block id: 1273 */
                        (***g_2087) = 0x69DA250BL;
                    }
                }
            }
        }
        for (l_2207 = 0; (l_2207 > 3); l_2207++)
        { /* block id: 1281 */
            int16_t l_2978 = 0L;
            int32_t *l_2979[3];
            int i;
            for (i = 0; i < 3; i++)
                l_2979[i] = &l_2128[3];
            l_2980--;
        }
    }
    return p_13;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_17(int32_t * p_18)
{ /* block id: 902 */
    int32_t *l_2091 = &g_59[0];
    int32_t *l_2092 = (void*)0;
    int32_t *l_2093 = &g_59[2];
    int32_t *l_2094 = &g_59[0];
    int32_t *l_2095 = &g_59[0];
    int32_t *l_2096[7];
    uint64_t l_2097[8];
    int i;
    for (i = 0; i < 7; i++)
        l_2096[i] = &g_52;
    for (i = 0; i < 8; i++)
        l_2097[i] = 0x817F3E41F723148ALL;
    --l_2097[0];
    return l_2091;
}


/* ------------------------------------------ */
/* 
 * reads : g_59 g_1816 g_57 g_6 g_811 g_812 g_809 g_692 g_1255 g_109 g_1604 g_52 g_64 g_1358 g_628 g_629 g_630 g_2 g_1253 g_1254 g_235 g_236 g_134 g_808 g_1855 g_204 g_1357 g_140 g_1917 g_1404 g_352 g_353 g_122 g_1926 g_627 g_967 g_1919 g_1920 g_1127 g_1128 g_503 g_656 g_1918 g_63 g_141 g_139 g_58 g_2086
 * writes: g_122 g_6 g_52 g_59 g_245 g_692 g_1604 g_134 g_1848 g_109 g_2 g_204 g_141 g_1404 g_666 g_1918 g_64 g_656 g_1926 g_805 g_377 g_628 g_57 g_2086
 */
static int32_t * func_19(uint32_t  p_20, int32_t * p_21, uint32_t  p_22, int32_t * p_23, uint8_t  p_24)
{ /* block id: 596 */
    int8_t l_1427 = (-2L);
    uint16_t **l_1430 = &g_630;
    int32_t l_1454 = 0x57BB5003L;
    int8_t *l_1457 = &l_1427;
    int8_t ** const l_1456 = &l_1457;
    uint64_t l_1469 = 18446744073709551615UL;
    int32_t *l_1516 = &g_59[3];
    uint16_t *****l_1549 = &g_1358;
    int32_t ***l_1562[3][9][3] = {{{&g_1127,&g_1127,&g_1127},{&g_1127,(void*)0,(void*)0},{(void*)0,&g_1127,&g_1127},{&g_1127,&g_1127,&g_1127},{(void*)0,&g_1127,(void*)0},{&g_1127,(void*)0,&g_1127},{(void*)0,&g_1127,(void*)0},{&g_1127,&g_1127,(void*)0},{&g_1127,&g_1127,&g_1127}},{{&g_1127,&g_1127,(void*)0},{&g_1127,(void*)0,&g_1127},{&g_1127,(void*)0,&g_1127},{(void*)0,&g_1127,&g_1127},{&g_1127,&g_1127,&g_1127},{(void*)0,&g_1127,&g_1127},{(void*)0,&g_1127,(void*)0},{&g_1127,(void*)0,&g_1127},{(void*)0,&g_1127,(void*)0}},{{&g_1127,&g_1127,(void*)0},{&g_1127,&g_1127,&g_1127},{&g_1127,&g_1127,(void*)0},{&g_1127,(void*)0,&g_1127},{&g_1127,(void*)0,&g_1127},{(void*)0,&g_1127,&g_1127},{&g_1127,&g_1127,&g_1127},{(void*)0,&g_1127,&g_1127},{(void*)0,&g_1127,(void*)0}}};
    int32_t l_1575 = 0x34166B3DL;
    int32_t l_1576[6];
    uint64_t l_1612 = 1UL;
    int32_t l_1696 = 0x652D287FL;
    uint32_t l_1791 = 18446744073709551608UL;
    int32_t ** const *l_1803 = (void*)0;
    int32_t ** const **l_1802 = &l_1803;
    int16_t *l_1804 = &g_122;
    float l_1817 = 0x8.9DD0DCp+46;
    const uint8_t l_1818 = 0UL;
    uint16_t ****l_1869 = (void*)0;
    int16_t ** const l_1914[9][4][3] = {{{&l_1804,(void*)0,&l_1804},{&l_1804,&l_1804,&l_1804},{&l_1804,(void*)0,&l_1804},{(void*)0,&l_1804,&l_1804}},{{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{(void*)0,&l_1804,&l_1804}},{{(void*)0,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804}},{{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{(void*)0,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804}},{{&l_1804,&l_1804,(void*)0},{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{(void*)0,&l_1804,(void*)0}},{{&l_1804,&l_1804,(void*)0},{&l_1804,(void*)0,(void*)0},{&l_1804,&l_1804,&l_1804},{&l_1804,(void*)0,&l_1804}},{{&l_1804,&l_1804,(void*)0},{&l_1804,&l_1804,&l_1804},{(void*)0,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804}},{{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{&l_1804,(void*)0,&l_1804}},{{&l_1804,&l_1804,&l_1804},{(void*)0,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804},{&l_1804,&l_1804,&l_1804}}};
    int16_t ** const *l_1913[6] = {&l_1914[3][2][1],&l_1914[3][2][1],&l_1914[3][2][1],&l_1914[3][2][1],&l_1914[3][2][1],&l_1914[3][2][1]};
    float l_1940 = 0xC.0E28ADp+62;
    uint8_t ***l_1946 = &g_1254;
    const float *l_1949 = &l_1940;
    const float **l_1948 = &l_1949;
    uint64_t **l_2058 = &g_809;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_1576[i] = 0L;
    for (p_22 = 0; (p_22 <= 4); p_22 += 1)
    { /* block id: 599 */
        uint64_t l_1434[3][8][2] = {{{18446744073709551615UL,18446744073709551615UL},{18446744073709551610UL,8UL},{18446744073709551612UL,9UL},{1UL,0x83749D746E267DEELL},{5UL,0x83749D746E267DEELL},{1UL,9UL},{7UL,1UL},{0UL,18446744073709551607UL}},{{0x83749D746E267DEELL,0x4D5F1CE9C6B8207FLL},{7UL,7UL},{0x4D5F1CE9C6B8207FLL,0x83749D746E267DEELL},{18446744073709551607UL,0UL},{1UL,7UL},{9UL,1UL},{0x83749D746E267DEELL,5UL},{0x83749D746E267DEELL,1UL}},{{9UL,7UL},{1UL,0UL},{18446744073709551607UL,0x83749D746E267DEELL},{0x4D5F1CE9C6B8207FLL,7UL},{7UL,0x4D5F1CE9C6B8207FLL},{0x83749D746E267DEELL,18446744073709551607UL},{0UL,1UL},{7UL,9UL}}};
        int32_t l_1444 = 0L;
        int32_t **l_1460 = &g_1128;
        const int16_t * const l_1510 = &g_122;
        const int16_t * const *l_1509[9] = {&l_1510,&l_1510,&l_1510,&l_1510,&l_1510,&l_1510,&l_1510,&l_1510,&l_1510};
        const int16_t * const **l_1508 = &l_1509[1];
        const int16_t * const ***l_1507 = &l_1508;
        const int16_t * const ****l_1506[8][3] = {{&l_1507,&l_1507,&l_1507},{&l_1507,&l_1507,&l_1507},{&l_1507,&l_1507,&l_1507},{&l_1507,&l_1507,&l_1507},{&l_1507,&l_1507,&l_1507},{&l_1507,&l_1507,&l_1507},{&l_1507,&l_1507,&l_1507},{&l_1507,&l_1507,&l_1507}};
        int64_t *l_1515[2];
        int64_t ** const l_1514 = &l_1515[0];
        uint16_t *****l_1547 = &g_1358;
        int32_t *l_1565 = &l_1444;
        uint32_t l_1567 = 18446744073709551612UL;
        uint32_t l_1582 = 18446744073709551606UL;
        uint64_t ****l_1615[1][2][1];
        uint8_t l_1616 = 0xC6L;
        int32_t l_1668 = 0x5179BBB9L;
        int32_t l_1680[4];
        int32_t l_1682[2][10] = {{0x691AC463L,0L,0xF8A3162BL,0xF8A3162BL,0L,0x691AC463L,0L,0xF8A3162BL,0xF8A3162BL,0L},{0x691AC463L,0L,0xF8A3162BL,0xF8A3162BL,0L,0x691AC463L,0L,0xF8A3162BL,0xF8A3162BL,0L}};
        int8_t **l_1740 = &l_1457;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1515[i] = &g_965;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
            {
                for (k = 0; k < 1; k++)
                    l_1615[i][j][k] = (void*)0;
            }
        }
        for (i = 0; i < 4; i++)
            l_1680[i] = 0xDFFEF7C2L;
    }
    if ((safe_sub_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u(((((safe_div_func_uint32_t_u_u((*l_1516), (((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((((void*)0 == &l_1562[2][3][2]) > (safe_div_func_int8_t_s_s(((l_1791++) , ((*l_1516) >= (safe_add_func_int16_t_s_s((safe_sub_func_uint32_t_u_u((safe_add_func_uint32_t_u_u(((safe_add_func_int16_t_s_s(((*l_1804) = ((void*)0 != l_1802)), (((safe_add_func_int32_t_s_s(0x820B8546L, ((*g_57) ^= ((safe_sub_func_int64_t_s_s((safe_sub_func_int8_t_s_s(((safe_add_func_uint8_t_u_u(((safe_div_func_uint8_t_u_u((~g_1816), ((*l_1457) = p_20))) > 0x107D3A81D91C9F38LL), l_1818)) , (-1L)), 0x92L)), 0x7AF5CA59F6FE0341LL)) == 0UL)))) , (***g_811)) != 0L))) || 0xB63EA78EF5C0018ELL), (*l_1516))), 1L)), p_20)))), (*g_1255)))), 4294967295UL)), g_1604)), 9)) < p_24) , (-1L)))) < (-4L)) , p_22) != p_22), (*g_1255))), p_24)))
    { /* block id: 772 */
        const int32_t l_1825 = 0x22D79CBDL;
        float ***l_1836 = &g_353;
        int64_t *l_1840[10] = {&g_1410,(void*)0,&g_1410,&g_1410,(void*)0,&g_1410,&g_1410,(void*)0,&g_1410,&g_1410};
        int64_t **l_1839 = &l_1840[6];
        int64_t ***l_1838 = &l_1839;
        int64_t *** const *l_1837 = &l_1838;
        int i;
        for (g_52 = (-21); (g_52 <= 7); g_52++)
        { /* block id: 775 */
            int8_t *l_1821 = &g_1764;
            int32_t **l_1823 = &g_245;
            (*l_1516) &= ((void*)0 == l_1821);
            (*l_1823) = p_21;
            if ((*p_21))
                continue;
        }
        l_1837 = ((+(l_1825 | ((safe_rshift_func_int16_t_s_u(l_1825, 8)) > ((((**g_235) = (safe_div_func_uint64_t_u_u(18446744073709551615UL, (((((safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u((((*g_809) = (p_24 & (****g_1358))) , 1L), (g_1604 ^= l_1825))), 250UL)) || (safe_rshift_func_int16_t_s_s(((l_1836 == &g_291) & l_1825), 0))) <= 0x9AL) >= (***g_1253)) , (**g_235))))) >= l_1825) < (-6L))))) , l_1837);
    }
    else
    { /* block id: 784 */
        int32_t ***l_1847 = &g_1127;
        int32_t l_1867 = (-1L);
        int32_t l_1890[10][7] = {{0x852D5DD5L,0x852D5DD5L,0x82B7E395L,0x852D5DD5L,0x852D5DD5L,0x82B7E395L,0x852D5DD5L},{(-7L),0L,0L,(-7L),0L,0L,(-7L)},{0xC27C6CBBL,0x852D5DD5L,0xC27C6CBBL,0xC27C6CBBL,0x852D5DD5L,0xC27C6CBBL,0xC27C6CBBL},{(-7L),(-7L),0x4813C416L,(-7L),(-7L),0x4813C416L,(-7L)},{0x852D5DD5L,0xC27C6CBBL,0xC27C6CBBL,0x852D5DD5L,0xC27C6CBBL,0xC27C6CBBL,0x852D5DD5L},{0L,(-7L),0L,0L,(-7L),0L,0L},{0x852D5DD5L,0x852D5DD5L,0x82B7E395L,0x852D5DD5L,0x852D5DD5L,0x82B7E395L,0x852D5DD5L},{(-7L),0L,0L,(-7L),0L,0L,(-7L)},{0xC27C6CBBL,0x852D5DD5L,0xC27C6CBBL,0xC27C6CBBL,0x852D5DD5L,0xC27C6CBBL,0xC27C6CBBL},{(-7L),(-7L),0x4813C416L,(-7L),(-7L),0x4813C416L,(-7L)}};
        int64_t l_1897[9][6][4] = {{{(-1L),0x10DC471AF007C32BLL,(-8L),0xE75D5D7A5CD70DFFLL},{(-1L),0x45CCDF24E35F623ALL,9L,0x391F03C04D5ED9C8LL},{0x10DC471AF007C32BLL,0x0A63ACCE09A886A7LL,0x391F03C04D5ED9C8LL,0x391F03C04D5ED9C8LL},{0x45CCDF24E35F623ALL,0x45CCDF24E35F623ALL,0xBAFCB2BE016B345DLL,0x391F03C04D5ED9C8LL},{(-6L),(-1L),(-8L),9L},{(-1L),0x0A63ACCE09A886A7LL,0xBAFCB2BE016B345DLL,(-8L)}},{{(-1L),0x0A63ACCE09A886A7LL,0xE75D5D7A5CD70DFFLL,9L},{0x0A63ACCE09A886A7LL,(-1L),0x564186D6770A78E2LL,0x391F03C04D5ED9C8LL},{0x0A63ACCE09A886A7LL,(-1L),0xE75D5D7A5CD70DFFLL,0xBAFCB2BE016B345DLL},{(-1L),(-6L),0xBAFCB2BE016B345DLL,0xBAFCB2BE016B345DLL},{(-1L),(-1L),(-8L),0x391F03C04D5ED9C8LL},{(-6L),(-1L),(-8L),9L}},{{(-1L),0x0A63ACCE09A886A7LL,0xBAFCB2BE016B345DLL,(-8L)},{(-1L),0x0A63ACCE09A886A7LL,0xE75D5D7A5CD70DFFLL,9L},{0x0A63ACCE09A886A7LL,(-1L),0x564186D6770A78E2LL,0x391F03C04D5ED9C8LL},{0x0A63ACCE09A886A7LL,(-1L),0xE75D5D7A5CD70DFFLL,0xBAFCB2BE016B345DLL},{(-1L),(-6L),0xBAFCB2BE016B345DLL,0xBAFCB2BE016B345DLL},{(-1L),(-1L),(-8L),0x391F03C04D5ED9C8LL}},{{(-6L),(-1L),(-8L),9L},{(-1L),0x0A63ACCE09A886A7LL,0xBAFCB2BE016B345DLL,(-8L)},{(-1L),0x0A63ACCE09A886A7LL,0xE75D5D7A5CD70DFFLL,9L},{0x0A63ACCE09A886A7LL,(-1L),0x564186D6770A78E2LL,0x391F03C04D5ED9C8LL},{0x0A63ACCE09A886A7LL,(-1L),0xE75D5D7A5CD70DFFLL,0xBAFCB2BE016B345DLL},{(-1L),(-6L),0xBAFCB2BE016B345DLL,0xBAFCB2BE016B345DLL}},{{(-1L),(-1L),(-8L),0x391F03C04D5ED9C8LL},{(-6L),(-1L),(-8L),9L},{(-1L),0x0A63ACCE09A886A7LL,0xBAFCB2BE016B345DLL,(-8L)},{(-1L),0x0A63ACCE09A886A7LL,0xE75D5D7A5CD70DFFLL,9L},{0x0A63ACCE09A886A7LL,(-1L),0x564186D6770A78E2LL,0x391F03C04D5ED9C8LL},{0x0A63ACCE09A886A7LL,(-1L),0xE75D5D7A5CD70DFFLL,0xBAFCB2BE016B345DLL}},{{(-1L),(-6L),0xBAFCB2BE016B345DLL,0xBAFCB2BE016B345DLL},{(-1L),(-1L),(-8L),0x391F03C04D5ED9C8LL},{(-6L),(-1L),(-8L),9L},{(-1L),0x0A63ACCE09A886A7LL,0xBAFCB2BE016B345DLL,(-8L)},{(-1L),0x0A63ACCE09A886A7LL,0xE75D5D7A5CD70DFFLL,9L},{0x0A63ACCE09A886A7LL,(-1L),0x564186D6770A78E2LL,0x391F03C04D5ED9C8LL}},{{0x0A63ACCE09A886A7LL,(-1L),0xE75D5D7A5CD70DFFLL,0xBAFCB2BE016B345DLL},{(-1L),(-6L),0xBAFCB2BE016B345DLL,0xBAFCB2BE016B345DLL},{(-1L),(-1L),(-8L),0x391F03C04D5ED9C8LL},{(-6L),(-1L),(-8L),9L},{(-1L),0x0A63ACCE09A886A7LL,0xBAFCB2BE016B345DLL,(-8L)},{(-1L),0x0A63ACCE09A886A7LL,0xE75D5D7A5CD70DFFLL,9L}},{{0x0A63ACCE09A886A7LL,(-1L),0x564186D6770A78E2LL,0x391F03C04D5ED9C8LL},{0x0A63ACCE09A886A7LL,(-1L),0xE75D5D7A5CD70DFFLL,0xBAFCB2BE016B345DLL},{(-1L),(-6L),0xBAFCB2BE016B345DLL,0xBAFCB2BE016B345DLL},{(-1L),(-1L),(-8L),0x391F03C04D5ED9C8LL},{(-6L),(-1L),(-8L),9L},{(-1L),0x0A63ACCE09A886A7LL,0xBAFCB2BE016B345DLL,(-8L)}},{{(-1L),0x0A63ACCE09A886A7LL,0xE75D5D7A5CD70DFFLL,9L},{0x0A63ACCE09A886A7LL,(-1L),0x564186D6770A78E2LL,0x391F03C04D5ED9C8LL},{0x0A63ACCE09A886A7LL,(-1L),0xE75D5D7A5CD70DFFLL,0xBAFCB2BE016B345DLL},{(-1L),(-6L),(-8L),(-8L)},{0x10DC471AF007C32BLL,0x10DC471AF007C32BLL,0x564186D6770A78E2LL,0xBAFCB2BE016B345DLL},{5L,0x0A63ACCE09A886A7LL,0x564186D6770A78E2LL,0xE75D5D7A5CD70DFFLL}}};
        float l_1901 = 0x7.FB8FEBp-46;
        int8_t ** const *l_1915 = &l_1456;
        uint64_t l_1927 = 0x2A5E740DA8045325LL;
        float ****l_1935 = (void*)0;
        uint32_t l_1952 = 0xF3435A01L;
        uint64_t l_1978 = 1UL;
        int32_t * const ***l_2090[10];
        int i, j, k;
        for (i = 0; i < 10; i++)
            l_2090[i] = &g_2087;
        for (p_22 = 0; (p_22 >= 20); ++p_22)
        { /* block id: 787 */
            const int32_t *l_1851 = &g_1816;
            const int32_t **l_1850[9][6][4] = {{{&l_1851,&l_1851,&l_1851,(void*)0},{&l_1851,&l_1851,&l_1851,&l_1851},{&l_1851,&l_1851,(void*)0,&l_1851},{&l_1851,&l_1851,&l_1851,(void*)0},{&l_1851,(void*)0,&l_1851,&l_1851},{&l_1851,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_1851,&l_1851,&l_1851},{(void*)0,&l_1851,&l_1851,&l_1851},{&l_1851,&l_1851,(void*)0,(void*)0},{&l_1851,&l_1851,&l_1851,&l_1851},{(void*)0,(void*)0,&l_1851,&l_1851},{(void*)0,&l_1851,(void*)0,&l_1851}},{{&l_1851,(void*)0,&l_1851,&l_1851},{&l_1851,&l_1851,&l_1851,&l_1851},{&l_1851,(void*)0,(void*)0,&l_1851},{&l_1851,&l_1851,&l_1851,(void*)0},{&l_1851,&l_1851,&l_1851,&l_1851},{&l_1851,&l_1851,(void*)0,&l_1851}},{{&l_1851,&l_1851,&l_1851,(void*)0},{&l_1851,(void*)0,&l_1851,&l_1851},{&l_1851,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1851,&l_1851,&l_1851},{(void*)0,&l_1851,&l_1851,&l_1851},{&l_1851,&l_1851,(void*)0,(void*)0}},{{&l_1851,&l_1851,&l_1851,&l_1851},{(void*)0,(void*)0,&l_1851,&l_1851},{(void*)0,&l_1851,(void*)0,&l_1851},{&l_1851,(void*)0,&l_1851,&l_1851},{&l_1851,&l_1851,&l_1851,&l_1851},{&l_1851,(void*)0,(void*)0,&l_1851}},{{&l_1851,&l_1851,&l_1851,(void*)0},{&l_1851,&l_1851,&l_1851,&l_1851},{&l_1851,&l_1851,(void*)0,&l_1851},{&l_1851,&l_1851,&l_1851,(void*)0},{&l_1851,(void*)0,&l_1851,&l_1851},{&l_1851,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_1851,&l_1851,&l_1851},{(void*)0,&l_1851,&l_1851,&l_1851},{&l_1851,&l_1851,(void*)0,(void*)0},{&l_1851,&l_1851,&l_1851,&l_1851},{(void*)0,(void*)0,&l_1851,&l_1851},{(void*)0,&l_1851,(void*)0,&l_1851}},{{&l_1851,(void*)0,&l_1851,&l_1851},{&l_1851,&l_1851,&l_1851,&l_1851},{&l_1851,(void*)0,(void*)0,&l_1851},{&l_1851,(void*)0,&l_1851,&l_1851},{(void*)0,(void*)0,&l_1851,&l_1851},{&l_1851,&l_1851,&l_1851,&l_1851}},{{&l_1851,&l_1851,(void*)0,&l_1851},{(void*)0,&l_1851,(void*)0,(void*)0},{&l_1851,&l_1851,&l_1851,&l_1851},{&l_1851,&l_1851,&l_1851,&l_1851},{(void*)0,&l_1851,(void*)0,&l_1851},{&l_1851,(void*)0,&l_1851,&l_1851}}};
            const int32_t ***l_1849 = &l_1850[1][2][0];
            const int32_t l_1871 = 0L;
            int32_t l_1876 = (-7L);
            int32_t l_1884 = 0xA2E27DC8L;
            int32_t l_1885 = 0x15B04647L;
            int32_t l_1886[5][4][8] = {{{0xD5B4E852L,0xD5B4E852L,0x02A5CC1AL,0x714FC627L,(-1L),0xAA777814L,0L,0xAA777814L},{(-1L),0x9074D5DFL,0x714FC627L,0x9074D5DFL,(-1L),0x00DF8FE5L,0xD5B4E852L,0xAA777814L},{0x9074D5DFL,(-1L),0L,0x714FC627L,0x714FC627L,0L,(-1L),0x9074D5DFL},{0x02A5CC1AL,0x00DF8FE5L,0L,(-1L),0xD5B4E852L,(-1L),0xD5B4E852L,(-1L)}},{{0x714FC627L,0x9E5F594FL,0x714FC627L,0xAA777814L,(-1L),(-1L),0L,0L},{0L,0x00DF8FE5L,0x02A5CC1AL,0x02A5CC1AL,0x00DF8FE5L,0L,(-1L),0xD5B4E852L},{0L,(-1L),0x9074D5DFL,0x00DF8FE5L,(-1L),0x00DF8FE5L,0x9074D5DFL,(-1L)},{0x714FC627L,0x9074D5DFL,(-1L),0x00DF8FE5L,0xD5B4E852L,0xAA777814L,0xAA777814L,0xD5B4E852L}},{{0x02A5CC1AL,0xD5B4E852L,0xD5B4E852L,0x02A5CC1AL,0x714FC627L,(-1L),0xAA777814L,0L},{0x9074D5DFL,0x02A5CC1AL,(-1L),0xAA777814L,(-1L),0x02A5CC1AL,0x9074D5DFL,(-1L)},{(-1L),0x02A5CC1AL,0x9074D5DFL,(-1L),(-1L),(-1L),(-1L),0x9074D5DFL},{0xD5B4E852L,0xD5B4E852L,0x02A5CC1AL,0x714FC627L,(-1L),0xAA777814L,0L,0xAA777814L}},{{(-1L),0x9074D5DFL,0x714FC627L,0x9074D5DFL,(-1L),0x00DF8FE5L,0xD5B4E852L,0xAA777814L},{0x9074D5DFL,(-1L),0L,0x714FC627L,0x714FC627L,0L,(-1L),0x9074D5DFL},{0x02A5CC1AL,0x00DF8FE5L,0L,(-1L),0xD5B4E852L,(-1L),0xD5B4E852L,(-1L)},{0x714FC627L,0x9E5F594FL,0x714FC627L,0xAA777814L,(-1L),(-1L),0L,0L}},{{0L,0x00DF8FE5L,0x02A5CC1AL,0x02A5CC1AL,0x00DF8FE5L,0L,(-1L),0xD5B4E852L},{0L,(-1L),0x9074D5DFL,0x00DF8FE5L,(-1L),0x00DF8FE5L,0x9074D5DFL,(-1L)},{0x714FC627L,0x9074D5DFL,(-1L),0x00DF8FE5L,0xD5B4E852L,0xAA777814L,0xAA777814L,0xD5B4E852L},{0x02A5CC1AL,0xD5B4E852L,0xD5B4E852L,0xD5B4E852L,0x00DF8FE5L,(-1L),0x9074D5DFL,0x714FC627L}}};
            int i, j, k;
            if ((safe_lshift_func_uint16_t_u_s((*g_630), (safe_add_func_uint64_t_u_u((**g_808), (0xEF60L <= ((g_1848[0][1] = l_1847) == l_1849)))))))
            { /* block id: 789 */
                uint32_t *l_1868 = &g_204;
                const int32_t l_1870[2] = {0x254D6C53L,0x254D6C53L};
                int32_t l_1883 = 0L;
                int32_t l_1887 = (-1L);
                int32_t l_1888 = 0x255EC5CFL;
                int32_t l_1891 = 0x47DBD025L;
                int32_t l_1892 = 0xAB35F121L;
                int32_t l_1893 = (-1L);
                int32_t l_1894 = 0x9D7F5FA4L;
                int32_t l_1896[10] = {0x7A3D3233L,0x7A3D3233L,0x7A3D3233L,0x7A3D3233L,0x7A3D3233L,0x7A3D3233L,0x7A3D3233L,0x7A3D3233L,0x7A3D3233L,0x7A3D3233L};
                uint16_t l_1898[9] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
                int i;
                if (((((!(safe_mul_func_int8_t_s_s(((*l_1457) |= g_59[4]), p_22))) != g_1855[0][1][2]) & ((safe_mod_func_uint64_t_u_u((((((((((*l_1868) ^= (safe_sub_func_int8_t_s_s((safe_add_func_int64_t_s_s(p_24, (((***g_811) || (((safe_div_func_int16_t_s_s((+((((**g_1254) |= (p_20 , 255UL)) <= ((safe_add_func_uint16_t_u_u(p_22, ((**g_629) = (l_1867 > 0x1BL)))) < l_1867)) && 1UL)), 7L)) , &l_1803) == &l_1803)) != l_1867))), (-1L)))) , p_22) , (*g_1357)) == l_1869) & p_24) | l_1870[0]) || (*l_1516)) || l_1871), 1L)) >= l_1871)) != 0xB4L))
                { /* block id: 794 */
                    (*l_1849) = (*l_1849);
                    if ((*g_57))
                        break;
                }
                else
                { /* block id: 797 */
                    int32_t *l_1872 = &l_1867;
                    int32_t *l_1873 = &l_1454;
                    int32_t *l_1874 = &l_1575;
                    int32_t l_1875 = 0L;
                    int32_t *l_1877 = &l_1575;
                    int32_t *l_1878 = (void*)0;
                    int32_t *l_1879 = &g_59[0];
                    int32_t *l_1880 = &g_6[1];
                    int32_t *l_1881 = (void*)0;
                    int32_t *l_1882[3][3][9] = {{{&l_1576[4],(void*)0,&l_1576[4],&g_59[0],&l_1867,&g_59[1],&g_64,&g_59[1],&l_1867},{&l_1575,&g_59[0],&g_59[0],&l_1575,&l_1576[4],&g_52,&g_59[0],&l_1576[5],&l_1575},{&l_1454,&g_59[0],(void*)0,&g_59[0],&l_1454,(void*)0,&l_1876,&l_1576[5],&l_1876}},{{&l_1576[5],&l_1576[4],&g_59[0],&g_59[0],&l_1576[4],&l_1576[5],&l_1576[4],&g_59[0],&g_59[0]},{(void*)0,(void*)0,&l_1867,&l_1576[5],&l_1867,(void*)0,(void*)0,&g_59[0],&l_1576[5]},{&l_1575,&l_1576[4],&g_52,&l_1576[5],&l_1576[5],&g_52,&l_1576[4],&l_1575,&l_1576[5]}},{{&g_52,&g_59[0],&l_1876,&g_59[1],&g_64,&g_59[1],&l_1876,&g_59[0],&g_52},{(void*)0,&l_1576[5],&g_59[0],&g_59[0],(void*)0,(void*)0,&g_59[0],&g_59[0],&l_1576[5]},{&l_1867,(void*)0,&l_1576[5],&l_1576[5],&g_64,&g_59[0],&g_64,&l_1576[5],&l_1576[5]}}};
                    int i, j, k;
                    l_1898[6]++;
                    (*g_140) = (void*)0;
                }
            }
            else
            { /* block id: 801 */
                const int16_t l_1910[7][2] = {{(-3L),9L},{9L,(-3L)},{9L,9L},{(-3L),9L},{9L,(-3L)},{9L,9L},{(-3L),9L}};
                int32_t **l_1912[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int i, j;
                (*g_57) = ((((p_20 , ((**l_1456) = (0xA31EBA3692EAB36ELL & ((-1L) || ((safe_rshift_func_uint16_t_u_s(p_22, 6)) , ((((safe_add_func_uint64_t_u_u((((safe_mul_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s(p_22, l_1910[0][1])), ((((((0L | (+((l_1890[6][4] <= ((void*)0 == l_1912[4])) ^ 0UL))) | p_20) || 6UL) == 0L) && 1UL) != l_1890[6][4]))) <= l_1890[6][4]) | p_24), l_1910[0][1])) , l_1890[6][4]) < 0x77F3FDBEF8FEFC33LL) & 0xABFFL)))))) , l_1913[3]) != &g_340) || 1UL);
                if ((*p_21))
                    break;
            }
            for (g_1404 = 0; g_1404 < 10; g_1404 += 1)
            {
                g_666[g_1404] = &g_57;
            }
            (*g_1917) = l_1915;
            (*p_21) = (*p_21);
        }
        (*l_1516) = (-9L);
        if (l_1890[4][6])
        { /* block id: 811 */
            int32_t *l_1921 = &l_1575;
            int32_t *l_1922 = &l_1575;
            int32_t *l_1923 = &g_59[1];
            int32_t *l_1924[5][4] = {{&l_1867,&l_1867,&l_1890[6][4],&l_1867},{&l_1867,&g_64,&g_64,&l_1867},{&g_64,&l_1867,&g_64,&g_64},{&l_1867,&l_1867,&l_1890[6][4],&l_1867},{&l_1867,&g_64,&g_64,&l_1867}};
            int16_t l_1925 = 0xF835L;
            float ** const *l_1937 = &g_353;
            float ** const **l_1936 = &l_1937;
            uint16_t *l_1943 = &g_1604;
            uint32_t *l_1947 = &g_204;
            uint32_t *l_1950 = &g_656;
            int i, j;
            ++l_1927;
            g_1926 ^= (((l_1890[8][5] = (l_1867 ^= ((*l_1950) = ((((((((safe_mul_func_int16_t_s_s((((*l_1516) = (safe_sub_func_uint32_t_u_u((~(l_1935 == l_1936)), g_1404))) > (((safe_sub_func_uint16_t_u_u((****g_1358), (((0xE9BFDFF23A3047F7LL ^ ((safe_rshift_func_uint16_t_u_s((--(*l_1943)), ((void*)0 == l_1946))) == ((*l_1947) = 4294967291UL))) || ((((*l_1921) = ((*p_21) ^= ((l_1948 != (*g_352)) != p_22))) > g_122) || l_1927)) >= 0x2A0E08D781F0879BLL))) < l_1890[6][4]) && (*p_21))), p_20)) & p_24) & (*g_1255)) , (*p_21)) && (*l_1516)) | (**g_235)) != 0UL) & p_24)))) <= l_1897[2][0][3]) , l_1952);
            return p_21;
        }
        else
        { /* block id: 823 */
            int32_t **l_1970 = &g_57;
            int32_t ***l_1969 = &l_1970;
            int32_t l_1976 = 0L;
            uint64_t l_1998 = 1UL;
            int64_t l_1999 = 0x8CD8E2AAE6EFD1C6LL;
            int64_t *l_2022 = &l_1897[6][3][3];
            int64_t ** const l_2021 = &l_2022;
            int8_t * const *l_2042 = &l_1457;
            int8_t * const **l_2041 = &l_2042;
            int16_t l_2074 = (-2L);
            int32_t * const ****l_2089[3];
            int i;
            for (i = 0; i < 3; i++)
                l_2089[i] = &g_2086;
            (*g_57) |= l_1927;
            if ((safe_div_func_uint8_t_u_u((safe_sub_func_int16_t_s_s(((*l_1804) |= (*l_1516)), (safe_add_func_int8_t_s_s((-1L), 1UL)))), 1L)))
            { /* block id: 826 */
                float *l_1959 = &g_805;
                int32_t ****l_1971 = &l_1969;
                int32_t *l_1977 = (void*)0;
                int32_t *l_1979 = &l_1575;
                int32_t *l_1980 = &g_1926;
                (*l_1959) = (-0x1.Cp+1);
                if (((*l_1980) = ((*l_1979) = (safe_rshift_func_uint16_t_u_s(((l_1978 = ((***l_1847) = ((safe_unary_minus_func_uint8_t_u((safe_rshift_func_int16_t_s_s((((****g_627)--) | (((*l_1516) = (((l_1696 ^= ((*g_57) || ((((safe_rshift_func_uint8_t_u_u(((((*l_1971) = l_1969) == (*l_1802)) & g_52), 0)) | (l_1976 = ((&g_1127 != l_1847) , ((((((*p_21) = (safe_lshift_func_int8_t_s_s(((p_22 && 0UL) == (safe_rshift_func_uint16_t_u_u(p_24, l_1897[6][4][3]))), 6))) < g_967) | l_1890[7][2]) , 0L) != (*l_1516))))) , 0x4E239224L) == (*l_1516)))) , (*g_57)) | p_20)) == (**g_1919))), l_1890[6][1])))) ^ 0x916DL))) , p_22), p_24)))))
                { /* block id: 838 */
                    int64_t l_1981 = (-5L);
                    (*l_1959) = l_1981;
                    if ((~(((safe_mul_func_int8_t_s_s(((***l_1915) &= p_22), (safe_div_func_uint16_t_u_u((~(**l_1970)), 0x023FL)))) <= ((***l_1969) & (safe_div_func_uint16_t_u_u(((*g_57) , (safe_mod_func_int8_t_s_s((((**g_235) = (((l_1867 = ((safe_div_func_int32_t_s_s(0x5040A210L, (*p_21))) && (0xF5B24401A35BE195LL != (((*l_1980) = (safe_mod_func_uint32_t_u_u((safe_sub_func_int8_t_s_s(0x12L, l_1897[7][2][3])), (**l_1970)))) >= (-1L))))) & 0xC1L) < (**g_629))) | l_1998), l_1952))), 0x33B1L)))) , 0x28ACL)))
                    { /* block id: 844 */
                        l_1999 |= ((*g_809) ^ (-1L));
                        (*l_1959) = p_22;
                        return (***l_1971);
                    }
                    else
                    { /* block id: 848 */
                        return p_23;
                    }
                }
                else
                { /* block id: 851 */
                    for (l_1575 = 0; (l_1575 <= 5); l_1575 += 1)
                    { /* block id: 854 */
                        uint16_t ***l_2000 = &g_629;
                        (**l_1549) = l_2000;
                    }
                }
            }
            else
            { /* block id: 858 */
                uint16_t l_2005 = 65528UL;
                int32_t *l_2006 = (void*)0;
                for (l_1454 = (-22); (l_1454 != 26); l_1454 = safe_add_func_int8_t_s_s(l_1454, 4))
                { /* block id: 861 */
                    float *****l_2003 = &l_1935;
                    int32_t l_2009 = 0xC306C614L;
                    (*l_2003) = &g_1332;
                    (**l_1969) = l_2006;
                    for (g_2 = 0; (g_2 <= 25); g_2 = safe_add_func_int64_t_s_s(g_2, 8))
                    { /* block id: 866 */
                        if (l_2009)
                            break;
                        return (*g_503);
                    }
                }
                for (l_1469 = (-10); (l_1469 >= 25); l_1469 = safe_add_func_int64_t_s_s(l_1469, 4))
                { /* block id: 873 */
                    int32_t * const l_2012 = (void*)0;
                    int32_t **l_2013 = &g_57;
                    uint8_t ****l_2014 = &l_1946;
                    uint32_t *l_2029 = (void*)0;
                    uint32_t *l_2030[3][4][1] = {{{&g_204},{(void*)0},{(void*)0},{&g_204}},{{(void*)0},{(void*)0},{&g_204},{(void*)0}},{{(void*)0},{&g_204},{(void*)0},{(void*)0}}};
                    uint32_t l_2085 = 18446744073709551612UL;
                    int i, j, k;
                    (*l_2013) = l_2012;
                    (*l_2014) = l_1946;
                    if (((safe_mul_func_uint8_t_u_u((((((*l_1516) , l_1890[6][4]) <= (p_20 & (0x52L != (safe_mul_func_int16_t_s_s(l_1897[6][3][3], ((safe_lshift_func_uint16_t_u_u((((l_2021 == (((safe_rshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s(((**g_1358) != ((safe_rshift_func_uint8_t_u_s(255UL, ((g_656++) , (***g_1918)))) , (***l_1549))), 0xCD78L)), 3)) , 0x97CCL) , (void*)0)) & 0xA026L) <= 0x7F15D0E5L), p_24)) & p_24)))))) ^ 0xB5C39D01L) , (***g_1253)), 0x7EL)) != p_24))
                    { /* block id: 877 */
                        int64_t ***l_2048 = (void*)0;
                        int64_t ****l_2047 = &l_2048;
                        int32_t l_2049 = 0x514C3F47L;
                        (*l_1970) = p_21;
                    }
                    else
                    { /* block id: 882 */
                        uint16_t l_2061[3][9][5] = {{{65535UL,0x020AL,0UL,65535UL,0x18A5L},{7UL,0xB465L,7UL,65530UL,65532UL},{65535UL,0UL,0UL,0x18A5L,0x18A5L},{0x0127L,0xB465L,0x0127L,65530UL,0x3BE8L},{65535UL,0x020AL,0UL,65535UL,0x18A5L},{7UL,0xB465L,7UL,65530UL,65532UL},{0xAC37L,1UL,65535UL,65535UL,65535UL},{0x1EC2L,4UL,0x1EC2L,0UL,0x0127L},{0xAC37L,0x96DEL,65535UL,0UL,65535UL}},{{0x839AL,4UL,0x839AL,0UL,7UL},{0xAC37L,1UL,65535UL,65535UL,65535UL},{0x1EC2L,4UL,0x1EC2L,0UL,0x0127L},{0xAC37L,0x96DEL,65535UL,0UL,65535UL},{0x839AL,4UL,0x839AL,0UL,7UL},{0xAC37L,1UL,65535UL,65535UL,65535UL},{0x1EC2L,4UL,0x1EC2L,0UL,0x0127L},{0xAC37L,0x96DEL,65535UL,0UL,65535UL},{0x839AL,4UL,0x839AL,0UL,7UL}},{{0xAC37L,1UL,65535UL,65535UL,65535UL},{0x1EC2L,4UL,0x1EC2L,0UL,0x0127L},{0xAC37L,0x96DEL,65535UL,0UL,65535UL},{0x839AL,4UL,0x839AL,0UL,7UL},{0xAC37L,1UL,65535UL,65535UL,65535UL},{0x1EC2L,4UL,0x1EC2L,0UL,0x0127L},{0xAC37L,0x96DEL,65535UL,0UL,65535UL},{0x839AL,4UL,0x839AL,0UL,7UL},{0xAC37L,1UL,65535UL,65535UL,65535UL}}};
                        int i, j, k;
                        (*l_1516) = ((safe_lshift_func_int8_t_s_s(((safe_div_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(((*g_1255) = (safe_rshift_func_int8_t_s_u(((void*)0 == l_2058), ((l_1897[1][1][2] & (*l_1516)) && (safe_mul_func_uint16_t_u_u(((*g_630) = l_2061[0][4][0]), (p_20 != (safe_rshift_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((safe_add_func_int8_t_s_s((((**g_808)--) , ((***g_1918) = (&g_1848[0][1] != (void*)0))), (safe_add_func_uint32_t_u_u(((safe_sub_func_uint8_t_u_u((((l_2074 ^ p_24) <= 0xA1595F25D69D59BDLL) | 0x6F81L), l_2061[0][0][0])) < g_59[0]), g_63[0][1][0])))), (**g_1254))), p_20))))))))), p_22)), p_22)) <= (*l_1516)), 5)) || 1L);
                        (**g_139) = (*g_140);
                    }
                    (*l_1516) = ((*p_21) = (safe_lshift_func_uint8_t_u_s((safe_mul_func_int16_t_s_s((l_1867 &= (p_22 != (l_1890[7][3] <= (safe_sub_func_uint32_t_u_u((0L || (((*l_2022) = 0L) == ((safe_sub_func_int32_t_s_s((((safe_mul_func_uint8_t_u_u(l_2085, ((void*)0 == p_23))) < 1L) , (((*g_1254) == (*g_1254)) >= (*p_21))), p_22)) & 0x0000L))), g_58[1]))))), 65526UL)), (**g_1919))));
                }
            }
            (*p_21) &= ((g_2086 = (l_2090[2] = g_2086)) != (void*)0);
        }
    }
    return p_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_6 g_64 g_58 g_59 g_57 g_51 g_109 g_230 g_231 g_245 g_200 g_234 g_235 g_236 g_134 g_321 g_52 g_196 g_142 g_138 g_139 g_140 g_141 g_630 g_377 g_629 g_676 g_204 g_656 g_692 g_808 g_811 g_517 g_628 g_809 g_320 g_503 g_63 g_1254 g_1255 g_1357 g_627 g_353 g_1332
 * writes: g_52 g_59 g_64 g_108 g_109 g_245 g_231 g_58 g_320 g_676 g_692 g_134 g_200 g_812 g_2 g_805 g_229 g_217 g_204 g_1332 g_656 g_1127 g_353 g_63 g_1404
 */
static int32_t * func_25(int32_t  p_26)
{ /* block id: 3 */
    uint8_t l_41 = 0xC3L;
    int32_t *l_47 = &g_6[1];
    uint32_t l_674 = 0xF5915918L;
    int64_t *l_675 = &g_676;
    int32_t **l_1078 = &g_245;
    int32_t *l_1091 = &g_377;
    uint8_t l_1093 = 255UL;
    int64_t **l_1100 = &l_675;
    const int32_t l_1171 = 0x3077EBC3L;
    int32_t l_1175[1][9][4] = {{{(-1L),3L,0xD44562B8L,0x24486B2DL},{0x35397D72L,3L,0x35397D72L,(-1L)},{3L,0x63F0D808L,(-1L),1L},{1L,0x24486B2DL,(-1L),0x63F0D808L},{0x50FC44A9L,(-1L),(-1L),0x50FC44A9L},{1L,(-1L),(-1L),0x35397D72L},{3L,(-1L),0x35397D72L,(-7L)},{0x35397D72L,(-7L),0xD44562B8L,(-7L)},{(-1L),(-1L),0x24486B2DL,0x35397D72L}}};
    int16_t l_1177 = 0x3E4FL;
    uint8_t *l_1252 = &l_1093;
    uint8_t **l_1251 = &l_1252;
    float ***l_1330 = &g_353;
    int32_t ***l_1333 = &g_1127;
    uint16_t *****l_1360 = &g_1358;
    uint64_t *** const l_1394 = &g_812;
    int32_t *l_1409 = &g_64;
    int i, j, k;
lbl_1230:
    (*l_1078) = func_27(g_2, (safe_add_func_float_f_f((func_33(((safe_add_func_uint32_t_u_u((g_6[2] , ((((l_41 , (func_42(func_44(&p_26, l_47)) < ((*l_675) = ((safe_mul_func_uint16_t_u_u((((safe_sub_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((((*g_630) || (g_377 , (**g_629))) != l_674), 0xFEL)), p_26)) && (*l_47)) & p_26), (*l_47))) < 1L)))) || 0x94L) , (*l_47)) && 0xA38B07EFL)), g_6[1])) >= 0x9E16B8F2L), g_6[0], &g_6[2], (*l_47), (*l_47)) < (*l_47)), (*l_47))), (*l_47));
    for (g_134 = 23; (g_134 <= (-13)); g_134--)
    { /* block id: 406 */
        uint8_t l_1087 = 0x3AL;
        int32_t *l_1094 = &g_377;
        int32_t l_1111[7][8][4] = {{{(-6L),0x07E08276L,(-6L),(-8L)},{0xAD3BC3EFL,0x07E08276L,0xFC159BADL,0x89FECDFEL},{0x07E08276L,(-2L),(-2L),0x07E08276L},{(-6L),0x89FECDFEL,(-2L),(-8L)},{0x07E08276L,0xAD3BC3EFL,0xFC159BADL,0xAD3BC3EFL},{0xAD3BC3EFL,(-2L),(-6L),0xAD3BC3EFL},{(-6L),0xAD3BC3EFL,(-8L),(-8L)},{0x89FECDFEL,0x89FECDFEL,0xFC159BADL,0x07E08276L}},{{0x89FECDFEL,(-2L),(-8L),0x89FECDFEL},{(-6L),0x07E08276L,(-6L),(-8L)},{0xAD3BC3EFL,0x07E08276L,0xFC159BADL,0x89FECDFEL},{0x07E08276L,(-2L),(-2L),0x07E08276L},{(-6L),0x89FECDFEL,0xFC159BADL,0xC60094DAL},{(-2L),(-8L),0x89FECDFEL,(-8L)},{(-8L),0xFC159BADL,0xA61352EDL,(-8L)},{0xA61352EDL,(-8L),0xC60094DAL,0xC60094DAL}},{{(-6L),(-6L),0x89FECDFEL,(-2L)},{(-6L),0xFC159BADL,0xC60094DAL,(-6L)},{0xA61352EDL,(-2L),0xA61352EDL,0xC60094DAL},{(-8L),(-2L),0x89FECDFEL,(-6L)},{(-2L),0xFC159BADL,0xFC159BADL,(-2L)},{0xA61352EDL,(-6L),0xFC159BADL,0xC60094DAL},{(-2L),(-8L),0x89FECDFEL,(-8L)},{(-8L),0xFC159BADL,0xA61352EDL,(-8L)}},{{0xA61352EDL,(-8L),0xC60094DAL,0xC60094DAL},{(-6L),(-6L),0x89FECDFEL,(-2L)},{(-6L),0xFC159BADL,0xC60094DAL,(-6L)},{0xA61352EDL,(-2L),0xA61352EDL,0xC60094DAL},{(-8L),(-2L),0x89FECDFEL,(-6L)},{(-2L),0xFC159BADL,0xFC159BADL,(-2L)},{0xA61352EDL,(-6L),0xFC159BADL,0xC60094DAL},{(-2L),(-8L),0x89FECDFEL,(-8L)}},{{(-8L),0xFC159BADL,0xA61352EDL,(-8L)},{0xA61352EDL,(-8L),0xC60094DAL,0xC60094DAL},{(-6L),(-6L),0x89FECDFEL,(-2L)},{(-6L),0xFC159BADL,0xC60094DAL,(-6L)},{0xA61352EDL,(-2L),0xA61352EDL,0xC60094DAL},{(-8L),(-2L),0x89FECDFEL,(-6L)},{(-2L),0xFC159BADL,0xFC159BADL,(-2L)},{0xA61352EDL,(-6L),0xFC159BADL,0xC60094DAL}},{{(-2L),(-8L),0x89FECDFEL,(-8L)},{(-8L),0xFC159BADL,0xA61352EDL,(-8L)},{0xA61352EDL,(-8L),0xC60094DAL,0xC60094DAL},{(-6L),(-6L),0x89FECDFEL,(-2L)},{(-6L),0xFC159BADL,0xC60094DAL,(-6L)},{0xA61352EDL,(-2L),0xA61352EDL,0xC60094DAL},{(-8L),(-2L),0x89FECDFEL,(-6L)},{(-2L),0xFC159BADL,0xFC159BADL,(-2L)}},{{0xA61352EDL,(-6L),0xFC159BADL,0xC60094DAL},{(-2L),(-8L),0x89FECDFEL,(-8L)},{(-8L),0xFC159BADL,0xA61352EDL,(-8L)},{0xA61352EDL,(-8L),0xC60094DAL,0xC60094DAL},{(-6L),(-6L),0x89FECDFEL,(-2L)},{(-6L),0xFC159BADL,0xC60094DAL,(-6L)},{0xA61352EDL,(-2L),0xA61352EDL,0xC60094DAL},{(-8L),(-2L),0x89FECDFEL,(-6L)}}};
        int32_t **l_1131 = &l_1091;
        int8_t *l_1139 = &g_200;
        int8_t **l_1138 = &l_1139;
        int8_t **l_1152[4];
        int8_t l_1226 = 0xA0L;
        uint16_t l_1233 = 0xAB84L;
        int64_t **l_1300 = &l_675;
        uint32_t l_1318 = 4294967295UL;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1152[i] = &l_1139;
    }
    for (g_204 = 0; (g_204 <= 4); g_204 += 1)
    { /* block id: 530 */
        int32_t l_1323 = (-5L);
        float ****l_1331[3][5][3] = {{{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330}},{{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330}},{{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330},{&l_1330,&l_1330,&l_1330}}};
        uint16_t *****l_1359 = &g_1358;
        int32_t **l_1386 = &g_1128;
        uint64_t ***l_1405 = &g_812;
        int i, j, k;
        if ((l_1323 > (((***g_628) |= (safe_rshift_func_uint8_t_u_u(((**g_1254) ^= ((((18446744073709551615UL < 0x3EF8C2663EC61CA2LL) || (((l_1323 || (((safe_mod_func_uint32_t_u_u(g_6[2], ((((((((((safe_rshift_func_uint8_t_u_s(((((g_1332 = l_1330) != l_1330) , l_1333) == (void*)0), 2)) | p_26) && 0UL) < (*l_47)) <= 255UL) && p_26) >= g_200) || (*l_47)) > 18446744073709551615UL) , p_26))) == p_26) == 0x33FCFB971EDA401ELL)) , l_1323) || l_1323)) , 65535UL) <= l_1323)), p_26))) , (***g_234))))
        { /* block id: 534 */
            uint64_t ***l_1344 = &g_812;
            uint64_t ****l_1343 = &l_1344;
            int8_t *l_1345[9][6] = {{(void*)0,(void*)0,&g_200,&g_967,&g_200,&g_967},{&g_200,(void*)0,&g_200,&g_967,&g_200,&g_200},{&g_967,&g_200,&g_200,&g_967,(void*)0,&g_967},{&g_967,&g_967,&g_200,&g_967,&g_967,&g_967},{&g_967,&g_967,&g_967,&g_967,&g_967,&g_967},{&g_200,&g_967,(void*)0,&g_967,(void*)0,&g_967},{(void*)0,&g_200,&g_967,&g_200,&g_200,&g_967},{(void*)0,(void*)0,&g_200,&g_967,&g_200,&g_967},{&g_200,(void*)0,&g_200,&g_967,&g_200,&g_200}};
            int32_t *l_1346 = &g_64;
            int32_t l_1347 = 0xCEDF91F1L;
            int16_t *l_1349 = &l_1177;
            int32_t l_1390 = 0x951FACE5L;
            int i, j;
            if ((l_1347 = ((safe_add_func_uint8_t_u_u((++(*g_1255)), (0xEBEF3235230328D5LL | ((((safe_div_func_int32_t_s_s(((*l_1346) = (9L != (safe_add_func_int8_t_s_s(((safe_unary_minus_func_int8_t_s((l_1323 = (((*l_1343) = (void*)0) == (void*)0)))) , (((((void*)0 == &g_627) || p_26) && p_26) , ((l_1323 , (*l_1251)) != (void*)0))), (-1L))))), 1L)) , (void*)0) != l_1346) != (***g_628))))) != g_63[2][5][1])))
            { /* block id: 540 */
                int32_t l_1348 = 0x3CD746CAL;
                (***g_139) &= (p_26 = l_1348);
            }
            else
            { /* block id: 543 */
                uint16_t ** const *l_1356 = &g_629;
                uint16_t ** const **l_1355 = &l_1356;
                uint16_t ** const ***l_1354 = &l_1355;
                int32_t l_1361 = 0L;
                uint32_t l_1374 = 0x2C8587EAL;
                uint32_t *l_1397[3][10] = {{&g_204,&g_656,&g_656,&g_204,&g_656,&g_656,&g_204,&g_656,&g_656,&g_204},{&g_656,&g_204,&g_656,&g_656,&g_204,&g_656,&g_656,&g_204,&g_656,&g_656},{&g_204,&g_204,&l_1374,&g_204,&g_204,&l_1374,&g_204,&g_204,&l_1374,&g_204}};
                int i, j;
                for (l_41 = 0; (l_41 <= 1); l_41 += 1)
                { /* block id: 546 */
                    uint64_t l_1373 = 0UL;
                    if (((void*)0 == l_1349))
                    { /* block id: 547 */
                        uint32_t l_1367 = 0xB306D01FL;
                        int32_t l_1368 = (-1L);
                        l_1368 &= (l_1367 = ((*l_1346) = (safe_mod_func_uint32_t_u_u(((safe_mod_func_int8_t_s_s((((l_1354 != (l_1360 = (l_1359 = g_1357))) , (((l_1361 & (safe_add_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_u(g_64, ((&g_967 == (void*)0) == p_26))) > 0x82L), (!(*l_1346))))) ^ 0x9EL) , (****g_627))) < 0x11CDL), p_26)) >= p_26), p_26))));
                    }
                    else
                    { /* block id: 553 */
                        int32_t l_1384 = 0xBA0543AFL;
                        uint32_t *l_1385 = &g_656;
                        (**g_140) &= ((*l_1346) = p_26);
                        (***g_139) ^= ((((safe_lshift_func_uint8_t_u_s(((**g_808) && ((*l_1346) |= (safe_div_func_int16_t_s_s(l_1373, (((**g_808) = l_1374) , ((****l_1355) &= p_26)))))), ((safe_sub_func_uint64_t_u_u(0xC31027224E6E6829LL, ((safe_mul_func_uint8_t_u_u((~(safe_add_func_uint16_t_u_u(l_1373, l_1323))), ((((*l_1385) = (safe_lshift_func_int16_t_s_u((&l_1177 != (g_52 , (void*)0)), l_1384))) , p_26) && 0xA8L))) && 0xCEAE71D7L))) , (*l_47)))) , (*g_809)) > 0xE1FC6B7837075E49LL) == l_1374);
                        (*l_1333) = l_1386;
                    }
                    (*l_1346) = (~((*l_1349) ^= l_1323));
                    (*g_1332) = (*l_1330);
                    for (g_64 = 1; (g_64 <= 4); g_64 += 1)
                    { /* block id: 568 */
                        uint16_t l_1391 = 0xBE03L;
                        int i, j, k;
                        g_63[(g_204 + 1)][(g_64 + 1)][l_41] |= (safe_lshift_func_int16_t_s_u(l_1390, (l_1391 & (((*l_1330) = (*g_1332)) != (void*)0))));
                    }
                }
                l_1361 = (&g_808 != ((((((safe_div_func_int8_t_s_s((((void*)0 != l_1394) , g_51), (l_1361 | ((1L >= (((*l_675) = p_26) >= p_26)) && ((**g_1254)--))))) <= (((*g_809) = (((++g_656) > (g_1404 = (((safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s((*g_630), p_26)), (*l_47))) == 0x9DL) , 0xA08AA751L))) != g_52)) , l_1323)) | (-1L)) , 0xE8L) == g_6[1]) , l_1405));
            }
            if (l_1177)
                goto lbl_1230;
        }
        else
        { /* block id: 581 */
            int32_t *l_1408 = &l_1323;
            int i;
            (***g_139) = 0xC29AB91EL;
            for (g_64 = (-25); (g_64 <= 14); ++g_64)
            { /* block id: 585 */
                return (*g_503);
            }
            if (p_26)
                break;
            l_1408 = &p_26;
        }
        if (p_26)
            continue;
    }
    return l_1409;
}


/* ------------------------------------------ */
/* 
 * reads : g_377 g_2 g_245 g_109 g_629 g_630 g_676 g_235 g_236 g_64 g_230 g_231 g_142 g_138 g_139 g_140 g_141 g_204 g_200 g_656 g_692 g_51 g_6 g_808 g_811 g_517 g_234 g_134 g_628 g_809 g_320 g_321 g_503 g_57 g_63 g_58
 * writes: g_676 g_134 g_64 g_245 g_231 g_109 g_200 g_812 g_2 g_692 g_805 g_229 g_217
 */
static int32_t * func_27(int64_t  p_28, float  p_29, int32_t  p_30)
{ /* block id: 269 */
    float l_713 = (-0x10.0p-1);
    int32_t l_714 = 0x76C1C0E4L;
    int32_t **l_717 = &g_245;
    int32_t **l_719 = &g_57;
    int32_t ***l_718 = &l_719;
    uint8_t * const l_720 = (void*)0;
    uint8_t *l_721 = (void*)0;
    int32_t l_722 = 0L;
    int32_t l_727 = 6L;
    int32_t l_728 = 0xE3084940L;
    int32_t l_729 = 0L;
    int32_t l_730[10][1] = {{0xCB239EE5L},{0xCB239EE5L},{1L},{0x9ABAAE84L},{1L},{0xCB239EE5L},{0xCB239EE5L},{1L},{0x9ABAAE84L},{1L}};
    float ***l_735 = &g_353;
    const uint16_t *l_738 = &g_2;
    volatile int32_t * volatile l_751[9] = {&g_58[1],&g_62,&g_58[1],&g_58[1],&g_62,&g_58[1],&g_58[1],&g_62,&g_58[1]};
    uint16_t ****l_755 = &g_628;
    int16_t *l_777 = (void*)0;
    int16_t **l_776 = &l_777;
    int16_t ***l_775 = &l_776;
    int8_t *l_802 = &g_200;
    uint32_t l_806 = 4294967286UL;
    const int8_t l_874[6] = {9L,0xDFL,9L,9L,0xDFL,9L};
    uint64_t **l_1016 = &g_809;
    int32_t *l_1076 = (void*)0;
    int i, j;
    if ((+(safe_add_func_int64_t_s_s(0x10BFD98CBB0942C2LL, (((safe_div_func_uint8_t_u_u(g_377, l_714)) < (((**l_717) = ((safe_mul_func_uint8_t_u_u(l_714, ((l_714 , &g_109) != ((g_2 < (l_717 == ((*l_718) = &g_245))) , l_720)))) >= (**l_717))) <= g_109)) & p_30)))))
    { /* block id: 272 */
        int32_t *l_723 = &l_722;
        int32_t *l_724 = (void*)0;
        int32_t *l_725 = &g_64;
        int32_t *l_726[6] = {&g_52,&g_52,&g_52,&g_52,&g_52,&g_52};
        float l_731[9][8] = {{0xC.43AA2Ep+21,0x5.064F88p+10,0xC.43AA2Ep+21,0x7.306D5Fp+65,0x1.5p-1,0x7.306D5Fp+65,0xC.43AA2Ep+21,0x5.064F88p+10},{0x1.5p-1,0x7.306D5Fp+65,0xC.43AA2Ep+21,0x5.064F88p+10,0xC.43AA2Ep+21,0x7.306D5Fp+65,0x1.5p-1,0x7.306D5Fp+65},{0x1.5p-1,0x5.064F88p+10,0x0.1p+1,0x5.064F88p+10,0x1.5p-1,0x7.0p-1,0x1.5p-1,0x5.064F88p+10},{0xC.43AA2Ep+21,0x5.064F88p+10,0xC.43AA2Ep+21,0x7.306D5Fp+65,0x1.5p-1,0x7.306D5Fp+65,0xC.43AA2Ep+21,0x5.064F88p+10},{0x1.5p-1,0x7.306D5Fp+65,0xC.43AA2Ep+21,0x5.064F88p+10,0xC.43AA2Ep+21,0x7.306D5Fp+65,0x1.5p-1,0x7.306D5Fp+65},{0x1.5p-1,0x5.064F88p+10,0x0.1p+1,0x5.064F88p+10,0x1.5p-1,0x7.0p-1,0x1.5p-1,0x7.306D5Fp+65},{0x0.1p+1,0x7.306D5Fp+65,0x0.1p+1,0x7.0p-1,0xC.43AA2Ep+21,0x7.0p-1,0x0.1p+1,0x7.306D5Fp+65},{0xC.43AA2Ep+21,0x7.0p-1,0x0.1p+1,0x7.306D5Fp+65,0x0.1p+1,0x7.0p-1,0xC.43AA2Ep+21,0x7.0p-1},{0xC.43AA2Ep+21,0x7.306D5Fp+65,0x1.5p-1,0x7.306D5Fp+65,0xC.43AA2Ep+21,0x5.064F88p+10,0xC.43AA2Ep+21,0x7.306D5Fp+65}};
        uint32_t l_732 = 0xA6963455L;
        int i, j;
        l_732++;
        if ((((void*)0 == l_735) >= (((void*)0 == &l_717) < (**g_629))))
        { /* block id: 274 */
            int32_t **l_747 = (void*)0;
            int32_t **l_748 = &l_725;
            float *l_749 = &l_731[0][5];
            float *l_750 = &g_231;
            for (g_676 = 0; (g_676 <= 5); g_676 += 1)
            { /* block id: 277 */
                uint8_t *l_739 = (void*)0;
                int i;
                l_726[g_676] = l_726[g_676];
                (**l_717) = ((((void*)0 != &l_717) | (((**g_235) = (+0x763D063B484452D8LL)) | (+((*l_725) ^= ((void*)0 == l_738))))) , p_28);
            }
            (*l_719) = &p_30;
            (*l_750) = (((***l_718) == (((((*l_749) = (((p_28 == (safe_add_func_float_f_f((safe_div_func_float_f_f(((-0x1.173FEBp+45) == ((((*g_230) != (safe_div_func_float_f_f((p_29 = (0x1.285614p-66 < (**l_717))), (&g_64 == ((*l_748) = (p_30 , &p_30)))))) == 0x6.736659p-5) == p_28)), p_28)), (-0x1.9p+1)))) != 0xE.FACAC0p+27) > 0x7.59E62Dp-78)) < p_30) == (*l_723)) > 0x0.Fp-1)) > p_28);
            l_751[6] = (****g_142);
        }
        else
        { /* block id: 289 */
            int16_t ****l_778 = &l_775;
            uint8_t *l_779[7][4][4] = {{{&g_109,&g_109,(void*)0,&g_109},{(void*)0,&g_109,(void*)0,&g_109},{(void*)0,&g_109,(void*)0,&g_109},{&g_109,&g_109,&g_109,(void*)0}},{{&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,&g_109},{&g_109,(void*)0,&g_109,&g_109},{&g_109,(void*)0,&g_109,&g_109}},{{&g_109,&g_109,(void*)0,&g_109},{&g_109,(void*)0,(void*)0,&g_109},{&g_109,(void*)0,(void*)0,&g_109},{&g_109,&g_109,&g_109,&g_109}},{{&g_109,&g_109,&g_109,(void*)0},{(void*)0,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,&g_109}},{{&g_109,&g_109,&g_109,(void*)0},{(void*)0,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,(void*)0,&g_109}},{{&g_109,&g_109,(void*)0,(void*)0},{&g_109,&g_109,(void*)0,(void*)0},{&g_109,&g_109,&g_109,(void*)0},{&g_109,&g_109,&g_109,&g_109}},{{&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,&g_109},{&g_109,&g_109,&g_109,(void*)0},{&g_109,&g_109,(void*)0,&g_109}}};
            int32_t l_780 = 0x368F9350L;
            int i, j, k;
            (*l_725) = ((+(safe_lshift_func_int8_t_s_u((g_204 >= (((void*)0 != l_755) && ((((!(safe_sub_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(((p_28 > (safe_mod_func_int32_t_s_s((p_30 || (safe_lshift_func_int16_t_s_s(4L, (safe_mod_func_uint64_t_u_u(((g_109 |= ((***l_718) |= (safe_rshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u(1UL, (safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s(p_28, ((((*l_778) = (((**g_235) = 0xAFF4843A8A0189A8LL) , l_775)) == (void*)0) | (*l_725)))), g_204)))), p_30)))) || p_30), p_28))))), p_30))) > 0x0AA1FBCCL), l_780)), g_200))) | p_30) && 0xEA8EL) < 0x1EL))), 6))) | g_656);
        }
    }
    else
    { /* block id: 296 */
        uint16_t l_793 = 0UL;
        int8_t **l_799 = (void*)0;
        int8_t **l_800 = (void*)0;
        int8_t **l_801[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int32_t l_807 = 0L;
        int i;
        (***l_718) = (((((**g_235) = ((safe_mul_func_int16_t_s_s((safe_add_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((((((safe_rshift_func_int8_t_s_s(((((l_793 = ((safe_sub_func_uint64_t_u_u((**l_717), p_30)) , 0x660C0528L)) , (~((safe_mul_func_uint8_t_u_u((((((p_28 > (((safe_div_func_uint16_t_u_u((((((((l_802 = &g_200) == (g_692[7] , l_720)) , ((*l_802) &= (((safe_sub_func_int16_t_s_s((g_51 <= (p_28 > 0x9B85AE5AB5975FD7LL)), (***l_718))) != 18446744073709551608UL) && l_793))) == g_204) > 0x4611DA44L) != g_6[2]) ^ p_30), p_28)) , l_793) < p_30)) == (***l_718)) && p_30) != 0L) <= (**l_717)), p_28)) > p_28))) , l_806) || 0xC531880FL), 0)) ^ 6UL) != 0UL) | (***l_718)) & 0UL) | p_28) <= p_30), 0x8CB2L)), p_28)), (**g_629))) == 0L)) & (-1L)) <= l_807) , l_793);
        (*g_811) = g_808;
        return (*l_719);
    }
    if ((safe_unary_minus_func_int8_t_s((safe_sub_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((((safe_unary_minus_func_int64_t_s((safe_div_func_uint32_t_u_u((0x6A92AC865821C327LL ^ ((safe_mul_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u((((safe_add_func_uint32_t_u_u((safe_lshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(0x9BL, 2)), g_377)), ((((*g_630) &= 1UL) , (void*)0) != (*l_776)))) , (safe_lshift_func_uint8_t_u_s(((((safe_div_func_uint64_t_u_u((0x637EEF8580B9F685LL ^ ((**l_719) && (((**g_235) = ((safe_mod_func_int8_t_s_s((-1L), g_51)) & (**l_717))) < p_30))), (**l_719))) == (**l_719)) ^ p_30) | p_30), g_517))) != (***l_718)), 7L)), 0x37L)) , (***g_234))), (**l_719))))) <= 0x0546L) && 0x7A3F542FB140EABBLL), (-7L))), p_30)))))
    { /* block id: 307 */
        (**l_717) = p_30;
    }
    else
    { /* block id: 309 */
        uint64_t l_851 = 0xF6814602716619DALL;
        int32_t *l_882[10] = {&l_722,(void*)0,&l_722,(void*)0,&l_722,(void*)0,&l_722,(void*)0,&l_722,(void*)0};
        int64_t **l_1030 = (void*)0;
        int i;
        for (l_729 = 0; (l_729 != 15); ++l_729)
        { /* block id: 312 */
            int16_t l_841 = 0L;
            (**l_717) &= l_841;
            if (p_28)
                break;
        }
        for (g_676 = 21; (g_676 == (-9)); g_676 = safe_sub_func_uint32_t_u_u(g_676, 2))
        { /* block id: 318 */
            uint8_t l_847 = 0x55L;
            uint16_t *l_863 = &g_2;
            uint16_t ** const l_862[2] = {&l_863,&l_863};
            uint16_t ** const *l_861 = &l_862[1];
            uint16_t ** const **l_860 = &l_861;
            uint32_t l_876[5] = {0UL,0UL,0UL,0UL,0UL};
            volatile int32_t * volatile l_883 = &g_63[4][6][1];/* VOLATILE GLOBAL l_883 */
            int32_t l_909 = 0xD48B6396L;
            int32_t l_1025[1];
            int i;
            for (i = 0; i < 1; i++)
                l_1025[i] = 0x5E6BC014L;
            for (l_714 = 0; (l_714 < 15); l_714++)
            { /* block id: 321 */
                int64_t l_846 = 0x3043C3D78C5D1CE8LL;
                int32_t l_850[9][10][2] = {{{0x131EED38L,6L},{0L,0L},{(-2L),9L},{0xAC3AE52DL,(-8L)},{0L,(-1L)},{6L,0L},{0x33F7A588L,0x36DBA307L},{0x33F7A588L,0L},{6L,(-1L)},{0L,(-8L)}},{{0xAC3AE52DL,9L},{(-2L),0L},{0L,6L},{0x131EED38L,6L},{0L,0L},{(-2L),(-1L)},{0x33F7A588L,6L},{(-2L),0x66114D08L},{0x131EED38L,(-2L)},{(-8L),0L}},{{(-8L),(-2L)},{0x131EED38L,0x66114D08L},{(-2L),6L},{0x33F7A588L,(-1L)},{9L,0x36DBA307L},{0x36DBA307L,0x131EED38L},{0L,0x131EED38L},{0x36DBA307L,0x36DBA307L},{9L,(-1L)},{0x33F7A588L,6L}},{{(-2L),0x66114D08L},{0x131EED38L,(-2L)},{(-8L),0L},{(-8L),(-2L)},{0x131EED38L,0x66114D08L},{(-2L),6L},{0x33F7A588L,(-1L)},{9L,0x36DBA307L},{0x36DBA307L,0x131EED38L},{0L,0x131EED38L}},{{0x36DBA307L,0x36DBA307L},{9L,(-1L)},{0x33F7A588L,6L},{(-2L),0x66114D08L},{0x131EED38L,(-2L)},{(-8L),0L},{(-8L),(-2L)},{0x131EED38L,0x66114D08L},{(-2L),6L},{0x33F7A588L,(-1L)}},{{9L,0x36DBA307L},{0x36DBA307L,0x131EED38L},{0L,0x131EED38L},{0x36DBA307L,0x36DBA307L},{9L,(-1L)},{0x33F7A588L,6L},{(-2L),0x66114D08L},{0x131EED38L,(-2L)},{(-8L),0L},{(-8L),(-2L)}},{{0x131EED38L,0x66114D08L},{(-2L),6L},{0x33F7A588L,(-1L)},{9L,0x36DBA307L},{0x36DBA307L,0x131EED38L},{0L,0x131EED38L},{0x36DBA307L,0x36DBA307L},{9L,(-1L)},{0x33F7A588L,6L},{(-2L),0x66114D08L}},{{0x131EED38L,(-2L)},{(-8L),0L},{(-8L),(-2L)},{0x131EED38L,0x66114D08L},{(-2L),6L},{0x33F7A588L,(-1L)},{9L,0x36DBA307L},{0x36DBA307L,0x131EED38L},{0L,0x131EED38L},{0x36DBA307L,0x36DBA307L}},{{9L,(-1L)},{0x33F7A588L,6L},{(-2L),0x66114D08L},{0x131EED38L,(-2L)},{(-8L),0L},{(-8L),(-2L)},{0x131EED38L,0x66114D08L},{(-2L),6L},{0x33F7A588L,(-1L)},{9L,0x36DBA307L}}};
                int i, j, k;
                --l_847;
                l_851++;
            }
            for (g_200 = 0; (g_200 == 16); ++g_200)
            { /* block id: 327 */
                uint16_t ** const *l_859[6] = {&g_629,&g_629,&g_629,&g_629,&g_629,&g_629};
                uint16_t ** const **l_858 = &l_859[2];
                int32_t l_875 = 0x68CB50BDL;
                int32_t l_880 = 0x4E14ADE8L;
                uint64_t l_898 = 0x5692D5D4791CC0AELL;
                uint64_t **l_1015 = &g_809;
                int i;
                if (((((1L >= (safe_sub_func_uint32_t_u_u(((l_860 = l_858) == &g_628), (safe_sub_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((!(((safe_lshift_func_int8_t_s_u(((safe_mul_func_int16_t_s_s(0x0432L, ((((*g_809) = (((((***g_628) &= (l_847 , (~0x91B8BE696245C5F8LL))) != l_874[4]) == ((**g_235) = ((l_851 <= l_875) != l_876[0]))) , p_30)) || 0xF9CE753B2C59FA5ELL) >= l_875))) < (**l_717)), 2)) & 0x8F94FF3F0B929875LL) , 1L)) != 0x7D624E95L), p_30)), l_851))))) , p_28) <= l_875) >= p_30))
                { /* block id: 332 */
                    for (l_875 = 4; (l_875 >= 0); l_875 -= 1)
                    { /* block id: 335 */
                        float *l_877 = &g_805;
                        float *l_878 = &g_229[1][1];
                        uint8_t l_879[10] = {0x31L,0x31L,0x31L,0x31L,0x31L,0x31L,0x31L,0x31L,0x31L,0x31L};
                        float *l_881 = &g_217;
                        int i, j, k;
                        (*g_321) = g_320[l_875][l_875][l_875];
                        p_29 = ((*l_881) = (((*l_878) = ((*l_877) = p_28)) != (l_880 = l_879[1])));
                        return (*g_503);
                    }
                    l_883 = (*g_140);
                }
                else
                { /* block id: 345 */
                    int32_t ****l_889 = &l_718;
                    int32_t **** const *l_888 = &l_889;
                    uint8_t *l_899 = &g_109;
                    l_730[5][0] &= ((safe_div_func_uint16_t_u_u((((safe_div_func_float_f_f(((l_888 != (((((safe_mod_func_int32_t_s_s(0x5D96AEA7L, (safe_sub_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s(((safe_sub_func_uint16_t_u_u((0xEF515E45E488CB81LL || l_898), (p_30 > (--(*l_899))))) < (!((safe_lshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_u(((safe_add_func_int16_t_s_s((l_909 |= (((**l_775) = l_863) != (void*)0)), (safe_lshift_func_int8_t_s_u((safe_mod_func_int64_t_s_s(((!(((void*)0 == &l_847) && (*g_630))) < p_28), p_30)), 7)))) ^ (****l_889)), p_28)), 8)) , 7UL))), (***l_718))), (***g_628))))) != g_64) != (*l_883)) <= 1UL) , &g_138[4])) , p_29), l_847)) , 0x22362B7EL) , 0x7BF1L), (*****l_888))) , 0xEC9371E6L);
                }
            }
        }
    }
    return l_1076;
}


/* ------------------------------------------ */
/* 
 * reads : g_234 g_235 g_236 g_134 g_245 g_139 g_140 g_141 g_59
 * writes: g_692 g_58
 */
static float  func_33(const int64_t  p_34, uint32_t  p_35, int32_t * const  p_36, const uint32_t  p_37, int64_t  p_38)
{ /* block id: 257 */
    uint8_t l_679 = 0xC7L;
    uint64_t *l_691 = &g_692[0];
    int32_t *l_693 = &g_59[0];
    int32_t *l_694 = &g_64;
    int32_t *l_695 = (void*)0;
    int32_t *l_696 = &g_64;
    int32_t l_697 = 0x404E0D54L;
    int32_t *l_698 = &g_64;
    int32_t *l_699 = &g_59[0];
    int32_t *l_700[8][9] = {{&g_59[1],&g_59[0],(void*)0,&g_59[1],&g_64,&g_64,(void*)0,(void*)0,&l_697},{(void*)0,&g_52,&g_64,&g_59[1],&g_52,&g_59[3],(void*)0,&g_59[0],&g_64},{&g_59[0],&g_6[0],&g_59[3],&g_64,&g_64,&g_59[3],&g_6[0],&g_59[3],&g_59[1]},{(void*)0,(void*)0,(void*)0,(void*)0,&g_59[0],&g_6[2],(void*)0,&g_59[3],&g_59[0]},{&l_697,&g_59[3],(void*)0,&g_52,&g_6[1],&g_59[0],&g_64,&g_6[1],&g_59[1]},{&l_697,&g_52,&g_52,&g_64,(void*)0,(void*)0,&g_59[0],&g_59[0],(void*)0},{(void*)0,&g_59[3],&g_52,&g_59[3],(void*)0,&g_59[1],&g_6[1],&g_64,&g_59[0]},{&g_59[3],(void*)0,(void*)0,&g_64,(void*)0,&g_59[0],&g_59[3],(void*)0,&g_6[2]}};
    uint16_t l_701[2][5][9] = {{{0x3193L,0x0169L,0x68CBL,4UL,0xB428L,0x54AAL,0UL,0x0169L,0xFF16L},{0xFBAEL,0x3764L,65535UL,0UL,0x1642L,0xCF60L,0xA464L,0x0169L,65535UL},{1UL,0xA464L,0x6B64L,1UL,4UL,0x285CL,0xE448L,0x1642L,65535UL},{0xFBAEL,0x1642L,0x6B64L,0UL,0UL,0x6B64L,0x1642L,0xFBAEL,1UL},{0x3193L,0UL,65535UL,0xFBAEL,4UL,65535UL,0x1642L,4UL,65530UL}},{{0UL,0x3193L,0x68CBL,0x3764L,0x1642L,0x774AL,0xE448L,0xB428L,1UL},{0xA464L,0x3193L,1UL,0x1642L,0xB428L,0xFF16L,0xA464L,0xE448L,65535UL},{0xE448L,0UL,65535UL,0x3764L,0UL,0xFF16L,0UL,0x3764L,65535UL},{0x1642L,0x1642L,0x54AAL,0xFBAEL,65535UL,0x774AL,0x3193L,0xE448L,0xFF16L},{0x1642L,0xA464L,0x774AL,0UL,0xA464L,65535UL,0xFBAEL,0xB428L,0x94A9L}}};
    int i, j, k;
    (***g_139) = (p_34 >= ((safe_div_func_uint32_t_u_u(((l_679 >= (((safe_mod_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(((l_679 && l_679) || (l_679 == ((*g_245) = ((((*l_691) = (4L | (((((&g_109 == (((safe_rshift_func_uint8_t_u_s((~(((***g_234) && (safe_mul_func_int16_t_s_s(p_37, 0UL))) <= l_679)), 3)) ^ l_679) , &g_109)) | l_679) ^ p_34) < p_35) , 0x55E8F43E16BC7677LL))) & p_35) <= 18446744073709551615UL)))), 5)), 9UL)) | g_134) && p_34)) , p_35), (-1L))) || 18446744073709551612UL));
    (*g_245) &= l_679;
    l_701[0][0][0]--;
    for (l_697 = 0; (l_697 <= (-4)); l_697--)
    { /* block id: 265 */
        return p_37;
    }
    return (*l_699);
}


/* ------------------------------------------ */
/* 
 * reads : g_51 g_245 g_109 g_64 g_200 g_234 g_235 g_236 g_134 g_59 g_230 g_321 g_52 g_6 g_196 g_2 g_142 g_138 g_139 g_140 g_141 g_58
 * writes: g_64 g_231 g_58 g_320
 */
static int64_t  func_42(int32_t  p_43)
{ /* block id: 152 */
    int32_t *l_364 = &g_52;
    int32_t *l_365 = &g_64;
    int32_t *l_366 = &g_64;
    int32_t *l_367 = (void*)0;
    int32_t *l_368[5];
    int16_t l_369[4] = {0xE332L,0xE332L,0xE332L,0xE332L};
    int32_t l_370 = 0xE000E158L;
    uint32_t l_371 = 0x4BA4E076L;
    int32_t *l_376[4];
    uint16_t l_394 = 0xC35CL;
    int64_t l_395 = 0x8C8A57AAD557342ALL;
    float ***l_398[1];
    uint64_t l_431[9][5] = {{0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL},{0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL},{0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL},{0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL},{0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL},{0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL},{0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL},{0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL,0xF4282519BD880C36LL},{0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL,0x4F4F9EC15FA3F534LL}};
    const int32_t ****l_443 = (void*)0;
    int16_t *l_496 = &l_369[3];
    int16_t **l_495 = &l_496;
    int16_t ***l_494 = &l_495;
    int16_t **** const l_493 = &l_494;
    int16_t ****l_497 = &l_494;
    int8_t *l_519 = &g_200;
    uint32_t l_567 = 4294967294UL;
    int64_t l_652 = 0x445D62980BD842CDLL;
    float l_659 = 0x6.54BF2Dp-32;
    int i, j;
    for (i = 0; i < 5; i++)
        l_368[i] = (void*)0;
    for (i = 0; i < 4; i++)
        l_376[i] = &g_377;
    for (i = 0; i < 1; i++)
        l_398[i] = &g_353;
    ++l_371;
    if (((p_43 != (safe_mod_func_uint16_t_u_u((((*l_365) = 0x6DE44C6FL) , (p_43 | (((g_51 && p_43) >= 0x7625L) == (safe_sub_func_int16_t_s_s(((*g_245) < ((safe_div_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s((safe_div_func_int32_t_s_s((((((safe_add_func_uint16_t_u_u((((safe_lshift_func_uint8_t_u_s((g_109 , (safe_lshift_func_int8_t_s_s(l_394, l_395))), 2)) > 3L) <= 0xB507F084D59CC582LL), p_43)) == 1UL) , p_43) < (*l_366)) , 6L), 0xC7C98F07L)), (-1L))), g_64)), p_43)) & g_64)), p_43))))), p_43))) < 5UL))
    { /* block id: 155 */
        float l_404 = (-0x1.Cp+1);
        float l_411 = 0x0.9p-1;
        int64_t l_412 = 0xA1822A156F8766C3LL;
        int32_t l_432 = (-7L);
        l_432 = ((safe_mul_func_float_f_f((&g_353 == l_398[0]), (safe_sub_func_float_f_f(((((safe_sub_func_float_f_f((*l_366), (!(l_404 == ((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f((l_411 == (l_412 <= ((((*g_230) = (p_43 == ((safe_mul_func_float_f_f((safe_add_func_float_f_f((safe_div_func_float_f_f(((((((safe_add_func_uint64_t_u_u(((safe_add_func_uint8_t_u_u(0x4CL, (~((((safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s((((~(safe_sub_func_uint16_t_u_u(l_412, l_412))) , g_200) | 0xC565L), 0xDB8AA756L)), p_43)) < 5UL) , 0x81DE108632886206LL) && (***g_234))))) == 0x62L), g_59[1])) < g_64) | 0x79L) , g_64) , 0x2.127C1Fp-32) != l_412), l_412)), 0x8.Ep-1)), 0xB.6DD22Ap-57)) == p_43))) == (-0x5.3p-1)) <= p_43))), p_43)), l_412)), 0xD.14B4C9p-65)) == p_43))))) < l_431[2][2]) >= 0xD.943B24p-68) < 0x3.27C208p+85), 0x1.Dp+1)))) <= p_43);
    }
    else
    { /* block id: 158 */
        int16_t l_449 = 0xCD59L;
        int32_t l_458 = 0L;
        int64_t *l_469 = &g_134;
        int64_t **l_468 = &l_469;
        int64_t ***l_467 = &l_468;
        int32_t l_478 = 0x64219A54L;
        int32_t * volatile *l_482 = &g_320[2][4][1];
        for (l_395 = 0; (l_395 < (-8)); l_395 = safe_sub_func_uint64_t_u_u(l_395, 5))
        { /* block id: 161 */
            const int32_t *l_448 = (void*)0;
            const int32_t **l_447 = &l_448;
            const int32_t ***l_446[7][3] = {{&l_447,&l_447,&l_447},{&l_447,&l_447,&l_447},{(void*)0,(void*)0,(void*)0},{&l_447,&l_447,&l_447},{&l_447,&l_447,&l_447},{(void*)0,&l_447,&l_447},{&l_447,(void*)0,&l_447}};
            const int32_t ****l_445 = &l_446[3][0];
            const int32_t *****l_444 = &l_445;
            int32_t l_459 = (-1L);
            int i, j;
            l_458 = (((void*)0 != (*g_321)) , (safe_mul_func_float_f_f((l_459 = (safe_sub_func_float_f_f((safe_add_func_float_f_f((safe_div_func_float_f_f((l_449 = ((*g_230) = (l_443 == ((*l_444) = l_443)))), (((safe_sub_func_uint64_t_u_u((~((safe_rshift_func_int8_t_s_u((safe_lshift_func_uint16_t_u_s((((p_43 < (+((0L >= g_52) <= ((void*)0 == &g_140)))) | p_43) && p_43), 7)), l_458)) >= (*g_245))), p_43)) && p_43) , p_43))), (-0x3.9p-1))), 0x2.3p-1))), g_6[1])));
        }
        for (l_458 = (-2); (l_458 >= 20); ++l_458)
        { /* block id: 170 */
            uint8_t l_479 = 0UL;
            (*****g_142) |= (~(safe_add_func_uint16_t_u_u((g_109 , 0x0419L), (safe_div_func_int8_t_s_s((((0xC7L != 5L) <= (l_467 != &l_468)) > (g_196 ^ (safe_sub_func_int8_t_s_s(((0x96L > ((((safe_lshift_func_uint16_t_u_s((safe_rshift_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s((p_43 , g_109), 0)), l_478)), 15)) <= 1UL) || 3UL) , 0x9FL)) < g_2), 0x3FL)))), p_43)))));
            l_479++;
        }
        (*l_482) = (*g_321);
    }
    for (l_394 = 19; (l_394 != 8); l_394 = safe_sub_func_int8_t_s_s(l_394, 1))
    { /* block id: 178 */
        int16_t *****l_498 = &l_497;
        int32_t l_501 = 0xF482893DL;
        int32_t l_513[1][1][3];
        uint16_t *l_551 = (void*)0;
        int16_t l_648 = 0x56DFL;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 3; k++)
                    l_513[i][j][k] = 0x11782B0EL;
            }
        }
    }
    return p_43;
}


/* ------------------------------------------ */
/* 
 * reads : g_52 g_59 g_64 g_58 g_6 g_2 g_57 g_51 g_109 g_230 g_231
 * writes: g_52 g_59 g_64 g_108 g_109 g_245
 */
static int32_t  func_44(int32_t * p_45, int32_t * p_46)
{ /* block id: 4 */
    uint16_t l_48[5][7][7] = {{{0x3A72L,1UL,0x3F54L,65528UL,0x8B96L,5UL,0xED73L},{0x6D06L,65527UL,1UL,5UL,0x8B96L,0x3F54L,0xA7ABL},{0x3637L,0xE96FL,65527UL,4UL,0x3047L,0xA7ABL,1UL},{0xDC63L,65528UL,0x6D06L,65534UL,0x4D7EL,65535UL,65528UL},{65535UL,65534UL,0xEAE1L,0xEAE1L,65534UL,65535UL,0xE96FL},{1UL,1UL,65535UL,0x65ABL,65531UL,65527UL,0x6D06L},{0x4E55L,0x3F54L,0UL,0x9D65L,0xED73L,1UL,65527UL}},{{65528UL,1UL,0x3709L,0x2772L,0xFEC1L,0xAE27L,0x1EC1L},{2UL,65534UL,0x895CL,65531UL,6UL,0x65ABL,0xE7AEL},{0UL,65528UL,0xFD17L,6UL,0xAE27L,65533UL,0UL},{0x6407L,0xE96FL,0xAE27L,1UL,65527UL,65533UL,0x9534L},{0xE7AEL,65527UL,0xA7ABL,0x6D06L,0x9773L,65533UL,0x353EL},{1UL,1UL,0xF93BL,65530UL,0xA7ABL,65533UL,5UL},{7UL,0x2772L,65535UL,65527UL,0xFD17L,0x65ABL,0x6407L}},{{65527UL,0xAE27L,2UL,0x1EBCL,2UL,0xAE27L,65527UL},{1UL,0x90F6L,5UL,0xED73L,1UL,1UL,0x65ABL},{65534UL,5UL,0xB705L,1UL,0xA97AL,65527UL,6UL},{0x1EBCL,65528UL,5UL,0xA7ABL,4UL,65535UL,1UL},{0x9D65L,1UL,2UL,1UL,0x00D2L,65535UL,0xFEC1L},{65532UL,0x18D7L,65535UL,0x895CL,65528UL,0xA7ABL,0xEAE1L},{0x3709L,65533UL,0xF93BL,0xDC63L,0x9D65L,0x3F54L,0xB705L}},{{0xE96FL,65531UL,0xA7ABL,0x1EC1L,0UL,5UL,0xB705L},{0x9534L,0x41B0L,0xAE27L,0x4E55L,0xB705L,0xE7AEL,0xEAE1L},{0x2772L,1UL,0xFD17L,65533UL,1UL,0x3637L,0xFEC1L},{65535UL,1UL,0x895CL,0x353EL,0x353EL,0x895CL,1UL},{0xE7AEL,5UL,0UL,0x9534L,0x353EL,1UL,0x3F54L},{0xEAE1L,0x1EC1L,65528UL,0x3637L,65530UL,0x9773L,0x90F6L},{5UL,0x3709L,0xEAE1L,0x9534L,65535UL,0x8B96L,0xED73L}},{{65531UL,0x90F6L,0xA7ABL,65527UL,0UL,65530UL,1UL},{0xFD17L,0x6D06L,65535UL,65534UL,0x3A72L,65532UL,65533UL},{1UL,0x9773L,0xFEC1L,65527UL,65535UL,5UL,65527UL},{0x3F54L,65528UL,0x8B96L,5UL,0xED73L,0x4D7EL,0xA97AL},{0x3F54L,1UL,65535UL,0x9773L,0x6407L,1UL,3UL},{1UL,0xE7AEL,0x8D03L,0xF93BL,0xFD17L,0x6407L,0x3047L},{0xFD17L,65530UL,5UL,0x4E55L,1UL,0x4E55L,5UL}}};
    int32_t *l_55 = &g_6[1];
    uint32_t l_73 = 0UL;
    const int32_t *l_105 = &g_59[2];
    int16_t *l_132 = &g_122;
    int32_t ** const l_186 = &g_57;
    int32_t ** const *l_185 = &l_186;
    int32_t *l_246[4] = {&g_6[1],&g_6[1],&g_6[1],&g_6[1]};
    int16_t ** const l_264 = (void*)0;
    int16_t l_305 = 0L;
    float *l_348 = &g_229[1][1];
    float **l_347[2][5][3] = {{{(void*)0,&l_348,&l_348},{(void*)0,&l_348,&l_348},{(void*)0,&l_348,(void*)0},{&l_348,(void*)0,&l_348},{&l_348,&l_348,&l_348}},{{&l_348,(void*)0,(void*)0},{&l_348,&l_348,(void*)0},{&l_348,&l_348,&l_348},{&l_348,&l_348,(void*)0},{&l_348,&l_348,(void*)0}}};
    int i, j, k;
    l_48[4][5][5]--;
    for (g_52 = 24; (g_52 < (-18)); --g_52)
    { /* block id: 8 */
        int32_t **l_106 = &g_57;
        uint32_t l_126 = 8UL;
        int16_t *l_131 = &g_122;
        int32_t l_203 = (-8L);
        l_55 = l_55;
        for (g_59[0] = 0; (g_59[0] == (-4)); --g_59[0])
        { /* block id: 12 */
            int32_t *l_92[7];
            int i;
            for (i = 0; i < 7; i++)
                l_92[i] = &g_6[1];
            for (g_64 = 0; (g_64 <= 1); g_64 += 1)
            { /* block id: 15 */
                int32_t **l_104 = &g_57;
                int32_t ***l_103 = &l_104;
                float *l_107 = &g_108;
                int i;
                g_109 &= (safe_mul_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_s((&g_58[g_64] == ((safe_rshift_func_int8_t_s_s((safe_sub_func_int8_t_s_s(l_73, (safe_mul_func_int8_t_s_s((safe_mod_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u(((((safe_sub_func_uint16_t_u_u(((((((safe_mul_func_float_f_f((-0x1.Bp-1), ((*l_107) = (((safe_add_func_uint64_t_u_u((((safe_div_func_int16_t_s_s((((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u(((((void*)0 == l_92[3]) || (g_58[g_64] >= ((safe_sub_func_int32_t_s_s((((safe_mul_func_uint8_t_u_u(((((safe_mul_func_uint16_t_u_u(g_59[4], ((((safe_lshift_func_int8_t_s_u((safe_add_func_uint16_t_u_u((((*l_103) = ((((0xEAEAAC79E06D3C1DLL > g_6[1]) >= (*l_55)) ^ g_2) , (void*)0)) == &p_46), g_2)), (*l_55))) | 8UL) , l_105) == (void*)0))) , (void*)0) != l_106) ^ 0x26L), 0xE2L)) ^ 0x5B9AADFED4114ABDLL) , 0x82A8E6F9L), (*g_57))) , 0L))) || (*l_55)), 9)), 65535UL)) , (void*)0) != (*l_106)), g_6[1])) > g_6[1]) <= (*l_105)), g_6[1])) , (*l_105)) <= g_64)))) > 0xF.E8B856p+4) == g_64) < g_59[0]) , g_51) != 0x46L), g_59[1])) >= g_2) , (**l_106)) | 0x98ED4FF94FDEA941LL), 0)), 65535UL)), g_59[3])))), 7)) , l_92[2])), 0)) < (**l_106)), 0x0DL));
            }
        }
        for (g_64 = 1; (g_64 >= 0); g_64 -= 1)
        { /* block id: 23 */
            int32_t ***l_120[7] = {&l_106,&l_106,&l_106,&l_106,&l_106,&l_106,&l_106};
            uint16_t l_124 = 65527UL;
            int16_t **l_164 = &l_131;
            int i;
        }
    }
    g_64 &= (safe_sub_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(((***l_185) <= ((((((((((void*)0 != (*l_185)) , 0x0.8EFB34p+85) >= (((safe_div_func_float_f_f((((&l_48[1][4][4] == &l_48[4][5][5]) , (*g_230)) == ((safe_mul_func_float_f_f(((g_245 = p_45) != (*l_186)), (*l_105))) <= (*l_105))), (-0x6.1p+1))) <= (*l_55)) != 0x6.AFE77Fp+23)) , &p_45) == &g_57) != (*p_45)) || g_59[0]) < 247UL) & (**l_186))), (*l_105))), g_59[4]));
    for (g_64 = 0; (g_64 <= 1); g_64 += 1)
    { /* block id: 86 */
        uint16_t *l_261 = &l_48[4][5][5];
        int32_t l_262 = (-1L);
        uint8_t *l_263 = &g_109;
        float *l_281 = &g_108;
        float *l_283[5];
        int16_t * const *l_337[9][10][2] = {{{&l_132,&l_132},{&l_132,&l_132},{&l_132,(void*)0},{&l_132,&l_132},{(void*)0,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132}},{{&l_132,&l_132},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{(void*)0,&l_132},{&l_132,(void*)0},{&l_132,&l_132},{&l_132,&l_132}},{{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132},{&l_132,&l_132}},{{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132},{&l_132,(void*)0},{&l_132,&l_132},{(void*)0,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132}},{{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{(void*)0,(void*)0},{&l_132,&l_132},{&l_132,&l_132},{&l_132,&l_132},{&l_132,(void*)0}},{{&l_132,&l_132},{&l_132,&l_132},{&l_132,(void*)0},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132}},{{&l_132,(void*)0},{&l_132,&l_132},{&l_132,&l_132},{&l_132,(void*)0},{&l_132,&l_132},{&l_132,&l_132},{&l_132,&l_132},{&l_132,(void*)0},{(void*)0,&l_132},{(void*)0,&l_132}},{{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{(void*)0,(void*)0},{&l_132,&l_132},{&l_132,&l_132},{&l_132,&l_132}},{{&l_132,(void*)0},{&l_132,&l_132},{&l_132,&l_132},{&l_132,(void*)0},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132},{&l_132,&l_132},{&l_132,&l_132},{(void*)0,&l_132}}};
        int16_t * const **l_336 = &l_337[3][3][1];
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_283[i] = &g_108;
    }
    return g_6[2];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_6[i], "g_6[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_51, "g_51", print_hash_value);
    transparent_crc(g_52, "g_52", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_58[i], "g_58[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_59[i], "g_59[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_62, "g_62", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_63[i][j][k], "g_63[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_64, "g_64", print_hash_value);
    transparent_crc_bytes (&g_108, sizeof(g_108), "g_108", print_hash_value);
    transparent_crc(g_109, "g_109", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    transparent_crc(g_134, "g_134", print_hash_value);
    transparent_crc(g_196, "g_196", print_hash_value);
    transparent_crc(g_200, "g_200", print_hash_value);
    transparent_crc(g_204, "g_204", print_hash_value);
    transparent_crc_bytes (&g_217, sizeof(g_217), "g_217", print_hash_value);
    transparent_crc_bytes (&g_221, sizeof(g_221), "g_221", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc_bytes(&g_229[i][j], sizeof(g_229[i][j]), "g_229[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_231, sizeof(g_231), "g_231", print_hash_value);
    transparent_crc(g_377, "g_377", print_hash_value);
    transparent_crc(g_517, "g_517", print_hash_value);
    transparent_crc(g_656, "g_656", print_hash_value);
    transparent_crc(g_676, "g_676", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_692[i], "g_692[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_805, sizeof(g_805), "g_805", print_hash_value);
    transparent_crc(g_965, "g_965", print_hash_value);
    transparent_crc(g_967, "g_967", print_hash_value);
    transparent_crc(g_1404, "g_1404", print_hash_value);
    transparent_crc(g_1410, "g_1410", print_hash_value);
    transparent_crc(g_1413, "g_1413", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1423[i][j][k], "g_1423[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1437, "g_1437", print_hash_value);
    transparent_crc_bytes (&g_1529, sizeof(g_1529), "g_1529", print_hash_value);
    transparent_crc(g_1604, "g_1604", print_hash_value);
    transparent_crc(g_1764, "g_1764", print_hash_value);
    transparent_crc(g_1816, "g_1816", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_1855[i][j][k], "g_1855[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_1889, sizeof(g_1889), "g_1889", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1895[i][j], "g_1895[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1926, "g_1926", print_hash_value);
    transparent_crc_bytes (&g_1951, sizeof(g_1951), "g_1951", print_hash_value);
    transparent_crc(g_2101, "g_2101", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2242[i], "g_2242[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_2263[i], "g_2263[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2265, "g_2265", print_hash_value);
    transparent_crc_bytes (&g_2267, sizeof(g_2267), "g_2267", print_hash_value);
    transparent_crc(g_2289, "g_2289", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2499[i][j], "g_2499[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2582, "g_2582", print_hash_value);
    transparent_crc(g_2586, "g_2586", print_hash_value);
    transparent_crc_bytes (&g_2595, sizeof(g_2595), "g_2595", print_hash_value);
    transparent_crc(g_2735, "g_2735", print_hash_value);
    transparent_crc(g_3176, "g_3176", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 699
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 56
breakdown:
   depth: 1, occurrence: 194
   depth: 2, occurrence: 47
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 5, occurrence: 5
   depth: 6, occurrence: 3
   depth: 8, occurrence: 1
   depth: 15, occurrence: 2
   depth: 17, occurrence: 2
   depth: 19, occurrence: 5
   depth: 22, occurrence: 4
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 26, occurrence: 2
   depth: 27, occurrence: 4
   depth: 29, occurrence: 5
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 32, occurrence: 2
   depth: 33, occurrence: 1
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 40, occurrence: 1
   depth: 46, occurrence: 1
   depth: 56, occurrence: 1

XXX total number of pointers: 705

XXX times a variable address is taken: 1323
XXX times a pointer is dereferenced on RHS: 535
breakdown:
   depth: 1, occurrence: 332
   depth: 2, occurrence: 117
   depth: 3, occurrence: 65
   depth: 4, occurrence: 18
   depth: 5, occurrence: 3
XXX times a pointer is dereferenced on LHS: 510
breakdown:
   depth: 1, occurrence: 374
   depth: 2, occurrence: 78
   depth: 3, occurrence: 38
   depth: 4, occurrence: 11
   depth: 5, occurrence: 9
XXX times a pointer is compared with null: 73
XXX times a pointer is compared with address of another variable: 25
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 9784

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1708
   level: 2, occurrence: 674
   level: 3, occurrence: 440
   level: 4, occurrence: 186
   level: 5, occurrence: 153
XXX number of pointers point to pointers: 363
XXX number of pointers point to scalars: 342
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 24.5
XXX average alias set size: 1.39

XXX times a non-volatile is read: 2941
XXX times a non-volatile is write: 1444
XXX times a volatile is read: 179
XXX    times read thru a pointer: 55
XXX times a volatile is write: 132
XXX    times written thru a pointer: 95
XXX times a volatile is available for access: 3.48e+03
XXX percentage of non-volatile access: 93.4

XXX forward jumps: 0
XXX backward jumps: 7

XXX stmts: 193
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 30
   depth: 2, occurrence: 40
   depth: 3, occurrence: 22
   depth: 4, occurrence: 30
   depth: 5, occurrence: 37

XXX percentage a fresh-made variable is used: 15.2
XXX percentage an existing variable is used: 84.8
********************* end of statistics **********************/


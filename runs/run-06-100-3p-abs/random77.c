/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2346672584
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   unsigned f0 : 18;
   unsigned f1 : 31;
   const unsigned f2 : 5;
   const unsigned f3 : 29;
   unsigned f4 : 24;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int32_t g_4 = 0xB13281D2L;
static int32_t * volatile g_3 = &g_4;/* VOLATILE GLOBAL g_3 */
static int32_t g_40 = 8L;
static int16_t g_56[9] = {0xB108L,1L,0xB108L,0xB108L,1L,0xB108L,0xB108L,1L,0xB108L};
static int16_t g_62 = 0xE856L;
static int16_t *g_61 = &g_62;
static uint16_t g_73 = 65528UL;
static int32_t g_79 = 0x9F139888L;
static uint16_t *g_88[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t *g_93[8][6][3] = {{{&g_79,&g_79,&g_79},{&g_79,&g_79,(void*)0},{&g_4,&g_79,&g_79},{&g_79,&g_4,&g_4},{&g_4,&g_4,&g_4},{(void*)0,&g_4,&g_4}},{{&g_79,&g_79,(void*)0},{&g_4,&g_79,&g_4},{&g_4,(void*)0,&g_4},{&g_79,&g_79,&g_4},{&g_4,&g_79,(void*)0},{&g_4,&g_4,(void*)0}},{{(void*)0,&g_4,&g_4},{&g_79,&g_4,&g_79},{&g_79,&g_79,&g_4},{(void*)0,&g_4,&g_79},{&g_4,(void*)0,&g_4},{&g_4,&g_4,&g_4}},{{&g_4,&g_79,&g_4},{&g_4,&g_4,&g_79},{&g_4,&g_79,&g_4},{&g_4,&g_4,&g_4},{&g_4,&g_79,&g_79},{&g_79,&g_4,&g_4}},{{&g_4,&g_79,(void*)0},{(void*)0,(void*)0,&g_79},{(void*)0,&g_79,&g_4},{&g_79,&g_4,&g_4},{&g_79,&g_79,&g_79},{&g_79,&g_4,&g_4}},{{(void*)0,&g_79,&g_4},{&g_4,&g_4,&g_79},{&g_79,&g_79,&g_79},{&g_79,&g_4,&g_79},{&g_79,(void*)0,(void*)0},{&g_4,&g_4,&g_4}},{{&g_79,&g_79,&g_79},{&g_4,&g_4,&g_79},{&g_4,&g_4,&g_4},{(void*)0,&g_4,(void*)0},{&g_4,&g_4,&g_79},{&g_4,(void*)0,&g_79}},{{&g_4,&g_4,&g_79},{(void*)0,&g_4,&g_4},{&g_79,(void*)0,&g_4},{&g_79,&g_4,&g_79},{&g_4,&g_79,&g_4},{&g_79,&g_79,&g_4}}};
static int32_t ** volatile g_92 = &g_93[0][2][0];/* VOLATILE GLOBAL g_92 */
static int32_t g_95 = 8L;
static volatile uint32_t g_106 = 8UL;/* VOLATILE GLOBAL g_106 */
static struct S0 g_113 = {465,30020,2,13001,3046};
static int64_t g_116 = 1L;
static float g_118 = (-0x1.7p+1);
static uint32_t g_155 = 0xF9DC3F5DL;
static int32_t ** volatile g_185 = &g_93[7][2][1];/* VOLATILE GLOBAL g_185 */
static int32_t ** volatile g_188 = &g_93[0][2][0];/* VOLATILE GLOBAL g_188 */
static int16_t g_212[6] = {0xFC20L,0xFC20L,0xFC20L,0xFC20L,0xFC20L,0xFC20L};
static uint64_t g_221 = 0x78632126A534EDE8LL;
static int64_t *g_236 = &g_116;
static int64_t **g_235 = &g_236;
static int64_t ***g_234 = &g_235;
static float * volatile g_264 = (void*)0;/* VOLATILE GLOBAL g_264 */
static int32_t ** volatile * const * volatile g_272 = (void*)0;/* VOLATILE GLOBAL g_272 */
static uint8_t g_279 = 6UL;
static int8_t g_309 = 0xDFL;
static struct S0 *g_317 = &g_113;
static struct S0 ** volatile g_316 = &g_317;/* VOLATILE GLOBAL g_316 */
static int64_t g_342 = 0x12E71CDC07732DF8LL;
static volatile int64_t g_391 = 0xAC58BC7904C0D534LL;/* VOLATILE GLOBAL g_391 */
static int8_t g_411 = 9L;
static int8_t g_432 = 0xB6L;
static int32_t ** volatile g_464 = &g_93[0][2][0];/* VOLATILE GLOBAL g_464 */
static uint16_t * const *g_503 = &g_88[0];
static uint16_t * const ** volatile g_502[1][10][6] = {{{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0},{&g_503,&g_503,(void*)0,&g_503,&g_503,(void*)0}}};
static float g_525 = 0x8.1DD4E5p+43;
static volatile uint64_t g_530 = 1UL;/* VOLATILE GLOBAL g_530 */
static uint64_t *g_585[9][3][1] = {{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}},{{&g_221},{&g_221},{&g_221}}};
static uint64_t **g_584[5] = {&g_585[2][2][0],&g_585[2][2][0],&g_585[2][2][0],&g_585[2][2][0],&g_585[2][2][0]};
static int32_t **g_612 = &g_93[6][1][0];
static int32_t *** volatile g_611 = &g_612;/* VOLATILE GLOBAL g_611 */
static int64_t **** volatile g_637 = (void*)0;/* VOLATILE GLOBAL g_637 */
static int64_t **** volatile g_638 = &g_234;/* VOLATILE GLOBAL g_638 */
static int32_t g_669 = 0x43D4EE43L;
static int32_t ** volatile g_682 = (void*)0;/* VOLATILE GLOBAL g_682 */
static int16_t g_715[4][6][1] = {{{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)}},{{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)}},{{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)}},{{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)},{(-1L)}}};
static float g_799 = (-0x5.1p+1);
static float * const  volatile g_798 = &g_799;/* VOLATILE GLOBAL g_798 */
static uint64_t g_896 = 7UL;
static volatile uint64_t g_942 = 18446744073709551614UL;/* VOLATILE GLOBAL g_942 */
static struct S0 ** volatile *g_950[8] = {&g_316,&g_316,&g_316,&g_316,&g_316,&g_316,&g_316,&g_316};
static int64_t ** const *g_963 = &g_235;
static int64_t ** const **g_962 = &g_963;
static int32_t *g_1029 = &g_669;
static int32_t ***g_1079 = &g_612;
static int32_t ****g_1078 = &g_1079;
static int32_t *****g_1077 = &g_1078;
static int32_t ** volatile g_1102[3][2] = {{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029}};
static int32_t ** volatile g_1103 = &g_1029;/* VOLATILE GLOBAL g_1103 */
static volatile int16_t g_1145 = 0L;/* VOLATILE GLOBAL g_1145 */
static volatile int16_t *g_1144[4][1] = {{&g_1145},{&g_1145},{&g_1145},{&g_1145}};
static volatile int16_t **g_1143[3][10] = {{&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0],&g_1144[3][0]}};
static volatile int16_t ***g_1142[6] = {&g_1143[1][8],&g_1143[1][8],&g_1143[1][8],&g_1143[1][8],&g_1143[1][8],&g_1143[1][8]};
static float * volatile g_1147 = (void*)0;/* VOLATILE GLOBAL g_1147 */
static float * volatile g_1148 = &g_525;/* VOLATILE GLOBAL g_1148 */
static volatile int16_t **g_1174[7][2] = {{&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0]},{&g_1144[3][0],&g_1144[3][0]}};
static float * volatile g_1311 = &g_525;/* VOLATILE GLOBAL g_1311 */
static uint16_t g_1349[5][1] = {{0x8D8EL},{0x8D8EL},{0x8D8EL},{0x8D8EL},{0x8D8EL}};
static int32_t g_1404 = 0L;
static uint8_t g_1516 = 255UL;
static struct S0 g_1585 = {467,44450,0,8882,2113};
static int16_t **g_1593 = &g_61;
static int16_t ** volatile *g_1592 = &g_1593;
static int16_t ** volatile * volatile *g_1591 = &g_1592;
static int16_t ** volatile * volatile * volatile * volatile g_1590 = &g_1591;/* VOLATILE GLOBAL g_1590 */
static int8_t g_1594[4] = {6L,6L,6L,6L};
static int64_t *****g_1623 = (void*)0;
static int32_t g_1659 = 0xD390FDDDL;
static int32_t g_1669 = 0xB91B3758L;
static uint8_t *** volatile g_1677 = (void*)0;/* VOLATILE GLOBAL g_1677 */
static volatile uint32_t g_1688[6] = {4294967295UL,0x87FC9D9FL,0x87FC9D9FL,4294967295UL,0x87FC9D9FL,0x87FC9D9FL};
static int64_t g_1691 = 0x6F9B42BDF422420BLL;
static uint32_t **g_1705 = (void*)0;
static volatile uint8_t g_1768[6] = {1UL,0x67L,0x67L,1UL,0x67L,0x67L};
static volatile uint8_t *g_1767[1][8] = {{&g_1768[2],&g_1768[2],&g_1768[2],&g_1768[2],&g_1768[2],&g_1768[2],&g_1768[2],&g_1768[2]}};
static volatile uint8_t * volatile *g_1766 = &g_1767[0][1];
static volatile uint8_t * volatile **g_1765[4][4][10] = {{{&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766},{&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766},{&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766},{&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766}},{{&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766},{(void*)0,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,(void*)0,&g_1766,&g_1766,&g_1766},{&g_1766,(void*)0,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766},{(void*)0,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,(void*)0}},{{&g_1766,(void*)0,&g_1766,&g_1766,&g_1766,&g_1766,(void*)0,&g_1766,&g_1766,&g_1766},{&g_1766,&g_1766,&g_1766,(void*)0,&g_1766,(void*)0,&g_1766,&g_1766,&g_1766,(void*)0},{&g_1766,(void*)0,(void*)0,&g_1766,&g_1766,(void*)0,&g_1766,&g_1766,&g_1766,&g_1766},{(void*)0,(void*)0,&g_1766,&g_1766,&g_1766,(void*)0,(void*)0,(void*)0,&g_1766,&g_1766}},{{&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,(void*)0},{&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,(void*)0,&g_1766,&g_1766,&g_1766},{(void*)0,(void*)0,&g_1766,&g_1766,(void*)0,&g_1766,&g_1766,&g_1766,&g_1766,(void*)0},{&g_1766,(void*)0,(void*)0,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766,&g_1766}}};
static uint8_t g_1770 = 254UL;
static int32_t g_1826[4] = {0xA04891A4L,0xA04891A4L,0xA04891A4L,0xA04891A4L};
static uint32_t *g_1831 = &g_155;
static uint32_t * const *g_1830 = &g_1831;
static uint32_t * const **g_1829[2] = {&g_1830,&g_1830};
static uint16_t g_2018[5][7][7] = {{{65528UL,0x711AL,0x0189L,0x0189L,0x711AL,65528UL,0x0BE3L},{0xD4E1L,5UL,0xA970L,0UL,65526UL,7UL,0UL},{0xDB3CL,0UL,65527UL,65528UL,0x71D6L,0x711AL,0x71D6L},{65526UL,5UL,0xAB17L,0xC234L,65527UL,0UL,0xB16AL},{0UL,0x711AL,7UL,0UL,0xD440L,4UL,0x0189L},{0x855EL,0x70C9L,0xB16AL,65529UL,0xD4E1L,65529UL,0xB16AL},{65527UL,65527UL,0xC5FBL,65535UL,4UL,0x0BE3L,0x71D6L}},{{65535UL,8UL,65534UL,5UL,0x855EL,0xFC03L,0UL},{0x0BE3L,0UL,4UL,0x71D6L,4UL,0UL,0x0BE3L},{0x0D7CL,65529UL,65526UL,0x6F94L,0xD4E1L,4UL,0xAB17L},{0x0189L,7UL,0UL,1UL,0xD440L,0xDB3CL,0xCC18L},{0x1DAAL,5UL,0UL,5UL,0x1DAAL,0x6F94L,0x0D7CL},{0xD440L,1UL,0UL,7UL,0x0189L,0x0BE3L,0UL},{0xAB17L,8UL,0xA970L,3UL,0UL,8UL,0xB16AL}},{{0xD440L,7UL,0xD440L,0xC5FBL,0UL,65528UL,0x7517L},{0x1DAAL,0xFC03L,0x4C87L,0x6F94L,0xB16AL,0xC866L,0x9A08L},{0UL,0xD440L,65528UL,4UL,4UL,65528UL,0xD440L},{0x9A08L,7UL,0xD4E1L,0xC234L,1UL,8UL,0xF955L},{7UL,65535UL,0xDB3CL,0UL,0x7517L,0x0BE3L,65527UL},{65534UL,0xB557L,65526UL,0xC234L,65535UL,0x6F94L,0x38F3L},{0xDB3CL,65527UL,0UL,4UL,0xC5FBL,0xCC18L,0UL}},{{0xF955L,3UL,0x855EL,0x6F94L,0xA970L,0xB557L,0xA970L},{0xC5FBL,65527UL,65527UL,0xC5FBL,65535UL,4UL,0x0BE3L},{0UL,0xB557L,65535UL,3UL,0x9A08L,0x70C9L,65526UL},{0xCC18L,65535UL,0x0BE3L,7UL,0xDB3CL,7UL,0x0BE3L},{0x855EL,7UL,0x0D7CL,5UL,65527UL,65535UL,0xA970L},{0x71D6L,0xD440L,0x0189L,65527UL,0xCC18L,0UL,0UL},{0x38F3L,0xFC03L,65527UL,0x85D7L,65527UL,0xFC03L,0x38F3L}},{{0UL,7UL,0xC5FBL,0x0189L,0xDB3CL,0UL,65527UL},{65526UL,8UL,0x1DAAL,0xD28AL,0x9A08L,65530UL,0xF955L},{65535UL,1UL,0xC5FBL,1UL,65535UL,0x71D6L,0xD440L},{0x0D7CL,5UL,65527UL,65535UL,0xA970L,65529UL,0x9A08L},{1UL,65528UL,0x0189L,0x7517L,0xC5FBL,0xC5FBL,0x7517L},{0x0D7CL,65535UL,0x0D7CL,8UL,65535UL,8UL,0xB16AL},{65535UL,4UL,0x0BE3L,0x71D6L,0x7517L,0xD440L,0UL}}};
static const struct S0 *g_2042 = (void*)0;
static const struct S0 ** volatile g_2041 = &g_2042;/* VOLATILE GLOBAL g_2041 */
static int32_t g_2055[6][3][10] = {{{(-2L),0xAFD77CCCL,0x13F5E3D1L,1L,1L,1L,0x767C8FA1L,0xDCA27289L,0x7FEFB659L,0xD7276CC7L},{0x767C8FA1L,0x5E40C883L,0x2F3CCD24L,0x4F69A750L,(-1L),0xE2095E04L,0L,1L,0x9DA97273L,1L},{0x623B4709L,1L,9L,0xDCA27289L,9L,5L,0x13F5E3D1L,(-5L),0x9DA97273L,0x5E40C883L}},{{0x767C8FA1L,0xD7276CC7L,0x9DA97273L,0xE2095E04L,1L,0x3F7CD3E5L,0xC0A10336L,0x7D3D8E1CL,(-1L),(-5L)},{(-1L),0xD7276CC7L,0xC0A10336L,(-5L),0x7D757520L,5L,0x13F5E3D1L,0x5E40C883L,0x7FEFB659L,0x3F7CD3E5L},{0L,5L,0x623B4709L,5L,(-3L),0xF9034E10L,1L,8L,1L,0xF9034E10L}},{{0L,0x7D3D8E1CL,0x767C8FA1L,0x7D3D8E1CL,0L,5L,0L,0xDCA27289L,1L,0x14C5530CL},{(-1L),0xF9034E10L,(-2L),(-1L),0L,0x3F7CD3E5L,0x7D757520L,0xD7276CC7L,9L,0x14C5530CL},{0x767C8FA1L,(-1L),1L,0xAFD77CCCL,0L,5L,0x623B4709L,5L,(-3L),0xF9034E10L}},{{0x13F5E3D1L,0x5E40C883L,0x7FEFB659L,0x3F7CD3E5L,(-3L),0x4F69A750L,1L,0x4F69A750L,(-3L),0x3F7CD3E5L},{0L,1L,0L,0xAFD77CCCL,0x7D757520L,8L,(-3L),1L,9L,(-5L)},{0xC0A10336L,8L,0L,(-1L),1L,0x14C5530CL,0L,1L,1L,0x5E40C883L}},{{0x7FEFB659L,(-5L),0L,0x7D3D8E1CL,9L,1L,(-2L),0x4F69A750L,1L,5L},{0L,0xE2095E04L,0x7FEFB659L,5L,(-2L),0xD7276CC7L,(-2L),5L,0x7FEFB659L,0xE2095E04L},{1L,(-5L),1L,(-5L),0x2F3CCD24L,0xAFD77CCCL,0L,0xD7276CC7L,(-1L),0x4F69A750L}},{{(-1L),8L,(-2L),0xE2095E04L,0x767C8FA1L,0xAFD77CCCL,(-3L),0xDCA27289L,0x9DA97273L,0x7D3D8E1CL},{1L,1L,0x767C8FA1L,0xDCA27289L,0x7FEFB659L,0xD7276CC7L,1L,8L,(-1L),0x3ACB6D64L},{0L,0x5E40C883L,0x623B4709L,1L,0x7FEFB659L,1L,0x623B4709L,0x5E40C883L,0L,0x7D3D8E1CL}}};
static int32_t *g_2069 = (void*)0;
static int32_t * const ***g_2119[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t * const **** volatile g_2118 = &g_2119[1];/* VOLATILE GLOBAL g_2118 */
static const struct S0 * const *g_2127 = &g_2042;
static const struct S0 * const **g_2126 = &g_2127;
static const struct S0 * const ***g_2125 = &g_2126;
static const struct S0 * const ****g_2124 = &g_2125;
static int8_t **g_2129 = (void*)0;
static float * volatile g_2182 = &g_525;/* VOLATILE GLOBAL g_2182 */
static volatile uint8_t g_2231 = 251UL;/* VOLATILE GLOBAL g_2231 */
static int64_t g_2405[2][6][1] = {{{0x7B1D92D4C6D434D1LL},{0x07D8B7E896B92F2ELL},{0x7B1D92D4C6D434D1LL},{0x07D8B7E896B92F2ELL},{0x7B1D92D4C6D434D1LL},{0x07D8B7E896B92F2ELL}},{{0x7B1D92D4C6D434D1LL},{0x07D8B7E896B92F2ELL},{0x7B1D92D4C6D434D1LL},{0x07D8B7E896B92F2ELL},{0x7B1D92D4C6D434D1LL},{0x07D8B7E896B92F2ELL}}};
static volatile int16_t g_2408[6][4] = {{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)},{(-5L),(-5L),(-5L),(-5L)}};
static struct S0 ** volatile g_2413 = (void*)0;/* VOLATILE GLOBAL g_2413 */
static int64_t g_2417 = 1L;
static const int32_t *g_2420 = &g_2055[2][2][5];
static const int32_t ** volatile g_2419[8] = {&g_2420,&g_2420,&g_2420,&g_2420,&g_2420,&g_2420,&g_2420,&g_2420};
static uint16_t g_2456 = 0x2E0DL;
static int64_t g_2462 = (-5L);
static const struct S0 ** volatile g_2495 = &g_2042;/* VOLATILE GLOBAL g_2495 */
static const struct S0 ** volatile g_2524[1][6] = {{&g_2042,&g_2042,&g_2042,&g_2042,&g_2042,&g_2042}};
static const struct S0 ** volatile g_2525 = &g_2042;/* VOLATILE GLOBAL g_2525 */
static const uint16_t *g_2542 = (void*)0;
static const uint16_t **g_2541 = &g_2542;
static const uint16_t ***g_2540[8][5][6] = {{{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{(void*)0,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,(void*)0,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,(void*)0,(void*)0}},{{&g_2541,&g_2541,(void*)0,&g_2541,(void*)0,&g_2541},{(void*)0,&g_2541,(void*)0,&g_2541,&g_2541,(void*)0},{&g_2541,&g_2541,(void*)0,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,(void*)0},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541}},{{&g_2541,&g_2541,(void*)0,&g_2541,(void*)0,(void*)0},{&g_2541,(void*)0,(void*)0,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_2541,(void*)0,(void*)0,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541}},{{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,(void*)0,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,(void*)0,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,(void*)0,&g_2541,&g_2541},{&g_2541,(void*)0,&g_2541,&g_2541,&g_2541,&g_2541}},{{&g_2541,(void*)0,(void*)0,&g_2541,&g_2541,(void*)0},{&g_2541,&g_2541,(void*)0,&g_2541,(void*)0,(void*)0},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,(void*)0},{&g_2541,&g_2541,&g_2541,&g_2541,(void*)0,(void*)0},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541}},{{&g_2541,&g_2541,&g_2541,(void*)0,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,(void*)0,(void*)0,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{(void*)0,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,(void*)0,(void*)0,&g_2541}},{{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,(void*)0,(void*)0,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{(void*)0,&g_2541,(void*)0,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,(void*)0,&g_2541}},{{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{(void*)0,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,(void*)0},{&g_2541,(void*)0,&g_2541,&g_2541,&g_2541,&g_2541},{&g_2541,&g_2541,&g_2541,&g_2541,&g_2541,&g_2541}}};
static int32_t ** volatile g_2563[9] = {&g_93[6][0][1],&g_93[6][0][1],&g_93[6][0][1],&g_93[6][0][1],&g_93[6][0][1],&g_93[6][0][1],&g_93[6][0][1],&g_93[6][0][1],&g_93[6][0][1]};
static int32_t ** volatile g_2564 = &g_2069;/* VOLATILE GLOBAL g_2564 */
static uint16_t ***g_2588 = (void*)0;
static const volatile uint64_t * volatile g_2610 = &g_530;/* VOLATILE GLOBAL g_2610 */
static const volatile uint64_t * volatile *g_2609 = &g_2610;
static const volatile uint64_t * volatile **g_2608 = &g_2609;
static const volatile uint64_t * volatile ** const * volatile g_2607 = &g_2608;/* VOLATILE GLOBAL g_2607 */
static int32_t g_2651 = (-1L);
static struct S0 **g_2659 = &g_317;
static struct S0 ***g_2658 = &g_2659;
static int8_t *g_2708 = &g_432;
static uint32_t g_2711 = 4294967295UL;
static volatile uint64_t g_2716 = 0xED17237C5A730128LL;/* VOLATILE GLOBAL g_2716 */
static volatile float g_2742 = 0x1.0p+1;/* VOLATILE GLOBAL g_2742 */
static const uint8_t g_2758 = 255UL;
static int32_t *g_2764 = (void*)0;
static int32_t *g_2781 = &g_95;
static volatile int32_t g_2831 = (-1L);/* VOLATILE GLOBAL g_2831 */
static int16_t ****g_2934 = (void*)0;
static int16_t ***** volatile g_2933 = &g_2934;/* VOLATILE GLOBAL g_2933 */
static int32_t g_2941 = 0xEC5F8E2BL;
static struct S0 ** const *g_2953 = &g_2659;
static struct S0 ** const ** volatile g_2952 = &g_2953;/* VOLATILE GLOBAL g_2952 */
static struct S0 ****g_3039 = &g_2658;
static struct S0 *****g_3038 = &g_3039;
static const uint16_t **** volatile g_3090 = &g_2540[2][1][0];/* VOLATILE GLOBAL g_3090 */
static int8_t g_3095 = 0L;


/* --- FORWARD DECLARATIONS --- */
static const int16_t  func_1(void);
static uint8_t  func_7(int64_t  p_8, int64_t  p_9);
static int32_t  func_10(int32_t  p_11, int32_t * p_12, uint16_t  p_13, int32_t * p_14, int32_t * p_15);
static int32_t * func_17(const struct S0  p_18, float  p_19, int32_t * p_20, uint8_t  p_21);
static uint16_t  func_22(int32_t * p_23);
static int32_t * func_24(int32_t  p_25, const int8_t  p_26);
static uint32_t  func_27(uint32_t  p_28, int32_t * p_29, uint64_t  p_30, int8_t  p_31);
static uint16_t  func_33(uint16_t  p_34, uint64_t  p_35, uint16_t  p_36, float  p_37);
static const int8_t  func_43(int32_t * const  p_44, int8_t  p_45, uint32_t  p_46);
static int32_t * func_57(int16_t * p_58, int32_t * const  p_59, int32_t  p_60);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_4 g_2182 g_525 g_2018 g_40 g_116 g_56 g_1585.f4 g_432 g_798 g_799 g_1585.f0 g_155 g_1691 g_92 g_93 g_2708 g_113.f3 g_61 g_62 g_1593 g_1831 g_1590 g_1591 g_1592 g_1029 g_669 g_1078 g_1079 g_234 g_235 g_236 g_962 g_963 g_1766 g_1767 g_1768 g_2055 g_2420 g_1830 g_2781 g_95 g_3038 g_2658 g_2659 g_317 g_316 g_1404 g_2609 g_2610 g_530 g_3090 g_1103 g_503 g_88
 * writes: g_4 g_118 g_432 g_40 g_525 g_79 g_1691 g_155 g_669 g_95 g_3038 g_317 g_1404 g_2540 g_2055 g_73 g_1768 g_116
 */
static const int16_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_2[5][3] = {{6UL,0xFDE2BBEF7D65121ALL,0xFDE2BBEF7D65121ALL},{6UL,0xFDE2BBEF7D65121ALL,0xFDE2BBEF7D65121ALL},{6UL,0xFDE2BBEF7D65121ALL,0xFDE2BBEF7D65121ALL},{6UL,0xFDE2BBEF7D65121ALL,0xFDE2BBEF7D65121ALL},{6UL,0xFDE2BBEF7D65121ALL,0xFDE2BBEF7D65121ALL}};
    int32_t *l_1313 = &g_40;
    const int32_t *l_1314 = &g_4;
    const struct S0 l_1315 = {327,35463,4,9176,712};
    int32_t *l_1316 = (void*)0;
    int32_t *l_2415 = &g_2055[1][2][5];
    int16_t *l_2471[9] = {&g_715[1][0][0],&g_715[1][0][0],&g_715[1][0][0],&g_715[1][0][0],&g_715[1][0][0],&g_715[1][0][0],&g_715[1][0][0],&g_715[1][0][0],&g_715[1][0][0]};
    float l_2509 = 0x5.0A643Bp-55;
    float l_2522[2];
    uint32_t l_2579 = 0x76B49890L;
    uint16_t **l_2586 = &g_88[0];
    uint16_t ***l_2585 = &l_2586;
    int32_t *****l_2633[1][3][1];
    int16_t ***l_2663 = &g_1593;
    int16_t **** const l_2662[3] = {&l_2663,&l_2663,&l_2663};
    int8_t l_2667 = (-5L);
    uint32_t **l_2763[1];
    float l_2777 = 0x1.D9EB03p-83;
    float l_2779 = (-0x2.7p-1);
    int8_t l_2780 = 0x36L;
    float *l_2786 = &g_118;
    int16_t l_2797 = 0x349DL;
    int32_t l_2822[7][5] = {{(-2L),(-2L),0x9CB35152L,0x9CB35152L,(-2L)},{0x74444991L,0xC0419A19L,0x74444991L,0xC0419A19L,0x74444991L},{(-2L),0x9CB35152L,0x9CB35152L,(-2L),(-2L)},{2L,0xC0419A19L,2L,0xC0419A19L,2L},{(-2L),(-2L),0x9CB35152L,0x9CB35152L,(-2L)},{0x74444991L,0xC0419A19L,0x74444991L,0xC0419A19L,0x74444991L},{(-2L),0x9CB35152L,0x9CB35152L,(-2L),(-2L)}};
    uint8_t **l_2872 = (void*)0;
    uint8_t ***l_2871 = &l_2872;
    uint8_t ****l_2870 = &l_2871;
    uint8_t l_2927[3][7][1] = {{{0x77L},{1UL},{0xC4L},{1UL},{0x77L},{0xA5L},{0x77L}},{{1UL},{0xC4L},{1UL},{0x77L},{0xA5L},{0x77L},{1UL}},{{0xC4L},{1UL},{0x77L},{0xA5L},{0x77L},{1UL},{0xC4L}}};
    uint8_t l_2946 = 0x01L;
    struct S0 ** const *l_2951 = &g_2659;
    int64_t ***l_2960 = (void*)0;
    const uint64_t *l_2979 = &g_896;
    const uint64_t **l_2978 = &l_2979;
    const uint64_t ***l_2977[9][8] = {{&l_2978,&l_2978,(void*)0,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978},{&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978},{&l_2978,(void*)0,&l_2978,&l_2978,(void*)0,&l_2978,&l_2978,&l_2978},{&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978},{(void*)0,&l_2978,&l_2978,&l_2978,&l_2978,(void*)0,&l_2978,(void*)0},{&l_2978,(void*)0,&l_2978,(void*)0,&l_2978,(void*)0,&l_2978,&l_2978},{&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978},{&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,(void*)0},{&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978,&l_2978}};
    uint16_t l_2984 = 0x8D07L;
    int8_t l_2995 = 0x1FL;
    int32_t l_3021 = 0xAFA93B93L;
    uint8_t l_3022 = 0xE2L;
    uint32_t l_3135 = 0x86CCB802L;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2522[i] = 0x0.049003p+67;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 1; k++)
                l_2633[i][j][k] = (void*)0;
        }
    }
    for (i = 0; i < 1; i++)
        l_2763[i] = &g_1831;
    (*g_3) &= l_2[2][0];
    for (g_4 = 0; (g_4 <= 2); g_4 += 1)
    { /* block id: 4 */
        int64_t l_16 = 0x714FFD8143836745LL;
        int32_t **l_1312[9][8][1] = {{{&g_1029},{&g_1029},{(void*)0},{&g_93[3][1][1]},{&g_93[5][4][1]},{(void*)0},{&g_93[7][3][0]},{&g_1029}},{{(void*)0},{&g_1029},{&g_93[7][3][0]},{(void*)0},{&g_93[5][4][1]},{&g_93[3][1][1]},{(void*)0},{&g_1029}},{{&g_1029},{(void*)0},{&g_1029},{&g_1029},{(void*)0},{&g_93[3][1][1]},{&g_93[5][4][1]},{(void*)0}},{{&g_93[7][3][0]},{&g_1029},{(void*)0},{&g_1029},{&g_93[7][3][0]},{(void*)0},{&g_93[5][4][1]},{&g_93[3][1][1]}},{{(void*)0},{&g_1029},{&g_1029},{(void*)0},{&g_1029},{&g_1029},{(void*)0},{&g_93[3][1][1]}},{{&g_93[5][4][1]},{(void*)0},{&g_93[7][3][0]},{&g_1029},{(void*)0},{&g_1029},{&g_93[7][3][0]},{(void*)0}},{{&g_93[5][4][1]},{&g_93[3][1][1]},{(void*)0},{&g_1029},{&g_1029},{(void*)0},{&g_1029},{&g_1029}},{{(void*)0},{&g_93[3][1][1]},{&g_93[5][4][1]},{(void*)0},{&g_93[7][3][0]},{&g_1029},{(void*)0},{&g_1029}},{{&g_93[7][3][0]},{(void*)0},{&g_93[5][4][1]},{&g_93[3][1][1]},{(void*)0},{&g_1029},{&g_1029},{(void*)0}}};
        uint32_t l_1317 = 18446744073709551615UL;
        int64_t *l_2416 = &g_2417;
        uint8_t l_2418 = 0UL;
        int32_t l_2457 = (-3L);
        float *l_2469 = &g_118;
        struct S0 l_2498 = {204,12045,1,2957,1934};
        float l_2514 = 0x9.1AB666p-4;
        uint16_t ***l_2587[6][9][3] = {{{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{(void*)0,&l_2586,&l_2586},{&l_2586,(void*)0,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,(void*)0,(void*)0},{&l_2586,&l_2586,(void*)0},{&l_2586,&l_2586,&l_2586}},{{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{(void*)0,&l_2586,&l_2586},{(void*)0,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{(void*)0,&l_2586,&l_2586}},{{(void*)0,(void*)0,&l_2586},{&l_2586,(void*)0,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,(void*)0,&l_2586},{&l_2586,(void*)0,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,(void*)0},{&l_2586,&l_2586,&l_2586}},{{&l_2586,&l_2586,(void*)0},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,(void*)0},{&l_2586,(void*)0,(void*)0},{&l_2586,&l_2586,(void*)0},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,(void*)0},{(void*)0,&l_2586,&l_2586},{&l_2586,(void*)0,(void*)0}},{{(void*)0,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,(void*)0},{&l_2586,&l_2586,&l_2586},{(void*)0,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{(void*)0,&l_2586,&l_2586},{&l_2586,&l_2586,(void*)0}},{{&l_2586,(void*)0,&l_2586},{&l_2586,(void*)0,&l_2586},{&l_2586,(void*)0,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,&l_2586},{&l_2586,&l_2586,(void*)0},{&l_2586,&l_2586,&l_2586}}};
        int32_t *l_2646 = &g_79;
        struct S0 ***l_2669 = &g_2659;
        uint64_t ** const l_2692 = &g_585[8][2][0];
        int16_t l_2715 = 0x6712L;
        int i, j, k;
    }
    if ((+((safe_add_func_uint8_t_u_u((((!(((*l_2786) = (*g_2182)) == (safe_mul_func_float_f_f(g_2018[1][5][4], (safe_div_func_float_f_f((*l_1313), ((((safe_div_func_float_f_f(0x0.2p+1, (0xF.5EA6A7p-92 != (safe_sub_func_float_f_f((*l_1314), ((safe_mul_func_float_f_f((*l_1314), g_116)) != g_40)))))) == g_525) > (*l_1313)) >= g_56[5]))))))) , (*l_1314)) , 0x84L), g_1585.f4)) <= l_2797)))
    { /* block id: 1199 */
        uint16_t l_2800 = 0UL;
        uint8_t *l_2812 = &g_1770;
        uint8_t **l_2811 = &l_2812;
        const int32_t l_2836 = 0x4010145EL;
        int32_t *l_2838 = &g_79;
        int32_t l_2894 = 1L;
        int32_t l_2895 = 0x0470A218L;
        const uint8_t l_2929 = 251UL;
        int32_t l_2940 = 0xA7B72A35L;
        int32_t l_2942 = 4L;
        int32_t l_2943[2][6] = {{0x230FC933L,0x230FC933L,(-7L),0x230FC933L,0x230FC933L,(-7L)},{0x230FC933L,0x230FC933L,(-7L),0x230FC933L,0x230FC933L,(-7L)}};
        int16_t l_2961 = (-1L);
        int32_t *l_2965 = &g_4;
        int32_t l_2976 = 0x381D9F48L;
        int32_t l_2980 = 0x2664D54CL;
        float *l_2981[6][8];
        int i, j;
        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 8; j++)
                l_2981[i][j] = (void*)0;
        }
        l_2800 |= (safe_rshift_func_int16_t_s_u(0xBD05L, 10));
        for (g_432 = 0; (g_432 <= 1); g_432 += 1)
        { /* block id: 1203 */
            float *l_2813[10][10] = {{(void*)0,&l_2522[1],&l_2509,(void*)0,&g_525,&l_2509,&g_525,&l_2522[0],&l_2509,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&l_2522[0],&g_525,(void*)0,&g_525,&l_2522[0],(void*)0},{&g_525,(void*)0,&g_525,&l_2522[0],(void*)0,(void*)0,(void*)0,(void*)0,&g_118,(void*)0},{&l_2522[0],&g_525,&l_2509,&g_525,(void*)0,&l_2509,&l_2522[1],(void*)0,&l_2509,&l_2777},{&l_2522[1],&g_118,&g_525,&g_799,&g_118,&g_118,&g_799,&g_525,&g_118,&l_2522[1]},{(void*)0,&l_2522[0],(void*)0,&g_118,&l_2777,(void*)0,(void*)0,&l_2522[0],&g_525,&g_118},{&g_799,(void*)0,&l_2509,&l_2522[0],&l_2777,&l_2509,&g_525,&l_2522[1],(void*)0,&l_2522[1]},{&l_2777,(void*)0,&g_118,&g_525,&g_118,(void*)0,&l_2777,(void*)0,&g_118,&l_2777},{&g_525,(void*)0,(void*)0,&g_118,(void*)0,&g_118,&g_118,&g_799,(void*)0,(void*)0},{&g_118,&g_118,&l_2522[0],&l_2522[0],(void*)0,&g_525,&g_118,&g_118,&g_525,(void*)0}};
            float **l_2814 = &l_2813[3][0];
            int32_t l_2821 = 1L;
            int32_t l_2858[5][1][1] = {{{0xDF698ED7L}},{{0x84DC6D1CL}},{{0xDF698ED7L}},{{0x84DC6D1CL}},{{0xDF698ED7L}}};
            uint8_t * const l_2863 = &g_1516;
            uint32_t l_2896 = 1UL;
            uint8_t *l_2920 = &g_1770;
            int64_t ***l_2925[2][9][5] = {{{&g_235,&g_235,(void*)0,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,(void*)0,&g_235,&g_235},{&g_235,&g_235,(void*)0,&g_235,&g_235},{(void*)0,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235}},{{&g_235,&g_235,(void*)0,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,(void*)0,&g_235,&g_235},{(void*)0,&g_235,&g_235,&g_235,&g_235},{&g_235,&g_235,&g_235,&g_235,&g_235}}};
            int32_t *l_2964 = &g_79;
            int i, j, k;
        }
        l_2984 = ((*g_798) == ((*l_2838) = (l_2980 <= (((*l_1313) = (g_118 = g_1585.f0)) > (safe_div_func_float_f_f(g_799, (g_525 = g_155)))))));
    }
    else
    { /* block id: 1274 */
        uint64_t l_2998 = 0xD6B62C11FE4038B3LL;
        int32_t l_3001 = (-5L);
        uint32_t * const l_3002 = (void*)0;
        float l_3020[3];
        struct S0 ***l_3024 = &g_2659;
        int8_t l_3037 = 0xCBL;
        int16_t l_3040[1];
        int32_t l_3041[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
        int32_t l_3072 = 0x02F6B390L;
        uint16_t *l_3145 = &g_1349[4][0];
        int i;
        for (i = 0; i < 3; i++)
            l_3020[i] = 0x0.Fp-1;
        for (i = 0; i < 1; i++)
            l_3040[i] = 0x9ACAL;
lbl_3023:
        for (g_1691 = (-8); (g_1691 < 21); ++g_1691)
        { /* block id: 1277 */
            uint32_t l_2991 = 0UL;
            uint16_t *l_2992[8];
            int32_t l_2993 = 0x4E3E913AL;
            int32_t l_2994 = 1L;
            int32_t l_2996 = 0x8DB434C3L;
            int32_t l_2997[7] = {0xB0ADC066L,0L,0xB0ADC066L,0xB0ADC066L,0L,0xB0ADC066L,0xB0ADC066L};
            int32_t ***l_3007 = &g_612;
            int i;
            for (i = 0; i < 8; i++)
                l_2992[i] = &g_2018[1][0][2];
            (*g_1029) &= (((((*g_92) == ((((*g_2708) & (((safe_add_func_uint32_t_u_u(0xFDD057E0L, ((*g_1831) = ((((((l_2991 <= (0x48L <= ((((l_2998--) | l_3001) < ((((void*)0 != l_3002) | (((safe_mul_func_uint8_t_u_u(((1L < 0UL) ^ g_113.f3), (*g_2708))) >= l_3001) , 0xB89F901D875EF126LL)) > l_2996)) | l_3001))) > l_3001) & l_2994) , (*g_61)) < (**g_1593)) , l_2993)))) & l_3001) >= 0x27L)) , (-6L)) , (void*)0)) && l_3001) >= l_3001) && (*****g_1590));
            if (l_2993)
                goto lbl_3023;
            (*g_2781) = (0x0A8CD63F4A6C57B4LL < (((((safe_rshift_func_uint16_t_u_s((l_3007 != (*g_1078)), ((safe_rshift_func_uint8_t_u_u((((safe_unary_minus_func_int64_t_s(((**g_234) == ((*l_1313) , (***g_962))))) & ((0L != (safe_add_func_uint16_t_u_u((l_2997[0] && (safe_sub_func_int64_t_s_s((safe_lshift_func_int8_t_s_u(((((safe_div_func_uint8_t_u_u((+(l_3001 == (*g_1831))), (*l_1314))) , l_3021) && (**g_1766)) , (*g_2708)), 0)), l_3022))), l_2998))) == 0x753849888F6DAB46LL)) != g_2055[2][2][5]), 7)) && 0x94L))) , (*g_2420)) ^ (**g_1830)) <= (*g_2420)) , l_2998));
            if ((*l_2415))
                continue;
        }
        (*g_2781) ^= 9L;
        l_3041[5] ^= (((-10L) != (((((void*)0 != l_3024) | (((safe_add_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(0xF8F3L, (safe_div_func_int32_t_s_s(1L, ((*l_1314) && (((safe_sub_func_uint32_t_u_u((**g_1830), (*g_1831))) && ((safe_mul_func_uint8_t_u_u(((*l_1313) = ((safe_rshift_func_uint8_t_u_u((((((((g_3038 = ((l_3037 ^ (**g_1830)) , g_3038)) == (void*)0) , (**g_2658)) != (*g_316)) && l_3040[0]) <= g_95) , (**g_1766)), 0)) & l_3040[0])), l_3001)) , l_2998)) || l_3040[0])))))), l_3001)) , (*g_1830)) == l_3002)) <= l_3040[0]) || (*l_2415))) ^ 8UL);
        if ((safe_mul_func_int8_t_s_s(1L, ((((**l_2951) = (**l_2951)) == (void*)0) , (safe_lshift_func_int16_t_s_s(((((l_3040[0] , (safe_lshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_u((safe_lshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((*g_2708), (((((safe_mod_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s((*l_1313), 4)), (safe_add_func_int32_t_s_s((((safe_div_func_uint64_t_u_u((((l_3040[0] || (!(l_3001 , (safe_sub_func_int64_t_s_s(((*l_1313) != (*l_2415)), l_3001))))) | 6UL) ^ 0UL), (***g_234))) , 0xD75EL) >= (****g_1591)), 0x4014F9C2L)))) , (*****g_1590)) != 0x32FAL) , l_3002) == (void*)0))), (*l_2415))), 6)), 12))) > (*l_2415)) , l_3037) == (*g_1831)), 7))))))
        { /* block id: 1290 */
            int16_t l_3089[4];
            int32_t l_3123 = 1L;
            int i;
            for (i = 0; i < 4; i++)
                l_3089[i] = 0x9A45L;
            for (g_1404 = 0; (g_1404 < 2); g_1404 = safe_add_func_int64_t_s_s(g_1404, 1))
            { /* block id: 1293 */
                const uint32_t l_3073[6][1] = {{0xF7218104L},{0xF7218104L},{0xF7218104L},{0xF7218104L},{0xF7218104L},{0xF7218104L}};
                int32_t l_3077 = (-1L);
                int32_t l_3092 = 0xD6AF9349L;
                int32_t l_3094 = 0x80A3D25EL;
                int16_t *l_3120 = (void*)0;
                int i, j;
                for (l_2797 = 26; (l_2797 >= (-4)); l_2797 = safe_sub_func_uint64_t_u_u(l_2797, 1))
                { /* block id: 1296 */
                    int64_t l_3071 = 0x6EF01F10FB6755CCLL;
                    int32_t l_3091[1][10][3] = {{{0x7A3857AFL,1L,0xB0D5AE47L},{(-2L),(-1L),(-2L)},{(-2L),0x7A3857AFL,(-1L)},{0x7A3857AFL,(-2L),(-2L)},{(-1L),(-2L),0xB0D5AE47L},{1L,0x7A3857AFL,0x0D1FE82BL},{(-1L),(-1L),0x0D1FE82BL},{0x7A3857AFL,1L,0xB0D5AE47L},{(-2L),(-1L),(-2L)},{(-2L),0x7A3857AFL,(-1L)}}};
                    int i, j, k;
                    for (g_95 = (-9); (g_95 == (-25)); g_95 = safe_sub_func_int32_t_s_s(g_95, 1))
                    { /* block id: 1299 */
                        (*l_2786) = (*l_1313);
                        l_3072 = l_3071;
                        return l_3073[0][0];
                    }
                    if (l_3073[3][0])
                    { /* block id: 1304 */
                        uint64_t *l_3076 = (void*)0;
                        const int32_t l_3080 = (-1L);
                        int32_t l_3093 = 0L;
                        uint16_t l_3096 = 65533UL;
                        l_3089[1] |= (safe_sub_func_uint64_t_u_u((l_3077 = (**g_2609)), ((safe_mul_func_uint16_t_u_u(l_3073[0][0], ((l_3001 ^= (l_3080 < (l_3041[5] = ((**g_1830) &= (*l_1313))))) & 0x9DL))) || ((l_3080 == ((safe_lshift_func_uint8_t_u_s(l_3073[4][0], 5)) <= l_3040[0])) & ((safe_mod_func_int64_t_s_s((((safe_rshift_func_uint16_t_u_s(l_3080, 7)) != 0x734FD4619093E3BALL) | 0x033D9D850E3FF6A1LL), (-4L))) & l_3001)))));
                        (*g_3090) = &g_2541;
                        if ((**g_1103))
                            continue;
                        l_3096--;
                    }
                    else
                    { /* block id: 1313 */
                        uint32_t *l_3122 = (void*)0;
                        int32_t l_3124 = 0x79AF8087L;
                        l_3091[0][6][1] = (safe_mul_func_uint8_t_u_u((safe_add_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_s((((safe_mul_func_int16_t_s_s((***g_1592), ((safe_lshift_func_uint8_t_u_u((**g_1766), 4)) & 0x89FB64E3EC5CE6BFLL))) && (safe_mod_func_uint32_t_u_u(((+((safe_mod_func_int32_t_s_s(((*l_2415) = l_3091[0][6][1]), 1L)) ^ (safe_sub_func_uint16_t_u_u((0xB0BD9912FDBB56AALL > (safe_mul_func_uint8_t_u_u((**g_1766), (((safe_div_func_uint8_t_u_u(((((l_3120 = (**g_1592)) != ((l_3123 ^= ((+((l_3091[0][6][1] >= 0L) == l_3071)) < (***g_963))) , (void*)0)) , l_3124) && (**g_1593)), (*l_1313))) == l_3077) , 8UL)))), 0UL)))) || (****g_1591)), l_3041[5]))) >= (*g_2781)), l_3091[0][0][2])) > 0xB508C1B59A6844B7LL), (*g_61))), 1UL));
                        (*g_2781) &= l_3089[1];
                    }
                }
                for (g_73 = 0; g_73 < 6; g_73 += 1)
                {
                    g_1768[g_73] = 0xDAL;
                }
            }
        }
        else
        { /* block id: 1323 */
            uint32_t l_3144 = 4294967294UL;
            (**g_2658) = (((safe_sub_func_int32_t_s_s((&g_1765[3][3][5] != &l_2871), (safe_sub_func_uint32_t_u_u(0UL, (l_3040[0] >= ((((safe_rshift_func_uint16_t_u_s(l_3072, 15)) && 0UL) , (*g_1831)) <= (safe_rshift_func_uint16_t_u_u((*l_2415), 6)))))))) == (safe_sub_func_uint16_t_u_u(l_3041[3], 0x00B3L))) , (void*)0);
            --l_3135;
            for (g_116 = 0; (g_116 < 29); g_116++)
            { /* block id: 1328 */
                if ((*g_3))
                    break;
            }
            (*l_2415) = ((**g_2609) , (safe_mul_func_int8_t_s_s((safe_sub_func_int8_t_s_s((*l_1314), l_3144)), (l_3145 != (*g_503)))));
        }
    }
    return (**g_1593);
}


/* ------------------------------------------ */
/* 
 * reads : g_1029 g_669
 * writes: g_669
 */
static uint8_t  func_7(int64_t  p_8, int64_t  p_9)
{ /* block id: 1024 */
    (*g_1029) &= 0x9C03D596L;
    return p_9;
}


/* ------------------------------------------ */
/* 
 * reads : g_2055 g_1029 g_669
 * writes: g_2055
 */
static int32_t  func_10(int32_t  p_11, int32_t * p_12, uint16_t  p_13, int32_t * p_14, int32_t * p_15)
{ /* block id: 1020 */
    (*p_15) = (*p_14);
    return (*g_1029);
}


/* ------------------------------------------ */
/* 
 * reads : g_342 g_432 g_2129 g_1029 g_669 g_1830 g_1831 g_155 g_1516 g_88 g_584 g_1593 g_61 g_62 g_234 g_235 g_236 g_113.f0 g_2182 g_1590 g_1591 g_525 g_962 g_963 g_116 g_1770 g_1592 g_715 g_585 g_2231 g_185 g_40 g_464 g_612 g_798 g_799 g_1077 g_1078 g_1079 g_221 g_411 g_2125 g_2126 g_2127 g_1148 g_1311 g_317 g_113 g_4 g_896 g_56 g_2124 g_1691 g_2405 g_2408 g_2042 g_2041
 * writes: g_896 g_432 g_2129 g_669 g_1516 g_715 g_116 g_1404 g_525 g_1623 g_95 g_40 g_62 g_221 g_279 g_309 g_93 g_342 g_155 g_1826 g_962 g_799 g_1770 g_2405 g_2042
 */
static int32_t * func_17(const struct S0  p_18, float  p_19, int32_t * p_20, uint8_t  p_21)
{ /* block id: 495 */
    uint32_t *l_1320 = &g_155;
    uint64_t *l_1321 = &g_896;
    int16_t * const *l_1325 = &g_61;
    int16_t * const **l_1324[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_1326 = 4L;
    int32_t l_1350 = (-5L);
    int32_t l_1355 = 1L;
    int32_t l_1356 = 0L;
    int32_t l_1358 = (-2L);
    int32_t l_1359 = 0L;
    int32_t l_1362 = 1L;
    int32_t l_1363 = 0L;
    int32_t l_1364 = 1L;
    int32_t l_1365[1];
    int32_t l_1366 = (-1L);
    int32_t * const *l_1432 = &g_1029;
    int32_t * const **l_1431[6][10][4] = {{{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,(void*)0},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,(void*)0},{&l_1432,&l_1432,&l_1432,(void*)0},{&l_1432,&l_1432,&l_1432,&l_1432}},{{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,(void*)0}},{{&l_1432,&l_1432,(void*)0,(void*)0},{(void*)0,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{(void*)0,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{(void*)0,(void*)0,&l_1432,&l_1432},{(void*)0,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,(void*)0},{(void*)0,&l_1432,&l_1432,&l_1432}},{{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,&l_1432},{(void*)0,&l_1432,(void*)0,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432}},{{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,(void*)0},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432},{(void*)0,&l_1432,&l_1432,(void*)0},{&l_1432,&l_1432,(void*)0,&l_1432}},{{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432},{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,(void*)0,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432},{&l_1432,(void*)0,&l_1432,(void*)0},{&l_1432,&l_1432,(void*)0,&l_1432},{&l_1432,&l_1432,&l_1432,&l_1432}}};
    int8_t l_1499 = 0x5CL;
    int16_t *l_1519 = &g_62;
    int32_t *l_1624[5] = {&l_1365[0],&l_1365[0],&l_1365[0],&l_1365[0],&l_1365[0]};
    const uint32_t l_1638 = 18446744073709551615UL;
    int64_t ****l_1639[8] = {&g_234,&g_234,&g_234,&g_234,&g_234,&g_234,&g_234,&g_234};
    int64_t * const ** const l_1641 = (void*)0;
    int64_t * const ** const *l_1640 = &l_1641;
    uint8_t *l_1679 = &g_1516;
    uint8_t **l_1678[3];
    uint16_t **l_1702 = &g_88[0];
    uint32_t * const **l_1832 = &g_1830;
    const uint8_t l_1869 = 0xB1L;
    struct S0 l_1915 = {501,37185,4,10471,1301};
    const int16_t l_1975[8][8] = {{0x5A54L,(-7L),0xBFE2L,(-7L),0x5A54L,0x7D13L,1L,(-4L)},{0x7D13L,0L,0x5A54L,0xE8C2L,0x4CC2L,0xD01CL,(-7L),(-7L)},{1L,(-5L),0x5A54L,0x5A54L,(-5L),1L,1L,0x4CC2L},{0x4CC2L,0x1FFDL,0xBFE2L,(-4L),(-7L),0xE8C2L,1L,0x7D13L},{0xBFE2L,(-6L),0x7D13L,(-4L),0x7D13L,(-6L),0xBFE2L,0x4CC2L},{(-5L),0x7D13L,0L,0x5A54L,0xE8C2L,0x4CC2L,0xD01CL,(-7L)},{(-4L),0L,0xF4DFL,0xE8C2L,0xE8C2L,0xF4DFL,0L,(-4L)},{(-5L),(-4L),(-6L),(-7L),0x7D13L,0L,0L,0x1FFDL}};
    int64_t l_1995 = 0x00A0771AA248D7DBLL;
    uint64_t l_2000 = 0x4483CDB507960C6BLL;
    int8_t l_2005 = 0L;
    int16_t l_2017 = 0L;
    int64_t *****l_2039 = &l_1639[4];
    uint8_t l_2104 = 1UL;
    int64_t ** const ***l_2117 = &g_962;
    float l_2128 = (-0x1.3p-1);
    int8_t *l_2131 = &l_2005;
    int8_t **l_2130[1];
    int32_t l_2132 = 0x6D150EB8L;
    uint64_t ***l_2139 = &g_584[0];
    int32_t l_2141 = 0xC7B641BAL;
    uint64_t l_2142[1];
    uint32_t l_2195 = 0x8A025993L;
    uint32_t l_2197[7];
    int32_t *l_2205 = (void*)0;
    int32_t l_2229[4] = {0xACA3D234L,0xACA3D234L,0xACA3D234L,0xACA3D234L};
    int64_t l_2230[10] = {0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL,0x694979DCB1B0985FLL};
    int16_t l_2280 = 0L;
    int64_t l_2305 = (-1L);
    const int8_t l_2316 = (-1L);
    int64_t l_2317 = 0xC2306FBF4255ED36LL;
    const struct S0 * const *l_2319 = (void*)0;
    int8_t l_2357 = 0x78L;
    int64_t l_2372 = (-1L);
    int32_t l_2406 = 0xA83E5F9AL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1365[i] = 0L;
    for (i = 0; i < 3; i++)
        l_1678[i] = &l_1679;
    for (i = 0; i < 1; i++)
        l_2130[i] = &l_2131;
    for (i = 0; i < 1; i++)
        l_2142[i] = 0x846250C50EEEE7CDLL;
    for (i = 0; i < 7; i++)
        l_2197[i] = 0x4FDC908BL;
    l_1326 ^= (safe_div_func_int16_t_s_s((((*l_1321) = (p_20 == l_1320)) > (safe_lshift_func_int16_t_s_u(p_21, ((void*)0 == l_1324[2])))), (4294967293UL && g_342)));
    for (g_432 = (-28); (g_432 != (-9)); ++g_432)
    { /* block id: 500 */
        int8_t l_1347 = 0x0DL;
        uint16_t *l_1348[9][1][4] = {{{(void*)0,(void*)0,(void*)0,&g_73}},{{&g_1349[0][0],&g_1349[2][0],&g_1349[0][0],(void*)0}},{{&g_1349[0][0],(void*)0,(void*)0,&g_1349[0][0]}},{{(void*)0,(void*)0,&g_73,(void*)0}},{{(void*)0,&g_1349[2][0],&g_73,&g_73}},{{(void*)0,(void*)0,(void*)0,&g_73}},{{&g_1349[0][0],&g_1349[2][0],&g_1349[0][0],(void*)0}},{{&g_1349[0][0],(void*)0,(void*)0,&g_1349[0][0]}},{{(void*)0,(void*)0,&g_73,(void*)0}}};
        int32_t l_1351 = 1L;
        int32_t l_1352 = 0xC0203932L;
        int8_t l_1353 = (-10L);
        int32_t l_1354 = 1L;
        int32_t l_1357 = 0xE6ACC9B4L;
        int32_t l_1360 = 0L;
        int32_t l_1361[3][6][2] = {{{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L}},{{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L}},{{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L}}};
        uint32_t l_1367 = 0xFFAFBCE7L;
        uint64_t l_1370 = 0xACE3193A194C80DALL;
        int8_t l_1371 = 1L;
        uint8_t *l_1372 = &g_279;
        uint32_t * const *l_1380[10][2][10] = {{{&l_1320,(void*)0,(void*)0,(void*)0,(void*)0,&l_1320,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320}},{{&l_1320,(void*)0,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0},{(void*)0,&l_1320,&l_1320,(void*)0,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320}},{{&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320},{&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320}},{{&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,(void*)0},{(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320}},{{&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320},{&l_1320,(void*)0,&l_1320,(void*)0,(void*)0,(void*)0,&l_1320,(void*)0,&l_1320,&l_1320}},{{&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0},{&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320}},{{(void*)0,(void*)0,&l_1320,(void*)0,(void*)0,&l_1320,&l_1320,&l_1320,(void*)0,(void*)0},{&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,(void*)0}},{{&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320},{&l_1320,(void*)0,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0}},{{(void*)0,&l_1320,&l_1320,(void*)0,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320},{&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320}},{{&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320,&l_1320},{&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,&l_1320,(void*)0,&l_1320,&l_1320}}};
        float l_1476 = 0x0.7p-1;
        int64_t l_1491 = 0x7F1CA4BC041DAE50LL;
        int32_t * const l_1520 = &l_1352;
        uint32_t l_1566[3][1][2];
        struct S0 l_1576 = {88,1155,0,13147,705};
        int16_t l_1578 = 0x1E9AL;
        int64_t ****l_1582 = &g_234;
        struct S0 **l_1662 = &g_317;
        uint32_t l_1708[5][4][6] = {{{0xE0C94C1AL,0x2D21BC60L,4294967295UL,0x07FF7D2AL,0x34A1E04FL,4294967291UL},{4294967291UL,6UL,4294967295UL,6UL,4294967291UL,0x2D21BC60L},{4294967291UL,0xE0C94C1AL,6UL,0x07FF7D2AL,1UL,1UL},{0xE0C94C1AL,0x34A1E04FL,0x34A1E04FL,0xE0C94C1AL,4294967295UL,1UL}},{{4294967295UL,1UL,6UL,0x2D21BC60L,0x07FF7D2AL,0x2D21BC60L},{4294967295UL,0UL,4294967295UL,4294967295UL,0x07FF7D2AL,4294967291UL},{6UL,1UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0x34A1E04FL,0x34A1E04FL,0xE0C94C1AL,4294967295UL,1UL,4294967295UL}},{{6UL,0xE0C94C1AL,4294967291UL,4294967295UL,4294967291UL,0xE0C94C1AL},{4294967295UL,6UL,4294967291UL,0x2D21BC60L,0x34A1E04FL,4294967295UL},{4294967295UL,0x2D21BC60L,0xE0C94C1AL,0xE0C94C1AL,0x2D21BC60L,4294967295UL},{0xE0C94C1AL,0x2D21BC60L,4294967295UL,0x07FF7D2AL,0x34A1E04FL,4294967291UL}},{{4294967291UL,6UL,4294967295UL,6UL,4294967291UL,0x2D21BC60L},{4294967291UL,0xE0C94C1AL,6UL,0x07FF7D2AL,1UL,1UL},{0xE0C94C1AL,0x34A1E04FL,0x34A1E04FL,0xE0C94C1AL,4294967295UL,1UL},{4294967295UL,1UL,6UL,0x2D21BC60L,0x07FF7D2AL,0x2D21BC60L}},{{4294967295UL,0UL,4294967295UL,4294967295UL,0x07FF7D2AL,4294967291UL},{6UL,1UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL},{0x34A1E04FL,0x34A1E04FL,0xE0C94C1AL,4294967295UL,1UL,4294967295UL},{6UL,0xE0C94C1AL,4294967291UL,4294967295UL,4294967291UL,0xE0C94C1AL}}};
        uint8_t ***l_1714 = (void*)0;
        uint8_t ****l_1713 = &l_1714;
        uint64_t l_1771[9] = {4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL};
        int32_t ***l_1833[5] = {&g_612,&g_612,&g_612,&g_612,&g_612};
        int64_t l_1848 = 2L;
        uint32_t l_1871 = 0x2098354EL;
        uint8_t l_1876 = 0x6CL;
        const int64_t l_1914[5][4][9] = {{{4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL,4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL,4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL},{0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL,0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL,0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL},{4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL,4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL,4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL},{0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL,0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL,0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL}},{{4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL,4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL,4L,0xD53C1B56EE8F166DLL,0xD53C1B56EE8F166DLL},{0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL,0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL,0x63004E1FB8348AEBLL,0x2781FB5C4B86E228LL,0x2781FB5C4B86E228LL},{4L,0xD53C1B56EE8F166DLL,1L,0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L},{0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L}},{{0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L},{0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L},{0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L},{0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L}},{{0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L},{0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L},{0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L},{0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L}},{{0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L},{0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L},{0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L,0xD53C1B56EE8F166DLL,1L,1L},{0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L,0x2781FB5C4B86E228LL,0L,0L}}};
        int64_t l_1927 = 0x14DEB7E0EB9B5BB0LL;
        int64_t ***l_1936 = (void*)0;
        int8_t *l_1979 = &l_1353;
        int8_t **l_1978[3];
        int16_t **l_1987 = &l_1519;
        uint64_t l_2022[7][5][7] = {{{0x039363F19E6A340ELL,0xF3007B62D9E77BBBLL,0UL,0UL,0xF3007B62D9E77BBBLL,0x039363F19E6A340ELL,2UL},{0x17683F3235E242DDLL,9UL,0x453459F525679FBALL,18446744073709551615UL,0xD7D66600926418FDLL,0xC3A6E2D3D9942A0CLL,0x6BB02BF7C89381C5LL},{0x039363F19E6A340ELL,0xF3007B62D9E77BBBLL,0UL,0UL,0xF3007B62D9E77BBBLL,0x039363F19E6A340ELL,2UL},{0x17683F3235E242DDLL,9UL,0x453459F525679FBALL,18446744073709551615UL,0xD7D66600926418FDLL,0xC3A6E2D3D9942A0CLL,0x6BB02BF7C89381C5LL},{0x039363F19E6A340ELL,0xF3007B62D9E77BBBLL,0UL,0UL,0xF3007B62D9E77BBBLL,0x039363F19E6A340ELL,2UL}},{{0x17683F3235E242DDLL,9UL,0x453459F525679FBALL,18446744073709551615UL,0xD7D66600926418FDLL,0xC3A6E2D3D9942A0CLL,0x6BB02BF7C89381C5LL},{0x039363F19E6A340ELL,0xF3007B62D9E77BBBLL,0UL,0UL,0xF3007B62D9E77BBBLL,0x039363F19E6A340ELL,2UL},{0x17683F3235E242DDLL,9UL,0x453459F525679FBALL,18446744073709551615UL,0xD7D66600926418FDLL,0xC3A6E2D3D9942A0CLL,0x6BB02BF7C89381C5LL},{0x039363F19E6A340ELL,0xF3007B62D9E77BBBLL,0UL,0UL,0xF3007B62D9E77BBBLL,0x039363F19E6A340ELL,2UL},{0x17683F3235E242DDLL,9UL,0x453459F525679FBALL,18446744073709551615UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL}},{{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL}},{{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL}},{{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL}},{{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL}},{{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL},{7UL,18446744073709551615UL,0x6BB02BF7C89381C5LL,4UL,0x453459F525679FBALL,0xC79F541B0D69795ELL,0xFAE055DA2209843DLL},{0x29188D9748CB18A6LL,0UL,2UL,2UL,0UL,0x29188D9748CB18A6LL,2UL}}};
        int16_t ***l_2054 = &g_1593;
        int16_t ****l_2053 = &l_2054;
        int16_t *****l_2052[6][4][8] = {{{&l_2053,&l_2053,(void*)0,(void*)0,&l_2053,&l_2053,&l_2053,&l_2053},{&l_2053,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,(void*)0},{&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053}},{{&l_2053,(void*)0,&l_2053,(void*)0,&l_2053,(void*)0,&l_2053,(void*)0},{&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,(void*)0},{&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,&l_2053}},{{(void*)0,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,(void*)0},{&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,(void*)0,&l_2053,(void*)0,&l_2053,(void*)0}},{{&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,(void*)0},{&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,&l_2053},{(void*)0,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,(void*)0}},{{&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,(void*)0,&l_2053,(void*)0,&l_2053,(void*)0},{&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,(void*)0}},{{&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,&l_2053,&l_2053},{&l_2053,(void*)0,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,&l_2053},{(void*)0,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053,(void*)0},{&l_2053,&l_2053,&l_2053,&l_2053,&l_2053,(void*)0,&l_2053,&l_2053}}};
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 2; k++)
                    l_1566[i][j][k] = 0UL;
            }
        }
        for (i = 0; i < 3; i++)
            l_1978[i] = &l_1979;
    }
    if (((g_2129 = g_2129) != l_2130[0]))
    { /* block id: 892 */
        uint32_t l_2138 = 0xBEAA1EDCL;
        uint64_t ****l_2140 = &l_2139;
        int32_t *l_2151 = (void*)0;
        int32_t *l_2156 = &g_40;
        int8_t **l_2171 = &l_2131;
        int32_t l_2198[1];
        int32_t l_2285[9];
        int i;
        for (i = 0; i < 1; i++)
            l_2198[i] = 0x3F5E3F86L;
        for (i = 0; i < 9; i++)
            l_2285[i] = 1L;
        (**l_1432) |= l_2132;
lbl_2281:
        l_2142[0] |= (((safe_rshift_func_int16_t_s_s(p_18.f0, 12)) && ((safe_unary_minus_func_int16_t_s(((safe_sub_func_uint32_t_u_u((l_2138 , (((((*l_2140) = l_2139) == (void*)0) || (l_2138 || l_2141)) > p_18.f4)), (*g_1029))) <= 0x4AB0L))) , 0x517A65E2L)) < (**g_1830));
        if ((safe_sub_func_int8_t_s_s((-1L), ((p_21 > ((*l_1679)--)) > (safe_div_func_uint8_t_u_u(253UL, (~p_21)))))))
        { /* block id: 897 */
            int32_t *l_2150[10] = {&l_1363,&g_95,&l_1363,&g_95,&l_1363,&g_95,&l_1363,&g_95,&l_1363,&g_95};
            int i;
            return l_2151;
        }
        else
        { /* block id: 899 */
            uint64_t **l_2170[2];
            uint32_t l_2172 = 0UL;
            int32_t l_2173[4][8] = {{1L,1L,1L,0x369350C0L,(-4L),0L,1L,1L},{1L,0xF6FDFC4DL,(-1L),0x9D02FAEDL,0xF6FDFC4DL,1L,0xF6FDFC4DL,0x9D02FAEDL},{0L,0xF6FDFC4DL,0L,1L,1L,0L,(-4L),0x369350C0L},{1L,1L,0xEBB4D12FL,1L,1L,1L,1L,0xEBB4D12FL}};
            const uint16_t **l_2194 = (void*)0;
            const uint16_t ***l_2193[4];
            const uint16_t *** const *l_2192 = &l_2193[2];
            struct S0 **l_2238 = &g_317;
            struct S0 ***l_2237[5][7] = {{&l_2238,&l_2238,&l_2238,&l_2238,&l_2238,&l_2238,&l_2238},{&l_2238,(void*)0,(void*)0,(void*)0,&l_2238,&l_2238,(void*)0},{&l_2238,&l_2238,&l_2238,&l_2238,&l_2238,&l_2238,&l_2238},{(void*)0,&l_2238,(void*)0,(void*)0,&l_2238,(void*)0,&l_2238},{&l_2238,&l_2238,&l_2238,&l_2238,&l_2238,&l_2238,&l_2238}};
            struct S0 ****l_2236 = &l_2237[4][0];
            struct S0 *****l_2235[8][5][6] = {{{(void*)0,&l_2236,&l_2236,&l_2236,(void*)0,(void*)0},{&l_2236,&l_2236,&l_2236,&l_2236,&l_2236,(void*)0},{&l_2236,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236},{(void*)0,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236},{&l_2236,&l_2236,(void*)0,&l_2236,&l_2236,&l_2236}},{{&l_2236,&l_2236,&l_2236,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236},{&l_2236,&l_2236,(void*)0,&l_2236,&l_2236,(void*)0},{&l_2236,&l_2236,(void*)0,(void*)0,&l_2236,&l_2236},{&l_2236,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236}},{{&l_2236,&l_2236,&l_2236,(void*)0,(void*)0,&l_2236},{(void*)0,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236},{(void*)0,(void*)0,&l_2236,&l_2236,&l_2236,&l_2236},{(void*)0,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236},{&l_2236,&l_2236,&l_2236,(void*)0,(void*)0,&l_2236}},{{&l_2236,&l_2236,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2236,(void*)0,&l_2236,(void*)0,&l_2236},{&l_2236,(void*)0,&l_2236,(void*)0,&l_2236,(void*)0},{&l_2236,(void*)0,&l_2236,(void*)0,(void*)0,&l_2236},{&l_2236,(void*)0,(void*)0,&l_2236,&l_2236,&l_2236}},{{(void*)0,&l_2236,&l_2236,(void*)0,&l_2236,&l_2236},{&l_2236,&l_2236,&l_2236,(void*)0,&l_2236,&l_2236},{&l_2236,&l_2236,&l_2236,&l_2236,(void*)0,(void*)0},{&l_2236,&l_2236,(void*)0,(void*)0,&l_2236,&l_2236},{&l_2236,&l_2236,(void*)0,(void*)0,&l_2236,&l_2236}},{{&l_2236,&l_2236,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2236,&l_2236,&l_2236,&l_2236,&l_2236,(void*)0},{&l_2236,&l_2236,&l_2236,&l_2236,(void*)0,&l_2236},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_2236},{&l_2236,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236}},{{(void*)0,(void*)0,&l_2236,&l_2236,&l_2236,&l_2236},{(void*)0,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236},{&l_2236,&l_2236,(void*)0,(void*)0,&l_2236,&l_2236},{&l_2236,&l_2236,&l_2236,&l_2236,&l_2236,(void*)0},{(void*)0,(void*)0,&l_2236,&l_2236,(void*)0,(void*)0}},{{&l_2236,(void*)0,(void*)0,&l_2236,&l_2236,&l_2236},{&l_2236,&l_2236,(void*)0,(void*)0,&l_2236,&l_2236},{&l_2236,&l_2236,(void*)0,&l_2236,&l_2236,(void*)0},{&l_2236,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236},{(void*)0,&l_2236,&l_2236,&l_2236,&l_2236,&l_2236}}};
            struct S0 * const *l_2242 = (void*)0;
            struct S0 * const **l_2241 = &l_2242;
            struct S0 * const ***l_2240 = &l_2241;
            struct S0 * const ****l_2239 = &l_2240;
            int16_t l_2251 = 0x9256L;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_2170[i] = &g_585[2][2][0];
            for (i = 0; i < 4; i++)
                l_2193[i] = &l_2194;
            for (l_1499 = 0; l_1499 < 4; l_1499 += 1)
            {
                for (g_432 = 0; g_432 < 6; g_432 += 1)
                {
                    for (l_2000 = 0; l_2000 < 1; l_2000 += 1)
                    {
                        g_715[l_1499][g_432][l_2000] = 0xD3F4L;
                    }
                }
            }
            l_2173[2][4] ^= ((((safe_unary_minus_func_uint8_t_u((safe_mod_func_int16_t_s_s(((p_21 , ((*g_1029) = ((~((l_2156 = (void*)0) == ((safe_div_func_uint16_t_u_u((~p_18.f4), (safe_div_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(0xC2L, (safe_lshift_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s(((p_18.f4 > (safe_rshift_func_uint16_t_u_u((((*l_1702) == (((((***g_234) = ((((l_2170[1] == (**l_2140)) , l_2171) == g_2129) && (**g_1593))) , p_18.f0) <= 3UL) , (*l_1702))) <= p_18.f0), l_2172))) | l_2172), l_2172)) < g_113.f0), 3)))), l_2172)))) , p_20))) && 8UL))) != p_18.f2), p_21)))) | l_2172) > 0UL) && p_21);
            for (l_1363 = 2; (l_1363 >= 1); --l_1363)
            { /* block id: 907 */
                int32_t l_2181 = (-1L);
                int8_t **l_2188 = &l_2131;
                int8_t **l_2189 = &l_2131;
                int8_t l_2196 = (-1L);
                for (g_1404 = 0; (g_1404 < (-3)); g_1404--)
                { /* block id: 910 */
                    uint16_t l_2178 = 65534UL;
                    int32_t *l_2183 = (void*)0;
                    for (g_669 = 0; (g_669 >= 0); g_669 -= 1)
                    { /* block id: 913 */
                        l_2178++;
                        (*g_2182) = l_2181;
                        g_1623 = (void*)0;
                    }
                    return l_2183;
                }
                l_2198[0] &= (l_2197[3] = ((safe_mul_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u((((((((((l_2189 = (l_2188 = (void*)0)) != l_2171) == (((*g_1590) != &g_1142[4]) , (l_2173[2][4] , (-0x1.2p+1)))) , ((((((((l_2172 > (((safe_sub_func_uint32_t_u_u(p_18.f3, (p_18.f2 != 0x31D65C57L))) && p_18.f1) >= 1UL)) ^ p_18.f4) & 1L) , &g_502[0][0][4]) != l_2192) != l_2195) >= (*g_2182)) , &g_584[3])) == (*l_2140)) >= (**l_1432)) < (****g_962)) < (-5L)) ^ l_2196), 0)), p_18.f1)) < l_2196));
            }
            for (g_95 = (-25); (g_95 > 26); g_95 = safe_add_func_uint32_t_u_u(g_95, 5))
            { /* block id: 927 */
                uint32_t l_2227 = 0x422D24ACL;
                int16_t *l_2247[1];
                int32_t l_2248 = 5L;
                int i;
                for (i = 0; i < 1; i++)
                    l_2247[i] = &g_62;
                for (l_2172 = 11; (l_2172 > 57); l_2172 = safe_add_func_int16_t_s_s(l_2172, 5))
                { /* block id: 930 */
                    float l_2207 = 0x2.7E3E6Dp-87;
                    int32_t l_2250 = 1L;
                    for (g_40 = 0; (g_40 <= (-5)); g_40--)
                    { /* block id: 933 */
                        int32_t *l_2206 = &l_1363;
                        uint32_t *l_2228 = &l_2197[3];
                        int32_t l_2232 = 0x92424469L;
                        uint32_t *l_2249 = &l_2138;
                        int32_t l_2254[4];
                        const int64_t * const l_2259 = &g_342;
                        const int64_t * const *l_2258 = &l_2259;
                        const int64_t * const * const *l_2257 = &l_2258;
                        const int64_t * const * const **l_2256 = &l_2257;
                        const int64_t * const * const ***l_2255 = &l_2256;
                        uint64_t l_2262 = 0xAEEFBAE6BA0D8F51LL;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_2254[i] = 1L;
                        l_2206 = l_2205;
                        l_2232 ^= (((*l_2131) = (((*l_1679) = ((0x39AB0C0BL && ((254UL >= g_1770) && ((((0UL ^ (+p_18.f2)) < ((safe_mul_func_uint8_t_u_u((l_2172 , 254UL), ((((((safe_lshift_func_uint16_t_u_s((safe_mul_func_uint16_t_u_u(((safe_sub_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u((safe_div_func_int16_t_s_s(0xFE7BL, (safe_lshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_u((((***l_2139) = ((g_715[1][3][0] |= ((***g_1592) = (((((((**l_1432) |= (((*l_2228) = ((safe_rshift_func_int16_t_s_u((***g_1592), 14)) && l_2227)) , l_2227)) == 0x4F5FL) , l_2229[3]) && l_2227) | p_21) , 4L))) ^ 0xD2CAL)) ^ 18446744073709551607UL), l_2227)), p_18.f1)))), 13)), 0L)) ^ 0x7DFBDDCB5EBD3B7CLL), p_18.f1)), l_2230[7])) || 65534UL) == 0x619BL) & p_18.f0) < p_18.f2) == p_18.f1))) <= 0x4DC80609L)) ^ 9L) & g_2231))) , l_2172)) < l_2173[2][4])) <= p_18.f1);
                        (**l_1432) = ((((((((p_18.f0 <= ((((**l_2171) = (safe_div_func_uint64_t_u_u(((0x9695L != (((p_18.f0 & (l_2235[3][2][0] != (l_2239 = (void*)0))) && (safe_sub_func_int16_t_s_s((((((((safe_add_func_uint16_t_u_u(((void*)0 != l_2247[0]), (((*g_1831) | ((*l_2249) &= (l_2248 = p_21))) || l_2250))) ^ p_18.f2) < l_2251) , (void*)0) == (void*)0) <= 9L) < l_2227), p_18.f3))) < p_18.f0)) < p_18.f4), 5L))) , 0x2677F0ADL) || 0x033FFB02L)) , p_18.f0) <= (****g_1591)) && 65530UL) >= p_18.f4) , 1UL) ^ 1UL) | 1L);
                        (*g_1029) = ((-1L) ^ ((((((safe_div_func_int32_t_s_s(l_2250, l_2250)) >= l_2232) != l_2254[1]) & ((l_2255 != (void*)0) || ((safe_sub_func_uint16_t_u_u(65535UL, ((***g_963) < l_2262))) >= (-5L)))) < l_2173[2][4]) <= 0xD732F0C697CCC234LL));
                    }
                }
                (****g_1077) = func_24(p_18.f2, l_2227);
            }
        }
        if ((!((p_18.f4 & (((((((safe_add_func_uint64_t_u_u(0x0B4551D6868DB944LL, ((**g_235) = ((safe_mod_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s(p_18.f1, 4)), (p_21 , (safe_lshift_func_int16_t_s_s((0xF9DCD163L != ((*g_1029) ^= p_18.f3)), ((((p_18.f1 , (safe_lshift_func_uint8_t_u_s(((safe_sub_func_uint32_t_u_u((((safe_rshift_func_int8_t_s_s((p_21 & (safe_rshift_func_uint16_t_u_s((p_18.f1 , p_18.f0), (*****g_1590)))), p_18.f1)) || p_18.f3) ^ p_18.f2), p_18.f3)) >= p_18.f3), 2))) <= 0x8078B896CBFC902FLL) , p_18.f4) , 0L)))))) && 1UL)))) <= l_2280) <= p_21) >= p_18.f2) > p_18.f4) & p_18.f3) & p_18.f0)) , p_18.f3)))
        { /* block id: 956 */
            if (p_18.f4)
                goto lbl_2281;
        }
        else
        { /* block id: 958 */
            int8_t l_2284 = 0L;
            int32_t l_2318 = 8L;
            int32_t *l_2337 = (void*)0;
            int32_t *l_2338 = &g_1404;
            int32_t *l_2339 = &g_1826[3];
            int32_t l_2346 = 5L;
            int32_t ***l_2347 = &g_612;
            l_2318 &= (safe_div_func_uint8_t_u_u((l_2284 == (l_2285[4] > (~((((safe_mul_func_int8_t_s_s(((-1L) && (safe_mul_func_uint16_t_u_u(((safe_add_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s(((((safe_rshift_func_int8_t_s_u(((p_18 , (((****l_2140)++) > (safe_mul_func_int16_t_s_s((((safe_sub_func_uint16_t_u_u(l_2284, ((l_2305 = g_411) | ((safe_div_func_int32_t_s_s(((((safe_lshift_func_int8_t_s_s(1L, 1)) >= (((safe_mul_func_int16_t_s_s((safe_rshift_func_int16_t_s_u(p_18.f2, 0)), (safe_rshift_func_uint8_t_u_s(6UL, l_2284)))) | p_18.f0) , 0xF0B4L)) >= (-1L)) <= p_18.f4), 0x2C0651DEL)) != l_2284)))) > l_2284) <= (**l_1432)), p_18.f3)))) , (-1L)), (**l_1432))) && 6UL) , 1UL) && 0L), p_18.f1)) , p_18.f2), l_2284)) || 0x5379D352L), (***g_1592)))), p_18.f0)) && l_2316) ^ p_18.f0) ^ l_2317)))), 2UL));
lbl_2348:
            l_2319 = (**g_2125);
            if ((((safe_mod_func_uint64_t_u_u((safe_div_func_int8_t_s_s((~((***l_2139)++)), (((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(l_2318, ((((*g_61) = (safe_div_func_int64_t_s_s((!0x5476669896BE9DE3LL), ((((*l_2339) = ((*l_2338) = (+((*l_1320)--)))) , (&l_2284 == ((*l_2171) = (*l_2171)))) , l_2284)))) | (l_2346 = ((safe_lshift_func_uint8_t_u_u((((safe_div_func_int16_t_s_s(0L, p_18.f3)) & ((*g_236) > ((255UL || l_2318) > p_18.f1))) || 5UL), p_18.f3)) > 0x0B3AL))) > 0x6166C1E4EE88E049LL))), p_18.f4)) <= (-1L)) & p_18.f4))), l_2284)) , (void*)0) == l_2347))
            { /* block id: 970 */
                if (p_18.f4)
                    goto lbl_2348;
            }
            else
            { /* block id: 972 */
                (*l_2117) = (*l_2117);
                return p_20;
            }
        }
    }
    else
    { /* block id: 977 */
        float *l_2349[2][3][5] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_799,&g_118,&g_799,&g_118,&g_799},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_2128,&g_118,&l_2128,&g_118,&l_2128},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_799,&g_118,&g_799,&g_118,&g_799}}};
        int32_t l_2352 = 0x482C53F3L;
        uint8_t l_2369 = 0x1FL;
        const int64_t *l_2397 = &l_2317;
        const int64_t * const * const l_2396[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        const int64_t * const * const *l_2395 = &l_2396[4];
        int16_t **l_2410[1];
        struct S0 *l_2412 = &g_1585;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2410[i] = &l_1519;
        (*g_1148) = p_18.f4;
        if (((((safe_add_func_float_f_f(0x1.8p-1, (-0x10.3p+1))) > (g_342 , ((l_2352 , (safe_div_func_float_f_f((safe_add_func_float_f_f(((l_2357 ^= (****g_962)) , (*g_1311)), p_18.f2)), (safe_sub_func_float_f_f((((-((safe_mul_func_float_f_f((safe_mul_func_float_f_f((!((*g_317) , (safe_sub_func_float_f_f((g_799 = (+p_19)), l_2352)))), l_2369)), g_4)) > 0x9.F4AFBBp-90)) != 0x7.47F85Bp+19) > 0x3.1365AFp-92), 0xD.05B849p-23))))) != p_21))) < l_2369) , p_18.f3))
        { /* block id: 981 */
            int32_t l_2370 = 0x29DEEC05L;
            uint32_t l_2371 = 0UL;
            int8_t l_2388 = 8L;
            int64_t ***l_2394[3];
            int32_t l_2409 = 0xCD52AF35L;
            int i;
            for (i = 0; i < 3; i++)
                l_2394[i] = &g_235;
            for (g_896 = 0; (g_896 <= 8); g_896 += 1)
            { /* block id: 984 */
                int64_t ***l_2393 = (void*)0;
                uint64_t ****l_2407 = &l_2139;
                int i;
                for (l_2017 = 7; (l_2017 >= 2); l_2017 -= 1)
                { /* block id: 987 */
                    int32_t l_2373 = 0x11530782L;
                    int i;
                    l_2371 |= (g_56[g_896] != l_2370);
                    for (g_1770 = 0; (g_1770 <= 1); g_1770 += 1)
                    { /* block id: 991 */
                        int64_t l_2374 = 1L;
                        int32_t l_2375 = 0x0FB63792L;
                        l_2373 &= l_2372;
                        l_2375 = ((**l_1432) &= (l_2374 >= (1L & p_18.f0)));
                        l_2388 |= (((l_2374 == (safe_add_func_uint64_t_u_u(((void*)0 != &g_1766), (safe_mul_func_uint8_t_u_u(0xF6L, (safe_lshift_func_int8_t_s_s(((safe_sub_func_uint64_t_u_u(((**g_2124) == &l_2319), p_18.f1)) != (safe_add_func_int32_t_s_s((safe_lshift_func_uint8_t_u_s(p_18.f0, 2)), (((*g_317) , g_1691) < g_56[g_896])))), 3))))))) >= 1UL) || 0xB7L);
                    }
                }
                l_2409 |= (g_56[g_896] >= ((safe_add_func_int64_t_s_s(((safe_mul_func_uint8_t_u_u(((l_2393 = (l_2394[0] = l_2393)) == l_2395), l_2371)) > 7L), (+((**l_1432) = (safe_add_func_uint32_t_u_u(((safe_mul_func_uint8_t_u_u(p_21, (((****g_1591) = (safe_lshift_func_int8_t_s_u(((*l_2131) |= (((l_2406 = (l_2370 |= (g_2405[0][0][0] |= 0x00E2L))) >= (***g_1592)) , ((l_2407 != (void*)0) >= l_2369))), p_18.f1))) < p_18.f2))) > l_2369), 0xEEA0BC42L)))))) == g_2408[0][1]));
                for (l_2357 = 1; (l_2357 >= 0); l_2357 -= 1)
                { /* block id: 1009 */
                    (*g_1029) = p_18.f3;
                }
            }
        }
        else
        { /* block id: 1013 */
            int16_t **l_2411 = &l_1519;
            struct S0 **l_2414 = &l_2412;
            (*g_1029) &= (l_2410[0] != l_2411);
            (*l_2414) = l_2412;
        }
        (*g_2041) = (*g_2127);
    }
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads : g_896 g_612
 * writes: g_896 g_93
 */
static uint16_t  func_22(int32_t * p_23)
{ /* block id: 400 */
    uint32_t l_1032 = 0UL;
    int32_t l_1058 = 0xD7217D80L;
    int32_t l_1059 = (-6L);
    int32_t l_1060 = 0xE6D82236L;
    int32_t l_1124[3];
    int32_t l_1163 = 0xAE0BE666L;
    uint32_t l_1170 = 0xAB5002D2L;
    int16_t **l_1175 = &g_61;
    uint64_t *l_1180 = &g_896;
    float l_1199 = 0xC.3700A0p-27;
    int64_t l_1200[10] = {0x859BBAB38EA9A95CLL,(-1L),(-1L),0x859BBAB38EA9A95CLL,0x79C9710862A6BB8ELL,0x859BBAB38EA9A95CLL,(-1L),(-1L),0x859BBAB38EA9A95CLL,0x79C9710862A6BB8ELL};
    uint32_t *l_1226 = &l_1170;
    int16_t *l_1235 = &g_715[0][4][0];
    float l_1245 = 0x5.017794p-69;
    uint16_t l_1277 = 2UL;
    int8_t *l_1285 = (void*)0;
    int i;
    for (i = 0; i < 3; i++)
        l_1124[i] = 0x6606A352L;
    for (g_896 = (-23); (g_896 == 47); g_896 = safe_add_func_uint16_t_u_u(g_896, 1))
    { /* block id: 403 */
        uint32_t l_1033 = 0UL;
        int32_t *l_1034 = &g_4;
        uint64_t l_1040 = 0x0E13699C358940BFLL;
        int32_t l_1119 = (-2L);
        int32_t l_1120 = 0xF76A39A9L;
        int32_t l_1121 = (-2L);
        int32_t l_1122 = 0x16F71B6DL;
        int32_t l_1123 = 0L;
        int32_t l_1125 = (-1L);
        int32_t l_1126 = 0xCE6EE211L;
        int32_t l_1128 = (-8L);
        int32_t l_1131 = 1L;
        int32_t l_1132 = (-1L);
        int8_t l_1133[4][6] = {{(-7L),0xA2L,(-7L),(-7L),0xA2L,(-7L)},{(-7L),0xA2L,(-7L),(-7L),0xA2L,(-7L)},{(-7L),0xA2L,(-7L),(-7L),0xA2L,(-7L)},{(-7L),0xA2L,(-7L),(-7L),0xA2L,(-7L)}};
        int32_t l_1134[4];
        int32_t l_1169[8] = {0xC1C736F5L,0xEECF3B6BL,0xC1C736F5L,1L,0xC1C736F5L,1L,1L,0xC1C736F5L};
        int64_t l_1194 = 0x5C1DEAE28F362FA8LL;
        int16_t *l_1223 = &g_56[4];
        int32_t l_1227 = 0x57AD6B7EL;
        struct S0 l_1282 = {9,33756,0,8420,2516};
        int i, j;
        for (i = 0; i < 4; i++)
            l_1134[i] = 0x3C7810CEL;
        l_1033 |= l_1032;
        (*g_612) = l_1034;
    }
    return l_1058;
}


/* ------------------------------------------ */
/* 
 * reads : g_279 g_185 g_40 g_464 g_342 g_612 g_798 g_799 g_669 g_1029
 * writes: g_279 g_221 g_309 g_93 g_342 g_40 g_669
 */
static int32_t * func_24(int32_t  p_25, const int8_t  p_26)
{ /* block id: 181 */
    uint8_t l_451 = 0UL;
    int32_t l_457[1][10] = {{9L,9L,9L,9L,9L,9L,9L,9L,9L,9L}};
    uint16_t l_460 = 0x82FDL;
    uint32_t l_463 = 0xFAF3B4D2L;
    float *l_497 = &g_118;
    uint16_t * const *l_505 = &g_88[3];
    struct S0 **l_520 = &g_317;
    int32_t l_582 = 0x0EEBC126L;
    int16_t l_613 = 0x4CC1L;
    int32_t * const l_668 = &g_669;
    int32_t * const *l_667 = &l_668;
    int32_t * const **l_666 = &l_667;
    int32_t *l_686 = &g_40;
    int32_t l_714 = (-1L);
    uint16_t l_716 = 0x6560L;
    int16_t l_747[5];
    int64_t l_855[6][1] = {{1L},{0x3C86F8C13EA91B9ELL},{0x3C86F8C13EA91B9ELL},{1L},{0x3C86F8C13EA91B9ELL},{0x3C86F8C13EA91B9ELL}};
    uint32_t l_944[5] = {0UL,0UL,0UL,0UL,0UL};
    int64_t ****l_960 = (void*)0;
    int32_t l_965 = 0xB6251A0BL;
    int8_t l_1007 = 1L;
    const uint16_t **l_1017 = (void*)0;
    uint16_t **l_1025[1];
    uint16_t ***l_1024 = &l_1025[0];
    int32_t *l_1026 = &l_582;
    int32_t *l_1027[6][5][7] = {{{&g_4,&l_582,&l_582,&l_714,&g_4,&g_40,&g_95},{&g_4,&l_714,&l_457[0][7],&g_40,&l_457[0][5],&g_40,&g_4},{(void*)0,&g_40,&g_669,&g_40,&l_582,&g_669,&g_95},{&l_457[0][5],&g_4,&l_457[0][2],&g_40,&g_4,(void*)0,(void*)0},{&g_669,&l_457[0][5],&g_669,&g_40,(void*)0,&g_95,&g_40}},{{&g_40,&g_79,&g_79,&l_714,&g_4,&l_582,&l_457[0][5]},{&l_714,(void*)0,&l_582,&l_457[0][5],&g_95,&l_457[0][7],&g_79},{&g_40,(void*)0,(void*)0,&l_582,&l_457[0][9],&l_582,(void*)0},{&g_40,&g_40,&g_95,&g_95,&g_79,(void*)0,&l_457[0][5]},{&g_4,&g_4,&l_582,&l_457[0][5],&g_4,&g_669,&l_714}},{{&g_40,&l_457[0][7],&g_4,(void*)0,&g_79,&g_4,(void*)0},{&g_4,&g_669,&g_669,&g_79,&l_457[0][9],&l_457[0][0],&g_95},{&l_714,&g_4,&g_669,&g_40,&g_95,(void*)0,&g_79},{&g_669,&l_457[0][5],&g_4,(void*)0,&g_4,&g_40,&g_669},{(void*)0,&l_457[0][9],&l_582,&g_4,(void*)0,(void*)0,&l_457[0][7]}},{{&g_669,(void*)0,&g_40,&g_4,&g_4,(void*)0,(void*)0},{&g_95,&g_4,(void*)0,&l_582,&l_582,(void*)0,&l_457[0][7]},{&g_4,&g_95,&l_582,&g_4,&l_457[0][5],(void*)0,&l_582},{&g_95,(void*)0,&g_40,&g_4,&g_4,&g_40,&l_714},{&l_457[0][1],(void*)0,&g_669,(void*)0,&l_714,(void*)0,(void*)0}},{{&g_40,&l_714,&l_457[0][5],&l_582,&l_714,&l_457[0][0],&l_714},{&g_4,&g_95,&g_95,&g_4,&l_582,&g_4,(void*)0},{&g_40,(void*)0,&l_457[0][7],&l_457[0][1],&g_4,&g_669,&g_40},{&g_40,&g_4,(void*)0,(void*)0,&l_457[0][5],&g_95,&l_582},{&l_457[0][7],&g_4,(void*)0,&g_4,&g_95,&g_669,&g_4}},{{(void*)0,(void*)0,&g_95,&l_582,&g_40,&l_457[0][4],(void*)0},{&l_582,&g_40,&g_40,(void*)0,&l_457[0][5],&l_457[0][9],&l_582},{&g_4,&g_669,&l_457[0][0],&g_4,&l_457[0][5],(void*)0,&l_457[0][9]},{&l_714,&l_582,&l_457[0][5],&g_4,&g_669,(void*)0,&l_457[0][4]},{&l_457[0][1],&g_4,(void*)0,&l_457[0][4],&g_4,&g_4,&l_457[0][4]}}};
    int32_t *l_1028 = &l_457[0][0];
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_747[i] = (-1L);
    for (i = 0; i < 1; i++)
        l_1025[i] = (void*)0;
    for (g_279 = (-12); (g_279 <= 15); g_279 = safe_add_func_uint32_t_u_u(g_279, 1))
    { /* block id: 184 */
        int32_t l_458[5];
        float *l_467 = &g_118;
        uint64_t *l_486 = &g_221;
        uint64_t **l_485 = &l_486;
        int16_t *l_493 = &g_212[0];
        int64_t ***l_494 = (void*)0;
        int32_t l_495 = 0x52D9A9B5L;
        float *l_496 = &g_118;
        int16_t l_581 = (-2L);
        int32_t **l_586 = (void*)0;
        int8_t l_650 = 2L;
        int32_t * const *l_665 = &g_93[0][2][0];
        int32_t * const **l_664[4][10][6] = {{{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,(void*)0,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,(void*)0},{&l_665,&l_665,&l_665,&l_665,(void*)0,&l_665},{&l_665,&l_665,(void*)0,&l_665,&l_665,(void*)0},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{(void*)0,&l_665,&l_665,&l_665,&l_665,&l_665}},{{&l_665,&l_665,&l_665,(void*)0,&l_665,(void*)0},{(void*)0,&l_665,(void*)0,&l_665,&l_665,&l_665},{&l_665,(void*)0,&l_665,&l_665,(void*)0,(void*)0},{(void*)0,&l_665,&l_665,(void*)0,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{(void*)0,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,(void*)0,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,(void*)0,(void*)0},{&l_665,(void*)0,&l_665,&l_665,&l_665,(void*)0},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665}},{{&l_665,&l_665,(void*)0,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,(void*)0},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{(void*)0,&l_665,&l_665,&l_665,&l_665,&l_665},{(void*)0,(void*)0,&l_665,&l_665,&l_665,(void*)0},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,(void*)0,(void*)0,(void*)0},{&l_665,(void*)0,&l_665,&l_665,&l_665,&l_665}},{{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,(void*)0,&l_665,&l_665},{&l_665,&l_665,(void*)0,&l_665,&l_665,&l_665},{&l_665,&l_665,&l_665,&l_665,&l_665,&l_665},{(void*)0,&l_665,(void*)0,&l_665,&l_665,&l_665},{(void*)0,&l_665,&l_665,&l_665,&l_665,&l_665},{&l_665,&l_665,(void*)0,(void*)0,&l_665,&l_665},{&l_665,(void*)0,&l_665,&l_665,(void*)0,&l_665}}};
        int64_t l_684[7] = {(-9L),(-1L),(-1L),(-9L),(-1L),(-1L),(-9L)};
        uint32_t l_687 = 0UL;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_458[i] = 0xE5446FACL;
        for (g_221 = (-3); (g_221 > 6); ++g_221)
        { /* block id: 187 */
            int32_t *l_440 = &g_40;
            int32_t *l_441 = &g_95;
            int32_t *l_442 = &g_79;
            int32_t *l_443 = (void*)0;
            int32_t *l_444 = (void*)0;
            int32_t *l_445 = &g_40;
            int32_t *l_446 = &g_95;
            int32_t *l_447 = &g_40;
            int32_t *l_448 = &g_40;
            int32_t *l_449 = &g_40;
            int32_t *l_450[1];
            int i;
            for (i = 0; i < 1; i++)
                l_450[i] = &g_95;
            ++l_451;
            for (g_309 = 0; (g_309 > 0); g_309 = safe_add_func_int8_t_s_s(g_309, 6))
            { /* block id: 191 */
                int32_t *l_456 = &g_95;
                int32_t l_459[1][9][6] = {{{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L},{0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L,0x2FFEEB47L,0x43F397CFL,0x2FFEEB47L}}};
                int i, j, k;
                (*g_185) = l_456;
                --l_460;
            }
            l_463 &= (*l_448);
            (*g_464) = &l_458[3];
        }
    }
    for (g_342 = 4; (g_342 >= 0); g_342 -= 1)
    { /* block id: 267 */
        int32_t l_733 = 0x391B883FL;
        uint64_t *l_734 = &g_221;
        uint8_t *l_735 = &g_279;
        int32_t l_778 = 1L;
        int32_t l_779[9][3][9] = {{{(-7L),4L,0xE16265F2L,0x5E85E53FL,8L,7L,0x54EF1818L,(-5L),0xE1BF295BL},{1L,0x1B00B65EL,(-1L),0x4A3357E6L,8L,0xF43147C5L,5L,0L,0L},{(-8L),(-2L),0xE1BF295BL,0L,0xE1BF295BL,(-2L),(-8L),0x3E181785L,6L}},{{(-1L),0xF9024C27L,(-1L),0xF43147C5L,0xC3D6B511L,(-1L),0x1B00B65EL,(-4L),0x89AC62C6L},{(-10L),0x8DE16758L,0xC0B915FCL,6L,1L,(-5L),0xA335AED5L,1L,0xE16265F2L},{0xCD0DAB41L,0x4DF31925L,5L,(-1L),1L,0L,0x89AC62C6L,0x1B00B65EL,0x071E0559L}},{{0L,1L,0L,0L,0x214BE26CL,1L,(-8L),(-8L),0xE4CE77ECL},{0xEF7E6776L,0x4BB38A69L,7L,5L,9L,0xEF7E6776L,0xD5047977L,0x4DF31925L,(-2L)},{0xF42D676DL,7L,0xE16265F2L,(-4L),(-4L),0xE16265F2L,7L,0xF42D676DL,0x8DE16758L}},{{(-7L),7L,(-1L),0x16897EB2L,0x8AF97889L,0x4F4F187EL,(-4L),(-1L),0x6893019DL},{0x26C5D285L,0xCD9EB861L,0xEE70858DL,0x8DE16758L,0x5E85E53FL,0xE3F99B52L,0x47C73FC3L,0L,0x8DE16758L},{(-1L),0xA4C2347EL,1L,0x1B00B65EL,0x6EF12F0AL,(-1L),0xB3A99711L,0L,(-2L)}},{{0x54EF1818L,4L,(-4L),4L,0x20566E4BL,(-7L),0x5E85E53FL,0x47C73FC3L,0xE4CE77ECL},{0x8322A853L,0x8A1BBAA0L,(-7L),(-1L),(-1L),0x6893019DL,1L,0x16897EB2L,0x071E0559L},{0xE16265F2L,1L,0xCD9EB861L,0xB548DBE4L,4L,0xB548DBE4L,0xCD9EB861L,1L,0xE16265F2L}},{{0x64403B18L,9L,0L,(-4L),0xCD0DAB41L,0xA8B5136EL,0x4DF31925L,0xF9024C27L,0x89AC62C6L},{0xA335AED5L,1L,0L,1L,7L,0x47C73FC3L,(-7L),0xE4CE77ECL,0x51471120L},{0x64403B18L,0xCD0DAB41L,0x4DF31925L,5L,(-1L),1L,0L,0x89AC62C6L,0x1B00B65EL}},{{0xE16265F2L,(-2L),(-10L),0L,0x8DE16758L,0xE16265F2L,4L,0L,0x20566E4BL},{0x8322A853L,0x071E0559L,0x89AC62C6L,0L,1L,(-1L),0x071E0559L,(-1L),(-3L)},{0x54EF1818L,0L,4L,1L,0xE1BF295BL,0xE1BF295BL,1L,4L,0L}},{{(-1L),1L,(-2L),0L,(-3L),(-1L),0x0095EE0BL,(-8L),0L},{0x26C5D285L,0x3E181785L,(-4L),0xE4CE77ECL,1L,0x54EF1818L,1L,0xC0B915FCL,0xEE70858DL},{(-7L),1L,0L,(-1L),(-1L),(-3L),(-3L),0x4A3357E6L,(-1L)}},{{0xF42D676DL,0L,0x20566E4BL,(-7L),0xEAEFCFACL,(-10L),0xCD9EB861L,1L,(-6L)},{0xEF7E6776L,(-7L),0xA4C2347EL,0x6893019DL,0x071E0559L,7L,4L,2L,0L},{0L,0xA47451ADL,0x214BE26CL,7L,0xE3F99B52L,0xE16265F2L,0xE3F99B52L,7L,0x214BE26CL}}};
        int32_t * const ***l_810 = &l_666;
        int64_t **l_837[7][2] = {{&g_236,&g_236},{&g_236,&g_236},{&g_236,&g_236},{&g_236,&g_236},{&g_236,&g_236},{&g_236,&g_236},{&g_236,&g_236}};
        uint32_t l_840 = 18446744073709551610UL;
        int8_t l_847 = (-2L);
        int32_t *l_946 = &l_714;
        int16_t l_978 = 7L;
        int64_t ****l_984 = &g_234;
        int i, j, k;
    }
    for (g_342 = 0; (g_342 < (-18)); --g_342)
    { /* block id: 393 */
        int16_t *l_1006 = (void*)0;
        int32_t l_1016 = (-1L);
        const uint16_t ***l_1018 = (void*)0;
        const uint16_t ***l_1019 = &l_1017;
        (*g_612) = &p_25;
        (*l_686) &= (safe_mul_func_uint8_t_u_u(((safe_div_func_float_f_f(((((((p_26 == 0xC.A3050Ap+95) < (*g_798)) != (safe_add_func_float_f_f((safe_sub_func_float_f_f(l_1016, (p_26 <= ((p_25 < (((&g_88[1] != ((*l_1019) = l_1017)) , (safe_mul_func_float_f_f((safe_sub_func_float_f_f((&l_1017 == l_1024), 0xB.FAE1B9p-95)), g_669))) > p_26)) >= p_26)))), 0x6.F5F46Bp-19))) <= 0xE.DFD215p-58) < 0xB.FF51A2p+4) != l_1016), l_1016)) , 0xEBL), p_26));
        (**l_667) = (-1L);
    }
    return g_1029;
}


/* ------------------------------------------ */
/* 
 * reads : g_79
 * writes:
 */
static uint32_t  func_27(uint32_t  p_28, int32_t * p_29, uint64_t  p_30, int8_t  p_31)
{ /* block id: 178 */
    int8_t l_418 = 1L;
    int32_t *l_419 = (void*)0;
    int32_t l_420 = (-1L);
    int32_t *l_421 = &g_40;
    int32_t *l_422 = &g_95;
    int32_t *l_423 = &g_79;
    int32_t l_424 = 0xF87A171AL;
    int32_t *l_425 = (void*)0;
    int32_t *l_426 = (void*)0;
    int32_t *l_427 = &l_424;
    int32_t *l_428 = &g_40;
    int32_t *l_429 = &l_420;
    int32_t *l_430[10] = {&l_424,&l_424,&l_424,&l_424,&l_424,&l_424,&l_424,&l_424,&l_424,&l_424};
    int32_t l_431 = (-1L);
    uint32_t l_433 = 0xAA98B150L;
    int i;
    l_433--;
    return (*l_423);
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_56 g_61 g_3 g_62 g_73 g_79 g_113.f1 g_234 g_235 g_92 g_93 g_309 g_236 g_116 g_411
 * writes: g_40 g_56 g_73 g_79 g_62 g_212
 */
static uint16_t  func_33(uint16_t  p_34, uint64_t  p_35, uint16_t  p_36, float  p_37)
{ /* block id: 5 */
    uint32_t l_38 = 0xC7DF39F3L;
    int32_t *l_39[10];
    int16_t *l_55 = &g_56[4];
    int32_t l_416[10][7][3] = {{{1L,0x0ADB614FL,0xBACF1E2EL},{0x832B1C84L,0xBACF1E2EL,0xC1BB2893L},{0x9EBAA961L,1L,8L},{1L,7L,0L},{0x0ADB614FL,8L,0xDBEEDF3FL},{(-1L),1L,0xDBEEDF3FL},{(-9L),0x544A887AL,0L}},{{8L,0L,8L},{(-10L),0L,0xC1BB2893L},{0xE45F982FL,1L,0xBACF1E2EL},{0L,1L,0x87ECD723L},{0x732DD5C3L,0x6327F81CL,0xE45F982FL},{1L,0x4ABCE9D9L,8L},{0xD8BA1270L,0L,0L}},{{0x4ABCE9D9L,1L,0x5F51B6DEL},{0L,0x15281CBCL,0x732DD5C3L},{0xF9E5B101L,0x7B6F859AL,(-10L)},{0x93FCAF53L,8L,0L},{3L,0x7B6F859AL,0xD2B96D31L},{1L,0x15281CBCL,0L},{(-7L),1L,0xFCF984D0L}},{{(-10L),0L,0x0ADB614FL},{0xDB97C4DAL,0x4ABCE9D9L,0L},{0x60A48CD3L,0x6327F81CL,1L},{0x5F51B6DEL,1L,0xA8E01941L},{9L,1L,8L},{0L,0L,1L},{0x5C61110AL,0L,(-10L)}},{{1L,0x544A887AL,0x168BC2BDL},{0x15281CBCL,1L,0xE0BCCA20L},{0x15281CBCL,8L,0x5C61110AL},{1L,7L,0x24344784L},{0x5C61110AL,1L,0x789EAF65L},{0L,0xBACF1E2EL,7L},{9L,0x0ADB614FL,(-4L)}},{{0x5F51B6DEL,0xA8E01941L,1L},{0x60A48CD3L,0x12309B60L,0xDB97C4DAL},{0xDB97C4DAL,6L,0x0033338EL},{(-10L),(-10L),(-6L)},{(-7L),0x60A48CD3L,0x6DBBE115L},{1L,1L,0x6327F81CL},{3L,7L,6L}},{{0x93FCAF53L,1L,0x6327F81CL},{0xF9E5B101L,0L,0x6DBBE115L},{0L,8L,(-6L)},{0x4ABCE9D9L,0x732DD5C3L,0x0033338EL},{0xD8BA1270L,0xFCF984D0L,0xDB97C4DAL},{1L,9L,1L},{0x732DD5C3L,(-10L),(-4L)}},{{0L,0xDAD0AC49L,7L},{8L,0L,0x517D6003L},{7L,0x4ABCE9D9L,7L},{0x6327F81CL,(-7L),0x87ECD723L},{1L,0L,0x0ADB614FL},{0xD8BA1270L,0L,0x7FBDF4B2L},{0x5F51B6DEL,(-7L),1L}},{{0x832B1C84L,0x4ABCE9D9L,0x0E0FE302L},{1L,0L,0x6327F81CL},{7L,0x6DBBE115L,0x12309B60L},{6L,1L,0L},{0x7FBDF4B2L,8L,0L},{(-4L),0xDAD0AC49L,0x5F51B6DEL},{0xDAD0AC49L,1L,0xDAD0AC49L}},{{0L,0x6327F81CL,(-9L)},{3L,0x1EFC0FE5L,0xDBEEDF3FL},{0xDB97C4DAL,0x0E0FE302L,1L},{0xE45F982FL,0xC1BB2893L,0xF9E5B101L},{0xDB97C4DAL,0xF710B6FDL,1L},{3L,0x5C61110AL,1L},{0L,0xF9E5B101L,0xD2B96D31L}}};
    uint16_t l_417 = 0UL;
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_39[i] = &g_40;
    l_417 &= (((g_40 = l_38) == (safe_div_func_int8_t_s_s(func_43(&g_4, (g_4 <= ((*g_61) = (safe_sub_func_int64_t_s_s((safe_lshift_func_int16_t_s_s((g_4 && ((safe_rshift_func_int8_t_s_u((safe_sub_func_int16_t_s_s(((*l_55) &= 1L), ((p_34 , func_57(g_61, ((l_55 != (void*)0) , l_39[5]), (*g_3))) != l_39[5]))), g_309)) >= l_416[0][6][1])), 4)), (*g_236))))), p_34), p_34))) ^ (***g_234));
    return p_35;
}


/* ------------------------------------------ */
/* 
 * reads : g_411
 * writes: g_79 g_212
 */
static const int8_t  func_43(int32_t * const  p_44, int8_t  p_45, uint32_t  p_46)
{ /* block id: 173 */
    for (g_79 = 0; g_79 < 6; g_79 += 1)
    {
        g_212[g_79] = 8L;
    }
    return g_411;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_62 g_73 g_61 g_79 g_3 g_113.f1 g_234 g_235 g_92 g_93
 * writes: g_73 g_79
 */
static int32_t * func_57(int16_t * p_58, int32_t * const  p_59, int32_t  p_60)
{ /* block id: 8 */
    const uint32_t l_66[3][3] = {{0xC2B2A8F3L,7UL,0xC2B2A8F3L},{0xC2B2A8F3L,7UL,0xC2B2A8F3L},{0xC2B2A8F3L,7UL,0xC2B2A8F3L}};
    uint16_t *l_72[5] = {&g_73,&g_73,&g_73,&g_73,&g_73};
    int32_t *l_78 = &g_79;
    int32_t l_100 = 0xE610DA34L;
    int32_t l_101 = (-1L);
    int32_t l_102 = 0x8A6F1982L;
    int32_t l_103 = 0L;
    int32_t l_105 = 4L;
    struct S0 *l_112 = &g_113;
    uint32_t l_175 = 0xA5E293B4L;
    int64_t *l_232 = &g_116;
    int64_t **l_231 = &l_232;
    int64_t ***l_230 = &l_231;
    const uint16_t l_240 = 0x08C1L;
    int16_t l_330 = 0x11AFL;
    int i, j;
    (*l_78) ^= ((((((safe_lshift_func_uint16_t_u_s(((!(l_66[0][0] , ((g_4 < (p_60 , (g_62 | (safe_unary_minus_func_uint64_t_u((safe_rshift_func_int8_t_s_u(l_66[0][0], 5))))))) != l_66[0][0]))) || (safe_mul_func_int16_t_s_s(0x506EL, (g_73++)))), (((safe_sub_func_uint8_t_u_u(g_4, g_62)) ^ 1L) , (*g_61)))) & p_60) != p_60) , (void*)0) == &g_4) && 0L);
    for (g_79 = 29; (g_79 > 6); --g_79)
    { /* block id: 13 */
        uint16_t **l_89 = &l_72[2];
        uint16_t *l_90 = &g_73;
        int32_t *l_91 = (void*)0;
        int32_t l_96 = 0L;
        int32_t l_104 = 0x36AE2794L;
        int32_t l_150 = (-7L);
        int32_t l_153 = (-1L);
        int32_t l_154[3];
        int64_t *l_192 = &g_116;
        int64_t **l_191 = &l_192;
        uint64_t l_260 = 18446744073709551615UL;
        uint32_t l_333[10] = {0xC104672EL,18446744073709551615UL,18446744073709551615UL,0xC104672EL,18446744073709551615UL,18446744073709551615UL,0xC104672EL,18446744073709551615UL,18446744073709551615UL,0xC104672EL};
        int i;
        for (i = 0; i < 3; i++)
            l_154[i] = 0x35A5BBF8L;
        p_60 ^= (*g_3);
    }
    (*l_78) |= (safe_lshift_func_int16_t_s_u((g_113.f1 && ((*g_234) == (void*)0)), 11));
    (*l_78) = (*g_3);
    return (*g_92);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_40, "g_40", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_56[i], "g_56[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_62, "g_62", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_79, "g_79", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_106, "g_106", print_hash_value);
    transparent_crc(g_113.f0, "g_113.f0", print_hash_value);
    transparent_crc(g_113.f1, "g_113.f1", print_hash_value);
    transparent_crc(g_113.f2, "g_113.f2", print_hash_value);
    transparent_crc(g_113.f3, "g_113.f3", print_hash_value);
    transparent_crc(g_113.f4, "g_113.f4", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc_bytes (&g_118, sizeof(g_118), "g_118", print_hash_value);
    transparent_crc(g_155, "g_155", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_212[i], "g_212[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_221, "g_221", print_hash_value);
    transparent_crc(g_279, "g_279", print_hash_value);
    transparent_crc(g_309, "g_309", print_hash_value);
    transparent_crc(g_342, "g_342", print_hash_value);
    transparent_crc(g_391, "g_391", print_hash_value);
    transparent_crc(g_411, "g_411", print_hash_value);
    transparent_crc(g_432, "g_432", print_hash_value);
    transparent_crc_bytes (&g_525, sizeof(g_525), "g_525", print_hash_value);
    transparent_crc(g_530, "g_530", print_hash_value);
    transparent_crc(g_669, "g_669", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_715[i][j][k], "g_715[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_799, sizeof(g_799), "g_799", print_hash_value);
    transparent_crc(g_896, "g_896", print_hash_value);
    transparent_crc(g_942, "g_942", print_hash_value);
    transparent_crc(g_1145, "g_1145", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_1349[i][j], "g_1349[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1404, "g_1404", print_hash_value);
    transparent_crc(g_1516, "g_1516", print_hash_value);
    transparent_crc(g_1585.f0, "g_1585.f0", print_hash_value);
    transparent_crc(g_1585.f1, "g_1585.f1", print_hash_value);
    transparent_crc(g_1585.f2, "g_1585.f2", print_hash_value);
    transparent_crc(g_1585.f3, "g_1585.f3", print_hash_value);
    transparent_crc(g_1585.f4, "g_1585.f4", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1594[i], "g_1594[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1659, "g_1659", print_hash_value);
    transparent_crc(g_1669, "g_1669", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1688[i], "g_1688[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1691, "g_1691", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1768[i], "g_1768[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1770, "g_1770", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1826[i], "g_1826[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_2018[i][j][k], "g_2018[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_2055[i][j][k], "g_2055[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2231, "g_2231", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_2405[i][j][k], "g_2405[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2408[i][j], "g_2408[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2417, "g_2417", print_hash_value);
    transparent_crc(g_2456, "g_2456", print_hash_value);
    transparent_crc(g_2462, "g_2462", print_hash_value);
    transparent_crc(g_2651, "g_2651", print_hash_value);
    transparent_crc(g_2711, "g_2711", print_hash_value);
    transparent_crc(g_2716, "g_2716", print_hash_value);
    transparent_crc_bytes (&g_2742, sizeof(g_2742), "g_2742", print_hash_value);
    transparent_crc(g_2758, "g_2758", print_hash_value);
    transparent_crc(g_2831, "g_2831", print_hash_value);
    transparent_crc(g_2941, "g_2941", print_hash_value);
    transparent_crc(g_3095, "g_3095", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 800
   depth: 1, occurrence: 13
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 5
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 2
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 36
breakdown:
   indirect level: 0, occurrence: 13
   indirect level: 1, occurrence: 5
   indirect level: 2, occurrence: 6
   indirect level: 3, occurrence: 7
   indirect level: 4, occurrence: 0
   indirect level: 5, occurrence: 5
XXX full-bitfields structs in the program: 13
breakdown:
   indirect level: 0, occurrence: 13
XXX times a bitfields struct's address is taken: 32
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 23
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 266

XXX max expression depth: 44
breakdown:
   depth: 1, occurrence: 119
   depth: 2, occurrence: 28
   depth: 3, occurrence: 2
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 13, occurrence: 3
   depth: 15, occurrence: 1
   depth: 17, occurrence: 1
   depth: 19, occurrence: 2
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 30, occurrence: 5
   depth: 31, occurrence: 3
   depth: 33, occurrence: 1
   depth: 39, occurrence: 1
   depth: 44, occurrence: 1

XXX total number of pointers: 631

XXX times a variable address is taken: 1876
XXX times a pointer is dereferenced on RHS: 504
breakdown:
   depth: 1, occurrence: 358
   depth: 2, occurrence: 85
   depth: 3, occurrence: 28
   depth: 4, occurrence: 24
   depth: 5, occurrence: 9
XXX times a pointer is dereferenced on LHS: 406
breakdown:
   depth: 1, occurrence: 333
   depth: 2, occurrence: 43
   depth: 3, occurrence: 18
   depth: 4, occurrence: 12
XXX times a pointer is compared with null: 49
XXX times a pointer is compared with address of another variable: 18
XXX times a pointer is compared with another pointer: 16
XXX times a pointer is qualified to be dereferenced: 12064

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2125
   level: 2, occurrence: 656
   level: 3, occurrence: 263
   level: 4, occurrence: 219
   level: 5, occurrence: 70
XXX number of pointers point to pointers: 298
XXX number of pointers point to scalars: 326
XXX number of pointers point to structs: 7
XXX percent of pointers has null in alias set: 28.8
XXX average alias set size: 1.37

XXX times a non-volatile is read: 2711
XXX times a non-volatile is write: 1258
XXX times a volatile is read: 202
XXX    times read thru a pointer: 87
XXX times a volatile is write: 37
XXX    times written thru a pointer: 5
XXX times a volatile is available for access: 2.27e+03
XXX percentage of non-volatile access: 94.3

XXX forward jumps: 1
XXX backward jumps: 12

XXX stmts: 117
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 21
   depth: 2, occurrence: 25
   depth: 3, occurrence: 15
   depth: 4, occurrence: 8
   depth: 5, occurrence: 19

XXX percentage a fresh-made variable is used: 15.1
XXX percentage an existing variable is used: 84.9
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


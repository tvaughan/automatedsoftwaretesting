# Run 06
Compiler with gcc 11 and clang 17, a threshold a 3% relative and 3 absolute diff

## random 0
```
#include "csmith.h"
uint16_t a;
void main() {
  int b;
  crc32_gentab();
  transparent_crc(a, "", b);
}
```

While they both unroll the loop Clang manages to do it more aggressively by going up to 32 iterations in one pass instead of 16 for GCC, this result in issuing only 78% percent of GCC instruction on a test run.

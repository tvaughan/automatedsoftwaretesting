/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2636058240
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   volatile int32_t  f0;
   volatile int32_t  f1;
   volatile int8_t  f2;
   const volatile int32_t  f3;
   uint64_t  f4;
   volatile int64_t  f5;
   uint64_t  f6;
   volatile int32_t  f7;
   uint64_t  f8;
};

struct S3 {
   int32_t  f0;
   const unsigned f1 : 20;
   volatile uint16_t  f2;
   const uint32_t  f3;
   int16_t  f4;
   int64_t  f5;
   volatile int32_t  f6;
   const struct S0  f7;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2 = (-1L);/* VOLATILE GLOBAL g_2 */
static volatile int32_t g_3[5] = {7L,7L,7L,7L,7L};
static volatile int32_t g_4 = 0L;/* VOLATILE GLOBAL g_4 */
static int32_t g_5 = 0x3F30B91EL;
static int32_t g_8 = (-1L);
static struct S3 g_9 = {-4L,191,0x031EL,0x0627E80AL,0xC6DDL,-1L,0x3C5AD058L,{1L,8L,0x0BL,0x2F23D37FL,1UL,-1L,9UL,0x898CBDB6L,4UL}};/* VOLATILE GLOBAL g_9 */
static struct S3 g_18 = {2L,618,0x38DBL,0xC8B24CECL,0xE444L,0L,-8L,{-9L,0x2A26B131L,8L,0xBD65BDF5L,18446744073709551614UL,5L,18446744073709551606UL,1L,0xBD1BD9F019D5C01CLL}};/* VOLATILE GLOBAL g_18 */


/* --- FORWARD DECLARATIONS --- */
static struct S3  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_8 g_9 g_18
 * writes: g_5 g_8 g_9.f5
 */
static struct S3  func_1(void)
{ /* block id: 0 */
    uint64_t l_13 = 0xCA18188558B047C1LL;
    for (g_5 = 18; (g_5 >= (-26)); g_5 = safe_sub_func_int8_t_s_s(g_5, 7))
    { /* block id: 3 */
        int32_t *l_10 = (void*)0;
        int32_t *l_11 = &g_8;
        int32_t *l_12[6][8] = {{&g_8,(void*)0,&g_8,&g_5,(void*)0,&g_8,&g_5,&g_8},{&g_5,&g_5,&g_5,&g_8,&g_8,&g_5,&g_5,&g_5},{(void*)0,&g_8,&g_8,&g_8,&g_8,(void*)0,&g_8,&g_5},{&g_8,(void*)0,&g_8,&g_5,&g_8,(void*)0,&g_8,&g_8},{&g_5,&g_8,&g_8,&g_5,&g_5,&g_5,&g_8,&g_8},{&g_8,(void*)0,(void*)0,(void*)0,(void*)0,&g_5,(void*)0,(void*)0}};
        int i, j;
        for (g_8 = 0; (g_8 <= 4); g_8 += 1)
        { /* block id: 6 */
            return g_9;
        }
        l_13++;
    }
    for (g_8 = 0; (g_8 > (-20)); g_8 = safe_sub_func_int64_t_s_s(g_8, 1))
    { /* block id: 13 */
        for (g_9.f5 = 1; (g_9.f5 <= 4); g_9.f5 += 1)
        { /* block id: 16 */
            return g_9;
        }
    }
    return g_18;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_3[i], "g_3[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_9.f0, "g_9.f0", print_hash_value);
    transparent_crc(g_9.f1, "g_9.f1", print_hash_value);
    transparent_crc(g_9.f2, "g_9.f2", print_hash_value);
    transparent_crc(g_9.f3, "g_9.f3", print_hash_value);
    transparent_crc(g_9.f4, "g_9.f4", print_hash_value);
    transparent_crc(g_9.f5, "g_9.f5", print_hash_value);
    transparent_crc(g_9.f6, "g_9.f6", print_hash_value);
    transparent_crc(g_9.f7.f0, "g_9.f7.f0", print_hash_value);
    transparent_crc(g_9.f7.f1, "g_9.f7.f1", print_hash_value);
    transparent_crc(g_9.f7.f2, "g_9.f7.f2", print_hash_value);
    transparent_crc(g_9.f7.f3, "g_9.f7.f3", print_hash_value);
    transparent_crc(g_9.f7.f4, "g_9.f7.f4", print_hash_value);
    transparent_crc(g_9.f7.f5, "g_9.f7.f5", print_hash_value);
    transparent_crc(g_9.f7.f6, "g_9.f7.f6", print_hash_value);
    transparent_crc(g_9.f7.f7, "g_9.f7.f7", print_hash_value);
    transparent_crc(g_9.f7.f8, "g_9.f7.f8", print_hash_value);
    transparent_crc(g_18.f0, "g_18.f0", print_hash_value);
    transparent_crc(g_18.f1, "g_18.f1", print_hash_value);
    transparent_crc(g_18.f2, "g_18.f2", print_hash_value);
    transparent_crc(g_18.f3, "g_18.f3", print_hash_value);
    transparent_crc(g_18.f4, "g_18.f4", print_hash_value);
    transparent_crc(g_18.f5, "g_18.f5", print_hash_value);
    transparent_crc(g_18.f6, "g_18.f6", print_hash_value);
    transparent_crc(g_18.f7.f0, "g_18.f7.f0", print_hash_value);
    transparent_crc(g_18.f7.f1, "g_18.f7.f1", print_hash_value);
    transparent_crc(g_18.f7.f2, "g_18.f7.f2", print_hash_value);
    transparent_crc(g_18.f7.f3, "g_18.f7.f3", print_hash_value);
    transparent_crc(g_18.f7.f4, "g_18.f7.f4", print_hash_value);
    transparent_crc(g_18.f7.f5, "g_18.f7.f5", print_hash_value);
    transparent_crc(g_18.f7.f6, "g_18.f7.f6", print_hash_value);
    transparent_crc(g_18.f7.f7, "g_18.f7.f7", print_hash_value);
    transparent_crc(g_18.f7.f8, "g_18.f7.f8", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 2
breakdown:
   depth: 0, occurrence: 1
   depth: 1, occurrence: 0
   depth: 2, occurrence: 2
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 2
breakdown:
   indirect level: 0, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 3
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 2
breakdown:
   depth: 1, occurrence: 5
   depth: 2, occurrence: 4

XXX total number of pointers: 3

XXX times a variable address is taken: 11
XXX times a pointer is dereferenced on RHS: 0
breakdown:
XXX times a pointer is dereferenced on LHS: 0
breakdown:
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 41
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 3
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 66.7
XXX average alias set size: 1.67

XXX times a non-volatile is read: 7
XXX times a non-volatile is write: 5
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 49
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 8
XXX max block depth: 2
breakdown:
   depth: 0, occurrence: 3
   depth: 1, occurrence: 3
   depth: 2, occurrence: 2

XXX percentage a fresh-made variable is used: 25
XXX percentage an existing variable is used: 75
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


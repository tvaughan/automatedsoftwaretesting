/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1357293960
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   float  f0;
   signed f1 : 31;
   uint32_t  f2;
   const uint16_t  f3;
   volatile uint64_t  f4;
};
#pragma pack(pop)

union U1 {
   volatile int16_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static float g_6 = 0x6.3C4078p+36;
static volatile int16_t g_7 = 6L;/* VOLATILE GLOBAL g_7 */
static uint8_t g_18 = 0x79L;
static int8_t g_21 = 0xBEL;
static int32_t g_50 = 0xC0F1FA6FL;
static uint64_t g_58 = 0UL;
static int32_t g_74 = 0x3CC114FFL;
static uint8_t g_79 = 0UL;
static uint8_t *g_78 = &g_79;
static uint64_t g_86 = 8UL;
static int64_t g_92[9][2] = {{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL},{0x2888A0E7B2E1D86ELL,0x2888A0E7B2E1D86ELL}};
static int16_t g_93 = 0L;
static uint16_t g_99 = 0xE981L;
static int32_t g_121[8][8] = {{9L,0xB2F8DC5AL,9L,(-3L),0xB2F8DC5AL,0x12900EB0L,0x12900EB0L,0xB2F8DC5AL},{0xB2F8DC5AL,0x12900EB0L,0x12900EB0L,0xB2F8DC5AL,(-3L),9L,0xB2F8DC5AL,9L},{0xB2F8DC5AL,0x8431F483L,0x5C77F51BL,0x8431F483L,0xB2F8DC5AL,0x5C77F51BL,0x83246266L,0x83246266L},{9L,0x8431F483L,(-3L),(-3L),(-3L),0x10A37157L,0x5C77F51BL,(-3L)},{9L,0x5C77F51BL,0xE529FE0DL,9L,0xE529FE0DL,0x5C77F51BL,9L,0x10A37157L},{(-3L),0x12900EB0L,0x83246266L,9L,9L,0x83246266L,0x12900EB0L,(-3L)},{0x10A37157L,9L,0x5C77F51BL,0xE529FE0DL,9L,0xE529FE0DL,0x5C77F51BL,9L},{(-3L),0x5C77F51BL,0x10A37157L,(-3L),0xE529FE0DL,0xE529FE0DL,(-3L),0x10A37157L}};
static int8_t g_160[7] = {0x57L,0x57L,0x57L,0x57L,0x57L,0x57L,0x57L};
static int32_t *g_171 = &g_121[0][1];
static uint64_t g_172 = 18446744073709551613UL;
static float g_225 = 0xA.D60D6Bp-82;
static uint8_t ****g_247 = (void*)0;
static uint8_t ****g_248 = (void*)0;
static uint8_t ***g_255 = (void*)0;
static uint8_t *** const *g_254 = &g_255;
static uint8_t *** const **g_253 = &g_254;
static uint32_t g_289 = 18446744073709551614UL;
static int64_t g_330 = (-1L);
static int16_t g_332 = 1L;
static int64_t *g_361 = &g_330;
static int64_t **g_360 = &g_361;
static int64_t ***g_359 = &g_360;
static float g_397 = 0x1.F02209p+91;
static int16_t g_399[10] = {0x2725L,0x603DL,0x2725L,0x603DL,0x2725L,0x603DL,0x2725L,0x603DL,0x2725L,0x603DL};
static uint32_t g_449 = 4294967292UL;
static int16_t g_491 = 3L;
static int16_t g_509 = 2L;
static const int64_t *g_544 = &g_330;
static const int64_t **g_543 = &g_544;
static const int64_t ***g_542 = &g_543;
static const int64_t ****g_541 = &g_542;
static uint32_t g_569[7][9] = {{0UL,4294967293UL,9UL,0UL,4294967291UL,0x0F64D708L,0xB6AD5CA4L,0x34FB258EL,0UL},{0UL,4294967287UL,4294967292UL,0UL,1UL,4294967295UL,0x8B7B97B2L,1UL,4294967292UL},{0xCE55A843L,1UL,0x4B74000FL,4294967287UL,0x71BF9CC3L,4294967287UL,0x4B74000FL,1UL,0xCE55A843L},{0x7D39FCB4L,0x14144C90L,1UL,4294967294UL,0UL,4294967295UL,0xFD5BAA61L,0x34FB258EL,0x8B7B97B2L},{1UL,9UL,4294967295UL,0UL,0x7D39FCB4L,0xBCB82769L,4294967293UL,0x4B74000FL,4294967294UL},{0x7D39FCB4L,0UL,0xFD5BAA61L,4294967292UL,4294967295UL,0UL,4294967294UL,0UL,4294967295UL},{0xCE55A843L,0xFD5BAA61L,0xFD5BAA61L,0xCE55A843L,9UL,0x4DC7CB11L,0x7D39FCB4L,0x8B7B97B2L,0x0F64D708L}};
static int64_t g_575 = 0xEB87E373AC1E6FCALL;
static volatile float **g_624 = (void*)0;
static float g_653 = 0x0.6p-1;
static int8_t g_656 = 0x65L;
static uint32_t g_657 = 0xA03C6BA6L;
static uint8_t g_658[2] = {8UL,8UL};
static float *g_712 = &g_6;
static float **g_711 = &g_712;
static int32_t g_728 = 0L;
static uint32_t **g_731 = (void*)0;
static int32_t *g_755 = &g_121[3][5];
static uint8_t g_805 = 0x54L;
static int8_t g_870 = 0x19L;
static int32_t g_875 = (-1L);
static uint32_t *g_906 = &g_657;
static const uint32_t g_909 = 0x383D4558L;
static const uint32_t *g_908 = &g_909;
static uint16_t g_914[1][9] = {{0xEDCAL,0xEDCAL,0xEDCAL,0xEDCAL,0xEDCAL,0xEDCAL,0xEDCAL,0xEDCAL,0xEDCAL}};
static int8_t g_919 = (-6L);
static float g_920[2] = {0x0.9p+1,0x0.9p+1};
static uint32_t g_921 = 0xC14148B1L;
static union U1 g_925 = {0L};/* VOLATILE GLOBAL g_925 */
static uint32_t * volatile * const g_970 = (void*)0;
static uint32_t * volatile * const  volatile *g_969[5][9] = {{&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970},{&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970},{&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970},{&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970},{&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970,&g_970}};
static uint32_t * volatile * const  volatile ** volatile g_971 = &g_969[4][0];/* VOLATILE GLOBAL g_971 */
static const volatile int32_t **g_975 = (void*)0;
static const volatile int32_t ** volatile * volatile g_974 = &g_975;/* VOLATILE GLOBAL g_974 */
static int8_t g_988 = 0x06L;
static volatile struct S0 g_1009 = {0x4.3BD5FAp-5,-6174,4294967290UL,0x1B05L,0x5AD290E8B73D7979LL};/* VOLATILE GLOBAL g_1009 */
static volatile union U1 g_1039 = {0x9BA3L};/* VOLATILE GLOBAL g_1039 */
static volatile uint32_t g_1047 = 4294967292UL;/* VOLATILE GLOBAL g_1047 */
static volatile uint32_t * const g_1046 = &g_1047;
static volatile uint32_t * const *g_1045 = &g_1046;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int8_t  func_10(int8_t  p_11, uint32_t  p_12, uint8_t  p_13, int32_t  p_14, uint16_t  p_15);
static int8_t  func_24(int64_t  p_25);
static uint32_t  func_30(uint8_t * p_31, uint8_t * p_32, uint64_t  p_33, const uint32_t  p_34);
static uint8_t * func_35(uint8_t * p_36, uint8_t * p_37);
static int32_t * func_38(uint8_t * const  p_39, int8_t  p_40, int8_t  p_41, const uint8_t * p_42);
static uint8_t * func_43(int8_t  p_44, uint16_t  p_45);
static uint32_t  func_51(const int8_t  p_52, uint64_t  p_53, uint16_t  p_54, uint8_t  p_55);
static int8_t  func_62(uint64_t * p_63, int64_t  p_64);
static int32_t * func_70(uint16_t  p_71, int32_t  p_72);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_6 g_18 g_21 g_509 g_755 g_121 g_171 g_74 g_78 g_79 g_870 g_805 g_361 g_330 g_86 g_875 g_359 g_360 g_92 g_449 g_491 g_575 g_543 g_544 g_542 g_658 g_93 g_569 g_99 g_914 g_921 g_919 g_160 g_925 g_541 g_711 g_712 g_728 g_969 g_971 g_974 g_399 g_657 g_50 g_988 g_1009 g_332 g_1039 g_731 g_1045
 * writes: g_18 g_805 g_728 g_330 g_121 g_656 g_93 g_171 g_160 g_658 g_86 g_74 g_906 g_908 g_79 g_914 g_921 g_92 g_6 g_969 g_875 g_50 g_988 g_509 g_99 g_731
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_16 = 0x73A4C924B5411457LL;
    uint8_t *l_17 = &g_18;
    int32_t l_884 = 0x4A438F33L;
    int32_t l_889 = 0xA3908946L;
    uint8_t * const **l_891 = (void*)0;
    const uint8_t *l_894 = &g_805;
    const uint8_t ** const l_893 = &l_894;
    const uint8_t ** const *l_892 = &l_893;
    uint32_t l_896 = 0x9D688367L;
    uint32_t *l_905 = (void*)0;
    union U1 *l_924 = &g_925;
    float l_945 = 0x6.42E3B3p-72;
    int16_t l_951 = 3L;
    int32_t l_953 = (-1L);
    int32_t l_954 = 0x32C243F6L;
    int16_t l_956[2];
    int32_t l_957 = (-8L);
    int32_t l_958 = 0xA978D034L;
    int32_t l_959 = 1L;
    int16_t l_963 = 0xE8D5L;
    const uint8_t l_987 = 9UL;
    int8_t *l_1026[4][6] = {{&g_160[1],&g_160[1],&g_919,&g_160[1],&g_160[1],&g_919},{&g_160[1],&g_160[1],&g_919,&g_160[1],&g_160[1],&g_919},{&g_160[1],&g_160[1],&g_919,&g_160[1],&g_160[1],&g_160[1]},{(void*)0,(void*)0,&g_160[1],(void*)0,(void*)0,&g_160[1]}};
    const int8_t *l_1029[7] = {(void*)0,(void*)0,&g_160[5],(void*)0,(void*)0,&g_160[5],(void*)0};
    int i, j;
    for (i = 0; i < 2; i++)
        l_956[i] = 0xB865L;
lbl_968:
    if ((((safe_add_func_uint32_t_u_u(((((safe_mod_func_int64_t_s_s(((1UL > 0UL) == (g_7 <= ((safe_lshift_func_int8_t_s_u(func_10(l_16, l_16, (g_6 , (++(*l_17))), (g_21 > (safe_mod_func_int8_t_s_s(func_24((((((safe_lshift_func_uint8_t_u_u((l_16 | (safe_sub_func_uint32_t_u_u(func_30(func_35(l_17, l_17), l_17, l_16, g_509), (*g_755)))), l_16)) , 4294967286UL) && (*g_171)) != 0xE1L) , 0xD42A03F1FFF5D747LL)), l_16))), l_16), 6)) | l_16))), l_16)) <= 0x2507L) < g_21) <= l_16), l_16)) != l_16) != 0x540C5B54L))
    { /* block id: 370 */
        int16_t l_883 = 0L;
        const uint8_t ** const **l_895 = &l_892;
        const uint32_t **l_907 = (void*)0;
        uint16_t *l_912 = (void*)0;
        uint16_t *l_913 = &g_914[0][7];
        int32_t l_917 = 0xFB23891FL;
        uint16_t *l_918[10] = {&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99};
        union U1 **l_926 = &l_924;
        int i;
        (*g_755) = ((l_884 = l_883) , ((1L || ((0x23L == (safe_div_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u((((0x67L != (((((l_889 ^ ((l_896 = (safe_unary_minus_func_int8_t_s(((((l_891 = (void*)0) != ((*l_895) = l_892)) >= 0L) >= g_491)))) , (***g_359))) , l_883) , 0x3846L) < g_575) > (**g_543))) > (***g_542)) || (*g_171)), 0x5103FC4D798EFA59LL)), l_883))) | g_658[0])) <= g_93));
        (*g_171) = (safe_mul_func_uint8_t_u_u(((safe_add_func_uint16_t_u_u(((*l_913) ^= (safe_add_func_int32_t_s_s((safe_mul_func_uint16_t_u_u(((0x95A7L == ((g_906 = l_905) == (g_908 = (void*)0))) ^ ((*g_78) = (safe_lshift_func_int16_t_s_u(((-3L) != g_569[0][5]), 10)))), g_99)), (*g_171)))), (0x214AL | (safe_mul_func_uint16_t_u_u((g_921++), (l_917 > l_917)))))) == g_919), 0x39L));
        (*l_926) = l_924;
    }
    else
    { /* block id: 383 */
        int64_t *l_942 = &g_92[8][0];
        int32_t l_946 = 0x30A6FA0AL;
        uint8_t *l_947 = &g_805;
        (*g_171) ^= ((safe_add_func_uint8_t_u_u(((*l_947) |= (safe_mod_func_uint8_t_u_u((((safe_mod_func_int8_t_s_s((g_160[5] > (**g_543)), (*g_78))) < (safe_rshift_func_int16_t_s_s(1L, (g_925 , (safe_unary_minus_func_uint8_t_u(((safe_mul_func_int16_t_s_s(((((l_896 != (safe_div_func_float_f_f((((*g_712) = (((0x1.E24819p-36 < (safe_sub_func_float_f_f((((*l_942) = (****g_541)) , (safe_sub_func_float_f_f((-0x1.0p-1), (**g_711)))), l_945))) >= 0x1.9p+1) >= l_884)) < l_896), 0x3.8F446Ep-44))) >= l_946) < l_16) , 6L), l_946)) > l_884))))))) != g_18), (-1L)))), g_728)) == l_889);
    }
    if (l_884)
    { /* block id: 389 */
        int16_t l_952 = 0L;
        int32_t l_955 = 0L;
        int32_t l_960 = 1L;
        int32_t l_961[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int64_t l_964 = 0x114369730D2B5F4ELL;
        int32_t *l_976 = &g_728;
        int32_t *l_977 = &g_875;
        int32_t *l_986[7][9][4] = {{{(void*)0,&g_74,&g_121[0][1],(void*)0},{(void*)0,&l_954,(void*)0,&l_955},{&g_50,&l_961[0],&g_121[0][1],&l_954},{(void*)0,&l_955,&l_884,&l_961[0]},{&g_121[0][1],&l_954,&l_884,&l_959},{(void*)0,&l_961[6],&g_121[0][1],(void*)0},{&g_50,&l_961[0],(void*)0,&l_961[0]},{(void*)0,&l_961[0],&g_121[0][1],&l_960},{(void*)0,&l_961[0],&l_958,&l_961[0]}},{{&g_50,&l_961[0],&l_884,(void*)0},{&l_953,&l_961[6],&g_121[0][1],&l_959},{&g_50,&l_954,&l_884,&l_961[0]},{&g_50,&l_955,&g_121[0][1],&l_954},{&l_953,&l_961[0],&l_884,&l_955},{&g_50,&l_954,&l_958,(void*)0},{(void*)0,&g_74,&g_121[0][1],(void*)0},{(void*)0,&l_954,(void*)0,&l_955},{&g_50,&l_961[0],&g_121[0][1],&l_954}},{{(void*)0,&l_955,&l_884,&l_961[0]},{&g_121[0][1],&l_954,&l_884,&l_959},{(void*)0,&l_961[6],&g_121[0][1],(void*)0},{&g_50,&l_961[0],(void*)0,&l_961[0]},{(void*)0,&l_961[0],&g_121[0][1],&l_960},{(void*)0,&l_961[0],&l_958,&l_961[0]},{&g_50,&l_961[0],&l_884,(void*)0},{&l_953,&l_961[6],&g_121[0][1],&l_959},{&g_50,&l_954,&l_884,&l_961[0]}},{{&g_50,&l_955,&g_121[0][1],&l_954},{&l_953,&l_961[0],&l_884,&l_955},{&g_50,&l_954,&l_958,(void*)0},{(void*)0,&g_74,&g_121[0][1],(void*)0},{(void*)0,&l_954,(void*)0,&l_955},{&g_50,&l_961[0],&g_121[0][1],&l_954},{(void*)0,&l_955,&l_884,&l_961[0]},{&g_121[0][1],&l_954,&l_884,&l_959},{(void*)0,&l_961[6],&g_121[0][1],(void*)0}},{{&g_50,&l_961[0],(void*)0,&l_961[0]},{(void*)0,&l_961[0],&g_121[0][1],&l_960},{(void*)0,&l_961[0],&l_958,&l_961[0]},{&g_50,&l_961[0],&l_953,&l_961[0]},{&g_121[0][1],&l_955,&l_884,&l_953},{&l_884,&g_74,&l_955,&l_960},{&l_884,&l_953,&l_884,&l_959},{&g_121[0][1],&l_960,&l_953,&l_953},{(void*)0,&g_74,&l_961[1],&l_961[0]}},{{&g_121[0][1],&l_884,&l_884,&l_961[0]},{&l_958,&g_74,&l_958,&l_953},{&l_884,&l_960,&g_50,&l_959},{&g_121[0][1],&l_953,&l_953,&l_960},{&l_884,&g_74,&l_953,&l_953},{&g_121[0][1],&l_955,&g_50,&l_961[0]},{&l_884,&l_954,&l_958,&l_960},{&l_958,&l_960,&l_884,&g_50},{&g_121[0][1],&l_960,&l_961[1],&l_960}},{{(void*)0,&l_954,&l_953,&l_961[0]},{&g_121[0][1],&l_955,&l_884,&l_953},{&l_884,&g_74,&l_955,&l_960},{&l_884,&l_953,&l_884,&l_959},{&g_121[0][1],&l_960,&l_953,&l_953},{(void*)0,&g_74,&l_961[1],&l_961[0]},{&g_121[0][1],&l_884,&l_884,&l_961[0]},{&l_958,&g_74,&l_958,&l_953},{&l_884,&l_960,&g_50,&l_959}}};
        int i, j, k;
        for (l_889 = 0; (l_889 > (-29)); l_889 = safe_sub_func_uint64_t_u_u(l_889, 1))
        { /* block id: 392 */
            int32_t *l_950[4];
            int32_t l_962 = 1L;
            uint8_t l_965 = 251UL;
            int i;
            for (i = 0; i < 4; i++)
                l_950[i] = &g_121[7][5];
            l_965--;
            if (l_889)
                goto lbl_968;
            (*g_171) = l_955;
        }
        (*g_971) = g_969[4][0];
        g_988 &= ((*g_755) = (safe_unary_minus_func_int64_t_s(((((+(g_50 |= (((*l_977) ^= (l_896 , ((*l_976) &= (0L > (g_974 == &g_975))))) , (((*l_17) = (safe_rshift_func_uint8_t_u_s((safe_mod_func_uint64_t_u_u((((**g_360) = ((safe_mul_func_uint16_t_u_u(((*g_171) , ((void*)0 == &l_893)), (safe_div_func_uint64_t_u_u(18446744073709551607UL, (0x785CL || g_399[1]))))) ^ (*g_755))) != g_657), g_728)), 2))) || l_960)))) ^ l_957) <= l_987) < g_21))));
    }
    else
    { /* block id: 405 */
        uint8_t *l_1022 = &g_658[1];
        uint8_t *l_1023 = &g_658[0];
        int32_t l_1048 = 0x074A6F42L;
        for (l_958 = 18; (l_958 <= (-11)); l_958--)
        { /* block id: 408 */
            int32_t *l_991 = &l_954;
            int32_t *l_992 = (void*)0;
            int32_t *l_993 = &l_959;
            int32_t *l_994 = &l_953;
            int32_t *l_995 = &g_74;
            int32_t *l_996 = &l_959;
            int32_t *l_997 = &l_957;
            int32_t *l_998 = &g_50;
            int32_t *l_999 = &l_954;
            int32_t *l_1000[9][4] = {{&g_74,&g_121[6][0],&l_884,&g_121[6][0]},{&g_121[6][0],&l_954,&l_884,&l_884},{&g_74,&g_74,&g_121[6][0],&l_884},{&g_121[0][1],&l_954,&g_121[0][1],&g_121[6][0]},{&g_121[0][1],&g_121[6][0],&g_121[6][0],&g_121[0][1]},{&g_74,&g_121[6][0],&l_884,&g_121[6][0]},{&g_121[6][0],&l_954,&l_884,&l_884},{&g_74,&g_74,&g_121[6][0],&l_884},{&g_121[0][1],&l_954,&g_121[0][1],&g_121[6][0]}};
            uint32_t l_1001[3];
            int16_t *l_1006 = &g_509;
            int8_t *l_1025 = &g_160[0];
            int8_t **l_1024[4] = {&l_1025,&l_1025,&l_1025,&l_1025};
            uint16_t *l_1030 = &g_914[0][7];
            uint16_t *l_1031 = &g_99;
            uint32_t *l_1041[10][5][5] = {{{(void*)0,(void*)0,&l_1001[1],(void*)0,&g_449},{&g_569[0][5],&l_896,&l_1001[2],&g_449,(void*)0},{&g_449,&l_1001[0],&g_569[0][5],&g_449,&g_449},{(void*)0,(void*)0,(void*)0,&g_569[0][5],&g_569[6][6]},{&g_569[0][5],(void*)0,(void*)0,(void*)0,&l_896}},{{&g_449,&g_569[0][5],(void*)0,&g_449,&g_449},{&l_1001[1],(void*)0,(void*)0,&l_896,&l_1001[0]},{&g_449,&g_569[0][5],(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_569[0][5],&l_896,&g_569[0][5]},{&l_896,(void*)0,&l_1001[2],&l_1001[2],(void*)0}},{{&g_449,(void*)0,&l_1001[1],(void*)0,(void*)0},{(void*)0,&g_449,&l_896,&g_569[0][5],&l_896},{&g_449,(void*)0,&l_1001[1],&l_1001[2],&l_1001[1]},{(void*)0,&g_449,&l_1001[0],&l_1001[1],&l_1001[1]},{&g_569[0][5],&l_896,(void*)0,(void*)0,(void*)0}},{{&l_1001[0],(void*)0,(void*)0,&g_569[0][5],(void*)0},{(void*)0,&g_449,&g_449,(void*)0,&l_1001[2]},{&l_896,&l_896,(void*)0,(void*)0,(void*)0},{&g_569[0][5],&g_569[2][5],&l_1001[2],&l_896,&l_1001[1]},{&l_896,&g_569[0][5],&l_1001[0],(void*)0,&g_569[2][1]}},{{&g_569[3][2],(void*)0,&g_569[0][5],(void*)0,(void*)0},{&l_1001[1],(void*)0,(void*)0,&g_569[0][5],&g_569[0][5]},{&g_569[2][5],&l_1001[1],&g_569[0][5],(void*)0,&g_569[0][5]},{&g_569[0][5],&g_569[0][5],&g_569[0][5],&l_1001[1],(void*)0},{&l_896,&l_1001[1],&g_449,&l_1001[2],&g_569[0][5]}},{{&l_1001[1],&g_449,&l_1001[1],&g_569[0][5],&g_569[6][5]},{&g_569[3][2],&l_1001[1],&g_569[4][8],(void*)0,(void*)0},{(void*)0,&g_569[0][5],&g_569[2][1],(void*)0,&l_1001[1]},{&l_1001[1],&l_1001[1],&l_896,(void*)0,&g_449},{&l_896,(void*)0,(void*)0,(void*)0,&l_896}},{{&l_1001[1],(void*)0,&g_449,&g_569[0][5],(void*)0},{(void*)0,&g_569[0][5],(void*)0,&l_896,&g_569[0][5]},{&g_569[0][5],&g_569[2][5],&g_569[0][5],(void*)0,(void*)0},{(void*)0,&l_896,(void*)0,(void*)0,&l_896},{(void*)0,&g_449,&l_1001[1],&g_569[0][5],&g_449}},{{(void*)0,(void*)0,&g_449,&l_1001[1],&l_1001[1]},{&l_1001[2],&l_896,&l_1001[2],&l_896,(void*)0},{(void*)0,&g_449,(void*)0,&l_1001[2],&g_569[6][5]},{(void*)0,(void*)0,&l_1001[2],(void*)0,&g_569[0][5]},{(void*)0,&l_896,(void*)0,&g_569[6][5],(void*)0}},{{&l_1001[1],&l_896,&l_1001[2],&g_569[2][5],&g_569[0][5]},{&l_896,&l_1001[2],&g_449,(void*)0,&g_569[0][5]},{&g_449,(void*)0,&l_1001[1],&l_1001[1],(void*)0},{&l_1001[1],(void*)0,(void*)0,&l_1001[2],&g_569[2][1]},{&g_569[2][5],(void*)0,&g_569[0][5],(void*)0,&l_1001[1]}},{{&g_569[2][1],&g_569[0][5],(void*)0,&l_1001[1],(void*)0},{&g_569[2][5],&g_449,&g_449,&g_569[0][5],&l_1001[2]},{&l_1001[1],&l_1001[1],(void*)0,&g_569[0][5],(void*)0},{&g_449,&l_1001[1],&l_896,&g_449,(void*)0},{&l_896,&g_569[2][1],&g_569[2][1],&g_569[2][1],&g_449}}};
            uint32_t **l_1040 = &l_1041[4][2][2];
            uint32_t ***l_1044 = &g_731;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1001[i] = 4294967295UL;
            ++l_1001[1];
            if ((safe_mul_func_int16_t_s_s((((*l_1006) = g_160[1]) | (((*l_1022) = (((safe_rshift_func_int8_t_s_u(((((*l_1031) = (g_1009 , ((safe_div_func_uint16_t_u_u(((*l_1030) &= ((safe_div_func_uint8_t_u_u(l_896, (((((safe_div_func_int8_t_s_s(((safe_rshift_func_int16_t_s_u((((safe_sub_func_int64_t_s_s((l_17 != (l_1026[1][3] = func_35(l_1022, (l_1023 = l_1022)))), (((((safe_div_func_int32_t_s_s((l_1029[5] == l_17), 0x86E918A2L)) | (-1L)) ^ g_399[6]) , &l_1022) != (void*)0))) > 0L) | 0x8CL), 6)) , (-1L)), g_332)) || l_963) , 1UL) , 0x39D4FE54L) ^ (*g_755)))) || l_956[1])), g_74)) && l_889))) ^ g_21) == 0x89901771L), 4)) , (void*)0) == (void*)0)) == l_956[1])), 0x5A29L)))
            { /* block id: 416 */
                int16_t l_1034 = (-1L);
                (*l_993) = (safe_rshift_func_int8_t_s_u((g_728 & ((void*)0 != &g_542)), 6));
                if ((*l_994))
                    continue;
                (*g_755) &= l_1034;
                if ((*g_755))
                    break;
            }
            else
            { /* block id: 421 */
                (*g_712) = l_963;
            }
            (*l_991) ^= ((*l_998) &= ((0x557EB0EFA0D2374BLL <= ((((safe_div_func_int16_t_s_s((((((safe_rshift_func_uint16_t_u_u((g_1039 , 7UL), (0UL != ((l_1040 == (void*)0) & ((l_953 != (safe_mul_func_uint8_t_u_u((((((*l_1044) = g_731) != g_1045) && l_1048) , 0UL), (*l_994)))) >= g_330))))) , l_1048) <= 0x0.Fp-1) , g_121[0][1]) , g_92[4][1]), 0xB1ABL)) <= g_914[0][4]) > (*l_997)) == 0x4F874D3B3C9DDAC5LL)) & 0x1BL));
        }
    }
    return g_914[0][7];
}


/* ------------------------------------------ */
/* 
 * reads : g_171
 * writes: g_74
 */
static int8_t  func_10(int8_t  p_11, uint32_t  p_12, uint8_t  p_13, int32_t  p_14, uint16_t  p_15)
{ /* block id: 367 */
    (*g_171) = 0x8546CB26L;
    return p_15;
}


/* ------------------------------------------ */
/* 
 * reads : g_330 g_755 g_656 g_78 g_79 g_121 g_870 g_805 g_361 g_86 g_875 g_359 g_360 g_92 g_449 g_171 g_74
 * writes: g_330 g_121 g_656 g_93 g_171 g_160 g_658 g_805 g_86
 */
static int8_t  func_24(int64_t  p_25)
{ /* block id: 340 */
    uint8_t l_819 = 247UL;
    int32_t l_827 = 0x58A31F7EL;
    int32_t l_828 = 0x1A18E345L;
    int32_t l_833 = 0x1B4A6FFDL;
    int32_t l_834 = (-3L);
    int32_t l_835 = 0x51FF2A28L;
    int32_t l_836[1][4];
    int32_t **l_848 = &g_171;
    uint32_t * const l_872 = &g_569[0][5];
    uint64_t l_880 = 18446744073709551607UL;
    int16_t l_881 = 0x20A1L;
    uint8_t *****l_882 = &g_248;
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
            l_836[i][j] = 0xD1A01A04L;
    }
    for (g_330 = 16; (g_330 >= (-2)); g_330 = safe_sub_func_uint16_t_u_u(g_330, 7))
    { /* block id: 343 */
        l_819 ^= ((*g_755) = 0x777FE9B0L);
    }
    for (g_656 = 24; (g_656 <= (-4)); g_656--)
    { /* block id: 349 */
        int32_t *l_824 = (void*)0;
        int32_t l_830 = (-1L);
        int32_t l_832 = 0x255CDB4BL;
        int32_t l_838 = 0x2F3F7DBEL;
        int32_t l_839 = (-10L);
        int32_t l_840 = 0x28738744L;
        int32_t l_841[6];
        int32_t **l_847 = &l_824;
        int32_t ***l_846[6][3][10] = {{{&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847,(void*)0,&l_847,&l_847,(void*)0},{&l_847,&l_847,&l_847,&l_847,(void*)0,&l_847,&l_847,&l_847,&l_847,(void*)0},{&l_847,&l_847,(void*)0,&l_847,&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847}},{{&l_847,&l_847,&l_847,&l_847,&l_847,(void*)0,(void*)0,(void*)0,&l_847,(void*)0},{&l_847,(void*)0,&l_847,&l_847,&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847},{&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847,&l_847,(void*)0,&l_847,(void*)0}},{{&l_847,(void*)0,(void*)0,(void*)0,&l_847,&l_847,&l_847,&l_847,&l_847,&l_847},{&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847,(void*)0,&l_847,&l_847,(void*)0},{&l_847,&l_847,&l_847,&l_847,(void*)0,&l_847,&l_847,&l_847,&l_847,(void*)0}},{{&l_847,&l_847,(void*)0,&l_847,&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847,(void*)0,(void*)0,(void*)0,&l_847,(void*)0},{&l_847,(void*)0,&l_847,&l_847,&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847}},{{&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847,&l_847,(void*)0,&l_847,(void*)0},{&l_847,(void*)0,(void*)0,(void*)0,&l_847,&l_847,&l_847,&l_847,&l_847,&l_847},{&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847,(void*)0,&l_847,&l_847,(void*)0}},{{&l_847,&l_847,&l_847,&l_847,(void*)0,&l_847,&l_847,&l_847,&l_847,(void*)0},{&l_847,&l_847,(void*)0,&l_847,&l_847,(void*)0,&l_847,(void*)0,&l_847,&l_847},{&l_847,&l_847,&l_847,&l_847,&l_847,(void*)0,(void*)0,(void*)0,&l_847,(void*)0}}};
        int64_t ****l_856 = &g_359;
        int64_t *****l_855[5] = {&l_856,&l_856,&l_856,&l_856,&l_856};
        int8_t *l_859 = (void*)0;
        int8_t *l_860 = &g_160[1];
        uint8_t *l_869 = &g_658[1];
        uint8_t *l_871 = &g_805;
        int32_t l_873 = 0x09A24E2DL;
        uint32_t l_874 = 0x546C81EFL;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_841[i] = 3L;
        for (g_93 = 0; (g_93 == 26); ++g_93)
        { /* block id: 352 */
            int32_t **l_825 = (void*)0;
            int32_t **l_826 = &g_171;
            int32_t *l_829[3][1][1];
            int64_t l_831 = 1L;
            int32_t l_837 = 0L;
            float l_842 = 0x4.4301A0p-31;
            uint16_t l_843 = 0x10CCL;
            int i, j, k;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_829[i][j][k] = &g_50;
                }
            }
            (*l_826) = l_824;
            l_843++;
        }
        l_848 = &g_171;
        l_874 ^= (((((((((safe_mod_func_uint8_t_u_u(p_25, ((*l_871) |= ((safe_rshift_func_uint8_t_u_u(((l_855[0] != (void*)0) >= (safe_rshift_func_int8_t_s_u(((*l_860) = p_25), 5))), 2)) <= ((((*g_78) > ((*l_869) = (safe_mul_func_int64_t_s_s(((void*)0 == l_824), ((-1L) != (safe_mod_func_uint32_t_u_u(((safe_rshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s(p_25, 0x89C9L)), 4)) , p_25), (*g_755)))))))) >= p_25) != g_870))))) & l_833) , l_872) != l_824) > l_873) , (*g_361)) >= p_25) | 0x67840D2E58048B3CLL) || 7L);
        (*l_847) = func_70(p_25, p_25);
    }
    (*l_848) = (((((g_79 && 0x7CF4966BAD40532ELL) , (((***g_359) = g_875) && (((safe_lshift_func_int8_t_s_s(g_92[7][1], 2)) , ((((&g_509 == (void*)0) < p_25) <= (((**g_360) = (p_25 <= ((safe_sub_func_uint8_t_u_u(255UL, l_880)) ^ g_449))) > 0xF61CBF452E8EB8C4LL)) >= p_25)) != l_881))) , l_882) == l_882) , &g_74);
    return (**l_848);
}


/* ------------------------------------------ */
/* 
 * reads : g_805 g_728
 * writes: g_805 g_728
 */
static uint32_t  func_30(uint8_t * p_31, uint8_t * p_32, uint64_t  p_33, const uint32_t  p_34)
{ /* block id: 328 */
    for (g_805 = 0; (g_805 != 54); g_805 = safe_add_func_uint32_t_u_u(g_805, 9))
    { /* block id: 331 */
        int64_t ****l_815 = &g_359;
        int64_t *****l_814 = &l_815;
        int32_t l_816 = 0x3C5021E9L;
        for (g_728 = 0; (g_728 >= (-15)); g_728 = safe_sub_func_uint32_t_u_u(g_728, 9))
        { /* block id: 334 */
            int64_t ****l_813 = (void*)0;
            int64_t ***** const l_812 = &l_813;
            l_814 = l_812;
        }
        if (l_816)
            continue;
    }
    return p_33;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t * func_35(uint8_t * p_36, uint8_t * p_37)
{ /* block id: 2 */
    uint8_t l_46 = 0UL;
    int32_t *l_49 = &g_50;
    uint64_t *l_56 = (void*)0;
    uint64_t *l_57 = &g_58;
    const uint8_t l_59 = 9UL;
    const uint8_t *l_368 = &l_46;
    int32_t **l_371 = &g_171;
    uint8_t *****l_400[1];
    int32_t l_497 = 1L;
    int32_t l_498 = 0L;
    int32_t l_508 = 0xF6C83595L;
    int32_t l_510 = 0L;
    uint16_t l_514[5] = {65533UL,65533UL,65533UL,65533UL,65533UL};
    uint16_t l_534 = 0xBE74L;
    int16_t *l_603 = &g_399[2];
    float *l_709 = &g_653;
    float **l_708 = &l_709;
    int32_t l_762 = 0x9825A84EL;
    int32_t l_763[4];
    int32_t l_764 = 0x2C8FA361L;
    int32_t l_765 = (-2L);
    int32_t l_766 = 2L;
    int32_t l_767 = (-10L);
    int32_t l_768 = 0x5C4EAB1EL;
    int32_t l_769 = 0xAAF010CAL;
    int32_t l_770 = 0xA64A1C35L;
    int32_t l_771 = (-8L);
    int32_t l_772 = 0xD789472DL;
    int32_t l_773 = 0L;
    int32_t l_775[3][6][7] = {{{(-8L),0L,0L,(-2L),(-4L),(-4L),(-2L)},{(-6L),0x1671531BL,(-6L),7L,0x6E813979L,(-6L),(-1L)},{(-2L),0x74A32B8DL,0xE297A11BL,0x56036D64L,0x74A32B8DL,(-8L),0x74A32B8DL},{0xDA54AA89L,7L,7L,0xDA54AA89L,(-1L),(-6L),0x6E813979L},{(-4L),(-8L),0L,0L,(-2L),(-4L),(-4L)},{(-1L),0x6E813979L,0xE207D6B1L,0x6E813979L,(-1L),(-10L),0x6E813979L}},{{(-1L),(-1L),(-1L),(-2L),0L,(-1L),0x74A32B8DL},{7L,0x1671531BL,1L,1L,0x1671531BL,7L,(-1L)},{(-1L),(-2L),(-8L),(-1L),0x74A32B8DL,0x1780AEACL,(-2L)},{(-1L),0x2289C6A4L,7L,(-2L),7L,0x2289C6A4L,(-1L)},{(-4L),(-2L),0L,0L,(-8L),(-4L),0L},{0xDA54AA89L,0x1671531BL,0x4DA2EB57L,0x6E813979L,0x6E813979L,0x4DA2EB57L,0x1671531BL}},{{(-2L),(-1L),0L,0x56036D64L,(-1L),0L,0x74A32B8DL},{(-6L),0x6E813979L,7L,(-6L),0x1671531BL,(-6L),7L},{(-8L),(-8L),(-8L),0x56036D64L,(-2L),0x3EDE6972L,(-8L)},{(-1L),7L,1L,0x6E813979L,0x2289C6A4L,0x2289C6A4L,0x6E813979L},{(-1L),0x74A32B8DL,(-1L),0L,(-2L),(-1L),(-1L)},{0x6E813979L,0x1671531BL,0xE207D6B1L,(-2L),0x1671531BL,0x63C0011AL,0x1671531BL}}};
    int32_t l_799 = 9L;
    int32_t l_806 = 0x2ABB06D5L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_400[i] = &g_248;
    for (i = 0; i < 4; i++)
        l_763[i] = 0x525BFD62L;
    return p_36;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_359
 */
static int32_t * func_38(uint8_t * const  p_39, int8_t  p_40, int8_t  p_41, const uint8_t * p_42)
{ /* block id: 141 */
    int64_t ****l_369 = &g_359;
    int32_t *l_370[10][7] = {{&g_121[0][7],(void*)0,&g_121[4][1],&g_121[0][1],&g_121[0][7],&g_121[0][1],&g_121[4][1]},{&g_50,(void*)0,&g_121[1][6],&g_121[0][1],&g_121[2][2],&g_121[2][2],&g_121[0][1]},{&g_121[0][1],&g_50,&g_121[0][1],&g_121[0][1],&g_74,&g_50,&g_74},{(void*)0,&g_121[0][1],(void*)0,&g_50,&g_121[2][2],&g_74,&g_74},{&g_121[0][7],&g_74,&g_50,&g_74,&g_121[0][7],&g_50,&g_50},{(void*)0,(void*)0,&g_121[0][1],(void*)0,&g_50,&g_121[2][2],&g_74},{&g_74,&g_121[0][1],&g_121[0][1],&g_50,&g_121[0][1],&g_121[0][1],&g_74},{(void*)0,(void*)0,&g_74,&g_50,(void*)0,&g_121[1][6],&g_121[0][1]},{&g_121[0][7],&g_121[0][1],&g_121[4][1],(void*)0,&g_121[0][7],(void*)0,&g_121[4][1]},{(void*)0,(void*)0,&g_74,&g_121[0][1],(void*)0,&g_121[2][2],(void*)0}};
    int i, j;
    (*l_369) = (void*)0;
    return l_370[5][2];
}


/* ------------------------------------------ */
/* 
 * reads : g_92 g_99 g_86 g_21 g_74 g_121 g_93 g_160 g_50 g_172 g_79 g_330 g_359 g_78
 * writes: g_92 g_86 g_93 g_99 g_160 g_171 g_172 g_79 g_330 g_74 g_359
 */
static uint8_t * func_43(int8_t  p_44, uint16_t  p_45)
{ /* block id: 35 */
    int32_t *l_124 = &g_121[0][1];
    int32_t **l_123 = &l_124;
    const uint8_t *l_135 = &g_79;
    const uint8_t **l_134 = &l_135;
    int64_t *l_136 = &g_92[7][1];
    const uint8_t **l_142 = (void*)0;
    const uint8_t ***l_141 = &l_142;
    uint32_t l_150[5];
    uint8_t **l_166 = &g_78;
    uint8_t ***l_165 = &l_166;
    uint8_t l_197 = 255UL;
    const uint16_t l_211 = 8UL;
    float *l_224 = &g_225;
    int32_t l_260 = 0x685772B1L;
    int32_t l_264[10][8] = {{1L,4L,1L,(-1L),(-10L),(-1L),1L,4L},{(-10L),(-1L),1L,4L,1L,(-1L),(-10L),(-1L)},{(-10L),4L,0xD9BE9A1AL,4L,(-10L),(-1L),(-10L),4L},{1L,4L,1L,(-1L),(-10L),(-1L),1L,4L},{(-10L),(-1L),1L,4L,1L,(-1L),(-10L),(-1L)},{(-10L),4L,0xD9BE9A1AL,4L,(-10L),(-1L),(-10L),4L},{1L,4L,1L,(-1L),(-10L),(-1L),1L,4L},{(-10L),(-1L),1L,4L,1L,(-1L),(-10L),(-1L)},{(-10L),4L,0xD9BE9A1AL,4L,(-10L),(-1L),(-10L),4L},{1L,4L,1L,(-1L),(-10L),(-1L),1L,4L}};
    uint8_t *l_367 = &l_197;
    int i, j;
    for (i = 0; i < 5; i++)
        l_150[i] = 0x3755EC10L;
    (*l_123) = &g_121[0][1];
lbl_366:
    (*l_123) = func_70((safe_mul_func_uint8_t_u_u((!(safe_sub_func_uint32_t_u_u(((((p_44 || (safe_rshift_func_int16_t_s_u((safe_add_func_int16_t_s_s((((*l_136) &= ((void*)0 == l_134)) != (3L && (safe_sub_func_int8_t_s_s(p_44, (safe_rshift_func_int16_t_s_s((&l_135 == ((*l_141) = (void*)0)), ((safe_div_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f((+(&g_79 != (void*)0)), 0xF.FC8C37p-60)), 0x1.7p-1)), l_150[2])) , 1L))))))), p_45)), p_45))) || 1L) ^ g_99) && p_44), g_86))), g_21)), (*l_124));
    for (g_93 = 20; (g_93 == (-9)); g_93 = safe_sub_func_uint16_t_u_u(g_93, 9))
    { /* block id: 42 */
        for (g_99 = 0; (g_99 > 55); g_99++)
        { /* block id: 45 */
            int8_t *l_159 = &g_160[1];
            uint8_t * const *l_162 = (void*)0;
            uint8_t * const **l_161 = &l_162;
            uint8_t * const ***l_163[4][3][8] = {{{(void*)0,&l_161,&l_161,(void*)0,&l_161,&l_161,&l_161,&l_161},{&l_161,&l_161,(void*)0,&l_161,&l_161,&l_161,(void*)0,&l_161},{&l_161,&l_161,&l_161,(void*)0,&l_161,&l_161,&l_161,&l_161}},{{&l_161,&l_161,(void*)0,(void*)0,&l_161,&l_161,&l_161,&l_161},{&l_161,(void*)0,&l_161,&l_161,(void*)0,&l_161,(void*)0,&l_161},{(void*)0,&l_161,(void*)0,&l_161,&l_161,(void*)0,&l_161,&l_161}},{{&l_161,&l_161,&l_161,(void*)0,(void*)0,&l_161,&l_161,&l_161},{&l_161,&l_161,&l_161,(void*)0,&l_161,&l_161,&l_161,&l_161},{(void*)0,&l_161,&l_161,&l_161,(void*)0,&l_161,&l_161,&l_161}},{{&l_161,&l_161,&l_161,(void*)0,&l_161,&l_161,(void*)0,&l_161},{&l_161,&l_161,&l_161,&l_161,&l_161,(void*)0,&l_161,&l_161},{&l_161,&l_161,&l_161,&l_161,&l_161,&l_161,&l_161,&l_161}}};
            uint8_t * const **l_164[6] = {&l_162,&l_162,&l_162,&l_162,&l_162,&l_162};
            int i, j, k;
            (*l_123) = func_70(p_45, (safe_rshift_func_int16_t_s_u((((((*l_159) |= (safe_lshift_func_int16_t_s_u(0xE397L, 8))) , (&p_44 != (((l_164[3] = l_161) != (l_165 = l_165)) , &p_44))) > (+(safe_lshift_func_int16_t_s_u((p_45 & 1L), 3)))) >= 0xE6C26E64L), 15)));
        }
    }
    if ((g_50 > 0x8B9BL))
    { /* block id: 52 */
        uint16_t l_170 = 65535UL;
        g_171 = (l_170 , &g_50);
    }
    else
    { /* block id: 54 */
        float l_198 = (-0x10.Cp-1);
        int32_t l_220 = (-9L);
        int32_t l_262[9][8] = {{4L,0xD7D14D15L,0xD7D14D15L,0xD7D14D15L,1L,1L,0x52C9F556L,(-1L)},{7L,0xD7D14D15L,1L,0x692F92BEL,0xB138E731L,2L,0xB138E731L,0x692F92BEL},{0xB138E731L,2L,0xB138E731L,0x692F92BEL,1L,0xD7D14D15L,7L,(-1L)},{0x52C9F556L,1L,1L,0xD7D14D15L,0xD7D14D15L,1L,1L,0x52C9F556L},{0x52C9F556L,0x692F92BEL,0xCC2756E3L,0xB138E731L,1L,7L,1L,7L},{0xB138E731L,1L,(-1L),1L,0xB138E731L,7L,2L,1L},{7L,0x692F92BEL,1L,4L,1L,1L,4L,1L},{1L,1L,1L,0x52C9F556L,0xCC2756E3L,0xD7D14D15L,2L,7L},{1L,2L,(-1L),1L,(-1L),2L,1L,7L}};
        int64_t *l_346[2];
        int64_t *l_347 = &g_330;
        int64_t ***l_365 = &g_360;
        int i, j;
        for (i = 0; i < 2; i++)
            l_346[i] = &g_330;
        ++g_172;
        for (g_93 = 21; (g_93 >= (-13)); g_93 = safe_sub_func_uint16_t_u_u(g_93, 6))
        { /* block id: 58 */
            uint32_t l_183 = 0x706A3B5EL;
            uint16_t l_196 = 1UL;
            const float *l_221[5][9] = {{&l_198,(void*)0,&l_198,(void*)0,&l_198,(void*)0,&l_198,(void*)0,&l_198},{&l_198,(void*)0,(void*)0,&l_198,(void*)0,&g_6,&g_6,(void*)0,&l_198},{&l_198,(void*)0,&l_198,&l_198,&l_198,(void*)0,&l_198,&l_198,&l_198},{&l_198,&l_198,&g_6,(void*)0,(void*)0,(void*)0,(void*)0,&g_6,&l_198},{&l_198,&l_198,&l_198,&l_198,&l_198,&l_198,&l_198,&l_198,&l_198}};
            int8_t l_228 = 0x63L;
            uint8_t *** const **l_257[2];
            int32_t l_266 = 1L;
            int32_t l_268 = 9L;
            int32_t l_269[6][9] = {{0xA846929BL,0x4825EF72L,0x4825EF72L,0xA846929BL,0x878FDA27L,0x4825EF72L,0xBD2A67ADL,0x878FDA27L,0x878FDA27L},{0x64537C15L,0L,6L,0xF29779F3L,6L,0L,0x64537C15L,0L,6L},{0xA846929BL,0x878FDA27L,0x4825EF72L,0xBD2A67ADL,0x878FDA27L,0x878FDA27L,0xBD2A67ADL,0x4825EF72L,0x878FDA27L},{0x6DCC5248L,0L,0x047D9953L,0xF29779F3L,0x047D9953L,0L,0x6DCC5248L,0L,0x047D9953L},{0xA846929BL,0x4825EF72L,0x4825EF72L,0xA846929BL,0x878FDA27L,0x4825EF72L,0xBD2A67ADL,0x878FDA27L,0x878FDA27L},{0x64537C15L,0L,6L,0xF29779F3L,6L,0L,0x64537C15L,0L,6L}};
            uint16_t l_303 = 5UL;
            int32_t l_358 = 1L;
            int i, j;
            for (i = 0; i < 2; i++)
                l_257[i] = &g_254;
            for (g_79 = (-3); (g_79 >= 20); ++g_79)
            { /* block id: 61 */
                int32_t ** const l_186 = &l_124;
                uint8_t *** const **l_256 = &g_254;
                int32_t l_259 = 0xD83B3E91L;
                int32_t l_263 = 0x318AA528L;
                int32_t l_265[5] = {0L,0L,0L,0L,0L};
                uint64_t *l_340 = (void*)0;
                uint8_t *l_355 = &l_197;
                int i;
            }
            for (g_330 = (-13); (g_330 >= (-25)); --g_330)
            { /* block id: 128 */
                int64_t ****l_362 = &g_359;
                int64_t ***l_364[9][10] = {{&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360},{(void*)0,&g_360,&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360,(void*)0,&g_360,&g_360,&g_360},{&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360},{(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360,&g_360},{&g_360,&g_360,&g_360,(void*)0,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,&g_360,(void*)0,&g_360}};
                int64_t ****l_363[3][6][1] = {{{(void*)0},{(void*)0},{&l_364[8][6]},{(void*)0},{(void*)0},{&l_364[8][6]}},{{(void*)0},{(void*)0},{&l_364[8][6]},{(void*)0},{(void*)0},{&l_364[8][6]}},{{(void*)0},{(void*)0},{&l_364[8][6]},{(void*)0},{(void*)0},{&l_364[8][6]}}};
                int i, j, k;
                (**l_123) = l_358;
                l_365 = ((*l_362) = g_359);
            }
            if ((p_45 , ((void*)0 != l_221[1][3])))
            { /* block id: 133 */
                if (p_44)
                    goto lbl_366;
            }
            else
            { /* block id: 135 */
                return (*l_166);
            }
        }
    }
    return (**l_165);
}


/* ------------------------------------------ */
/* 
 * reads : g_99 g_74 g_92 g_21 g_86 g_58
 * writes: g_99 g_74 g_78 g_121
 */
static uint32_t  func_51(const int8_t  p_52, uint64_t  p_53, uint16_t  p_54, uint8_t  p_55)
{ /* block id: 26 */
    int32_t *l_94 = &g_74;
    int32_t *l_95 = (void*)0;
    int32_t *l_96 = &g_74;
    int32_t *l_97 = &g_74;
    int32_t *l_98[7][4] = {{(void*)0,&g_74,(void*)0,&g_74},{(void*)0,&g_74,(void*)0,&g_74},{(void*)0,&g_74,(void*)0,&g_74},{(void*)0,&g_74,(void*)0,&g_74},{(void*)0,&g_74,(void*)0,&g_74},{(void*)0,&g_74,(void*)0,&g_74},{(void*)0,&g_74,(void*)0,&g_74}};
    uint64_t *l_112[7][5][7] = {{{&g_58,&g_58,&g_86,&g_58,&g_58,&g_86,&g_86},{&g_86,&g_58,(void*)0,&g_86,(void*)0,(void*)0,&g_86},{&g_86,&g_58,&g_58,&g_58,&g_86,&g_58,&g_58},{(void*)0,(void*)0,&g_86,(void*)0,&g_58,(void*)0,(void*)0},{&g_86,&g_58,&g_86,&g_58,&g_58,&g_58,(void*)0}},{{(void*)0,&g_86,(void*)0,&g_58,&g_58,&g_86,&g_86},{&g_58,&g_58,&g_86,&g_86,&g_86,&g_86,&g_86},{&g_58,&g_86,(void*)0,&g_86,(void*)0,&g_58,&g_58},{&g_58,&g_86,(void*)0,&g_58,&g_58,&g_58,&g_58},{&g_58,&g_58,&g_58,(void*)0,&g_58,&g_86,&g_58}},{{&g_86,&g_86,&g_86,&g_58,&g_58,&g_86,&g_86},{&g_58,(void*)0,&g_86,(void*)0,&g_86,&g_86,(void*)0},{&g_58,&g_86,&g_58,&g_86,&g_86,&g_58,&g_86},{&g_58,&g_86,&g_86,(void*)0,&g_58,&g_58,&g_86},{&g_58,&g_58,&g_86,&g_86,&g_58,&g_86,&g_58}},{{&g_86,&g_58,&g_58,&g_86,(void*)0,&g_86,&g_86},{&g_58,&g_58,&g_86,&g_86,&g_86,&g_58,&g_86},{&g_58,(void*)0,&g_86,(void*)0,&g_58,&g_58,&g_86},{&g_86,(void*)0,&g_58,&g_86,&g_58,&g_58,&g_58},{&g_86,&g_58,&g_58,&g_86,&g_58,&g_86,&g_86}},{{&g_58,&g_58,&g_86,&g_86,&g_86,&g_86,&g_86},{&g_86,&g_86,&g_58,&g_86,&g_86,(void*)0,(void*)0},{&g_86,&g_86,&g_58,(void*)0,(void*)0,&g_58,&g_86},{(void*)0,&g_58,(void*)0,&g_58,&g_86,&g_58,(void*)0},{&g_86,(void*)0,&g_86,&g_58,(void*)0,&g_58,&g_86}},{{&g_86,&g_58,(void*)0,&g_86,&g_86,&g_58,&g_86},{&g_58,&g_86,&g_86,&g_58,&g_58,&g_86,&g_58},{&g_86,&g_86,&g_58,&g_58,&g_86,&g_86,&g_58},{&g_86,&g_86,&g_86,&g_58,&g_58,(void*)0,&g_86},{&g_58,&g_58,&g_86,(void*)0,&g_58,&g_86,&g_86}},{{&g_58,&g_86,&g_86,&g_86,&g_86,&g_86,(void*)0},{&g_86,&g_86,&g_58,&g_86,&g_58,&g_58,&g_58},{&g_58,&g_86,&g_58,&g_86,&g_86,(void*)0,&g_58},{&g_58,&g_58,(void*)0,&g_58,(void*)0,&g_58,&g_58},{&g_58,(void*)0,&g_86,(void*)0,&g_86,&g_58,&g_58}}};
    uint8_t **l_120[2];
    uint8_t **l_122 = &g_78;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_120[i] = &g_78;
    g_99--;
    g_121[0][1] = (safe_sub_func_uint8_t_u_u(255UL, (((*l_96) = (+0UL)) && (safe_mod_func_int8_t_s_s(((safe_rshift_func_int8_t_s_u((safe_lshift_func_uint8_t_u_u((+(((p_53++) | (safe_mul_func_int8_t_s_s((*l_94), (*l_97)))) , g_92[1][0])), (p_55 &= (+(g_92[1][1] | (1L < ((&g_79 == (g_78 = ((4294967293UL < g_21) , (void*)0))) ^ (*l_97)))))))), g_21)) & g_86), g_99)))));
    l_122 = &g_78;
    return g_58;
}


/* ------------------------------------------ */
/* 
 * reads : g_21 g_78 g_86 g_79 g_74 g_92 g_93
 * writes: g_86 g_79 g_74 g_93
 */
static int8_t  func_62(uint64_t * p_63, int64_t  p_64)
{ /* block id: 5 */
    uint8_t * const l_77 = (void*)0;
    int32_t *l_89 = &g_74;
    for (p_64 = 17; (p_64 <= (-7)); p_64 = safe_sub_func_int16_t_s_s(p_64, 3))
    { /* block id: 8 */
        int32_t *l_67 = (void*)0;
        int32_t **l_68 = (void*)0;
        int32_t **l_69 = &l_67;
        int32_t *l_73 = &g_74;
        uint8_t *l_76 = (void*)0;
        (*l_69) = l_67;
        l_89 = func_70(p_64, (0xE48CL && ((l_73 != (((g_21 | ((~(l_76 == (void*)0)) & (l_77 == g_78))) || 6UL) , (void*)0)) != 0xF554C62BL)));
    }
    for (g_79 = 0; (g_79 <= 47); g_79++)
    { /* block id: 17 */
        for (g_74 = 0; (g_74 <= 1); g_74 += 1)
        { /* block id: 20 */
            int i, j;
            g_93 ^= g_92[(g_74 + 4)][g_74];
            return g_92[(g_74 + 5)][g_74];
        }
    }
    return g_86;
}


/* ------------------------------------------ */
/* 
 * reads : g_86
 * writes: g_86
 */
static int32_t * func_70(uint16_t  p_71, int32_t  p_72)
{ /* block id: 10 */
    int32_t *l_80 = (void*)0;
    int32_t *l_81 = &g_74;
    int32_t *l_82 = &g_74;
    int32_t *l_83 = &g_74;
    int32_t *l_84[5][4] = {{(void*)0,(void*)0,&g_74,(void*)0},{(void*)0,&g_74,&g_74,(void*)0},{&g_74,(void*)0,&g_74,&g_74},{(void*)0,(void*)0,&g_74,(void*)0},{(void*)0,&g_74,&g_74,&g_74}};
    int32_t l_85 = (-10L);
    int i, j;
    --g_86;
    return &g_74;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc_bytes (&g_6, sizeof(g_6), "g_6", print_hash_value);
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_50, "g_50", print_hash_value);
    transparent_crc(g_58, "g_58", print_hash_value);
    transparent_crc(g_74, "g_74", print_hash_value);
    transparent_crc(g_79, "g_79", print_hash_value);
    transparent_crc(g_86, "g_86", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_92[i][j], "g_92[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_99, "g_99", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_121[i][j], "g_121[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_160[i], "g_160[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_172, "g_172", print_hash_value);
    transparent_crc_bytes (&g_225, sizeof(g_225), "g_225", print_hash_value);
    transparent_crc(g_289, "g_289", print_hash_value);
    transparent_crc(g_330, "g_330", print_hash_value);
    transparent_crc(g_332, "g_332", print_hash_value);
    transparent_crc_bytes (&g_397, sizeof(g_397), "g_397", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_399[i], "g_399[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_449, "g_449", print_hash_value);
    transparent_crc(g_491, "g_491", print_hash_value);
    transparent_crc(g_509, "g_509", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_569[i][j], "g_569[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_575, "g_575", print_hash_value);
    transparent_crc_bytes (&g_653, sizeof(g_653), "g_653", print_hash_value);
    transparent_crc(g_656, "g_656", print_hash_value);
    transparent_crc(g_657, "g_657", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_658[i], "g_658[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_728, "g_728", print_hash_value);
    transparent_crc(g_805, "g_805", print_hash_value);
    transparent_crc(g_870, "g_870", print_hash_value);
    transparent_crc(g_875, "g_875", print_hash_value);
    transparent_crc(g_909, "g_909", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_914[i][j], "g_914[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_919, "g_919", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc_bytes(&g_920[i], sizeof(g_920[i]), "g_920[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_921, "g_921", print_hash_value);
    transparent_crc(g_925.f0, "g_925.f0", print_hash_value);
    transparent_crc(g_988, "g_988", print_hash_value);
    transparent_crc_bytes (&g_1009.f0, sizeof(g_1009.f0), "g_1009.f0", print_hash_value);
    transparent_crc(g_1009.f1, "g_1009.f1", print_hash_value);
    transparent_crc(g_1009.f2, "g_1009.f2", print_hash_value);
    transparent_crc(g_1009.f3, "g_1009.f3", print_hash_value);
    transparent_crc(g_1009.f4, "g_1009.f4", print_hash_value);
    transparent_crc(g_1039.f0, "g_1039.f0", print_hash_value);
    transparent_crc(g_1047, "g_1047", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 263
   depth: 1, occurrence: 1
XXX total union variables: 1

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 1
breakdown:
   indirect level: 0, occurrence: 1
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 1
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 39
breakdown:
   depth: 1, occurrence: 76
   depth: 2, occurrence: 19
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 12, occurrence: 1
   depth: 16, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 2
   depth: 24, occurrence: 4
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 36, occurrence: 1
   depth: 39, occurrence: 1

XXX total number of pointers: 272

XXX times a variable address is taken: 708
XXX times a pointer is dereferenced on RHS: 98
breakdown:
   depth: 1, occurrence: 63
   depth: 2, occurrence: 31
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 118
breakdown:
   depth: 1, occurrence: 105
   depth: 2, occurrence: 12
   depth: 3, occurrence: 1
XXX times a pointer is compared with null: 17
XXX times a pointer is compared with address of another variable: 1
XXX times a pointer is compared with another pointer: 7
XXX times a pointer is qualified to be dereferenced: 2852

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 451
   level: 2, occurrence: 146
   level: 3, occurrence: 15
   level: 4, occurrence: 21
   level: 5, occurrence: 15
XXX number of pointers point to pointers: 108
XXX number of pointers point to scalars: 163
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.2
XXX average alias set size: 1.37

XXX times a non-volatile is read: 720
XXX times a non-volatile is write: 377
XXX times a volatile is read: 4
XXX    times read thru a pointer: 0
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 333
XXX percentage of non-volatile access: 99.5

XXX forward jumps: 0
XXX backward jumps: 4

XXX stmts: 74
XXX max block depth: 3
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 22
   depth: 2, occurrence: 15
   depth: 3, occurrence: 9

XXX percentage a fresh-made variable is used: 21.1
XXX percentage an existing variable is used: 78.9
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


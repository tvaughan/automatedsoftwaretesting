#!/bin/bash
set -e
PATH_TO_CSMITH="../csmith/data"
testcases=("vect")
flags=("O3")
size=100

for i in $(seq 0 $size);
do
    cp "$PATH_TO_CSMITH/random$i/random$i-gcc.c" random$i.c
#mv ./test_case$i/random$i-gcc.c ./test_case$i/random$i.c
## Create a copy of our test cases since they will be modified 
done

## Next we have to run creduce on these test cases
for i in $(seq 0 $size);
do
    for flag in ${flags[@]}
    do ## Change the compilation flags for a better analysis
    sed -i "s|$(grep "FLAGS=" ./script1.sh)|FLAGS="$flag"|" ./script1.sh   
        for test in ${testcases[@]};
        do ## Change the testcase folder and filename
        sed -i "s|$(grep "TESTCASE=" ./script1.sh)|TESTCASE="$test"|" ./script1.sh
        sed -i "s|$(grep "FILE_NAME=" ./script1.sh)|FILE_NAME="random$i"|" ./script1.sh
        ## create the copy and specify which reduction we run on it for later use afterwards
        cp random$i.c random$i-$test-$flag.c
        echo reducing test number $i in regards to $test with flag $flag
        creduce --n 32 ./script1.sh random$i-$test-$flag.c || true
        rm random$i-$test-$flag.c.orig
        done
    done
done

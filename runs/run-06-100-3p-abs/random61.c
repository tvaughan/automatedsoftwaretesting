/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3413761932
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   const volatile float  f0;
   const volatile signed f1 : 1;
   volatile uint8_t  f2;
   uint32_t  f3;
   uint32_t  f4;
   const volatile uint32_t  f5;
   uint8_t  f6;
};

/* --- GLOBAL VARIABLES --- */
static int8_t g_2[4] = {0x30L,0x30L,0x30L,0x30L};
static volatile int32_t g_3 = (-4L);/* VOLATILE GLOBAL g_3 */
static volatile int32_t g_4[6][2] = {{0xDB5CABFCL,0xDB5CABFCL},{0xDB5CABFCL,0x580849F8L},{0x1F07B4BCL,0x2B3B7BEDL},{0x580849F8L,0x2B3B7BEDL},{0x1F07B4BCL,0x580849F8L},{0xDB5CABFCL,0xDB5CABFCL}};
static volatile int32_t g_5 = 6L;/* VOLATILE GLOBAL g_5 */
static int32_t g_6[6][5] = {{0x37400631L,0x37400631L,0x927213ECL,0x37400631L,0x37400631L},{0L,5L,0L,4L,0x193E0566L},{0x37400631L,0x6E229EDCL,0x6E229EDCL,0x37400631L,0x6E229EDCL},{0x193E0566L,5L,(-8L),5L,0x193E0566L},{0x6E229EDCL,0x37400631L,0x6E229EDCL,0x6E229EDCL,0x37400631L},{0x193E0566L,4L,0L,5L,0L}};
static uint8_t g_47 = 0xF0L;
static uint64_t g_65 = 0xDB5E4941AE2D2C90LL;
static uint8_t g_69[1] = {255UL};
static uint32_t g_70 = 18446744073709551615UL;
static uint16_t g_74[6] = {65530UL,65530UL,65530UL,65530UL,65530UL,65530UL};
static int8_t g_88 = (-5L);
static uint16_t g_129 = 0x1754L;
static uint16_t g_138 = 65527UL;
static uint64_t *g_146 = &g_65;
static uint64_t **g_145[9][7][4] = {{{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,(void*)0,&g_146,&g_146},{&g_146,(void*)0,(void*)0,&g_146}},{{(void*)0,(void*)0,&g_146,&g_146},{(void*)0,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,(void*)0,(void*)0,&g_146},{&g_146,(void*)0,(void*)0,&g_146},{&g_146,(void*)0,&g_146,&g_146}},{{(void*)0,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{(void*)0,&g_146,&g_146,&g_146}},{{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{(void*)0,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,&g_146}},{{&g_146,&g_146,&g_146,&g_146},{(void*)0,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,(void*)0},{&g_146,&g_146,(void*)0,&g_146},{&g_146,&g_146,&g_146,(void*)0},{&g_146,(void*)0,&g_146,&g_146},{&g_146,(void*)0,&g_146,(void*)0}},{{(void*)0,&g_146,&g_146,&g_146},{(void*)0,&g_146,(void*)0,(void*)0},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,&g_146}},{{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,(void*)0,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{(void*)0,&g_146,&g_146,&g_146}},{{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{(void*)0,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,&g_146,&g_146},{&g_146,&g_146,(void*)0,&g_146},{(void*)0,&g_146,&g_146,(void*)0}},{{&g_146,&g_146,&g_146,(void*)0},{&g_146,&g_146,&g_146,(void*)0},{&g_146,&g_146,&g_146,(void*)0},{&g_146,&g_146,&g_146,(void*)0},{&g_146,&g_146,&g_146,(void*)0},{&g_146,&g_146,(void*)0,&g_146},{&g_146,&g_146,&g_146,&g_146}}};
static uint64_t ***g_144 = &g_145[2][2][1];
static const int8_t g_156[6] = {0xC8L,(-1L),(-1L),0xC8L,(-1L),(-1L)};
static uint16_t g_178 = 0x8734L;
static uint64_t **g_190 = &g_146;
static uint64_t ***g_189[8] = {&g_190,(void*)0,(void*)0,&g_190,(void*)0,(void*)0,&g_190,(void*)0};
static uint8_t g_200 = 255UL;
static int32_t g_207 = 0x792A7B37L;
static float g_208[6] = {(-0x6.Dp-1),(-0x6.Dp-1),(-0x6.Dp-1),(-0x6.Dp-1),(-0x6.Dp-1),(-0x6.Dp-1)};
static uint64_t g_211 = 4UL;
static uint16_t *g_246 = &g_178;
static int64_t g_252 = 0L;
static volatile int8_t g_264 = 1L;/* VOLATILE GLOBAL g_264 */
static volatile int8_t g_265 = 1L;/* VOLATILE GLOBAL g_265 */
static volatile int8_t g_266 = 0xC9L;/* VOLATILE GLOBAL g_266 */
static volatile int8_t g_267[2] = {(-10L),(-10L)};
static volatile int8_t g_268 = (-1L);/* VOLATILE GLOBAL g_268 */
static volatile int8_t g_269[9][3][7] = {{{(-1L),1L,0xFDL,0xEAL,1L,0xEAL,0xFDL},{5L,5L,(-8L),0xCFL,0x90L,(-8L),0x90L},{1L,0xFDL,0xFDL,1L,0xEAL,(-1L),1L}},{{0x7DL,0x90L,(-1L),(-1L),0x90L,0x7DL,0x90L},{0x28L,1L,0x67L,1L,1L,0x67L,1L},{0x90L,0x90L,0x7DL,0x90L,(-1L),(-1L),0x90L}},{{(-1L),1L,(-1L),0xEAL,1L,0xFDL,0xFDL},{0xCFL,0x90L,(-8L),0x90L,0xCFL,(-8L),5L},{1L,0xFDL,0xEAL,1L,0xEAL,0xFDL,1L}},{{0x7DL,5L,0x90L,(-1L),5L,(-1L),0x90L},{1L,1L,0x67L,1L,0x28L,0x67L,0x28L},{0xCFL,0x90L,0x90L,0xCFL,(-1L),0x7DL,0xCFL}},{{(-1L),0x28L,0xEAL,0xEAL,0x28L,(-1L),0xFDL},{0x90L,0xCFL,(-8L),5L,5L,(-8L),0xCFL},{0x28L,0xFDL,0xEBL,0xEAL,5L,5L,0xEAL}},{{0xE6L,0x90L,0xE6L,(-1L),0x90L,(-8L),(-8L)},{0xFDL,0xEAL,1L,0xEAL,0xFDL,1L,(-1L)},{0x7DL,(-8L),(-1L),0x7DL,(-1L),(-8L),0x7DL}},{{0xEBL,(-1L),0x67L,5L,(-1L),5L,0x67L},{0x7DL,0x7DL,5L,0x90L,(-1L),5L,(-1L)},{0xFDL,0x67L,0x67L,0xFDL,5L,0xEBL,0xFDL}},{{0xE6L,(-1L),(-1L),(-1L),(-1L),0xE6L,(-8L)},{0xEAL,0xFDL,1L,(-1L),(-1L),1L,0xFDL},{(-1L),(-8L),0xE6L,(-1L),(-1L),(-1L),(-1L)}},{{0xEBL,0xFDL,0xEBL,5L,0xFDL,0x67L,0x67L},{0x90L,(-1L),5L,(-1L),0x90L,5L,0x7DL},{(-1L),0x67L,5L,(-1L),5L,0x67L,(-1L)}}};
static volatile int8_t g_270 = 0x04L;/* VOLATILE GLOBAL g_270 */
static volatile int8_t g_271 = (-1L);/* VOLATILE GLOBAL g_271 */
static volatile int8_t g_272 = 0x4FL;/* VOLATILE GLOBAL g_272 */
static volatile int8_t g_273[10] = {(-1L),9L,0x5FL,9L,(-1L),(-1L),9L,0x5FL,9L,(-1L)};
static volatile int8_t g_274 = (-10L);/* VOLATILE GLOBAL g_274 */
static volatile int8_t g_275[3] = {0x28L,0x28L,0x28L};
static volatile int8_t g_276 = 0xE0L;/* VOLATILE GLOBAL g_276 */
static volatile int8_t g_277 = 0L;/* VOLATILE GLOBAL g_277 */
static volatile int8_t g_278 = 4L;/* VOLATILE GLOBAL g_278 */
static volatile int8_t g_279 = 0xD2L;/* VOLATILE GLOBAL g_279 */
static volatile int8_t g_280 = 0L;/* VOLATILE GLOBAL g_280 */
static volatile int8_t g_281[6] = {0x09L,0x09L,0x52L,0x09L,0x09L,0x52L};
static volatile int8_t g_282 = 0x52L;/* VOLATILE GLOBAL g_282 */
static volatile int8_t g_283 = 6L;/* VOLATILE GLOBAL g_283 */
static volatile int8_t g_284 = (-2L);/* VOLATILE GLOBAL g_284 */
static volatile int8_t g_285 = (-1L);/* VOLATILE GLOBAL g_285 */
static volatile int8_t g_286 = 1L;/* VOLATILE GLOBAL g_286 */
static volatile int8_t g_287 = (-10L);/* VOLATILE GLOBAL g_287 */
static volatile int8_t g_288 = 2L;/* VOLATILE GLOBAL g_288 */
static volatile int8_t g_289 = 0x73L;/* VOLATILE GLOBAL g_289 */
static volatile int8_t g_290 = 8L;/* VOLATILE GLOBAL g_290 */
static volatile int8_t g_291 = (-9L);/* VOLATILE GLOBAL g_291 */
static volatile int8_t g_292[3][2] = {{0xD8L,0xD8L},{0xD8L,0xD8L},{0xD8L,0xD8L}};
static volatile int8_t g_293[3] = {(-1L),(-1L),(-1L)};
static volatile int8_t g_294 = 0xE9L;/* VOLATILE GLOBAL g_294 */
static volatile int8_t g_295 = 0x5BL;/* VOLATILE GLOBAL g_295 */
static volatile int8_t g_296 = (-1L);/* VOLATILE GLOBAL g_296 */
static volatile int8_t g_297 = (-1L);/* VOLATILE GLOBAL g_297 */
static volatile int8_t g_298 = (-1L);/* VOLATILE GLOBAL g_298 */
static volatile int8_t g_299[8][2][9] = {{{2L,(-1L),0xA1L,(-1L),(-1L),1L,0x47L,2L,0xE7L},{(-9L),(-8L),0x5CL,(-3L),0x1FL,1L,2L,1L,(-1L)}},{{(-1L),0x1FL,1L,8L,(-1L),0x7CL,(-1L),0xF6L,0x5CL},{0x81L,1L,1L,0x60L,(-5L),(-9L),0x1FL,(-1L),0xABL}},{{0x19L,(-1L),0x5CL,(-1L),0xB1L,(-1L),0x5CL,(-1L),0x19L},{(-4L),(-2L),0xA1L,1L,6L,1L,0xBCL,0xA2L,1L}},{{(-1L),(-4L),1L,(-9L),(-1L),0xD0L,(-1L),0x0EL,5L},{(-4L),6L,0xBCL,0xABL,0x19L,(-2L),0xF6L,0xD0L,0x47L}},{{0x19L,0xFBL,0xE7L,1L,(-1L),(-1L),0xD0L,0x60L,1L},{0x19L,1L,1L,0x22L,6L,0xABL,1L,0x60L,0x47L}},{{0x19L,0x47L,0x81L,0x5CL,5L,0xC1L,1L,1L,0xC1L},{0xF6L,0x81L,(-9L),0x81L,0xF6L,8L,(-9L),(-8L),0xD0L}},{{0x22L,0x55L,0x0EL,(-4L),1L,(-1L),0xBCL,0x7CL,0x5CL},{0x7CL,0xABL,0x19L,1L,1L,8L,0xFBL,1L,(-4L)}},{{(-1L),1L,0x47L,2L,0xE7L,0xC1L,(-1L),0x1FL,0x7CL},{1L,0xF6L,2L,(-1L),(-1L),0xABL,(-3L),(-5L),9L}}};
static volatile int8_t g_300 = 0xD0L;/* VOLATILE GLOBAL g_300 */
static volatile int8_t g_301 = 0L;/* VOLATILE GLOBAL g_301 */
static volatile int8_t g_302 = 0x6AL;/* VOLATILE GLOBAL g_302 */
static volatile int8_t g_303 = 1L;/* VOLATILE GLOBAL g_303 */
static volatile int8_t g_304 = 2L;/* VOLATILE GLOBAL g_304 */
static volatile int8_t g_305 = 1L;/* VOLATILE GLOBAL g_305 */
static volatile int8_t g_306 = (-3L);/* VOLATILE GLOBAL g_306 */
static volatile int8_t g_307[2] = {0x91L,0x91L};
static volatile int8_t g_308 = 0L;/* VOLATILE GLOBAL g_308 */
static volatile int8_t g_309 = 1L;/* VOLATILE GLOBAL g_309 */
static volatile int8_t g_310 = (-1L);/* VOLATILE GLOBAL g_310 */
static volatile int8_t g_311 = 0L;/* VOLATILE GLOBAL g_311 */
static volatile int8_t g_312 = 1L;/* VOLATILE GLOBAL g_312 */
static volatile int8_t g_313 = 1L;/* VOLATILE GLOBAL g_313 */
static volatile int8_t g_314 = 0L;/* VOLATILE GLOBAL g_314 */
static volatile int8_t g_315[4][5][3] = {{{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)}},{{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)}},{{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)}},{{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)},{(-1L),0xB5L,(-1L)}}};
static volatile int8_t g_316 = 0x61L;/* VOLATILE GLOBAL g_316 */
static volatile int8_t g_317 = (-8L);/* VOLATILE GLOBAL g_317 */
static volatile int8_t g_318 = 0xBBL;/* VOLATILE GLOBAL g_318 */
static volatile int8_t g_319 = 0xFFL;/* VOLATILE GLOBAL g_319 */
static volatile int8_t g_320[7] = {0x24L,0xE0L,0x24L,0x24L,0xE0L,0x24L,0x24L};
static volatile int8_t g_321 = (-1L);/* VOLATILE GLOBAL g_321 */
static volatile int8_t g_322 = (-7L);/* VOLATILE GLOBAL g_322 */
static volatile int8_t g_323 = 9L;/* VOLATILE GLOBAL g_323 */
static volatile int8_t g_324 = 0xB5L;/* VOLATILE GLOBAL g_324 */
static volatile int8_t g_325 = 0xB8L;/* VOLATILE GLOBAL g_325 */
static volatile int8_t *g_263[8][7][3] = {{{&g_269[0][1][2],&g_274,&g_312},{(void*)0,&g_271,&g_275[2]},{&g_287,&g_279,&g_302},{&g_281[1],(void*)0,&g_284},{&g_283,&g_319,&g_272},{(void*)0,(void*)0,&g_270},{(void*)0,&g_279,&g_277}},{{&g_285,&g_271,(void*)0},{&g_325,&g_274,&g_317},{(void*)0,&g_285,(void*)0},{&g_309,&g_304,&g_277},{&g_316,&g_276,&g_270},{&g_274,(void*)0,&g_272},{&g_297,(void*)0,&g_284}},{{&g_274,&g_299[0][0][2],&g_302},{&g_316,&g_307[1],&g_275[2]},{&g_309,&g_325,&g_312},{(void*)0,&g_303,&g_316},{&g_280,&g_280,(void*)0},{&g_298,&g_324,&g_322},{(void*)0,&g_315[1][1][0],&g_279}},{{(void*)0,&g_318,(void*)0},{&g_296,(void*)0,&g_279},{&g_294,&g_291,&g_322},{&g_301,&g_321,(void*)0},{&g_318,&g_298,&g_316},{(void*)0,&g_289,&g_269[0][1][2]},{&g_318,&g_286,(void*)0}},{{&g_301,(void*)0,&g_319},{&g_294,(void*)0,&g_297},{&g_296,(void*)0,&g_287},{(void*)0,(void*)0,&g_285},{(void*)0,(void*)0,&g_292[2][0]},{&g_298,&g_286,&g_307[1]},{&g_280,&g_289,&g_274}},{{&g_268,&g_298,&g_307[1]},{&g_265,&g_321,&g_292[2][0]},{&g_273[2],&g_291,&g_285},{&g_289,(void*)0,&g_287},{&g_313,&g_318,&g_297},{&g_289,&g_315[1][1][0],&g_319},{&g_273[2],&g_324,(void*)0}},{{&g_265,&g_280,&g_269[0][1][2]},{&g_268,&g_303,&g_316},{&g_280,&g_280,(void*)0},{&g_298,&g_324,&g_322},{(void*)0,&g_315[1][1][0],&g_279},{(void*)0,&g_318,(void*)0},{&g_296,(void*)0,&g_279}},{{&g_294,&g_291,&g_322},{&g_301,&g_321,(void*)0},{&g_318,&g_298,&g_316},{(void*)0,&g_289,&g_269[0][1][2]},{&g_318,&g_286,(void*)0},{&g_301,(void*)0,&g_319},{&g_294,(void*)0,&g_297}}};
static volatile int8_t * const *g_262 = &g_263[1][3][2];
static uint32_t g_337 = 0UL;
static int32_t *g_353 = &g_207;
static int32_t **g_352 = &g_353;
static uint64_t *g_361 = (void*)0;
static int8_t g_419 = 0xFDL;
static uint64_t g_435 = 0x4CCF6A44C2CC62D4LL;
static int8_t * const g_442[10] = {(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88,(void*)0,&g_88};
static int8_t * const *g_441 = &g_442[0];
static int8_t * const * volatile *g_440[9] = {&g_441,&g_441,&g_441,&g_441,&g_441,&g_441,&g_441,&g_441,&g_441};
static int8_t * const * volatile ** volatile g_439 = &g_440[7];/* VOLATILE GLOBAL g_439 */
static int8_t * const * volatile ** volatile *g_438 = &g_439;
static int32_t g_454 = (-5L);
static struct S0 g_513 = {-0x9.9p-1,0,0x46L,0UL,0xF8292C22L,1UL,249UL};/* VOLATILE GLOBAL g_513 */
static uint8_t g_518 = 6UL;
static int32_t *g_548 = (void*)0;
static int32_t ***g_591 = (void*)0;
static uint16_t * volatile *g_601 = &g_246;
static float g_609 = 0x0.7p-1;
static volatile struct S0 g_660 = {0x2.8p-1,0,0UL,5UL,9UL,0UL,252UL};/* VOLATILE GLOBAL g_660 */
static volatile struct S0 *g_659 = &g_660;
static const int32_t *g_689 = &g_207;
static uint32_t g_736 = 0xCE505CDEL;
static uint64_t ****g_758 = (void*)0;
static uint64_t *****g_757 = &g_758;
static int16_t g_787 = (-6L);
static int16_t g_790 = (-10L);
static int8_t g_792 = 0xDBL;
static uint32_t g_793[4][10] = {{0xC805BC60L,0x23BDCE60L,0xE6E81EE3L,0UL,0x36846721L,0UL,0xE6E81EE3L,0x23BDCE60L,0xC805BC60L,0x6FCF09A6L},{5UL,1UL,0xE6E81EE3L,0x36846721L,0x23BDCE60L,0x23BDCE60L,0x36846721L,0xE6E81EE3L,1UL,5UL},{1UL,0UL,0xC805BC60L,0x36846721L,0UL,5UL,0UL,0x36846721L,0xC805BC60L,0UL},{0x6FCF09A6L,0xE6E81EE3L,5UL,0UL,0UL,0x9E4BD8D1L,0x9E4BD8D1L,0UL,0UL,5UL}};
static int32_t g_840[3][8] = {{0xC81C6579L,0x61D68B57L,0xA2AF6878L,0L,0x61D68B57L,0L,0xA2AF6878L,0x61D68B57L},{0xAF682680L,0xA2AF6878L,0xC81C6579L,0xAF682680L,0L,0L,0xAF682680L,0xC81C6579L},{0xC81C6579L,0xC81C6579L,0x61D68B57L,0xA2AF6878L,0L,0x61D68B57L,0L,0xA2AF6878L}};
static int8_t g_956 = (-5L);
static struct S0 g_991 = {0x0.5p-1,-0,1UL,18446744073709551615UL,0x93494288L,0UL,1UL};/* VOLATILE GLOBAL g_991 */
static struct S0 *g_990 = &g_991;
static int32_t g_1003 = (-6L);
static int32_t * const g_1002 = &g_1003;
static int32_t * const *g_1001 = &g_1002;
static int32_t * const **g_1000 = &g_1001;
static int32_t * const ***g_999 = &g_1000;
static int32_t * const ****g_998 = &g_999;
static int64_t g_1026 = 0x8E7884B404D78CE1LL;
static uint8_t g_1120[4] = {0UL,0UL,0UL,0UL};
static volatile uint32_t g_1127 = 18446744073709551615UL;/* VOLATILE GLOBAL g_1127 */
static const volatile uint32_t * volatile g_1126 = &g_1127;/* VOLATILE GLOBAL g_1126 */
static const volatile uint32_t * volatile *g_1125[3] = {&g_1126,&g_1126,&g_1126};
static uint32_t *g_1189 = &g_513.f4;
static uint32_t * volatile *g_1188 = &g_1189;
static int16_t g_1245 = (-2L);
static float g_1247[6] = {0x6.F36C61p+37,0x6.F36C61p+37,0x6.F36C61p+37,0x6.F36C61p+37,0x6.F36C61p+37,0x6.F36C61p+37};
static float * volatile g_1302 = &g_609;/* VOLATILE GLOBAL g_1302 */
static volatile uint32_t g_1342[7][6] = {{0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L},{0x5AAF153EL,0x57616FD4L,0x5AAF153EL,0x57616FD4L,0x5AAF153EL,0x57616FD4L},{0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L},{0x5AAF153EL,0x57616FD4L,0x5AAF153EL,0x57616FD4L,0x5AAF153EL,0x57616FD4L},{0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L},{0x5AAF153EL,0x57616FD4L,0x5AAF153EL,0x57616FD4L,0x5AAF153EL,0x57616FD4L},{0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L,0x77BE82FFL,0x57616FD4L}};
static volatile uint32_t *g_1341 = &g_1342[0][1];
static volatile uint32_t ** volatile g_1340 = &g_1341;/* VOLATILE GLOBAL g_1340 */
static volatile uint32_t ** volatile *g_1339 = &g_1340;
static volatile uint32_t ** volatile **g_1338[8][5] = {{&g_1339,(void*)0,&g_1339,(void*)0,&g_1339},{&g_1339,&g_1339,&g_1339,(void*)0,&g_1339},{&g_1339,&g_1339,&g_1339,&g_1339,&g_1339},{&g_1339,&g_1339,&g_1339,&g_1339,&g_1339},{(void*)0,&g_1339,&g_1339,&g_1339,&g_1339},{&g_1339,&g_1339,&g_1339,&g_1339,&g_1339},{(void*)0,&g_1339,(void*)0,&g_1339,&g_1339},{(void*)0,&g_1339,&g_1339,&g_1339,(void*)0}};
static struct S0 g_1347 = {-0x8.Dp+1,-0,0x55L,18446744073709551615UL,0xB5B77EAFL,0UL,0x5EL};/* VOLATILE GLOBAL g_1347 */
static struct S0 ** volatile g_1349 = &g_990;/* VOLATILE GLOBAL g_1349 */
static int16_t *g_1384 = &g_1245;
static int16_t **g_1383[8] = {&g_1384,&g_1384,&g_1384,&g_1384,&g_1384,&g_1384,&g_1384,&g_1384};
static int16_t *** volatile g_1385 = (void*)0;/* VOLATILE GLOBAL g_1385 */
static volatile struct S0 g_1395[3][2] = {{{0x1.9p-1,-0,247UL,0UL,9UL,18446744073709551607UL,0x41L},{0x1.9p-1,-0,247UL,0UL,9UL,18446744073709551607UL,0x41L}},{{-0x4.Fp-1,0,0x0FL,0xF0E680A2L,4294967295UL,18446744073709551615UL,0x94L},{0x1.9p-1,-0,247UL,0UL,9UL,18446744073709551607UL,0x41L}},{{0x1.9p-1,-0,247UL,0UL,9UL,18446744073709551607UL,0x41L},{-0x4.Fp-1,0,0x0FL,0xF0E680A2L,4294967295UL,18446744073709551615UL,0x94L}}};
static int8_t g_1447 = 0xE4L;
static uint32_t g_1529 = 0UL;
static volatile int8_t g_1595 = 0xABL;/* VOLATILE GLOBAL g_1595 */
static int32_t ** const *g_1652 = (void*)0;
static int32_t ** volatile g_1687[1] = {&g_548};
static int32_t ** volatile g_1689 = &g_548;/* VOLATILE GLOBAL g_1689 */
static float g_1696[2][4] = {{(-0x1.Dp+1),(-0x1.Dp+1),(-0x1.Dp+1),(-0x1.Dp+1)},{(-0x1.Dp+1),(-0x1.Dp+1),(-0x1.Dp+1),(-0x1.Dp+1)}};
static int16_t ***g_1708 = &g_1383[5];
static int16_t ****g_1707 = &g_1708;
static int16_t ****g_1711[8] = {&g_1708,&g_1708,&g_1708,&g_1708,&g_1708,&g_1708,&g_1708,&g_1708};
static int16_t ***** volatile g_1710 = &g_1711[2];/* VOLATILE GLOBAL g_1710 */
static int32_t ** volatile g_1713 = &g_548;/* VOLATILE GLOBAL g_1713 */
static volatile uint8_t *g_1717 = &g_991.f2;
static volatile uint8_t * const * volatile g_1716 = &g_1717;/* VOLATILE GLOBAL g_1716 */
static volatile uint8_t * const * volatile * volatile g_1715 = &g_1716;/* VOLATILE GLOBAL g_1715 */
static uint64_t ***g_1736 = (void*)0;
static int32_t **g_1807 = &g_548;
static int32_t *** const g_1806[5] = {&g_1807,&g_1807,&g_1807,&g_1807,&g_1807};
static int32_t *** const *g_1805 = &g_1806[3];
static float g_1812 = 0x0.4p-1;
static volatile uint8_t ***g_1921 = (void*)0;
static int8_t *g_1943[2] = {(void*)0,(void*)0};
static int8_t **g_1942[7][10][3] = {{{&g_1943[1],(void*)0,&g_1943[0]},{&g_1943[0],&g_1943[1],&g_1943[1]},{(void*)0,&g_1943[1],&g_1943[1]},{&g_1943[0],&g_1943[0],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[0]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[0]},{(void*)0,&g_1943[1],&g_1943[1]},{&g_1943[1],(void*)0,&g_1943[0]},{&g_1943[1],(void*)0,&g_1943[1]}},{{&g_1943[0],&g_1943[1],&g_1943[1]},{(void*)0,&g_1943[1],&g_1943[1]},{&g_1943[0],&g_1943[1],&g_1943[0]},{&g_1943[1],(void*)0,&g_1943[1]},{&g_1943[0],(void*)0,&g_1943[1]},{&g_1943[0],&g_1943[1],&g_1943[1]},{&g_1943[1],(void*)0,&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]}},{{&g_1943[1],(void*)0,&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{(void*)0,&g_1943[1],(void*)0},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[0],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]}},{{(void*)0,(void*)0,&g_1943[0]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[0],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{(void*)0,(void*)0,&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[0]},{&g_1943[1],&g_1943[1],&g_1943[1]}},{{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],(void*)0},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[0],(void*)0,&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],(void*)0,&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]}},{{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{(void*)0,&g_1943[1],(void*)0},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[0],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{(void*)0,(void*)0,&g_1943[0]},{&g_1943[1],&g_1943[1],&g_1943[1]}},{{&g_1943[0],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{(void*)0,(void*)0,&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[0]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1]}}};
static int8_t ***g_1941 = &g_1942[5][8][0];
static float g_1983 = 0x6.0p+1;
static int8_t * const ** const  volatile g_1990 = &g_441;/* VOLATILE GLOBAL g_1990 */
static float * volatile g_2010 = &g_208[0];/* VOLATILE GLOBAL g_2010 */
static float * volatile g_2054 = &g_1983;/* VOLATILE GLOBAL g_2054 */
static int32_t g_2060 = 0xD4C74F6DL;


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static int32_t  func_7(int32_t  p_8, uint16_t  p_9, uint64_t  p_10, const uint32_t  p_11);
static int32_t  func_12(uint8_t  p_13, int8_t  p_14, int64_t  p_15, float  p_16);
static uint8_t  func_19(uint32_t  p_20, uint32_t  p_21, int16_t  p_22);
static uint32_t  func_23(int32_t  p_24, const int32_t  p_25, uint16_t  p_26);
static uint16_t  func_29(int16_t  p_30, float  p_31);
static int32_t  func_32(float  p_33);
static int32_t  func_37(const int32_t  p_38);
static int32_t  func_41(int32_t  p_42, int8_t  p_43, uint8_t  p_44, float  p_45);
static int16_t  func_52(float  p_53, uint16_t  p_54, uint8_t * p_55);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_513.f4 g_1349 g_990 g_1347.f4 g_353 g_207 g_246 g_178 g_1707 g_999 g_1000 g_1001 g_1002 g_1003 g_991 g_337 g_601 g_1120 g_1189 g_998 g_1715 g_1716 g_1717 g_200 g_47 g_1384 g_518 g_1807 g_146 g_65 g_1245 g_1302 g_609 g_208 g_2 g_1026 g_88 g_1188 g_956 g_548 g_1805 g_1806 g_2060
 * writes: g_6 g_513.f4 g_1347.f4 g_207 g_1708 g_1003 g_1120 g_252 g_1245 g_518 g_548 g_956 g_736 g_178
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    uint8_t l_36 = 0x46L;
    int16_t l_1531 = 0x478AL;
    int16_t l_1702 = (-6L);
    const int32_t l_1712 = 0L;
    uint16_t l_1733 = 0x1219L;
    int32_t l_1745 = 0x1BFA5635L;
    int32_t l_1746 = 0xACD99057L;
    int8_t l_1751 = 0xE3L;
    int32_t l_1762 = 0xB71C51CEL;
    int32_t l_1763 = (-1L);
    int32_t l_1764 = 0x91F018BDL;
    int32_t l_1765 = 0x79099F9BL;
    int32_t l_1767 = (-10L);
    uint32_t *l_1786[8] = {&g_1347.f3,&g_1347.f3,&g_1347.f3,&g_1347.f3,&g_1347.f3,&g_1347.f3,&g_1347.f3,&g_1347.f3};
    uint32_t **l_1785 = &l_1786[6];
    uint32_t ***l_1784 = &l_1785;
    uint64_t l_1808[3];
    const uint32_t l_1813[7][1][3] = {{{0xD4157EEFL,0x599721E4L,0xD4157EEFL}},{{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}},{{0xA43C949AL,0x599721E4L,0xA43C949AL}},{{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}},{{0xD4157EEFL,0x599721E4L,0xD4157EEFL}},{{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}},{{0xA43C949AL,0x599721E4L,0xA43C949AL}}};
    int32_t l_1821[8][8] = {{5L,3L,(-1L),2L,0xC08C89E2L,(-1L),2L,0xA28265ACL},{3L,(-1L),(-1L),0L,0L,0x85C17C91L,0xC08C89E2L,(-1L)},{3L,0xA28265ACL,(-8L),0xC08C89E2L,0xC08C89E2L,(-8L),0xA28265ACL,3L},{(-1L),5L,(-1L),0x85C17C91L,3L,3L,(-1L),0xA28265ACL},{5L,0x85C17C91L,2L,0x22C2F79BL,0x85C17C91L,3L,0xC08C89E2L,3L},{0L,5L,4L,5L,0L,(-8L),0x22C2F79BL,0L},{3L,0xA28265ACL,(-1L),3L,3L,0x85C17C91L,(-1L),5L},{0xA28265ACL,(-1L),(-1L),0x22C2F79BL,(-1L),(-1L),0x22C2F79BL,(-1L)}};
    int16_t ***l_1826 = &g_1383[6];
    int32_t *l_1850[2][5] = {{&g_840[1][3],&g_840[1][3],&g_840[1][3],&g_840[1][3],&g_840[1][3]},{&g_840[2][5],(void*)0,&g_840[2][5],(void*)0,&g_840[2][5]}};
    uint8_t l_1868 = 1UL;
    int64_t l_1869[9][4] = {{0x89564304387277C1LL,0xAFDAB487D721D096LL,(-1L),(-1L)},{0x45F1831BBC2F2F6FLL,0x45F1831BBC2F2F6FLL,0xDA05BF953B07C3EFLL,0xAFDAB487D721D096LL},{0xAFDAB487D721D096LL,0x89564304387277C1LL,0xDA05BF953B07C3EFLL,0x89564304387277C1LL},{0x45F1831BBC2F2F6FLL,1L,(-1L),0xDA05BF953B07C3EFLL},{0x89564304387277C1LL,1L,1L,0x89564304387277C1LL},{1L,0x89564304387277C1LL,0x45F1831BBC2F2F6FLL,0xAFDAB487D721D096LL},{1L,0x45F1831BBC2F2F6FLL,1L,(-1L)},{0x89564304387277C1LL,0xAFDAB487D721D096LL,(-1L),(-1L)},{0x45F1831BBC2F2F6FLL,0x45F1831BBC2F2F6FLL,0xDA05BF953B07C3EFLL,0xAFDAB487D721D096LL}};
    int64_t *l_1870 = &g_252;
    int32_t l_1871 = 1L;
    const uint8_t l_1872 = 1UL;
    uint64_t l_1896[5];
    int32_t l_1898 = 1L;
    uint32_t l_1899 = 0xB4E053BFL;
    uint16_t **l_2008[4][10] = {{&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,(void*)0},{&g_246,&g_246,&g_246,&g_246,&g_246,(void*)0,&g_246,&g_246,&g_246,&g_246},{&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246},{&g_246,&g_246,&g_246,(void*)0,&g_246,&g_246,&g_246,&g_246,&g_246,&g_246}};
    int8_t ****l_2040 = (void*)0;
    int8_t *****l_2039 = &l_2040;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1808[i] = 18446744073709551615UL;
    for (i = 0; i < 5; i++)
        l_1896[i] = 0x7495E07A4C689727LL;
    for (g_6[0][3] = 3; (g_6[0][3] >= 0); g_6[0][3] -= 1)
    { /* block id: 3 */
        int32_t l_1030 = 9L;
        int64_t *l_1031[2];
        uint32_t *l_1528 = &g_1529;
        int32_t l_1530 = 1L;
        int16_t l_1550 = 0x0C25L;
        int32_t *l_1701 = &g_454;
        int32_t *l_1706 = &g_840[0][1];
        uint8_t l_1747 = 255UL;
        int32_t l_1755 = 0xA7112C67L;
        int32_t l_1757 = 0x45F0CE14L;
        int32_t l_1758 = 0x9D10CA35L;
        int32_t l_1759 = 0x35D7BA3AL;
        int32_t l_1766 = 0xCD605953L;
        int32_t l_1768 = 0xB5DD2099L;
        uint16_t l_1810 = 0x9537L;
        int i;
        for (i = 0; i < 2; i++)
            l_1031[i] = &g_1026;
    }
    for (g_513.f4 = 23; (g_513.f4 <= 1); g_513.f4--)
    { /* block id: 797 */
        uint64_t l_1816 = 0xA883033FED6DE1DBLL;
        uint32_t *l_1818 = &g_1347.f4;
        int8_t *l_1819 = (void*)0;
        int8_t *l_1820[5] = {&l_1751,&l_1751,&l_1751,&l_1751,&l_1751};
        int32_t l_1830 = (-1L);
        int i;
        if (((l_1821[7][2] = (((*l_1818) &= ((l_1816 , (((((void*)0 == (*g_1349)) < (safe_unary_minus_func_int16_t_s(l_1762))) <= l_1733) >= 1UL)) != l_1816)) < ((*g_353) |= (-1L)))) > (safe_sub_func_int64_t_s_s((safe_rshift_func_uint16_t_u_u(l_1764, (*g_246))), l_1762))))
        { /* block id: 801 */
            int16_t l_1829[8] = {0x56C8L,0x56C8L,0x56C8L,0x56C8L,0x56C8L,0x56C8L,0x56C8L,0x56C8L};
            int64_t *l_1847 = &g_252;
            int16_t *l_1848 = &l_1702;
            uint8_t *l_1849 = &g_518;
            int i;
            (*g_1707) = l_1826;
            (****g_999) &= l_1821[7][2];
            (*****g_998) = ((((*g_990) , (*g_246)) < (safe_sub_func_uint32_t_u_u(0xB9B243A0L, (g_337 & (l_1830 = l_1829[4]))))) || ((safe_mod_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(65528UL, ((safe_div_func_int16_t_s_s(l_1808[0], (**g_601))) != (g_1120[0]--)))), ((*l_1849) ^= (~(safe_sub_func_int8_t_s_s((((*l_1848) = ((*g_1384) = (((*l_1847) = (safe_rshift_func_uint8_t_u_u(((safe_sub_func_int32_t_s_s((~(((*g_1189) || (*****g_998)) | (***g_1715))), 0UL)) <= l_1829[5]), g_200))) >= g_47))) > l_1829[5]), l_1829[0])))))) <= l_1829[6]));
            return l_36;
        }
        else
        { /* block id: 812 */
            l_1850[0][2] = ((*g_1807) = (void*)0);
        }
    }
    if ((safe_mod_func_uint16_t_u_u((((**g_601) > ((*g_1384) = ((safe_add_func_uint32_t_u_u((safe_mod_func_int32_t_s_s(((*g_146) > ((*l_1870) = ((safe_add_func_int32_t_s_s(((((*g_1384) , (-(safe_add_func_float_f_f((*g_1302), ((safe_mul_func_float_f_f((safe_div_func_float_f_f((((-0x2.Fp-1) >= g_337) <= g_1347.f4), g_208[0])), (safe_mul_func_float_f_f(((g_2[1] >= g_1026) > g_88), l_1868)))) > g_991.f4))))) , 0x27909C01L) , (***g_1000)), (**g_1188))) || l_1869[6][1]))), (*g_1189))), (*g_1189))) && l_1871))) > 4294967294UL), l_1872)))
    { /* block id: 819 */
        int32_t l_1882 = 1L;
        uint32_t *l_1948 = &g_793[2][9];
        int64_t l_1950 = 6L;
        uint8_t *l_1982 = &g_518;
        int16_t *l_2001 = (void*)0;
        uint8_t **l_2006 = &l_1982;
        uint8_t ***l_2005 = &l_2006;
        int16_t *l_2007 = &g_1245;
        for (g_956 = 0; (g_956 >= (-11)); --g_956)
        { /* block id: 822 */
            uint32_t l_1877 = 18446744073709551615UL;
            int8_t ***l_1945 = &g_1942[6][2][2];
            int32_t l_1955 = (-1L);
            int32_t *l_1970 = &g_1003;
            int8_t * const *l_1989[3][8][8] = {{{&g_442[0],&g_1943[1],&g_1943[1],&g_442[4],&g_1943[1],&g_1943[0],&g_1943[0],&g_1943[0]},{&g_442[0],(void*)0,&g_442[6],&g_1943[0],&g_1943[1],&g_1943[1],&g_442[9],&g_1943[0]},{&g_442[0],&g_1943[1],&g_442[0],(void*)0,&g_1943[1],&g_1943[0],&g_1943[0],&g_1943[1]},{(void*)0,&g_1943[1],&g_1943[0],(void*)0,&g_1943[0],&g_442[3],&g_1943[1],&g_1943[1]},{&g_442[4],(void*)0,&g_1943[1],&g_442[4],&g_442[3],&g_442[0],&g_1943[1],&g_1943[1]},{&g_442[0],&g_442[0],&g_442[8],&g_1943[1],&g_442[3],&g_1943[1],&g_1943[1],&g_442[3]},{&g_442[4],&g_442[7],(void*)0,&g_1943[1],&g_442[6],&g_1943[1],&g_442[3],&g_442[0]},{&g_1943[1],&g_1943[1],&g_1943[1],(void*)0,&g_442[4],&g_1943[1],&g_442[3],&g_442[8]}},{{&g_1943[1],&g_1943[0],&g_1943[0],&g_442[9],&g_1943[0],(void*)0,&g_1943[1],&g_1943[1]},{&g_1943[1],&g_1943[0],&g_442[9],(void*)0,&g_442[0],&g_442[0],&g_442[4],&g_442[7]},{&g_442[4],&g_442[4],&g_1943[1],&g_1943[1],&g_442[0],&g_442[0],&g_442[9],(void*)0},{&g_1943[1],(void*)0,&g_442[0],&g_1943[1],(void*)0,(void*)0,&g_1943[1],&g_442[0]},{&g_1943[1],&g_1943[1],&g_1943[1],&g_1943[1],&g_1943[1],&g_442[4],&g_1943[1],&g_1943[1]},{&g_1943[0],&g_442[0],&g_442[3],&g_1943[1],&g_1943[1],(void*)0,&g_442[0],&g_1943[1]},{&g_442[0],&g_1943[1],&g_1943[1],&g_1943[1],&g_1943[0],&g_1943[0],&g_1943[0],&g_442[0]},{&g_442[7],&g_1943[1],&g_442[0],&g_1943[1],&g_1943[0],&g_1943[1],&g_442[8],(void*)0}},{{&g_442[6],&g_1943[1],&g_442[3],&g_1943[1],&g_1943[1],&g_442[4],(void*)0,&g_442[7]},{&g_442[3],&g_1943[0],&g_442[0],(void*)0,&g_1943[1],&g_442[8],&g_442[0],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[0],&g_442[9],&g_442[7],&g_1943[1],(void*)0,&g_442[8]},{&g_1943[1],&g_1943[1],&g_1943[1],(void*)0,&g_1943[1],&g_442[0],(void*)0,&g_442[0]},{(void*)0,&g_1943[1],&g_1943[1],&g_1943[1],(void*)0,&g_1943[0],&g_1943[1],&g_442[3]},{&g_1943[1],&g_1943[1],&g_1943[1],&g_1943[1],&g_1943[1],&g_442[4],&g_442[0],&g_1943[1]},{&g_1943[1],&g_1943[1],&g_1943[1],&g_442[4],&g_442[0],&g_1943[1],&g_1943[1],&g_1943[1]},{&g_1943[1],&g_442[8],&g_1943[1],&g_442[3],&g_1943[0],&g_1943[1],(void*)0,&g_1943[0]}}};
            volatile uint32_t ** volatile ***l_2011 = (void*)0;
            int8_t ****l_2038 = &l_1945;
            int8_t *****l_2037 = &l_2038;
            float *l_2053 = &g_609;
            int i, j, k;
            for (g_736 = (-1); (g_736 >= 7); g_736 = safe_add_func_uint32_t_u_u(g_736, 1))
            { /* block id: 825 */
                return l_1877;
            }
        }
    }
    else
    { /* block id: 892 */
        for (l_1751 = 0; (l_1751 == 27); l_1751 = safe_add_func_uint64_t_u_u(l_1751, 5))
        { /* block id: 895 */
            int32_t *l_2059 = &l_1745;
            for (g_178 = (-28); (g_178 == 52); ++g_178)
            { /* block id: 898 */
                (***g_1805) = (*g_1807);
            }
            (*g_1807) = l_2059;
        }
    }
    return g_2060;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_7(int32_t  p_8, uint16_t  p_9, uint64_t  p_10, const uint32_t  p_11)
{ /* block id: 740 */
    int16_t l_1703 = 1L;
    l_1703 &= p_9;
    return l_1703;
}


/* ------------------------------------------ */
/* 
 * reads : g_999 g_1000 g_1001 g_1002 g_315 g_6 g_1003 g_1189 g_513.f4 g_998 g_1302 g_609 g_299 g_352 g_353 g_288 g_991.f6 g_69 g_246 g_178 g_956 g_757 g_1689 g_325 g_513.f3 g_337 g_211
 * writes: g_513.f3 g_1003 g_1026 g_353 g_337 g_211 g_1652 g_956 g_200 g_513.f6 g_758 g_548
 */
static int32_t  func_12(uint8_t  p_13, int8_t  p_14, int64_t  p_15, float  p_16)
{ /* block id: 657 */
    int8_t **l_1555 = (void*)0;
    int32_t l_1558 = 0x99D66560L;
    uint8_t *l_1559[2];
    int16_t * const *l_1565 = &g_1384;
    int16_t * const * const *l_1564 = &l_1565;
    int64_t *l_1568 = &g_1026;
    int32_t l_1569 = (-1L);
    int16_t l_1576[1];
    uint16_t l_1589 = 7UL;
    int32_t l_1593[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    int64_t l_1594 = 0xBBE04A89D23285BFLL;
    int16_t l_1596 = (-1L);
    uint16_t l_1597 = 65531UL;
    int32_t l_1601 = (-4L);
    int16_t l_1607 = 1L;
    int64_t l_1619 = 0x24AC3F5E285CDFE3LL;
    int32_t *l_1631 = &l_1593[6];
    int32_t ***l_1653 = &g_352;
    int32_t *l_1690 = &l_1593[5];
    int32_t *l_1691 = &g_1003;
    int32_t *l_1692 = (void*)0;
    int32_t *l_1693 = (void*)0;
    int32_t *l_1694 = &g_840[2][2];
    int32_t *l_1695[10][10][2] = {{{&g_6[0][3],(void*)0},{&l_1601,&l_1601},{(void*)0,&g_6[0][3]},{&l_1601,&g_6[0][3]},{(void*)0,&l_1601},{&l_1601,(void*)0},{&g_6[0][3],&l_1601},{&g_6[0][3],(void*)0},{&l_1601,&l_1601},{(void*)0,(void*)0}},{{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601}},{{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]}},{{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]}},{{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601}},{{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0}},{{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]}},{{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0}},{{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601}},{{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]},{&l_1601,(void*)0},{&g_840[1][0],(void*)0},{&l_1601,&l_1593[7]},{&l_1593[7],&l_1601},{(void*)0,&g_840[1][0]},{(void*)0,&l_1601},{&l_1593[7],&l_1593[7]}}};
    int32_t l_1697 = 0x2A147539L;
    uint16_t l_1698 = 0UL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1559[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_1576[i] = 7L;
lbl_1668:
    for (g_513.f3 = 18; (g_513.f3 > 38); ++g_513.f3)
    { /* block id: 660 */
        (****g_999) = p_13;
    }
    if ((((safe_mod_func_int8_t_s_s(((((void*)0 != l_1555) | (safe_div_func_uint64_t_u_u(((((p_13 = l_1558) == ((((-1L) >= ((*l_1568) = (safe_mul_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((l_1558 >= ((g_315[1][1][0] > (((void*)0 == l_1564) , (((safe_div_func_int8_t_s_s(l_1558, (-1L))) & p_15) || 0x0B465164L))) < g_6[2][2])), l_1558)), 0x76L)))) && (-1L)) ^ (*g_1002))) == l_1569) < l_1558), 0x4AEE69F0D797D182LL))) > l_1569), 0x72L)) > p_15) , l_1558))
    { /* block id: 665 */
        uint32_t l_1572 = 18446744073709551607UL;
        uint16_t *l_1588 = (void*)0;
        int32_t l_1590 = 0x8FE6117BL;
        int32_t l_1591[1];
        int32_t *l_1592[3][2][4] = {{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}}};
        int32_t l_1600 = 0xF1A581BFL;
        uint32_t l_1602 = 0x0BEC4511L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1591[i] = 0L;
        l_1558 |= ((safe_add_func_float_f_f(0x1.3p+1, (l_1572 < (safe_div_func_float_f_f(((-l_1576[0]) > (+((((safe_add_func_float_f_f(((safe_mul_func_float_f_f(0xF.879EFBp-48, ((((safe_div_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u(1UL, ((0x7F56DB3F2E12E729LL || ((*l_1568) = 0x1FF5886C41466124LL)) >= ((((((*g_1189) != ((*****g_998) = 0x8DEF12E1L)) , 0xB057L) , (void*)0) == l_1588) < l_1589)))) , l_1589) == 0UL), p_13)) >= 0x49C2L) | 0xFAL) , 0x1.Ap+1))) >= p_15), (*g_1302))) >= 0x4.C5CCAFp+33) == l_1572) >= 0xD.99B830p+39))), 0x7.611328p-49))))) , 0xAEE60983L);
        ++l_1597;
        l_1602--;
    }
    else
    { /* block id: 671 */
        int32_t l_1611 = (-8L);
        int32_t l_1612 = 0x7F502799L;
        int32_t l_1613 = 0xE5C0E51EL;
        float l_1614[8][8] = {{(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20},{(-0x1.Cp-1),(-0x1.Cp-1),0x5.Dp+1,(-0x1.Cp-1),(-0x1.Cp-1),0x5.Dp+1,(-0x1.Cp-1),(-0x1.Cp-1)},{0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1)},{(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20},{(-0x1.Cp-1),(-0x1.Cp-1),0x5.Dp+1,(-0x1.Cp-1),(-0x1.Cp-1),0x5.Dp+1,(-0x1.Cp-1),(-0x1.Cp-1)},{0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1)},{(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20,0xF.9C9C28p+20,(-0x1.Cp-1),0xF.9C9C28p+20},{(-0x1.Cp-1),(-0x1.Cp-1),0x5.Dp+1,(-0x1.Cp-1),(-0x1.Cp-1),0x5.Dp+1,(-0x1.Cp-1),(-0x1.Cp-1)}};
        int32_t l_1615 = 0L;
        uint32_t l_1616 = 0UL;
        int i, j;
        for (l_1594 = 0; (l_1594 >= 25); l_1594++)
        { /* block id: 674 */
            int32_t *l_1608 = &l_1593[5];
            int32_t *l_1609 = &l_1601;
            int32_t *l_1610[10][1] = {{&l_1593[7]},{&l_1593[7]},{&l_1593[7]},{&l_1593[7]},{&l_1593[7]},{&l_1593[7]},{&l_1593[7]},{&l_1593[7]},{&l_1593[7]},{&l_1593[7]}};
            uint64_t **l_1630 = &g_361;
            int i, j;
            ++l_1616;
            if (l_1619)
                break;
            (*l_1608) = ((**g_1001) &= (safe_div_func_int64_t_s_s(l_1589, (safe_sub_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s(((*l_1609) = (safe_lshift_func_int16_t_s_s(l_1612, 13))), g_299[0][0][2])), 6)), p_14)))));
            (*l_1608) = ((void*)0 != l_1630);
        }
        l_1631 = ((*g_352) = &l_1558);
        for (g_337 = 0; (g_337 > 23); g_337 = safe_add_func_uint8_t_u_u(g_337, 2))
        { /* block id: 686 */
            uint64_t l_1654 = 0x2119C5A248F91961LL;
            int32_t l_1657 = 3L;
            int32_t l_1681[6] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
            int i;
            for (g_211 = 0; (g_211 > 16); ++g_211)
            { /* block id: 689 */
                int32_t **l_1636 = &l_1631;
                int32_t *l_1649 = &g_840[1][3];
                int32_t *l_1650 = &l_1593[5];
                int32_t *l_1651[9][5][5] = {{{&g_207,&l_1612,(void*)0,&l_1593[5],&l_1558},{(void*)0,&l_1601,&l_1593[5],&l_1593[5],&l_1593[5]},{(void*)0,&l_1593[5],&l_1612,&l_1593[5],(void*)0},{&l_1593[5],&l_1601,&l_1601,&l_1593[5],&g_207},{&g_6[0][3],(void*)0,&l_1601,&l_1593[5],&g_1003}},{{&g_207,&l_1612,(void*)0,&l_1593[5],&l_1558},{(void*)0,&l_1601,&l_1593[5],&l_1593[5],&l_1593[5]},{(void*)0,&l_1593[5],&l_1612,&l_1593[5],(void*)0},{&l_1593[5],&l_1601,&l_1601,&l_1593[5],&g_207},{&g_6[0][3],(void*)0,&l_1601,&l_1593[5],&g_1003}},{{&g_207,&l_1612,(void*)0,&l_1593[5],&l_1558},{(void*)0,&l_1601,&l_1593[5],&l_1593[5],&l_1593[5]},{(void*)0,&l_1593[5],&l_1612,&l_1593[5],(void*)0},{&l_1593[5],&l_1601,&l_1601,&l_1593[5],&g_207},{&g_6[0][3],(void*)0,&l_1601,&l_1593[5],&g_1003}},{{&g_207,&l_1612,(void*)0,&l_1593[5],&l_1558},{(void*)0,&l_1601,&l_1593[5],&l_1593[5],&l_1593[5]},{(void*)0,&l_1593[5],&l_1612,&l_1593[5],(void*)0},{&l_1593[5],&l_1601,&l_1601,&l_1593[5],&g_207},{&g_6[0][3],(void*)0,&g_207,&g_840[1][3],&g_6[2][1]}},{{&l_1558,(void*)0,(void*)0,&g_840[1][3],&l_1613},{&l_1558,&g_207,&g_6[0][3],&g_840[1][3],&l_1593[2]},{&l_1601,&g_6[0][3],(void*)0,&g_840[1][3],&g_6[0][3]},{&g_840[1][3],&l_1593[5],&l_1593[5],&g_840[1][3],&l_1558},{(void*)0,(void*)0,&g_207,&g_840[1][3],&g_6[2][1]}},{{&l_1558,(void*)0,(void*)0,&g_840[1][3],&l_1613},{&l_1558,&g_207,&g_6[0][3],&g_840[1][3],&l_1593[2]},{&l_1601,&g_6[0][3],(void*)0,&g_840[1][3],&g_6[0][3]},{&g_840[1][3],&l_1593[5],&l_1593[5],&g_840[1][3],&l_1558},{(void*)0,(void*)0,&g_207,&g_840[1][3],&g_6[2][1]}},{{&l_1558,(void*)0,(void*)0,&g_840[1][3],&l_1613},{&l_1558,&g_207,&g_6[0][3],&g_840[1][3],&l_1593[2]},{&l_1601,&g_6[0][3],(void*)0,&g_840[1][3],&g_6[0][3]},{&g_840[1][3],&l_1593[5],&l_1593[5],&g_840[1][3],&l_1558},{(void*)0,(void*)0,&g_207,&g_840[1][3],&g_6[2][1]}},{{&l_1558,(void*)0,(void*)0,&g_840[1][3],&l_1613},{&l_1558,&g_207,&g_6[0][3],&g_840[1][3],&l_1593[2]},{&l_1601,&g_6[0][3],(void*)0,&g_840[1][3],&g_6[0][3]},{&g_840[1][3],&l_1593[5],&l_1593[5],&g_840[1][3],&l_1558},{(void*)0,(void*)0,&g_207,&g_840[1][3],&g_6[2][1]}},{{&l_1558,(void*)0,(void*)0,&g_840[1][3],&l_1613},{&l_1558,&g_207,&g_6[0][3],&g_840[1][3],&l_1593[2]},{&l_1601,&g_6[0][3],(void*)0,&g_840[1][3],&g_6[0][3]},{&g_840[1][3],&l_1593[5],&l_1593[5],&g_840[1][3],&l_1558},{(void*)0,(void*)0,&g_207,&g_840[1][3],&g_6[2][1]}}};
                int32_t ** const l_1648[9] = {&l_1649,&l_1649,&l_1650,&l_1649,&l_1649,&l_1650,&l_1649,&l_1649,&l_1650};
                int32_t ** const *l_1647 = &l_1648[6];
                int32_t ** const **l_1646[10][9] = {{&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647},{&l_1647,&l_1647,&l_1647,(void*)0,(void*)0,&l_1647,&l_1647,&l_1647,&l_1647},{&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647},{&l_1647,&l_1647,&l_1647,(void*)0,(void*)0,&l_1647,&l_1647,&l_1647,&l_1647},{&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647},{&l_1647,&l_1647,&l_1647,(void*)0,(void*)0,&l_1647,&l_1647,&l_1647,(void*)0},{&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647},{&l_1647,&l_1647,(void*)0,(void*)0,(void*)0,(void*)0,&l_1647,&l_1647,(void*)0},{&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647},{&l_1647,&l_1647,(void*)0,(void*)0,(void*)0,(void*)0,&l_1647,&l_1647,(void*)0}};
                float *l_1655 = (void*)0;
                float *l_1656[10][1][10] = {{{(void*)0,&g_208[3],&g_1247[1],&g_609,&g_1247[5],&l_1614[2][4],(void*)0,(void*)0,&l_1614[2][4],&g_1247[5]}},{{&l_1614[2][4],&g_208[3],&g_208[3],&l_1614[2][4],&l_1614[2][4],(void*)0,&g_208[0],&l_1614[2][1],&g_1247[1],&g_1247[5]}},{{&g_1247[1],&g_1247[5],&g_1247[1],&g_208[0],&g_208[3],&g_609,&g_208[3],&g_208[0],&g_1247[1],&g_1247[5]}},{{&g_1247[5],&l_1614[2][4],&g_1247[5],&l_1614[2][4],(void*)0,(void*)0,(void*)0,&g_1247[1],&l_1614[2][4],&l_1614[2][4]}},{{&l_1614[2][4],&g_1247[5],(void*)0,&g_609,&g_609,(void*)0,&g_1247[5],&l_1614[2][4],&l_1614[2][4],(void*)0}},{{&g_1247[5],&g_1247[5],&g_208[0],&l_1614[2][4],(void*)0,&g_609,&l_1614[2][1],(void*)0,&l_1614[2][1],&g_609}},{{&g_1247[1],(void*)0,&g_208[0],(void*)0,&g_1247[1],(void*)0,&l_1614[2][4],&l_1614[2][4],&g_1247[5],(void*)0}},{{&l_1614[2][4],(void*)0,(void*)0,(void*)0,&g_1247[1],&l_1614[2][4],&l_1614[2][4],&g_1247[1],(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_1247[5],(void*)0,&g_1247[1],&g_1247[5],&g_1247[1],&g_208[0],&g_208[3],&g_609}},{{&g_208[0],&l_1614[2][1],&g_1247[1],&g_1247[5],(void*)0,&g_1247[5],&g_1247[1],&l_1614[2][1],&g_208[0],(void*)0}}};
                int8_t *l_1662 = &g_956;
                const int32_t *l_1666 = &l_1611;
                uint64_t ****l_1671 = &g_189[3];
                uint32_t l_1682 = 1UL;
                int i, j, k;
                (*l_1636) = ((*g_352) = (*g_352));
                if ((~((*l_1662) &= (safe_mul_func_int8_t_s_s((safe_mod_func_int16_t_s_s(((safe_add_func_uint16_t_u_u((((((((g_288 , ((((safe_div_func_float_f_f((*g_1302), (**l_1636))) < ((g_1652 = &g_352) == l_1653)) == (l_1657 = l_1654)) >= ((safe_mul_func_uint8_t_u_u(l_1654, (((g_991.f6 != ((safe_lshift_func_uint16_t_u_s(0x9F0FL, p_13)) >= g_69[0])) < 1L) != (-2L)))) , (-0x1.8p+1)))) , 0xB68E0CA0L) > 4294967295UL) , l_1616) < (*g_246)) != p_15) < l_1654), p_15)) ^ 0x5BL), (***l_1653))), 0x23L)))))
                { /* block id: 695 */
                    const int32_t *l_1665[3][9][9] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
                    int i, j, k;
                    for (l_1654 = 0; (l_1654 > 51); l_1654++)
                    { /* block id: 698 */
                        p_16 = (*g_1302);
                    }
                    for (g_200 = 0; (g_200 <= 5); g_200 += 1)
                    { /* block id: 703 */
                        int8_t l_1667 = 3L;
                        l_1666 = l_1665[0][8][7];
                        return l_1667;
                    }
                }
                else
                { /* block id: 707 */
                    uint8_t l_1672 = 0UL;
                    if (p_13)
                        goto lbl_1668;
                    for (g_513.f6 = 3; (g_513.f6 > 31); ++g_513.f6)
                    { /* block id: 711 */
                        (*g_757) = l_1671;
                        return l_1657;
                    }
                    l_1672++;
                }
                for (p_13 = 0; (p_13 <= 13); p_13 = safe_add_func_uint8_t_u_u(p_13, 1))
                { /* block id: 719 */
                    int16_t l_1679 = (-1L);
                    int32_t l_1680 = (-3L);
                    for (p_15 = 0; (p_15 <= (-8)); p_15--)
                    { /* block id: 722 */
                        if ((*****g_998))
                            break;
                    }
                    --l_1682;
                    for (l_1596 = 0; (l_1596 >= (-3)); l_1596 = safe_sub_func_uint32_t_u_u(l_1596, 3))
                    { /* block id: 728 */
                        int32_t **l_1688[10][8] = {{&l_1631,&l_1651[2][2][3],&l_1631,&g_353,(void*)0,&g_353,(void*)0,&l_1651[2][4][2]},{&l_1649,(void*)0,(void*)0,&g_548,(void*)0,&g_548,&l_1631,&g_548},{&g_548,&g_353,&g_353,&l_1651[2][4][2],&l_1651[0][1][3],&l_1651[0][1][3],&l_1651[8][3][1],&l_1631},{&l_1631,&l_1651[7][2][3],&l_1651[0][1][3],&g_353,&l_1651[0][1][3],(void*)0,(void*)0,&l_1651[0][1][3]},{&g_353,&g_548,&g_548,&g_353,&l_1651[0][1][3],&g_548,&g_353,&l_1651[2][2][3]},{(void*)0,&l_1649,&g_548,&l_1631,&g_353,&l_1651[7][2][3],(void*)0,&l_1651[0][2][2]},{(void*)0,&l_1649,&l_1631,(void*)0,&l_1631,&g_548,&l_1651[0][1][3],(void*)0},{&l_1649,&g_548,&g_353,&l_1631,(void*)0,(void*)0,(void*)0,&l_1631},{&g_353,&l_1651[7][2][3],&g_548,&l_1651[0][1][3],&g_353,&l_1651[0][1][3],&g_548,&l_1651[7][2][3]},{(void*)0,&g_353,&l_1631,&l_1631,&g_353,&g_548,(void*)0,&g_548}};
                        int i, j;
                        if (l_1679)
                            break;
                        (*g_1689) = (***g_999);
                    }
                }
            }
        }
    }
    (*l_1631) = (*l_1631);
    ++l_1698;
    return g_325;
}


/* ------------------------------------------ */
/* 
 * reads : g_1347.f3 g_1001 g_1002 g_1003 g_304 g_1188 g_1189 g_513.f4 g_207 g_1349 g_990
 * writes: g_1347.f3 g_1003 g_207 g_990
 */
static uint8_t  func_19(uint32_t  p_20, uint32_t  p_21, int16_t  p_22)
{ /* block id: 648 */
    uint32_t l_1534 = 9UL;
    struct S0 **l_1549 = &g_990;
    for (g_1347.f3 = 0; (g_1347.f3 > 54); g_1347.f3++)
    { /* block id: 651 */
        float *l_1546 = &g_1247[2];
        int32_t l_1547[5];
        int32_t *l_1548 = &g_207;
        int i;
        for (i = 0; i < 5; i++)
            l_1547[i] = (-1L);
        (*l_1548) |= ((((p_20 < l_1534) && ((**g_1001) ^= p_20)) <= ((l_1534 | ((((~(safe_div_func_uint64_t_u_u((safe_add_func_uint8_t_u_u((((safe_mod_func_uint32_t_u_u(((safe_sub_func_uint8_t_u_u(((&p_22 != &p_22) ^ g_304), ((safe_mod_func_uint16_t_u_u(((l_1546 == l_1546) , l_1547[1]), 0x40FCL)) | p_20))) & l_1534), 0xDD27AD64L)) > 0xF383806B093B6206LL) & 7UL), 249UL)), 3UL))) && p_20) && (**g_1188)) ^ (-1L))) | 2UL)) ^ 9UL);
    }
    (*l_1549) = (*g_1349);
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads : g_1302 g_609 g_246 g_178 g_1188 g_1189 g_1120 g_267 g_792 g_1000 g_1001 g_1002 g_1003 g_999 g_787 g_74 g_1447 g_513.f4 g_294 g_601
 * writes: g_513.f4 g_1026 g_1247 g_609 g_1447 g_1003 g_178
 */
static uint32_t  func_23(int32_t  p_24, const int32_t  p_25, uint16_t  p_26)
{ /* block id: 619 */
    int32_t l_1472 = 0x682B3E77L;
    int64_t l_1475 = 0xEB8AEE9445783F86LL;
    int8_t l_1486 = 0L;
    int32_t l_1487 = 0xEEF2086EL;
    int64_t *l_1488 = &g_1026;
    if (((*g_1302) , ((safe_div_func_uint32_t_u_u((((((*g_246) , ((((**g_1188) = l_1472) > (safe_sub_func_uint32_t_u_u((l_1472 != l_1475), (safe_lshift_func_int8_t_s_u((((safe_mod_func_int64_t_s_s(((*l_1488) = (safe_div_func_int32_t_s_s(((safe_mul_func_uint16_t_u_u((+l_1475), (((l_1487 = (((((l_1475 || (+((p_24 , 0x2956L) <= l_1486))) >= l_1486) , 0L) & p_24) | l_1472)) != l_1472) , 1UL))) | p_26), l_1486))), g_1120[3])) , 0x10A1L) <= p_24), g_267[1]))))) >= l_1486)) ^ p_25) != g_792) < (***g_1000)), l_1472)) & 0UL)))
    { /* block id: 623 */
        int8_t ***l_1490 = (void*)0;
        int8_t ****l_1489 = &l_1490;
        int8_t ***l_1492 = (void*)0;
        int8_t ****l_1491 = &l_1492;
        (*l_1491) = ((*l_1489) = (void*)0);
    }
    else
    { /* block id: 626 */
        int8_t *** const l_1504 = (void*)0;
        float *l_1517 = &g_1247[4];
        float *l_1518[5][1] = {{&g_208[1]},{&g_208[3]},{&g_208[1]},{&g_208[3]},{&g_208[1]}};
        int32_t l_1519 = 1L;
        int32_t l_1520 = 0x0B037A31L;
        int i, j;
lbl_1525:
        l_1520 = (safe_div_func_float_f_f((-0x6.2p-1), (safe_div_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f(((safe_add_func_float_f_f((!((((void*)0 == l_1504) != (safe_sub_func_float_f_f(((*g_1302) = ((l_1519 = (safe_sub_func_float_f_f(((((safe_sub_func_float_f_f((l_1487 = (safe_div_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f(0x6.786A87p-16, l_1475)), ((*l_1517) = ((void*)0 != (*g_999))))), ((0x1.8p+1 >= (*g_1302)) < 0x9.Ep-1)))), l_1472)) , 4UL) , 0x1.809E50p-37) >= g_609), 0x1.Bp+1))) < 0x5.B57D4Ep+95)), p_26))) == (-0x1.1p+1))), l_1520)) == p_24), g_787)), g_74[3])), p_24))));
        for (g_1447 = 3; (g_1447 > 28); ++g_1447)
        { /* block id: 634 */
            for (g_1003 = 0; (g_1003 > (-22)); g_1003 = safe_sub_func_int16_t_s_s(g_1003, 1))
            { /* block id: 637 */
                return (**g_1188);
            }
            if (p_26)
                break;
            if (g_1003)
                goto lbl_1525;
        }
    }
    l_1487 &= ((p_26 >= g_294) < ((**g_601) = (safe_lshift_func_uint8_t_u_u(0xE2L, 2))));
    return l_1472;
}


/* ------------------------------------------ */
/* 
 * reads : g_1302 g_609 g_787 g_352 g_790 g_1245 g_69 g_513.f4 g_146 g_65 g_292 g_138 g_601 g_246 g_1026 g_1247 g_1120 g_1188 g_1189 g_353 g_990 g_513 g_1347 g_991 g_840 g_1338 g_1349 g_659 g_660 g_281 g_178 g_435 g_1000 g_1001 g_1002 g_1003 g_1383 g_1395 g_689 g_6 g_207 g_200 g_275
 * writes: g_208 g_609 g_991.f3 g_1245 g_513.f4 g_1026 g_787 g_353 g_790 g_178 g_1247 g_840 g_454 g_990 g_65 g_1003 g_991.f4 g_47 g_69 g_518
 */
static uint16_t  func_29(int16_t  p_30, float  p_31)
{ /* block id: 544 */
    uint8_t l_1298 = 0x8AL;
    float *l_1301 = &g_208[3];
    uint32_t **l_1303 = (void*)0;
    uint32_t ***l_1304 = &l_1303;
    uint64_t *****l_1343[5][6] = {{(void*)0,&g_758,(void*)0,&g_758,(void*)0,&g_758},{(void*)0,&g_758,(void*)0,&g_758,(void*)0,&g_758},{(void*)0,&g_758,(void*)0,&g_758,(void*)0,&g_758},{(void*)0,&g_758,(void*)0,&g_758,(void*)0,&g_758},{(void*)0,&g_758,(void*)0,&g_758,(void*)0,&g_758}};
    int32_t l_1401 = 0xF2457285L;
    uint32_t l_1402 = 0UL;
    int32_t l_1405 = 0xBE7096A2L;
    int32_t l_1414 = 0x9F84342EL;
    int32_t l_1435 = 0L;
    int32_t l_1440 = 0x820EB2E8L;
    int32_t l_1442 = 1L;
    int32_t l_1444 = (-4L);
    int32_t l_1449 = 4L;
    int i, j;
    l_1298--;
lbl_1305:
    (*g_1302) = ((*l_1301) = l_1298);
    (*l_1301) = ((((*l_1304) = l_1303) != &g_1189) == (*g_1302));
    for (g_991.f3 = 1; (g_991.f3 <= 5); g_991.f3 += 1)
    { /* block id: 552 */
        int64_t l_1369[7][9][4] = {{{0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL},{0xA1D91013AF657BB4LL,0x55C88155BE03668ELL,0x0BA5CEA4B4C382EBLL,0x0BA5CEA4B4C382EBLL},{0x19CD072A24BC2CADLL,0x19CD072A24BC2CADLL,1L,0x55C88155BE03668ELL},{0x55C88155BE03668ELL,0xA1D91013AF657BB4LL,1L,0xA1D91013AF657BB4LL},{0x19CD072A24BC2CADLL,0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L},{0xA1D91013AF657BB4LL,0x47F041F077602817LL,0x47F041F077602817LL,0xA1D91013AF657BB4LL},{0x47F041F077602817LL,0xA1D91013AF657BB4LL,0x19CD072A24BC2CADLL,0x55C88155BE03668ELL},{0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL},{0xA1D91013AF657BB4LL,0x55C88155BE03668ELL,0x0BA5CEA4B4C382EBLL,0x0BA5CEA4B4C382EBLL}},{{0x19CD072A24BC2CADLL,0x19CD072A24BC2CADLL,1L,0x55C88155BE03668ELL},{0x55C88155BE03668ELL,0xA1D91013AF657BB4LL,1L,0xA1D91013AF657BB4LL},{0x19CD072A24BC2CADLL,0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L},{0xA1D91013AF657BB4LL,0x47F041F077602817LL,0x47F041F077602817LL,0xA1D91013AF657BB4LL},{0x47F041F077602817LL,0xA1D91013AF657BB4LL,0x19CD072A24BC2CADLL,0x55C88155BE03668ELL},{0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL},{0xA1D91013AF657BB4LL,0x55C88155BE03668ELL,0x0BA5CEA4B4C382EBLL,0x0BA5CEA4B4C382EBLL},{0x19CD072A24BC2CADLL,0x19CD072A24BC2CADLL,1L,0x55C88155BE03668ELL},{0x55C88155BE03668ELL,0xA1D91013AF657BB4LL,1L,0xA1D91013AF657BB4LL}},{{0x19CD072A24BC2CADLL,0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L},{0xA1D91013AF657BB4LL,0x47F041F077602817LL,0x55C88155BE03668ELL,0x47F041F077602817LL},{0x55C88155BE03668ELL,0x47F041F077602817LL,0xE6412CFD0F4F75C1LL,0x0BA5CEA4B4C382EBLL},{0x55C88155BE03668ELL,0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L},{0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L,1L},{0xE6412CFD0F4F75C1LL,0xE6412CFD0F4F75C1LL,0x19CD072A24BC2CADLL,0x0BA5CEA4B4C382EBLL},{0x0BA5CEA4B4C382EBLL,0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL},{0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L,0x19CD072A24BC2CADLL},{0x47F041F077602817LL,0x55C88155BE03668ELL,0x55C88155BE03668ELL,0x47F041F077602817LL}},{{0x55C88155BE03668ELL,0x47F041F077602817LL,0xE6412CFD0F4F75C1LL,0x0BA5CEA4B4C382EBLL},{0x55C88155BE03668ELL,0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L},{0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L,1L},{0xE6412CFD0F4F75C1LL,0xE6412CFD0F4F75C1LL,0x19CD072A24BC2CADLL,0x0BA5CEA4B4C382EBLL},{0x0BA5CEA4B4C382EBLL,0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL},{0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L,0x19CD072A24BC2CADLL},{0x47F041F077602817LL,0x55C88155BE03668ELL,0x55C88155BE03668ELL,0x47F041F077602817LL},{0x55C88155BE03668ELL,0x47F041F077602817LL,0xE6412CFD0F4F75C1LL,0x0BA5CEA4B4C382EBLL},{0x55C88155BE03668ELL,0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L}},{{0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L,1L},{0xE6412CFD0F4F75C1LL,0xE6412CFD0F4F75C1LL,0x19CD072A24BC2CADLL,0x0BA5CEA4B4C382EBLL},{0x0BA5CEA4B4C382EBLL,0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL},{0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L,0x19CD072A24BC2CADLL},{0x47F041F077602817LL,0x55C88155BE03668ELL,0x55C88155BE03668ELL,0x47F041F077602817LL},{0x55C88155BE03668ELL,0x47F041F077602817LL,0xE6412CFD0F4F75C1LL,0x0BA5CEA4B4C382EBLL},{0x55C88155BE03668ELL,0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L},{0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L,1L},{0xE6412CFD0F4F75C1LL,0xE6412CFD0F4F75C1LL,0x19CD072A24BC2CADLL,0x0BA5CEA4B4C382EBLL}},{{0x0BA5CEA4B4C382EBLL,0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL},{0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L,0x19CD072A24BC2CADLL},{0x47F041F077602817LL,0x55C88155BE03668ELL,0x55C88155BE03668ELL,0x47F041F077602817LL},{0x55C88155BE03668ELL,0x47F041F077602817LL,0xE6412CFD0F4F75C1LL,0x0BA5CEA4B4C382EBLL},{0x55C88155BE03668ELL,0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L},{0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L,1L},{0xE6412CFD0F4F75C1LL,0xE6412CFD0F4F75C1LL,0x19CD072A24BC2CADLL,0x0BA5CEA4B4C382EBLL},{0x0BA5CEA4B4C382EBLL,0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL},{0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L,0x19CD072A24BC2CADLL}},{{0x47F041F077602817LL,0x55C88155BE03668ELL,0x55C88155BE03668ELL,0x47F041F077602817LL},{0x55C88155BE03668ELL,0x47F041F077602817LL,0xE6412CFD0F4F75C1LL,0x0BA5CEA4B4C382EBLL},{0x55C88155BE03668ELL,0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L},{0x47F041F077602817LL,0x0BA5CEA4B4C382EBLL,1L,1L},{0xE6412CFD0F4F75C1LL,0xE6412CFD0F4F75C1LL,0x19CD072A24BC2CADLL,0x0BA5CEA4B4C382EBLL},{0x0BA5CEA4B4C382EBLL,0x47F041F077602817LL,0x19CD072A24BC2CADLL,0x47F041F077602817LL},{0xE6412CFD0F4F75C1LL,0x55C88155BE03668ELL,1L,0x19CD072A24BC2CADLL},{0x47F041F077602817LL,0x55C88155BE03668ELL,0x55C88155BE03668ELL,0x47F041F077602817LL},{0x55C88155BE03668ELL,0x47F041F077602817LL,0xE6412CFD0F4F75C1LL,0x0BA5CEA4B4C382EBLL}}};
        int16_t **l_1386 = &g_1384;
        int32_t l_1399 = (-9L);
        int32_t l_1448 = 0x203B29AEL;
        int i, j, k;
        for (g_1245 = 0; (g_1245 >= 0); g_1245 -= 1)
        { /* block id: 555 */
            int32_t *l_1306 = &g_840[1][3];
            uint16_t *l_1324[6] = {&g_178,&g_178,&g_178,&g_178,&g_178,&g_178};
            uint32_t l_1380 = 4294967287UL;
            int32_t l_1433 = 0x9C18C8AFL;
            int32_t l_1439 = 0L;
            int32_t l_1445 = 0x5FA01E40L;
            uint16_t l_1452[2];
            uint8_t l_1469 = 251UL;
            int i;
            for (i = 0; i < 2; i++)
                l_1452[i] = 4UL;
            for (g_513.f4 = 0; (g_513.f4 <= 0); g_513.f4 += 1)
            { /* block id: 558 */
                uint32_t *l_1337 = (void*)0;
                uint32_t **l_1336 = &l_1337;
                uint32_t ***l_1335 = &l_1336;
                uint32_t ****l_1334 = &l_1335;
                uint32_t *****l_1333 = &l_1334;
                int32_t l_1374 = 0x866F0B5CL;
                uint64_t ** const l_1381 = &g_361;
                uint32_t *l_1398 = &g_991.f4;
                int64_t *l_1400 = &g_1026;
                int8_t l_1416 = 0xDAL;
                int32_t l_1436 = 0x733F1E04L;
                int32_t l_1438 = 0x3390DAFCL;
                int32_t l_1443 = 0xC49D642DL;
                int32_t l_1450 = 6L;
                for (g_1026 = 1; (g_1026 <= 5); g_1026 += 1)
                { /* block id: 561 */
                    for (g_787 = 5; (g_787 >= 0); g_787 -= 1)
                    { /* block id: 564 */
                        int16_t *l_1309 = &g_790;
                        uint8_t l_1323 = 251UL;
                        int i;
                        if (g_787)
                            goto lbl_1305;
                        (*g_352) = l_1306;
                        (*g_353) = (((*l_1309) ^= (-1L)) || (((safe_div_func_uint16_t_u_u(((((((~(+g_69[g_1245])) , ((g_1247[g_513.f4] = (safe_div_func_float_f_f(((!(safe_add_func_float_f_f(((((((g_69[g_513.f4] | ((((void*)0 == &g_999) && (p_30 != l_1298)) , ((**g_601) = (((safe_div_func_uint64_t_u_u((*g_146), g_292[2][0])) || g_138) ^ l_1323)))) <= p_30) , 0xE1E3769EL) , 0xC.CF97C8p+71) > (-0x1.Bp+1)) < g_1247[g_1026]), g_1120[2]))) == 0x0.Bp-1), 0x1.F88061p+64))) , g_1245)) , 0x21CC9CA7L) > p_30) || p_30) , 0xE53EL), 0x3E95L)) , (-2L)) < (**g_1188)));
                    }
                }
                if (((((*g_990) , l_1324[5]) != (void*)0) ^ (safe_add_func_int32_t_s_s((safe_rshift_func_uint16_t_u_s((*l_1306), 6)), (((safe_add_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u((((((*l_1333) = (void*)0) == g_1338[4][1]) && (-1L)) && p_30), ((void*)0 == l_1343[4][0]))), (-1L))) >= l_1298) , 1L)))))
                { /* block id: 574 */
                    uint32_t l_1344 = 1UL;
                    for (g_454 = 0; (g_454 >= 0); g_454 -= 1)
                    { /* block id: 577 */
                        struct S0 *l_1348[4] = {&g_513,&g_513,&g_513,&g_513};
                        int32_t l_1366[10] = {(-1L),(-5L),(-2L),(-5L),(-1L),(-1L),(-5L),6L,(-5L),(-5L)};
                        int i;
                        l_1344--;
                        (*g_1349) = (g_1347 , l_1348[1]);
                        (***g_1000) ^= (safe_mod_func_int8_t_s_s(((safe_sub_func_uint8_t_u_u(247UL, g_1347.f4)) <= ((((255UL && (safe_lshift_func_uint16_t_u_u(((((*g_659) , ((safe_add_func_uint64_t_u_u(((*g_146) = (safe_lshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s(((((safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s(l_1366[3], (safe_add_func_int64_t_s_s(l_1369[1][0][3], (((safe_rshift_func_int16_t_s_s(g_281[1], ((safe_mul_func_uint8_t_u_u(((*g_1189) , 0x2DL), l_1374)) > p_30))) , p_30) && p_30))))), 1L)) && 0L) || l_1366[5]) != (**g_1188)), l_1374)), 7))), g_178)) != g_435)) , 7L) & 0xB4D6269DAAA5FC11LL), l_1366[3]))) >= (*l_1306)) >= l_1344) | 0x2AD8695C86989781LL)), (*l_1306)));
                        if (p_30)
                            continue;
                    }
                }
                else
                { /* block id: 584 */
                    uint32_t l_1379 = 0x17A63DF7L;
                    int32_t l_1382 = 2L;
                    l_1382 ^= ((((safe_div_func_int64_t_s_s(p_30, ((l_1374 <= ((safe_add_func_uint32_t_u_u(0x770B6FBBL, (*l_1306))) && l_1379)) ^ (p_30 ^ ((l_1380 , (void*)0) != l_1381))))) <= l_1298) | 0x553949BEL) , 0x3DF3C182L);
                    l_1386 = g_1383[6];
                    if (p_30)
                        continue;
                    (***g_1000) ^= 7L;
                }
                (*l_1306) |= (safe_add_func_uint8_t_u_u(l_1374, (((l_1401 = ((+(safe_div_func_int16_t_s_s(((0x87L != l_1369[5][7][1]) ^ (safe_sub_func_uint8_t_u_u(((*g_1002) >= (((*l_1400) = ((safe_unary_minus_func_int8_t_s(1L)) , ((1L != (l_1399 = (g_1395[2][1] , (safe_mod_func_int8_t_s_s(((*g_689) || ((*l_1398) = (l_1298 , 4294967286UL))), g_200))))) != 0x3E8FFCB8L))) < (-1L))), l_1374))), p_30))) & l_1298)) < l_1402) && p_30)));
                for (g_1026 = 0; (g_1026 >= 0); g_1026 -= 1)
                { /* block id: 597 */
                    int16_t l_1434 = 0x9132L;
                    int32_t l_1437 = 0x22C1909EL;
                    int32_t l_1441 = 0L;
                    int32_t l_1446[9][7] = {{0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L},{8L,8L,8L,8L,8L,8L,8L},{0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L},{8L,8L,8L,8L,8L,8L,8L},{0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L},{8L,8L,8L,8L,8L,8L,8L},{0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L},{8L,8L,8L,8L,8L,8L,8L},{0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L,0xBF4356D6L}};
                    int i, j;
                    for (g_47 = 0; (g_47 <= 5); g_47 += 1)
                    { /* block id: 600 */
                        int8_t l_1403 = 1L;
                        int32_t *l_1404 = &l_1399;
                        int32_t *l_1406 = &g_1003;
                        int32_t *l_1407 = &l_1405;
                        int32_t l_1408 = 1L;
                        int32_t *l_1409 = &l_1408;
                        int32_t *l_1410 = (void*)0;
                        int32_t *l_1411 = &l_1399;
                        int32_t *l_1412 = &l_1408;
                        int32_t *l_1413 = &l_1374;
                        int32_t *l_1415 = &g_207;
                        int32_t *l_1417 = &g_840[0][0];
                        int32_t *l_1418 = &g_207;
                        int32_t *l_1419 = &l_1408;
                        int32_t *l_1420 = (void*)0;
                        int32_t *l_1421 = &l_1408;
                        int32_t *l_1422 = &g_1003;
                        int32_t l_1423[1];
                        int32_t *l_1424 = &g_207;
                        int32_t *l_1425 = &g_1003;
                        int32_t *l_1426 = (void*)0;
                        int32_t *l_1427 = (void*)0;
                        int32_t *l_1428 = (void*)0;
                        int32_t *l_1429 = &l_1423[0];
                        int32_t *l_1430 = &l_1405;
                        int32_t *l_1431 = (void*)0;
                        int32_t *l_1432[6] = {(void*)0,&l_1399,&l_1399,(void*)0,&l_1399,&l_1399};
                        int8_t l_1451 = 0xBBL;
                        int16_t *l_1455 = &l_1434;
                        uint8_t *l_1460 = &g_518;
                        int i, j;
                        for (i = 0; i < 1; i++)
                            l_1423[i] = (-1L);
                        ++l_1452[0];
                        (*l_1429) = ((((*l_1455) = (-7L)) > ((((l_1440 ^= (safe_mul_func_int16_t_s_s((g_275[(g_1245 + 1)] , ((safe_sub_func_int64_t_s_s((((((*l_1460) = (g_69[g_1026] &= g_275[g_1245])) && (safe_mod_func_int64_t_s_s(((*g_990) , 2L), (safe_add_func_int8_t_s_s(((l_1469 |= (4L ^ ((safe_div_func_int64_t_s_s(((*l_1306) &= ((l_1436 &= 5L) < (l_1448 , (safe_sub_func_uint8_t_u_u(p_30, p_30))))), 0x3A1D1F685517897DLL)) | 0xD103FB12L))) < l_1448), 0x4BL))))) == 1UL) , p_30), p_30)) != l_1437)), (**g_601)))) == (*g_689)) ^ 0L) || 0x54202587L)) > g_1120[2]);
                    }
                    (*g_352) = &l_1446[3][4];
                }
            }
            return g_69[g_1245];
        }
        return (**g_601);
    }
    return (**g_601);
}


/* ------------------------------------------ */
/* 
 * reads : g_1003 g_999 g_1000 g_47 g_178 g_74 g_601 g_246 g_129 g_591 g_352 g_353 g_207 g_69 g_689 g_6 g_435 g_441 g_442 g_790 g_190 g_146 g_65 g_787 g_1188 g_1189 g_513.f4 g_1001 g_1002 g_70 g_513.f3 g_1125 g_263 g_956 g_200 g_156 g_998
 * writes: g_1003 g_956 g_178 g_591 g_435 g_689 g_138 g_200 g_518 g_787 g_207 g_790 g_65 g_1120 g_1245 g_74 g_70 g_513.f3
 */
static int32_t  func_32(float  p_33)
{ /* block id: 406 */
    const int32_t l_1036 = (-7L);
    int32_t l_1037 = 0x09CA2ABFL;
    uint8_t **l_1078 = (void*)0;
    int32_t l_1083 = (-10L);
    int32_t l_1084[9][7][4] = {{{0L,(-7L),0L,0xCCB2B182L},{0x76C58177L,0xE2670225L,1L,1L},{0L,0x21C900A1L,0x79C80FDBL,0xF6D37BFFL},{(-1L),(-1L),0L,0xA1A41ED9L},{0L,0x9B6BFA21L,0xE0B6DFF4L,0xE2B37638L},{(-1L),0L,(-1L),0L},{1L,0x19D83BD3L,(-7L),0xE2670225L}},{{0x683D0C84L,0xA1A41ED9L,0xCCC0E478L,0x19D83BD3L},{0x9B6BFA21L,(-3L),0xCCC0E478L,1L},{0x683D0C84L,0x76C58177L,(-7L),0x79C80FDBL},{1L,(-1L),(-1L),0xFC9FF9B4L},{(-1L),0xFC9FF9B4L,0xE0B6DFF4L,0xCCC0E478L},{0L,1L,0L,(-1L)},{(-1L),0x3B2F99A2L,0x79C80FDBL,0L}},{{0L,(-1L),1L,0L},{0x76C58177L,0L,0L,0x76C58177L},{0L,(-1L),0x3B2F99A2L,1L},{0x7B9CC1A7L,1L,0xE2670225L,(-3L)},{0L,(-1L),(-6L),(-3L)},{0x9DF1B1BDL,1L,(-1L),1L},{0xCCB2B182L,(-1L),0L,0x76C58177L}},{{(-7L),0L,0xCCB2B182L,0L},{(-1L),(-1L),0x76C58177L,0L},{0xE2B37638L,0x3B2F99A2L,0L,(-1L)},{1L,1L,(-1L),0xCCC0E478L},{(-3L),0xFC9FF9B4L,0L,0xFC9FF9B4L},{0xF6D37BFFL,(-1L),0xFC9FF9B4L,0x79C80FDBL},{0x79C80FDBL,0x76C58177L,0L,1L}},{{(-6L),(-3L),(-1L),0x19D83BD3L},{(-6L),0xA1A41ED9L,1L,0x683D0C84L},{0L,0xE2B37638L,0xA1A41ED9L,(-1L)},{(-3L),1L,(-1L),1L},{0x79C80FDBL,(-6L),0x21C900A1L,0L},{0x9B6BFA21L,0x21C900A1L,1L,(-3L)},{1L,(-1L),0x9DF1B1BDL,0L}},{{0L,0x683D0C84L,0xE2670225L,0xE2670225L},{0L,0L,(-9L),0xF9872919L},{0xE2670225L,(-1L),(-3L),0xCCC0E478L},{(-1L),0x19D83BD3L,0xCCB2B182L,(-3L)},{0x76C58177L,0x19D83BD3L,0x683D0C84L,0xCCC0E478L},{0x19D83BD3L,(-1L),1L,0xF9872919L},{(-1L),0L,0x76C58177L,0xE2670225L}},{{0x9DF1B1BDL,0x683D0C84L,0L,0L},{(-9L),(-1L),0L,(-3L)},{0xE0B6DFF4L,0x21C900A1L,(-1L),0L},{0xF9872919L,(-6L),(-7L),1L},{0x7B9CC1A7L,1L,0x7B9CC1A7L,(-1L)},{0L,0xE2B37638L,0L,0x683D0C84L},{(-1L),0L,0x3B2F99A2L,0xE2B37638L}},{{(-6L),0xFC9FF9B4L,0x3B2F99A2L,0x9B6BFA21L},{(-1L),0x9DF1B1BDL,0L,0L},{0L,0L,0x7B9CC1A7L,0xA1A41ED9L},{0x7B9CC1A7L,0xA1A41ED9L,(-7L),0x3B2F99A2L},{0xF9872919L,0L,(-1L),0L},{0xE0B6DFF4L,1L,0L,(-1L)},{(-9L),0xE0B6DFF4L,0L,1L}},{{0x9DF1B1BDL,0x76C58177L,0x76C58177L,0x9DF1B1BDL},{(-1L),0xCCC0E478L,1L,0L},{0x19D83BD3L,0x9B6BFA21L,0x683D0C84L,0x79C80FDBL},{0x76C58177L,0x7B9CC1A7L,0xCCB2B182L,0x79C80FDBL},{(-1L),0x9B6BFA21L,(-3L),0L},{0xE2670225L,0xCCC0E478L,(-9L),0x9DF1B1BDL},{0L,0x76C58177L,0xE2670225L,1L}}};
    const int16_t *l_1104 = (void*)0;
    int8_t l_1144[1][2];
    float l_1147 = 0x0.5A1978p+27;
    uint8_t *l_1213 = &g_1120[1];
    uint8_t **l_1212 = &l_1213;
    uint16_t l_1222 = 0xF269L;
    int32_t ****l_1238[10] = {&g_591,(void*)0,(void*)0,&g_591,&g_591,&g_591,(void*)0,(void*)0,&g_591,&g_591};
    int32_t *** const *l_1239 = &g_591;
    int32_t *l_1297 = &g_840[1][3];
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_1144[i][j] = 0x83L;
    }
    for (g_1003 = 0; (g_1003 <= 3); g_1003 += 1)
    { /* block id: 409 */
        uint16_t l_1032 = 0xDA30L;
        uint32_t *l_1072 = &g_793[2][9];
        uint32_t **l_1071 = &l_1072;
        uint32_t ***l_1070[3][6] = {{(void*)0,&l_1071,(void*)0,(void*)0,&l_1071,(void*)0},{(void*)0,&l_1071,(void*)0,(void*)0,&l_1071,(void*)0},{(void*)0,&l_1071,(void*)0,(void*)0,&l_1071,(void*)0}};
        uint32_t ***l_1073 = &l_1071;
        int32_t l_1085 = 1L;
        int32_t l_1088 = (-1L);
        int32_t l_1091 = 0x30D43356L;
        int32_t l_1092 = 4L;
        int32_t l_1093 = (-7L);
        int32_t l_1094 = 0L;
        int32_t l_1095 = 8L;
        uint64_t *****l_1113 = (void*)0;
        uint32_t l_1133 = 0x0C8279D3L;
        int32_t l_1140 = 0x6CE789C5L;
        int32_t l_1141 = 0x54838995L;
        int32_t l_1142[1];
        int32_t l_1146 = 0xCA7FC035L;
        uint8_t *l_1210 = &g_47;
        uint8_t **l_1209[3][7][9] = {{{&l_1210,&l_1210,&l_1210,(void*)0,&l_1210,(void*)0,&l_1210,&l_1210,&l_1210},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{&l_1210,&l_1210,&l_1210,(void*)0,&l_1210,(void*)0,&l_1210,&l_1210,&l_1210},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210}},{{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0}},{{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210},{(void*)0,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,(void*)0},{&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210,&l_1210}}};
        uint16_t l_1220 = 65535UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1142[i] = 0x6B237FBDL;
        for (g_956 = 3; (g_956 >= 0); g_956 -= 1)
        { /* block id: 412 */
            return l_1032;
        }
        for (g_178 = 0; (g_178 <= 3); g_178 += 1)
        { /* block id: 417 */
            uint8_t l_1033 = 0x2AL;
            int32_t ****l_1034 = &g_591;
            float *l_1035 = (void*)0;
            int32_t l_1074 = 2L;
            int32_t l_1086 = 7L;
            int32_t l_1087 = 9L;
            int32_t l_1089[6] = {(-5L),0x02FB053EL,(-5L),(-5L),0x02FB053EL,(-5L)};
            int32_t l_1090 = 0xAB0B9921L;
            int64_t l_1119 = 0xD575D29A5CE71D88LL;
            const int64_t l_1190[7][10] = {{0xB97AA8EB2332292ELL,0x36B0AB5C1BB2A5BELL,0xB97AA8EB2332292ELL,7L,0x36B0AB5C1BB2A5BELL,0xBB8DDF11FDEC32A5LL,0xBB8DDF11FDEC32A5LL,0x36B0AB5C1BB2A5BELL,7L,0xB97AA8EB2332292ELL},{0x91ADD9B912252A1DLL,0x91ADD9B912252A1DLL,0x2AC663E2661C558DLL,0x36B0AB5C1BB2A5BELL,4L,0x2AC663E2661C558DLL,4L,0x36B0AB5C1BB2A5BELL,0x2AC663E2661C558DLL,0x91ADD9B912252A1DLL},{4L,0xBB8DDF11FDEC32A5LL,0xB97AA8EB2332292ELL,4L,0L,0L,7L,0xB2DDA6519B39CAA5LL,0x2AC663E2661C558DLL,7L},{0xB2DDA6519B39CAA5LL,0xB97AA8EB2332292ELL,0x2AC663E2661C558DLL,0L,0xB97AA8EB2332292ELL,0L,0x2AC663E2661C558DLL,0xB97AA8EB2332292ELL,0xB2DDA6519B39CAA5LL,0xB2DDA6519B39CAA5LL},{7L,0xBB8DDF11FDEC32A5LL,0x91ADD9B912252A1DLL,0xB97AA8EB2332292ELL,0xB97AA8EB2332292ELL,0x91ADD9B912252A1DLL,0xBB8DDF11FDEC32A5LL,7L,0x91ADD9B912252A1DLL,7L},{0xB97AA8EB2332292ELL,0x2AC663E2661C558DLL,0L,0xB97AA8EB2332292ELL,0L,0x2AC663E2661C558DLL,0xB97AA8EB2332292ELL,0xB2DDA6519B39CAA5LL,0xB2DDA6519B39CAA5LL,0xB97AA8EB2332292ELL},{0xB2DDA6519B39CAA5LL,7L,0L,0L,7L,0xB2DDA6519B39CAA5LL,0x2AC663E2661C558DLL,7L,0x2AC663E2661C558DLL,0xB2DDA6519B39CAA5LL}};
            int i, j;
            l_1037 = ((-0x2.8p+1) <= (l_1033 != ((((*g_999) != ((*l_1034) = &g_352)) <= ((g_1003 > (p_33 = g_47)) < l_1036)) < l_1032)));
            for (g_435 = 0; (g_435 <= 3); g_435 += 1)
            { /* block id: 423 */
                const int32_t *l_1038 = &g_6[4][1];
                const int32_t **l_1039 = &g_689;
                uint32_t *l_1065 = &g_793[0][7];
                uint32_t **l_1064 = &l_1065;
                uint32_t ***l_1063[2][6] = {{&l_1064,&l_1064,&l_1064,&l_1064,&l_1064,&l_1064},{&l_1064,&l_1064,&l_1064,&l_1064,&l_1064,&l_1064}};
                uint8_t *l_1076[8][3][8] = {{{&l_1033,&g_518,(void*)0,&g_513.f6,&g_991.f6,&g_991.f6,&g_991.f6,&g_513.f6},{&g_991.f6,&g_991.f6,&g_991.f6,&g_513.f6,(void*)0,&g_518,&l_1033,(void*)0},{(void*)0,&g_47,&g_47,&g_518,&g_991.f6,&g_518,(void*)0,&g_991.f6}},{{(void*)0,&g_513.f6,&l_1033,&g_991.f6,(void*)0,&g_200,&g_47,&g_200},{&g_991.f6,&g_200,&l_1033,&g_200,&g_991.f6,&g_200,&g_200,&g_47},{&l_1033,&g_513.f6,&g_69[0],&g_991.f6,&g_47,&g_518,&g_69[0],&g_200}},{{(void*)0,&g_47,&g_69[0],&g_991.f6,&l_1033,&g_518,&g_200,&g_47},{&g_47,&g_991.f6,&l_1033,&g_47,&l_1033,&g_991.f6,&g_47,&g_47},{&g_200,&g_518,&l_1033,&g_991.f6,&g_69[0],&g_47,(void*)0,&g_200}},{{&g_69[0],&g_518,&g_47,&g_991.f6,&g_69[0],&g_513.f6,&l_1033,&g_47},{&g_200,&g_200,&g_991.f6,&g_200,&l_1033,&g_200,&g_991.f6,&g_200},{&g_47,&g_200,(void*)0,&g_991.f6,&l_1033,&g_513.f6,(void*)0,&g_991.f6}},{{(void*)0,&g_518,&g_991.f6,&g_518,&g_47,&g_47,(void*)0,(void*)0},{&l_1033,&g_518,(void*)0,&g_513.f6,&g_991.f6,&g_991.f6,&g_991.f6,&g_513.f6},{&g_991.f6,&g_991.f6,&g_991.f6,&g_513.f6,(void*)0,&g_518,&l_1033,(void*)0}},{{(void*)0,&g_47,&g_47,&g_518,&g_991.f6,&g_518,(void*)0,&g_991.f6},{(void*)0,&g_513.f6,&l_1033,&g_991.f6,(void*)0,&g_513.f6,&g_69[0],&g_513.f6},{&g_991.f6,&g_991.f6,&l_1033,&g_991.f6,&g_991.f6,&g_513.f6,(void*)0,&g_200}},{{&g_991.f6,&g_200,&g_200,&g_47,&g_69[0],&g_991.f6,(void*)0,&g_991.f6},{&g_69[0],&g_200,&g_200,&g_513.f6,&l_1033,&g_518,(void*)0,&g_991.f6},{&g_69[0],&g_991.f6,&l_1033,&g_200,&l_1033,&g_991.f6,&g_69[0],&g_991.f6}},{{(void*)0,&g_518,&l_1033,&g_513.f6,&g_200,&g_200,&g_69[0],&g_991.f6},{(void*)0,&g_991.f6,&g_69[0],&g_47,&g_200,&g_200,&g_991.f6,&g_200},{(void*)0,&g_513.f6,&g_991.f6,&g_991.f6,&l_1033,&g_991.f6,&g_991.f6,&g_513.f6}}};
                uint8_t **l_1075 = &l_1076[7][2][1];
                uint8_t ***l_1077[8] = {(void*)0,&l_1075,(void*)0,&l_1075,(void*)0,&l_1075,(void*)0,&l_1075};
                int32_t *l_1079 = &g_840[1][3];
                int32_t *l_1080 = &l_1037;
                int32_t *l_1081 = &g_840[1][3];
                int32_t *l_1082[1][4][6] = {{{&g_6[0][3],&l_1037,&g_6[0][3],&g_6[0][3],&l_1037,&g_6[0][3]},{&g_6[0][3],&l_1037,&g_6[0][3],&g_6[0][3],&l_1037,&g_6[0][3]},{&g_6[0][3],&l_1037,&g_6[0][3],&g_6[0][3],&l_1037,&g_6[0][3]},{&g_6[0][3],&l_1037,&g_6[0][3],&g_6[0][3],&l_1037,&g_6[0][3]}}};
                uint64_t l_1096[2][7] = {{18446744073709551608UL,1UL,18446744073709551608UL,1UL,18446744073709551608UL,1UL,18446744073709551608UL},{1UL,1UL,1UL,1UL,1UL,1UL,1UL}};
                int i, j, k;
                (*l_1039) = l_1038;
                for (g_138 = 0; (g_138 <= 7); g_138 += 1)
                { /* block id: 427 */
                    uint32_t ***l_1067 = &l_1064;
                    uint32_t ***l_1068 = &l_1064;
                    uint32_t ***l_1069[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_1069[i] = &l_1064;
                    if ((safe_lshift_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((0UL > g_74[(g_178 + 1)]), (**g_601))), g_129)), (safe_sub_func_int8_t_s_s(0xD2L, ((safe_add_func_int16_t_s_s((safe_sub_func_int8_t_s_s(((safe_mod_func_uint16_t_u_u(65529UL, l_1032)) & (safe_div_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((((safe_unary_minus_func_uint32_t_u(((safe_rshift_func_int8_t_s_u((((safe_sub_func_int32_t_s_s((****l_1034), (l_1037 ^ l_1037))) > 0x5F52L) ^ (-7L)), l_1036)) != (****l_1034)))) , g_69[0]) >= (**l_1039)), l_1032)), l_1037))), 0x5DL)), 65532UL)) >= 0xADL))))))
                    { /* block id: 428 */
                        uint32_t ****l_1066[8][3][4] = {{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}}};
                        int i, j, k;
                        l_1073 = (l_1070[0][2] = (l_1069[2] = (l_1068 = (l_1067 = l_1063[0][0]))));
                    }
                    else
                    { /* block id: 434 */
                        p_33 = (((****l_1034) < (*l_1038)) , l_1074);
                        if ((*g_353))
                            break;
                        if (g_74[(g_178 + 1)])
                            break;
                    }
                    return (**g_352);
                }
                l_1078 = l_1075;
                l_1096[0][4]--;
            }
            for (g_200 = 0; (g_200 <= 3); g_200 += 1)
            { /* block id: 446 */
                int64_t l_1107[2][5][10] = {{{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L}},{{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L},{1L,4L,1L,4L,1L,4L,1L,4L,1L,4L}}};
                int32_t l_1115[4][5] = {{0L,(-3L),1L,(-3L),0L},{0L,(-3L),1L,(-3L),0L},{0L,(-3L),1L,(-3L),0L},{0L,(-3L),1L,(-3L),0L}};
                int16_t l_1118 = 1L;
                uint32_t *l_1164[8][4][3] = {{{&g_991.f4,&g_991.f4,&g_513.f4},{&g_991.f4,&g_991.f4,&g_513.f4},{(void*)0,&g_991.f4,&g_991.f4},{&g_991.f4,&g_513.f4,&g_991.f4}},{{&g_991.f4,(void*)0,(void*)0},{&g_513.f4,&g_513.f4,&g_513.f4},{&g_513.f4,&g_513.f4,&g_991.f4},{(void*)0,&g_513.f4,(void*)0}},{{&g_513.f4,(void*)0,&g_991.f4},{&g_513.f4,&g_991.f4,&g_991.f4},{&g_991.f4,(void*)0,(void*)0},{&g_991.f4,&g_513.f4,&g_513.f4}},{{(void*)0,&g_513.f4,(void*)0},{&g_991.f4,&g_991.f4,(void*)0},{&g_991.f4,&g_513.f4,&g_513.f4},{(void*)0,(void*)0,(void*)0}},{{&g_991.f4,&g_991.f4,&g_991.f4},{&g_991.f4,&g_513.f4,&g_991.f4},{&g_991.f4,&g_991.f4,(void*)0},{(void*)0,(void*)0,&g_991.f4}},{{&g_991.f4,&g_513.f4,&g_991.f4},{&g_513.f4,(void*)0,&g_991.f4},{&g_513.f4,(void*)0,&g_991.f4},{(void*)0,(void*)0,(void*)0}},{{&g_513.f4,(void*)0,&g_991.f4},{&g_513.f4,&g_513.f4,&g_991.f4},{&g_513.f4,(void*)0,(void*)0},{&g_513.f4,&g_513.f4,&g_991.f4}},{{(void*)0,(void*)0,&g_991.f4},{&g_513.f4,&g_991.f4,&g_513.f4},{&g_513.f4,&g_991.f4,&g_513.f4},{&g_991.f4,&g_991.f4,(void*)0}}};
                uint32_t **l_1163 = &l_1164[5][2][2];
                int i, j, k;
            }
        }
        for (g_435 = 0; (g_435 <= 3); g_435 += 1)
        { /* block id: 499 */
            int32_t l_1192[5][5][9] = {{{0xD3EE81B4L,(-1L),0xED7C8457L,0xEE016C3CL,0x133D9136L,0L,0x8244913FL,1L,1L},{0xDE5D7B10L,9L,0xE5612FA2L,0xA4A9AC02L,1L,0xA4A9AC02L,0xE5612FA2L,9L,0xDE5D7B10L},{0xED7C8457L,0x7EBDCC77L,0x79510461L,0xF9A91199L,(-7L),0xD3EE81B4L,0xC34CFDDAL,1L,9L},{0xA4A9AC02L,0x6E524F7AL,0xF6C506E1L,9L,0x2DA90EB0L,1L,(-10L),(-1L),0xD3EE81B4L},{0xED7C8457L,(-7L),0xC34CFDDAL,0x17F5FDECL,0xFE6899DAL,(-9L),0L,0x078C1CE2L,(-7L)}},{{0xDE5D7B10L,1L,1L,0xFE6899DAL,0x9D12DD73L,0x17F5FDECL,1L,0xDE5D7B10L,0xC34CFDDAL},{0xD3EE81B4L,0L,1L,0L,0x306D2E27L,0x6E524F7AL,0xFE6899DAL,0x6E524F7AL,0x306D2E27L},{0x6E524F7AL,0xC34CFDDAL,0xC34CFDDAL,0x6E524F7AL,0xDE5D7B10L,(-1L),0x8244913FL,9L,0x078C1CE2L},{9L,0xDE5D7B10L,0xF6C506E1L,0L,1L,0x306D2E27L,(-7L),0x91F3A7BBL,0x79510461L},{0x4CE20055L,0xEE016C3CL,0x79510461L,(-1L),0xDE5D7B10L,9L,0xED7C8457L,1L,1L}},{{0xA4A9AC02L,1L,0xE5612FA2L,1L,0x306D2E27L,(-10L),0x17F5FDECL,(-1L),0x6E524F7AL},{1L,0x8244913FL,0xED7C8457L,2L,0x9D12DD73L,(-9L),0x17F5FDECL,(-1L),0x8244913FL},{0x79510461L,0xED7C8457L,0xAFE78CFCL,0xFE6899DAL,0xFE6899DAL,0xAFE78CFCL,0xED7C8457L,0x79510461L,(-1L)},{1L,0L,0xF1AAC31DL,(-10L),0x2DA90EB0L,0x7EBDCC77L,(-7L),0xD3EE81B4L,0x306D2E27L},{1L,0x4CE20055L,0x078C1CE2L,0x7EBDCC77L,(-7L),0x2DA90EB0L,0x8244913FL,0xEE016C3CL,(-1L)}},{{0x91F3A7BBL,0x79510461L,0x9D12DD73L,(-1L),1L,0xF9A91199L,0xFE6899DAL,0x133D9136L,0x8244913FL},{0xC34CFDDAL,9L,0x79510461L,0x2DA90EB0L,0x133D9136L,1L,1L,0xED7C8457L,0x6E524F7AL},{0xA4A9AC02L,9L,(-7L),(-1L),1L,0xF6C506E1L,0xF6C506E1L,1L,(-1L)},{0xF9A91199L,0xE7127E58L,0xF9A91199L,(-7L),(-7L),0x133D9136L,0xE5612FA2L,0x306D2E27L,0xE7127E58L},{0x033E2C89L,(-1L),0xE5612FA2L,0xDE5D7B10L,0x8244913FL,0x9D12DD73L,0x306D2E27L,0x033E2C89L,0x2DA90EB0L}},{{1L,0xF1AAC31DL,0xFE6899DAL,(-7L),2L,(-1L),9L,0x4CE20055L,0L},{1L,(-1L),0xA4A9AC02L,(-1L),0x58A3450AL,(-10L),0x033E2C89L,1L,0x306D2E27L},{1L,0x033E2C89L,0x79510461L,0xAFE78CFCL,0xEE016C3CL,(-10L),0x133D9136L,6L,6L},{0L,(-1L),0xE7127E58L,1L,0xE7127E58L,(-1L),0L,(-1L),0x078C1CE2L},{2L,0xC34CFDDAL,0xDE5D7B10L,1L,0x17F5FDECL,0x9D12DD73L,0xFE6899DAL,1L,1L}}};
            int32_t l_1223 = 0L;
            int i, j, k;
            if (l_1192[3][0][4])
                break;
            for (g_518 = 0; (g_518 <= 3); g_518 += 1)
            { /* block id: 503 */
                int8_t l_1207 = 0x37L;
                int16_t *l_1208[4][8];
                uint8_t ***l_1211[3];
                int8_t *l_1221 = (void*)0;
                int i, j;
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 8; j++)
                        l_1208[i][j] = &g_790;
                }
                for (i = 0; i < 3; i++)
                    l_1211[i] = &l_1209[0][1][1];
                (*g_353) = ((l_1223 = (safe_add_func_uint64_t_u_u(((safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s((safe_rshift_func_int16_t_s_u((((safe_lshift_func_int8_t_s_s((safe_add_func_uint16_t_u_u(l_1192[3][0][4], (g_787 = (l_1083 = l_1207)))), 7)) | (l_1192[3][0][1] , (((l_1212 = l_1209[2][6][6]) == (void*)0) <= ((l_1084[7][3][1] & (safe_lshift_func_int8_t_s_u((l_1092 &= (((safe_rshift_func_int16_t_s_s((safe_sub_func_int8_t_s_s(((g_435 || (((((void*)0 == (*g_441)) >= l_1220) != 0x6.14118Ep-79) , 0L)) >= l_1142[0]), l_1032)), 0)) < l_1093) ^ l_1142[0])), l_1222))) ^ l_1192[3][0][4])))) , (-8L)), 10)), 5)), l_1207)) >= l_1192[3][0][4]), (-10L)))) , 0x979A739EL);
            }
        }
    }
    for (g_790 = 7; (g_790 >= 0); g_790 -= 1)
    { /* block id: 515 */
        int32_t l_1234 = (-5L);
        int16_t *l_1237 = &g_787;
        int16_t *l_1244 = &g_1245;
        uint16_t *l_1246[6];
        int16_t l_1248 = 0x38A4L;
        int32_t l_1253 = 1L;
        int32_t l_1258 = 0x2CD8B3CBL;
        int32_t l_1259[3][9][6] = {{{0x9E194C93L,0x7E7FEE5BL,0xA1F6D378L,(-1L),(-1L),0xA1F6D378L},{(-10L),(-10L),0x8B348690L,0xA1F6D378L,1L,0x3B503233L},{0x3B503233L,0L,(-10L),0x7E7FEE5BL,4L,0x8B348690L},{0L,0x3B503233L,(-10L),0x9E194C93L,(-10L),0x3B503233L},{0xA2FCD2F2L,0x9E194C93L,0x8B348690L,0xEB576445L,(-1L),0xA1F6D378L},{0xEB576445L,(-1L),0xA1F6D378L,(-10L),(-5L),(-1L)},{0x77EC205FL,(-1L),1L,1L,(-1L),0x77EC205FL},{(-1L),0x9E194C93L,0x3B503233L,0L,(-10L),0x7E7FEE5BL},{1L,0x3B503233L,(-5L),0L,4L,(-1L)}},{{1L,0L,0L,0L,1L,(-10L)},{(-1L),(-10L),4L,1L,(-1L),0xA2FCD2F2L},{0x77EC205FL,0x7E7FEE5BL,(-1L),(-10L),0xA2FCD2F2L,0xA2FCD2F2L},{0xEB576445L,4L,4L,0xEB576445L,(-1L),(-10L)},{0xA2FCD2F2L,0x8B348690L,0L,0x9E194C93L,0L,(-1L)},{0L,0x77EC205FL,(-5L),0x7E7FEE5BL,0L,0x7E7FEE5BL},{0x3B503233L,0x8B348690L,0x3B503233L,0xA1F6D378L,(-1L),0x77EC205FL},{(-10L),4L,1L,(-1L),0xA2FCD2F2L,(-1L)},{0x9E194C93L,0x7E7FEE5BL,0xA1F6D378L,(-1L),(-1L),0xA1F6D378L}},{{(-10L),(-10L),0x8B348690L,0xA1F6D378L,1L,0x3B503233L},{0x3B503233L,0L,(-10L),0x7E7FEE5BL,4L,0x8B348690L},{0L,(-1L),0L,0xEB576445L,0L,(-1L)},{0xA1F6D378L,0xEB576445L,0x7E7FEE5BL,0xA2FCD2F2L,4L,(-1L)},{0xA2FCD2F2L,4L,(-1L),0L,(-1L),0x8B348690L},{0L,4L,0x3B503233L,0x3B503233L,4L,0L},{4L,0xEB576445L,(-1L),(-10L),0L,0x77EC205FL},{0x3B503233L,(-1L),(-1L),0x9E194C93L,(-5L),1L},{0x3B503233L,(-10L),0x9E194C93L,(-10L),0x3B503233L,0L}}};
        uint64_t l_1269 = 8UL;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_1246[i] = &g_129;
        (****g_999) &= (((g_74[5] |= (safe_add_func_uint64_t_u_u(((**g_190)++), (safe_div_func_uint16_t_u_u((((l_1036 == (safe_rshift_func_uint8_t_u_s(((*l_1213) = 2UL), 7))) < ((safe_add_func_uint64_t_u_u(0xFD9FD04843E54AB8LL, (l_1234 || ((*l_1237) |= (safe_lshift_func_int8_t_s_u(l_1234, 1)))))) == l_1234)) | (((l_1238[7] = &g_591) != l_1239) , ((safe_mul_func_int16_t_s_s(((*l_1244) = (safe_div_func_uint16_t_u_u(((**g_601) = (**g_601)), 0xC8F7L))), l_1234)) | (**g_1188)))), 0x7F23L))))) >= l_1234) && l_1234);
        for (g_70 = 1; (g_70 <= 8); g_70 += 1)
        { /* block id: 526 */
            uint16_t l_1250 = 0x40C5L;
            int32_t l_1254 = 0xABCD8C4BL;
            int32_t l_1256 = (-1L);
            int32_t l_1257 = 0L;
            int32_t l_1261 = 0xC71C584FL;
            int32_t l_1264 = 1L;
            int32_t l_1266 = 0xF4D0D90EL;
            int32_t l_1267 = (-8L);
            uint32_t *l_1279 = &g_70;
            uint32_t **l_1278 = &l_1279;
            for (g_513.f3 = 0; (g_513.f3 <= 7); g_513.f3 += 1)
            { /* block id: 529 */
                float l_1249 = (-0x1.0p+1);
                int32_t l_1255 = 1L;
                int32_t l_1260 = 0xD90C95BFL;
                int32_t l_1262 = 0x655AE619L;
                int32_t l_1263 = 0x1F457BCBL;
                int32_t l_1265 = 0x3FE881F1L;
                int32_t l_1268[5] = {0x71C34898L,0x71C34898L,0x71C34898L,0x71C34898L,0x71C34898L};
                int i;
                l_1250++;
                l_1269++;
                for (l_1258 = 2; (l_1258 >= 0); l_1258 -= 1)
                { /* block id: 534 */
                    uint8_t l_1280 = 0UL;
                    int i, j, k;
                    (*g_353) = ((safe_mod_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_s(((l_1278 != ((l_1280 |= (*g_246)) , g_1125[0])) , (((void*)0 != g_263[g_513.f3][(l_1258 + 3)][l_1258]) || ((safe_add_func_uint64_t_u_u((l_1266 | (-9L)), (safe_sub_func_int16_t_s_s(g_956, ((safe_mod_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u(((l_1256 = (((safe_add_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((((safe_div_func_int16_t_s_s((g_263[(l_1258 + 4)][(l_1258 + 4)][l_1258] != (void*)0), 0xECE3L)) | l_1280) >= l_1280), g_200)), l_1248)) < g_70) & l_1280)) >= g_1003), 1)), (*g_146))), 0x9CL)) , 0x21A4L))))) & 0x6DABL))), l_1265)), l_1259[1][8][1])) && l_1262), l_1253)) <= g_156[3]);
                }
            }
        }
    }
    l_1297 = (***g_999);
    return (*****g_998);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_37(const int32_t  p_38)
{ /* block id: 4 */
    uint8_t *l_46 = &g_47;
    int32_t l_976 = (-1L);
    int8_t ****l_984 = (void*)0;
    int8_t *****l_983[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const int32_t ** const l_1011 = &g_689;
    const int32_t ** const *l_1010[4][10] = {{&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011},{&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011},{&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011},{&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011,&l_1011}};
    const int32_t ** const **l_1009 = &l_1010[2][9];
    int32_t l_1016[2];
    int i, j;
    for (i = 0; i < 2; i++)
        l_1016[i] = 0L;
    return p_38;
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_6 g_65 g_70 g_74 g_69 g_353 g_207 g_793 g_246 g_178 g_454 g_252 g_659 g_790 g_352 g_88 g_601 g_689 g_156 g_513.f4
 * writes: g_65 g_69 g_70 g_74 g_207 g_208 g_353 g_252 g_956
 */
static int32_t  func_41(int32_t  p_42, int8_t  p_43, uint8_t  p_44, float  p_45)
{ /* block id: 6 */
    const int64_t l_59[4][6][5] = {{{0L,0L,0xBA01C9AC7196A48ALL,0x4792D8C20E5E2E4BLL,1L},{0L,0xC6C2DB602708CDB5LL,0xA24E228C51F7EE04LL,0xAE9822C2642A2CA0LL,0xC6C2DB602708CDB5LL},{0x6BBB30732BAB2BD6LL,0L,(-1L),2L,0x84BBC1846E972F33LL},{4L,0xC6C2DB602708CDB5LL,0x85C2C102D1F73E70LL,0xBFA2F90942075D7BLL,0x601BA2AAC5DA298BLL},{0x767EE7A741179D74LL,0L,(-1L),0x84BBC1846E972F33LL,0xA52FA57A880E1B4ALL},{3L,0x4E194E4064544A95LL,0x4E194E4064544A95LL,3L,6L}},{{1L,0x6FFBBA6F2DB44E93LL,0xA2AD41D66485393FLL,0L,6L},{0L,0x601BA2AAC5DA298BLL,5L,6L,4L},{6L,(-1L),2L,0L,0x84BBC1846E972F33LL},{0xF48B4F12B152D5F2LL,(-1L),1L,3L,0x45490A9DAEE1D35ELL},{0x88841F9F80411BC3LL,0x6BBB30732BAB2BD6LL,0x84BBC1846E972F33LL,0x84BBC1846E972F33LL,0x6BBB30732BAB2BD6LL},{0xC6C2DB602708CDB5LL,0L,(-5L),0xBFA2F90942075D7BLL,0xAE9822C2642A2CA0LL}},{{0x4792D8C20E5E2E4BLL,1L,0xA2AD41D66485393FLL,2L,0x6FFBBA6F2DB44E93LL},{(-5L),0xF48B4F12B152D5F2LL,0L,0xAE9822C2642A2CA0LL,(-1L)},{0x4792D8C20E5E2E4BLL,(-1L),0x5A975881DD571B4DLL,0x4792D8C20E5E2E4BLL,0L},{0xC6C2DB602708CDB5LL,3L,0x85C2C102D1F73E70LL,0xF48B4F12B152D5F2LL,0xF48B4F12B152D5F2LL},{0x88841F9F80411BC3LL,9L,0x88841F9F80411BC3LL,0L,0x4792D8C20E5E2E4BLL},{0xF48B4F12B152D5F2LL,0x4E194E4064544A95LL,6L,(-1L),0xAE9822C2642A2CA0LL}},{{6L,0xA52FA57A880E1B4ALL,0xBA01C9AC7196A48ALL,0x6FFBBA6F2DB44E93LL,2L},{0L,6L,1L,0xA9B2235412097756LL,6L},{0L,0xA2AD41D66485393FLL,(-1L),2L,0x2FFABE4BCA0F9639LL},{0L,0xA24E228C51F7EE04LL,(-1L),0xA24E228C51F7EE04LL,0L},{0xECBC59A6287676E3LL,0x88841F9F80411BC3LL,0xBA01C9AC7196A48ALL,0x2FFABE4BCA0F9639LL,(-1L)},{0L,0xAC42121A659E808ELL,0x85C2C102D1F73E70LL,0L,1L}}};
    uint64_t *l_64 = &g_65;
    uint8_t *l_68 = &g_69[0];
    int32_t l_71[8][7] = {{0L,1L,(-8L),0L,0xD598C55CL,0x10F8796DL,1L},{0xC6B82E5CL,0xDF2DC135L,0xC6498872L,2L,0xC6498872L,0xDF2DC135L,0xC6B82E5CL},{0xDF2DC135L,1L,0x133A9B92L,0xC6498872L,0xC6B82E5CL,0xDF2DC135L,0xC6498872L},{0L,0xD598C55CL,0x10F8796DL,1L,1L,0x10F8796DL,0xD598C55CL},{1L,(-3L),0x133A9B92L,2L,(-3L),0xC6498872L,0xD598C55CL},{0xC4023A1BL,1L,0xC6498872L,0xC4023A1BL,0xD598C55CL,0xC4023A1BL,0xC6498872L},{0xC6B82E5CL,0xC6B82E5CL,(-8L),2L,1L,0x0DA2B3C2L,0xC6B82E5CL},{0xC6B82E5CL,0xC6498872L,0x133A9B92L,1L,0xDF2DC135L,0xDF2DC135L,1L}};
    uint16_t *l_72 = (void*)0;
    uint16_t *l_73 = &g_74[3];
    int32_t l_950 = 0x2924171CL;
    int64_t *l_955 = &g_252;
    float *l_975 = (void*)0;
    int i, j, k;
    l_950 ^= (safe_lshift_func_uint8_t_u_s(((((*l_73) = ((func_52(g_47, ((*l_73) |= ((l_71[7][0] = (safe_unary_minus_func_int16_t_s((((-8L) && (g_70 |= ((((*l_68) = ((safe_mul_func_uint8_t_u_u(l_59[1][5][1], l_59[1][5][1])) & (safe_sub_func_int32_t_s_s((g_6[0][3] < (safe_add_func_int8_t_s_s(g_47, (((*l_64) |= (p_42 || l_59[1][5][1])) && (((safe_sub_func_int8_t_s_s(0x1FL, g_6[0][3])) , 0x9E84L) <= 65535UL))))), l_59[3][4][2])))) <= l_59[1][5][1]) || g_65))) & 0xA8D4E4AF0E761D3FLL)))) , p_44)), l_68) , 0x9693L) && 5UL)) ^ (**g_601)) == p_44), p_44));
    l_71[1][4] = (safe_rshift_func_uint8_t_u_u(((((safe_sub_func_int64_t_s_s((g_956 = ((*l_955) = 8L)), (safe_mod_func_int8_t_s_s((safe_mod_func_int64_t_s_s(((safe_lshift_func_uint16_t_u_u(p_44, (((((((l_950 &= ((*l_64) = 1UL)) && p_44) ^ ((((safe_sub_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((((8UL && l_71[7][0]) && ((safe_sub_func_uint64_t_u_u(((safe_rshift_func_int16_t_s_s((((((p_44 && (0xA694L ^ (safe_mul_func_uint16_t_u_u(p_43, 0x74CEL)))) , p_43) | (*g_689)) == g_156[3]) , g_156[3]), 15)) > g_207), l_59[0][2][0])) , l_71[7][3])) == 0xA0E7F51D4DBBD2ADLL), l_71[0][0])), g_513.f4)), p_44)) ^ 0x59F33C9BL) <= p_44) , 18446744073709551615UL)) != 0x0AL) , 0xE6L) || 0xEFL) < 0xA800L))) , 1L), 6UL)), l_71[1][3])))) , g_252) && (**g_601)) & l_59[1][5][1]), l_71[6][6]));
    p_45 = l_71[7][0];
    return l_71[7][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_69 g_353 g_207 g_793 g_246 g_178 g_454 g_252 g_659 g_790 g_352 g_88
 * writes: g_207 g_208 g_353
 */
static int16_t  func_52(float  p_53, uint16_t  p_54, uint8_t * p_55)
{ /* block id: 12 */
    uint64_t l_106 = 0x9C33BC337E5C3585LL;
    int32_t l_127 = 7L;
    uint64_t ****l_153 = &g_144;
    int32_t l_209 = 2L;
    int32_t l_210 = 0x5D6FB8A8L;
    uint16_t *l_247 = &g_129;
    int8_t *l_345[8][4][6] = {{{&g_2[0],(void*)0,&g_2[0],&g_2[3],&g_2[0],&g_2[0]},{&g_2[2],&g_2[3],&g_2[3],&g_2[1],&g_2[0],&g_2[2]},{&g_2[3],(void*)0,&g_2[1],&g_2[1],(void*)0,&g_2[3]},{&g_2[2],&g_2[0],&g_2[1],&g_2[3],&g_2[3],&g_2[2]}},{{&g_2[0],&g_2[0],&g_2[3],&g_2[0],(void*)0,&g_2[0]},{&g_2[0],(void*)0,&g_2[0],&g_2[3],&g_2[0],&g_2[0]},{&g_2[2],&g_2[3],&g_2[3],&g_2[1],&g_2[0],&g_2[2]},{&g_2[3],(void*)0,&g_2[1],&g_2[1],(void*)0,&g_2[3]}},{{&g_2[2],&g_2[0],&g_2[1],&g_2[3],&g_2[3],&g_2[2]},{&g_2[0],&g_2[0],&g_2[3],&g_2[0],(void*)0,&g_2[0]},{&g_2[0],(void*)0,&g_2[0],&g_2[3],&g_2[0],&g_2[0]},{&g_2[2],&g_2[3],&g_2[3],&g_2[1],&g_2[0],&g_2[2]}},{{&g_2[3],(void*)0,&g_2[1],&g_2[1],(void*)0,&g_2[3]},{&g_2[2],&g_2[0],&g_2[1],&g_2[3],&g_2[3],&g_2[2]},{&g_2[0],&g_2[0],&g_2[3],&g_2[0],(void*)0,&g_2[0]},{&g_2[0],(void*)0,&g_2[0],&g_2[3],&g_2[0],&g_2[0]}},{{&g_2[2],&g_2[3],&g_2[3],&g_2[1],&g_2[0],&g_2[2]},{&g_2[3],(void*)0,&g_2[1],&g_2[1],(void*)0,&g_2[3]},{&g_2[2],&g_2[0],&g_2[1],&g_2[0],&g_2[3],&g_2[2]},{&g_2[2],&g_2[0],&g_2[0],&g_2[2],&g_2[2],&g_2[2]}},{{&g_2[2],&g_2[2],&g_2[2],&g_2[0],&g_2[0],&g_2[2]},{&g_2[2],&g_2[3],&g_2[0],&g_88,&g_2[0],&g_2[2]},{&g_2[0],&g_2[2],&g_88,&g_88,&g_2[2],&g_2[0]},{&g_2[2],&g_2[0],&g_88,&g_2[0],&g_2[3],&g_2[2]}},{{&g_2[2],&g_2[0],&g_2[0],&g_2[2],&g_2[2],&g_2[2]},{&g_2[2],&g_2[2],&g_2[2],&g_2[0],&g_2[0],&g_2[2]},{&g_2[2],&g_2[3],&g_2[0],&g_88,&g_2[0],&g_2[2]},{&g_2[0],&g_2[2],&g_88,&g_88,&g_2[2],&g_2[0]}},{{&g_2[2],&g_2[0],&g_88,&g_2[0],&g_2[3],&g_2[2]},{&g_2[2],&g_2[0],&g_2[0],&g_2[2],&g_2[2],&g_2[2]},{&g_2[2],&g_2[2],&g_2[2],&g_2[0],&g_2[0],&g_2[2]},{&g_2[2],&g_2[3],&g_2[0],&g_88,&g_2[0],&g_2[2]}}};
    int8_t **l_344 = &l_345[5][2][1];
    int32_t l_397 = 0x0FE08873L;
    const uint16_t l_406[6][7][6] = {{{0x4009L,0x1C55L,0x4009L,0UL,1UL,0x0D7AL},{65535UL,65533UL,0UL,0xF7F5L,65530UL,0x7F5DL},{65535UL,0x3700L,0UL,0xF7F5L,0x062EL,0UL},{65535UL,7UL,0x007DL,0UL,0UL,0x4009L},{0x4009L,0xAAF1L,65535UL,65527UL,65533UL,0x007DL},{8UL,0xBA3EL,65535UL,65535UL,7UL,0x4009L},{2UL,0x1E4AL,0x007DL,0xF852L,65535UL,0UL}},{{0xF852L,65535UL,0UL,65535UL,0x9374L,0x7F5DL},{0x0D7AL,65535UL,0UL,0UL,65535UL,0x0D7AL},{0xCAB4L,0x1E4AL,0x4009L,1UL,7UL,65527UL},{0UL,0xBA3EL,1UL,8UL,65533UL,0xF7F5L},{0UL,0xAAF1L,8UL,1UL,0UL,65535UL},{0xCAB4L,7UL,0xCA8DL,0UL,0x062EL,2UL},{0x0D7AL,0x3700L,0xF7F5L,65535UL,65530UL,2UL}},{{0xF852L,65533UL,0xCA8DL,0xF852L,1UL,65535UL},{2UL,0x1C55L,8UL,65535UL,0xAAF1L,0xF7F5L},{8UL,0xC334L,1UL,65527UL,0xAAF1L,65527UL},{0x4009L,0x1C55L,0x4009L,0UL,1UL,0x0D7AL},{65535UL,1UL,0x5F86L,0xFCC9L,0UL,0UL},{0xB782L,0x0D7AL,9UL,0xFCC9L,0UL,9UL},{8UL,8UL,0xBE23L,9UL,0x4009L,9UL}},{{9UL,65535UL,8UL,1UL,1UL,0xBE23L},{65532UL,0x7F5DL,8UL,0xB782L,8UL,9UL},{0x52CEL,0xF852L,0xBE23L,0UL,0xCA8DL,9UL},{0UL,0xCA8DL,9UL,8UL,0xF7F5L,0UL},{65526UL,0xCA8DL,0x5F86L,0x5F86L,0xCA8DL,65526UL},{1UL,0xF852L,9UL,0x87B6L,8UL,1UL},{0x5F86L,0x7F5DL,65535UL,65532UL,1UL,0xFCC9L}},{{0x5F86L,65535UL,65532UL,0x87B6L,0x4009L,8UL},{1UL,8UL,0UL,0x5F86L,0UL,0x52CEL},{65526UL,0x0D7AL,0xFCC9L,8UL,0UL,0x52CEL},{0UL,1UL,0UL,0UL,0x007DL,8UL},{0x52CEL,65527UL,65532UL,0xB782L,65535UL,0xFCC9L},{65532UL,1UL,65535UL,1UL,65535UL,1UL},{9UL,65527UL,9UL,9UL,0x007DL,65526UL}},{{8UL,1UL,0x5F86L,0xFCC9L,0UL,0UL},{0xB782L,0x0D7AL,9UL,0xFCC9L,0UL,9UL},{8UL,8UL,0xBE23L,9UL,0x4009L,9UL},{9UL,65535UL,8UL,1UL,1UL,0xBE23L},{65532UL,0x7F5DL,8UL,0xB782L,8UL,9UL},{0x52CEL,0xF852L,0xBE23L,0UL,0xCA8DL,9UL},{0UL,0xCA8DL,9UL,8UL,0xF7F5L,0UL}}};
    struct S0 *l_512 = &g_513;
    int8_t ***l_716 = &l_344;
    int8_t ****l_715 = &l_716;
    int8_t *****l_714 = &l_715;
    int32_t ****l_717 = &g_591;
    const uint32_t *l_762 = &g_736;
    int8_t l_770 = 0x90L;
    int8_t l_785[4];
    uint64_t l_852[5][6] = {{18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL},{18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL},{18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL},{18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL},{18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL,18446744073709551607UL,0x472C90B7612125BFLL}};
    int16_t l_938 = 4L;
    uint16_t l_947 = 0x831CL;
    float *l_948 = (void*)0;
    float *l_949 = &g_208[3];
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_785[i] = 0xF2L;
    if ((&g_69[0] == (void*)0))
    { /* block id: 13 */
        const int32_t *l_75 = &g_6[5][2];
        const int32_t **l_76 = &l_75;
        uint8_t *l_107 = (void*)0;
        int32_t l_124 = 8L;
        uint64_t ***l_147 = &g_145[2][2][1];
        uint64_t *l_157 = &g_65;
        int16_t l_158 = (-10L);
        float l_230 = 0x8.Dp-1;
        const int8_t *l_396 = &g_156[5];
        const int8_t **l_395 = &l_396;
        const int8_t ***l_394 = &l_395;
        const int8_t ****l_393 = &l_394;
        const int8_t *****l_392[9][9] = {{(void*)0,(void*)0,&l_393,&l_393,&l_393,&l_393,&l_393,(void*)0,(void*)0},{&l_393,&l_393,&l_393,&l_393,(void*)0,&l_393,&l_393,&l_393,(void*)0},{&l_393,&l_393,&l_393,&l_393,(void*)0,(void*)0,&l_393,&l_393,&l_393},{&l_393,(void*)0,&l_393,&l_393,&l_393,&l_393,(void*)0,&l_393,&l_393},{&l_393,&l_393,&l_393,(void*)0,(void*)0,&l_393,&l_393,(void*)0,(void*)0},{&l_393,&l_393,&l_393,&l_393,(void*)0,&l_393,&l_393,&l_393,&l_393},{&l_393,&l_393,&l_393,&l_393,&l_393,&l_393,&l_393,&l_393,&l_393},{&l_393,&l_393,(void*)0,&l_393,&l_393,&l_393,&l_393,&l_393,(void*)0},{&l_393,&l_393,(void*)0,(void*)0,&l_393,&l_393,&l_393,&l_393,&l_393}};
        uint8_t l_424 = 0x57L;
        int8_t l_558 = (-1L);
        int32_t l_559 = 1L;
        int32_t ***l_589[6][3] = {{&g_352,&g_352,&g_352},{&g_352,&g_352,&g_352},{&g_352,&g_352,&g_352},{&g_352,&g_352,&g_352},{&g_352,&g_352,&g_352},{&g_352,&g_352,&g_352}};
        float l_807 = 0xC.9D207Cp+98;
        int i, j;
    }
    else
    { /* block id: 363 */
        uint64_t *****l_934 = (void*)0;
        uint8_t *l_935 = &g_518;
        (*g_353) = (safe_add_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u((((((safe_lshift_func_uint16_t_u_u(((safe_sub_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(p_54, (safe_mod_func_int64_t_s_s((((safe_sub_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_u((safe_add_func_int8_t_s_s((((safe_mul_func_uint8_t_u_u((((void*)0 != &l_715) < g_69[0]), (safe_sub_func_int64_t_s_s(((safe_rshift_func_int8_t_s_s(p_54, 6)) & (1L >= (l_934 == &g_758))), (l_935 == &g_200))))) >= (*p_55)) || (*g_353)), g_793[2][9])), (*g_246))), l_209)) <= 0x2FD4BD1CL) < 0xA9B0L), p_54)))), 0x72L)) < g_454), p_54)) ^ p_54) == p_54) && p_54) && g_252), p_54)), p_54));
    }
    (*l_949) = (p_53 < ((((safe_add_func_float_f_f(p_54, ((p_54 , &g_246) != &l_247))) == l_938) >= (g_659 == (void*)0)) , (safe_mul_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_div_func_float_f_f(g_790, p_53)), 0x1.952DF0p-13)), p_53)), l_947))));
    (*g_352) = &l_209;
    return g_88;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_4[i][j], "g_4[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_5, "g_5", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_6[i][j], "g_6[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_65, "g_65", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_69[i], "g_69[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_70, "g_70", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_74[i], "g_74[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_88, "g_88", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    transparent_crc(g_138, "g_138", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_156[i], "g_156[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_178, "g_178", print_hash_value);
    transparent_crc(g_200, "g_200", print_hash_value);
    transparent_crc(g_207, "g_207", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_208[i], sizeof(g_208[i]), "g_208[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_211, "g_211", print_hash_value);
    transparent_crc(g_252, "g_252", print_hash_value);
    transparent_crc(g_264, "g_264", print_hash_value);
    transparent_crc(g_265, "g_265", print_hash_value);
    transparent_crc(g_266, "g_266", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_267[i], "g_267[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_268, "g_268", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_269[i][j][k], "g_269[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_270, "g_270", print_hash_value);
    transparent_crc(g_271, "g_271", print_hash_value);
    transparent_crc(g_272, "g_272", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_273[i], "g_273[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_274, "g_274", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_275[i], "g_275[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_276, "g_276", print_hash_value);
    transparent_crc(g_277, "g_277", print_hash_value);
    transparent_crc(g_278, "g_278", print_hash_value);
    transparent_crc(g_279, "g_279", print_hash_value);
    transparent_crc(g_280, "g_280", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_281[i], "g_281[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_282, "g_282", print_hash_value);
    transparent_crc(g_283, "g_283", print_hash_value);
    transparent_crc(g_284, "g_284", print_hash_value);
    transparent_crc(g_285, "g_285", print_hash_value);
    transparent_crc(g_286, "g_286", print_hash_value);
    transparent_crc(g_287, "g_287", print_hash_value);
    transparent_crc(g_288, "g_288", print_hash_value);
    transparent_crc(g_289, "g_289", print_hash_value);
    transparent_crc(g_290, "g_290", print_hash_value);
    transparent_crc(g_291, "g_291", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_292[i][j], "g_292[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_293[i], "g_293[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_294, "g_294", print_hash_value);
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc(g_296, "g_296", print_hash_value);
    transparent_crc(g_297, "g_297", print_hash_value);
    transparent_crc(g_298, "g_298", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_299[i][j][k], "g_299[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_300, "g_300", print_hash_value);
    transparent_crc(g_301, "g_301", print_hash_value);
    transparent_crc(g_302, "g_302", print_hash_value);
    transparent_crc(g_303, "g_303", print_hash_value);
    transparent_crc(g_304, "g_304", print_hash_value);
    transparent_crc(g_305, "g_305", print_hash_value);
    transparent_crc(g_306, "g_306", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_307[i], "g_307[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_308, "g_308", print_hash_value);
    transparent_crc(g_309, "g_309", print_hash_value);
    transparent_crc(g_310, "g_310", print_hash_value);
    transparent_crc(g_311, "g_311", print_hash_value);
    transparent_crc(g_312, "g_312", print_hash_value);
    transparent_crc(g_313, "g_313", print_hash_value);
    transparent_crc(g_314, "g_314", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_315[i][j][k], "g_315[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_316, "g_316", print_hash_value);
    transparent_crc(g_317, "g_317", print_hash_value);
    transparent_crc(g_318, "g_318", print_hash_value);
    transparent_crc(g_319, "g_319", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_320[i], "g_320[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_321, "g_321", print_hash_value);
    transparent_crc(g_322, "g_322", print_hash_value);
    transparent_crc(g_323, "g_323", print_hash_value);
    transparent_crc(g_324, "g_324", print_hash_value);
    transparent_crc(g_325, "g_325", print_hash_value);
    transparent_crc(g_337, "g_337", print_hash_value);
    transparent_crc(g_419, "g_419", print_hash_value);
    transparent_crc(g_435, "g_435", print_hash_value);
    transparent_crc(g_454, "g_454", print_hash_value);
    transparent_crc_bytes (&g_513.f0, sizeof(g_513.f0), "g_513.f0", print_hash_value);
    transparent_crc(g_513.f1, "g_513.f1", print_hash_value);
    transparent_crc(g_513.f2, "g_513.f2", print_hash_value);
    transparent_crc(g_513.f3, "g_513.f3", print_hash_value);
    transparent_crc(g_513.f4, "g_513.f4", print_hash_value);
    transparent_crc(g_513.f5, "g_513.f5", print_hash_value);
    transparent_crc(g_513.f6, "g_513.f6", print_hash_value);
    transparent_crc(g_518, "g_518", print_hash_value);
    transparent_crc_bytes (&g_609, sizeof(g_609), "g_609", print_hash_value);
    transparent_crc_bytes (&g_660.f0, sizeof(g_660.f0), "g_660.f0", print_hash_value);
    transparent_crc(g_660.f1, "g_660.f1", print_hash_value);
    transparent_crc(g_660.f2, "g_660.f2", print_hash_value);
    transparent_crc(g_660.f3, "g_660.f3", print_hash_value);
    transparent_crc(g_660.f4, "g_660.f4", print_hash_value);
    transparent_crc(g_660.f5, "g_660.f5", print_hash_value);
    transparent_crc(g_660.f6, "g_660.f6", print_hash_value);
    transparent_crc(g_736, "g_736", print_hash_value);
    transparent_crc(g_787, "g_787", print_hash_value);
    transparent_crc(g_790, "g_790", print_hash_value);
    transparent_crc(g_792, "g_792", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_793[i][j], "g_793[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_840[i][j], "g_840[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_956, "g_956", print_hash_value);
    transparent_crc_bytes (&g_991.f0, sizeof(g_991.f0), "g_991.f0", print_hash_value);
    transparent_crc(g_991.f1, "g_991.f1", print_hash_value);
    transparent_crc(g_991.f2, "g_991.f2", print_hash_value);
    transparent_crc(g_991.f3, "g_991.f3", print_hash_value);
    transparent_crc(g_991.f4, "g_991.f4", print_hash_value);
    transparent_crc(g_991.f5, "g_991.f5", print_hash_value);
    transparent_crc(g_991.f6, "g_991.f6", print_hash_value);
    transparent_crc(g_1003, "g_1003", print_hash_value);
    transparent_crc(g_1026, "g_1026", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1120[i], "g_1120[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1127, "g_1127", print_hash_value);
    transparent_crc(g_1245, "g_1245", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_1247[i], sizeof(g_1247[i]), "g_1247[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_1342[i][j], "g_1342[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1347.f0, sizeof(g_1347.f0), "g_1347.f0", print_hash_value);
    transparent_crc(g_1347.f1, "g_1347.f1", print_hash_value);
    transparent_crc(g_1347.f2, "g_1347.f2", print_hash_value);
    transparent_crc(g_1347.f3, "g_1347.f3", print_hash_value);
    transparent_crc(g_1347.f4, "g_1347.f4", print_hash_value);
    transparent_crc(g_1347.f5, "g_1347.f5", print_hash_value);
    transparent_crc(g_1347.f6, "g_1347.f6", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc_bytes(&g_1395[i][j].f0, sizeof(g_1395[i][j].f0), "g_1395[i][j].f0", print_hash_value);
            transparent_crc(g_1395[i][j].f1, "g_1395[i][j].f1", print_hash_value);
            transparent_crc(g_1395[i][j].f2, "g_1395[i][j].f2", print_hash_value);
            transparent_crc(g_1395[i][j].f3, "g_1395[i][j].f3", print_hash_value);
            transparent_crc(g_1395[i][j].f4, "g_1395[i][j].f4", print_hash_value);
            transparent_crc(g_1395[i][j].f5, "g_1395[i][j].f5", print_hash_value);
            transparent_crc(g_1395[i][j].f6, "g_1395[i][j].f6", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1447, "g_1447", print_hash_value);
    transparent_crc(g_1529, "g_1529", print_hash_value);
    transparent_crc(g_1595, "g_1595", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc_bytes(&g_1696[i][j], sizeof(g_1696[i][j]), "g_1696[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1812, sizeof(g_1812), "g_1812", print_hash_value);
    transparent_crc_bytes (&g_1983, sizeof(g_1983), "g_1983", print_hash_value);
    transparent_crc(g_2060, "g_2060", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 476
   depth: 1, occurrence: 2
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 7
breakdown:
   indirect level: 0, occurrence: 2
   indirect level: 1, occurrence: 4
   indirect level: 2, occurrence: 1
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 5
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 2
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 2

XXX max expression depth: 40
breakdown:
   depth: 1, occurrence: 126
   depth: 2, occurrence: 47
   depth: 3, occurrence: 1
   depth: 4, occurrence: 1
   depth: 5, occurrence: 2
   depth: 8, occurrence: 1
   depth: 10, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 1
   depth: 30, occurrence: 4
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 40, occurrence: 1

XXX total number of pointers: 446

XXX times a variable address is taken: 1283
XXX times a pointer is dereferenced on RHS: 212
breakdown:
   depth: 1, occurrence: 135
   depth: 2, occurrence: 49
   depth: 3, occurrence: 15
   depth: 4, occurrence: 9
   depth: 5, occurrence: 4
XXX times a pointer is dereferenced on LHS: 242
breakdown:
   depth: 1, occurrence: 204
   depth: 2, occurrence: 19
   depth: 3, occurrence: 11
   depth: 4, occurrence: 4
   depth: 5, occurrence: 4
XXX times a pointer is compared with null: 32
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 11
XXX times a pointer is qualified to be dereferenced: 7060

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 667
   level: 2, occurrence: 216
   level: 3, occurrence: 99
   level: 4, occurrence: 92
   level: 5, occurrence: 50
XXX number of pointers point to pointers: 202
XXX number of pointers point to scalars: 240
XXX number of pointers point to structs: 4
XXX percent of pointers has null in alias set: 31.4
XXX average alias set size: 1.59

XXX times a non-volatile is read: 1566
XXX times a non-volatile is write: 801
XXX times a volatile is read: 75
XXX    times read thru a pointer: 37
XXX times a volatile is write: 17
XXX    times written thru a pointer: 7
XXX times a volatile is available for access: 2.82e+03
XXX percentage of non-volatile access: 96.3

XXX forward jumps: 0
XXX backward jumps: 12

XXX stmts: 140
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 22
   depth: 2, occurrence: 25
   depth: 3, occurrence: 18
   depth: 4, occurrence: 19
   depth: 5, occurrence: 21

XXX percentage a fresh-made variable is used: 16.6
XXX percentage an existing variable is used: 83.4
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


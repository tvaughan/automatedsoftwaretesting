/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      930334045
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   signed f0 : 13;
   volatile int8_t  f1;
   float  f2;
   int8_t  f3;
   uint8_t  f4;
   const volatile int32_t  f5;
   uint32_t  f6;
   int16_t  f7;
};

struct S1 {
   const signed f0 : 23;
   unsigned f1 : 22;
   unsigned f2 : 14;
   const unsigned f3 : 17;
};

/* --- GLOBAL VARIABLES --- */
static float g_18[8] = {0x9.3C1DE6p-17,0x0.Dp+1,0x9.3C1DE6p-17,0x9.3C1DE6p-17,0x0.Dp+1,0x9.3C1DE6p-17,0x9.3C1DE6p-17,0x0.Dp+1};
static int32_t g_25[1][4] = {{1L,1L,1L,1L}};
static int16_t g_28 = 0x9532L;
static uint16_t g_32 = 65535UL;
static uint16_t g_46 = 5UL;
static float g_48[1][10] = {{0x2.5p+1,0x2.5p+1,0x2.5p+1,0x2.5p+1,0x2.5p+1,0x2.5p+1,0x2.5p+1,0x2.5p+1,0x2.5p+1,0x2.5p+1}};
static struct S1 g_118 = {567,35,23,116};
static volatile struct S0 g_119 = {-64,0x3EL,0xC.2F7F93p-26,0x8DL,255UL,-10L,0xE789AE46L,-6L};/* VOLATILE GLOBAL g_119 */
static int32_t *g_121 = (void*)0;
static int16_t g_127 = (-5L);
static int32_t g_160 = 4L;
static uint32_t g_176 = 4UL;
static uint8_t g_183 = 0xF9L;
static int16_t g_202[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
static int32_t g_207[5] = {(-4L),(-4L),(-4L),(-4L),(-4L)};
static int8_t g_217 = (-1L);
static int32_t ** volatile g_248 = (void*)0;/* VOLATILE GLOBAL g_248 */
static const uint8_t * const  volatile **g_263 = (void*)0;
static float g_270 = (-0x1.0p-1);
static float g_273 = (-0x1.Ep+1);
static int16_t *g_276 = (void*)0;
static int16_t **g_275 = &g_276;
static volatile uint8_t **g_298 = (void*)0;
static const struct S1 *g_315 = &g_118;
static const struct S1 ** volatile g_314[9][3] = {{&g_315,&g_315,&g_315},{&g_315,&g_315,(void*)0},{&g_315,&g_315,&g_315},{&g_315,&g_315,(void*)0},{&g_315,&g_315,&g_315},{(void*)0,&g_315,&g_315},{&g_315,&g_315,&g_315},{(void*)0,&g_315,&g_315},{&g_315,&g_315,&g_315}};
static const struct S1 ** volatile g_316 = &g_315;/* VOLATILE GLOBAL g_316 */
static int32_t g_346 = 1L;
static int32_t ** volatile g_348 = &g_121;/* VOLATILE GLOBAL g_348 */
static float * volatile g_350 = &g_48[0][3];/* VOLATILE GLOBAL g_350 */
static int32_t g_393 = 0x40CED65FL;
static int8_t g_399 = 0xC1L;
static uint16_t g_416 = 0x32A2L;
static uint64_t g_426 = 8UL;
static uint8_t g_452[3][6][6] = {{{0UL,0UL,255UL,255UL,0xFFL,1UL},{255UL,1UL,251UL,0xC5L,255UL,255UL},{0x86L,251UL,254UL,254UL,251UL,0x86L},{255UL,9UL,0x27L,255UL,0UL,0xF5L},{255UL,254UL,1UL,1UL,0x70L,0x53L},{255UL,0x86L,1UL,255UL,0x41L,0x6FL}},{{255UL,0UL,0UL,254UL,0x20L,0UL},{0x86L,0x88L,0x41L,0xC5L,255UL,0x0EL},{255UL,0x53L,255UL,255UL,0xB0L,0x81L},{0UL,0xB0L,0UL,251UL,248UL,251UL},{0x0EL,0x81L,0xF5L,0x81L,0x0EL,255UL},{0xC5L,248UL,0x3CL,0xF5L,255UL,1UL}},{{0x41L,1UL,0xADL,248UL,0xB0L,1UL},{0xB0L,0x27L,0x3CL,0UL,255UL,255UL},{0xB0L,0x6FL,0xF5L,0x70L,0UL,251UL},{251UL,254UL,0UL,246UL,0x04L,0x81L},{0xFFL,0x3CL,255UL,0x7CL,0xA3L,0x0EL},{0x18L,0xB0L,0x41L,1UL,0UL,0UL}}};
static int32_t *g_483 = &g_393;
static int32_t **g_482[2] = {&g_483,&g_483};
static float * volatile g_562 = (void*)0;/* VOLATILE GLOBAL g_562 */
static float * volatile g_564 = (void*)0;/* VOLATILE GLOBAL g_564 */
static const int16_t g_585 = 0x63CCL;
static int32_t ***g_589 = (void*)0;
static int32_t ***g_591[9] = {&g_482[1],&g_482[1],&g_482[0],&g_482[1],&g_482[1],&g_482[0],&g_482[1],&g_482[1],&g_482[0]};
static float * volatile g_662 = &g_48[0][7];/* VOLATILE GLOBAL g_662 */
static volatile struct S0 g_688 = {66,0x3EL,-0x1.8p+1,-8L,0x15L,7L,1UL,0L};/* VOLATILE GLOBAL g_688 */
static uint16_t *g_690 = &g_416;
static uint16_t ** const g_689 = &g_690;
static uint64_t g_717 = 18446744073709551615UL;
static uint32_t g_761 = 4UL;
static int16_t g_777 = 0x277CL;
static struct S0 g_841 = {-60,-7L,0xD.12633Ap+38,0x43L,0x69L,0x71F68343L,0x8E131C08L,0x7E3FL};/* VOLATILE GLOBAL g_841 */
static int32_t g_850[10] = {0x30937BAFL,0x30937BAFL,0x30937BAFL,0x30937BAFL,0x30937BAFL,0x30937BAFL,0x30937BAFL,0x30937BAFL,0x30937BAFL,0x30937BAFL};
static uint32_t g_855 = 0xAD95D0D1L;
static int32_t * volatile g_892 = &g_346;/* VOLATILE GLOBAL g_892 */
static volatile int8_t g_940 = (-1L);/* VOLATILE GLOBAL g_940 */
static volatile struct S0 * const  volatile * volatile g_1011 = (void*)0;/* VOLATILE GLOBAL g_1011 */
static int32_t ** const  volatile *g_1055 = (void*)0;
static int32_t ** const  volatile **g_1054[5] = {&g_1055,&g_1055,&g_1055,&g_1055,&g_1055};
static int32_t ** const  volatile *** volatile g_1053 = &g_1054[2];/* VOLATILE GLOBAL g_1053 */
static uint32_t g_1067 = 0xFD22ED17L;
static volatile struct S0 g_1081 = {67,0x42L,0x6.0p+1,-1L,1UL,-1L,18446744073709551609UL,0x6590L};/* VOLATILE GLOBAL g_1081 */
static volatile struct S0 g_1094 = {87,0L,-0x1.Bp+1,0x9FL,0x93L,0L,8UL,0L};/* VOLATILE GLOBAL g_1094 */
static float * volatile g_1142[7] = {&g_841.f2,&g_841.f2,&g_841.f2,&g_841.f2,&g_841.f2,&g_841.f2,&g_841.f2};
static float * volatile g_1143 = &g_18[6];/* VOLATILE GLOBAL g_1143 */
static const uint8_t g_1159 = 0x23L;
static float * volatile g_1173 = &g_18[3];/* VOLATILE GLOBAL g_1173 */
static struct S1 g_1207[8] = {{-2664,917,127,115},{-2664,917,127,115},{-2664,917,127,115},{-2664,917,127,115},{-2664,917,127,115},{-2664,917,127,115},{-2664,917,127,115},{-2664,917,127,115}};
static struct S1 *g_1206[3] = {&g_1207[6],&g_1207[6],&g_1207[6]};
static struct S0 g_1241[2][2][8] = {{{{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{44,1L,0xD.22C3CAp+86,0x29L,0x32L,0L,0x5856EF86L,0x25AAL},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{44,1L,0xD.22C3CAp+86,0x29L,0x32L,0L,0x5856EF86L,0x25AAL}},{{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{44,1L,0xD.22C3CAp+86,0x29L,0x32L,0L,0x5856EF86L,0x25AAL},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L}}},{{{-31,0x52L,0xC.B7E1C8p+63,-1L,0x93L,-4L,0x0A774533L,-1L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{-31,0x52L,0xC.B7E1C8p+63,-1L,0x93L,-4L,0x0A774533L,-1L},{35,0xB2L,0xD.3D310Ap-22,0x38L,250UL,0L,18446744073709551609UL,0L},{-31,0x52L,0xC.B7E1C8p+63,-1L,0x93L,-4L,0x0A774533L,-1L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L}},{{71,0L,0x6.DC5B83p-16,0xF2L,0x94L,-7L,0UL,-1L},{35,0xB2L,0xD.3D310Ap-22,0x38L,250UL,0L,18446744073709551609UL,0L},{35,0xB2L,0xD.3D310Ap-22,0x38L,250UL,0L,18446744073709551609UL,0L},{35,0xB2L,0xD.3D310Ap-22,0x38L,250UL,0L,18446744073709551609UL,0L},{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{-31,0x52L,0xC.B7E1C8p+63,-1L,0x93L,-4L,0x0A774533L,-1L},{38,8L,0x7.99D86Bp+47,1L,0xD2L,0xC3BFFA79L,8UL,2L},{35,0xB2L,0xD.3D310Ap-22,0x38L,250UL,0L,18446744073709551609UL,0L}}}};
static const volatile int64_t g_1253 = (-1L);/* VOLATILE GLOBAL g_1253 */
static const volatile int64_t *g_1252 = &g_1253;
static const int32_t *g_1257 = &g_850[8];
static const int32_t * volatile *g_1256[10] = {&g_1257,(void*)0,&g_1257,&g_1257,(void*)0,&g_1257,&g_1257,(void*)0,&g_1257,&g_1257};
static const int32_t * volatile * volatile *g_1255 = &g_1256[5];
static const int32_t * volatile * volatile **g_1254 = &g_1255;
static int32_t **g_1318 = &g_121;
static volatile struct S0 g_1324 = {-15,0x4EL,0x1.FCBD22p+43,4L,0UL,6L,0UL,-5L};/* VOLATILE GLOBAL g_1324 */
static uint64_t *g_1395 = &g_426;
static uint64_t **g_1394[2][9] = {{&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395},{&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395,&g_1395}};
static struct S0 g_1409 = {73,-1L,0x6.4BCB5Dp+5,0x76L,0UL,-1L,0x342A6663L,0x3C7BL};/* VOLATILE GLOBAL g_1409 */
static volatile struct S0 g_1441 = {21,0x3BL,0x2.Dp+1,0x8AL,250UL,0x837DE1CAL,18446744073709551606UL,0x35D4L};/* VOLATILE GLOBAL g_1441 */
static struct S0 g_1442 = {-13,0x4AL,0x8.C12D07p+46,-1L,248UL,0x10724752L,18446744073709551615UL,0x829FL};/* VOLATILE GLOBAL g_1442 */
static int32_t g_1456 = 0x9FF370D2L;
static const struct S0 g_1465 = {64,-4L,0x3.AF5AE4p-57,0x37L,0xA5L,-1L,1UL,-9L};/* VOLATILE GLOBAL g_1465 */
static struct S0 g_1540 = {-33,-3L,0x5.4p+1,0x4EL,2UL,-1L,0x36EC6646L,1L};/* VOLATILE GLOBAL g_1540 */
static struct S0 *g_1539 = &g_1540;
static volatile uint16_t g_1555 = 6UL;/* VOLATILE GLOBAL g_1555 */
static int32_t ** volatile g_1561 = &g_121;/* VOLATILE GLOBAL g_1561 */
static const int8_t g_1609 = 0L;
static int64_t g_1643 = 4L;
static int64_t *g_1642 = &g_1643;
static struct S0 ** const  volatile g_1646[1] = {&g_1539};
static struct S0 ** volatile g_1647 = &g_1539;/* VOLATILE GLOBAL g_1647 */
static float * volatile g_1648 = &g_270;/* VOLATILE GLOBAL g_1648 */
static float * volatile g_1650 = &g_841.f2;/* VOLATILE GLOBAL g_1650 */
static struct S0 ** volatile g_1666 = &g_1539;/* VOLATILE GLOBAL g_1666 */
static volatile struct S0 g_1669 = {12,0x93L,0x5.E449DCp-71,0xBCL,255UL,0xDE7B08D4L,0UL,0x8C2EL};/* VOLATILE GLOBAL g_1669 */
static int32_t g_1731 = 0L;
static float * volatile g_1762 = &g_1442.f2;/* VOLATILE GLOBAL g_1762 */
static const int32_t g_1765 = 0x32D5CD45L;
static struct S1 **g_1769 = &g_1206[2];
static struct S1 *** volatile g_1768 = &g_1769;/* VOLATILE GLOBAL g_1768 */
static const int32_t ** volatile g_1785 = &g_1257;/* VOLATILE GLOBAL g_1785 */
static const int32_t ** const  volatile g_1787 = (void*)0;/* VOLATILE GLOBAL g_1787 */
static volatile uint64_t g_1821[9][10][2] = {{{0xB42BEC7BD30E8CA8LL,0x9E0819FB65CD2ACBLL},{0x499C2A4DD296D0EBLL,18446744073709551615UL},{0UL,0xA667C67DA2D99BCFLL},{18446744073709551615UL,0UL},{0xBA06236483EB5977LL,0x00F515F187075A8ALL},{0xBA06236483EB5977LL,0UL},{18446744073709551615UL,0xA667C67DA2D99BCFLL},{0UL,18446744073709551615UL},{0x499C2A4DD296D0EBLL,0x9E0819FB65CD2ACBLL},{0xB42BEC7BD30E8CA8LL,1UL}},{{1UL,0x499C2A4DD296D0EBLL},{1UL,0xE65B1ABE30CF05B6LL},{2UL,0x56FF0350500CFEDDLL},{18446744073709551615UL,0xF11C56E3F305EEE6LL},{0xA66B8DC494286B33LL,0x8796545BABB4A525LL},{18446744073709551612UL,18446744073709551615UL},{0xFA56EAB2C51FEFD1LL,0xA64D556C1E4672F9LL},{0x56FF0350500CFEDDLL,0xBA06236483EB5977LL},{0x1DFB655E0004CF34LL,2UL},{0x056E781AC6767173LL,2UL}},{{0x1DFB655E0004CF34LL,0xBA06236483EB5977LL},{0x56FF0350500CFEDDLL,0xA64D556C1E4672F9LL},{0xFA56EAB2C51FEFD1LL,18446744073709551615UL},{18446744073709551612UL,0x8796545BABB4A525LL},{0xA66B8DC494286B33LL,0xF11C56E3F305EEE6LL},{18446744073709551615UL,0x56FF0350500CFEDDLL},{2UL,0xE65B1ABE30CF05B6LL},{1UL,0x499C2A4DD296D0EBLL},{1UL,1UL},{0xB42BEC7BD30E8CA8LL,0x9E0819FB65CD2ACBLL}},{{0x499C2A4DD296D0EBLL,18446744073709551615UL},{0UL,0xA667C67DA2D99BCFLL},{18446744073709551615UL,0UL},{0xBA06236483EB5977LL,0x00F515F187075A8ALL},{0xBA06236483EB5977LL,0UL},{18446744073709551615UL,0xA667C67DA2D99BCFLL},{0UL,18446744073709551615UL},{0x499C2A4DD296D0EBLL,0x9E0819FB65CD2ACBLL},{0xB42BEC7BD30E8CA8LL,1UL},{1UL,0x499C2A4DD296D0EBLL}},{{1UL,0xE65B1ABE30CF05B6LL},{2UL,0x56FF0350500CFEDDLL},{18446744073709551615UL,0xF11C56E3F305EEE6LL},{0xA66B8DC494286B33LL,0x8796545BABB4A525LL},{18446744073709551612UL,18446744073709551615UL},{0xFA56EAB2C51FEFD1LL,0xA64D556C1E4672F9LL},{0x56FF0350500CFEDDLL,0xBA06236483EB5977LL},{0x1DFB655E0004CF34LL,2UL},{0x056E781AC6767173LL,2UL},{0x1DFB655E0004CF34LL,0xBA06236483EB5977LL}},{{0x56FF0350500CFEDDLL,0xA64D556C1E4672F9LL},{0xFA56EAB2C51FEFD1LL,18446744073709551615UL},{18446744073709551612UL,0x8796545BABB4A525LL},{0xA66B8DC494286B33LL,0xF11C56E3F305EEE6LL},{18446744073709551615UL,0x56FF0350500CFEDDLL},{2UL,0xE65B1ABE30CF05B6LL},{1UL,0x499C2A4DD296D0EBLL},{1UL,1UL},{0xB42BEC7BD30E8CA8LL,0x9E0819FB65CD2ACBLL},{0x499C2A4DD296D0EBLL,18446744073709551615UL}},{{0UL,0xA667C67DA2D99BCFLL},{18446744073709551615UL,0UL},{0xBA06236483EB5977LL,0x00F515F187075A8ALL},{0xBA06236483EB5977LL,0UL},{18446744073709551615UL,0xA667C67DA2D99BCFLL},{0UL,18446744073709551615UL},{0x499C2A4DD296D0EBLL,0x9E0819FB65CD2ACBLL},{0xB42BEC7BD30E8CA8LL,1UL},{1UL,0x499C2A4DD296D0EBLL},{1UL,0xE65B1ABE30CF05B6LL}},{{2UL,0x56FF0350500CFEDDLL},{18446744073709551615UL,0xF11C56E3F305EEE6LL},{0xA66B8DC494286B33LL,0x8796545BABB4A525LL},{18446744073709551612UL,18446744073709551615UL},{0xFA56EAB2C51FEFD1LL,0xA64D556C1E4672F9LL},{0x56FF0350500CFEDDLL,0xBA06236483EB5977LL},{0x1DFB655E0004CF34LL,2UL},{0x056E781AC6767173LL,18446744073709551612UL},{0xA64D556C1E4672F9LL,0xE65B1ABE30CF05B6LL},{0x056E781AC6767173LL,0xBA06236483EB5977LL}},{{0x499C2A4DD296D0EBLL,0UL},{1UL,18446744073709551615UL},{0xF11C56E3F305EEE6LL,8UL},{18446744073709551615UL,0x056E781AC6767173LL},{18446744073709551612UL,0x9E0819FB65CD2ACBLL},{0xA667C67DA2D99BCFLL,0x1DFB655E0004CF34LL},{18446744073709551615UL,18446744073709551615UL},{0xFA56EAB2C51FEFD1LL,18446744073709551609UL},{0x1DFB655E0004CF34LL,18446744073709551615UL},{18446744073709551615UL,0x00F515F187075A8ALL}}};
static struct S0 g_1856 = {64,0x0CL,0xF.318549p+82,0x79L,0xBEL,0x37E9C5EAL,18446744073709551615UL,0x0EDFL};/* VOLATILE GLOBAL g_1856 */
static const int64_t g_1886[6] = {7L,8L,8L,7L,8L,8L};
static int32_t * const  volatile g_1897 = (void*)0;/* VOLATILE GLOBAL g_1897 */
static struct S0 g_1912 = {-0,8L,0x7.1p+1,0x7BL,0UL,0xBA4DA158L,0x1514202AL,0xD7BDL};/* VOLATILE GLOBAL g_1912 */
static volatile uint64_t g_1961 = 7UL;/* VOLATILE GLOBAL g_1961 */
static int16_t * volatile **g_1984 = (void*)0;
static int16_t * volatile ** volatile * const  volatile g_1983[10][8][3] = {{{&g_1984,&g_1984,&g_1984},{(void*)0,&g_1984,(void*)0},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984}},{{&g_1984,&g_1984,&g_1984},{(void*)0,(void*)0,(void*)0},{&g_1984,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984}},{{&g_1984,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984},{&g_1984,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984}},{{&g_1984,&g_1984,&g_1984},{&g_1984,(void*)0,&g_1984},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,(void*)0,(void*)0},{&g_1984,&g_1984,(void*)0}},{{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_1984,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984}},{{(void*)0,&g_1984,&g_1984},{&g_1984,&g_1984,(void*)0},{(void*)0,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{(void*)0,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984}},{{&g_1984,&g_1984,&g_1984},{&g_1984,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984}},{{&g_1984,&g_1984,(void*)0},{(void*)0,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,(void*)0,&g_1984},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,&g_1984},{(void*)0,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984}},{{&g_1984,(void*)0,&g_1984},{&g_1984,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,(void*)0},{(void*)0,&g_1984,&g_1984},{(void*)0,(void*)0,&g_1984},{&g_1984,&g_1984,&g_1984}},{{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,&g_1984},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,(void*)0},{&g_1984,&g_1984,&g_1984}}};
static uint8_t *g_1995 = &g_452[0][0][1];
static uint8_t **g_1994[7][4] = {{&g_1995,(void*)0,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,(void*)0},{(void*)0,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,(void*)0},{&g_1995,&g_1995,&g_1995,&g_1995}};
static struct S1 g_2028 = {-2437,1114,86,125};
static struct S1 g_2030 = {1026,494,106,39};
static uint16_t g_2125 = 0xE20BL;
static uint16_t g_2126 = 65533UL;
static uint16_t g_2127 = 1UL;
static uint16_t * const g_2124[5][4][4] = {{{&g_2126,&g_2127,&g_2126,&g_2126},{&g_2127,&g_2127,&g_2125,&g_2127},{&g_2127,&g_2126,&g_2126,&g_2127},{&g_2126,&g_2127,&g_2126,&g_2126}},{{&g_2127,&g_2127,&g_2125,&g_2127},{&g_2127,&g_2126,&g_2126,&g_2127},{&g_2126,&g_2127,&g_2126,&g_2126},{&g_2127,&g_2127,&g_2125,&g_2127}},{{&g_2127,&g_2126,&g_2126,&g_2127},{&g_2126,&g_2127,&g_2126,&g_2126},{&g_2127,&g_2127,&g_2125,&g_2127},{&g_2127,&g_2126,&g_2126,&g_2127}},{{&g_2126,&g_2127,&g_2126,&g_2126},{&g_2127,&g_2127,&g_2125,&g_2127},{&g_2127,&g_2126,&g_2126,&g_2127},{&g_2126,&g_2127,&g_2126,&g_2126}},{{&g_2127,&g_2127,&g_2125,&g_2127},{&g_2127,&g_2126,&g_2126,&g_2127},{&g_2126,&g_2127,&g_2126,&g_2126},{&g_2127,&g_2127,&g_2125,&g_2127}}};
static uint16_t * const *g_2123[1] = {&g_2124[0][0][1]};
static uint32_t g_2130 = 4294967295UL;
static volatile uint32_t g_2133[4] = {0x0AB71105L,0x0AB71105L,0x0AB71105L,0x0AB71105L};
static const volatile uint8_t *g_2204[7] = {&g_688.f4,&g_688.f4,&g_688.f4,&g_688.f4,&g_688.f4,&g_688.f4,&g_688.f4};
static const volatile uint8_t **g_2203 = &g_2204[6];
static const volatile uint8_t *** volatile g_2202 = &g_2203;/* VOLATILE GLOBAL g_2202 */
static const volatile uint8_t *** volatile *g_2201 = &g_2202;
static const volatile uint8_t *** volatile * volatile *g_2200 = &g_2201;
static int32_t g_2252 = 0x739A3A41L;
static volatile float g_2257 = 0x0.1p+1;/* VOLATILE GLOBAL g_2257 */
static int8_t *g_2267 = &g_1540.f3;
static int8_t **g_2266 = &g_2267;
static int8_t ** volatile *g_2265 = &g_2266;
static int8_t ** volatile ** volatile g_2268 = &g_2265;/* VOLATILE GLOBAL g_2268 */
static int32_t *g_2286 = &g_2252;
static int32_t ** volatile g_2285[7] = {&g_2286,&g_2286,&g_2286,&g_2286,&g_2286,&g_2286,&g_2286};
static float *g_2328 = &g_841.f2;
static float **g_2327[9][1][6] = {{{&g_2328,&g_2328,&g_2328,&g_2328,&g_2328,&g_2328}},{{&g_2328,&g_2328,&g_2328,&g_2328,&g_2328,&g_2328}},{{(void*)0,&g_2328,&g_2328,&g_2328,&g_2328,&g_2328}},{{&g_2328,(void*)0,&g_2328,&g_2328,&g_2328,&g_2328}},{{&g_2328,&g_2328,&g_2328,&g_2328,&g_2328,&g_2328}},{{(void*)0,&g_2328,(void*)0,&g_2328,&g_2328,&g_2328}},{{&g_2328,(void*)0,(void*)0,&g_2328,&g_2328,&g_2328}},{{&g_2328,&g_2328,&g_2328,&g_2328,&g_2328,&g_2328}},{{&g_2328,&g_2328,&g_2328,&g_2328,&g_2328,&g_2328}}};
static volatile int16_t g_2329 = 0x5841L;/* VOLATILE GLOBAL g_2329 */
static int32_t g_2351[6] = {0xAEA7E00CL,0xE8CBA4B7L,0xAEA7E00CL,0xAEA7E00CL,0xE8CBA4B7L,0xAEA7E00CL};
static struct S0 g_2362 = {48,1L,0x5.7p-1,-1L,254UL,1L,0xDFFCEB0BL,0x73D2L};/* VOLATILE GLOBAL g_2362 */
static uint16_t g_2372[9] = {0xE725L,0xE725L,0xE725L,0xE725L,0xE725L,0xE725L,0xE725L,0xE725L,0xE725L};
static int32_t ** volatile g_2444 = &g_2286;/* VOLATILE GLOBAL g_2444 */
static uint32_t g_2473 = 18446744073709551612UL;
static uint16_t **g_2483 = &g_690;
static int32_t ** const *g_2491 = (void*)0;
static int32_t ** const **g_2490 = &g_2491;
static const struct S0 g_2512 = {-55,0x7DL,0x7.0A18DEp-34,0x70L,0x7DL,0L,0UL,9L};/* VOLATILE GLOBAL g_2512 */
static int32_t * const *g_2579 = &g_483;
static int32_t * const **g_2578[6] = {&g_2579,&g_2579,&g_2579,&g_2579,&g_2579,&g_2579};
static int32_t * const ***g_2577 = &g_2578[4];
static int32_t * const ****g_2576[1] = {&g_2577};
static struct S0 g_2588 = {-59,0x8BL,0x1.4p+1,0xC5L,0x6CL,1L,0x0024D027L,0x7CE6L};/* VOLATILE GLOBAL g_2588 */
static float g_2593 = 0x6.66BEFDp+78;
static volatile int16_t * volatile g_2662[4] = {&g_1081.f7,&g_1081.f7,&g_1081.f7,&g_1081.f7};
static volatile int16_t * volatile *g_2661 = &g_2662[1];
static int8_t *** const g_2667 = &g_2266;
static int8_t *** const *g_2666 = &g_2667;
static uint32_t g_2687 = 0x4FDDA2D0L;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static float  func_13(uint32_t  p_14, uint16_t  p_15);
static int8_t  func_35(int16_t * p_36, const uint16_t  p_37, int32_t * p_38);
static int32_t  func_60(const uint16_t  p_61, uint16_t  p_62, uint16_t  p_63, struct S1  p_64);
static uint16_t  func_65(uint32_t  p_66);
static uint16_t  func_71(uint16_t * p_72, int16_t * p_73);
static uint16_t * func_75(int16_t * p_76);
static int8_t  func_98(float * p_99, int16_t * p_100, int16_t * p_101, int16_t ** p_102, uint16_t  p_103);
static int16_t ** func_107(const struct S1  p_108, const int16_t * p_109, int64_t  p_110, uint32_t  p_111, int8_t  p_112);
static struct S1  func_113(struct S1  p_114, int64_t  p_115, int32_t * p_116, struct S1  p_117);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2328 g_841.f2 g_1648 g_270 g_160 g_1318 g_1143 g_18 g_2666 g_2667 g_2266 g_2267 g_1395 g_426 g_2687 g_1995 g_452 g_850 g_2286 g_1856.f3
 * writes: g_841.f2 g_160 g_121 g_1540.f3 g_426 g_2252
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_2 = 0xB5CA9869L;
    int32_t l_3 = 1L;
    int8_t l_16 = (-1L);
    int16_t ** const *l_2382[4];
    int16_t ** const **l_2381[1];
    int32_t *l_2383 = (void*)0;
    int16_t l_2417 = 0x1B3FL;
    uint32_t l_2450 = 0xDA1C9231L;
    uint8_t ***l_2470[9][3] = {{&g_1994[4][3],&g_1994[3][0],&g_1994[3][0]},{&g_1994[0][3],&g_1994[0][1],&g_1994[0][1]},{&g_1994[4][3],&g_1994[3][0],&g_1994[3][0]},{&g_1994[0][3],&g_1994[0][1],&g_1994[0][1]},{&g_1994[4][3],&g_1994[3][0],&g_1994[3][0]},{&g_1994[0][3],&g_1994[0][1],&g_1994[0][1]},{&g_1994[4][3],&g_1994[3][0],&g_1994[3][0]},{&g_1994[0][3],&g_1994[0][1],&g_1994[0][1]},{&g_1994[4][3],&g_1994[3][0],&g_1994[3][0]}};
    int32_t l_2475 = (-6L);
    uint32_t l_2482[10] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    int32_t ***l_2494 = &g_1318;
    int32_t ****l_2493 = &l_2494;
    uint32_t l_2611 = 0xAF3AAD1FL;
    int32_t *l_2636[10][8][3] = {{{&g_25[0][1],(void*)0,&g_2351[0]},{&g_2351[0],&g_207[3],&g_160},{&g_850[6],(void*)0,&g_25[0][0]},{&g_207[1],(void*)0,&g_2351[0]},{&l_3,&l_3,&g_850[8]},{(void*)0,(void*)0,&l_3},{&g_2351[0],&g_25[0][2],&l_2475},{&g_2351[0],(void*)0,&g_2252}},{{(void*)0,&g_2351[0],&l_2475},{(void*)0,&l_2475,&l_3},{&g_1731,&g_25[0][0],&g_850[8]},{&g_2252,&l_2475,&g_2351[0]},{&g_207[1],&g_850[8],&g_25[0][0]},{&g_207[1],&g_2351[0],&g_160},{&g_25[0][0],&l_2475,&g_2351[0]},{(void*)0,&g_25[0][1],(void*)0}},{{&g_25[0][2],&l_2475,&g_25[0][2]},{&g_25[0][1],&g_2351[0],(void*)0},{&g_850[1],&g_850[8],&l_3},{&g_2351[0],(void*)0,&g_346},{&g_25[0][1],&g_25[0][2],&g_2351[3]},{(void*)0,&g_2351[0],&g_25[0][1]},{&g_25[0][2],&g_850[6],&g_850[8]},{&l_3,&g_207[0],&g_160}},{{&g_25[0][2],&g_2351[0],&g_160},{(void*)0,(void*)0,&g_2351[0]},{&g_25[0][1],&g_207[3],&g_2351[0]},{&g_346,&g_850[3],&g_2252},{&l_3,&g_2351[3],(void*)0},{&g_160,&g_1731,(void*)0},{&g_2351[0],&g_850[8],(void*)0},{&g_207[1],&l_3,(void*)0}},{{&g_25[0][2],&g_160,(void*)0},{&g_850[8],&l_2475,&g_2252},{&g_2252,&g_2351[0],&g_2351[0]},{&g_25[0][3],&g_25[0][1],&g_2351[0]},{&g_25[0][2],(void*)0,&g_160},{&g_2351[0],(void*)0,&g_160},{&g_2351[3],(void*)0,&g_850[8]},{&g_207[0],(void*)0,&g_25[0][1]}},{{&g_850[6],(void*)0,&g_2351[3]},{&g_2351[0],&g_25[0][1],&g_346},{&g_207[3],&g_2351[0],&g_207[3]},{&g_160,&l_2475,&g_207[1]},{(void*)0,&g_160,&g_2351[0]},{&g_160,&l_3,&g_25[0][3]},{&g_160,&g_850[8],&g_850[6]},{&g_160,&g_1731,&l_3}},{{(void*)0,&g_2351[3],&g_25[0][2]},{&g_160,&g_850[3],&g_207[0]},{&g_207[3],&g_207[3],&l_3},{&g_2351[0],(void*)0,(void*)0},{&g_850[6],&g_2351[0],&g_850[1]},{&g_207[0],&g_207[0],&g_2351[0]},{&g_2351[3],&g_850[6],&g_850[1]},{&g_2351[0],&g_2351[0],(void*)0}},{{&g_25[0][2],&g_25[0][2],&l_3},{&g_25[0][3],(void*)0,&g_207[0]},{&g_2252,&l_3,&g_25[0][2]},{&g_850[8],(void*)0,&l_3},{&g_25[0][2],&g_850[1],&g_850[6]},{&g_207[1],&g_346,&g_25[0][3]},{&g_2351[0],&g_850[1],&g_2351[0]},{&g_160,(void*)0,&g_207[1]}},{{&l_3,&l_3,&g_207[3]},{&g_346,(void*)0,&g_346},{&g_25[0][1],&g_25[0][2],&g_2351[3]},{(void*)0,&g_2351[0],&g_25[0][1]},{&g_25[0][2],&g_850[6],&g_850[8]},{&l_3,&g_207[0],&g_160},{&g_25[0][2],&g_2351[0],&g_160},{(void*)0,(void*)0,&g_2351[0]}},{{&g_25[0][1],&g_207[3],&g_2351[0]},{&g_346,&g_850[3],&g_2252},{&l_3,&g_2351[3],(void*)0},{&g_160,&g_1731,(void*)0},{&g_2351[0],&g_850[8],(void*)0},{&g_207[1],&l_3,(void*)0},{&g_25[0][2],&g_160,(void*)0},{&g_850[8],&l_2475,&g_2252}}};
    int32_t ** const ***l_2655 = &g_2490;
    uint16_t l_2690 = 0xCB63L;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_2382[i] = &g_275;
    for (i = 0; i < 1; i++)
        l_2381[i] = &l_2382[1];
    if ((l_3 |= l_2))
    { /* block id: 2 */
        int32_t l_17 = (-1L);
        const uint8_t l_2354 = 0x08L;
        (*g_2328) = (safe_sub_func_float_f_f((safe_add_func_float_f_f((((safe_div_func_float_f_f((-0x1.Ep-1), (-0x7.1p+1))) == (((-(safe_div_func_float_f_f(func_13(l_16, l_17), l_2354))) <= (l_3 <= (l_2 != l_2354))) != (-0x5.6p-1))) >= (*g_2328)), (*g_1648))), (-0x5.4p-1)));
    }
    else
    { /* block id: 1073 */
        int32_t *l_2355[9] = {&l_3,&l_3,&l_3,&l_3,&l_3,&l_3,&l_3,&l_3,&l_3};
        int32_t *l_2384 = &g_1456;
        uint8_t ***l_2472 = &g_1994[4][3];
        const int8_t l_2515[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        const uint8_t l_2524[8][2] = {{0x15L,251UL},{0x15L,251UL},{0x15L,251UL},{0x15L,251UL},{0x15L,251UL},{0x15L,251UL},{0x15L,251UL},{0x15L,251UL}};
        const uint32_t l_2551[8][3][5] = {{{1UL,1UL,0x6FAB55B4L,1UL,1UL},{0UL,0x2A9A81A7L,0UL,0UL,0x2A9A81A7L},{1UL,0xBB0A2297L,0xBB0A2297L,1UL,0xBB0A2297L}},{{0x2A9A81A7L,0x2A9A81A7L,7UL,0x2A9A81A7L,0x2A9A81A7L},{0xBB0A2297L,1UL,0xBB0A2297L,0xBB0A2297L,1UL},{0x2A9A81A7L,0UL,0UL,0x2A9A81A7L,0UL}},{{1UL,1UL,0x6FAB55B4L,1UL,1UL},{0UL,0x2A9A81A7L,0UL,0UL,0x2A9A81A7L},{1UL,0xBB0A2297L,0xBB0A2297L,1UL,0xBB0A2297L}},{{0x2A9A81A7L,0x2A9A81A7L,7UL,0x2A9A81A7L,0x2A9A81A7L},{0xBB0A2297L,1UL,0xBB0A2297L,0xBB0A2297L,1UL},{0x2A9A81A7L,0UL,0UL,0x2A9A81A7L,0UL}},{{1UL,1UL,0x6FAB55B4L,1UL,1UL},{0UL,0x2A9A81A7L,0UL,0UL,0x2A9A81A7L},{1UL,0xBB0A2297L,0xBB0A2297L,1UL,0xBB0A2297L}},{{0x2A9A81A7L,0x2A9A81A7L,7UL,0x2A9A81A7L,0x2A9A81A7L},{0xBB0A2297L,1UL,0xBB0A2297L,0xBB0A2297L,1UL},{0x2A9A81A7L,0UL,0UL,0x2A9A81A7L,0UL}},{{1UL,1UL,0x6FAB55B4L,1UL,1UL},{0UL,0x2A9A81A7L,0UL,0UL,0x2A9A81A7L},{1UL,0xBB0A2297L,0xBB0A2297L,1UL,0xBB0A2297L}},{{0x2A9A81A7L,0x2A9A81A7L,7UL,0x2A9A81A7L,0x2A9A81A7L},{0xBB0A2297L,1UL,0xBB0A2297L,0xBB0A2297L,1UL},{0x2A9A81A7L,0UL,0UL,0x2A9A81A7L,0UL}}};
        uint64_t **l_2565 = &g_1395;
        uint8_t *l_2585 = &g_1409.f4;
        int8_t l_2608 = 0L;
        int32_t l_2641 = 0xC2CC1901L;
        int16_t **l_2642[4] = {&g_276,&g_276,&g_276,&g_276};
        int16_t l_2673 = 5L;
        int16_t l_2686 = 0x29C5L;
        int32_t *l_2691 = (void*)0;
        int i, j, k;
        for (g_160 = 6; (g_160 >= 0); g_160 -= 1)
        { /* block id: 1076 */
            float ***l_2371 = &g_2327[8][0][1];
            int32_t *l_2386 = &g_1456;
            struct S1 l_2413 = {-1034,1372,1,291};
            int32_t l_2414 = 0L;
            int8_t l_2415 = 9L;
            int32_t l_2416 = (-1L);
            uint8_t l_2436 = 7UL;
            int32_t l_2516 = 0x7228C764L;
            uint32_t l_2517[9][3][3] = {{{0x34E14868L,0x18352B95L,0x9A5A0DD6L},{0xE99554C0L,0x0751C18EL,0x69F4E359L},{0x34E14868L,0x00249590L,0x34E14868L}},{{0UL,0xE99554C0L,0x69F4E359L},{3UL,4294967293UL,0x9A5A0DD6L},{0x95FE2990L,0xE99554C0L,0xE99554C0L}},{{0x9A5A0DD6L,0x00249590L,3UL},{0x95FE2990L,0x0751C18EL,0x95FE2990L},{3UL,0x18352B95L,3UL}},{{0UL,0UL,0xE99554C0L},{0x34E14868L,0x18352B95L,0x9A5A0DD6L},{0xE99554C0L,0x0751C18EL,0x69F4E359L}},{{0x34E14868L,0x00249590L,0x34E14868L},{0UL,0xE99554C0L,0x69F4E359L},{3UL,4294967293UL,0x34E14868L}},{{0x69F4E359L,0x95FE2990L,0x95FE2990L},{0x34E14868L,4294967293UL,0x5E898998L},{0x69F4E359L,0UL,0x69F4E359L}},{{0x9A5A0DD6L,0xE30A5FB2L,0x5E898998L},{0xE99554C0L,0xE99554C0L,0x95FE2990L},{3UL,0xE30A5FB2L,0x34E14868L}},{{0x95FE2990L,0UL,0x0751C18EL},{3UL,4294967293UL,3UL},{0xE99554C0L,0x95FE2990L,0x0751C18EL}},{{0x9A5A0DD6L,0x18352B95L,0x34E14868L},{0x69F4E359L,0x95FE2990L,0x95FE2990L},{0x34E14868L,4294967293UL,0x5E898998L}}};
            int16_t l_2567 = 0x63B7L;
            int32_t l_2590 = (-1L);
            int32_t l_2646[6][7] = {{(-1L),0L,(-5L),0x64809D64L,0xC948F190L,0xE7CA1BBCL,1L},{0x64809D64L,0L,0L,0xE7CA1BBCL,0xE7CA1BBCL,0L,0L},{(-1L),0x64809D64L,0L,0x2A61A511L,0x0EC3E6A4L,0L,(-1L)},{0L,0x2346F0C1L,0xC65F96F7L,(-5L),0x74C19BBCL,0xE7CA1BBCL,0x74C19BBCL},{0x2A61A511L,0x74C19BBCL,0x74C19BBCL,0x2A61A511L,(-1L),(-1L),(-5L)},{(-1L),0x74C19BBCL,0L,0xE7CA1BBCL,(-1L),(-1L),0xC948F190L}};
            uint16_t l_2658 = 0UL;
            int16_t **l_2674[8][3][10] = {{{&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276},{&g_276,&g_276,(void*)0,&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276},{(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0}},{{&g_276,&g_276,(void*)0,&g_276,(void*)0,(void*)0,&g_276,&g_276,(void*)0,&g_276},{&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,(void*)0},{(void*)0,&g_276,&g_276,(void*)0,&g_276,(void*)0,&g_276,(void*)0,&g_276,&g_276}},{{&g_276,(void*)0,&g_276,&g_276,(void*)0,&g_276,(void*)0,&g_276,&g_276,&g_276},{(void*)0,&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276},{&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,(void*)0,&g_276,&g_276,&g_276}},{{(void*)0,(void*)0,&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0},{&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276,(void*)0,&g_276,&g_276},{&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276}},{{&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276},{(void*)0,&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276},{(void*)0,&g_276,&g_276,(void*)0,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276}},{{&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276},{&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276,&g_276,&g_276,(void*)0,&g_276},{&g_276,&g_276,(void*)0,&g_276,&g_276,(void*)0,&g_276,(void*)0,&g_276,(void*)0}},{{&g_276,(void*)0,(void*)0,(void*)0,&g_276,&g_276,&g_276,(void*)0,(void*)0,&g_276},{&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276,(void*)0,&g_276,&g_276},{&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276,&g_276,(void*)0}},{{&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276},{&g_276,&g_276,&g_276,&g_276,(void*)0,(void*)0,(void*)0,&g_276,&g_276,&g_276},{&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,&g_276,(void*)0,&g_276}}};
            int i, j, k;
            (*g_1318) = &g_850[(g_160 + 3)];
            (*g_1318) = l_2355[3];
        }
        (*g_2286) = ((((safe_mul_func_float_f_f((safe_sub_func_float_f_f((*g_1143), (-0x1.8p+1))), ((*g_2328) , ((*g_2328) = (-0x5.9p+1))))) < ((safe_div_func_float_f_f((safe_mul_func_float_f_f((((**l_2565) ^= (!((****g_2666) = (safe_add_func_uint64_t_u_u(18446744073709551615UL, l_2686))))) , 0x1.C204FAp+28), g_2687)), ((((safe_lshift_func_uint8_t_u_s((*g_1995), 0)) < g_850[2]) == 2UL) , 0xF.579A99p+96))) > l_2690)) , (void*)0) != l_2691);
        (***l_2493) = &l_2641;
    }
    return g_1856.f3;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static float  func_13(uint32_t  p_14, uint16_t  p_15)
{ /* block id: 3 */
    uint32_t l_23 = 6UL;
    int32_t *l_2350[5];
    int i;
    for (i = 0; i < 5; i++)
        l_2350[i] = &g_2351[0];
    for (p_14 = 0; (p_14 <= 7); p_14 += 1)
    { /* block id: 6 */
        int32_t *l_26 = &g_25[0][0];
        int32_t l_2353 = 1L;
        for (p_15 = 2; (p_15 <= 7); p_15 += 1)
        { /* block id: 9 */
            int32_t *l_24 = &g_25[0][1];
            int16_t *l_27 = &g_28;
            uint16_t *l_31 = &g_32;
            int16_t *l_2352 = &g_1540.f7;
        }
    }
    return p_15;
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_28 g_118 g_119 g_25 g_121 g_160 g_176 g_183 g_207 g_217 g_127 g_46 g_263 g_48 g_275 g_202 g_1318 g_841.f3 g_1648 g_690 g_416 g_689 g_1666 g_276 g_1409.f3 g_1669 g_1442.f7 g_452 g_1252 g_1253 g_1561 g_1642 g_1643 g_1395 g_426 g_850 g_1731 g_1409.f6 g_18 g_1324.f0 g_1762 g_1768 g_892 g_346 g_1785 g_1257 g_1409.f4 g_348 g_1456 g_1821 g_1441.f1 g_841.f7 g_1324.f3 g_1856 g_1540.f3 g_688.f0 g_1143 g_662 g_1650 g_1540.f6 g_1241.f6 g_1254 g_1255 g_1912 g_350 g_1961 g_1465.f3 g_1207.f3 g_1094.f5 g_1983 g_1241.f7 g_1442.f3 g_855 g_1995 g_1769 g_1540.f5 g_1409.f7 g_1647 g_1539 g_841 g_1540 g_1465.f0 g_2130 g_2133 g_1256 g_2200 g_316 g_315 g_2030.f2 g_1081.f6 g_2125 g_1241.f3 g_2265 g_2268 g_777 g_2327 g_2328 g_2286 g_2329 g_1409.f0
 * writes: g_32 g_18 g_46 g_48 g_28 g_121 g_127 g_25 g_160 g_176 g_183 g_202 g_207 g_217 g_270 g_273 g_118.f2 g_416 g_1540.f3 g_1442.f4 g_841.f3 g_1539 g_1409.f3 g_1643 g_1442.f7 g_1409.f6 g_426 g_1540.f6 g_777 g_841.f7 g_1442.f2 g_1769 g_1731 g_1257 g_1409.f4 g_346 g_1456 g_1821 g_855 g_1540.f2 g_483 g_841.f2 g_1067 g_1961 g_850 g_1856.f7 g_1994 g_1206 g_452 g_690 g_393 g_2123 g_2130 g_1318 g_717 g_2252 g_2265 g_841.f6 g_1912.f7 g_1912.f4 g_2327 g_1409.f0
 */
static int8_t  func_35(int16_t * p_36, const uint16_t  p_37, int32_t * p_38)
{ /* block id: 13 */
    int32_t *l_39[10][3][1] = {{{(void*)0},{&g_25[0][2]},{(void*)0}},{{&g_25[0][1]},{&g_25[0][1]},{&g_25[0][1]}},{{(void*)0},{&g_25[0][2]},{(void*)0}},{{&g_25[0][1]},{&g_25[0][1]},{&g_25[0][1]}},{{(void*)0},{&g_25[0][2]},{(void*)0}},{{&g_25[0][1]},{&g_25[0][1]},{&g_25[0][1]}},{{(void*)0},{&g_25[0][2]},{(void*)0}},{{&g_25[0][1]},{&g_25[0][1]},{&g_25[0][1]}},{{(void*)0},{&g_25[0][2]},{(void*)0}},{{&g_25[0][1]},{&g_25[0][1]},{&g_25[0][1]}}};
    uint64_t l_40 = 0x8211ABD849EDE151LL;
    uint32_t l_50[2][10] = {{18446744073709551612UL,18446744073709551612UL,0xD9FD67D0L,0xD9FD67D0L,18446744073709551612UL,18446744073709551612UL,0xD9FD67D0L,0xD9FD67D0L,18446744073709551612UL,18446744073709551612UL},{18446744073709551612UL,0xD9FD67D0L,0xD9FD67D0L,18446744073709551612UL,18446744073709551612UL,0xD9FD67D0L,0xD9FD67D0L,18446744073709551612UL,18446744073709551612UL,0xD9FD67D0L}};
    uint16_t l_53 = 0xEDC4L;
    int16_t *l_74 = (void*)0;
    int16_t **l_77[1][9][2] = {{{&l_74,&l_74},{&l_74,&l_74},{&l_74,&l_74},{&l_74,&l_74},{&l_74,&l_74},{&l_74,&l_74},{&l_74,&l_74},{&l_74,&l_74},{&l_74,&l_74}}};
    uint8_t l_1987 = 255UL;
    int64_t l_1988 = 0x686823555328F60ELL;
    int16_t l_2298 = 0x1ED6L;
    struct S1 l_2299 = {-2595,125,76,279};
    int i, j, k;
    l_40++;
    for (g_32 = 0; (g_32 < 28); ++g_32)
    { /* block id: 17 */
        float *l_45 = &g_18[7];
        float *l_47 = &g_48[0][3];
        int32_t l_49[7];
        int i;
        for (i = 0; i < 7; i++)
            l_49[i] = 4L;
        (*l_47) = (g_46 = ((*l_45) = 0xB.2CBD72p+72));
        l_50[1][4]++;
        ++l_53;
    }
    g_1409.f0 &= (safe_mod_func_int32_t_s_s((safe_mod_func_int32_t_s_s(func_60(func_65((safe_lshift_func_int16_t_s_s(1L, (l_39[9][2][0] == (((safe_div_func_int16_t_s_s((l_1988 = (func_71((((void*)0 == l_74) , func_75((p_36 = (void*)0))), (*g_275)) | (((0x1BA1774B236FAD73LL < p_37) , g_1241[1][0][5].f7) , l_1987))), g_1442.f3)) >= p_37) , l_39[3][0][0]))))), l_2298, p_37, l_2299), p_37)), 1L));
    return p_37;
}


/* ------------------------------------------ */
/* 
 * reads : g_841.f6 g_1666 g_1539 g_1912.f4 g_316 g_315 g_118 g_2327 g_2328 g_841.f2 g_892 g_346 g_2286 g_2329
 * writes: g_841.f6 g_1912.f4 g_2327 g_1442.f2 g_841.f2 g_2252
 */
static int32_t  func_60(const uint16_t  p_61, uint16_t  p_62, uint16_t  p_63, struct S1  p_64)
{ /* block id: 1036 */
    struct S0 *l_2302[6][7][6] = {{{&g_1409,&g_1912,&g_1912,&g_1409,&g_1856,&g_1540},{&g_1912,&g_1409,&g_1912,(void*)0,&g_1241[1][0][5],&g_1856},{(void*)0,(void*)0,&g_1540,&g_1409,&g_1409,(void*)0},{&g_1409,&g_1409,(void*)0,&g_1856,(void*)0,&g_1409},{&g_1241[1][1][4],&g_1856,&g_1540,&g_1856,&g_1409,&g_1912},{&g_1409,&g_1912,&g_1241[1][1][4],&g_1409,&g_1409,&g_1540},{(void*)0,&g_1241[1][1][4],&g_1409,(void*)0,&g_1912,&g_1912}},{{&g_1912,&g_1856,&g_1912,&g_1409,&g_1912,&g_1442},{&g_1409,&g_1241[1][1][4],&g_1912,&g_1241[1][1][4],&g_1409,&g_1912},{&g_1241[1][0][5],&g_1912,(void*)0,&g_1912,&g_1409,&g_1540},{(void*)0,&g_1856,(void*)0,&g_1912,(void*)0,&g_1540},{&g_1409,&g_1409,(void*)0,(void*)0,&g_1409,&g_1912},{(void*)0,(void*)0,&g_1912,&g_1912,&g_1241[1][0][5],&g_1442},{(void*)0,&g_1409,&g_1912,&g_1442,&g_1856,&g_1912}},{{(void*)0,&g_1912,&g_1409,&g_1912,&g_1442,&g_1540},{(void*)0,&g_1241[1][0][5],&g_1241[1][1][4],(void*)0,&g_1912,&g_1912},{&g_1409,(void*)0,&g_1540,&g_1912,&g_1912,&g_1409},{(void*)0,(void*)0,(void*)0,&g_1912,&g_1912,(void*)0},{&g_1241[1][0][5],&g_1241[1][0][5],&g_1540,&g_1241[1][1][4],&g_1442,&g_1856},{&g_1409,&g_1912,&g_1912,&g_1409,&g_1856,&g_1540},{&g_1912,&g_1409,&g_1912,(void*)0,&g_1241[1][0][5],&g_1856}},{{(void*)0,(void*)0,&g_1540,&g_1409,&g_1409,(void*)0},{&g_1409,&g_1409,(void*)0,&g_1856,(void*)0,&g_1409},{&g_1241[1][1][4],&g_1856,&g_1540,&g_1856,&g_1409,&g_1912},{&g_1409,&g_1912,&g_1241[1][1][4],&g_1409,&g_1409,&g_1540},{(void*)0,&g_1241[1][1][4],&g_1409,(void*)0,&g_1912,&g_1912},{&g_1912,&g_1856,&g_1912,&g_1409,&g_1912,&g_1442},{&g_1409,&g_1241[1][1][4],&g_1912,&g_1241[1][1][4],&g_1409,&g_1912}},{{&g_1241[1][0][5],&g_1912,(void*)0,&g_1912,&g_1409,&g_1540},{(void*)0,&g_1856,(void*)0,&g_1912,(void*)0,&g_1540},{&g_1409,&g_1409,(void*)0,(void*)0,&g_1409,&g_1912},{&g_1241[1][0][5],&g_1912,(void*)0,&g_841,&g_1912,&g_1856},{&g_1912,&g_1856,&g_1912,&g_1540,(void*)0,(void*)0},{&g_1912,&g_1912,&g_1409,&g_841,&g_1540,&g_1442},{&g_1241[1][0][5],&g_1912,&g_1241[1][0][5],&g_1856,&g_1912,(void*)0}},{{(void*)0,&g_1540,(void*)0,&g_1409,&g_1241[1][0][5],&g_1409},{&g_1856,&g_1540,&g_1409,&g_1912,&g_1912,&g_1409},{&g_1912,&g_1912,(void*)0,(void*)0,&g_1540,&g_1856},{&g_1856,&g_1912,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1241[1][0][5],&g_1856,(void*)0,&g_1912,&g_1912,&g_1856},{&g_1540,&g_1912,(void*)0,&g_1241[1][0][1],&g_1241[1][1][4],&g_1409},{&g_1241[1][0][1],&g_1241[1][1][4],&g_1409,(void*)0,&g_1241[1][0][5],&g_1409}}};
    int32_t l_2318 = (-10L);
    float *l_2326 = &g_1442.f2;
    float **l_2325 = &l_2326;
    int32_t l_2342 = 0x0D747A76L;
    int i, j, k;
lbl_2349:
    for (g_841.f6 = (-11); (g_841.f6 != 36); g_841.f6 = safe_add_func_int8_t_s_s(g_841.f6, 1))
    { /* block id: 1039 */
        struct S0 **l_2303[1];
        int32_t l_2311 = 1L;
        int32_t l_2317[1];
        uint32_t l_2344 = 0x6FD450B8L;
        int32_t *l_2347 = (void*)0;
        int i;
        for (i = 0; i < 1; i++)
            l_2303[i] = (void*)0;
        for (i = 0; i < 1; i++)
            l_2317[i] = 0x50D66DC1L;
        if (((l_2302[5][2][0] = l_2302[1][4][4]) == (*g_1666)))
        { /* block id: 1041 */
            uint8_t l_2319 = 246UL;
            for (g_1912.f4 = 0; (g_1912.f4 >= 4); ++g_1912.f4)
            { /* block id: 1044 */
                int32_t *l_2306 = (void*)0;
                int32_t *l_2307 = &g_2252;
                int32_t l_2308[8] = {0L,0L,0L,0L,0L,0L,0L,0L};
                int32_t *l_2309 = &g_1731;
                int32_t *l_2310 = &g_2252;
                int32_t *l_2312 = &l_2311;
                int32_t *l_2313 = &g_1731;
                int32_t *l_2314 = &g_207[1];
                int32_t *l_2315 = &g_850[8];
                int32_t *l_2316[9] = {&l_2311,&l_2311,&l_2311,&l_2311,&l_2311,&l_2311,&l_2311,&l_2311,&l_2311};
                int i;
                ++l_2319;
                (*g_2328) = ((safe_sub_func_float_f_f(l_2317[0], (((-(((p_63 , (**g_316)) , l_2325) == (g_2327[8][0][1] = g_2327[8][0][1]))) >= p_64.f1) >= (*g_2328)))) > ((**l_2325) = 0x8.4p-1));
                if (g_118.f2)
                    goto lbl_2349;
            }
            (*g_2286) = (*g_892);
        }
        else
        { /* block id: 1051 */
            int32_t *l_2330 = &l_2311;
            int32_t *l_2331 = &l_2317[0];
            int32_t *l_2332 = &g_207[3];
            int32_t *l_2333 = &g_1731;
            int32_t *l_2334 = &g_1731;
            int32_t *l_2335 = &g_2252;
            int32_t *l_2336 = &g_25[0][2];
            int32_t *l_2337 = (void*)0;
            int32_t *l_2338 = &g_850[9];
            int32_t *l_2339 = (void*)0;
            int32_t *l_2340 = &g_346;
            int32_t *l_2341[10][1][2] = {{{&l_2311,&g_850[8]}},{{&l_2317[0],&l_2317[0]}},{{&g_850[8],&l_2311}},{{&g_25[0][1],&l_2311}},{{&g_850[8],&l_2317[0]}},{{&l_2317[0],&g_850[8]}},{{&l_2311,&g_25[0][1]}},{{&l_2311,&g_850[8]}},{{&l_2317[0],&l_2317[0]}},{{&g_850[8],&l_2311}}};
            int32_t l_2343 = 0L;
            int i, j, k;
            if (g_2329)
                break;
            l_2344++;
        }
        l_2347 = &l_2311;
        for (g_1912.f4 = 0; (g_1912.f4 <= 2); g_1912.f4 += 1)
        { /* block id: 1058 */
            int8_t l_2348 = 0x74L;
            return l_2348;
        }
    }
    return l_2342;
}


/* ------------------------------------------ */
/* 
 * reads : g_855 g_1731 g_1995 g_452 g_1318 g_1769 g_1540.f6 g_176 g_1540.f5 g_1642 g_1643 g_1395 g_426 g_346 g_689 g_690 g_416 g_1409.f7 g_1647 g_1539 g_841 g_1540 g_1465.f0 g_892 g_2130 g_2133 g_1255 g_1256 g_1143 g_18 g_1762 g_1912.f0 g_1465.f3 g_2200 g_316 g_315 g_118 g_2030.f2 g_1081.f6 g_2125 g_1241.f3 g_2265 g_2268 g_121 g_1409.f3 g_777
 * writes: g_855 g_1731 g_1994 g_346 g_1540.f6 g_1067 g_217 g_121 g_176 g_1206 g_452 g_426 g_690 g_393 g_1643 g_1456 g_2123 g_2130 g_1318 g_1442.f2 g_416 g_841.f7 g_127 g_717 g_1540.f3 g_2252 g_2265 g_841.f6 g_777 g_1409.f3 g_1912.f7 g_46
 */
static uint16_t  func_65(uint32_t  p_66)
{ /* block id: 887 */
    int32_t **** const l_1996 = (void*)0;
    uint8_t **l_1999 = &g_1995;
    uint32_t l_2015[6] = {0xF1FC4EC6L,0xF1FC4EC6L,0xF1FC4EC6L,0xF1FC4EC6L,0xF1FC4EC6L,0xF1FC4EC6L};
    int16_t l_2016 = 0x9569L;
    int16_t *l_2058 = &g_841.f7;
    int32_t l_2065 = 0xB04300B3L;
    int32_t l_2066 = 0xAE59C233L;
    int32_t l_2067 = 0L;
    int32_t l_2068 = 0L;
    int32_t l_2109[2][10] = {{6L,0x6CB0F92CL,1L,6L,0xA0CA0EC3L,0xA0CA0EC3L,6L,1L,0x6CB0F92CL,6L},{1L,0xAAC6E85CL,0x6CB0F92CL,0xA0CA0EC3L,0xAAC6E85CL,0xA0CA0EC3L,0x6CB0F92CL,0xAAC6E85CL,1L,1L}};
    int16_t l_2111[5][6][2] = {{{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L},{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L}},{{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L},{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L}},{{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L},{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L}},{{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L},{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L}},{{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L},{0L,0xF8E8L},{5L,0xF8E8L},{0L,0x8592L}}};
    uint32_t l_2166 = 18446744073709551615UL;
    int64_t l_2223 = (-8L);
    float l_2233[5][6] = {{0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5},{0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5},{0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5},{0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5},{0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5,0x4.B58FE4p-5}};
    uint8_t l_2234 = 0x4DL;
    uint8_t ****l_2249 = (void*)0;
    int8_t **l_2253 = (void*)0;
    int32_t *l_2284 = &g_850[8];
    uint32_t l_2295 = 7UL;
    int i, j, k;
    for (g_855 = 14; (g_855 > 39); ++g_855)
    { /* block id: 890 */
        int32_t *l_1991 = &g_1731;
        uint8_t *l_1993 = &g_452[0][1][0];
        uint8_t **l_1992 = &l_1993;
        uint8_t **l_1998 = &l_1993;
        uint8_t ***l_1997[9][2] = {{&l_1992,&l_1998},{&l_1998,&l_1998},{&l_1992,&l_1998},{&l_1998,&l_1998},{&l_1992,&l_1998},{&l_1998,&l_1998},{&l_1992,&l_1998},{&l_1998,&l_1998},{&l_1992,&l_1998}};
        int32_t l_2003[5][6] = {{0L,0x951F6A65L,0xE52A2187L,1L,1L,0xE52A2187L},{1L,1L,0xE52A2187L,0x951F6A65L,0L,0xE52A2187L},{0x951F6A65L,0L,0xE52A2187L,0L,0x951F6A65L,0xE52A2187L},{0L,0x951F6A65L,0xE52A2187L,1L,1L,0xE52A2187L},{1L,1L,0xE52A2187L,0x951F6A65L,0L,0xE52A2187L}};
        int32_t *l_2004 = (void*)0;
        int32_t *l_2005 = &g_346;
        struct S1 *l_2027 = &g_2028;
        struct S1 *l_2029 = &g_2030;
        int64_t *l_2090 = &g_1643;
        int64_t l_2106 = 0x2BDD2F3EC7049582LL;
        int8_t l_2107 = 0L;
        int32_t l_2108[8];
        int32_t l_2110 = 1L;
        uint16_t * const *l_2122 = &g_690;
        uint8_t l_2148 = 0x49L;
        int8_t *l_2255 = &g_217;
        int8_t **l_2254 = &l_2255;
        int32_t *l_2293 = &g_25[0][0];
        int32_t *l_2294[6];
        int i, j;
        for (i = 0; i < 8; i++)
            l_2108[i] = 0xB63FAC9AL;
        for (i = 0; i < 6; i++)
            l_2294[i] = &l_2109[0][6];
        if ((((*l_1991) = (-4L)) , (((g_1994[4][3] = l_1992) != (l_1999 = (((void*)0 != l_1996) , &l_1993))) & (safe_sub_func_int16_t_s_s((!(((*l_1991) ^= p_66) > ((*l_2005) = l_2003[3][2]))), ((safe_sub_func_int16_t_s_s((safe_add_func_uint16_t_u_u((+(safe_div_func_int8_t_s_s((safe_add_func_uint64_t_u_u(p_66, 1UL)), (*g_1995)))), p_66)), l_2015[4])) , l_2016))))))
        { /* block id: 896 */
            int64_t l_2017 = (-5L);
            int32_t l_2041 = 1L;
            int32_t l_2048 = 5L;
            int32_t l_2055 = 0xC11E6135L;
            uint16_t * const l_2059 = &g_32;
            uint8_t l_2062 = 0UL;
            uint32_t l_2063 = 0x3A5A2FB7L;
            int32_t *l_2064[3];
            uint32_t l_2069 = 18446744073709551615UL;
            int i;
            for (i = 0; i < 3; i++)
                l_2064[i] = &l_2003[3][2];
            if (p_66)
                break;
            for (g_1540.f6 = 0; (g_1540.f6 <= 4); g_1540.f6 += 1)
            { /* block id: 900 */
                int64_t l_2021 = (-1L);
                uint16_t *l_2033 = (void*)0;
                uint64_t *l_2039 = &g_426;
                int32_t l_2040 = (-2L);
                int i, j;
                for (g_1067 = 0; (g_1067 <= 4); g_1067 += 1)
                { /* block id: 903 */
                    uint64_t l_2022[3][2] = {{1UL,1UL},{18446744073709551615UL,1UL},{1UL,18446744073709551615UL}};
                    uint8_t **l_2034 = &l_1993;
                    int i, j;
                    for (g_217 = 4; (g_217 >= 1); g_217 -= 1)
                    { /* block id: 906 */
                        int32_t *l_2018 = (void*)0;
                        int32_t *l_2019 = &g_160;
                        int32_t *l_2020[8] = {&g_850[8],&l_2003[3][2],&g_850[8],&l_2003[3][2],&g_850[8],&l_2003[3][2],&g_850[8],&l_2003[3][2]};
                        int i, j;
                        (*g_1318) = &l_2003[g_217][(g_1540.f6 + 1)];
                        --l_2022[0][0];
                    }
                    for (g_176 = 1; (g_176 <= 4); g_176 += 1)
                    { /* block id: 912 */
                        struct S1 *l_2025[8][7][4] = {{{&g_1207[6],&g_1207[7],(void*)0,(void*)0},{&g_1207[6],&g_1207[7],&g_118,&g_118},{&g_1207[6],(void*)0,(void*)0,&g_1207[7]},{&g_1207[6],&g_118,&g_1207[2],(void*)0},{&g_1207[6],&g_1207[7],&g_1207[6],&g_1207[7]},{&g_1207[6],(void*)0,&g_1207[2],&g_118},{&g_1207[6],&g_1207[7],(void*)0,(void*)0}},{{&g_1207[6],&g_118,&g_118,&g_1207[7]},{&g_1207[6],(void*)0,(void*)0,&g_1207[7]},{&g_1207[6],&g_1207[7],&g_1207[2],&g_118},{&g_1207[6],&g_1207[7],&g_1207[6],&g_1207[7]},{&g_1207[6],&g_118,&g_1207[2],&g_1207[7]},{&g_1207[6],&g_1207[7],(void*)0,(void*)0},{&g_1207[6],&g_1207[7],&g_118,&g_118}},{{&g_1207[6],(void*)0,(void*)0,&g_1207[7]},{&g_1207[6],&g_118,&g_1207[2],(void*)0},{&g_1207[6],&g_1207[7],&g_1207[6],&g_1207[7]},{&g_1207[6],(void*)0,&g_1207[2],&g_118},{&g_1207[6],&g_1207[7],(void*)0,(void*)0},{&g_1207[6],&g_118,&g_118,&g_1207[7]},{&g_1207[6],(void*)0,(void*)0,&g_1207[7]}},{{&g_1207[6],&g_1207[7],&g_1207[2],&g_118},{&g_1207[6],&g_1207[7],&g_1207[6],&g_1207[7]},{&g_1207[6],&g_118,&g_1207[2],&g_1207[7]},{&g_1207[6],&g_1207[7],(void*)0,&g_118},{&g_1207[6],(void*)0,&g_118,&g_118},{&g_1207[6],&g_1207[6],&g_1207[6],(void*)0},{&g_1207[6],&g_118,&g_118,(void*)0}},{{&g_1207[6],(void*)0,&g_1207[6],(void*)0},{&g_1207[6],(void*)0,&g_118,&g_118},{&g_1207[6],(void*)0,&g_1207[6],&g_1207[6]},{&g_1207[6],&g_118,&g_118,(void*)0},{&g_1207[6],&g_118,&g_1207[6],(void*)0},{&g_1207[6],(void*)0,&g_118,&g_1207[6]},{&g_1207[6],(void*)0,&g_1207[6],(void*)0}},{{&g_1207[6],&g_1207[6],&g_118,(void*)0},{&g_1207[6],(void*)0,&g_1207[6],&g_118},{&g_1207[6],(void*)0,&g_118,&g_118},{&g_1207[6],&g_1207[6],&g_1207[6],(void*)0},{&g_1207[6],&g_118,&g_118,(void*)0},{&g_1207[6],(void*)0,&g_1207[6],(void*)0},{&g_1207[6],(void*)0,&g_118,&g_118}},{{&g_1207[6],(void*)0,&g_1207[6],&g_1207[6]},{&g_1207[6],&g_118,&g_118,(void*)0},{&g_1207[6],&g_118,&g_1207[6],(void*)0},{&g_1207[6],(void*)0,&g_118,&g_1207[6]},{&g_1207[6],(void*)0,&g_1207[6],(void*)0},{&g_1207[6],&g_1207[6],&g_118,(void*)0},{&g_1207[6],(void*)0,&g_1207[6],&g_118}},{{&g_1207[6],(void*)0,&g_118,&g_118},{&g_1207[6],&g_1207[6],&g_1207[6],(void*)0},{&g_1207[6],&g_118,&g_118,(void*)0},{&g_1207[6],(void*)0,&g_1207[6],(void*)0},{&g_1207[6],(void*)0,&g_118,&g_118},{&g_1207[6],(void*)0,&g_1207[6],&g_1207[6]},{&g_1207[6],&g_118,&g_118,(void*)0}}};
                        struct S1 **l_2026[5];
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                            l_2026[i] = &l_2025[7][3][0];
                        l_2029 = (l_2027 = ((*g_1769) = l_2025[7][3][0]));
                        (*l_1991) |= (((l_2003[g_1540.f6][(g_1540.f6 + 1)] || l_2003[g_176][g_1540.f6]) < ((void*)0 == l_2033)) != (l_2034 != (((safe_mod_func_int8_t_s_s(((safe_rshift_func_int8_t_s_u((((1UL ^ (g_1540.f5 & (((void*)0 == l_2039) == (*g_1642)))) >= p_66) , p_66), (*g_1995))) ^ (*g_1395)), 0x07L)) >= (-4L)) , l_2034)));
                    }
                }
                l_2048 &= (((void*)0 == &l_2016) != (l_2003[g_1540.f6][g_1540.f6] < ((l_2041 &= (l_2040 = ((*g_1995) = 0x19L))) != ((safe_mod_func_int16_t_s_s(1L, (safe_mod_func_int64_t_s_s(((safe_add_func_uint64_t_u_u((l_2017 & 0xDA34A6BFF4C36BC8LL), ((*g_1395)++))) == (0x28EFL && ((p_66 , (*g_1642)) || (-1L)))), 0xFA20BF6F7578CCBCLL)))) == (*l_2005)))));
            }
            l_2063 ^= (safe_sub_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u(0x595DL, l_2055)) == (safe_sub_func_int64_t_s_s((((0x9052L <= (((p_66 || (p_66 != (((((*g_689) = (*g_689)) == l_2059) , (safe_mul_func_uint16_t_u_u((*g_690), p_66))) < p_66))) == 0L) , l_2062)) > p_66) && p_66), (*l_1991)))), (*g_1995)));
            l_2069++;
        }
        else
        { /* block id: 928 */
            const uint8_t *l_2097 = &g_1442.f4;
            const uint8_t **l_2096 = &l_2097;
            int32_t l_2105[8] = {6L,6L,6L,6L,6L,6L,6L,6L};
            int32_t l_2112 = 2L;
            uint32_t l_2114 = 0x11505905L;
            uint16_t **l_2128 = &g_690;
            int8_t l_2181 = 0x0BL;
            int16_t ** const l_2196 = &g_276;
            uint32_t l_2199 = 0x2DB37C63L;
            const volatile uint8_t *** volatile * volatile *l_2205 = &g_2201;
            struct S0 *l_2220 = &g_1241[1][0][5];
            int32_t *l_2287 = &l_2110;
            int i;
            for (g_393 = 0; (g_393 <= (-4)); --g_393)
            { /* block id: 931 */
                int64_t *l_2089[2];
                int64_t **l_2091 = (void*)0;
                int64_t **l_2092 = (void*)0;
                int64_t *l_2093[1][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                int32_t l_2098 = 0x6A8A8F84L;
                uint64_t l_2099 = 1UL;
                int32_t *l_2100 = &g_1456;
                int32_t *l_2101 = &l_2003[2][2];
                int32_t *l_2102 = (void*)0;
                int32_t *l_2103 = (void*)0;
                int32_t *l_2104[5];
                int8_t l_2113 = 0x80L;
                uint8_t *l_2131 = &g_841.f4;
                uint8_t *l_2132[8][6][5] = {{{&g_1540.f4,&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4},{&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1]},{&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_1540.f4},{&g_1540.f4,&g_452[0][0][1],&g_1540.f4,(void*)0,&g_1540.f4},{&g_1912.f4,&g_1912.f4,(void*)0,&g_452[0][0][1],&g_1540.f4},{&g_452[0][0][1],(void*)0,&g_1912.f4,(void*)0,&g_452[0][0][1]}},{{&g_1540.f4,&g_452[0][0][1],(void*)0,&g_1912.f4,&g_1912.f4},{&g_1540.f4,(void*)0,&g_1540.f4,&g_452[0][0][1],&g_1540.f4},{&g_1540.f4,&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4},{&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1]},{&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_1540.f4},{&g_1540.f4,&g_452[0][0][1],&g_1540.f4,(void*)0,&g_1540.f4}},{{&g_1912.f4,&g_1912.f4,(void*)0,&g_452[0][0][1],&g_1540.f4},{&g_452[0][0][1],(void*)0,&g_1912.f4,(void*)0,&g_452[0][0][1]},{&g_1540.f4,&g_452[0][0][1],(void*)0,&g_1912.f4,&g_1912.f4},{&g_1540.f4,(void*)0,&g_1540.f4,&g_452[0][0][1],&g_1540.f4},{&g_1540.f4,&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4},{&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1]}},{{&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_1540.f4},{&g_1540.f4,&g_452[0][0][1],&g_1540.f4,(void*)0,&g_1540.f4},{&g_1912.f4,&g_1912.f4,(void*)0,&g_452[0][0][1],&g_1540.f4},{&g_452[0][0][1],(void*)0,&g_1912.f4,(void*)0,&g_452[0][0][1]},{&g_1540.f4,&g_452[0][0][1],(void*)0,&g_1912.f4,&g_1912.f4},{&g_1540.f4,(void*)0,&g_1540.f4,&g_452[0][0][1],&g_1540.f4}},{{&g_1540.f4,&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4},{&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1]},{&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_1540.f4},{&g_1540.f4,&g_452[0][0][1],&g_1540.f4,(void*)0,&g_1540.f4},{&g_1912.f4,&g_1912.f4,(void*)0,&g_452[0][0][1],&g_1540.f4},{&g_452[0][0][1],(void*)0,&g_1912.f4,(void*)0,&g_452[0][0][1]}},{{&g_1540.f4,&g_452[0][0][1],(void*)0,&g_1912.f4,&g_1912.f4},{&g_1540.f4,(void*)0,&g_1540.f4,&g_452[0][0][1],&g_1540.f4},{&g_1540.f4,&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4},{&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1]},{&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_1540.f4},{&g_1540.f4,&g_452[0][0][1],&g_1540.f4,(void*)0,&g_1540.f4}},{{&g_1912.f4,&g_1912.f4,(void*)0,&g_452[0][0][1],&g_1540.f4},{&g_452[0][0][1],(void*)0,&g_1912.f4,(void*)0,&g_452[0][0][1]},{&g_1540.f4,&g_452[0][0][1],(void*)0,&g_1912.f4,&g_1912.f4},{&g_1540.f4,(void*)0,&g_1540.f4,&g_452[0][0][1],&g_1540.f4},{&g_1540.f4,&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4},{&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1]}},{{&g_1912.f4,&g_452[0][0][1],&g_452[0][0][1],&g_1912.f4,&g_1540.f4},{&g_1540.f4,&g_452[0][0][1],&g_1540.f4,(void*)0,&g_1540.f4},{&g_1912.f4,&g_1912.f4,(void*)0,&g_452[0][0][1],&g_1540.f4},{&g_452[0][0][1],(void*)0,&g_1912.f4,(void*)0,&g_452[0][0][1]},{&g_1540.f4,&g_452[0][0][1],(void*)0,&g_1912.f4,&g_1912.f4},{&g_1540.f4,(void*)0,&g_1540.f4,&g_452[0][0][1],&g_1540.f4}}};
                int8_t l_2140 = (-4L);
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_2089[i] = (void*)0;
                for (i = 0; i < 5; i++)
                    l_2104[i] = (void*)0;
                l_2098 = (g_1409.f7 < (safe_rshift_func_uint8_t_u_s(((((**l_1999)--) && (((*l_2100) = ((((*g_1995) = ((safe_mod_func_uint64_t_u_u(((((*g_1642) = (safe_sub_func_uint16_t_u_u((!((*g_1395) = (safe_mul_func_uint16_t_u_u((((safe_sub_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_s((&l_2058 == (void*)0), ((p_66 , l_2089[1]) == (l_2093[0][0] = l_2090)))) >= ((safe_mul_func_int8_t_s_s(((l_2096 == l_1999) , ((**g_1647) , 0x1DL)), l_2098)) > p_66)), 0xA4EAL)) < 6L) <= p_66), 0x368EL)))), l_2099))) <= 1L) && p_66), l_2099)) , (*g_1995))) > 0L) >= g_1465.f0)) , p_66)) || (*g_892)), 5)));
                --l_2114;
                if ((safe_add_func_int8_t_s_s((safe_lshift_func_int8_t_s_u(((*l_1991) = ((+(((g_2123[0] = l_2122) == l_2128) ^ ((*g_1395) >= (!((0x921FCA1A2C3EC446LL >= p_66) & (1UL | ((l_2114 & ((((l_2131 = (((l_2105[5] >= (g_2130 ^= ((*l_2090) ^= (l_2016 != l_2105[7])))) == 0xD3BAC65792F6AD7ALL) , (void*)0)) == l_2132[0][0][1]) & 2L) & (-3L))) , (**g_689)))))))) ^ p_66)), g_2133[1])), 0x54L)))
                { /* block id: 945 */
                    float l_2134[6][3] = {{0x0.2p+1,(-0x1.1p+1),0x3.E473B8p-92},{(-0x1.1p+1),(-0x1.Ap-1),(-0x1.1p+1)},{(-0x1.1p+1),0x1.5p+1,(-0x1.Ap-1)},{0x1.5p+1,(-0x1.1p+1),(-0x1.1p+1)},{(-0x1.Ap-1),(-0x1.1p+1),0x3.E473B8p-92},{0x7.769644p+83,0x1.5p+1,0x5.4p+1}};
                    int8_t l_2135 = (-1L);
                    float *l_2145[2];
                    int32_t l_2146 = (-8L);
                    int32_t l_2147 = 1L;
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_2145[i] = &g_1409.f2;
                    if (l_2135)
                        break;
                    (*l_2101) = (((safe_div_func_float_f_f(((safe_lshift_func_int8_t_s_u((p_66 <= p_66), 3)) , p_66), 0xD.F656D6p-51)) > (l_2140 > (l_2147 = ((safe_add_func_float_f_f((p_66 != 0xB.5A3D2Dp-86), 0xF.C59F30p-90)) != (l_2146 = (safe_sub_func_float_f_f(p_66, 0x5.CF7F2Bp-63))))))) != l_2148);
                    if (((4L && (safe_add_func_int8_t_s_s(p_66, (!0x13E5ADACL)))) || l_2135))
                    { /* block id: 950 */
                        int32_t **l_2156[2];
                        int32_t ***l_2157 = &g_1318;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_2156[i] = &l_2104[4];
                        (*l_2101) = (((safe_div_func_int64_t_s_s(((((*g_1255) != ((*l_2157) = (p_66 , l_2156[1]))) == ((safe_sub_func_int64_t_s_s(p_66, (((*l_1991) = ((**l_2122) &= (safe_rshift_func_uint16_t_u_s(((((safe_div_func_int64_t_s_s((((((((*g_1762) = (p_66 >= (*g_1143))) < 0x8.2p-1) == ((safe_add_func_float_f_f((&g_1054[2] != &g_1054[4]), 0x2.3p+1)) == g_176)) != l_2105[7]) , (void*)0) == (void*)0), 1L)) , 0x39FF27238E12CEC0LL) < (*g_1395)) && 0x6920L), 14)))) != 6L))) <= p_66)) | g_1912.f0), p_66)) >= p_66) || l_2166);
                        (**l_2157) = &l_2110;
                        return p_66;
                    }
                    else
                    { /* block id: 958 */
                        (*g_1318) = &l_2146;
                    }
                }
                else
                { /* block id: 961 */
                    float l_2197[1][5];
                    int32_t l_2198[4][5][8] = {{{0x6EFE4B8BL,0x414B6201L,0x30F49F3AL,0xAA578751L,0xE12BFF02L,0x85D6E6F1L,0xA9125288L,(-2L)},{0x6EFE4B8BL,(-2L),5L,1L,0xA9125288L,(-8L),1L,(-9L)},{0x4E9F73AAL,0xCE17F12FL,(-1L),(-3L),0xA401DCB3L,(-1L),0L,(-1L)},{0L,0x30F49F3AL,(-9L),(-2L),0L,(-2L),0x2BC143C8L,0xCB16D351L},{0xFDC2D9B1L,(-9L),0x5FD06AF4L,0x57E2090FL,0x6EFE4B8BL,0x278C25C3L,0xAA578751L,(-9L)}},{{0xF2D04C4EL,0x39C8D846L,(-9L),(-9L),1L,0x677D4389L,0x85D6E6F1L,0x38DCE72FL},{0x2BC143C8L,(-9L),0xCB16D351L,0xAA578751L,0xF2D04C4EL,0x27605C45L,(-9L),0x414B6201L},{0x80BCDC6FL,0xAA578751L,1L,0L,0L,1L,0xAA578751L,0x80BCDC6FL},{0x677D4389L,0x6EFE4B8BL,0x94F15A67L,0xFBC9C90BL,0xCE17F12FL,(-9L),0xE12BFF02L,0L},{0xAA578751L,0xCB16D351L,(-9L),0x2BC143C8L,0xFBC9C90BL,(-9L),1L,(-3L)}},{{1L,0x6EFE4B8BL,8L,0xBFA35E03L,0x39C8D846L,1L,1L,1L},{(-3L),0xAA578751L,0x85D6E6F1L,0x30F49F3AL,1L,0x27605C45L,(-9L),0xCE17F12FL},{(-2L),(-9L),0x30F49F3AL,0L,0L,0x677D4389L,0xBFA35E03L,0xF2D04C4EL},{0x57E2090FL,0x39C8D846L,6L,0xCE17F12FL,0x39C8D846L,0x278C25C3L,0xA401DCB3L,(-2L)},{(-9L),(-9L),(-1L),(-9L),0x80BCDC6FL,(-2L),0x27605C45L,0L}},{{0xAA578751L,0x30F49F3AL,0x414B6201L,0x6EFE4B8BL,0x4E9F73AAL,(-1L),(-1L),(-1L)},{0L,0xCE17F12FL,3L,0xCE17F12FL,0L,(-8L),0x80BCDC6FL,0xBFA35E03L},{0xFBC9C90BL,(-2L),(-2L),0x666C4E11L,0xBFA35E03L,0x85D6E6F1L,(-3L),0xCE17F12FL},{0x2BC143C8L,0x414B6201L,(-2L),0xFDC2D9B1L,1L,0xFBC9C90BL,0x80BCDC6FL,1L},{0xBFA35E03L,0x2BC143C8L,3L,0xBFA35E03L,0x6EFE4B8BL,0L,(-1L),0x57E2090FL}}};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_2197[i][j] = 0x5.Dp+1;
                    }
                    l_2198[0][1][3] = ((safe_lshift_func_uint8_t_u_s(((safe_div_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((*g_1642), ((((safe_mod_func_int16_t_s_s(((((safe_lshift_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_u((safe_sub_func_int32_t_s_s(p_66, (l_2181 , 0xC5A3D95CL))), 2)) && (p_66 && (((safe_lshift_func_int16_t_s_s(((*l_2058) |= (safe_div_func_uint8_t_u_u(((((((((((p_66 , ((g_1643 || ((safe_rshift_func_uint16_t_u_u(((**g_689)--), 4)) < 0L)) <= (safe_sub_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u((p_66 < l_2105[7]), 0L)), (*g_1395))))) , p_66) , p_66) , (void*)0) != &g_1642) != (*l_2005)) ^ (*g_1642)) != 0x36A0AC76L) , (void*)0) == l_2196), 0x48L))), 8)) , l_2105[3]) , g_1465.f3))), p_66)) > p_66) & 0xAA57L) ^ p_66), p_66)) != 0x009FDBD8L) == 18446744073709551613UL) , p_66))), 0x61L)) , l_2198[0][1][3]), 5)) >= l_2199);
                    (*g_1769) = l_2029;
                }
            }
            l_2205 = ((((**g_1647) , &p_66) == (void*)0) , g_2200);
            for (g_127 = 0; (g_127 <= 15); g_127++)
            { /* block id: 971 */
                struct S0 *l_2219 = &g_1241[1][0][5];
                int32_t l_2221[1][10][5] = {{{3L,(-9L),0xB639F808L,0x40B52CA3L,(-9L)},{0x4A520119L,0xB639F808L,0xB639F808L,0x4A520119L,0x40B52CA3L},{0xBAF6C82BL,0x4A520119L,0xB0016EBFL,(-9L),(-9L)},{1L,0xB639F808L,1L,0x4979EBF7L,0xB639F808L},{3L,0xB0016EBFL,0x4979EBF7L,3L,0x4979EBF7L},{3L,3L,(-9L),0xB639F808L,0x40B52CA3L},{1L,0x40B52CA3L,0x4979EBF7L,0x4979EBF7L,0x40B52CA3L},{0x40B52CA3L,0xB0016EBFL,1L,0x40B52CA3L,0x4979EBF7L},{0xB639F808L,0x40B52CA3L,(-9L),0x40B52CA3L,0xB639F808L},{1L,3L,0xB0016EBFL,0x4979EBF7L,3L}}};
                uint32_t *l_2222[1][10] = {{&g_2130,&l_2015[0],&l_2015[0],&g_2130,&l_2015[0],&l_2015[0],&g_2130,&l_2015[0],&l_2015[0],&g_2130}};
                int32_t l_2224[6];
                struct S1 l_2241 = {834,495,60,18};
                uint8_t ****l_2250[10][7][3] = {{{&l_1997[1][0],&l_1997[0][1],&l_1997[7][0]},{&l_1997[5][1],&l_1997[7][0],&l_1997[5][1]},{&l_1997[5][1],&l_1997[6][1],(void*)0},{&l_1997[1][0],(void*)0,&l_1997[5][1]},{&l_1997[7][0],&l_1997[7][0],&l_1997[2][0]},{&l_1997[7][0],&l_1997[6][1],(void*)0},{&l_1997[7][0],&l_1997[0][1],&l_1997[7][0]}},{{&l_1997[1][0],&l_1997[5][1],&l_1997[2][0]},{&l_1997[5][1],&l_1997[7][0],(void*)0},{&l_1997[5][1],(void*)0,&l_1997[7][0]},{&l_1997[1][0],&l_1997[7][0],&l_1997[5][1]},{&l_1997[7][0],&l_1997[3][0],&l_1997[7][0]},{&l_1997[7][0],(void*)0,&l_1997[7][0]},{&l_1997[7][0],&l_1997[7][0],&l_1997[7][0]}},{{&l_1997[1][0],&l_1997[7][0],&l_1997[7][0]},{&l_1997[5][1],&l_1997[0][1],&l_1997[5][1]},{&l_1997[5][1],&l_1997[5][1],&l_1997[1][1]},{&l_1997[1][0],&l_1997[3][0],(void*)0},{&l_1997[7][0],(void*)0,&l_1997[7][0]},{&l_1997[7][0],&l_1997[5][1],&l_1997[7][0]},{&l_1997[7][0],&l_1997[7][0],(void*)0}},{{&l_1997[1][0],&l_1997[0][1],&l_1997[7][0]},{&l_1997[5][1],&l_1997[7][0],&l_1997[5][1]},{&l_1997[5][1],&l_1997[6][1],(void*)0},{&l_1997[1][0],(void*)0,&l_1997[5][1]},{&l_1997[7][0],&l_1997[7][0],&l_1997[2][0]},{&l_1997[7][0],&l_1997[6][1],(void*)0},{&l_1997[7][0],&l_1997[0][1],&l_1997[7][0]}},{{&l_1997[1][0],&l_1997[5][1],&l_1997[2][0]},{&l_1997[5][1],&l_1997[7][0],(void*)0},{&l_1997[5][1],(void*)0,&l_1997[7][0]},{&l_1997[1][0],&l_1997[7][0],&l_1997[5][1]},{&l_1997[7][0],&l_1997[3][0],&l_1997[7][0]},{&l_1997[7][0],(void*)0,&l_1997[7][0]},{&l_1997[7][0],&l_1997[7][0],&l_1997[7][0]}},{{&l_1997[1][0],&l_1997[7][0],&l_1997[7][0]},{&l_1997[5][1],&l_1997[0][1],&l_1997[5][1]},{&l_1997[5][1],&l_1997[5][1],&l_1997[1][1]},{&l_1997[1][0],&l_1997[3][0],(void*)0},{&l_1997[7][0],(void*)0,&l_1997[7][0]},{&l_1997[7][0],&l_1997[5][1],&l_1997[7][0]},{&l_1997[7][0],&l_1997[7][0],(void*)0}},{{&l_1997[1][0],&l_1997[0][1],&l_1997[7][0]},{&l_1997[5][1],&l_1997[7][0],&l_1997[5][1]},{&l_1997[5][1],&l_1997[6][1],(void*)0},{&l_1997[1][0],(void*)0,&l_1997[5][1]},{&l_1997[7][0],&l_1997[7][0],&l_1997[2][0]},{&l_1997[7][0],&l_1997[6][1],(void*)0},{&l_1997[7][0],&l_1997[8][0],&l_1997[7][0]}},{{(void*)0,&l_1997[7][0],&l_1997[7][0]},{&l_1997[7][0],&l_1997[0][1],&l_1997[7][0]},{&l_1997[7][0],&l_1997[5][1],&l_1997[5][0]},{(void*)0,&l_1997[7][0],(void*)0},{&l_1997[5][0],&l_1997[3][1],&l_1997[2][1]},{&l_1997[7][0],&l_1997[5][1],&l_1997[7][0]},{&l_1997[5][0],&l_1997[7][0],&l_1997[8][0]}},{{(void*)0,&l_1997[7][0],&l_1997[2][1]},{&l_1997[7][0],&l_1997[8][0],&l_1997[7][0]},{&l_1997[7][0],&l_1997[5][1],&l_1997[8][1]},{(void*)0,&l_1997[3][1],&l_1997[7][0]},{&l_1997[5][0],&l_1997[5][1],&l_1997[7][0]},{&l_1997[7][0],&l_1997[5][1],&l_1997[8][0]},{&l_1997[5][0],&l_1997[0][1],(void*)0}},{{(void*)0,&l_1997[2][0],&l_1997[7][0]},{&l_1997[7][0],&l_1997[7][0],(void*)0},{&l_1997[7][0],(void*)0,&l_1997[8][0]},{(void*)0,&l_1997[5][1],&l_1997[7][0]},{&l_1997[5][0],&l_1997[7][0],&l_1997[7][0]},{&l_1997[7][0],(void*)0,(void*)0},{&l_1997[5][0],&l_1997[8][0],&l_1997[7][0]}}};
                int i, j, k;
                for (i = 0; i < 6; i++)
                    l_2224[i] = 0x4D01B9E1L;
                if (((((safe_unary_minus_func_uint64_t_u(p_66)) , ((safe_mod_func_uint8_t_u_u((((p_66 && ((l_2223 = ((p_66 , ((*g_1642) = (1UL != ((safe_add_func_uint64_t_u_u(0x4B3BD84B2157D193LL, (safe_mul_func_uint16_t_u_u((safe_div_func_uint32_t_u_u(((safe_lshift_func_int8_t_s_s((l_2219 != (((void*)0 == (*l_2128)) , l_2220)), 2)) | 0x50E4C711004FD61ALL), p_66)), p_66)))) , l_2221[0][1][4])))) , 0x2295D407L)) , p_66)) || p_66) , l_2224[5]), (*l_1991))) | 247UL)) , p_66) == p_66))
                { /* block id: 974 */
                    int32_t *l_2231 = &l_2108[3];
                    float *l_2232[9][9][3] = {{{(void*)0,&g_273,&g_1912.f2},{(void*)0,&g_1442.f2,&g_270},{&g_48[0][3],&g_1241[1][0][5].f2,&g_270},{&g_1409.f2,&g_270,&g_1540.f2},{&g_1912.f2,&g_1442.f2,&g_1241[1][0][5].f2},{&g_1540.f2,&g_1409.f2,(void*)0},{&g_1442.f2,&g_1442.f2,&g_1241[1][0][5].f2},{&g_1540.f2,&g_48[0][9],&g_1540.f2},{&g_270,(void*)0,&g_270}},{{&g_273,&g_1912.f2,&g_270},{&g_1409.f2,&g_841.f2,&g_1912.f2},{&g_18[6],&g_270,&g_1409.f2},{(void*)0,(void*)0,&g_273},{&g_18[6],&g_1856.f2,(void*)0},{&g_1409.f2,&g_1241[1][0][5].f2,&g_270},{&g_273,&g_1442.f2,(void*)0},{&g_270,&g_273,(void*)0},{&g_1540.f2,&g_48[0][3],(void*)0}},{{&g_1442.f2,&g_48[0][3],&g_48[0][3]},{&g_1540.f2,&g_48[0][3],&g_18[1]},{&g_1912.f2,&g_273,&g_1409.f2},{&g_1409.f2,&g_1442.f2,&g_18[7]},{&g_48[0][3],&g_1241[1][0][5].f2,(void*)0},{(void*)0,&g_1856.f2,(void*)0},{(void*)0,(void*)0,&g_1912.f2},{&g_1912.f2,&g_270,(void*)0},{&g_273,&g_841.f2,(void*)0}},{{&g_18[7],&g_1912.f2,&g_18[7]},{&g_48[0][3],(void*)0,&g_1409.f2},{&g_270,&g_48[0][9],&g_18[1]},{&g_1409.f2,&g_1442.f2,&g_48[0][3]},{&g_1912.f2,&g_1409.f2,(void*)0},{&g_1409.f2,&g_1442.f2,(void*)0},{&g_270,&g_270,(void*)0},{&g_48[0][3],&g_1241[1][0][5].f2,&g_270},{&g_18[7],&g_1442.f2,(void*)0}},{{&g_273,&g_273,&g_273},{&g_1912.f2,&g_1856.f2,&g_1409.f2},{(void*)0,&g_273,&g_1912.f2},{(void*)0,&g_1442.f2,&g_270},{&g_48[0][3],&g_1241[1][0][5].f2,&g_270},{&g_1409.f2,&g_270,&g_1540.f2},{&g_1409.f2,(void*)0,(void*)0},{&g_270,&g_1540.f2,&g_1856.f2},{&g_1409.f2,(void*)0,(void*)0}},{{&g_1241[1][0][5].f2,&g_841.f2,&g_270},{(void*)0,&g_1912.f2,(void*)0},{&g_1912.f2,&g_18[1],&g_48[0][6]},{(void*)0,&g_841.f2,&g_1912.f2},{&g_1409.f2,&g_1912.f2,&g_18[7]},{&g_1409.f2,&g_273,&g_48[0][9]},{&g_1409.f2,(void*)0,&g_1856.f2},{(void*)0,&g_48[0][3],&g_1912.f2},{&g_1912.f2,&g_1912.f2,(void*)0}},{{(void*)0,&g_270,(void*)0},{&g_1241[1][0][5].f2,&g_273,&g_270},{&g_1409.f2,&g_1241[1][0][5].f2,(void*)0},{&g_270,&g_273,&g_48[0][3]},{&g_1409.f2,&g_270,(void*)0},{&g_18[7],&g_1912.f2,&g_1409.f2},{(void*)0,&g_48[0][3],&g_1856.f2},{&g_1856.f2,(void*)0,&g_1241[1][0][5].f2},{&g_1856.f2,&g_273,&g_1409.f2}},{{&g_841.f2,&g_1912.f2,&g_1241[1][0][5].f2},{&g_841.f2,&g_841.f2,&g_1856.f2},{&g_1409.f2,&g_18[1],&g_1409.f2},{&g_1912.f2,&g_1912.f2,(void*)0},{&g_48[0][6],&g_841.f2,&g_48[0][3]},{&g_48[0][3],(void*)0,(void*)0},{(void*)0,&g_1540.f2,&g_270},{&g_48[0][3],(void*)0,(void*)0},{&g_48[0][6],&g_1409.f2,(void*)0}},{{&g_1912.f2,&g_1442.f2,&g_1912.f2},{&g_1409.f2,(void*)0,&g_1856.f2},{&g_841.f2,&g_48[0][3],&g_48[0][9]},{&g_841.f2,(void*)0,&g_18[7]},{&g_1856.f2,&g_48[0][3],&g_1912.f2},{&g_1856.f2,(void*)0,&g_48[0][6]},{(void*)0,&g_1442.f2,(void*)0},{&g_18[7],&g_1409.f2,&g_270},{&g_1409.f2,(void*)0,(void*)0}}};
                    uint64_t *l_2248 = &g_717;
                    int8_t *l_2251 = &g_1540.f3;
                    int32_t l_2256 = 0x37316A37L;
                    int32_t *l_2258 = &l_2109[1][9];
                    int32_t *l_2259[10][2][8] = {{{&g_160,&l_2224[5],&l_2221[0][2][4],(void*)0,&l_2109[0][2],(void*)0,&l_2066,&l_2065},{&g_850[8],&g_207[1],&l_2105[7],&g_850[8],&g_850[8],&l_2105[7],&g_207[1],&g_850[8]}},{{&l_2112,&l_2109[0][2],&g_346,&l_2109[0][6],&l_2224[4],&g_207[2],&g_850[8],&l_2224[5]},{&g_207[1],&g_160,&l_2224[0],&l_2105[6],&l_2109[0][6],&g_207[2],&l_2105[7],&l_2066}},{{&g_207[1],&l_2109[0][2],&l_2065,&l_2109[0][0],&g_346,&l_2105[7],&l_2224[1],&g_346},{&l_2066,&g_207[1],&g_160,&l_2110,&g_850[8],(void*)0,&l_2224[5],(void*)0}},{{&g_25[0][1],&l_2224[5],&l_2109[0][2],&g_850[8],&l_2109[0][2],&l_2224[1],&l_2224[4],&l_2224[0]},{&l_2109[0][2],&l_2109[0][0],&l_2105[7],(void*)0,&l_2224[0],&g_850[8],&g_207[2],&l_2066}},{{&g_25[0][0],&g_850[8],&g_850[8],&l_2112,&l_2224[5],&g_25[0][3],&l_2065,(void*)0},{&g_850[8],&l_2221[0][2][4],(void*)0,&l_2110,&l_2224[5],&l_2109[0][0],&l_2224[5],&l_2224[0]}},{{&l_2105[6],(void*)0,&g_207[1],&g_850[8],&g_160,&g_850[8],&g_207[1],(void*)0},{&g_207[2],&g_850[8],&l_2112,&l_2224[5],&l_2066,&l_2224[0],&l_2224[0],&g_346}},{{&l_2224[0],&g_346,&l_2109[0][2],&l_2105[7],&g_207[2],&l_2065,&l_2224[0],&g_160},{(void*)0,&l_2105[7],&l_2112,&l_2224[0],&g_25[0][1],&g_207[2],&g_207[1],&g_850[8]}},{{&g_25[0][1],&g_207[2],&g_207[1],&g_850[8],&l_2109[0][0],(void*)0,&l_2224[5],(void*)0},{(void*)0,&l_2224[0],(void*)0,&l_2105[7],&l_2066,&g_850[8],&l_2109[0][6],&g_207[1]}},{{&g_207[2],&g_207[1],(void*)0,&l_2066,&l_2065,(void*)0,&l_2224[1],&l_2068},{(void*)0,&l_2105[7],&l_2066,&g_160,&l_2112,&l_2109[0][2],(void*)0,&g_207[2]}},{{&l_2105[7],&l_2112,&l_2224[0],&l_2065,&l_2003[3][5],&l_2068,&g_1731,&g_850[8]},{&l_2224[5],&g_160,&l_2003[3][5],&g_207[1],&g_850[8],&g_850[8],&g_207[1],&l_2003[3][5]}}};
                    uint32_t l_2260 = 0x2F0D9F09L;
                    int i, j, k;
                    l_2112 ^= ((safe_add_func_uint64_t_u_u(p_66, (((*l_2090) ^= (((l_2105[4] ^= (safe_lshift_func_int16_t_s_u(((((g_2252 = (safe_add_func_float_f_f((((*l_2231) = 0x5B7F37C0L) , ((l_2224[2] = 0x9.4F934Fp-22) > (((l_2234 && (((*l_2251) |= (((**g_316) , ((((((safe_sub_func_int64_t_s_s(((((**l_1999) ^= ((safe_rshift_func_int8_t_s_u((safe_mod_func_uint64_t_u_u(p_66, (l_2241 , ((*g_1395)++)))), 5)) != (safe_lshift_func_int8_t_s_s(((((*l_2248) = (safe_rshift_func_uint16_t_u_u(((void*)0 != l_1999), 3))) | 0x704BAE9140DE197BLL) == g_841.f4), 0)))) | p_66) > g_2030.f2), p_66)) < g_1081.f6) != (-1L)) , p_66) , l_2249) == l_2250[7][3][0])) > 4294967291UL)) < g_2125)) , (-0x1.Ap+1)) >= p_66))), g_1241[1][0][5].f3))) , l_2253) == l_2254) != 0L), l_2256))) || (-1L)) != l_2256)) ^ 0x910B8B7CB4D62422LL))) , (-5L));
                    ++l_2260;
                }
                else
                { /* block id: 986 */
                    int8_t l_2271[6][6] = {{0x5DL,0x5DL,0x90L,9L,4L,0x90L},{9L,4L,0x90L,4L,9L,0x90L},{4L,9L,0x90L,0x5DL,0x5DL,0x90L},{0x5DL,0x5DL,0x90L,9L,4L,0x90L},{9L,4L,0x90L,4L,9L,0x90L},{4L,9L,0x90L,0x5DL,0x5DL,0x90L}};
                    int32_t l_2272 = 0x699B53D0L;
                    int32_t l_2273 = 1L;
                    int32_t l_2274 = 0xCD93D51DL;
                    int32_t l_2275 = 1L;
                    int32_t l_2277 = 0x82A91ADEL;
                    int32_t l_2278 = 0xDD9846ACL;
                    uint16_t l_2279 = 0x9765L;
                    int i, j;
                    (*g_1318) = &l_2105[6];
                    if (p_66)
                        break;
                    for (g_1731 = 0; (g_1731 < 8); g_1731 = safe_add_func_uint8_t_u_u(g_1731, 9))
                    { /* block id: 991 */
                        int32_t *l_2269 = &g_850[8];
                        int32_t *l_2270[6][9] = {{&l_2003[3][5],&g_25[0][2],(void*)0,&g_25[0][2],&l_2003[3][5],(void*)0,&l_2224[2],&l_2003[1][5],&l_2224[2]},{(void*)0,&g_2252,&l_2224[2],&l_2224[2],&g_2252,(void*)0,&g_2252,&l_2224[2],&l_2224[2]},{&l_2003[3][5],(void*)0,&l_2224[2],&l_2003[1][5],&l_2224[2],(void*)0,&l_2003[3][5],&g_25[0][2],(void*)0},{(void*)0,&g_2252,(void*)0,(void*)0,(void*)0,(void*)0,&g_2252,(void*)0,(void*)0},{&l_2003[1][0],&g_25[0][2],&l_2224[2],&l_2105[2],&l_2224[1],&l_2105[2],&l_2224[2],&g_25[0][2],&l_2003[1][0]},{(void*)0,(void*)0,&l_2224[2],(void*)0,(void*)0,(void*)0,(void*)0,&l_2224[2],(void*)0}};
                        float l_2276 = 0x8.449A31p-98;
                        int i, j;
                        (*g_2268) = g_2265;
                        (*g_121) = (*g_121);
                        l_2279--;
                        if (l_2105[7])
                            break;
                    }
                }
                for (l_2181 = (-30); (l_2181 <= (-27)); l_2181 = safe_add_func_uint8_t_u_u(l_2181, 7))
                { /* block id: 1000 */
                    if (p_66)
                        break;
                }
                l_2287 = ((*g_1318) = l_2284);
                for (g_841.f6 = 7; (g_841.f6 == 50); g_841.f6 = safe_add_func_uint32_t_u_u(g_841.f6, 8))
                { /* block id: 1007 */
                    return l_2241.f0;
                }
            }
            for (l_2148 = 11; (l_2148 >= 12); l_2148 = safe_add_func_int64_t_s_s(l_2148, 6))
            { /* block id: 1013 */
                uint16_t l_2292 = 0xDE74L;
                for (g_777 = 0; (g_777 <= 4); g_777 += 1)
                { /* block id: 1016 */
                    for (g_1409.f3 = 0; (g_1409.f3 <= 4); g_1409.f3 += 1)
                    { /* block id: 1019 */
                        int i, j;
                        l_2233[g_1409.f3][g_777] = 0xE.06E0FAp-81;
                        l_2292 ^= (*l_2005);
                    }
                    for (g_1912.f7 = 8; (g_1912.f7 >= 0); g_1912.f7 -= 1)
                    { /* block id: 1025 */
                        return p_66;
                    }
                }
                return p_66;
            }
        }
        for (g_416 = 0; g_416 < 5; g_416 += 1)
        {
            for (g_841.f6 = 0; g_841.f6 < 6; g_841.f6 += 1)
            {
                for (g_46 = 0; g_46 < 2; g_46 += 1)
                {
                    l_2111[g_416][g_841.f6][g_46] = 0xA007L;
                }
            }
        }
        l_2295--;
    }
    return p_66;
}


/* ------------------------------------------ */
/* 
 * reads : g_1409.f3 g_1669 g_176 g_1442.f7 g_452 g_1252 g_1253 g_1561 g_121 g_1318 g_207 g_1642 g_1643 g_1395 g_426 g_850 g_1731 g_1409.f6 g_18 g_25 g_119.f1 g_275 g_276 g_1324.f0 g_202 g_1762 g_1768 g_892 g_346 g_1785 g_1257 g_1409.f4 g_348 g_1456 g_690 g_416 g_1821 g_1441.f1 g_841.f7 g_1324.f3 g_1856 g_1540.f3 g_688.f0 g_1143 g_662 g_1650 g_1540.f6 g_689 g_1241.f6 g_1254 g_1255 g_1912 g_350 g_48 g_1961 g_1465.f3 g_1207.f3 g_32 g_1094.f5 g_1983
 * writes: g_1409.f3 g_160 g_1643 g_1442.f7 g_207 g_176 g_1409.f6 g_121 g_426 g_1540.f6 g_25 g_777 g_841.f7 g_18 g_1442.f2 g_1769 g_1731 g_1257 g_1409.f4 g_346 g_1456 g_1821 g_217 g_855 g_1540.f2 g_48 g_483 g_841.f2 g_1067 g_1961 g_850 g_1856.f7 g_32
 */
static uint16_t  func_71(uint16_t * p_72, int16_t * p_73)
{ /* block id: 721 */
    int16_t l_1668 = 9L;
    int32_t l_1687 = 3L;
    int32_t l_1691 = 0L;
    int32_t l_1693 = 0xE9CC09B1L;
    int32_t l_1695 = 0x59C7A27DL;
    int32_t l_1701 = 1L;
    int32_t l_1702 = 0xBCDE982EL;
    int32_t l_1703 = 0xAA592C1DL;
    int32_t l_1706 = 0xE97D6077L;
    int32_t l_1708 = 1L;
    int32_t l_1709 = 0xC1300A53L;
    int32_t l_1710[2];
    int16_t ***l_1726 = &g_275;
    int8_t l_1739 = 0L;
    int32_t l_1741 = 9L;
    int8_t l_1754 = 0xD1L;
    struct S1 **l_1767 = &g_1206[2];
    int32_t l_1791 = 0x7177DD2AL;
    int8_t l_1964 = 0x56L;
    int8_t l_1980[3][5][7] = {{{0x9BL,(-2L),0xF8L,0xF8L,(-2L),0x9BL,(-2L)},{0x9FL,0xFCL,0xFCL,0x9FL,1L,0x9FL,0xFCL},{0x9BL,0x9BL,0x92L,(-2L),0x92L,0x9BL,0x9BL},{0xFCL,0x9FL,1L,0x9FL,0xFCL,0xFCL,0x9FL},{0xF8L,0x8CL,0xF8L,0x92L,0x92L,0xF8L,0x8CL}},{{0x9FL,0x10L,1L,1L,0x10L,0x9FL,0x10L},{0xF8L,0x92L,0x92L,0xF8L,0x8CL,0xF8L,0x92L},{0xFCL,0xFCL,0x9FL,1L,0x9FL,0xFCL,0xFCL},{0x9BL,0x92L,(-2L),0x92L,0x9BL,0x9BL,0x92L},{0x45L,0x10L,0x45L,0x9FL,0x9FL,0x45L,0x10L}},{{0x92L,0x8CL,(-2L),(-2L),0x8CL,0x92L,0x8CL},{0x45L,0x9FL,0x9FL,0x45L,0x10L,0x45L,0x9FL},{0x9BL,0x9BL,0x92L,(-2L),0x92L,0x9BL,0x9BL},{0xFCL,0x9FL,1L,0x9FL,0xFCL,0xFCL,0x9FL},{0xF8L,0x8CL,0xF8L,0x92L,0x92L,0xF8L,0x8CL}}};
    int64_t l_1981[3][2];
    int32_t *l_1982 = &l_1741;
    int16_t * volatile ** volatile * volatile l_1985 = &g_1984;/* VOLATILE GLOBAL l_1985 */
    int8_t l_1986 = (-1L);
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1710[i] = 4L;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
            l_1981[i][j] = 0L;
    }
    for (g_1409.f3 = 0; (g_1409.f3 <= 9); g_1409.f3 += 1)
    { /* block id: 724 */
        int32_t l_1677 = 0x4455ECC1L;
        int32_t l_1692 = 1L;
        int32_t l_1694 = 0xF4CAB37CL;
        int32_t l_1696 = 0xF00D1C51L;
        int32_t l_1698[1][8] = {{7L,7L,7L,7L,7L,7L,7L,7L}};
        int32_t l_1711[6][9][4] = {{{(-2L),(-9L),(-1L),(-1L)},{(-1L),(-9L),0L,0x4570B6F4L},{(-9L),(-9L),(-9L),(-1L)},{(-10L),0L,0x70217CA5L,(-2L)},{(-5L),(-10L),(-1L),0L},{0x4570B6F4L,0x7AA58F01L,(-1L),0x8AC0AB97L},{(-5L),0x70217CA5L,0x70217CA5L,(-5L)},{(-10L),0x4570B6F4L,(-9L),1L},{(-9L),1L,0L,0x7AA58F01L}},{{(-1L),0x3AF80600L,(-1L),0x7AA58F01L},{(-2L),1L,(-1L),1L},{0x0A6AF0B7L,0x4570B6F4L,(-9L),(-5L)},{0L,0x70217CA5L,(-2L),0x8AC0AB97L},{0x3AF80600L,0x7AA58F01L,0x8AC0AB97L,0L},{0x3AF80600L,(-10L),(-2L),(-2L)},{0L,0L,(-9L),(-1L)},{0x0A6AF0B7L,(-9L),(-1L),0x4570B6F4L},{(-2L),(-9L),(-1L),(-1L)}},{{(-1L),(-9L),0L,0x4570B6F4L},{(-9L),(-9L),(-9L),(-1L)},{(-10L),0L,0x70217CA5L,(-2L)},{(-5L),(-10L),(-1L),0L},{0x4570B6F4L,0x7AA58F01L,(-1L),0x8AC0AB97L},{0x0A6AF0B7L,(-1L),(-1L),0x0A6AF0B7L},{0x70217CA5L,(-5L),0x1AF11F6EL,0L},{0x1AF11F6EL,0L,(-1L),0x3AF80600L},{0x4570B6F4L,1L,0x8AC0AB97L,0x3AF80600L}},{{(-9L),0L,0x4570B6F4L,0L},{(-1L),(-5L),0x7AA58F01L,0x0A6AF0B7L},{(-1L),(-1L),(-9L),(-2L)},{1L,0x3AF80600L,(-2L),(-1L)},{1L,0x70217CA5L,(-9L),(-9L)},{(-1L),(-1L),0x7AA58F01L,(-10L)},{(-1L),0x7AA58F01L,0x4570B6F4L,(-5L)},{(-9L),0x1AF11F6EL,0x8AC0AB97L,0x4570B6F4L},{0x4570B6F4L,0x1AF11F6EL,(-1L),(-5L)}},{{0x1AF11F6EL,0x7AA58F01L,0x1AF11F6EL,(-10L)},{0x70217CA5L,(-1L),(-1L),(-9L)},{0x0A6AF0B7L,0x70217CA5L,(-10L),(-1L)},{(-5L),0x3AF80600L,(-10L),(-2L)},{0x0A6AF0B7L,(-1L),(-1L),0x0A6AF0B7L},{0x70217CA5L,(-5L),0x1AF11F6EL,0L},{0x1AF11F6EL,0L,(-1L),0x3AF80600L},{0x4570B6F4L,1L,0x8AC0AB97L,0x3AF80600L},{(-9L),0L,0x4570B6F4L,0L}},{{(-1L),(-5L),0x7AA58F01L,0x0A6AF0B7L},{(-1L),(-1L),(-9L),(-2L)},{1L,0x3AF80600L,(-2L),(-1L)},{1L,0x70217CA5L,(-9L),(-9L)},{(-1L),(-1L),0x7AA58F01L,(-10L)},{(-1L),0x7AA58F01L,0x4570B6F4L,(-5L)},{(-9L),0x1AF11F6EL,0x8AC0AB97L,0x4570B6F4L},{0x4570B6F4L,0x1AF11F6EL,(-1L),(-5L)},{0x1AF11F6EL,0x7AA58F01L,0x1AF11F6EL,(-10L)}}};
        int32_t l_1716 = 0x012029BBL;
        uint8_t *l_1761 = &g_1409.f4;
        uint8_t **l_1760 = &l_1761;
        const int32_t *l_1766 = &l_1693;
        int32_t **l_1781 = &g_483;
        struct S0 **l_1871 = &g_1539;
        uint16_t l_1907 = 0x3129L;
        int i, j, k;
        for (g_160 = 2; (g_160 <= 9); g_160 += 1)
        { /* block id: 727 */
            int32_t l_1676 = 0xA7D8CC9CL;
            int32_t l_1690[9][8][3] = {{{0xDDDADDD2L,(-4L),(-6L)},{4L,0xCD87FBB4L,4L},{0xDDDADDD2L,0L,(-1L)},{4L,0xEC65071AL,0L},{0xDDDADDD2L,9L,0xDDDADDD2L},{4L,0x9CC7894FL,1L},{0xDDDADDD2L,(-4L),(-6L)},{4L,0xCD87FBB4L,4L}},{{0xDDDADDD2L,0L,(-1L)},{4L,0xEC65071AL,0L},{0xDDDADDD2L,9L,0xDDDADDD2L},{4L,0x9CC7894FL,1L},{0xDDDADDD2L,(-4L),(-6L)},{4L,0xCD87FBB4L,4L},{0xDDDADDD2L,0L,(-1L)},{4L,0xEC65071AL,0L}},{{0xDDDADDD2L,9L,0xDDDADDD2L},{4L,0x9CC7894FL,1L},{0xDDDADDD2L,(-4L),(-6L)},{4L,0xCD87FBB4L,4L},{0xDDDADDD2L,0L,(-1L)},{4L,0xEC65071AL,0L},{0xDDDADDD2L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L}},{{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L},{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L},{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L},{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L}},{{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L},{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L},{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L},{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L}},{{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L},{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L},{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L},{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L}},{{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L},{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L},{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L},{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L}},{{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L},{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L},{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L},{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L}},{{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L},{0x540E38A4L,(-6L),1L},{0x305DB198L,0L,0x305DB198L},{0x540E38A4L,0xDDDADDD2L,(-1L)},{0x305DB198L,0xD0249810L,7L},{0x540E38A4L,(-1L),0x540E38A4L},{0x305DB198L,1L,1L}}};
            int8_t l_1712[9][5] = {{(-1L),(-1L),0x9FL,0x9FL,(-1L)},{(-1L),8L,(-1L),0x8FL,0L},{0xE4L,8L,0x8FL,(-10L),9L},{0x30L,(-1L),(-1L),0x30L,(-10L)},{0xE4L,0x9FL,0L,0x42L,(-10L)},{(-1L),0xE4L,9L,(-1L),9L},{(-1L),(-1L),(-10L),0x42L,0L},{0x6AL,(-3L),(-10L),0x30L,(-1L)},{0x8FL,0xE4L,0x6AL,0xE4L,0x9FL}};
            const int32_t *l_1764[4] = {&g_1765,&g_1765,&g_1765,&g_1765};
            int8_t *l_1772[10];
            int8_t **l_1771 = &l_1772[5];
            int8_t ***l_1770 = &l_1771;
            struct S0 * const *l_1779 = (void*)0;
            int8_t l_1794 = 0xD0L;
            int32_t l_1795 = 0xE6C4B004L;
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_1772[i] = &g_1409.f3;
            for (g_1643 = 7; (g_1643 >= 0); g_1643 -= 1)
            { /* block id: 730 */
                int32_t l_1688 = (-1L);
                int32_t l_1689[10][1][8] = {{{(-1L),0x74E21506L,0x74E21506L,(-1L),0x81C2C180L,0x4B476EDEL,0xE37301D5L,0L}},{{(-9L),(-1L),0x73D1C725L,0xE37301D5L,0x73D1C725L,(-1L),(-9L),0xBBE3C8E0L}},{{0x73D1C725L,(-1L),(-9L),0xBBE3C8E0L,0x4B476EDEL,0x4B476EDEL,0xBBE3C8E0L,(-9L)}},{{0x74E21506L,0x74E21506L,(-1L),0x81C2C180L,0x4B476EDEL,0xE37301D5L,0L,0xE37301D5L}},{{0x73D1C725L,(-9L),0x81C2C180L,(-9L),0x73D1C725L,3L,0x74E21506L,0xE37301D5L}},{{(-9L),0x4B476EDEL,0L,0x81C2C180L,0x81C2C180L,0L,0x4B476EDEL,(-9L)}},{{(-1L),3L,0L,0xBBE3C8E0L,0x74E21506L,0x73D1C725L,0x74E21506L,0xBBE3C8E0L}},{{0x81C2C180L,0x98C7DABAL,0x81C2C180L,0xE37301D5L,0xBBE3C8E0L,0x73D1C725L,0L,0L}},{{0L,3L,(-1L),(-1L),3L,0L,0xBBE3C8E0L,0x74E21506L}},{{0L,0x4B476EDEL,(-9L),3L,0xBBE3C8E0L,3L,(-9L),0x4B476EDEL}}};
                uint64_t l_1713 = 0x546AF6FDC39DB7F0LL;
                int8_t l_1742 = (-7L);
                int i, j, k;
                if (l_1668)
                    break;
                for (g_1442.f7 = 0; (g_1442.f7 <= 2); g_1442.f7 += 1)
                { /* block id: 734 */
                    uint16_t *l_1670[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    uint32_t *l_1673 = &g_176;
                    int16_t ***l_1679[10][10][2] = {{{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,(void*)0},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,(void*)0},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,(void*)0},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,(void*)0},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,(void*)0},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}},{{&g_275,&g_275},{&g_275,(void*)0},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275},{&g_275,&g_275}}};
                    int16_t ****l_1678 = &l_1679[6][1][1];
                    int16_t *****l_1680 = &l_1678;
                    int32_t l_1697 = 0L;
                    int32_t l_1699 = 0x27BAF707L;
                    int32_t l_1700 = 1L;
                    int32_t l_1704 = 0xC4C349F2L;
                    int32_t l_1705 = 0xC458AB9DL;
                    int32_t l_1707[9][2] = {{0L,(-1L)},{(-1L),(-1L)},{0L,(-1L)},{0x903422C1L,0x903422C1L},{0x903422C1L,(-1L)},{0L,(-1L)},{(-1L),(-1L)},{0L,(-1L)},{0x903422C1L,0x903422C1L}};
                    int i, j, k;
                    (*l_1680) = (((((((*l_1673) |= (((g_1669 , l_1670[4]) == (void*)0) | (g_207[3] = (safe_rshift_func_uint8_t_u_s(1UL, 2))))) && (((((0x6B59L < ((safe_mul_func_int8_t_s_s(0x65L, g_452[g_1442.f7][(g_1442.f7 + 3)][(g_1442.f7 + 2)])) | (0x1A86CD58B2B08E3FLL && (*g_1252)))) != l_1668) > g_452[g_1442.f7][(g_1442.f7 + 3)][(g_1442.f7 + 2)]) , 6L) > 0x9A5AL)) != l_1676) <= g_452[g_1442.f7][(g_1442.f7 + 3)][(g_1442.f7 + 2)]) , l_1677) , l_1678);
                    if (g_452[g_1442.f7][(g_1442.f7 + 3)][(g_1442.f7 + 2)])
                        continue;
                    for (g_1409.f6 = 0; (g_1409.f6 <= 4); g_1409.f6 += 1)
                    { /* block id: 741 */
                        int32_t *l_1681 = &g_25[0][1];
                        int32_t *l_1682 = &g_207[1];
                        int32_t *l_1683 = &g_207[1];
                        int32_t *l_1684 = &g_207[1];
                        int32_t *l_1685 = &g_850[2];
                        int32_t *l_1686[6] = {&g_850[7],&g_850[7],&g_850[7],&g_850[7],&g_850[7],&g_850[7]};
                        uint32_t *l_1732 = &g_1540.f6;
                        int i;
                        l_1713++;
                        (*g_1318) = (l_1716 , (*g_1561));
                        (*l_1682) = (*l_1682);
                        (*l_1681) |= (l_1692 = (((0x4.47D712p+57 >= (safe_mul_func_float_f_f((((((*l_1732) = ((((~l_1677) , &g_275) != (((-1L) < ((safe_rshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_u(((*g_1642) == ((*g_1395) ^= l_1699)), ((((safe_rshift_func_uint16_t_u_s((((l_1726 != ((*l_1678) = &g_275)) || (safe_div_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((*l_1685), (*l_1682))), l_1698[0][4]))) == g_1731), (*l_1682))) == 0x71B38355L) >= 0xD594959E2173F9FCLL) >= l_1694))), l_1676)) , l_1710[1])) , &g_275)) | 6UL)) , g_18[(g_1409.f6 + 2)]) >= 0xC.5B2DFDp-15) <= g_18[(g_1442.f7 + 3)]), g_18[(g_1442.f7 + 4)]))) , l_1677) <= 0xB0L));
                    }
                }
                for (l_1676 = 0; (l_1676 <= 9); l_1676 += 1)
                { /* block id: 754 */
                    uint16_t l_1733 = 0x2285L;
                    int32_t l_1740[6];
                    uint8_t **l_1759 = (void*)0;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_1740[i] = (-4L);
                    l_1733 |= 0x69960D6CL;
                    for (g_777 = 0; (g_777 <= 4); g_777 += 1)
                    { /* block id: 758 */
                        int32_t *l_1734 = &l_1708;
                        int32_t *l_1735 = (void*)0;
                        int32_t *l_1736 = (void*)0;
                        int32_t *l_1737 = &l_1689[2][0][2];
                        int32_t *l_1738[2];
                        uint64_t l_1743[2][4] = {{0UL,18446744073709551613UL,18446744073709551613UL,0UL},{18446744073709551613UL,0UL,18446744073709551613UL,18446744073709551613UL}};
                        const int32_t **l_1763[3];
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_1738[i] = &g_25[0][3];
                        for (i = 0; i < 3; i++)
                            l_1763[i] = &g_1257;
                        l_1743[0][1]++;
                        (*g_1762) = ((g_119.f1 , (((void*)0 != (*g_275)) != ((safe_div_func_float_f_f((g_18[g_1643] = (safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_mul_func_float_f_f(l_1754, (safe_div_func_float_f_f((((+((*g_1395) ^= ((((((+l_1690[2][5][2]) , l_1759) == l_1760) & (g_841.f7 = ((void*)0 == &g_690))) >= g_1324.f0) && l_1690[7][3][2]))) , l_1698[0][1]) > g_850[8]), l_1740[5])))), g_202[0])), l_1701))), l_1710[1])) >= 0x9.0p+1))) <= l_1716);
                        l_1766 = (l_1764[1] = (l_1668 , &l_1695));
                    }
                    (*g_1768) = l_1767;
                }
            }
            (*l_1770) = (l_1668 , (void*)0);
            for (g_1731 = 0; (g_1731 <= 9); g_1731 += 1)
            { /* block id: 773 */
                int32_t * const *l_1780 = &g_483;
                int32_t l_1782[6][3] = {{0x3A89A39AL,0x3A89A39AL,0xCB4171DCL},{0xB28E47A6L,0xB28E47A6L,0xF874E5FCL},{0x3A89A39AL,0x3A89A39AL,0xCB4171DCL},{0xB28E47A6L,0xB28E47A6L,0xF874E5FCL},{0x3A89A39AL,0x3A89A39AL,0xCB4171DCL},{0xB28E47A6L,0xB28E47A6L,0xF874E5FCL}};
                int i, j;
                if ((safe_mod_func_uint8_t_u_u((((safe_mul_func_uint16_t_u_u((((safe_sub_func_int32_t_s_s(l_1668, (&g_1539 == l_1779))) && 0L) || 65533UL), 0xCB9BL)) != (((*g_892) , l_1780) == l_1781)) <= l_1782[2][2]), l_1782[2][2])))
                { /* block id: 774 */
                    const int32_t *l_1784[6] = {&l_1782[1][2],&l_1782[1][2],(void*)0,&l_1782[1][2],&l_1782[1][2],(void*)0};
                    int i;
                    for (g_841.f7 = 0; (g_841.f7 <= 2); g_841.f7 += 1)
                    { /* block id: 777 */
                        int32_t *l_1783 = &g_207[1];
                        (*l_1783) ^= l_1706;
                    }
                    for (g_426 = 0; (g_426 <= 2); g_426 += 1)
                    { /* block id: 782 */
                        int32_t l_1786[2][9][9] = {{{0L,0x29A38311L,(-9L),0x4E02F421L,6L,(-1L),(-6L),0xEF9D9D63L,(-4L)},{0x73D1FBC9L,0x7980FF03L,0xEF9D9D63L,1L,0x13D52859L,1L,0xEF9D9D63L,0x7980FF03L,0x73D1FBC9L},{6L,0x29A38311L,0xB1FF39BBL,0x13D52859L,(-6L),1L,0xBB9699FDL,(-9L),0x7980FF03L},{0x29A38311L,1L,6L,0xE2DE29A7L,0xE2DE29A7L,6L,0x7980FF03L,0x4E02F421L,0L},{0xBB9699FDL,0x73D1FBC9L,0xEF9D9D63L,0xB1FF39BBL,0xE2DE29A7L,(-1L),(-9L),(-6L),(-6L)},{0x29A38311L,(-4L),0x73D1FBC9L,0x4E02F421L,0x73D1FBC9L,(-4L),0x29A38311L,(-1L),0L},{(-1L),1L,0x29A38311L,0x4E02F421L,(-6L),0xE2DE29A7L,0x13D52859L,0x73D1FBC9L,0x13D52859L},{0x13D52859L,0xBB9699FDL,0xB1FF39BBL,0xB1FF39BBL,0xBB9699FDL,0x13D52859L,0L,(-1L),0x29A38311L},{0x73D1FBC9L,0xEF9D9D63L,0xB1FF39BBL,0xE2DE29A7L,(-1L),(-9L),(-6L),(-6L),(-9L)}},{{0xB1FF39BBL,(-6L),0x29A38311L,(-6L),0xB1FF39BBL,0x73D1FBC9L,0L,0x4E02F421L,0x7980FF03L},{(-4L),(-6L),0x73D1FBC9L,0x7980FF03L,0xEF9D9D63L,1L,0x13D52859L,1L,0xEF9D9D63L},{0L,0xEF9D9D63L,0xEF9D9D63L,0L,(-9L),0x73D1FBC9L,0x29A38311L,0x13D52859L,7L},{0L,0xBB9699FDL,6L,0x73D1FBC9L,7L,(-9L),(-9L),7L,0x73D1FBC9L},{(-4L),1L,(-4L),6L,(-9L),0x13D52859L,0x7980FF03L,0xE2DE29A7L,0x73D1FBC9L},{0xB1FF39BBL,(-4L),7L,0x29A38311L,0xEF9D9D63L,0xE2DE29A7L,0xEF9D9D63L,0x29A38311L,7L},{0x73D1FBC9L,0x73D1FBC9L,1L,6L,0xB1FF39BBL,(-4L),7L,0x29A38311L,0xEF9D9D63L},{0x13D52859L,0x7980FF03L,0xE2DE29A7L,0x73D1FBC9L,(-1L),(-1L),0x73D1FBC9L,0xE2DE29A7L,0x7980FF03L},{(-1L),0x4E02F421L,1L,0L,0xBB9699FDL,6L,0x73D1FBC9L,7L,(-9L)}}};
                        const int32_t **l_1788 = &g_1257;
                        int i, j, k;
                        (*g_1785) = l_1784[1];
                        l_1786[1][4][6] = 0x8.4C8A11p-36;
                        (*l_1788) = (*g_1785);
                    }
                }
                else
                { /* block id: 787 */
                    int8_t **l_1789 = &l_1772[5];
                    int32_t *l_1790[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_1790[i] = &l_1693;
                    l_1791 ^= (l_1782[2][2] && ((void*)0 != l_1789));
                    (*g_892) ^= (((*l_1761)--) && (-5L));
                    (*g_1318) = (*g_348);
                }
                if (l_1794)
                    continue;
            }
            for (g_1456 = 1; (g_1456 <= 9); g_1456 += 1)
            { /* block id: 797 */
                uint32_t l_1816 = 0x33140CA0L;
                for (l_1703 = 9; (l_1703 >= 0); l_1703 -= 1)
                { /* block id: 800 */
                    int16_t l_1806 = (-1L);
                    int32_t l_1811 = 0L;
                    int32_t l_1815 = (-7L);
                    if (l_1795)
                    { /* block id: 801 */
                        int32_t *l_1796 = &l_1795;
                        l_1766 = l_1796;
                        return (*g_690);
                    }
                    else
                    { /* block id: 804 */
                        int64_t l_1797 = 0xCA5BF33C82301F23LL;
                        int32_t *l_1798 = &l_1741;
                        int32_t l_1799 = 0L;
                        int32_t *l_1800 = &l_1693;
                        int32_t *l_1801 = (void*)0;
                        int32_t l_1802 = 2L;
                        int32_t *l_1803 = &l_1694;
                        int32_t *l_1804 = &g_850[2];
                        int32_t *l_1805 = &l_1691;
                        int32_t *l_1807 = &l_1696;
                        int32_t *l_1808 = &l_1791;
                        int32_t *l_1809 = (void*)0;
                        int32_t *l_1810 = (void*)0;
                        int32_t l_1812 = 0x37E1F58CL;
                        int32_t *l_1813 = &l_1706;
                        int32_t *l_1814[8][8] = {{&g_160,&l_1691,&l_1691,&g_160,&g_850[8],&l_1701,(void*)0,&g_850[1]},{&l_1691,&l_1691,&g_346,&g_160,&g_850[1],&l_1701,&l_1701,&g_850[1]},{&g_160,&l_1691,&l_1691,&g_160,&g_850[8],&l_1701,(void*)0,&g_850[1]},{&l_1691,&l_1691,&g_346,&g_160,&g_850[1],&l_1701,&l_1701,&g_850[1]},{&g_160,&l_1691,&l_1691,&g_160,&g_850[8],&l_1701,(void*)0,&g_850[1]},{&l_1691,&l_1691,&g_346,&g_160,&g_850[1],&l_1701,&l_1701,&g_850[1]},{&g_160,&l_1691,&l_1691,&g_160,&g_850[8],&l_1701,(void*)0,&g_850[1]},{&l_1691,&l_1691,&g_346,&g_160,&g_850[1],&l_1701,&l_1701,&g_850[1]}};
                        int i, j;
                        ++l_1816;
                        return l_1811;
                    }
                }
            }
        }
        for (g_346 = 9; (g_346 >= 0); g_346 -= 1)
        { /* block id: 813 */
            float l_1819 = 0xF.9F574Bp-18;
            int32_t l_1828 = 3L;
            int32_t *l_1878 = &l_1716;
            int8_t *l_1888 = (void*)0;
            int32_t l_1954 = 0x664670C2L;
            int32_t l_1955[6];
            int32_t l_1979 = 0L;
            int i;
            for (i = 0; i < 6; i++)
                l_1955[i] = 0x7C17A596L;
            l_1819 = 0xC.9EFD9Fp+28;
            if (l_1739)
            { /* block id: 815 */
                int32_t *l_1820[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_1820[i] = &l_1741;
                --g_1821[0][0][0];
            }
            else
            { /* block id: 817 */
                int16_t l_1829 = 1L;
                struct S0 **l_1870[7] = {&g_1539,&g_1539,&g_1539,&g_1539,&g_1539,&g_1539,&g_1539};
                int32_t *l_1879[5];
                int32_t *l_1881 = &l_1702;
                struct S1 l_1906 = {-1192,1921,32,350};
                int32_t l_1952 = 0xE5B92606L;
                int32_t l_1956 = 0x62446864L;
                int32_t l_1957 = 0x9FD6A0E2L;
                int32_t l_1958 = 1L;
                int32_t l_1959 = 1L;
                int32_t l_1960[5] = {(-3L),(-3L),(-3L),(-3L),(-3L)};
                int i;
                for (i = 0; i < 5; i++)
                    l_1879[i] = (void*)0;
                if ((safe_sub_func_uint32_t_u_u((l_1695 ^ (safe_add_func_uint16_t_u_u((l_1828 |= 0UL), (((*g_1642) |= (l_1829 ^ g_1441.f1)) == (((safe_rshift_func_int16_t_s_s((g_777 = l_1829), ((g_841.f7 <= l_1706) | 0xC1L))) , ((l_1693 || 1L) ^ 6L)) < 0x24L))))), 0xB4341758L)))
                { /* block id: 821 */
                    int32_t l_1832 = 0L;
                    struct S1 **l_1833 = &g_1206[1];
                    float *l_1875 = &g_1540.f2;
                    float *l_1876[6] = {&g_48[0][3],&g_48[0][3],&g_48[0][3],&g_48[0][3],&g_48[0][3],&g_48[0][3]};
                    int32_t *l_1877 = &l_1716;
                    int32_t l_1899 = 0x6FB4E491L;
                    int i;
                    if (l_1829)
                    { /* block id: 822 */
                        l_1677 &= l_1832;
                    }
                    else
                    { /* block id: 824 */
                        struct S1 ***l_1834[6];
                        int32_t l_1847 = 0L;
                        int8_t l_1848 = 0xDFL;
                        int32_t *l_1849 = &l_1828;
                        int8_t *l_1866 = (void*)0;
                        int8_t *l_1867 = &g_217;
                        uint32_t *l_1872 = &g_855;
                        int i;
                        for (i = 0; i < 6; i++)
                            l_1834[i] = &l_1767;
                        (*g_1318) = (void*)0;
                        (*g_1768) = l_1833;
                        (*l_1849) = (l_1829 | (((safe_mod_func_int64_t_s_s((*g_1642), ((((g_1324.f3 && (-1L)) & (safe_add_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_u((l_1668 | (l_1701 == (-6L))), 1)), (safe_lshift_func_uint16_t_u_s((((safe_rshift_func_uint16_t_u_u((*g_690), 5)) & g_1731) , l_1847), 12))))) <= l_1832) & l_1848))) , 65527UL) , 65531UL));
                        (*l_1849) ^= ((safe_sub_func_uint8_t_u_u((safe_unary_minus_func_int64_t_s(((!(safe_add_func_int64_t_s_s((g_1856 , ((*g_1642) = (((l_1741 && ((0x5C9F6486L && ((*l_1872) = ((safe_sub_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((safe_add_func_int16_t_s_s((~l_1832), ((!(~0x4EL)) ^ ((((((*l_1867) = g_1540.f3) < ((*l_1761)--)) , l_1870[2]) != l_1871) , 0x85513400L)))), l_1695)), l_1832)) , 0x7E1AF17AL))) | g_688.f0)) , 0xD8L) < 0x25L))), 0x31E96A8EB09CFBD7LL))) && (*g_1395)))), l_1703)) , l_1693);
                    }
                    if (l_1829)
                        break;
                    (*g_1650) = (safe_sub_func_float_f_f(((*g_662) = ((*g_1143) <= ((*l_1875) = (&g_1395 == ((&g_263 != &g_263) , &g_1395))))), ((((void*)0 == &g_263) , (l_1878 = ((*l_1781) = l_1877))) != l_1879[4])));
                    for (g_1540.f6 = 0; (g_1540.f6 <= 2); g_1540.f6 += 1)
                    { /* block id: 842 */
                        int32_t *l_1880 = &l_1709;
                        const int64_t *l_1885 = &g_1886[1];
                        const int64_t **l_1884 = &l_1885;
                        int8_t **l_1887[5];
                        int32_t *l_1898[10][3] = {{&l_1828,&l_1828,&l_1695},{&l_1828,(void*)0,&l_1698[0][4]},{&g_25[0][1],&l_1692,&g_25[0][1]},{&l_1698[0][4],(void*)0,&l_1828},{&l_1695,&l_1828,&l_1828},{(void*)0,&l_1695,&g_25[0][1]},{(void*)0,(void*)0,&l_1698[0][4]},{(void*)0,&l_1698[0][4],&l_1695},{&l_1695,&l_1698[0][4],(void*)0},{&l_1698[0][4],(void*)0,(void*)0}};
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                            l_1887[i] = (void*)0;
                        l_1881 = l_1880;
                        (*g_1318) = l_1880;
                        (**g_1318) = ((g_452[g_1540.f6][g_1540.f6][(g_1540.f6 + 2)] | (safe_sub_func_uint64_t_u_u(((&g_1643 == ((*l_1884) = &g_1643)) ^ l_1832), ((((&l_1739 != (l_1888 = l_1761)) <= (safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint8_t_u_u(l_1739, (safe_mod_func_uint8_t_u_u((l_1687 , (safe_div_func_uint16_t_u_u((((*l_1766) > 0x2DL) <= (**g_689)), 6L))), l_1701)))), 12))) , (*l_1881)) == g_1241[1][0][5].f6)))) > (*l_1766));
                        l_1899 = ((*l_1880) = l_1832);
                    }
                }
                else
                { /* block id: 851 */
                    int8_t l_1923 = 0L;
                    int32_t l_1943 = (-1L);
                    int32_t l_1944 = 3L;
                    int32_t l_1953 = 0L;
                    const uint16_t l_1972 = 0x7A86L;
                    if ((safe_rshift_func_uint16_t_u_u(((*g_690) , (safe_mul_func_uint8_t_u_u(((safe_mod_func_uint64_t_u_u(((((l_1906 , (*g_1254)) == (void*)0) ^ ((*g_1395) | l_1907)) >= (safe_add_func_uint64_t_u_u((*g_1395), (((safe_rshift_func_uint8_t_u_s((g_1912 , (safe_rshift_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((safe_div_func_uint32_t_u_u((safe_add_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_s((**g_689), 14)) > (*g_690)), (*g_1395))), l_1668)), l_1923)), 7))), 4)) | g_1856.f4) < 0x8B4C3D32L)))), l_1923)) && l_1741), l_1923))), 11)))
                    { /* block id: 852 */
                        uint8_t ** const *l_1934 = (void*)0;
                        int8_t *l_1935 = &l_1923;
                        int32_t l_1940 = 2L;
                        uint32_t *l_1941 = &g_1067;
                        float *l_1942[8][7] = {{(void*)0,&g_1442.f2,&g_1540.f2,(void*)0,(void*)0,&g_1540.f2,&g_1442.f2},{&g_1409.f2,&g_1856.f2,&g_48[0][3],(void*)0,(void*)0,&g_48[0][3],&g_1856.f2},{(void*)0,&g_1442.f2,&g_1540.f2,(void*)0,(void*)0,&g_1540.f2,&g_1442.f2},{&g_1409.f2,&g_1856.f2,&g_48[0][3],(void*)0,(void*)0,&g_48[0][3],&g_1856.f2},{(void*)0,&g_1442.f2,&g_1540.f2,(void*)0,(void*)0,&g_1540.f2,&g_1442.f2},{&g_1409.f2,&g_1856.f2,&g_48[0][3],(void*)0,(void*)0,&g_48[0][3],&g_1856.f2},{(void*)0,&g_1442.f2,&g_1540.f2,(void*)0,(void*)0,&g_1540.f2,&g_1442.f2},{&g_1409.f2,&g_1856.f2,&g_48[0][3],(void*)0,(void*)0,&g_48[0][3],&g_1856.f2}};
                        int32_t *l_1945 = &g_850[8];
                        int32_t *l_1946 = &l_1741;
                        int32_t *l_1947 = (void*)0;
                        int32_t *l_1948 = &l_1940;
                        int32_t *l_1949 = (void*)0;
                        int32_t *l_1950 = &g_850[8];
                        int32_t *l_1951[7][10][3] = {{{&l_1692,&g_207[1],&l_1944},{&l_1694,&l_1940,(void*)0},{&l_1692,(void*)0,&g_1731},{&l_1706,&l_1706,&l_1693},{&g_850[8],&l_1694,&g_1731},{&g_346,(void*)0,(void*)0},{&g_207[1],&l_1692,&l_1944},{&l_1694,&g_346,(void*)0},{&l_1694,&g_25[0][2],&g_1731},{&g_25[0][1],&g_25[0][1],&l_1693}},{{&g_25[0][2],&l_1692,&g_1731},{&l_1940,&l_1694,(void*)0},{&g_25[0][3],&g_25[0][3],&l_1944},{(void*)0,&g_207[1],(void*)0},{(void*)0,&g_850[8],&g_1731},{&g_25[0][1],&g_25[0][1],&l_1693},{(void*)0,(void*)0,&g_1731},{&g_207[1],&l_1694,(void*)0},{&l_1692,&g_207[1],&l_1944},{&l_1694,&l_1940,(void*)0}},{{&l_1692,(void*)0,&g_1731},{&l_1706,&l_1706,&l_1693},{&g_850[8],&l_1694,&g_1731},{&g_346,(void*)0,(void*)0},{&g_207[1],&l_1692,&l_1944},{&l_1694,&g_346,(void*)0},{&l_1694,&g_25[0][2],&g_207[1]},{&l_1828,&g_207[4],&g_25[0][1]},{&l_1943,&l_1696,&g_207[1]},{&l_1695,&l_1709,&g_25[0][1]}},{{(void*)0,(void*)0,&l_1692},{&l_1791,&g_25[0][1],&g_25[0][1]},{&l_1943,(void*)0,&g_207[1]},{&g_207[4],&l_1828,&g_25[0][1]},{&l_1696,&l_1943,&g_207[1]},{&g_25[0][1],&l_1701,&g_25[0][1]},{&l_1677,&l_1677,&l_1692},{&l_1701,&l_1695,&g_25[0][1]},{&l_1696,&l_1696,&g_207[1]},{(void*)0,(void*)0,&g_25[0][1]}},{{(void*)0,&l_1943,&g_207[1]},{&l_1692,&l_1791,&g_25[0][1]},{&l_1677,&l_1677,&l_1692},{&l_1709,&l_1692,&g_25[0][1]},{&l_1943,&l_1943,&g_207[1]},{&l_1828,&g_207[4],&g_25[0][1]},{&l_1943,&l_1696,&g_207[1]},{&l_1695,&l_1709,&g_25[0][1]},{(void*)0,(void*)0,&l_1692},{&l_1791,&g_25[0][1],&g_25[0][1]}},{{&l_1943,(void*)0,&g_207[1]},{&g_207[4],&l_1828,&g_25[0][1]},{&l_1696,&l_1943,&g_207[1]},{&g_25[0][1],&l_1701,&g_25[0][1]},{&l_1677,&l_1677,&l_1692},{&l_1701,&l_1695,&g_25[0][1]},{&l_1696,&l_1696,&g_207[1]},{(void*)0,(void*)0,&g_25[0][1]},{(void*)0,&l_1943,&g_207[1]},{&l_1692,&l_1791,&g_25[0][1]}},{{&l_1677,&l_1677,&l_1692},{&l_1709,&l_1692,&g_25[0][1]},{&l_1943,&l_1943,&g_207[1]},{&l_1828,&g_207[4],&g_25[0][1]},{&l_1943,&l_1696,&g_207[1]},{&l_1695,&l_1709,&g_25[0][1]},{(void*)0,(void*)0,&l_1692},{&l_1791,&g_25[0][1],&g_25[0][1]},{&l_1943,(void*)0,&g_207[1]},{&g_207[4],&l_1828,&g_25[0][1]}}};
                        int i, j, k;
                        l_1709 = (*l_1766);
                        l_1828 = ((safe_mul_func_float_f_f((safe_div_func_float_f_f((l_1944 = (l_1943 = (safe_mul_func_float_f_f((safe_div_func_float_f_f(((safe_mul_func_float_f_f(l_1693, ((((*l_1941) = (((((((*l_1935) = (((void*)0 != l_1934) & (*g_690))) ^ (safe_mod_func_uint64_t_u_u((*l_1766), (*l_1881)))) || (safe_mod_func_uint8_t_u_u(((4UL ^ ((&g_690 == (void*)0) != l_1940)) < (*l_1766)), l_1940))) & (*g_690)) != (-2L)) & 0x1B3FL)) , l_1940) > (*l_1766)))) != (-0x9.Bp-1)), (*g_350))), (-0x2.Dp+1))))), (*l_1881))), (-0x9.Fp+1))) >= (-0x1.6p-1));
                        --g_1961;
                        (*l_1948) = ((*l_1945) = (7L != (((l_1964 | (safe_unary_minus_func_uint32_t_u((*l_1766)))) > (safe_mul_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(l_1923, g_1465.f3)), ((safe_sub_func_int32_t_s_s(l_1972, (1L == g_1207[6].f3))) , (l_1879[1] != (void*)0))))) ^ 0x61D7BCDC4A3E840DLL)));
                    }
                    else
                    { /* block id: 862 */
                        int32_t *l_1973 = &l_1710[0];
                        (*g_1561) = &l_1710[1];
                        l_1973 = (*g_348);
                        return (*g_690);
                    }
                }
                for (g_1856.f7 = 0; (g_1856.f7 <= 7); g_1856.f7 += 1)
                { /* block id: 870 */
                    return (*g_690);
                }
                for (g_32 = 0; (g_32 <= 2); g_32 += 1)
                { /* block id: 875 */
                    int i, j, k;
                    (*l_1881) = (safe_add_func_int64_t_s_s((~g_452[g_32][(g_32 + 3)][g_32]), ((safe_rshift_func_int16_t_s_s((*l_1766), l_1979)) <= g_1094.f5)));
                }
                (*l_1881) = 0x58E5683BL;
            }
            return l_1980[2][0][6];
        }
    }
    (*l_1982) &= l_1981[0][1];
    l_1985 = g_1983[5][2][1];
    return l_1986;
}


/* ------------------------------------------ */
/* 
 * reads : g_28 g_118 g_32 g_119 g_25 g_121 g_160 g_176 g_183 g_207 g_217 g_127 g_46 g_263 g_48 g_275 g_202 g_1318 g_841.f3 g_1648 g_690 g_416 g_689 g_1666
 * writes: g_28 g_46 g_121 g_127 g_25 g_48 g_18 g_160 g_176 g_183 g_202 g_207 g_32 g_217 g_270 g_273 g_118.f2 g_416 g_1540.f3 g_1442.f4 g_841.f3 g_1539
 */
static uint16_t * func_75(int16_t * p_76)
{ /* block id: 25 */
    int8_t l_78 = 9L;
    int32_t l_79 = (-3L);
    float *l_104[3];
    uint16_t *l_1653 = (void*)0;
    int i;
    for (i = 0; i < 3; i++)
        l_104[i] = (void*)0;
lbl_82:
    l_79 &= l_78;
    for (g_28 = 0; (g_28 == (-19)); g_28 = safe_sub_func_int32_t_s_s(g_28, 4))
    { /* block id: 29 */
        const uint8_t l_83 = 0xDCL;
        int16_t *l_106[3];
        int32_t l_1667 = (-1L);
        int i;
        for (i = 0; i < 3; i++)
            l_106[i] = &g_28;
        if (g_28)
            goto lbl_82;
        if (g_28)
            break;
        if (l_83)
        { /* block id: 32 */
            uint16_t *l_84[8][8] = {{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0},{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0},{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0},{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0},{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0},{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0},{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0},{&g_32,&g_32,(void*)0,(void*)0,&g_32,&g_32,(void*)0,(void*)0}};
            int i, j;
            return l_84[0][0];
        }
        else
        { /* block id: 34 */
            int32_t l_120 = 5L;
            const int64_t *l_1657 = &g_1643;
            for (l_78 = (-8); (l_78 >= 25); l_78 = safe_add_func_uint8_t_u_u(l_78, 4))
            { /* block id: 37 */
                uint32_t l_92 = 18446744073709551615UL;
                int32_t l_1652 = 1L;
                if (l_79)
                    break;
                for (l_79 = 0; (l_79 > 23); l_79 = safe_add_func_int32_t_s_s(l_79, 5))
                { /* block id: 41 */
                    int32_t *l_89 = &g_25[0][1];
                    int32_t l_90 = 0L;
                    int32_t *l_91[6] = {&l_90,(void*)0,(void*)0,&l_90,(void*)0,(void*)0};
                    int16_t *l_105 = &g_28;
                    int i;
                    l_92--;
                    for (g_46 = 0; (g_46 <= 0); g_46 += 1)
                    { /* block id: 45 */
                        return &g_46;
                    }
                    for (g_46 = 0; (g_46 != 40); g_46++)
                    { /* block id: 50 */
                        int32_t **l_122 = &l_91[0];
                        int8_t *l_1651[1][9];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 9; j++)
                                l_1651[i][j] = &g_1409.f3;
                        }
                        l_1652 &= (+(g_1540.f3 = func_98(l_104[1], l_105, l_106[2], func_107(func_113(g_118, (g_32 | (g_119 , l_120)), ((*l_122) = (g_121 = &g_25[0][3])), g_118), p_76, g_118.f1, l_120, l_120), l_79)));
                        if ((**g_1318))
                            break;
                        return l_1653;
                    }
                }
            }
            for (g_1442.f4 = 17; (g_1442.f4 == 33); ++g_1442.f4)
            { /* block id: 704 */
                int8_t *l_1656 = &g_841.f3;
                int32_t l_1658 = (-1L);
                (*g_1648) = ((((*l_1656) |= 1L) , &g_1643) != l_1657);
                if (l_1658)
                    break;
                if ((l_83 ^ (safe_div_func_int16_t_s_s((safe_mod_func_int16_t_s_s(l_1658, ((*g_690) &= (safe_lshift_func_int8_t_s_s((&l_1653 != (void*)0), 2))))), l_78))))
                { /* block id: 709 */
                    return (*g_689);
                }
                else
                { /* block id: 711 */
                    struct S0 *l_1665 = &g_841;
                    (*g_1666) = l_1665;
                }
                l_1667 = l_79;
            }
            if (l_79)
                break;
            if (l_120)
                continue;
        }
    }
    return (*g_689);
}


/* ------------------------------------------ */
/* 
 * reads : g_121 g_25
 * writes: g_416
 */
static int8_t  func_98(float * p_99, int16_t * p_100, int16_t * p_101, int16_t ** p_102, uint16_t  p_103)
{ /* block id: 228 */
    int32_t l_437 = (-6L);
    int32_t *l_438[4] = {&g_393,&g_393,&g_393,&g_393};
    int32_t l_454 = (-1L);
    int32_t **l_465 = &g_121;
    int32_t l_497[10][2][8] = {{{0L,0x794C1ADDL,0L,0L,0x9FDC198AL,0xCDF76C4CL,1L,0x6D584A5DL},{(-7L),0x9613D825L,0L,2L,(-1L),1L,0x794C1ADDL,0xCDF76C4CL}},{{2L,0x9FDC198AL,1L,0L,0L,1L,0x9FDC198AL,2L},{0L,0xCA7AB26AL,1L,1L,0x794C1ADDL,(-1L),0L,0x59C54C6BL}},{{0L,0L,0L,0x6D584A5DL,1L,(-1L),0x60FA8983L,1L},{0x60FA8983L,0xCA7AB26AL,0x0AC79F11L,0xF57A844EL,0xCA7AB26AL,1L,0x59C54C6BL,0x794C1ADDL}},{{1L,0x9FDC198AL,0x9613D825L,1L,0x59C54C6BL,1L,0x9613D825L,0x9FDC198AL},{0L,0x9613D825L,(-1L),0xCA7AB26AL,0L,0xCDF76C4CL,0x6D584A5DL,0L}},{{0xF57A844EL,0x794C1ADDL,1L,0xF57A844EL,0L,(-1L),0x6D584A5DL,0x60FA8983L},{0x9613D825L,0xF57A844EL,(-1L),0xCDF76C4CL,(-7L),0L,0x9613D825L,0x59C54C6BL}},{{(-7L),0L,0x9613D825L,0x59C54C6BL,0xA1DD7417L,0xA1DD7417L,0x59C54C6BL,0x9613D825L},{0x794C1ADDL,0x794C1ADDL,0x0AC79F11L,0L,0x9FDC198AL,(-1L),0x60FA8983L,0x6D584A5DL}},{{(-1L),2L,0L,0x9613D825L,(-7L),1L,0L,0x6D584A5DL},{2L,0xA1DD7417L,1L,0L,0L,1L,0x9FDC198AL,0x9613D825L}},{{0xF57A844EL,0xCA7AB26AL,1L,0x59C54C6BL,0x794C1ADDL,0x3AA84FFCL,0x794C1ADDL,0x59C54C6BL},{0L,0xB3C11F47L,0L,0xCDF76C4CL,0x59C54C6BL,(-1L),1L,0x60FA8983L}},{{0x60FA8983L,1L,0L,0xF57A844EL,1L,1L,0x59C54C6BL,0L},{0x60FA8983L,0x9FDC198AL,0x0502AB8FL,0xCA7AB26AL,0x59C54C6BL,0x0ED60FAAL,2L,0x9FDC198AL}},{{0L,2L,(-1L),1L,0x794C1ADDL,0xCDF76C4CL,0xCDF76C4CL,0x794C1ADDL},{0xF57A844EL,0L,0L,0xF57A844EL,0L,0xA1DD7417L,0x6D584A5DL,1L}}};
    uint16_t l_505 = 0xFFFDL;
    int8_t l_531 = 0x74L;
    uint16_t l_607[2][5][5] = {{{7UL,7UL,7UL,7UL,7UL},{0xB4F5L,1UL,0xB4F5L,1UL,0xB4F5L},{7UL,7UL,7UL,7UL,7UL},{0xB4F5L,1UL,0xB4F5L,1UL,0xB4F5L},{7UL,7UL,7UL,7UL,7UL}},{{0xB4F5L,1UL,0xB4F5L,1UL,0xB4F5L},{7UL,7UL,7UL,7UL,7UL},{0xB4F5L,1UL,0xB4F5L,1UL,0xB4F5L},{7UL,7UL,7UL,7UL,7UL},{0xB4F5L,1UL,0xB4F5L,1UL,0xB4F5L}}};
    int32_t l_620[2];
    int32_t ***l_663 = &g_482[1];
    uint8_t *l_680 = &g_183;
    uint64_t *l_681 = (void*)0;
    uint64_t *l_682 = &g_426;
    uint32_t *l_683 = &g_176;
    uint8_t *l_684 = &g_452[1][2][1];
    int16_t *l_685 = &g_127;
    struct S1 l_686[4][5] = {{{1736,1941,28,135},{-70,304,125,237},{-70,304,125,237},{1736,1941,28,135},{1641,1034,20,27}},{{1736,1941,28,135},{-63,295,83,197},{1798,465,25,270},{1798,465,25,270},{-63,295,83,197}},{{1641,1034,20,27},{-70,304,125,237},{1798,465,25,270},{-2226,1827,65,193},{-2226,1827,65,193}},{{-70,304,125,237},{1641,1034,20,27},{-70,304,125,237},{1798,465,25,270},{-2226,1827,65,193}}};
    int32_t *l_687 = &l_437;
    int32_t l_700[5][5] = {{(-10L),(-10L),(-10L),(-10L),(-10L)},{(-1L),(-1L),(-1L),(-1L),(-1L)},{(-10L),(-10L),(-10L),(-10L),(-10L)},{(-1L),(-1L),(-1L),(-1L),(-1L)},{(-10L),(-10L),(-10L),(-10L),(-10L)}};
    const int8_t l_758[9] = {4L,4L,4L,4L,4L,4L,4L,4L,4L};
    struct S1 *l_796[7];
    struct S1 **l_795 = &l_796[2];
    const int32_t ***l_820[4];
    const int32_t ****l_819 = &l_820[3];
    float l_857 = 0x2.0p-1;
    int16_t l_858 = 0L;
    int64_t l_891 = 8L;
    uint32_t l_993 = 3UL;
    uint16_t ** const l_1015 = &g_690;
    struct S0 *l_1040 = &g_841;
    uint32_t l_1100 = 0x19F942D6L;
    uint64_t l_1420 = 0x2E1D901697C5DF98LL;
    int64_t l_1545 = 1L;
    uint64_t l_1631 = 0xECC0BFAF371A609DLL;
    int64_t *l_1640 = &l_891;
    int64_t **l_1641 = (void*)0;
    int8_t *l_1644 = &g_1241[1][0][5].f3;
    uint8_t l_1645 = 255UL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_620[i] = 0x31B9F134L;
    for (i = 0; i < 7; i++)
        l_796[i] = &l_686[3][0];
    for (i = 0; i < 4; i++)
        l_820[i] = (void*)0;
    for (g_416 = 0; (g_416 > 50); g_416++)
    { /* block id: 231 */
        int32_t **l_439 = &l_438[2];
        int32_t *l_440 = &g_393;
        int32_t l_443 = 0x31BE45C6L;
        int32_t *l_453[1][6] = {{&g_160,&g_160,&g_160,&g_160,&g_160,&g_160}};
        uint8_t *l_457 = &g_452[0][1][2];
        int32_t ***l_466 = &l_465;
        int8_t *l_467 = &g_217;
        int8_t *l_468 = &g_399;
        uint64_t l_556 = 0xDF20E0BD1B28AD16LL;
        const int32_t *l_568 = (void*)0;
        const int32_t **l_567 = &l_568;
        int64_t l_652[3];
        struct S1 *l_661 = &g_118;
        int i, j;
        for (i = 0; i < 3; i++)
            l_652[i] = 0L;
    }
    return (**l_465);
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_217 g_127 g_183 g_207 g_121 g_46 g_263 g_48 g_176 g_160 g_25 g_275 g_118.f2 g_202 g_119.f3 g_28
 * writes: g_32 g_25 g_207 g_121 g_18 g_48 g_270 g_273 g_118.f2 g_202
 */
static int16_t ** func_107(const struct S1  p_108, const int16_t * p_109, int64_t  p_110, uint32_t  p_111, int8_t  p_112)
{ /* block id: 112 */
    int32_t *l_229 = &g_25[0][1];
    int16_t *l_237 = &g_202[0];
    int32_t l_239[5][8][6] = {{{(-1L),0x67D54B74L,1L,0x83C70057L,0xB8F03CA4L,1L},{0x83C70057L,0xB8F03CA4L,1L,0xA1BA6F03L,0xECB08BCFL,8L},{0xE0F3894CL,0xB8F03CA4L,0x7A3C9018L,0x7A3C9018L,0xB8F03CA4L,0xE0F3894CL},{0x80B8EACDL,0x67D54B74L,0x68137329L,0xEE1CDBD8L,0xBDDAC85EL,0x3EE96B9AL},{0x7A3C9018L,0x55E82829L,0x90AD85E0L,0x658ABCB2L,0xA7DB8A58L,1L},{0x7A3C9018L,0xAF875AC5L,0x658ABCB2L,0xEE1CDBD8L,3L,0xA1BA6F03L},{0x80B8EACDL,0xBDDAC85EL,(-9L),0x7A3C9018L,0L,(-1L)},{0xE0F3894CL,(-1L),1L,0xA1BA6F03L,5L,(-1L)}},{{0x83C70057L,0xA7DB8A58L,(-9L),0x83C70057L,3L,0xA1BA6F03L},{(-1L),0xB04CBC72L,0x658ABCB2L,0x71367456L,0xAF875AC5L,1L},{0x658ABCB2L,0x65F59F45L,0x90AD85E0L,0x3EE96B9AL,0xAF875AC5L,0x3EE96B9AL},{0x68137329L,0xB04CBC72L,0x68137329L,1L,3L,0xE0F3894CL},{0xA1BA6F03L,0xA7DB8A58L,0x7A3C9018L,1L,5L,8L},{0x71367456L,(-1L),1L,1L,0L,1L},{0xA1BA6F03L,0xBDDAC85EL,1L,1L,3L,0x68137329L},{0x68137329L,0xAF875AC5L,0xA1BA6F03L,0x3EE96B9AL,0xA7DB8A58L,1L}},{{0x658ABCB2L,0x55E82829L,(-6L),0x09A50845L,0x658ABCB2L,(-5L)},{0x3A988937L,0x83C70057L,0L,5L,(-9L),0xFF5385E6L},{5L,(-9L),0xFF5385E6L,(-6L),1L,(-10L)},{0x4A21636CL,(-9L),5L,5L,(-9L),0x4A21636CL},{0xD74E9707L,0x83C70057L,(-5L),(-8L),0x658ABCB2L,0x2BD65C10L},{5L,8L,0x018CA5EBL,0xB4AD64C4L,0x90AD85E0L,0xAC475FA8L},{5L,0xA1BA6F03L,0xB4AD64C4L,(-8L),0x68137329L,(-6L)},{0xD74E9707L,0x658ABCB2L,0x3B3AB43EL,5L,0x7A3C9018L,0x3A988937L}},{{0x4A21636CL,0xE0F3894CL,0xAC475FA8L,(-6L),1L,0x3A988937L},{5L,0x90AD85E0L,0x3B3AB43EL,5L,1L,(-6L)},{0x3A988937L,0x3EE96B9AL,0xB4AD64C4L,0x09A50845L,0xA1BA6F03L,0xAC475FA8L},{0xB4AD64C4L,0xEE1CDBD8L,0x018CA5EBL,0x2BD65C10L,0xA1BA6F03L,0x2BD65C10L},{(-5L),0x3EE96B9AL,(-5L),0xFF5385E6L,1L,0x4A21636CL},{(-6L),0x90AD85E0L,5L,0xAC475FA8L,1L,(-10L)},{0x09A50845L,0xE0F3894CL,0xFF5385E6L,0xAC475FA8L,0x7A3C9018L,0xFF5385E6L},{(-6L),0x658ABCB2L,0L,0xFF5385E6L,0x68137329L,(-5L)}},{{(-5L),0xA1BA6F03L,(-6L),0x2BD65C10L,0x90AD85E0L,0L},{0xB4AD64C4L,8L,(-6L),0x09A50845L,0x658ABCB2L,(-5L)},{0x3A988937L,0x83C70057L,0L,5L,(-9L),0xFF5385E6L},{5L,(-9L),0xFF5385E6L,(-6L),1L,(-10L)},{0x4A21636CL,(-9L),5L,5L,(-9L),0x4A21636CL},{0xD74E9707L,0x83C70057L,(-5L),(-8L),0x658ABCB2L,0x2BD65C10L},{5L,8L,0x018CA5EBL,0xB4AD64C4L,0x90AD85E0L,0xAC475FA8L},{5L,0xA1BA6F03L,0xB4AD64C4L,(-8L),0x68137329L,(-6L)}}};
    uint8_t *l_262 = &g_183;
    uint8_t **l_261 = &l_262;
    uint8_t ***l_260 = &l_261;
    uint32_t l_307 = 0UL;
    int32_t l_349 = (-9L);
    struct S1 *l_362 = &g_118;
    struct S1 **l_361[2][3] = {{&l_362,&l_362,&l_362},{&l_362,&l_362,&l_362}};
    int32_t *l_384 = &g_346;
    uint64_t l_432[3][7];
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
            l_432[i][j] = 0UL;
    }
    for (p_110 = 23; (p_110 > (-6)); p_110 = safe_sub_func_int32_t_s_s(p_110, 1))
    { /* block id: 115 */
        int32_t * const l_228 = (void*)0;
        uint16_t *l_233 = &g_32;
        int16_t *l_238[5];
        int32_t *l_246 = &g_207[1];
        int32_t l_304 = (-1L);
        uint8_t **l_323 = &l_262;
        struct S1 l_389 = {1769,1824,113,208};
        uint32_t l_400 = 4294967289UL;
        int32_t l_428 = (-1L);
        int32_t l_429 = (-1L);
        int32_t l_430 = 0x43D81265L;
        int32_t l_431 = (-1L);
        int i;
        for (i = 0; i < 5; i++)
            l_238[i] = &g_127;
        l_229 = l_228;
        if (((*l_246) = (+(((((*g_121) = ((((l_239[3][6][2] &= (safe_rshift_func_uint16_t_u_u((((*l_233)--) > ((safe_unary_minus_func_int32_t_s((l_237 == (l_238[1] = l_233)))) != (g_217 == (l_233 != l_233)))), 13))) & (safe_add_func_int32_t_s_s((((safe_lshift_func_uint16_t_u_s(g_127, p_108.f1)) >= 2UL) <= ((safe_mod_func_uint64_t_u_u(((((-1L) <= p_108.f1) & g_183) , 0xCF8D8C506045916ELL), 1UL)) < 0x92L)), g_207[1]))) == p_108.f0) || 0x349930D5L)) | p_108.f1) , p_108.f2) < g_46))))
        { /* block id: 122 */
            int32_t *l_247 = &g_25[0][0];
            int32_t **l_249[9] = {&g_121,&l_229,&g_121,&g_121,&l_229,&g_121,&g_121,&l_229,&g_121};
            uint8_t *l_251 = (void*)0;
            uint8_t **l_250 = &l_251;
            uint8_t ***l_252 = &l_250;
            int i;
            g_121 = l_247;
            (*l_252) = l_250;
        }
        else
        { /* block id: 125 */
            float *l_253 = (void*)0;
            float *l_254 = &g_18[3];
            float *l_268 = &g_48[0][3];
            float *l_269 = &g_270;
            int32_t l_271 = (-4L);
            float *l_272[9][6] = {{&g_273,(void*)0,&g_273,&g_273,(void*)0,&g_273},{&g_273,(void*)0,&g_273,&g_273,(void*)0,&g_273},{&g_273,(void*)0,&g_273,&g_273,(void*)0,&g_273},{&g_273,(void*)0,&g_273,&g_273,(void*)0,&g_273},{&g_273,(void*)0,&g_273,&g_273,(void*)0,&g_273},{&g_273,(void*)0,&g_273,&g_273,&g_273,&g_273},{&g_273,&g_273,&g_273,&g_273,&g_273,&g_273},{&g_273,&g_273,&g_273,&g_273,&g_273,&g_273},{&g_273,&g_273,&g_273,&g_273,&g_273,&g_273}};
            int16_t **l_274 = &l_238[4];
            int i, j;
            g_273 = (l_271 = (((*l_269) = (((*l_254) = (0x9.7p-1 < p_108.f3)) == ((safe_sub_func_float_f_f(((*l_268) = ((safe_add_func_float_f_f(((!(7L != 0xFDE0CAD51E188A6ELL)) , p_108.f0), ((((((l_260 != g_263) <= (g_48[0][6] <= (safe_add_func_float_f_f(((safe_add_func_float_f_f((-0x10.1p-1), p_110)) <= p_112), 0x0.7p+1)))) >= (-0x2.Ap-1)) < g_176) < g_176) <= 0x1.Ep-1))) > p_112)), 0x0.06BCCAp+34)) < 0x9.Bp-1))) == g_160));
            if ((*g_121))
                break;
            return g_275;
        }
        for (p_112 = 0; (p_112 < 7); p_112++)
        { /* block id: 136 */
            uint8_t *l_295[5] = {&g_183,&g_183,&g_183,&g_183,&g_183};
            const int32_t l_296 = 0xB81BF733L;
            int32_t l_305 = 0x0276F2A3L;
            int32_t l_306 = 6L;
            int32_t *l_310 = &l_239[3][6][0];
            int16_t **l_313[2][4][5] = {{{&l_238[0],(void*)0,(void*)0,&l_238[0],&l_238[0]},{&l_238[1],&l_238[1],&l_238[1],&l_238[1],&l_238[1]},{&l_238[0],&l_238[0],(void*)0,(void*)0,&l_238[0]},{(void*)0,&l_238[1],(void*)0,&l_238[1],(void*)0}},{{&l_238[0],(void*)0,(void*)0,&l_238[0],&l_238[0]},{&l_238[1],&l_238[1],&l_238[1],&l_238[1],&l_238[1]},{&l_238[0],&l_238[0],(void*)0,(void*)0,&l_238[0]},{(void*)0,&l_238[1],(void*)0,&l_238[1],(void*)0}}};
            int64_t l_324 = (-1L);
            uint32_t l_347 = 6UL;
            int i, j, k;
        }
        for (l_304 = 0; (l_304 != 28); l_304++)
        { /* block id: 183 */
            uint32_t *l_368 = (void*)0;
            int32_t l_376[6][3] = {{0x50748642L,1L,1L},{0x4570321CL,0xA75AEB01L,0x4570321CL},{0x50748642L,0x50748642L,1L},{0x8766082EL,0xA75AEB01L,0x8766082EL},{0x50748642L,1L,1L},{0x4570321CL,0xA75AEB01L,0x4570321CL}};
            int32_t l_380 = 0x7A2C516AL;
            int32_t **l_383[7];
            int32_t *l_392[9] = {(void*)0,&g_393,(void*)0,(void*)0,&g_393,(void*)0,(void*)0,&g_393,(void*)0};
            struct S1 l_398 = {1136,1727,52,38};
            int32_t *l_427 = &g_25[0][1];
            int i, j;
            for (i = 0; i < 7; i++)
                l_383[i] = (void*)0;
            l_376[3][0] = (safe_mod_func_int16_t_s_s((~((g_127 == (g_118.f2 ^= p_111)) ^ (((safe_rshift_func_int16_t_s_s((l_380 |= (+(safe_lshift_func_uint16_t_u_u((((safe_add_func_uint16_t_u_u(l_376[2][1], ((((*l_237) ^= ((l_376[0][0] < l_376[5][0]) == ((!(((*g_121) > (1L >= (safe_mod_func_uint16_t_u_u(((((void*)0 == &g_298) || 8L) , 65535UL), 65529UL)))) > p_108.f1)) < l_376[2][0]))) == p_108.f3) != p_108.f2))) && g_119.f3) == g_28), p_108.f1)))), 5)) <= g_207[2]) && l_239[3][6][2]))), p_108.f0));
        }
    }
    return &g_276;
}


/* ------------------------------------------ */
/* 
 * reads : g_25 g_127 g_121 g_119.f1 g_32 g_28 g_119.f4 g_118.f3 g_160 g_118 g_176 g_119.f0 g_183 g_119.f5 g_207
 * writes: g_121 g_127 g_25 g_48 g_18 g_160 g_176 g_183 g_202 g_207 g_32 g_217
 */
static struct S1  func_113(struct S1  p_114, int64_t  p_115, int32_t * p_116, struct S1  p_117)
{ /* block id: 53 */
    int32_t *l_123 = &g_25[0][3];
    int16_t *l_126 = &g_127;
    struct S1 l_225 = {-1536,1461,28,98};
    g_121 = (p_114 , l_123);
    (*p_116) &= (safe_mul_func_int16_t_s_s(((*l_126) = p_114.f3), 0x261DL));
    for (p_115 = 28; (p_115 == 8); --p_115)
    { /* block id: 59 */
        int64_t l_142 = 3L;
        int32_t l_156 = (-1L);
        int32_t l_157 = 0xE5EC6406L;
        int32_t l_185 = (-1L);
        for (g_127 = 0; (g_127 == 29); ++g_127)
        { /* block id: 62 */
            uint16_t l_145 = 8UL;
            uint32_t l_158[7];
            int i;
            for (i = 0; i < 7; i++)
                l_158[i] = 0UL;
            if ((*g_121))
            { /* block id: 63 */
                if ((*g_121))
                    break;
                if ((*l_123))
                    break;
            }
            else
            { /* block id: 66 */
                float *l_148 = &g_48[0][9];
                float *l_155 = &g_18[0];
                int32_t *l_159 = &g_160;
                if ((*g_121))
                    break;
                (*l_159) &= ((((((safe_sub_func_int8_t_s_s((((safe_mul_func_uint16_t_u_u(((p_117 , (l_156 = (safe_div_func_float_f_f((safe_div_func_float_f_f(l_142, (safe_div_func_float_f_f(0x1.8p-1, l_145)))), ((*l_155) = ((safe_add_func_float_f_f(0xC.82FA94p-52, ((*l_148) = (*l_123)))) > (safe_add_func_float_f_f((safe_div_func_float_f_f((((g_119.f1 , (safe_lshift_func_int16_t_s_u(l_142, 14))) < ((*l_123) && p_117.f0)) , g_32), 0xD.330705p-1)), g_28)))))))) , g_119.f4), p_114.f3)) != 0UL) & l_157), l_158[5])) , (void*)0) != (void*)0) == (*p_116)) || 4294967290UL) < g_118.f3);
                (*l_123) = (*g_121);
            }
            for (l_145 = 0; (l_145 <= 7); l_145 += 1)
            { /* block id: 76 */
                int16_t l_184 = 9L;
                for (l_156 = 1; (l_156 <= 7); l_156 += 1)
                { /* block id: 79 */
                    int16_t *l_171 = &g_127;
                    uint32_t *l_175 = &g_176;
                    uint8_t *l_182[1][4][5] = {{{&g_183,&g_183,&g_183,&g_183,&g_183},{(void*)0,(void*)0,&g_183,(void*)0,&g_183},{(void*)0,(void*)0,&g_183,&g_183,(void*)0},{&g_183,(void*)0,&g_183,&g_183,(void*)0}}};
                    int i, j, k;
                    l_185 ^= (safe_sub_func_int64_t_s_s((g_118 , (safe_rshift_func_int16_t_s_u((safe_mod_func_uint64_t_u_u(((safe_mul_func_int16_t_s_s(0xA6E0L, (safe_add_func_int32_t_s_s(((p_116 != p_116) != ((&g_127 != l_171) & ((((~((((((g_183 |= (safe_sub_func_uint32_t_u_u(((*l_175) &= p_114.f3), (((safe_sub_func_uint16_t_u_u((+(safe_lshift_func_uint8_t_u_u(g_119.f0, 4))), (((l_123 != g_121) != (-4L)) , (*l_123)))) , (void*)0) == (void*)0)))) < g_118.f2) < 0L) <= 4UL) , 0xDA1E438FE63D49D5LL) > p_115)) || 0xCAL) != (*l_123)) , (*l_123)))), l_184)))) | p_117.f0), g_28)), 1))), g_118.f3));
                }
            }
        }
        l_123 = &g_25[0][1];
        if (l_157)
            continue;
        for (l_185 = (-2); (l_185 <= 3); l_185++)
        { /* block id: 90 */
            uint32_t l_199 = 18446744073709551615UL;
            for (g_127 = 4; (g_127 != 11); g_127++)
            { /* block id: 93 */
                int16_t *l_200 = (void*)0;
                int16_t *l_201 = &g_202[2];
                uint8_t *l_205 = &g_183;
                int32_t *l_206 = &g_207[1];
                uint16_t *l_212 = (void*)0;
                uint16_t *l_213 = &g_32;
                int8_t *l_216[7][8] = {{(void*)0,&g_217,&g_217,(void*)0,(void*)0,(void*)0,(void*)0,&g_217},{&g_217,&g_217,(void*)0,&g_217,(void*)0,(void*)0,(void*)0,&g_217},{&g_217,&g_217,&g_217,(void*)0,&g_217,&g_217,&g_217,&g_217},{&g_217,&g_217,&g_217,&g_217,(void*)0,&g_217,&g_217,&g_217},{&g_217,(void*)0,(void*)0,(void*)0,&g_217,(void*)0,&g_217,&g_217},{&g_217,(void*)0,(void*)0,(void*)0,(void*)0,&g_217,&g_217,(void*)0},{&g_217,&g_217,(void*)0,&g_217,(void*)0,&g_217,&g_217,&g_217}};
                int16_t **l_218 = &l_126;
                int i, j;
                (*l_206) |= (safe_lshift_func_int16_t_s_s(((~l_156) , ((safe_add_func_int8_t_s_s(((g_119.f5 == (safe_mod_func_int8_t_s_s(((((*g_121) && (*g_121)) != p_117.f3) & (((*l_205) = ((((!(((~((*l_201) = l_199)) >= (safe_rshift_func_uint8_t_u_s((((l_199 , p_116) == (void*)0) >= l_157), 6))) <= g_160)) & 1L) , 0x63L) > p_117.f2)) || 248UL)), g_176))) == 251UL), 255UL)) > 0xC66AC990L)), g_118.f3));
                (*l_206) &= ((*l_123) = ((((p_114 , (safe_div_func_int16_t_s_s((safe_mul_func_int8_t_s_s((&g_207[1] == &g_160), (g_217 = ((++(*l_213)) , 1L)))), g_118.f3))) , ((*l_218) = l_126)) != (void*)0) <= (safe_rshift_func_int8_t_s_u((!(l_199 != p_114.f2)), 1))));
            }
            for (g_176 = 0; (g_176 > 34); g_176 = safe_add_func_int8_t_s_s(g_176, 7))
            { /* block id: 105 */
                struct S1 l_224 = {-2154,1014,22,182};
                return l_224;
            }
        }
    }
    (*l_123) |= (-1L);
    return l_225;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 8; i++)
    {
        transparent_crc_bytes(&g_18[i], sizeof(g_18[i]), "g_18[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_25[i][j], "g_25[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_28, "g_28", print_hash_value);
    transparent_crc(g_32, "g_32", print_hash_value);
    transparent_crc(g_46, "g_46", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc_bytes(&g_48[i][j], sizeof(g_48[i][j]), "g_48[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_118.f0, "g_118.f0", print_hash_value);
    transparent_crc(g_118.f1, "g_118.f1", print_hash_value);
    transparent_crc(g_118.f2, "g_118.f2", print_hash_value);
    transparent_crc(g_118.f3, "g_118.f3", print_hash_value);
    transparent_crc(g_119.f0, "g_119.f0", print_hash_value);
    transparent_crc(g_119.f1, "g_119.f1", print_hash_value);
    transparent_crc_bytes (&g_119.f2, sizeof(g_119.f2), "g_119.f2", print_hash_value);
    transparent_crc(g_119.f3, "g_119.f3", print_hash_value);
    transparent_crc(g_119.f4, "g_119.f4", print_hash_value);
    transparent_crc(g_119.f5, "g_119.f5", print_hash_value);
    transparent_crc(g_119.f6, "g_119.f6", print_hash_value);
    transparent_crc(g_119.f7, "g_119.f7", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_160, "g_160", print_hash_value);
    transparent_crc(g_176, "g_176", print_hash_value);
    transparent_crc(g_183, "g_183", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_202[i], "g_202[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_207[i], "g_207[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_217, "g_217", print_hash_value);
    transparent_crc_bytes (&g_270, sizeof(g_270), "g_270", print_hash_value);
    transparent_crc_bytes (&g_273, sizeof(g_273), "g_273", print_hash_value);
    transparent_crc(g_346, "g_346", print_hash_value);
    transparent_crc(g_393, "g_393", print_hash_value);
    transparent_crc(g_399, "g_399", print_hash_value);
    transparent_crc(g_416, "g_416", print_hash_value);
    transparent_crc(g_426, "g_426", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_452[i][j][k], "g_452[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_585, "g_585", print_hash_value);
    transparent_crc(g_688.f0, "g_688.f0", print_hash_value);
    transparent_crc(g_688.f1, "g_688.f1", print_hash_value);
    transparent_crc_bytes (&g_688.f2, sizeof(g_688.f2), "g_688.f2", print_hash_value);
    transparent_crc(g_688.f3, "g_688.f3", print_hash_value);
    transparent_crc(g_688.f4, "g_688.f4", print_hash_value);
    transparent_crc(g_688.f5, "g_688.f5", print_hash_value);
    transparent_crc(g_688.f6, "g_688.f6", print_hash_value);
    transparent_crc(g_688.f7, "g_688.f7", print_hash_value);
    transparent_crc(g_717, "g_717", print_hash_value);
    transparent_crc(g_761, "g_761", print_hash_value);
    transparent_crc(g_777, "g_777", print_hash_value);
    transparent_crc(g_841.f0, "g_841.f0", print_hash_value);
    transparent_crc(g_841.f1, "g_841.f1", print_hash_value);
    transparent_crc_bytes (&g_841.f2, sizeof(g_841.f2), "g_841.f2", print_hash_value);
    transparent_crc(g_841.f3, "g_841.f3", print_hash_value);
    transparent_crc(g_841.f4, "g_841.f4", print_hash_value);
    transparent_crc(g_841.f5, "g_841.f5", print_hash_value);
    transparent_crc(g_841.f6, "g_841.f6", print_hash_value);
    transparent_crc(g_841.f7, "g_841.f7", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_850[i], "g_850[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_855, "g_855", print_hash_value);
    transparent_crc(g_940, "g_940", print_hash_value);
    transparent_crc(g_1067, "g_1067", print_hash_value);
    transparent_crc(g_1081.f0, "g_1081.f0", print_hash_value);
    transparent_crc(g_1081.f1, "g_1081.f1", print_hash_value);
    transparent_crc_bytes (&g_1081.f2, sizeof(g_1081.f2), "g_1081.f2", print_hash_value);
    transparent_crc(g_1081.f3, "g_1081.f3", print_hash_value);
    transparent_crc(g_1081.f4, "g_1081.f4", print_hash_value);
    transparent_crc(g_1081.f5, "g_1081.f5", print_hash_value);
    transparent_crc(g_1081.f6, "g_1081.f6", print_hash_value);
    transparent_crc(g_1081.f7, "g_1081.f7", print_hash_value);
    transparent_crc(g_1094.f0, "g_1094.f0", print_hash_value);
    transparent_crc(g_1094.f1, "g_1094.f1", print_hash_value);
    transparent_crc_bytes (&g_1094.f2, sizeof(g_1094.f2), "g_1094.f2", print_hash_value);
    transparent_crc(g_1094.f3, "g_1094.f3", print_hash_value);
    transparent_crc(g_1094.f4, "g_1094.f4", print_hash_value);
    transparent_crc(g_1094.f5, "g_1094.f5", print_hash_value);
    transparent_crc(g_1094.f6, "g_1094.f6", print_hash_value);
    transparent_crc(g_1094.f7, "g_1094.f7", print_hash_value);
    transparent_crc(g_1159, "g_1159", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1207[i].f0, "g_1207[i].f0", print_hash_value);
        transparent_crc(g_1207[i].f1, "g_1207[i].f1", print_hash_value);
        transparent_crc(g_1207[i].f2, "g_1207[i].f2", print_hash_value);
        transparent_crc(g_1207[i].f3, "g_1207[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_1241[i][j][k].f0, "g_1241[i][j][k].f0", print_hash_value);
                transparent_crc(g_1241[i][j][k].f1, "g_1241[i][j][k].f1", print_hash_value);
                transparent_crc_bytes(&g_1241[i][j][k].f2, sizeof(g_1241[i][j][k].f2), "g_1241[i][j][k].f2", print_hash_value);
                transparent_crc(g_1241[i][j][k].f3, "g_1241[i][j][k].f3", print_hash_value);
                transparent_crc(g_1241[i][j][k].f4, "g_1241[i][j][k].f4", print_hash_value);
                transparent_crc(g_1241[i][j][k].f5, "g_1241[i][j][k].f5", print_hash_value);
                transparent_crc(g_1241[i][j][k].f6, "g_1241[i][j][k].f6", print_hash_value);
                transparent_crc(g_1241[i][j][k].f7, "g_1241[i][j][k].f7", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1253, "g_1253", print_hash_value);
    transparent_crc(g_1324.f0, "g_1324.f0", print_hash_value);
    transparent_crc(g_1324.f1, "g_1324.f1", print_hash_value);
    transparent_crc_bytes (&g_1324.f2, sizeof(g_1324.f2), "g_1324.f2", print_hash_value);
    transparent_crc(g_1324.f3, "g_1324.f3", print_hash_value);
    transparent_crc(g_1324.f4, "g_1324.f4", print_hash_value);
    transparent_crc(g_1324.f5, "g_1324.f5", print_hash_value);
    transparent_crc(g_1324.f6, "g_1324.f6", print_hash_value);
    transparent_crc(g_1324.f7, "g_1324.f7", print_hash_value);
    transparent_crc(g_1409.f0, "g_1409.f0", print_hash_value);
    transparent_crc(g_1409.f1, "g_1409.f1", print_hash_value);
    transparent_crc_bytes (&g_1409.f2, sizeof(g_1409.f2), "g_1409.f2", print_hash_value);
    transparent_crc(g_1409.f3, "g_1409.f3", print_hash_value);
    transparent_crc(g_1409.f4, "g_1409.f4", print_hash_value);
    transparent_crc(g_1409.f5, "g_1409.f5", print_hash_value);
    transparent_crc(g_1409.f6, "g_1409.f6", print_hash_value);
    transparent_crc(g_1409.f7, "g_1409.f7", print_hash_value);
    transparent_crc(g_1441.f0, "g_1441.f0", print_hash_value);
    transparent_crc(g_1441.f1, "g_1441.f1", print_hash_value);
    transparent_crc_bytes (&g_1441.f2, sizeof(g_1441.f2), "g_1441.f2", print_hash_value);
    transparent_crc(g_1441.f3, "g_1441.f3", print_hash_value);
    transparent_crc(g_1441.f4, "g_1441.f4", print_hash_value);
    transparent_crc(g_1441.f5, "g_1441.f5", print_hash_value);
    transparent_crc(g_1441.f6, "g_1441.f6", print_hash_value);
    transparent_crc(g_1441.f7, "g_1441.f7", print_hash_value);
    transparent_crc(g_1442.f0, "g_1442.f0", print_hash_value);
    transparent_crc(g_1442.f1, "g_1442.f1", print_hash_value);
    transparent_crc_bytes (&g_1442.f2, sizeof(g_1442.f2), "g_1442.f2", print_hash_value);
    transparent_crc(g_1442.f3, "g_1442.f3", print_hash_value);
    transparent_crc(g_1442.f4, "g_1442.f4", print_hash_value);
    transparent_crc(g_1442.f5, "g_1442.f5", print_hash_value);
    transparent_crc(g_1442.f6, "g_1442.f6", print_hash_value);
    transparent_crc(g_1442.f7, "g_1442.f7", print_hash_value);
    transparent_crc(g_1456, "g_1456", print_hash_value);
    transparent_crc(g_1465.f0, "g_1465.f0", print_hash_value);
    transparent_crc(g_1465.f1, "g_1465.f1", print_hash_value);
    transparent_crc_bytes (&g_1465.f2, sizeof(g_1465.f2), "g_1465.f2", print_hash_value);
    transparent_crc(g_1465.f3, "g_1465.f3", print_hash_value);
    transparent_crc(g_1465.f4, "g_1465.f4", print_hash_value);
    transparent_crc(g_1465.f5, "g_1465.f5", print_hash_value);
    transparent_crc(g_1465.f6, "g_1465.f6", print_hash_value);
    transparent_crc(g_1465.f7, "g_1465.f7", print_hash_value);
    transparent_crc(g_1540.f0, "g_1540.f0", print_hash_value);
    transparent_crc(g_1540.f1, "g_1540.f1", print_hash_value);
    transparent_crc_bytes (&g_1540.f2, sizeof(g_1540.f2), "g_1540.f2", print_hash_value);
    transparent_crc(g_1540.f3, "g_1540.f3", print_hash_value);
    transparent_crc(g_1540.f4, "g_1540.f4", print_hash_value);
    transparent_crc(g_1540.f5, "g_1540.f5", print_hash_value);
    transparent_crc(g_1540.f6, "g_1540.f6", print_hash_value);
    transparent_crc(g_1540.f7, "g_1540.f7", print_hash_value);
    transparent_crc(g_1555, "g_1555", print_hash_value);
    transparent_crc(g_1609, "g_1609", print_hash_value);
    transparent_crc(g_1643, "g_1643", print_hash_value);
    transparent_crc(g_1669.f0, "g_1669.f0", print_hash_value);
    transparent_crc(g_1669.f1, "g_1669.f1", print_hash_value);
    transparent_crc_bytes (&g_1669.f2, sizeof(g_1669.f2), "g_1669.f2", print_hash_value);
    transparent_crc(g_1669.f3, "g_1669.f3", print_hash_value);
    transparent_crc(g_1669.f4, "g_1669.f4", print_hash_value);
    transparent_crc(g_1669.f5, "g_1669.f5", print_hash_value);
    transparent_crc(g_1669.f6, "g_1669.f6", print_hash_value);
    transparent_crc(g_1669.f7, "g_1669.f7", print_hash_value);
    transparent_crc(g_1731, "g_1731", print_hash_value);
    transparent_crc(g_1765, "g_1765", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1821[i][j][k], "g_1821[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1856.f0, "g_1856.f0", print_hash_value);
    transparent_crc(g_1856.f1, "g_1856.f1", print_hash_value);
    transparent_crc_bytes (&g_1856.f2, sizeof(g_1856.f2), "g_1856.f2", print_hash_value);
    transparent_crc(g_1856.f3, "g_1856.f3", print_hash_value);
    transparent_crc(g_1856.f4, "g_1856.f4", print_hash_value);
    transparent_crc(g_1856.f5, "g_1856.f5", print_hash_value);
    transparent_crc(g_1856.f6, "g_1856.f6", print_hash_value);
    transparent_crc(g_1856.f7, "g_1856.f7", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1886[i], "g_1886[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1912.f0, "g_1912.f0", print_hash_value);
    transparent_crc(g_1912.f1, "g_1912.f1", print_hash_value);
    transparent_crc_bytes (&g_1912.f2, sizeof(g_1912.f2), "g_1912.f2", print_hash_value);
    transparent_crc(g_1912.f3, "g_1912.f3", print_hash_value);
    transparent_crc(g_1912.f4, "g_1912.f4", print_hash_value);
    transparent_crc(g_1912.f5, "g_1912.f5", print_hash_value);
    transparent_crc(g_1912.f6, "g_1912.f6", print_hash_value);
    transparent_crc(g_1912.f7, "g_1912.f7", print_hash_value);
    transparent_crc(g_1961, "g_1961", print_hash_value);
    transparent_crc(g_2028.f0, "g_2028.f0", print_hash_value);
    transparent_crc(g_2028.f1, "g_2028.f1", print_hash_value);
    transparent_crc(g_2028.f2, "g_2028.f2", print_hash_value);
    transparent_crc(g_2028.f3, "g_2028.f3", print_hash_value);
    transparent_crc(g_2030.f0, "g_2030.f0", print_hash_value);
    transparent_crc(g_2030.f1, "g_2030.f1", print_hash_value);
    transparent_crc(g_2030.f2, "g_2030.f2", print_hash_value);
    transparent_crc(g_2030.f3, "g_2030.f3", print_hash_value);
    transparent_crc(g_2125, "g_2125", print_hash_value);
    transparent_crc(g_2126, "g_2126", print_hash_value);
    transparent_crc(g_2127, "g_2127", print_hash_value);
    transparent_crc(g_2130, "g_2130", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2133[i], "g_2133[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2252, "g_2252", print_hash_value);
    transparent_crc_bytes (&g_2257, sizeof(g_2257), "g_2257", print_hash_value);
    transparent_crc(g_2329, "g_2329", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2351[i], "g_2351[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2362.f0, "g_2362.f0", print_hash_value);
    transparent_crc(g_2362.f1, "g_2362.f1", print_hash_value);
    transparent_crc_bytes (&g_2362.f2, sizeof(g_2362.f2), "g_2362.f2", print_hash_value);
    transparent_crc(g_2362.f3, "g_2362.f3", print_hash_value);
    transparent_crc(g_2362.f4, "g_2362.f4", print_hash_value);
    transparent_crc(g_2362.f5, "g_2362.f5", print_hash_value);
    transparent_crc(g_2362.f6, "g_2362.f6", print_hash_value);
    transparent_crc(g_2362.f7, "g_2362.f7", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2372[i], "g_2372[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2473, "g_2473", print_hash_value);
    transparent_crc(g_2512.f0, "g_2512.f0", print_hash_value);
    transparent_crc(g_2512.f1, "g_2512.f1", print_hash_value);
    transparent_crc_bytes (&g_2512.f2, sizeof(g_2512.f2), "g_2512.f2", print_hash_value);
    transparent_crc(g_2512.f3, "g_2512.f3", print_hash_value);
    transparent_crc(g_2512.f4, "g_2512.f4", print_hash_value);
    transparent_crc(g_2512.f5, "g_2512.f5", print_hash_value);
    transparent_crc(g_2512.f6, "g_2512.f6", print_hash_value);
    transparent_crc(g_2512.f7, "g_2512.f7", print_hash_value);
    transparent_crc(g_2588.f0, "g_2588.f0", print_hash_value);
    transparent_crc(g_2588.f1, "g_2588.f1", print_hash_value);
    transparent_crc_bytes (&g_2588.f2, sizeof(g_2588.f2), "g_2588.f2", print_hash_value);
    transparent_crc(g_2588.f3, "g_2588.f3", print_hash_value);
    transparent_crc(g_2588.f4, "g_2588.f4", print_hash_value);
    transparent_crc(g_2588.f5, "g_2588.f5", print_hash_value);
    transparent_crc(g_2588.f6, "g_2588.f6", print_hash_value);
    transparent_crc(g_2588.f7, "g_2588.f7", print_hash_value);
    transparent_crc_bytes (&g_2593, sizeof(g_2593), "g_2593", print_hash_value);
    transparent_crc(g_2687, "g_2687", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 650
   depth: 1, occurrence: 33
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 5
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 2
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 60
breakdown:
   indirect level: 0, occurrence: 33
   indirect level: 1, occurrence: 14
   indirect level: 2, occurrence: 13
XXX full-bitfields structs in the program: 17
breakdown:
   indirect level: 0, occurrence: 17
XXX times a bitfields struct's address is taken: 192
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 54
XXX times a single bitfield on LHS: 4
XXX times a single bitfield on RHS: 100

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 219
   depth: 2, occurrence: 60
   depth: 3, occurrence: 7
   depth: 4, occurrence: 4
   depth: 7, occurrence: 1
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 3
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 5
   depth: 26, occurrence: 1
   depth: 27, occurrence: 3
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 33, occurrence: 1
   depth: 42, occurrence: 1
   depth: 45, occurrence: 1

XXX total number of pointers: 586

XXX times a variable address is taken: 1897
XXX times a pointer is dereferenced on RHS: 377
breakdown:
   depth: 1, occurrence: 288
   depth: 2, occurrence: 64
   depth: 3, occurrence: 23
   depth: 4, occurrence: 2
XXX times a pointer is dereferenced on LHS: 375
breakdown:
   depth: 1, occurrence: 334
   depth: 2, occurrence: 35
   depth: 3, occurrence: 5
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 48
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 24
XXX times a pointer is qualified to be dereferenced: 9624

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1625
   level: 2, occurrence: 402
   level: 3, occurrence: 110
   level: 4, occurrence: 36
   level: 5, occurrence: 20
XXX number of pointers point to pointers: 210
XXX number of pointers point to scalars: 355
XXX number of pointers point to structs: 21
XXX percent of pointers has null in alias set: 29.4
XXX average alias set size: 1.54

XXX times a non-volatile is read: 2198
XXX times a non-volatile is write: 1074
XXX times a volatile is read: 124
XXX    times read thru a pointer: 16
XXX times a volatile is write: 37
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 6.24e+03
XXX percentage of non-volatile access: 95.3

XXX forward jumps: 1
XXX backward jumps: 4

XXX stmts: 223
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 28
   depth: 1, occurrence: 27
   depth: 2, occurrence: 37
   depth: 3, occurrence: 40
   depth: 4, occurrence: 41
   depth: 5, occurrence: 50

XXX percentage a fresh-made variable is used: 16.2
XXX percentage an existing variable is used: 83.8
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


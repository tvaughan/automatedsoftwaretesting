/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      3621447819
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int8_t g_18[1] = {0xA2L};
static int8_t g_24 = 0xACL;
static int8_t *g_23 = &g_24;
static int64_t g_29 = 0x8FF8CD3BED162C5DLL;
static volatile uint64_t g_62 = 0x656D2926A0939583LL;/* VOLATILE GLOBAL g_62 */
static uint32_t g_78 = 0xB4DFC4B7L;
static int32_t g_82 = (-8L);
static uint8_t g_114 = 247UL;
static uint8_t g_127 = 0x52L;
static uint64_t g_135 = 0x3BAD782A0B0D0ED2LL;
static int16_t g_149 = 0xFDE8L;
static int64_t g_164 = 0x1653A522D215C821LL;
static int32_t g_166 = 0L;
static uint32_t g_169 = 4294967294UL;
static uint8_t *g_190 = &g_127;
static uint8_t ** volatile g_189 = &g_190;/* VOLATILE GLOBAL g_189 */
static uint32_t g_213 = 0x4F80C1E2L;
static int8_t g_218[3] = {0x05L,0x05L,0x05L};
static uint16_t g_242 = 2UL;
static uint16_t g_248 = 0x9316L;
static uint8_t g_251[4][3] = {{247UL,247UL,247UL},{255UL,7UL,255UL},{247UL,247UL,247UL},{255UL,7UL,255UL}};
static int64_t *g_283 = (void*)0;
static int64_t ** volatile g_282[9] = {&g_283,&g_283,&g_283,&g_283,&g_283,&g_283,&g_283,&g_283,&g_283};
static uint32_t *g_289 = &g_213;
static uint32_t ** volatile g_288 = &g_289;/* VOLATILE GLOBAL g_288 */
static uint64_t g_302 = 18446744073709551614UL;
static const uint64_t g_328 = 0x33CB234E90412A0FLL;
static uint64_t *g_330 = &g_302;
static uint64_t * const  volatile *g_329 = &g_330;
static uint64_t * const  volatile ** volatile g_331 = &g_329;/* VOLATILE GLOBAL g_331 */
static int32_t *g_368 = &g_82;
static int32_t ** volatile g_367 = &g_368;/* VOLATILE GLOBAL g_367 */
static volatile uint8_t g_396 = 255UL;/* VOLATILE GLOBAL g_396 */
static volatile uint32_t g_427 = 4294967289UL;/* VOLATILE GLOBAL g_427 */
static int64_t ***g_455 = (void*)0;
static int64_t **g_464 = &g_283;
static int64_t ***g_463 = &g_464;
static int64_t ****g_462[2][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static uint64_t *g_514 = &g_135;
static int32_t * volatile g_539 = &g_166;/* VOLATILE GLOBAL g_539 */
static int32_t g_563 = (-9L);
static int32_t * volatile g_564 = &g_166;/* VOLATILE GLOBAL g_564 */
static int32_t g_584[2] = {1L,1L};
static uint32_t g_602 = 0UL;
static const int64_t g_663 = 0xA62FA618256DCDBCLL;
static int32_t * volatile g_667 = &g_584[0];/* VOLATILE GLOBAL g_667 */
static int16_t g_676 = 0L;
static int64_t g_737[7] = {0x8F80848FAAC30016LL,0x8F80848FAAC30016LL,0x8F80848FAAC30016LL,0x8F80848FAAC30016LL,0x8F80848FAAC30016LL,0x8F80848FAAC30016LL,0x8F80848FAAC30016LL};
static uint16_t *g_744[5][6] = {{&g_242,&g_242,&g_242,&g_242,&g_242,&g_242},{&g_242,&g_242,&g_242,&g_242,&g_242,&g_242},{&g_242,&g_242,&g_242,&g_242,&g_242,&g_242},{&g_242,&g_242,&g_242,&g_242,&g_242,&g_242},{&g_242,&g_242,&g_242,&g_242,&g_242,&g_242}};
static uint16_t ** volatile g_743 = &g_744[3][5];/* VOLATILE GLOBAL g_743 */
static int32_t ** volatile g_777 = &g_368;/* VOLATILE GLOBAL g_777 */
static int8_t g_800 = 0x3EL;
static uint32_t g_802 = 4294967295UL;
static volatile uint32_t g_850 = 0UL;/* VOLATILE GLOBAL g_850 */
static uint32_t *g_877 = &g_802;
static uint32_t **g_876 = &g_877;
static uint16_t **g_887 = &g_744[0][3];
static uint16_t *** volatile g_886 = &g_887;/* VOLATILE GLOBAL g_886 */
static volatile uint8_t g_940[3] = {0xE1L,0xE1L,0xE1L};
static int32_t * volatile g_1028 = &g_166;/* VOLATILE GLOBAL g_1028 */
static uint32_t g_1120 = 0xC244CA3FL;
static volatile int64_t g_1149 = 0xBE3F62817C337848LL;/* VOLATILE GLOBAL g_1149 */
static volatile uint32_t g_1166 = 0xD0AB8954L;/* VOLATILE GLOBAL g_1166 */
static int32_t ** volatile g_1221[1][5] = {{&g_368,&g_368,&g_368,&g_368,&g_368}};
static int32_t ** volatile g_1222[9][5] = {{(void*)0,&g_368,&g_368,(void*)0,&g_368},{(void*)0,&g_368,&g_368,&g_368,(void*)0},{&g_368,(void*)0,&g_368,&g_368,(void*)0},{(void*)0,&g_368,&g_368,&g_368,&g_368},{(void*)0,(void*)0,&g_368,(void*)0,(void*)0},{&g_368,&g_368,&g_368,&g_368,(void*)0},{(void*)0,&g_368,&g_368,(void*)0,&g_368},{(void*)0,&g_368,&g_368,&g_368,(void*)0},{&g_368,(void*)0,&g_368,&g_368,(void*)0}};
static int32_t ** const  volatile g_1223 = &g_368;/* VOLATILE GLOBAL g_1223 */
static int32_t ** volatile g_1224 = &g_368;/* VOLATILE GLOBAL g_1224 */
static int32_t ** volatile g_1225 = (void*)0;/* VOLATILE GLOBAL g_1225 */
static int32_t ** volatile g_1226 = (void*)0;/* VOLATILE GLOBAL g_1226 */
static uint16_t g_1234[1] = {0UL};
static int64_t *****g_1281 = (void*)0;
static int32_t g_1283 = 0x5FA6E212L;
static uint8_t g_1361 = 255UL;
static int32_t ** volatile g_1364 = &g_368;/* VOLATILE GLOBAL g_1364 */
static int32_t ** volatile g_1371 = &g_368;/* VOLATILE GLOBAL g_1371 */
static volatile uint16_t g_1405 = 0x429CL;/* VOLATILE GLOBAL g_1405 */
static int32_t ** volatile g_1429 = &g_368;/* VOLATILE GLOBAL g_1429 */
static uint64_t **g_1438 = &g_330;
static uint64_t ***g_1437 = &g_1438;
static const int32_t g_1494 = (-1L);
static volatile uint32_t *g_1537 = (void*)0;
static volatile uint32_t * volatile *g_1536[1] = {&g_1537};
static volatile uint32_t * volatile ** volatile g_1535[2][8][1] = {{{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]}},{{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]},{&g_1536[0]}}};
static volatile uint32_t * volatile ** volatile * volatile g_1534[9][4][6] = {{{&g_1535[1][0][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][3][0],&g_1535[1][5][0],&g_1535[0][6][0]},{&g_1535[1][5][0],&g_1535[1][0][0],&g_1535[0][4][0],&g_1535[1][6][0],&g_1535[0][4][0],(void*)0},{&g_1535[0][7][0],&g_1535[1][6][0],&g_1535[0][6][0],&g_1535[0][4][0],&g_1535[1][7][0],&g_1535[0][7][0]},{&g_1535[0][4][0],&g_1535[1][7][0],&g_1535[0][7][0],&g_1535[1][4][0],&g_1535[0][5][0],&g_1535[0][4][0]}},{{&g_1535[0][2][0],&g_1535[1][5][0],&g_1535[0][4][0],&g_1535[0][3][0],&g_1535[0][4][0],&g_1535[1][5][0]},{&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][3][0],&g_1535[0][4][0],&g_1535[1][5][0],&g_1535[1][4][0]},{&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][5][0],(void*)0,(void*)0},{&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][0][0],&g_1535[0][7][0],&g_1535[1][5][0],&g_1535[0][4][0]}},{{&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][6][0],&g_1535[0][4][0],&g_1535[0][7][0]},{(void*)0,&g_1535[1][5][0],&g_1535[0][6][0],&g_1535[0][4][0],&g_1535[0][5][0],&g_1535[0][4][0]},{&g_1535[1][6][0],&g_1535[1][7][0],&g_1535[1][3][0],&g_1535[1][3][0],&g_1535[1][7][0],&g_1535[1][6][0]},{&g_1535[0][2][0],&g_1535[1][6][0],(void*)0,&g_1535[0][5][0],&g_1535[0][4][0],&g_1535[1][5][0]}},{{&g_1535[0][4][0],&g_1535[1][0][0],&g_1535[1][4][0],&g_1535[0][4][0],&g_1535[1][5][0],&g_1535[0][7][0]},{&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][5][0],&g_1535[0][4][0],&g_1535[1][3][0]},{&g_1535[0][5][0],(void*)0,&g_1535[0][7][0],&g_1535[1][5][0],&g_1535[0][4][0],&g_1535[0][4][0]},{&g_1535[0][4][0],(void*)0,&g_1535[1][7][0],&g_1535[0][4][0],(void*)0,&g_1535[0][7][0]}},{{&g_1535[1][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][4][0],&g_1535[0][6][0]},{&g_1535[0][4][0],&g_1535[0][7][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0]},{&g_1535[0][3][0],&g_1535[0][2][0],&g_1535[0][3][0],&g_1535[0][7][0],&g_1535[0][4][0],&g_1535[0][4][0]},{(void*)0,&g_1535[0][5][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][6][0]}},{{&g_1535[0][4][0],(void*)0,&g_1535[0][4][0],&g_1535[0][7][0],&g_1535[1][5][0],&g_1535[0][7][0]},{&g_1535[0][5][0],&g_1535[0][4][0],&g_1535[1][7][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0]},{&g_1535[0][4][0],&g_1535[0][7][0],&g_1535[0][7][0],&g_1535[0][4][0],(void*)0,&g_1535[1][3][0]},{&g_1535[1][3][0],&g_1535[0][2][0],(void*)0,&g_1535[0][4][0],&g_1535[0][7][0],&g_1535[0][4][0]}},{{&g_1535[1][0][0],&g_1535[1][4][0],&g_1535[0][4][0],&g_1535[1][5][0],&g_1535[0][7][0],&g_1535[1][0][0]},{&g_1535[0][7][0],&g_1535[0][2][0],&g_1535[1][6][0],&g_1535[0][7][0],(void*)0,&g_1535[0][4][0]},{(void*)0,&g_1535[0][7][0],&g_1535[1][5][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][6][0]},{&g_1535[0][3][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][3][0],&g_1535[1][5][0],&g_1535[1][3][0]}},{{&g_1535[0][3][0],(void*)0,&g_1535[0][3][0],&g_1535[0][4][0],&g_1535[0][4][0],(void*)0},{&g_1535[1][0][0],&g_1535[0][5][0],&g_1535[0][7][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][4][0]},{&g_1535[0][7][0],&g_1535[0][2][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0]},{&g_1535[1][0][0],&g_1535[0][7][0],&g_1535[1][5][0],&g_1535[0][4][0],&g_1535[1][4][0],&g_1535[1][0][0]}},{{&g_1535[0][3][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[1][3][0],(void*)0,&g_1535[0][4][0]},{&g_1535[0][3][0],(void*)0,&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0],&g_1535[0][4][0]},{(void*)0,(void*)0,&g_1535[0][4][0],&g_1535[0][7][0],&g_1535[0][4][0],&g_1535[1][4][0]},{&g_1535[0][7][0],(void*)0,&g_1535[1][7][0],&g_1535[1][5][0],&g_1535[0][4][0],&g_1535[0][4][0]}}};
static int32_t ** volatile g_1591[5][10][5] = {{{&g_368,&g_368,(void*)0,(void*)0,&g_368},{(void*)0,(void*)0,&g_368,(void*)0,(void*)0},{&g_368,(void*)0,&g_368,(void*)0,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{(void*)0,&g_368,&g_368,&g_368,&g_368},{(void*)0,(void*)0,(void*)0,&g_368,&g_368},{&g_368,(void*)0,&g_368,&g_368,&g_368},{(void*)0,&g_368,&g_368,(void*)0,&g_368},{&g_368,&g_368,&g_368,(void*)0,&g_368}},{{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,(void*)0,(void*)0,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{(void*)0,&g_368,(void*)0,&g_368,&g_368},{&g_368,(void*)0,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,(void*)0,&g_368,(void*)0,&g_368},{(void*)0,&g_368,&g_368,&g_368,(void*)0}},{{&g_368,&g_368,&g_368,&g_368,(void*)0},{&g_368,&g_368,(void*)0,(void*)0,&g_368},{&g_368,&g_368,(void*)0,(void*)0,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{(void*)0,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,(void*)0,&g_368},{&g_368,&g_368,&g_368,&g_368,(void*)0},{(void*)0,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368}},{{&g_368,&g_368,&g_368,(void*)0,&g_368},{&g_368,&g_368,(void*)0,&g_368,&g_368},{&g_368,&g_368,(void*)0,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{(void*)0,(void*)0,(void*)0,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,(void*)0},{&g_368,(void*)0,(void*)0,&g_368,&g_368},{&g_368,&g_368,&g_368,(void*)0,&g_368}},{{&g_368,&g_368,&g_368,&g_368,&g_368},{(void*)0,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,(void*)0,(void*)0,&g_368},{&g_368,&g_368,(void*)0,&g_368,(void*)0},{(void*)0,&g_368,&g_368,&g_368,(void*)0},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368,(void*)0,&g_368},{&g_368,&g_368,&g_368,&g_368,&g_368},{(void*)0,&g_368,&g_368,&g_368,&g_368}}};
static int32_t ** volatile g_1594 = &g_368;/* VOLATILE GLOBAL g_1594 */
static int64_t g_1714 = 0xD2FFC85EDB6F1457LL;
static int32_t ** volatile g_1730 = &g_368;/* VOLATILE GLOBAL g_1730 */
static int16_t g_1749 = 0xD8D1L;
static int32_t ** volatile g_1765 = &g_368;/* VOLATILE GLOBAL g_1765 */
static int32_t ** volatile g_1841 = (void*)0;/* VOLATILE GLOBAL g_1841 */
static volatile int16_t g_1847 = (-9L);/* VOLATILE GLOBAL g_1847 */
static uint64_t g_1864 = 0xD185501326AD9651LL;
static volatile int32_t g_1884 = 0x437B24A6L;/* VOLATILE GLOBAL g_1884 */
static volatile uint32_t * volatile * volatile * volatile * volatile g_1989 = (void*)0;/* VOLATILE GLOBAL g_1989 */
static volatile uint64_t *g_2034 = &g_62;
static volatile uint64_t **g_2033 = &g_2034;
static int32_t ** volatile g_2063 = (void*)0;/* VOLATILE GLOBAL g_2063 */
static int32_t ** volatile g_2064 = &g_368;/* VOLATILE GLOBAL g_2064 */
static volatile uint32_t *g_2126 = (void*)0;
static volatile uint32_t **g_2125 = &g_2126;
static volatile uint32_t ** volatile *g_2124 = &g_2125;
static const volatile int64_t g_2158 = (-7L);/* VOLATILE GLOBAL g_2158 */
static int8_t g_2191 = 0xADL;
static volatile uint16_t * volatile ** volatile *g_2193 = (void*)0;
static uint8_t g_2201[9] = {248UL,0x62L,248UL,248UL,0x62L,248UL,248UL,0x62L,248UL};
static uint8_t * const g_2200 = &g_2201[0];
static uint8_t * const *g_2199 = &g_2200;
static uint8_t * const **g_2198 = &g_2199;
static const int32_t *g_2273 = &g_82;
static const int32_t ** volatile g_2272 = &g_2273;/* VOLATILE GLOBAL g_2272 */
static volatile uint32_t * volatile **g_2292[4][7] = {{&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0]},{&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0]},{&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0]},{&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0],&g_1536[0]}};
static volatile uint32_t * volatile ***g_2291 = &g_2292[0][4];
static const int64_t **g_2301 = (void*)0;
static const int64_t ***g_2300 = &g_2301;
static int32_t ** volatile g_2342 = &g_368;/* VOLATILE GLOBAL g_2342 */
static uint32_t ***g_2372 = &g_876;
static uint32_t ****g_2371[1] = {&g_2372};
static uint32_t *****g_2370 = &g_2371[0];
static int8_t g_2399 = 0x8FL;
static int32_t ** volatile g_2402 = &g_368;/* VOLATILE GLOBAL g_2402 */
static int64_t **** volatile g_2415[6][8][5] = {{{(void*)0,&g_455,&g_455,&g_455,&g_463},{&g_463,&g_463,&g_463,&g_455,&g_463},{(void*)0,&g_455,&g_455,&g_455,(void*)0},{&g_455,&g_463,&g_455,&g_455,&g_455},{&g_455,&g_463,&g_463,&g_463,&g_463},{&g_455,(void*)0,&g_455,&g_463,(void*)0},{(void*)0,&g_455,&g_463,&g_455,&g_463},{(void*)0,&g_455,&g_463,&g_463,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,&g_455},{&g_455,&g_463,(void*)0,(void*)0,&g_463},{&g_463,&g_463,&g_463,&g_463,(void*)0},{&g_455,&g_455,&g_463,(void*)0,&g_463},{&g_463,&g_463,&g_463,&g_463,(void*)0},{&g_463,&g_455,&g_463,&g_455,(void*)0},{(void*)0,&g_455,(void*)0,&g_455,(void*)0},{&g_463,&g_463,&g_463,&g_455,&g_463}},{{(void*)0,(void*)0,&g_455,&g_463,(void*)0},{(void*)0,(void*)0,&g_463,(void*)0,(void*)0},{&g_455,&g_455,&g_455,&g_463,&g_463},{&g_455,&g_463,(void*)0,(void*)0,&g_455},{(void*)0,&g_455,(void*)0,(void*)0,&g_463},{&g_463,&g_463,&g_455,&g_463,(void*)0},{(void*)0,&g_463,&g_463,&g_455,&g_463},{(void*)0,&g_463,&g_463,&g_463,&g_463}},{{&g_463,&g_455,&g_455,&g_463,&g_463},{(void*)0,(void*)0,&g_463,&g_455,&g_455},{&g_455,(void*)0,(void*)0,&g_455,&g_455},{&g_455,&g_463,(void*)0,&g_455,&g_455},{(void*)0,&g_463,(void*)0,&g_455,&g_455},{(void*)0,(void*)0,&g_455,(void*)0,(void*)0},{(void*)0,(void*)0,&g_455,&g_455,&g_463},{&g_463,&g_463,(void*)0,&g_463,(void*)0}},{{(void*)0,&g_455,&g_455,&g_455,(void*)0},{&g_463,(void*)0,&g_455,&g_463,(void*)0},{&g_463,(void*)0,&g_455,(void*)0,&g_463},{&g_463,&g_455,&g_463,&g_463,&g_463},{&g_455,(void*)0,&g_455,(void*)0,&g_463},{&g_463,(void*)0,&g_455,(void*)0,&g_455},{(void*)0,&g_455,(void*)0,&g_463,&g_455},{&g_455,&g_463,&g_455,&g_455,&g_463}},{{&g_463,&g_455,(void*)0,&g_455,&g_463},{&g_455,&g_463,&g_463,&g_463,&g_463},{&g_455,&g_463,&g_463,&g_455,(void*)0},{&g_463,&g_463,(void*)0,&g_463,(void*)0},{&g_455,&g_455,&g_455,&g_463,(void*)0},{(void*)0,&g_455,&g_455,&g_455,&g_463},{&g_463,&g_463,&g_463,&g_455,&g_463},{(void*)0,&g_455,&g_463,&g_455,&g_463}}};
static int32_t ** volatile g_2677 = &g_368;/* VOLATILE GLOBAL g_2677 */
static int32_t ** volatile g_2680 = &g_368;/* VOLATILE GLOBAL g_2680 */
static int32_t g_2751 = 0xCAD504F8L;
static volatile int32_t *g_2799 = &g_1884;
static volatile int32_t * const *g_2798 = &g_2799;
static int8_t g_2826[1] = {0xF9L};
static uint8_t g_2833 = 8UL;
static int32_t ** volatile g_2842 = &g_368;/* VOLATILE GLOBAL g_2842 */
static int32_t ** volatile g_2861 = &g_368;/* VOLATILE GLOBAL g_2861 */
static int32_t ** volatile g_2862 = &g_368;/* VOLATILE GLOBAL g_2862 */
static uint8_t g_2899 = 250UL;
static int32_t ** volatile g_2903 = &g_368;/* VOLATILE GLOBAL g_2903 */
static uint32_t **g_2944 = &g_289;
static int32_t ** volatile g_3011 = &g_368;/* VOLATILE GLOBAL g_3011 */
static int32_t g_3034 = 8L;


/* --- FORWARD DECLARATIONS --- */
static int16_t  func_1(void);
static uint16_t  func_2(const int16_t  p_3);
static uint32_t  func_19(int8_t * p_20, int8_t  p_21, uint32_t  p_22);
static int32_t * const  func_30(uint32_t  p_31, uint64_t  p_32);
static uint8_t  func_33(const uint16_t  p_34);
static int64_t * func_35(int64_t * p_36, int64_t * p_37);
static int64_t  func_40(uint16_t  p_41, int16_t  p_42);
static int8_t  func_91(uint64_t  p_92, uint8_t  p_93);
static uint8_t  func_96(uint32_t  p_97, uint32_t * p_98, const int64_t * p_99);
static const int64_t * func_102(const int64_t  p_103, int64_t * p_104, uint32_t * p_105, int8_t * const  p_106, uint32_t  p_107);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_23 g_24 g_29 g_62 g_82 g_166 g_877 g_876 g_802 g_190 g_584 g_368 g_367 g_213 g_127 g_940 g_514 g_135 g_887 g_251 g_329 g_330 g_302 g_189 g_331 g_149 g_1028 g_114 g_886 g_800 g_539 g_1361 g_242 g_1364 g_1371 g_1405 g_564 g_1120 g_563 g_1429 g_1437 g_667 g_396 g_1438 g_1534 g_218 g_1494 g_288 g_289 g_1594 g_464 g_283 g_1749 g_2677 g_1234 g_164 g_2201 g_2198 g_2199 g_2200 g_2273 g_1714 g_2372 g_1765 g_78 g_2124 g_2125 g_2126 g_2399 g_3011 g_2862 g_3034 g_737
 * writes: g_18 g_29 g_24 g_62 g_78 g_82 g_166 g_802 g_127 g_584 g_737 g_676 g_213 g_940 g_744 g_302 g_218 g_800 g_114 g_251 g_1281 g_887 g_1361 g_242 g_602 g_368 g_1405 g_563 g_1437 g_135 g_1120 g_149 g_1749 g_1234 g_2944 g_1438
 */
static int16_t  func_1(void)
{ /* block id: 0 */
    int16_t l_16[1][3][7] = {{{0x0BDBL,(-8L),1L,0L,1L,(-8L),0x0BDBL},{0x0BDBL,(-8L),1L,0L,1L,(-8L),0x0BDBL},{0x0BDBL,(-8L),1L,0L,1L,(-8L),0x0BDBL}}};
    int8_t *l_17[4];
    uint8_t l_25 = 0UL;
    int64_t *l_28 = &g_29;
    int32_t l_2911 = 0x29849A9DL;
    uint8_t l_2912 = 0x22L;
    int16_t *l_2913 = (void*)0;
    int16_t *l_2914 = &g_1749;
    uint16_t *l_2916 = &g_1234[0];
    uint16_t ***l_2923 = &g_887;
    int32_t **l_2925 = &g_368;
    int32_t l_2933 = 0xC89A4604L;
    uint8_t l_2934 = 0UL;
    const uint64_t **l_3036 = (void*)0;
    int32_t l_3040 = 0xF5AEAE4CL;
    int32_t l_3043 = (-1L);
    int32_t l_3044[6][9][4] = {{{0xC5EAD429L,0x4F97732DL,0x7F2C4AEEL,1L},{0L,1L,0x608BB703L,(-3L)},{0x4F97732DL,1L,0x767E9E7BL,(-3L)},{0x7F2C4AEEL,1L,0xF43943D4L,1L},{0x944424B2L,0x4F97732DL,1L,0x4F97732DL},{1L,0xC5EAD429L,0x767E9E7BL,0L},{0xF94912F5L,0L,0xC5EAD429L,0xF94912F5L},{0L,0x4F97732DL,(-1L),0x944424B2L},{0L,0x7F2C4AEEL,0xC5EAD429L,(-3L)}},{{0xF94912F5L,0x944424B2L,0x767E9E7BL,0x767E9E7BL},{1L,1L,1L,0x944424B2L},{0x944424B2L,0xF94912F5L,0xF43943D4L,0x4F97732DL},{0x7F2C4AEEL,0L,0x767E9E7BL,0xF43943D4L},{0x4F97732DL,0L,0x608BB703L,0x4F97732DL},{0L,0xF94912F5L,0x7F2C4AEEL,0x944424B2L},{0xC5EAD429L,1L,0xC5EAD429L,0x767E9E7BL},{0x4F97732DL,0x944424B2L,0x9665A3D2L,(-3L)},{1L,0x7F2C4AEEL,0xF43943D4L,0x944424B2L}},{{1L,0x4F97732DL,0xF43943D4L,0xF94912F5L},{1L,0L,0x9665A3D2L,0x7F2C4AEEL},{0xC5EAD429L,0x6D1558E4L,0x6D1558E4L,0xC5EAD429L},{0x6D1558E4L,0xC5EAD429L,0L,0x9665A3D2L},{1L,0x1F4C8AFCL,0x944424B2L,0xF43943D4L},{0xC5EAD429L,0x9665A3D2L,1L,0xF43943D4L},{0L,0x1F4C8AFCL,(-1L),0x9665A3D2L},{0x767E9E7BL,0xC5EAD429L,1L,0xC5EAD429L},{0x1F4C8AFCL,0x6D1558E4L,1L,0x7F2C4AEEL}},{{0x608BB703L,1L,0x6D1558E4L,0x608BB703L},{1L,0xC5EAD429L,0L,0x767E9E7BL},{1L,0L,0x6D1558E4L,0xF43943D4L},{0x608BB703L,0x767E9E7BL,1L,1L},{0x1F4C8AFCL,0x1F4C8AFCL,1L,0x767E9E7BL},{0x767E9E7BL,0x608BB703L,(-1L),0xC5EAD429L},{0L,1L,1L,(-1L)},{0xC5EAD429L,1L,0x944424B2L,0xC5EAD429L},{1L,0x608BB703L,0L,0x767E9E7BL}},{{0x6D1558E4L,0x1F4C8AFCL,0x6D1558E4L,1L},{0xC5EAD429L,0x767E9E7BL,0L,0xF43943D4L},{0x1F4C8AFCL,0L,(-1L),0x767E9E7BL},{0x9665A3D2L,0xC5EAD429L,(-1L),0x608BB703L},{0x1F4C8AFCL,1L,0L,0x7F2C4AEEL},{0xC5EAD429L,0x6D1558E4L,0x6D1558E4L,0xC5EAD429L},{0x6D1558E4L,0xC5EAD429L,0L,0x9665A3D2L},{1L,0x1F4C8AFCL,0x944424B2L,0xF43943D4L},{0xC5EAD429L,0x9665A3D2L,1L,0xF43943D4L}},{{0L,0x1F4C8AFCL,(-1L),0x9665A3D2L},{0x767E9E7BL,0xC5EAD429L,1L,0xC5EAD429L},{0x1F4C8AFCL,0x6D1558E4L,1L,0x7F2C4AEEL},{0x608BB703L,1L,0x6D1558E4L,0x608BB703L},{1L,0xC5EAD429L,0L,0x767E9E7BL},{1L,0L,0x6D1558E4L,0xF43943D4L},{0x608BB703L,0x767E9E7BL,1L,1L},{0x1F4C8AFCL,0x1F4C8AFCL,1L,0x767E9E7BL},{0x767E9E7BL,0x608BB703L,(-1L),0xC5EAD429L}}};
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_17[i] = &g_18[0];
    if ((((*l_2916) |= func_2(((*l_2914) ^= (safe_mod_func_int64_t_s_s((((safe_mul_func_uint64_t_u_u((safe_rshift_func_uint32_t_u_u((safe_div_func_int32_t_s_s((safe_mod_func_uint8_t_u_u((safe_add_func_int64_t_s_s(((g_18[0] = l_16[0][1][1]) != ((-4L) != 0x4E8FE752L)), (func_19(g_23, (l_25 ^ (safe_lshift_func_int64_t_s_s(((*l_28) = (-6L)), 55))), g_24) != (safe_add_func_uint64_t_u_u((((((void*)0 != &g_1749) | l_16[0][1][1]) >= 1L) && l_16[0][1][0]), l_2911))))), 0x64L)), l_2912)), l_16[0][0][5])), 0xC622A37772B60255LL)) | l_25) && l_16[0][2][6]), 1UL))))) && g_164))
    { /* block id: 1269 */
        int32_t **l_2917 = &g_368;
        for (g_800 = 0; (g_800 <= 2); g_800 += 1)
        { /* block id: 1272 */
            return g_2201[6];
        }
        (*l_2917) = (*g_1371);
    }
    else
    { /* block id: 1276 */
        int64_t l_2921 = 0x532CC6DEE6A0846ALL;
        int32_t **l_2928[10] = {(void*)0,&g_368,(void*)0,&g_368,(void*)0,&g_368,(void*)0,&g_368,(void*)0,&g_368};
        uint8_t **l_2947[2];
        uint8_t ***l_2946 = &l_2947[1];
        int16_t l_2948 = 1L;
        uint64_t l_3010 = 0x0191B82821F11DB3LL;
        uint32_t * const *l_3022 = &g_877;
        uint32_t * const ** const l_3021 = &l_3022;
        int16_t l_3037 = 0x2135L;
        int i;
        for (i = 0; i < 2; i++)
            l_2947[i] = &g_190;
        for (g_166 = 22; (g_166 >= 17); g_166 = safe_sub_func_uint64_t_u_u(g_166, 7))
        { /* block id: 1279 */
            int32_t *l_2920 = (void*)0;
            int32_t ***l_2926 = (void*)0;
            int32_t ***l_2927 = &l_2925;
            const int32_t l_2932 = 0L;
            int16_t l_2936 = 4L;
            uint32_t **l_2943 = (void*)0;
            int8_t l_2945 = (-1L);
            uint64_t ***l_2955[7] = {&g_1438,&g_1438,&g_1438,&g_1438,&g_1438,&g_1438,&g_1438};
            const int32_t l_2970 = 0x87ECE5C8L;
            int32_t l_2985 = 9L;
            int32_t l_3009[4][5] = {{1L,1L,0xA7891452L,(-4L),0xA7891452L},{(-5L),(-5L),0xCDDD0561L,0L,0xCDDD0561L},{1L,1L,0xA7891452L,(-4L),0xA7891452L},{(-5L),(-5L),0xCDDD0561L,0L,0xCDDD0561L}};
            int32_t l_3038 = 0L;
            int32_t l_3039[4] = {(-1L),(-1L),(-1L),(-1L)};
            int32_t l_3041 = 1L;
            int32_t l_3042 = (-6L);
            uint32_t l_3045[6][6][4] = {{{4294967295UL,0x7E623F49L,0x494FA5F5L,4294967295UL},{1UL,4294967294UL,4294967295UL,0xFDBAC792L},{0x19CBF16DL,0x0011D095L,0x338D015CL,4294967295UL},{0UL,4294967286UL,4294967295UL,7UL},{4294967295UL,0x0011D095L,0x962F1B5CL,0xD4587BC4L},{0x8EBF8BA6L,5UL,0x8EBF8BA6L,0x494FA5F5L}},{{4294967295UL,0x3BE4C34CL,0x494FA5F5L,4294967295UL},{0UL,0x962F1B5CL,0x7E623F49L,0x3BE4C34CL},{0x3BE4C34CL,0xAA9345F6L,0x7E623F49L,7UL},{0UL,4294967295UL,0x494FA5F5L,0x7E623F49L},{4294967295UL,4294967286UL,0x8EBF8BA6L,0UL},{0x8EBF8BA6L,0UL,0x962F1B5CL,0x338D015CL}},{{4294967295UL,0x962F1B5CL,4294967295UL,0x494FA5F5L},{0UL,4294967287UL,0x9727DFDDL,0x9727DFDDL},{0UL,0UL,4294967295UL,0xD4587BC4L},{0x338D015CL,0xAA9345F6L,9UL,0UL},{0x07DC449CL,1UL,0x494FA5F5L,9UL},{0x0011D095L,1UL,4294967295UL,0UL}},{{1UL,0xAA9345F6L,0x96302F01L,0xD4587BC4L},{4294967295UL,0UL,1UL,0x9727DFDDL},{4294967295UL,4294967287UL,4294967295UL,0x494FA5F5L},{0x07DC449CL,0x962F1B5CL,4294967295UL,0x338D015CL},{0xF95A04BEL,0UL,0x7E623F49L,0UL},{0x962F1B5CL,4294967286UL,0x9727DFDDL,0x7E623F49L}},{{0x0011D095L,4294967295UL,1UL,7UL},{0x8EBF8BA6L,0xAA9345F6L,0x07DC449CL,0x3BE4C34CL},{0x8EBF8BA6L,0x962F1B5CL,1UL,4294967295UL},{0x0011D095L,0x3BE4C34CL,0x9727DFDDL,0x494FA5F5L},{0x962F1B5CL,5UL,0x7E623F49L,0xD4587BC4L},{0xF95A04BEL,0x0011D095L,4294967295UL,7UL}},{{0x07DC449CL,4294967286UL,4294967295UL,4294967295UL},{4294967295UL,1UL,1UL,4294967295UL},{4294967295UL,0UL,0x96302F01L,0x3BE4C34CL},{1UL,5UL,4294967295UL,0x9727DFDDL},{0x0011D095L,0xF95A04BEL,0x494FA5F5L,0x9727DFDDL},{0x07DC449CL,5UL,9UL,0x3BE4C34CL}}};
            int i, j, k;
            l_2921 ^= l_25;
            l_2933 = (+((l_2923 != (((safe_unary_minus_func_int32_t_s((((*l_2927) = l_2925) != l_2928[3]))) && (safe_mod_func_int32_t_s_s((safe_unary_minus_func_int8_t_s(1L)), l_2932))) , l_2923)) < (-1L)));
            if ((l_2934 >= (((((*g_23) |= ((***g_2198) <= (**g_189))) == ((safe_unary_minus_func_int16_t_s((0L ^ (((*l_2916) = l_2936) > (safe_mul_func_int16_t_s_s(0L, ((*g_330) && (safe_mod_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((**g_1438), (((((g_2944 = l_2943) != l_2943) != (*g_2273)) , 0x8EL) >= 4L))), g_1714))))))))) && (*g_2273))) , l_2945) <= (***g_2372))))
            { /* block id: 1286 */
                uint64_t ****l_2956 = &l_2955[5];
                int32_t l_2965 = (-7L);
                l_2948 &= ((*g_667) = ((void*)0 != l_2946));
                if ((safe_add_func_int8_t_s_s((safe_sub_func_int64_t_s_s(l_2912, (((safe_add_func_int8_t_s_s(((*g_190) <= (((((*l_2956) = l_2955[5]) != (void*)0) , ((safe_rshift_func_int16_t_s_s(((safe_rshift_func_uint64_t_u_s((safe_div_func_uint32_t_u_u(((safe_mod_func_int8_t_s_s(l_2965, (safe_rshift_func_uint64_t_u_s((0x0A6E3E09L || ((*g_190) > (((***g_331) |= 6UL) == (((safe_mod_func_uint64_t_u_u(0x547385DA6A1060E7LL, (*g_514))) == (*g_2273)) <= l_2965)))), l_2970)))) <= 0x68L), l_2965)), 33)) || (*g_514)), g_584[0])) > 0UL)) <= 65535UL)), (*g_2200))) || l_2965) <= l_2965))), 255UL)))
                { /* block id: 1291 */
                    for (g_602 = 12; (g_602 >= 27); ++g_602)
                    { /* block id: 1294 */
                        int8_t l_2983 = 0xFBL;
                        int32_t **l_2984 = &g_368;
                        (*l_2984) = func_30((safe_div_func_uint32_t_u_u(4294967286UL, (((*l_28) = 0xAD3C064365FE7CECLL) && (safe_lshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_u(((((safe_rshift_func_uint8_t_u_u(1UL, 4)) <= l_2965) != (-1L)) , (safe_rshift_func_uint16_t_u_u(l_2983, 7))), 8)), 5))))), (*g_330));
                        (*g_368) ^= 0xC1B2CC3AL;
                        (**g_1765) |= 0x2FEE4CE2L;
                    }
                    return l_2985;
                }
                else
                { /* block id: 1301 */
                    return g_78;
                }
            }
            else
            { /* block id: 1304 */
                uint16_t l_2986 = 0xF5FEL;
                int8_t l_3004[8] = {0x43L,0x43L,0x43L,0x43L,0x43L,0x43L,0x43L,0x43L};
                uint64_t l_3019 = 0xBBFE67035BD5662BLL;
                uint64_t **l_3035 = (void*)0;
                int i;
                (*g_3011) = func_30(l_2986, ((safe_sub_func_int8_t_s_s((g_800 |= ((safe_rshift_func_int64_t_s_u((((safe_unary_minus_func_uint64_t_u(((*g_288) != (**g_2124)))) < ((+(safe_add_func_uint8_t_u_u(0x8AL, ((safe_add_func_uint32_t_u_u((+((((g_2399 != (((safe_div_func_uint64_t_u_u(((safe_mod_func_uint32_t_u_u(((l_3004[0] , 0x4F7FL) > ((safe_mul_func_int64_t_s_s(l_3004[0], (((safe_rshift_func_uint8_t_u_s(l_3004[0], 7)) < 0x8FA981976FB6943BLL) & l_3009[2][1]))) , 1UL)), 4294967295UL)) == 0xCD5E1B16L), 0x56F43EA100FB43F2LL)) != l_2986) == (***g_2198))) | l_2986) != (**g_329)) > 0UL)), 0xFAC672FDL)) , (*g_2200))))) , (*g_23))) == l_16[0][1][1]), l_3010)) && 0x177A705574BCB8F0LL)), 0x8FL)) == (*g_2273)));
                (**l_2925) = ((safe_div_func_uint32_t_u_u(((safe_mul_func_uint64_t_u_u((safe_rshift_func_uint64_t_u_u((+l_3019), ((~(**g_2862)) ^ ((void*)0 == l_3021)))), ((safe_sub_func_uint16_t_u_u((**l_2925), (safe_add_func_int64_t_s_s((safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_s((!(((((l_2986 , (safe_div_func_int32_t_s_s(((-1L) && g_3034), (((l_3035 = ((*g_1437) = (*g_1437))) == l_3036) , 0x1FAE4819L)))) >= (**g_329)) <= 1L) | (**g_876)) , 0xAC7AD52DL)), g_1494)), (*g_368))), 0x2A744BD52A7AB228LL)))) > (**l_2925)))) == (-10L)), l_3037)) , (*g_667));
            }
            l_3045[4][4][1]++;
        }
        if (g_302)
            goto lbl_3048;
lbl_3048:
        (*g_1028) ^= 0x5A0BD590L;
        return g_1749;
    }
    return g_737[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_2677
 * writes: g_368
 */
static uint16_t  func_2(const int16_t  p_3)
{ /* block id: 1265 */
    int32_t **l_2915[7][10][3] = {{{&g_368,(void*)0,&g_368},{&g_368,(void*)0,&g_368},{&g_368,&g_368,&g_368},{&g_368,(void*)0,&g_368},{&g_368,&g_368,&g_368},{&g_368,&g_368,(void*)0},{(void*)0,&g_368,&g_368},{&g_368,(void*)0,(void*)0},{(void*)0,&g_368,&g_368},{&g_368,(void*)0,&g_368}},{{(void*)0,&g_368,&g_368},{&g_368,(void*)0,&g_368},{&g_368,&g_368,(void*)0},{(void*)0,&g_368,(void*)0},{&g_368,&g_368,(void*)0},{&g_368,&g_368,&g_368},{&g_368,&g_368,(void*)0},{(void*)0,&g_368,(void*)0},{(void*)0,&g_368,(void*)0},{&g_368,&g_368,&g_368}},{{&g_368,&g_368,(void*)0},{(void*)0,&g_368,&g_368},{&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368},{&g_368,(void*)0,(void*)0},{(void*)0,&g_368,(void*)0},{&g_368,(void*)0,&g_368},{&g_368,(void*)0,(void*)0},{(void*)0,&g_368,(void*)0}},{{&g_368,&g_368,&g_368},{(void*)0,&g_368,&g_368},{(void*)0,&g_368,&g_368},{(void*)0,(void*)0,&g_368},{(void*)0,&g_368,(void*)0},{(void*)0,&g_368,&g_368},{&g_368,&g_368,(void*)0},{&g_368,(void*)0,(void*)0},{&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368}},{{&g_368,(void*)0,(void*)0},{&g_368,&g_368,&g_368},{&g_368,(void*)0,&g_368},{&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368},{&g_368,&g_368,&g_368},{(void*)0,&g_368,(void*)0},{(void*)0,&g_368,(void*)0},{(void*)0,(void*)0,&g_368},{(void*)0,&g_368,&g_368}},{{(void*)0,(void*)0,&g_368},{&g_368,&g_368,(void*)0},{(void*)0,&g_368,&g_368},{&g_368,&g_368,(void*)0},{&g_368,&g_368,&g_368},{(void*)0,&g_368,(void*)0},{&g_368,(void*)0,&g_368},{&g_368,&g_368,(void*)0},{&g_368,(void*)0,(void*)0},{&g_368,&g_368,(void*)0}},{{(void*)0,&g_368,(void*)0},{&g_368,&g_368,(void*)0},{&g_368,&g_368,&g_368},{(void*)0,&g_368,&g_368},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_368,&g_368},{&g_368,(void*)0,&g_368},{&g_368,&g_368,(void*)0},{&g_368,&g_368,&g_368},{(void*)0,(void*)0,&g_368}}};
    int i, j, k;
    (*g_2677) = (void*)0;
    return p_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_23 g_24 g_62 g_82 g_166 g_877 g_876 g_802 g_190 g_584 g_368 g_367 g_213 g_127 g_940 g_514 g_135 g_887 g_251 g_329 g_330 g_302 g_189 g_331 g_149 g_1028 g_114 g_886 g_800 g_539 g_1361 g_242 g_1364 g_1371 g_1405 g_564 g_1120 g_563 g_1429 g_1437 g_667 g_396 g_1438 g_1534 g_218 g_1494 g_288 g_289 g_1594 g_464 g_283
 * writes: g_24 g_62 g_78 g_82 g_166 g_802 g_127 g_584 g_737 g_676 g_213 g_940 g_744 g_302 g_218 g_800 g_114 g_251 g_1281 g_887 g_1361 g_242 g_602 g_368 g_1405 g_563 g_1437 g_135 g_1120 g_149
 */
static uint32_t  func_19(int8_t * p_20, int8_t  p_21, uint32_t  p_22)
{ /* block id: 3 */
    uint32_t l_45 = 8UL;
    int64_t *l_1600 = &g_737[2];
    const uint8_t *l_1611 = &g_114;
    const uint8_t **l_1610 = &l_1611;
    int32_t l_1624 = (-1L);
    int64_t ***** const l_1625 = &g_462[1][0];
    int16_t *l_1626 = (void*)0;
    int16_t *l_1627[9] = {&g_149,&g_149,&g_676,&g_149,&g_149,&g_676,&g_149,&g_149,&g_676};
    int32_t l_1628 = 0x34798B6CL;
    int32_t l_1672 = 0xF748F8E4L;
    int32_t l_1677[10][9][1] = {{{0xB70F0C9AL},{0x5596FD8EL},{(-1L)},{1L},{0xA50B333DL},{(-1L)},{0L},{(-1L)},{0x1A3FB6B4L}},{{0xA50B333DL},{0x2398FB94L},{7L},{0x2398FB94L},{0xA50B333DL},{0x1A3FB6B4L},{(-1L)},{0L},{(-1L)}},{{0xA50B333DL},{1L},{(-1L)},{0x5596FD8EL},{0xB70F0C9AL},{0x3020D2D9L},{(-1L)},{0x10853CC5L},{0xCB83EC34L}},{{7L},{1L},{0x3020D2D9L},{0x5534F8BBL},{0x1A3FB6B4L},{0xB3082C07L},{(-1L)},{(-1L)},{0xB3082C07L}},{{0x1A3FB6B4L},{0x5534F8BBL},{0x3020D2D9L},{1L},{7L},{0xCB83EC34L},{0x10853CC5L},{(-1L)},{0x3020D2D9L}},{{7L},{0L},{0xB3082C07L},{1L},{0x02BA007BL},{0xB3082C07L},{0xAFA39AF5L},{0x10853CC5L},{0x3020D2D9L}},{{0x02BA007BL},{0x5534F8BBL},{0xCB83EC34L},{0x5534F8BBL},{0x02BA007BL},{0x3020D2D9L},{0x10853CC5L},{0xAFA39AF5L},{0xB3082C07L}},{{0x02BA007BL},{1L},{0xB3082C07L},{0L},{7L},{0x3020D2D9L},{(-1L)},{0x10853CC5L},{0xCB83EC34L}},{{7L},{1L},{0x3020D2D9L},{0x5534F8BBL},{0x1A3FB6B4L},{0xB3082C07L},{(-1L)},{(-1L)},{0xB3082C07L}},{{0x1A3FB6B4L},{0x5534F8BBL},{0x3020D2D9L},{1L},{7L},{0xCB83EC34L},{0x10853CC5L},{(-1L)},{0x3020D2D9L}}};
    uint64_t ****l_1715[7][8][4] = {{{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,(void*)0,&g_1437,&g_1437},{(void*)0,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,&g_1437},{(void*)0,&g_1437,&g_1437,&g_1437},{&g_1437,(void*)0,&g_1437,&g_1437}},{{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,(void*)0,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,&g_1437}},{{&g_1437,(void*)0,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,(void*)0,&g_1437,&g_1437},{(void*)0,&g_1437,&g_1437,(void*)0}},{{&g_1437,&g_1437,&g_1437,&g_1437},{(void*)0,&g_1437,&g_1437,&g_1437},{&g_1437,(void*)0,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,(void*)0}},{{&g_1437,&g_1437,(void*)0,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,(void*)0,(void*)0},{&g_1437,(void*)0,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,(void*)0,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,(void*)0,&g_1437,&g_1437}},{{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,(void*)0,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,(void*)0,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437}},{{&g_1437,&g_1437,&g_1437,&g_1437},{&g_1437,(void*)0,&g_1437,(void*)0},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,(void*)0,&g_1437},{&g_1437,&g_1437,&g_1437,(void*)0},{&g_1437,&g_1437,(void*)0,(void*)0},{&g_1437,(void*)0,&g_1437,&g_1437},{&g_1437,&g_1437,&g_1437,&g_1437}}};
    int32_t l_1725 = 0x205BCC77L;
    uint16_t ***l_1808[7][7] = {{&g_887,&g_887,&g_887,&g_887,&g_887,&g_887,&g_887},{&g_887,&g_887,&g_887,&g_887,&g_887,&g_887,&g_887},{&g_887,&g_887,&g_887,&g_887,&g_887,&g_887,&g_887},{&g_887,&g_887,&g_887,&g_887,&g_887,&g_887,&g_887},{&g_887,&g_887,&g_887,&g_887,&g_887,&g_887,&g_887},{&g_887,&g_887,&g_887,&g_887,&g_887,&g_887,&g_887},{&g_887,&g_887,&g_887,&g_887,&g_887,&g_887,&g_887}};
    uint16_t ****l_1807 = &l_1808[3][2];
    const int8_t l_1810 = (-6L);
    uint16_t l_1848 = 0x518CL;
    uint32_t ***l_1895 = &g_876;
    int32_t l_1901[4] = {0L,0L,0L,0L};
    int8_t l_1940 = 0xE6L;
    int32_t l_1957 = 0xCD807180L;
    uint32_t *l_2043 = (void*)0;
    uint32_t ** const *l_2044 = &g_876;
    uint16_t *l_2068 = &g_248;
    uint8_t l_2150 = 0x22L;
    int32_t *l_2359 = (void*)0;
    const uint16_t l_2365 = 0UL;
    uint32_t l_2390 = 4294967295UL;
    uint8_t l_2420 = 255UL;
    const uint16_t l_2431[1] = {0x7BB3L};
    uint32_t **l_2502 = &g_289;
    uint32_t ***l_2501[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t l_2511 = 0x1DA26C01L;
    uint32_t l_2524 = 0xCC7DE1C9L;
    uint8_t l_2538 = 0UL;
    uint16_t l_2589[8][2] = {{0UL,65535UL},{65527UL,65535UL},{0UL,65535UL},{65527UL,65535UL},{0UL,65535UL},{65527UL,65535UL},{0UL,65535UL},{65527UL,65535UL}};
    const uint64_t l_2648 = 0UL;
    uint16_t l_2676 = 0xF5E0L;
    int64_t **l_2785[3][2][2] = {{{(void*)0,&l_1600},{(void*)0,&l_1600}},{{(void*)0,&l_1600},{(void*)0,&l_1600}},{{(void*)0,&l_1600},{(void*)0,&l_1600}}};
    int32_t *l_2820 = (void*)0;
    int32_t **l_2819 = &l_2820;
    int8_t l_2864[4][3];
    uint16_t l_2906 = 0UL;
    int i, j, k;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
            l_2864[i][j] = 0L;
    }
    (*g_367) = func_30((p_21 | func_33(((l_1600 = func_35((((safe_lshift_func_int8_t_s_s((func_40(((g_29 != (0xDDF34FFDL == ((safe_sub_func_int8_t_s_s(((*g_23) |= 0xFAL), (g_23 != &p_21))) == (p_21 , l_45)))) || (+(safe_mod_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u((safe_sub_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u((l_45 , (*p_20)), p_22)) && l_45), g_29)), g_24)), g_24)), p_22)))), p_22) <= p_22), 2)) == p_22) , (void*)0), (*g_464))) != (*g_464)))), p_22);
    (*g_368) &= (((**g_1438) = 1UL) || (p_22 && (**g_876)));
    if ((safe_sub_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s((*g_23), 6)), (safe_rshift_func_int32_t_s_u(((safe_add_func_uint16_t_u_u(((((l_1610 != &l_1611) > ((safe_sub_func_uint64_t_u_u((safe_div_func_int32_t_s_s((safe_add_func_int16_t_s_s((g_149 = (((*g_877) = p_22) , (((((**g_876) = (((l_1600 != l_1600) >= (**g_1364)) , (safe_mul_func_int8_t_s_s(((((g_1281 = &g_462[0][3]) == (((safe_lshift_func_uint8_t_u_s((safe_add_func_uint16_t_u_u((65535UL > l_1624), 1UL)), 7)) <= l_45) , l_1625)) , 0x26L) <= (*g_190)), l_1624)))) < l_45) && 65535UL) <= p_22))), p_21)), (-9L))), l_45)) == l_1628)) == p_21) || l_1624), p_21)) & p_21), l_45)))))
    { /* block id: 727 */
        uint16_t l_1629 = 0xB9C0L;
        uint64_t l_1636 = 4UL;
        int32_t l_1670 = 0xBEC2D3ACL;
        int32_t l_1680 = (-3L);
        int32_t l_1682[4];
        uint64_t l_1718 = 1UL;
        int16_t l_1723[5];
        uint16_t l_1739[9] = {65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL};
        int32_t *l_1770[4][2][7] = {{{(void*)0,&l_1682[1],(void*)0,(void*)0,(void*)0,(void*)0,&l_1682[1]},{(void*)0,&l_1682[1],&l_1682[3],&l_1682[3],&l_1682[1],(void*)0,&l_1682[1]}},{{(void*)0,(void*)0,(void*)0,(void*)0,&l_1682[1],(void*)0,(void*)0},{&l_1680,&l_1680,(void*)0,&l_1682[3],(void*)0,&l_1680,&l_1680}},{{&l_1680,(void*)0,&l_1682[3],(void*)0,&l_1680,&l_1680,(void*)0},{(void*)0,&l_1682[1],(void*)0,(void*)0,(void*)0,(void*)0,&l_1682[1]}},{{(void*)0,&l_1682[1],&l_1682[3],&l_1682[3],&l_1682[1],(void*)0,&l_1682[1]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_1682[1],(void*)0,(void*)0}}};
        uint32_t **l_1784[2];
        uint32_t ***l_1783 = &l_1784[1];
        uint32_t ****l_1782[2][5][8] = {{{&l_1783,&l_1783,(void*)0,&l_1783,&l_1783,&l_1783,&l_1783,(void*)0},{&l_1783,(void*)0,&l_1783,&l_1783,&l_1783,&l_1783,(void*)0,&l_1783},{(void*)0,&l_1783,&l_1783,&l_1783,(void*)0,&l_1783,(void*)0,&l_1783},{&l_1783,(void*)0,&l_1783,(void*)0,&l_1783,&l_1783,&l_1783,&l_1783},{&l_1783,&l_1783,&l_1783,&l_1783,&l_1783,&l_1783,&l_1783,&l_1783}},{{&l_1783,&l_1783,(void*)0,&l_1783,&l_1783,&l_1783,&l_1783,&l_1783},{&l_1783,(void*)0,&l_1783,&l_1783,(void*)0,&l_1783,(void*)0,(void*)0},{(void*)0,&l_1783,&l_1783,&l_1783,&l_1783,(void*)0,&l_1783,&l_1783},{&l_1783,&l_1783,&l_1783,(void*)0,&l_1783,&l_1783,&l_1783,&l_1783},{&l_1783,(void*)0,&l_1783,(void*)0,&l_1783,&l_1783,&l_1783,&l_1783}}};
        uint32_t *****l_1781 = &l_1782[0][3][2];
        uint32_t ****l_1804 = (void*)0;
        int32_t l_1840 = 1L;
        int64_t **l_1912 = &l_1600;
        uint64_t ***** const l_1945 = &l_1715[1][2][1];
        int64_t * const *l_1978 = (void*)0;
        int64_t * const **l_1977[4] = {&l_1978,&l_1978,&l_1978,&l_1978};
        int64_t * const ***l_1976 = &l_1977[3];
        uint64_t ***l_1990 = &g_1438;
        int16_t l_2062 = 0x238BL;
        int8_t l_2066 = 0L;
        int16_t *l_2134 = &g_676;
        uint8_t **l_2142 = &g_190;
        int64_t l_2211 = (-1L);
        int16_t l_2231 = 0x130CL;
        uint16_t l_2380 = 0x4DB8L;
        uint64_t l_2384 = 0UL;
        int8_t **l_2444 = &g_23;
        int64_t l_2526[8][9][3] = {{{0x719298037F3D1E7DLL,0xD945D53B164AEADBLL,0x652072F1D7CB5B6FLL},{0x0DFA86E48C6A7EAELL,8L,0xB77638222FE82150LL},{0xD945D53B164AEADBLL,(-1L),(-1L)},{0x01617E3CEDB52015LL,1L,0x3C65306BD755E538LL},{0xFA9791142BF4A9CFLL,0x9EF1CF844ED8ECEELL,0x652072F1D7CB5B6FLL},{0x4B7A3AED3EF12D64LL,0xCE4C8BB38C0F79CBLL,0x3103EE402FC24BD3LL},{0x4673FFDFA3877515LL,0L,0xFDF005D3D94BFEC4LL},{0L,0xCE4C8BB38C0F79CBLL,0x48F1D9C005DB3240LL},{(-4L),0x9EF1CF844ED8ECEELL,4L}},{{0xB77638222FE82150LL,1L,0x49E90ACFBF7B5BDBLL},{7L,(-1L),0x9CB1593AEE7E9482LL},{0x3C65306BD755E538LL,8L,4L},{(-4L),0xD945D53B164AEADBLL,0x05C0E06A67EA1BE2LL},{0x3103EE402FC24BD3LL,8L,0x2080C0C3E404C5C0LL},{0x4673FFDFA3877515LL,0x652072F1D7CB5B6FLL,0x05C0E06A67EA1BE2LL},{0x48F1D9C005DB3240LL,0xB1D2C0F4AEB97D69LL,4L},{0x4673FFDFA3877515LL,0xD945D53B164AEADBLL,(-1L)},{0xDAF6D627CF3F8CE4LL,0xB7A4D68258FFC12BLL,0xDAF6D627CF3F8CE4LL}},{{7L,0x9CB1593AEE7E9482LL,0xFDF005D3D94BFEC4LL},{4L,0xB1D2C0F4AEB97D69LL,0x01617E3CEDB52015LL},{0xFA9791142BF4A9CFLL,0xE1C9418E7A130ACFLL,0x0D722C878C84C850LL},{0x3C5535721BE7830CLL,8L,0x2080C0C3E404C5C0LL},{0xFA9791142BF4A9CFLL,(-1L),0L},{4L,8L,0x48F1D9C005DB3240LL},{7L,(-6L),0xB20226B50FFF6AF4LL},{0xDAF6D627CF3F8CE4LL,0xE5C9114784355638LL,0x4B7A3AED3EF12D64LL},{0x4673FFDFA3877515LL,0x05C0E06A67EA1BE2LL,0L}},{{0x01617E3CEDB52015LL,8L,0xC0BC5042F7866722LL},{0x1AE5059A09FAE641LL,0L,0x0D722C878C84C850LL},{0x2080C0C3E404C5C0LL,8L,0x49E90ACFBF7B5BDBLL},{0xE1C9418E7A130ACFLL,0x05C0E06A67EA1BE2LL,0xFDF005D3D94BFEC4LL},{0x48F1D9C005DB3240LL,0xE5C9114784355638LL,0x06FFBB3F4301ED44LL},{(-6L),(-6L),(-1L)},{0x4B7A3AED3EF12D64LL,8L,0x26AAB0778B0BB3C3LL},{0xE1C9418E7A130ACFLL,(-1L),4L},{0xC0BC5042F7866722LL,8L,0x1D61AFEE84A3AFCELL}},{{0x1AE5059A09FAE641LL,0xE1C9418E7A130ACFLL,4L},{0x49E90ACFBF7B5BDBLL,0xB1D2C0F4AEB97D69LL,0x26AAB0778B0BB3C3LL},{0x4673FFDFA3877515LL,0x9CB1593AEE7E9482LL,(-1L)},{0x06FFBB3F4301ED44LL,0xB7A4D68258FFC12BLL,0x06FFBB3F4301ED44LL},{7L,0xD945D53B164AEADBLL,0xFDF005D3D94BFEC4LL},{0x26AAB0778B0BB3C3LL,0xB1D2C0F4AEB97D69LL,0x49E90ACFBF7B5BDBLL},{0xFA9791142BF4A9CFLL,0L,0x0D722C878C84C850LL},{0x1D61AFEE84A3AFCELL,8L,0xC0BC5042F7866722LL},{0xFA9791142BF4A9CFLL,7L,0L}},{{0x26AAB0778B0BB3C3LL,8L,0x4B7A3AED3EF12D64LL},{7L,0xB20226B50FFF6AF4LL,0xB20226B50FFF6AF4LL},{0x06FFBB3F4301ED44LL,0xE5C9114784355638LL,0x48F1D9C005DB3240LL},{0x4673FFDFA3877515LL,0x719298037F3D1E7DLL,0L},{0x49E90ACFBF7B5BDBLL,8L,0x2080C0C3E404C5C0LL},{0x1AE5059A09FAE641LL,(-1L),0x0D722C878C84C850LL},{0xC0BC5042F7866722LL,8L,0x01617E3CEDB52015LL},{0xE1C9418E7A130ACFLL,0x719298037F3D1E7DLL,0xFDF005D3D94BFEC4LL},{0x4B7A3AED3EF12D64LL,0xE5C9114784355638LL,0xDAF6D627CF3F8CE4LL}},{{(-6L),0xB20226B50FFF6AF4LL,(-1L)},{0x48F1D9C005DB3240LL,8L,4L},{0xE1C9418E7A130ACFLL,7L,4L},{0x2080C0C3E404C5C0LL,8L,0x3C5535721BE7830CLL},{0x1AE5059A09FAE641LL,0L,4L},{0x01617E3CEDB52015LL,0xB1D2C0F4AEB97D69LL,4L},{0x4673FFDFA3877515LL,0xD945D53B164AEADBLL,(-1L)},{0xDAF6D627CF3F8CE4LL,0xB7A4D68258FFC12BLL,0xDAF6D627CF3F8CE4LL},{7L,0x9CB1593AEE7E9482LL,0xFDF005D3D94BFEC4LL}},{{4L,0xB1D2C0F4AEB97D69LL,0x01617E3CEDB52015LL},{0xFA9791142BF4A9CFLL,0xE1C9418E7A130ACFLL,0x0D722C878C84C850LL},{0x3C5535721BE7830CLL,8L,0x2080C0C3E404C5C0LL},{0xFA9791142BF4A9CFLL,(-1L),0L},{4L,8L,0x48F1D9C005DB3240LL},{7L,(-6L),0xB20226B50FFF6AF4LL},{0xDAF6D627CF3F8CE4LL,0xE5C9114784355638LL,0x4B7A3AED3EF12D64LL},{0x4673FFDFA3877515LL,0x05C0E06A67EA1BE2LL,0L},{0x01617E3CEDB52015LL,8L,0xC0BC5042F7866722LL}}};
        uint16_t l_2633 = 3UL;
        int64_t *l_2860 = &g_164;
        int64_t l_2876 = (-5L);
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1682[i] = 0xB25861EDL;
        for (i = 0; i < 5; i++)
            l_1723[i] = 1L;
        for (i = 0; i < 2; i++)
            l_1784[i] = &g_289;
    }
    else
    { /* block id: 1258 */
        int32_t *l_2904[9][3] = {{&g_82,&l_1672,&g_82},{&g_584[0],&g_1283,(void*)0},{&g_1283,&g_1283,(void*)0},{&l_1677[1][6][0],&g_1283,&g_82},{&g_563,&g_563,&g_584[0]},{&l_1677[1][6][0],&g_584[0],&g_1283},{&g_1283,&g_584[0],(void*)0},{&g_563,&g_166,&g_166},{&g_1283,&l_1672,(void*)0}};
        int8_t l_2905 = (-6L);
        int i, j;
        l_2359 = l_2904[4][0];
        ++l_2906;
        return p_22;
    }
    return p_21;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * const  func_30(uint32_t  p_31, uint64_t  p_32)
{ /* block id: 718 */
    int32_t * const l_1601[7] = {&g_584[0],&g_584[0],&g_82,&g_584[0],&g_584[0],&g_82,&g_584[0]};
    int i;
    return l_1601[1];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_33(const uint16_t  p_34)
{ /* block id: 716 */
    return p_34;
}


/* ------------------------------------------ */
/* 
 * reads : g_23 g_464 g_283
 * writes: g_24 g_368
 */
static int64_t * func_35(int64_t * p_36, int64_t * p_37)
{ /* block id: 710 */
    int32_t l_1597 = (-1L);
    int32_t l_1598 = 0xE00754E5L;
    int32_t **l_1599 = &g_368;
    l_1598 ^= (+((*g_23) = l_1597));
    (*l_1599) = &l_1597;
    return (*g_464);
}


/* ------------------------------------------ */
/* 
 * reads : g_62 g_82 g_877 g_876 g_802 g_190 g_584 g_23 g_24 g_368 g_367 g_127 g_940 g_514 g_135 g_887 g_251 g_329 g_330 g_302 g_189 g_331 g_149 g_1028 g_114 g_166 g_886 g_800 g_539 g_29 g_1361 g_242 g_1364 g_1371 g_1405 g_564 g_1120 g_563 g_1429 g_1437 g_667 g_396 g_1438 g_213 g_1534 g_218 g_1494 g_288 g_289 g_1594
 * writes: g_24 g_62 g_78 g_82 g_166 g_802 g_127 g_584 g_737 g_676 g_213 g_940 g_744 g_302 g_218 g_800 g_114 g_251 g_1281 g_887 g_1361 g_242 g_602 g_368 g_1405 g_563 g_1437 g_135 g_1120
 */
static int64_t  func_40(uint16_t  p_41, int16_t  p_42)
{ /* block id: 5 */
    int32_t l_57[1];
    int32_t l_59 = 0xBA4AEFC5L;
    int32_t l_60[3];
    uint32_t l_805 = 0x80852253L;
    int32_t l_807 = 8L;
    int32_t *l_809 = &g_584[0];
    int16_t l_843 = 1L;
    uint64_t l_861[9] = {0UL,0xAD93B6698D30D498LL,0xAD93B6698D30D498LL,0UL,0xAD93B6698D30D498LL,0xAD93B6698D30D498LL,0UL,0xAD93B6698D30D498LL,0xAD93B6698D30D498LL};
    uint8_t *l_1021 = &g_127;
    int32_t l_1036 = 0xBE32BA4CL;
    const int64_t * const l_1083[3] = {&g_29,&g_29,&g_29};
    int32_t l_1084 = (-5L);
    int8_t l_1086 = 0L;
    uint32_t ****l_1089 = (void*)0;
    uint8_t l_1113[4] = {253UL,253UL,253UL,253UL};
    uint16_t l_1152[8];
    int8_t l_1184 = 0x00L;
    int64_t *****l_1280 = &g_462[1][0];
    uint16_t *l_1282[6] = {&g_1234[0],&g_242,&g_1234[0],&g_1234[0],&g_242,&g_1234[0]};
    int32_t l_1286[6][10][4] = {{{0x7F8F1BBBL,0L,0x7F8F1BBBL,0xD0D6C362L},{0L,5L,(-8L),0L},{0L,1L,0x22004F8FL,5L},{0x6F9C9C80L,0x22004F8FL,0x22004F8FL,0x6F9C9C80L},{0L,0xD0D6C362L,(-8L),0x7F8F1BBBL},{0L,0x494EC68BL,0x7F8F1BBBL,0x2278B310L},{0x7F8F1BBBL,0x2278B310L,(-1L),0x2278B310L},{0x22004F8FL,0x494EC68BL,1L,0x7F8F1BBBL},{0x6D7082C6L,0xD0D6C362L,0x2278B310L,0x6F9C9C80L},{1L,0x22004F8FL,5L,5L}},{{1L,1L,0x2278B310L,0L},{0x6D7082C6L,5L,1L,0xD0D6C362L},{0x22004F8FL,0L,(-1L),1L},{0x7F8F1BBBL,0L,0x7F8F1BBBL,0xD0D6C362L},{0L,5L,(-8L),0L},{0L,1L,0x22004F8FL,5L},{0x6F9C9C80L,0x22004F8FL,0x22004F8FL,0x6F9C9C80L},{0L,0xD0D6C362L,(-8L),0x7F8F1BBBL},{0L,0x494EC68BL,0x7F8F1BBBL,0x2278B310L},{0x7F8F1BBBL,0x2278B310L,(-1L),0x2278B310L}},{{0x22004F8FL,0x494EC68BL,1L,0x7F8F1BBBL},{0x6D7082C6L,0xD0D6C362L,0x2278B310L,0x6F9C9C80L},{1L,0x22004F8FL,5L,5L},{1L,1L,0x2278B310L,0L},{0x6D7082C6L,5L,1L,0xD0D6C362L},{0x22004F8FL,0L,(-1L),1L},{0x7F8F1BBBL,0L,0x7F8F1BBBL,0xD0D6C362L},{0L,5L,(-8L),0L},{0L,1L,0x22004F8FL,5L},{0x6F9C9C80L,0x22004F8FL,0x22004F8FL,0x6F9C9C80L}},{{0L,0xD0D6C362L,(-8L),0x7F8F1BBBL},{0L,0x494EC68BL,0x7F8F1BBBL,0x2278B310L},{0x7F8F1BBBL,0x2278B310L,(-1L),0x2278B310L},{0x22004F8FL,0x494EC68BL,1L,0x7F8F1BBBL},{0x6D7082C6L,0xD0D6C362L,0x2278B310L,0x6F9C9C80L},{1L,0x22004F8FL,5L,5L},{1L,1L,0x2278B310L,0L},{0x6D7082C6L,5L,1L,0xD0D6C362L},{0x22004F8FL,0L,(-1L),0x2278B310L},{5L,0L,5L,0x494EC68BL}},{{0L,0x22004F8FL,(-1L),0xD0D6C362L},{0xD0D6C362L,0x2278B310L,0x6F9C9C80L,0x22004F8FL},{1L,0x6F9C9C80L,0x6F9C9C80L,1L},{0xD0D6C362L,0x494EC68BL,(-1L),5L},{0L,(-8L),5L,0L},{5L,0L,0x6D7082C6L,0L},{0x6F9C9C80L,(-8L),0x2278B310L,5L},{0x7F8F1BBBL,0x494EC68BL,0L,1L},{0x2278B310L,0x6F9C9C80L,0x22004F8FL,0x22004F8FL},{0x2278B310L,0x2278B310L,0L,0xD0D6C362L}},{{0x7F8F1BBBL,0x22004F8FL,0x2278B310L,0x494EC68BL},{0x6F9C9C80L,0L,0x6D7082C6L,0x2278B310L},{5L,0L,5L,0x494EC68BL},{0L,0x22004F8FL,(-1L),0xD0D6C362L},{0xD0D6C362L,0x2278B310L,0x6F9C9C80L,0x22004F8FL},{1L,0x6F9C9C80L,0x6F9C9C80L,1L},{0xD0D6C362L,0x494EC68BL,(-1L),5L},{0L,(-8L),5L,0L},{5L,0L,0x6D7082C6L,0L},{0x6F9C9C80L,(-8L),0x2278B310L,5L}}};
    const uint64_t *l_1392 = &g_135;
    const uint64_t **l_1391 = &l_1392;
    uint64_t * const *l_1435 = &g_330;
    uint64_t * const **l_1434 = &l_1435;
    int32_t l_1454 = 0x10FCDA9BL;
    const int32_t *l_1493 = &g_1494;
    uint16_t l_1595 = 0xA2C9L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_57[i] = 0xD7D552B8L;
    for (i = 0; i < 3; i++)
        l_60[i] = 0xA9177C83L;
    for (i = 0; i < 8; i++)
        l_1152[i] = 0xBFDCL;
lbl_937:
    for (g_24 = 0; (g_24 <= 0); g_24 += 1)
    { /* block id: 8 */
        int64_t l_76 = (-2L);
        int32_t *l_812[9][4] = {{&g_563,&l_59,&l_60[1],&l_60[1]},{&l_59,&l_59,&g_584[0],&l_60[1]},{&g_563,&l_59,&l_60[1],&l_60[1]},{&l_59,&l_59,&g_584[0],&l_60[1]},{&g_563,&l_59,&l_60[1],&l_60[1]},{&l_59,&l_59,&g_584[0],&l_60[1]},{&g_563,&l_59,&l_60[1],&l_60[1]},{&l_59,&l_59,&g_584[0],&l_60[1]},{&g_563,&l_59,&l_60[1],&l_60[1]}};
        uint64_t **l_899 = &g_514;
        uint32_t **l_901 = &g_289;
        uint32_t *** const l_900 = &l_901;
        int i, j;
        for (p_42 = 0; (p_42 <= 0); p_42 += 1)
        { /* block id: 11 */
            int32_t *l_58 = (void*)0;
            int32_t *l_61 = (void*)0;
            --g_62;
        }
        for (l_59 = 0; (l_59 <= 0); l_59 += 1)
        { /* block id: 16 */
            uint32_t *l_77 = &g_78;
            int64_t *l_79[7][7] = {{&l_76,&l_76,&l_76,&l_76,&l_76,&l_76,&l_76},{&g_29,&g_29,&l_76,&g_29,&g_29,&l_76,&g_29},{&l_76,&l_76,&l_76,&l_76,&l_76,&l_76,&l_76},{&g_29,&g_29,&g_29,&g_29,&g_29,&g_29,&g_29},{&l_76,&l_76,&l_76,&l_76,&l_76,&l_76,&l_76},{&g_29,&g_29,&g_29,&g_29,&g_29,&g_29,&g_29},{&l_76,&l_76,&l_76,&l_76,&l_76,&l_76,&l_76}};
            int32_t l_80[4][7][2] = {{{0x473CB3F0L,0x24E562A0L},{(-1L),0x473CB3F0L},{0x24E562A0L,0x92F697C7L},{1L,1L},{(-1L),1L},{1L,0x92F697C7L},{0x24E562A0L,0x473CB3F0L}},{{(-1L),0x24E562A0L},{0x473CB3F0L,0x92F697C7L},{0x473CB3F0L,0x24E562A0L},{(-1L),0x3B62B766L},{0x92F697C7L,(-5L)},{(-1L),(-1L)},{0x69A7C888L,(-1L)}},{{(-1L),(-5L)},{0x92F697C7L,0x3B62B766L},{0x69A7C888L,0x92F697C7L},{0x3B62B766L,(-5L)},{0x3B62B766L,0x92F697C7L},{0x69A7C888L,0x3B62B766L},{0x92F697C7L,(-5L)}},{{(-1L),(-1L)},{0x69A7C888L,(-1L)},{(-1L),(-5L)},{0x92F697C7L,0x3B62B766L},{0x69A7C888L,0x92F697C7L},{0x3B62B766L,(-5L)},{0x3B62B766L,0x92F697C7L}}};
            int32_t *l_81 = &g_82;
            uint32_t *l_799 = (void*)0;
            uint32_t *l_801 = &g_802;
            int8_t *l_806[2][10][3] = {{{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_218[1],(void*)0}}};
            int i, j, k;
            (*l_81) &= ((l_80[2][6][1] = (((safe_div_func_int32_t_s_s(0x2551AD04L, l_57[l_59])) | (safe_sub_func_uint32_t_u_u((l_57[l_59] >= ((*l_77) = (safe_div_func_int8_t_s_s(((0x65D282856065D6F9LL <= 1L) || (((!l_60[0]) > ((void*)0 != &g_24)) >= ((safe_mul_func_int8_t_s_s(p_42, l_76)) | 0x33L))), g_62)))), l_57[l_59]))) , 0x8BDD0C5DF97F7489LL)) && g_62);
        }
        if (p_42)
            continue;
        for (g_166 = 0; (g_166 <= 0); g_166 += 1)
        { /* block id: 393 */
            int8_t l_890[10][5][5] = {{{0xC0L,(-5L),0xD2L,0x45L,4L},{0x86L,0L,0x9BL,1L,0x0BL},{(-6L),(-5L),0x15L,(-5L),(-6L)},{0x0BL,1L,0x9BL,0L,0x86L},{4L,0x45L,0xD2L,(-5L),0xC0L}},{{0x86L,(-1L),(-1L),1L,0x86L},{(-10L),(-5L),0x67L,0x45L,(-6L)},{0x86L,0L,0x9BL,(-1L),0x0BL},{4L,(-5L),7L,(-5L),4L},{0x0BL,(-1L),0x9BL,0L,0x86L}},{{(-6L),0x45L,0x67L,(-5L),(-10L)},{0x86L,1L,(-1L),(-1L),0x86L},{0xC0L,(-5L),0xD2L,0x45L,4L},{0x86L,0L,0x9BL,1L,0x0BL},{(-6L),(-5L),0x15L,(-5L),(-6L)}},{{0x0BL,1L,0x9BL,0L,0x86L},{4L,0x45L,0xD2L,(-5L),0xC0L},{0x86L,(-1L),(-1L),1L,0x86L},{(-10L),(-5L),0x67L,0x45L,(-6L)},{0x86L,0L,0x08L,0xA1L,0x1EL}},{{0xD7L,(-9L),1L,(-9L),0xD7L},{0x1EL,0xA1L,0x08L,0L,0L},{0x43L,1L,0x72L,(-9L),0x5FL},{0L,0x86L,0L,0xA1L,0L},{0L,(-9L),1L,1L,0xD7L}},{{0L,(-9L),0x08L,0x86L,0x1EL},{0x43L,(-9L),0x1BL,(-9L),0x43L},{0x1EL,0x86L,0x08L,(-9L),0L},{0xD7L,1L,1L,(-9L),0L},{0L,0xA1L,0L,0x86L,0L}},{{0x5FL,(-9L),0x72L,1L,0x43L},{0L,0L,0x08L,0xA1L,0x1EL},{0xD7L,(-9L),1L,(-9L),0xD7L},{0x1EL,0xA1L,0x08L,0L,0L},{0x43L,1L,0x72L,(-9L),0x5FL}},{{0L,0x86L,0L,0xA1L,0L},{0L,(-9L),1L,1L,0xD7L},{0L,(-9L),0x08L,0x86L,0x1EL},{0x43L,(-9L),0x1BL,(-9L),0x43L},{0x1EL,0x86L,0x08L,(-9L),0L}},{{0xD7L,1L,1L,(-9L),0L},{0L,0xA1L,0L,0x86L,0L},{0x5FL,(-9L),0x72L,1L,0x43L},{0L,0L,0x08L,0xA1L,0x1EL},{0xD7L,(-9L),1L,(-9L),0xD7L}},{{0x1EL,0xA1L,0x08L,0L,0L},{0x43L,1L,0x72L,(-9L),0x5FL},{0L,0x86L,0L,0xA1L,0L},{0L,(-9L),1L,1L,0xD7L},{0L,(-9L),0x08L,0x86L,0x1EL}}};
            uint32_t ***l_892 = &g_876;
            uint8_t **l_898 = &g_190;
            int i, j, k;
            if (p_42)
            { /* block id: 394 */
                uint32_t ****l_893 = &l_892;
                (*l_809) = (((((safe_mod_func_int32_t_s_s(((p_42 | ((((*g_877) = p_42) < 1UL) || ((l_890[6][2][0] , (+((**l_898) = ((((*l_893) = l_892) == (void*)0) != ((**g_876) &= (safe_div_func_uint32_t_u_u(((l_898 == &g_190) == ((void*)0 == l_899)), p_42))))))) , 0x79E2L))) == 1L), l_890[1][0][2])) , &g_288) == l_900) < p_42) & p_42);
            }
            else
            { /* block id: 400 */
                int64_t *l_902 = &g_737[5];
                int32_t l_909 = 0x606DCC45L;
                int16_t *l_910 = &g_676;
                (*g_368) &= (0x8A29L != ((*l_910) = (0xF48FL || ((0x2B322796972602C9LL == ((*l_902) = (*l_809))) , ((**g_876) || (((void*)0 == &l_76) && ((((((safe_unary_minus_func_uint32_t_u((safe_sub_func_uint32_t_u_u(0x3331B8AFL, (safe_unary_minus_func_uint64_t_u(((safe_sub_func_uint16_t_u_u(p_42, l_909)) <= 0L))))))) < (*g_23)) | 1L) || 0xC9L) >= p_42) , p_42)))))));
                for (l_59 = 0; (l_59 <= 0); l_59 += 1)
                { /* block id: 406 */
                    int32_t *l_911 = &g_563;
                    l_911 = (*g_367);
                }
            }
        }
    }
lbl_1293:
    for (g_213 = 0; (g_213 != 55); ++g_213)
    { /* block id: 414 */
        uint64_t l_931 = 0UL;
        int32_t l_949 = 1L;
        uint16_t *l_964[10][3][3] = {{{&g_242,&g_242,&g_248},{&g_242,(void*)0,&g_242},{&g_248,&g_242,&g_242}},{{&g_248,&g_242,&g_242},{&g_242,(void*)0,&g_242},{&g_242,&g_242,&g_242}},{{&g_248,&g_248,&g_242},{(void*)0,&g_248,&g_242},{(void*)0,(void*)0,&g_248}},{{(void*)0,&g_248,(void*)0},{&g_248,&g_242,&g_242},{&g_242,&g_242,&g_242}},{{&g_248,(void*)0,&g_242},{(void*)0,(void*)0,&g_248},{&g_242,&g_248,&g_242}},{{&g_248,(void*)0,&g_248},{(void*)0,&g_242,&g_242},{&g_248,(void*)0,(void*)0}},{{&g_248,&g_248,&g_242},{&g_248,&g_242,&g_242},{(void*)0,(void*)0,(void*)0}},{{&g_248,&g_242,(void*)0},{&g_242,(void*)0,&g_248},{(void*)0,&g_242,&g_248}},{{&g_248,&g_248,&g_248},{&g_248,(void*)0,&g_248},{&g_242,&g_242,&g_248}},{{&g_248,(void*)0,(void*)0},{&g_242,&g_248,(void*)0},{&g_248,(void*)0,&g_242}}};
        uint32_t l_970 = 7UL;
        uint32_t **l_991[7][10][2] = {{{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0}},{{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877}},{{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877}},{{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877}},{{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877}},{{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877}},{{&g_877,&g_877},{&g_877,&g_877},{(void*)0,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,(void*)0},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877},{&g_877,&g_877}}};
        uint8_t *l_1018 = &g_114;
        uint8_t *l_1022 = &g_127;
        int8_t l_1023[7] = {0x18L,0x18L,0x18L,0x18L,0x18L,0x18L,0x18L};
        int16_t l_1025 = 0xC35DL;
        const int8_t l_1039 = 0x72L;
        int64_t l_1085 = 0x8B8068BE969F7172LL;
        uint32_t *l_1119 = &g_1120;
        int32_t l_1141 = 1L;
        int32_t l_1145 = 0L;
        int32_t l_1155 = (-7L);
        uint8_t l_1156[3];
        int32_t l_1161 = 2L;
        int32_t l_1163 = 0x9B36B6BFL;
        uint8_t l_1206 = 251UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1156[i] = 254UL;
        for (l_843 = (-29); (l_843 == (-1)); l_843 = safe_add_func_uint64_t_u_u(l_843, 3))
        { /* block id: 417 */
            uint32_t l_936[8];
            int32_t l_939 = 0L;
            uint16_t *l_968 = &g_248;
            uint32_t * const *l_990[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            uint32_t l_1024 = 2UL;
            int8_t l_1027 = 0xD2L;
            int64_t *l_1033[7][7] = {{(void*)0,&g_737[1],&g_29,&g_29,&g_737[1],(void*)0,&g_737[1]},{(void*)0,(void*)0,(void*)0,(void*)0,&g_737[1],(void*)0,(void*)0},{&g_737[4],&g_737[4],(void*)0,&g_29,(void*)0,&g_737[4],&g_737[4]},{&g_737[4],(void*)0,&g_29,(void*)0,&g_737[4],&g_737[4],(void*)0},{(void*)0,&g_737[1],(void*)0,(void*)0,(void*)0,(void*)0,&g_737[1]},{(void*)0,&g_737[1],&g_29,&g_29,&g_737[1],(void*)0,&g_737[1]},{(void*)0,(void*)0,(void*)0,(void*)0,&g_737[1],(void*)0,(void*)0}};
            uint8_t *l_1042 = &g_251[2][0];
            int32_t l_1045[7][9] = {{(-2L),5L,(-2L),(-2L),5L,(-2L),(-2L),5L,(-2L)},{1L,0xBFAD58B1L,1L,0x7D9439D0L,0x1C0E3683L,0x7D9439D0L,1L,0xBFAD58B1L,1L},{(-2L),5L,(-2L),(-2L),5L,(-2L),(-2L),5L,(-2L)},{1L,0xBFAD58B1L,1L,0x7D9439D0L,0x1C0E3683L,0x7D9439D0L,1L,0xBFAD58B1L,1L},{(-2L),5L,(-2L),(-2L),5L,(-2L),(-2L),5L,(-2L)},{1L,0xBFAD58B1L,1L,0x7D9439D0L,0x1C0E3683L,0x7D9439D0L,1L,0xBFAD58B1L,1L},{(-2L),5L,(-2L),(-2L),5L,(-2L),(-2L),5L,(-2L)}};
            int i, j;
            for (i = 0; i < 8; i++)
                l_936[i] = 0UL;
            for (p_41 = 0; (p_41 > 19); p_41++)
            { /* block id: 420 */
                int32_t l_918 = 0x5F5B15E8L;
                if (l_918)
                    break;
                (*g_368) |= l_918;
            }
            if (((2UL <= ((safe_div_func_uint64_t_u_u(0x0DAD227A274F8CC1LL, (safe_mod_func_int16_t_s_s(((safe_div_func_uint8_t_u_u((((((safe_rshift_func_int16_t_s_u(((safe_add_func_uint8_t_u_u(p_41, ((safe_mul_func_int64_t_s_s(((*l_809) &= l_931), ((safe_div_func_uint8_t_u_u(((*g_190) || (safe_div_func_uint64_t_u_u((((((0x581691E9527B88D5LL || (0xDC9DL && (p_42 ^ 0xE1CFC337L))) , 2UL) | (**g_876)) , p_41) & 251UL), p_41))), l_936[2])) <= (*g_23)))) && 0xD215L))) > l_931), 9)) <= 255UL) | (*g_190)) <= (*g_190)) == 0x1B0DAD9982DB7443LL), 0xE5L)) & 0x2EL), p_41)))) == 18446744073709551608UL)) || 0x68E0L))
            { /* block id: 425 */
                int32_t *l_938[5][4] = {{&l_59,&g_584[0],&l_807,&l_807},{&g_584[0],&g_584[0],(void*)0,&l_59},{&g_584[0],&g_584[0],&l_807,&g_584[0]},{&l_59,&l_59,&l_59,&l_807},{&g_584[0],&l_59,(void*)0,&g_584[0]}};
                const int8_t *l_948 = &g_24;
                int i, j;
                if (l_931)
                    goto lbl_937;
                g_940[2]--;
                if ((safe_rshift_func_uint8_t_u_u(0xBCL, ((safe_rshift_func_int32_t_s_u((l_59 = ((*g_368) = ((*l_809) = (-1L)))), 28)) , l_931))))
                { /* block id: 431 */
                    int8_t l_947 = (-4L);
                    for (g_127 = 0; (g_127 <= 0); g_127 += 1)
                    { /* block id: 434 */
                        (*l_809) = (l_947 || p_42);
                    }
                    l_938[4][3] = (void*)0;
                }
                else
                { /* block id: 438 */
                    l_949 = (l_948 != (void*)0);
                    if (p_41)
                        continue;
                }
            }
            else
            { /* block id: 442 */
                uint16_t l_959[1][5] = {{65535UL,65535UL,65535UL,65535UL,65535UL}};
                uint16_t **l_965 = (void*)0;
                uint16_t **l_966 = (void*)0;
                uint16_t **l_967 = &l_964[1][2][2];
                int8_t *l_969 = &g_218[1];
                int64_t *l_971 = (void*)0;
                int32_t l_972 = 6L;
                int8_t *l_973 = (void*)0;
                int8_t *l_974 = &g_800;
                int32_t l_975[2][6][9] = {{{0L,0L,0L,0L,0xFC90E08DL,0L,0L,0L,0L},{(-1L),(-5L),4L,(-5L),(-1L),(-1L),(-5L),4L,(-5L)},{0L,0xFC90E08DL,1L,1L,0xFC90E08DL,0L,0xFC90E08DL,1L,1L},{(-1L),(-1L),(-5L),4L,(-5L),(-1L),(-1L),(-5L),4L},{0L,0xCA22B033L,1L,0L,0L,1L,0xCA22B033L,1L,0L},{4L,(-1L),(-1L),4L,(-1L),4L,(-1L),(-1L),4L}},{{0L,0L,0xFC90E08DL,0L,0L,0L,0L,0xFC90E08DL,0L},{(-1L),(-1L),0x327C47EDL,0x327C47EDL,(-1L),(-1L),(-1L),0x327C47EDL,0x327C47EDL},{0L,0L,0L,0xFC90E08DL,0L,0L,0L,0L,0xFC90E08DL},{4L,(-1L),4L,(-1L),(-1L),4L,(-1L),4L,(-1L)},{1L,0L,0L,1L,0xCA22B033L,1L,0L,0L,1L},{(-5L),(-1L),0x327C47EDL,(-1L),(-5L),(-5L),(-1L),0x327C47EDL,(-1L)}}};
                uint8_t **l_1019 = &l_1018;
                uint8_t **l_1020[8] = {&g_190,&g_190,&g_190,&g_190,&g_190,&g_190,&g_190,&g_190};
                uint16_t l_1026 = 65535UL;
                int i, j, k;
                l_975[0][5][1] = (~(((*l_974) = ((((p_41 > (**g_367)) != ((l_939 = (l_972 = ((safe_mod_func_uint16_t_u_u(7UL, (safe_div_func_uint64_t_u_u((*g_514), (safe_rshift_func_int8_t_s_u(((safe_rshift_func_int8_t_s_s(((*l_969) = (l_959[0][2] <= (safe_sub_func_uint32_t_u_u(((((*g_23) |= 0x54L) , ((**g_329) = (((-1L) && (((((safe_lshift_func_uint8_t_u_u((((*g_887) = &l_959[0][2]) != (l_968 = ((*l_967) = l_964[1][2][2]))), l_939)) != g_251[3][0]) , l_936[2]) >= l_959[0][2]) < 1L)) != (**g_329)))) & 18446744073709551612UL), 0xB8C5C897L)))), 6)) & (*g_190)), l_970)))))) != 0x36779BF6056A9908LL))) <= l_959[0][2])) | 0x1DF6ADB6L) != (*l_809))) == 6UL));
                (*g_1028) = ((*g_368) = ((safe_mul_func_uint16_t_u_u((((safe_mul_func_int8_t_s_s(0xD7L, (safe_mod_func_int32_t_s_s(((l_1027 &= (safe_mul_func_int8_t_s_s(((safe_mod_func_int16_t_s_s(((safe_lshift_func_uint64_t_u_u((safe_div_func_int32_t_s_s((l_990[1] != l_991[3][6][1]), ((*l_809) = (~((safe_div_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s((((((safe_sub_func_uint32_t_u_u((safe_add_func_uint64_t_u_u(((((safe_sub_func_uint16_t_u_u((((l_1023[6] = (9L & ((p_41 , l_974) == ((safe_mul_func_int8_t_s_s(((*g_23) = (safe_add_func_int64_t_s_s(((safe_lshift_func_uint32_t_u_u(((safe_mod_func_uint32_t_u_u((safe_unary_minus_func_int64_t_s((safe_add_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(((safe_div_func_uint64_t_u_u(((l_1021 = ((*l_1019) = l_1018)) == l_1022), (*g_514))) && (*g_877)), 0xF0A0L)), (**g_189))))), 0xBD4EBC1CL)) | p_41), p_42)) , l_959[0][1]), p_42))), (*g_190))) , l_1022)))) >= 5UL) & (*l_809)), 0x9464L)) | (*l_809)) , 0xDBL) , (***g_331)), l_975[0][5][1])), (-1L))) < l_970) < p_42) , p_42) | p_42), 3)), g_149)) <= 0x03L))))), l_931)) ^ l_939), l_1024)) < l_1025), l_1026))) >= l_975[0][5][1]), p_42)))) >= p_41) && 0x23848456L), p_42)) & l_936[2]));
            }
            (*g_368) ^= ((*l_809) | ((safe_lshift_func_uint32_t_u_s((safe_mul_func_uint32_t_u_u(((l_939 = (4294967295UL | (*g_877))) , l_1024), (*g_877))), 5)) < (((((safe_lshift_func_int64_t_s_s(l_931, 20)) == l_1036) , (void*)0) != (void*)0) & p_42)));
            l_949 ^= (((p_42 , (safe_mul_func_uint16_t_u_u(l_1039, (((*g_877) = (4UL > p_41)) || ((*l_809) = (0x2DE5L <= (l_1045[0][6] |= (l_939 &= (((*l_1021)--) > (((*l_1042)--) , ((*l_809) || (-1L)))))))))))) , 0x36EC1D1A8499DCCCLL) && (safe_rshift_func_int64_t_s_s((safe_sub_func_int8_t_s_s((0xA16F4DB532BB791FLL == 0x35D73296F8A165DDLL), 6UL)), p_42)));
        }
        if (p_42)
            break;
        for (l_1025 = 0; (l_1025 <= (-15)); l_1025--)
        { /* block id: 475 */
            int32_t l_1065[3][9][6] = {{{0x960A6E9EL,0x31D03EA1L,0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL},{(-1L),(-1L),0xCC1AD08BL,0x62939D6CL,0xCC1AD08BL,(-1L)},{0xCC1AD08BL,0x31D03EA1L,0x62939D6CL,0x62939D6CL,0x31D03EA1L,0xCC1AD08BL},{(-1L),0xCC1AD08BL,0x62939D6CL,0xCC1AD08BL,(-1L),(-1L)},{0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL,0x31D03EA1L,0x960A6E9EL},{0x960A6E9EL,0x31D03EA1L,0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL},{(-1L),(-1L),0xCC1AD08BL,0x62939D6CL,0xCC1AD08BL,(-1L)},{0xCC1AD08BL,0x31D03EA1L,0x62939D6CL,0x62939D6CL,0x31D03EA1L,0xCC1AD08BL},{(-1L),0xCC1AD08BL,0x62939D6CL,0xCC1AD08BL,(-1L),(-1L)}},{{0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL,0x31D03EA1L,0x960A6E9EL},{0x960A6E9EL,0x31D03EA1L,0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL},{(-1L),(-1L),0xCC1AD08BL,0x62939D6CL,0xCC1AD08BL,(-1L)},{0xCC1AD08BL,0x31D03EA1L,0x62939D6CL,0x62939D6CL,0x31D03EA1L,0xCC1AD08BL},{(-1L),0xCC1AD08BL,0x62939D6CL,0xCC1AD08BL,(-1L),(-1L)},{0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL,0x31D03EA1L,0x960A6E9EL},{0x960A6E9EL,0x31D03EA1L,0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL},{(-1L),(-1L),0xCC1AD08BL,0x62939D6CL,0xCC1AD08BL,(-1L)},{0xCC1AD08BL,0x31D03EA1L,0x62939D6CL,0x62939D6CL,0x31D03EA1L,0xCC1AD08BL}},{{(-1L),0xCC1AD08BL,0x62939D6CL,0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL},{0x62939D6CL,0x960A6E9EL,0x960A6E9EL,0x62939D6CL,(-1L),0x62939D6CL},{0x62939D6CL,(-1L),0x62939D6CL,0x960A6E9EL,0x960A6E9EL,0x62939D6CL},{0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL,0x31D03EA1L,0x960A6E9EL,0xCC1AD08BL},{0x960A6E9EL,(-1L),0x31D03EA1L,0x31D03EA1L,(-1L),0x960A6E9EL},{0xCC1AD08BL,0x960A6E9EL,0x31D03EA1L,0x960A6E9EL,0xCC1AD08BL,0xCC1AD08BL},{0x62939D6CL,0x960A6E9EL,0x960A6E9EL,0x62939D6CL,(-1L),0x62939D6CL},{0x62939D6CL,(-1L),0x62939D6CL,0x960A6E9EL,0x960A6E9EL,0x62939D6CL},{0xCC1AD08BL,0xCC1AD08BL,0x960A6E9EL,0x31D03EA1L,0x960A6E9EL,0xCC1AD08BL}}};
            int32_t l_1074 = 0x65C73F9FL;
            int32_t *l_1109 = &l_59;
            int32_t l_1112 = 0xF5A62AD1L;
            int64_t ****l_1128[4][3] = {{&g_463,&g_463,&g_463},{&g_455,(void*)0,&g_455},{&g_463,&g_463,&g_463},{&g_455,(void*)0,&g_455}};
            int32_t l_1160[1];
            uint64_t l_1171 = 0x2EE56BB6C4623F70LL;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_1160[i] = (-1L);
            if (g_149)
                goto lbl_937;
            for (l_1036 = 0; (l_1036 == 22); l_1036 = safe_add_func_int32_t_s_s(l_1036, 9))
            { /* block id: 479 */
                int16_t l_1061 = 0xEA4AL;
                int32_t l_1140 = 1L;
                int32_t l_1142 = 1L;
                int32_t l_1143 = 0x414476EDL;
                int32_t l_1146 = 1L;
                int32_t l_1148[4][8][8] = {{{1L,1L,1L,4L,(-4L),1L,1L,(-1L)},{0L,(-1L),(-6L),(-10L),0x187F0EA7L,1L,(-2L),1L},{0x92C654E8L,(-2L),(-5L),0xC929653EL,0L,1L,1L,1L},{0L,(-4L),1L,1L,(-4L),0L,0x1985C9E4L,0x92C654E8L},{0xA92E5F8AL,(-6L),(-5L),1L,0xD8DC20B2L,0xC0885419L,0xA8791DDEL,(-1L)},{(-1L),(-10L),1L,1L,0x8557EE16L,0L,0xC599C4EAL,0x92C654E8L},{(-1L),0x8557EE16L,1L,1L,0L,0x5CC0BEB5L,0xD8DC20B2L,1L},{0L,(-5L),0xC0885419L,0xC929653EL,4L,(-6L),0L,1L}},{{1L,1L,0L,(-10L),(-1L),(-9L),0L,(-1L)},{4L,1L,1L,4L,1L,(-1L),0x6FF2E2EEL,0L},{(-10L),0xC0885419L,0x1985C9E4L,1L,(-1L),1L,0xA8791DDEL,3L},{0xC929653EL,0x887CB75EL,0L,0L,1L,(-1L),1L,0L},{1L,1L,1L,(-1L),(-2L),0xD8DC20B2L,4L,1L},{1L,0x1985C9E4L,(-1L),(-1L),1L,(-6L),(-2L),0xC0885419L},{1L,0x92C654E8L,1L,0xA92E5F8AL,(-2L),0xD5360823L,0xA92E5F8AL,0x059E7E80L},{1L,0x8557EE16L,1L,0L,1L,(-5L),0x187F0EA7L,0xC599C4EAL}},{{0x187F0EA7L,(-9L),5L,(-6L),0x69409EAAL,(-5L),8L,0x8BCC912AL},{0xD8DC20B2L,0x5CC0BEB5L,0L,1L,1L,0x8557EE16L,(-1L),0x5CC0BEB5L},{0x6E288B00L,0x6FF2E2EEL,1L,0L,(-1L),0L,0xD5360823L,(-1L)},{8L,5L,0x8BCC912AL,(-1L),0x6E288B00L,1L,0x5CC0BEB5L,6L},{(-1L),(-1L),1L,(-5L),(-5L),1L,(-1L),(-1L)},{0L,1L,1L,8L,(-7L),0L,0x7DE19621L,0x187F0EA7L},{(-1L),0x8BCC912AL,5L,8L,8L,0L,4L,0x9E2D1AE8L},{0xC0885419L,1L,0xC599C4EAL,1L,0x6FF2E2EEL,1L,0xC0885419L,(-1L)}},{{0x9E2D1AE8L,(-1L),0xD5360823L,(-9L),4L,1L,5L,(-7L)},{(-6L),5L,(-9L),0x187F0EA7L,(-1L),0L,1L,0x6E288B00L},{1L,0x6FF2E2EEL,(-1L),(-7L),0x6FF2E2EEL,0x8557EE16L,0L,(-6L)},{0L,0x5CC0BEB5L,1L,1L,(-1L),(-5L),1L,1L},{(-1L),(-9L),6L,1L,9L,1L,3L,1L},{(-5L),(-7L),0xB9E9E083L,(-7L),(-5L),(-4L),(-1L),1L},{8L,(-6L),(-5L),0x05A89ED1L,1L,0xD5360823L,0x9E2D1AE8L,(-7L)},{8L,6L,(-5L),0xD8DC20B2L,(-1L),8L,(-1L),(-1L)}}};
                int64_t l_1151[3];
                int32_t l_1162 = 0x987FA2F6L;
                int32_t *l_1227 = &g_563;
                int32_t *l_1228 = &l_1143;
                int32_t *l_1229 = &l_60[1];
                int32_t *l_1230 = &l_1084;
                int32_t *l_1231 = &l_1143;
                int32_t *l_1232 = &l_807;
                int32_t *l_1233[9][9][3] = {{{(void*)0,&l_1163,&l_1112},{&l_1084,&g_584[0],&g_584[0]},{&g_584[0],&l_1160[0],&g_166},{(void*)0,&g_584[0],&l_1112},{&g_82,(void*)0,&g_584[0]},{(void*)0,&l_1143,&g_82},{(void*)0,&g_584[0],&l_1148[1][7][1]},{&l_60[1],&g_584[0],&l_1145},{&l_1155,&l_1112,&l_1163}},{{&l_1160[0],(void*)0,&l_1155},{&g_584[0],&l_949,&l_949},{&l_1112,(void*)0,&l_1148[1][7][1]},{&l_1160[0],&g_584[0],&l_1112},{&l_1074,&l_59,&g_584[0]},{&l_1163,&g_584[0],&l_1074},{(void*)0,&l_59,&l_1148[1][7][1]},{(void*)0,&g_584[0],&l_1163},{&l_1074,(void*)0,&l_60[1]}},{{&l_60[1],&l_949,&l_807},{(void*)0,(void*)0,(void*)0},{&g_82,&l_1112,(void*)0},{&g_584[0],&l_807,&g_584[1]},{&l_1160[0],&l_1140,&l_1142},{(void*)0,&l_59,&l_1155},{&l_60[1],&l_60[2],&g_584[0]},{&g_166,&l_1112,&l_60[1]},{&l_1141,&g_584[0],&l_60[1]}},{{&l_1140,&l_1112,(void*)0},{&l_1160[0],&l_1160[0],&l_1112},{&l_1161,(void*)0,&l_949},{&l_1163,&l_1160[0],&l_1145},{&g_584[0],&l_1155,&l_1084},{&l_1160[0],&g_563,&g_584[1]},{&l_60[1],&g_82,(void*)0},{(void*)0,&g_166,&l_59},{(void*)0,&g_166,&l_1160[0]}},{{&l_60[1],&l_1112,&g_166},{&l_1160[0],(void*)0,&l_1074},{&g_584[0],&l_1074,&l_1160[0]},{&l_1163,&g_584[0],(void*)0},{&l_1161,&l_1148[1][7][1],&l_1160[0]},{&l_1160[0],&l_1155,&l_1140},{&l_1140,&l_60[1],(void*)0},{&l_1141,&l_1161,&g_584[1]},{&g_166,&l_59,&g_584[0]}},{{&l_60[1],(void*)0,&l_1160[0]},{(void*)0,&l_1112,&l_1141},{&l_1160[0],&l_1112,(void*)0},{&g_584[0],&l_1145,(void*)0},{&l_1160[0],(void*)0,&l_1148[1][7][1]},{&l_1074,&l_60[0],&l_1148[1][7][1]},{&l_1161,&l_1161,&l_1143},{&l_1142,&l_1112,&g_584[0]},{&l_1145,&g_584[0],&g_82}},{{&l_1143,&l_1143,&g_584[1]},{(void*)0,&l_1145,&g_82},{&l_1142,&l_1148[1][7][1],&g_584[0]},{&l_1160[0],&l_1146,&l_1143},{&l_1084,&l_1112,&l_1148[1][7][1]},{&l_1155,&l_1160[0],&l_1148[1][7][1]},{&l_59,&l_1142,(void*)0},{&l_1148[1][7][1],&g_82,(void*)0},{&l_1161,&l_1163,&l_1141}},{{(void*)0,&l_1140,&l_1160[0]},{&g_82,&l_1141,&g_584[0]},{&l_1160[0],(void*)0,&g_584[1]},{&l_1148[1][7][1],&l_59,(void*)0},{&l_1112,(void*)0,&l_1140},{&l_1148[1][7][1],&l_60[0],&l_1160[0]},{(void*)0,&l_1112,(void*)0},{&l_60[1],&g_82,&l_1160[0]},{&l_1155,(void*)0,&l_1074}},{{&l_1160[0],&l_1160[0],&g_166},{&l_1161,&l_1148[1][7][1],&l_1160[0]},{&g_584[0],&l_1160[0],&l_59},{&l_1160[0],&l_1160[0],(void*)0},{&l_949,&l_1148[1][7][1],&g_584[1]},{(void*)0,&l_1160[0],&l_1084},{&l_1163,(void*)0,&l_1145},{(void*)0,&g_82,&l_949},{&l_59,&l_1112,&l_1112}}};
                uint8_t *l_1247[5];
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_1151[i] = (-1L);
                for (i = 0; i < 5; i++)
                    l_1247[i] = (void*)0;
            }
        }
    }
    if (((safe_mod_func_int8_t_s_s(((*g_23) = 0x2FL), (((((safe_rshift_func_int16_t_s_u(((p_42 ^ ((g_1281 = l_1280) != l_1280)) <= p_42), (--p_41))) == (*l_809)) , (***g_331)) > (l_1286[0][9][0] = p_42)) & ((safe_div_func_int16_t_s_s((((safe_add_func_int32_t_s_s((safe_mul_func_int16_t_s_s((*l_809), (*l_809))), 2L)) & 0x89L) || (*l_809)), g_114)) < (**g_876))))) != 65535UL))
    { /* block id: 555 */
        uint32_t *****l_1295 = &l_1089;
        uint32_t **l_1298 = (void*)0;
        uint32_t ***l_1297 = &l_1298;
        uint32_t ****l_1296 = &l_1297;
        int32_t l_1299 = 0x369009F5L;
        int32_t l_1300 = (-1L);
        int32_t l_1301[5];
        uint8_t *l_1316 = &l_1113[3];
        int32_t l_1356 = 0x2F4A08DFL;
        int64_t l_1360 = (-1L);
        int32_t *l_1431 = &g_82;
        int32_t *l_1458 = &g_584[0];
        uint64_t ****l_1473[7][2] = {{&g_1437,&g_1437},{&g_1437,&g_1437},{&g_1437,&g_1437},{&g_1437,&g_1437},{&g_1437,&g_1437},{&g_1437,&g_1437},{&g_1437,&g_1437}};
        int i, j;
        for (i = 0; i < 5; i++)
            l_1301[i] = 0x5776B3EEL;
lbl_1345:
        if ((*l_809))
        { /* block id: 556 */
            if (g_24)
                goto lbl_1293;
        }
        else
        { /* block id: 558 */
            int32_t l_1294 = 0x1A6F71EAL;
            return l_1294;
        }
        if ((((*l_1295) = l_1089) != l_1296))
        { /* block id: 562 */
            uint16_t ***l_1311 = &g_887;
            uint8_t **l_1317 = &l_1316;
            int32_t l_1318[2];
            uint32_t l_1357 = 0xA5D0CA09L;
            int32_t l_1389[3][1];
            uint64_t * const *l_1393 = &g_330;
            int i, j;
            for (i = 0; i < 2; i++)
                l_1318[i] = 0x36AECCA1L;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1389[i][j] = (-1L);
            }
            if (((*l_809) |= (((((*g_1028) , (p_41++)) , ((safe_div_func_uint32_t_u_u((safe_mod_func_uint16_t_u_u(((+((**g_329) && (safe_div_func_int8_t_s_s((((*l_1311) = (*g_886)) != (((safe_mul_func_uint16_t_u_u(0x7A66L, ((((safe_mod_func_uint16_t_u_u((((*l_1317) = l_1316) != (void*)0), l_1301[0])) , l_1089) != ((65528UL | g_802) , (void*)0)) != 0x5AL))) < p_41) , &g_744[3][5])), l_1318[1])))) , p_41), p_42)), 0x33B405EFL)) <= g_800)) < (*g_23)) , (*g_539))))
            { /* block id: 567 */
                int8_t l_1341 = 0x85L;
                int32_t l_1344 = 0xA632B2E4L;
                int32_t *l_1347 = &g_584[0];
                int32_t *l_1348 = &l_1344;
                int32_t *l_1349 = &l_807;
                int32_t *l_1350 = (void*)0;
                int32_t *l_1351 = &l_1299;
                int32_t *l_1352 = &g_1283;
                int32_t *l_1353 = (void*)0;
                int32_t *l_1354[3][7] = {{&g_1283,&l_60[1],&l_1301[1],&g_82,&g_82,&l_1301[1],&l_60[1]},{&g_1283,&l_60[1],&l_1301[1],&g_82,&g_82,&l_1301[1],&l_60[1]},{&g_1283,&l_60[1],&l_1301[1],&g_82,&g_82,&l_1301[1],&l_60[1]}};
                int8_t l_1355 = (-5L);
                int i, j;
                for (l_1036 = 0; (l_1036 >= 4); ++l_1036)
                { /* block id: 570 */
                    const uint16_t l_1326 = 0x0297L;
                    uint64_t l_1340[1][6] = {{2UL,2UL,2UL,2UL,2UL,2UL}};
                    uint32_t l_1346[1];
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_1346[i] = 0xFA34CFAFL;
                    for (l_1184 = 0; (l_1184 > 27); l_1184 = safe_add_func_uint8_t_u_u(l_1184, 9))
                    { /* block id: 573 */
                        uint16_t **l_1339 = &l_1282[1];
                        int32_t l_1342 = 0xD61D9F3BL;
                        uint16_t *l_1343[4][2] = {{(void*)0,&g_1234[0]},{&g_1234[0],(void*)0},{&g_1234[0],&g_1234[0]},{(void*)0,&g_1234[0]}};
                        int i, j;
                        (*g_368) = ((7UL < (safe_unary_minus_func_int8_t_s(((safe_rshift_func_uint16_t_u_u(l_1326, (l_1344 = ((p_42 , (safe_div_func_int8_t_s_s(l_1318[1], l_1301[4]))) , ((safe_div_func_int32_t_s_s((((safe_sub_func_uint64_t_u_u((((*l_809) >= (p_41 = (((p_41 > ((((((*g_23) = ((safe_rshift_func_uint8_t_u_s((safe_add_func_uint32_t_u_u(((((((((safe_mul_func_uint64_t_u_u(0xFA4D4872756665ABLL, (((*l_1339) = &p_41) != &p_41))) | 4UL) || l_1318[1]) , (*g_877)) > 0xD26D8326L) & l_1340[0][3]) | l_1341) , l_1299), 0x83F9B875L)), 3)) ^ l_1342)) != 0xD8L) & l_1342) <= l_1318[0]) ^ 0xFCL)) ^ 0L) <= g_29))) ^ p_42), l_1340[0][3])) & l_1341) && l_1341), p_42)) , l_1341))))) <= g_29)))) <= (-4L));
                        if (g_82)
                            goto lbl_1345;
                    }
                    if (l_1346[0])
                        continue;
                }
                --l_1357;
                ++g_1361;
                for (g_242 = 0; (g_242 <= 3); g_242 += 1)
                { /* block id: 587 */
                    uint32_t l_1368 = 0x91408C59L;
                    for (g_602 = 2; (g_602 <= 8); g_602 += 1)
                    { /* block id: 590 */
                        int32_t l_1365 = 0x90226653L;
                        int32_t l_1366 = 0xACB0C2C7L;
                        int32_t l_1367[4][5] = {{(-2L),0L,0L,(-2L),0L},{(-2L),(-2L),9L,(-2L),(-2L)},{0L,(-2L),0L,0L,(-2L)},{(-2L),0L,0L,(-2L),0L}};
                        int i, j;
                        (*l_809) = l_1152[(g_242 + 1)];
                        (*g_1364) = &l_1286[0][9][0];
                        --l_1368;
                    }
                }
            }
            else
            { /* block id: 596 */
                int32_t l_1390 = 5L;
                int64_t ** const *l_1394 = &g_464;
                int32_t l_1403 = 0xC6A4D129L;
                int32_t l_1404 = (-3L);
                (*g_1371) = (*g_367);
                l_1318[1] ^= ((*g_368) = (*l_809));
                (*l_809) ^= ((((((safe_rshift_func_int8_t_s_s((safe_sub_func_int8_t_s_s(p_41, 0UL)), 6)) > (safe_div_func_uint16_t_u_u((((((((!(-7L)) == p_42) , (safe_lshift_func_int32_t_s_u(((((0xA22DL | (!(((safe_lshift_func_int16_t_s_u((~(((8L & (**g_189)) & (safe_add_func_int8_t_s_s(((((p_42 & ((safe_add_func_uint32_t_u_u((4294967292UL & 0x409B1D57L), l_1318[0])) ^ l_1389[2][0])) == 0xA591L) < p_42) >= l_1318[1]), l_1301[4]))) != l_1301[2])), l_1390)) , l_1391) != l_1393))) ^ l_1301[1]) | l_1318[1]) != 0xC1L), p_42))) , (void*)0) != l_1394) , (**g_189)) & (*g_190)), 65530UL))) , p_42) , &g_743) == (void*)0) > (*g_877));
                for (l_1084 = 0; (l_1084 != (-24)); l_1084 = safe_sub_func_uint64_t_u_u(l_1084, 3))
                { /* block id: 603 */
                    uint16_t ****l_1397 = &l_1311;
                    uint16_t ****l_1398 = (void*)0;
                    uint16_t ***l_1400 = &g_887;
                    uint16_t ****l_1399 = &l_1400;
                    uint32_t **l_1401 = &g_289;
                    int32_t *l_1402[9][4][3] = {{{(void*)0,&l_1299,&g_166},{&l_1318[1],&l_59,&g_82},{&l_1300,(void*)0,&g_166},{&l_1300,&l_1301[1],&l_807}},{{&l_1300,&l_1299,&l_1318[1]},{&l_1318[1],&l_1301[1],&g_82},{(void*)0,(void*)0,&l_1318[1]},{&l_1300,&l_59,&l_807}},{{(void*)0,&l_1299,&g_166},{&l_1318[1],&l_59,&g_82},{&l_1300,(void*)0,&g_166},{&l_1300,&l_1301[1],&l_807}},{{&l_1300,&l_1299,&l_1318[1]},{&l_1318[1],&l_1301[1],&g_82},{(void*)0,(void*)0,&l_1318[1]},{&l_1300,&l_59,&l_807}},{{(void*)0,&l_1299,&g_166},{&l_1318[1],&l_59,&g_82},{&l_1300,(void*)0,&g_166},{&l_1300,&l_1301[1],&l_807}},{{&l_1300,&l_1299,&l_1318[1]},{&l_1318[1],&l_1301[1],&g_82},{(void*)0,(void*)0,&l_1318[1]},{&l_1300,&l_59,&l_807}},{{(void*)0,&l_1299,&g_166},{&l_1318[1],&l_59,&g_82},{&l_1300,(void*)0,&g_166},{&l_1300,&l_1301[1],&l_807}},{{&l_1300,&l_1299,&l_1318[1]},{&l_1318[1],&l_1301[1],&g_82},{(void*)0,(void*)0,&l_1318[1]},{&l_1300,&l_59,&l_807}},{{(void*)0,&l_1299,&g_166},{&l_1318[1],&l_59,&g_82},{&l_1300,(void*)0,&g_166},{&l_1300,&l_1301[1],&l_807}}};
                    int i, j, k;
                    (*l_1399) = ((*l_1397) = l_1311);
                    l_1401 = &g_289;
                    --g_1405;
                    for (g_242 = 0; (g_242 <= 11); g_242 = safe_add_func_uint16_t_u_u(g_242, 1))
                    { /* block id: 610 */
                        (*g_368) = (*g_564);
                    }
                }
            }
        }
        else
        { /* block id: 615 */
            uint32_t l_1424 = 0xBB2AA6DCL;
            int32_t *l_1425 = &g_563;
            int64_t *****l_1451 = (void*)0;
            int16_t l_1455 = 0L;
            const int64_t l_1456 = 0xCF6674696027FB93LL;
            const int32_t l_1461 = 0x649D6312L;
            const uint64_t ***l_1472[10] = {(void*)0,(void*)0,&l_1391,(void*)0,(void*)0,&l_1391,(void*)0,(void*)0,&l_1391,(void*)0};
            const uint64_t ****l_1471 = &l_1472[3];
            int32_t l_1504 = 0x7ABB960EL;
            int32_t l_1506[6][5][8] = {{{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L}},{{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L}},{{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L}},{{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L}},{{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L}},{{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L},{0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L,0xF8972B71L}}};
            int i, j, k;
            (*l_1425) ^= ((*g_368) = (((void*)0 != &g_743) != (l_1424 = (safe_mul_func_int8_t_s_s(p_42, ((((*l_809) & 0x1E1610ADAAC48540LL) != (safe_div_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_s((safe_mod_func_int64_t_s_s((safe_mod_func_int32_t_s_s((&l_1152[4] != ((0x0551A92EADDD1B40LL > (safe_rshift_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(p_41, ((0x19DFL & g_1120) <= (**g_189)))), p_42))) , &p_41)), (*g_877))), l_1356)), 3)), (**g_329)))) & 7L))))));
            (*g_368) = (-1L);
            if (((***g_331) && p_41))
            { /* block id: 620 */
                int32_t *l_1441 = &l_1356;
                uint32_t *l_1447[9][7] = {{&g_78,&g_802,&g_802,&l_805,&l_805,&g_802,&g_802},{&g_602,&g_602,&g_802,&g_802,&g_802,&g_802,&g_602},{&g_78,&g_802,&g_802,&l_805,&l_805,&g_802,&g_802},{&g_602,&g_602,&g_802,&g_802,&g_802,&g_802,&g_602},{&g_78,&g_802,&g_802,&l_805,&l_805,&g_802,&g_802},{&g_602,&g_602,&g_802,&g_802,&g_802,&g_802,&g_602},{&g_78,&g_802,&g_802,&l_805,&l_805,&g_802,&g_802},{&g_602,&g_602,&g_802,&g_802,&g_802,&g_802,&g_602},{&g_78,&g_802,&g_802,&l_805,&l_805,&g_802,&g_802}};
                int32_t **l_1457[5][6][8] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_809,&g_368,&l_1431,&g_368,&l_809,&l_1425,&l_1441,&l_809},{(void*)0,&l_1441,&g_368,&l_1441,&l_809,&g_368,(void*)0,(void*)0},{&l_809,(void*)0,&l_809,&l_809,(void*)0,&l_809,&l_1431,(void*)0},{(void*)0,&l_1431,(void*)0,(void*)0,&l_1431,(void*)0,&l_1425,&l_1431},{&g_368,&g_368,&l_1431,(void*)0,&l_809,(void*)0,&l_1425,(void*)0}},{{&g_368,&l_809,&l_1431,&l_809,&g_368,&l_1441,&l_1431,(void*)0},{&l_1431,(void*)0,(void*)0,&l_1441,(void*)0,&l_1431,&l_809,&l_809},{&l_1431,&l_1431,(void*)0,&g_368,(void*)0,&l_1431,&l_1431,(void*)0},{(void*)0,&l_1431,&l_1431,(void*)0,(void*)0,&l_1431,&l_1425,&l_809},{&g_368,(void*)0,&l_1431,&l_1431,&l_1431,(void*)0,&l_1425,&l_1441},{&l_1441,(void*)0,(void*)0,&l_1431,&l_809,(void*)0,&l_809,&g_368}},{{(void*)0,&l_809,(void*)0,(void*)0,(void*)0,&g_368,&l_1425,&l_809},{&l_1431,&l_809,&l_1431,&g_368,&l_809,&l_1441,(void*)0,&g_368},{&l_1431,&l_1431,&l_1441,(void*)0,(void*)0,(void*)0,(void*)0,&l_1441},{(void*)0,(void*)0,&l_1425,&l_809,&l_809,&l_1431,(void*)0,&l_1441},{&l_809,&l_1431,(void*)0,&l_1431,(void*)0,&g_368,&l_809,&l_1441},{&l_1431,&l_1441,&g_368,&l_809,&l_1431,&l_809,&g_368,&l_1441}},{{&l_1425,&l_1425,&l_1431,(void*)0,&l_1431,(void*)0,&l_1441,&g_368},{&l_809,(void*)0,&l_1441,&g_368,&l_1425,&l_1431,&l_1441,&l_809},{&g_368,&g_368,&l_1431,(void*)0,(void*)0,&l_1431,&g_368,&g_368},{(void*)0,&l_1431,&g_368,&g_368,(void*)0,(void*)0,&l_809,&l_809},{&g_368,&l_1441,(void*)0,&l_809,&g_368,(void*)0,(void*)0,&l_809},{(void*)0,&l_1431,&l_1425,&l_1425,&l_1425,&l_1431,(void*)0,&l_1431}},{{&l_809,&g_368,&l_1441,&l_1431,(void*)0,&l_1431,(void*)0,(void*)0},{&l_1431,(void*)0,&l_1431,&l_809,(void*)0,(void*)0,&l_1425,&l_1425},{&l_809,&l_1425,(void*)0,(void*)0,&l_1425,&l_809,&l_809,&l_1431},{(void*)0,&l_1441,&l_1431,&l_1431,&g_368,&g_368,&l_1431,(void*)0},{&g_368,&l_1431,&l_809,&l_1431,(void*)0,&l_1431,&l_1441,&l_1431},{(void*)0,(void*)0,&l_809,(void*)0,(void*)0,(void*)0,&g_368,&l_1425}}};
                int i, j, k;
                for (l_807 = 5; (l_807 >= (-11)); l_807--)
                { /* block id: 623 */
                    int32_t *l_1428 = &l_1300;
                    (*g_1429) = l_1428;
                    if (l_1356)
                    { /* block id: 625 */
                        int32_t **l_1430 = (void*)0;
                        l_1431 = l_1425;
                        return p_41;
                    }
                    else
                    { /* block id: 628 */
                        uint64_t * const ***l_1436 = &l_1434;
                        uint64_t ****l_1439 = &g_1437;
                        int32_t **l_1440[3][10][8] = {{{&g_368,&l_809,&l_1428,&l_1425,&l_1428,&l_809,&g_368,&l_809},{&l_809,&l_809,&g_368,&l_1425,&l_1431,&l_1425,&l_1425,&g_368},{&g_368,&l_1431,&l_809,(void*)0,&l_1431,(void*)0,&g_368,&l_1425},{&l_809,(void*)0,(void*)0,&g_368,&l_1428,&l_1425,&l_1431,(void*)0},{&g_368,&l_1431,&g_368,(void*)0,(void*)0,&g_368,&l_1431,&g_368},{&l_809,&l_1425,&g_368,&l_1425,(void*)0,&g_368,(void*)0,&l_1428},{&l_1425,&g_368,(void*)0,(void*)0,&l_1425,&g_368,(void*)0,&l_809},{(void*)0,&l_809,&l_1431,&g_368,&l_809,(void*)0,&l_1428,(void*)0},{&l_809,&l_1428,&g_368,&l_809,&l_1428,&l_809,&g_368,&l_1428},{&l_1425,&g_368,&g_368,&l_809,(void*)0,&g_368,(void*)0,(void*)0}},{{&g_368,(void*)0,&l_1425,&g_368,&l_1425,&l_809,(void*)0,(void*)0},{&g_368,&g_368,&g_368,&g_368,&g_368,&g_368,&g_368,&l_1428},{&g_368,&g_368,&g_368,&l_1428,(void*)0,(void*)0,&l_1428,&g_368},{(void*)0,(void*)0,&l_1431,&l_1425,&l_1428,&g_368,(void*)0,(void*)0},{&g_368,&l_1431,(void*)0,&g_368,&g_368,&l_809,(void*)0,(void*)0},{&l_1431,(void*)0,&l_809,&l_1425,(void*)0,&l_809,&l_1428,&g_368},{&g_368,&l_809,(void*)0,&l_1428,(void*)0,&g_368,(void*)0,&l_1428},{(void*)0,&l_809,(void*)0,&g_368,&l_1428,&g_368,&l_809,(void*)0},{(void*)0,&l_809,&g_368,&g_368,&l_809,&l_809,&l_1428,(void*)0},{(void*)0,&l_1428,&l_1431,&l_809,&l_1428,&l_809,&l_1431,&l_1428}},{{(void*)0,&l_1431,&g_368,&l_809,(void*)0,&g_368,&g_368,(void*)0},{&g_368,(void*)0,(void*)0,&g_368,(void*)0,(void*)0,(void*)0,&l_809},{&l_1431,&g_368,&g_368,(void*)0,&g_368,&l_1431,&l_1431,&l_1428},{&g_368,&g_368,&g_368,&l_1425,&l_1428,(void*)0,&l_1425,&l_1431},{(void*)0,(void*)0,&g_368,&l_1425,(void*)0,&g_368,(void*)0,&g_368},{&g_368,&l_1431,&l_1425,&l_1431,&g_368,&l_809,(void*)0,(void*)0},{&g_368,&l_1428,&l_809,(void*)0,&l_1425,&l_809,(void*)0,&l_1431},{&g_368,&l_809,&l_809,&l_1428,(void*)0,&g_368,(void*)0,&l_1425},{&l_1425,&l_809,&l_1425,(void*)0,&l_1428,&g_368,(void*)0,(void*)0},{&l_809,&g_368,&l_1425,&l_1425,&g_368,&l_1425,&l_809,&g_368}}};
                        int i, j, k;
                        (*l_1425) &= (safe_mul_func_int32_t_s_s(0L, (((*l_1436) = l_1434) != ((*l_1439) = g_1437))));
                        l_1441 = (*g_367);
                        return p_42;
                    }
                }
                l_1458 = ((safe_sub_func_uint64_t_u_u((0x9B01412A1C4123C7LL | (l_1455 &= (((safe_mod_func_uint8_t_u_u((+(l_1447[1][4] == (void*)0)), ((p_42 || (safe_lshift_func_int8_t_s_s(((~9L) < p_42), 0))) && ((((l_1451 = l_1451) == (void*)0) == ((((((safe_lshift_func_uint8_t_u_s((*l_1431), 6)) , 0x6951242AAA8307E6LL) > p_42) > (**g_1429)) < (*l_1441)) > (*g_368))) , l_1454)))) , 0x70840A56L) <= (**g_876)))), l_1456)) , l_1441);
            }
            else
            { /* block id: 639 */
                uint16_t l_1464 = 8UL;
                int64_t *l_1474[6];
                int32_t l_1503 = (-4L);
                int32_t l_1505 = 0x3B44884DL;
                int32_t l_1507 = 0xC0231823L;
                int32_t l_1508 = (-1L);
                int32_t l_1509 = 0x223F47C2L;
                int32_t l_1510[8][4] = {{0x499B72C6L,0x56B24CDFL,0x383C13DEL,0x56B24CDFL},{0x56B24CDFL,(-1L),0x383C13DEL,0x383C13DEL},{0x499B72C6L,0x499B72C6L,0x56B24CDFL,0x383C13DEL},{(-1L),(-1L),(-1L),0x56B24CDFL},{(-1L),0x56B24CDFL,0x56B24CDFL,(-1L)},{0x499B72C6L,0x56B24CDFL,0x383C13DEL,0x56B24CDFL},{0x56B24CDFL,(-1L),0x383C13DEL,0x383C13DEL},{0x499B72C6L,0x499B72C6L,0x56B24CDFL,0x383C13DEL}};
                const int64_t *l_1516 = &l_1456;
                const int64_t **l_1515 = &l_1516;
                const int64_t *l_1518[1][7] = {{&g_663,&g_663,&l_1456,&g_663,&g_663,&l_1456,&g_663}};
                const int64_t **l_1517 = &l_1518[0][6];
                int i, j;
                for (i = 0; i < 6; i++)
                    l_1474[i] = &g_737[2];
lbl_1514:
                if (((p_41 |= 0x3D52L) == ((*l_1431) = ((safe_mod_func_int32_t_s_s((*g_667), p_42)) || ((*l_1425) = ((((*l_809) == l_1461) & (safe_sub_func_uint32_t_u_u((((**g_876) ^= l_1464) <= ((safe_lshift_func_uint64_t_u_u(((safe_lshift_func_uint16_t_u_s(((((safe_add_func_uint32_t_u_u(0xABFA5D96L, 0x594E3AEDL)) <= ((l_1471 != l_1473[4][1]) > (*l_1425))) , (void*)0) != (void*)0), 6)) & (*l_1425)), 27)) || p_42)), 0UL))) | 0x5B86L))))))
                { /* block id: 644 */
                    return p_41;
                }
                else
                { /* block id: 646 */
                    int16_t *l_1476 = (void*)0;
                    int16_t *l_1477[3];
                    int32_t l_1488[10][7] = {{0x2AE5D73CL,0L,0xF6694029L,0L,0xF6694029L,0L,0x2AE5D73CL},{5L,0xBE43B2C7L,0xF6694029L,0L,0x8B35299FL,0L,5L},{0x2AE5D73CL,0xBE43B2C7L,0x2C2CC7C6L,0L,0x8B35299FL,0x6E4C790DL,0x2AE5D73CL},{0x2AE5D73CL,0L,0xF6694029L,0L,0xF6694029L,0L,0x2AE5D73CL},{5L,0xBE43B2C7L,0xF6694029L,0L,0x8B35299FL,0L,5L},{0x2AE5D73CL,0xBE43B2C7L,0x2C2CC7C6L,0L,0x8B35299FL,0x6E4C790DL,0x2AE5D73CL},{0x2AE5D73CL,0L,0xF6694029L,0L,0xF6694029L,0L,0x2AE5D73CL},{5L,0xBE43B2C7L,0xF6694029L,0L,0x8B35299FL,0L,5L},{0x2AE5D73CL,0xBE43B2C7L,0x2C2CC7C6L,0L,0x8B35299FL,0x6E4C790DL,0x2AE5D73CL},{0x2AE5D73CL,0L,0xF6694029L,0L,0xF6694029L,0L,0x2AE5D73CL}};
                    const int32_t *l_1495 = &g_1283;
                    int32_t *l_1496 = &l_1488[8][3];
                    int32_t *l_1497 = &l_60[1];
                    int32_t *l_1498 = &l_59;
                    int32_t *l_1499 = &g_584[0];
                    int32_t *l_1500 = &l_1301[3];
                    int32_t *l_1501 = &l_1488[7][3];
                    int32_t *l_1502[9][3] = {{&l_1286[0][9][0],&l_1286[0][9][0],&l_1286[0][9][0]},{&l_1488[8][3],&l_1299,&l_1488[8][3]},{&l_1286[0][9][0],&l_1286[0][9][0],&l_1286[0][9][0]},{&l_1488[8][3],&l_1299,&l_1488[8][3]},{&l_1286[0][9][0],&l_1286[0][9][0],&l_1286[0][9][0]},{&l_1488[8][3],&l_1299,&l_1488[8][3]},{&l_1286[0][9][0],&l_1286[0][9][0],&l_1286[0][9][0]},{&l_1488[8][3],&l_1299,&l_1488[8][3]},{&l_1286[0][9][0],&l_1286[0][9][0],&l_1286[0][9][0]}};
                    uint64_t l_1511 = 0UL;
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1477[i] = &l_843;
                    l_1488[0][6] = (((safe_unary_minus_func_int16_t_s(((*l_1431) = ((0x3BAFA6EEL & (*l_809)) == (*l_1425))))) <= (!(+(safe_rshift_func_int16_t_s_s((g_676 = (*l_809)), 9))))) , (((safe_add_func_uint64_t_u_u(p_42, (safe_mod_func_uint8_t_u_u(((*g_190) &= l_1464), (0xAA85D4F4L | (safe_sub_func_uint8_t_u_u(1UL, (p_41 , l_1488[8][3])))))))) , (*l_1425)) || g_396));
                    for (l_1299 = 25; (l_1299 == 15); l_1299 = safe_sub_func_int8_t_s_s(l_1299, 7))
                    { /* block id: 653 */
                        const int32_t *l_1492[8] = {&l_1461,&l_1461,&l_1461,&l_1461,&l_1461,&l_1461,&l_1461,&l_1461};
                        const int32_t **l_1491[7] = {&l_1492[7],&l_1492[7],&l_1492[7],&l_1492[7],&l_1492[7],&l_1492[7],&l_1492[7]};
                        int i;
                        l_1495 = (l_1493 = &l_1084);
                    }
                    l_1511++;
                    if (g_242)
                        goto lbl_1514;
                }
                (*l_1458) |= ((*g_368) = ((((***g_1437) = (&l_1360 == ((*l_1517) = ((*l_1515) = (void*)0)))) & (safe_add_func_int64_t_s_s(p_41, (g_213 | (safe_mul_func_uint16_t_u_u(0xC247L, p_41)))))) && (++p_41)));
            }
        }
    }
    else
    { /* block id: 668 */
        uint32_t ****l_1538 = (void*)0;
        int32_t l_1540 = (-2L);
        int16_t l_1543 = 0x4E39L;
        for (g_602 = (-13); (g_602 == 3); ++g_602)
        { /* block id: 671 */
            uint32_t *l_1567 = (void*)0;
            int32_t l_1568 = 3L;
            if (((!((safe_mul_func_uint8_t_u_u((0xF9706C6BL < (-1L)), ((0x8453DD84ABDB6FDCLL >= (((safe_add_func_int8_t_s_s(((*g_23) = (((**g_876) = (((safe_mul_func_int32_t_s_s((g_1534[8][2][2] == l_1538), (+l_1540))) & (safe_mul_func_uint8_t_u_u(((p_41 | 65535UL) == g_218[1]), p_42))) > 0x00422CD6A6A83976LL)) != p_42)), p_41)) < l_1540) < 0xB1BDL)) == (*g_368)))) & 0x7116C24CL)) | (*l_1493)))
            { /* block id: 674 */
                (*g_368) &= l_1543;
            }
            else
            { /* block id: 676 */
                int32_t *l_1556 = &l_57[0];
                int32_t l_1569 = 0x3A041BD9L;
                int64_t *l_1570 = (void*)0;
                int64_t *l_1571 = (void*)0;
                int64_t *l_1572 = (void*)0;
                int64_t *l_1573 = &g_737[6];
                uint32_t **l_1574 = &g_877;
                (*g_368) ^= ((0x6DBBL < g_114) ^ (((safe_add_func_uint8_t_u_u((safe_mod_func_int8_t_s_s((safe_rshift_func_int64_t_s_s((((((((*l_1556) ^= (safe_rshift_func_uint32_t_u_u(((**g_876)++), 27))) , (((*l_1573) = ((safe_mod_func_int64_t_s_s((safe_add_func_uint8_t_u_u((p_41 , 2UL), (safe_mul_func_int16_t_s_s((((l_1568 = (safe_mul_func_uint32_t_u_u(l_1543, (safe_add_func_int16_t_s_s(((l_1567 = (*g_288)) != l_1556), ((*g_23) > (((**g_876) | (*l_1493)) && p_42))))))) > l_1569) || p_42), p_41)))), l_1543)) || 5UL)) > p_41)) != 0L) & p_42) <= 0UL) <= p_42), 48)), (*g_23))), l_1540)) == p_41) & (*g_23)));
                (*g_368) |= ((void*)0 != l_1574);
            }
            if (l_1540)
                continue;
            for (g_82 = 0; (g_82 < (-7)); g_82 = safe_sub_func_int16_t_s_s(g_82, 9))
            { /* block id: 688 */
                uint64_t l_1588 = 0UL;
                int32_t * const l_1593 = &l_1084;
                for (g_166 = 0; (g_166 >= 22); ++g_166)
                { /* block id: 691 */
                    for (g_135 = 0; (g_135 <= 3); g_135 += 1)
                    { /* block id: 694 */
                        int i, j, k;
                        return l_1286[(g_135 + 2)][g_135][g_135];
                    }
                    if (l_1543)
                        continue;
                    for (g_1120 = 6; (g_1120 < 14); ++g_1120)
                    { /* block id: 700 */
                        int32_t l_1581 = 3L;
                        int32_t *l_1582 = &l_1084;
                        int32_t *l_1583 = &l_807;
                        int32_t *l_1584 = &l_1568;
                        int32_t *l_1585 = &l_60[1];
                        int32_t *l_1586 = &l_1286[1][2][3];
                        int32_t *l_1587[1][10] = {{&l_59,&l_1581,&l_1581,&l_59,(void*)0,&l_59,&l_1581,&l_1581,&l_59,(void*)0}};
                        int32_t **l_1592 = &l_1584;
                        int i, j;
                        l_1588--;
                        (*l_1592) = &l_1540;
                    }
                    (*g_1594) = l_1593;
                }
            }
        }
    }
    return l_1595;
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_78 g_114 g_127 g_62 g_24 g_149 g_169 g_164 g_189 g_166 g_23 g_213 g_82 g_190 g_251 g_218 g_248 g_135 g_282 g_288 g_302 g_328 g_329 g_331 g_367 g_330 g_368 g_396 g_427 g_514 g_242 g_539 g_564 g_289 g_464 g_283 g_563 g_667 g_676 g_463 g_584 g_737 g_743 g_777
 * writes: g_114 g_78 g_29 g_127 g_135 g_149 g_166 g_169 g_213 g_218 g_164 g_242 g_248 g_251 g_190 g_288 g_302 g_329 g_368 g_427 g_563 g_602 g_289 g_584 g_676
 */
static int8_t  func_91(uint64_t  p_92, uint8_t  p_93)
{ /* block id: 20 */
    uint32_t l_100 = 0x175C2255L;
    uint32_t *l_101[9] = {&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78};
    int64_t *l_108 = (void*)0;
    uint8_t *l_113[1][8] = {{&g_114,&g_114,(void*)0,&g_114,&g_114,(void*)0,&g_114,&g_114}};
    int32_t l_115 = 0xE81B9928L;
    int32_t l_582 = 1L;
    int32_t l_583[2][8] = {{0x5167262AL,0x57CFE16BL,0x5167262AL,0x57CFE16BL,0x5167262AL,0x57CFE16BL,0x5167262AL,0x57CFE16BL},{0x5167262AL,0x57CFE16BL,0x5167262AL,0x57CFE16BL,0x5167262AL,0x57CFE16BL,0x5167262AL,0x57CFE16BL}};
    uint32_t l_591[4][5] = {{8UL,8UL,8UL,8UL,8UL},{0x9C28889AL,0x9C28889AL,0x9C28889AL,0x9C28889AL,0x9C28889AL},{8UL,8UL,8UL,8UL,8UL},{0x9C28889AL,0x9C28889AL,0x9C28889AL,0x9C28889AL,0x9C28889AL}};
    int64_t *****l_592 = (void*)0;
    const uint32_t l_641 = 0xE1B1DAA9L;
    const int64_t *l_662 = &g_663;
    const int64_t **l_661 = &l_662;
    uint32_t l_696 = 18446744073709551615UL;
    uint32_t ***l_721 = (void*)0;
    uint32_t ****l_720[8][8] = {{&l_721,(void*)0,(void*)0,&l_721,(void*)0,(void*)0,&l_721,&l_721},{(void*)0,&l_721,&l_721,&l_721,&l_721,(void*)0,&l_721,(void*)0},{&l_721,(void*)0,&l_721,(void*)0,&l_721,&l_721,&l_721,&l_721},{(void*)0,&l_721,&l_721,(void*)0,(void*)0,&l_721,(void*)0,(void*)0},{&l_721,(void*)0,&l_721,&l_721,&l_721,&l_721,&l_721,&l_721},{(void*)0,(void*)0,&l_721,&l_721,&l_721,&l_721,&l_721,(void*)0},{(void*)0,&l_721,&l_721,&l_721,&l_721,&l_721,&l_721,(void*)0},{&l_721,(void*)0,(void*)0,&l_721,(void*)0,(void*)0,&l_721,&l_721}};
    int32_t *l_736[1][5][5] = {{{(void*)0,&g_563,&g_584[0],&g_584[0],&g_563},{(void*)0,&g_563,&g_584[0],(void*)0,(void*)0},{&g_563,(void*)0,&g_563,&g_584[0],(void*)0},{&g_563,(void*)0,(void*)0,(void*)0,&g_563},{&g_563,(void*)0,(void*)0,&g_563,(void*)0}}};
    int i, j, k;
    if (((safe_add_func_uint8_t_u_u((p_92 != func_96(l_100, l_101[8], func_102((g_29 & l_100), l_108, (((((((void*)0 == &g_29) < (safe_add_func_int8_t_s_s(((((safe_mul_func_uint8_t_u_u((l_115 |= ((l_100 < p_93) ^ l_100)), l_100)) <= 0x0BL) < l_100) > g_78), 1UL))) ^ 0x20CCL) < p_92) , l_100) , l_101[8]), &g_24, g_29))), p_92)) , (*g_539)))
    { /* block id: 222 */
        int32_t *l_568 = &g_166;
        int32_t l_569 = 0x3ADBF9BBL;
        int32_t l_570 = 1L;
        int32_t *l_571 = &l_115;
        int32_t l_572[3][7][8] = {{{0x81F99338L,0x9FB8486FL,0xF790C9ABL,0xC4076AB1L,3L,0x4C066D70L,8L,0x7C520808L},{8L,0L,0x5CF9498FL,0L,0L,1L,0xD47B4CE0L,(-9L)},{0x7C520808L,(-4L),0x387DD15DL,0x95569543L,0xD47B4CE0L,0x95569543L,0x387DD15DL,(-4L)},{0x81F99338L,0x387DD15DL,0x4C066D70L,0L,(-5L),1L,0xC4076AB1L,(-5L)},{0L,(-9L),0x51F7F5B7L,0L,0x81F99338L,0x3BB97F6FL,0xC4076AB1L,8L},{0x387DD15DL,0L,0x4C066D70L,1L,0L,8L,0x387DD15DL,0xD47B4CE0L},{0L,8L,0x387DD15DL,0xD47B4CE0L,0L,0L,0xD47B4CE0L,0x387DD15DL}},{{(-9L),(-9L),0x5CF9498FL,0x81F99338L,(-4L),0L,8L,0xC4076AB1L},{0x4C066D70L,1L,0xF790C9ABL,0x387DD15DL,0L,0x95569543L,(-5L),0xC4076AB1L},{0x9FB8486FL,0x3BBB0E6DL,(-5L),0x95569543L,0L,(-5L),0x4C066D70L,0x5E47ECAFL},{(-5L),8L,0x81F99338L,0L,1L,8L,1L,0L},{0L,3L,0L,0x76CA7786L,0L,1L,(-5L),0xF790C9ABL},{0xF790C9ABL,0x5CF9498FL,0x51F7F5B7L,(-5L),0x5CF9498FL,(-5L),0L,0L},{0xF790C9ABL,0x4C066D70L,(-1L),8L,0L,0L,0x9FB8486FL,0x4C066D70L}},{{0L,0x9FB8486FL,1L,0x5CF9498FL,1L,0x76CA7786L,0x76CA7786L,1L},{(-5L),0L,0L,(-5L),0L,0x3BBB0E6DL,0x387DD15DL,(-5L)},{0x9FB8486FL,(-5L),8L,0x387DD15DL,0x7C520808L,(-9L),0x9FB8486FL,0L},{1L,(-5L),0x5E47ECAFL,0x3BB97F6FL,0x4C066D70L,0x3BBB0E6DL,0x3BB97F6FL,0x9FB8486FL},{1L,0L,0x51F7F5B7L,0x95569543L,0x3BBB0E6DL,0x76CA7786L,0xF790C9ABL,0x76CA7786L},{0x7C520808L,0x9FB8486FL,0L,0x9FB8486FL,0x7C520808L,0L,1L,0x387DD15DL},{0x5E47ECAFL,0x4C066D70L,(-5L),0L,0x95569543L,(-5L),0x3BBB0E6DL,0x9FB8486FL}}};
        int64_t l_573 = 0x5BA0A04C0176B235LL;
        int32_t *l_574 = &g_166;
        int32_t *l_575 = &g_166;
        int32_t *l_576 = &g_166;
        int32_t *l_577 = &l_115;
        int32_t *l_578 = &l_569;
        int32_t *l_579 = (void*)0;
        int32_t *l_580 = (void*)0;
        int32_t *l_581[6][8] = {{&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2]},{&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2]},{&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2]},{&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2]},{&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2]},{&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2],&l_572[2][4][2]}};
        uint8_t l_585 = 255UL;
        int8_t *l_599 = &g_218[0];
        int8_t *l_600 = (void*)0;
        int8_t *l_601[5];
        int16_t *l_603[4][4][10] = {{{&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,(void*)0,(void*)0,(void*)0},{&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,&g_149,(void*)0},{&g_149,(void*)0,&g_149,(void*)0,(void*)0,(void*)0,(void*)0,&g_149,(void*)0,&g_149}},{{(void*)0,(void*)0,&g_149,(void*)0,&g_149,&g_149,&g_149,(void*)0,&g_149,&g_149},{(void*)0,(void*)0,(void*)0,&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,(void*)0},{(void*)0,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,(void*)0,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149}},{{(void*)0,&g_149,(void*)0,(void*)0,(void*)0,(void*)0,&g_149,(void*)0,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149,(void*)0},{&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149,&g_149,(void*)0,&g_149,&g_149},{(void*)0,(void*)0,(void*)0,&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,(void*)0}},{{(void*)0,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,(void*)0,&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149},{(void*)0,&g_149,(void*)0,(void*)0,(void*)0,(void*)0,&g_149,(void*)0,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149,&g_149,(void*)0}}};
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_601[i] = (void*)0;
        l_585++;
        (*l_574) = (((((safe_div_func_uint64_t_u_u(((*g_514) |= (~(l_591[3][1] = p_93))), l_115)) || ((p_93 | (((void*)0 == l_592) > (l_583[1][0] ^= (safe_mul_func_int8_t_s_s((((***g_331) >= (((*l_599) = (safe_div_func_int64_t_s_s((*l_574), (*l_571)))) <= (g_602 = ((*g_289) , p_92)))) && g_248), (-1L)))))) <= g_78)) >= g_251[2][0]) , p_92) , 0L);
    }
    else
    { /* block id: 230 */
        int8_t l_606 = 0x75L;
        uint32_t **l_610 = &g_289;
        int64_t ** const *l_618 = &g_464;
        int64_t ** const **l_617 = &l_618;
        const int32_t *l_619[3][9] = {{&g_82,(void*)0,&l_582,&g_82,&l_583[1][0],&l_583[1][0],&l_583[1][0],&l_583[1][0],&g_82},{&g_82,&l_583[0][2],&g_82,&l_583[1][0],&g_584[0],&l_582,(void*)0,(void*)0,&l_582},{&l_582,(void*)0,&g_82,(void*)0,&l_582,&g_82,&l_583[1][0],&l_583[1][0],&l_583[1][0]}};
        const int64_t *l_659[7][4] = {{&g_29,&g_164,&g_29,&g_29},{&g_164,&g_164,&g_164,&g_164},{&g_164,&g_29,&g_29,&g_164},{&g_29,&g_164,&g_29,&g_29},{&g_164,&g_164,&g_164,&g_164},{&g_164,&g_29,&g_29,&g_164},{&g_29,&g_164,&g_29,&g_29}};
        const int64_t **l_658 = &l_659[4][0];
        int32_t l_680 = 5L;
        int32_t l_682 = 0xE6E71B8EL;
        int32_t l_683 = (-1L);
        int32_t l_684 = 0x38E2E1E2L;
        int32_t l_685 = 6L;
        int32_t l_687 = 1L;
        int32_t l_695 = 0x7956FA70L;
        uint32_t ***l_719 = &l_610;
        uint32_t ****l_718 = &l_719;
        int32_t *l_722 = &l_683;
        int i, j;
        if (((((safe_sub_func_uint64_t_u_u((*g_330), l_606)) || (safe_rshift_func_uint16_t_u_u((!((&l_583[1][0] == (((((*l_610) = (void*)0) != ((safe_rshift_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u(0x77210A90L, ((((safe_lshift_func_uint64_t_u_u((((l_617 != (void*)0) > ((void*)0 == (***l_617))) , 0xCA234B868BB9B6EELL), 17)) , 1UL) , 1UL) , p_92))) || p_93), (*g_190))) , (void*)0)) || 0UL) , l_619[1][1])) & 0x4C556F8DAED605B9LL)), l_582))) || l_583[1][0]) , p_92))
        { /* block id: 232 */
            int32_t l_624 = 0x13F73EAEL;
            int32_t **l_642[3];
            int i;
            for (i = 0; i < 3; i++)
                l_642[i] = &g_368;
            g_563 |= ((((g_78++) ^ (safe_add_func_uint16_t_u_u((l_624 || (safe_sub_func_uint32_t_u_u((3UL != 9UL), ((((*g_190) &= l_624) || (g_24 ^ ((((*g_564) | (((safe_mod_func_int32_t_s_s((safe_add_func_int64_t_s_s((~(safe_mod_func_uint32_t_u_u(((p_92 != p_93) | (safe_mod_func_int8_t_s_s(((+(safe_sub_func_uint64_t_u_u((safe_sub_func_uint32_t_u_u(((g_127 || g_29) ^ 1L), g_169)), 0xA8C88A1EA63DC7B9LL))) < p_93), l_624))), p_92))), l_641)), (-3L))) && g_218[0]) > g_82)) , l_642[0]) == &l_619[2][1]))) && l_115)))), p_92))) , &p_93) == l_113[0][0]);
        }
        else
        { /* block id: 236 */
            uint8_t l_677[8];
            int32_t l_686 = 0xAE5EC10DL;
            int32_t l_688 = 1L;
            int32_t l_690[3];
            int i;
            for (i = 0; i < 8; i++)
                l_677[i] = 0x37L;
            for (i = 0; i < 3; i++)
                l_690[i] = 0L;
            for (g_149 = 17; (g_149 == 3); g_149--)
            { /* block id: 239 */
                uint64_t ** const l_665 = &g_330;
                int32_t l_681[5] = {0x146AE00EL,0x146AE00EL,0x146AE00EL,0x146AE00EL,0x146AE00EL};
                int i;
                for (g_602 = 17; (g_602 == 6); g_602--)
                { /* block id: 242 */
                    int32_t l_651 = 0x9A503299L;
                    const int64_t ***l_660[2][8][1] = {{{&l_658},{&l_658},{&l_658},{&l_658},{&l_658},{&l_658},{&l_658},{&l_658}},{{&l_658},{&l_658},{&l_658},{&l_658},{&l_658},{&l_658},{&l_658},{&l_658}}};
                    int32_t l_664 = 1L;
                    int32_t l_666 = 0x346A8A06L;
                    int32_t l_689 = 0x256899E2L;
                    int32_t l_691 = 0x9832AA6CL;
                    int32_t l_692 = (-1L);
                    int32_t l_693 = 0x6145C247L;
                    int32_t l_694[5][5][4];
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                    {
                        for (j = 0; j < 5; j++)
                        {
                            for (k = 0; k < 4; k++)
                                l_694[i][j][k] = (-1L);
                        }
                    }
                    (*g_667) = (safe_add_func_uint16_t_u_u(((*g_190) , (safe_mul_func_int32_t_s_s((-1L), (l_651 = 0x2454B729L)))), ((l_666 &= (((void*)0 == &g_78) , (safe_lshift_func_int32_t_s_s((-1L), (safe_sub_func_int8_t_s_s((safe_mod_func_int32_t_s_s((((((l_661 = l_658) == (l_664 , (void*)0)) , l_665) != (void*)0) , (*g_539)), l_664)), l_641)))))) >= (*g_23))));
                    for (g_127 = 0; (g_127 < 1); g_127 = safe_add_func_int64_t_s_s(g_127, 4))
                    { /* block id: 249 */
                        int8_t *l_674 = &g_218[1];
                        int16_t *l_675 = &g_676;
                        int32_t *l_678 = &l_583[0][0];
                        int32_t *l_679[6][5] = {{&l_115,&l_115,&l_115,&l_115,&l_115},{&l_115,(void*)0,&l_115,(void*)0,&l_115},{&l_115,&l_115,&l_115,&l_115,&l_115},{&l_115,(void*)0,&l_115,(void*)0,&l_115},{&l_115,&l_115,&l_115,&l_115,&l_115},{&l_115,(void*)0,&l_115,(void*)0,&l_115}};
                        int64_t **l_716 = (void*)0;
                        int i, j;
                        l_677[2] = ((safe_sub_func_uint8_t_u_u((*g_190), ((l_666 |= (((*l_675) ^= ((((safe_rshift_func_uint64_t_u_s(((**g_329) = p_92), 31)) >= ((*l_674) = (*g_23))) <= (*g_23)) != (g_251[2][0] & (0x643253158E5EC7C1LL != p_93)))) || (l_664 , g_127))) != p_92))) | p_93);
                        --l_696;
                        (*l_678) = ((safe_mul_func_int64_t_s_s(((**g_329) <= ((((((~((p_92 >= ((safe_div_func_int32_t_s_s((((*l_675) &= p_92) && l_693), (((*g_23) == (*g_190)) ^ (safe_sub_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u(((safe_rshift_func_uint64_t_u_s(((safe_add_func_uint32_t_u_u(g_164, ((safe_mod_func_int16_t_s_s(((p_92 && ((*g_514) ^= p_92)) , (-1L)), g_427)) <= 0x1124D043L))) < p_93), 54)) > p_92), 4)), g_218[1]))))) , p_92)) , p_93)) & (-8L)) , 0x9370L) , l_716) == (*g_463)) != 0xDA57L)), p_93)) || g_302);
                        (*l_678) ^= (0x1E8C1814L || g_169);
                    }
                }
            }
            return (*g_23);
        }
lbl_738:
        (*l_722) = (safe_unary_minus_func_uint16_t_u((l_718 != l_720[0][1])));
        for (l_606 = 0; (l_606 >= (-18)); --l_606)
        { /* block id: 268 */
            uint16_t *l_742[1];
            uint16_t * const * const l_741 = &l_742[0];
            int32_t l_752 = 0x76D0AA52L;
            uint32_t l_763[5][2][5] = {{{0xD961C2A5L,0xFCA0324DL,8UL,0xFCA0324DL,0xD961C2A5L},{1UL,4294967289UL,4294967287UL,0xCEFAB4CCL,4294967295UL}},{{4294967287UL,4294967289UL,1UL,1UL,4294967289UL},{8UL,0xFCA0324DL,0xD961C2A5L,4294967289UL,4294967295UL}},{{0xFCA0324DL,1UL,0xD961C2A5L,0xACA381FBL,0xD961C2A5L},{4294967295UL,4294967295UL,1UL,8UL,4294967292UL}},{{0xFCA0324DL,4294967292UL,4294967287UL,8UL,8UL},{8UL,0x9D1A7110L,8UL,0xACA381FBL,0xCEFAB4CCL}},{{4294967287UL,4294967292UL,0xFCA0324DL,4294967289UL,0xCEFAB4CCL},{1UL,4294967295UL,4294967295UL,1UL,8UL}}};
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_742[i] = &g_248;
            for (g_302 = 0; (g_302 <= 0); g_302 += 1)
            { /* block id: 271 */
                int32_t l_725 = 0L;
                int32_t l_731 = 0xF7AD2858L;
                int32_t l_774 = 6L;
                for (g_563 = 0; (g_563 <= 0); g_563 += 1)
                { /* block id: 274 */
                    uint64_t l_732 = 6UL;
                    int64_t *l_751 = (void*)0;
                    const uint32_t *l_755[9][2];
                    uint32_t * const l_757[4][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
                    int64_t *****l_758 = &g_462[1][0];
                    int32_t l_759 = 0x2C041B59L;
                    int32_t l_773 = (-1L);
                    int i, j;
                    for (i = 0; i < 9; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_755[i][j] = &g_78;
                    }
                    (*l_722) &= p_93;
                    if ((*g_667))
                        break;
                    if (((*g_667) |= 0L))
                    { /* block id: 278 */
                        int32_t *l_726 = &l_583[1][6];
                        int32_t *l_727 = (void*)0;
                        int32_t *l_728 = (void*)0;
                        int32_t *l_729 = &l_683;
                        int32_t *l_730[3];
                        int32_t **l_735[9][4] = {{&l_722,&l_722,&l_728,&l_726},{&l_722,&l_730[2],&l_728,&l_728},{&l_729,&l_728,&l_726,&l_728},{&l_726,&l_728,&l_729,&l_728},{&l_728,&l_730[2],&l_722,&l_726},{&l_728,&l_722,&l_722,&l_728},{&l_728,&l_728,&l_722,(void*)0},{&l_728,&l_728,&l_729,&l_728},{&l_726,&l_722,&l_726,&l_728}};
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_730[i] = &l_695;
                        l_732--;
                        l_736[0][4][4] = (void*)0;
                    }
                    else
                    { /* block id: 281 */
                        if (g_737[2])
                            break;
                        if (l_100)
                            goto lbl_738;
                    }
                    if (((safe_rshift_func_uint64_t_u_u(0x5A86D3AFC42E7FA1LL, 24)) , ((l_741 != g_743) != 1UL)))
                    { /* block id: 285 */
                        const uint32_t **l_756 = &l_755[0][1];
                        int i, j;
                        l_759 = (safe_add_func_uint32_t_u_u((safe_div_func_uint8_t_u_u(((l_752 = (safe_rshift_func_uint32_t_u_s(0xCB516D88L, (((l_751 == (*l_658)) , (l_752 == ((((((p_92 || (((g_676 = (l_732 != ((safe_lshift_func_uint16_t_u_s(65535UL, (((*l_756) = l_755[0][1]) == l_757[1][0]))) || p_92))) != p_93) & (-6L))) >= 255UL) > l_732) < 0xF71DL) , l_758) != &g_462[1][3]))) , (-1L))))) , (*g_190)), p_93)), p_93));
                    }
                    else
                    { /* block id: 290 */
                        int32_t **l_764[8][5][6] = {{{&g_368,(void*)0,&l_736[0][4][4],(void*)0,&l_736[0][4][4],&l_736[0][4][4]},{(void*)0,&l_736[0][4][4],&l_736[0][4][4],(void*)0,&l_736[0][1][1],&l_736[0][4][4]},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&g_368,(void*)0,&g_368},{&l_736[0][1][1],&l_736[0][0][2],&l_736[0][1][1],&l_736[0][4][4],(void*)0,&g_368},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][1][1],&l_736[0][1][1],&l_736[0][4][4]}},{{&l_736[0][4][4],&l_736[0][4][4],(void*)0,&l_736[0][1][1],&l_736[0][4][4],&l_736[0][4][4]},{&l_736[0][4][4],(void*)0,&g_368,&l_736[0][4][4],&g_368,(void*)0},{&l_736[0][1][1],&l_736[0][4][4],&g_368,&g_368,&l_736[0][4][4],&l_736[0][4][4]},{&l_736[0][4][4],&g_368,(void*)0,(void*)0,&g_368,&l_736[0][4][4]},{(void*)0,&g_368,&l_736[0][4][4],(void*)0,&l_736[0][4][4],&g_368}},{{&g_368,&l_736[0][4][4],&l_736[0][1][1],&l_736[0][4][4],&g_368,&g_368},{&g_368,(void*)0,&l_736[0][0][2],&l_736[0][4][4],&g_368,&g_368},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&g_368,&g_368},{&l_736[0][1][1],&g_368,&l_736[0][0][2],(void*)0,&l_736[0][4][4],(void*)0},{&g_368,(void*)0,&g_368,&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]}},{{&l_736[0][0][2],&g_368,&l_736[0][1][1],&g_368,&g_368,&l_736[0][1][1]},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&g_368,&g_368,&l_736[0][4][4]},{&l_736[0][0][2],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]},{&g_368,&l_736[0][0][2],&l_736[0][4][4],(void*)0,&l_736[0][4][4],&l_736[0][4][4]},{&l_736[0][1][1],(void*)0,&l_736[0][4][4],&l_736[0][4][4],(void*)0,&l_736[0][1][1]}},{{&l_736[0][4][4],(void*)0,&l_736[0][1][1],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]},{&l_736[0][4][4],&l_736[0][0][2],&g_368,&l_736[0][0][2],&l_736[0][4][4],(void*)0},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][0][2],&l_736[0][4][4],&g_368,&g_368},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&g_368,&g_368},{&l_736[0][1][1],&g_368,&l_736[0][0][2],(void*)0,&l_736[0][4][4],(void*)0}},{{&g_368,(void*)0,&g_368,&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]},{&l_736[0][0][2],&g_368,&l_736[0][1][1],&g_368,&g_368,&l_736[0][1][1]},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&g_368,&g_368,&l_736[0][4][4]},{&l_736[0][0][2],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]},{&g_368,&l_736[0][0][2],&l_736[0][4][4],(void*)0,&l_736[0][4][4],&l_736[0][4][4]}},{{&l_736[0][1][1],(void*)0,&l_736[0][4][4],&l_736[0][4][4],(void*)0,&l_736[0][1][1]},{&l_736[0][4][4],(void*)0,&l_736[0][1][1],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]},{&l_736[0][4][4],&l_736[0][0][2],&g_368,&l_736[0][0][2],&l_736[0][4][4],(void*)0},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][0][2],&l_736[0][4][4],&g_368,&g_368},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&g_368,&g_368}},{{&l_736[0][1][1],&g_368,&l_736[0][0][2],(void*)0,&l_736[0][4][4],(void*)0},{&g_368,(void*)0,&g_368,&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]},{&l_736[0][0][2],&g_368,&l_736[0][1][1],&g_368,&g_368,&l_736[0][1][1]},{&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&g_368,&g_368,&l_736[0][4][4]},{&l_736[0][0][2],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4],&l_736[0][4][4]}}};
                        int i, j, k;
                        (*g_367) = (((((+(safe_div_func_int64_t_s_s(l_763[4][1][3], 0x6EF5D354AF73A05CLL))) , (((***g_331) < 0x51FC06942BF8E2F6LL) == 248UL)) ^ (p_92 ^ 0x3EL)) >= (((p_92 < (-8L)) || p_93) , 0x93L)) , &g_584[0]);
                        l_774 |= ((safe_div_func_uint16_t_u_u((p_92 || 0xCC07L), (18446744073709551609UL ^ ((safe_lshift_func_uint16_t_u_u((p_92 ^ p_92), 10)) || p_92)))) , (safe_add_func_int64_t_s_s(l_731, (safe_lshift_func_uint64_t_u_u((l_773 , (g_169 <= (-1L))), (*g_514))))));
                        l_752 = p_93;
                    }
                }
                return (*g_23);
            }
        }
    }
    for (p_92 = 0; (p_92 != 51); ++p_92)
    { /* block id: 302 */
        int32_t l_794 = 0xAE036C3DL;
        int32_t l_796 = (-1L);
        (*g_777) = &g_166;
        for (l_100 = 0; (l_100 == 36); l_100 = safe_add_func_int16_t_s_s(l_100, 8))
        { /* block id: 306 */
            int16_t *l_787 = &g_149;
            int32_t l_795 = 0x38A2DA89L;
            l_795 = ((**g_777) = (safe_div_func_uint8_t_u_u((((~(safe_rshift_func_uint16_t_u_u((0UL == (*g_330)), p_93))) , (*g_23)) , ((g_602 = (safe_mod_func_int16_t_s_s(((*l_787) &= (&g_667 != (void*)0)), g_218[1]))) >= (safe_mod_func_int8_t_s_s((safe_lshift_func_uint32_t_u_s((safe_lshift_func_int8_t_s_u((*g_23), l_794)), 29)), p_93)))), (*g_23))));
            l_736[0][4][4] = &l_583[1][0];
        }
        l_796 |= (&p_92 != (void*)0);
    }
    return (*g_23);
}


/* ------------------------------------------ */
/* 
 * reads : g_127 g_218 g_329 g_330 g_302 g_514 g_251 g_23 g_24 g_82 g_62 g_248 g_242 g_539 g_164 g_564 g_166 g_189 g_190
 * writes: g_127 g_149 g_302 g_169 g_166 g_563 g_368
 */
static uint8_t  func_96(uint32_t  p_97, uint32_t * p_98, const int64_t * p_99)
{ /* block id: 192 */
    int32_t l_516 = 0x68F1AE90L;
    for (p_97 = 0; (p_97 <= 2); p_97 += 1)
    { /* block id: 195 */
        int8_t l_557 = 0xD5L;
        uint32_t l_567 = 1UL;
        for (g_127 = 0; (g_127 <= 2); g_127 += 1)
        { /* block id: 198 */
            int32_t l_513 = (-10L);
            int64_t *** const l_528 = &g_464;
            for (g_149 = 0; (g_149 <= 8); g_149 += 1)
            { /* block id: 201 */
                int32_t **l_515 = &g_368;
                int64_t ***l_527 = &g_464;
                uint32_t l_538 = 0xC84A117DL;
                int64_t l_551 = 1L;
                uint32_t ** volatile *l_566 = &g_288;
                uint32_t ** volatile **l_565 = &l_566;
                int i;
                if (((g_218[g_127] | ((((p_97 , 0xC48B76258BA6A2A6LL) >= (--(**g_329))) , ((((((0x3999D381L && (((l_513 | ((l_515 = ((g_514 != (void*)0) , &g_368)) == (void*)0)) ^ 0x77L) ^ g_218[g_127])) <= 0xBC6B3AAE569DF78ALL) , p_97) <= g_251[0][0]) <= 0x51L) < (*g_23))) | l_516)) < 0xB3879D9AL))
                { /* block id: 204 */
                    uint32_t *l_531 = &g_169;
                    (*g_539) = ((safe_mod_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_u(p_97, l_513)) , ((safe_add_func_uint64_t_u_u(((safe_rshift_func_uint8_t_u_s((l_527 == l_528), 6)) & p_97), (g_82 < (safe_add_func_uint32_t_u_u(((*l_531) = g_62), ((((safe_mul_func_int32_t_s_s(g_248, ((safe_sub_func_uint64_t_u_u(((safe_sub_func_uint32_t_u_u((2L > l_538), g_218[1])) , (*g_330)), (**g_329))) != g_24))) >= 0x488B7F67L) < 0x16AC3CC480CAD9B9LL) , 0L)))))) && g_242)), 0x56FD0D4BL)), 0x5FB2L)) > l_513);
                }
                else
                { /* block id: 207 */
                    uint16_t *l_553 = &g_242;
                    uint16_t **l_552 = &l_553;
                    int8_t *l_562[4] = {&l_557,&l_557,&l_557,&l_557};
                    int i;
                    (*g_564) = (safe_mod_func_int64_t_s_s((safe_mod_func_int32_t_s_s((safe_sub_func_int16_t_s_s((l_516 <= ((*g_330) & (~g_218[1]))), (safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_s(l_551, (g_563 = ((((0xF64C0364BA101134LL ^ (&g_248 != ((*l_552) = &g_242))) || (((((~((safe_lshift_func_int8_t_s_u(((l_557 || ((safe_add_func_int8_t_s_s((safe_sub_func_int64_t_s_s(l_513, l_513)), 0UL)) > 5L)) <= p_97), l_513)) <= g_164)) , p_97) & p_97) <= 0UL) <= p_97)) ^ p_97) != 1UL)))), 6)))), l_557)), p_97));
                    if ((*g_539))
                        break;
                    (*l_515) = p_98;
                }
                (*l_565) = &g_288;
                if (p_97)
                    break;
                (*l_515) = p_98;
            }
        }
        return l_567;
    }
    return (**g_189);
}


/* ------------------------------------------ */
/* 
 * reads : g_114 g_127 g_29 g_62 g_24 g_149 g_169 g_164 g_78 g_189 g_166 g_23 g_213 g_82 g_190 g_251 g_218 g_248 g_135 g_282 g_288 g_302 g_328 g_329 g_331 g_367 g_330 g_368 g_396 g_427
 * writes: g_114 g_78 g_29 g_127 g_135 g_149 g_166 g_169 g_213 g_218 g_164 g_242 g_248 g_251 g_190 g_288 g_302 g_329 g_368 g_427
 */
static const int64_t * func_102(const int64_t  p_103, int64_t * p_104, uint32_t * p_105, int8_t * const  p_106, uint32_t  p_107)
{ /* block id: 22 */
    uint32_t *l_125 = &g_78;
    uint8_t *l_145 = &g_127;
    const int32_t l_155 = 0x1E7103E2L;
    int32_t *l_172 = (void*)0;
    int8_t l_220 = 0x1EL;
    int32_t l_246 = (-1L);
    int32_t l_247 = 0L;
    uint16_t l_259[2][8][8] = {{{65526UL,0xDBB4L,65535UL,1UL,0xE8ADL,0UL,0x8A01L,5UL},{0x8BA2L,0x1A06L,5UL,5UL,1UL,5UL,0xEC42L,0UL},{0xEC42L,0x8A01L,6UL,9UL,0x8EFBL,0xEC42L,65526UL,0xA422L},{2UL,65526UL,1UL,0x8BA2L,0xEC42L,0x0948L,65526UL,1UL},{65528UL,0x8BA2L,6UL,0xEC42L,5UL,5UL,0xEC42L,6UL},{5UL,5UL,0xEC42L,6UL,0x8BA2L,65528UL,0x1A06L,1UL},{0x0948L,0xEC42L,0x8BA2L,1UL,65526UL,2UL,5UL,1UL},{0xEC42L,0x8EFBL,9UL,6UL,0x8A01L,0xEC42L,0x8A01L,6UL}},{{1UL,0xDBB4L,1UL,0xEC42L,0UL,5UL,0xDBB4L,1UL},{65535UL,1UL,0xA422L,0x8BA2L,5UL,0xE8ADL,0UL,0xA422L},{65535UL,5UL,1UL,9UL,0UL,0x1A06L,0x1A06L,0UL},{1UL,5UL,5UL,1UL,0x8A01L,1UL,1UL,0xEC42L},{0xEC42L,0UL,0xF927L,0xA422L,65526UL,0xEC42L,0x8EFBL,9UL},{0x0948L,0UL,1UL,1UL,0x8BA2L,1UL,0UL,1UL},{5UL,5UL,1UL,5UL,5UL,0x1A06L,0x8BA2L,1UL},{65528UL,5UL,0UL,0xF927L,0xEC42L,0xE8ADL,0x1A06L,5UL}}};
    uint32_t l_313 = 0UL;
    uint16_t *l_335 = &l_259[1][6][7];
    const int64_t *l_510 = &g_29;
    int i, j, k;
    for (g_114 = 0; (g_114 == 37); g_114 = safe_add_func_uint64_t_u_u(g_114, 4))
    { /* block id: 25 */
        uint32_t *l_124 = &g_78;
        int32_t l_143[9][5] = {{(-2L),0xFED4FFBAL,0xFED4FFBAL,(-2L),0x4494E200L},{0xB19D5435L,0L,0x3A478E54L,(-7L),0L},{(-2L),0xFED4FFBAL,0xFED4FFBAL,(-2L),0x4494E200L},{0xB19D5435L,0L,0x3A478E54L,(-7L),0L},{(-2L),0xFED4FFBAL,0xFED4FFBAL,(-2L),0x4494E200L},{0xB19D5435L,0L,0x3A478E54L,(-7L),0L},{(-2L),0xFED4FFBAL,0xFED4FFBAL,(-2L),0x4494E200L},{0xB19D5435L,0L,0x3A478E54L,(-7L),0L},{(-2L),0xFED4FFBAL,0xFED4FFBAL,(-2L),0x4494E200L}};
        int32_t l_173[6] = {(-1L),(-7L),(-7L),(-1L),(-7L),(-7L)};
        int32_t l_222 = (-1L);
        int64_t **l_233 = (void*)0;
        int64_t l_238 = 0x6A61331B938573CALL;
        int32_t *l_243 = &g_166;
        int32_t *l_244 = &g_166;
        int32_t *l_245[5] = {&l_143[8][4],&l_143[8][4],&l_143[8][4],&l_143[8][4],&l_143[8][4]};
        uint8_t *l_273 = &g_251[2][0];
        int32_t l_281[2][5][4] = {{{(-1L),(-1L),0xA02BDA32L,0xE489FF0DL},{(-1L),(-1L),0xB0B35412L,(-1L)},{1L,0xE489FF0DL,1L,0xB0B35412L},{0x763C7C4FL,0xE489FF0DL,0xA02BDA32L,(-1L)},{0xE489FF0DL,(-1L),(-1L),0xE489FF0DL}},{{1L,1L,0xA02BDA32L,(-1L)},{(-1L),0xB0B35412L,(-1L),0xB0B35412L},{0xB0B35412L,0xA02BDA32L,0x278173FFL,0xB0B35412L},{0x278173FFL,0xB0B35412L,(-1L),(-1L)},{1L,1L,(-1L),(-1L)}}};
        int16_t *l_327[2];
        int16_t l_334 = 3L;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_327[i] = &g_149;
        for (g_78 = 0; (g_78 != 50); g_78++)
        { /* block id: 28 */
            int32_t l_133 = 0xA102B466L;
            uint8_t *l_147 = (void*)0;
            const int32_t l_154 = (-10L);
            int32_t l_223 = 0xD79EB32EL;
            for (g_29 = 0; (g_29 != 20); g_29 = safe_add_func_uint64_t_u_u(g_29, 5))
            { /* block id: 31 */
                const int32_t l_132 = 0xA71313D8L;
                uint8_t *l_146 = (void*)0;
                int64_t *l_160 = &g_29;
                int32_t * const l_171 = &l_143[7][4];
                for (p_107 = (-22); (p_107 >= 7); p_107 = safe_add_func_int64_t_s_s(p_107, 1))
                { /* block id: 34 */
                    uint8_t *l_126 = &g_127;
                    uint64_t *l_134 = &g_135;
                    int32_t l_136[3][4][4] = {{{0x3F9A7C97L,0xD2F62356L,0xFD108918L,0x6650442CL},{1L,0x393687EAL,0x9CFB06A0L,0xFD108918L},{0x7195015FL,0x393687EAL,0x7195015FL,0x6650442CL},{0x393687EAL,0xD2F62356L,3L,(-1L)}},{{(-1L),0xFD108918L,1L,0xD2F62356L},{0xC7375A68L,1L,1L,0xC7375A68L},{(-1L),0x6650442CL,3L,0x7195015FL},{0x393687EAL,1L,0x7195015FL,1L}},{{0x7195015FL,1L,0x9CFB06A0L,1L},{1L,1L,0xFD108918L,0x7195015FL},{0x3F9A7C97L,0x6650442CL,1L,0xC7375A68L},{0xFD108918L,1L,0xD2F62356L,0xD2F62356L}}};
                    int32_t *l_144 = &l_143[8][4];
                    int16_t *l_148 = &g_149;
                    uint16_t l_158 = 0x7E7FL;
                    int i, j, k;
                    (*l_144) = ((((*l_126) &= (l_124 != l_125)) != (safe_rshift_func_uint16_t_u_s((safe_sub_func_uint64_t_u_u(((*l_134) = (l_132 == l_133)), l_136[0][0][2])), 5))) , (safe_div_func_uint16_t_u_u(((((safe_mul_func_int64_t_s_s(((g_29 & (safe_mul_func_uint32_t_u_u(g_62, 4294967295UL))) <= (l_143[8][4] | p_103)), g_24)) || g_29) > g_127) , l_136[1][2][2]), g_29)));
                    if ((((l_146 = l_145) != l_147) < ((((((*l_148) |= l_132) >= (safe_lshift_func_int8_t_s_u(l_143[0][4], p_107))) < (safe_sub_func_int8_t_s_s((l_154 >= ((((*p_106) != (l_155 >= (safe_mul_func_int8_t_s_s((p_103 != 0x9291L), (-1L))))) < 1L) > p_107)), l_143[8][0]))) ^ p_103) <= 4294967295UL)))
                    { /* block id: 40 */
                        return &g_29;
                    }
                    else
                    { /* block id: 42 */
                        if (g_149)
                            break;
                        if (l_158)
                            break;
                    }
                }
                if ((~(-1L)))
                { /* block id: 47 */
                    int64_t **l_161 = (void*)0;
                    int64_t **l_162[5][5] = {{&l_160,(void*)0,(void*)0,&l_160,&l_160},{&l_160,&l_160,&l_160,&l_160,&l_160},{&l_160,&l_160,(void*)0,(void*)0,&l_160},{&l_160,&l_160,&l_160,&l_160,&l_160},{&l_160,(void*)0,(void*)0,&l_160,&l_160}};
                    int64_t *l_163 = &g_164;
                    int16_t *l_165 = &g_149;
                    int i, j;
                    g_169 &= (l_155 , ((g_166 = (5L > ((*l_165) = ((l_163 = l_160) == &p_103)))) || (safe_add_func_uint16_t_u_u(g_127, 0L))));
                    return l_160;
                }
                else
                { /* block id: 53 */
                    uint8_t *l_174[9][2] = {{&g_127,(void*)0},{&g_127,&g_127},{&g_127,&g_127},{(void*)0,&g_127},{(void*)0,&g_127},{&g_127,&g_127},{&g_127,(void*)0},{&g_127,(void*)0},{&g_127,&g_127}};
                    uint8_t **l_175 = &l_146;
                    int32_t l_176 = (-4L);
                    uint32_t *l_212 = &g_213;
                    int32_t *l_214 = (void*)0;
                    int32_t *l_215 = &l_133;
                    int8_t *l_216 = (void*)0;
                    int8_t *l_217 = &g_218[1];
                    int64_t *l_219 = &g_164;
                    int16_t *l_221[2];
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_221[i] = &g_149;
                    l_172 = ((p_107 , ((g_164 , ((!0xDF92E931907E6BEBLL) , p_106)) != l_146)) , l_171);
                    l_133 ^= ((g_78 | ((l_173[2] , (((((*l_175) = l_174[7][1]) != (void*)0) <= ((((l_176 , (*l_171)) >= (((-1L) | (!l_176)) != l_176)) , p_107) & g_24)) , l_176)) , l_173[2])) == p_107);
                    g_166 = (((((((((*l_171) = 0xC49E95A2L) == ((safe_sub_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_u(((safe_add_func_int64_t_s_s((safe_mod_func_uint32_t_u_u((p_107 <= 0x2963C60DL), 0x1443286DL)), (-2L))) | (~((safe_mul_func_int16_t_s_s((g_189 == &l_147), (safe_lshift_func_int8_t_s_s((safe_sub_func_int16_t_s_s(0x182FL, ((safe_div_func_int32_t_s_s((g_164 & g_149), g_78)) || l_133))), 2)))) != (-1L)))), 1)) , 0x7924F713FB69F9E0LL), (-6L))) | p_107)) ^ p_107) , p_107) ^ l_154) || 1L) <= g_78) ^ l_176);
                    if ((safe_unary_minus_func_uint16_t_u(((l_223 = (l_222 &= ((safe_rshift_func_int16_t_s_s((((void*)0 != p_106) | ((p_107 == (((*l_219) = (((((*l_217) = (safe_lshift_func_int16_t_s_s((g_166 <= (safe_div_func_int16_t_s_s(g_24, (((((*l_215) &= ((p_103 , ((safe_rshift_func_uint16_t_u_s(l_176, ((l_160 == (((*l_145)--) , (((*l_212) |= (safe_sub_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_u((*g_23), p_107)) == g_24), g_62))) , &g_135))) ^ 2UL))) & (-7L))) == (*l_171))) ^ p_103) >= l_154) ^ g_164)))), g_78))) & g_29) , l_173[1]) | 0x492C190A5E154013LL)) || 0x48CA7A099D88FCDELL)) , l_220)), 11)) != 0xCEEBA043L))) == g_82))))
                    { /* block id: 66 */
                        uint32_t l_230[8][5] = {{18446744073709551607UL,0xE2DABF53L,18446744073709551607UL,0xE2DABF53L,18446744073709551607UL},{0x01886804L,0x01886804L,0x01886804L,0x01886804L,0x01886804L},{18446744073709551607UL,0xE2DABF53L,18446744073709551607UL,0xE2DABF53L,18446744073709551607UL},{0x01886804L,0x01886804L,0x01886804L,0x01886804L,0x01886804L},{18446744073709551607UL,0xE2DABF53L,18446744073709551607UL,0xE2DABF53L,18446744073709551607UL},{0x01886804L,0x01886804L,0x01886804L,0x01886804L,0x01886804L},{18446744073709551607UL,0xE2DABF53L,18446744073709551607UL,0xE2DABF53L,18446744073709551607UL},{0x01886804L,0x01886804L,0x01886804L,0x01886804L,0x01886804L}};
                        int64_t **l_232 = &l_219;
                        int64_t ***l_231 = &l_232;
                        int i, j;
                        if (p_107)
                            break;
                        (*l_172) = ((safe_mod_func_uint64_t_u_u((safe_lshift_func_uint32_t_u_s((safe_lshift_func_int32_t_s_s((0xECL && p_107), l_230[1][1])), (((((*l_231) = &p_104) != (l_233 = (void*)0)) < (safe_mod_func_int64_t_s_s((l_173[2] , 7L), ((*g_190) && (safe_mul_func_uint32_t_u_u(g_218[1], l_238)))))) == l_133))), p_107)) , 0L);
                    }
                    else
                    { /* block id: 71 */
                        uint16_t *l_241 = &g_242;
                        (*l_172) |= (safe_rshift_func_uint32_t_u_u(1UL, 21));
                        (*l_172) &= (p_107 > ((*l_241) = 0UL));
                    }
                }
            }
        }
        g_248++;
        g_251[2][0]--;
        if (g_62)
        { /* block id: 81 */
            const int16_t l_264 = (-1L);
            int32_t l_279 = 0xF1BBA7F9L;
            uint8_t **l_280 = &g_190;
            uint16_t *l_312 = (void*)0;
            uint64_t *l_322 = &g_302;
            const uint32_t l_362 = 0x434ADF81L;
            int32_t l_422 = 0x77EAE306L;
            if (p_107)
            { /* block id: 82 */
                int64_t l_254 = 0xC9D0AA39097EB817LL;
                uint8_t *l_274 = &g_251[1][1];
                uint8_t **l_275 = (void*)0;
                uint8_t **l_276[5] = {&l_273,&l_273,&l_273,&l_273,&l_273};
                int i;
                l_247 |= (l_254 | (safe_mod_func_int32_t_s_s((safe_div_func_int16_t_s_s((((((((((((l_259[1][0][5] ^ (l_254 ^ (g_242 = (((safe_rshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s((l_264 && ((safe_mod_func_int64_t_s_s((-5L), (safe_mod_func_int8_t_s_s((((safe_add_func_uint64_t_u_u(((((((((safe_add_func_int64_t_s_s((((l_273 == ((*g_189) = l_274)) && (safe_sub_func_int16_t_s_s(((l_279 ^= (-1L)) || (**g_189)), g_166))) <= 0x4FL), p_107)) , l_280) == (void*)0) > l_254) == (-1L)) , 0xE7EC9DE8AB1072B0LL) <= p_103) & l_281[1][3][1]), p_103)) , l_279) && p_107), p_107)))) & p_107)), 8L)), l_264)) | 0x92L) , g_29)))) && 0UL) && 0xDA2174A22506B0F8LL) && 0xEFDCEB63L) | g_135) , p_106) == (void*)0) ^ p_103) > (*p_106)) , g_282[3]) == (void*)0), 0x15FFL)), p_107)));
            }
            else
            { /* block id: 87 */
                int16_t *l_299 = (void*)0;
                int16_t *l_300 = &g_149;
                int8_t *l_301 = &g_218[0];
                int32_t l_307[3][5] = {{0x8540E818L,0xEE46ABD3L,0L,0xEE46ABD3L,0x8540E818L},{0L,0xEE46ABD3L,0L,0x8540E818L,0L},{0L,0L,0L,0x8540E818L,0L}};
                uint32_t *l_314 = (void*)0;
                uint32_t *l_315 = &g_169;
                int i, j;
                for (g_242 = 0; (g_242 <= 33); ++g_242)
                { /* block id: 90 */
                    uint16_t l_291[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_291[i] = 0xDF18L;
                    for (l_220 = 0; (l_220 < (-7)); l_220--)
                    { /* block id: 93 */
                        uint32_t ** volatile *l_290 = &g_288;
                        (*l_290) = g_288;
                        (*l_243) &= g_114;
                        if (g_218[1])
                            continue;
                    }
                    ++l_291[2];
                }
                (*l_244) = (((*p_106) || ((safe_rshift_func_int8_t_s_s((~(*g_23)), ((safe_rshift_func_uint64_t_u_u((65534UL > ((*l_300) = 0x2544L)), ((l_279 = (g_302 = ((*l_301) = (*g_23)))) , 1UL))) || ((*l_315) = ((0L & (((((safe_div_func_int64_t_s_s((safe_rshift_func_uint64_t_u_s((l_307[2][2] > ((*l_125) ^= ((safe_sub_func_uint64_t_u_u((((safe_div_func_int16_t_s_s(((((&l_259[1][0][5] != l_312) != (*g_23)) , p_107) >= l_313), p_107)) , g_251[3][2]) , 0x37289F6784C01210LL), 0x21D277308337835ELL)) & (**g_189)))), 42)), l_313)) , l_264) > l_279) || p_103) , (*p_106))) >= g_114))))) , 255UL)) <= 6UL);
            }
            if ((safe_sub_func_uint8_t_u_u((((*l_244) , p_103) , ((((*g_190) != ((safe_lshift_func_uint64_t_u_u((safe_mod_func_int64_t_s_s(l_220, l_264)), ((*l_322) &= 0x28589467803C6AD3LL))) == (safe_mod_func_uint32_t_u_u((((**g_189) > ((((safe_mod_func_int16_t_s_s((&l_264 == l_327[0]), l_264)) , 0x1CB252DFL) < g_328) , p_107)) > l_264), (*l_243))))) , g_218[1]) | 1L)), 0x20L)))
            { /* block id: 109 */
                (*g_331) = g_329;
            }
            else
            { /* block id: 111 */
                int32_t l_358 = 8L;
                int32_t l_365[6][9][3] = {{{0xEFA34E49L,(-1L),0L},{(-4L),0xC905C787L,5L},{(-8L),0x95B77F47L,0L},{(-4L),6L,(-8L)},{0xEFA34E49L,0xC905C787L,1L},{0xEFA34E49L,0L,(-8L)},{(-4L),(-2L),0x600EE0E7L},{0xF76D7174L,0x5629CD99L,(-8L)},{0x5881B6FDL,0x4D2738DFL,0x5881B6FDL}},{{(-1L),(-4L),0L},{(-1L),5L,0xED27EC4DL},{0x5881B6FDL,(-8L),0xF76D7174L},{0xF76D7174L,0xEFA34E49L,0L},{0x5881B6FDL,0x5629CD99L,0xEE05E7DDL},{(-1L),(-8L),0x24570CD1L},{(-1L),(-8L),0x600EE0E7L},{0x5881B6FDL,0xBE47D957L,0xED27EC4DL},{0xF76D7174L,0x4D2738DFL,0x24570CD1L}},{{0x5881B6FDL,0xEFA34E49L,0x50EFE6FDL},{(-1L),0xBE47D957L,(-8L)},{(-1L),0L,0xF76D7174L},{0x5881B6FDL,(-4L),0x600EE0E7L},{0xF76D7174L,0x5629CD99L,(-8L)},{0x5881B6FDL,0x4D2738DFL,0x5881B6FDL},{(-1L),(-4L),0L},{(-1L),5L,0xED27EC4DL},{0x5881B6FDL,(-8L),0xF76D7174L}},{{0xF76D7174L,0xEFA34E49L,0L},{0x5881B6FDL,0x5629CD99L,0xEE05E7DDL},{(-1L),(-8L),0x24570CD1L},{(-1L),(-8L),0x600EE0E7L},{0x5881B6FDL,0xBE47D957L,0xED27EC4DL},{0xF76D7174L,0x4D2738DFL,0x24570CD1L},{0x5881B6FDL,0xEFA34E49L,0x50EFE6FDL},{(-1L),0xBE47D957L,(-8L)},{(-1L),0L,0xF76D7174L}},{{0x5881B6FDL,(-4L),0x600EE0E7L},{0xF76D7174L,0x5629CD99L,(-8L)},{0x5881B6FDL,0x4D2738DFL,0x5881B6FDL},{(-1L),(-4L),0L},{(-1L),5L,0xED27EC4DL},{0x5881B6FDL,(-8L),0xF76D7174L},{0xF76D7174L,0xEFA34E49L,0L},{0x5881B6FDL,0x5629CD99L,0xEE05E7DDL},{(-1L),(-8L),0x24570CD1L}},{{(-1L),(-8L),0x600EE0E7L},{0x5881B6FDL,0xBE47D957L,0xED27EC4DL},{0xF76D7174L,0x4D2738DFL,0x24570CD1L},{0x5881B6FDL,0xEFA34E49L,0x50EFE6FDL},{(-1L),0xBE47D957L,(-8L)},{(-1L),0L,0xF76D7174L},{0x5881B6FDL,(-4L),0x600EE0E7L},{0xF76D7174L,0x5629CD99L,(-8L)},{0x5881B6FDL,0x4D2738DFL,0x5881B6FDL}}};
                uint32_t l_397 = 0x8318B369L;
                const int16_t *l_418 = &g_149;
                int32_t l_423 = 0x69BFDA5EL;
                const int64_t *l_425 = (void*)0;
                int i, j, k;
                for (g_248 = 0; (g_248 <= 36); ++g_248)
                { /* block id: 114 */
                    uint64_t l_369 = 18446744073709551610UL;
                    if (g_251[2][0])
                        break;
                    if (((l_334 , l_335) != &g_242))
                    { /* block id: 116 */
                        int8_t l_361 = 0x51L;
                        int8_t *l_363 = &g_218[1];
                        int64_t *l_364 = &g_29;
                        int32_t **l_366 = (void*)0;
                        l_365[1][7][0] |= ((*p_106) >= (safe_add_func_uint16_t_u_u(((g_149 &= (p_107 < l_279)) , (safe_sub_func_int32_t_s_s(((&g_282[1] == (void*)0) & (safe_rshift_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((((*l_243) ^= p_103) != (safe_div_func_int32_t_s_s((safe_rshift_func_uint16_t_u_u(((safe_mul_func_uint64_t_u_u(((((*l_364) = ((safe_mod_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(((*l_363) = (safe_add_func_int32_t_s_s((safe_lshift_func_uint32_t_u_s(((l_358 | (65535UL | ((safe_mul_func_int8_t_s_s((-1L), (*g_190))) , 0x7712L))) ^ l_361), l_362)), p_103))), p_103)), p_107)) || l_279)) >= l_264) == (*p_106)), p_103)) ^ l_361), 15)), (-8L)))), g_24)), 4))), g_213))), l_264)));
                        (*g_367) = &g_82;
                    }
                    else
                    { /* block id: 123 */
                        if (p_103)
                            break;
                        if (l_279)
                            break;
                        --l_369;
                    }
                    for (l_358 = 0; (l_358 == (-11)); l_358 = safe_sub_func_uint64_t_u_u(l_358, 7))
                    { /* block id: 130 */
                        return l_322;
                    }
                }
                if ((safe_rshift_func_uint32_t_u_u((g_135 <= (((safe_mod_func_int8_t_s_s((safe_div_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u(((*g_330) = ((((safe_sub_func_uint32_t_u_u((((*l_124) = (+((*g_190) | (safe_mod_func_uint32_t_u_u(((((*g_330) > ((**g_367) && (l_365[1][7][0] < (((safe_mul_func_uint32_t_u_u((0x74F29AA272F2B780LL < l_155), ((safe_div_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((safe_lshift_func_int64_t_s_u((+1L), 15)), g_396)), 4294967295UL)) ^ (*g_190)))) , l_279) , 1L)))) , l_322) != (void*)0), (-7L)))))) && p_107), l_365[1][5][1])) == p_107) , (**g_329)) != l_397)), p_107)), (*g_368))), 255UL)) && 0UL) > 0x6F17395755694701LL)), 8)))
                { /* block id: 136 */
                    uint64_t l_421 = 0x3B850C3E0D10F8B9LL;
                    l_423 = ((safe_rshift_func_uint64_t_u_u((((*l_244) = 0xFF92C7C0L) ^ (l_422 = ((((((safe_lshift_func_int32_t_s_u((safe_add_func_uint32_t_u_u((l_365[1][7][0] = ((-1L) <= (((safe_unary_minus_func_int64_t_s(l_264)) & ((((*l_125) = (((safe_sub_func_int64_t_s_s((~((safe_add_func_uint16_t_u_u(((((safe_sub_func_uint16_t_u_u((((((safe_mod_func_int8_t_s_s(((safe_div_func_int16_t_s_s((((safe_sub_func_uint64_t_u_u((((void*)0 == l_418) && (safe_mul_func_int32_t_s_s(p_103, 5L))), 0xE8B0EF38B845C3CALL)) , (void*)0) == (void*)0), 0x0CACL)) , (-1L)), (*g_23))) & g_251[2][2]) | p_103) | p_107) && (**g_329)), (-6L))) && (***g_331)) , 0UL) > (*g_368)), p_103)) <= g_164)), 0x69DB755043EF79BDLL)) == p_103) != (*g_190))) >= g_251[2][0]) || l_421)) | p_107))), 0xCD5755D4L)), 26)) , l_358) || l_362) >= l_421) == (*g_23)) == 0L))), l_397)) != p_103);
                }
                else
                { /* block id: 142 */
                    int32_t l_426[7] = {0xEFC90063L,(-1L),0xEFC90063L,0xEFC90063L,(-1L),0xEFC90063L,0xEFC90063L};
                    int i;
                    for (l_246 = 2; (l_246 >= 0); l_246 -= 1)
                    { /* block id: 145 */
                        const int64_t *l_424 = &l_238;
                        return l_425;
                    }
                    --g_427;
                    (*l_243) = (-1L);
                }
            }
        }
        else
        { /* block id: 152 */
            uint32_t l_430 = 4294967289UL;
            int32_t **l_433 = (void*)0;
            int32_t **l_434 = &l_244;
            const int64_t *l_468 = (void*)0;
            int64_t l_508 = 3L;
            --l_430;
            (*g_367) = ((*l_434) = &l_246);
            for (g_302 = 0; (g_302 <= 2); g_302 += 1)
            { /* block id: 158 */
                int64_t **l_444[3][1][5] = {{{&g_283,&g_283,&g_283,&g_283,&g_283}},{{&g_283,&g_283,&g_283,&g_283,&g_283}},{{&g_283,(void*)0,(void*)0,&g_283,(void*)0}}};
                int64_t l_507 = 3L;
                int i, j, k;
                (*g_368) &= ((+(-1L)) > 0UL);
                (*l_434) = &l_143[(g_302 + 4)][g_302];
                (*g_368) |= ((safe_lshift_func_int32_t_s_s(((*l_243) = (safe_add_func_uint8_t_u_u((safe_add_func_int16_t_s_s(g_218[g_302], g_164)), ((((((safe_mul_func_uint64_t_u_u(((l_444[2][0][1] == &p_104) || p_103), 0x16652E78C9CE54CFLL)) , (((~255UL) >= l_143[(g_302 + 4)][g_302]) & (safe_lshift_func_int8_t_s_u(((p_107 && p_107) ^ (*p_106)), g_218[g_302])))) , p_103) >= p_107) && (-8L)) , (*p_106))))), p_107)) && g_213);
                for (l_246 = 1; (l_246 >= 0); l_246 -= 1)
                { /* block id: 165 */
                    int32_t l_454 = 0x96339FD3L;
                    int8_t * const l_471 = &g_218[g_302];
                    uint32_t l_509 = 0x2F779442L;
                    int i, j, k;
                }
            }
        }
    }
    return l_510;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_18[i], "g_18[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_24, "g_24", print_hash_value);
    transparent_crc(g_29, "g_29", print_hash_value);
    transparent_crc(g_62, "g_62", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_82, "g_82", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc(g_149, "g_149", print_hash_value);
    transparent_crc(g_164, "g_164", print_hash_value);
    transparent_crc(g_166, "g_166", print_hash_value);
    transparent_crc(g_169, "g_169", print_hash_value);
    transparent_crc(g_213, "g_213", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_218[i], "g_218[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_242, "g_242", print_hash_value);
    transparent_crc(g_248, "g_248", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_251[i][j], "g_251[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_302, "g_302", print_hash_value);
    transparent_crc(g_328, "g_328", print_hash_value);
    transparent_crc(g_396, "g_396", print_hash_value);
    transparent_crc(g_427, "g_427", print_hash_value);
    transparent_crc(g_563, "g_563", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_584[i], "g_584[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_602, "g_602", print_hash_value);
    transparent_crc(g_663, "g_663", print_hash_value);
    transparent_crc(g_676, "g_676", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_737[i], "g_737[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_800, "g_800", print_hash_value);
    transparent_crc(g_802, "g_802", print_hash_value);
    transparent_crc(g_850, "g_850", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_940[i], "g_940[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1120, "g_1120", print_hash_value);
    transparent_crc(g_1149, "g_1149", print_hash_value);
    transparent_crc(g_1166, "g_1166", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1234[i], "g_1234[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1283, "g_1283", print_hash_value);
    transparent_crc(g_1361, "g_1361", print_hash_value);
    transparent_crc(g_1405, "g_1405", print_hash_value);
    transparent_crc(g_1494, "g_1494", print_hash_value);
    transparent_crc(g_1714, "g_1714", print_hash_value);
    transparent_crc(g_1749, "g_1749", print_hash_value);
    transparent_crc(g_1847, "g_1847", print_hash_value);
    transparent_crc(g_1864, "g_1864", print_hash_value);
    transparent_crc(g_1884, "g_1884", print_hash_value);
    transparent_crc(g_2158, "g_2158", print_hash_value);
    transparent_crc(g_2191, "g_2191", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_2201[i], "g_2201[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2399, "g_2399", print_hash_value);
    transparent_crc(g_2751, "g_2751", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2826[i], "g_2826[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2833, "g_2833", print_hash_value);
    transparent_crc(g_2899, "g_2899", print_hash_value);
    transparent_crc(g_3034, "g_3034", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 709
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 228
   depth: 2, occurrence: 64
   depth: 3, occurrence: 4
   depth: 4, occurrence: 1
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 12, occurrence: 3
   depth: 13, occurrence: 2
   depth: 15, occurrence: 3
   depth: 16, occurrence: 2
   depth: 17, occurrence: 1
   depth: 18, occurrence: 4
   depth: 19, occurrence: 2
   depth: 20, occurrence: 4
   depth: 21, occurrence: 3
   depth: 22, occurrence: 1
   depth: 23, occurrence: 3
   depth: 24, occurrence: 3
   depth: 25, occurrence: 1
   depth: 26, occurrence: 3
   depth: 27, occurrence: 1
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 2
   depth: 31, occurrence: 2
   depth: 33, occurrence: 2
   depth: 34, occurrence: 3
   depth: 36, occurrence: 1
   depth: 38, occurrence: 1
   depth: 42, occurrence: 1
   depth: 43, occurrence: 1
   depth: 52, occurrence: 1

XXX total number of pointers: 563

XXX times a variable address is taken: 1556
XXX times a pointer is dereferenced on RHS: 447
breakdown:
   depth: 1, occurrence: 334
   depth: 2, occurrence: 91
   depth: 3, occurrence: 22
XXX times a pointer is dereferenced on LHS: 384
breakdown:
   depth: 1, occurrence: 337
   depth: 2, occurrence: 35
   depth: 3, occurrence: 10
   depth: 4, occurrence: 2
XXX times a pointer is compared with null: 57
XXX times a pointer is compared with address of another variable: 19
XXX times a pointer is compared with another pointer: 19
XXX times a pointer is qualified to be dereferenced: 8683

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1548
   level: 2, occurrence: 422
   level: 3, occurrence: 126
   level: 4, occurrence: 27
   level: 5, occurrence: 32
XXX number of pointers point to pointers: 284
XXX number of pointers point to scalars: 279
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 34.8
XXX average alias set size: 1.52

XXX times a non-volatile is read: 2563
XXX times a non-volatile is write: 1125
XXX times a volatile is read: 142
XXX    times read thru a pointer: 32
XXX times a volatile is write: 73
XXX    times written thru a pointer: 11
XXX times a volatile is available for access: 1.85e+03
XXX percentage of non-volatile access: 94.5

XXX forward jumps: 4
XXX backward jumps: 8

XXX stmts: 243
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 24
   depth: 1, occurrence: 33
   depth: 2, occurrence: 36
   depth: 3, occurrence: 49
   depth: 4, occurrence: 53
   depth: 5, occurrence: 48

XXX percentage a fresh-made variable is used: 16.5
XXX percentage an existing variable is used: 83.5
XXX total OOB instances added: 0
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3256876376
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   const signed f0 : 26;
   volatile uint16_t  f1;
};

union U1 {
   int64_t  f0;
   volatile float  f1;
   uint32_t  f2;
   float  f3;
   signed f4 : 16;
};

/* --- GLOBAL VARIABLES --- */
static int64_t g_3 = 0L;
static int32_t g_5 = 0x11151C3CL;
static int32_t g_12[4][6][1] = {{{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L}},{{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L}},{{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L}},{{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L},{0x6B495482L}}};
static uint32_t g_14 = 1UL;
static float g_27 = 0x0.00BAF3p+33;
static float *g_26 = &g_27;
static int16_t g_43 = 0x8818L;
static int16_t g_49 = 6L;
static volatile union U0 g_69 = {0x58A71D4FL};/* VOLATILE GLOBAL g_69 */
static int32_t g_84 = 0xFA302576L;
static uint32_t g_89 = 4UL;
static union U1 g_95 = {-1L};/* VOLATILE GLOBAL g_95 */
static union U1 *g_98 = &g_95;
static volatile union U0 g_102 = {1L};/* VOLATILE GLOBAL g_102 */
static volatile union U0 *g_101 = &g_102;
static volatile uint16_t g_108 = 0x702EL;/* VOLATILE GLOBAL g_108 */
static uint16_t g_112 = 65535UL;
static uint16_t g_136 = 0x8590L;
static int8_t g_140 = 0L;
static uint32_t *g_162 = (void*)0;
static uint32_t **g_161 = &g_162;
static int32_t *g_173[2][3] = {{&g_12[2][4][0],&g_84,&g_12[2][4][0]},{&g_12[2][4][0],&g_84,&g_12[2][4][0]}};
static int32_t **g_172 = &g_173[0][2];
static const union U1 g_194 = {0x1FCDCBAFF72D6A1BLL};/* VOLATILE GLOBAL g_194 */
static const union U1 g_196 = {1L};/* VOLATILE GLOBAL g_196 */
static uint64_t g_205 = 18446744073709551612UL;
static float g_226 = 0x8.Ap-1;
static float g_229 = 0x8.952072p-46;
static volatile union U0 ** const  volatile g_239 = &g_101;/* VOLATILE GLOBAL g_239 */
static int64_t g_253[7] = {(-8L),(-8L),(-8L),(-8L),(-8L),(-8L),(-8L)};
static union U0 g_261 = {0xDDA60239L};/* VOLATILE GLOBAL g_261 */
static volatile union U0 g_292 = {0x65222BB7L};/* VOLATILE GLOBAL g_292 */
static union U0 g_298 = {0xD39461BCL};/* VOLATILE GLOBAL g_298 */
static union U0 *g_297 = &g_298;
static union U0 g_300 = {0x96DD3B48L};/* VOLATILE GLOBAL g_300 */
static union U0 g_302 = {-1L};/* VOLATILE GLOBAL g_302 */
static union U0 g_305 = {-1L};/* VOLATILE GLOBAL g_305 */
static union U0 g_306 = {0x409E2346L};/* VOLATILE GLOBAL g_306 */
static union U0 g_307 = {0x444259CDL};/* VOLATILE GLOBAL g_307 */
static union U0 g_308[1] = {{0xA202B604L}};
static const union U1 *g_422 = (void*)0;
static const union U1 ** volatile g_421 = &g_422;/* VOLATILE GLOBAL g_421 */
static uint32_t *** volatile g_426 = &g_161;/* VOLATILE GLOBAL g_426 */
static int32_t g_461[8] = {3L,3L,3L,3L,3L,3L,3L,3L};
static const union U1 **g_466 = &g_422;
static const union U1 *** volatile g_465 = &g_466;/* VOLATILE GLOBAL g_465 */
static uint32_t g_505 = 1UL;
static volatile int32_t g_513 = 0L;/* VOLATILE GLOBAL g_513 */
static uint32_t ***g_536 = &g_161;
static volatile float g_543 = (-0x8.3p-1);/* VOLATILE GLOBAL g_543 */
static volatile union U1 g_561 = {0xE012FC3BE621C992LL};/* VOLATILE GLOBAL g_561 */
static int32_t *g_566 = &g_12[3][1][0];
static int32_t ** volatile g_565 = &g_566;/* VOLATILE GLOBAL g_565 */
static union U1 g_573[4] = {{0L},{0L},{0L},{0L}};
static union U0 ***g_576 = (void*)0;
static volatile int32_t * volatile g_669 = (void*)0;/* VOLATILE GLOBAL g_669 */
static volatile int32_t * volatile * volatile g_670 = &g_669;/* VOLATILE GLOBAL g_670 */
static uint8_t g_736 = 0UL;
static const uint16_t g_841 = 1UL;
static uint32_t ****g_848[6][1][4] = {{{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0}}};
static uint32_t *****g_847 = &g_848[5][0][3];
static volatile union U0 g_849 = {0xA5BADA2AL};/* VOLATILE GLOBAL g_849 */
static int32_t * volatile g_889[1][2] = {{&g_84,&g_84}};
static volatile int32_t * volatile * volatile g_930 = &g_669;/* VOLATILE GLOBAL g_930 */
static union U1 g_941[2] = {{9L},{9L}};
static const uint32_t g_969 = 18446744073709551615UL;
static union U1 g_977 = {0x5F48A30844A70272LL};/* VOLATILE GLOBAL g_977 */
static volatile int32_t * volatile * volatile g_1011 = &g_669;/* VOLATILE GLOBAL g_1011 */
static uint32_t *g_1070 = &g_505;
static uint32_t **g_1069 = &g_1070;
static uint32_t ***g_1068 = &g_1069;
static volatile union U1 g_1100 = {0xD0E857878602B690LL};/* VOLATILE GLOBAL g_1100 */
static volatile int32_t * volatile * volatile g_1122 = &g_669;/* VOLATILE GLOBAL g_1122 */
static volatile int32_t g_1138 = (-7L);/* VOLATILE GLOBAL g_1138 */
static int8_t **g_1146[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int8_t * volatile * volatile g_1162 = (void*)0;/* VOLATILE GLOBAL g_1162 */
static const int8_t **g_1187 = (void*)0;
static int64_t g_1189 = 2L;
static const volatile float *g_1199 = (void*)0;
static const volatile float ** volatile g_1198[4][8][8] = {{{&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,(void*)0,(void*)0,&g_1199,&g_1199,&g_1199},{&g_1199,(void*)0,&g_1199,&g_1199,(void*)0,&g_1199,(void*)0,(void*)0},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199},{(void*)0,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,(void*)0,&g_1199},{&g_1199,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,(void*)0},{&g_1199,(void*)0,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199}},{{(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,(void*)0,&g_1199},{&g_1199,(void*)0,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199},{&g_1199,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,(void*)0},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,(void*)0,(void*)0,&g_1199}},{{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,(void*)0},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{(void*)0,&g_1199,&g_1199,(void*)0,(void*)0,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{(void*)0,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,(void*)0},{(void*)0,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199}},{{&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199,(void*)0},{&g_1199,&g_1199,(void*)0,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199},{&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199,&g_1199}}};
static const volatile float ** volatile * volatile g_1197[10][3] = {{&g_1198[2][4][3],&g_1198[2][4][3],&g_1198[2][4][3]},{(void*)0,(void*)0,(void*)0},{&g_1198[2][4][3],&g_1198[2][4][3],&g_1198[2][4][3]},{(void*)0,(void*)0,(void*)0},{&g_1198[2][4][3],&g_1198[2][4][3],&g_1198[2][4][3]},{(void*)0,(void*)0,(void*)0},{&g_1198[2][4][3],&g_1198[2][4][3],&g_1198[2][4][3]},{(void*)0,(void*)0,(void*)0},{&g_1198[2][4][3],&g_1198[2][4][3],&g_1198[2][4][3]},{(void*)0,(void*)0,(void*)0}};
static volatile int32_t g_1228[1][3][1] = {{{0x92028322L},{0x92028322L},{0x92028322L}}};
static volatile int32_t *g_1227 = &g_1228[0][0][0];
static volatile int32_t * volatile *g_1226[9] = {&g_1227,&g_1227,&g_1227,&g_1227,&g_1227,&g_1227,&g_1227,&g_1227,&g_1227};
static volatile int32_t * volatile * volatile g_1236 = &g_669;/* VOLATILE GLOBAL g_1236 */
static uint16_t g_1248[9][7][4] = {{{1UL,0xBCB3L,65534UL,65534UL},{1UL,1UL,1UL,65534UL},{0xAB23L,0xBCB3L,0xAB23L,1UL},{0xAB23L,1UL,1UL,0xAB23L},{1UL,1UL,65534UL,1UL},{1UL,0xBCB3L,65534UL,65534UL},{1UL,1UL,1UL,65534UL}},{{0xAB23L,0xBCB3L,0xAB23L,1UL},{0xAB23L,1UL,1UL,0xAB23L},{1UL,1UL,65534UL,1UL},{1UL,0xBCB3L,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL}},{{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL},{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L}},{{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL},{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L}},{{65534UL,0xAB23L,0xAB23L,65534UL},{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL},{1UL,0xAB23L,0xBCB3L,0xAB23L}},{{0xAB23L,1UL,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL},{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L}},{{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL},{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL}},{{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L},{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,0xAB23L,0xAB23L,65534UL},{1UL,0xAB23L,0xBCB3L,0xAB23L},{0xAB23L,1UL,0xBCB3L,0xBCB3L}},{{1UL,1UL,0xAB23L,0xBCB3L},{65534UL,1UL,65534UL,0xAB23L},{65534UL,65534UL,65534UL,0xBCB3L},{0xAB23L,65534UL,1UL,65534UL},{65534UL,1UL,1UL,1UL},{0xAB23L,0xAB23L,65534UL,1UL},{0xBCB3L,1UL,0xBCB3L,65534UL}}};
static volatile int32_t * volatile * volatile g_1249 = &g_669;/* VOLATILE GLOBAL g_1249 */
static int32_t * const *g_1270 = (void*)0;
static volatile union U1 g_1273[3][2][5] = {{{{0xD9118851594E273CLL},{0xDC8079141A04940FLL},{0xD9118851594E273CLL},{0xDC8079141A04940FLL},{0xD9118851594E273CLL}},{{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL}}},{{{0xD9118851594E273CLL},{0xDC8079141A04940FLL},{0xD9118851594E273CLL},{0xDC8079141A04940FLL},{0xD9118851594E273CLL}},{{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL}}},{{{0xD9118851594E273CLL},{0xDC8079141A04940FLL},{0xD9118851594E273CLL},{0xDC8079141A04940FLL},{0xD9118851594E273CLL}},{{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL},{0xE83E513DAE61BAFELL}}}};
static volatile union U1 g_1299 = {0x7AAB5D5B7FC9A685LL};/* VOLATILE GLOBAL g_1299 */
static int32_t g_1310 = 0xBA3AC81FL;
static volatile int32_t * volatile *g_1316 = &g_669;
static volatile int32_t * volatile **g_1315 = &g_1316;
static union U1 g_1352[9][10][2] = {{{{0L},{0L}},{{1L},{0L}},{{0x4433AABA28558BFALL},{-3L}},{{0x19666F12A693D833LL},{0x9D2FA84845845230LL}},{{0x74693C422BC9836ALL},{0x19666F12A693D833LL}},{{0x0F3AE938863C5F8CLL},{3L}},{{0x0F3AE938863C5F8CLL},{0x19666F12A693D833LL}},{{0x74693C422BC9836ALL},{0x9D2FA84845845230LL}},{{0x19666F12A693D833LL},{-3L}},{{0x4433AABA28558BFALL},{0L}}},{{{1L},{0L}},{{0L},{-3L}},{{0x35F6F0C4467E1F26LL},{0x0F3AE938863C5F8CLL}},{{0x74693C422BC9836ALL},{0x35F6F0C4467E1F26LL}},{{1L},{3L}},{{0x9D2FA84845845230LL},{0L}},{{0x74693C422BC9836ALL},{1L}},{{0L},{-3L}},{{0L},{0x4433AABA28558BFALL}},{{1L},{0x4433AABA28558BFALL}}},{{{0L},{-3L}},{{0L},{1L}},{{0x74693C422BC9836ALL},{0L}},{{0x9D2FA84845845230LL},{3L}},{{1L},{0x35F6F0C4467E1F26LL}},{{0x74693C422BC9836ALL},{0x0F3AE938863C5F8CLL}},{{0x35F6F0C4467E1F26LL},{-3L}},{{0L},{0L}},{{1L},{0L}},{{0x4433AABA28558BFALL},{-3L}}},{{{0x19666F12A693D833LL},{0x9D2FA84845845230LL}},{{0x74693C422BC9836ALL},{0x19666F12A693D833LL}},{{0x0F3AE938863C5F8CLL},{3L}},{{0x0F3AE938863C5F8CLL},{0x19666F12A693D833LL}},{{0x74693C422BC9836ALL},{0x9D2FA84845845230LL}},{{0x19666F12A693D833LL},{-3L}},{{0x4433AABA28558BFALL},{0L}},{{1L},{0L}},{{0L},{-3L}},{{0x35F6F0C4467E1F26LL},{0x0F3AE938863C5F8CLL}}},{{{0x74693C422BC9836ALL},{0x35F6F0C4467E1F26LL}},{{1L},{3L}},{{0x9D2FA84845845230LL},{0L}},{{0x74693C422BC9836ALL},{1L}},{{0L},{-3L}},{{0L},{0x4433AABA28558BFALL}},{{1L},{0x4433AABA28558BFALL}},{{0L},{-3L}},{{0L},{1L}},{{0x74693C422BC9836ALL},{0L}}},{{{0x9D2FA84845845230LL},{3L}},{{1L},{0x35F6F0C4467E1F26LL}},{{0x74693C422BC9836ALL},{0x0F3AE938863C5F8CLL}},{{0x35F6F0C4467E1F26LL},{-3L}},{{0L},{0L}},{{1L},{0L}},{{0x4433AABA28558BFALL},{-3L}},{{0x19666F12A693D833LL},{0x9D2FA84845845230LL}},{{0x74693C422BC9836ALL},{0x19666F12A693D833LL}},{{0x0F3AE938863C5F8CLL},{3L}}},{{{0x0F3AE938863C5F8CLL},{0x19666F12A693D833LL}},{{0x74693C422BC9836ALL},{0x9D2FA84845845230LL}},{{0x19666F12A693D833LL},{-3L}},{{0x4433AABA28558BFALL},{0L}},{{1L},{0L}},{{0L},{-3L}},{{0x35F6F0C4467E1F26LL},{0x0F3AE938863C5F8CLL}},{{0x74693C422BC9836ALL},{0x35F6F0C4467E1F26LL}},{{1L},{3L}},{{0x9D2FA84845845230LL},{0L}}},{{{0x74693C422BC9836ALL},{1L}},{{3L},{0xD92963D33749F215LL}},{{0x74693C422BC9836ALL},{1L}},{{0x5E978BFEF2885C18LL},{1L}},{{0x74693C422BC9836ALL},{0xD92963D33749F215LL}},{{3L},{3L}},{{-1L},{3L}},{{0xBA04F6F557090B44LL},{0x72C470A072145016LL}},{{3L},{0x169E27B5F5CF7186LL}},{{-1L},{1L}}},{{{0x169E27B5F5CF7186LL},{0xD92963D33749F215LL}},{{-7L},{-7L}},{{0x5E978BFEF2885C18LL},{0x74693C422BC9836ALL}},{{1L},{0xD92963D33749F215LL}},{{-3L},{0xBA04F6F557090B44LL}},{{-1L},{-3L}},{{1L},{0x72C470A072145016LL}},{{1L},{-3L}},{{-1L},{0xBA04F6F557090B44LL}},{{-3L},{0xD92963D33749F215LL}}}};
static union U1 * const g_1351[1] = {&g_1352[6][7][0]};
static union U1 * const *g_1350 = &g_1351[0];
static int8_t g_1362[2] = {1L,1L};
static int8_t g_1375 = 0x4AL;
static volatile int32_t g_1399 = 0xAF02B204L;/* VOLATILE GLOBAL g_1399 */
static union U1 ** volatile g_1411[8][7] = {{&g_98,&g_98,(void*)0,&g_98,&g_98,(void*)0,&g_98},{&g_98,&g_98,&g_98,&g_98,&g_98,&g_98,&g_98},{(void*)0,&g_98,(void*)0,(void*)0,&g_98,(void*)0,(void*)0},{&g_98,&g_98,&g_98,&g_98,&g_98,&g_98,&g_98},{&g_98,(void*)0,(void*)0,&g_98,(void*)0,(void*)0,&g_98},{&g_98,&g_98,&g_98,&g_98,&g_98,&g_98,&g_98},{&g_98,&g_98,(void*)0,&g_98,&g_98,(void*)0,&g_98},{&g_98,&g_98,&g_98,&g_98,&g_98,&g_98,&g_98}};
static union U1 g_1442 = {0L};/* VOLATILE GLOBAL g_1442 */
static volatile union U1 g_1495 = {0x8D82A773B382E074LL};/* VOLATILE GLOBAL g_1495 */
static union U1 g_1558 = {-9L};/* VOLATILE GLOBAL g_1558 */
static volatile union U0 g_1573 = {2L};/* VOLATILE GLOBAL g_1573 */
static uint16_t * volatile g_1575 = &g_136;/* VOLATILE GLOBAL g_1575 */
static uint16_t * volatile * volatile g_1574 = &g_1575;/* VOLATILE GLOBAL g_1574 */
static volatile union U1 g_1582 = {0xE8CCBC625C6FB434LL};/* VOLATILE GLOBAL g_1582 */
static int32_t g_1586 = 0L;
static union U1 **g_1593[10] = {&g_98,(void*)0,&g_98,(void*)0,&g_98,&g_98,(void*)0,&g_98,(void*)0,&g_98};
static union U1 ***g_1592 = &g_1593[2];
static union U1 ****g_1591[1][9][10] = {{{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,&g_1592},{&g_1592,&g_1592,(void*)0,&g_1592,&g_1592,&g_1592,(void*)0,&g_1592,(void*)0,&g_1592}}};
static volatile union U1 g_1599 = {2L};/* VOLATILE GLOBAL g_1599 */
static const volatile union U1 g_1628 = {7L};/* VOLATILE GLOBAL g_1628 */
static int8_t ***** volatile g_1632 = (void*)0;/* VOLATILE GLOBAL g_1632 */
static int8_t ***g_1635 = &g_1146[5];
static int8_t ****g_1634 = &g_1635;
static int8_t ***** const  volatile g_1633[9][3] = {{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634},{&g_1634,&g_1634,&g_1634}};
static int8_t ***** volatile g_1636[8] = {&g_1634,&g_1634,&g_1634,&g_1634,&g_1634,&g_1634,&g_1634,&g_1634};
static int8_t ***** volatile g_1637 = &g_1634;/* VOLATILE GLOBAL g_1637 */
static volatile union U0 ** volatile g_1638 = &g_101;/* VOLATILE GLOBAL g_1638 */
static uint32_t g_1662 = 0xE1B372C4L;
static volatile union U1 g_1725 = {0x052F9121C9AD4532LL};/* VOLATILE GLOBAL g_1725 */
static int32_t ** volatile g_1758 = &g_566;/* VOLATILE GLOBAL g_1758 */
static uint8_t g_1817 = 1UL;
static uint32_t **g_1870 = &g_162;
static uint32_t *** const g_1869 = &g_1870;
static uint32_t *** const *g_1868 = &g_1869;
static uint32_t *** const **g_1867 = &g_1868;
static union U0 g_1890 = {-1L};/* VOLATILE GLOBAL g_1890 */
static volatile union U0 ** const  volatile g_1935 = &g_101;/* VOLATILE GLOBAL g_1935 */
static float *g_1999 = (void*)0;
static union U0 g_2045 = {0xE75331E3L};/* VOLATILE GLOBAL g_2045 */
static union U1 g_2110 = {3L};/* VOLATILE GLOBAL g_2110 */
static uint8_t g_2175 = 0x11L;
static int32_t g_2188 = 0x8967C74EL;
static union U1 g_2267 = {0x2DEDBDE70362B2AFLL};/* VOLATILE GLOBAL g_2267 */
static union U1 *****g_2283[3][2] = {{&g_1591[0][8][0],&g_1591[0][8][0]},{&g_1591[0][8][0],&g_1591[0][8][0]},{&g_1591[0][8][0],&g_1591[0][8][0]}};
static const uint32_t * const *g_2444 = (void*)0;
static volatile int32_t * volatile ** const g_2452 = &g_1226[2];
static union U0 g_2468[3][1] = {{{-1L}},{{-1L}},{{-1L}}};
static const uint16_t *g_2528 = &g_1248[8][2][1];
static const uint16_t **g_2527 = &g_2528;
static float **g_2540[4] = {&g_26,&g_26,&g_26,&g_26};
static float ***g_2539 = &g_2540[3];
static float ****g_2538[5] = {&g_2539,&g_2539,&g_2539,&g_2539,&g_2539};
static union U0 g_2553 = {0x8472449FL};/* VOLATILE GLOBAL g_2553 */


/* --- FORWARD DECLARATIONS --- */
static const uint16_t  func_1(void);
static int32_t * func_15(int32_t  p_16, float * p_17, float * p_18, uint32_t  p_19, const uint16_t  p_20);
static float * func_21(int32_t  p_22, float * p_23, int32_t * p_24, int8_t  p_25);
static int8_t  func_36(uint32_t  p_37, int8_t  p_38, float  p_39);
static uint16_t  func_44(int8_t  p_45);
static uint32_t  func_53(uint32_t  p_54);
static uint32_t ** func_55(int32_t * p_56);
static int8_t  func_65(int32_t * p_66);
static union U0  func_77(int64_t  p_78, int16_t  p_79);
static int16_t  func_80(const uint8_t  p_81, uint16_t  p_82);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const uint16_t  func_1(void)
{ /* block id: 0 */
    uint8_t l_2 = 253UL;
    int32_t *l_4 = &g_5;
    uint32_t l_10[9][6][4] = {{{18446744073709551610UL,0x3B06D9E9L,0x1B78CD90L,18446744073709551609UL},{0x7B17B4D2L,1UL,18446744073709551615UL,18446744073709551609UL},{0x997EC53AL,0x3B06D9E9L,0x1B27067DL,0xCD43A1E2L},{18446744073709551609UL,18446744073709551615UL,0x72E7EB66L,9UL},{0UL,0x7DD14560L,0x93D7F285L,18446744073709551615UL},{0xF11BBB4FL,18446744073709551615UL,0x0DEEA39BL,7UL}},{{0x4416DA84L,0x3A50EB6BL,0UL,0x7DD14560L},{18446744073709551615UL,1UL,0x15BFEB1EL,0UL},{0xF1939EFDL,0x867A7BF7L,1UL,6UL},{6UL,0xAC3D6C1DL,0UL,18446744073709551610UL},{18446744073709551610UL,0x3C27F355L,0xF8169C14L,8UL},{0UL,0x1B27067DL,0xCC80FC3AL,0x15BFEB1EL}},{{1UL,18446744073709551615UL,0x8DC5336DL,7UL},{0xAC3D6C1DL,0x132E8824L,0xE26B5CFDL,18446744073709551608UL},{1UL,0x997EC53AL,0xCDF9C775L,0xAC3D6C1DL},{0xB5FB49A5L,0x1B78CD90L,0x450BB101L,0x80095520L},{1UL,0x8DC5336DL,1UL,0x3A50EB6BL},{0xC245E320L,18446744073709551615UL,0x7B17B4D2L,0x99C77E21L}},{{0UL,1UL,18446744073709551610UL,0xE936BE85L},{0x132E8824L,0x6C2E3A4CL,0x80095520L,18446744073709551615UL},{6UL,18446744073709551615UL,0x3C27F355L,0x0DEEA39BL},{0x3A50EB6BL,0x93D7F285L,0x997EC53AL,0x0C8FBFC7L},{0x7A14A9C7L,18446744073709551607UL,0x08BBD188L,18446744073709551607UL},{0x0C8FBFC7L,7UL,0x38299FA9L,1UL}},{{0x25C7E4DDL,1UL,0x99C77E21L,0xC6F73980L},{7UL,0xE936BE85L,18446744073709551615UL,0x4416DA84L},{7UL,18446744073709551607UL,0x99C77E21L,0x132E8824L},{0x25C7E4DDL,0x4416DA84L,0x38299FA9L,0x1B78CD90L},{0x0C8FBFC7L,18446744073709551615UL,0x08BBD188L,18446744073709551615UL},{0x7A14A9C7L,18446744073709551610UL,0x997EC53AL,0x4F823AAFL}},{{0x3A50EB6BL,0x08BBD188L,0x3C27F355L,18446744073709551610UL},{6UL,0x1466AC3AL,0x80095520L,1UL},{0x132E8824L,18446744073709551615UL,18446744073709551610UL,0x7A14A9C7L},{0xC245E320L,18446744073709551615UL,6UL,0x3C27F355L},{0xFF5FF74FL,18446744073709551608UL,18446744073709551615UL,18446744073709551615UL},{7UL,18446744073709551610UL,0x96A53086L,0UL}},{{18446744073709551615UL,0xC245E320L,7UL,0xE936BE85L},{18446744073709551615UL,18446744073709551610UL,0x300CE3D9L,18446744073709551612UL},{0xCDF9C775L,0x1466AC3AL,0x132E8824L,18446744073709551607UL},{0x7DD14560L,7UL,18446744073709551612UL,0x300CE3D9L},{0xF11BBB4FL,9UL,0xE26B5CFDL,0x1B27067DL},{0x32DA3BB1L,0xAC3D6C1DL,0xF11BBB4FL,6UL}},{{0x1B78CD90L,18446744073709551613UL,18446744073709551615UL,0x0DEEA39BL},{1UL,18446744073709551615UL,18446744073709551610UL,0x72E7EB66L},{0x997EC53AL,0x300CE3D9L,0x1B09C821L,1UL},{18446744073709551615UL,0x93D7F285L,0x2FE2CCF5L,6UL},{18446744073709551607UL,18446744073709551615UL,0xBB645487L,0xBB645487L},{1UL,1UL,18446744073709551607UL,0x7B17B4D2L}},{{0xBFB66FEBL,0xE936BE85L,0x6C04840CL,0xF1939EFDL},{18446744073709551615UL,0x15BFEB1EL,18446744073709551612UL,0x6C04840CL},{6UL,0x15BFEB1EL,0xB5FB49A5L,0xF1939EFDL},{0x15BFEB1EL,0xE936BE85L,8UL,0x7B17B4D2L},{1UL,1UL,0x3A50EB6BL,0xBB645487L},{0x3B06D9E9L,18446744073709551615UL,0xCDF9C775L,6UL}}};
    int8_t *l_2216 = &g_1375;
    int32_t l_2251 = 0x5D24B411L;
    int32_t l_2252[3][3][10] = {{{0x87EAAD8AL,0xEE870590L,3L,0xEE870590L,0x87EAAD8AL,0x38541C8EL,3L,0x38541C8EL,0x87EAAD8AL,0xEE870590L},{(-1L),0xEE870590L,(-1L),9L,(-2L),0xEE870590L,(-2L),9L,(-1L),0xEE870590L},{0x87EAAD8AL,9L,0x45E9BC94L,0xCEAD84D8L,0x87EAAD8AL,0xCEAD84D8L,0x45E9BC94L,9L,0x87EAAD8AL,9L}},{{(-2L),0xCEAD84D8L,(-1L),0x38541C8EL,(-1L),0xCEAD84D8L,(-2L),0x38541C8EL,(-2L),0xCEAD84D8L},{0x87EAAD8AL,0x38541C8EL,3L,0x38541C8EL,0x87EAAD8AL,0xEE870590L,3L,0xEE870590L,0x87EAAD8AL,0x38541C8EL},{(-1L),0x38541C8EL,(-1L),0xCEAD84D8L,(-2L),0x38541C8EL,(-2L),0xCEAD84D8L,(-1L),0x38541C8EL}},{{0x87EAAD8AL,0xCEAD84D8L,0x45E9BC94L,9L,0x87EAAD8AL,9L,0x45E9BC94L,0xCEAD84D8L,0x87EAAD8AL,0xCEAD84D8L},{(-2L),9L,(-1L),0xEE870590L,(-1L),9L,(-2L),0xEE870590L,(-2L),9L},{0x87EAAD8AL,0xEE870590L,3L,0xEE870590L,0x87EAAD8AL,0x38541C8EL,3L,0x38541C8EL,0x87EAAD8AL,0xEE870590L}}};
    union U1 *****l_2282 = (void*)0;
    int8_t l_2305 = 0x05L;
    const float *l_2345[4][7][1] = {{{&g_1442.f3},{(void*)0},{(void*)0},{&g_1442.f3},{&g_1352[6][7][0].f3},{&g_226},{&g_1442.f3}},{{&g_226},{&g_1352[6][7][0].f3},{&g_1442.f3},{(void*)0},{(void*)0},{&g_1442.f3},{&g_1352[6][7][0].f3}},{{&g_226},{&g_1442.f3},{&g_977.f3},{&g_1442.f3},{&g_27},{&g_196.f3},{&g_196.f3}},{{&g_27},{&g_1442.f3},{&g_977.f3},{&g_27},{&g_977.f3},{&g_1442.f3},{&g_27}}};
    const float **l_2344 = &l_2345[3][4][0];
    union U0 *l_2376 = (void*)0;
    uint32_t ***l_2403 = (void*)0;
    const int8_t ***l_2441 = &g_1187;
    const uint32_t * const **l_2445[8][5] = {{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444},{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444},{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444},{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444},{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444},{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444},{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444},{&g_2444,&g_2444,&g_2444,&g_2444,&g_2444}};
    int64_t l_2450 = 0L;
    uint64_t l_2456 = 0xD94E078AECB3775BLL;
    uint64_t l_2467 = 0x658120332D9FAA34LL;
    int64_t l_2513 = 0x820B6D71226635AELL;
    int16_t l_2523 = (-1L);
    union U0 *l_2552 = &g_2553;
    uint16_t l_2554 = 0xB4A0L;
    int i, j, k;
    return l_2467;
}


/* ------------------------------------------ */
/* 
 * reads : g_566 g_1189 g_84 g_308.f0 g_847 g_848 g_12 g_1362 g_1236 g_669 g_172 g_26 g_1228 g_1011 g_1316 g_1442 g_27 g_226 g_140 g_1315 g_1248 g_5 g_565 g_1227 g_1100.f4 g_1495 g_136 g_108 g_101 g_69 g_89 g_1069 g_1070 g_14 g_49 g_1637 g_239 g_1638 g_205 g_736 g_977.f2 g_1352.f4 g_977.f4 g_297 g_302 g_298 g_1634 g_1635 g_1146 g_573.f2 g_561.f4 g_1662 g_941.f4 g_1558.f0 g_1375 g_561.f2 g_1299.f0 g_849.f0 g_1558.f2 g_95.f3
 * writes: g_12 g_1189 g_84 g_112 g_1100.f2 g_14 g_1350 g_140 g_136 g_1248 g_736 g_173 g_27 g_98 g_669 g_297 g_226 g_49 g_253 g_573.f0 g_101 g_108 g_505 g_977.f2 g_1634 g_205 g_1662 g_43 g_1558.f0 g_1558.f2 g_95.f3
 */
static int32_t * func_15(int32_t  p_16, float * p_17, float * p_18, uint32_t  p_19, const uint16_t  p_20)
{ /* block id: 651 */
    int32_t *l_1323 = &g_84;
    union U1 **l_1327[9];
    int32_t l_1373 = 0x31DFE84CL;
    int32_t l_1381 = (-8L);
    int32_t l_1383[7][6] = {{4L,0xEEBBBC58L,4L,0L,0L,4L},{0xABFFBFCDL,0xABFFBFCDL,0L,0xCD5F0805L,0L,0xABFFBFCDL},{0L,0xEEBBBC58L,0xCD5F0805L,0xCD5F0805L,0xEEBBBC58L,0L},{0xABFFBFCDL,0L,0xCD5F0805L,0L,0xABFFBFCDL,0xABFFBFCDL},{4L,0L,0L,4L,0xEEBBBC58L,4L},{4L,0xEEBBBC58L,4L,0L,0L,4L},{0xABFFBFCDL,0xABFFBFCDL,0L,0xCD5F0805L,0L,0xABFFBFCDL}};
    int64_t l_1401 = 0x2166B214F9DF44AELL;
    union U0 *l_1421[1][1];
    int8_t * const l_1423 = &g_1375;
    int8_t * const *l_1422[4][3];
    uint32_t l_1467[1][6][5] = {{{4294967286UL,0x3913ED5AL,4294967286UL,0x3913ED5AL,4294967286UL},{0x9A888BADL,0x931296C4L,0x931296C4L,0x9A888BADL,0x9A888BADL},{5UL,0x3913ED5AL,5UL,0x3913ED5AL,5UL},{0x9A888BADL,0x9A888BADL,0x931296C4L,0x931296C4L,0x9A888BADL},{4294967286UL,0x3913ED5AL,4294967286UL,0x3913ED5AL,4294967286UL},{0x9A888BADL,0x931296C4L,0x931296C4L,0x9A888BADL,0x9A888BADL}}};
    uint32_t ** const * const l_1480 = &g_1069;
    uint32_t ** const * const *l_1479[10][10] = {{&l_1480,&l_1480,(void*)0,&l_1480,(void*)0,&l_1480,&l_1480,&l_1480,(void*)0,&l_1480},{&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,(void*)0,&l_1480},{&l_1480,&l_1480,&l_1480,(void*)0,&l_1480,&l_1480,(void*)0,&l_1480,&l_1480,&l_1480},{&l_1480,(void*)0,&l_1480,&l_1480,(void*)0,&l_1480,(void*)0,&l_1480,&l_1480,&l_1480},{&l_1480,&l_1480,(void*)0,(void*)0,(void*)0,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480},{(void*)0,(void*)0,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480},{&l_1480,(void*)0,(void*)0,&l_1480,&l_1480,(void*)0,&l_1480,&l_1480,&l_1480,&l_1480},{&l_1480,&l_1480,&l_1480,(void*)0,&l_1480,&l_1480,&l_1480,(void*)0,&l_1480,&l_1480},{&l_1480,&l_1480,&l_1480,&l_1480,(void*)0,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480},{&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480,&l_1480}};
    int8_t *l_1504 = &g_1362[1];
    int8_t *l_1505 = &g_1362[1];
    uint8_t *l_1549 = &g_736;
    uint64_t l_1562 = 0xC49125A03E9BC56ALL;
    uint16_t l_1607 = 0xBBA4L;
    union U1 *****l_1648 = &g_1591[0][8][0];
    int16_t l_1684 = (-1L);
    float l_1714 = 0x0.3p-1;
    union U0 *l_1889 = &g_1890;
    union U0 ***l_1903 = (void*)0;
    uint64_t l_1931[9][9][1];
    int8_t ** const *l_1938 = &g_1146[6];
    int8_t ** const **l_1937[3];
    int8_t ** const ***l_1936 = &l_1937[0];
    uint32_t **l_1961 = &g_162;
    uint16_t l_1964[4] = {65534UL,65534UL,65534UL,65534UL};
    uint16_t l_2007 = 0x7A99L;
    volatile union U0 **l_2019 = (void*)0;
    int8_t ****l_2038[6][1];
    int8_t l_2060[5][6] = {{(-1L),7L,(-1L),1L,3L,3L},{1L,3L,3L,1L,(-1L),7L},{(-1L),(-1L),0x11L,7L,0L,0L},{0L,(-1L),0x7BL,(-1L),0L,0x55L},{0L,(-1L),(-1L),(-2L),(-1L),0x11L}};
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_1327[i] = &g_98;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
            l_1421[i][j] = &g_302;
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
            l_1422[i][j] = &l_1423;
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
                l_1931[i][j][k] = 0xBB4B691B1FA2B947LL;
        }
    }
    for (i = 0; i < 3; i++)
        l_1937[i] = &l_1938;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
            l_2038[i][j] = (void*)0;
    }
    if (((*g_566) = 0L))
    { /* block id: 653 */
        return l_1323;
    }
    else
    { /* block id: 655 */
        union U0 ** const l_1326 = &g_297;
        int32_t l_1330[9][3] = {{0x54061CBCL,0x54061CBCL,0x54061CBCL},{0x70518E9CL,0x70518E9CL,0x70518E9CL},{0x54061CBCL,0x54061CBCL,0x54061CBCL},{0x70518E9CL,0x70518E9CL,0x70518E9CL},{0x54061CBCL,0x54061CBCL,0x54061CBCL},{0x70518E9CL,0x70518E9CL,0x70518E9CL},{0x54061CBCL,0x54061CBCL,0x54061CBCL},{0x70518E9CL,0x70518E9CL,0x70518E9CL},{0x54061CBCL,0x54061CBCL,0x54061CBCL}};
        int32_t *l_1336[7][2] = {{&g_461[4],&g_5},{(void*)0,(void*)0},{&g_5,&g_461[4]},{(void*)0,&g_461[4]},{&g_5,(void*)0},{(void*)0,&g_5},{&g_461[4],(void*)0}};
        int32_t ** const l_1335 = &l_1336[5][1];
        int32_t ** const *l_1334 = &l_1335;
        union U1 * const l_1354 = (void*)0;
        union U1 * const *l_1353[5];
        uint16_t l_1359[2][7];
        int32_t l_1385[4][7][5] = {{{8L,(-1L),0xCDF6292BL,0x296DCD3DL,0x9ECF339FL},{0x2D6FDB59L,0x9120AF41L,0xF960976AL,(-5L),0xF960976AL},{(-1L),(-1L),0xFB27D9B7L,0L,(-1L)},{7L,(-1L),0xCDF6292BL,7L,(-5L)},{0x00BA747FL,0L,0x5E163A08L,0x792B9093L,7L},{0L,(-1L),0x4BA46E90L,0x2D6FDB59L,0L},{0L,(-1L),8L,(-1L),0xF4FC547DL}},{{0xF4FC547DL,0x9120AF41L,0x5A2FB23AL,2L,(-1L)},{0L,(-1L),(-1L),0x00BA747FL,0x792B9093L},{0x5A2FB23AL,0xB39689EAL,(-8L),0x9120AF41L,(-1L)},{2L,0x5A2FB23AL,0x9120AF41L,0xF4FC547DL,0L},{(-1L),0x792B9093L,0x7C0C47EFL,0xF4FC547DL,0L},{0x8D28220FL,0x885E4391L,0L,0x9120AF41L,2L},{0x792B9093L,0x5E163A08L,0L,0x00BA747FL,(-1L)}},{{0x3529F72BL,0x0F1801E8L,0xAC51D762L,2L,(-1L)},{1L,0x1FC31EABL,(-1L),(-1L),0x1FC31EABL},{(-5L),0xF960976AL,0xD2939D83L,0xCDF6292BL,0x262A0E2FL},{7L,0x296DCD3DL,0L,(-4L),0x9120AF41L},{0x4BA46E90L,0xF960976AL,(-6L),0xCA3B5830L,(-1L)},{7L,0xC64059FAL,9L,(-1L),0xD2939D83L},{0x96D8380FL,(-4L),0xF3FAD301L,0x96D8380FL,8L}},{{0x7C0C47EFL,0L,0xC64059FAL,(-6L),(-1L)},{0x32C54F39L,(-6L),0xFB27D9B7L,(-9L),0xF6992090L},{(-4L),1L,0xB39689EAL,7L,(-1L)},{0x6E24413CL,8L,0x9096268AL,(-9L),0L},{0x119BB809L,(-1L),0x9096268AL,(-1L),0xB96D546FL},{0x5A2FB23AL,(-9L),0xB39689EAL,0x32C54F39L,0x96D8380FL},{9L,7L,0xFB27D9B7L,9L,0x5A2FB23AL}}};
        int64_t l_1400[4];
        int8_t ***l_1409[8] = {&g_1146[3],(void*)0,(void*)0,&g_1146[3],(void*)0,(void*)0,&g_1146[3],(void*)0};
        int8_t *** const *l_1408 = &l_1409[1];
        uint32_t ** const *l_1482 = &g_1069;
        uint32_t ** const **l_1481 = &l_1482;
        int64_t *l_1492 = &g_253[2];
        int64_t *l_1493 = (void*)0;
        int64_t *l_1494 = &g_573[2].f0;
        union U1 ***l_1518 = &l_1327[5];
        int32_t *l_1610 = (void*)0;
        const int8_t *l_1761 = &g_1375;
        const int8_t * const *l_1760 = &l_1761;
        const int8_t * const **l_1759[8][9] = {{(void*)0,&l_1760,&l_1760,&l_1760,(void*)0,&l_1760,&l_1760,(void*)0,(void*)0},{&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760},{&l_1760,(void*)0,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,(void*)0,&l_1760},{&l_1760,(void*)0,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760},{&l_1760,&l_1760,&l_1760,&l_1760,(void*)0,(void*)0,&l_1760,(void*)0,(void*)0},{&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760},{&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,(void*)0},{&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,&l_1760,(void*)0}};
        int8_t l_1801 = 1L;
        int32_t l_1832[9][4][3] = {{{0x135D0AACL,1L,0x5F342E26L},{(-4L),1L,0xA0593930L},{0x8EC78A35L,1L,(-1L)},{1L,(-4L),0xA0593930L}},{{0x29863B9EL,0x2B85465AL,0x5F342E26L},{1L,0x29863B9EL,1L},{0L,0x8EC78A35L,0x8EC78A35L},{0L,1L,0x29863B9EL}},{{1L,0x5F342E26L,0x2B85465AL},{0x29863B9EL,0xA0593930L,(-4L)},{1L,(-1L),1L},{0x8EC78A35L,0xA0593930L,1L}},{{(-4L),0x5F342E26L,1L},{0x135D0AACL,1L,8L},{8L,0x8EC78A35L,8L},{(-5L),0x29863B9EL,1L}},{{8L,0x2B85465AL,1L},{9L,(-4L),1L},{0x5F342E26L,1L,8L},{0x67856FC8L,(-4L),0x5F342E26L}},{{1L,(-5L),0x8EC78A35L},{0x2B85465AL,0x6CF9285BL,0x0D41304DL},{0x6CF9285BL,0x6CF9285BL,(-1L)},{8L,(-5L),1L}},{{8L,(-4L),0x135D0AACL},{0x0D41304DL,1L,9L},{(-5L),8L,0x135D0AACL},{0x8EC78A35L,0x5F342E26L,1L}},{{1L,0x8EC78A35L,(-1L)},{0x65B205B6L,0x0D41304DL,0x0D41304DL},{0x65B205B6L,(-1L),0x8EC78A35L},{1L,1L,0x5F342E26L}},{{0x8EC78A35L,0x135D0AACL,8L},{(-5L),9L,1L},{0x0D41304DL,0x135D0AACL,(-4L)},{8L,1L,(-5L)}}};
        int8_t l_1925 = 8L;
        uint32_t l_1963 = 1UL;
        uint32_t l_2034 = 0UL;
        int8_t ****l_2040 = (void*)0;
        uint32_t ****l_2183 = &g_536;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_1353[i] = &l_1354;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 7; j++)
                l_1359[i][j] = 0xFFDCL;
        }
        for (i = 0; i < 4; i++)
            l_1400[i] = (-4L);
        for (g_1189 = 0; (g_1189 <= 0); g_1189 += 1)
        { /* block id: 658 */
            union U1 **l_1328[3][4];
            int32_t l_1329 = 0x7694D1E2L;
            uint16_t l_1333 = 65531UL;
            union U1 ****l_1337 = (void*)0;
            union U1 ***l_1339 = &l_1327[5];
            union U1 ****l_1338 = &l_1339;
            uint32_t **l_1342 = &g_162;
            uint32_t *l_1343 = &g_14;
            union U1 * const *l_1349 = (void*)0;
            union U1 * const **l_1348[10] = {&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349,&l_1349};
            int8_t *l_1360 = &g_140;
            uint16_t *l_1361[10][4][6] = {{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}},{{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136},{&g_136,&g_136,&g_136,&g_136,&g_136,&g_136}}};
            int32_t *l_1363 = &l_1330[6][1];
            int32_t l_1379 = 1L;
            int32_t l_1384 = 9L;
            int32_t l_1386 = 0xA06B0A0FL;
            int32_t l_1388 = 0xA69E8225L;
            int32_t l_1389 = 0xBDED6595L;
            int32_t l_1390[4];
            int32_t l_1471 = 0x68665FC0L;
            int i, j, k;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 4; j++)
                    l_1328[i][j] = (void*)0;
            }
            for (i = 0; i < 4; i++)
                l_1390[i] = 0x1FA2278DL;
            if ((safe_add_func_int64_t_s_s((p_19 > ((l_1326 == (((*l_1323) = ((&l_1326 == &g_239) <= (l_1327[5] != ((((((0xDC55L > (((((*l_1323) ^ g_308[0].f0) , (p_19 & p_19)) , (void*)0) != (*g_847))) <= p_20) || (*l_1323)) == 0xB07F25199831A33DLL) , p_16) , l_1328[1][0])))) , l_1326)) > l_1329)), l_1330[4][2])))
            { /* block id: 660 */
                (*g_566) = 0x0F15D756L;
            }
            else
            { /* block id: 662 */
                for (g_112 = 0; g_112 < 9; g_112 += 1)
                {
                    for (g_1100.f2 = 0; g_1100.f2 < 3; g_1100.f2 += 1)
                    {
                        l_1330[g_112][g_1100.f2] = 0xBFC24CCDL;
                    }
                }
            }
            (*l_1363) |= ((*g_566) = ((((safe_rshift_func_int16_t_s_u((((l_1333 , l_1334) != &l_1335) < (((*l_1338) = &l_1327[5]) != ((safe_sub_func_uint16_t_u_u((g_1248[6][0][0] = (g_136 = ((*l_1323) = (((*l_1343) = ((void*)0 == l_1342)) && (safe_sub_func_uint32_t_u_u((safe_div_func_int64_t_s_s((l_1327[2] != (l_1353[1] = (g_1350 = &g_98))), (safe_mod_func_uint32_t_u_u((((*l_1360) = (safe_sub_func_int8_t_s_s((-1L), l_1359[0][0]))) , l_1333), p_16)))), p_16)))))), g_12[2][0][0])) , (void*)0))), l_1329)) > 3UL) | g_1362[0]) , 8L));
            for (g_736 = 0; (g_736 <= 3); g_736 += 1)
            { /* block id: 677 */
                uint16_t l_1372 = 6UL;
                int32_t l_1382[4][10] = {{0x73B95C39L,1L,0x82CAB5CAL,1L,0x82CAB5CAL,1L,0x73B95C39L,0x73B95C39L,1L,0x82CAB5CAL},{1L,0x73B95C39L,0x73B95C39L,1L,0x82CAB5CAL,1L,0x82CAB5CAL,1L,0x73B95C39L,0x73B95C39L},{0x82CAB5CAL,0x73B95C39L,(-2L),1L,1L,(-2L),0x73B95C39L,0x82CAB5CAL,0x73B95C39L,(-2L)},{1L,1L,1L,1L,1L,(-2L),(-2L),1L,1L,1L}};
                int16_t l_1391 = (-9L);
                int64_t l_1396 = 0xA55A3549A8D90AD4LL;
                union U1 * const l_1410[4][2][8] = {{{&g_573[2],&g_1352[8][4][0],&g_573[2],&g_941[0],&g_941[0],&g_573[2],&g_1352[8][4][0],&g_573[2]},{&g_573[0],&g_941[0],(void*)0,&g_941[0],&g_573[0],&g_573[0],&g_941[0],(void*)0}},{{&g_573[0],&g_573[0],&g_941[0],(void*)0,&g_941[0],&g_573[0],&g_573[0],&g_941[0]},{&g_573[2],&g_941[0],&g_941[0],&g_573[2],&g_1352[8][4][0],&g_573[2],&g_941[0],&g_941[0]}},{{&g_941[0],&g_1352[8][4][0],(void*)0,(void*)0,&g_573[0],&g_573[2],&g_573[0],&g_1352[8][4][0]},{(void*)0,&g_573[0],(void*)0,&g_573[2],&g_573[2],(void*)0,&g_573[0],(void*)0}},{{&g_941[0],&g_573[2],&g_1352[8][4][0],&g_573[2],&g_941[0],&g_941[0],&g_573[2],&g_1352[8][4][0]},{&g_941[0],&g_941[0],&g_573[2],&g_1352[8][4][0],&g_573[2],&g_941[0],&g_941[0],&g_573[2]}}};
                union U1 *l_1413 = (void*)0;
                int8_t * const **l_1424 = &l_1422[0][2];
                union U0 *l_1448[5][2];
                int i, j, k;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_1448[i][j] = &g_302;
                }
                if ((!((safe_add_func_int32_t_s_s(p_16, ((&g_1068 == &g_1068) <= (l_1373 = ((*g_566) = (l_1372 = (safe_mul_func_int8_t_s_s((((~p_16) | 0x11B1EEDBL) || ((safe_mul_func_int16_t_s_s(l_1330[7][2], ((*l_1323) = ((void*)0 == (*g_1236))))) && (*l_1363))), p_16)))))))) >= p_19)))
                { /* block id: 682 */
                    int64_t l_1374 = 7L;
                    int32_t l_1387 = 0xFF3C92FEL;
                    int32_t l_1393 = 0x62FD5F7AL;
                    int32_t l_1394 = 0xFD749D06L;
                    int32_t l_1395 = 0x08333BE6L;
                    int32_t l_1397 = 0x4116B498L;
                    int32_t l_1398[10] = {0x47AD1A00L,0xAD0D5D2FL,0xAD0D5D2FL,0x47AD1A00L,0L,0x47AD1A00L,0xAD0D5D2FL,0xAD0D5D2FL,0x47AD1A00L,0L};
                    union U1 **l_1412 = &g_98;
                    int i;
                    (*g_172) = &l_1329;
                    for (g_14 = 0; (g_14 <= 0); g_14 += 1)
                    { /* block id: 686 */
                        int32_t *l_1376 = &g_84;
                        int32_t *l_1377 = &g_12[3][1][0];
                        int32_t *l_1378 = &g_84;
                        int32_t *l_1380[8] = {&l_1330[0][0],&g_12[2][0][0],&g_12[2][0][0],&l_1330[0][0],&g_12[2][0][0],&g_12[2][0][0],&l_1330[0][0],&g_12[2][0][0]};
                        int32_t l_1392 = 0xE0CB21FBL;
                        uint8_t l_1402[1][5];
                        int8_t *** const l_1407[10][5][5] = {{{&g_1146[2],&g_1146[5],&g_1146[5],(void*)0,&g_1146[7]},{&g_1146[5],&g_1146[5],&g_1146[2],&g_1146[5],(void*)0},{&g_1146[5],&g_1146[2],(void*)0,(void*)0,&g_1146[5]},{&g_1146[8],&g_1146[7],&g_1146[4],&g_1146[6],(void*)0},{&g_1146[8],&g_1146[2],&g_1146[2],&g_1146[5],&g_1146[5]}},{{&g_1146[5],&g_1146[0],&g_1146[5],&g_1146[5],&g_1146[0]},{&g_1146[5],(void*)0,&g_1146[5],&g_1146[5],&g_1146[5]},{&g_1146[2],&g_1146[0],&g_1146[5],&g_1146[5],(void*)0},{(void*)0,(void*)0,&g_1146[5],&g_1146[2],&g_1146[5]},{&g_1146[3],&g_1146[0],&g_1146[5],&g_1146[5],(void*)0}},{{&g_1146[5],&g_1146[5],&g_1146[8],(void*)0,&g_1146[0]},{&g_1146[3],&g_1146[5],&g_1146[5],&g_1146[3],(void*)0},{&g_1146[0],(void*)0,&g_1146[5],(void*)0,(void*)0},{&g_1146[5],(void*)0,(void*)0,&g_1146[5],&g_1146[5]},{&g_1146[5],&g_1146[5],&g_1146[5],(void*)0,&g_1146[8]}},{{&g_1146[5],&g_1146[5],&g_1146[3],&g_1146[5],&g_1146[8]},{&g_1146[3],&g_1146[6],&g_1146[3],(void*)0,&g_1146[5]},{&g_1146[5],&g_1146[5],&g_1146[3],&g_1146[5],&g_1146[5]},{&g_1146[5],&g_1146[5],(void*)0,(void*)0,(void*)0},{&g_1146[4],&g_1146[5],&g_1146[5],&g_1146[3],&g_1146[7]}},{{&g_1146[3],&g_1146[0],(void*)0,(void*)0,&g_1146[8]},{&g_1146[2],&g_1146[6],&g_1146[5],&g_1146[5],(void*)0},{&g_1146[5],&g_1146[5],&g_1146[5],&g_1146[2],(void*)0},{&g_1146[5],&g_1146[2],(void*)0,&g_1146[5],&g_1146[8]},{&g_1146[5],&g_1146[5],(void*)0,&g_1146[5],&g_1146[2]}},{{&g_1146[5],&g_1146[5],&g_1146[5],&g_1146[5],&g_1146[5]},{&g_1146[5],&g_1146[4],&g_1146[5],&g_1146[5],&g_1146[5]},{&g_1146[5],(void*)0,&g_1146[5],&g_1146[6],&g_1146[0]},{&g_1146[5],(void*)0,&g_1146[5],(void*)0,&g_1146[5]},{&g_1146[3],&g_1146[5],(void*)0,&g_1146[5],(void*)0}},{{&g_1146[5],(void*)0,&g_1146[5],&g_1146[5],&g_1146[5]},{(void*)0,(void*)0,&g_1146[5],(void*)0,&g_1146[5]},{&g_1146[5],(void*)0,&g_1146[5],&g_1146[5],(void*)0},{&g_1146[5],&g_1146[8],&g_1146[2],&g_1146[2],(void*)0},{&g_1146[5],(void*)0,&g_1146[5],(void*)0,(void*)0}},{{&g_1146[2],(void*)0,&g_1146[3],(void*)0,&g_1146[3]},{&g_1146[3],(void*)0,&g_1146[4],&g_1146[5],(void*)0},{&g_1146[5],&g_1146[5],&g_1146[5],&g_1146[5],&g_1146[5]},{&g_1146[5],&g_1146[5],&g_1146[2],&g_1146[8],(void*)0},{&g_1146[5],(void*)0,(void*)0,&g_1146[5],&g_1146[5]}},{{&g_1146[0],&g_1146[0],&g_1146[8],&g_1146[5],(void*)0},{&g_1146[5],&g_1146[5],&g_1146[5],&g_1146[0],&g_1146[5]},{(void*)0,&g_1146[3],&g_1146[2],&g_1146[5],(void*)0},{&g_1146[5],&g_1146[5],&g_1146[0],&g_1146[5],&g_1146[3]},{&g_1146[5],&g_1146[8],&g_1146[6],(void*)0,(void*)0}},{{&g_1146[5],&g_1146[5],&g_1146[5],&g_1146[3],(void*)0},{&g_1146[5],&g_1146[5],&g_1146[3],&g_1146[0],(void*)0},{(void*)0,&g_1146[3],&g_1146[5],&g_1146[6],&g_1146[5]},{&g_1146[5],&g_1146[4],&g_1146[5],&g_1146[5],&g_1146[5]},{&g_1146[3],&g_1146[7],&g_1146[5],&g_1146[8],(void*)0}}};
                        int8_t *** const *l_1406[9][1][9] = {{{&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[5][4][0],&l_1407[6][4][3],&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[6][4][3],&l_1407[5][4][0],&l_1407[3][1][4]}},{{&l_1407[3][1][4],(void*)0,&l_1407[3][1][4],(void*)0,(void*)0,&l_1407[3][1][4],&l_1407[6][4][3],&l_1407[3][1][4],&l_1407[6][3][3]}},{{&l_1407[2][2][4],&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[5][4][3],&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[2][2][4]}},{{(void*)0,(void*)0,(void*)0,&l_1407[5][4][3],&l_1407[6][4][3],&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[3][1][4]}},{{(void*)0,&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[5][4][0],&l_1407[5][4][0],&l_1407[3][1][4],&l_1407[3][1][4],(void*)0,(void*)0}},{{(void*)0,&l_1407[6][4][3],&l_1407[3][1][4],&l_1407[8][4][1],&l_1407[5][4][0],&l_1407[3][1][4],&l_1407[6][3][3],&l_1407[5][4][3],&l_1407[5][4][3]}},{{&l_1407[2][2][4],(void*)0,&l_1407[6][4][3],(void*)0,&l_1407[6][4][3],(void*)0,&l_1407[2][2][4],&l_1407[3][1][4],(void*)0}},{{&l_1407[3][1][4],&l_1407[3][1][4],&l_1407[2][2][4],(void*)0,&l_1407[5][4][3],&l_1407[5][4][0],&l_1407[3][1][4],&l_1407[6][4][3],&l_1407[3][1][4]}},{{&l_1407[3][1][4],(void*)0,&l_1407[8][4][1],&l_1407[8][4][1],(void*)0,&l_1407[3][1][4],(void*)0,&l_1407[3][1][4],&l_1407[2][2][4]}}};
                        int8_t *** const **l_1405[6] = {&l_1406[6][0][4],&l_1406[6][0][4],&l_1406[6][0][4],&l_1406[6][0][4],&l_1406[6][0][4],(void*)0};
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 5; j++)
                                l_1402[i][j] = 0xD7L;
                        }
                        ++l_1402[0][2];
                        l_1408 = (void*)0;
                        (*g_26) = 0x2.6761E7p+88;
                    }
                    (*l_1412) = l_1410[0][1][4];
                    for (l_1391 = 0; (l_1391 >= 0); l_1391 -= 1)
                    { /* block id: 694 */
                        int32_t *l_1414 = &l_1383[6][1];
                        int32_t *l_1415 = &g_12[1][0][0];
                        int32_t *l_1416 = &l_1398[2];
                        int32_t *l_1417[10] = {&l_1394,&l_1394,&l_1394,&l_1394,&l_1394,&l_1394,&l_1394,&l_1394,&l_1394,&l_1394};
                        uint8_t l_1418 = 0xFEL;
                        int i, j, k;
                        l_1413 = ((**l_1339) = l_1413);
                        (*g_566) &= g_1228[g_1189][l_1391][g_1189];
                        (*g_1316) = (*g_1011);
                        ++l_1418;
                    }
                }
                else
                { /* block id: 701 */
                    for (l_1389 = 0; (l_1389 >= 0); l_1389 -= 1)
                    { /* block id: 704 */
                        return p_18;
                    }
                    for (l_1333 = 0; (l_1333 <= 0); l_1333 += 1)
                    { /* block id: 709 */
                        (*g_172) = p_17;
                        if ((*l_1323))
                            break;
                    }
                    (*l_1326) = l_1421[0][0];
                }
                (*l_1424) = l_1422[2][2];
                for (l_1391 = 3; (l_1391 >= 0); l_1391 -= 1)
                { /* block id: 718 */
                    uint64_t l_1443 = 0UL;
                    int32_t l_1470[10][3][4] = {{{0x88C72D6EL,(-10L),0xC289368BL,0x6B190EF8L},{0xE93E2AD3L,0xC289368BL,4L,4L},{(-8L),(-8L),9L,0L}},{{0xCC23E711L,0x42E38413L,(-5L),0xB633550FL},{9L,0x27B5D9F0L,0x72550788L,(-5L)},{0xA308B2DDL,0x27B5D9F0L,0x581E1EC3L,0xB633550FL}},{{0x27B5D9F0L,0x42E38413L,0x6B190EF8L,0L},{0x522CDB70L,(-8L),0xCC23E711L,4L},{0x581E1EC3L,0xC289368BL,(-1L),0x6B190EF8L}},{{0x42E38413L,(-10L),0x42E38413L,(-8L)},{0x6B190EF8L,0x98FA6A13L,0xE93E2AD3L,0x7D969231L},{0x72550788L,0xA308B2DDL,1L,0x98FA6A13L}},{{7L,0xE93E2AD3L,1L,0xCC23E711L},{0x72550788L,0L,0xE93E2AD3L,1L},{0x6B190EF8L,(-1L),0x42E38413L,0x72550788L}},{{0x42E38413L,0x72550788L,(-1L),(-5L)},{0x581E1EC3L,0xCC23E711L,0xCC23E711L,0x581E1EC3L},{0x522CDB70L,0xB633550FL,0x6B190EF8L,0L}},{{0x27B5D9F0L,(-1L),0x581E1EC3L,(-1L)},{0xA308B2DDL,(-6L),0x72550788L,(-1L)},{9L,(-1L),(-5L),0L}},{{0xCC23E711L,0xB633550FL,9L,0x581E1EC3L},{(-8L),0xCC23E711L,4L,(-5L)},{0xE93E2AD3L,0x72550788L,0xC289368BL,0x72550788L}},{{0x88C72D6EL,(-1L),(-6L),1L},{0x7D969231L,0L,1L,0xCC23E711L},{(-5L),0xE93E2AD3L,(-1L),0x98FA6A13L}},{{(-5L),0xA308B2DDL,1L,0x7D969231L},{0x7D969231L,0x98FA6A13L,(-6L),(-8L)},{0x88C72D6EL,(-10L),0xC289368BL,0x6B190EF8L}}};
                    int i, j, k;
                    (*g_566) &= ((7UL ^ ((*l_1363) || ((0UL || ((((*p_18) = (((-0x1.2p-1) <= ((safe_sub_func_float_f_f((+((void*)0 == &g_239)), ((*g_26) = (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f((((safe_mul_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(((*l_1323) != ((safe_mod_func_uint64_t_u_u(18446744073709551607UL, (safe_mod_func_uint64_t_u_u((((((g_1442 , (void*)0) != (*g_847)) & (*l_1323)) , l_1382[0][8]) <= (*l_1323)), l_1443)))) > (*l_1363))), p_19)), p_16)) , (*l_1363)) < (*g_26)), (*g_26))), (*g_26))), l_1443))))) != l_1385[1][1][4])) <= (*p_18))) > l_1443) , 0x8AFD970661A3605DLL)) & p_20))) >= l_1330[4][2]);
                    if ((~((~((*l_1360) ^= ((*l_1323) & (safe_mod_func_int32_t_s_s((*l_1323), p_20))))) >= g_1362[0])))
                    { /* block id: 723 */
                        union U0 *l_1449 = &g_307;
                        uint64_t *l_1462 = &l_1443;
                        int16_t *l_1468[8] = {&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43,&g_43};
                        int i;
                        (**g_1315) = (*g_1011);
                        (*l_1363) &= ((((p_20 ^ l_1443) && ((((*l_1326) = l_1448[3][0]) == l_1449) != ((((safe_lshift_func_int16_t_s_u((g_49 = (safe_add_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((((safe_sub_func_int64_t_s_s(0xD4791247DB5E40C8LL, (!(!g_84)))) , (safe_rshift_func_int16_t_s_s(((((p_16 , ((*l_1462) = 0xCE20EA82D1E7CB8BLL)) , ((safe_mul_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u((g_1248[4][1][2] != l_1467[0][2][1]), p_16)), g_1228[0][0][0])) == 0x26FD2DF5L)) | p_20) != 0xC4L), 3))) , l_1359[0][2]), l_1385[3][0][3])), p_19))), 6)) < 0xC16CC6A8A6511E7ALL) , g_5) < 0L))) & l_1385[2][2][1]) && 0xCAFD9486EC69A08FLL);
                    }
                    else
                    { /* block id: 729 */
                        int32_t *l_1469[5];
                        uint32_t l_1472 = 0x7E876DFDL;
                        int i;
                        for (i = 0; i < 5; i++)
                            l_1469[i] = &l_1390[3];
                        ++l_1472;
                    }
                    for (l_1373 = 0; (l_1373 <= 0); l_1373 += 1)
                    { /* block id: 734 */
                        return (*g_565);
                    }
                }
                for (l_1381 = 0; (l_1381 >= 0); l_1381 -= 1)
                { /* block id: 740 */
                    return (*g_565);
                }
            }
        }
        (*g_566) = (((***l_1480) = ((safe_sub_func_int64_t_s_s(0xB2DF313367E748B3LL, ((l_1400[2] & (safe_mod_func_int16_t_s_s((g_140 || (*l_1323)), (func_77((l_1479[7][7] == l_1481), (safe_mod_func_int32_t_s_s(((*g_566) ^= (safe_mul_func_uint16_t_u_u(((*g_1227) , (safe_rshift_func_int8_t_s_u((((+((safe_div_func_int8_t_s_s((((((((((*l_1494) = ((*l_1492) = (l_1330[4][2] = g_1100.f4))) && ((*l_1492) = 3L)) != (-1L)) > 0xAB13C3041664352BLL) , g_1495) , p_16) < 0xAA37879E4555CF11LL) , (*l_1323)), p_19)) || p_19)) , (void*)0) != (void*)0), (*l_1323)))), g_136))), (*l_1323)))) , p_19)))) >= p_20))) && g_89)) , (*l_1323));
        if ((safe_mul_func_int8_t_s_s((safe_sub_func_int64_t_s_s((((safe_rshift_func_int16_t_s_s(((safe_rshift_func_int8_t_s_u((((l_1504 == l_1505) > (safe_mod_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(((safe_lshift_func_int16_t_s_s((*l_1323), ((safe_mod_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s((safe_add_func_int32_t_s_s(p_19, p_16)), 2)) || ((0UL ^ (&g_466 != (l_1518 = &l_1327[4]))) < (safe_div_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(p_16, (*l_1323))), 247UL)))), l_1359[1][6])) & p_19))) != p_20), 0x23L)), 0x325DAD94L))) || p_20), 7)) ^ (*l_1323)), 10)) , p_16) == l_1400[2]), (*l_1323))), 0x73L)))
        { /* block id: 753 */
            int32_t l_1529 = 4L;
            union U0 *l_1532 = &g_298;
            uint32_t **l_1585 = &g_1070;
            int32_t *l_1659 = &g_12[3][3][0];
            uint32_t l_1706 = 0x67D54B2CL;
            const int8_t * const **l_1763 = &l_1760;
            int32_t l_1799 = (-4L);
            int32_t l_1800[5][4][9] = {{{0xAE3A7995L,0xDCF9EB7FL,1L,0xDCF9EB7FL,0xAE3A7995L,0L,0x001B9711L,0x2EBF48DCL,0x6A4DE3D1L},{0xC2873FCFL,(-8L),0xD7FDC0B7L,2L,1L,1L,(-6L),0xC2873FCFL,9L},{0xAA2BA4BCL,0x4F462E05L,1L,8L,0x5D7E7EA4L,0L,1L,(-1L),1L},{0x88A140C8L,8L,(-6L),(-6L),8L,0x88A140C8L,1L,(-1L),8L}},{{0x6A4DE3D1L,0x4F462E05L,(-3L),0xDCF9EB7FL,(-1L),0x2EBF48DCL,(-1L),0xDCF9EB7FL,(-3L)},{9L,(-8L),1L,8L,(-3L),8L,1L,(-3L),(-3L)},{0x43149150L,0x4F462E05L,(-10L),0L,(-10L),0x4F462E05L,0x43149150L,0x2EBF48DCL,1L},{2L,0xC2873FCFL,(-8L),0xD7FDC0B7L,2L,1L,1L,(-6L),0xC2873FCFL}},{{0xAE3A7995L,1L,0xAE3A7995L,8L,(-1L),(-1L),1L,0x2EBF48DCL,1L},{(-3L),8L,1L,9L,9L,1L,8L,(-3L),8L},{1L,0L,0x5D7E7EA4L,8L,1L,0x4F462E05L,0xAA2BA4BCL,0x4F462E05L,1L},{0xC2873FCFL,8L,8L,0xD7FDC0B7L,(-3L),2L,0xEA605E19L,0xD7FDC0B7L,8L}},{{1L,1L,(-1L),0L,0x6A4DE3D1L,0L,(-1L),1L,1L},{8L,0xD7FDC0B7L,0xEA605E19L,2L,(-3L),0xD7FDC0B7L,8L,8L,0xC2873FCFL},{1L,0x4F462E05L,0xAA2BA4BCL,0x4F462E05L,1L,8L,0x5D7E7EA4L,0L,1L},{8L,(-3L),8L,1L,9L,9L,1L,8L,(-3L)}},{{1L,0x2EBF48DCL,1L,(-1L),(-1L),8L,0xAE3A7995L,1L,0xAE3A7995L},{0xC2873FCFL,(-6L),1L,1L,2L,0xD7FDC0B7L,(-8L),0xC2873FCFL,2L},{1L,0x2EBF48DCL,0x43149150L,0x4F462E05L,(-10L),0L,(-10L),0x4F462E05L,0x43149150L},{(-3L),(-3L),(-8L),(-6L),0x88A140C8L,2L,(-8L),0xEA605E19L,(-3L)}}};
            union U1 ***l_1907 = &l_1327[5];
            int i, j, k;
            for (g_14 = 0; (g_14 <= 3); g_14 += 1)
            { /* block id: 756 */
                uint32_t l_1525 = 1UL;
                const int32_t *l_1569[10][5] = {{&l_1373,&l_1373,&l_1373,&l_1373,&l_1373},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1373,&l_1373,&l_1373,&l_1373,&l_1373},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1373,&l_1373,&l_1373,&l_1373,&l_1373},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1373,&l_1373,&l_1373,&l_1373,&l_1373},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1373,&l_1373,&l_1373,&l_1373,&l_1373},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                int32_t l_1605 = 0x2750000BL;
                int8_t **** const l_1631 = &l_1409[5];
                int i, j;
                for (g_977.f2 = 0; (g_977.f2 <= 0); g_977.f2 += 1)
                { /* block id: 759 */
                    uint32_t l_1527 = 0x9F25D9ABL;
                    uint64_t *l_1528[6][1][3] = {{{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0}}};
                    uint64_t l_1561[10] = {0x432E437C562FF0F1LL,18446744073709551615UL,0x432E437C562FF0F1LL,18446744073709551615UL,0x432E437C562FF0F1LL,18446744073709551615UL,0x432E437C562FF0F1LL,18446744073709551615UL,0x432E437C562FF0F1LL,18446744073709551615UL};
                    int32_t l_1587 = 0x4036044CL;
                    int8_t ***l_1597 = &g_1146[4];
                    int32_t l_1606 = 0x91A17B0DL;
                    uint32_t *****l_1618 = &g_848[5][0][3];
                    int i, j, k;
                }
                for (g_49 = 0; (g_49 <= 2); g_49 += 1)
                { /* block id: 808 */
                    if (p_16)
                    { /* block id: 809 */
                        return (*g_565);
                    }
                    else
                    { /* block id: 811 */
                        (*g_1637) = l_1631;
                        (*g_1638) = (*g_239);
                        (*g_1316) = (*g_1236);
                        return p_17;
                    }
                }
            }
            for (g_205 = 0; (g_205 > 30); ++g_205)
            { /* block id: 821 */
                uint32_t l_1653 = 0xED380F86L;
                const int64_t *l_1710[5][3] = {{&g_1352[6][7][0].f0,&g_1442.f0,&g_1442.f0},{&g_1352[6][7][0].f0,&g_1442.f0,&g_1442.f0},{&g_1352[6][7][0].f0,&g_1442.f0,&g_1442.f0},{&g_1352[6][7][0].f0,&g_1442.f0,&g_1442.f0},{&g_1352[6][7][0].f0,&g_1442.f0,&g_1442.f0}};
                int8_t * const **l_1712 = &l_1422[2][2];
                int i, j;
                (*g_172) = &l_1383[3][4];
                (*g_566) &= (safe_add_func_int8_t_s_s(((*l_1323) <= ((*l_1323) == ((-1L) < (+(safe_mod_func_uint16_t_u_u((l_1648 != ((*l_1323) , &g_1591[0][6][9])), p_19)))))), ((safe_mul_func_int8_t_s_s((((l_1653 == (!(safe_mul_func_int16_t_s_s((((safe_rshift_func_uint8_t_u_s(255UL, 5)) > l_1653) && 1UL), (-3L))))) != p_19) >= 0x90D7L), 255UL)) >= g_736)));
                if (l_1529)
                { /* block id: 824 */
                    (*p_18) = 0xA.10F58Dp+26;
                    if (p_16)
                        continue;
                    return l_1659;
                }
                else
                { /* block id: 828 */
                    uint32_t l_1677 = 4294967292UL;
                    int32_t l_1682 = 0xD8E4ECABL;
                    uint32_t l_1709[5] = {2UL,2UL,2UL,2UL,2UL};
                    int64_t *l_1711 = &g_253[2];
                    uint32_t *l_1713[5][1];
                    int i, j;
                    for (i = 0; i < 5; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_1713[i][j] = &g_14;
                    }
                    for (g_977.f2 = 0; (g_977.f2 > 40); g_977.f2 = safe_add_func_uint8_t_u_u(g_977.f2, 4))
                    { /* block id: 831 */
                        (*g_1316) = (void*)0;
                    }
                    if ((((g_1662 = p_20) == 0xC8BE2DE6L) , ((safe_rshift_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((func_77(((((-5L) && ((safe_mul_func_uint8_t_u_u(((g_1362[1] , (safe_div_func_int32_t_s_s((9L < g_1352[6][7][0].f4), (safe_mul_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s(0x1D2CL, (l_1677 != (safe_div_func_int32_t_s_s((((4294967290UL || 0x1F1C1A1BL) <= p_20) != p_20), p_20))))), 4)), g_308[0].f0))))) ^ p_19), 0x6BL)) != p_19)) , (void*)0) != &l_1480), g_977.f4) , 0UL), p_19)), p_16)) ^ p_16)))
                    { /* block id: 835 */
                        int16_t *l_1683 = &g_43;
                        (*l_1323) &= (safe_sub_func_uint32_t_u_u((((**l_1326) , ((l_1682 = 0x4CB0L) & ((*l_1683) = ((void*)0 == (**g_1634))))) && (l_1684 |= 0x0FD626FAL)), (((((4294967295UL && (*l_1659)) != (g_12[2][1][0] , (safe_mul_func_int8_t_s_s(0xAFL, l_1677)))) | 18446744073709551609UL) | p_20) ^ g_573[2].f2)));
                        if (l_1653)
                            continue;
                        (*l_1323) = (**g_565);
                    }
                    else
                    { /* block id: 842 */
                        (*l_1659) |= p_19;
                    }
                    l_1383[3][4] ^= (safe_mod_func_uint64_t_u_u(p_20, (safe_mul_func_uint16_t_u_u(((((safe_sub_func_uint8_t_u_u(((p_19 | (safe_mod_func_uint32_t_u_u(((*l_1659) = ((((safe_mod_func_int32_t_s_s(((safe_rshift_func_uint8_t_u_u(g_561.f4, (*l_1659))) == (safe_mul_func_int16_t_s_s(((((safe_sub_func_uint32_t_u_u((+(safe_mod_func_uint16_t_u_u(((0UL > ((*l_1549) = l_1706)) > (safe_rshift_func_int8_t_s_u((((((((g_1662 & l_1709[1]) < (((*l_1659) < p_19) != g_941[1].f4)) ^ p_20) == (*l_1323)) , l_1710[1][0]) != l_1711) | l_1709[1]), g_1558.f0))), p_20))), (*l_1659))) == g_941[1].f4) , l_1712) == l_1712), p_20))), p_20)) < g_1375) & 4L) && g_561.f2)), p_19))) | (*l_1323)), (*l_1323))) , g_1299.f0) , (*l_1323)) <= 2L), g_5))));
                }
            }
            for (g_14 = (-27); (g_14 != 9); g_14 = safe_add_func_uint64_t_u_u(g_14, 9))
            { /* block id: 852 */
                int32_t l_1722 = 8L;
                uint32_t l_1741 = 0xF437A5A9L;
                int16_t l_1755 = 5L;
                uint16_t l_1786 = 65529UL;
                int32_t l_1803[4] = {0x13ADF230L,0x13ADF230L,0x13ADF230L,0x13ADF230L};
                int64_t l_1804 = (-1L);
                uint32_t l_1838[7][8][4] = {{{4294967295UL,0x8504EBBAL,0x43B84399L,0x43B84399L},{4294967293UL,4294967293UL,8UL,0UL},{5UL,0x056C92DEL,0x9ADF0E65L,4294967295UL},{8UL,0x5E797E64L,1UL,0x9ADF0E65L},{0x6B648A8BL,0x5E797E64L,0xE4B5CA3AL,4294967295UL},{0x5E797E64L,0x056C92DEL,0UL,0UL},{4294967290UL,4294967293UL,5UL,0x43B84399L},{0xE4B5CA3AL,0x8504EBBAL,1UL,0UL}},{{0x056C92DEL,4294967295UL,0x056C92DEL,4294967293UL},{0UL,0x4E52984EL,4294967295UL,9UL},{1UL,0x6B648A8BL,4294967287UL,0x4E52984EL},{0x50D9FA7FL,4294967295UL,4294967287UL,5UL},{1UL,0UL,4294967295UL,0xCDF70D52L},{0UL,0x04A86947L,0x056C92DEL,1UL},{0x056C92DEL,1UL,1UL,0x848451CEL},{0xE4B5CA3AL,5UL,5UL,0xE4B5CA3AL}},{{4294967290UL,4294967295UL,0UL,0x9C50E850L},{0x5E797E64L,1UL,0xE4B5CA3AL,0x04A86947L},{0x6B648A8BL,0xC63C1D9CL,1UL,0x04A86947L},{8UL,1UL,0x9ADF0E65L,0x9C50E850L},{5UL,4294967295UL,8UL,0xE4B5CA3AL},{4294967293UL,5UL,0x43B84399L,0x848451CEL},{4294967295UL,1UL,0x8504EBBAL,1UL},{0xAE56AC9CL,0x04A86947L,0xC63C1D9CL,0xCDF70D52L}},{{9UL,0UL,0xCDF70D52L,5UL},{0x848451CEL,4294967295UL,0x04A86947L,0x4E52984EL},{0x848451CEL,0x6B648A8BL,0xCDF70D52L,9UL},{9UL,0x4E52984EL,0xC63C1D9CL,4294967293UL},{0xAE56AC9CL,4294967295UL,0x8504EBBAL,0UL},{4294967295UL,0x8504EBBAL,0x43B84399L,0x43B84399L},{4294967293UL,4294967293UL,8UL,0UL},{5UL,0x056C92DEL,0x9ADF0E65L,4294967295UL}},{{8UL,0x5E797E64L,1UL,0x9ADF0E65L},{0x6B648A8BL,0x5E797E64L,0xE4B5CA3AL,4294967295UL},{0x5E797E64L,0x056C92DEL,0UL,0UL},{4294967290UL,4294967293UL,5UL,0x43B84399L},{0xE4B5CA3AL,0x8504EBBAL,1UL,0UL},{0x056C92DEL,4294967295UL,0x056C92DEL,0x9C50E850L},{4294967287UL,0x5E797E64L,0UL,5UL},{0x50D9FA7FL,0x8504EBBAL,1UL,0x5E797E64L}},{{0xAE56AC9CL,0UL,1UL,0x848451CEL},{0x50D9FA7FL,0x6B648A8BL,0UL,0x04A86947L},{4294967287UL,0x056C92DEL,0x4E52984EL,0x50D9FA7FL},{0x4E52984EL,0x50D9FA7FL,0xC63C1D9CL,0UL},{1UL,0x848451CEL,0x848451CEL,1UL},{9UL,0x9ADF0E65L,4294967287UL,0xCDF70D52L},{8UL,0xC63C1D9CL,1UL,0x056C92DEL},{0x8504EBBAL,4294967293UL,0x50D9FA7FL,0x056C92DEL}},{{4294967295UL,0xC63C1D9CL,0xE4B5CA3AL,0xCDF70D52L},{0x848451CEL,0x9ADF0E65L,4294967295UL,1UL},{0x9C50E850L,0x848451CEL,4294967290UL,0UL},{0UL,0x50D9FA7FL,4294967295UL,0x50D9FA7FL},{4294967295UL,0x056C92DEL,4294967293UL,0x04A86947L},{5UL,0x6B648A8BL,0x04A86947L,0x848451CEL},{0UL,0UL,0x056C92DEL,0x5E797E64L},{0UL,0x8504EBBAL,0x04A86947L,5UL}}};
                union U1 * const **l_1847 = &l_1353[3];
                union U1 * const ***l_1846 = &l_1847;
                uint16_t l_1920 = 0x9523L;
                int i, j, k;
            }
            (*l_1323) = p_16;
        }
        else
        { /* block id: 915 */
            int16_t l_1926 = 0x6648L;
            (*g_566) = (255UL >= (safe_mul_func_int8_t_s_s((((((*l_1323) && (-1L)) >= ((l_1925 && 0x1B513056ED384A1DLL) == ((*g_566) || l_1926))) | ((safe_div_func_int64_t_s_s((&g_1069 != (void*)0), g_849.f0)) || (-5L))) > 0UL), l_1926)));
        }
        for (g_1558.f0 = 8; (g_1558.f0 > 0); --g_1558.f0)
        { /* block id: 920 */
            uint16_t l_1939 = 6UL;
            uint32_t **** const l_1949 = (void*)0;
            int16_t *l_1952[10] = {&l_1684,(void*)0,&l_1684,(void*)0,&l_1684,(void*)0,&l_1684,(void*)0,&l_1684,(void*)0};
            uint8_t l_1962[4] = {0x30L,0x30L,0x30L,0x30L};
            int8_t l_1968[6][7] = {{0x68L,0L,1L,1L,0L,0x68L,0L},{0xD2L,6L,6L,0xD2L,0x17L,0xD2L,6L},{0xC8L,0xC8L,0x68L,1L,0x68L,0xC8L,0xC8L},{0x72L,6L,(-9L),6L,0x72L,0x72L,6L},{0x22L,0L,0x22L,0x68L,0x68L,0x22L,0L},{6L,0x17L,(-9L),(-9L),0x17L,6L,0x17L}};
            float *l_1998[1][9] = {{&l_1714,&g_1352[6][7][0].f3,&l_1714,&l_1714,&g_1352[6][7][0].f3,&l_1714,&l_1714,&g_1352[6][7][0].f3,&l_1714}};
            int16_t l_2000 = 8L;
            int32_t l_2058[4][6] = {{0xC61DB05FL,1L,0x42E8CDABL,1L,0xC61DB05FL,0xC61DB05FL},{0L,1L,1L,0L,0xAD165720L,0L},{0L,0xAD165720L,0L,1L,1L,0L},{0xC61DB05FL,0xC61DB05FL,1L,0x42E8CDABL,1L,0xC61DB05FL}};
            int32_t l_2095 = 7L;
            int32_t l_2096 = 0x6BA8177EL;
            int32_t l_2097 = (-10L);
            int32_t l_2098[8];
            uint32_t *****l_2111 = &g_848[5][0][3];
            float l_2132 = 0xF.E2D4ABp-71;
            uint32_t ****l_2148 = &g_1068;
            int i, j;
            for (i = 0; i < 8; i++)
                l_2098[i] = (-1L);
            ++l_1931[4][6][0];
        }
    }
    for (g_1558.f2 = 0; (g_1558.f2 <= 2); g_1558.f2 += 1)
    { /* block id: 1016 */
        int32_t *l_2186 = &l_1381;
        return p_17;
    }
    (*l_1323) ^= 0xB4F5FDFDL;
    return p_18;
}


/* ------------------------------------------ */
/* 
 * reads : g_172 g_1189
 * writes: g_14 g_173 g_1189
 */
static float * func_21(int32_t  p_22, float * p_23, int32_t * p_24, int8_t  p_25)
{ /* block id: 16 */
    float l_1253 = 0x8.7p+1;
    int32_t *l_1266 = &g_5;
    int32_t **l_1265 = &l_1266;
    uint32_t ***l_1269[6] = {&g_161,&g_161,&g_161,&g_161,&g_161,&g_161};
    int32_t l_1272 = (-4L);
    int32_t *l_1302 = &g_84;
    float *l_1320 = (void*)0;
    int i;
    for (p_22 = 0; (p_22 >= (-9)); p_22 = safe_sub_func_int32_t_s_s(p_22, 5))
    { /* block id: 19 */
        int16_t *l_42 = &g_43;
        uint32_t *l_731[3][8][4] = {{{&g_14,&g_573[2].f2,&g_14,&g_89},{(void*)0,&g_573[2].f2,&g_14,&g_89},{&g_573[2].f2,(void*)0,&g_14,(void*)0},{(void*)0,&g_89,&g_14,(void*)0},{&g_573[2].f2,&g_89,(void*)0,&g_14},{&g_573[2].f2,(void*)0,&g_14,&g_89},{&g_89,&g_573[2].f2,&g_89,(void*)0},{&g_14,&g_14,&g_89,(void*)0}},{{&g_14,&g_573[2].f2,&g_573[2].f2,&g_14},{&g_14,&g_14,&g_573[2].f2,&g_89},{&g_14,&g_14,&g_89,(void*)0},{&g_14,(void*)0,&g_89,(void*)0},{&g_89,(void*)0,(void*)0,&g_573[2].f2},{(void*)0,&g_14,(void*)0,&g_89},{&g_14,&g_14,&g_14,&g_14},{&g_573[2].f2,(void*)0,&g_14,&g_573[2].f2}},{{(void*)0,&g_14,(void*)0,&g_89},{&g_89,&g_14,&g_89,&g_89},{(void*)0,&g_14,(void*)0,&g_573[2].f2},{&g_14,(void*)0,&g_89,&g_14},{&g_14,&g_14,&g_573[2].f2,&g_89},{&g_14,&g_14,(void*)0,&g_573[2].f2},{&g_14,(void*)0,&g_89,(void*)0},{&g_89,(void*)0,(void*)0,(void*)0}}};
        int32_t l_732 = (-1L);
        int32_t l_733 = 2L;
        int32_t ***l_734[2];
        uint8_t *l_735 = &g_736;
        uint32_t l_737[4][7];
        uint16_t *l_1247 = &g_1248[0][0][2];
        float *l_1314[7][3];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_734[i] = &g_172;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 7; j++)
                l_737[i][j] = 18446744073709551606UL;
        }
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 3; j++)
                l_1314[i][j] = &g_573[2].f3;
        }
        for (g_14 = 0; (g_14 <= 0); g_14 += 1)
        { /* block id: 22 */
            return p_23;
        }
    }
    (*g_172) = &l_1272;
    for (g_1189 = 0; (g_1189 != 0); g_1189 = safe_add_func_uint16_t_u_u(g_1189, 9))
    { /* block id: 647 */
        float *l_1319 = &g_226;
        return l_1319;
    }
    return l_1320;
}


/* ------------------------------------------ */
/* 
 * reads : g_12 g_308.f0 g_194.f2 g_566 g_43 g_573.f0 g_297 g_298 g_736 g_307.f0 g_561.f2 g_112 g_194.f0 g_89 g_461 g_140 g_26 g_27 g_95.f4 g_205 g_561.f0 g_565 g_849 g_573.f4 g_136 g_194.f4 g_670 g_669 g_930 g_941 g_561.f4 g_3 g_849.f0 g_465 g_466 g_5 g_977 g_253 g_300.f0 g_1011 g_505 g_841 g_261.f0 g_573.f2 g_941.f2 g_172 g_1068 g_1069 g_292.f0 g_513 g_1070 g_1100 g_305.f0 g_941.f4 g_977.f2 g_49 g_1122 g_98 g_1100.f4 g_196.f2 g_306.f0 g_1162 g_1100.f0 g_1100.f2 g_1187 g_1189 g_1197 g_1226 g_1236
 * writes: g_12 g_43 g_112 g_140 g_847 g_205 g_136 g_27 g_669 g_161 g_253 g_505 g_173 g_736 g_89 g_1070 g_461 g_49 g_98 g_1146 g_566 g_941.f2 g_1187 g_95.f2 g_1197 g_95.f0 g_889
 */
static int8_t  func_36(uint32_t  p_37, int8_t  p_38, float  p_39)
{ /* block id: 397 */
    int32_t l_740 = 0xD670A49BL;
    int32_t l_743 = 0x6300D09CL;
    int32_t l_744[2];
    int32_t l_746 = 0x506318CAL;
    int16_t l_747 = 0xB44AL;
    uint16_t l_748[1];
    union U0 **l_753 = (void*)0;
    int32_t ** const *l_817 = &g_172;
    const int32_t *l_851 = &l_743;
    int32_t l_961 = 1L;
    union U0 **** const l_1001 = &g_576;
    int8_t *l_1022 = &g_140;
    int8_t **l_1021[7] = {&l_1022,&l_1022,&l_1022,&l_1022,&l_1022,&l_1022,&l_1022};
    int8_t l_1039[8];
    int32_t l_1067 = 0L;
    uint32_t ***l_1072[2];
    uint32_t ***l_1073 = &g_1069;
    volatile int32_t * volatile l_1137[10][1] = {{&g_1138},{&g_1138},{&g_1138},{&g_1138},{&g_1138},{&g_1138},{&g_1138},{&g_1138},{&g_1138},{&g_1138}};
    union U1 *l_1141 = &g_573[0];
    int32_t l_1239 = 0xC3736C91L;
    int64_t l_1246 = 0x348AB2A0505A9616LL;
    int i, j;
    for (i = 0; i < 2; i++)
        l_744[i] = 0L;
    for (i = 0; i < 1; i++)
        l_748[i] = 0xF99BL;
    for (i = 0; i < 8; i++)
        l_1039[i] = 0L;
    for (i = 0; i < 2; i++)
        l_1072[i] = (void*)0;
    if ((safe_lshift_func_uint8_t_u_u(l_740, l_740)))
    { /* block id: 398 */
        int32_t *l_741 = &g_12[1][1][0];
        int32_t *l_742[8][10][3] = {{{(void*)0,&g_12[0][5][0],&g_84},{&g_84,&g_12[0][5][0],&g_12[2][0][0]},{&g_12[0][1][0],&g_12[2][0][0],&g_84},{&g_84,&g_12[2][0][0],&g_12[2][0][0]},{&g_12[0][1][0],(void*)0,&g_84},{&g_84,&g_12[2][0][0],&g_84},{(void*)0,&g_84,&g_12[2][0][0]},{(void*)0,&g_12[2][0][0],&g_84},{&g_12[2][0][0],&g_84,&g_12[2][0][0]},{&g_84,&g_12[2][0][0],&g_84}},{{&g_84,(void*)0,&g_12[0][2][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0]},{(void*)0,&g_12[2][0][0],&g_12[0][2][0]},{(void*)0,&g_12[0][5][0],&g_84},{&g_84,&g_12[0][5][0],&g_12[2][0][0]},{&g_12[0][1][0],&g_12[2][0][0],&g_84},{&g_84,&g_12[2][0][0],&g_12[2][0][0]},{&g_12[0][1][0],(void*)0,&g_84},{&g_84,&g_12[2][0][0],&g_84},{(void*)0,&g_84,&g_12[2][0][0]}},{{(void*)0,&g_12[2][0][0],&g_84},{&g_12[2][0][0],&g_84,&g_12[2][0][0]},{&g_84,&g_12[2][0][0],&g_84},{&g_84,(void*)0,&g_12[0][2][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0]},{(void*)0,&g_12[2][0][0],&g_12[0][2][0]},{(void*)0,&g_12[0][5][0],&g_84},{&g_84,&g_12[0][5][0],&g_12[2][0][0]},{&g_12[0][1][0],&g_12[2][0][0],&g_84},{&g_84,&g_12[2][0][0],&g_12[2][0][0]}},{{&g_12[0][1][0],(void*)0,&g_84},{&g_84,&g_12[2][0][0],&g_84},{(void*)0,&g_84,&g_12[2][0][0]},{(void*)0,&g_12[3][4][0],&g_12[0][4][0]},{&g_12[2][0][0],&g_12[1][0][0],&g_12[3][4][0]},{&g_12[2][0][0],&g_12[3][3][0],&g_12[2][0][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_84},{&g_12[2][0][0],&g_12[1][4][0],&g_12[3][3][0]},{&g_12[2][0][0],(void*)0,&g_84},{&g_12[0][5][0],(void*)0,&g_12[2][0][0]}},{{&g_12[0][2][0],(void*)0,&g_12[3][4][0]},{&g_12[2][0][0],(void*)0,&g_12[0][4][0]},{&g_84,&g_12[1][4][0],&g_12[1][4][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_12[1][0][0]},{&g_12[0][2][0],&g_12[3][3][0],&g_12[1][0][0]},{&g_12[0][5][0],&g_12[1][0][0],&g_12[1][4][0]},{&g_12[2][0][0],&g_12[3][4][0],&g_12[0][4][0]},{&g_12[2][0][0],&g_12[1][0][0],&g_12[3][4][0]},{&g_12[2][0][0],&g_12[3][3][0],&g_12[2][0][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_84}},{{&g_12[2][0][0],&g_12[1][4][0],&g_12[3][3][0]},{&g_12[2][0][0],(void*)0,&g_84},{&g_12[0][5][0],(void*)0,&g_12[2][0][0]},{&g_12[0][2][0],(void*)0,&g_12[3][4][0]},{&g_12[2][0][0],(void*)0,&g_12[0][4][0]},{&g_84,&g_12[1][4][0],&g_12[1][4][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_12[1][0][0]},{&g_12[0][2][0],&g_12[3][3][0],&g_12[1][0][0]},{&g_12[0][5][0],&g_12[1][0][0],&g_12[1][4][0]},{&g_12[2][0][0],&g_12[3][4][0],&g_12[0][4][0]}},{{&g_12[2][0][0],&g_12[1][0][0],&g_12[3][4][0]},{&g_12[2][0][0],&g_12[3][3][0],&g_12[2][0][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_84},{&g_12[2][0][0],&g_12[1][4][0],&g_12[3][3][0]},{&g_12[2][0][0],(void*)0,&g_84},{&g_12[0][5][0],(void*)0,&g_12[2][0][0]},{&g_12[0][2][0],(void*)0,&g_12[3][4][0]},{&g_12[2][0][0],(void*)0,&g_12[0][4][0]},{&g_84,&g_12[1][4][0],&g_12[1][4][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_12[1][0][0]}},{{&g_12[0][2][0],&g_12[3][3][0],&g_12[1][0][0]},{&g_12[0][5][0],&g_12[1][0][0],&g_12[1][4][0]},{&g_12[2][0][0],&g_12[3][4][0],&g_12[0][4][0]},{&g_12[2][0][0],&g_12[1][0][0],&g_12[3][4][0]},{&g_12[2][0][0],&g_12[3][3][0],&g_12[2][0][0]},{&g_12[2][0][0],&g_12[2][0][0],&g_84},{&g_12[2][0][0],&g_12[1][4][0],&g_12[3][3][0]},{&g_12[2][0][0],(void*)0,&g_84},{&g_12[0][5][0],(void*)0,&g_12[2][0][0]},{&g_12[0][2][0],(void*)0,&g_12[3][4][0]}}};
        int64_t l_745 = 0x0FECEC376D75767ALL;
        int8_t *l_763 = (void*)0;
        int8_t **l_762 = &l_763;
        uint16_t *l_781 = (void*)0;
        uint16_t *l_795 = (void*)0;
        uint16_t *l_796 = &g_112;
        union U0 **l_797 = &g_297;
        int i, j, k;
        --l_748[0];
        l_744[0] = ((*g_566) = (((*l_741) , (((safe_rshift_func_int16_t_s_u((l_753 != l_753), 14)) <= (safe_rshift_func_uint8_t_u_s((0x15524DF1CF938AD5LL || (-1L)), (p_38 , (safe_mul_func_uint16_t_u_u((safe_add_func_int64_t_s_s(((safe_add_func_uint32_t_u_u((((*l_762) = &g_140) == (void*)0), (safe_mod_func_int8_t_s_s(p_38, g_308[0].f0)))) && 0L), l_746)), l_743)))))) == p_37)) < g_194.f2));
        if ((((-1L) < p_38) | l_746))
        { /* block id: 403 */
            uint64_t l_778 = 0UL;
            uint16_t *l_780 = &l_748[0];
            uint16_t **l_779 = &l_780;
            int32_t l_782 = 1L;
            for (g_43 = 0; (g_43 == (-28)); g_43 = safe_sub_func_uint8_t_u_u(g_43, 5))
            { /* block id: 406 */
                uint16_t l_770 = 65535UL;
                l_770 |= ((*g_566) = (safe_add_func_int64_t_s_s(g_573[2].f0, p_37)));
            }
            l_782 |= (safe_mul_func_int16_t_s_s((((safe_rshift_func_uint8_t_u_u(l_743, (~((0xD8A36C8AL | (safe_add_func_int32_t_s_s(((*l_741) = (((*g_297) , (void*)0) != ((0x41FEDD7872ABE8ECLL == (l_778 & (((*l_779) = &l_748[0]) == (l_781 = &g_136)))) , (void*)0))), p_38))) == p_37)))) != 253UL) & g_736), l_778));
        }
        else
        { /* block id: 414 */
            int32_t l_783 = 0xD7E04B58L;
            int32_t l_784 = 0xA8DDF88AL;
            int32_t l_785[10][10][2] = {{{(-4L),4L},{4L,(-1L)},{1L,0L},{0x672D4EBCL,(-10L)},{0x303149DAL,0xE1F90A4AL},{(-4L),0x423E75FAL},{1L,0x672D4EBCL},{(-1L),0xA711585AL},{0xE1F90A4AL,(-10L)},{1L,0x7DB1B6BAL}},{{0x423E75FAL,(-4L)},{4L,0xE1F90A4AL},{(-1L),0x33ABB5BBL},{0x7DB1B6BAL,0x33ABB5BBL},{(-1L),0xE1F90A4AL},{4L,(-4L)},{0x423E75FAL,0x7DB1B6BAL},{1L,(-10L)},{0xE1F90A4AL,0xA711585AL},{(-1L),0x672D4EBCL}},{{1L,0x423E75FAL},{(-4L),0xE1F90A4AL},{0x303149DAL,(-10L)},{0x672D4EBCL,0L},{1L,(-1L)},{4L,(-1L)},{0x7DB1B6BAL,(-1L)},{0x83F87767L,(-4L)},{0x41B537DAL,0x7032CDF3L},{0xCB980B4FL,0x41B537DAL}},{{(-1L),1L},{(-1L),0x41B537DAL},{0xCB980B4FL,0x7032CDF3L},{0x41B537DAL,(-4L)},{0x83F87767L,(-1L)},{0x7DB1B6BAL,(-1L)},{(-1L),0x41B537DAL},{0xB3ABA0D7L,0x89A6FC47L},{(-1L),1L},{0xCB980B4FL,(-4L)}},{{0x7DB1B6BAL,(-1L)},{1L,(-1L)},{0x4943088EL,0x7032CDF3L},{(-4L),1L},{0xB3ABA0D7L,0x9A7341C4L},{(-1L),0x7DB1B6BAL},{(-1L),(-4L)},{0x4943088EL,(-4L)},{0x9A7341C4L,(-4L)},{0x4943088EL,(-4L)}},{{(-1L),0x7DB1B6BAL},{(-1L),0x9A7341C4L},{0xB3ABA0D7L,1L},{(-4L),0x7032CDF3L},{0x4943088EL,(-1L)},{1L,(-1L)},{0x7DB1B6BAL,(-4L)},{0xCB980B4FL,1L},{(-1L),0x89A6FC47L},{0xB3ABA0D7L,0x41B537DAL}},{{(-1L),(-1L)},{0x7DB1B6BAL,(-1L)},{0x83F87767L,(-4L)},{0x41B537DAL,0x7032CDF3L},{0xCB980B4FL,0x41B537DAL},{(-1L),1L},{(-1L),0x41B537DAL},{0xCB980B4FL,0x7032CDF3L},{0x41B537DAL,(-4L)},{0x83F87767L,(-1L)}},{{0x7DB1B6BAL,(-1L)},{(-1L),0x41B537DAL},{0xB3ABA0D7L,0x89A6FC47L},{(-1L),1L},{0xCB980B4FL,(-4L)},{0x7DB1B6BAL,(-1L)},{1L,(-1L)},{0x4943088EL,0x7032CDF3L},{(-4L),1L},{0xB3ABA0D7L,0x9A7341C4L}},{{(-1L),0x7DB1B6BAL},{(-1L),(-4L)},{0x4943088EL,(-4L)},{0x9A7341C4L,(-4L)},{0x4943088EL,(-4L)},{(-1L),0x7DB1B6BAL},{(-1L),0x9A7341C4L},{0xB3ABA0D7L,1L},{(-4L),0x7032CDF3L},{0x4943088EL,(-1L)}},{{1L,(-1L)},{0x7DB1B6BAL,(-4L)},{0xCB980B4FL,1L},{(-1L),0x89A6FC47L},{0xB3ABA0D7L,0x41B537DAL},{(-1L),(-1L)},{0x7DB1B6BAL,(-1L)},{0x83F87767L,(-4L)},{0x41B537DAL,0x7032CDF3L},{0xCB980B4FL,0x41B537DAL}}};
            int32_t l_786 = 0xC2B2F22FL;
            uint64_t l_787[5][9][5] = {{{0UL,0UL,0x59907D5E0A33C36ALL,0UL,0UL},{0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL},{0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL},{0UL,0UL,0x59907D5E0A33C36ALL,0UL,0UL},{0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL},{0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL},{0UL,0UL,0x59907D5E0A33C36ALL,0UL,0UL},{0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL},{0UL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL}},{{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL}},{{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL}},{{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL}},{{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL},{0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL,0UL,0x4CEB39D31FEA2FC4LL,0x4CEB39D31FEA2FC4LL},{0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL},{0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL,0x59907D5E0A33C36ALL,0x4CEB39D31FEA2FC4LL,0x59907D5E0A33C36ALL}}};
            int i, j, k;
            --l_787[3][8][4];
        }
        (*g_566) = ((safe_rshift_func_int8_t_s_u((l_743 || ((safe_div_func_uint64_t_u_u(g_307.f0, (((1L | ((*l_796) |= (safe_unary_minus_func_int64_t_s(g_561.f2)))) <= (*l_741)) ^ ((l_797 = l_797) == l_753)))) & (65526UL == ((((~(safe_add_func_int32_t_s_s((safe_sub_func_int8_t_s_s(g_194.f0, 1L)), p_38))) || 0UL) , 0x450E16ADE906B76CLL) <= g_89)))), 3)) , (-1L));
    }
    else
    { /* block id: 420 */
        int8_t l_823 = (-1L);
        uint32_t ***l_880 = (void*)0;
        int8_t l_887[6];
        int32_t l_897 = (-1L);
        int32_t l_903[8][2] = {{0xD871D5BCL,0xD871D5BCL},{0xD871D5BCL,1L},{0xD871D5BCL,0xD871D5BCL},{0xD871D5BCL,1L},{0xD871D5BCL,0xD871D5BCL},{0xD871D5BCL,1L},{0xD871D5BCL,0xD871D5BCL},{0xD871D5BCL,1L}};
        uint8_t *l_918[5][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
        const int32_t **l_940 = &l_851;
        const int32_t ***l_939 = &l_940;
        uint32_t *l_1036 = &g_505;
        uint32_t **l_1035 = &l_1036;
        int16_t l_1160[4];
        int32_t l_1238 = 2L;
        uint32_t l_1240 = 0x887E8CE2L;
        int i, j;
        for (i = 0; i < 6; i++)
            l_887[i] = 0xBBL;
        for (i = 0; i < 4; i++)
            l_1160[i] = 1L;
        if ((6L || (-1L)))
        { /* block id: 421 */
            const int32_t *l_820 = &l_743;
            const int32_t **l_819 = &l_820;
            const int32_t ***l_818[1][6][9] = {{{&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,&l_819},{(void*)0,(void*)0,&l_819,&l_819,(void*)0,(void*)0,&l_819,(void*)0,&l_819},{(void*)0,&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,(void*)0,(void*)0},{&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,&l_819},{&l_819,(void*)0,&l_819,(void*)0,&l_819,&l_819,&l_819,&l_819,(void*)0},{&l_819,&l_819,&l_819,&l_819,&l_819,&l_819,(void*)0,(void*)0,&l_819}}};
            int i, j, k;
            (*g_566) = (safe_add_func_int32_t_s_s(((safe_lshift_func_uint16_t_u_u((safe_sub_func_int32_t_s_s((safe_add_func_int32_t_s_s((safe_mul_func_int8_t_s_s((safe_div_func_int32_t_s_s((g_461[4] , (safe_rshift_func_uint8_t_u_u((((l_817 == l_818[0][2][4]) , g_140) < p_38), 2))), 0x0851D1B1L)), (((0xAECCBFD22FECE078LL < 18446744073709551615UL) == ((safe_sub_func_float_f_f(((p_37 != (*g_26)) == 0x0.9p+1), p_38)) , l_823)) , 0x92L))), (**l_819))), p_38)), (**l_819))) && 0x545BFE10L), l_823));
        }
        else
        { /* block id: 423 */
            uint8_t l_826[4][3];
            union U0 *l_835 = &g_308[0];
            int8_t *l_837 = &g_140;
            uint16_t *l_838 = &g_136;
            const uint16_t *l_840 = &g_841;
            const uint16_t **l_839 = &l_840;
            uint32_t ****l_846 = (void*)0;
            uint32_t *****l_845 = &l_846;
            const int32_t *l_850 = &g_12[2][0][0];
            int32_t l_857 = 0x2CD6FAB8L;
            uint32_t ***l_879 = &g_161;
            int32_t l_902 = 9L;
            int32_t l_904[10][4] = {{0xF86436B9L,1L,0x6CDF87E9L,8L},{1L,9L,0x405F1154L,9L},{9L,0x6CDF87E9L,0xF86436B9L,9L},{0xF86436B9L,9L,8L,8L},{1L,1L,0x405F1154L,1L},{1L,0x6CDF87E9L,8L,1L},{0xF86436B9L,1L,0xF86436B9L,8L},{9L,1L,0x405F1154L,1L},{1L,0x6CDF87E9L,0x6CDF87E9L,1L},{0xF86436B9L,1L,0x6CDF87E9L,8L}};
            int16_t l_949[7];
            union U1 **l_962 = &g_98;
            uint32_t **l_976 = &g_162;
            uint8_t l_984 = 252UL;
            uint32_t *l_1018 = &g_505;
            union U0 ****l_1046 = &g_576;
            uint8_t l_1086[6] = {255UL,255UL,255UL,255UL,255UL,255UL};
            uint8_t l_1232 = 254UL;
            int32_t l_1237[7][5] = {{0x6D7D6CC6L,0x6D7D6CC6L,0x4020C0E1L,(-3L),1L},{0xF2C9FC13L,5L,0x4020C0E1L,0xFB587E25L,1L},{(-2L),0x4020C0E1L,(-9L),0x4020C0E1L,(-2L)},{(-3L),5L,1L,(-2L),0x6D7D6CC6L},{(-3L),0x6D7D6CC6L,1L,1L,1L},{(-2L),0L,(-2L),5L,0x6D7D6CC6L},{0xF2C9FC13L,1L,0x6D7D6CC6L,5L,(-2L)}};
            int i, j;
            for (i = 0; i < 4; i++)
            {
                for (j = 0; j < 3; j++)
                    l_826[i][j] = 0xA5L;
            }
            for (i = 0; i < 7; i++)
                l_949[i] = 0x6E85L;
lbl_1195:
            if ((p_37 , (safe_mod_func_uint32_t_u_u(l_826[0][2], ((**g_565) = ((((((safe_lshift_func_uint8_t_u_s((safe_rshift_func_int8_t_s_u(((safe_lshift_func_int8_t_s_s((((*l_837) = (((void*)0 != l_835) && (!p_37))) , ((l_838 == ((*l_839) = &g_112)) || g_95.f4)), 4)) || ((0x93C293BEL | p_37) ^ p_37)), g_205)), 0)) ^ g_561.f0) , 0xD.8BC658p-10) == 0xD.693B67p+66) , (-2L)) , p_38))))))
            { /* block id: 427 */
                (*g_566) |= (safe_sub_func_int16_t_s_s(((((safe_unary_minus_func_int8_t_s(p_37)) || ((g_847 = l_845) != &g_848[5][0][3])) , g_849) , g_561.f2), p_38));
            }
            else
            { /* block id: 430 */
                int32_t l_886[6] = {1L,0x15EF2813L,1L,1L,0x15EF2813L,1L};
                int32_t l_888 = 0xB0535AD3L;
                const union U1 * const *l_951 = &g_422;
                const union U1 * const **l_950 = &l_951;
                int i;
                l_851 = l_850;
                if ((!(safe_mod_func_uint16_t_u_u(g_89, (*l_851)))))
                { /* block id: 432 */
                    uint16_t l_858 = 0xE305L;
                    int32_t l_881 = 0xBE3E4F80L;
                    int16_t l_885 = (-8L);
                    int32_t l_905 = 0x5DCB2E0BL;
                    int32_t l_906 = 0x514AC16DL;
                    int32_t l_907 = 0xFC75F625L;
                    int32_t l_908 = 0x0C44FF7EL;
                    uint32_t *l_928[5][4] = {{&g_573[2].f2,&g_89,&g_573[2].f2,&g_89},{&g_573[2].f2,&g_89,&g_573[2].f2,&g_89},{&g_573[2].f2,&g_89,&g_573[2].f2,&g_89},{&g_573[2].f2,&g_89,&g_573[2].f2,&g_89},{&g_573[2].f2,&g_89,&g_573[2].f2,&g_89}};
                    int i, j;
                    if (p_38)
                    { /* block id: 433 */
                        int32_t *l_856 = (void*)0;
                        int32_t **l_855 = &l_856;
                        int16_t *l_876 = &g_43;
                        uint64_t *l_884 = &g_205;
                        (*g_566) = (((&g_461[4] != ((*l_855) = &g_461[4])) , 1L) == l_857);
                        ++l_858;
                        l_888 = ((safe_rshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s(((g_573[2].f4 , (l_886[2] |= ((0x1B141CA8048824CCLL > ((((safe_unary_minus_func_int64_t_s((((*g_566) = ((safe_rshift_func_uint8_t_u_s((((safe_mod_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((safe_rshift_func_uint16_t_u_s(((*l_838) ^= (safe_div_func_int16_t_s_s(((*l_876) = (0xB4L == p_37)), ((safe_rshift_func_int16_t_s_u((l_881 = ((l_879 != l_880) >= (*l_850))), ((((*l_884) ^= (safe_mul_func_int8_t_s_s((7UL ^ ((void*)0 == l_856)), p_37))) <= 18446744073709551606UL) , p_37))) , 0x2048L)))), 8)), 0x27CDL)), l_885)) < 0x7E68L) , 246UL), p_37)) && l_885)) != 1L))) >= g_561.f0) & 0L) < p_37)) < 1UL))) && l_887[5]), g_194.f0)), 8)) | l_888);
                    }
                    else
                    { /* block id: 444 */
                        int32_t *l_890 = &l_744[1];
                        int32_t *l_891 = &g_12[2][4][0];
                        int32_t l_892 = 0x9B80CADAL;
                        int32_t *l_893 = &g_12[2][0][0];
                        int32_t *l_894 = (void*)0;
                        int32_t *l_895 = (void*)0;
                        int32_t *l_896 = (void*)0;
                        int32_t *l_898 = &g_12[2][0][0];
                        int32_t *l_899 = &l_888;
                        int32_t *l_900 = (void*)0;
                        int32_t *l_901[8] = {&l_743,(void*)0,(void*)0,&l_743,(void*)0,(void*)0,&l_743,(void*)0};
                        uint8_t l_909 = 0x45L;
                        uint64_t *l_927 = &g_205;
                        int16_t *l_929[10][5][3] = {{{&l_885,&g_43,&l_747},{&g_43,&g_43,&l_885},{&l_885,&g_43,&l_747},{&g_49,(void*)0,(void*)0},{(void*)0,&l_885,&g_43}},{{(void*)0,&g_49,&g_43},{&g_43,&g_49,&l_885},{&g_43,&g_43,&l_747},{&g_43,&g_49,&l_885},{&g_49,&g_49,&g_49}},{{&g_49,&l_885,&g_43},{(void*)0,(void*)0,(void*)0},{&l_885,&g_43,(void*)0},{(void*)0,&g_43,&l_885},{&l_885,&l_747,&l_885}},{{(void*)0,(void*)0,&g_49},{&g_49,&l_747,&g_43},{&g_49,&l_747,(void*)0},{&g_43,(void*)0,&g_49},{&g_43,&g_49,(void*)0}},{{&g_43,&g_43,&g_43},{(void*)0,(void*)0,&g_49},{(void*)0,&g_43,&l_885},{&g_49,(void*)0,&l_885},{&l_885,&g_43,(void*)0}},{{&g_43,(void*)0,(void*)0},{&g_43,&g_43,&g_43},{(void*)0,(void*)0,&g_49},{&l_747,&g_43,&l_885},{(void*)0,&g_49,&l_747}},{{&l_885,(void*)0,&l_885},{(void*)0,&l_747,&g_43},{&l_747,&l_747,&g_43},{(void*)0,(void*)0,(void*)0},{&g_43,&l_747,&l_747}},{{&g_43,&g_43,&l_885},{&l_885,&g_43,&l_747},{&g_49,(void*)0,(void*)0},{(void*)0,&l_885,&g_43},{(void*)0,&g_49,&g_43}},{{&g_43,&g_49,&l_885},{&g_43,&g_43,&l_747},{&g_43,&g_49,&l_885},{&g_49,&g_49,&g_49},{&g_49,&l_885,&g_43}},{{(void*)0,(void*)0,(void*)0},{&l_885,&g_43,(void*)0},{(void*)0,&g_43,&l_885},{&l_885,&l_747,&l_885},{(void*)0,(void*)0,&g_49}}};
                        int i, j, k;
                        l_909--;
                        (*g_26) = ((~(safe_mul_func_int16_t_s_s(((*l_893) = ((+(0xB9L < (1L ^ ((*l_838)--)))) < (l_907 &= (((((l_823 <= (p_38 != (0x2E32F81CL & p_38))) ^ (((((((l_918[3][1] != ((safe_lshift_func_int16_t_s_u((safe_sub_func_int16_t_s_s(((safe_lshift_func_int16_t_s_s(p_38, 13)) || (safe_add_func_uint64_t_u_u(((*l_927) = 8UL), 0x3DBAA4E2AF4FB7E3LL))), p_37)), g_12[1][3][0])) , &l_826[1][2])) <= g_194.f4) , l_906) , p_37) & 0xEEL) , 0UL) != p_38)) ^ 4L) , l_928[0][2]) != (void*)0)))), p_37))) , p_37);
                        (*g_930) = (*g_670);
                    }
                }
                else
                { /* block id: 453 */
                    uint16_t l_948 = 0UL;
                    l_888 |= (((*l_850) , (((safe_add_func_uint16_t_u_u((((((safe_add_func_int16_t_s_s((safe_sub_func_int32_t_s_s((safe_lshift_func_int8_t_s_u((&g_930 != (l_939 = (void*)0)), 6)), ((*g_566) = (g_941[1] , (((((!2L) & (~(safe_lshift_func_int8_t_s_u((g_561.f4 >= (safe_add_func_uint32_t_u_u(p_38, (((l_886[0] < (p_37 , (*l_850))) , (void*)0) == (void*)0)))), g_573[2].f0)))) == g_89) , p_38) == 0x95L))))), l_948)) , p_37) , l_949[0]) , g_3) , g_849.f0), p_37)) <= (-7L)) , l_950)) == &l_951);
                }
                if ((0x888F2308L & p_37))
                { /* block id: 458 */
                    uint32_t *l_966 = &g_505;
                    uint32_t **l_965 = &l_966;
                    const uint32_t *l_968 = &g_969;
                    const uint32_t **l_967 = &l_968;
                    int32_t l_975 = 0xFD741AC5L;
                    uint32_t **l_978 = &g_162;
                    int64_t *l_981[4][4][8] = {{{&g_3,&g_253[2],&g_941[1].f0,&g_573[2].f0,&g_941[1].f0,(void*)0,&g_941[1].f0,(void*)0},{(void*)0,&g_253[2],&g_253[2],&g_253[3],&g_253[2],&g_253[2],&g_3,(void*)0},{(void*)0,&g_253[1],&g_253[3],(void*)0,&g_253[2],(void*)0,&g_253[6],(void*)0},{(void*)0,&g_253[6],&g_941[1].f0,&g_573[2].f0,&g_977.f0,&g_573[2].f0,&g_253[2],(void*)0}},{{&g_3,&g_941[1].f0,&g_977.f0,&g_941[1].f0,(void*)0,&g_253[2],&g_977.f0,&g_253[3]},{(void*)0,&g_977.f0,&g_253[5],&g_253[2],&g_253[4],(void*)0,&g_977.f0,(void*)0},{&g_253[5],(void*)0,&g_573[2].f0,(void*)0,&g_253[5],(void*)0,(void*)0,&g_941[1].f0},{(void*)0,&g_977.f0,&g_253[2],&g_253[2],(void*)0,&g_941[1].f0,&g_3,(void*)0}},{{&g_941[1].f0,&g_253[2],&g_253[2],(void*)0,&g_573[2].f0,&g_573[2].f0,(void*)0,&g_253[2]},{(void*)0,&g_253[2],&g_573[2].f0,&g_941[1].f0,(void*)0,(void*)0,&g_977.f0,&g_573[2].f0},{(void*)0,&g_3,&g_253[2],&g_573[2].f0,&g_977.f0,&g_253[6],&g_977.f0,&g_253[2]},{&g_573[2].f0,&g_253[2],(void*)0,&g_941[1].f0,&g_253[2],&g_941[1].f0,&g_253[2],&g_3}},{{&g_3,&g_253[2],(void*)0,(void*)0,(void*)0,&g_253[2],(void*)0,&g_941[1].f0},{&g_941[1].f0,&g_941[1].f0,&g_977.f0,&g_253[2],&g_3,&g_3,&g_253[2],&g_977.f0},{&g_253[2],&g_253[2],&g_977.f0,(void*)0,(void*)0,(void*)0,(void*)0,&g_573[2].f0},{&g_573[2].f0,&g_3,(void*)0,&g_573[2].f0,(void*)0,(void*)0,&g_253[3],&g_573[2].f0}}};
                    int i, j, k;
                    (*g_26) = 0x1.2p-1;
                    (*g_26) = (safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_sub_func_float_f_f((*l_851), (*g_26))), ((safe_div_func_float_f_f(((+l_961) >= ((l_962 == (*g_465)) > ((safe_sub_func_float_f_f((((*l_965) = &g_505) != ((*l_967) = &p_37)), (-0x5.Cp-1))) >= ((safe_sub_func_float_f_f(((((safe_div_func_float_f_f(((~((g_5 | p_38) != 1L)) , 0x5.5F0BF9p+90), l_975)) >= (*l_850)) == (*l_851)) != (*g_26)), 0x7.3p-1)) == l_886[0])))), l_975)) < p_39))), (*g_26)));
                    (*g_566) = (((*l_879) = l_976) == (g_977 , l_978));
                    l_975 = (safe_mul_func_uint16_t_u_u(((g_253[2] ^= (p_38 , 0xAF276FC25C0E75C4LL)) || ((**l_940) && (safe_rshift_func_int8_t_s_s(((l_984 = ((*l_851) & (-6L))) && ((safe_mul_func_uint8_t_u_u(g_736, (safe_sub_func_uint32_t_u_u(((l_975 >= (((safe_add_func_uint32_t_u_u(p_38, (safe_rshift_func_int16_t_s_u(l_975, 13)))) <= 0x15C8159F0684195CLL) , 0x2FBCCED9L)) > l_975), p_37)))) ^ l_975)), 2)))), l_886[2]));
                }
                else
                { /* block id: 468 */
                    union U1 *l_1008 = &g_941[1];
                    for (g_43 = (-20); (g_43 > 21); g_43 = safe_add_func_int64_t_s_s(g_43, 3))
                    { /* block id: 471 */
                        (*g_566) = ((p_38 , ((((safe_lshift_func_uint16_t_u_s(p_37, 4)) ^ (safe_lshift_func_int16_t_s_u((safe_mod_func_uint64_t_u_u(((void*)0 == l_1001), ((((*l_837) = (safe_div_func_uint64_t_u_u(g_308[0].f0, 9UL))) & ((0xEAL >= ((safe_div_func_uint32_t_u_u(0x95D4B89EL, (safe_lshift_func_int8_t_s_u(0x2FL, 6)))) ^ 0UL)) > g_300.f0)) | p_38))), p_37))) , (void*)0) == l_1008)) == g_573[2].f4);
                        if ((*l_851))
                            break;
                    }
                }
                for (p_38 = 0; (p_38 >= 15); p_38 = safe_add_func_uint16_t_u_u(p_38, 6))
                { /* block id: 479 */
                    (*g_1011) = (*g_930);
                }
            }
            if (((safe_mul_func_int16_t_s_s(((safe_add_func_float_f_f(p_37, (*g_26))) , (((safe_mod_func_int32_t_s_s(((p_37 = (++(*l_1018))) , 1L), (l_744[0] ^= (l_1021[5] == ((safe_mod_func_uint16_t_u_u((p_38 == (safe_add_func_uint8_t_u_u(p_38, g_5))), (*l_851))) , &l_837))))) || p_38) == (**l_940))), 0xD0FEL)) , (*l_850)))
            { /* block id: 486 */
                const uint32_t *l_1057 = &g_969;
                const uint32_t **l_1056[6] = {&l_1057,&l_1057,&l_1057,&l_1057,&l_1057,&l_1057};
                int32_t * const *l_1087 = (void*)0;
                int32_t l_1129 = (-1L);
                int32_t l_1130 = (-10L);
                int32_t l_1131 = 0xD3FD169BL;
                int32_t l_1132 = 0x27E9BCB7L;
                uint64_t l_1133[9] = {0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL,0x59CF4B30CCFFB7BCLL};
                volatile int32_t * volatile *l_1136[10] = {&g_669,&g_669,&g_669,&g_669,&g_669,&g_669,&g_669,&g_669,&g_669,&g_669};
                int8_t **l_1144 = &l_837;
                union U0 ** const **l_1173 = (void*)0;
                uint32_t l_1179 = 18446744073709551615UL;
                uint32_t l_1181 = 0x47A07FB6L;
                float l_1191 = 0xC.7432F2p+93;
                int i;
                if ((**l_940))
                { /* block id: 487 */
                    uint16_t l_1041 = 65535UL;
                    int32_t l_1047 = 0x18550D1FL;
                    float l_1060[7] = {0x0.1p-1,0x0.1p-1,0x0.1p-1,0x0.1p-1,0x0.1p-1,0x0.1p-1,0x0.1p-1};
                    int i;
                    if ((safe_mod_func_int16_t_s_s((((((((safe_add_func_uint32_t_u_u(p_37, 0x01C837D0L)) || (safe_mul_func_int16_t_s_s(((safe_lshift_func_int16_t_s_s((l_1035 != (void*)0), ((safe_sub_func_int16_t_s_s(0x94B9L, (((((l_1039[2] <= (safe_unary_minus_func_uint32_t_u(l_1041))) , (safe_add_func_int8_t_s_s(((*l_1022) = g_841), ((p_38 , g_253[3]) | g_261.f0)))) != p_38) == g_573[2].f2) && p_37))) && (*g_566)))) <= p_38), g_941[1].f2))) >= p_38) ^ (*l_851)) ^ g_841) != (*l_850)) && 5UL), p_37)))
                    { /* block id: 489 */
                        (*g_172) = (*g_565);
                    }
                    else
                    { /* block id: 491 */
                        int64_t l_1058 = 0xA8F82E8365EA7A40LL;
                        union U1 **l_1059[1][1];
                        int32_t l_1061 = (-9L);
                        int16_t *l_1066 = &g_43;
                        uint32_t ****l_1071[8] = {&g_1068,&g_1068,&g_1068,&g_1068,&g_1068,&g_1068,&g_1068,&g_1068};
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_1059[i][j] = &g_98;
                        }
                        l_1047 |= (safe_rshift_func_uint16_t_u_u(((**l_940) > ((void*)0 == l_1046)), 13));
                        l_897 ^= p_38;
                        (*g_566) = (((safe_mod_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((0xE8L ^ ((safe_mod_func_int64_t_s_s((((*l_1066) = (((safe_lshift_func_uint8_t_u_s(((void*)0 == l_1056[5]), 3)) <= l_1058) & (p_37 < (((void*)0 == l_1059[0][0]) <= (p_38 <= ((l_904[3][3] |= (++g_736)) & (safe_mod_func_int16_t_s_s((p_38 & g_12[2][0][0]), 0xC060L)))))))) | l_1067), 0xB601A3681B43351FLL)) , g_89)), g_194.f4)), p_38)) || 4UL) != l_1058);
                        l_1073 = (l_1072[0] = g_1068);
                    }
                }
                else
                { /* block id: 501 */
                    const uint32_t l_1088 = 6UL;
                    uint64_t *l_1111[9][1][5] = {{{(void*)0,(void*)0,&g_205,(void*)0,(void*)0}},{{(void*)0,&g_205,(void*)0,(void*)0,&g_205}},{{(void*)0,&g_205,&g_205,(void*)0,&g_205}},{{&g_205,&g_205,(void*)0,&g_205,&g_205}},{{&g_205,(void*)0,&g_205,&g_205,(void*)0}},{{&g_205,(void*)0,(void*)0,&g_205,(void*)0}},{{(void*)0,(void*)0,&g_205,(void*)0,(void*)0}},{{(void*)0,&g_205,(void*)0,(void*)0,&g_205}},{{(void*)0,&g_205,&g_205,(void*)0,&g_205}}};
                    float l_1117[9] = {0x2.61142Dp-44,0x2.61142Dp-44,0x7.72F25Ap-5,0x2.61142Dp-44,0x2.61142Dp-44,0x7.72F25Ap-5,0x2.61142Dp-44,0x2.61142Dp-44,0x7.72F25Ap-5};
                    int32_t *l_1118 = &g_461[4];
                    int32_t l_1119 = 0xDDE7EC2EL;
                    int32_t *l_1123 = &l_744[1];
                    int32_t *l_1124 = &g_12[2][0][0];
                    int32_t *l_1125 = &l_904[2][3];
                    int32_t *l_1126 = &l_743;
                    int32_t *l_1127 = &g_84;
                    int32_t *l_1128[5][9] = {{&l_904[0][0],&g_12[2][0][0],&g_84,&g_12[2][0][0],&l_904[0][0],&l_904[0][0],&g_12[2][0][0],&g_84,&g_12[2][0][0]},{&g_12[2][0][0],&l_897,&g_84,&l_897,&l_904[0][0],&g_12[2][0][0],&l_904[0][0],&l_897,&l_897},{&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&l_897,&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&l_897},{&g_84,&l_904[0][0],&g_84,&g_12[2][0][0],&g_12[2][0][0],&g_84,&l_904[0][0],&g_84,&g_12[2][0][0]},{&g_84,&g_12[2][0][0],&g_12[2][0][0],&g_84,&l_904[0][0],&g_84,&g_12[2][0][0],&g_12[2][0][0],&g_84}};
                    int i, j, k;
                    for (g_89 = 0; (g_89 <= 6); g_89 += 1)
                    { /* block id: 504 */
                        float **l_1094 = &g_26;
                        int32_t *l_1095 = &l_743;
                        int i;
                        l_904[2][1] &= ((((~(+((safe_add_func_uint32_t_u_u(((g_253[g_89] ^ 0x0EDC66EDL) == 4294967295UL), (safe_lshift_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((((p_37 < (safe_mul_func_int8_t_s_s(p_37, ((*g_1068) == &l_1036)))) & 0L) , p_38), (-5L))), g_292.f0)))) & (**l_940)))) == 0x87EC8100BD8EEC31LL) != (-1L)) , g_253[g_89]);
                        (*l_1095) |= (safe_mul_func_uint8_t_u_u(((((((l_1086[5] , ((p_37 != ((void*)0 == l_1087)) , l_1088)) > ((safe_mul_func_uint16_t_u_u(p_37, (!((*l_838) = (safe_mul_func_int8_t_s_s((-1L), (p_38 ^ ((void*)0 == l_1094)))))))) & g_573[2].f4)) != 1L) > (*l_850)) != 0xD387A680L) == (-10L)), g_513));
                        (*g_566) = ((*l_1095) = 0x61ED1C06L);
                    }
                    l_1119 &= (safe_mul_func_uint16_t_u_u((((*g_1069) = (**g_1068)) != &p_37), (safe_sub_func_uint16_t_u_u(((((g_1100 , g_941[1].f2) <= (((+((safe_mod_func_uint64_t_u_u(((!((safe_add_func_int64_t_s_s((safe_sub_func_int8_t_s_s(5L, (((*l_1118) &= ((p_37 , ((((safe_mod_func_uint16_t_u_u(((((g_205--) != (g_573[2].f0 != (p_38 | (safe_lshift_func_uint8_t_u_u(((+(-8L)) && (*l_851)), 7))))) , 5L) | 0x743CD942L), g_194.f2)) & g_305.f0) > p_37) >= p_38)) , p_38)) , 246UL))), g_300.f0)) && 0x4CCDF086L)) <= p_38), g_941[1].f4)) != 1L)) || l_1088) || (*g_566))) <= (*l_851)) & g_977.f2), 0x3A85L))));
                    for (g_49 = (-16); (g_49 < (-23)); g_49--)
                    { /* block id: 517 */
                        (*g_1122) = (*g_1011);
                    }
                    ++l_1133[5];
                }
                l_1137[9][0] = (*g_1122);
                for (l_1067 = 0; (l_1067 >= 0); l_1067 -= 1)
                { /* block id: 525 */
                    int16_t l_1142 = 0xD652L;
                    int32_t l_1159 = (-4L);
                    uint32_t l_1161 = 4294967291UL;
                    if ((safe_mod_func_int64_t_s_s(g_561.f0, g_253[6])))
                    { /* block id: 526 */
                        int8_t l_1143 = 2L;
                        int8_t ***l_1145 = (void*)0;
                        int32_t **l_1158[10][1] = {{&g_566},{&g_173[0][2]},{&g_566},{&g_173[0][2]},{&g_566},{&g_173[0][2]},{&g_566},{&g_173[0][2]},{&g_566},{&g_173[0][2]}};
                        uint32_t *l_1180 = &g_941[1].f2;
                        int i, j;
                        l_744[0] |= (((l_1142 = (1UL <= (l_1141 != ((*l_962) = (*l_962))))) && l_1143) >= ((g_1146[5] = l_1144) == ((((((safe_rshift_func_int8_t_s_s((safe_div_func_int8_t_s_s((safe_add_func_uint64_t_u_u((~g_1100.f4), g_196.f2)), (safe_div_func_int32_t_s_s((safe_rshift_func_uint16_t_u_s((((((g_566 = &l_903[7][1]) == &g_84) , p_38) || p_37) <= l_1159), g_306.f0)), l_1160[0])))), 5)) & p_37) & 1UL) < p_37) , l_1161) , g_1162)));
                        (*g_566) |= ((g_1100.f0 , (((safe_rshift_func_uint16_t_u_u(((*l_838)--), (safe_div_func_int8_t_s_s((((((*l_1180) = ((0x272FL != (safe_add_func_uint32_t_u_u(((((((void*)0 != l_1173) & p_38) , ((safe_lshift_func_uint8_t_u_u((safe_unary_minus_func_uint8_t_u((g_736 = ((((safe_lshift_func_uint16_t_u_u(g_253[3], ((*l_817) == (void*)0))) ^ l_1179) < p_37) , 255UL)))), 4)) , g_253[2])) , 0L) != 4294967286UL), p_38))) , (**l_940))) <= 4294967290UL) >= 0L) , (-1L)), p_37)))) && g_1100.f2) , l_1159)) != l_1142);
                    }
                    else
                    { /* block id: 536 */
                        return l_1181;
                    }
                    for (p_38 = 1; (p_38 >= 0); p_38 -= 1)
                    { /* block id: 541 */
                        int16_t *l_1186 = &l_949[0];
                        int8_t **l_1188[4][9][7] = {{{&l_837,(void*)0,&l_1022,&l_1022,&l_837,&l_1022,&l_837},{&l_1022,&l_837,&l_1022,&l_837,&l_837,&l_837,&l_837},{(void*)0,&l_1022,&l_837,&l_837,&l_837,&l_1022,&l_1022},{&l_1022,&l_1022,&l_837,&l_837,&l_1022,&l_837,&l_1022},{&l_1022,&l_837,&l_837,(void*)0,&l_837,&l_837,&l_1022},{&l_1022,&l_837,&l_837,(void*)0,&l_837,&l_1022,&l_837},{&l_1022,(void*)0,&l_837,&l_1022,&l_837,&l_837,&l_1022},{&l_837,&l_837,&l_837,&l_837,&l_837,&l_837,&l_1022},{(void*)0,&l_837,&l_1022,&l_837,&l_1022,&l_1022,(void*)0}},{{&l_1022,(void*)0,&l_1022,&l_1022,&l_837,&l_1022,&l_1022},{&l_837,&l_1022,&l_837,&l_1022,&l_1022,&l_837,&l_1022},{&l_1022,(void*)0,&l_1022,&l_1022,&l_837,(void*)0,&l_837},{(void*)0,&l_837,&l_837,&l_1022,&l_1022,&l_837,&l_1022},{&l_1022,&l_837,(void*)0,&l_1022,&l_837,&l_1022,&l_837},{&l_1022,&l_1022,&l_837,&l_1022,(void*)0,&l_837,&l_1022},{&l_1022,&l_1022,&l_1022,&l_1022,&l_837,(void*)0,&l_837},{&l_837,&l_837,(void*)0,&l_837,(void*)0,&l_837,&l_837},{&l_1022,&l_1022,&l_837,&l_837,&l_1022,(void*)0,(void*)0}},{{&l_837,&l_1022,&l_837,&l_1022,&l_837,&l_837,&l_1022},{&l_837,&l_837,&l_837,(void*)0,&l_837,&l_1022,&l_837},{&l_1022,&l_1022,&l_1022,(void*)0,&l_837,&l_837,&l_837},{&l_1022,&l_837,&l_1022,&l_1022,&l_1022,&l_1022,(void*)0},{&l_1022,(void*)0,&l_837,&l_837,&l_837,&l_837,&l_837},{&l_1022,&l_837,&l_837,&l_1022,&l_1022,(void*)0,(void*)0},{&l_837,&l_1022,&l_837,(void*)0,&l_1022,&l_1022,&l_1022},{&l_1022,(void*)0,&l_837,&l_1022,&l_837,&l_837,&l_1022},{&l_1022,&l_837,&l_1022,&l_1022,(void*)0,&l_1022,&l_837}},{{(void*)0,&l_1022,(void*)0,&l_1022,&l_837,(void*)0,(void*)0},{&l_1022,&l_1022,(void*)0,&l_837,(void*)0,&l_1022,&l_1022},{&l_1022,(void*)0,&l_1022,&l_837,&l_837,&l_837,(void*)0},{&l_837,&l_837,&l_837,(void*)0,(void*)0,&l_1022,&l_1022},{&l_1022,&l_837,(void*)0,&l_837,&l_837,(void*)0,&l_837},{(void*)0,&l_1022,&l_1022,&l_837,&l_837,&l_837,(void*)0},{&l_837,&l_837,&l_1022,&l_837,(void*)0,&l_1022,&l_1022},{&l_1022,&l_837,&l_837,&l_1022,&l_1022,(void*)0,&l_837},{&l_1022,(void*)0,&l_837,&l_837,(void*)0,&l_837,&l_837}}};
                        uint32_t *l_1190 = &g_95.f2;
                        int i, j, k;
                        l_903[l_1067][l_1067] = (l_903[(p_38 + 5)][l_1067] | (((safe_lshift_func_uint16_t_u_s(g_461[2], ((*l_1186) = (8UL && g_308[0].f0)))) || ((g_1187 = g_1187) == l_1188[2][6][4])) ^ ((((*l_1190) = g_1189) >= ((p_37 != (-1L)) , l_1161)) == l_1159)));
                    }
                    (*g_26) = (*g_26);
                    for (p_38 = 0; (p_38 >= 0); p_38 -= 1)
                    { /* block id: 550 */
                        uint8_t l_1192 = 0x87L;
                        (*g_1122) = (*g_1122);
                        if (p_37)
                            break;
                        (*l_940) = (*l_940);
                        --l_1192;
                    }
                }
                return p_37;
            }
            else
            { /* block id: 558 */
                int16_t l_1196 = 0x76B7L;
                int32_t l_1201 = 0x13DE12C1L;
                uint32_t l_1203 = 0x0B1AE5ECL;
                if (g_977.f0)
                    goto lbl_1195;
                (*g_26) = p_39;
                for (g_140 = 0; (g_140 <= 0); g_140 += 1)
                { /* block id: 563 */
                    int16_t l_1202 = (-1L);
                    for (l_1067 = 0; (l_1067 <= 0); l_1067 += 1)
                    { /* block id: 566 */
                        const volatile float ** volatile * volatile *l_1200 = &g_1197[5][1];
                        (*l_1200) = (l_1196 , g_1197[7][0]);
                    }
                    for (g_89 = 0; (g_89 <= 0); g_89 += 1)
                    { /* block id: 571 */
                        int64_t *l_1233 = &g_95.f0;
                        int32_t l_1234[7][5][2] = {{{1L,(-2L)},{2L,(-2L)},{1L,2L},{2L,2L},{2L,2L}},{{1L,(-2L)},{2L,(-2L)},{1L,2L},{2L,2L},{2L,2L}},{{1L,(-2L)},{2L,(-2L)},{1L,2L},{2L,2L},{2L,2L}},{{1L,(-2L)},{2L,(-2L)},{1L,2L},{2L,2L},{2L,2L}},{{1L,(-2L)},{2L,(-2L)},{1L,2L},{2L,2L},{2L,2L}},{{1L,(-2L)},{2L,(-2L)},{1L,2L},{2L,2L},{2L,2L}},{{1L,(-2L)},{2L,(-2L)},{1L,2L},{2L,2L},{2L,2L}}};
                        int32_t *l_1235 = &l_744[0];
                        int i, j, k;
                        l_1203++;
                        (*l_1235) ^= (safe_mul_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s(((safe_div_func_int64_t_s_s((safe_div_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u(((safe_mod_func_int16_t_s_s(g_941[1].f2, ((g_1100.f0 > ((((*l_1233) = ((safe_mod_func_int32_t_s_s((safe_mod_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s((((!(((g_307.f0 < (*l_851)) == ((safe_rshift_func_uint8_t_u_u(((void*)0 != g_1226[2]), g_307.f0)) < (safe_unary_minus_func_uint16_t_u((((safe_mul_func_uint16_t_u_u(((p_38 , 0x5461L) , l_1203), g_3)) , l_1203) == (*g_566)))))) || 65535UL)) <= g_140) || l_1203))), l_1232)), l_1202)) , p_38)) < 0UL) , p_38)) , 1UL))) || p_38), l_1234[2][4][0])), p_38)), p_37)) >= p_37), 250UL)) & l_1203), p_38));
                        g_889[g_140][(g_89 + 1)] = &l_1234[5][3][1];
                    }
                    (*g_1236) = (*g_930);
                }
            }
            --l_1240;
        }
    }
    for (g_205 = 0; (g_205 > 8); ++g_205)
    { /* block id: 585 */
        uint8_t l_1245[4] = {0UL,0UL,0UL,0UL};
        int i;
        return l_1245[2];
    }
    return l_1246;
}


/* ------------------------------------------ */
/* 
 * reads : g_43 g_49 g_14 g_69 g_12 g_89 g_108 g_101 g_26 g_3 g_112 g_95.f4 g_136 g_95.f0 g_205 g_27 g_102.f0 g_194.f2 g_140 g_196.f4 g_172 g_173 g_84 g_239 g_261 g_194.f4 g_292 g_297 g_194.f0 g_298.f0 g_308.f0 g_196.f2 g_95.f2 g_306.f0 g_307.f0 g_305.f0 g_253 g_302.f0 g_292.f0 g_421 g_426 g_69.f0
 * writes: g_49 g_14 g_89 g_98 g_101 g_108 g_27 g_112 g_136 g_140 g_95.f4 g_84 g_205 g_226 g_229 g_173 g_95.f0 g_253 g_95.f2 g_43 g_422 g_161 g_461 g_297
 */
static uint16_t  func_44(int8_t  p_45)
{ /* block id: 26 */
    int64_t l_48 = 0x0E9FB3884E3EED79LL;
    int32_t l_428 = 0xAAD10D60L;
    int32_t *l_456 = &g_5;
    union U0 ** const l_458 = (void*)0;
    union U0 ** const *l_457[1];
    int32_t l_475 = 0x38A6EF68L;
    int32_t l_477 = 0x9D10375DL;
    int32_t l_481 = 0x5998A3CFL;
    int32_t l_484 = (-4L);
    int32_t l_491 = 3L;
    float *l_512 = &g_226;
    uint32_t ***l_533[10] = {&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161,&g_161};
    int64_t l_544 = 0x5C6F82B4DF5DAF9ALL;
    int32_t l_545 = 1L;
    uint32_t *l_560 = &g_14;
    uint32_t *l_562 = &g_95.f2;
    union U0 **l_575 = &g_297;
    union U0 *** const l_574 = &l_575;
    int32_t l_578 = 0x50E91ABFL;
    const int32_t l_651 = 1L;
    uint64_t l_687[3];
    uint32_t l_701 = 3UL;
    int i;
    for (i = 0; i < 1; i++)
        l_457[i] = &l_458;
    for (i = 0; i < 3; i++)
        l_687[i] = 1UL;
    if (g_43)
    { /* block id: 27 */
        int32_t *l_47[8] = {&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0],&g_12[2][0][0]};
        int32_t **l_46 = &l_47[0];
        uint32_t *l_50 = &g_14;
        int16_t *l_427 = &g_43;
        int i;
        (*l_46) = (void*)0;
        g_49 &= l_48;
        l_428 = ((p_45 & p_45) <= (((*l_50) ^= p_45) == (((*l_427) = (safe_sub_func_int32_t_s_s(l_48, func_53(l_48)))) != (l_48 ^ g_196.f2))));
    }
    else
    { /* block id: 213 */
        union U0 ** const **l_459 = &l_457[0];
        int32_t *l_460[10][9] = {{&g_5,&g_461[2],&g_5,&g_461[7],&g_5,&g_461[2],&g_5,&g_461[4],&g_5},{&g_461[4],&g_461[2],&g_461[7],&g_461[4],&g_461[4],&g_461[2],&g_461[4],&g_461[4],&g_461[7]},{&g_461[7],&g_461[7],(void*)0,&g_461[5],&g_5,&g_461[2],&g_461[2],&g_461[4],&g_461[2]},{&g_461[7],&g_5,&g_461[2],&g_461[2],&g_5,&g_461[7],&g_461[5],&g_5,&g_5},{&g_461[4],&g_461[4],(void*)0,&g_5,&g_5,&g_5,&g_5,(void*)0,&g_461[4]},{&g_5,&g_461[1],&g_461[7],&g_461[4],&g_5,&g_461[4],&g_461[5],&g_461[5],&g_461[4]},{&g_461[4],(void*)0,&g_5,(void*)0,&g_461[4],&g_461[1],&g_461[2],&g_461[7],&g_5},{&g_461[2],&g_461[1],&g_461[4],(void*)0,&g_5,(void*)0,&g_461[4],&g_461[1],&g_461[2]},{&g_461[5],&g_461[4],&g_5,&g_461[4],&g_461[7],&g_461[1],&g_5,&g_461[1],&g_461[7]},{&g_5,&g_5,&g_5,&g_5,(void*)0,&g_461[4],&g_461[4],&g_461[7],&g_5}};
        int32_t l_462 = 0xAA5391E9L;
        int32_t l_474 = (-5L);
        int32_t l_483 = 0xA6B91523L;
        uint64_t l_486 = 18446744073709551612UL;
        uint64_t l_510 = 0xB680B1F56A834ACBLL;
        uint8_t l_524 = 0x6CL;
        uint16_t l_599 = 65535UL;
        int32_t l_605 = 4L;
        int32_t l_606 = 0x25424842L;
        int32_t l_607 = 1L;
        int32_t l_608[8] = {0xF1F2BE47L,0xF1F2BE47L,0x9FC66C6BL,0xF1F2BE47L,0xF1F2BE47L,0x9FC66C6BL,0xF1F2BE47L,0xF1F2BE47L};
        uint16_t l_616 = 0x0161L;
        int64_t l_628 = 0xBE120C59CB2D1E33LL;
        uint16_t l_629 = 0xA193L;
        int i, j;
lbl_463:
        l_462 = (safe_add_func_float_f_f((((*g_26) = (safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f((*g_26), ((safe_mul_func_float_f_f((safe_div_func_float_f_f((((((safe_add_func_uint8_t_u_u(0xBEL, (g_69.f0 == (18446744073709551615UL | (safe_lshift_func_uint8_t_u_s((p_45 <= p_45), (((g_461[5] = (safe_mul_func_int8_t_s_s(((+(safe_div_func_int8_t_s_s((((((l_456 = l_456) == &g_5) ^ (((*l_459) = l_457[0]) == (void*)0)) , g_95.f0) && g_308[0].f0), g_112))) == 1UL), (-10L)))) , p_45) , p_45))))))) ^ p_45) <= p_45) , p_45) <= 0x6.09210Bp-81), p_45)), l_48)) >= p_45))), (*g_26))), 0x0.0376F3p+84)), l_48)), p_45))) <= (-0x1.Ep-1)), 0x2.4p-1));
        for (p_45 = 8; (p_45 >= 3); p_45 -= 1)
        { /* block id: 221 */
            int64_t l_471 = 1L;
            int32_t l_476 = 0x86D42293L;
            int32_t l_479 = 0L;
            int32_t l_480 = 1L;
            float *l_514 = (void*)0;
            float **l_515 = (void*)0;
            float **l_516 = &l_512;
            int16_t *l_586 = &g_49;
            union U0 ***l_598[9] = {&l_575,&l_575,&l_575,&l_575,&l_575,&l_575,&l_575,&l_575,&l_575};
            uint8_t l_603 = 9UL;
            int32_t l_609 = 0x05042896L;
            int32_t l_611 = (-10L);
            int32_t l_612 = 0x9371F043L;
            int32_t l_613 = 0xB47E1CDAL;
            int32_t l_614 = (-5L);
            int32_t l_615 = (-5L);
            int i;
            if (g_302.f0)
                goto lbl_463;
        }
        (*l_575) = (**l_574);
        for (l_607 = 0; (l_607 < (-28)); l_607 = safe_sub_func_int32_t_s_s(l_607, 5))
        { /* block id: 313 */
            int32_t *l_621 = &l_608[4];
            int32_t *l_622 = &l_477;
            int32_t *l_623 = &l_608[1];
            int32_t l_624 = (-1L);
            int32_t *l_625 = &l_608[2];
            int32_t *l_626 = &l_483;
            int32_t *l_627[5][5] = {{&l_484,&l_484,&l_484,&l_484,&l_484},{&l_428,&l_428,&l_428,&l_428,&l_428},{&l_484,&l_484,&l_484,&l_484,&l_484},{&l_428,&l_428,&l_428,&l_428,&l_428},{&l_484,&l_484,&l_484,&l_484,&l_484}};
            uint32_t *** const *l_677 = &l_533[0];
            int32_t l_704 = 1L;
            uint8_t l_706 = 0x5FL;
            int32_t *l_713 = &g_461[4];
            int i, j;
            --l_629;
        }
    }
    return g_253[6];
}


/* ------------------------------------------ */
/* 
 * reads : g_69 g_12 g_43 g_89 g_108 g_101 g_26 g_3 g_112 g_49 g_95.f4 g_136 g_95.f0 g_205 g_27 g_102.f0 g_194.f2 g_140 g_196.f4 g_172 g_173 g_84 g_239 g_261 g_194.f4 g_292 g_297 g_194.f0 g_298.f0 g_308.f0 g_196.f2 g_95.f2 g_306.f0 g_307.f0 g_305.f0 g_253 g_302.f0 g_292.f0 g_421 g_426
 * writes: g_89 g_98 g_101 g_108 g_27 g_112 g_136 g_140 g_95.f4 g_84 g_205 g_226 g_229 g_173 g_95.f0 g_253 g_95.f2 g_43 g_422 g_161
 */
static uint32_t  func_53(uint32_t  p_54)
{ /* block id: 31 */
    int32_t *l_57 = &g_12[2][0][0];
    (*g_426) = func_55(l_57);
    return p_54;
}


/* ------------------------------------------ */
/* 
 * reads : g_69 g_12 g_43 g_89 g_108 g_101 g_26 g_3 g_112 g_49 g_95.f4 g_136 g_95.f0 g_205 g_27 g_102.f0 g_194.f2 g_140 g_196.f4 g_172 g_173 g_84 g_239 g_261 g_194.f4 g_292 g_297 g_194.f0 g_298.f0 g_308.f0 g_196.f2 g_95.f2 g_306.f0 g_307.f0 g_305.f0 g_253 g_302.f0 g_292.f0 g_421
 * writes: g_89 g_98 g_101 g_108 g_27 g_112 g_136 g_140 g_95.f4 g_84 g_205 g_226 g_229 g_173 g_95.f0 g_253 g_95.f2 g_43 g_422
 */
static uint32_t ** func_55(int32_t * p_56)
{ /* block id: 32 */
    float *l_61 = &g_27;
    int32_t l_62 = 0L;
    uint16_t *l_111 = &g_112;
    uint32_t *l_115 = (void*)0;
    uint32_t *l_116[2];
    int32_t l_117 = 0xBF928973L;
    int16_t *l_118[10][10][2] = {{{(void*)0,&g_43},{&g_43,&g_43},{&g_49,&g_49},{&g_43,&g_43},{(void*)0,&g_49},{&g_43,&g_43},{&g_49,&g_43},{&g_49,&g_43},{&g_49,&g_49},{(void*)0,&g_49}},{{&g_49,&g_43},{&g_49,&g_43},{&g_49,&g_43},{&g_43,&g_49},{(void*)0,&g_43},{&g_43,&g_49},{&g_49,&g_43},{&g_43,&g_43},{(void*)0,(void*)0},{&g_49,&g_43}},{{(void*)0,&g_43},{&g_49,&g_49},{&g_43,(void*)0},{&g_43,&g_49},{&g_43,(void*)0},{&g_43,&g_43},{&g_43,&g_43},{&g_43,&g_43},{&g_43,&g_49},{&g_43,(void*)0}},{{&g_43,(void*)0},{(void*)0,&g_43},{(void*)0,&g_43},{(void*)0,&g_43},{(void*)0,(void*)0},{&g_43,(void*)0},{&g_43,&g_49},{&g_43,&g_43},{&g_43,&g_43},{&g_43,&g_49}},{{(void*)0,(void*)0},{&g_49,&g_43},{&g_43,&g_43},{&g_43,(void*)0},{&g_43,&g_49},{&g_49,&g_43},{&g_43,&g_43},{(void*)0,&g_43},{&g_43,&g_49},{(void*)0,&g_43}},{{&g_49,(void*)0},{&g_43,(void*)0},{&g_49,&g_43},{(void*)0,&g_49},{&g_43,&g_49},{&g_49,&g_49},{&g_49,&g_49},{&g_49,&g_49},{&g_43,&g_49},{(void*)0,&g_43}},{{&g_49,(void*)0},{&g_43,(void*)0},{&g_49,&g_43},{(void*)0,&g_49},{&g_43,&g_43},{(void*)0,&g_43},{&g_43,&g_43},{&g_49,&g_49},{&g_43,(void*)0},{&g_43,&g_43}},{{&g_43,&g_43},{&g_49,(void*)0},{(void*)0,&g_49},{&g_43,&g_43},{&g_43,&g_43},{&g_43,&g_49},{&g_43,(void*)0},{&g_43,(void*)0},{(void*)0,&g_43},{(void*)0,&g_43}},{{(void*)0,&g_43},{(void*)0,(void*)0},{&g_43,(void*)0},{&g_43,&g_49},{&g_43,&g_43},{&g_43,&g_43},{&g_43,&g_49},{(void*)0,(void*)0},{&g_49,&g_43},{&g_43,&g_43}},{{&g_43,(void*)0},{&g_43,&g_49},{&g_49,&g_43},{&g_43,&g_43},{(void*)0,&g_43},{&g_43,&g_49},{(void*)0,&g_43},{&g_49,(void*)0},{&g_43,(void*)0},{&g_49,&g_43}}};
    int32_t l_119 = 0xC7E41B64L;
    int32_t l_120 = 8L;
    union U1 *l_134 = &g_95;
    uint8_t l_186 = 1UL;
    int32_t l_187[7][3] = {{0x7AACED72L,0x2C819DA8L,0x7AACED72L},{0x7AACED72L,0x2C819DA8L,0x7AACED72L},{0x7AACED72L,0x2C819DA8L,0x7AACED72L},{0x7AACED72L,0x2C819DA8L,0x7AACED72L},{0x7AACED72L,0x2C819DA8L,0x7AACED72L},{0x7AACED72L,0x2C819DA8L,0x7AACED72L},{0x7AACED72L,0x2C819DA8L,0x7AACED72L}};
    const union U1 *l_193 = &g_194;
    const union U1 *l_195[3];
    uint64_t *l_204 = &g_205;
    int32_t *l_216 = &l_187[0][1];
    uint64_t l_224 = 18446744073709551607UL;
    float *l_225 = &g_226;
    float *l_227 = (void*)0;
    float *l_228 = &g_229;
    uint64_t l_230 = 0x69E2DA4D15D3D01ELL;
    int8_t *l_231[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int64_t l_238 = 0x4A84839B6CC735A5LL;
    int8_t l_293 = (-4L);
    uint64_t l_310 = 0x8CA9857A78FE824BLL;
    union U0 **l_356 = &g_297;
    uint64_t l_374 = 18446744073709551606UL;
    float l_395[8][1][8] = {{{0x0.6p-1,0x4.1p-1,0x1.D2784Ap-71,0x1.D2784Ap-71,0x4.1p-1,0x0.6p-1,(-0x5.8p+1),0x4.1p-1}},{{0x9.6220E6p-12,(-0x5.8p+1),0x1.D2784Ap-71,0x9.6220E6p-12,0x1.D2784Ap-71,(-0x5.8p+1),0x9.6220E6p-12,0x0.6p-1}},{{0x4.1p-1,(-0x5.8p+1),0x9.6220E6p-12,0x0.6p-1,0x0.6p-1,0x9.6220E6p-12,(-0x5.8p+1),0x1.D2784Ap-71}},{{0x1.Ep+1,0x0.6p-1,0x7.56A15Ep+51,0x1.8p-1,0x0.6p-1,0x1.8p-1,0x7.56A15Ep+51,0x0.6p-1}},{{0x1.D2784Ap-71,0x7.56A15Ep+51,0x1.Ep+1,0x1.D2784Ap-71,0x1.8p-1,0x1.8p-1,0x1.D2784Ap-71,0x1.Ep+1}},{{0x0.6p-1,0x0.6p-1,0x9.6220E6p-12,(-0x5.8p+1),0x1.D2784Ap-71,0x9.6220E6p-12,0x1.D2784Ap-71,(-0x5.8p+1)}},{{0x1.Ep+1,(-0x5.8p+1),0x1.Ep+1,0x1.8p-1,(-0x5.8p+1),0x7.56A15Ep+51,0x7.56A15Ep+51,(-0x5.8p+1)}},{{(-0x5.8p+1),0x7.56A15Ep+51,0x7.56A15Ep+51,(-0x5.8p+1),0x1.8p-1,0x1.Ep+1,(-0x5.8p+1),0x1.Ep+1}}};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_116[i] = &g_95.f2;
    for (i = 0; i < 3; i++)
        l_195[i] = &g_196;
    if ((!(safe_sub_func_int16_t_s_s((l_61 != p_56), ((l_62 & (((safe_sub_func_int8_t_s_s(func_65(p_56), (((*l_111) |= l_62) == (((p_56 == (void*)0) ^ ((((safe_rshift_func_int16_t_s_s((l_120 = (l_119 = (((l_117 = g_49) < g_95.f4) || l_62))), 0)) , g_43) >= l_62) & 1L)) == 18446744073709551615UL)))) , g_112) & l_62)) != l_62)))))
    { /* block id: 53 */
        int32_t *l_123 = &l_119;
        uint16_t *l_135[5];
        int8_t *l_139 = &g_140;
        int32_t *l_171[4][3][8] = {{{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117}},{{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117}},{{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117}},{{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117},{&l_62,&l_117,&l_62,&l_117,&l_62,&l_117,&l_62,&l_117}}};
        int32_t **l_170 = &l_171[3][2][3];
        int32_t l_174 = 1L;
        uint32_t * const *l_181 = &l_116[0];
        const uint32_t *l_185 = &g_89;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_135[i] = &g_136;
        g_95.f4 = (safe_sub_func_int32_t_s_s(((*l_123) = (*p_56)), (safe_div_func_uint64_t_u_u(1UL, (safe_mul_func_int8_t_s_s(g_112, (g_89 != ((safe_add_func_int64_t_s_s((safe_mod_func_uint32_t_u_u((((*l_139) = (((-9L) | ((l_134 != l_134) != (&l_120 == ((g_136++) , &g_12[2][0][0])))) ^ (-10L))) && g_95.f0), g_112)), l_62)) , 1L))))))));
        (*l_123) = (l_123 != p_56);
        for (l_119 = 0; (l_119 == 11); l_119 = safe_add_func_int16_t_s_s(l_119, 5))
        { /* block id: 61 */
            int32_t *l_143 = &l_120;
            uint32_t **l_158 = &l_116[1];
        }
        (**l_170) = (*l_123);
    }
    else
    { /* block id: 86 */
        int32_t l_190[4] = {0L,0L,0L,0L};
        const union U1 *l_192 = (void*)0;
        const union U1 **l_191[2];
        int32_t *l_197 = &l_62;
        int i;
        for (i = 0; i < 2; i++)
            l_191[i] = &l_192;
        (*l_197) = (safe_div_func_int8_t_s_s((&g_95 == (l_190[2] , (l_195[1] = (l_193 = l_134)))), 0xA6L));
        for (l_117 = 0; (l_117 <= 2); l_117 += 1)
        { /* block id: 92 */
            return &g_162;
        }
        g_101 = (void*)0;
    }
    if (((safe_add_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u(g_95.f4, ((2L && (g_140 ^= ((safe_mul_func_int8_t_s_s(((--(*l_204)) , (safe_lshift_func_uint8_t_u_s((((l_62 | ((*l_204) = ((((*g_26) = ((*g_26) >= (safe_sub_func_float_f_f(((*l_228) = (((*l_225) = ((safe_div_func_float_f_f(0xC.DCF2E0p+64, ((-(((((safe_unary_minus_func_int32_t_s(((*l_216) = (l_119 &= (*p_56))))) == (~((safe_div_func_uint32_t_u_u(0xF9CF0709L, 0x4E212550L)) && (safe_mul_func_uint16_t_u_u((safe_add_func_int32_t_s_s(0L, (*p_56))), g_102.f0))))) , (*g_26)) <= l_224) == (*g_26))) < (*g_26)))) <= 0x1.Ep-1)) != (*g_26))), 0x1.24B164p+3)))) , 0x29L) && (*l_216)))) || 0x9962E55FL) != g_194.f2), 1))), 255UL)) <= l_230))) , 0xA6EBB11826830DA4LL))), (*p_56))) < g_196.f4))
    { /* block id: 105 */
        int64_t l_237 = 0x0965D61E199A9337LL;
        int32_t l_242 = 0xA7BFF7CCL;
        int32_t l_268 = 5L;
        int32_t l_269[5][6] = {{0x2F3BBD96L,1L,0x6F827C98L,0xE14E27A1L,0x6F827C98L,1L},{0x6F827C98L,0x2F3BBD96L,(-10L),1L,1L,(-10L)},{0x6F827C98L,0x6F827C98L,1L,0xE14E27A1L,0x52AFE84FL,0xE14E27A1L},{0x2F3BBD96L,0x6F827C98L,0x2F3BBD96L,(-10L),1L,1L},{1L,0x2F3BBD96L,0x2F3BBD96L,1L,0x6F827C98L,0xE14E27A1L}};
        int32_t l_270 = 0L;
        int32_t l_271 = 0x3A7C03D4L;
        int32_t l_272 = 9L;
        int32_t l_273[7];
        union U0 *l_301 = &g_302;
        union U0 ***l_387 = &l_356;
        uint32_t l_396[2];
        int i, j;
        for (i = 0; i < 7; i++)
            l_273[i] = 0xDC0335DBL;
        for (i = 0; i < 2; i++)
            l_396[i] = 18446744073709551615UL;
        if ((**g_172))
        { /* block id: 106 */
            for (l_186 = 0; (l_186 <= 58); l_186++)
            { /* block id: 109 */
                uint32_t **l_236 = &l_116[0];
                for (l_117 = 0; (l_117 <= (-4)); l_117--)
                { /* block id: 112 */
                    return &g_162;
                }
                (*g_172) = (*g_172);
                (*g_172) = (*g_172);
                if ((*p_56))
                    continue;
            }
        }
        else
        { /* block id: 119 */
            l_238 = l_237;
            (*g_239) = &g_102;
        }
        for (l_230 = (-25); (l_230 > 2); ++l_230)
        { /* block id: 125 */
            uint8_t *l_247 = &l_186;
            int64_t *l_251 = &g_95.f0;
            int64_t *l_252[8];
            int32_t l_254 = 5L;
            int32_t l_262 = 8L;
            int32_t l_263 = 0xE80497F3L;
            const float l_264 = (-0x9.Cp+1);
            int32_t l_274 = 0x6343D952L;
            int32_t l_275 = (-1L);
            int8_t l_276 = (-1L);
            int32_t l_277 = (-1L);
            uint8_t l_278 = 0x30L;
            union U0 *l_304[4][3] = {{&g_306,&g_307,&g_306},{&g_308[0],&g_305,(void*)0},{&g_308[0],&g_308[0],&g_305},{&g_306,&g_305,&g_305}};
            uint64_t l_362[6][8][2] = {{{0x2B24482857D9F95CLL,0xB52A1DFCF9E425B9LL},{0x5D0BC74A590BD286LL,0xB52A1DFCF9E425B9LL},{0x43ED56C7AF7ED305LL,0x27224D034F18C485LL},{0xFFCBA9512BAA61B4LL,1UL},{0x2B24482857D9F95CLL,0x9BA9D7E2D96D4037LL},{0x2B24482857D9F95CLL,1UL},{0xFFCBA9512BAA61B4LL,0x27224D034F18C485LL},{0x43ED56C7AF7ED305LL,0xB52A1DFCF9E425B9LL}},{{0x5D0BC74A590BD286LL,0xB52A1DFCF9E425B9LL},{0x43ED56C7AF7ED305LL,0x27224D034F18C485LL},{0xFFCBA9512BAA61B4LL,1UL},{0x2B24482857D9F95CLL,0x9BA9D7E2D96D4037LL},{0x2B24482857D9F95CLL,1UL},{0xFFCBA9512BAA61B4LL,0x27224D034F18C485LL},{0x43ED56C7AF7ED305LL,0xB52A1DFCF9E425B9LL},{0x5D0BC74A590BD286LL,0xB52A1DFCF9E425B9LL}},{{0x43ED56C7AF7ED305LL,0x27224D034F18C485LL},{0xFFCBA9512BAA61B4LL,1UL},{0x2B24482857D9F95CLL,0x9BA9D7E2D96D4037LL},{0x2B24482857D9F95CLL,1UL},{0xFFCBA9512BAA61B4LL,0x27224D034F18C485LL},{0x43ED56C7AF7ED305LL,0xB52A1DFCF9E425B9LL},{0x5D0BC74A590BD286LL,0xB52A1DFCF9E425B9LL},{0x43ED56C7AF7ED305LL,0x27224D034F18C485LL}},{{0xFFCBA9512BAA61B4LL,1UL},{0x2B24482857D9F95CLL,0x9BA9D7E2D96D4037LL},{0x2B24482857D9F95CLL,1UL},{0xFFCBA9512BAA61B4LL,0x27224D034F18C485LL},{0x43ED56C7AF7ED305LL,0xB52A1DFCF9E425B9LL},{0x5D0BC74A590BD286LL,0xB52A1DFCF9E425B9LL},{0x43ED56C7AF7ED305LL,0x27224D034F18C485LL},{0xFFCBA9512BAA61B4LL,1UL}},{{0x2B24482857D9F95CLL,0x9BA9D7E2D96D4037LL},{0x2B24482857D9F95CLL,1UL},{0xFFCBA9512BAA61B4LL,0x27224D034F18C485LL},{0x43ED56C7AF7ED305LL,0xB52A1DFCF9E425B9LL},{0x5D0BC74A590BD286LL,0xB52A1DFCF9E425B9LL},{0x43ED56C7AF7ED305LL,0x27224D034F18C485LL},{0xFFCBA9512BAA61B4LL,1UL},{0x2B24482857D9F95CLL,0x9BA9D7E2D96D4037LL}},{{0x2B24482857D9F95CLL,1UL},{0xFFCBA9512BAA61B4LL,0x27224D034F18C485LL},{0x43ED56C7AF7ED305LL,0xB52A1DFCF9E425B9LL},{0x5D0BC74A590BD286LL,0xB52A1DFCF9E425B9LL},{0x43ED56C7AF7ED305LL,0x27224D034F18C485LL},{0xFFCBA9512BAA61B4LL,1UL},{0x2B24482857D9F95CLL,0x9BA9D7E2D96D4037LL},{0x2B24482857D9F95CLL,1UL}}};
            uint16_t l_371 = 65527UL;
            int8_t l_383 = 0x42L;
            int i, j, k;
            for (i = 0; i < 8; i++)
                l_252[i] = &l_237;
            l_242 = l_237;
            if ((g_194.f2 | (safe_mod_func_int16_t_s_s((-5L), (g_43 = ((*l_216) = ((*l_216) & ((safe_div_func_int16_t_s_s((((((*l_247) = (*l_216)) < (safe_sub_func_uint8_t_u_u(((g_253[2] = ((*l_251) = (+0x3161F875L))) || ((func_77(l_254, (l_263 = ((safe_lshift_func_uint16_t_u_u(((*l_216) && (safe_sub_func_uint64_t_u_u(((g_136 = (++(*l_111))) || ((*l_111) = ((((l_262 = (l_242 = (g_95.f2 = (g_261 , ((*l_216) > g_194.f4))))) >= l_237) | 0xC29E382DL) & 0x116EL))), l_254))), l_237)) || 18446744073709551606UL))) , (**g_172)) >= l_237)), 0x47L))) , 65533UL) | (*l_216)), 0xA1BCL)) & 1UL))))))))
            { /* block id: 139 */
                int32_t *l_265 = &l_120;
                int32_t *l_266 = &l_242;
                int32_t *l_267[5][4] = {{&l_120,(void*)0,&l_120,(void*)0},{&l_120,(void*)0,&l_120,(void*)0},{&l_120,(void*)0,&l_120,(void*)0},{&l_120,(void*)0,&l_120,(void*)0},{&l_120,(void*)0,&l_120,(void*)0}};
                union U0 *l_299 = &g_300;
                const int8_t l_309 = 0xE0L;
                float l_344[8][9][1];
                int i, j, k;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 9; j++)
                    {
                        for (k = 0; k < 1; k++)
                            l_344[i][j][k] = 0x5.A60534p-37;
                    }
                }
                l_278--;
                for (g_43 = (-16); (g_43 >= 20); ++g_43)
                { /* block id: 143 */
                    int32_t **l_296 = &l_267[2][0];
                    union U0 **l_303 = (void*)0;
                    int32_t l_343 = 0xA25FE5F7L;
                    union U0 **l_354 = (void*)0;
                    union U0 ***l_355 = &l_303;
                    union U0 ***l_357 = &l_356;
                    union U0 **l_359 = (void*)0;
                    union U0 ***l_358 = &l_359;
                    int32_t l_368 = 0x6292D70AL;
                    int32_t l_369 = 1L;
                    int32_t l_370 = (-2L);
                    int8_t l_393 = (-1L);
                    int16_t *l_416[7][7] = {{&g_43,&g_43,&g_43,(void*)0,&g_49,&g_49,(void*)0},{&g_49,&g_43,&g_49,&g_43,&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43,(void*)0,&g_49,&g_49,(void*)0},{&g_49,&g_43,&g_49,&g_43,&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43,(void*)0,&g_49,&g_49,(void*)0},{&g_49,&g_43,&g_49,&g_43,&g_43,&g_43,&g_43},{&g_43,&g_43,&g_43,(void*)0,&g_49,&g_49,(void*)0}};
                    int i, j;
                    if (((safe_add_func_uint8_t_u_u(((((safe_lshift_func_uint16_t_u_u((g_194.f4 | ((safe_lshift_func_uint16_t_u_s((((safe_sub_func_int8_t_s_s((safe_unary_minus_func_uint32_t_u((l_293 = (g_12[2][0][0] ^ (g_292 , (*l_216)))))), g_205)) <= ((safe_lshift_func_uint8_t_u_s(((((((*g_172) = (*g_172)) != ((*l_296) = p_56)) != (g_253[4] = (((l_299 = g_297) != (l_304[1][0] = l_301)) == g_12[2][0][0]))) >= g_194.f0) , 0UL), l_273[6])) , 0x86L)) , g_12[2][0][0]), 1)) && 3UL)), 3)) , g_3) > g_298.f0) <= g_308[0].f0), l_309)) & (*l_216)))
                    { /* block id: 150 */
                        if (l_310)
                            break;
                    }
                    else
                    { /* block id: 152 */
                        float l_316 = 0x1.F6875Fp-55;
                        const int32_t l_321 = (-7L);
                        int32_t l_345[1][2][6] = {{{0x166EDB2BL,0xC1866E46L,0xC1866E46L,0x166EDB2BL,0xC1866E46L,0xC1866E46L},{0x166EDB2BL,0xC1866E46L,0xC1866E46L,0x166EDB2BL,0xC1866E46L,0xC1866E46L}}};
                        int i, j, k;
                        (*l_228) = (((((*l_216) = ((*l_251) = (*l_216))) & ((safe_add_func_uint64_t_u_u((*l_265), ((*l_204) = (((l_242 | ((6UL < ((~(safe_rshift_func_int8_t_s_s(0x2BL, 4))) >= (l_262 = (g_3 , (((-1L) == (l_262 , 0x1D72FDCEL)) == (*p_56)))))) > l_276)) > g_196.f2) >= l_254)))) , (*l_265))) , p_56) == p_56);
                        (*l_265) = ((l_269[2][4] , &g_173[0][2]) == (void*)0);
                        l_345[0][0][1] |= (((++g_89) >= (g_95.f2--)) | ((l_321 & l_276) == ((safe_sub_func_uint64_t_u_u((*l_265), ((safe_lshift_func_int16_t_s_s(1L, (((safe_lshift_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(((g_253[1] = (safe_add_func_uint32_t_u_u((((safe_unary_minus_func_int64_t_s((l_268 = (((safe_lshift_func_uint8_t_u_s((1L == (safe_lshift_func_uint16_t_u_u(g_194.f4, 5))), ((safe_div_func_uint64_t_u_u((g_306.f0 > (0xF862143EL < (safe_div_func_uint32_t_u_u(1UL, l_343)))), l_277)) && 1L))) & (*l_216)) == (*p_56))))) , 65530UL) != l_269[0][0]), (*p_56)))) == l_273[6]), l_269[2][4])), l_321)), 4)) < l_254) >= 0xF23B7BDE48DDDB46LL))) == l_321))) < g_49)));
                    }
                    if ((safe_lshift_func_uint16_t_u_s(1UL, (((*l_216) = (((safe_div_func_int16_t_s_s((safe_add_func_int8_t_s_s(0x3EL, (((g_307.f0 == l_242) | (((((*l_216) , (g_95.f2 = (safe_mod_func_uint64_t_u_u((((*l_355) = l_354) == ((*l_358) = ((*l_357) = l_356))), (safe_add_func_uint64_t_u_u((l_263 = ((*l_204) ^= ((func_77((g_49 >= 0xE32DL), (*l_216)) , g_108) >= g_305.f0))), l_254)))))) ^ l_274) != l_362[5][0][1]) , 2UL)) == (*p_56)))), l_278)) > 0xF6BB8693L) <= g_140)) || l_271))))
                    { /* block id: 172 */
                        int32_t l_365 = (-1L);
                        int32_t l_366 = 0L;
                        int32_t l_367 = 0xD7324796L;
                        if ((**g_172))
                            break;
                        (*l_216) = (safe_add_func_int64_t_s_s((l_254 |= (g_253[1] |= 0x5303D7F307CFA46ELL)), 18446744073709551615UL));
                        l_371--;
                        --l_374;
                    }
                    else
                    { /* block id: 179 */
                        int8_t l_386[5][9][4] = {{{0x1DL,0xCFL,0xCFL,0x1DL},{0xF0L,0xCFL,1L,0xCFL},{0xCFL,(-1L),1L,1L},{0xF0L,0xF0L,0xCFL,(-1L)},{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L},{0xCFL,0x1DL,(-1L),0x1DL},{0x1DL,0xF0L,(-1L),(-1L)},{0xCFL,0xCFL,0x1DL,(-1L)}},{{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L},{0xCFL,0x1DL,(-1L),0x1DL},{0x1DL,0xF0L,(-1L),(-1L)},{0xCFL,0xCFL,0x1DL,(-1L)},{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L},{0xCFL,0x1DL,(-1L),0x1DL},{0x1DL,0xF0L,(-1L),(-1L)}},{{0xCFL,0xCFL,0x1DL,(-1L)},{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L},{0xCFL,0x1DL,(-1L),0x1DL},{0x1DL,0xF0L,(-1L),(-1L)},{0xCFL,0xCFL,0x1DL,(-1L)},{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L},{0xCFL,0x1DL,(-1L),0x1DL}},{{0x1DL,0xF0L,(-1L),(-1L)},{0xCFL,0xCFL,0x1DL,(-1L)},{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L},{0xCFL,0x1DL,(-1L),0x1DL},{0x1DL,0xF0L,(-1L),(-1L)},{0xCFL,0xCFL,0x1DL,(-1L)},{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L}},{{0xCFL,0x1DL,(-1L),0x1DL},{0x1DL,0xF0L,(-1L),(-1L)},{0xCFL,0xCFL,0x1DL,(-1L)},{1L,0xF0L,1L,0x1DL},{1L,0x1DL,0x1DL,1L},{0xCFL,0x1DL,(-1L),0x1DL},{0x1DL,0xF0L,(-1L),(-1L)},{0xCFL,0xCFL,0x1DL,(-1L)},{1L,0xF0L,1L,0x1DL}}};
                        union U0 ***l_388[3];
                        int32_t l_389 = 0x866EB6EAL;
                        int32_t l_390 = 3L;
                        int32_t l_391 = 0x56878A80L;
                        int32_t l_392 = (-1L);
                        int32_t l_394 = 0xDDFC5413L;
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                            l_388[i] = &l_354;
                        (*l_216) = (safe_mul_func_int16_t_s_s((((safe_rshift_func_uint8_t_u_u(((*l_265) != l_263), 6)) == (safe_lshift_func_uint16_t_u_s(((g_308[0].f0 | g_302.f0) , (l_383 | (*p_56))), (((safe_lshift_func_int16_t_s_s(l_386[2][0][2], 11)) , (g_292.f0 , ((l_383 , l_387) == l_388[0]))) >= 0x6D85FC0F2815A318LL)))) != l_269[2][4]), l_268));
                        ++l_396[0];
                    }
                    for (g_140 = 0; (g_140 <= 14); g_140++)
                    { /* block id: 185 */
                        uint16_t l_417 = 65535UL;
                        (*l_266) = (safe_mul_func_uint16_t_u_u(((safe_add_func_uint32_t_u_u((!0x96L), (((safe_div_func_uint8_t_u_u(((g_95.f4 <= 0x7F9EL) , (((safe_rshift_func_uint8_t_u_u((safe_mod_func_int16_t_s_s(g_305.f0, (*l_265))), 3)) == ((safe_sub_func_int16_t_s_s(((((void*)0 == l_416[3][1]) && l_274) != (((*p_56) || (*p_56)) == l_417)), 65535UL)) <= g_253[2])) , 0xE5L)), 0x26L)) , (*l_265)) < (*l_266)))) , l_371), (*l_266)));
                    }
                    if ((*p_56))
                        continue;
                }
            }
            else
            { /* block id: 190 */
                (*l_216) = ((((*l_216) & ((*l_251) = ((-1L) == g_292.f0))) != (!g_307.f0)) != l_383);
            }
        }
        for (l_237 = 0; (l_237 >= (-10)); l_237 = safe_sub_func_uint32_t_u_u(l_237, 4))
        { /* block id: 197 */
            (*g_172) = &l_62;
        }
    }
    else
    { /* block id: 200 */
        (*g_421) = l_195[0];
    }
    for (l_117 = 0; (l_117 < (-14)); --l_117)
    { /* block id: 205 */
        uint32_t **l_425[5] = {&l_115,&l_115,&l_115,&l_115,&l_115};
        int i;
        return &g_162;
    }
    return &g_162;
}


/* ------------------------------------------ */
/* 
 * reads : g_69 g_12 g_43 g_89 g_108 g_101 g_26 g_3
 * writes: g_89 g_98 g_101 g_108 g_27
 */
static int8_t  func_65(int32_t * p_66)
{ /* block id: 33 */
    uint32_t l_70 = 4294967295UL;
    int32_t l_71 = 1L;
    int32_t l_74 = 0xE5B73647L;
    l_71 = ((safe_rshift_func_int8_t_s_u((g_69 , g_12[0][4][0]), l_70)) && l_70);
    l_74 &= (safe_rshift_func_uint16_t_u_u((l_71 = 65535UL), 3));
    (*g_26) = ((l_71 && l_74) , (safe_mul_func_float_f_f(l_70, (func_77(l_70, ((l_71 && g_43) , func_80(l_70, l_70))) , 0x4.Dp+1))));
    return g_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_108 g_101 g_69
 * writes: g_98 g_101 g_108
 */
static union U0  func_77(int64_t  p_78, int16_t  p_79)
{ /* block id: 41 */
    union U1 *l_94[5][5] = {{&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,(void*)0,&g_95,&g_95},{&g_95,&g_95,&g_95,(void*)0,&g_95}};
    union U1 **l_96[10][3] = {{&l_94[2][2],&l_94[2][2],&l_94[2][2]},{&l_94[1][2],&l_94[1][2],&l_94[1][2]},{&l_94[2][2],&l_94[2][2],&l_94[2][2]},{&l_94[1][2],&l_94[1][2],&l_94[1][2]},{&l_94[2][2],&l_94[2][2],&l_94[2][2]},{&l_94[1][2],&l_94[1][2],&l_94[1][2]},{&l_94[2][2],&l_94[2][2],&l_94[2][2]},{&l_94[1][2],&l_94[1][2],&l_94[1][2]},{&l_94[2][2],&l_94[2][2],&l_94[2][2]},{&l_94[1][2],&l_94[1][2],&l_94[1][2]}};
    union U1 *l_97 = &g_95;
    volatile union U0 *l_100[10][5] = {{&g_69,&g_69,&g_69,(void*)0,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69},{&g_69,&g_69,(void*)0,&g_69,&g_69},{(void*)0,&g_69,(void*)0,&g_69,&g_69},{&g_69,&g_69,&g_69,&g_69,&g_69},{(void*)0,&g_69,&g_69,&g_69,&g_69}};
    volatile union U0 **l_99[2];
    int32_t *l_103 = &g_84;
    int32_t l_104 = 9L;
    int32_t *l_105 = &l_104;
    int32_t *l_106 = (void*)0;
    int32_t *l_107[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int i, j;
    for (i = 0; i < 2; i++)
        l_99[i] = &l_100[2][1];
    g_98 = (l_97 = l_94[2][2]);
    g_101 = &g_69;
    g_108++;
    return (*g_101);
}


/* ------------------------------------------ */
/* 
 * reads : g_89
 * writes: g_89
 */
static int16_t  func_80(const uint8_t  p_81, uint16_t  p_82)
{ /* block id: 37 */
    int32_t *l_83 = &g_84;
    int32_t *l_85 = (void*)0;
    int32_t l_86 = 0x74181134L;
    int32_t *l_87 = &l_86;
    int32_t *l_88[3][1][7] = {{{&l_86,&l_86,&l_86,&l_86,&l_86,&l_86,&l_86}},{{&g_84,&g_84,&g_84,&g_84,&g_84,&g_84,&g_84}},{{&l_86,&l_86,&l_86,&l_86,&l_86,&l_86,&l_86}}};
    int32_t **l_92 = (void*)0;
    int32_t **l_93 = &l_87;
    int i, j, k;
    g_89--;
    (*l_93) = &l_86;
    return g_89;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_12[i][j][k], "g_12[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc_bytes (&g_27, sizeof(g_27), "g_27", print_hash_value);
    transparent_crc(g_43, "g_43", print_hash_value);
    transparent_crc(g_49, "g_49", print_hash_value);
    transparent_crc(g_69.f0, "g_69.f0", print_hash_value);
    transparent_crc(g_84, "g_84", print_hash_value);
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_95.f0, "g_95.f0", print_hash_value);
    transparent_crc_bytes (&g_95.f1, sizeof(g_95.f1), "g_95.f1", print_hash_value);
    transparent_crc(g_95.f2, "g_95.f2", print_hash_value);
    transparent_crc_bytes (&g_95.f3, sizeof(g_95.f3), "g_95.f3", print_hash_value);
    transparent_crc(g_95.f4, "g_95.f4", print_hash_value);
    transparent_crc(g_102.f0, "g_102.f0", print_hash_value);
    transparent_crc(g_108, "g_108", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    transparent_crc(g_136, "g_136", print_hash_value);
    transparent_crc(g_140, "g_140", print_hash_value);
    transparent_crc(g_194.f0, "g_194.f0", print_hash_value);
    transparent_crc_bytes (&g_194.f1, sizeof(g_194.f1), "g_194.f1", print_hash_value);
    transparent_crc(g_194.f2, "g_194.f2", print_hash_value);
    transparent_crc_bytes (&g_194.f3, sizeof(g_194.f3), "g_194.f3", print_hash_value);
    transparent_crc(g_194.f4, "g_194.f4", print_hash_value);
    transparent_crc(g_196.f0, "g_196.f0", print_hash_value);
    transparent_crc_bytes (&g_196.f1, sizeof(g_196.f1), "g_196.f1", print_hash_value);
    transparent_crc(g_196.f2, "g_196.f2", print_hash_value);
    transparent_crc_bytes (&g_196.f3, sizeof(g_196.f3), "g_196.f3", print_hash_value);
    transparent_crc(g_196.f4, "g_196.f4", print_hash_value);
    transparent_crc(g_205, "g_205", print_hash_value);
    transparent_crc_bytes (&g_226, sizeof(g_226), "g_226", print_hash_value);
    transparent_crc_bytes (&g_229, sizeof(g_229), "g_229", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_253[i], "g_253[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_261.f0, "g_261.f0", print_hash_value);
    transparent_crc(g_292.f0, "g_292.f0", print_hash_value);
    transparent_crc(g_298.f0, "g_298.f0", print_hash_value);
    transparent_crc(g_300.f0, "g_300.f0", print_hash_value);
    transparent_crc(g_302.f0, "g_302.f0", print_hash_value);
    transparent_crc(g_305.f0, "g_305.f0", print_hash_value);
    transparent_crc(g_306.f0, "g_306.f0", print_hash_value);
    transparent_crc(g_307.f0, "g_307.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_308[i].f0, "g_308[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_461[i], "g_461[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_505, "g_505", print_hash_value);
    transparent_crc(g_513, "g_513", print_hash_value);
    transparent_crc_bytes (&g_543, sizeof(g_543), "g_543", print_hash_value);
    transparent_crc(g_561.f0, "g_561.f0", print_hash_value);
    transparent_crc_bytes (&g_561.f1, sizeof(g_561.f1), "g_561.f1", print_hash_value);
    transparent_crc(g_561.f2, "g_561.f2", print_hash_value);
    transparent_crc_bytes (&g_561.f3, sizeof(g_561.f3), "g_561.f3", print_hash_value);
    transparent_crc(g_561.f4, "g_561.f4", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_573[i].f0, "g_573[i].f0", print_hash_value);
        transparent_crc_bytes(&g_573[i].f1, sizeof(g_573[i].f1), "g_573[i].f1", print_hash_value);
        transparent_crc(g_573[i].f2, "g_573[i].f2", print_hash_value);
        transparent_crc_bytes(&g_573[i].f3, sizeof(g_573[i].f3), "g_573[i].f3", print_hash_value);
        transparent_crc(g_573[i].f4, "g_573[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_736, "g_736", print_hash_value);
    transparent_crc(g_841, "g_841", print_hash_value);
    transparent_crc(g_849.f0, "g_849.f0", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_941[i].f0, "g_941[i].f0", print_hash_value);
        transparent_crc_bytes(&g_941[i].f1, sizeof(g_941[i].f1), "g_941[i].f1", print_hash_value);
        transparent_crc(g_941[i].f2, "g_941[i].f2", print_hash_value);
        transparent_crc_bytes(&g_941[i].f3, sizeof(g_941[i].f3), "g_941[i].f3", print_hash_value);
        transparent_crc(g_941[i].f4, "g_941[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_969, "g_969", print_hash_value);
    transparent_crc(g_977.f0, "g_977.f0", print_hash_value);
    transparent_crc_bytes (&g_977.f1, sizeof(g_977.f1), "g_977.f1", print_hash_value);
    transparent_crc(g_977.f2, "g_977.f2", print_hash_value);
    transparent_crc_bytes (&g_977.f3, sizeof(g_977.f3), "g_977.f3", print_hash_value);
    transparent_crc(g_977.f4, "g_977.f4", print_hash_value);
    transparent_crc(g_1100.f0, "g_1100.f0", print_hash_value);
    transparent_crc_bytes (&g_1100.f1, sizeof(g_1100.f1), "g_1100.f1", print_hash_value);
    transparent_crc(g_1100.f2, "g_1100.f2", print_hash_value);
    transparent_crc_bytes (&g_1100.f3, sizeof(g_1100.f3), "g_1100.f3", print_hash_value);
    transparent_crc(g_1100.f4, "g_1100.f4", print_hash_value);
    transparent_crc(g_1138, "g_1138", print_hash_value);
    transparent_crc(g_1189, "g_1189", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1228[i][j][k], "g_1228[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1248[i][j][k], "g_1248[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1273[i][j][k].f0, "g_1273[i][j][k].f0", print_hash_value);
                transparent_crc_bytes(&g_1273[i][j][k].f1, sizeof(g_1273[i][j][k].f1), "g_1273[i][j][k].f1", print_hash_value);
                transparent_crc(g_1273[i][j][k].f2, "g_1273[i][j][k].f2", print_hash_value);
                transparent_crc_bytes(&g_1273[i][j][k].f3, sizeof(g_1273[i][j][k].f3), "g_1273[i][j][k].f3", print_hash_value);
                transparent_crc(g_1273[i][j][k].f4, "g_1273[i][j][k].f4", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1299.f0, "g_1299.f0", print_hash_value);
    transparent_crc_bytes (&g_1299.f1, sizeof(g_1299.f1), "g_1299.f1", print_hash_value);
    transparent_crc(g_1299.f2, "g_1299.f2", print_hash_value);
    transparent_crc_bytes (&g_1299.f3, sizeof(g_1299.f3), "g_1299.f3", print_hash_value);
    transparent_crc(g_1299.f4, "g_1299.f4", print_hash_value);
    transparent_crc(g_1310, "g_1310", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1352[i][j][k].f0, "g_1352[i][j][k].f0", print_hash_value);
                transparent_crc_bytes(&g_1352[i][j][k].f1, sizeof(g_1352[i][j][k].f1), "g_1352[i][j][k].f1", print_hash_value);
                transparent_crc(g_1352[i][j][k].f2, "g_1352[i][j][k].f2", print_hash_value);
                transparent_crc_bytes(&g_1352[i][j][k].f3, sizeof(g_1352[i][j][k].f3), "g_1352[i][j][k].f3", print_hash_value);
                transparent_crc(g_1352[i][j][k].f4, "g_1352[i][j][k].f4", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1362[i], "g_1362[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1375, "g_1375", print_hash_value);
    transparent_crc(g_1399, "g_1399", print_hash_value);
    transparent_crc(g_1442.f0, "g_1442.f0", print_hash_value);
    transparent_crc_bytes (&g_1442.f1, sizeof(g_1442.f1), "g_1442.f1", print_hash_value);
    transparent_crc(g_1442.f2, "g_1442.f2", print_hash_value);
    transparent_crc_bytes (&g_1442.f3, sizeof(g_1442.f3), "g_1442.f3", print_hash_value);
    transparent_crc(g_1442.f4, "g_1442.f4", print_hash_value);
    transparent_crc(g_1495.f0, "g_1495.f0", print_hash_value);
    transparent_crc_bytes (&g_1495.f1, sizeof(g_1495.f1), "g_1495.f1", print_hash_value);
    transparent_crc(g_1495.f2, "g_1495.f2", print_hash_value);
    transparent_crc_bytes (&g_1495.f3, sizeof(g_1495.f3), "g_1495.f3", print_hash_value);
    transparent_crc(g_1495.f4, "g_1495.f4", print_hash_value);
    transparent_crc(g_1558.f0, "g_1558.f0", print_hash_value);
    transparent_crc_bytes (&g_1558.f1, sizeof(g_1558.f1), "g_1558.f1", print_hash_value);
    transparent_crc(g_1558.f2, "g_1558.f2", print_hash_value);
    transparent_crc_bytes (&g_1558.f3, sizeof(g_1558.f3), "g_1558.f3", print_hash_value);
    transparent_crc(g_1558.f4, "g_1558.f4", print_hash_value);
    transparent_crc(g_1573.f0, "g_1573.f0", print_hash_value);
    transparent_crc(g_1582.f0, "g_1582.f0", print_hash_value);
    transparent_crc_bytes (&g_1582.f1, sizeof(g_1582.f1), "g_1582.f1", print_hash_value);
    transparent_crc(g_1582.f2, "g_1582.f2", print_hash_value);
    transparent_crc_bytes (&g_1582.f3, sizeof(g_1582.f3), "g_1582.f3", print_hash_value);
    transparent_crc(g_1582.f4, "g_1582.f4", print_hash_value);
    transparent_crc(g_1586, "g_1586", print_hash_value);
    transparent_crc(g_1599.f0, "g_1599.f0", print_hash_value);
    transparent_crc_bytes (&g_1599.f1, sizeof(g_1599.f1), "g_1599.f1", print_hash_value);
    transparent_crc(g_1599.f2, "g_1599.f2", print_hash_value);
    transparent_crc_bytes (&g_1599.f3, sizeof(g_1599.f3), "g_1599.f3", print_hash_value);
    transparent_crc(g_1599.f4, "g_1599.f4", print_hash_value);
    transparent_crc(g_1628.f0, "g_1628.f0", print_hash_value);
    transparent_crc_bytes (&g_1628.f1, sizeof(g_1628.f1), "g_1628.f1", print_hash_value);
    transparent_crc(g_1628.f2, "g_1628.f2", print_hash_value);
    transparent_crc_bytes (&g_1628.f3, sizeof(g_1628.f3), "g_1628.f3", print_hash_value);
    transparent_crc(g_1628.f4, "g_1628.f4", print_hash_value);
    transparent_crc(g_1662, "g_1662", print_hash_value);
    transparent_crc(g_1725.f0, "g_1725.f0", print_hash_value);
    transparent_crc_bytes (&g_1725.f1, sizeof(g_1725.f1), "g_1725.f1", print_hash_value);
    transparent_crc(g_1725.f2, "g_1725.f2", print_hash_value);
    transparent_crc_bytes (&g_1725.f3, sizeof(g_1725.f3), "g_1725.f3", print_hash_value);
    transparent_crc(g_1725.f4, "g_1725.f4", print_hash_value);
    transparent_crc(g_1817, "g_1817", print_hash_value);
    transparent_crc(g_1890.f0, "g_1890.f0", print_hash_value);
    transparent_crc(g_2045.f0, "g_2045.f0", print_hash_value);
    transparent_crc(g_2110.f0, "g_2110.f0", print_hash_value);
    transparent_crc_bytes (&g_2110.f1, sizeof(g_2110.f1), "g_2110.f1", print_hash_value);
    transparent_crc(g_2110.f2, "g_2110.f2", print_hash_value);
    transparent_crc_bytes (&g_2110.f3, sizeof(g_2110.f3), "g_2110.f3", print_hash_value);
    transparent_crc(g_2110.f4, "g_2110.f4", print_hash_value);
    transparent_crc(g_2175, "g_2175", print_hash_value);
    transparent_crc(g_2188, "g_2188", print_hash_value);
    transparent_crc(g_2267.f0, "g_2267.f0", print_hash_value);
    transparent_crc_bytes (&g_2267.f1, sizeof(g_2267.f1), "g_2267.f1", print_hash_value);
    transparent_crc(g_2267.f2, "g_2267.f2", print_hash_value);
    transparent_crc_bytes (&g_2267.f3, sizeof(g_2267.f3), "g_2267.f3", print_hash_value);
    transparent_crc(g_2267.f4, "g_2267.f4", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_2468[i][j].f0, "g_2468[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2553.f0, "g_2553.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 644
XXX total union variables: 22

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 84
breakdown:
   indirect level: 0, occurrence: 22
   indirect level: 1, occurrence: 26
   indirect level: 2, occurrence: 14
   indirect level: 3, occurrence: 11
   indirect level: 4, occurrence: 7
   indirect level: 5, occurrence: 4
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 66
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 26
XXX times a single bitfield on LHS: 1
XXX times a single bitfield on RHS: 94

XXX max expression depth: 37
breakdown:
   depth: 1, occurrence: 234
   depth: 2, occurrence: 56
   depth: 3, occurrence: 4
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 6, occurrence: 2
   depth: 7, occurrence: 1
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 2
   depth: 17, occurrence: 4
   depth: 18, occurrence: 3
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 21, occurrence: 2
   depth: 22, occurrence: 1
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 4
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 2
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 34, occurrence: 1
   depth: 36, occurrence: 2
   depth: 37, occurrence: 2

XXX total number of pointers: 595

XXX times a variable address is taken: 1526
XXX times a pointer is dereferenced on RHS: 301
breakdown:
   depth: 1, occurrence: 255
   depth: 2, occurrence: 40
   depth: 3, occurrence: 5
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 345
breakdown:
   depth: 1, occurrence: 326
   depth: 2, occurrence: 17
   depth: 3, occurrence: 2
XXX times a pointer is compared with null: 46
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 8574

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1357
   level: 2, occurrence: 286
   level: 3, occurrence: 124
   level: 4, occurrence: 19
   level: 5, occurrence: 36
XXX number of pointers point to pointers: 233
XXX number of pointers point to scalars: 330
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 30.6
XXX average alias set size: 1.53

XXX times a non-volatile is read: 1968
XXX times a non-volatile is write: 997
XXX times a volatile is read: 136
XXX    times read thru a pointer: 28
XXX times a volatile is write: 40
XXX    times written thru a pointer: 15
XXX times a volatile is available for access: 6.86e+03
XXX percentage of non-volatile access: 94.4

XXX forward jumps: 0
XXX backward jumps: 10

XXX stmts: 228
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 33
   depth: 2, occurrence: 26
   depth: 3, occurrence: 31
   depth: 4, occurrence: 43
   depth: 5, occurrence: 64

XXX percentage a fresh-made variable is used: 16.4
XXX percentage an existing variable is used: 83.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1975403946
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   signed f0 : 3;
   volatile signed f1 : 12;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int16_t g_15 = 4L;
static volatile struct S0 g_32[7] = {{-0,63},{0,-16},{-0,63},{-0,63},{0,-16},{-0,63},{-0,63}};
static int32_t g_37[1] = {0xC2A41B87L};
static uint64_t g_48 = 0UL;
static int64_t g_54 = 0xA59E478DFCE24956LL;
static struct S0 g_55 = {-1,44};/* VOLATILE GLOBAL g_55 */
static struct S0 g_57 = {-1,1};/* VOLATILE GLOBAL g_57 */
static int64_t g_79 = 0xC2AE6BA1441E233CLL;
static const struct S0 * volatile * const g_82 = (void*)0;
static uint32_t g_92 = 2UL;
static const struct S0 *g_94 = &g_55;
static const struct S0 * volatile *g_93 = &g_94;
static struct S0 g_123 = {-1,-11};/* VOLATILE GLOBAL g_123 */
static int32_t g_129[1] = {0x4CBC4FE3L};
static float g_134 = 0x0.Fp-1;
static int32_t *g_166[5] = {&g_129[0],&g_129[0],&g_129[0],&g_129[0],&g_129[0]};
static int32_t ** volatile g_165 = &g_166[4];/* VOLATILE GLOBAL g_165 */
static uint64_t g_170 = 18446744073709551615UL;
static float g_233[6][5] = {{0x6.0A1730p+6,0x1.Dp+1,0x1.Dp+1,0x6.0A1730p+6,0x1.Dp+1},{0x6.0A1730p+6,0x6.0A1730p+6,0x1.8p-1,0x6.0A1730p+6,0x6.0A1730p+6},{0x1.Dp+1,0x6.0A1730p+6,0x1.Dp+1,0x1.Dp+1,0x6.0A1730p+6},{0x6.0A1730p+6,0x1.Dp+1,0x1.Dp+1,0x6.0A1730p+6,0x1.Dp+1},{0x6.0A1730p+6,0x6.0A1730p+6,0x1.8p-1,0x6.0A1730p+6,0x6.0A1730p+6},{0x1.Dp+1,0x6.0A1730p+6,0x1.Dp+1,0x1.Dp+1,0x6.0A1730p+6}};
static int32_t *g_237 = &g_37[0];
static int32_t ** volatile g_236 = &g_237;/* VOLATILE GLOBAL g_236 */
static uint8_t g_245 = 0xC8L;
static int16_t g_251 = (-1L);
static int8_t g_258[2] = {(-1L),(-1L)};
static int16_t g_261 = 0x6AFCL;
static uint8_t g_263 = 253UL;
static uint64_t g_303 = 0UL;
static uint16_t g_374 = 7UL;
static uint16_t *g_373 = &g_374;
static int32_t *g_377 = &g_37[0];
static volatile struct S0 g_396 = {-0,27};/* VOLATILE GLOBAL g_396 */
static uint16_t g_401[2] = {5UL,5UL};
static int32_t g_427 = (-1L);
static uint32_t g_432 = 0UL;
static float g_453[8][2] = {{0xC.E898F2p-36,0xC.E898F2p-36},{0xC.E898F2p-36,0xC.E898F2p-36},{0xC.E898F2p-36,0xC.E898F2p-36},{0xC.E898F2p-36,0xC.E898F2p-36},{0xC.E898F2p-36,0xC.E898F2p-36},{0xC.E898F2p-36,0xC.E898F2p-36},{0xC.E898F2p-36,0xC.E898F2p-36},{0xC.E898F2p-36,0xC.E898F2p-36}};
static uint32_t g_493 = 0xB553B662L;
static uint64_t g_511 = 18446744073709551615UL;
static int32_t ** volatile g_540 = (void*)0;/* VOLATILE GLOBAL g_540 */
static int8_t *g_578 = (void*)0;
static int8_t **g_577 = &g_578;
static int8_t ***g_580 = &g_577;
static int8_t **** volatile g_579 = &g_580;/* VOLATILE GLOBAL g_579 */
static int16_t g_621 = 0xB3EBL;
static const float g_636 = 0x5.5p+1;
static float * volatile g_666[9][1] = {{&g_453[0][1]},{(void*)0},{&g_453[0][1]},{(void*)0},{&g_453[0][1]},{(void*)0},{&g_453[0][1]},{(void*)0},{&g_453[0][1]}};
static float * volatile g_667[4][8][4] = {{{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]}},{{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]}},{{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]}},{{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]},{&g_453[5][1],&g_453[2][0],&g_453[5][1],&g_453[2][0]}}};
static float * volatile g_668 = (void*)0;/* VOLATILE GLOBAL g_668 */
static float * volatile g_669 = &g_233[4][0];/* VOLATILE GLOBAL g_669 */
static int32_t ** volatile g_672 = &g_377;/* VOLATILE GLOBAL g_672 */
static uint32_t g_686[1][7] = {{0x0D9DA1E1L,0x0D9DA1E1L,0x0D9DA1E1L,0x0D9DA1E1L,0x0D9DA1E1L,0x0D9DA1E1L,0x0D9DA1E1L}};
static struct S0 g_691 = {0,11};/* VOLATILE GLOBAL g_691 */
static struct S0 g_693 = {1,-51};/* VOLATILE GLOBAL g_693 */
static struct S0 g_694 = {-0,-22};/* VOLATILE GLOBAL g_694 */
static struct S0 *g_732[9] = {&g_123,&g_123,&g_123,&g_123,&g_123,&g_123,&g_123,&g_123,&g_123};
static struct S0 * const *g_731 = &g_732[0];
static struct S0 g_737 = {-0,-40};/* VOLATILE GLOBAL g_737 */
static int32_t g_743 = 0x5238F60CL;
static volatile struct S0 g_769[4] = {{-1,3},{-1,3},{-1,3},{-1,3}};
static volatile struct S0 g_770[2] = {{-0,59},{-0,59}};
static struct S0 g_787 = {1,3};/* VOLATILE GLOBAL g_787 */
static int32_t ** volatile g_797 = &g_377;/* VOLATILE GLOBAL g_797 */
static volatile struct S0 g_798 = {1,-27};/* VOLATILE GLOBAL g_798 */
static volatile int16_t g_873 = (-1L);/* VOLATILE GLOBAL g_873 */
static volatile int16_t *g_872 = &g_873;
static volatile int16_t ** volatile g_871[7] = {&g_872,&g_872,&g_872,&g_872,&g_872,&g_872,&g_872};
static float g_876[1] = {(-0x1.Ep+1)};
static int16_t *g_924 = (void*)0;
static int16_t **g_923 = &g_924;
static const int32_t g_934[10][10] = {{0x860EC279L,9L,0x860EC279L,0x860EC279L,9L,0x860EC279L,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL},{0x860EC279L,0x860EC279L,9L,0x860EC279L,0x860EC279L,9L,0x860EC279L,0x860EC279L,9L,0x860EC279L},{0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L},{0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL},{0x860EC279L,0x860EC279L,9L,0x860EC279L,0x860EC279L,9L,0x860EC279L,0x860EC279L,9L,0x860EC279L},{0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L},{0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL},{0x860EC279L,0x860EC279L,9L,0x860EC279L,0x860EC279L,9L,0x860EC279L,0x860EC279L,9L,0x860EC279L},{0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L},{0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL,0x860EC279L,0xBFD5F88CL,0xBFD5F88CL}};
static int32_t * volatile g_938[2][5] = {{&g_37[0],&g_37[0],&g_427,&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_427,&g_37[0],&g_37[0]}};
static int32_t * const  volatile g_939 = &g_427;/* VOLATILE GLOBAL g_939 */
static const volatile struct S0 g_959 = {1,-41};/* VOLATILE GLOBAL g_959 */
static int32_t * volatile g_987[8][3][2] = {{{&g_427,(void*)0},{(void*)0,&g_427},{&g_427,&g_427}},{{(void*)0,(void*)0},{&g_427,(void*)0},{(void*)0,&g_427}},{{&g_427,&g_427},{(void*)0,(void*)0},{&g_427,(void*)0}},{{(void*)0,&g_427},{&g_427,&g_427},{(void*)0,(void*)0}},{{&g_427,(void*)0},{(void*)0,&g_427},{&g_427,&g_427}},{{(void*)0,(void*)0},{&g_427,(void*)0},{(void*)0,&g_427}},{{&g_427,&g_427},{(void*)0,(void*)0},{&g_427,(void*)0}},{{(void*)0,&g_427},{&g_427,&g_427},{(void*)0,(void*)0}}};
static int32_t ** volatile g_1063[9][2] = {{&g_377,&g_377},{&g_377,&g_377},{&g_237,&g_237},{&g_377,&g_237},{&g_237,&g_377},{&g_377,&g_377},{&g_377,&g_377},{&g_237,&g_237},{&g_377,&g_237}};
static int8_t g_1074 = 3L;
static int32_t ** volatile g_1076[2] = {&g_377,&g_377};
static int32_t ** volatile g_1077 = (void*)0;/* VOLATILE GLOBAL g_1077 */
static int32_t ** volatile g_1105 = (void*)0;/* VOLATILE GLOBAL g_1105 */
static struct S0 g_1124 = {1,8};/* VOLATILE GLOBAL g_1124 */
static uint64_t *g_1134 = &g_48;
static uint64_t **g_1133 = &g_1134;
static int32_t ** volatile g_1158 = &g_377;/* VOLATILE GLOBAL g_1158 */
static uint16_t g_1164 = 0x4B3BL;
static volatile uint8_t g_1185 = 248UL;/* VOLATILE GLOBAL g_1185 */
static volatile uint8_t *g_1184[7][8][4] = {{{(void*)0,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,(void*)0,&g_1185},{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0}},{{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0}},{{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185}},{{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185}},{{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185}},{{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,(void*)0,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185}},{{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,(void*)0},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185},{&g_1185,&g_1185,&g_1185,&g_1185}}};
static int32_t ** volatile g_1188 = &g_237;/* VOLATILE GLOBAL g_1188 */
static int32_t g_1245 = 1L;
static int32_t * volatile *g_1258 = &g_237;
static int32_t * volatile ** volatile g_1257[6] = {&g_1258,&g_1258,&g_1258,&g_1258,&g_1258,&g_1258};
static int32_t * volatile ** volatile g_1259 = &g_1258;/* VOLATILE GLOBAL g_1259 */
static struct S0 g_1295 = {-1,-56};/* VOLATILE GLOBAL g_1295 */
static const int32_t *g_1298 = &g_427;
static const struct S0 g_1318 = {0,-54};/* VOLATILE GLOBAL g_1318 */
static volatile struct S0 * volatile * volatile **g_1340 = (void*)0;
static volatile int8_t g_1370 = (-5L);/* VOLATILE GLOBAL g_1370 */
static volatile int8_t g_1371[3][1][1] = {{{0x50L}},{{0x50L}},{{0x50L}}};
static volatile int8_t g_1372 = 3L;/* VOLATILE GLOBAL g_1372 */
static volatile int8_t g_1373 = 0L;/* VOLATILE GLOBAL g_1373 */
static volatile int8_t g_1374 = 0L;/* VOLATILE GLOBAL g_1374 */
static volatile int8_t g_1375 = (-1L);/* VOLATILE GLOBAL g_1375 */
static volatile int8_t g_1376 = (-3L);/* VOLATILE GLOBAL g_1376 */
static volatile int8_t g_1377 = 1L;/* VOLATILE GLOBAL g_1377 */
static volatile int8_t g_1378 = 0x5AL;/* VOLATILE GLOBAL g_1378 */
static volatile int8_t g_1379[6] = {(-8L),(-8L),(-8L),(-8L),(-8L),(-8L)};
static volatile int8_t g_1380 = 0x1EL;/* VOLATILE GLOBAL g_1380 */
static volatile int8_t g_1381 = 0x3BL;/* VOLATILE GLOBAL g_1381 */
static volatile int8_t g_1382 = 7L;/* VOLATILE GLOBAL g_1382 */
static volatile int8_t g_1383 = (-1L);/* VOLATILE GLOBAL g_1383 */
static volatile int8_t g_1384[3] = {0x02L,0x02L,0x02L};
static volatile int8_t g_1385 = 0x56L;/* VOLATILE GLOBAL g_1385 */
static volatile int8_t g_1386 = 0xFEL;/* VOLATILE GLOBAL g_1386 */
static volatile int8_t g_1387[5] = {2L,2L,2L,2L,2L};
static volatile int8_t g_1388 = 0x55L;/* VOLATILE GLOBAL g_1388 */
static volatile int8_t g_1389 = 0x77L;/* VOLATILE GLOBAL g_1389 */
static volatile int8_t g_1390 = 0x56L;/* VOLATILE GLOBAL g_1390 */
static volatile int8_t g_1391 = 0L;/* VOLATILE GLOBAL g_1391 */
static volatile int8_t g_1392 = (-9L);/* VOLATILE GLOBAL g_1392 */
static volatile int8_t g_1393 = (-1L);/* VOLATILE GLOBAL g_1393 */
static volatile int8_t g_1394[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static volatile int8_t g_1395 = 0x63L;/* VOLATILE GLOBAL g_1395 */
static volatile int8_t g_1396 = 0x0AL;/* VOLATILE GLOBAL g_1396 */
static volatile int8_t g_1397 = 0xF9L;/* VOLATILE GLOBAL g_1397 */
static volatile int8_t g_1398 = 1L;/* VOLATILE GLOBAL g_1398 */
static volatile int8_t g_1399 = 0x6AL;/* VOLATILE GLOBAL g_1399 */
static volatile int8_t g_1400 = (-2L);/* VOLATILE GLOBAL g_1400 */
static volatile int8_t g_1401[6][7] = {{(-1L),(-6L),0x29L,(-6L),(-1L),(-1L),(-6L)},{(-1L),0xD9L,(-1L),0L,0L,(-1L),0xD9L},{(-6L),(-5L),0x29L,0x29L,(-5L),(-6L),(-5L)},{(-1L),0L,0L,(-1L),0xD9L,(-1L),0L},{(-1L),(-1L),(-6L),0x29L,(-6L),(-1L),(-1L)},{7L,0L,0x81L,0L,7L,7L,0L}};
static volatile int8_t g_1402 = 0x0AL;/* VOLATILE GLOBAL g_1402 */
static volatile int8_t g_1403 = 0xD0L;/* VOLATILE GLOBAL g_1403 */
static volatile int8_t * volatile g_1369[8][9][3] = {{{&g_1373,(void*)0,&g_1373},{(void*)0,&g_1370,(void*)0},{(void*)0,(void*)0,&g_1401[4][2]},{&g_1403,&g_1394[6],&g_1380},{&g_1391,&g_1391,&g_1385},{&g_1403,(void*)0,&g_1377},{(void*)0,&g_1385,&g_1393},{(void*)0,&g_1382,&g_1400},{&g_1373,(void*)0,&g_1393}},{{(void*)0,&g_1378,&g_1377},{&g_1401[4][2],(void*)0,&g_1385},{&g_1380,(void*)0,&g_1380},{&g_1385,(void*)0,&g_1401[4][2]},{&g_1377,&g_1378,(void*)0},{&g_1393,(void*)0,&g_1373},{&g_1400,&g_1382,(void*)0},{&g_1393,&g_1385,(void*)0},{&g_1377,(void*)0,&g_1403}},{{&g_1385,&g_1391,&g_1391},{&g_1380,&g_1394[6],&g_1403},{&g_1401[4][2],(void*)0,(void*)0},{(void*)0,&g_1370,(void*)0},{&g_1373,(void*)0,&g_1373},{(void*)0,&g_1370,(void*)0},{(void*)0,(void*)0,&g_1401[4][2]},{&g_1403,&g_1394[6],&g_1380},{&g_1391,&g_1391,&g_1385}},{{&g_1403,(void*)0,&g_1377},{(void*)0,&g_1385,&g_1393},{(void*)0,&g_1382,&g_1400},{&g_1373,(void*)0,&g_1393},{(void*)0,&g_1378,&g_1377},{&g_1401[4][2],(void*)0,&g_1385},{&g_1380,(void*)0,(void*)0},{&g_1391,&g_1385,&g_1373},{&g_1384[0],&g_1386,&g_1380}},{{&g_1401[4][2],(void*)0,(void*)0},{&g_1372,&g_1388,&g_1400},{&g_1401[4][2],&g_1391,(void*)0},{&g_1384[0],&g_1382,(void*)0},{&g_1391,&g_1397,&g_1397},{(void*)0,(void*)0,(void*)0},{&g_1373,&g_1393,(void*)0},{&g_1380,(void*)0,&g_1400},{(void*)0,&g_1381,(void*)0}},{{&g_1400,(void*)0,&g_1380},{(void*)0,&g_1393,&g_1373},{(void*)0,(void*)0,(void*)0},{&g_1397,&g_1397,&g_1391},{(void*)0,&g_1382,&g_1384[0]},{(void*)0,&g_1391,&g_1401[4][2]},{&g_1400,&g_1388,&g_1372},{(void*)0,(void*)0,&g_1401[4][2]},{&g_1380,&g_1386,&g_1384[0]}},{{&g_1373,&g_1385,&g_1391},{(void*)0,&g_1374,(void*)0},{&g_1391,&g_1385,&g_1373},{&g_1384[0],&g_1386,&g_1380},{&g_1401[4][2],(void*)0,(void*)0},{&g_1372,&g_1388,&g_1400},{&g_1401[4][2],&g_1391,(void*)0},{&g_1384[0],&g_1382,(void*)0},{&g_1391,&g_1397,&g_1397}},{{(void*)0,(void*)0,(void*)0},{&g_1373,&g_1393,(void*)0},{&g_1380,(void*)0,&g_1400},{(void*)0,&g_1381,(void*)0},{&g_1400,(void*)0,&g_1380},{(void*)0,&g_1393,&g_1373},{(void*)0,(void*)0,(void*)0},{&g_1397,&g_1397,&g_1391},{(void*)0,&g_1382,&g_1384[0]}}};
static volatile int8_t * volatile * volatile g_1368 = &g_1369[6][7][0];/* VOLATILE GLOBAL g_1368 */
static volatile int8_t g_1406 = (-8L);/* VOLATILE GLOBAL g_1406 */
static volatile int8_t *g_1405 = &g_1406;
static volatile int8_t * volatile *g_1404 = &g_1405;
static volatile int8_t * volatile * volatile *g_1367[8][1][10] = {{{&g_1404,(void*)0,&g_1368,&g_1404,(void*)0,(void*)0,(void*)0,(void*)0,&g_1404,&g_1368}},{{&g_1404,&g_1404,&g_1368,&g_1404,(void*)0,&g_1404,(void*)0,&g_1368,&g_1404,(void*)0}},{{&g_1404,(void*)0,&g_1368,&g_1404,(void*)0,(void*)0,(void*)0,(void*)0,&g_1404,&g_1368}},{{&g_1404,&g_1404,&g_1368,&g_1404,(void*)0,&g_1404,(void*)0,&g_1368,&g_1404,(void*)0}},{{&g_1404,(void*)0,&g_1368,&g_1404,(void*)0,(void*)0,(void*)0,(void*)0,&g_1404,&g_1368}},{{&g_1404,&g_1404,&g_1368,&g_1404,(void*)0,&g_1404,(void*)0,&g_1368,&g_1404,(void*)0}},{{&g_1404,(void*)0,&g_1368,&g_1404,(void*)0,(void*)0,(void*)0,(void*)0,&g_1404,&g_1368}},{{&g_1404,&g_1404,&g_1368,&g_1404,(void*)0,&g_1404,&g_1368,&g_1404,&g_1404,&g_1404}}};
static volatile int8_t * volatile * volatile * volatile * const g_1366[2] = {&g_1367[0][0][7],&g_1367[0][0][7]};
static volatile int8_t * volatile * volatile * volatile * const *g_1365 = &g_1366[1];
static const struct S0 ** volatile g_1408 = (void*)0;/* VOLATILE GLOBAL g_1408 */
static float * volatile g_1540 = (void*)0;/* VOLATILE GLOBAL g_1540 */
static float * volatile g_1541 = &g_453[3][1];/* VOLATILE GLOBAL g_1541 */
static struct S0 g_1554 = {-0,-12};/* VOLATILE GLOBAL g_1554 */
static int8_t * volatile * volatile g_1564 = &g_578;/* VOLATILE GLOBAL g_1564 */
static volatile uint64_t g_1594 = 0x0A068810C8759300LL;/* VOLATILE GLOBAL g_1594 */
static const volatile int32_t g_1595 = 0x2541990DL;/* VOLATILE GLOBAL g_1595 */
static uint8_t *** volatile g_1670 = (void*)0;/* VOLATILE GLOBAL g_1670 */
static struct S0 g_1718 = {0,-18};/* VOLATILE GLOBAL g_1718 */
static int32_t *g_1799[1] = {(void*)0};
static const struct S0 g_1818[4][6] = {{{0,21},{-1,-57},{-1,12},{1,32},{-1,12},{-1,-57}},{{0,21},{-1,-57},{-1,12},{1,32},{-1,12},{-1,-57}},{{0,21},{-1,-57},{-1,12},{1,32},{-1,12},{-1,-57}},{{0,21},{-1,-57},{-1,12},{1,32},{-1,12},{-1,-57}}};
static volatile struct S0 g_1851 = {1,25};/* VOLATILE GLOBAL g_1851 */
static uint64_t g_1921 = 18446744073709551615UL;
static int64_t g_1949 = 0x14C5879FA34512DALL;
static int32_t **g_1968[1] = {&g_1799[0]};
static int32_t ***g_1967 = &g_1968[0];
static struct S0 g_1993 = {-1,43};/* VOLATILE GLOBAL g_1993 */
static struct S0 g_1994[1] = {{-1,-41}};
static int16_t g_2036 = 9L;
static uint32_t *g_2072 = &g_432;


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static int16_t  func_7(int32_t  p_8);
static struct S0  func_9(float  p_10, uint64_t  p_11, uint8_t  p_12, uint32_t  p_13, const uint32_t  p_14);
static int64_t  func_16(float  p_17);
static int32_t * func_18(const uint8_t  p_19, uint32_t  p_20);
static int32_t  func_21(int64_t  p_22);
static int16_t  func_26(int32_t * p_27, int8_t  p_28, int16_t  p_29);
static int32_t * func_30(int32_t * const  p_31);
static uint64_t  func_33(int32_t * p_34, uint64_t  p_35);
static struct S0  func_38(int32_t  p_39, int16_t  p_40, int32_t * p_41, int16_t  p_42, int8_t  p_43);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_2 = 1UL;
    uint32_t *l_1563 = &g_493;
    int8_t * const *l_1869 = &g_578;
    int8_t * const **l_1868 = &l_1869;
    int8_t * const ***l_1867 = &l_1868;
    int8_t * const ****l_1866 = &l_1867;
    int32_t l_1878 = (-8L);
    int32_t l_1901 = 0xA4D08029L;
    int32_t l_1903[6] = {(-2L),(-2L),0x47546218L,(-2L),(-2L),0x47546218L};
    int32_t l_1905 = 0x5F3BC1C8L;
    uint8_t l_1906 = 5UL;
    uint16_t l_2029 = 0xEBA9L;
    int i;
    l_2--;
    return l_1903[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_1564 g_373 g_374 g_580 g_577 g_578 g_129 g_79 g_1258 g_237 g_37 g_493 g_511 g_797 g_377 g_15 g_82 g_57.f1 g_55.f1 g_48 g_54 g_123 g_165 g_170 g_731 g_732 g_1164 g_987 g_1851 g_672
 * writes: g_374 g_92 g_79 g_37 g_493 g_511 g_1397 g_770 g_48 g_57.f1 g_93 g_54 g_134 g_237 g_251 g_1164 g_577 g_987
 */
static int16_t  func_7(int32_t  p_8)
{ /* block id: 675 */
    int8_t **** const l_1568 = &g_580;
    int32_t l_1569 = 0x47DDDAA8L;
    int16_t l_1580[4] = {1L,1L,1L,1L};
    uint32_t *l_1581 = (void*)0;
    uint32_t *l_1582 = &g_92;
    uint64_t *l_1590 = &g_48;
    int8_t l_1596 = 0L;
    int32_t l_1604 = 0x69FBB20AL;
    int32_t l_1607 = 0xDED7D927L;
    int32_t l_1611 = 3L;
    int16_t l_1612 = 0x6125L;
    int32_t l_1613 = 0xB5A40554L;
    int32_t l_1614 = 0x8C11D743L;
    int32_t l_1615 = 0xA351F623L;
    int32_t l_1616 = (-10L);
    int32_t l_1617[8] = {0x33ACFEE7L,0x33ACFEE7L,0x33ACFEE7L,0x33ACFEE7L,0x33ACFEE7L,0x33ACFEE7L,0x33ACFEE7L,0x33ACFEE7L};
    uint8_t *l_1669 = &g_245;
    uint8_t **l_1668 = &l_1669;
    uint16_t **l_1766 = (void*)0;
    uint16_t ***l_1765 = &l_1766;
    struct S0 ***l_1805 = (void*)0;
    struct S0 ****l_1804 = &l_1805;
    int i;
    if (((p_8 , g_1564) != ((+0x96L) , (((((((*l_1582) = ((safe_add_func_uint16_t_u_u(((void*)0 != l_1568), (l_1569 & (((--(*g_373)) ^ p_8) , ((***l_1568) == ((safe_div_func_uint64_t_u_u(((safe_div_func_int8_t_s_s(((!(((~(safe_mul_func_uint16_t_u_u(p_8, l_1569))) & p_8) <= l_1580[1])) != g_129[0]), p_8)) || 0x98L), p_8)) , (void*)0)))))) , p_8)) | (-1L)) , l_1580[0]) != p_8) <= 0x9747L) , (**l_1568)))))
    { /* block id: 678 */
        int32_t l_1589 = 1L;
        int32_t l_1602 = (-7L);
        int32_t l_1603 = 0x6926C213L;
        int32_t l_1605[3][10][5] = {{{(-9L),(-8L),(-8L),(-9L),2L},{0x244A393AL,(-1L),(-1L),(-3L),(-3L)},{0x0E95AD58L,(-3L),(-1L),0x2E13D8D2L,0x1561B408L},{0x3C84165BL,0xFC25F058L,(-1L),(-3L),(-1L)},{(-8L),0xCC800963L,0x0E95AD58L,(-9L),0x1AE40F12L},{0x5EBFE9B1L,(-1L),0xC25816AEL,0x3C84165BL,0x3C84165BL},{0x5EBFE9B1L,0x9A8D10CCL,0x5EBFE9B1L,(-1L),0xFF376602L},{(-8L),0x5EBFE9B1L,0x2E13D8D2L,0xCC800963L,0x63C08C6EL},{0x3C84165BL,(-1L),0xFF376602L,(-1L),0xDCE4FC2DL},{0x0E95AD58L,0xC25816AEL,0x2E13D8D2L,0x63C08C6EL,7L}},{{0x244A393AL,0xFF376602L,0x5EBFE9B1L,(-1L),0xC25816AEL},{(-9L),0x1AE40F12L,0xC25816AEL,0x244A393AL,0xC25816AEL},{(-1L),(-1L),0x0E95AD58L,0L,7L},{(-3L),0L,(-1L),(-8L),0xDCE4FC2DL},{0xFC25F058L,5L,(-1L),0xFF376602L,0x63C08C6EL},{(-1L),0L,(-1L),(-1L),0xFF376602L},{0x9AAE9C71L,(-1L),(-8L),0x1561B408L,0x3C84165BL},{(-1L),0x1AE40F12L,0x1561B408L,0x1561B408L,0x1AE40F12L},{0xDCE4FC2DL,0xFF376602L,5L,(-1L),(-1L)},{(-3L),0xC25816AEL,(-9L),0xFF376602L,0x1561B408L}},{{7L,(-1L),0x6F822455L,(-8L),(-3L)},{(-3L),0x5EBFE9B1L,0x244A393AL,0L,2L},{0xDCE4FC2DL,0x9A8D10CCL,0x1AE40F12L,0x244A393AL,(-1L)},{(-1L),(-1L),0x1AE40F12L,(-1L),(-1L)},{0x9AAE9C71L,0xCC800963L,0x244A393AL,0x63C08C6EL,(-1L)},{(-1L),0xFC25F058L,0x6F822455L,(-1L),0x0E95AD58L},{0xFC25F058L,(-3L),(-9L),0xCC800963L,(-1L)},{(-3L),(-1L),5L,(-1L),0x244A393AL},{0x63C08C6EL,0x3C84165BL,0x9AAE9C71L,0L,0xFF376602L},{0xC25816AEL,0x3C84165BL,0x3C84165BL,0xC25816AEL,(-1L)}}};
        uint32_t *l_1651 = &g_686[0][0];
        uint8_t **l_1672 = &l_1669;
        uint32_t l_1695 = 0UL;
        int i, j, k;
        for (g_79 = 28; (g_79 > 8); g_79 = safe_sub_func_uint64_t_u_u(g_79, 6))
        { /* block id: 681 */
            uint64_t *l_1591 = &g_48;
            int32_t l_1608 = 0xC0E3455DL;
            int32_t l_1609 = 0xE38B046FL;
            int32_t l_1610[10][1][10] = {{{1L,0x2C2DE85BL,4L,0xF1345BD8L,0x20470BF4L,(-7L),0x3B706977L,0x412B4339L,4L,0x412B4339L}},{{0x20470BF4L,0xAA2BE1BEL,(-6L),0xF1345BD8L,(-6L),0xAA2BE1BEL,0x20470BF4L,0x271803A8L,0xD3199A9FL,0xF1345BD8L}},{{0xD3199A9FL,0x412B4339L,(-6L),0x8BCE64C5L,1L,0x271803A8L,0L,0x412B4339L,0L,0x271803A8L}},{{0x3B706977L,0x412B4339L,4L,0x412B4339L,0x3B706977L,(-7L),0x20470BF4L,0xF1345BD8L,4L,0x2C2DE85BL}},{{0x3B706977L,0xAA2BE1BEL,0L,0x2C2DE85BL,(-6L),0x271803A8L,0x3B706977L,0x271803A8L,(-6L),0x2C2DE85BL}},{{0xD3199A9FL,0x2C2DE85BL,0xD3199A9FL,0x8BCE64C5L,0x3B706977L,0xAA2BE1BEL,0L,0x2C2DE85BL,(-6L),0x271803A8L}},{{0x20470BF4L,0xF1345BD8L,4L,0x2C2DE85BL,1L,(-7L),1L,0x2C2DE85BL,4L,0xF1345BD8L}},{{1L,0xAA2BE1BEL,0xD3199A9FL,0x412B4339L,(-6L),0x8BCE64C5L,1L,0x271803A8L,0L,0x412B4339L}},{{0xD3199A9FL,0xF1345BD8L,0L,0x8BCE64C5L,0x20470BF4L,0x8BCE64C5L,0L,0xF1345BD8L,0xD3199A9FL,0x271803A8L}},{{1L,0x2C2DE85BL,4L,0xF1345BD8L,0x20470BF4L,(-7L),0x3B706977L,0x412B4339L,4L,0x412B4339L}}};
            uint32_t l_1619 = 0x960145AFL;
            int32_t l_1650 = 0xD676C869L;
            uint8_t ***l_1671[6] = {&l_1668,&l_1668,&l_1668,&l_1668,&l_1668,&l_1668};
            int i, j, k;
        }
        (**g_1258) &= (safe_mul_func_uint8_t_u_u(l_1613, (l_1596 , p_8)));
    }
    else
    { /* block id: 737 */
        uint16_t l_1717[5] = {0x17ECL,0x17ECL,0x17ECL,0x17ECL,0x17ECL};
        const int8_t *l_1740 = &g_258[1];
        const int8_t **l_1739 = &l_1740;
        const int8_t *** const l_1738 = &l_1739;
        uint16_t ***l_1764 = (void*)0;
        int32_t *l_1768[3][10] = {{&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611},{&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611},{&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611,&l_1611}};
        uint16_t l_1814 = 0x3E4DL;
        struct S0 *l_1848 = (void*)0;
        int i, j;
        if ((p_8 > p_8))
        { /* block id: 738 */
            return p_8;
        }
        else
        { /* block id: 740 */
            int16_t l_1730 = 0xE09EL;
            uint16_t **l_1763[8] = {&g_373,&g_373,&g_373,&g_373,&g_373,&g_373,&g_373,&g_373};
            uint16_t ***l_1762[8];
            int8_t l_1767 = 1L;
            float l_1798 = 0x6.Fp+1;
            int32_t *l_1803 = (void*)0;
            int i;
            for (i = 0; i < 8; i++)
                l_1762[i] = &l_1763[2];
            for (g_493 = 0; (g_493 == 21); ++g_493)
            { /* block id: 743 */
                int32_t l_1720 = 0x77C8C1D9L;
                int16_t **l_1721 = &g_924;
                int32_t l_1727 = 0x7644F16CL;
                int8_t ***l_1747 = (void*)0;
                int32_t *l_1751 = &l_1607;
                float l_1797 = 0x1.4p+1;
                int16_t l_1800 = (-1L);
                struct S0 *l_1822 = &g_1124;
            }
            for (g_511 = 0; (g_511 < 60); g_511++)
            { /* block id: 812 */
                for (g_374 = (-29); (g_374 != 27); g_374 = safe_add_func_int64_t_s_s(g_374, 1))
                { /* block id: 815 */
                    struct S0 *l_1834 = &g_1124;
                    l_1834 = l_1834;
                }
            }
        }
        for (g_493 = (-30); (g_493 >= 34); g_493 = safe_add_func_uint16_t_u_u(g_493, 2))
        { /* block id: 822 */
            uint8_t l_1841 = 255UL;
            int16_t l_1844 = (-9L);
            int8_t **l_1845 = (void*)0;
            uint64_t *l_1854 = &g_303;
            uint64_t *l_1855[1][5][7] = {{{&g_48,&g_48,&g_48,&g_48,&g_48,&g_48,&g_48},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_48,&g_48,&g_48,&g_48,&g_48,&g_48,&g_48},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_48,&g_48,&g_48,&g_48,&g_48,&g_48,&g_48}}};
            int16_t l_1856[5];
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_1856[i] = (-1L);
            for (l_1604 = 1; (l_1604 >= 0); l_1604 -= 1)
            { /* block id: 825 */
                struct S0 ****l_1838 = &l_1805;
                int8_t *l_1839 = (void*)0;
                int32_t l_1840 = 0xEF4FEBFAL;
                int i;
                if (((safe_unary_minus_func_uint64_t_u(l_1617[(l_1604 + 6)])) ^ (l_1840 &= (&l_1805 != l_1838))))
                { /* block id: 827 */
                    for (g_1397 = 0; g_1397 < 2; g_1397 += 1)
                    {
                        struct S0 tmp = {-1,-42};
                        g_770[g_1397] = tmp;
                    }
                    l_1841++;
                }
                else
                { /* block id: 830 */
                    (*g_1258) = func_30((*g_797));
                    if (p_8)
                        continue;
                    if (p_8)
                        continue;
                }
                (*g_1258) = &p_8;
                if (p_8)
                    break;
                for (g_251 = 1; (g_251 >= 0); g_251 -= 1)
                { /* block id: 839 */
                    return l_1844;
                }
                for (g_1164 = 0; (g_1164 <= 1); g_1164 += 1)
                { /* block id: 844 */
                    struct S0 *l_1846 = &g_1718;
                    struct S0 **l_1847 = (void*)0;
                    int i, j, k;
                    (**g_1258) = (((((**l_1568) = l_1845) != (void*)0) , (*g_731)) == (l_1848 = l_1846));
                    g_987[(g_1164 + 3)][(l_1604 + 1)][l_1604] = g_987[l_1604][g_1164][g_1164];
                }
            }
            l_1616 ^= (((safe_add_func_int64_t_s_s(p_8, ((g_1851 , (1UL <= (safe_mod_func_uint32_t_u_u(4294967291UL, ((**g_672) = 1L))))) > (-1L)))) , l_1854) != (l_1855[0][1][0] = l_1590));
            if (l_1856[1])
                continue;
        }
        return p_8;
    }
    return l_1569;
}


/* ------------------------------------------ */
/* 
 * reads : g_94 g_55
 * writes: g_251
 */
static struct S0  func_9(float  p_10, uint64_t  p_11, uint8_t  p_12, uint32_t  p_13, const uint32_t  p_14)
{ /* block id: 304 */
    float l_644 = 0x0.Cp-1;
    int32_t l_645 = 1L;
    int32_t l_646 = 0x363F794BL;
    int32_t l_647 = 0x45F61FA5L;
    int32_t l_648 = 0xF5D27322L;
    int32_t l_649 = 0x29285298L;
    int32_t l_650 = 1L;
    int32_t l_651 = 0x2F43179FL;
    int32_t l_652 = 0x3E96C018L;
    int32_t l_653 = 3L;
    int32_t l_654 = 0x15633D05L;
    int32_t l_655 = (-1L);
    int32_t l_656[7][6][6] = {{{(-1L),(-1L),0xDFB52CFFL,0xD9D8C9A7L,0xF40D42C5L,1L},{0xC8F45199L,0x1ACD6301L,(-3L),0x1F62A395L,0x36774425L,0L},{(-1L),(-1L),0x65B3F2B5L,0x9E7B3C20L,1L,0xF3018C5DL},{0x2E304B33L,0x47402522L,(-1L),0x63ED9C73L,0x9D03E228L,0x36774425L},{0L,0xC2383DB2L,0xC8F45199L,(-3L),0x63C1D34AL,0xCBEA0B3DL},{9L,0xD9D8C9A7L,0x4DA53F6CL,0x7A013FB9L,0x9E7B3C20L,0xAA5A8172L}},{{(-8L),0xFB53433DL,0x2177D605L,(-1L),0xDFB52CFFL,0x9D03E228L},{(-1L),(-1L),0x63ED9C73L,0xAA5A8172L,0xCF9B4995L,0x1F62A395L},{1L,0x83812EA5L,0x2E304B33L,(-1L),0x2E304B33L,0x83812EA5L},{0x404944F4L,(-10L),0xC3C100CEL,0x9D03E228L,0x65B3F2B5L,0xD2CA478FL},{0xD2CA478FL,0L,(-1L),0x5A181AD9L,0x5BB31A8BL,0L},{(-1L),0L,(-1L),0xBF22F229L,0x65B3F2B5L,0L}},{{0x36774425L,(-10L),0xCBEA0B3DL,0xF3018C5DL,0x2E304B33L,(-7L)},{(-1L),0x83812EA5L,(-1L),0xE4E4B637L,0xCF9B4995L,9L},{0x65B3F2B5L,(-1L),0L,(-1L),0xDFB52CFFL,0xFB53433DL},{0x59F5C512L,0xFB53433DL,(-1L),0x2E304B33L,0x9E7B3C20L,0xC8F45199L},{0xE052BEDDL,0xD9D8C9A7L,9L,0x31BF5A8AL,0x63C1D34AL,9L},{(-1L),0xC2383DB2L,0x47402522L,9L,0x9D03E228L,(-3L)}},{{(-1L),0x47402522L,0xD2CA478FL,0xE042566AL,1L,(-8L)},{0x31BF5A8AL,(-1L),0xC2383DB2L,0x36774425L,0x36774425L,0xC2383DB2L},{0x1ACD6301L,0x1ACD6301L,0x9E7B3C20L,5L,0xF40D42C5L,0xBF22F229L},{0x7A013FB9L,(-1L),0L,0x59F5C512L,0L,0x9E7B3C20L},{0xC2383DB2L,0x7A013FB9L,0L,0xCF9B4995L,0x1ACD6301L,0xBF22F229L},{5L,0xCF9B4995L,0x9E7B3C20L,(-10L),0xD2CA478FL,0xC2383DB2L}},{{(-10L),0xD2CA478FL,0xC2383DB2L,9L,0x2177D605L,(-8L)},{(-7L),0xE052BEDDL,0xD2CA478FL,9L,(-1L),(-3L)},{(-1L),0xBF22F229L,0x47402522L,5L,5L,9L},{0xE4E4B637L,0L,9L,0xE052BEDDL,(-1L),0xC8F45199L},{0x5BB31A8BL,0x9E7B3C20L,(-1L),9L,(-10L),0xFB53433DL},{0L,0xC8F45199L,0L,0xC3C100CEL,0xBF22F229L,9L}},{{0xF3018C5DL,0x63ED9C73L,(-1L),(-7L),(-1L),(-7L)},{0xCBEA0B3DL,0x63ED9C73L,1L,0x2177D605L,(-1L),0L},{0x5A181AD9L,0xFB53433DL,0xBF22F229L,0x83812EA5L,0xE042566AL,(-7L)},{0x05095ECBL,0xF3018C5DL,(-1L),0x83812EA5L,0x1F62A395L,0x2177D605L},{0x5A181AD9L,(-1L),9L,0x2177D605L,0x5BB31A8BL,0x1F62A395L},{1L,0xE4E4B637L,0x59F5C512L,(-1L),0x9D03E228L,1L}},{{0xE4E4B637L,0xC8F45199L,(-1L),9L,0xF40D42C5L,0xCF9B4995L},{0L,0xCBEA0B3DL,9L,0xAA5A8172L,0x950DA920L,(-1L)},{0x4DA53F6CL,0xF40D42C5L,0L,0L,0xFB53433DL,1L},{9L,9L,(-1L),0xD2CA478FL,1L,(-3L)},{0xF3018C5DL,0xAA5A8172L,(-1L),0x9D03E228L,(-10L),0xE4E4B637L},{(-1L),1L,(-1L),(-1L),9L,9L}}};
    uint64_t l_657 = 0UL;
    int64_t l_660 = 0x66DB1200A4473242LL;
    int8_t ****l_692 = (void*)0;
    int32_t *l_697 = (void*)0;
    uint8_t *l_702 = &g_245;
    uint16_t l_705[8][10][3] = {{{0xB6C4L,65535UL,65535UL},{8UL,65526UL,65527UL},{0x6B60L,0UL,4UL},{65531UL,65535UL,1UL},{0x1885L,65532UL,0xB989L},{0x91D8L,65535UL,65527UL},{65527UL,0UL,0xFE03L},{0x30A2L,65526UL,0x3061L},{65535UL,65535UL,0x3176L},{65535UL,6UL,65535UL}},{{0xA1ECL,0xE10CL,0x7687L},{0UL,0x3176L,65535UL},{0xEFC6L,65531UL,1UL},{0x06BEL,0xB6C4L,0UL},{0x7BD4L,6UL,0xDB8CL},{65535UL,0xEFC6L,65535UL},{0UL,0xC710L,0x2F65L},{0x8400L,1UL,0xB931L},{65532UL,0UL,0xB931L},{0UL,0UL,0x2F65L}},{{65535UL,0x1885L,65535UL},{0xFE03L,0x6B60L,0xDB8CL},{1UL,0x9CD0L,0UL},{65533UL,0x8400L,1UL},{65533UL,65526UL,65535UL},{65527UL,65535UL,0x7687L},{0xC9EAL,0x3061L,65535UL},{0xE095L,65535UL,0x3176L},{0x3061L,0xE095L,0x3061L},{0UL,0x2F65L,0xFE03L}},{{65533UL,1UL,65527UL},{0xDB8CL,65527UL,0xB989L},{0xCBCBL,65535UL,1UL},{0xDB8CL,1UL,4UL},{65533UL,1UL,65527UL},{0UL,65535UL,65535UL},{0x3061L,0x99DEL,65535UL},{0xE095L,65535UL,0x1885L},{0xC9EAL,1UL,0x47B0L},{65527UL,65532UL,0xC9EAL}},{{65533UL,65533UL,1UL},{65533UL,1UL,0x3061L},{0x5870L,0xC9EAL,65533UL},{65531UL,65532UL,65535UL},{0UL,0UL,0xCBCBL},{0x8400L,1UL,0xA949L},{0UL,1UL,0x8B34L},{65532UL,0UL,65528UL},{4UL,65532UL,65527UL},{1UL,0xC9EAL,1UL}},{{8UL,0x30A2L,0x9CD0L},{65528UL,0xA949L,0xB989L},{1UL,0xE10CL,0x5870L},{65535UL,1UL,0UL},{1UL,1UL,65527UL},{65533UL,65535UL,65535UL},{0UL,0UL,1UL},{1UL,0x5870L,0xB6C4L},{0xEFC6L,1UL,0xE095L},{0xFE03L,65535UL,1UL}},{{65532UL,0xEFC6L,0xE095L},{65535UL,65526UL,0xB6C4L},{65535UL,6UL,1UL},{0x04A9L,65533UL,65535UL},{0xDB8CL,0UL,65527UL},{0xE10CL,0xB931L,0UL},{0xDAABL,0x06BEL,0x5870L},{0xC9EAL,1UL,0xB989L},{1UL,65532UL,0x9CD0L},{1UL,65535UL,1UL}},{{0UL,65535UL,65527UL},{0xCBCBL,65532UL,65528UL},{0x3176L,65527UL,0x8B34L},{0x47B0L,1UL,0xA949L},{0x47B0L,0UL,0xCBCBL},{0x3176L,65533UL,65535UL},{0xCBCBL,1UL,65533UL},{0UL,0UL,0x3061L},{1UL,0xDB8CL,0x2D7EL},{1UL,65535UL,0UL}}};
    uint8_t l_766[8][2][9] = {{{1UL,0xB9L,5UL,255UL,0xC7L,0UL,248UL,1UL,248UL},{1UL,0UL,0x12L,0x12L,0UL,1UL,0xCBL,1UL,0x9DL}},{{1UL,1UL,0x46L,0UL,1UL,0x6CL,0xC7L,0xC7L,0x6CL},{0x12L,0x37L,0x9DL,0x37L,0x12L,0x76L,0xCBL,1UL,0x87L}},{{0xC2L,0xC7L,1UL,255UL,1UL,0xB9L,248UL,0xB9L,1UL},{0xCBL,0x19L,0x19L,0xCBL,7UL,0x76L,0x9DL,1UL,0xB8L}},{{254UL,0UL,0xF1L,1UL,1UL,0x6CL,0x6CL,1UL,1UL},{8UL,0xCDL,8UL,0x5DL,7UL,1UL,0x87L,0x24L,0x76L}},{{0x46L,0xC2L,1UL,5UL,1UL,0x46L,1UL,255UL,0UL},{0x9DL,0x9DL,0x87L,0UL,8UL,7UL,0x24L,1UL,0xB8L}},{{0xC7L,248UL,0x46L,5UL,0xF1L,0xF1L,5UL,0x46L,248UL},{0x5DL,0xCBL,0x87L,1UL,0x19L,0UL,0x9DL,0x24L,0xCDL}},{{255UL,0xC7L,0UL,248UL,1UL,248UL,0UL,0xC7L,255UL},{0x19L,0xCBL,7UL,0x76L,0x9DL,1UL,0xB8L,0x87L,0x37L}},{{254UL,248UL,0UL,0x46L,0x46L,0UL,248UL,254UL,1UL},{0x19L,0x9DL,0xB8L,8UL,0x12L,0x5DL,0xCDL,0x76L,0x76L}}};
    struct S0 **l_806 = &g_732[0];
    struct S0 ***l_805 = &l_806;
    int8_t ***l_858 = &g_577;
    int64_t l_908 = 0x2A7A8F3D0566414DLL;
    int16_t **l_925 = &g_924;
    const int32_t *l_936 = &g_427;
    int16_t l_1103 = (-8L);
    uint64_t **l_1136[3][1][8] = {{{&g_1134,&g_1134,&g_1134,(void*)0,&g_1134,&g_1134,&g_1134,&g_1134}},{{&g_1134,(void*)0,&g_1134,(void*)0,&g_1134,&g_1134,&g_1134,(void*)0}},{{&g_1134,(void*)0,&g_1134,&g_1134,&g_1134,&g_1134,&g_1134,(void*)0}}};
    int32_t **l_1138 = &g_166[4];
    uint16_t l_1142 = 0x2641L;
    const int32_t l_1178 = 5L;
    const struct S0 *l_1202 = (void*)0;
    uint32_t l_1248 = 0x7E761EB1L;
    int8_t l_1249[3];
    int32_t l_1279 = (-10L);
    int16_t l_1291 = 0L;
    const int8_t *l_1348 = &g_1074;
    const int8_t **l_1347 = &l_1348;
    const int8_t ***l_1346 = &l_1347;
    const int8_t ****l_1345 = &l_1346;
    const struct S0 *l_1407[9][6] = {{&g_694,(void*)0,(void*)0,(void*)0,&g_55,(void*)0},{&g_1318,(void*)0,&g_694,&g_55,&g_1295,&g_737},{&g_1318,(void*)0,&g_55,(void*)0,&g_1318,&g_55},{&g_694,&g_55,&g_1295,&g_1318,&g_737,&g_123},{&g_57,(void*)0,&g_737,&g_55,&g_123,&g_123},{&g_1295,&g_1295,&g_1295,&g_1295,&g_1295,&g_55},{&g_123,&g_57,&g_55,(void*)0,(void*)0,&g_737},{&g_55,&g_57,&g_694,(void*)0,(void*)0,(void*)0},{(void*)0,&g_57,(void*)0,&g_691,&g_1295,&g_57}};
    float l_1482[7] = {0x3.Fp+1,0x3.Fp+1,0x3.Fp+1,0x3.Fp+1,0x3.Fp+1,0x3.Fp+1,0x3.Fp+1};
    const int32_t **l_1561 = &l_936;
    const int32_t **l_1562 = &g_1298;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1249[i] = 0x1EL;
    for (g_251 = 2; (g_251 >= (-29)); --g_251)
    { /* block id: 307 */
        int16_t l_639 = 0L;
        int32_t *l_640 = &g_37[0];
        int32_t *l_641 = &g_37[0];
        int32_t *l_642 = &g_37[0];
        int32_t *l_643[10] = {&g_37[0],&g_37[0],&g_37[0],&g_37[0],&g_37[0],&g_37[0],&g_37[0],&g_37[0],&g_37[0],&g_37[0]};
        int8_t *l_679 = (void*)0;
        int i;
        l_657--;
    }
    return (*g_94);
}


/* ------------------------------------------ */
/* 
 * reads : g_15 g_32 g_37 g_55 g_57.f0 g_48 g_79 g_82 g_57.f1 g_92 g_54 g_123 g_165 g_170 g_129 g_94 g_236 g_245 g_261 g_237 g_303 g_258 g_427 g_373 g_374 g_432 g_493 g_401 g_377 g_511 g_251 g_263
 * writes: g_48 g_54 g_57 g_55 g_79 g_37 g_92 g_93 g_134 g_166 g_233 g_237 g_245 g_251 g_261 g_263 g_303 g_427 g_258 g_377 g_621 g_432
 */
static int64_t  func_16(float  p_17)
{ /* block id: 2 */
    uint32_t l_603 = 0UL;
    int32_t **l_605 = &g_377;
    (*l_605) = func_18((g_15 & (func_21(g_15) == l_603)), g_401[0]);
    for (g_48 = 0; (g_48 <= 0); g_48 += 1)
    { /* block id: 287 */
        int16_t *l_618 = &g_261;
        int16_t *l_619 = &g_251;
        int16_t *l_620 = &g_621;
        int i;
        if (g_129[g_48])
            break;
        (*g_237) = (safe_lshift_func_uint8_t_u_s(g_493, (safe_mul_func_uint16_t_u_u(((**l_605) , (((*g_373) | 0x4FF4L) <= (safe_div_func_uint32_t_u_u(g_493, g_129[g_48])))), ((safe_rshift_func_int8_t_s_u((0x95L == (((*l_620) = (safe_sub_func_int16_t_s_s((((*l_619) &= ((*l_618) ^= (((safe_sub_func_int32_t_s_s(0x9134C7D4L, 0L)) , g_511) ^ 8UL))) == (**l_605)), (*g_373)))) >= (**l_605))), (**l_605))) , (**l_605))))));
        if ((*g_377))
            break;
    }
    for (g_432 = 0; (g_432 <= 1); g_432 += 1)
    { /* block id: 297 */
        int8_t *l_633 = &g_258[1];
        uint8_t *l_634 = &g_263;
        int32_t l_635 = 0xBA1820F4L;
        int i;
        l_635 &= (((((g_401[g_432] > ((safe_sub_func_int16_t_s_s(((+g_401[g_432]) , (safe_lshift_func_uint8_t_u_u(((*l_634) &= ((safe_mod_func_int8_t_s_s(0x5FL, (((*l_633) = (((252UL || ((((*g_237) = (**l_605)) , 249UL) == (((safe_add_func_uint64_t_u_u((((g_401[g_432] & g_401[g_432]) ^ 0x5C3740A3E5DEABCCLL) | g_374), g_401[g_432])) && g_251) ^ 255UL))) <= g_401[g_432]) != 0xB9490112L)) | g_401[g_432]))) != (-6L))), 7))), g_401[g_432])) == l_603)) , 4L) < 0x6EFA9371976981D2LL) , (void*)0) == (void*)0);
    }
    return (**l_605);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_18(const uint8_t  p_19, uint32_t  p_20)
{ /* block id: 282 */
    int32_t *l_604[8];
    int i;
    for (i = 0; i < 8; i++)
        l_604[i] = &g_37[0];
    return l_604[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_15 g_37 g_55 g_57.f0 g_48 g_79 g_82 g_57.f1 g_92 g_54 g_123 g_165 g_170 g_129 g_94 g_236 g_245 g_261 g_237 g_303 g_258 g_427 g_373 g_374 g_432 g_493
 * writes: g_48 g_54 g_57 g_55 g_79 g_37 g_92 g_93 g_134 g_166 g_233 g_237 g_245 g_251 g_261 g_263 g_303 g_427 g_258
 */
static int32_t  func_21(int64_t  p_22)
{ /* block id: 3 */
    int32_t *l_36 = &g_37[0];
    float l_63 = 0xE.35F201p-67;
    struct S0 *l_597 = &g_123;
    struct S0 **l_596 = &l_597;
    struct S0 ***l_595 = &l_596;
    uint8_t *l_598 = &g_245;
    int8_t *l_601 = &g_258[1];
    uint16_t l_602 = 0xC54BL;
    g_427 &= (safe_rshift_func_uint8_t_u_u((p_22 & (safe_unary_minus_func_uint8_t_u(1UL))), (func_26(func_30((l_36 = (g_32[2] , ((func_33(l_36, p_22) != ((((g_15 >= (((*l_36) >= (safe_add_func_float_f_f((safe_add_func_float_f_f(((*l_36) != (*l_36)), l_63)), p_22))) < p_22)) >= (*l_36)) != (*l_36)) , g_37[0])) , l_36)))), g_129[0], g_123.f0) < p_22)));
    l_602 ^= (safe_div_func_int16_t_s_s((func_38(((*g_373) , ((safe_add_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((l_595 == &l_596), ((*l_598) = (&l_36 == (void*)0)))), ((*l_36) == (((*l_36) != (safe_lshift_func_uint16_t_u_s((((*l_601) = (g_432 == ((-1L) >= 0UL))) <= 1L), 13))) >= p_22)))) > p_22)), g_493, l_36, g_57.f0, p_22) , g_427), 0xA3C9L));
    return p_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_37 g_123.f1 g_57.f0 g_129 g_15 g_54 g_94 g_55 g_48 g_123.f0 g_92 g_79 g_82 g_57.f1 g_123 g_165 g_170 g_236 g_245 g_261 g_237 g_303 g_258
 * writes: g_166 g_54 g_57 g_37 g_134 g_233 g_48 g_79 g_92 g_93 g_237 g_245 g_251 g_261 g_263 g_303
 */
static int16_t  func_26(int32_t * p_27, int8_t  p_28, int16_t  p_29)
{ /* block id: 82 */
    int8_t l_183[3][4][4] = {{{7L,7L,0xB7L,7L},{7L,(-10L),(-10L),7L},{(-10L),7L,(-10L),(-10L)},{7L,7L,0xB7L,7L}},{{7L,(-10L),(-10L),7L},{(-10L),7L,(-10L),(-10L)},{7L,7L,0xB7L,7L},{(-10L),0xB7L,0xB7L,(-10L)}},{{0xB7L,(-10L),0xB7L,0xB7L},{(-10L),(-10L),7L,(-10L)},{(-10L),0xB7L,0xB7L,(-10L)},{0xB7L,(-10L),0xB7L,0xB7L}}};
    uint16_t l_214[9] = {65534UL,65534UL,65534UL,65534UL,65534UL,65534UL,65534UL,65534UL,65534UL};
    int32_t l_270 = 0x18D95EC4L;
    int32_t l_271[7] = {9L,9L,(-7L),9L,9L,(-7L),9L};
    int32_t l_313 = (-1L);
    struct S0 *l_322 = &g_57;
    uint8_t l_353[2];
    uint16_t l_571 = 65535UL;
    int32_t **l_584 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_353[i] = 9UL;
    for (p_28 = 0; (p_28 != (-27)); p_28 = safe_sub_func_uint32_t_u_u(p_28, 3))
    { /* block id: 85 */
        int32_t **l_175 = &g_166[4];
        int32_t l_182[1][5][2] = {{{0xBE2DA87BL,0xBE2DA87BL},{0xBE2DA87BL,0xBE2DA87BL},{0xBE2DA87BL,0xBE2DA87BL},{0xBE2DA87BL,0xBE2DA87BL},{0xBE2DA87BL,0xBE2DA87BL}}};
        struct S0 *l_198[1][3];
        struct S0 **l_197 = &l_198[0][1];
        uint16_t l_255 = 0x04A4L;
        uint16_t l_256[3][7][2] = {{{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL}},{{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL}},{{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL},{65530UL,65530UL}}};
        int32_t l_259 = 9L;
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 3; j++)
                l_198[i][j] = (void*)0;
        }
        if ((safe_sub_func_uint8_t_u_u((0xDE4584C0B326B5A9LL & (0xCA5C7507L > (&g_82 == &g_82))), ((*p_27) <= ((((p_27 == ((*l_175) = p_27)) , (g_123.f1 ^ (safe_div_func_int64_t_s_s((((((safe_div_func_int16_t_s_s(((((((safe_add_func_uint64_t_u_u(1UL, 1UL)) | l_182[0][4][0]) , p_29) || g_57.f0) || g_129[0]) <= 4L), p_29)) < l_182[0][0][0]) , l_183[2][0][0]) , g_15) < p_29), g_129[0])))) >= 0x15L) != g_37[0])))))
        { /* block id: 87 */
            int16_t l_213 = 1L;
            float * const l_230 = &g_134;
            if ((*p_27))
            { /* block id: 88 */
                int64_t l_229 = (-1L);
                int32_t l_231[9][8][3] = {{{2L,(-1L),0L},{(-1L),1L,0xEB692CA0L},{(-3L),0x6E98DB74L,0x55A2B3F6L},{(-1L),0x4172F5B4L,(-5L)},{(-3L),(-6L),(-6L)},{(-1L),0x28CB9ACBL,5L},{2L,0x6E98DB74L,(-6L)},{(-5L),(-10L),(-5L)}},{{0x21C0D936L,(-1L),0x55A2B3F6L},{(-1L),(-10L),0xEB692CA0L},{0x6E98DB74L,0x6E98DB74L,0L},{(-1L),0x28CB9ACBL,(-5L)},{0x6E98DB74L,(-6L),(-1L)},{(-1L),0x4172F5B4L,5L},{0x21C0D936L,0x6E98DB74L,(-1L)},{(-5L),1L,(-5L)}},{{2L,(-1L),0L},{(-1L),1L,0xEB692CA0L},{(-3L),0x6E98DB74L,0x55A2B3F6L},{(-1L),0x4172F5B4L,(-5L)},{(-3L),(-6L),(-6L)},{(-1L),0x28CB9ACBL,5L},{2L,0x6E98DB74L,(-6L)},{(-5L),(-10L),(-5L)}},{{0x21C0D936L,(-1L),0x55A2B3F6L},{(-1L),(-10L),0xEB692CA0L},{0x6E98DB74L,0x6E98DB74L,0L},{(-1L),0x28CB9ACBL,(-5L)},{0x6E98DB74L,(-6L),(-1L)},{(-1L),0x4172F5B4L,5L},{0x21C0D936L,0x6E98DB74L,(-1L)},{(-5L),1L,(-5L)}},{{2L,(-1L),0L},{(-1L),1L,0xEB692CA0L},{(-3L),0x6E98DB74L,0x55A2B3F6L},{(-1L),0x4172F5B4L,(-5L)},{(-3L),(-6L),(-6L)},{(-1L),0x28CB9ACBL,5L},{2L,0x6E98DB74L,(-6L)},{(-5L),(-10L),(-5L)}},{{0x21C0D936L,(-1L),0x55A2B3F6L},{(-1L),(-10L),0xEB692CA0L},{0x6E98DB74L,0x6E98DB74L,0L},{(-1L),0x28CB9ACBL,(-5L)},{0x6E98DB74L,(-6L),(-1L)},{(-1L),0x4172F5B4L,5L},{0x21C0D936L,0x6E98DB74L,(-1L)},{(-5L),1L,(-5L)}},{{2L,(-1L),0L},{(-1L),1L,0xEB692CA0L},{(-3L),0x6E98DB74L,0x55A2B3F6L},{(-1L),0x4172F5B4L,(-5L)},{(-3L),(-6L),(-6L)},{(-1L),0x28CB9ACBL,5L},{2L,0x6E98DB74L,(-6L)},{(-5L),(-10L),(-5L)}},{{0x21C0D936L,(-1L),0x55A2B3F6L},{(-1L),(-10L),0xEB692CA0L},{0x6E98DB74L,0x6E98DB74L,0L},{(-1L),0x28CB9ACBL,(-5L)},{0x6E98DB74L,(-6L),(-1L)},{(-1L),0x4172F5B4L,5L},{0x21C0D936L,0x6E98DB74L,(-1L)},{(-5L),1L,(-5L)}},{{2L,(-1L),0L},{(-1L),1L,0xEB692CA0L},{(-3L),0x6E98DB74L,0x55A2B3F6L},{(-1L),0x4172F5B4L,(-5L)},{(-3L),(-6L),(-6L)},{(-1L),0x28CB9ACBL,5L},{2L,0x6E98DB74L,(-6L)},{(-5L),(-10L),(-5L)}}};
                int i, j, k;
                for (p_29 = 0; (p_29 < 7); p_29++)
                { /* block id: 91 */
                    int8_t l_204[7][6][6] = {{{0xC1L,0xFDL,(-3L),0xB5L,1L,1L},{0L,0x5DL,0L,0xC1L,0L,(-6L)},{1L,0xC8L,0xE7L,1L,(-10L),(-6L)},{0L,0x5BL,0L,0L,1L,1L},{(-6L),0L,(-3L),(-1L),0xFDL,0xE7L},{(-3L),0x40L,(-1L),(-6L),0xFDL,(-6L)}},{{0x2BL,0L,0x2BL,1L,1L,1L},{1L,0x5BL,0xC1L,0xE7L,(-10L),0xF8L},{(-1L),0xC8L,1L,0xE7L,0L,1L},{1L,0x5DL,0L,1L,1L,0x2BL},{0x2BL,0xFDL,1L,(-6L),0x5BL,0L},{(-3L),0xAEL,1L,(-1L),0x5DL,0x2BL}},{{(-6L),6L,0L,0L,0x8AL,1L},{0x5AL,0L,1L,0x9CL,0xE7L,0L},{0x3AL,0L,0x93L,0x93L,0L,0x3AL},{0x5CL,0L,(-5L),0x0AL,(-3L),0xF6L},{0x93L,0xF8L,9L,0x3DL,(-1L),0xC4L},{0x93L,1L,0x3DL,0x0AL,0x2BL,0x9CL}},{{0x5CL,(-3L),0x3AL,0x93L,0xC1L,0xC3L},{0x3AL,1L,0xC4L,0x9CL,1L,0xC3L},{0x5AL,(-1L),0x3AL,0x5AL,0L,0x9CL},{0xC3L,(-6L),0x3DL,7L,1L,0xC4L},{0x3DL,0xB5L,9L,0xF6L,1L,0xF6L},{(-5L),(-6L),(-5L),1L,0L,0x3AL}},{{0x9CL,(-1L),0x93L,0xC4L,1L,0L},{7L,1L,1L,0xC4L,0xC1L,1L},{0x9CL,(-3L),(-1L),1L,0x2BL,(-5L)},{(-5L),1L,0x9CL,0xF6L,(-1L),(-1L)},{0x3DL,0xF8L,0x9CL,7L,(-3L),(-5L)},{0xC3L,0L,(-1L),0x5AL,0L,1L}},{{0x5AL,0L,1L,0x9CL,0xE7L,0L},{0x3AL,0L,0x93L,0x93L,0L,0x3AL},{0x5CL,0L,(-5L),0x0AL,(-3L),0xF6L},{0x93L,0xF8L,9L,0x3DL,(-1L),0xC4L},{0x93L,1L,0x3DL,0x0AL,0x2BL,0x9CL},{0x5CL,(-3L),0x3AL,0x93L,0xC1L,0xC3L}},{{0x3AL,1L,0xC4L,0x9CL,1L,0xC3L},{0x5AL,(-1L),0x3AL,0x5AL,0L,0x9CL},{0xC3L,(-6L),0x3DL,7L,1L,0xC4L},{0x3DL,0xB5L,9L,0xF6L,1L,0xF6L},{(-5L),(-6L),(-5L),1L,0L,0x3AL},{0x9CL,(-1L),0x93L,0xC4L,1L,0L}}};
                    int32_t l_215 = 0x3036A653L;
                    int i, j, k;
                    for (g_54 = 0; (g_54 < 13); ++g_54)
                    { /* block id: 94 */
                        struct S0 *l_188 = &g_57;
                        int32_t l_205 = 0x92B1ACEAL;
                        int8_t *l_208 = &l_204[4][5][1];
                        float *l_216 = &g_134;
                        float *l_232 = &g_233[4][0];
                        (*l_188) = (*g_94);
                        (*l_216) = ((safe_add_func_float_f_f(((((safe_sub_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(0x8B1CL, ((safe_div_func_uint64_t_u_u((l_197 == (void*)0), (safe_mod_func_int32_t_s_s(((*p_27) = (safe_add_func_uint64_t_u_u((+(p_28 < (((((l_204[1][4][4] , ((p_28 != l_205) , (((safe_mod_func_int8_t_s_s(((*l_208) = 0x21L), ((safe_mul_func_uint16_t_u_u((safe_mod_func_int16_t_s_s((-1L), p_29)), l_213)) | l_214[5]))) | g_55.f0) ^ 0xA0ADL))) | p_29) >= g_48) <= g_123.f0) & l_214[6]))), g_123.f0))), g_129[0])))) , 0L))), g_54)) > 0x23L) , p_29) <= l_215), l_205)) != p_28);
                        (*l_232) = (((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((safe_add_func_float_f_f(p_29, p_28)), (l_231[6][7][0] = ((safe_div_func_float_f_f(0x5.3055ACp+63, ((safe_rshift_func_uint16_t_u_u(((safe_mul_func_uint16_t_u_u(p_28, 7UL)) | l_229), 8)) , ((*l_216) = (((void*)0 != l_230) , p_29))))) != g_37[0])))) == 0xA.8C28E4p-39), g_92)) >= g_55.f0) < 0x1.Bp-1);
                        if (l_182[0][0][0])
                            continue;
                    }
                    for (l_229 = 0; (l_229 == 18); l_229 = safe_add_func_int64_t_s_s(l_229, 2))
                    { /* block id: 106 */
                        (*g_236) = func_30(func_30(func_30(p_27)));
                    }
                }
            }
            else
            { /* block id: 110 */
                (*p_27) = 0x39558DB1L;
            }
            if ((*p_27))
                break;
        }
        else
        { /* block id: 114 */
            uint8_t *l_244 = &g_245;
            int16_t *l_250 = &g_251;
            int16_t *l_252 = (void*)0;
            int32_t l_253 = 0xE164FB72L;
            int32_t l_254 = 0xA8BEC59FL;
            int8_t *l_257[2];
            int16_t *l_260 = &g_261;
            uint8_t *l_262 = &g_263;
            int i;
            for (i = 0; i < 2; i++)
                l_257[i] = &g_258[1];
            (*p_27) = (safe_sub_func_uint8_t_u_u(((*l_262) = ((safe_add_func_int64_t_s_s(((((safe_rshift_func_uint8_t_u_u(((*l_244)--), p_29)) , ((*l_260) &= (safe_div_func_int16_t_s_s((0x63AD2DFDL >= l_183[0][2][0]), (255UL || ((l_182[0][4][0] != ((l_253 = ((*l_250) = g_55.f1)) & l_254)) , (l_259 = (l_255 > (((((l_182[0][4][0] = p_29) <= 0xF2L) && 0xC1L) & p_29) > l_256[0][0][0]))))))))) , l_256[0][0][0]) ^ l_256[0][0][0]), l_254)) | 0xC1AD6298BA18F781LL)), l_214[5]));
        }
    }
    for (p_28 = 0; (p_28 > 3); ++p_28)
    { /* block id: 127 */
        int32_t l_269 = (-10L);
        int32_t l_273 = 0xF9BB1D7BL;
        int32_t l_274 = 0x1EA1B5D2L;
        int32_t l_275 = 0xB794F4AFL;
        int32_t l_276 = 0x641DCC97L;
        int32_t l_277 = 0xCDF3AC24L;
        int32_t l_278 = 1L;
        int32_t l_279[2];
        int32_t l_312 = 0x6E1C05ADL;
        float *l_481[7] = {&g_233[4][0],&g_453[2][0],&g_233[4][0],&g_233[4][0],&g_453[2][0],&g_233[4][0],&g_233[4][0]};
        uint16_t **l_522 = &g_373;
        int32_t *l_542 = &l_279[1];
        int32_t l_569 = (-1L);
        int8_t *l_576 = &g_258[1];
        int8_t **l_575[10][4] = {{(void*)0,&l_576,&l_576,(void*)0},{&l_576,&l_576,&l_576,&l_576},{&l_576,&l_576,&l_576,&l_576},{&l_576,&l_576,(void*)0,&l_576},{&l_576,&l_576,&l_576,(void*)0},{&l_576,(void*)0,(void*)0,&l_576},{&l_576,(void*)0,&l_576,(void*)0},{(void*)0,&l_576,&l_576,&l_576},{&l_576,&l_576,(void*)0,&l_576},{&l_576,&l_576,&l_576,(void*)0}};
        int32_t **l_583[3];
        int i, j;
        for (i = 0; i < 2; i++)
            l_279[i] = (-1L);
        for (i = 0; i < 3; i++)
            l_583[i] = &g_166[1];
        for (g_251 = (-29); (g_251 == 19); ++g_251)
        { /* block id: 130 */
            int32_t *l_268[2];
            int16_t l_272[7][9] = {{0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L},{0L,0L,0L,0L,0L,0L,0L,0L,0L}};
            uint8_t l_280 = 0x5DL;
            uint8_t *l_299 = (void*)0;
            uint8_t *l_300[6][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_263,(void*)0,&g_263,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_263,(void*)0,&g_263,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_263,(void*)0,&g_263,(void*)0}};
            uint16_t *l_301 = (void*)0;
            uint16_t *l_302[2];
            int i, j;
            for (i = 0; i < 2; i++)
                l_268[i] = &g_37[0];
            for (i = 0; i < 2; i++)
                l_302[i] = &l_214[8];
            l_280--;
            (*p_27) = 0x76C98522L;
            g_57.f0 ^= ((safe_mul_func_float_f_f(p_29, ((func_38((safe_lshift_func_int8_t_s_s((((((((g_54 , (safe_rshift_func_int16_t_s_s((safe_div_func_int8_t_s_s((p_29 ^ ((((safe_add_func_int32_t_s_s((*p_27), (**g_236))) != (l_271[0] = (safe_rshift_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s(p_28, (((safe_sub_func_uint8_t_u_u((((g_263 = p_29) && ((g_303--) >= (safe_mod_func_uint32_t_u_u(p_29, (safe_lshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s(l_312, g_258[1])), 10)))))) < g_123.f0), l_276)) , l_270) | (-1L)))), 7)))) == 250UL) || (-4L))), l_270)), 7))) & 0x9E045A7BL) >= l_313) | 0xD2F496A8B989A8E6LL) >= 3UL) > g_92) < g_261), 2)), l_279[1], p_27, p_29, l_183[2][0][0]) , g_261) < 0xE.40E753p-29))) , 0xCF099D6DL);
            (*g_237) |= (-2L);
        }
        for (g_79 = 5; (g_79 >= 0); g_79 -= 1)
        { /* block id: 141 */
            struct S0 *l_321 = &g_123;
            struct S0 **l_320 = &l_321;
            int32_t l_335 = 8L;
            uint32_t *l_341 = (void*)0;
            int32_t *l_342 = &l_273;
            int16_t *l_346 = &g_261;
            uint32_t *l_358 = &g_92;
            int32_t l_399 = 0x7B1E90C7L;
            int32_t l_400[8][9][3] = {{{0L,1L,6L},{0xEA8C4918L,0xD1AC72E4L,0xD1AC72E4L},{(-9L),0xEA8C4918L,0xB4559933L},{0x6F6F7E08L,0xBFE1B017L,0L},{0x4689E1DEL,(-1L),(-1L)},{(-1L),1L,1L},{0L,(-1L),(-1L)},{7L,0xBFE1B017L,1L},{1L,0xEA8C4918L,(-1L)}},{{0x770184F1L,0xD1AC72E4L,0xB765CC06L},{(-8L),1L,0xFC29A356L},{1L,(-1L),0xCB761770L},{0x55F8126BL,(-1L),0x03CD6C90L},{(-1L),0L,0L},{0xCCF39708L,1L,0x58822933L},{0xCFB8120AL,1L,0xD2ADCC10L},{1L,0x7DA4C627L,0L},{1L,0x3BC6F35CL,0xCCF39708L}},{{0xCFB8120AL,(-1L),(-1L)},{0xCCF39708L,(-8L),0x22E1C9E0L},{(-1L),(-1L),0x4689E1DEL},{0x55F8126BL,8L,(-9L)},{1L,(-8L),0xEA8C4918L},{0xCA55B8ACL,(-9L),1L},{1L,1L,7L},{0xD1AC72E4L,0xCFB8120AL,8L},{0x03CD6C90L,0L,2L}},{{0x7DA4C627L,0x1B7FFEE6L,0x0FAD6F11L},{0xEA8C4918L,0x03CD6C90L,2L},{1L,1L,8L},{0x4689E1DEL,0x7FEB0DE3L,7L},{0xD2ADCC10L,4L,1L},{0L,1L,0xEA8C4918L},{5L,0x2E7747F3L,0xD2ADCC10L},{(-1L),0x07555441L,1L},{0x6F6F7E08L,(-1L),(-6L)}},{{0x1B7FFEE6L,0L,0xCB761770L},{0x429E8BBBL,7L,0L},{0xBFE1B017L,1L,(-7L)},{(-6L),1L,0xCA65A85AL},{(-4L),7L,(-1L)},{1L,0L,(-9L)},{1L,(-1L),1L},{0xCCF39708L,0x07555441L,0xCFB8120AL},{(-9L),0x2E7747F3L,0L}},{{(-1L),1L,(-3L)},{0x3BC6F35CL,4L,0x3BC6F35CL},{(-1L),0x7FEB0DE3L,(-1L)},{0xCB761770L,1L,4L},{(-8L),0x03CD6C90L,0xB765CC06L},{0xCA65A85AL,0x1B7FFEE6L,6L},{(-8L),0L,(-1L)},{0xCB761770L,0xCFB8120AL,0x59AAC7A9L},{(-1L),1L,0x55F8126BL}},{{0x3BC6F35CL,(-9L),0xCCF39708L},{(-1L),(-8L),0xFC29A356L},{(-9L),0x429E8BBBL,0x2E7747F3L},{0xCCF39708L,0xCB761770L,0L},{1L,(-1L),0x22E1C9E0L},{1L,0xEA8C4918L,(-1L)},{(-4L),0x0FAD6F11L,(-1L)},{(-6L),1L,(-1L)},{0xBFE1B017L,0x22E1C9E0L,(-1L)}},{{0x429E8BBBL,8L,0x22E1C9E0L},{0x1B7FFEE6L,(-7L),0L},{0x6F6F7E08L,4L,0x2E7747F3L},{(-1L),0x3BC6F35CL,0xFC29A356L},{5L,0xB765CC06L,0xCCF39708L},{0L,0x55F8126BL,0x55F8126BL},{0xD2ADCC10L,0L,0x59AAC7A9L},{0x4689E1DEL,0x58822933L,(-1L)},{1L,1L,6L}}};
            int32_t l_478 = (-7L);
            int i, j, k;
        }
        for (l_274 = 0; (l_274 > (-6)); l_274 = safe_sub_func_int32_t_s_s(l_274, 4))
        { /* block id: 271 */
            int32_t *l_585[9][4] = {{&l_276,&l_279[0],&g_37[0],&l_279[0]},{&l_279[0],&l_271[6],&g_37[0],&g_37[0]},{&l_276,&l_276,&l_279[0],&g_37[0]},{&l_276,&l_271[6],&l_276,&l_279[0]},{&l_276,&l_279[0],&l_279[0],&l_276},{&l_276,&l_279[0],&g_37[0],&l_279[0]},{&l_279[0],&l_271[6],&g_37[0],&g_37[0]},{&l_276,&l_276,&l_279[0],&g_37[0]},{&l_276,&l_271[6],&l_276,&l_279[0]}};
            uint8_t l_586[10][1][1] = {{{0xCAL}},{{0xCAL}},{{254UL}},{{0x33L}},{{254UL}},{{0x33L}},{{0x33L}},{{0x33L}},{{0xCAL}},{{0x33L}}};
            int i, j, k;
            l_584 = l_583[1];
            --l_586[2][0][0];
        }
    }
    return g_79;
}


/* ------------------------------------------ */
/* 
 * reads : g_48 g_15 g_79 g_82 g_37 g_57.f1 g_55.f1 g_92 g_54 g_123 g_165 g_170
 * writes: g_48 g_79 g_37 g_57.f1 g_92 g_93 g_54 g_134
 */
static int32_t * func_30(int32_t * const  p_31)
{ /* block id: 13 */
    uint32_t l_68[3][7] = {{0x9CA2468FL,2UL,2UL,0x9CA2468FL,0UL,0x9CA2468FL,2UL},{0UL,0UL,8UL,0xB79C1411L,8UL,0UL,0UL},{1UL,2UL,0x5D188285L,2UL,1UL,1UL,2UL}};
    int32_t l_69 = 0x1F4251D1L;
    struct S0 *l_72[2][3];
    int32_t *l_96 = &l_69;
    int32_t **l_95 = &l_96;
    int32_t l_101 = 0x91F7CAE9L;
    int32_t l_103 = 0x4A66755DL;
    int32_t l_104 = 0x55470A06L;
    int32_t l_105 = 0xDA5B0066L;
    int32_t l_106 = 0x831ADEC5L;
    int32_t l_107 = (-1L);
    int32_t l_109 = (-6L);
    int32_t l_110[4] = {0xE9B1403BL,0xE9B1403BL,0xE9B1403BL,0xE9B1403BL};
    struct S0 **l_116 = &l_72[1][1];
    struct S0 ***l_115 = &l_116;
    int i, j;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
            l_72[i][j] = (void*)0;
    }
    for (g_48 = 0; (g_48 != 32); g_48 = safe_add_func_int8_t_s_s(g_48, 8))
    { /* block id: 16 */
        struct S0 * const l_75 = &g_57;
        struct S0 *l_77 = &g_55;
        struct S0 **l_76 = &l_77;
        int32_t l_78[8][4][2] = {{{(-1L),0xCAB9BE05L},{0x5237AE93L,0xCAB9BE05L},{(-1L),0x0944B0CEL},{7L,0xCAB9BE05L}},{{0xEF148A9BL,0xCAB9BE05L},{7L,0x0944B0CEL},{(-1L),0xCAB9BE05L},{0x5237AE93L,0xCAB9BE05L}},{{(-1L),0x0944B0CEL},{7L,0xCAB9BE05L},{0xEF148A9BL,0xCAB9BE05L},{7L,0x0944B0CEL}},{{(-1L),0xCAB9BE05L},{0x5237AE93L,0xCAB9BE05L},{(-1L),0x0944B0CEL},{7L,0xCAB9BE05L}},{{0xEF148A9BL,0xCAB9BE05L},{7L,0x0944B0CEL},{(-1L),0xCAB9BE05L},{0x5237AE93L,0xCAB9BE05L}},{{(-1L),0x0944B0CEL},{7L,0xCAB9BE05L},{0xEF148A9BL,0xCAB9BE05L},{7L,0x0944B0CEL}},{{(-1L),0xCAB9BE05L},{0x5237AE93L,0xCAB9BE05L},{(-1L),0x0944B0CEL},{7L,0xCAB9BE05L}},{{0xEF148A9BL,0xCAB9BE05L},{7L,0x0944B0CEL},{(-1L),0xCAB9BE05L},{0x5237AE93L,0xCAB9BE05L}}};
        struct S0 **l_81 = &l_72[1][1];
        struct S0 ***l_80 = &l_81;
        int64_t *l_83 = &g_79;
        int16_t l_90 = 0x500CL;
        uint32_t *l_91 = &g_92;
        int i, j, k;
        g_57.f1 ^= ((*p_31) = (safe_sub_func_uint32_t_u_u(((l_69 |= ((l_68[1][6] = 0x6AL) | 0xFBL)) != (safe_sub_func_uint64_t_u_u((g_79 ^= ((l_72[1][1] == l_72[0][0]) >= (g_15 & (safe_mul_func_int8_t_s_s((l_75 != ((*l_76) = (void*)0)), l_78[1][1][1]))))), (((*l_80) = &l_72[1][1]) == g_82)))), g_37[0])));
        (*p_31) ^= ((l_83 == (void*)0) > (((g_55.f1 == (safe_mod_func_uint64_t_u_u(0x161CA2EBD359D2D1LL, l_69))) > (((*l_91) = ((&l_72[1][2] != ((*l_80) = &l_72[1][1])) , ((safe_mul_func_float_f_f(l_78[7][0][0], (safe_sub_func_float_f_f((l_68[1][6] , l_90), 0x2.Dp+1)))) , l_78[1][1][1]))) >= l_68[1][6])) & 0x089D3BBDL));
        g_93 = g_82;
    }
    (*l_95) = p_31;
    for (l_69 = 2; (l_69 >= 0); l_69 -= 1)
    { /* block id: 32 */
        uint32_t l_97 = 0x0B9BD427L;
        int32_t l_102 = 9L;
        int32_t l_108[2][7] = {{(-7L),0x1361DE36L,0x1361DE36L,(-7L),0x1361DE36L,0x1361DE36L,(-7L)},{0xC3E2E3E2L,0xC4DE7E06L,0xC3E2E3E2L,0xC3E2E3E2L,0xC4DE7E06L,0xC3E2E3E2L,0xC3E2E3E2L}};
        const uint32_t *l_127 = &g_92;
        int i, j;
        for (g_92 = 0; (g_92 <= 6); g_92 += 1)
        { /* block id: 35 */
            int32_t *l_100[9][3] = {{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]},{&g_37[0],&g_37[0],&g_37[0]}};
            int32_t l_111 = 2L;
            uint32_t l_112 = 0UL;
            int i, j;
            ++l_97;
            --l_112;
            return &g_37[0];
        }
        for (l_101 = 6; (l_101 >= 2); l_101 -= 1)
        { /* block id: 42 */
            int32_t l_130 = 0x178462E0L;
            int i, j;
            l_115 = l_115;
            if (l_68[l_69][(l_69 + 1)])
                continue;
            if (g_48)
                continue;
            for (g_54 = 6; (g_54 >= 0); g_54 -= 1)
            { /* block id: 48 */
                int32_t *l_128[7];
                int64_t * const l_131 = &g_54;
                int32_t l_132 = 0x366B1B60L;
                float *l_133[9];
                uint64_t l_144[7] = {18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL};
                int i, j;
                for (i = 0; i < 7; i++)
                    l_128[i] = &g_129[0];
                for (i = 0; i < 9; i++)
                    l_133[i] = &g_134;
                g_134 = ((safe_mod_func_int8_t_s_s((l_68[l_69][g_54] ^ l_68[l_69][(l_69 + 1)]), (safe_lshift_func_uint16_t_u_u((l_68[l_69][g_54] ^ (safe_div_func_int64_t_s_s((l_132 |= ((((g_123 , 0x4B32L) , &g_54) == ((l_130 |= (((safe_lshift_func_uint8_t_u_s((p_31 == (l_68[l_69][(l_69 + 1)] , ((~(p_31 == (void*)0)) , l_127))), l_108[1][5])) , p_31) != &g_37[0])) , l_131)) , g_123.f0)), l_108[0][4]))), 6)))) , l_130);
                for (l_105 = 4; (l_105 >= 0); l_105 -= 1)
                { /* block id: 54 */
                    int32_t *l_135 = &l_108[0][4];
                    int32_t *l_136 = (void*)0;
                    int32_t *l_137 = &l_110[3];
                    int32_t *l_138 = &l_104;
                    int32_t *l_139 = &l_102;
                    int32_t *l_140 = &l_110[3];
                    int32_t *l_141 = &g_37[0];
                    int32_t *l_142[10];
                    int32_t l_143[4] = {0x33B4ECD8L,0x33B4ECD8L,0x33B4ECD8L,0x33B4ECD8L};
                    int i;
                    for (i = 0; i < 10; i++)
                        l_142[i] = &g_37[0];
                    --l_144[0];
                    if (l_108[0][4])
                        break;
                }
            }
        }
        for (l_105 = 6; (l_105 >= 2); l_105 -= 1)
        { /* block id: 62 */
            for (l_109 = 0; (l_109 <= 0); l_109 += 1)
            { /* block id: 65 */
                int32_t *l_153[1][10][4];
                int32_t *l_159[9] = {&g_129[0],&g_129[0],&g_129[0],&g_129[0],&g_129[0],&g_129[0],&g_129[0],&g_129[0],&g_129[0]};
                int32_t **l_158 = &l_159[0];
                int32_t l_169 = (-1L);
                int i, j, k;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 10; j++)
                    {
                        for (k = 0; k < 4; k++)
                            l_153[i][j][k] = &g_129[0];
                    }
                }
                (**l_95) ^= 0x301C7F31L;
                for (l_101 = 6; (l_101 >= 2); l_101 -= 1)
                { /* block id: 69 */
                    int32_t **l_154 = (void*)0;
                    int32_t **l_155[6];
                    uint64_t *l_162 = &g_48;
                    int i, j;
                    for (i = 0; i < 6; i++)
                        l_155[i] = (void*)0;
                    l_102 ^= ((safe_div_func_int8_t_s_s((safe_mod_func_int32_t_s_s(((*l_96) ^= 0x55FBD205L), (safe_lshift_func_uint8_t_u_s(((l_153[0][4][2] = l_153[0][4][2]) == &g_129[0]), (safe_rshift_func_uint8_t_u_s((((l_158 == ((safe_sub_func_uint64_t_u_u((--(*l_162)), ((void*)0 == &g_32[6]))) , g_165)) >= (l_110[l_109] , (safe_div_func_uint8_t_u_u(0x61L, l_110[l_69])))) < l_68[l_109][l_101]), l_68[l_109][l_105])))))), l_108[1][6])) > l_169);
                    if ((**l_95))
                        break;
                }
            }
            return &g_37[0];
        }
    }
    (*p_31) ^= g_170;
    return &g_37[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_32.f1 g_15 g_37 g_55 g_57.f0
 * writes: g_48 g_54 g_57 g_55
 */
static uint64_t  func_33(int32_t * p_34, uint64_t  p_35)
{ /* block id: 4 */
    const int32_t l_47 = 3L;
    int64_t *l_53 = &g_54;
    struct S0 *l_56 = (void*)0;
    struct S0 *l_58 = &g_55;
    g_57 = func_38((((((safe_add_func_int8_t_s_s((-1L), p_35)) > ((1L <= ((g_48 = ((safe_unary_minus_func_uint64_t_u(l_47)) | 0x162CL)) < (((((((((safe_mod_func_uint32_t_u_u(0x2B0402ADL, g_32[2].f1)) <= (((safe_add_func_int64_t_s_s(((*l_53) = ((p_34 != p_34) > g_15)), 9UL)) <= g_37[0]) , p_35)) <= 9L) ^ l_47) , 0x6128B293C34F9703LL) , 0x02EEDA7DL) >= p_35) | 4UL) || 4L))) < l_47)) != (*p_34)) & l_47) || 0x3B05E262L), p_35, &g_37[0], p_35, p_35);
    (*l_58) = g_55;
    return g_57.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_55
 * writes:
 */
static struct S0  func_38(int32_t  p_39, int16_t  p_40, int32_t * p_41, int16_t  p_42, int8_t  p_43)
{ /* block id: 7 */
    return g_55;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_15, "g_15", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_32[i].f0, "g_32[i].f0", print_hash_value);
        transparent_crc(g_32[i].f1, "g_32[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_37[i], "g_37[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_48, "g_48", print_hash_value);
    transparent_crc(g_54, "g_54", print_hash_value);
    transparent_crc(g_55.f0, "g_55.f0", print_hash_value);
    transparent_crc(g_55.f1, "g_55.f1", print_hash_value);
    transparent_crc(g_57.f0, "g_57.f0", print_hash_value);
    transparent_crc(g_57.f1, "g_57.f1", print_hash_value);
    transparent_crc(g_79, "g_79", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    transparent_crc(g_123.f0, "g_123.f0", print_hash_value);
    transparent_crc(g_123.f1, "g_123.f1", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_129[i], "g_129[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_134, sizeof(g_134), "g_134", print_hash_value);
    transparent_crc(g_170, "g_170", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc_bytes(&g_233[i][j], sizeof(g_233[i][j]), "g_233[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_245, "g_245", print_hash_value);
    transparent_crc(g_251, "g_251", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_258[i], "g_258[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_261, "g_261", print_hash_value);
    transparent_crc(g_263, "g_263", print_hash_value);
    transparent_crc(g_303, "g_303", print_hash_value);
    transparent_crc(g_374, "g_374", print_hash_value);
    transparent_crc(g_396.f0, "g_396.f0", print_hash_value);
    transparent_crc(g_396.f1, "g_396.f1", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_401[i], "g_401[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_427, "g_427", print_hash_value);
    transparent_crc(g_432, "g_432", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc_bytes(&g_453[i][j], sizeof(g_453[i][j]), "g_453[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_493, "g_493", print_hash_value);
    transparent_crc(g_511, "g_511", print_hash_value);
    transparent_crc(g_621, "g_621", print_hash_value);
    transparent_crc_bytes (&g_636, sizeof(g_636), "g_636", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_686[i][j], "g_686[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_691.f0, "g_691.f0", print_hash_value);
    transparent_crc(g_691.f1, "g_691.f1", print_hash_value);
    transparent_crc(g_693.f0, "g_693.f0", print_hash_value);
    transparent_crc(g_693.f1, "g_693.f1", print_hash_value);
    transparent_crc(g_694.f0, "g_694.f0", print_hash_value);
    transparent_crc(g_694.f1, "g_694.f1", print_hash_value);
    transparent_crc(g_737.f0, "g_737.f0", print_hash_value);
    transparent_crc(g_737.f1, "g_737.f1", print_hash_value);
    transparent_crc(g_743, "g_743", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_769[i].f0, "g_769[i].f0", print_hash_value);
        transparent_crc(g_769[i].f1, "g_769[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_770[i].f0, "g_770[i].f0", print_hash_value);
        transparent_crc(g_770[i].f1, "g_770[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_787.f0, "g_787.f0", print_hash_value);
    transparent_crc(g_787.f1, "g_787.f1", print_hash_value);
    transparent_crc(g_798.f0, "g_798.f0", print_hash_value);
    transparent_crc(g_798.f1, "g_798.f1", print_hash_value);
    transparent_crc(g_873, "g_873", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc_bytes(&g_876[i], sizeof(g_876[i]), "g_876[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_934[i][j], "g_934[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_959.f0, "g_959.f0", print_hash_value);
    transparent_crc(g_959.f1, "g_959.f1", print_hash_value);
    transparent_crc(g_1074, "g_1074", print_hash_value);
    transparent_crc(g_1124.f0, "g_1124.f0", print_hash_value);
    transparent_crc(g_1124.f1, "g_1124.f1", print_hash_value);
    transparent_crc(g_1164, "g_1164", print_hash_value);
    transparent_crc(g_1185, "g_1185", print_hash_value);
    transparent_crc(g_1245, "g_1245", print_hash_value);
    transparent_crc(g_1295.f0, "g_1295.f0", print_hash_value);
    transparent_crc(g_1295.f1, "g_1295.f1", print_hash_value);
    transparent_crc(g_1318.f0, "g_1318.f0", print_hash_value);
    transparent_crc(g_1318.f1, "g_1318.f1", print_hash_value);
    transparent_crc(g_1370, "g_1370", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1371[i][j][k], "g_1371[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1372, "g_1372", print_hash_value);
    transparent_crc(g_1373, "g_1373", print_hash_value);
    transparent_crc(g_1374, "g_1374", print_hash_value);
    transparent_crc(g_1375, "g_1375", print_hash_value);
    transparent_crc(g_1376, "g_1376", print_hash_value);
    transparent_crc(g_1377, "g_1377", print_hash_value);
    transparent_crc(g_1378, "g_1378", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1379[i], "g_1379[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1380, "g_1380", print_hash_value);
    transparent_crc(g_1381, "g_1381", print_hash_value);
    transparent_crc(g_1382, "g_1382", print_hash_value);
    transparent_crc(g_1383, "g_1383", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1384[i], "g_1384[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1385, "g_1385", print_hash_value);
    transparent_crc(g_1386, "g_1386", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1387[i], "g_1387[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1388, "g_1388", print_hash_value);
    transparent_crc(g_1389, "g_1389", print_hash_value);
    transparent_crc(g_1390, "g_1390", print_hash_value);
    transparent_crc(g_1391, "g_1391", print_hash_value);
    transparent_crc(g_1392, "g_1392", print_hash_value);
    transparent_crc(g_1393, "g_1393", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1394[i], "g_1394[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1395, "g_1395", print_hash_value);
    transparent_crc(g_1396, "g_1396", print_hash_value);
    transparent_crc(g_1397, "g_1397", print_hash_value);
    transparent_crc(g_1398, "g_1398", print_hash_value);
    transparent_crc(g_1399, "g_1399", print_hash_value);
    transparent_crc(g_1400, "g_1400", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_1401[i][j], "g_1401[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1402, "g_1402", print_hash_value);
    transparent_crc(g_1403, "g_1403", print_hash_value);
    transparent_crc(g_1406, "g_1406", print_hash_value);
    transparent_crc(g_1554.f0, "g_1554.f0", print_hash_value);
    transparent_crc(g_1554.f1, "g_1554.f1", print_hash_value);
    transparent_crc(g_1594, "g_1594", print_hash_value);
    transparent_crc(g_1595, "g_1595", print_hash_value);
    transparent_crc(g_1718.f0, "g_1718.f0", print_hash_value);
    transparent_crc(g_1718.f1, "g_1718.f1", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_1818[i][j].f0, "g_1818[i][j].f0", print_hash_value);
            transparent_crc(g_1818[i][j].f1, "g_1818[i][j].f1", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1851.f0, "g_1851.f0", print_hash_value);
    transparent_crc(g_1851.f1, "g_1851.f1", print_hash_value);
    transparent_crc(g_1921, "g_1921", print_hash_value);
    transparent_crc(g_1949, "g_1949", print_hash_value);
    transparent_crc(g_1993.f0, "g_1993.f0", print_hash_value);
    transparent_crc(g_1993.f1, "g_1993.f1", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1994[i].f0, "g_1994[i].f0", print_hash_value);
        transparent_crc(g_1994[i].f1, "g_1994[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2036, "g_2036", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 464
   depth: 1, occurrence: 22
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 2
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 53
breakdown:
   indirect level: 0, occurrence: 22
   indirect level: 1, occurrence: 13
   indirect level: 2, occurrence: 6
   indirect level: 3, occurrence: 6
   indirect level: 4, occurrence: 5
   indirect level: 5, occurrence: 1
XXX full-bitfields structs in the program: 22
breakdown:
   indirect level: 0, occurrence: 22
XXX times a bitfields struct's address is taken: 32
XXX times a bitfields struct on LHS: 5
XXX times a bitfields struct on RHS: 23
XXX times a single bitfield on LHS: 2
XXX times a single bitfield on RHS: 60

XXX max expression depth: 39
breakdown:
   depth: 1, occurrence: 92
   depth: 2, occurrence: 30
   depth: 3, occurrence: 1
   depth: 4, occurrence: 2
   depth: 6, occurrence: 2
   depth: 10, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 21, occurrence: 1
   depth: 23, occurrence: 2
   depth: 24, occurrence: 1
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 29, occurrence: 1
   depth: 31, occurrence: 1
   depth: 39, occurrence: 1

XXX total number of pointers: 469

XXX times a variable address is taken: 1110
XXX times a pointer is dereferenced on RHS: 207
breakdown:
   depth: 1, occurrence: 163
   depth: 2, occurrence: 42
   depth: 3, occurrence: 2
XXX times a pointer is dereferenced on LHS: 285
breakdown:
   depth: 1, occurrence: 252
   depth: 2, occurrence: 30
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 30
XXX times a pointer is compared with address of another variable: 11
XXX times a pointer is compared with another pointer: 12
XXX times a pointer is qualified to be dereferenced: 5926

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 994
   level: 2, occurrence: 285
   level: 3, occurrence: 54
   level: 4, occurrence: 26
   level: 5, occurrence: 5
XXX number of pointers point to pointers: 178
XXX number of pointers point to scalars: 268
XXX number of pointers point to structs: 23
XXX percent of pointers has null in alias set: 30.5
XXX average alias set size: 1.52

XXX times a non-volatile is read: 1605
XXX times a non-volatile is write: 831
XXX times a volatile is read: 82
XXX    times read thru a pointer: 5
XXX times a volatile is write: 47
XXX    times written thru a pointer: 20
XXX times a volatile is available for access: 4.76e+03
XXX percentage of non-volatile access: 95

XXX forward jumps: 0
XXX backward jumps: 6

XXX stmts: 102
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 26
   depth: 1, occurrence: 20
   depth: 2, occurrence: 24
   depth: 3, occurrence: 12
   depth: 4, occurrence: 15
   depth: 5, occurrence: 5

XXX percentage a fresh-made variable is used: 17.4
XXX percentage an existing variable is used: 82.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3617227754
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   int8_t  f0;
   float  f1;
   uint64_t  f2;
};

union U1 {
   uint16_t  f0;
   const int32_t  f1;
};

union U2 {
   const volatile int32_t  f0;
};

union U3 {
   unsigned f0 : 6;
   uint32_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2 = 0x9C3FBB4EL;/* VOLATILE GLOBAL g_2 */
static volatile int32_t g_3 = (-1L);/* VOLATILE GLOBAL g_3 */
static volatile int32_t g_4 = 0L;/* VOLATILE GLOBAL g_4 */
static int32_t g_5 = 0x31551DA6L;
static int32_t g_8 = 1L;
static int32_t g_12[6][9][4] = {{{0x996E1580L,7L,0xA9733147L,0x6CD219FAL},{0x095DBC2AL,1L,0x005AB136L,0L},{1L,5L,0x80090AE7L,0x8E10A3BDL},{(-1L),8L,0x8E10A3BDL,1L},{0xC8C5CECCL,0xC3B03C3FL,4L,0x996E1580L},{0L,2L,0x96947A33L,0L},{0x095DBC2AL,0x5D2D9150L,0x92758E92L,0x5E45DB0FL},{0x80090AE7L,0xA5B4D633L,0xB60F9AA2L,0x92758E92L},{0x4A08DEB2L,0xCF13E13AL,0L,0xDFE012D2L}},{{5L,0x005AB136L,5L,0x1E707165L},{0xC3B03C3FL,2L,(-1L),4L},{0L,(-1L),7L,0x9F4802D1L},{0L,0xC8C5CECCL,0xAFA0B2B0L,0x80090AE7L},{1L,0xB60F9AA2L,0xB60F9AA2L,1L},{0x83CE7179L,0xA6001648L,0x0A165018L,1L},{(-1L),4L,0x9F4802D1L,(-1L)},{(-7L),0x0F213ED4L,0xC9CD49FCL,(-1L)},{5L,4L,0x5E45DB0FL,1L}},{{0L,0xA6001648L,2L,1L},{0x92758E92L,0xB60F9AA2L,0xA5B4D633L,0x80090AE7L},{(-1L),0xC8C5CECCL,0x0457A23AL,0x9F4802D1L},{0x0F213ED4L,0x83CE7179L,0x131E34F9L,0xA5B4D633L},{0xA9733147L,(-9L),(-1L),7L},{0L,2L,0xDFE012D2L,0L},{0xCF13E13AL,(-1L),(-8L),0xDFE012D2L},{0xDFE012D2L,0x3D2CF3E4L,0x9F4802D1L,0x3EFC1676L},{(-1L),8L,(-1L),0x9D9E62B5L}},{{(-1L),(-9L),0x59842251L,0x131E34F9L},{1L,0x78B15B45L,7L,0xA9733147L},{8L,(-1L),0x9D9E62B5L,0x80090AE7L},{0x3C9E4A04L,0x4071E994L,(-10L),0x02AD9236L},{0x83CE7179L,8L,(-1L),2L},{(-1L),4L,0xA9733147L,(-1L)},{0x5E45DB0FL,(-1L),0xC9CD49FCL,(-6L)},{0xCF13E13AL,1L,(-4L),1L},{1L,0L,(-1L),0x02AD9236L}},{{0x92758E92L,1L,0x131E34F9L,0x0457A23AL},{0x6CD219FAL,0xC8C5CECCL,4L,0x4A08DEB2L},{0x6CD219FAL,0x78B15B45L,0x131E34F9L,5L},{0x92758E92L,0x4A08DEB2L,(-1L),7L},{1L,0xC1348862L,(-4L),0x3EFC1676L},{0xCF13E13AL,(-1L),0xC9CD49FCL,(-4L)},{0x5E45DB0FL,0x3D2CF3E4L,0xA9733147L,0xA6001648L},{(-1L),2L,(-1L),0xAFA0B2B0L},{0x83CE7179L,0x4A08DEB2L,(-10L),0x131E34F9L}},{{0x3C9E4A04L,0x5EBDA184L,0x9D9E62B5L,0x9F4802D1L},{8L,0xBCB1A7A8L,7L,0x0457A23AL},{1L,0x4071E994L,0x59842251L,0x3C9E4A04L},{(-1L),0xA6001648L,(-1L),(-1L)},{(-1L),1L,0x9F4802D1L,(-1L)},{0xDFE012D2L,0xA59E478DL,(-8L),(-1L)},{0xCF13E13AL,0L,0xDFE012D2L,2L},{0L,0L,(-1L),0x3C9E4A04L},{0xA9733147L,0xB60F9AA2L,0x131E34F9L,4L}}};
static volatile int32_t g_28 = 0x5FF9B037L;/* VOLATILE GLOBAL g_28 */
static int32_t g_29 = 0xDB28543BL;
static volatile int32_t g_32 = 0x0B879B68L;/* VOLATILE GLOBAL g_32 */
static volatile int32_t g_33 = 7L;/* VOLATILE GLOBAL g_33 */
static volatile int32_t g_34 = 0xC4999369L;/* VOLATILE GLOBAL g_34 */
static volatile int32_t g_35 = (-4L);/* VOLATILE GLOBAL g_35 */
static int32_t g_36 = 0x1012106DL;
static int32_t *g_42 = &g_36;
static union U1 g_60 = {3UL};
static union U2 g_81 = {1L};/* VOLATILE GLOBAL g_81 */
static uint8_t g_83[4][3][9] = {{{0xE0L,0x22L,250UL,255UL,255UL,250UL,0x22L,0xE0L,0xE0L},{4UL,9UL,4UL,0UL,4UL,9UL,4UL,0UL,4UL},{0xE0L,255UL,0x22L,0x22L,255UL,0xE0L,250UL,250UL,0xE0L}},{{1UL,0UL,255UL,0UL,1UL,0UL,255UL,0UL,1UL},{255UL,0x22L,0x22L,255UL,0xE0L,250UL,250UL,0xE0L,255UL},{4UL,0UL,4UL,9UL,4UL,0UL,4UL,9UL,4UL}},{{255UL,255UL,250UL,0x22L,0xE0L,0xE0L,0x22L,250UL,255UL},{1UL,9UL,255UL,9UL,1UL,9UL,255UL,9UL,1UL},{0xE0L,0x22L,250UL,255UL,255UL,250UL,0x22L,0xE0L,0xE0L}},{{4UL,9UL,4UL,0UL,4UL,9UL,4UL,0UL,4UL},{0xE0L,255UL,0x22L,0x22L,255UL,0xE0L,250UL,250UL,0xE0L},{1UL,0UL,255UL,0UL,1UL,0UL,255UL,0UL,1UL}}};
static int64_t g_90 = 0L;
static float g_91 = 0x1.4p+1;
static int8_t g_93 = (-6L);
static uint32_t g_99 = 0x4A66755DL;
static int8_t *g_104 = &g_93;
static uint8_t g_141[1] = {1UL};
static uint64_t g_142 = 1UL;
static uint16_t * volatile g_145 = &g_60.f0;/* VOLATILE GLOBAL g_145 */
static int64_t g_187[10] = {0xEE14A350FA80559ALL,1L,0xEE14A350FA80559ALL,1L,0xEE14A350FA80559ALL,1L,0xEE14A350FA80559ALL,1L,0xEE14A350FA80559ALL,1L};
static int16_t g_210 = (-9L);
static union U2 g_223[1] = {{0xEFDAD2A3L}};
static float * volatile g_232 = &g_91;/* VOLATILE GLOBAL g_232 */
static volatile int16_t g_236[3][4] = {{0xDF23L,0xDF23L,0xDF23L,0xDF23L},{0xDF23L,0xDF23L,0xDF23L,0xDF23L},{0xDF23L,0xDF23L,0xDF23L,0xDF23L}};
static union U1 *g_243[1] = {&g_60};
static union U1 ** volatile g_242 = &g_243[0];/* VOLATILE GLOBAL g_242 */
static union U1 ** volatile g_245 = &g_243[0];/* VOLATILE GLOBAL g_245 */
static union U2 ** volatile g_246 = (void*)0;/* VOLATILE GLOBAL g_246 */
static union U2 *g_247 = (void*)0;
static const volatile union U3 *g_263 = (void*)0;
static const volatile union U3 * volatile * volatile g_262[9] = {&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263,&g_263};
static union U2 g_336 = {-1L};/* VOLATILE GLOBAL g_336 */
static volatile int16_t * volatile g_339[8][6] = {{&g_236[1][3],(void*)0,&g_236[1][0],&g_236[0][0],(void*)0,&g_236[1][1]},{(void*)0,&g_236[1][1],(void*)0,&g_236[1][0],&g_236[2][1],(void*)0},{(void*)0,&g_236[2][3],&g_236[1][0],&g_236[0][0],&g_236[1][0],&g_236[2][3]},{&g_236[1][3],(void*)0,&g_236[2][3],&g_236[1][1],&g_236[2][3],&g_236[1][0]},{&g_236[1][1],&g_236[2][1],&g_236[0][0],&g_236[2][3],&g_236[2][1],&g_236[1][0]},{&g_236[1][1],&g_236[2][1],(void*)0,&g_236[2][3],&g_236[2][3],(void*)0},{(void*)0,(void*)0,&g_236[2][1],&g_236[1][1],&g_236[1][0],(void*)0},{&g_236[1][1],&g_236[2][3],(void*)0,&g_236[1][1],&g_236[2][1],&g_236[2][1]}};
static volatile int16_t * volatile * volatile g_338 = &g_339[3][1];/* VOLATILE GLOBAL g_338 */
static float g_369 = 0x0.9p+1;
static float * volatile g_368 = &g_369;/* VOLATILE GLOBAL g_368 */
static int64_t * volatile * const  volatile g_413 = (void*)0;/* VOLATILE GLOBAL g_413 */
static int32_t g_418 = (-3L);
static int32_t *g_421[6][8] = {{(void*)0,&g_36,&g_5,&g_36,(void*)0,&g_36,&g_36,&g_12[0][8][0]},{&g_36,(void*)0,&g_12[3][1][2],&g_5,&g_36,&g_36,&g_36,&g_36},{&g_36,&g_12[3][1][2],&g_12[3][1][2],&g_36,(void*)0,&g_5,&g_36,&g_36},{&g_36,&g_36,&g_12[0][8][0],(void*)0,&g_36,&g_36,&g_36,(void*)0},{&g_5,&g_36,&g_5,&g_36,&g_36,&g_5,&g_36,&g_5},{&g_12[3][1][2],&g_36,&g_36,&g_36,&g_8,&g_8,&g_36,&g_36}};
static union U0 g_434 = {0x83L};
static union U0 *g_448 = &g_434;
static union U0 * volatile *g_447 = &g_448;
static union U0 * volatile ** const  volatile g_446 = &g_447;/* VOLATILE GLOBAL g_446 */
static union U0 * volatile ** volatile g_450 = (void*)0;/* VOLATILE GLOBAL g_450 */
static union U0 * volatile ** volatile * volatile g_449 = &g_450;/* VOLATILE GLOBAL g_449 */
static int8_t g_479 = 0xC6L;
static int32_t g_481 = 0x3590FEDEL;
static volatile int16_t g_519 = 0xF973L;/* VOLATILE GLOBAL g_519 */
static float * volatile g_524[2] = {&g_91,&g_91};
static float * volatile g_525 = &g_369;/* VOLATILE GLOBAL g_525 */
static volatile uint8_t g_527[8][1] = {{254UL},{255UL},{254UL},{255UL},{254UL},{255UL},{254UL},{255UL}};
static int64_t **g_532 = (void*)0;
static uint16_t *g_634 = &g_60.f0;
static union U2 g_640[6] = {{0x9E693314L},{0x9E693314L},{0x9E693314L},{0x9E693314L},{0x9E693314L},{0x9E693314L}};
static union U2 g_643[4][3][7] = {{{{0x9B9192D7L},{0x6648F396L},{0x6648F396L},{0x9B9192D7L},{0x6648F396L},{0x6648F396L},{0x9B9192D7L}},{{0xC971874EL},{0x961AEEACL},{0xC971874EL},{0L},{0x7709B3EBL},{0L},{0xC971874EL}},{{0x9B9192D7L},{0x9B9192D7L},{0xCEC8D71AL},{0x9B9192D7L},{0x9B9192D7L},{0xCEC8D71AL},{0x9B9192D7L}}},{{{0x7709B3EBL},{0L},{0xC971874EL},{0x961AEEACL},{0xC971874EL},{0L},{0x7709B3EBL}},{{0x6648F396L},{0x9B9192D7L},{0x6648F396L},{0x6648F396L},{0x9B9192D7L},{0x6648F396L},{0x6648F396L}},{{0x7709B3EBL},{0x961AEEACL},{0L},{0x961AEEACL},{0x7709B3EBL},{0x7604E481L},{0x7709B3EBL}}},{{{0x9B9192D7L},{0x6648F396L},{0x6648F396L},{0x9B9192D7L},{0x6648F396L},{0x6648F396L},{0x9B9192D7L}},{{0xC971874EL},{0x961AEEACL},{0xC971874EL},{0L},{0x7709B3EBL},{0L},{0xC971874EL}},{{0x9B9192D7L},{0x9B9192D7L},{0xCEC8D71AL},{0x9B9192D7L},{0x9B9192D7L},{0xCEC8D71AL},{0x9B9192D7L}}},{{{0x7709B3EBL},{0L},{0xC971874EL},{0x961AEEACL},{0xC971874EL},{0L},{0x7709B3EBL}},{{0x6648F396L},{0x9B9192D7L},{0x6648F396L},{0x6648F396L},{0x9B9192D7L},{0x6648F396L},{0x6648F396L}},{{0x7709B3EBL},{0x961AEEACL},{0L},{0x961AEEACL},{0x7709B3EBL},{0x7604E481L},{0x7709B3EBL}}}};
static float * volatile g_658 = &g_91;/* VOLATILE GLOBAL g_658 */
static const uint16_t g_693 = 0x5343L;
static union U3 g_721 = {4294967295UL};
static uint16_t g_810 = 0UL;
static uint32_t *g_851 = &g_99;
static uint32_t *g_852 = &g_99;
static int8_t g_854[4][6] = {{0xB0L,(-1L),(-1L),0xB0L,(-1L),(-1L)},{0xB0L,(-1L),(-1L),0xB0L,(-1L),(-1L)},{0xB0L,(-1L),(-1L),0xB0L,(-1L),(-1L)},{0xB0L,(-1L),(-1L),0xB0L,(-1L),(-1L)}};
static const int16_t g_866[10][9] = {{0x533BL,8L,0x2F26L,(-1L),0x2F26L,8L,0x533BL,0x533BL,8L},{(-1L),8L,0xC539L,8L,(-1L),(-2L),(-2L),(-1L),8L},{0x533BL,0x2F26L,0x533BL,(-2L),0xC539L,0xC539L,(-2L),0x533BL,0x2F26L},{0x2F26L,0x533BL,(-2L),0xC539L,0xC539L,(-2L),0x533BL,0x2F26L,0x533BL},{8L,(-1L),(-2L),(-2L),(-2L),0x533BL,0x2F26L,0x533BL,(-2L)},{0x533BL,(-1L),(-1L),0x533BL,4L,(-2L),4L,0x533BL,(-1L)},{4L,4L,0x2F26L,(-2L),8L,(-2L),0x2F26L,4L,4L},{(-1L),0x533BL,4L,(-2L),4L,0x533BL,(-1L),(-1L),0x533BL},{(-2L),0x533BL,0x2F26L,0x533BL,(-2L),0xC539L,0xC539L,(-2L),0x533BL},{(-1L),4L,(-1L),0xC539L,0x2F26L,0x2F26L,0xC539L,(-1L),4L}};
static const int16_t *g_865 = &g_866[5][2];
static int32_t **g_879 = &g_421[0][2];
static int32_t ***g_878 = &g_879;
static int32_t **** volatile g_877 = &g_878;/* VOLATILE GLOBAL g_877 */
static int16_t *g_907 = &g_210;
static float * volatile g_931 = &g_369;/* VOLATILE GLOBAL g_931 */
static volatile union U2 g_982 = {7L};/* VOLATILE GLOBAL g_982 */
static const volatile union U2 g_1008 = {1L};/* VOLATILE GLOBAL g_1008 */
static int16_t g_1012 = (-1L);
static int16_t * volatile * volatile g_1029 = &g_907;/* VOLATILE GLOBAL g_1029 */
static int16_t * volatile * volatile *g_1028 = &g_1029;
static uint64_t g_1030 = 0xF9DA9E0F60AB519ELL;
static float * volatile g_1032 = &g_369;/* VOLATILE GLOBAL g_1032 */
static union U0 **g_1042 = &g_448;
static union U0 ***g_1041 = &g_1042;
static int64_t g_1082 = (-6L);
static int64_t g_1083 = (-4L);
static int32_t g_1086[8][2][9] = {{{9L,0L,0x0234FE4BL,0x9F799802L,0x315D6B75L,1L,0xF69BC4F1L,1L,0x315D6B75L},{0L,0L,0L,0L,0L,2L,1L,0xC9C42026L,0x315D6B75L}},{{0xD4233091L,0L,0x67C43565L,0xF69BC4F1L,0x7579BFD5L,0L,(-9L),0xAC700531L,0L},{(-3L),5L,0xE3E4D2ADL,0xE3CE20C3L,0L,0xC9C42026L,3L,0x67C43565L,0L}},{{(-3L),2L,1L,0xD4233091L,0x315D6B75L,0L,0x88C53FC8L,1L,(-10L)},{0xD4233091L,0xAC700531L,0x74A33494L,1L,0xAC700531L,0xC9C42026L,9L,0L,0x0234FE4BL}},{{0L,0xAC700531L,1L,0L,(-10L),0L,1L,5L,5L},{9L,2L,0L,4L,0L,2L,9L,0x74A33494L,1L}},{{3L,5L,0L,9L,0x67C43565L,1L,0x88C53FC8L,2L,0xC9C42026L},{1L,0L,1L,0x93354F34L,2L,1L,3L,0x74A33494L,0x7579BFD5L}},{{(-1L),0L,0x74A33494L,0x93354F34L,0xE3E4D2ADL,0x7579BFD5L,(-9L),5L,1L},{0x8801E29FL,0L,1L,9L,0x0234FE4BL,0x55293233L,1L,0L,0x7579BFD5L}},{{0x88C53FC8L,0x55293233L,0xE3E4D2ADL,4L,0x0234FE4BL,0xAC700531L,0xF69BC4F1L,1L,0xC9C42026L},{0xE3CE20C3L,1L,0x67C43565L,0L,0xE3E4D2ADL,0xE3E4D2ADL,0L,0x67C43565L,1L}},{{0x88C53FC8L,0x0234FE4BL,0L,1L,2L,0xE3E4D2ADL,0x8801E29FL,0xAC700531L,5L},{0x8801E29FL,0x315D6B75L,0x0234FE4BL,0xD4233091L,0x67C43565L,0xAC700531L,0x74A33494L,2L,0xC454F0BCL}}};
static uint32_t g_1087 = 18446744073709551615UL;
static union U3 *g_1093 = &g_721;
static volatile uint32_t g_1097 = 0xA443D014L;/* VOLATILE GLOBAL g_1097 */
static volatile uint32_t *g_1096 = &g_1097;
static volatile uint64_t g_1155 = 1UL;/* VOLATILE GLOBAL g_1155 */
static union U2 g_1183 = {1L};/* VOLATILE GLOBAL g_1183 */
static union U2 g_1191 = {8L};/* VOLATILE GLOBAL g_1191 */
static int8_t g_1194 = 0x5DL;
static volatile union U2 g_1231 = {0x457D696BL};/* VOLATILE GLOBAL g_1231 */
static const int32_t *g_1252 = &g_12[4][3][0];
static union U2 g_1256 = {3L};/* VOLATILE GLOBAL g_1256 */
static int32_t *g_1267 = &g_481;
static volatile union U2 g_1331[1][4][7] = {{{{1L},{0xE0E5951FL},{0xE0E5951FL},{1L},{4L},{0x252A992AL},{4L}},{{0L},{0x420C8286L},{0x420C8286L},{0L},{8L},{0xFCD51A8BL},{8L}},{{1L},{0xE0E5951FL},{0xE0E5951FL},{1L},{4L},{0x252A992AL},{4L}},{{0L},{0x420C8286L},{0x420C8286L},{0L},{8L},{0xFCD51A8BL},{8L}}}};
static uint32_t *g_1349 = (void*)0;
static int64_t g_1352 = 1L;
static volatile uint8_t g_1371 = 250UL;/* VOLATILE GLOBAL g_1371 */
static volatile union U2 g_1422 = {-4L};/* VOLATILE GLOBAL g_1422 */
static volatile union U2 g_1427 = {0xF37639CCL};/* VOLATILE GLOBAL g_1427 */
static float * volatile g_1467[1][2][10] = {{{(void*)0,&g_91,&g_91,(void*)0,(void*)0,&g_91,&g_91,(void*)0,(void*)0,&g_91},{(void*)0,(void*)0,&g_91,&g_91,(void*)0,(void*)0,&g_91,&g_91,(void*)0,(void*)0}}};
static float g_1536[4][6] = {{0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1),0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1)},{0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1),0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1)},{0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1),0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1)},{0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1),0x8.896CD7p-9,(-0x3.Ep+1),(-0x3.Ep+1)}};
static float * volatile g_1535 = &g_1536[1][2];/* VOLATILE GLOBAL g_1535 */
static uint16_t g_1627 = 0x2936L;
static uint16_t * const  volatile *g_1654 = (void*)0;
static uint16_t * const  volatile * volatile * volatile g_1653 = &g_1654;/* VOLATILE GLOBAL g_1653 */
static volatile uint32_t **g_1674 = &g_1096;
static volatile uint32_t *** volatile g_1673 = &g_1674;/* VOLATILE GLOBAL g_1673 */
static uint8_t g_1729 = 0UL;
static float *g_1743[4][8][1] = {{{&g_1536[1][2]},{&g_434.f1},{&g_1536[1][2]},{&g_434.f1},{&g_1536[0][1]},{&g_1536[1][2]},{&g_1536[0][1]},{&g_1536[1][2]}},{{&g_434.f1},{&g_434.f1},{&g_1536[1][2]},{&g_1536[0][1]},{&g_1536[1][2]},{&g_1536[0][1]},{&g_434.f1},{&g_1536[1][2]}},{{&g_434.f1},{&g_1536[1][2]},{&g_434.f1},{&g_1536[0][1]},{&g_1536[1][2]},{&g_1536[0][1]},{&g_1536[1][2]},{&g_434.f1}},{{&g_434.f1},{&g_1536[1][2]},{&g_1536[0][1]},{&g_1536[1][2]},{&g_1536[0][1]},{&g_434.f1},{&g_1536[1][2]},{&g_434.f1}}};
static float **g_1742 = &g_1743[0][6][0];
static float ***g_1741 = &g_1742;
static float **** volatile g_1740 = &g_1741;/* VOLATILE GLOBAL g_1740 */
static const uint32_t g_1770 = 0xCB1EAF4AL;
static uint32_t g_1774[8] = {1UL,1UL,0x326BAAEEL,1UL,1UL,0x326BAAEEL,1UL,1UL};
static int32_t ****g_1801 = &g_878;
static int16_t ** const **g_1832 = (void*)0;
static int16_t ** const *** volatile g_1831 = &g_1832;/* VOLATILE GLOBAL g_1831 */
static int16_t **g_1893 = &g_907;
static int16_t ***g_1892 = &g_1893;
static int16_t ****g_1891 = &g_1892;
static int16_t *****g_1890[5] = {&g_1891,&g_1891,&g_1891,&g_1891,&g_1891};
static uint8_t *g_1974 = &g_83[0][0][1];
static volatile union U2 g_2010 = {-7L};/* VOLATILE GLOBAL g_2010 */
static union U1 g_2024 = {0xA340L};
static union U1 *g_2023 = &g_2024;
static volatile int16_t g_2055[7][1][3] = {{{8L,8L,8L}},{{8L,8L,8L}},{{8L,8L,8L}},{{8L,8L,8L}},{{8L,8L,8L}},{{8L,8L,8L}},{{8L,8L,8L}}};
static union U1 **g_2076 = &g_243[0];
static union U1 ***g_2075 = &g_2076;
static union U2 **** volatile g_2082 = (void*)0;/* VOLATILE GLOBAL g_2082 */
static uint32_t **g_2158 = &g_851;
static uint32_t ***g_2157 = &g_2158;
static uint16_t ***g_2242 = (void*)0;
static int16_t g_2291 = 0x062DL;
static uint32_t g_2292 = 0UL;
static volatile union U2 g_2373[4] = {{0x904C67DCL},{0x904C67DCL},{0x904C67DCL},{0x904C67DCL}};
static volatile union U2 g_2414[10] = {{-7L},{0xB5FD3911L},{-7L},{-7L},{0xB5FD3911L},{-7L},{-7L},{0xB5FD3911L},{-7L},{-7L}};
static union U3 ** volatile g_2432[10][1] = {{&g_1093},{&g_1093},{&g_1093},{&g_1093},{&g_1093},{&g_1093},{&g_1093},{&g_1093},{&g_1093},{&g_1093}};
static union U3 ** volatile g_2433 = &g_1093;/* VOLATILE GLOBAL g_2433 */
static volatile uint64_t g_2460 = 1UL;/* VOLATILE GLOBAL g_2460 */
static volatile union U2 g_2625[10][10][2] = {{{{0xF7B01DCBL},{7L}},{{0xF7B01DCBL},{1L}},{{0x6D3B504CL},{0x5B5918B0L}},{{1L},{0x74EB19AEL}},{{-2L},{-1L}},{{0xFFE81D1EL},{0x14A50148L}},{{0x14A50148L},{0xF90C52FBL}},{{-1L},{0x5B5918B0L}},{{1L},{1L}},{{-1L},{0xF90C52FBL}}},{{{0xF7B01DCBL},{0L}},{{0xFFE81D1EL},{1L}},{{-1L},{0x74EB19AEL}},{{0x6D3B504CL},{0xFFE81D1EL}},{{-1L},{0x6D3B504CL}},{{1L},{0L}},{{0xF90C52FBL},{1L}},{{-1L},{7L}},{{1L},{-5L}},{{7L},{0x3D42A87EL}}},{{{1L},{0xF765F64BL}},{{0xF90C52FBL},{1L}},{{0xD2DEEA46L},{0xFFE81D1EL}},{{0L},{0xFFE81D1EL}},{{0xD2DEEA46L},{1L}},{{0xF90C52FBL},{0xF765F64BL}},{{1L},{0x3D42A87EL}},{{7L},{-5L}},{{1L},{7L}},{{-1L},{1L}}},{{{0xF90C52FBL},{0L}},{{1L},{0x6D3B504CL}},{{-1L},{0xFFE81D1EL}},{{0x6D3B504CL},{-5L}},{{0L},{1L}},{{1L},{0xF90C52FBL}},{{0xF765F64BL},{1L}},{{7L},{7L}},{{0L},{-8L}},{{0L},{1L}}},{{{1L},{1L}},{{1L},{0x6D3B504CL}},{{0xD2DEEA46L},{-5L}},{{0x3D42A87EL},{-8L}},{{-1L},{0x3D42A87EL}},{{0xF765F64BL},{0L}},{{0xF765F64BL},{0x3D42A87EL}},{{-1L},{-8L}},{{0x3D42A87EL},{-5L}},{{0xD2DEEA46L},{0x6D3B504CL}}},{{{1L},{1L}},{{1L},{1L}},{{0L},{-8L}},{{0L},{7L}},{{7L},{1L}},{{0xF765F64BL},{0xF90C52FBL}},{{1L},{1L}},{{0L},{-5L}},{{0x6D3B504CL},{0xFFE81D1EL}},{{-1L},{0x6D3B504CL}}},{{{1L},{0L}},{{0xF90C52FBL},{1L}},{{-1L},{7L}},{{1L},{-5L}},{{7L},{0x3D42A87EL}},{{1L},{0xF765F64BL}},{{0xF90C52FBL},{1L}},{{0xD2DEEA46L},{0xFFE81D1EL}},{{0L},{0xFFE81D1EL}},{{0xD2DEEA46L},{1L}}},{{{0xF90C52FBL},{0xF765F64BL}},{{1L},{0x3D42A87EL}},{{7L},{-5L}},{{1L},{7L}},{{-1L},{1L}},{{0xF90C52FBL},{0L}},{{1L},{0x6D3B504CL}},{{-1L},{0xFFE81D1EL}},{{0x6D3B504CL},{-5L}},{{0L},{1L}}},{{{1L},{0xF90C52FBL}},{{0xF765F64BL},{1L}},{{7L},{7L}},{{0L},{-8L}},{{0L},{1L}},{{1L},{1L}},{{1L},{0x6D3B504CL}},{{0xD2DEEA46L},{-5L}},{{0x3D42A87EL},{-8L}},{{-1L},{0x3D42A87EL}}},{{{0xF765F64BL},{0L}},{{0xF765F64BL},{0x3D42A87EL}},{{-1L},{-8L}},{{0x3D42A87EL},{-5L}},{{0xD2DEEA46L},{0x6D3B504CL}},{{1L},{1L}},{{1L},{1L}},{{0L},{-8L}},{{0L},{7L}},{{7L},{1L}}}};
static union U0 * const *g_2687[2][1] = {{(void*)0},{(void*)0}};
static union U2 g_2697 = {4L};/* VOLATILE GLOBAL g_2697 */
static int64_t *g_2702 = &g_1083;
static int64_t * volatile *g_2701[6][5] = {{&g_2702,&g_2702,&g_2702,&g_2702,&g_2702},{&g_2702,&g_2702,&g_2702,&g_2702,&g_2702},{&g_2702,&g_2702,&g_2702,(void*)0,&g_2702},{&g_2702,&g_2702,&g_2702,(void*)0,&g_2702},{&g_2702,&g_2702,&g_2702,&g_2702,&g_2702},{&g_2702,&g_2702,&g_2702,&g_2702,&g_2702}};
static int64_t * volatile * volatile * volatile g_2700 = &g_2701[2][2];/* VOLATILE GLOBAL g_2700 */
static volatile union U2 g_2740 = {0x633436E6L};/* VOLATILE GLOBAL g_2740 */
static const int32_t *g_2910 = &g_1086[5][0][4];
static const int32_t ** volatile g_2909 = &g_2910;/* VOLATILE GLOBAL g_2909 */
static float * volatile g_2972 = &g_434.f1;/* VOLATILE GLOBAL g_2972 */
static int32_t * volatile g_2991 = &g_5;/* VOLATILE GLOBAL g_2991 */
static uint32_t g_2993 = 0x683D9286L;
static union U2 g_3016 = {-1L};/* VOLATILE GLOBAL g_3016 */
static uint32_t **g_3076[3] = {&g_1349,&g_1349,&g_1349};
static uint32_t ***g_3075[7][7] = {{&g_3076[1],(void*)0,&g_3076[2],&g_3076[2],&g_3076[0],&g_3076[1],&g_3076[1]},{(void*)0,&g_3076[0],&g_3076[2],&g_3076[0],(void*)0,&g_3076[2],&g_3076[0]},{&g_3076[2],&g_3076[2],&g_3076[2],&g_3076[0],&g_3076[2],&g_3076[2],&g_3076[0]},{&g_3076[2],&g_3076[0],&g_3076[2],&g_3076[2],&g_3076[0],&g_3076[2],&g_3076[2]},{&g_3076[2],&g_3076[0],&g_3076[0],&g_3076[2],&g_3076[0],&g_3076[0],&g_3076[0]},{(void*)0,&g_3076[1],&g_3076[2],(void*)0,&g_3076[2],&g_3076[1],(void*)0},{&g_3076[1],&g_3076[0],&g_3076[2],&g_3076[2],(void*)0,&g_3076[1],&g_3076[2]}};
static uint32_t ****g_3074[2][3] = {{(void*)0,(void*)0,(void*)0},{&g_3075[1][4],&g_3075[1][4],&g_3075[1][4]}};
static union U2 g_3199 = {0x15440B3BL};/* VOLATILE GLOBAL g_3199 */
static volatile float g_3216 = (-0x9.8p+1);/* VOLATILE GLOBAL g_3216 */
static volatile uint16_t g_3270[3][8] = {{65535UL,65535UL,0x06CAL,65535UL,65535UL,0x06CAL,65535UL,65535UL},{1UL,65535UL,1UL,1UL,65535UL,1UL,1UL,65535UL},{65535UL,1UL,1UL,65535UL,1UL,1UL,65535UL,1UL}};
static volatile int64_t g_3288[10][9] = {{0xB76FE9E8ED0D2768LL,0L,0xB76FE9E8ED0D2768LL,0x44DE290CF275CAABLL,0x44DE290CF275CAABLL,0xB76FE9E8ED0D2768LL,0L,0xB76FE9E8ED0D2768LL,0x44DE290CF275CAABLL},{8L,0x401F2ED68D3C43E7LL,0x401F2ED68D3C43E7LL,8L,1L,8L,0x401F2ED68D3C43E7LL,0x401F2ED68D3C43E7LL,8L},{0xCBC424DD6B500AC4LL,0x44DE290CF275CAABLL,9L,0x44DE290CF275CAABLL,0xCBC424DD6B500AC4LL,0xCBC424DD6B500AC4LL,0x44DE290CF275CAABLL,9L,0x44DE290CF275CAABLL},{0x401F2ED68D3C43E7LL,1L,(-6L),(-6L),1L,0x401F2ED68D3C43E7LL,1L,(-6L),(-6L)},{0xCBC424DD6B500AC4LL,0xCBC424DD6B500AC4LL,0x44DE290CF275CAABLL,9L,0x44DE290CF275CAABLL,0xCBC424DD6B500AC4LL,0xCBC424DD6B500AC4LL,0x44DE290CF275CAABLL,9L},{8L,1L,8L,0x401F2ED68D3C43E7LL,0x401F2ED68D3C43E7LL,8L,1L,8L,0x401F2ED68D3C43E7LL},{0xB76FE9E8ED0D2768LL,0x44DE290CF275CAABLL,0x44DE290CF275CAABLL,0xB76FE9E8ED0D2768LL,0L,0xB76FE9E8ED0D2768LL,0x44DE290CF275CAABLL,0x44DE290CF275CAABLL,0xB76FE9E8ED0D2768LL},{0xBCFA95724AC7F1B4LL,0x401F2ED68D3C43E7LL,(-6L),0x401F2ED68D3C43E7LL,0xBCFA95724AC7F1B4LL,0xBCFA95724AC7F1B4LL,0x401F2ED68D3C43E7LL,(-6L),0x401F2ED68D3C43E7LL},{0x44DE290CF275CAABLL,0L,9L,9L,0L,0x44DE290CF275CAABLL,0L,9L,9L},{0xBCFA95724AC7F1B4LL,0xBCFA95724AC7F1B4LL,0x401F2ED68D3C43E7LL,(-6L),0x401F2ED68D3C43E7LL,0xBCFA95724AC7F1B4LL,0xBCFA95724AC7F1B4LL,0x401F2ED68D3C43E7LL,(-6L)}};
static uint32_t ** const *g_3313 = (void*)0;
static uint32_t ** const **g_3312 = &g_3313;
static union U2 g_3329 = {8L};/* VOLATILE GLOBAL g_3329 */
static const union U1 *g_3331[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static const union U1 ** const  volatile g_3330[3][8][8] = {{{&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[0]},{&g_3331[3],&g_3331[3],&g_3331[3],&g_3331[3],&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[3]},{&g_3331[2],&g_3331[0],&g_3331[1],&g_3331[1],&g_3331[0],&g_3331[2],&g_3331[0],&g_3331[2]},{&g_3331[3],(void*)0,&g_3331[2],&g_3331[0],&g_3331[0],&g_3331[2],&g_3331[3],(void*)0},{&g_3331[3],&g_3331[3],&g_3331[2],&g_3331[0],&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[2]},{&g_3331[1],&g_3331[2],&g_3331[2],&g_3331[1],&g_3331[3],(void*)0,(void*)0,&g_3331[3]},{&g_3331[3],&g_3331[1],&g_3331[2],&g_3331[3],&g_3331[2],&g_3331[1],&g_3331[3],&g_3331[0]},{&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[2],&g_3331[0],&g_3331[2],&g_3331[2],&g_3331[3]}},{{&g_3331[2],&g_3331[2],&g_3331[1],&g_3331[2],&g_3331[0],(void*)0,&g_3331[2],&g_3331[3]},{&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[2],&g_3331[2],&g_3331[2],(void*)0},{&g_3331[3],&g_3331[0],&g_3331[2],&g_3331[3],&g_3331[3],&g_3331[2],&g_3331[0],&g_3331[3]},{&g_3331[1],&g_3331[0],&g_3331[2],&g_3331[0],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2]},{&g_3331[3],&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[0],&g_3331[2],&g_3331[3],&g_3331[2]},{&g_3331[3],&g_3331[0],&g_3331[2],&g_3331[2],&g_3331[0],&g_3331[2],&g_3331[2],&g_3331[2]},{&g_3331[2],&g_3331[0],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[0]},{&g_3331[3],&g_3331[2],&g_3331[2],&g_3331[0],&g_3331[2],(void*)0,&g_3331[3],&g_3331[2]}},{{&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[2],&g_3331[3],&g_3331[3]},{&g_3331[2],&g_3331[2],&g_3331[2],(void*)0,&g_3331[2],&g_3331[1],&g_3331[2],&g_3331[2]},{&g_3331[2],&g_3331[1],&g_3331[2],&g_3331[2],(void*)0,(void*)0,&g_3331[2],&g_3331[2]},{&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[0],&g_3331[2],&g_3331[3],&g_3331[3]},{&g_3331[2],&g_3331[3],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[3]},{&g_3331[3],(void*)0,&g_3331[2],&g_3331[3],&g_3331[2],&g_3331[2],&g_3331[0],&g_3331[2]},{&g_3331[2],&g_3331[0],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2],&g_3331[2]},{&g_3331[2],&g_3331[3],&g_3331[2],(void*)0,&g_3331[2],&g_3331[2],&g_3331[3],&g_3331[2]}}};
static const union U1 g_3333 = {0x8F21L};
static uint32_t * volatile *** const g_3341 = (void*)0;
static uint32_t * volatile *** const *g_3340 = &g_3341;
static union U2 g_3363 = {0x99864573L};/* VOLATILE GLOBAL g_3363 */
static uint8_t g_3415 = 0xDEL;
static int32_t g_3421 = (-1L);
static uint32_t g_3436 = 0x1A2B05C3L;
static uint16_t ****g_3451 = &g_2242;
static volatile union U2 g_3547 = {0xF80C82DEL};/* VOLATILE GLOBAL g_3547 */
static volatile union U2 g_3587 = {0L};/* VOLATILE GLOBAL g_3587 */
static union U1 g_3647 = {65531UL};
static union U2 g_3687 = {0xB95A7913L};/* VOLATILE GLOBAL g_3687 */
static volatile uint16_t g_3691[8] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static int32_t ** volatile g_3697 = &g_1267;/* VOLATILE GLOBAL g_3697 */
static float * volatile g_3772 = &g_1536[0][2];/* VOLATILE GLOBAL g_3772 */
static union U2 **g_3806[8][3] = {{&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247}};
static union U2 ** volatile *g_3805 = &g_3806[1][2];
static uint32_t g_3814[7] = {18446744073709551615UL,18446744073709551615UL,18446744073709551606UL,18446744073709551615UL,18446744073709551615UL,18446744073709551606UL,18446744073709551615UL};
static volatile uint8_t * volatile g_3871 = &g_527[4][0];/* VOLATILE GLOBAL g_3871 */
static volatile uint8_t * const  volatile * volatile g_3870 = &g_3871;/* VOLATILE GLOBAL g_3870 */
static volatile uint8_t * const  volatile * volatile *g_3869 = &g_3870;
static float * volatile g_3883 = &g_434.f1;/* VOLATILE GLOBAL g_3883 */
static volatile union U2 g_3946[2] = {{0x67EEAE07L},{0x67EEAE07L}};
static union U2 g_4012 = {0xE88B5C23L};/* VOLATILE GLOBAL g_4012 */
static uint8_t g_4035 = 0UL;
static union U2 g_4038[1][3][1] = {{{{-3L}},{{-3L}},{{-3L}}}};
static volatile int32_t g_4072 = 1L;/* VOLATILE GLOBAL g_4072 */
static volatile int32_t * volatile g_4071[7] = {&g_4072,&g_4072,(void*)0,&g_4072,&g_4072,(void*)0,&g_4072};
static volatile int32_t * volatile *g_4070 = &g_4071[0];
static volatile int32_t * volatile * volatile *g_4069 = &g_4070;
static volatile int32_t * volatile * volatile ** volatile g_4068[8][10] = {{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069},{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069},{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069},{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069},{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069},{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069},{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069},{&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069,&g_4069}};
static union U2 g_4082 = {7L};/* VOLATILE GLOBAL g_4082 */
static int8_t g_4118 = (-1L);
static union U2 g_4129 = {3L};/* VOLATILE GLOBAL g_4129 */
static const volatile union U2 g_4209 = {0x9AEEF13BL};/* VOLATILE GLOBAL g_4209 */
static const uint16_t *g_4276[1][6] = {{&g_60.f0,&g_60.f0,&g_60.f0,&g_60.f0,&g_60.f0,&g_60.f0}};
static const uint16_t **g_4275 = &g_4276[0][0];
static float g_4303 = 0x6.1CCD68p+12;
static const int16_t **g_4369[2][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
static int8_t g_4394[10][1] = {{0x6EL},{(-7L)},{0x6EL},{(-7L)},{0x6EL},{(-7L)},{0x6EL},{(-7L)},{0x6EL},{(-7L)}};
static volatile int64_t * volatile * volatile *** volatile g_4447 = (void*)0;/* VOLATILE GLOBAL g_4447 */


/* --- FORWARD DECLARATIONS --- */
static const int32_t  func_1(void);
static int32_t * func_13(int16_t  p_14, int32_t * p_15, int8_t  p_16, int32_t * p_17, const int32_t * const  p_18);
static uint16_t  func_20(int32_t * p_21, uint64_t  p_22);
static union U0  func_23(int32_t * p_24, int32_t * p_25);
static uint32_t  func_45(float  p_46);
static const float  func_50(int32_t * p_51, int32_t * p_52);
static union U3  func_53(int32_t * p_54, int32_t  p_55);
static union U0  func_56(union U1  p_57, const int32_t * p_58, const int32_t * p_59);
static int32_t * func_61(int32_t * p_62);
static int32_t * func_63(int64_t  p_64, uint16_t  p_65, union U1  p_66);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_1267 g_481
 * writes:
 */
static const int32_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_26 = (void*)0;
    uint64_t *l_2998 = &g_142;
    uint16_t l_3000[10][8] = {{7UL,9UL,65535UL,65535UL,9UL,7UL,0x379BL,7UL},{9UL,7UL,0x379BL,7UL,9UL,65535UL,65535UL,9UL},{7UL,0x2025L,0x2025L,7UL,3UL,9UL,3UL,7UL},{0x2025L,3UL,0x2025L,65535UL,0x379BL,0x379BL,65535UL,0x2025L},{3UL,3UL,0x379BL,65535UL,7UL,65535UL,3UL,65535UL},{65535UL,9UL,0x379BL,3UL,3UL,0x379BL,9UL,65535UL},{9UL,0x2025L,65535UL,65535UL,65535UL,0x2025L,9UL,9UL},{0x2025L,65535UL,0x379BL,0x379BL,65535UL,0x2025L,3UL,0x2025L},{65535UL,0x2025L,3UL,0x2025L,65535UL,0x379BL,0x379BL,65535UL},{0x2025L,9UL,9UL,0x2025L,65535UL,65535UL,65535UL,0x2025L}};
    int8_t l_3626 = 3L;
    int32_t *l_3679 = &g_12[3][1][3];
    int8_t l_3730 = 0x70L;
    int32_t l_3745 = (-8L);
    int32_t l_3746 = 0x431B02CBL;
    int32_t l_3749 = 0x7D17DB8BL;
    int32_t l_3750 = 1L;
    union U1 l_3800[3][1] = {{{0x0773L}},{{0x0773L}},{{0x0773L}}};
    float l_3813 = 0xC.C29479p-7;
    int32_t l_3836 = 4L;
    int32_t *l_3839 = &g_1086[5][0][4];
    int32_t **l_3838 = &l_3839;
    float l_3896 = (-0x1.0p-1);
    int8_t l_3912 = 9L;
    uint32_t ****l_3917 = (void*)0;
    const union U2 ***l_3942 = (void*)0;
    union U3 l_3981 = {0xB270A956L};
    uint32_t l_3982 = 0x3C25B867L;
    uint32_t * const **l_3986 = (void*)0;
    uint32_t * const ***l_3985[6][3] = {{&l_3986,&l_3986,(void*)0},{&l_3986,&l_3986,&l_3986},{(void*)0,&l_3986,&l_3986},{&l_3986,&l_3986,&l_3986},{(void*)0,(void*)0,&l_3986},{&l_3986,&l_3986,&l_3986}};
    uint16_t *** const *l_3997 = &g_2242;
    union U1 l_4057 = {65535UL};
    uint8_t l_4058 = 5UL;
    const uint32_t l_4059 = 0x002AE4C6L;
    union U1 l_4067[5] = {{9UL},{9UL},{9UL},{9UL},{9UL}};
    int32_t ***l_4074 = &l_3838;
    int32_t ****l_4073 = &l_4074;
    const union U2 *l_4081 = &g_4082;
    const union U2 * const *l_4080[6] = {&l_4081,&l_4081,&l_4081,&l_4081,&l_4081,&l_4081};
    const union U2 * const **l_4079[8];
    const union U2 * const ***l_4078[3];
    const union U2 * const ****l_4077 = &l_4078[2];
    int64_t ***l_4205 = &g_532;
    int64_t l_4242[3][2][7] = {{{1L,5L,0x2F35F859DA18C262LL,0x2F35F859DA18C262LL,5L,1L,2L},{3L,0x4731A910F96D2DB3LL,0x982ADE7207BA224ELL,0x982ADE7207BA224ELL,0x4731A910F96D2DB3LL,3L,1L}},{{1L,5L,0x2F35F859DA18C262LL,0x2F35F859DA18C262LL,5L,1L,2L},{3L,0x4731A910F96D2DB3LL,0x982ADE7207BA224ELL,0x982ADE7207BA224ELL,0x4731A910F96D2DB3LL,3L,1L}},{{1L,5L,0x2F35F859DA18C262LL,0x2F35F859DA18C262LL,5L,1L,2L},{3L,0x4731A910F96D2DB3LL,0x982ADE7207BA224ELL,0x982ADE7207BA224ELL,0x4731A910F96D2DB3LL,3L,1L}}};
    int32_t l_4249 = 0xC2B13DDFL;
    int32_t l_4250 = 0x20B7631DL;
    uint32_t l_4251 = 0x71DEB95DL;
    int8_t l_4268 = 2L;
    uint16_t **l_4273 = (void*)0;
    uint16_t **l_4274 = &g_634;
    int32_t l_4392 = 0xE779655AL;
    int32_t l_4431 = 0x3183F3FCL;
    int64_t l_4433 = 9L;
    uint64_t l_4441 = 0x208EC6E4B56A5CB5LL;
    int64_t *l_4452 = (void*)0;
    int64_t *l_4453 = &g_1352;
    int8_t *l_4454[10] = {&g_4394[7][0],&g_4394[7][0],&g_4394[7][0],&g_4394[7][0],&g_4394[7][0],&g_4394[7][0],&g_4394[7][0],&g_4394[7][0],&g_4394[7][0],&g_4394[7][0]};
    int32_t l_4455 = 0xF88EA143L;
    int32_t l_4456[2];
    const float l_4457 = (-0x7.Fp+1);
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_4079[i] = &l_4080[2];
    for (i = 0; i < 3; i++)
        l_4078[i] = &l_4079[1];
    for (i = 0; i < 2; i++)
        l_4456[i] = 0L;
    return (*g_1267);
}


/* ------------------------------------------ */
/* 
 * reads : g_1028 g_1029 g_907 g_210 g_104 g_93 g_83 g_1891 g_1892 g_1893 g_1974 g_2625.f0 g_1252 g_12 g_447 g_448 g_879 g_481
 * writes: g_142 g_810 g_12 g_421 g_481
 */
static int32_t * func_13(int16_t  p_14, int32_t * p_15, int8_t  p_16, int32_t * p_17, const int32_t * const  p_18)
{ /* block id: 1606 */
    int16_t l_3629 = 0x32E2L;
    union U0 * const **l_3640 = (void*)0;
    union U0 * const ***l_3639[5][2] = {{&l_3640,&l_3640},{&l_3640,&l_3640},{&l_3640,&l_3640},{&l_3640,&l_3640},{&l_3640,&l_3640}};
    union U2 **l_3643 = &g_247;
    union U2 ***l_3642[7] = {&l_3643,&l_3643,&l_3643,&l_3643,&l_3643,&l_3643,&l_3643};
    union U2 *** const *l_3641[9][2][10] = {{{(void*)0,(void*)0,&l_3642[1],&l_3642[1],(void*)0,&l_3642[1],&l_3642[2],(void*)0,&l_3642[1],(void*)0},{&l_3642[1],(void*)0,&l_3642[1],(void*)0,&l_3642[2],&l_3642[1],&l_3642[3],&l_3642[1],(void*)0,(void*)0}},{{(void*)0,&l_3642[3],&l_3642[1],&l_3642[6],&l_3642[6],&l_3642[1],&l_3642[3],(void*)0,(void*)0,&l_3642[1]},{&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],(void*)0,&l_3642[3],(void*)0,&l_3642[1],&l_3642[1],&l_3642[6]}},{{(void*)0,(void*)0,&l_3642[1],(void*)0,&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[2],&l_3642[3],&l_3642[1]},{(void*)0,(void*)0,&l_3642[1],(void*)0,&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[6],&l_3642[1],&l_3642[1]}},{{&l_3642[1],&l_3642[1],&l_3642[2],&l_3642[1],&l_3642[1],(void*)0,&l_3642[1],(void*)0,(void*)0,&l_3642[1]},{(void*)0,&l_3642[2],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[3],&l_3642[6],&l_3642[1],&l_3642[1]}},{{&l_3642[1],&l_3642[1],&l_3642[6],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[3],&l_3642[6],&l_3642[1]},{&l_3642[6],&l_3642[2],&l_3642[1],(void*)0,&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],(void*)0,&l_3642[1]}},{{&l_3642[1],&l_3642[1],&l_3642[2],&l_3642[1],&l_3642[6],&l_3642[6],&l_3642[1],(void*)0,&l_3642[1],&l_3642[1]},{(void*)0,(void*)0,(void*)0,&l_3642[1],&l_3642[4],&l_3642[2],&l_3642[1],&l_3642[2],&l_3642[2],(void*)0}},{{(void*)0,&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[3],&l_3642[1],(void*)0,(void*)0,&l_3642[2]},{&l_3642[1],&l_3642[2],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[2],&l_3642[1],&l_3642[1]}},{{&l_3642[1],&l_3642[1],&l_3642[1],(void*)0,(void*)0,(void*)0,&l_3642[3],&l_3642[1],&l_3642[1],&l_3642[2]},{&l_3642[1],&l_3642[2],(void*)0,(void*)0,(void*)0,&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[3]}},{{&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],&l_3642[1],(void*)0,&l_3642[1],(void*)0,(void*)0,&l_3642[1]},{&l_3642[2],(void*)0,&l_3642[1],&l_3642[1],(void*)0,&l_3642[2],&l_3642[1],&l_3642[4],&l_3642[2],(void*)0}}};
    int16_t l_3644 = 0x3D2BL;
    union U1 * const l_3645 = &g_2024;
    union U1 *l_3646 = &g_3647;
    union U3 l_3648 = {0xEFE2A41EL};
    uint64_t *l_3661 = (void*)0;
    uint64_t *l_3662 = &g_142;
    int32_t l_3663 = 0xB025E53CL;
    int32_t l_3664 = 0x8C65B3E3L;
    uint16_t *l_3665 = &g_810;
    uint8_t **l_3671 = (void*)0;
    uint8_t ***l_3670 = &l_3671;
    const uint32_t *l_3678 = &g_1770;
    const uint32_t **l_3677 = &l_3678;
    const uint32_t ***l_3676 = &l_3677;
    int i, j, k;
    l_3646 = (((***g_1028) >= (safe_sub_func_int8_t_s_s(l_3629, (((((safe_lshift_func_int8_t_s_u(((safe_sub_func_int16_t_s_s((safe_mod_func_int32_t_s_s((~(0x5598L < (((*g_104) < (safe_mul_func_int8_t_s_s(p_14, ((((((0UL >= p_16) || (l_3639[0][0] == &l_3640)) > l_3629) || p_14) , l_3641[6][0][6]) == &l_3642[1])))) && l_3629))), p_14)), l_3644)) , 0x33L), 5)) | l_3644) | l_3629) != 0xD16C0875L) > (-3L))))) , l_3645);
    (*p_15) = (((p_14 <= (l_3648 , (safe_add_func_uint64_t_u_u(((((((safe_div_func_uint16_t_u_u(((*l_3665) = (l_3644 <= ((((l_3664 = ((((safe_rshift_func_uint8_t_u_u((p_16 >= (l_3663 ^= ((safe_lshift_func_uint8_t_u_u((((l_3629 != (safe_mul_func_uint8_t_u_u((p_14 <= p_16), (((safe_div_func_int64_t_s_s(p_16, ((*l_3662) = 0x03B2CF3ED41791DFLL))) | p_16) <= l_3648.f0)))) == g_83[0][0][0]) | (****g_1891)), l_3629)) != 0xF4L))), (*g_1974))) < p_14) || g_2625[3][9][1].f0) != l_3644)) == l_3629) <= 0xF8582CC45C8D9601LL) != (*g_1252)))), 4L)) == p_14) , l_3663) >= (*g_1252)) > (*g_1974)) != (*g_1974)), 0UL)))) , (*g_447)) == (void*)0);
    (*g_879) = &l_3664;
    (*p_15) &= ((safe_add_func_int8_t_s_s((safe_mod_func_uint8_t_u_u((*g_1974), 0x3BL)), ((**g_1893) > ((((p_16 , (void*)0) != l_3670) || (safe_mul_func_uint8_t_u_u((l_3629 & (l_3676 == &l_3677)), (*g_1974)))) && 0xA3F8L)))) , l_3663);
    return p_15;
}


/* ------------------------------------------ */
/* 
 * reads : g_12 g_907 g_1974 g_1012 g_3016 g_104 g_93 g_1267 g_481 g_5 g_1028 g_1029 g_60.f0 g_3074 g_145 g_878 g_879 g_1801 g_446 g_447 g_448 g_434 g_1041 g_1042 g_2702 g_1083 g_210 g_2991 g_83 g_1191.f0 g_1674 g_1096 g_1097 g_865 g_866 g_1893 g_245 g_243 g_2023 g_2024 g_3199 g_141 g_1194 g_35 g_1371 g_810 g_2972 g_3270 g_418 g_3288 g_2075 g_2076 g_1627 g_3363 g_1252 g_232 g_2292 g_2082 g_1891 g_1892 g_3415 g_3421 g_1256.f0 g_3436 g_1030 g_36 g_634 g_1087 g_3547 g_479 g_1742 g_1743 g_3587 g_1774 g_142 g_721.f1
 * writes: g_210 g_83 g_1012 g_634 g_142 g_721.f1 g_60.f0 g_12 g_369 g_3074 g_93 g_421 g_878 g_434 g_810 g_3270 g_141 g_3312 g_3340 g_1627 g_1083 g_243 g_91 g_1267 g_1194 g_2292 g_418 g_3436 g_3451 g_1030 g_481 g_3421 g_1349 g_479 g_42 g_1352 g_1774
 */
static uint16_t  func_20(int32_t * p_21, uint64_t  p_22)
{ /* block id: 1353 */
    float l_3007 = (-0x1.3p+1);
    int32_t l_3009 = 0xBAD988B5L;
    int64_t l_3015 = 0x7037EB1A50201369LL;
    uint16_t *l_3018[6] = {&g_1627,&g_1627,&g_1627,&g_1627,&g_1627,&g_1627};
    int32_t l_3026 = 0x55860BACL;
    uint8_t l_3027 = 253UL;
    int32_t l_3045 = (-1L);
    int32_t l_3046 = 0L;
    int32_t l_3051 = 0x44CD963EL;
    int32_t l_3053 = 0L;
    int32_t l_3054 = 0x25A5C9CAL;
    int32_t l_3060 = 0x4D8D0F68L;
    int32_t l_3062 = 0L;
    const union U1 l_3170 = {65534UL};
    uint32_t **l_3187 = &g_1349;
    const uint16_t *l_3207 = &g_693;
    const uint16_t **l_3206 = &l_3207;
    union U2 ***l_3236 = (void*)0;
    union U2 *** const *l_3235 = &l_3236;
    union U2 *** const **l_3234 = &l_3235;
    int32_t l_3262 = 0x78C54BDDL;
    int32_t l_3263 = 0L;
    uint32_t l_3265[9] = {0xBDA14FEAL,0x07C97A80L,0xBDA14FEAL,0xBDA14FEAL,0x07C97A80L,0xBDA14FEAL,0xBDA14FEAL,0x07C97A80L,0xBDA14FEAL};
    int32_t *l_3324 = &g_1086[5][0][4];
    int16_t **l_3348 = &g_907;
    union U1 *l_3365 = (void*)0;
    union U0 ****l_3435 = &g_1041;
    uint16_t ****l_3452 = &g_2242;
    uint32_t l_3458 = 0UL;
    uint32_t l_3472 = 18446744073709551613UL;
    const uint32_t l_3509[3] = {0x9DAED4AAL,0x9DAED4AAL,0x9DAED4AAL};
    uint16_t l_3535 = 0x0168L;
    int8_t l_3551 = (-6L);
    union U0 ***l_3573 = (void*)0;
    uint64_t *l_3574 = &g_142;
    union U1 l_3581 = {0x1EEEL};
    uint32_t l_3604 = 0xE868853FL;
    int32_t l_3608 = (-8L);
    int32_t *l_3622[10] = {&g_29,(void*)0,&g_29,&g_29,(void*)0,&g_29,&g_29,(void*)0,&g_29,&g_29};
    int8_t l_3623[3][7] = {{0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L},{(-7L),6L,(-7L),6L,(-7L),6L,(-7L)},{0x07L,0x07L,0x07L,0x07L,0x07L,0x07L,0x07L}};
    float l_3624 = 0x7.AA1946p+95;
    uint8_t l_3625[3];
    int i, j;
    for (i = 0; i < 3; i++)
        l_3625[i] = 0x75L;
    if ((*p_21))
    { /* block id: 1354 */
        union U3 l_3008[8] = {{0xDF3A458BL},{0xDF3A458BL},{0xDF3A458BL},{0xDF3A458BL},{0xDF3A458BL},{0xDF3A458BL},{0xDF3A458BL},{0xDF3A458BL}};
        int16_t *l_3010 = (void*)0;
        int16_t *l_3011 = &g_1012;
        int32_t l_3012 = (-1L);
        uint16_t **l_3017[7];
        int32_t *l_3031[1];
        int32_t ** const *l_3093 = &g_879;
        int64_t * const **l_3097 = (void*)0;
        int64_t * const ***l_3096 = &l_3097;
        uint32_t ***l_3180 = &g_3076[2];
        union U1 l_3190[9][6] = {{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}},{{0xF078L},{0x5638L},{0xF078L},{1UL},{0x70A1L},{1UL}}};
        uint8_t l_3238 = 0x80L;
        int16_t l_3268 = (-1L);
        union U2 *l_3327[8] = {&g_640[0],&g_2697,&g_640[0],&g_640[0],&g_2697,&g_640[0],&g_640[0],&g_2697};
        int64_t ***l_3371 = (void*)0;
        int64_t ****l_3370 = &l_3371;
        int64_t *****l_3369 = &l_3370;
        int32_t l_3375 = 0xF84172C7L;
        uint32_t l_3384[6] = {4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL};
        float **l_3416 = &g_1743[0][6][0];
        uint32_t l_3426 = 4294967288UL;
        float l_3429 = 0x1.Cp-1;
        union U0 l_3433 = {0xDAL};
        uint64_t *l_3434[3];
        int i, j;
        for (i = 0; i < 7; i++)
            l_3017[i] = &g_634;
        for (i = 0; i < 1; i++)
            l_3031[i] = &g_12[1][8][2];
        for (i = 0; i < 3; i++)
            l_3434[i] = &g_1030;
        l_3012 &= (((*l_3011) ^= (safe_add_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(p_22, (((*g_1974) = (((0x42F6L == (p_22 , ((*g_907) = p_22))) <= 0xB2L) ^ (+p_22))) || (0xF7B2476343994DC2LL > ((l_3008[4] , p_22) || p_22))))), l_3009))) != l_3008[4].f0);
        if ((((safe_div_func_int8_t_s_s(l_3015, (g_3016 , (*g_104)))) < (p_22 == (((p_22 || ((g_634 = l_3010) != l_3018[4])) & ((safe_sub_func_int32_t_s_s((*p_21), ((*g_1267) ^ l_3015))) > p_22)) , p_22))) < p_22))
        { /* block id: 1360 */
            int32_t l_3043 = 0xC66F26B7L;
            int32_t l_3047 = (-1L);
            int32_t l_3048 = 0x72DE4D3CL;
            int32_t l_3050 = 0x917BC668L;
            float l_3055 = (-0x8.Ep+1);
            int32_t l_3057 = 0x36E740C4L;
            int32_t l_3058 = 0L;
            int32_t l_3061[10];
            uint16_t l_3111 = 0x62E4L;
            int64_t **l_3129 = (void*)0;
            union U1 l_3132 = {65534UL};
            uint32_t l_3156 = 0xA2886F1AL;
            int16_t **l_3178[7][10] = {{&l_3011,&l_3011,&l_3011,&l_3011,&l_3010,&l_3010,&l_3011,&l_3010,&l_3010,&l_3011},{&l_3011,&l_3011,&l_3011,&l_3011,&l_3010,&l_3010,&l_3011,&l_3010,&l_3010,&l_3011},{&l_3011,&l_3011,&l_3011,&l_3011,&l_3010,&l_3010,&l_3011,&l_3010,&l_3010,&l_3011},{&l_3011,&l_3011,&l_3011,&l_3011,&l_3010,&l_3010,&l_3011,&l_3010,&l_3010,&l_3011},{&l_3011,&l_3011,&l_3011,&l_3011,&l_3010,&l_3010,&l_3011,&l_3010,&l_3010,&l_3011},{&l_3011,&l_3011,&l_3011,&l_3011,&l_3010,&l_3010,&l_3011,&l_3010,&l_3010,&l_3011},{&l_3011,&l_3011,&l_3011,&l_3011,&l_3010,&l_3010,&l_3011,&l_3010,&l_3010,&l_3011}};
            int32_t *l_3181 = (void*)0;
            int16_t *****l_3185 = &g_1891;
            int32_t l_3254 = (-1L);
            int16_t l_3258[2][6];
            int32_t l_3269 = 5L;
            int32_t *l_3320 = &g_1086[5][0][4];
            int64_t *** const l_3339 = (void*)0;
            int64_t *** const *l_3338 = &l_3339;
            int64_t *** const **l_3337 = &l_3338;
            const int8_t *l_3350[1][1][3];
            const int8_t * const *l_3349[2][8] = {{&l_3350[0][0][0],&l_3350[0][0][1],&l_3350[0][0][0],&l_3350[0][0][1],&l_3350[0][0][0],&l_3350[0][0][1],&l_3350[0][0][0],&l_3350[0][0][1]},{&l_3350[0][0][0],&l_3350[0][0][1],&l_3350[0][0][0],&l_3350[0][0][1],&l_3350[0][0][0],&l_3350[0][0][1],&l_3350[0][0][0],&l_3350[0][0][1]}};
            int i, j, k;
            for (i = 0; i < 10; i++)
                l_3061[i] = (-7L);
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 6; j++)
                    l_3258[i][j] = 1L;
            }
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 3; k++)
                        l_3350[i][j][k] = &g_479;
                }
            }
lbl_3218:
            for (g_142 = 0; (g_142 >= 3); g_142 = safe_add_func_int32_t_s_s(g_142, 6))
            { /* block id: 1363 */
                int32_t *l_3023 = &l_3009;
                int32_t *l_3024 = (void*)0;
                int32_t *l_3025[10][7];
                int32_t ***l_3092 = &g_879;
                int64_t l_3109 = 3L;
                int i, j;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 7; j++)
                        l_3025[i][j] = &g_12[0][8][0];
                }
                l_3027++;
                if (((*g_104) <= l_3026))
                { /* block id: 1365 */
                    int32_t * const l_3030 = (void*)0;
                    int32_t l_3036 = (-6L);
                    int32_t l_3038 = 8L;
                    uint64_t l_3039 = 0xA0650405B5FD8E31LL;
                    int32_t l_3042[6] = {1L,1L,1L,1L,1L,1L};
                    uint32_t l_3063 = 4294967295UL;
                    int i;
                    l_3031[0] = l_3030;
                    for (g_721.f1 = 0; (g_721.f1 < 47); ++g_721.f1)
                    { /* block id: 1369 */
                        int64_t l_3034 = 0x9E0CE76FF1360E80LL;
                        int32_t l_3035 = 0x5CF825AAL;
                        int32_t l_3037 = 0xF980C0BBL;
                        int32_t l_3044 = 0xE83FEF97L;
                        int32_t l_3049 = 0xFC36FA3EL;
                        int32_t l_3052 = 0x20FA0512L;
                        int32_t l_3056 = 0xA4B98134L;
                        int32_t l_3059[6][4] = {{0x74BBFD87L,0x74BBFD87L,1L,(-10L)},{(-10L),0xDAA54151L,1L,0xDAA54151L},{0x74BBFD87L,1L,0xA71BBC80L,1L},{0xDAA54151L,1L,1L,0xDAA54151L},{1L,0xDAA54151L,0x74BBFD87L,(-10L)},{1L,0x74BBFD87L,1L,0xA71BBC80L}};
                        int i, j;
                        --l_3039;
                        l_3063--;
                    }
                }
                else
                { /* block id: 1373 */
                    uint16_t l_3068 = 2UL;
                    for (g_60.f0 = 2; (g_60.f0 <= 6); g_60.f0 = safe_add_func_int32_t_s_s(g_60.f0, 6))
                    { /* block id: 1376 */
                        float *l_3073 = &g_369;
                        int32_t l_3089 = 4L;
                        l_3068++;
                        (*p_21) ^= (safe_add_func_int64_t_s_s(((void*)0 == (**g_1028)), g_60.f0));
                        (*l_3073) = p_22;
                        (*l_3023) = ((l_3068 , (((*g_104) ^= (((g_3074[0][1] = g_3074[0][1]) == (void*)0) < (*g_145))) , (0x87D1FF6D6A48EDD8LL <= (safe_div_func_int64_t_s_s((l_3060 != (safe_lshift_func_uint16_t_u_s(0UL, ((safe_add_func_int16_t_s_s(0x1AD8L, ((safe_add_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((l_3068 ^ (-9L)), (*p_21))), l_3089)) ^ g_1012))) <= l_3057)))), (-7L)))))) & l_3048);
                    }
                    for (l_3051 = 25; (l_3051 <= 14); --l_3051)
                    { /* block id: 1386 */
                        (**g_878) = p_21;
                        (*l_3023) = (p_22 == 0x6.6ADA73p-80);
                        l_3057 |= (((*g_1801) = l_3092) != l_3093);
                    }
                    if (((((***g_1041) = (***g_446)) , (safe_rshift_func_int16_t_s_s((l_3096 != (void*)0), 7))) < ((safe_div_func_int64_t_s_s(p_22, (((*g_104) & ((p_22 <= 0xFA97L) == (((safe_add_func_float_f_f((safe_sub_func_float_f_f((l_3068 == (((p_22 == (*g_2702)) , p_22) == l_3060)), p_22)), l_3043)) , 0x855DL) , l_3068))) && 0x641ADBEFL))) & (*g_907))))
                    { /* block id: 1393 */
                        int64_t l_3104[8][10][3] = {{{0x65A0E690F4F09AECLL,(-1L),(-1L)},{0xA4A0C03FD7205740LL,0xA4A0C03FD7205740LL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL}},{{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL}},{{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL}},{{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL}},{{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL}},{{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL}},{{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,0xA1D3F0B6FAC25880LL,0xA4A0C03FD7205740LL},{0xA1D3F0B6FAC25880LL,0xA56D5C3CFE35F7D0LL,0xA4A0C03FD7205740LL},{0x673878ECA9908F5CLL,0x673878ECA9908F5CLL,0xA4A0C03FD7205740LL},{0xA56D5C3CFE35F7D0LL,(-1L),0x673878ECA9908F5CLL},{(-1L),0xA35BD3F0805EA7DELL,0x673878ECA9908F5CLL},{1L,1L,0x673878ECA9908F5CLL},{0xA35BD3F0805EA7DELL,(-1L),0x673878ECA9908F5CLL},{(-1L),0xA35BD3F0805EA7DELL,0x673878ECA9908F5CLL}},{{1L,1L,0x673878ECA9908F5CLL},{0xA35BD3F0805EA7DELL,(-1L),0x673878ECA9908F5CLL},{(-1L),0xA35BD3F0805EA7DELL,0x673878ECA9908F5CLL},{1L,1L,0x673878ECA9908F5CLL},{0xA35BD3F0805EA7DELL,(-1L),0x673878ECA9908F5CLL},{(-1L),0xA35BD3F0805EA7DELL,0x673878ECA9908F5CLL},{1L,1L,0x673878ECA9908F5CLL},{0xA35BD3F0805EA7DELL,(-1L),0x673878ECA9908F5CLL},{(-1L),0xA35BD3F0805EA7DELL,0x673878ECA9908F5CLL},{1L,1L,0x673878ECA9908F5CLL}}};
                        uint32_t l_3105 = 0x8DC9A49BL;
                        int i, j, k;
                        l_3105++;
                    }
                    else
                    { /* block id: 1395 */
                        int16_t l_3108 = 3L;
                        int32_t l_3110 = 0xA33F1971L;
                        (*p_21) |= l_3108;
                        ++l_3111;
                    }
                }
            }
            if ((((*g_1974) = (safe_add_func_uint32_t_u_u(((*p_21) < (-8L)), ((((-1L) ^ (safe_div_func_int32_t_s_s((*g_2991), l_3053))) , ((safe_mod_func_int64_t_s_s((safe_unary_minus_func_uint32_t_u(((safe_rshift_func_uint16_t_u_s((safe_mul_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(((*g_1974) , (0x2CD3FF87L > ((0x6700L <= (0x75L == p_22)) || 18446744073709551615UL))), 0xBE1CL)), 0L)), l_3047)) | 0xE544E2EDL))), g_1191.f0)) , (*g_1801))) != (void*)0)))) < 246UL))
            { /* block id: 1402 */
                union U3 l_3138 = {0x188A18EDL};
                union U1 *l_3149 = &g_2024;
                int32_t l_3152 = 1L;
                const int32_t l_3154[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
                int32_t l_3155 = 0x1FB2D82BL;
                int16_t **l_3177 = &g_907;
                int16_t ***** const l_3186 = &g_1891;
                union U0 l_3198 = {-1L};
                const uint16_t **l_3208 = &l_3207;
                int32_t l_3239 = 0x1FD4E0DFL;
                int32_t l_3247 = 0x81F6A2DCL;
                int32_t l_3249 = 0x5EE586ECL;
                int32_t l_3252[9] = {0xA5BC82ECL,0xA5BC82ECL,7L,0xA5BC82ECL,0xA5BC82ECL,7L,0xA5BC82ECL,0xA5BC82ECL,7L};
                union U2 **l_3287[5][10][5] = {{{&g_247,&g_247,&g_247,&g_247,&g_247},{(void*)0,&g_247,&g_247,(void*)0,&g_247},{&g_247,&g_247,(void*)0,&g_247,(void*)0},{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,(void*)0},{&g_247,&g_247,&g_247,&g_247,&g_247},{(void*)0,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,&g_247}},{{(void*)0,&g_247,(void*)0,&g_247,&g_247},{&g_247,(void*)0,&g_247,&g_247,(void*)0},{&g_247,&g_247,&g_247,&g_247,&g_247},{(void*)0,&g_247,&g_247,(void*)0,&g_247},{&g_247,&g_247,(void*)0,&g_247,(void*)0},{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,(void*)0},{&g_247,&g_247,&g_247,&g_247,&g_247},{(void*)0,&g_247,&g_247,&g_247,&g_247}},{{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,&g_247},{(void*)0,&g_247,(void*)0,&g_247,&g_247},{&g_247,(void*)0,&g_247,&g_247,(void*)0},{&g_247,&g_247,&g_247,&g_247,&g_247},{(void*)0,&g_247,&g_247,(void*)0,(void*)0},{(void*)0,(void*)0,&g_247,&g_247,&g_247},{(void*)0,(void*)0,&g_247,&g_247,(void*)0},{&g_247,&g_247,&g_247,(void*)0,&g_247},{(void*)0,&g_247,&g_247,(void*)0,&g_247}},{{(void*)0,&g_247,&g_247,&g_247,(void*)0},{&g_247,(void*)0,&g_247,&g_247,(void*)0},{&g_247,(void*)0,&g_247,&g_247,&g_247},{(void*)0,&g_247,&g_247,(void*)0,(void*)0},{&g_247,&g_247,&g_247,(void*)0,(void*)0},{(void*)0,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,(void*)0},{(void*)0,(void*)0,&g_247,&g_247,&g_247},{(void*)0,(void*)0,&g_247,&g_247,(void*)0}},{{&g_247,&g_247,&g_247,(void*)0,&g_247},{(void*)0,&g_247,&g_247,(void*)0,&g_247},{(void*)0,&g_247,&g_247,&g_247,(void*)0},{&g_247,(void*)0,&g_247,&g_247,(void*)0},{&g_247,(void*)0,&g_247,&g_247,&g_247},{(void*)0,&g_247,&g_247,(void*)0,(void*)0},{&g_247,&g_247,&g_247,(void*)0,(void*)0},{(void*)0,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,&g_247},{&g_247,&g_247,&g_247,&g_247,(void*)0}}};
                union U1 **l_3319 = (void*)0;
                int i, j, k;
                if ((((((((-7L) | p_22) < ((**g_1674) == ((((l_3129 != &g_2702) ^ ((l_3027 ^ (l_3132 , (l_3008[4] , (((*g_865) && (**g_1893)) >= p_22)))) ^ 0L)) != 0xB947FFC478FCE614LL) ^ 0xE25AL))) | 7L) , &g_1155) == (void*)0) < p_22))
                { /* block id: 1403 */
                    union U3 l_3136 = {4294967291UL};
                    union U3 *l_3137[2];
                    int32_t l_3153[6];
                    union U1 l_3179 = {0x75A4L};
                    int i;
                    for (i = 0; i < 2; i++)
                        l_3137[i] = &g_721;
                    for (i = 0; i < 6; i++)
                        l_3153[i] = 0xB790DDB7L;
                    l_3155 = ((safe_mod_func_uint16_t_u_u((((!(*g_1974)) ^ ((l_3138 = l_3136) , (l_3153[1] ^= ((0L >= (safe_add_func_int16_t_s_s((safe_add_func_int32_t_s_s((safe_mod_func_int64_t_s_s((l_3152 = ((safe_mul_func_int8_t_s_s(((safe_mul_func_int16_t_s_s(0xD495L, ((*g_245) != ((*g_2023) , l_3149)))) ^ ((*p_21) = (safe_rshift_func_uint8_t_u_s((*g_1974), 7)))), ((*g_104) = ((l_3136.f0 , (*g_1974)) != 0x3EL)))) >= p_22)), 3L)), 0x91675C17L)), p_22))) , (*p_21))))) , p_22), l_3154[8])) != 0x0BCA90B9L);
                    l_3181 = func_63(l_3156, (((p_22 , (((l_3054 ^= ((~(g_142 = ((safe_add_func_int8_t_s_s(((((safe_div_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(((2L < ((safe_lshift_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((((((l_3170 , (safe_add_func_int8_t_s_s((&p_21 != &p_21), ((safe_lshift_func_int8_t_s_u(((safe_rshift_func_uint8_t_u_u(((l_3136.f0 <= (*p_21)) < ((l_3177 == l_3178[6][8]) == 0xAC19744615094A77LL)), 6)) <= p_22), l_3009)) > (*g_1974))))) <= (*g_1974)) && p_22) , (*p_21)) == (**g_1674)), 3)), 1)) != (*p_21))) & (-2L)), p_22)), (*g_1974))), p_22)) , l_3179) , (*g_104)) && l_3053), p_22)) , 0xD5068EC9CCFBB4CELL))) | p_22)) , p_22) , (void*)0)) == l_3180) , 0UL), l_3179);
                    for (g_1012 = 0; (g_1012 < (-30)); g_1012--)
                    { /* block id: 1415 */
                        int16_t *****l_3184 = &g_1891;
                        int32_t l_3193 = (-1L);
                        union U1 *l_3196 = &g_60;
                        uint32_t l_3197 = 5UL;
                        const float l_3215[8][3][7] = {{{0x0.Fp+1,(-0x5.Cp+1),0x7.8p+1,(-0x10.5p-1),(-0x6.Fp+1),(-0x10.5p-1),0x7.8p+1},{0x4.FBFBB3p-85,0x4.FBFBB3p-85,0x6.8p-1,0xA.06D027p-10,0x0.Ep-1,0xA.06D027p-10,0x6.8p-1},{0x0.Fp+1,(-0x5.Cp+1),0x7.8p+1,(-0x10.5p-1),(-0x6.Fp+1),(-0x10.5p-1),0x7.8p+1}},{{0x4.FBFBB3p-85,0x4.FBFBB3p-85,0x6.8p-1,0xA.06D027p-10,0x0.Ep-1,0xA.06D027p-10,0x6.8p-1},{0x0.Fp+1,(-0x5.Cp+1),0x7.8p+1,(-0x10.5p-1),(-0x6.Fp+1),(-0x10.5p-1),0x7.8p+1},{0x4.FBFBB3p-85,0x4.FBFBB3p-85,0x6.8p-1,0xA.06D027p-10,0x0.Ep-1,0xA.06D027p-10,0x6.8p-1}},{{0x0.Fp+1,(-0x5.Cp+1),0x7.8p+1,(-0x10.5p-1),(-0x6.Fp+1),(-0x10.5p-1),0x7.8p+1},{0x4.FBFBB3p-85,0x4.FBFBB3p-85,0x6.8p-1,0xA.06D027p-10,0x0.Ep-1,0xA.06D027p-10,0x6.8p-1},{0x0.Fp+1,(-0x5.Cp+1),0x7.8p+1,(-0x10.5p-1),(-0x6.Fp+1),(-0x10.5p-1),0x7.8p+1}},{{0x4.FBFBB3p-85,0x4.FBFBB3p-85,0x6.8p-1,0xA.06D027p-10,0x0.Ep-1,0xA.06D027p-10,0x6.8p-1},{(-0x6.Fp+1),(-0x9.6p-1),0x0.Fp+1,(-0x4.Bp-1),0x0.Ap-1,(-0x4.Bp-1),0x0.Fp+1},{0x0.Ep-1,0x0.Ep-1,0x4.FBFBB3p-85,(-0x5.7p+1),0x7.3p-1,(-0x5.7p+1),0x4.FBFBB3p-85}},{{(-0x6.Fp+1),(-0x9.6p-1),0x0.Fp+1,(-0x4.Bp-1),0x0.Ap-1,(-0x4.Bp-1),0x0.Fp+1},{0x0.Ep-1,0x0.Ep-1,0x4.FBFBB3p-85,(-0x5.7p+1),0x7.3p-1,(-0x5.7p+1),0x4.FBFBB3p-85},{(-0x6.Fp+1),(-0x9.6p-1),0x0.Fp+1,(-0x4.Bp-1),0x0.Ap-1,(-0x4.Bp-1),0x0.Fp+1}},{{0x0.Ep-1,0x0.Ep-1,0x4.FBFBB3p-85,(-0x5.7p+1),0x7.3p-1,(-0x5.7p+1),0x4.FBFBB3p-85},{(-0x6.Fp+1),(-0x9.6p-1),0x0.Fp+1,(-0x4.Bp-1),0x0.Ap-1,(-0x4.Bp-1),0x0.Fp+1},{0x0.Ep-1,0x0.Ep-1,0x4.FBFBB3p-85,(-0x5.7p+1),0x7.3p-1,(-0x5.7p+1),0x4.FBFBB3p-85}},{{(-0x6.Fp+1),(-0x9.6p-1),0x0.Fp+1,(-0x4.Bp-1),0x0.Ap-1,(-0x4.Bp-1),0x0.Fp+1},{0x0.Ep-1,0x0.Ep-1,0x4.FBFBB3p-85,(-0x5.7p+1),0x7.3p-1,(-0x5.7p+1),0x4.FBFBB3p-85},{(-0x6.Fp+1),(-0x9.6p-1),0x0.Fp+1,(-0x4.Bp-1),0x0.Ap-1,(-0x4.Bp-1),0x0.Fp+1}},{{0x0.Ep-1,0x0.Ep-1,0x4.FBFBB3p-85,(-0x5.7p+1),0x7.3p-1,(-0x5.7p+1),0x4.FBFBB3p-85},{(-0x6.Fp+1),(-0x9.6p-1),0x0.Fp+1,(-0x4.Bp-1),0x0.Ap-1,(-0x4.Bp-1),0x0.Fp+1},{0x0.Ep-1,0x0.Ep-1,0x4.FBFBB3p-85,(-0x5.7p+1),0x7.3p-1,(-0x5.7p+1),0x4.FBFBB3p-85}}};
                        uint64_t l_3217 = 0xDD8EA466FED4F92CLL;
                        int i, j, k;
                        (*l_3181) ^= ((l_3185 = l_3184) == l_3186);
                        (*p_21) ^= (l_3187 == (((((safe_div_func_uint8_t_u_u(((l_3198 , g_3199) , p_22), l_3138.f0)) || l_3179.f0) , 0x0DL) || (*g_1974)) , (void*)0));
                        l_3153[1] ^= ((safe_add_func_float_f_f((safe_add_func_float_f_f(0x5.Cp-1, (((0x0AL || ((((p_22 = (safe_add_func_int32_t_s_s(((l_3208 = l_3206) != (void*)0), ((((safe_sub_func_int8_t_s_s(1L, (safe_mod_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(((l_3154[5] >= (((*l_3181) = (*p_21)) , (l_3217 ^= ((**g_447) , (p_22 > p_22))))) , 0xE4L), l_3193)), p_22)))) , g_141[0]) > g_1194) & 0xB06CF70DL)))) ^ g_35) & 0x04ED563A3E2363F3LL) != l_3154[6])) , (*l_3181)) > l_3045))), l_3152)) , (*g_2991));
                    }
                    if (l_3136.f0)
                        goto lbl_3218;
                }
                else
                { /* block id: 1428 */
                    uint8_t l_3219 = 0UL;
                    union U1 l_3224[8][6] = {{{0x3715L},{1UL},{5UL},{1UL},{0x561EL},{1UL}},{{65535UL},{1UL},{65535UL},{0xD68DL},{0xF5EDL},{1UL}},{{65528UL},{0x074AL},{0xBB48L},{0xC59EL},{65535UL},{0xF5EDL}},{{0UL},{1UL},{0xD68DL},{0xC59EL},{0xC59EL},{0xD68DL}},{{65528UL},{65528UL},{1UL},{0xD68DL},{0xBB48L},{65535UL}},{{65535UL},{0x561EL},{65528UL},{1UL},{0x074AL},{1UL}},{{0x3715L},{65535UL},{65528UL},{0UL},{65528UL},{65535UL}},{{65535UL},{0UL},{1UL},{65535UL},{0UL},{0xD68DL}}};
                    float *l_3237[10] = {&g_91,&g_1536[1][2],&g_91,&g_1536[1][2],&g_91,&g_1536[1][2],&g_91,&g_1536[1][2],&g_91,&g_1536[1][2]};
                    int32_t l_3240 = 0x70DDF669L;
                    int32_t l_3241[3];
                    int16_t l_3289 = 0xDD98L;
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_3241[i] = 5L;
                    (*g_2972) = (l_3219 < (((((void*)0 == (*g_1041)) > l_3219) == ((safe_mul_func_float_f_f(((((p_22 == (safe_div_func_float_f_f((l_3008[4] , 0xC.706ADEp-68), ((l_3224[3][3] , (-(l_3241[2] = (l_3240 = (l_3053 = (safe_div_func_float_f_f((safe_add_func_float_f_f((l_3238 = ((g_810 &= ((safe_add_func_uint64_t_u_u((safe_rshift_func_int8_t_s_s(((((void*)0 == l_3234) , l_3009) < p_22), (*g_104))), g_1371)) ^ 4294967295UL)) , p_22)), l_3239)), l_3154[8]))))))) <= 0x7.5p+1)))) != p_22) > p_22) <= 0x0.Cp+1), p_22)) > p_22)) != l_3154[5]));
                    for (l_3050 = 0; (l_3050 <= 8); l_3050 += 1)
                    { /* block id: 1437 */
                        if (l_3051)
                            break;
                    }
                    for (l_3015 = 0; (l_3015 <= 1); l_3015 += 1)
                    { /* block id: 1442 */
                        int64_t l_3242[6];
                        int32_t l_3243 = 0xDED51327L;
                        int32_t l_3244 = 4L;
                        int32_t l_3245 = 0xE8D0F9C0L;
                        int32_t l_3246 = 0xDF8EE279L;
                        int32_t l_3248 = 0xB5640609L;
                        int8_t l_3250 = 0x7BL;
                        int32_t l_3251 = 0xD3DDAF80L;
                        int32_t l_3253 = 0x9EC7A19FL;
                        int32_t l_3255 = 0L;
                        int32_t l_3256 = 0L;
                        int32_t l_3257 = 1L;
                        int32_t l_3259 = 0L;
                        int32_t l_3260 = 0xEE75660AL;
                        int32_t l_3261 = (-7L);
                        int32_t l_3264 = 0xAF922C7DL;
                        union U3 l_3281[5][10][3] = {{{{4294967295UL},{0x9BA6C087L},{0x72E6A9A0L}},{{4294967291UL},{0xF588AD0EL},{0UL}},{{4294967291UL},{0x2F1E6503L},{4294967291UL}},{{4294967295UL},{4294967288UL},{0x09351C59L}},{{4294967291UL},{4294967295UL},{1UL}},{{4294967291UL},{4294967287UL},{0x1258A300L}},{{4294967295UL},{0xF5291004L},{0UL}},{{4294967291UL},{1UL},{0xBC10518CL}},{{4294967291UL},{4294967290UL},{4294967295UL}},{{4294967295UL},{0x9BA6C087L},{0x72E6A9A0L}}},{{{4294967291UL},{0xF588AD0EL},{0UL}},{{4294967291UL},{0x09351C59L},{0x5A79FD4DL}},{{0UL},{4294967291UL},{0x9DE411E7L}},{{0x5A79FD4DL},{0UL},{7UL}},{{0x5A79FD4DL},{0x72E6A9A0L},{1UL}},{{0UL},{4294967295UL},{6UL}},{{0x5A79FD4DL},{0xBC10518CL},{4294967295UL}},{{0x5A79FD4DL},{0UL},{0UL}},{{0UL},{0x1258A300L},{0x225AE2C5L}},{{0x5A79FD4DL},{1UL},{4UL}}},{{{0x5A79FD4DL},{0x09351C59L},{0x5A79FD4DL}},{{0UL},{4294967291UL},{0x9DE411E7L}},{{0x5A79FD4DL},{0UL},{7UL}},{{0x5A79FD4DL},{0x72E6A9A0L},{1UL}},{{0UL},{4294967295UL},{6UL}},{{0x5A79FD4DL},{0xBC10518CL},{4294967295UL}},{{0x5A79FD4DL},{0UL},{0UL}},{{0UL},{0x1258A300L},{0x225AE2C5L}},{{0x5A79FD4DL},{1UL},{4UL}},{{0x5A79FD4DL},{0x09351C59L},{0x5A79FD4DL}}},{{{0UL},{4294967291UL},{0x9DE411E7L}},{{0x5A79FD4DL},{0UL},{7UL}},{{0x5A79FD4DL},{0x72E6A9A0L},{1UL}},{{0UL},{4294967295UL},{6UL}},{{0x5A79FD4DL},{0xBC10518CL},{4294967295UL}},{{0x5A79FD4DL},{0UL},{0UL}},{{0UL},{0x1258A300L},{0x225AE2C5L}},{{0x5A79FD4DL},{1UL},{4UL}},{{0x5A79FD4DL},{0x09351C59L},{0x5A79FD4DL}},{{0UL},{4294967291UL},{0x9DE411E7L}}},{{{0x5A79FD4DL},{0UL},{7UL}},{{0x5A79FD4DL},{0x72E6A9A0L},{1UL}},{{0UL},{4294967295UL},{6UL}},{{0x5A79FD4DL},{0xBC10518CL},{4294967295UL}},{{0x5A79FD4DL},{0UL},{0UL}},{{0UL},{0x1258A300L},{0x225AE2C5L}},{{0x5A79FD4DL},{1UL},{4UL}},{{0x5A79FD4DL},{0x09351C59L},{0x5A79FD4DL}},{{0UL},{4294967291UL},{0x9DE411E7L}},{{0x5A79FD4DL},{0UL},{7UL}}}};
                        int i, j, k;
                        for (i = 0; i < 6; i++)
                            l_3242[i] = 0x5D8C0297D8EDC062LL;
                        l_3265[7]--;
                        ++g_3270[0][4];
                        (*p_21) = (safe_sub_func_uint8_t_u_u(255UL, ((l_3247 || ((((*g_104) || (safe_mul_func_int16_t_s_s(((l_3241[2] ^= (safe_mod_func_uint8_t_u_u(0x72L, ((safe_div_func_int32_t_s_s((((*g_1974) > (l_3281[3][6][2] , (+(&g_1729 != ((((l_3252[0] = (((p_22 ^ (((safe_rshift_func_int16_t_s_u(((**l_3177) = (safe_rshift_func_int8_t_s_s(((((((((*g_104) = ((void*)0 == l_3287[4][0][1])) ^ 0x2BL) , 0UL) , 0x8812DDD6L) , 5L) < 0x4DL) | g_418), p_22))), 11)) >= 0x85B613A7L) , (*g_104))) < g_3288[2][4]) ^ l_3289)) , l_3190[3][0]) , l_3008[3]) , &g_83[0][0][2]))))) > p_22), p_22)) || (*g_104))))) != 0xFC00B10DFFE3B664LL), p_22))) >= l_3242[2]) <= 0x7E35L)) & 0x03C3BE9CL)));
                    }
                }
                for (l_3198.f0 = 18; (l_3198.f0 >= (-13)); l_3198.f0--)
                { /* block id: 1454 */
                    const uint8_t l_3314 = 0x32L;
                    int32_t **l_3321 = (void*)0;
                    int32_t *l_3323 = &l_3239;
                    int32_t **l_3322[5][4] = {{&l_3323,&l_3323,&l_3323,&l_3323},{&l_3323,&l_3323,&l_3323,&l_3323},{&l_3323,&l_3323,&l_3323,&l_3323},{&l_3323,&l_3323,&l_3323,&l_3323},{&l_3323,&l_3323,&l_3323,&l_3323}};
                    const union U1 *l_3332[9] = {(void*)0,&g_3333,&g_3333,(void*)0,&g_3333,&g_3333,(void*)0,&g_3333,&g_3333};
                    int64_t *** const l_3336 = (void*)0;
                    int64_t *** const *l_3335[9][3] = {{&l_3336,&l_3336,&l_3336},{(void*)0,(void*)0,&l_3336},{(void*)0,&l_3336,&l_3336},{&l_3336,&l_3336,(void*)0},{&l_3336,(void*)0,&l_3336},{(void*)0,&l_3336,&l_3336},{&l_3336,&l_3336,&l_3336},{&l_3336,(void*)0,&l_3336},{&l_3336,&l_3336,&l_3336}};
                    int64_t *** const ** const l_3334 = &l_3335[8][2];
                    int i, j;
                    for (l_3239 = 0; (l_3239 < 20); l_3239 = safe_add_func_uint32_t_u_u(l_3239, 2))
                    { /* block id: 1457 */
                        uint8_t **l_3298 = &g_1974;
                        uint8_t ***l_3299 = &l_3298;
                        uint8_t *l_3305 = &g_141[0];
                        uint32_t ** const **l_3307 = (void*)0;
                        uint32_t ** const ***l_3308 = (void*)0;
                        uint32_t ** const ***l_3309 = (void*)0;
                        uint32_t ** const ***l_3310 = (void*)0;
                        uint32_t ** const ***l_3311[1][5][8] = {{{&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307},{&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307},{&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307},{&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307},{&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307,&l_3307}}};
                        int i, j, k;
                        (*p_21) = (safe_sub_func_uint8_t_u_u((*g_1974), (((((safe_add_func_uint16_t_u_u((&g_1974 == ((*l_3299) = l_3298)), (!(safe_lshift_func_uint16_t_u_u((4L || (p_22 == l_3239)), 13))))) != (safe_rshift_func_uint8_t_u_u(((*l_3305) = ((*g_1267) && p_22)), (+(((g_3312 = l_3307) != (void*)0) > p_22))))) != l_3314) ^ 0x47L) > g_3270[1][6])));
                    }
                    (*p_21) |= (safe_add_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(((((l_3319 = (*g_2075)) == ((l_3320 == (void*)0) , &l_3149)) < 246UL) == 0xE6FA29B601838E71LL), ((*g_448) , ((l_3324 = func_63((p_22 , 0xA31378405CAD1F1FLL), p_22, l_3132)) != (void*)0)))), l_3045));
                    for (g_1012 = 0; (g_1012 <= (-21)); g_1012--)
                    { /* block id: 1468 */
                        union U2 *l_3328[5][4] = {{&g_643[0][1][3],&g_3329,&g_643[0][1][3],&g_643[0][1][3]},{&g_3329,&g_3329,(void*)0,&g_3329},{&g_3329,&g_643[0][1][3],&g_643[0][1][3],&g_3329},{&g_643[0][1][3],&g_3329,&g_643[0][1][3],&g_643[0][1][3]},{&g_3329,&g_3329,(void*)0,&g_3329}};
                        int i, j;
                        l_3328[0][1] = l_3327[7];
                        l_3332[3] = &l_3170;
                        (**g_878) = p_21;
                        if ((*g_2991))
                            continue;
                    }
                    l_3337 = l_3334;
                }
                g_3340 = (void*)0;
                for (g_1627 = 0; (g_1627 >= 28); ++g_1627)
                { /* block id: 1479 */
                    int32_t l_3364 = 3L;
                    if ((safe_mod_func_uint8_t_u_u((safe_add_func_uint8_t_u_u((l_3348 != (void*)0), (l_3349[1][2] != &l_3350[0][0][1]))), (0x3DA9L || ((((safe_mul_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u((+((*g_2702) = (safe_mod_func_uint32_t_u_u(4294967290UL, (safe_add_func_int8_t_s_s(p_22, 0xBAL)))))), ((safe_mod_func_int8_t_s_s(((safe_unary_minus_func_int32_t_s((g_3363 , (*g_1252)))) > l_3239), (*g_104))) | p_22))) && p_22), l_3155)) && p_22) == p_22) && (**g_1674))))))
                    { /* block id: 1481 */
                        (**g_2075) = (void*)0;
                        (*g_232) = 0xC.37FD87p+37;
                        if (l_3364)
                            continue;
                    }
                    else
                    { /* block id: 1485 */
                        int32_t **l_3372 = &l_3031[0];
                        int32_t **l_3373 = (void*)0;
                        int32_t **l_3374 = &g_1267;
                        (*l_3374) = ((*l_3372) = ((***g_1801) = (((((void*)0 == l_3365) && (l_3265[7] < (((g_141[0] > ((+(*g_2702)) , p_22)) , ((*g_2702) &= (0xAE3A7F1CL >= (((*p_21) ^= ((l_3369 = (void*)0) == (void*)0)) <= l_3364)))) > p_22))) || l_3060) , p_21)));
                        l_3247 &= l_3375;
                    }
                    return (*g_145);
                }
            }
            else
            { /* block id: 1496 */
                int8_t *l_3376 = &g_1194;
                (*p_21) &= (0x6D34L & (((*l_3376) |= (*g_104)) != 0x45L));
            }
        }
        else
        { /* block id: 1500 */
            (**g_878) = func_63(p_22, p_22, l_3190[5][2]);
        }
        for (g_1083 = 0; (g_1083 <= (-17)); --g_1083)
        { /* block id: 1505 */
            int64_t l_3379 = 0xACBFDAE4233E3663LL;
            int32_t l_3408 = 0x8470C842L;
            int32_t l_3413 = 0x44D482BBL;
            int64_t *** const l_3414 = (void*)0;
            float **l_3417 = &g_1743[0][6][0];
            int32_t l_3419 = (-3L);
            l_3379 |= (*g_1252);
            for (g_2292 = 0; (g_2292 <= 8); g_2292 += 1)
            { /* block id: 1509 */
                uint8_t l_3411[1][5] = {{0xAAL,0xAAL,0xAAL,0xAAL,0xAAL}};
                float *l_3412 = &g_369;
                uint32_t l_3418 = 1UL;
                int i, j;
                if (((l_3419 = (safe_mod_func_uint32_t_u_u((((safe_div_func_uint8_t_u_u((((void*)0 == g_2082) || l_3384[5]), ((((((safe_lshift_func_int16_t_s_s((0xF3L != (safe_lshift_func_int8_t_s_u((((((safe_add_func_int8_t_s_s(0L, (safe_mul_func_int16_t_s_s(((****g_1891) |= ((safe_mul_func_uint8_t_u_u((safe_mod_func_int64_t_s_s(((((l_3413 = ((*l_3412) = ((safe_div_func_float_f_f(((((safe_sub_func_float_f_f(((safe_div_func_float_f_f((safe_mul_func_float_f_f((!(l_3408 = (p_22 > (safe_add_func_float_f_f(p_22, p_22))))), ((safe_div_func_float_f_f(0x1.840035p-45, 0xA.69DF52p+33)) != l_3263))), p_22)) > l_3411[0][3]), l_3411[0][3])) <= p_22) == 0xB.C68219p+44) <= 0x9.7p+1), p_22)) == 0x1.3BC909p-51))) < 0x4.7p-1) , l_3414) != (void*)0), p_22)), (*g_1974))) < l_3411[0][1])), g_3415)))) || (-7L)) , l_3416) == l_3417) && l_3411[0][4]), l_3046))), 1)) != l_3418) > p_22) , (-1L)) | l_3265[3]) || (*p_21)))) != p_22) <= l_3027), (*p_21)))) != (*p_21)))
                { /* block id: 1515 */
                    if ((!1UL))
                    { /* block id: 1516 */
                        return (*g_145);
                    }
                    else
                    { /* block id: 1518 */
                        return g_3421;
                    }
                }
                else
                { /* block id: 1521 */
                    for (g_418 = 6; (g_418 >= 0); g_418 -= 1)
                    { /* block id: 1524 */
                        return p_22;
                    }
                }
                l_3009 |= ((safe_mod_func_uint64_t_u_u(((g_1256.f0 != (safe_div_func_int32_t_s_s(l_3426, l_3413))) & g_3421), (safe_lshift_func_uint8_t_u_u((*g_1974), 1)))) ^ (g_3415 != ((safe_lshift_func_uint8_t_u_s(((*p_21) <= ((safe_unary_minus_func_int16_t_s((l_3265[4] == 0UL))) && (*g_104))), 1)) ^ 1L)));
                return p_22;
            }
            (*p_21) |= (l_3060 > (-1L));
        }
        g_3436 &= ((l_3435 = ((((l_3433 , (p_22 = (((*p_21) < 4UL) == (***g_1028)))) == 18446744073709551609UL) , l_3008[6]) , l_3435)) == (void*)0);
    }
    else
    { /* block id: 1536 */
        uint16_t ****l_3450 = &g_2242;
        int32_t l_3461 = 0x511C5D64L;
        int32_t l_3466 = 1L;
        int32_t l_3468[8][1] = {{0x5744168DL},{0L},{0x5744168DL},{0L},{0x5744168DL},{0L},{0x5744168DL},{0L}};
        float l_3469 = 0x9.4p+1;
        uint16_t **l_3492 = &g_634;
        uint8_t *l_3558 = &g_3415;
        const uint32_t *l_3560 = &g_2292;
        union U3 l_3571 = {0xEF07FC15L};
        uint64_t *l_3575[4];
        uint32_t *l_3590 = (void*)0;
        uint32_t *l_3591 = (void*)0;
        uint32_t *l_3592 = &g_721.f1;
        uint32_t *l_3593 = &l_3571.f1;
        uint32_t *l_3594 = (void*)0;
        uint32_t *l_3595 = &g_1774[3];
        int32_t *l_3605 = &l_3026;
        int32_t *l_3606 = &g_12[0][8][0];
        int32_t *l_3607[6] = {&g_29,&g_29,&g_29,&g_29,&g_29,&g_29};
        uint32_t l_3609 = 3UL;
        int i, j;
        for (i = 0; i < 4; i++)
            l_3575[i] = &g_142;
        if ((((safe_rshift_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u(((~(safe_lshift_func_int8_t_s_u((((((*p_21) = (+(~(&l_3235 == &g_2082)))) && (safe_mul_func_int16_t_s_s(0xFF81L, p_22))) > (safe_rshift_func_uint16_t_u_u(l_3054, 11))) < ((l_3452 = (g_3451 = l_3450)) == l_3450)), 0))) , ((((safe_add_func_uint8_t_u_u((p_22 , ((+((safe_mul_func_int16_t_s_s(1L, l_3265[0])) <= l_3054)) && p_22)), p_22)) < l_3458) != 0L) , 7UL)), (-8L))), (*g_1974))) ^ 1L) , l_3051))
        { /* block id: 1540 */
            int64_t l_3467 = 0L;
            int32_t l_3471 = 0xBE45EB6FL;
            for (g_1030 = 0; (g_1030 > 22); g_1030 = safe_add_func_uint64_t_u_u(g_1030, 1))
            { /* block id: 1543 */
                int16_t l_3462[9];
                int32_t *l_3463 = &l_3026;
                int32_t *l_3464 = &l_3009;
                int32_t *l_3465[3];
                int32_t l_3470 = 0x52D1C2FEL;
                int i;
                for (i = 0; i < 9; i++)
                    l_3462[i] = 0xC7FCL;
                for (i = 0; i < 3; i++)
                    l_3465[i] = &g_3421;
                l_3472--;
            }
        }
        else
        { /* block id: 1546 */
            int32_t l_3516 = (-1L);
            int32_t l_3517[7][7] = {{0xA25DCF4AL,0L,0xA25DCF4AL,0x06FACC9FL,0xA130A57AL,0xCA78F957L,0x8D661CF9L},{0xAF842136L,(-3L),(-1L),0x61A4D5FFL,(-1L),(-3L),0xAF842136L},{(-1L),0xB4419E0DL,0x8D661CF9L,0xA130A57AL,1L,0xCA78F957L,1L},{(-3L),0x2EBA8E7CL,0x2EBA8E7CL,(-3L),0x256B857FL,1L,0x61A4D5FFL},{(-8L),0L,0x8D661CF9L,0xA25DCF4AL,0xA25DCF4AL,0x8D661CF9L,0L},{0x256B857FL,0xAF842136L,(-1L),1L,0x2EBA8E7CL,0x61A4D5FFL,0x61A4D5FFL},{0xCA78F957L,(-8L),0xA25DCF4AL,(-8L),0xCA78F957L,0xB4419E0DL,1L}};
            int64_t l_3520 = 0x77D04CF66192042BLL;
            int32_t *l_3524 = &l_3045;
            int32_t *l_3525 = &l_3046;
            int32_t *l_3526 = (void*)0;
            int32_t *l_3527 = &l_3517[2][4];
            int32_t *l_3528 = &g_12[4][5][2];
            int32_t *l_3529 = &l_3466;
            int32_t *l_3530 = &l_3045;
            int32_t *l_3531 = &g_481;
            int32_t *l_3532 = &g_29;
            int32_t *l_3533 = &g_29;
            int32_t *l_3534[3];
            int64_t l_3548 = 0L;
            int i, j;
            for (i = 0; i < 3; i++)
                l_3534[i] = (void*)0;
            for (g_481 = 0; (g_481 <= 0); g_481 += 1)
            { /* block id: 1549 */
                float l_3479[7] = {0xE.95DC79p+29,0xE.95DC79p+29,0x5.3B2724p-2,0xE.95DC79p+29,0xE.95DC79p+29,0x5.3B2724p-2,0xE.95DC79p+29};
                uint16_t *l_3510 = (void*)0;
                int32_t l_3515[8][8][4] = {{{(-1L),(-3L),0L,(-1L)},{0L,(-1L),(-1L),(-1L)},{9L,9L,0xBDB089DDL,(-8L)},{9L,(-3L),(-1L),9L},{0L,(-8L),0L,(-1L)},{(-1L),(-8L),0xBDB089DDL,9L},{(-8L),(-3L),(-3L),(-8L)},{0L,9L,(-3L),(-1L)}},{{(-8L),(-1L),0xBDB089DDL,(-1L)},{(-1L),(-3L),0L,(-1L)},{0L,(-1L),(-1L),(-1L)},{9L,9L,0xBDB089DDL,(-8L)},{9L,(-3L),(-1L),9L},{0L,(-8L),0L,(-1L)},{(-1L),(-8L),0xBDB089DDL,9L},{(-8L),(-3L),(-3L),(-8L)}},{{0L,9L,(-3L),(-1L)},{(-8L),(-1L),0xBDB089DDL,(-1L)},{(-1L),(-3L),0L,(-1L)},{(-2L),(-1L),(-6L),(-6L)},{0L,0L,9L,(-3L)},{0L,0xBDB089DDL,(-6L),0L},{(-2L),(-3L),(-2L),(-6L)},{(-1L),(-3L),9L,0L}},{{(-3L),0xBDB089DDL,0xBDB089DDL,(-3L)},{(-2L),0L,0xBDB089DDL,(-6L)},{(-3L),(-1L),9L,(-1L)},{(-1L),0xBDB089DDL,(-2L),(-1L)},{(-2L),(-1L),(-6L),(-6L)},{0L,0L,9L,(-3L)},{0L,0xBDB089DDL,(-6L),0L},{(-2L),(-3L),(-2L),(-6L)}},{{(-1L),(-3L),9L,0L},{(-3L),0xBDB089DDL,0xBDB089DDL,(-3L)},{(-2L),0L,0xBDB089DDL,(-6L)},{(-3L),(-1L),9L,(-1L)},{(-1L),0xBDB089DDL,(-2L),(-1L)},{(-2L),(-1L),(-6L),(-6L)},{0L,0L,9L,(-3L)},{0L,0xBDB089DDL,(-6L),0L}},{{(-2L),(-3L),(-2L),(-6L)},{(-1L),(-3L),9L,0L},{(-3L),0xBDB089DDL,0xBDB089DDL,(-3L)},{(-2L),0L,0xBDB089DDL,(-6L)},{(-3L),(-1L),9L,(-1L)},{(-1L),0xBDB089DDL,(-2L),(-1L)},{(-2L),(-1L),(-6L),(-6L)},{0L,0L,9L,(-3L)}},{{0L,0xBDB089DDL,(-6L),0L},{(-2L),(-3L),(-2L),(-6L)},{(-1L),(-3L),9L,0L},{(-3L),0xBDB089DDL,0xBDB089DDL,(-3L)},{(-2L),0L,0xBDB089DDL,(-6L)},{(-3L),(-1L),9L,(-1L)},{(-1L),0xBDB089DDL,(-2L),(-1L)},{(-2L),(-1L),(-6L),(-6L)}},{{0L,0L,9L,(-3L)},{0L,0xBDB089DDL,(-6L),0L},{(-2L),(-3L),(-2L),(-6L)},{(-1L),(-3L),9L,0L},{(-3L),0xBDB089DDL,0xBDB089DDL,(-3L)},{(-2L),0L,0xBDB089DDL,(-6L)},{(-3L),(-1L),9L,(-1L)},{(-1L),0xBDB089DDL,(-2L),(-1L)}}};
                int i, j, k;
                (*p_21) = 0x9D7866FDL;
                for (g_3421 = 0; (g_3421 >= 0); g_3421 -= 1)
                { /* block id: 1553 */
                    uint16_t *****l_3488[3][6] = {{&l_3450,&l_3450,&l_3450,&l_3450,&l_3450,&l_3450},{&l_3450,&l_3450,&l_3450,&l_3450,&l_3450,&l_3450},{&l_3450,&l_3450,&l_3450,&l_3450,&l_3450,&l_3450}};
                    int32_t l_3491 = 0x3A1C46B1L;
                    union U1 l_3507 = {0x12A4L};
                    uint32_t *l_3508 = &g_721.f1;
                    int32_t l_3518 = 0xFD87DB53L;
                    int32_t l_3519[1];
                    uint8_t l_3521[6][8] = {{246UL,0xFDL,2UL,2UL,0xFDL,246UL,2UL,254UL},{0xFDL,246UL,2UL,254UL,246UL,246UL,254UL,2UL},{0xFDL,0xFDL,0x8DL,254UL,0xFDL,247UL,254UL,254UL},{246UL,0xFDL,2UL,2UL,0xFDL,246UL,2UL,254UL},{0xFDL,246UL,2UL,254UL,246UL,246UL,254UL,2UL},{0xFDL,0xFDL,0x8DL,254UL,0xFDL,247UL,254UL,254UL}};
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_3519[i] = 0xA79EF6C3L;
                    if (g_3288[(g_481 + 1)][(g_3421 + 6)])
                        break;
                    l_3491 |= (safe_div_func_uint64_t_u_u(18446744073709551615UL, ((*g_1974) & ((safe_div_func_uint32_t_u_u(((((*g_104) = l_3466) <= (safe_mod_func_uint64_t_u_u(g_1191.f0, 2UL))) , (((safe_mul_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u((*g_104), 7)), (((safe_add_func_int8_t_s_s((((g_3451 = (void*)0) != &g_2242) == ((safe_lshift_func_int8_t_s_s(((*g_245) == (**g_2075)), 1)) , 1L)), (*g_1974))) || 1L) > 0x89C17C22L))) > 0x62284CE4FAE8F376LL) >= l_3265[7])), 0x1096CED0L)) && (*p_21)))));
                    if (((g_36 != ((void*)0 == l_3492)) <= (safe_div_func_int32_t_s_s((safe_add_func_uint8_t_u_u((((((l_3458 , ((safe_mul_func_uint16_t_u_u(((((**g_1893) = (safe_sub_func_uint64_t_u_u((((safe_mul_func_int16_t_s_s(6L, (l_3491 = (l_3060 = (((*g_104) = (~((*g_634) |= 0x10F4L))) , p_22))))) , ((!(((*g_104) &= p_22) || ((l_3508 = ((*l_3187) = func_63(p_22, g_3288[(g_481 + 1)][(g_3421 + 6)], l_3507))) != p_21))) , p_22)) && (*g_634)), l_3509[1]))) , l_3510) == (void*)0), 0x4DA6L)) > p_22)) | (-3L)) != 0UL) | (-9L)) > g_1087), l_3468[6][0])), 0x10E0674FL))))
                    { /* block id: 1566 */
                        return g_3288[(g_481 + 1)][(g_3421 + 6)];
                    }
                    else
                    { /* block id: 1568 */
                        int32_t *l_3511 = (void*)0;
                        int32_t *l_3512 = &g_12[1][5][2];
                        int32_t *l_3513 = &l_3262;
                        int32_t *l_3514[7] = {&g_29,&g_29,&g_29,&g_29,&g_29,&g_29,&g_29};
                        int i;
                        --l_3521[3][2];
                    }
                }
            }
            --l_3535;
            (*l_3531) = ((0xB1B68694L <= (((l_3265[7] < ((*g_1267) ^ (~(((!((((safe_unary_minus_func_uint32_t_u((((safe_lshift_func_int8_t_s_s((*g_104), 6)) , (*l_3527)) & (safe_mul_func_uint8_t_u_u(((g_3547 , (p_22 , ((*g_2023) , ((*p_21) < p_22)))) != p_22), (*g_104)))))) , (*g_1252)) ^ p_22) & l_3548)) , (*g_2702)) == p_22)))) && 1L) >= 0xD3L)) && 1UL);
        }
        if ((*p_21))
        { /* block id: 1576 */
            return (*g_145);
        }
        else
        { /* block id: 1578 */
            const uint32_t *l_3561 = &g_99;
            int32_t *l_3562 = &l_3053;
            int64_t l_3568 = 0L;
            union U3 *l_3572 = &l_3571;
            int64_t *l_3578 = &g_1352;
            uint64_t *l_3580[2][7][4] = {{{(void*)0,(void*)0,&g_1030,(void*)0},{(void*)0,&g_1030,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1030},{(void*)0,(void*)0,&g_1030,(void*)0},{(void*)0,&g_1030,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1030},{(void*)0,(void*)0,&g_1030,(void*)0}},{{(void*)0,&g_1030,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1030},{(void*)0,(void*)0,&g_1030,(void*)0},{(void*)0,&g_1030,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1030},{(void*)0,(void*)0,&g_1030,(void*)0},{(void*)0,&g_1030,(void*)0,(void*)0}}};
            uint64_t **l_3579 = &l_3580[0][5][2];
            int i, j, k;
            for (g_479 = 2; (g_479 > 6); ++g_479)
            { /* block id: 1581 */
                float *l_3552 = &l_3007;
                float *l_3553 = &g_434.f1;
                uint64_t *l_3559[9][8][3] = {{{(void*)0,&g_1030,(void*)0},{&g_142,(void*)0,&g_142},{&g_142,(void*)0,(void*)0},{&g_1030,&g_1030,(void*)0},{(void*)0,&g_1030,&g_1030},{&g_1030,&g_142,&g_142},{&g_1030,&g_1030,&g_1030},{(void*)0,(void*)0,&g_1030}},{{&g_1030,(void*)0,(void*)0},{&g_142,&g_142,(void*)0},{&g_142,(void*)0,&g_1030},{(void*)0,(void*)0,&g_142},{&g_1030,&g_1030,&g_1030},{&g_1030,&g_142,&g_1030},{&g_142,&g_1030,&g_142},{&g_1030,&g_1030,&g_1030}},{{&g_142,(void*)0,(void*)0},{&g_1030,(void*)0,(void*)0},{&g_142,&g_1030,&g_1030},{&g_1030,&g_142,&g_1030},{&g_142,&g_1030,&g_142},{&g_1030,&g_1030,&g_1030},{&g_1030,&g_142,(void*)0},{(void*)0,&g_1030,&g_1030}},{{&g_142,&g_1030,&g_142},{&g_142,&g_1030,&g_1030},{&g_142,(void*)0,(void*)0},{&g_1030,&g_142,&g_142},{&g_142,&g_142,&g_142},{&g_142,&g_142,&g_142},{&g_1030,(void*)0,(void*)0},{&g_142,&g_1030,&g_1030}},{{&g_142,&g_142,&g_1030},{&g_142,&g_1030,(void*)0},{&g_1030,(void*)0,&g_142},{&g_1030,&g_142,&g_1030},{&g_1030,&g_142,&g_1030},{&g_1030,&g_142,&g_142},{&g_142,(void*)0,(void*)0},{(void*)0,&g_1030,&g_1030}},{{(void*)0,&g_1030,&g_1030},{(void*)0,(void*)0,(void*)0},{&g_142,&g_142,&g_142},{&g_1030,&g_1030,&g_142},{&g_1030,&g_1030,&g_142},{&g_1030,&g_142,(void*)0},{&g_1030,(void*)0,&g_1030},{&g_142,&g_1030,&g_142}},{{&g_142,&g_1030,&g_1030},{&g_142,(void*)0,(void*)0},{&g_1030,&g_142,&g_142},{&g_142,&g_142,&g_142},{&g_142,&g_142,&g_142},{&g_1030,(void*)0,(void*)0},{&g_142,&g_1030,&g_1030},{&g_142,&g_142,&g_1030}},{{&g_142,&g_1030,(void*)0},{&g_1030,(void*)0,&g_142},{&g_1030,&g_142,&g_1030},{&g_1030,&g_142,&g_1030},{&g_1030,&g_142,&g_142},{&g_142,(void*)0,(void*)0},{(void*)0,&g_1030,&g_1030},{(void*)0,&g_1030,&g_1030}},{{(void*)0,(void*)0,(void*)0},{&g_142,&g_142,&g_142},{&g_1030,&g_1030,&g_142},{&g_1030,&g_1030,&g_142},{&g_1030,&g_142,(void*)0},{&g_1030,(void*)0,&g_1030},{&g_142,&g_1030,&g_142},{&g_142,&g_1030,&g_1030}}};
                int32_t **l_3563 = &g_42;
                int i, j, k;
                l_3009 = (((*l_3553) = ((*l_3552) = l_3551)) != (safe_add_func_float_f_f((((**g_878) = l_3552) != ((*l_3563) = (((((4294967287UL >= (0xA4C1A30AE963B52BLL == ((*g_2702) |= (p_22 ^ (((g_1030 = (l_3558 != (void*)0)) >= p_22) | (((p_22 != 0UL) , l_3560) != l_3561)))))) <= p_22) , 0xBE34BEF6L) < p_22) , l_3562))), l_3046)));
            }
            l_3466 |= (*p_21);
            (***g_1801) = func_63(l_3054, (safe_mod_func_uint8_t_u_u((safe_sub_func_int16_t_s_s(l_3568, (safe_mod_func_int8_t_s_s(((((*l_3572) = l_3571) , func_63((*g_2702), ((l_3573 != (void*)0) , (*g_634)), (((l_3575[3] = l_3574) == ((*l_3579) = ((((((*l_3578) = (safe_sub_func_int8_t_s_s(0x8DL, (*g_104)))) > 18446744073709551610UL) & p_22) == 0x1D47L) , &g_1030))) , (*g_2023)))) == (*g_1742)), (*g_1974))))), (*g_104))), l_3581);
        }
        l_3051 |= (safe_mul_func_uint16_t_u_u((p_22 && (((((safe_unary_minus_func_uint16_t_u(3UL)) != (safe_add_func_int32_t_s_s((((g_3587 , (((safe_mul_func_uint16_t_u_u(p_22, 9UL)) , ((*p_21) = (&g_246 != (*l_3235)))) || (++(*l_3595)))) | (safe_sub_func_uint8_t_u_u(0x7AL, (safe_rshift_func_int8_t_s_u(((((safe_sub_func_uint32_t_u_u((0x26DFL && p_22), p_22)) , 250UL) == l_3468[3][0]) || (-1L)), 0))))) , l_3604), p_22))) >= 0UL) | l_3571.f0) ^ p_22)), p_22));
        l_3609--;
    }
    l_3625[1] = (l_3623[1][2] = ((*p_21) |= ((safe_mod_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u(((safe_mod_func_int64_t_s_s(1L, l_3458)) , ((safe_rshift_func_int8_t_s_s(l_3045, 1)) | (((**g_1042) , &g_104) == (void*)0))), 0)), (safe_mul_func_int8_t_s_s((*g_104), (*g_1974))))) <= (****g_1891))));
    return p_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_29 g_36 g_60 g_877 g_878 g_879 g_421 g_1087 g_187 g_481 g_1267 g_634 g_60.f0 g_1083 g_1155 g_104 g_93 g_1042 g_448 g_1041 g_1032 g_369 g_1086 g_1535 g_1194 g_32 g_907 g_446 g_447 g_434 g_145 g_810 g_1352 g_141 g_866 g_854 g_242 g_243 g_1096 g_1097 g_1093 g_418 g_1029 g_12 g_1627 g_1653 g_865 g_479 g_1673 g_1674 g_210 g_721.f0 g_1082 g_931 g_1729 g_83 g_90 g_1740 g_1028 g_223.f0 g_1893 g_1974 g_721.f1 g_2024.f0 g_852 g_99 g_223 g_1774 g_1891 g_1892 g_368 g_1030 g_2242 g_721 g_2075 g_2076 g_2292 g_245 g_232 g_519 g_2158 g_2157 g_2373 g_1801 g_2023 g_2024 g_1422.f0 g_1012 g_2414 g_2433 g_2460 g_1191.f0 g_1252 g_2625 g_2697 g_413 g_2700 g_2740 g_1741 g_2702 g_449 g_450 g_2972 g_2991
 * writes: g_29 g_36 g_32 g_42 g_1087 g_90 g_481 g_60.f0 g_1083 g_93 g_721.f1 g_448 g_1349 g_91 g_434.f1 g_369 g_1536 g_1194 g_210 g_434.f2 g_810 g_421 g_721 g_854 g_12 g_479 g_1082 g_246 g_1741 g_1774 g_187 g_2157 g_2024.f0 g_1030 g_2242 g_83 g_2292 g_1627 g_2158 g_243 g_1093 g_2460 g_141 g_1654 g_104 g_99 g_2687 g_2701 g_1012 g_2909 g_434 g_1267
 */
static union U0  func_23(int32_t * p_24, int32_t * p_25)
{ /* block id: 9 */
    int64_t l_2878[9] = {0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL,0xF2A07FD1A1F245C1LL};
    int32_t l_2886[10] = {0x91592F99L,0x91592F99L,0x91592F99L,0x91592F99L,0x91592F99L,0x91592F99L,0x91592F99L,0x91592F99L,0x91592F99L,0x91592F99L};
    union U1 l_2899 = {0x276FL};
    union U0 l_2904 = {0x32L};
    uint16_t **l_2922 = &g_634;
    union U1 ** const *l_2985[9][2];
    int32_t * volatile *l_2992 = &g_1267;
    int i, j;
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
            l_2985[i][j] = &g_2076;
    }
    for (g_29 = (-27); (g_29 > (-25)); ++g_29)
    { /* block id: 12 */
        uint32_t l_41 = 0xAED293E3L;
        int32_t l_2862 = 0L;
        union U3 l_2900 = {0x0DF5F2C6L};
        uint32_t **l_2905 = (void*)0;
        const union U1 *l_2984 = &g_2024;
        const union U1 * const * const l_2983[4][2] = {{&l_2984,&l_2984},{&l_2984,&l_2984},{&l_2984,&l_2984},{&l_2984,&l_2984}};
        const union U1 * const * const *l_2982[2];
        int i, j;
        for (i = 0; i < 2; i++)
            l_2982[i] = &l_2983[2][0];
        for (g_36 = 0; (g_36 <= (-7)); g_36 = safe_sub_func_uint64_t_u_u(g_36, 6))
        { /* block id: 15 */
            g_32 = (safe_mul_func_uint8_t_u_u(l_41, 0x20L));
        }
        for (l_41 = 0; (l_41 <= 3); l_41 += 1)
        { /* block id: 20 */
            int16_t l_2889 = 0xC44BL;
            int32_t l_2892 = 0xDC601750L;
            uint8_t l_2923 = 3UL;
            const int16_t **l_2990 = &g_865;
            const int16_t ** const *l_2989 = &l_2990;
            if (((g_42 = p_24) == ((safe_mod_func_uint32_t_u_u((4L >= func_45((safe_add_func_float_f_f(l_41, (!func_50((func_53(p_24, g_29) , p_25), p_24)))))), 1L)) , (**g_878))))
            { /* block id: 1304 */
                uint16_t l_2863 = 0x2B28L;
                --l_2863;
            }
            else
            { /* block id: 1306 */
                uint32_t l_2881 = 0x5CFBEB44L;
                int16_t *l_2890 = &g_1012;
                uint32_t *l_2891 = &g_1774[6];
                int32_t l_2929 = 0xDE76EB28L;
                union U1 *l_2950 = (void*)0;
                int64_t l_2965 = 8L;
                (*p_25) ^= (safe_rshift_func_uint16_t_u_u((((*l_2891) = (safe_lshift_func_uint8_t_u_s(((void*)0 == (*g_449)), (safe_div_func_int16_t_s_s((safe_sub_func_int32_t_s_s(0xE91F226DL, (safe_rshift_func_int16_t_s_u(((*l_2890) = (safe_mod_func_uint16_t_u_u((l_2878[3] |= 0x0228L), (safe_sub_func_int16_t_s_s(((**g_1893) &= l_2881), (((safe_rshift_func_int8_t_s_s(0xDCL, ((safe_mul_func_int16_t_s_s(((l_2886[3] < l_2886[6]) == ((safe_lshift_func_uint8_t_u_u((l_2889 &= 0x9AL), l_2862)) | 9UL)), (*g_634))) != l_2886[1]))) >= l_2886[3]) , 0L)))))), 6)))), l_2881))))) | g_1352), l_2892));
                for (g_1087 = 0; (g_1087 <= 3); g_1087 += 1)
                { /* block id: 1315 */
                    uint8_t l_2903 = 0xCAL;
                    uint16_t **l_2918 = &g_634;
                    union U0 l_2924 = {0xDFL};
                    uint32_t ***l_2925[4][4];
                    int32_t l_2927[9];
                    float *l_2971 = &l_2904.f1;
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 4; j++)
                            l_2925[i][j] = &l_2905;
                    }
                    for (i = 0; i < 9; i++)
                        l_2927[i] = 0xFD19B02AL;
                    if ((g_12[(g_1087 + 2)][(l_41 + 4)][l_41] = ((0UL | (safe_add_func_int8_t_s_s(l_2878[3], ((((*g_104) = (safe_add_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((l_2899 , l_2899.f0), (((((*l_2891) = (l_2900 , (safe_mod_func_uint8_t_u_u(l_2878[1], 1UL)))) , l_2903) & ((l_2904 , (void*)0) != l_2905)) , l_2899.f0))), (*g_1096)))) && (*g_1974)) >= l_2892)))) != l_2886[2])))
                    { /* block id: 1319 */
                        int32_t *l_2908 = &g_1086[0][0][3];
                        int32_t **l_2907 = &l_2908;
                        int32_t ***l_2906[2];
                        int32_t l_2917 = 0x70C17578L;
                        uint16_t ***l_2919 = &l_2918;
                        uint16_t **l_2921[8] = {&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634};
                        uint16_t ***l_2920 = &l_2921[2];
                        int64_t *l_2926[7][5] = {{&l_2878[1],&g_187[6],&g_187[6],&l_2878[1],&g_187[6]},{&l_2878[2],&l_2878[2],&l_2878[2],&l_2878[2],&l_2878[2]},{&g_187[6],&g_187[6],&g_1352,&g_1352,&g_187[6]},{(void*)0,&l_2878[2],&l_2878[2],(void*)0,&l_2878[2]},{&g_187[6],&g_187[6],&l_2878[1],&g_187[6],&g_187[6]},{&l_2878[2],(void*)0,&l_2878[2],&l_2878[2],(void*)0},{&g_187[6],&g_1352,&g_1352,&g_187[6],&g_1352}};
                        float *l_2928 = &g_91;
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_2906[i] = &l_2907;
                        g_2909 = (l_2892 , (void*)0);
                        (*l_2928) = (safe_div_func_float_f_f(((*g_1032) <= 0x0.Cp-1), (((l_2927[4] = ((*g_2702) = (safe_sub_func_uint16_t_u_u(((((g_12[(g_1087 + 2)][(l_41 + 4)][l_41] &= (((safe_add_func_int32_t_s_s(l_2917, l_2917)) , (l_2922 = ((*l_2920) = ((*l_2919) = l_2918)))) != &g_634)) <= l_2923) == ((l_2924 , l_2925[3][0]) == (void*)0)) , 0x5C82L), 65532UL)))) , 0x3.157FB1p+5) != l_2923)));
                    }
                    else
                    { /* block id: 1328 */
                        int32_t l_2948 = 0xE6D6BE06L;
                        union U1 l_2949 = {1UL};
                        l_2929 = ((l_2878[3] ^ (-4L)) > l_2927[4]);
                        (***g_1801) = func_63((safe_lshift_func_uint16_t_u_s(65532UL, (safe_mul_func_int8_t_s_s((((void*)0 == &g_2082) != ((-(safe_div_func_float_f_f(((*g_1096) , (safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f((*g_931), (safe_mul_func_float_f_f((((!(((***g_1041) = (**g_1042)) , (0x0.424968p-6 <= l_41))) < ((l_2927[4] < l_2904.f0) < l_2900.f0)) == l_2900.f0), l_2948)))), l_2881)), 0x2.782552p-9)), l_2923))), l_2862))) , l_41)), (*g_104))))), l_2881, l_2949);
                        l_2950 = (*g_2076);
                    }
                    if ((*g_1267))
                        continue;
                    (*g_2972) = ((*l_2971) = (0x4.DAE037p+94 > (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f((((safe_mul_func_uint8_t_u_u(l_2899.f0, (safe_mod_func_int64_t_s_s(((safe_lshift_func_int16_t_s_u(l_2929, (l_2878[7] , l_2929))) & ((safe_lshift_func_uint16_t_u_s(((0L > (*g_104)) ^ l_2965), 3)) || ((((safe_mod_func_int32_t_s_s((safe_mod_func_uint16_t_u_u((~((*g_1974) = 8UL)), g_12[(g_1087 + 2)][(l_41 + 4)][l_41])), 0xA56106B2L)) == l_41) && l_2924.f0) && (*p_25)))), 0x567D09328D4F5152LL)))) , l_2862) < l_2923), l_2862)), 0x0.Fp-1)), 0x0.7p-1))));
                }
            }
            for (l_2904.f2 = 0; (l_2904.f2 <= 3); l_2904.f2 += 1)
            { /* block id: 1342 */
                int16_t ***l_2973 = (void*)0;
                uint64_t *l_2988 = &g_434.f2;
                int i, j, k;
                (*g_1267) ^= (l_2973 == ((g_12[l_41][l_41][l_41] = (safe_sub_func_int64_t_s_s((((safe_div_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s(l_2878[3], (l_2862 &= l_2899.f0))), l_2878[8])) == ((safe_mul_func_int8_t_s_s((l_2982[1] == l_2985[0][0]), ((*g_104) <= ((((((*l_2988) = (safe_add_func_int8_t_s_s(0x9FL, 0xB7L))) ^ (*g_2702)) , l_2892) ^ l_2886[7]) || 65535UL)))) , 0xAAL)) , (*g_2702)), 18446744073709551614UL))) , l_2989));
            }
            return (***g_446);
        }
    }
    (*l_2992) = g_2991;
    return (**g_1042);
}


/* ------------------------------------------ */
/* 
 * reads : g_1030 g_1974 g_83 g_634 g_2373 g_1029 g_907 g_210 g_104 g_93 g_1801 g_878 g_879 g_2023 g_2024 g_1267 g_481 g_1422.f0 g_1012 g_141 g_2414 g_90 g_60.f0 g_1892 g_1893 g_1674 g_1096 g_1097 g_1673 g_2433 g_2460 g_1191.f0 g_1042 g_448 g_865 g_866 g_368 g_369 g_854 g_877 g_421 g_1653 g_1252 g_12 g_145 g_2625 g_2024.f0 g_36 g_1774 g_446 g_447 g_434 g_931 g_2697 g_413 g_2700 g_1891 g_2740 g_1740 g_1741 g_2702 g_1083
 * writes: g_1030 g_83 g_60.f0 g_421 g_481 g_90 g_93 g_1627 g_810 g_1093 g_2460 g_369 g_141 g_721.f0 g_479 g_1654 g_1083 g_104 g_99 g_2687 g_2701 g_210 g_2024.f0
 */
static uint32_t  func_45(float  p_46)
{ /* block id: 1111 */
    uint8_t l_2354 = 1UL;
    uint64_t *l_2355[9] = {&g_1030,&g_1030,&g_1030,&g_1030,&g_1030,&g_1030,&g_1030,&g_1030,&g_1030};
    int16_t *** const *l_2367 = &g_1892;
    const union U1 l_2370 = {0xDF12L};
    float **l_2380 = (void*)0;
    float **l_2381[2][8] = {{&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0]},{&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0],&g_1743[0][6][0]}};
    int64_t l_2382[1][8];
    int32_t l_2383 = 0x45E92913L;
    int32_t *l_2384 = (void*)0;
    uint8_t l_2397 = 0x7EL;
    union U3 **l_2399 = (void*)0;
    int64_t *l_2403[7];
    int64_t **l_2402 = &l_2403[6];
    uint8_t l_2417 = 1UL;
    int32_t l_2438 = 0x57D315D3L;
    int32_t l_2439 = (-1L);
    int32_t l_2441 = 5L;
    int32_t l_2442 = 0x0738B983L;
    int32_t l_2444 = 0xFED77CBAL;
    int32_t l_2450 = 0x685FEB4FL;
    int32_t l_2451 = 0xA352BA2AL;
    int32_t l_2454 = 0xE263D413L;
    int32_t l_2455 = 6L;
    int32_t l_2456 = 0x91AB292FL;
    int32_t l_2458 = 0L;
    union U2 **l_2480 = &g_247;
    union U2 ***l_2479 = &l_2480;
    union U1 l_2504 = {0xCBC2L};
    int32_t l_2534 = 0xFC0631CFL;
    union U0 ***l_2569 = &g_1042;
    uint32_t *l_2575 = &g_1774[4];
    int32_t ***l_2597 = &g_879;
    float l_2726 = 0x3.B38362p+66;
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
            l_2382[i][j] = 0x6BF40340A1BDD196LL;
    }
    for (i = 0; i < 7; i++)
        l_2403[i] = (void*)0;
    if ((l_2354 & (((++g_1030) || 0x462BFC98A5CB8EC4LL) || (safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u(l_2354, ((+((*g_1974)++)) || (65530UL && (safe_add_func_uint16_t_u_u(((*g_634) = (l_2367 != (void*)0)), (safe_mod_func_int8_t_s_s((l_2370 , (safe_mod_func_uint32_t_u_u((g_2373[2] , (l_2383 |= (safe_sub_func_int64_t_s_s((safe_rshift_func_uint16_t_u_s((((safe_add_func_int64_t_s_s(((l_2380 = l_2380) == l_2381[0][3]), 7UL)) && l_2382[0][3]) == (*g_1974)), (**g_1029))), (-1L))))), l_2370.f0))), (*g_104))))))))), 11)))))
    { /* block id: 1117 */
        int32_t l_2385 = 0L;
        (***g_1801) = l_2384;
        return l_2385;
    }
    else
    { /* block id: 1120 */
        union U1 l_2387[3][1] = {{{0xE839L}},{{0xE839L}},{{0xE839L}}};
        uint64_t *l_2388 = &g_1030;
        int32_t l_2398 = 0x11988AFCL;
        int32_t l_2445 = 0L;
        int32_t l_2446 = 1L;
        int32_t l_2447 = (-1L);
        int32_t l_2449 = 1L;
        int32_t l_2452 = 1L;
        int32_t l_2453 = 7L;
        int32_t l_2459[8][5] = {{0L,1L,1L,0L,1L},{0x6FD3FCBEL,0L,(-3L),0x7B65CB5CL,0x7B65CB5CL},{1L,0L,1L,1L,0L},{0x7B65CB5CL,1L,1L,0x7B65CB5CL,1L},{0x7B65CB5CL,0x7B65CB5CL,(-3L),0L,0x6FD3FCBEL},{1L,0x6FD3FCBEL,1L,1L,0x6FD3FCBEL},{0x6FD3FCBEL,1L,1L,0x6FD3FCBEL,1L},{0L,0x6FD3FCBEL,(-3L),0x6FD3FCBEL,0L}};
        const float **l_2467 = (void*)0;
        int64_t l_2503[6][10][4] = {{{1L,(-8L),1L,0x092D7B6EE5EA0C89LL},{1L,0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L},{(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL,0x092D7B6EE5EA0C89LL},{0x092D7B6EE5EA0C89LL,(-8L),0xD315BEF1D057D9B5LL,0xD315BEF1D057D9B5LL},{(-1L),(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL},{1L,(-8L),1L,0x092D7B6EE5EA0C89LL},{1L,0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L},{(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL,0x092D7B6EE5EA0C89LL},{0x092D7B6EE5EA0C89LL,(-8L),0xD315BEF1D057D9B5LL,0xD315BEF1D057D9B5LL},{(-1L),(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL}},{{1L,(-8L),1L,0x092D7B6EE5EA0C89LL},{1L,0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L},{(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL,0x092D7B6EE5EA0C89LL},{0x092D7B6EE5EA0C89LL,(-8L),0xD315BEF1D057D9B5LL,0xD315BEF1D057D9B5LL},{(-1L),(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL},{1L,(-8L),1L,0x092D7B6EE5EA0C89LL},{1L,0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L},{(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL,0x092D7B6EE5EA0C89LL},{0x092D7B6EE5EA0C89LL,(-8L),0xD315BEF1D057D9B5LL,0xD315BEF1D057D9B5LL},{(-1L),(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL}},{{1L,(-8L),1L,0x092D7B6EE5EA0C89LL},{1L,0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L},{(-1L),0x092D7B6EE5EA0C89LL,0xD315BEF1D057D9B5LL,0x092D7B6EE5EA0C89LL},{0x092D7B6EE5EA0C89LL,(-8L),0xD315BEF1D057D9B5LL,0xD315BEF1D057D9B5LL},{(-1L),(-1L),1L,(-8L)},{0xD315BEF1D057D9B5LL,(-1L),0xD315BEF1D057D9B5LL,1L},{0xD315BEF1D057D9B5LL,1L,1L,0xD315BEF1D057D9B5LL},{0x092D7B6EE5EA0C89LL,1L,(-8L),1L},{1L,(-1L),(-8L),(-8L)},{0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L,(-8L)}},{{0xD315BEF1D057D9B5LL,(-1L),0xD315BEF1D057D9B5LL,1L},{0xD315BEF1D057D9B5LL,1L,1L,0xD315BEF1D057D9B5LL},{0x092D7B6EE5EA0C89LL,1L,(-8L),1L},{1L,(-1L),(-8L),(-8L)},{0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L,(-8L)},{0xD315BEF1D057D9B5LL,(-1L),0xD315BEF1D057D9B5LL,1L},{0xD315BEF1D057D9B5LL,1L,1L,0xD315BEF1D057D9B5LL},{0x092D7B6EE5EA0C89LL,1L,(-8L),1L},{1L,(-1L),(-8L),(-8L)},{0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L,(-8L)}},{{0xD315BEF1D057D9B5LL,(-1L),0xD315BEF1D057D9B5LL,1L},{0xD315BEF1D057D9B5LL,1L,1L,0xD315BEF1D057D9B5LL},{0x092D7B6EE5EA0C89LL,1L,(-8L),1L},{1L,(-1L),(-8L),(-8L)},{0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L,(-8L)},{0xD315BEF1D057D9B5LL,(-1L),0xD315BEF1D057D9B5LL,1L},{0xD315BEF1D057D9B5LL,1L,1L,0xD315BEF1D057D9B5LL},{0x092D7B6EE5EA0C89LL,1L,(-8L),1L},{1L,(-1L),(-8L),(-8L)},{0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L,(-8L)}},{{0xD315BEF1D057D9B5LL,(-1L),0xD315BEF1D057D9B5LL,1L},{0xD315BEF1D057D9B5LL,1L,1L,0xD315BEF1D057D9B5LL},{0x092D7B6EE5EA0C89LL,1L,(-8L),1L},{1L,(-1L),(-8L),(-8L)},{0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L,(-8L)},{0xD315BEF1D057D9B5LL,(-1L),0xD315BEF1D057D9B5LL,1L},{0xD315BEF1D057D9B5LL,1L,1L,0xD315BEF1D057D9B5LL},{0x092D7B6EE5EA0C89LL,1L,(-8L),1L},{1L,(-1L),(-8L),(-8L)},{0x092D7B6EE5EA0C89LL,0x092D7B6EE5EA0C89LL,1L,(-8L)}}};
        uint16_t l_2550 = 0x86B4L;
        int64_t l_2564 = 1L;
        union U3 l_2699 = {0x135BA086L};
        int64_t *l_2845 = &g_1352;
        int64_t ** const l_2844 = &l_2845;
        int64_t ** const *l_2843 = &l_2844;
        int32_t *l_2861[4][1][9] = {{{&l_2439,&l_2447,&l_2439,&l_2438,&g_12[5][4][1],&l_2439,(void*)0,&l_2453,&l_2438}},{{&l_2439,&g_12[5][4][1],&l_2438,&l_2439,&l_2447,&l_2439,&l_2438,&g_12[5][4][1],&l_2439}},{{&g_481,&g_12[5][4][1],&l_2459[3][1],&l_2438,(void*)0,&g_481,&l_2438,&l_2534,&l_2438}},{{&l_2438,&l_2447,&l_2459[3][1],&l_2459[3][1],&l_2447,&l_2438,(void*)0,&l_2534,&l_2459[3][1]}}};
        int i, j, k;
        for (l_2383 = 0; (l_2383 <= 5); l_2383 += 1)
        { /* block id: 1123 */
            int32_t l_2386 = (-7L);
            int16_t ** const *l_2396 = &g_1893;
            union U1 l_2406 = {65527UL};
            int32_t l_2440 = 0xBB931B0DL;
            int32_t l_2443[10][6] = {{(-4L),0xF13A533CL,(-4L),(-4L),0xF13A533CL,(-4L)},{(-4L),0xF13A533CL,(-4L),(-4L),0xF13A533CL,(-4L)},{(-4L),(-4L),0L,0L,(-4L),0L},{0L,(-4L),0L,0L,(-4L),0L},{0L,(-4L),0L,0L,(-4L),0L},{0L,(-4L),0L,0L,(-4L),0L},{0L,(-4L),0L,0L,(-4L),0L},{0L,(-4L),0L,0L,(-4L),0L},{0L,(-4L),0L,0L,(-4L),0L},{0L,(-4L),0L,0L,(-4L),0L}};
            float l_2457 = 0x3.10BAFBp+42;
            float * const *l_2466 = &g_1743[0][4][0];
            union U3 l_2505 = {0xD85B91FCL};
            uint64_t l_2583 = 0UL;
            union U2 ****l_2602 = (void*)0;
            int8_t *l_2629 = &g_93;
            union U0 * const *l_2684 = (void*)0;
            union U0 ****l_2698[10][9] = {{&l_2569,&g_1041,&l_2569,&g_1041,&g_1041,&g_1041,&l_2569,&l_2569,&l_2569},{&l_2569,&l_2569,(void*)0,&g_1041,&l_2569,&l_2569,&l_2569,&g_1041,&l_2569},{&l_2569,&l_2569,&g_1041,&g_1041,&l_2569,&l_2569,&l_2569,&l_2569,&l_2569},{&l_2569,&g_1041,&l_2569,&g_1041,&g_1041,&g_1041,&l_2569,&l_2569,&l_2569},{&l_2569,&l_2569,(void*)0,&g_1041,&l_2569,&l_2569,&l_2569,&g_1041,&l_2569},{&l_2569,&l_2569,&g_1041,&g_1041,&l_2569,&l_2569,&l_2569,&l_2569,&l_2569},{&l_2569,&g_1041,&l_2569,&g_1041,&g_1041,&g_1041,&l_2569,&l_2569,&l_2569},{&l_2569,&l_2569,(void*)0,&g_1041,&l_2569,&l_2569,&l_2569,&g_1041,&l_2569},{&l_2569,&l_2569,&g_1041,&g_1041,&l_2569,&l_2569,&l_2569,&l_2569,&l_2569},{&l_2569,&g_1041,&l_2569,&g_1041,&g_1041,&g_1041,&l_2569,&l_2569,&l_2569}};
            int i, j;
            if (((*g_1267) ^= ((l_2386 , ((*g_2023) , (void*)0)) == (l_2387[2][0] , l_2388))))
            { /* block id: 1125 */
                uint16_t l_2389 = 65533UL;
                int64_t *l_2392 = &g_90;
                union U3 l_2393 = {1UL};
                int64_t **l_2405 = &l_2403[6];
                int32_t *l_2434 = (void*)0;
                int32_t *l_2435 = &g_481;
                int32_t *l_2436 = &l_2386;
                int32_t *l_2437[2][8][5] = {{{&g_29,&g_12[2][6][0],&l_2386,&l_2383,&g_8},{&g_481,(void*)0,&g_29,&g_5,&g_481},{&g_8,&g_481,(void*)0,&g_8,&g_12[0][8][0]},{(void*)0,&g_12[0][8][0],(void*)0,(void*)0,&g_12[0][8][0]},{&g_8,&g_481,&g_29,&g_12[2][6][0],(void*)0},{&g_12[2][6][0],&g_12[0][8][0],&l_2386,&g_12[0][8][0],(void*)0},{&g_12[0][8][0],&l_2386,&g_8,&g_8,&g_29},{&g_12[2][6][0],&g_5,&g_12[0][8][0],&g_481,&l_2386}},{{&g_8,(void*)0,(void*)0,(void*)0,&g_36},{(void*)0,&g_12[2][6][0],&l_2383,(void*)0,&l_2383},{&g_8,&g_8,&g_29,&g_481,&g_8},{&g_29,&g_12[0][8][0],&g_12[2][6][0],&g_5,(void*)0},{&g_8,&g_8,&g_481,&g_8,(void*)0},{&g_5,&g_12[0][8][0],&g_481,(void*)0,&g_29},{&g_29,&g_36,&g_12[0][8][0],&l_2383,&g_36},{&g_12[0][8][0],&g_12[2][6][0],&g_8,&g_36,&g_36}}};
                int8_t l_2448 = 0xF2L;
                int32_t l_2465 = 0xAFFB80BBL;
                const float ***l_2468 = &l_2467;
                uint8_t l_2501[7] = {255UL,252UL,252UL,255UL,252UL,252UL,255UL};
                const union U3 **l_2543 = (void*)0;
                int i, j, k;
                if (((0L ^ l_2389) , (((0x8E1C9646ABAFA499LL >= ((((*l_2392) = l_2382[0][1]) == ((l_2393 , (l_2398 = ((((((((g_1422.f0 >= (safe_sub_func_uint8_t_u_u(((void*)0 != l_2396), (*g_1974)))) , 0x0571A61B918F6E36LL) >= g_1012) >= (*g_1974)) < g_141[0]) != l_2386) && 0L) ^ l_2397))) <= l_2387[2][0].f0)) > l_2386)) , (void*)0) != l_2399)))
                { /* block id: 1128 */
                    int64_t ***l_2404[10] = {&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532,&g_532};
                    int32_t l_2413 = 0xBE8B728AL;
                    int i;
                    if ((safe_sub_func_int64_t_s_s(((l_2405 = l_2402) != (void*)0), (l_2406 , (l_2417 |= ((safe_rshift_func_uint8_t_u_s((safe_div_func_uint8_t_u_u(((l_2398 ^= (safe_lshift_func_int8_t_s_u(((*g_104) ^= l_2413), (*g_1974)))) != (l_2413 , 1L)), (g_2414[5] , (safe_rshift_func_int8_t_s_u((1L ^ l_2389), l_2389))))), l_2393.f0)) != (-6L)))))))
                    { /* block id: 1133 */
                        int32_t l_2418 = 0x5DD0038FL;
                        uint16_t *l_2425 = &g_1627;
                        uint16_t *l_2431 = &g_810;
                        l_2386 = l_2418;
                        l_2386 |= l_2393.f0;
                        (*g_1267) ^= ((safe_lshift_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u((l_2398 ^= ((safe_mul_func_int16_t_s_s(l_2406.f0, ((*l_2425) = ((*g_634) |= (((l_2413 & l_2418) == ((*l_2392) ^= l_2389)) & 0x3E17168CL))))) == l_2389)), 12)), ((safe_mod_func_uint32_t_u_u((((*g_104) = ((((*l_2431) = (safe_mul_func_uint8_t_u_u((((l_2393.f0 ^ (!0x5DL)) >= ((***g_1892) <= 0xA86EL)) ^ 0x7E4973F2DFE02593LL), 0x5CL))) || l_2389) && l_2413)) , 0xDA2504B5L), 0x92C5F34EL)) == 0xA272L))) > l_2413);
                        return (**g_1674);
                    }
                    else
                    { /* block id: 1144 */
                        l_2386 = l_2413;
                        return (***g_1673);
                    }
                }
                else
                { /* block id: 1148 */
                    (*g_2433) = &l_2393;
                }
                ++g_2460;
                if ((safe_rshift_func_uint8_t_u_s((l_2465 & (*l_2435)), (l_2466 == ((*l_2468) = l_2467)))))
                { /* block id: 1153 */
                    uint32_t l_2473 = 0UL;
                    int8_t l_2498 = 0x20L;
                    int32_t l_2506[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2506[i] = 0x2BAB9349L;
                    if (l_2440)
                        break;
                    if ((*l_2435))
                    { /* block id: 1155 */
                        int8_t l_2469 = 0L;
                        int32_t l_2471 = 0x3DC88A89L;
                        int32_t l_2472 = 0x2FCF99E5L;
                        (*g_879) = func_63(l_2469, (~(*g_1096)), l_2370);
                        --l_2473;
                        (*l_2435) &= ((!(safe_rshift_func_uint16_t_u_u(65535UL, 3))) > 1UL);
                        l_2479 = (void*)0;
                    }
                    else
                    { /* block id: 1160 */
                        int64_t l_2497[9] = {7L,7L,7L,7L,7L,7L,7L,7L,7L};
                        int32_t l_2502[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_2502[i] = 0xEE3D0E51L;
                        l_2506[0] = (~((safe_sub_func_uint64_t_u_u(((*l_2388) &= g_1191.f0), ((void*)0 == (*g_1042)))) || ((safe_div_func_int8_t_s_s(1L, (safe_sub_func_uint16_t_u_u((l_2453 || (safe_mod_func_uint32_t_u_u((((safe_sub_func_int16_t_s_s(((safe_mod_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_u((((l_2505 , (*g_104)) == 0x50L) < l_2473), 6)) >= l_2449), 0xA6A8L)) & 0x72L), (-2L))) , 1UL) == 1L), 8L))), l_2502[1])))) , (*l_2436))));
                        l_2506[0] = (safe_div_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(((***g_1673) == ((*g_1974) ^ (safe_rshift_func_uint16_t_u_u((((*g_1042) == (void*)0) && ((safe_mod_func_int16_t_s_s((safe_rshift_func_uint16_t_u_s(((safe_sub_func_int16_t_s_s(((safe_unary_minus_func_uint64_t_u(g_90)) , (safe_lshift_func_int8_t_s_u((((*l_2392) = ((*l_2435) & ((((safe_add_func_uint8_t_u_u(((*l_2436) = (3L && 0x64C5L)), (l_2370 , l_2497[1]))) ^ (*g_634)) & l_2506[0]) ^ l_2506[1]))) , (*g_104)), 0))), (*g_865))) & l_2506[0]), l_2502[0])), l_2473)) , 0x2871L)), 3)))), 0x7CF6A6F9L)), l_2502[1]));
                    }
                    for (l_2473 = 0; (l_2473 <= 5); l_2473 += 1)
                    { /* block id: 1174 */
                        return (*g_1096);
                    }
                }
                else
                { /* block id: 1177 */
                    uint8_t l_2527 = 0xB1L;
                    union U1 l_2544 = {1UL};
                    uint8_t *l_2549[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    for (l_2397 = 0; (l_2397 <= 5); l_2397 += 1)
                    { /* block id: 1180 */
                        p_46 = (!((l_2527 = ((*g_368) = (safe_mul_func_float_f_f(0xC.B4EECBp-74, (*g_368))))) >= (-0x10.Dp-1)));
                    }
                    (**g_878) = ((g_721.f0 = (safe_rshift_func_int8_t_s_u((safe_mod_func_int64_t_s_s(((safe_mod_func_uint64_t_u_u(g_854[1][4], ((g_141[0] = ((l_2534 = g_1097) , (l_2453 = ((safe_div_func_int64_t_s_s((safe_add_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u(l_2527, ((*g_1974) = (((void*)0 != l_2543) <= (l_2544 , (safe_div_func_uint16_t_u_u(0xE97EL, (safe_div_func_uint64_t_u_u(0xC487B60DEC5E2515LL, l_2417))))))))) & l_2459[3][1]), 0xF3DFL)), l_2443[6][5])), 0x9EDF5D58D90473E9LL)) != l_2505.f0)))) & 1UL))) | l_2443[6][5]), 4L)), 5))) , (void*)0);
                }
                --l_2550;
            }
            else
            { /* block id: 1193 */
                union U0 ***l_2567 = &g_1042;
                union U0 ****l_2568 = &l_2567;
                uint32_t **l_2574[8][10] = {{&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349},{&g_1349,&g_1349,(void*)0,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349},{&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,(void*)0,&g_1349,(void*)0},{&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349},{&g_1349,&g_1349,&g_1349,(void*)0,(void*)0,&g_1349,&g_1349,&g_1349,(void*)0,&g_1349},{&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349},{&g_1349,&g_1349,(void*)0,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349},{&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,&g_1349,(void*)0,&g_1349,(void*)0}};
                int32_t l_2576 = 0xF83382B7L;
                int32_t l_2578 = 0xB288D2DEL;
                int32_t l_2579 = (-1L);
                int32_t l_2580[3];
                int16_t l_2582[1][2];
                int8_t l_2652 = 1L;
                union U1 ** const *l_2653[10][1] = {{&g_2076},{&g_2076},{&g_2076},{&g_2076},{&g_2076},{&g_2076},{&g_2076},{&g_2076},{&g_2076},{&g_2076}};
                int16_t l_2655[8];
                union U3 **l_2670 = &g_1093;
                int32_t l_2688 = 0L;
                int i, j;
                for (i = 0; i < 3; i++)
                    l_2580[i] = 0xFA6C993DL;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_2582[i][j] = 0xB90AL;
                }
                for (i = 0; i < 8; i++)
                    l_2655[i] = 1L;
                if (((safe_lshift_func_int8_t_s_u((!(safe_mod_func_int8_t_s_s(((*g_104) |= (safe_div_func_int64_t_s_s(1L, (safe_mul_func_int8_t_s_s((safe_add_func_uint64_t_u_u(l_2387[2][0].f0, l_2564)), (safe_rshift_func_uint8_t_u_u((((*l_2568) = l_2567) != l_2569), 2))))))), (g_479 = (~((*g_1974) = ((l_2449 | ((safe_unary_minus_func_uint8_t_u(((safe_mod_func_int8_t_s_s(0xF9L, 0x3FL)) <= (((l_2575 = func_61((***g_877))) != l_2384) ^ l_2576)))) || l_2576)) || l_2564))))))), l_2406.f0)) >= (-1L)))
                { /* block id: 1199 */
                    int32_t *l_2577[9][8][3] = {{{&l_2383,&l_2450,&l_2447},{&g_481,&l_2444,&l_2443[6][3]},{(void*)0,(void*)0,(void*)0},{&l_2458,&l_2443[6][5],&l_2451},{&l_2459[1][0],(void*)0,(void*)0},{(void*)0,&l_2454,(void*)0},{&l_2534,&l_2459[1][0],(void*)0},{(void*)0,&l_2454,&l_2451}},{{&l_2446,&l_2455,(void*)0},{&g_5,(void*)0,&l_2443[6][3]},{&l_2576,&l_2455,&l_2447},{(void*)0,&l_2458,(void*)0},{&l_2444,&l_2440,&l_2459[5][1]},{&l_2453,&l_2451,&l_2398},{&l_2442,&l_2449,&l_2446},{&l_2451,(void*)0,&l_2398}},{{&l_2446,&l_2450,(void*)0},{&l_2451,&l_2440,&l_2440},{&l_2458,&l_2576,&l_2398},{&g_481,&l_2458,&g_12[0][8][0]},{&g_8,&l_2459[3][4],(void*)0},{&g_5,&l_2451,&g_29},{&l_2446,&l_2459[3][4],&g_36},{&l_2442,&l_2458,(void*)0}},{{&g_29,&l_2576,&l_2398},{&g_8,&l_2440,&l_2442},{&l_2576,&l_2450,&l_2458},{&l_2398,(void*)0,&l_2456},{&l_2455,&l_2459[3][1],&l_2450},{&l_2458,&l_2444,(void*)0},{&l_2458,(void*)0,&l_2447},{&l_2443[6][3],&l_2451,&l_2443[6][5]}},{{&l_2450,&l_2447,&l_2386},{&g_481,&g_5,&g_481},{&l_2449,&g_36,&g_5},{&l_2441,(void*)0,&l_2398},{&l_2442,&l_2386,(void*)0},{&l_2449,&g_12[0][8][0],&g_5},{&l_2442,&l_2450,(void*)0},{&l_2441,&l_2458,&g_12[0][8][0]}},{{&l_2449,&l_2444,&l_2455},{&g_481,&g_481,&g_29},{&l_2450,&l_2455,&l_2446},{&l_2443[6][3],&l_2444,&l_2451},{&l_2458,&l_2383,&l_2442},{&l_2458,&l_2449,&g_12[3][3][2]},{&l_2455,&l_2458,&l_2459[3][1]},{&l_2398,&l_2443[6][3],(void*)0}},{{&l_2576,&l_2383,&l_2445},{&g_8,&g_8,&l_2534},{&g_29,&l_2576,&l_2455},{&l_2442,&l_2444,&l_2444},{&l_2446,(void*)0,&l_2446},{&g_5,&l_2442,&l_2444},{&g_8,&g_5,&l_2455},{&g_481,&l_2456,&l_2534}},{{&l_2458,(void*)0,&l_2445},{&l_2451,&l_2456,(void*)0},{&l_2446,&g_12[1][6][1],&l_2459[3][1]},{&l_2451,&g_29,&g_12[3][3][2]},{&l_2398,&l_2455,&l_2442},{&g_12[0][8][0],(void*)0,&l_2451},{(void*)0,&g_8,&l_2446},{&l_2451,(void*)0,&g_29}},{{&g_12[1][6][1],&l_2455,&l_2455},{&l_2444,(void*)0,&g_12[0][8][0]},{&l_2383,&l_2459[5][1],(void*)0},{(void*)0,&l_2444,&g_5},{(void*)0,&l_2446,(void*)0},{&l_2398,&l_2444,&l_2398},{&l_2386,&l_2459[5][1],&g_5},{&l_2454,(void*)0,&g_481}}};
                    int16_t l_2581 = 0L;
                    uint16_t * const *l_2603 = &g_634;
                    int i, j, k;
                    ++l_2583;
                    for (g_1627 = 0; (g_1627 <= 3); g_1627 += 1)
                    { /* block id: 1203 */
                        uint16_t l_2590 = 1UL;
                        int i, j;
                        l_2459[3][1] = (safe_mul_func_uint8_t_u_u(((-10L) ^ (0x374226B9L | ((l_2590 | (((safe_sub_func_int16_t_s_s((safe_div_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s(l_2576, ((void*)0 != l_2597))), (safe_mul_func_uint16_t_u_u((++(*g_634)), (l_2602 != (void*)0))))), l_2580[0])) , (***g_1673)) <= l_2443[1][5])) == 8L))), (*g_1974)));
                        (*g_1653) = l_2603;
                    }
                    if (l_2564)
                        continue;
                    if (l_2582[0][1])
                        break;
                }
                else
                { /* block id: 1210 */
                    uint32_t ***l_2620[8][9][3] = {{{&l_2574[6][2],&l_2574[5][9],&l_2574[6][5]},{&l_2574[6][2],&l_2574[5][9],(void*)0},{&l_2574[6][2],&l_2574[6][2],&l_2574[6][5]},{&l_2574[6][2],&l_2574[6][2],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[6][2]},{&l_2574[6][2],&l_2574[5][9],&l_2574[6][5]},{&l_2574[6][2],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]}},{{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]}},{{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0}},{{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]}},{{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]}},{{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]}},{{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]}},{{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],&l_2574[1][8]},{(void*)0,(void*)0,&l_2574[6][2]},{&l_2574[6][5],(void*)0,(void*)0},{&l_2574[6][2],&l_2574[1][8],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[1][8]},{&l_2574[6][5],&l_2574[1][8],(void*)0},{(void*)0,&l_2574[6][2],&l_2574[6][2]},{&l_2574[6][2],&l_2574[6][2],&l_2574[6][2]}}};
                    union U1 ***l_2641 = &g_2076;
                    int32_t l_2651 = 0xE633B7DFL;
                    int i, j, k;
                    (*g_1267) ^= (l_2582[0][0] < ((safe_sub_func_int32_t_s_s(l_2503[3][6][2], (*g_1252))) < ((l_2443[1][5] = (safe_sub_func_uint32_t_u_u((((((((*g_104) = (safe_div_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s((safe_div_func_int64_t_s_s((((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(0xBBL, (safe_mul_func_int16_t_s_s(0L, 0L)))), (*g_145))) || (l_2398 = (safe_unary_minus_func_int8_t_s(l_2583)))) && (((l_2620[3][6][2] == (void*)0) <= l_2582[0][0]) < l_2446)), 0x60A7A9D0E5178DD4LL)))), l_2446))) , 0xBF9300B03BCF1EEALL) , l_2505) , (void*)0) == &g_1654) & l_2440), 1UL))) , (**g_1893))));
                    for (g_1083 = 0; (g_1083 <= 5); g_1083 += 1)
                    { /* block id: 1217 */
                        int8_t *l_2628 = &g_1194;
                        int8_t **l_2630 = (void*)0;
                        int8_t **l_2631 = (void*)0;
                        int8_t **l_2632[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        union U1 ** const **l_2654 = &l_2653[4][0];
                        int i, j;
                        p_46 = (((safe_lshift_func_int8_t_s_s(((safe_add_func_int32_t_s_s(((g_2625[3][9][1] , (++(*l_2575))) , (l_2628 == (g_104 = l_2629))), (((0xA3D6L <= (l_2445 |= (((l_2582[0][1] , (safe_mul_func_int8_t_s_s(((((safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_add_func_float_f_f((l_2641 != ((*l_2654) = ((safe_lshift_func_int8_t_s_s((safe_rshift_func_int16_t_s_u(((((*g_1974) = ((l_2652 |= ((safe_lshift_func_uint16_t_u_s((0L & (safe_mod_func_int32_t_s_s(l_2453, (~l_2459[2][4])))), l_2459[2][1])) == l_2651)) == 6L)) && (*g_1974)) | 0xF0716BD6L), 5)), g_210)) , l_2653[6][0]))), l_2579)), l_2440)), (-0x8.Cp+1))) , g_2024.f0) & 1L) == l_2651), l_2651))) == l_2655[1]) ^ l_2406.f0))) ^ (*g_1252)) >= 0x819E8F64C48FB477LL))) < g_36), g_1774[4])) , (-0x1.4p+1)) , 0xC.1EDC54p+13);
                    }
                    (*g_1267) |= ((safe_mod_func_int64_t_s_s((safe_mul_func_uint16_t_u_u((((***g_446) , 0UL) ^ l_2447), l_2582[0][1])), 0x8EAA157877EF40C7LL)) >= (*g_1974));
                    for (g_99 = 0; (g_99 <= 5); g_99 += 1)
                    { /* block id: 1230 */
                        return l_2651;
                    }
                }
                (*l_2670) = (void*)0;
                for (l_2576 = 8; (l_2576 >= 0); l_2576 -= 1)
                { /* block id: 1237 */
                    union U0 * const **l_2685 = (void*)0;
                    union U0 * const **l_2686[7][7][2] = {{{&l_2684,(void*)0},{&l_2684,(void*)0},{&l_2684,(void*)0},{(void*)0,&l_2684},{&l_2684,&l_2684},{&l_2684,&l_2684},{(void*)0,(void*)0}},{{&l_2684,(void*)0},{&l_2684,(void*)0},{&l_2684,&l_2684},{&l_2684,&l_2684},{&l_2684,&l_2684},{&l_2684,(void*)0},{&l_2684,(void*)0}},{{&l_2684,(void*)0},{(void*)0,&l_2684},{&l_2684,&l_2684},{&l_2684,&l_2684},{(void*)0,(void*)0},{&l_2684,(void*)0},{&l_2684,(void*)0}},{{&l_2684,&l_2684},{&l_2684,&l_2684},{&l_2684,&l_2684},{&l_2684,(void*)0},{&l_2684,(void*)0},{&l_2684,(void*)0},{(void*)0,&l_2684}},{{&l_2684,&l_2684},{&l_2684,&l_2684},{(void*)0,(void*)0},{&l_2684,(void*)0},{&l_2684,(void*)0},{&l_2684,&l_2684},{&l_2684,&l_2684}},{{&l_2684,&l_2684},{&l_2684,(void*)0},{&l_2684,(void*)0},{&l_2684,(void*)0},{(void*)0,&l_2684},{&l_2684,&l_2684},{&l_2684,&l_2684}},{{(void*)0,(void*)0},{&l_2684,(void*)0},{&l_2684,(void*)0},{&l_2684,&l_2684},{&l_2684,&l_2684},{&l_2684,&l_2684},{&l_2684,(void*)0}}};
                    int32_t l_2689 = 0x8DAB3FDEL;
                    int i, j, k;
                    for (l_2583 = 0; (l_2583 <= 5); l_2583 += 1)
                    { /* block id: 1240 */
                        if (l_2583)
                            break;
                    }
                    (*g_1267) |= (l_2440 , ((((-p_46) >= 0x1.50D49Bp+30) < ((*g_931) = l_2503[4][0][2])) , ((l_2689 = ((safe_add_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u((((void*)0 == (**l_2569)) != (((safe_sub_func_int32_t_s_s((((safe_add_func_uint32_t_u_u(((safe_rshift_func_int16_t_s_u(0x7052L, 5)) && ((g_2687[1][0] = l_2684) == (*l_2569))), l_2579)) , 0x4235446F5B73D2A1LL) | l_2688), l_2453)) || 0x1131A02425F4D44ALL) , l_2446)), (*g_145))), l_2689)), 0x05L)) | l_2689)) ^ l_2582[0][1])));
                    for (l_2439 = 7; (l_2439 >= 0); l_2439 -= 1)
                    { /* block id: 1249 */
                        uint32_t l_2690 = 0x80992601L;
                        l_2690++;
                    }
                }
            }
            (*g_1267) = (((l_2445 | ((safe_add_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(0xDEL, l_2398)), ((((g_2697 , 0xC861C3E5L) , (l_2505 , ((-1L) || ((void*)0 != l_2698[7][4])))) ^ (*g_865)) , 255UL))) | (*g_1974))) , l_2699) , l_2406.f0);
            for (l_2442 = 0; (l_2442 <= 4); l_2442 += 1)
            { /* block id: 1257 */
                union U0 l_2723 = {-6L};
                int64_t ***l_2846 = &l_2402;
                int32_t l_2856 = 0xF8E600BDL;
                uint64_t l_2857 = 0xF065A552F9DAE9E2LL;
                int i, j;
                l_2447 ^= (l_2459[l_2442][l_2442] || l_2505.f0);
                for (l_2417 = 0; (l_2417 <= 5); l_2417 += 1)
                { /* block id: 1261 */
                    int32_t l_2706 = 1L;
                    float ** const *l_2847[10] = {&l_2381[1][3],&l_2381[0][3],(void*)0,&l_2381[0][3],&l_2381[1][3],&l_2381[1][3],&l_2381[0][3],(void*)0,&l_2381[0][3],&l_2381[1][3]};
                    int i;
                    for (g_60.f0 = 0; (g_60.f0 <= 8); g_60.f0 += 1)
                    { /* block id: 1264 */
                        uint32_t l_2703 = 4294967295UL;
                        int16_t *l_2711 = (void*)0;
                        (*g_2700) = g_413;
                        (*g_1267) = l_2703;
                        (*g_1267) = ((safe_add_func_uint64_t_u_u((0L || (l_2445 &= ((0x4E4EL & ((l_2459[l_2442][l_2442] = (l_2706 ^ (safe_add_func_uint64_t_u_u(l_2406.f0, l_2446)))) , ((****g_1891) = (((l_2706 <= ((safe_mul_func_int16_t_s_s(((*g_104) || ((*g_1974) = 0xADL)), 0x7B80L)) != 0L)) != l_2706) >= 18446744073709551612UL)))) <= 1UL))), 0xB9D06D64F49B4328LL)) <= l_2706);
                    }
                    if (((((*g_1267) = ((safe_div_func_int16_t_s_s((l_2459[3][1] = (safe_lshift_func_int8_t_s_u(((l_2443[6][5] | 0UL) != ((!((l_2447 &= ((**g_1893) |= (safe_rshift_func_uint8_t_u_s((l_2459[l_2442][l_2442] & ((0x1C64CCEB2877AAC8LL < ((*g_1267) == (((((safe_lshift_func_int16_t_s_u((l_2723 , 0xE002L), 4)) > (safe_mul_func_uint8_t_u_u((l_2459[3][1] | l_2443[6][1]), 0x95L))) | l_2505.f0) && (***g_1673)) , 0xCCF06F89L))) && (*g_1252))), l_2452)))) > l_2505.f0)) < 0xA8B6L)), 6))), l_2505.f0)) , (*g_1267))) || 0x6FF3F011L) != 0x53L))
                    { /* block id: 1277 */
                        int64_t ** const **l_2758 = (void*)0;
                        int64_t *l_2762 = &g_187[6];
                        int64_t *l_2763 = &g_1352;
                        int64_t *l_2764 = (void*)0;
                        int64_t *l_2765[2];
                        int64_t *l_2766 = &l_2503[0][5][1];
                        int64_t *l_2767 = &g_1082;
                        int64_t *l_2768 = &l_2382[0][6];
                        int64_t *l_2769 = (void*)0;
                        int64_t *l_2770 = &l_2503[2][3][1];
                        int64_t *l_2771 = &l_2564;
                        int64_t *l_2772 = &l_2382[0][5];
                        int64_t *l_2773[4][3][5] = {{{&g_1083,(void*)0,&g_1352,&l_2564,&g_1083},{(void*)0,&g_1083,&g_1352,&g_1082,&l_2564},{&g_1352,&l_2503[4][3][1],&l_2564,&l_2503[4][3][1],&g_1352}},{{&g_1352,&l_2503[4][3][1],&g_1083,&g_187[6],&g_1082},{&g_1082,&g_1083,&l_2564,&g_1352,&g_1352},{&l_2503[4][3][1],(void*)0,&g_187[6],&l_2503[4][3][1],&g_1082}},{{&l_2564,&g_1352,&g_1352,&g_1352,&g_1352},{&g_1082,&g_1352,&g_1352,&g_1083,&l_2564},{(void*)0,&g_1082,&g_187[6],&g_1082,&g_1083}},{{&g_187[6],&l_2503[4][3][1],&l_2564,&l_2564,&g_1352},{(void*)0,&l_2564,&g_1083,&g_1352,&g_1083},{&g_1082,&g_1082,&l_2564,&g_1352,(void*)0}}};
                        int64_t *l_2774[9];
                        int64_t *l_2775 = &l_2382[0][3];
                        int64_t *l_2776 = &g_187[3];
                        int64_t *l_2777 = &g_187[6];
                        int64_t *l_2778 = &l_2382[0][3];
                        int64_t *l_2779 = &g_1083;
                        int64_t *l_2780 = (void*)0;
                        int64_t *l_2781 = &l_2382[0][3];
                        int64_t *l_2782 = &g_1352;
                        int64_t *l_2783 = &l_2382[0][1];
                        int64_t *l_2784 = &g_1083;
                        int64_t *l_2785 = (void*)0;
                        int64_t *l_2786 = &g_90;
                        int64_t *l_2787 = &l_2564;
                        int64_t *l_2788 = (void*)0;
                        int64_t *l_2789 = &l_2382[0][3];
                        int64_t *l_2790 = &l_2503[2][2][2];
                        int64_t *l_2791 = &l_2382[0][3];
                        int64_t *l_2792 = &g_1082;
                        int64_t *l_2793 = (void*)0;
                        int64_t *l_2794[2];
                        int64_t *l_2795 = &l_2564;
                        int64_t *l_2796[9][5] = {{&g_1352,(void*)0,(void*)0,&g_1352,&l_2503[3][4][3]},{&g_1083,&l_2564,&l_2382[0][3],&l_2382[0][3],&l_2382[0][3]},{&g_1082,&l_2503[1][1][2],&l_2382[0][3],&l_2503[3][4][3],&l_2503[3][4][3]},{&l_2564,(void*)0,&l_2564,&l_2503[4][3][1],(void*)0},{(void*)0,(void*)0,&l_2503[3][4][3],(void*)0,(void*)0},{&l_2564,&l_2503[5][6][1],(void*)0,&l_2382[0][3],(void*)0},{&g_1082,&g_1082,&l_2503[3][4][3],(void*)0,&g_1352},{&l_2503[5][6][1],&l_2564,&l_2564,&l_2503[5][6][1],(void*)0},{(void*)0,(void*)0,&l_2382[0][3],&l_2382[0][3],(void*)0}};
                        int64_t *l_2797 = &g_1082;
                        int64_t *l_2798 = &g_1352;
                        int64_t *l_2799[4];
                        int64_t *l_2800 = (void*)0;
                        int64_t *l_2801[10] = {&l_2564,&l_2564,&l_2564,&l_2564,&l_2564,&l_2564,&l_2564,&l_2564,&l_2564,&l_2564};
                        int64_t *l_2802[3][8] = {{&l_2382[0][1],&l_2382[0][1],&g_1083,&l_2382[0][1],&l_2382[0][1],&g_1083,&l_2382[0][1],&l_2382[0][1]},{&g_1352,&l_2382[0][1],&g_1352,&g_1352,&l_2382[0][1],&g_1352,&g_1352,&l_2382[0][1]},{&l_2382[0][1],&g_1352,&g_1352,&l_2382[0][1],&g_1352,&g_1352,&l_2382[0][1],&g_1352}};
                        int64_t *l_2803[3][1];
                        int64_t *l_2804 = &l_2382[0][1];
                        int64_t *l_2805 = &l_2503[4][3][1];
                        int64_t *l_2806 = &g_90;
                        int64_t *l_2807 = &g_1352;
                        int64_t *l_2808 = &g_1083;
                        int64_t *l_2809 = (void*)0;
                        int64_t *l_2810 = &g_1082;
                        int64_t *l_2811[10][2][2] = {{{(void*)0,&l_2503[5][5][0]},{&l_2503[4][3][1],&l_2564}},{{&g_1082,&g_1082},{&g_1082,&g_1082}},{{&g_1082,&l_2564},{&l_2503[4][3][1],&l_2503[5][5][0]}},{{(void*)0,&g_1082},{&g_1083,(void*)0}},{{&l_2382[0][3],&g_187[6]},{&l_2382[0][3],(void*)0}},{{&g_1083,&g_1082},{(void*)0,&l_2503[5][5][0]}},{{&l_2503[4][3][1],&l_2564},{&g_1082,&g_1082}},{{&g_1082,&g_1082},{&g_1082,&l_2564}},{{&l_2503[4][3][1],&l_2503[5][5][0]},{(void*)0,&g_1082}},{{&g_1083,(void*)0},{&l_2382[0][3],&g_187[6]}}};
                        int64_t *l_2812 = &g_1352;
                        int64_t *l_2813[5][6] = {{&g_90,&l_2503[5][3][3],&l_2503[5][3][3],&g_90,&l_2382[0][3],(void*)0},{(void*)0,&g_90,&l_2503[4][3][1],&g_90,(void*)0,(void*)0},{&g_90,(void*)0,(void*)0,(void*)0,(void*)0,&g_90},{&l_2503[5][3][3],&g_90,&l_2382[0][3],(void*)0,&l_2382[0][3],&g_90},{&l_2382[0][3],&l_2503[5][3][3],(void*)0,&l_2503[4][3][1],&l_2503[4][3][1],(void*)0}};
                        int64_t *l_2814[7][4][6] = {{{&g_90,&g_1083,&l_2503[5][1][2],&g_1083,&g_187[6],&l_2564},{&g_187[1],&g_90,&l_2503[5][1][2],&g_1352,&g_1083,&g_1083},{&l_2503[4][0][2],&g_1352,&l_2564,&l_2503[5][1][2],&l_2564,&g_1083},{&l_2503[5][1][2],&l_2564,&g_1083,&l_2503[4][3][1],&g_1083,&l_2564}},{{(void*)0,&g_1083,&g_187[6],&l_2503[0][7][2],&g_1083,&g_1083},{&g_1083,&g_187[1],&g_90,&l_2564,&l_2564,&g_187[6]},{&g_1083,&g_187[1],&l_2503[0][7][2],&l_2564,&g_1083,(void*)0},{&l_2503[4][3][1],&g_1083,&g_187[6],&g_1083,&g_1083,&g_1083}},{{&l_2382[0][3],&l_2564,&g_187[6],&g_1083,&g_1352,&l_2503[4][3][1]},{&l_2564,&g_1083,&g_90,&l_2503[0][7][2],&l_2382[0][3],&l_2564},{&l_2382[0][3],&l_2564,(void*)0,&l_2503[5][1][2],&g_1083,&l_2382[0][3]},{&l_2382[0][3],&g_187[6],&l_2503[5][1][2],&l_2503[0][7][2],(void*)0,&g_187[6]}},{{&l_2564,&l_2382[0][3],&g_1083,&g_1083,&l_2503[5][1][0],&g_1083},{&l_2503[4][3][1],(void*)0,&g_1352,(void*)0,&l_2503[4][3][1],&g_1083},{&g_90,&g_90,&l_2503[4][3][1],&l_2503[4][0][2],&l_2564,&l_2503[0][7][2]},{&l_2503[5][1][0],&l_2503[4][3][1],&g_187[6],&g_90,&l_2382[0][3],&l_2503[0][7][2]}},{{(void*)0,&g_187[1],&l_2503[4][3][1],&g_1083,&g_187[6],&g_1083},{&l_2382[0][3],&g_187[6],&g_1352,&g_90,&g_1083,&g_1083},{&g_1083,&g_1083,&g_1083,&g_1083,&g_1083,&g_187[6]},{&g_1083,&l_2564,&l_2503[5][1][2],&g_1083,&l_2503[0][7][2],&l_2382[0][3]}},{{&l_2564,&g_1083,(void*)0,&g_187[6],&l_2503[0][7][2],&l_2564},{&l_2564,&l_2564,&g_90,&g_187[1],&g_1083,&l_2503[4][3][1]},{&l_2382[0][3],&g_1083,&g_1083,&l_2503[5][1][0],&g_1083,&l_2503[5][1][0]},{&l_2564,&g_187[6],&l_2564,&l_2382[0][3],&g_187[6],(void*)0}},{{&l_2503[4][0][2],&g_187[1],&g_1083,&g_1083,&l_2382[0][3],&g_1083},{(void*)0,&l_2503[4][3][1],&g_1083,&g_1083,&l_2564,&l_2382[0][3]},{&l_2503[4][0][2],&g_90,&g_1083,&l_2382[0][3],&l_2503[4][3][1],&g_1352},{&l_2564,(void*)0,&g_187[6],&l_2503[5][1][0],&l_2503[5][1][0],&g_187[6]}}};
                        int64_t *l_2815 = &l_2382[0][3];
                        int64_t *l_2816[6][5] = {{&l_2382[0][3],&l_2382[0][3],&g_1352,&l_2564,&l_2382[0][3]},{&l_2382[0][3],&g_187[7],&g_1352,&l_2382[0][6],&g_1352},{&l_2382[0][3],&l_2382[0][3],&g_187[7],&l_2382[0][3],&l_2503[4][3][1]},{&l_2382[0][3],&l_2503[4][3][1],&l_2503[4][3][1],&l_2382[0][3],&l_2382[0][3]},{&l_2382[0][3],&g_1083,&l_2382[0][3],&l_2382[0][6],(void*)0},{&l_2503[4][3][1],&l_2503[4][3][1],&l_2382[0][3],&l_2564,(void*)0}};
                        int64_t *l_2817 = (void*)0;
                        int64_t *l_2818 = &l_2564;
                        int64_t *l_2819 = &l_2503[4][3][1];
                        int64_t *l_2820[5][1][7] = {{{(void*)0,&g_1082,&g_90,(void*)0,&l_2564,&l_2503[3][8][1],&l_2564}},{{&g_187[1],&l_2564,&l_2564,&g_187[1],&l_2503[4][1][1],&g_187[5],(void*)0}},{{&g_187[5],&l_2564,(void*)0,&l_2503[3][8][1],&g_90,&l_2503[4][1][1],(void*)0}},{{(void*)0,&g_1082,&g_187[5],&g_187[6],&g_187[5],&g_1082,(void*)0}},{{&g_1352,&g_187[6],&g_1082,(void*)0,&g_187[5],&g_1352,&l_2564}}};
                        int64_t *l_2821 = &g_187[6];
                        int64_t *l_2822 = &g_187[6];
                        int64_t *l_2823[4][7][3] = {{{&g_187[7],(void*)0,&g_187[6]},{&g_1082,&l_2382[0][7],(void*)0},{(void*)0,&g_90,&g_1352},{(void*)0,&l_2503[4][3][1],&l_2382[0][7]},{&g_187[6],(void*)0,&l_2382[0][3]},{&g_1083,&g_1352,&g_1083},{&g_1083,(void*)0,(void*)0}},{{&g_1352,&g_1083,&g_1352},{&g_187[6],&g_187[0],&g_1082},{&l_2503[4][3][1],&l_2503[4][2][0],&g_90},{&g_187[6],(void*)0,&g_187[7]},{&g_1352,(void*)0,&g_1083},{&g_1083,&g_1082,&l_2564},{&g_1083,&g_1083,&g_1082}},{{&g_187[6],&l_2503[5][4][1],&g_1083},{(void*)0,&l_2382[0][7],&l_2382[0][3]},{&l_2503[5][4][1],&l_2382[0][7],&g_1083},{&l_2382[0][3],&l_2503[5][4][1],&l_2503[4][3][1]},{(void*)0,&g_1083,&l_2564},{&g_1082,&g_1082,&l_2382[0][3]},{&l_2503[4][3][1],(void*)0,&g_1082}},{{&g_187[0],(void*)0,(void*)0},{&g_1082,&l_2503[4][2][0],(void*)0},{&l_2564,&g_187[0],(void*)0},{&g_187[6],&g_1083,&g_1082},{&l_2382[0][3],(void*)0,&l_2382[0][3]},{&g_187[2],&g_1352,&l_2564},{&g_1083,(void*)0,&l_2503[4][3][1]}}};
                        int64_t *l_2824 = (void*)0;
                        int64_t *l_2825 = &g_90;
                        int64_t *l_2826[8] = {&g_1083,&g_1083,&g_1083,&g_1083,&g_1083,&g_1083,&g_1083,&g_1083};
                        int64_t *l_2827 = &l_2382[0][2];
                        int64_t *l_2828 = &l_2382[0][3];
                        int64_t *l_2829 = (void*)0;
                        int64_t *l_2830 = &g_1082;
                        int64_t *l_2831 = (void*)0;
                        int64_t *l_2832 = &g_187[3];
                        int64_t *l_2833 = (void*)0;
                        int64_t *l_2834 = &l_2564;
                        int64_t *l_2835 = &g_1082;
                        int64_t *l_2836 = &l_2503[4][3][1];
                        int64_t *l_2837[8] = {&g_1352,(void*)0,&g_1352,&g_1352,(void*)0,&g_1352,&g_1352,(void*)0};
                        int64_t *l_2838[7][2][5] = {{{&g_90,&g_187[1],&l_2564,&l_2382[0][5],&l_2382[0][1]},{&l_2503[5][6][1],&g_187[6],&l_2382[0][5],&g_187[9],&l_2564}},{{&l_2382[0][1],&g_1352,&g_187[6],&l_2382[0][1],&g_1352},{&l_2503[5][6][1],&l_2564,&g_187[1],&g_90,&l_2382[0][1]}},{{&g_90,&g_1082,&g_1082,&g_187[9],&g_1083},{&g_187[1],&g_1352,&g_187[1],&g_1352,&g_1083}},{{&g_187[6],&l_2382[0][1],&l_2382[0][1],&g_187[6],&l_2382[0][1]},{&l_2564,&g_187[6],&g_187[7],&g_1083,&g_1352}},{{&l_2564,&l_2503[5][6][1],&l_2382[0][1],&g_187[1],&l_2564},{&g_187[6],&g_1352,&g_187[1],&g_1083,&l_2382[0][1]}},{{&l_2503[5][8][3],&l_2382[0][5],(void*)0,&g_187[6],&l_2382[0][5]},{&l_2382[0][1],&l_2503[5][6][1],&l_2564,&g_1352,&g_1352}},{{&l_2503[5][6][1],&g_187[6],&l_2564,&g_187[9],&l_2564},{&g_1352,&g_1352,(void*)0,&g_90,&g_1352}}};
                        int64_t *l_2839[8] = {&l_2503[4][3][1],&l_2503[4][3][1],&l_2503[4][3][1],&l_2503[4][3][1],&l_2503[4][3][1],&l_2503[4][3][1],&l_2503[4][3][1],&l_2503[4][3][1]};
                        int64_t *l_2840 = (void*)0;
                        int64_t *l_2841 = (void*)0;
                        int64_t *l_2842 = (void*)0;
                        int64_t ** const l_2761[6][6][6] = {{{&l_2797,&l_2773[3][2][2],&l_2789,&l_2762,&l_2771,&l_2819},{(void*)0,&l_2814[4][2][4],&l_2783,&l_2787,&l_2829,&l_2829},{(void*)0,&l_2809,&l_2809,(void*)0,&l_2762,&l_2795},{&l_2764,&l_2839[7],&l_2791,&l_2787,&l_2814[4][2][4],&l_2801[3]},{(void*)0,&l_2827,&l_2812,&l_2835,&l_2814[4][2][4],&l_2831},{(void*)0,(void*)0,&l_2783,&l_2837[6],&l_2767,&l_2817}},{{(void*)0,&l_2815,(void*)0,&l_2793,&l_2837[6],&l_2839[7]},{&l_2777,&l_2821,&l_2823[0][4][0],&l_2783,&l_2779,&l_2812},{&l_2784,&l_2781,&l_2814[4][2][4],&l_2806,&l_2835,&l_2821},{&l_2824,&l_2773[3][2][2],&l_2841,&l_2784,(void*)0,&l_2784},{&l_2819,(void*)0,&l_2819,&l_2797,&l_2809,&l_2781},{&l_2767,&l_2783,&l_2833,&l_2762,(void*)0,&l_2810}},{{&l_2831,&l_2799[0],(void*)0,&l_2762,&l_2841,&l_2797},{&l_2767,(void*)0,&l_2804,&l_2797,&l_2826[3],&l_2814[4][2][4]},{&l_2819,&l_2769,&l_2839[7],&l_2784,(void*)0,&l_2787},{&l_2824,(void*)0,&l_2827,&l_2806,&l_2821,&l_2795},{&l_2784,&l_2801[3],&l_2771,&l_2783,&l_2829,&l_2835},{&l_2777,&l_2824,&l_2829,&l_2793,&l_2793,&l_2829}},{{(void*)0,(void*)0,&l_2762,&l_2837[6],&l_2789,&l_2779},{(void*)0,&l_2817,(void*)0,&l_2835,&l_2819,&l_2762},{(void*)0,(void*)0,(void*)0,&l_2787,(void*)0,&l_2779},{&l_2769,&l_2787,&l_2762,&l_2773[3][2][2],&l_2823[0][4][0],&l_2829},{&l_2773[3][2][2],&l_2823[0][4][0],&l_2829,&l_2795,&l_2797,&l_2835},{&l_2810,&l_2812,&l_2771,&l_2767,&l_2804,&l_2795}},{{&l_2804,&l_2837[6],&l_2827,&l_2777,&l_2764,&l_2787},{&l_2827,&l_2814[4][2][4],&l_2839[7],&l_2819,&l_2839[7],&l_2814[4][2][4]},{(void*)0,&l_2826[3],&l_2804,&l_2815,&l_2787,&l_2797},{&l_2821,&l_2793,(void*)0,(void*)0,&l_2769,&l_2810},{&l_2789,&l_2793,&l_2833,&l_2831,&l_2787,&l_2781},{&l_2791,&l_2826[3],&l_2819,&l_2769,&l_2839[7],&l_2784}},{{&l_2826[3],&l_2814[4][2][4],&l_2841,&l_2823[0][4][0],&l_2764,&l_2821},{&l_2815,&l_2837[6],&l_2814[4][2][4],&l_2829,&l_2804,&l_2812},{&l_2841,&l_2812,&l_2823[0][4][0],&l_2791,&l_2797,&l_2839[7]},{&l_2817,&l_2823[0][4][0],(void*)0,(void*)0,&l_2823[0][4][0],&l_2817},{&l_2829,&l_2787,&l_2783,&l_2814[4][2][4],(void*)0,&l_2831},{&l_2797,(void*)0,&l_2819,(void*)0,&l_2826[3],&l_2806}}};
                        int64_t ** const *l_2760 = &l_2761[3][1][4];
                        int64_t ** const **l_2759[4][6] = {{&l_2760,&l_2760,&l_2760,&l_2760,&l_2760,&l_2760},{&l_2760,&l_2760,&l_2760,&l_2760,&l_2760,&l_2760},{&l_2760,&l_2760,&l_2760,&l_2760,&l_2760,&l_2760},{&l_2760,&l_2760,&l_2760,&l_2760,&l_2760,&l_2760}};
                        uint8_t *l_2848 = (void*)0;
                        uint8_t *l_2849 = (void*)0;
                        uint8_t *l_2850[9] = {&g_141[0],&g_141[0],&g_141[0],&g_141[0],&g_141[0],&g_141[0],&g_141[0],&g_141[0],&g_141[0]};
                        const int32_t l_2851 = (-2L);
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_2765[i] = &g_90;
                        for (i = 0; i < 9; i++)
                            l_2774[i] = &g_187[6];
                        for (i = 0; i < 2; i++)
                            l_2794[i] = &g_1082;
                        for (i = 0; i < 4; i++)
                            l_2799[i] = (void*)0;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_2803[i][j] = &l_2503[1][8][2];
                        }
                        (*g_1267) &= (safe_lshift_func_uint16_t_u_s((safe_mul_func_uint8_t_u_u(((safe_mod_func_int64_t_s_s((safe_div_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((safe_mod_func_int16_t_s_s((0xA58956D0L == (((safe_unary_minus_func_uint8_t_u((l_2459[l_2442][l_2442] = ((g_2740 , (((safe_sub_func_uint16_t_u_u(((safe_div_func_uint16_t_u_u(l_2452, (((7L > (((p_46 = p_46) , l_2446) , ((safe_mod_func_int32_t_s_s((safe_sub_func_int16_t_s_s((safe_unary_minus_func_int64_t_s((safe_rshift_func_uint8_t_u_s(((*g_1974) = (safe_sub_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((l_2843 = &l_2402) == l_2846), (-1L))), l_2723.f0))), l_2443[6][5])))), (*g_865))), l_2505.f0)) == (*g_104)))) , l_2443[9][0]) , (**g_1893)))) < g_854[2][4]), l_2452)) , (*g_1740)) == l_2847[8])) && (*g_2702))))) != l_2503[0][0][3]) > l_2723.f0)), l_2387[2][0].f0)), l_2723.f0)), l_2550)), l_2706)) || l_2851), l_2851)), l_2723.f0));
                        (*g_1267) = (0xD3C440C8L < 6UL);
                        if (l_2459[l_2442][l_2442])
                            break;
                    }
                    else
                    { /* block id: 1285 */
                        return l_2706;
                    }
                    for (g_2024.f0 = 0; (g_2024.f0 <= 5); g_2024.f0 += 1)
                    { /* block id: 1290 */
                        int32_t *l_2852 = &l_2456;
                        int32_t *l_2853 = &l_2459[6][4];
                        int32_t *l_2854 = &l_2450;
                        int32_t *l_2855[10][7][3] = {{{&g_36,(void*)0,&l_2453},{(void*)0,(void*)0,(void*)0},{&l_2454,&l_2442,(void*)0},{&g_12[5][5][1],&g_12[0][8][0],&l_2447},{&l_2440,(void*)0,&l_2455},{&l_2383,&g_5,&l_2459[7][3]},{&l_2440,&l_2451,(void*)0}},{{&g_12[5][5][1],(void*)0,&l_2446},{&l_2454,&l_2438,&g_481},{(void*)0,&l_2446,&l_2441},{&g_36,&l_2440,&l_2451},{&l_2459[l_2442][l_2442],&l_2447,&l_2383},{&g_481,&l_2450,&l_2440},{&l_2451,(void*)0,(void*)0}},{{&l_2450,&l_2453,&g_481},{&l_2447,&g_481,&l_2446},{&l_2443[6][5],&l_2440,&l_2442},{&l_2459[l_2442][l_2442],&l_2451,&g_12[5][7][0]},{&l_2440,&l_2440,(void*)0},{&g_36,&g_481,&l_2447},{(void*)0,&l_2453,&l_2451}},{{&l_2456,(void*)0,&l_2459[l_2442][l_2442]},{&l_2442,&l_2450,&l_2440},{&l_2439,&l_2447,&l_2449},{&l_2440,&l_2440,&l_2438},{&l_2459[7][3],&l_2446,&l_2449},{&l_2438,&l_2438,&l_2455},{&l_2445,(void*)0,&l_2443[6][5]}},{{(void*)0,&l_2451,&l_2443[6][5]},{&g_5,&g_5,&l_2398},{&l_2452,(void*)0,&l_2443[6][5]},{&l_2438,&g_12[0][8][0],&l_2443[6][5]},{(void*)0,&l_2442,&l_2455},{(void*)0,(void*)0,&l_2449},{&l_2459[2][4],(void*)0,&l_2438}},{{&l_2446,(void*)0,&l_2449},{&l_2453,&l_2455,&l_2440},{&l_2449,&l_2446,&l_2459[l_2442][l_2442]},{&l_2451,&g_12[0][8][0],&l_2451},{&l_2398,&l_2459[7][3],&l_2447},{&l_2458,&l_2440,(void*)0},{&g_12[0][8][0],&g_36,&g_12[5][7][0]}},{{&l_2451,&l_2440,&l_2442},{&g_12[0][8][0],&l_2383,&l_2446},{&l_2458,&l_2451,&g_481},{&l_2398,&l_2456,(void*)0},{&l_2451,&g_5,&l_2440},{&l_2449,&l_2441,&l_2383},{&l_2453,(void*)0,&l_2451}},{{&l_2446,&l_2459[3][1],&l_2441},{&l_2459[2][4],&g_481,&g_481},{&l_2447,&l_2451,&l_2445},{&g_5,&l_2452,(void*)0},{&g_12[0][8][0],&l_2447,&g_5},{&g_12[0][8][0],&l_2455,&l_2452},{&l_2446,&l_2447,&l_2438}},{{&l_2440,&l_2452,(void*)0},{&g_481,&l_2451,(void*)0},{&g_36,&l_2438,&l_2459[2][4]},{&g_5,(void*)0,&l_2446},{(void*)0,&g_5,&l_2453},{&l_2447,&l_2459[7][3],&l_2449},{&l_2440,&l_2459[2][4],&l_2451}},{{&l_2449,&l_2449,&l_2398},{&l_2459[l_2442][l_2442],&l_2450,&l_2458},{&l_2441,&l_2439,&g_12[0][8][0]},{&g_481,&l_2459[7][4],&l_2451},{(void*)0,&l_2441,&g_12[0][8][0]},{&l_2440,(void*)0,&l_2458},{&l_2446,&g_5,&l_2398}}};
                        int i, j, k;
                        l_2857++;
                    }
                }
            }
            for (l_2446 = 5; (l_2446 >= 1); l_2446 -= 1)
            { /* block id: 1297 */
                union U3 **l_2860[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_2860[i] = &g_1093;
                (*g_2433) = &l_2699;
                l_2861[1][0][1] = &l_2386;
            }
        }
        return (**g_1674);
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_210 g_145 g_60.f0 g_1267 g_481 g_1093 g_1041 g_1042 g_104 g_93 g_931 g_369 g_12 g_1893 g_907 g_1194 g_1673 g_1674 g_1096 g_1097 g_1974 g_83 g_1032 g_721.f1 g_187 g_2024.f0 g_852 g_99 g_223 g_1774 g_1086 g_1891 g_1892 g_879 g_368 g_479 g_854 g_1030 g_2242 g_721 g_2075 g_2076 g_2292 g_29 g_634 g_245 g_243 g_60 g_878 g_421 g_232 g_519 g_141 g_1627 g_2158 g_2157
 * writes: g_210 g_481 g_721 g_434.f1 g_12 g_369 g_187 g_2157 g_93 g_2024.f0 g_1774 g_421 g_479 g_854 g_1030 g_2242 g_83 g_2292 g_91 g_1627 g_2158 g_243
 */
static const float  func_50(int32_t * p_51, int32_t * p_52)
{ /* block id: 831 */
    uint16_t l_1795 = 0x0EAFL;
    int32_t l_1808[5][3] = {{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L)}};
    int64_t l_1851[1][9] = {{0x731177CE8A844216LL,0x731177CE8A844216LL,0x731177CE8A844216LL,0x731177CE8A844216LL,0x731177CE8A844216LL,0x731177CE8A844216LL,0x731177CE8A844216LL,0x731177CE8A844216LL,0x731177CE8A844216LL}};
    union U2 *l_1854 = &g_223[0];
    int16_t *****l_1889[4];
    union U0 ****l_1894 = &g_1041;
    int32_t l_1930[3][1][5] = {{{0L,0x503BC4D2L,0x503BC4D2L,0L,0x503BC4D2L}},{{0L,0L,1L,0L,0L}},{{0x503BC4D2L,0L,0x503BC4D2L,0x503BC4D2L,0L}}};
    uint16_t **l_2021[3][1][9] = {{{&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634}},{{&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634}},{{&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634,&g_634}}};
    union U3 l_2093 = {4294967295UL};
    int64_t l_2096 = (-2L);
    union U1 l_2115 = {5UL};
    int32_t l_2124[1];
    uint8_t l_2130 = 0x2DL;
    uint32_t **l_2136 = &g_851;
    union U3 **l_2209 = &g_1093;
    float ***l_2217 = (void*)0;
    const int16_t **l_2257 = &g_865;
    const int16_t ***l_2256 = &l_2257;
    const int16_t ****l_2255 = &l_2256;
    uint32_t ** const *l_2258 = (void*)0;
    uint8_t **l_2312 = &g_1974;
    int64_t l_2342 = 1L;
    const int32_t l_2353 = 0L;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_1889[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_2124[i] = 0x09BA4CA7L;
    for (g_210 = (-20); (g_210 >= 10); g_210 = safe_add_func_uint8_t_u_u(g_210, 9))
    { /* block id: 834 */
        int32_t l_1791 = 0x5A28500EL;
        int32_t l_1797 = 0xAA261109L;
        int32_t ****l_1802 = &g_878;
        union U0 l_1825 = {0x44L};
        union U1 **l_1839 = &g_243[0];
        uint8_t *l_1898[7] = {&g_141[0],&g_83[1][2][3],&g_83[1][2][3],&g_141[0],&g_83[1][2][3],&g_83[1][2][3],&g_141[0]};
        uint8_t **l_1897 = &l_1898[5];
        uint8_t **l_1899 = (void*)0;
        uint8_t *l_1900 = &g_83[0][0][0];
        int16_t *****l_1915 = &g_1891;
        int32_t l_1942 = 0x2CB32014L;
        int32_t l_1944 = (-1L);
        int32_t l_1958 = 1L;
        uint8_t l_1975 = 7UL;
        uint32_t **l_2011 = &g_1349;
        int32_t l_2038[5][10] = {{(-5L),1L,1L,0L,(-7L),1L,0xEF33C438L,0xEF33C438L,1L,(-7L)},{0L,1L,1L,0L,1L,(-5L),0x47BDAFFCL,1L,0x7AACADB6L,0L},{1L,0x5B27CFE7L,0x7AACADB6L,0x47BDAFFCL,1L,0L,1L,0x47BDAFFCL,0x7AACADB6L,0x5B27CFE7L},{(-7L),(-7L),0x1DA25D6DL,0L,3L,(-9L),(-5L),0x7AACADB6L,1L,1L},{(-7L),0L,(-9L),0L,0L,(-9L),0L,(-7L),0L,(-5L)}};
        uint32_t l_2043 = 0UL;
        int8_t l_2054 = 7L;
        uint32_t l_2056 = 0x3E0C886BL;
        int64_t l_2078 = (-1L);
        union U2 **l_2081[7] = {&g_247,&l_1854,&l_1854,&g_247,&l_1854,&l_1854,&g_247};
        union U2 *** const l_2080 = &l_2081[2];
        int i, j;
    }
    if (((*g_1267) &= ((safe_add_func_uint8_t_u_u(l_1795, (safe_div_func_uint64_t_u_u(((*g_145) < l_1808[0][1]), 18446744073709551611UL)))) && (safe_mod_func_uint64_t_u_u((((safe_mul_func_uint8_t_u_u((l_2093 , (18446744073709551615UL != 0x336DCBC1E4C3BA4ALL)), (safe_add_func_uint8_t_u_u((l_1930[0][0][1] ^ 3UL), l_1851[0][3])))) != 0x59CBL) > l_1808[0][1]), l_2096)))))
    { /* block id: 1005 */
        union U3 l_2100 = {0x46C2A812L};
        union U0 **l_2103[3];
        int32_t l_2104 = 0L;
        const union U2 *l_2108 = &g_643[3][1][0];
        const union U2 **l_2107[6] = {&l_2108,&l_2108,&l_2108,&l_2108,&l_2108,&l_2108};
        float *l_2109 = &g_434.f1;
        int i;
        for (i = 0; i < 3; i++)
            l_2103[i] = &g_448;
        (*l_2109) = (safe_sub_func_float_f_f((-0x1.1p-1), ((!(((((*g_1093) = l_2100) , (safe_div_func_float_f_f(((**l_1894) != l_2103[1]), ((l_2104 ^= (l_1930[0][0][0] , (*g_104))) , ((safe_mul_func_float_f_f(((l_2107[4] != &l_1854) , l_2104), l_1851[0][3])) >= (*g_931)))))) <= l_2100.f0) <= l_2100.f0)) < l_2100.f0)));
    }
    else
    { /* block id: 1009 */
        float l_2114[7][10] = {{0x1.Bp-1,0x2.2FC02Ap-65,0x0.2p-1,0x6.6p+1,0x5.FDA867p-65,0x8.B3891Fp-51,0x8.B3891Fp-51,0x5.FDA867p-65,0x6.6p+1,0x0.2p-1},{(-0x7.9p+1),(-0x7.9p+1),0x9.63742Cp-61,0x8.B3891Fp-51,0x6.6p+1,0x1.450B21p+42,0x9.FDB97Ap+82,(-0x5.6p-1),0x8.7DDDB4p-31,(-0x2.3p+1)},{0x9.63742Cp-61,0x6.6p+1,0x8.Fp+1,(-0x5.4p-1),0x6.Fp+1,(-0x2.3p+1),0x9.FDB97Ap+82,0x0.9p+1,0x1.7p+1,0x9.7p-1},{(-0x5.6p-1),(-0x7.9p+1),0x4.7p-1,0x7.7019AAp-57,(-0x2.3p+1),(-0x5.4p-1),0x8.B3891Fp-51,(-0x5.4p-1),(-0x2.3p+1),0x7.7019AAp-57},{0x0.3p+1,0x2.2FC02Ap-65,0x0.3p+1,0x9.FDB97Ap+82,0x7.94E7DEp+52,(-0x5.6p-1),0x1.450B21p+42,(-0x7.9p+1),0x1.4p+1,0xA.B03AC5p-81},{(-0x8.1p+1),0xA.B03AC5p-81,0x5.FDA867p-65,0x0.9p+1,0x1.Bp-1,0x4.7p-1,(-0x2.3p+1),(-0x7.9p+1),0x6.Fp+1,0x1.2DB87Ep+6},{0x0.2p-1,0x8.B3891Fp-51,0x0.3p+1,(-0x8.1p+1),0x1.4p+1,0x9.7p-1,(-0x5.4p-1),(-0x5.4p-1),0x9.7p-1,0x1.4p+1}};
        int32_t l_2116[4] = {0x74B5312BL,0x74B5312BL,0x74B5312BL,0x74B5312BL};
        int64_t *l_2123[10][5] = {{&g_90,&g_1352,&g_1083,&g_1352,&g_1083},{(void*)0,(void*)0,&l_1851[0][8],&l_1851[0][3],&l_1851[0][0]},{&g_1352,&g_90,&g_90,&g_1352,&g_1083},{&l_1851[0][3],&l_1851[0][3],&g_1082,&g_1082,&l_1851[0][3]},{&g_1083,&g_90,&l_2096,&g_1083,&g_1083},{&g_1083,(void*)0,&g_1083,&g_1082,&l_1851[0][8]},{&g_1352,&g_1352,&g_1083,&g_1352,&g_1352},{&g_1083,&l_1851[0][3],(void*)0,&l_1851[0][3],(void*)0},{&g_1083,&g_1083,&g_1083,&g_1352,(void*)0},{&l_1851[0][3],&g_1083,&g_1083,&l_1851[0][3],(void*)0}};
        int32_t l_2135 = 0x7545C407L;
        uint32_t ***l_2161 = &g_2158;
        float *l_2210[1];
        uint16_t * const l_2225[5][6][4] = {{{&l_2115.f0,&l_2115.f0,&g_2024.f0,&g_60.f0},{(void*)0,(void*)0,&g_60.f0,&g_2024.f0},{&l_2115.f0,(void*)0,&l_2115.f0,(void*)0},{&g_2024.f0,&g_810,&g_810,(void*)0},{&g_60.f0,&l_2115.f0,&g_2024.f0,&g_810},{&g_60.f0,&l_2115.f0,&g_2024.f0,&l_2115.f0}},{{&g_60.f0,&g_2024.f0,&g_810,&g_810},{&g_2024.f0,&g_2024.f0,&l_2115.f0,&g_60.f0},{&l_2115.f0,&g_60.f0,&g_60.f0,(void*)0},{(void*)0,&g_60.f0,&g_2024.f0,&g_60.f0},{&l_2115.f0,&g_60.f0,(void*)0,(void*)0},{&g_60.f0,&g_60.f0,(void*)0,&g_60.f0}},{{&l_1795,&g_2024.f0,&g_810,&g_810},{(void*)0,&g_2024.f0,&l_2115.f0,&l_2115.f0},{&g_810,&l_2115.f0,&g_60.f0,&g_810},{&g_810,&l_2115.f0,&l_2115.f0,(void*)0},{&l_2115.f0,&g_2024.f0,&g_2024.f0,&l_2115.f0},{(void*)0,&l_2115.f0,&l_2115.f0,&g_810}},{{&g_60.f0,&g_60.f0,(void*)0,&g_2024.f0},{&g_810,(void*)0,&l_2115.f0,&g_2024.f0},{&l_2115.f0,&g_60.f0,&g_60.f0,&g_810},{&l_1795,&l_2115.f0,&l_1795,&l_2115.f0},{&l_2115.f0,&g_2024.f0,(void*)0,&g_60.f0},{&g_2024.f0,&l_1795,&g_810,&g_2024.f0}},{{&g_810,&g_60.f0,&g_810,(void*)0},{&g_2024.f0,&l_2115.f0,(void*)0,(void*)0},{&l_2115.f0,&l_2115.f0,&l_1795,&g_810},{&l_1795,&g_810,&g_60.f0,&l_2115.f0},{&l_2115.f0,&g_60.f0,&l_2115.f0,&g_60.f0},{&g_810,&g_60.f0,(void*)0,&l_2115.f0}}};
        const union U0 *l_2346 = &g_434;
        union U1 *l_2352 = (void*)0;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2210[i] = &l_2114[2][4];
        if (((safe_lshift_func_uint16_t_u_s(65532UL, (safe_div_func_int8_t_s_s(l_1808[1][0], ((l_2115 , (((((l_2116[2] = (l_2116[3] || l_1808[4][1])) || (safe_lshift_func_uint16_t_u_s((l_2116[0] != ((safe_mod_func_uint32_t_u_u(4294967286UL, (((safe_lshift_func_uint16_t_u_u(l_1930[1][0][0], 7)) & (l_2124[0] = l_2116[3])) || l_2124[0]))) | l_1851[0][3])), l_1930[0][0][1]))) && l_2116[1]) , 0xD989L) & 0UL)) | (*p_51)))))) , l_1851[0][3]))
        { /* block id: 1012 */
            uint32_t **l_2125[6] = {&g_852,&g_852,&g_852,&g_852,&g_852,&g_852};
            union U1 l_2126 = {0UL};
            int32_t *l_2137 = &l_1930[0][0][0];
            int i;
            (*l_2137) ^= (((((l_2125[0] != (l_2126 , (((safe_mul_func_int8_t_s_s(((((~l_2116[3]) < l_2116[0]) == (l_2130 <= (l_2116[2] == (safe_sub_func_int16_t_s_s(((**g_1893) = 0xD127L), (((*g_1267) = ((*p_51) = (((l_2093 , (l_2135 ^= g_1194)) & 0L) || 0L))) <= (***g_1673))))))) , (*g_104)), (*g_1974))) | l_2116[1]) , l_2136))) <= 0x17L) >= l_1851[0][7]) , l_2116[3]) == 0xE9063373L);
            (*g_1032) = ((*g_1032) >= 0x2.5DD37Cp-45);
            for (g_721.f1 = 19; (g_721.f1 >= 33); g_721.f1 = safe_add_func_int64_t_s_s(g_721.f1, 1))
            { /* block id: 1021 */
                uint32_t ***l_2155[1];
                uint32_t ****l_2156 = (void*)0;
                int32_t l_2172[7] = {0x5B5E7E0FL,0x5B5E7E0FL,1L,0x5B5E7E0FL,0x5B5E7E0FL,1L,0x5B5E7E0FL};
                int32_t l_2193 = 1L;
                uint64_t l_2195 = 0x7B913F13FDD2B6C9LL;
                int i;
                for (i = 0; i < 1; i++)
                    l_2155[i] = &l_2136;
                (*p_51) ^= ((*g_1267) ^= (((safe_lshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u(((+(safe_unary_minus_func_int64_t_s((safe_unary_minus_func_uint16_t_u(0xD737L))))) > (g_187[6] &= l_1808[2][1])), (((((0x801981F21012EA6ELL && (((*g_104) = (safe_div_func_int64_t_s_s(((safe_lshift_func_uint8_t_u_s((0x95L >= (0x12859164L | ((safe_add_func_int64_t_s_s(l_2116[1], ((g_2157 = l_2155[0]) != (((safe_mul_func_int16_t_s_s(((9L <= l_2116[0]) && l_2116[1]), l_1930[0][0][2])) >= l_1930[1][0][4]) , l_2161)))) > (*l_2137)))), 5)) ^ (*l_2137)), (*l_2137)))) , 0x153123F66D416F2CLL)) ^ 1L) == 0x12A7D4E6L) <= l_1808[1][0]) | l_1808[4][1]))) , (*g_1974)), l_1808[1][0])), 2)) , l_2124[0]) & (*g_145)));
                for (g_2024.f0 = 0; (g_2024.f0 <= 47); ++g_2024.f0)
                { /* block id: 1029 */
                    int32_t l_2185 = 9L;
                    uint32_t *l_2188 = &g_1774[4];
                    int32_t l_2194 = 0x89156872L;
                    uint32_t **l_2196 = (void*)0;
                    (*g_1267) ^= (((safe_div_func_uint64_t_u_u(((safe_div_func_uint32_t_u_u(((safe_rshift_func_uint8_t_u_u((((((((1UL ^ (((((safe_div_func_int64_t_s_s((l_2172[4] = l_2172[6]), (safe_sub_func_uint16_t_u_u((safe_div_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_u(((((((((l_2185 = (((*g_852) , (*l_1854)) , ((****g_1891) |= (safe_add_func_uint32_t_u_u((((safe_mod_func_uint16_t_u_u(((l_2093.f0 != ((safe_sub_func_uint32_t_u_u(((l_2185 == (safe_lshift_func_uint8_t_u_u((*l_2137), 5))) , (**g_1674)), ((*l_2188)++))) & (safe_add_func_uint64_t_u_u(g_12[0][8][0], g_1086[4][0][1])))) && 0x82L), 0x7FC7L)) && l_1795) , l_2193), 1UL))))) , (void*)0) != (void*)0) , (*g_931)) <= l_2194) <= l_2194) < l_1930[0][0][0]) , 0xF7L), (*g_1974))) != l_2193), l_2195)), l_2193)))) || l_2172[6]) , 0xE32C69C1L) | 0x660226F0L) && (*g_145))) , l_2135) <= 0xC2B4L) < 0UL) ^ l_2093.f0) != l_2194) && 0xAED7L), (*g_1974))) != 0xA195800DL), 0x416E83F4L)) == (*g_1974)), l_2193)) , l_2196) == l_2196);
                }
            }
        }
        else
        { /* block id: 1037 */
lbl_2295:
            for (g_721.f1 = 15; (g_721.f1 == 17); ++g_721.f1)
            { /* block id: 1040 */
                (*g_879) = func_61(p_52);
                return (*g_1032);
            }
        }
        l_1808[1][0] = ((safe_div_func_float_f_f(((safe_sub_func_float_f_f((0xD.B560B0p+21 != ((safe_sub_func_float_f_f((safe_mul_func_float_f_f((0x6.9p-1 >= ((*g_368) == 0x1.Dp-1)), ((0xD.CDC047p+94 != (l_2130 != ((safe_sub_func_float_f_f(((0x0.8p-1 == ((l_2209 != (void*)0) >= l_2115.f0)) >= 0x4.C9BE8Dp-89), l_2124[0])) > (-0x6.7p+1)))) != l_2116[3]))), (-0x1.2p-1))) == l_2124[0])), l_2135)) != l_2116[1]), l_2135)) != l_1930[0][0][0]);
        for (g_479 = 0; (g_479 <= 7); g_479 += 1)
        { /* block id: 1048 */
            int32_t l_2218 = 0x5842D420L;
            int8_t *l_2223 = (void*)0;
            int8_t *l_2224 = &g_854[1][4];
            int32_t l_2234 = 0x1E3E58BDL;
            union U1 **l_2263 = &g_2023;
            int32_t l_2288 = 0L;
            int32_t l_2289 = (-3L);
            int32_t l_2290[5];
            union U2 * const *l_2335 = (void*)0;
            union U2 * const **l_2334[5];
            union U2 * const ** const *l_2333[7][3][3] = {{{(void*)0,&l_2334[0],&l_2334[0]},{(void*)0,&l_2334[1],&l_2334[0]},{&l_2334[3],&l_2334[1],&l_2334[1]}},{{&l_2334[4],&l_2334[0],(void*)0},{&l_2334[0],&l_2334[1],(void*)0},{&l_2334[4],&l_2334[1],&l_2334[0]}},{{&l_2334[0],&l_2334[0],&l_2334[3]},{&l_2334[4],&l_2334[1],&l_2334[3]},{&l_2334[0],&l_2334[1],(void*)0}},{{(void*)0,&l_2334[0],&l_2334[0]},{(void*)0,&l_2334[1],&l_2334[0]},{&l_2334[3],&l_2334[1],&l_2334[1]}},{{&l_2334[4],&l_2334[0],(void*)0},{&l_2334[0],&l_2334[1],(void*)0},{&l_2334[4],&l_2334[1],&l_2334[0]}},{{&l_2334[0],&l_2334[0],&l_2334[3]},{&l_2334[4],&l_2334[1],&l_2334[3]},{&l_2334[0],&l_2334[1],(void*)0}},{{(void*)0,&l_2334[0],&l_2334[0]},{(void*)0,&l_2334[1],&l_2334[0]},{&l_2334[3],&l_2334[1],&l_2334[1]}}};
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_2290[i] = 0x5C74422DL;
            for (i = 0; i < 5; i++)
                l_2334[i] = &l_2335;
            if (((((g_1774[g_479] < 65532UL) > (l_1930[0][0][0] = (g_1774[g_479] | (safe_div_func_int32_t_s_s((safe_sub_func_int16_t_s_s((safe_mod_func_int8_t_s_s(((l_2217 != (void*)0) > ((l_2218 ^= l_2096) > ((safe_mul_func_uint16_t_u_u((0x3DCAL & ((((*l_2224) ^= (*g_104)) || (l_2225[4][5][1] != l_2225[2][1][0])) && l_2124[0])), g_1774[g_479])) , 1L))), l_2135)), l_2096)), l_1795))))) | (*p_51)) & 4UL))
            { /* block id: 1052 */
                (*g_1267) |= l_2130;
            }
            else
            { /* block id: 1054 */
                uint64_t *l_2230 = (void*)0;
                uint64_t *l_2231[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t l_2232 = 0xC37C3DA6L;
                int32_t l_2233 = 0x96FC4C46L;
                uint16_t ****l_2239 = (void*)0;
                uint16_t ***l_2241[3][10] = {{&l_2021[0][0][5],(void*)0,(void*)0,&l_2021[0][0][5],(void*)0,(void*)0,&l_2021[0][0][5],(void*)0,(void*)0,&l_2021[0][0][5]},{(void*)0,&l_2021[0][0][5],(void*)0,(void*)0,&l_2021[0][0][5],(void*)0,(void*)0,&l_2021[0][0][5],(void*)0,(void*)0},{&l_2021[0][0][5],&l_2021[0][0][5],(void*)0,&l_2021[0][0][5],&l_2021[0][0][5],(void*)0,&l_2021[0][0][5],&l_2021[0][0][5],(void*)0,&l_2021[0][0][5]}};
                uint16_t ****l_2240 = &l_2241[2][7];
                uint16_t ****l_2243 = &g_2242;
                uint32_t **l_2253 = &g_1349;
                uint32_t ***l_2252[9][1] = {{&l_2253},{&l_2253},{&l_2253},{&l_2253},{&l_2253},{&l_2253},{&l_2253},{&l_2253},{&l_2253}};
                uint32_t ****l_2254 = &l_2252[1][0];
                union U0 *l_2260 = &g_434;
                uint16_t l_2278 = 0xBA0BL;
                int32_t l_2287[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
                float **l_2328 = &g_1743[0][4][0];
                int i, j;
                if (((((safe_mod_func_uint8_t_u_u(((*g_1974) = (safe_div_func_uint32_t_u_u((((--g_1030) || (l_2233 < (safe_lshift_func_uint16_t_u_s(((((*l_2240) = &l_2021[2][0][1]) != ((*l_2243) = g_2242)) , (((safe_mod_func_uint64_t_u_u((safe_mod_func_int64_t_s_s((safe_add_func_int64_t_s_s((((safe_div_func_float_f_f((((*l_2254) = l_2252[3][0]) != ((&g_1974 != (((0x5E77D5EE5EC6B710LL == (l_2255 == (void*)0)) <= 0UL) , (void*)0)) , l_2258)), 0x6.512DA0p-51)) < (*g_1032)) , l_1851[0][3]), 3UL)), l_1795)), l_2234)) <= 0xD883D955C1E8E586LL) , 0xE9E9L)), 10)))) == l_2096), l_2233))), g_1774[g_479])) || l_2130) , (**l_2209)) , (-2L)))
                { /* block id: 1060 */
                    union U0 *l_2259[3][6][7] = {{{&g_434,&g_434,(void*)0,&g_434,&g_434,(void*)0,&g_434},{&g_434,&g_434,&g_434,(void*)0,&g_434,&g_434,&g_434},{&g_434,&g_434,&g_434,(void*)0,&g_434,(void*)0,&g_434},{&g_434,&g_434,(void*)0,&g_434,&g_434,&g_434,&g_434},{&g_434,&g_434,(void*)0,&g_434,&g_434,&g_434,(void*)0},{&g_434,&g_434,(void*)0,&g_434,&g_434,(void*)0,&g_434}},{{&g_434,&g_434,&g_434,&g_434,&g_434,&g_434,&g_434},{&g_434,&g_434,&g_434,(void*)0,&g_434,(void*)0,(void*)0},{&g_434,&g_434,&g_434,&g_434,&g_434,&g_434,(void*)0},{(void*)0,(void*)0,(void*)0,&g_434,&g_434,&g_434,&g_434},{&g_434,&g_434,(void*)0,&g_434,&g_434,(void*)0,&g_434},{&g_434,&g_434,(void*)0,&g_434,&g_434,&g_434,&g_434}},{{(void*)0,(void*)0,(void*)0,(void*)0,&g_434,&g_434,&g_434},{&g_434,&g_434,&g_434,(void*)0,&g_434,&g_434,&g_434},{&g_434,&g_434,(void*)0,&g_434,(void*)0,&g_434,&g_434},{&g_434,&g_434,&g_434,&g_434,&g_434,(void*)0,&g_434},{&g_434,&g_434,&g_434,(void*)0,&g_434,(void*)0,&g_434},{(void*)0,&g_434,&g_434,&g_434,&g_434,(void*)0,&g_434}}};
                    int i, j, k;
                    l_2260 = l_2259[1][0][4];
                }
                else
                { /* block id: 1062 */
                    uint32_t l_2273 = 0x06C150AAL;
                    int32_t l_2274 = (-10L);
                    int32_t l_2281 = 0xCAD605E1L;
                    int32_t l_2282 = (-1L);
                    int32_t l_2283 = 0L;
                    int32_t l_2284 = 0xD73E908CL;
                    int32_t l_2285 = 0x6D497CD2L;
                    int32_t l_2286[7][5] = {{(-3L),0x50C77E13L,1L,0x50C77E13L,(-3L)},{(-1L),0x50C77E13L,(-9L),(-3L),(-9L)},{(-9L),(-9L),1L,(-3L),(-5L)},{0x50C77E13L,(-1L),(-1L),0x50C77E13L,(-9L)},{0x50C77E13L,(-3L),0xC420B417L,0xC420B417L,(-3L)},{(-9L),(-1L),0xC420B417L,1L,1L},{(-1L),(-9L),(-1L),0xC420B417L,1L}};
                    const int16_t ***l_2332 = &l_2257;
                    int i, j;
                    (*p_51) |= ((((safe_mul_func_uint16_t_u_u(((0x245EA2E3EEA3BC8FLL > (l_2116[1] = (l_2263 != (*g_2075)))) >= (((((safe_mul_func_uint8_t_u_u(l_2135, (*g_104))) || ((safe_unary_minus_func_uint16_t_u((safe_add_func_uint8_t_u_u((*g_1974), ((safe_div_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((*g_1974), l_2233)), 251UL)) > 4L))))) <= (-1L))) >= l_2273) , 0x76D8L) >= l_2232)), l_2135)) && l_2273) < l_2233) || l_2218);
                    (*p_51) |= (-5L);
                    if ((*p_51))
                    { /* block id: 1066 */
                        int32_t *l_2275 = &l_1930[0][0][0];
                        int32_t *l_2276 = &g_481;
                        int32_t *l_2277[8][4][8] = {{{&l_2218,&l_2234,&g_5,&l_2234,&l_2232,&l_1930[0][0][0],&l_2116[0],(void*)0},{&l_2218,&l_2124[0],&g_5,(void*)0,&g_8,&l_2124[0],(void*)0,(void*)0},{&l_2232,&l_2135,&l_2124[0],&l_2124[0],&l_2135,&l_2232,&g_12[0][8][0],&l_2234},{&l_2124[0],&g_8,(void*)0,&g_5,&l_2124[0],&l_2218,&g_8,&l_2116[0]}},{{&l_2124[0],&l_2218,&g_8,&l_2116[0],&l_2232,(void*)0,&g_29,&g_8},{&g_5,&l_2232,(void*)0,&l_2135,(void*)0,&l_2232,&g_5,&l_2124[0]},{&g_36,&l_2124[0],&g_29,(void*)0,&l_1930[0][0][0],&l_2124[0],&l_2116[0],(void*)0},{&l_2218,&l_1930[0][0][0],&l_1930[1][0][2],&g_8,&l_1930[0][0][0],&l_2218,&l_2234,&l_2135}},{{&g_36,&l_2116[0],(void*)0,(void*)0,(void*)0,(void*)0,&l_2116[0],&g_36},{&g_5,&l_2116[0],&l_2218,&g_29,&l_2232,(void*)0,(void*)0,&l_2218},{&l_2124[0],&l_2232,&l_2124[0],&l_1930[1][0][2],&g_29,(void*)0,&g_5,&l_2218},{&l_2234,&l_2116[0],&l_2218,(void*)0,&l_2234,(void*)0,&l_2218,&l_2116[0]}},{{&l_2218,&l_2116[0],&g_12[0][8][0],&l_2218,&l_2218,&l_2218,&l_1930[0][0][0],&l_1930[1][0][2]},{&g_12[2][8][1],&l_1930[0][0][0],(void*)0,&l_2124[0],&l_2218,&l_2124[0],&l_1930[0][0][0],&g_36},{(void*)0,&l_2124[0],&g_12[0][8][0],&l_2218,&l_2232,&l_2232,&l_2218,&g_12[0][8][0]},{&l_2232,&l_2232,&l_2218,&g_12[0][8][0],&l_2124[0],(void*)0,&g_5,&g_29}},{{&l_2124[0],&l_2218,&l_2124[0],(void*)0,&l_1930[0][0][0],&g_12[2][8][1],(void*)0,&g_29},{&l_2218,&l_2218,&l_2218,&g_12[0][8][0],&l_2116[0],&l_2218,&l_2116[0],&g_12[0][8][0]},{(void*)0,&l_2234,(void*)0,&l_2218,&l_2116[0],&l_2234,&l_2234,&g_36},{(void*)0,&g_29,&l_1930[1][0][2],&l_2124[0],&l_2232,&l_2124[0],&l_2116[0],&l_1930[1][0][2]}},{{(void*)0,&l_2232,&g_29,&l_2218,&l_2116[0],&g_5,&g_5,&l_2116[0]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_2116[0],&g_36,&g_29,&l_2218},{&l_2218,&l_1930[0][0][0],&g_8,&l_1930[1][0][2],&l_1930[0][0][0],&l_2218,&l_2218,&l_2218},{&l_2124[0],&l_1930[0][0][0],(void*)0,&g_29,&l_2124[0],&g_36,&l_1930[0][0][0],&g_36}},{{&l_2232,(void*)0,&l_2135,(void*)0,&l_2232,&g_5,&l_2124[0],&l_2135},{(void*)0,&l_2232,&l_2116[0],&g_8,&l_2218,&l_2124[0],&g_5,(void*)0},{&g_12[2][8][1],&g_29,&l_2116[0],(void*)0,&l_2218,&l_2234,&l_2124[0],&l_2124[0]},{&l_2218,&l_2234,&l_2135,&l_2135,&l_2234,&l_2218,(void*)0,&l_2218}},{{&g_8,(void*)0,&l_1930[1][0][2],&g_36,&l_2124[0],&g_12[0][8][0],(void*)0,&l_2218},{&g_29,&g_12[2][8][1],&l_2218,&g_36,&l_2218,(void*)0,&l_2124[0],&l_2218},{&l_2116[0],&l_2218,(void*)0,&l_2234,(void*)0,&l_2218,&l_2116[0],&l_2234},{&l_2218,&l_2234,&l_2124[0],&l_1930[1][0][2],(void*)0,&l_2135,&g_36,(void*)0}}};
                        int i, j, k;
                        l_2274 = (*g_1032);
                        l_2278++;
                        g_2292--;
                        if ((*p_51))
                            continue;
                    }
                    else
                    { /* block id: 1071 */
                        uint32_t l_2311 = 0x2674D907L;
                        int32_t l_2313[5][1] = {{0L},{6L},{0L},{6L},{0L}};
                        int16_t ***l_2331 = (void*)0;
                        int i, j;
                        if (g_99)
                            goto lbl_2295;
                        l_2313[3][0] = (l_2130 , ((safe_div_func_int8_t_s_s(0x07L, ((safe_lshift_func_uint16_t_u_u(((l_2135 = ((~(safe_add_func_int8_t_s_s((((safe_add_func_int32_t_s_s((-9L), (safe_add_func_int16_t_s_s(0x28D9L, ((safe_mod_func_int64_t_s_s((((-1L) <= ((((*g_104) = ((((safe_div_func_int64_t_s_s((l_2311 ^= g_29), (l_2096 || (((*g_634) > (((void*)0 == l_2312) ^ 0xF61160DDL)) & l_2313[2][0])))) | l_2116[3]) == (*g_1974)) == l_2289)) > l_2232) && l_2289)) , l_2130), l_2278)) > l_2218))))) , 0xE8EDL) < l_1851[0][3]), g_60.f0))) ^ l_2273)) , l_2116[3]), 0)) ^ l_2313[2][0]))) , l_2286[3][0]));
                        (*p_51) |= ((((safe_add_func_int64_t_s_s((l_1808[1][0] != (((((((safe_mul_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s(((**g_245) , (safe_add_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((l_2283 > (5UL < (&l_2289 != &l_2274))), (~(((void*)0 == l_2263) < ((void*)0 != (**g_878)))))), l_2093.f0))))), l_2232)) && (*g_104)) == l_2313[2][0]) < 4UL) , (void*)0) == l_2328) && (*g_1974))), l_2287[3])) & 0L) , g_1774[g_479]) & l_2313[2][0]);
                        (*g_1267) = ((l_2116[3] , (((l_2116[3] != (0UL || (safe_add_func_int64_t_s_s(0x7EA8D29EDD65E72BLL, l_2313[4][0])))) == (l_2331 == (((*g_104) && (g_1774[g_479] | 0xADA1L)) , l_2332))) , l_2289)) || l_1851[0][4]);
                    }
                    if (l_1930[0][0][0])
                        break;
                }
                if (g_1774[g_479])
                    continue;
                if (l_2116[3])
                    continue;
            }
            for (l_2288 = 0; (l_2288 <= 0); l_2288 += 1)
            { /* block id: 1087 */
                union U2 **l_2339 = &g_247;
                union U2 ***l_2338[8];
                union U2 ****l_2337 = &l_2338[3];
                union U2 *****l_2336 = &l_2337;
                const union U0 *l_2348 = &g_434;
                int i, j;
                for (i = 0; i < 8; i++)
                    l_2338[i] = &l_2339;
                if (((l_2333[2][1][1] == (((*g_232) = l_1851[l_2288][(l_2288 + 7)]) , ((*l_2336) = (void*)0))) && ((((safe_sub_func_int32_t_s_s(((((g_519 , (0xF5864DC9L > l_2093.f0)) & 0UL) && 0L) & (l_2342 = (l_1851[l_2288][(l_2288 + 4)] &= l_2124[0]))), (*p_51))) , (***g_1673)) , g_141[0]) , l_2130)))
                { /* block id: 1092 */
                    const int32_t l_2345 = 1L;
                    const union U0 **l_2347[10][3] = {{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346},{&l_2346,&l_2346,&l_2346}};
                    int i, j;
                    for (g_1627 = (-16); (g_1627 != 5); g_1627 = safe_add_func_int32_t_s_s(g_1627, 7))
                    { /* block id: 1095 */
                        (*g_2157) = (l_2345 , (*l_2161));
                    }
                    l_2348 = l_2346;
                }
                else
                { /* block id: 1099 */
                    for (g_210 = 0; (g_210 > 9); g_210 = safe_add_func_uint64_t_u_u(g_210, 5))
                    { /* block id: 1102 */
                        const uint32_t l_2351 = 1UL;
                        (*p_51) |= l_2351;
                    }
                }
            }
        }
        (*g_2076) = l_2352;
    }
    return l_2353;
}


/* ------------------------------------------ */
/* 
 * reads : g_60 g_29 g_877 g_878 g_879 g_421 g_1087 g_187 g_481 g_1267 g_634 g_60.f0 g_1083 g_1155 g_104 g_93 g_1042 g_448 g_1041 g_1032 g_369 g_1086 g_1535 g_1194 g_32 g_907 g_446 g_447 g_434 g_145 g_810 g_1352 g_141 g_866 g_854 g_242 g_243 g_1096 g_1097 g_1093 g_418 g_1029 g_12 g_1627 g_1653 g_865 g_479 g_1673 g_1674 g_210 g_721.f0 g_1082 g_931 g_1729 g_83 g_90 g_1740 g_1028 g_223.f0 g_36
 * writes: g_1087 g_90 g_481 g_60.f0 g_1083 g_93 g_721.f1 g_448 g_1349 g_91 g_434.f1 g_369 g_1536 g_1194 g_210 g_434.f2 g_810 g_421 g_721 g_854 g_12 g_479 g_1082 g_246 g_1741 g_1774 g_187 g_36
 */
static union U3  func_53(int32_t * p_54, int32_t  p_55)
{ /* block id: 22 */
    int32_t l_1780 = 0x1D19DE27L;
    int64_t *l_1785 = &g_187[6];
    int8_t l_1786 = 0xB7L;
    int32_t *l_1787[5] = {&g_36,&g_36,&g_36,&g_36,&g_36};
    union U3 l_1788 = {0x4830779CL};
    int i;
    g_36 &= ((func_56(g_60, func_61(&g_5), (g_29 , (***g_877))) , (safe_lshift_func_uint16_t_u_s((safe_div_func_int32_t_s_s((l_1780 , (safe_div_func_uint64_t_u_u((safe_add_func_int64_t_s_s((((*l_1785) = (l_1780 ^ l_1780)) != l_1786), 0xAD3A348549504FD1LL)), l_1786))), p_55)), 7))) > p_55);
    return l_1788;
}


/* ------------------------------------------ */
/* 
 * reads : g_1087 g_187 g_481 g_1267 g_634 g_60.f0 g_1155 g_104 g_93 g_1042 g_448 g_1041 g_1032 g_369 g_1086 g_1535 g_32 g_907 g_446 g_447 g_434 g_145 g_810 g_1352 g_141 g_866 g_1194 g_854 g_878 g_879 g_242 g_243 g_60 g_1096 g_1097 g_1093 g_418 g_1029 g_12 g_1627 g_1653 g_865 g_29 g_1673 g_479 g_1674 g_210 g_721.f0 g_931 g_1082 g_1729 g_83 g_1740 g_1028 g_90 g_1083 g_223.f0
 * writes: g_1087 g_90 g_481 g_60.f0 g_1083 g_93 g_721.f1 g_448 g_1349 g_91 g_434.f1 g_369 g_1536 g_1194 g_210 g_434.f2 g_810 g_421 g_721 g_854 g_12 g_479 g_1082 g_246 g_1741 g_1774
 */
static union U0  func_56(union U1  p_57, const int32_t * p_58, const int32_t * p_59)
{ /* block id: 645 */
    uint32_t l_1475 = 0xF98A421FL;
    int32_t l_1476[9] = {0xE768D1D0L,0x4E9C82F2L,0xE768D1D0L,0x4E9C82F2L,0xE768D1D0L,0x4E9C82F2L,0xE768D1D0L,0x4E9C82F2L,0xE768D1D0L};
    const union U0 l_1519 = {-7L};
    uint32_t ** const l_1543 = &g_1349;
    int64_t l_1544 = 1L;
    int64_t l_1559[1];
    int32_t l_1560 = 0x703C7ECDL;
    uint16_t l_1561 = 65534UL;
    union U3 l_1590 = {1UL};
    int8_t *l_1773[9] = {&g_479,&g_479,&g_479,&g_479,&g_479,&g_479,&g_479,&g_479,&g_479};
    int i;
    for (i = 0; i < 1; i++)
        l_1559[i] = 0xB8FDA93838F5197DLL;
    for (g_1087 = 2; (g_1087 <= 9); g_1087 += 1)
    { /* block id: 648 */
        int16_t **l_1472 = &g_907;
        int16_t ***l_1471[6] = {&l_1472,&l_1472,&l_1472,&l_1472,&l_1472,&l_1472};
        const uint16_t *l_1496[9][6] = {{&g_693,&g_810,&g_693,&g_810,&g_693,&g_810},{&g_60.f0,&g_810,&g_60.f0,&g_810,(void*)0,&g_810},{&g_693,&g_810,&g_693,&g_810,&g_693,&g_810},{&g_60.f0,&g_810,&g_60.f0,&g_810,(void*)0,&g_810},{&g_693,&g_810,&g_693,&g_810,&g_693,&g_810},{&g_60.f0,&g_810,&g_60.f0,&g_810,(void*)0,&g_810},{&g_693,&g_810,&g_693,&g_810,&g_693,&g_810},{&g_60.f0,&g_810,&g_60.f0,&g_810,(void*)0,&g_810},{&g_693,&g_810,&g_693,&g_810,&g_693,&g_810}};
        const uint16_t ** const l_1495 = &l_1496[7][4];
        union U0 l_1502 = {1L};
        int i, j;
        if (g_187[g_1087])
        { /* block id: 649 */
            int16_t ****l_1473 = &l_1471[1];
            int32_t l_1474[2];
            int64_t *l_1494 = &g_90;
            int i;
            for (i = 0; i < 2; i++)
                l_1474[i] = 0xD11AA836L;
            if ((*p_58))
                break;
            (*g_1267) = (((((((safe_sub_func_int16_t_s_s((((*l_1473) = l_1471[2]) != (void*)0), 0xB44BL)) <= (((*l_1494) = ((((l_1474[0] <= (l_1476[0] ^= l_1475)) , (safe_div_func_int8_t_s_s((safe_sub_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u((safe_add_func_int8_t_s_s(p_57.f0, (safe_lshift_func_uint16_t_u_s(p_57.f0, (safe_div_func_uint32_t_u_u(((+((safe_lshift_func_int8_t_s_s(((p_57.f0 < ((safe_mod_func_uint64_t_u_u((l_1474[0] ^ (-7L)), p_57.f0)) >= p_57.f0)) , 0x3CL), 2)) ^ 1L)) && (-4L)), l_1474[0])))))), g_187[g_1087])), l_1476[0])), l_1476[0]))) || l_1474[0]) != g_187[g_1087])) , 0x67E6L)) > 0xADA51567486CA722LL) | l_1474[1]) & l_1475) , l_1495) == (void*)0);
            (*g_1267) |= 0x41A351F6L;
        }
        else
        { /* block id: 656 */
            uint32_t l_1501[2];
            int i;
            for (i = 0; i < 2; i++)
                l_1501[i] = 0x78DE901CL;
            l_1501[1] = (safe_sub_func_uint16_t_u_u(((*g_634)--), g_187[g_1087]));
        }
        return l_1502;
    }
lbl_1628:
    for (g_1083 = 0; (g_1083 > (-16)); --g_1083)
    { /* block id: 664 */
        int8_t l_1505 = (-1L);
        int32_t l_1506 = 0xC0AEE040L;
        if (l_1505)
            break;
        l_1506 &= (g_1155 != (*g_104));
        (*g_1267) |= l_1476[0];
    }
lbl_1744:
    for (g_93 = 0; (g_93 < (-16)); g_93 = safe_sub_func_uint8_t_u_u(g_93, 3))
    { /* block id: 671 */
        uint32_t **l_1528 = &g_1349;
        int32_t l_1529 = 0x059D437CL;
        float *l_1530 = &g_91;
        int32_t l_1531[6][9] = {{(-1L),(-7L),(-1L),0xE30F91AEL,0xE30F91AEL,(-1L),(-7L),(-1L),0xE30F91AEL},{(-1L),(-1L),(-1L),(-1L),7L,(-1L),(-1L),(-1L),(-1L)},{0x0D935AF4L,0xE30F91AEL,0xE059A078L,0xE30F91AEL,0x0D935AF4L,0x0D935AF4L,0xE30F91AEL,0xE059A078L,0xE30F91AEL},{(-1L),7L,0x7487B8BAL,0x7487B8BAL,7L,(-1L),7L,0x7487B8BAL,0x7487B8BAL},{0x0D935AF4L,0x0D935AF4L,0xE30F91AEL,(-7L),(-1L),0xE30F91AEL,0xE30F91AEL,(-1L),(-7L)},{0x7487B8BAL,(-10L),0x7487B8BAL,(-1L),(-1L),0x7487B8BAL,(-10L),0x7487B8BAL,(-1L)}};
        int32_t l_1532[3];
        float *l_1533 = &g_434.f1;
        float *l_1534 = &g_369;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1532[i] = 1L;
        for (g_721.f1 = 0; (g_721.f1 <= 4); g_721.f1 = safe_add_func_int32_t_s_s(g_721.f1, 7))
        { /* block id: 674 */
            l_1476[0] = (*g_1267);
        }
        (*g_1535) = ((*l_1534) = (p_57.f0 > (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f(((*l_1533) = (safe_sub_func_float_f_f(((((**g_1041) = (*g_1042)) == (l_1519 , &l_1519)) , (((((((+(l_1531[4][7] = (safe_mul_func_float_f_f((p_57.f0 > 0x6.63B65Dp+49), (safe_div_func_float_f_f(((((*l_1530) = (l_1529 = ((safe_add_func_float_f_f((((+((((*l_1528) = func_63(l_1475, p_57.f0, p_57)) != &g_1097) ^ l_1529)) == l_1476[0]) , 0x8.DB5A96p-80), l_1529)) == 0x0.DF16C0p+10))) == l_1519.f0) > p_57.f0), 0x0.2p+1)))))) < l_1532[2]) > 0xE.97B4D8p-30) > (-0x6.6p+1)) <= (-0x1.4p-1)) < 0x0.7F51CAp+76) > (*g_1032))), (-0x8.Dp+1)))), 0x1.19AA7Bp-23)), l_1532[2])), g_1086[4][1][7]))));
    }
    for (g_1194 = 3; (g_1194 >= 0); g_1194 -= 1)
    { /* block id: 688 */
        uint32_t l_1537 = 0x8D13EEF6L;
        uint8_t *l_1542[2][8];
        int32_t l_1552 = 0x008DD051L;
        int32_t l_1553 = 0x44D984F4L;
        int32_t l_1554 = (-1L);
        int32_t l_1555 = 0x7525B866L;
        int32_t l_1557 = 0xA4A66D5AL;
        int32_t l_1558[2];
        int32_t l_1573 = 0x3F35AE3DL;
        uint32_t **l_1588 = &g_1349;
        uint32_t ***l_1587 = &l_1588;
        union U3 l_1597 = {0UL};
        float l_1604[6][6] = {{0x6.A9BDA0p-82,0xC.9DDF74p+13,0x6.274B40p-68,0xC.9DDF74p+13,0x6.A9BDA0p-82,0x2.638154p+8},{0xA.E685E8p+40,0x2.638154p+8,0x1.B345D6p-15,0x6.A9BDA0p-82,0x9.3F53E3p+13,0x5.5C3AF3p-33},{0x4.Dp-1,0x0.4FCE60p+20,0x9.3F53E3p+13,0x2.638154p+8,0x5.5C3AF3p-33,0x5.5C3AF3p-33},{0x2.E5851Dp+99,0x1.B345D6p-15,0x1.B345D6p-15,0x2.E5851Dp+99,0xF.9FD9B8p-29,0x2.638154p+8},{0x5.5C3AF3p-33,0xB.CE25FCp+1,0x6.274B40p-68,0x0.7p+1,0xC.9DDF74p+13,0x9.3F53E3p+13},{0x6.274B40p-68,0x4.Dp-1,0x8.732F88p-40,0x0.4FCE60p+20,0xC.9DDF74p+13,0x0.4FCE60p+20}};
        uint16_t **l_1667 = &g_634;
        uint16_t ***l_1666 = &l_1667;
        float *l_1687 = &g_369;
        int8_t l_1690[7];
        const union U1 l_1754 = {0UL};
        union U0 l_1775 = {1L};
        int i, j;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 8; j++)
                l_1542[i][j] = &g_141[0];
        }
        for (i = 0; i < 2; i++)
            l_1558[i] = 0L;
        for (i = 0; i < 7; i++)
            l_1690[i] = 4L;
        l_1537 = (*p_58);
        if (((safe_mul_func_uint8_t_u_u(g_32, (l_1476[0] = p_57.f0))) < (((void*)0 == l_1543) , (0x25B8L >= ((*g_907) = l_1537)))))
        { /* block id: 692 */
            if (((*g_1267) ^= (0UL >= l_1537)))
            { /* block id: 694 */
                return (***g_446);
            }
            else
            { /* block id: 696 */
                return (***g_446);
            }
        }
        else
        { /* block id: 699 */
            int32_t *l_1545 = &l_1476[4];
            int32_t *l_1546 = &l_1476[0];
            int32_t *l_1547 = (void*)0;
            int32_t *l_1548 = &l_1476[6];
            int32_t *l_1549 = &g_12[3][6][3];
            int32_t *l_1550 = (void*)0;
            int32_t *l_1551[8];
            int32_t l_1556 = (-1L);
            uint64_t *l_1564 = &g_434.f2;
            union U0 *l_1643[6][2] = {{&g_434,&g_434},{&g_434,&g_434},{&g_434,&g_434},{&g_434,&g_434},{&g_434,&g_434},{&g_434,&g_434}};
            int32_t l_1668 = 0xB5769F9FL;
            int i, j;
            for (i = 0; i < 8; i++)
                l_1551[i] = &g_36;
            l_1561--;
            if ((((*l_1564) = 18446744073709551615UL) && ((safe_sub_func_uint16_t_u_u((*g_145), ((safe_add_func_uint64_t_u_u((safe_div_func_int16_t_s_s((((safe_mul_func_uint8_t_u_u(g_810, l_1537)) != ((*l_1548) ^= l_1573)) , (((safe_add_func_int8_t_s_s((0x84A80C373CC7FB81LL || (safe_mod_func_uint16_t_u_u(((((safe_unary_minus_func_uint32_t_u(((safe_sub_func_int32_t_s_s(((safe_lshift_func_int8_t_s_s(((*g_104) ^= (safe_add_func_int64_t_s_s(((((((p_57.f0 , (safe_mul_func_uint8_t_u_u(g_1352, (18446744073709551610UL && 1L)))) == 0x86L) , (void*)0) == (void*)0) <= l_1561) > g_141[0]), l_1537))), p_57.f0)) != l_1560), (*g_1267))) || (*g_1267)))) == p_57.f0) <= p_57.f0) & p_57.f0), (*g_634)))), g_866[4][4])) || p_57.f0) <= (-1L))), l_1544)), 0xBC15DE82E26D6097LL)) && 0x8832CE99802A39BFLL))) | g_1087)))
            { /* block id: 704 */
                int8_t l_1600 = 3L;
                union U1 l_1610 = {6UL};
                float l_1615 = (-0x1.Bp+1);
                uint64_t l_1617[2][3] = {{3UL,18446744073709551608UL,3UL},{3UL,18446744073709551608UL,3UL}};
                uint32_t l_1621 = 8UL;
                int i, j;
                for (l_1560 = 0; (l_1560 <= 3); l_1560 += 1)
                { /* block id: 707 */
                    int i, j;
                    for (g_810 = 2; (g_810 <= 8); g_810 += 1)
                    { /* block id: 710 */
                        int i, j;
                        if (g_854[l_1560][g_1194])
                            break;
                        return (***g_446);
                    }
                    if (g_854[l_1560][(g_1194 + 2)])
                        break;
                    for (l_1557 = 0; (l_1557 <= 1); l_1557 += 1)
                    { /* block id: 717 */
                        uint32_t ****l_1589 = &l_1587;
                        int8_t *l_1598 = &g_854[1][4];
                        uint32_t *l_1599[2][4];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 4; j++)
                                l_1599[i][j] = &l_1475;
                        }
                        (*l_1589) = l_1587;
                        (**g_878) = &g_12[(g_1194 + 2)][(g_1194 + 2)][g_1194];
                        (*l_1545) = ((0x1947EC23L >= (l_1600 |= (((((*l_1598) = (((((**g_242) , ((*g_1093) = (l_1590 , (((((g_854[(l_1557 + 2)][l_1557] <= (safe_lshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(p_57.f0, (safe_lshift_func_uint8_t_u_s(0x33L, ((*g_104) = ((((*g_1096) ^ (p_57.f0 > p_57.f0)) > (*g_1267)) < p_57.f0)))))), p_57.f0))) > g_854[l_1560][(g_1194 + 2)]) <= g_1194) , p_57.f0) , l_1597)))) , (*p_58)) , g_854[l_1560][(g_1194 + 2)]) && (*g_104))) & g_418) && p_57.f0) >= 1L))) ^ 18446744073709551615UL);
                    }
                }
                if ((l_1600 && l_1519.f0))
                { /* block id: 727 */
                    const float *l_1602[3][2][7] = {{{&g_1536[1][2],&g_91,&g_1536[1][2],&g_91,&g_1536[1][2],&g_1536[1][2],&g_91},{&g_1536[0][0],&g_369,&g_1536[0][0],&g_91,&g_91,&g_1536[0][0],&g_369}},{{&g_91,&g_369,&g_1536[1][2],&g_1536[1][2],&g_369,&g_91,&g_369},{&g_1536[0][0],&g_91,&g_91,&g_1536[0][0],&g_369,&g_1536[0][0],&g_91}},{{&g_1536[1][2],&g_1536[1][2],&g_91,&g_1536[1][2],&g_91,&g_1536[1][2],&g_1536[1][2]},{&g_1536[1][2],&g_91,&g_1536[1][2],&g_91,&g_1536[1][2],&g_1536[1][2],&g_91}}};
                    const float **l_1601 = &l_1602[0][0][0];
                    int32_t l_1603[9][3][2] = {{{0xD70A13D9L,0x4EAB012AL},{0x46CE27A4L,9L},{(-7L),(-1L)}},{{0x5BB53B6EL,0x84D934E6L},{0xFF68FE36L,0xFF68FE36L},{0L,0xD70A13D9L}},{{1L,0x56494030L},{1L,(-4L)},{0x30C9DBD8L,1L}},{{9L,0x6D232AD7L},{9L,1L},{0x30C9DBD8L,(-4L)}},{{1L,0x56494030L},{1L,0xD70A13D9L},{0L,0xFF68FE36L}},{{0xFF68FE36L,0x84D934E6L},{0x5BB53B6EL,(-1L)},{(-7L),9L}},{{0x46CE27A4L,0x4EAB012AL},{0xD70A13D9L,3L},{0x84D934E6L,0x98804B05L}},{{0x4EAB012AL,0x98804B05L},{0x84D934E6L,3L},{0xD70A13D9L,0x4EAB012AL}},{{0x46CE27A4L,9L},{(-7L),(-1L)},{0x5BB53B6EL,0x84D934E6L}}};
                    int i, j, k;
                    (**g_878) = func_63(((void*)0 != l_1601), ((((*l_1564) = l_1603[2][2][1]) < (((&g_1194 != l_1542[1][7]) < ((*g_145) >= 0x4187L)) & p_57.f0)) , ((safe_unary_minus_func_uint64_t_u(((safe_div_func_int64_t_s_s((safe_add_func_int16_t_s_s(l_1590.f0, 0x604AL)), l_1603[5][0][0])) | 0x66L))) , 65535UL)), l_1610);
                }
                else
                { /* block id: 730 */
                    int64_t l_1614 = 0x795A4A135D0CBF2CLL;
                    int32_t l_1616[7][8] = {{3L,3L,3L,3L,3L,3L,3L,3L},{3L,3L,3L,3L,3L,3L,3L,3L},{3L,3L,3L,3L,3L,3L,3L,3L},{3L,3L,3L,3L,3L,3L,3L,3L},{3L,3L,3L,3L,3L,3L,3L,3L},{3L,3L,3L,3L,3L,3L,3L,3L},{3L,3L,3L,3L,3L,3L,3L,3L}};
                    int i, j;
                    if (((*l_1546) = (*p_58)))
                    { /* block id: 732 */
                        uint16_t l_1611[1][4][9] = {{{0x723EL,65535UL,0x723EL,0x5F24L,0x5F24L,0x723EL,65535UL,0x723EL,0x5F24L},{0x4F27L,0x7B5BL,0x7B5BL,0x4F27L,0UL,0x4F27L,0x7B5BL,0x7B5BL,0x4F27L},{65535UL,0x5F24L,65526UL,0x5F24L,65535UL,65535UL,0x5F24L,65526UL,0x5F24L},{0x7B5BL,0UL,0UL,0UL,0UL,0x7B5BL,0UL,0UL,0UL}}};
                        int i, j, k;
                        --l_1611[0][1][7];
                        l_1560 = ((*l_1545) &= ((void*)0 == l_1549));
                        if (l_1600)
                            break;
                    }
                    else
                    { /* block id: 737 */
                        l_1557 |= ((*l_1545) = 0x4A1E7003L);
                        if (l_1614)
                            break;
                        if ((*p_58))
                            continue;
                        (*g_879) = (void*)0;
                    }
                    l_1617[1][2]--;
                }
                for (g_1083 = 3; (g_1083 >= 0); g_1083 -= 1)
                { /* block id: 748 */
                    int32_t *l_1620[5][7][3] = {{{&l_1555,&g_36,&g_36},{&g_12[5][5][0],&l_1552,&g_8},{(void*)0,&g_12[0][8][0],&g_8},{&l_1556,&g_36,&g_36},{&g_8,&l_1557,&l_1556},{&l_1552,&l_1560,&g_8},{&g_8,&g_12[0][8][0],&g_12[0][8][0]}},{{&l_1476[8],&g_12[1][8][3],&l_1556},{&l_1552,&g_12[0][8][0],&g_12[0][8][0]},{&l_1552,&l_1560,&l_1552},{&l_1558[0],&l_1557,&g_12[5][5][0]},{&g_36,&g_36,&l_1556},{&g_36,&g_12[0][8][0],&g_12[1][8][3]},{&g_36,&l_1552,&l_1558[0]}},{{&g_36,&g_36,&l_1476[8]},{&l_1558[0],(void*)0,&l_1552},{&l_1552,(void*)0,&g_12[4][5][0]},{&l_1552,&g_12[4][5][0],(void*)0},{&l_1476[8],&l_1552,&g_12[4][5][0]},{&g_8,&l_1556,&l_1552},{&l_1552,&g_8,&l_1476[8]}},{{&g_8,&l_1552,&l_1558[0]},{&l_1556,&l_1552,&g_12[1][8][3]},{(void*)0,&l_1552,&l_1556},{&g_12[5][5][0],&l_1552,&g_12[5][5][0]},{&l_1555,&g_8,&l_1552},{&l_1553,&l_1556,&g_12[0][8][0]},{&l_1557,&l_1552,&l_1556}},{{&l_1552,&g_12[4][5][0],&g_12[0][8][0]},{&l_1557,(void*)0,&g_8},{&l_1553,(void*)0,&l_1556},{&l_1555,&g_36,&g_36},{&g_12[5][5][0],&l_1552,&g_8},{(void*)0,&g_12[0][8][0],&g_8},{&l_1556,&g_36,&g_36}}};
                    int i, j, k;
                    (*l_1549) |= ((*l_1546) &= ((void*)0 != (*g_1029)));
                    for (l_1590.f1 = 0; (l_1590.f1 <= 3); l_1590.f1 += 1)
                    { /* block id: 753 */
                        int i, j, k;
                        l_1620[0][6][0] = &g_12[g_1194][(l_1590.f1 + 5)][g_1194];
                        ++l_1621;
                    }
                }
            }
            else
            { /* block id: 758 */
                uint32_t l_1624 = 0x4D706CACL;
                union U0 *l_1644 = &g_434;
                l_1624++;
                if (g_1627)
                    break;
                if (l_1560)
                    goto lbl_1628;
                (*g_1267) |= (safe_add_func_uint64_t_u_u(2UL, (((safe_add_func_int32_t_s_s(((safe_lshift_func_int16_t_s_s((safe_add_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((0UL && (((safe_mul_func_uint16_t_u_u(((l_1643[5][0] == l_1644) < (safe_add_func_int64_t_s_s((safe_mul_func_uint8_t_u_u(p_57.f0, (safe_add_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_s(((void*)0 == g_1653), (safe_mul_func_int16_t_s_s(((!((*g_907) = (((safe_mul_func_int16_t_s_s((((safe_rshift_func_int16_t_s_s((((249UL ^ (((*g_104) = ((safe_mod_func_int16_t_s_s(((((safe_lshift_func_int8_t_s_s((3L < 0x2AL), 3)) <= 0xF769L) > l_1624) == 0x4ED7L), 65535UL)) || l_1624)) > 0x77L)) , l_1666) != &l_1667), 9)) & 8L) , 0x5446L), l_1624)) != l_1476[0]) , 0xF823L))) ^ 1UL), (*g_865))))) | p_57.f0), 0L)))), g_29))), l_1624)) , 18446744073709551606UL) > g_1352)), (*g_865))), 2)), 0x7155B1535F6150BALL)), 7)) && l_1668), 0x1AA543F8L)) != 0x980AL) < l_1624)));
            }
        }
        if (l_1544)
            break;
        for (g_479 = 0; (g_479 <= 3); g_479 += 1)
        { /* block id: 770 */
            uint64_t *l_1684[10] = {&g_434.f2,&g_434.f2,&g_434.f2,&g_434.f2,&g_434.f2,&g_434.f2,&g_434.f2,&g_434.f2,&g_434.f2,&g_434.f2};
            int32_t l_1685 = 0x74EE5B6CL;
            int32_t *l_1686[4] = {&g_1086[5][0][4],&g_1086[5][0][4],&g_1086[5][0][4],&g_1086[5][0][4]};
            union U0 l_1745 = {-1L};
            int i, j;
            if ((((*g_1267) = (safe_lshift_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u((0xE1A6A594L & (0UL && (g_1673 != ((safe_mul_func_uint8_t_u_u(g_854[g_1194][(g_479 + 2)], (p_57.f0 & g_854[g_1194][(g_479 + 2)]))) , &g_1674)))), ((l_1558[1] = l_1561) >= (safe_lshift_func_int8_t_s_s((((safe_lshift_func_int16_t_s_u((!(safe_mod_func_uint16_t_u_u((((((l_1554 |= (((l_1685 = l_1559[0]) >= g_12[0][8][0]) != 0xD6L)) , l_1687) == (void*)0) || l_1685) >= 0x5E6FL), (*g_634)))), 6)) < 4294967295UL) && p_57.f0), 1))))), 9))) <= 0L))
            { /* block id: 775 */
                return l_1519;
            }
            else
            { /* block id: 777 */
                uint32_t *l_1693 = &l_1537;
                const int32_t l_1704 = 0L;
                int32_t l_1731 = (-1L);
                union U3 l_1737 = {4294967287UL};
                union U1 l_1738 = {0x096AL};
                int16_t *l_1750 = &g_1012;
                int16_t **l_1751 = &l_1750;
                const uint32_t *l_1769 = &g_1770;
                const uint32_t **l_1768 = &l_1769;
                const uint32_t ***l_1767 = &l_1768;
                if ((safe_add_func_int32_t_s_s(l_1690[4], ((*g_1267) = ((safe_div_func_uint32_t_u_u((((*l_1693) = (**g_1674)) != (+((*g_907) &= 4L))), (+(g_721.f0 <= (safe_mul_func_int8_t_s_s((g_141[0] , (*g_104)), (*g_104))))))) < (safe_lshift_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((safe_div_func_int16_t_s_s(l_1704, p_57.f0)), 4)), (*g_634))))))))
                { /* block id: 781 */
                    float l_1730 = 0xA.D1E85Bp-79;
                    int32_t l_1732 = (-4L);
                    int8_t l_1733 = (-6L);
                    union U0 *l_1734 = &g_434;
                    for (g_1082 = 3; (g_1082 >= 0); g_1082 -= 1)
                    { /* block id: 784 */
                        int i, j;
                        l_1733 = (((*g_931) > ((((((l_1731 = (safe_rshift_func_uint8_t_u_u((g_187[6] , (safe_sub_func_uint32_t_u_u((((p_57.f0 , (((safe_mul_func_uint16_t_u_u(p_57.f0, (safe_mod_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((((safe_rshift_func_int8_t_s_u((((((safe_lshift_func_uint8_t_u_u(7UL, (safe_mod_func_int64_t_s_s(((*g_104) > p_57.f0), (safe_rshift_func_int8_t_s_s((g_854[g_479][g_1082] = (*g_104)), 0)))))) , (safe_sub_func_int64_t_s_s(((safe_mod_func_uint32_t_u_u((safe_rshift_func_int8_t_s_s(0xF8L, 0)), 1UL)) <= 0L), 0xC0EE21BEE05AB2CELL))) & p_57.f0) && g_854[g_479][g_1082]) && 0UL), 2)) && 2L) | (*g_104)), 7UL)), p_57.f0)))) != p_57.f0) < p_57.f0)) , p_57.f0) <= 4UL), g_1729))), g_83[0][0][0]))) <= l_1561) ^ (*g_104)) >= l_1685) != p_57.f0) , l_1732)) <= p_57.f0);
                        g_246 = &g_247;
                        return (*g_448);
                    }
                    for (g_90 = 3; (g_90 >= 0); g_90 -= 1)
                    { /* block id: 793 */
                        return (***g_1041);
                    }
                    (*g_447) = l_1734;
                }
                else
                { /* block id: 797 */
                    float ***l_1739 = (void*)0;
                    for (g_1087 = 0; (g_1087 <= 3); g_1087 += 1)
                    { /* block id: 800 */
                        (**g_878) = func_63((l_1685 = (safe_sub_func_uint64_t_u_u(9UL, (l_1737 , (-7L))))), ((void*)0 == &g_1674), l_1738);
                        (*g_1740) = l_1739;
                        if (l_1573)
                            goto lbl_1744;
                        return l_1745;
                    }
                    for (l_1537 = 0; (l_1537 <= 3); l_1537 += 1)
                    { /* block id: 809 */
                        (*g_1267) = (safe_lshift_func_uint16_t_u_u(((l_1560 = p_57.f0) != g_854[g_1194][(g_479 + 2)]), 4));
                        return l_1519;
                    }
                }
                (*l_1687) = (safe_add_func_float_f_f((((*l_1751) = l_1750) == (void*)0), ((**g_1028) == &g_866[2][6])));
                l_1731 ^= (safe_mul_func_uint16_t_u_u((((l_1754 , (safe_mul_func_int8_t_s_s(0xF6L, (safe_mul_func_uint16_t_u_u(((***l_1666) = (safe_mul_func_int8_t_s_s(6L, p_57.f0))), (safe_lshift_func_int16_t_s_u(((((((~(safe_unary_minus_func_int32_t_s(((g_1774[4] = ((safe_add_func_int32_t_s_s(((*g_1267) = (((((*l_1767) = (void*)0) != (void*)0) , ((l_1685 = g_90) | ((((g_1083 && ((l_1773[2] != &g_854[g_1194][(g_479 + 2)]) > 0x00L)) & 0xCFA63A12031DEB7CLL) >= p_57.f0) ^ g_223[0].f0))) == g_141[0])), l_1745.f0)) < p_57.f0)) <= p_57.f0)))) != 8L) >= l_1559[0]) , (-2L)) <= g_210) > l_1552), l_1745.f0))))))) <= (-1L)) | 4L), p_57.f0));
                return l_1775;
            }
        }
    }
    return l_1519;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_61(int32_t * p_62)
{ /* block id: 23 */
    union U1 l_67 = {0UL};
    int32_t *l_69 = &g_12[0][8][0];
    int32_t **l_68[8][3][7] = {{{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_69,&l_69,&l_69,&l_69,&l_69,&l_69,&l_69},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
    uint32_t l_72[1];
    int64_t l_183 = (-6L);
    union U3 l_190 = {0x0CE8C50EL};
    int16_t *l_209 = &g_210;
    const int8_t *l_230 = (void*)0;
    uint32_t l_237 = 0x623C689BL;
    int16_t l_293 = (-4L);
    union U1 l_420 = {65535UL};
    uint32_t l_424 = 0x74C6A9D3L;
    int64_t l_430 = 0x8D6F02563EA5185ALL;
    union U0 *l_433 = &g_434;
    union U0 *l_435 = &g_434;
    uint64_t *l_520 = &g_142;
    union U1 **l_540 = &g_243[0];
    uint16_t *l_565 = &l_420.f0;
    uint16_t **l_564 = &l_565;
    union U2 *l_639 = &g_640[0];
    int32_t l_657 = 0x2000F924L;
    union U0 **l_769[10][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
    int8_t l_776 = 1L;
    float l_787 = 0xC.E10C0Cp+90;
    uint16_t *l_788 = (void*)0;
    float l_795 = 0x8.6B60F0p+29;
    int32_t l_802 = 5L;
    uint8_t *l_807 = &g_83[3][2][7];
    uint8_t *l_808 = (void*)0;
    uint8_t *l_809 = &g_141[0];
    const uint32_t *l_886 = &l_72[0];
    union U3 *l_892[5];
    union U3 **l_891[9] = {&l_892[2],&l_892[4],&l_892[2],&l_892[2],&l_892[4],&l_892[2],&l_892[2],&l_892[4],&l_892[0]};
    int16_t *l_908 = &l_293;
    uint64_t l_967 = 0UL;
    int64_t *l_1038 = &g_187[4];
    int32_t l_1100 = (-1L);
    int64_t l_1222[9][7] = {{0L,0xF9C0018812281F33LL,1L,(-10L),0x4899CC8EA4BD1047LL,1L,(-4L)},{0x14195D7EDCAEA251LL,0L,0x510B6D6FD20B4261LL,5L,0L,1L,0L},{(-1L),0x510B6D6FD20B4261LL,0x510B6D6FD20B4261LL,(-1L),0L,8L,1L},{(-10L),(-4L),1L,0x510B6D6FD20B4261LL,0xDD5F520416A0E53ALL,5L,0x92ACCBEFBE213612LL},{0xF9C0018812281F33LL,2L,(-1L),0xDF9A5AE150DD60C9LL,0L,0x4899CC8EA4BD1047LL,1L},{1L,8L,(-1L),0x4899CC8EA4BD1047LL,0x241BA87621421E5DLL,0L,0L},{0xA876A695CFB4412ALL,0x4899CC8EA4BD1047LL,0xC6DBD45141E5E6B1LL,0x4899CC8EA4BD1047LL,0xA876A695CFB4412ALL,0L,(-4L)},{0x510B6D6FD20B4261LL,(-1L),0x92ACCBEFBE213612LL,0xDF9A5AE150DD60C9LL,0xB77D4EAADEA14E9ALL,0xDD5F520416A0E53ALL,0L},{1L,0xA876A695CFB4412ALL,(-4L),0x510B6D6FD20B4261LL,2L,0L,0xB77D4EAADEA14E9ALL}};
    int32_t l_1223 = (-7L);
    int32_t l_1360 = (-5L);
    int32_t *l_1468 = &g_481;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_72[i] = 1UL;
    for (i = 0; i < 5; i++)
        l_892[i] = &g_721;
    return l_1468;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_63(int64_t  p_64, uint16_t  p_65, union U1  p_66)
{ /* block id: 24 */
    return &g_12[0][8][0];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_8, "g_8", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_12[i][j][k], "g_12[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_28, "g_28", print_hash_value);
    transparent_crc(g_29, "g_29", print_hash_value);
    transparent_crc(g_32, "g_32", print_hash_value);
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc(g_34, "g_34", print_hash_value);
    transparent_crc(g_35, "g_35", print_hash_value);
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc(g_60.f0, "g_60.f0", print_hash_value);
    transparent_crc(g_81.f0, "g_81.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_83[i][j][k], "g_83[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_90, "g_90", print_hash_value);
    transparent_crc_bytes (&g_91, sizeof(g_91), "g_91", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_99, "g_99", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_141[i], "g_141[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_142, "g_142", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_187[i], "g_187[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_210, "g_210", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_223[i].f0, "g_223[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_236[i][j], "g_236[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_336.f0, "g_336.f0", print_hash_value);
    transparent_crc_bytes (&g_369, sizeof(g_369), "g_369", print_hash_value);
    transparent_crc(g_418, "g_418", print_hash_value);
    transparent_crc(g_434.f0, "g_434.f0", print_hash_value);
    transparent_crc(g_479, "g_479", print_hash_value);
    transparent_crc(g_481, "g_481", print_hash_value);
    transparent_crc(g_519, "g_519", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_527[i][j], "g_527[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_640[i].f0, "g_640[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_643[i][j][k].f0, "g_643[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_693, "g_693", print_hash_value);
    transparent_crc(g_721.f0, "g_721.f0", print_hash_value);
    transparent_crc(g_810, "g_810", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_854[i][j], "g_854[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_866[i][j], "g_866[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_982.f0, "g_982.f0", print_hash_value);
    transparent_crc(g_1008.f0, "g_1008.f0", print_hash_value);
    transparent_crc(g_1012, "g_1012", print_hash_value);
    transparent_crc(g_1030, "g_1030", print_hash_value);
    transparent_crc(g_1082, "g_1082", print_hash_value);
    transparent_crc(g_1083, "g_1083", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_1086[i][j][k], "g_1086[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1087, "g_1087", print_hash_value);
    transparent_crc(g_1097, "g_1097", print_hash_value);
    transparent_crc(g_1155, "g_1155", print_hash_value);
    transparent_crc(g_1183.f0, "g_1183.f0", print_hash_value);
    transparent_crc(g_1191.f0, "g_1191.f0", print_hash_value);
    transparent_crc(g_1194, "g_1194", print_hash_value);
    transparent_crc(g_1231.f0, "g_1231.f0", print_hash_value);
    transparent_crc(g_1256.f0, "g_1256.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_1331[i][j][k].f0, "g_1331[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1352, "g_1352", print_hash_value);
    transparent_crc(g_1371, "g_1371", print_hash_value);
    transparent_crc(g_1422.f0, "g_1422.f0", print_hash_value);
    transparent_crc(g_1427.f0, "g_1427.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc_bytes(&g_1536[i][j], sizeof(g_1536[i][j]), "g_1536[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1627, "g_1627", print_hash_value);
    transparent_crc(g_1729, "g_1729", print_hash_value);
    transparent_crc(g_1770, "g_1770", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1774[i], "g_1774[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2010.f0, "g_2010.f0", print_hash_value);
    transparent_crc(g_2024.f0, "g_2024.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_2055[i][j][k], "g_2055[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2291, "g_2291", print_hash_value);
    transparent_crc(g_2292, "g_2292", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2373[i].f0, "g_2373[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_2414[i].f0, "g_2414[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2460, "g_2460", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_2625[i][j][k].f0, "g_2625[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2697.f0, "g_2697.f0", print_hash_value);
    transparent_crc(g_2740.f0, "g_2740.f0", print_hash_value);
    transparent_crc(g_2993, "g_2993", print_hash_value);
    transparent_crc(g_3016.f0, "g_3016.f0", print_hash_value);
    transparent_crc(g_3199.f0, "g_3199.f0", print_hash_value);
    transparent_crc_bytes (&g_3216, sizeof(g_3216), "g_3216", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_3270[i][j], "g_3270[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_3288[i][j], "g_3288[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3329.f0, "g_3329.f0", print_hash_value);
    transparent_crc(g_3333.f0, "g_3333.f0", print_hash_value);
    transparent_crc(g_3363.f0, "g_3363.f0", print_hash_value);
    transparent_crc(g_3415, "g_3415", print_hash_value);
    transparent_crc(g_3421, "g_3421", print_hash_value);
    transparent_crc(g_3436, "g_3436", print_hash_value);
    transparent_crc(g_3547.f0, "g_3547.f0", print_hash_value);
    transparent_crc(g_3587.f0, "g_3587.f0", print_hash_value);
    transparent_crc(g_3647.f0, "g_3647.f0", print_hash_value);
    transparent_crc(g_3687.f0, "g_3687.f0", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_3691[i], "g_3691[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_3814[i], "g_3814[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_3946[i].f0, "g_3946[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_4012.f0, "g_4012.f0", print_hash_value);
    transparent_crc(g_4035, "g_4035", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_4038[i][j][k].f0, "g_4038[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_4072, "g_4072", print_hash_value);
    transparent_crc(g_4082.f0, "g_4082.f0", print_hash_value);
    transparent_crc(g_4118, "g_4118", print_hash_value);
    transparent_crc(g_4129.f0, "g_4129.f0", print_hash_value);
    transparent_crc(g_4209.f0, "g_4209.f0", print_hash_value);
    transparent_crc_bytes (&g_4303, sizeof(g_4303), "g_4303", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_4394[i][j], "g_4394[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1219
XXX total union variables: 133

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 48
breakdown:
   indirect level: 0, occurrence: 35
   indirect level: 1, occurrence: 5
   indirect level: 2, occurrence: 8
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 19
XXX times a bitfields struct on LHS: 2
XXX times a bitfields struct on RHS: 46
XXX times a single bitfield on LHS: 1
XXX times a single bitfield on RHS: 45

XXX max expression depth: 49
breakdown:
   depth: 1, occurrence: 298
   depth: 2, occurrence: 74
   depth: 3, occurrence: 9
   depth: 4, occurrence: 4
   depth: 5, occurrence: 3
   depth: 6, occurrence: 2
   depth: 8, occurrence: 2
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 3
   depth: 14, occurrence: 4
   depth: 15, occurrence: 2
   depth: 16, occurrence: 4
   depth: 17, occurrence: 2
   depth: 18, occurrence: 4
   depth: 19, occurrence: 4
   depth: 20, occurrence: 7
   depth: 21, occurrence: 2
   depth: 22, occurrence: 4
   depth: 23, occurrence: 4
   depth: 24, occurrence: 6
   depth: 25, occurrence: 6
   depth: 26, occurrence: 1
   depth: 27, occurrence: 3
   depth: 30, occurrence: 1
   depth: 31, occurrence: 3
   depth: 32, occurrence: 2
   depth: 34, occurrence: 3
   depth: 35, occurrence: 1
   depth: 36, occurrence: 2
   depth: 37, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 43, occurrence: 1
   depth: 46, occurrence: 1
   depth: 49, occurrence: 1

XXX total number of pointers: 931

XXX times a variable address is taken: 2526
XXX times a pointer is dereferenced on RHS: 807
breakdown:
   depth: 1, occurrence: 684
   depth: 2, occurrence: 67
   depth: 3, occurrence: 48
   depth: 4, occurrence: 8
XXX times a pointer is dereferenced on LHS: 576
breakdown:
   depth: 1, occurrence: 513
   depth: 2, occurrence: 37
   depth: 3, occurrence: 19
   depth: 4, occurrence: 7
XXX times a pointer is compared with null: 85
XXX times a pointer is compared with address of another variable: 28
XXX times a pointer is compared with another pointer: 31
XXX times a pointer is qualified to be dereferenced: 14463

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 3211
   level: 2, occurrence: 470
   level: 3, occurrence: 461
   level: 4, occurrence: 197
   level: 5, occurrence: 17
XXX number of pointers point to pointers: 396
XXX number of pointers point to scalars: 478
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 30
XXX average alias set size: 1.66

XXX times a non-volatile is read: 3887
XXX times a non-volatile is write: 1766
XXX times a volatile is read: 314
XXX    times read thru a pointer: 102
XXX times a volatile is write: 59
XXX    times written thru a pointer: 10
XXX times a volatile is available for access: 2.94e+03
XXX percentage of non-volatile access: 93.8

XXX forward jumps: 0
XXX backward jumps: 17

XXX stmts: 314
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 25
   depth: 1, occurrence: 30
   depth: 2, occurrence: 37
   depth: 3, occurrence: 52
   depth: 4, occurrence: 69
   depth: 5, occurrence: 101

XXX percentage a fresh-made variable is used: 17.7
XXX percentage an existing variable is used: 82.3
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


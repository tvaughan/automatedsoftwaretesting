/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1603648540
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_6 = 0xC2DB2212L;
static uint32_t g_30[2] = {4294967290UL,4294967290UL};
static int64_t g_34[7][8][4] = {{{0L,0x7EA1B14898D08F49LL,(-1L),0x162D50C857617C0ELL},{(-10L),(-1L),0xF75DF1563CFEBDABLL,0x85C2F4FE10EF7D59LL},{0x85C2F4FE10EF7D59LL,0x16971DB95674643ALL,(-1L),(-1L)},{(-1L),(-1L),0x85C2F4FE10EF7D59LL,0xF75DF1563CFEBDABLL},{0xAB56411D7FEAE215LL,0xF4CAA5D732ADAA23LL,0x162D50C857617C0ELL,(-1L)},{0x3579942796A450C1LL,0x85C2F4FE10EF7D59LL,(-1L),0x162D50C857617C0ELL},{0x8131DDA87B8B0D58LL,0x85C2F4FE10EF7D59LL,(-9L),(-1L)},{0x85C2F4FE10EF7D59LL,0xF4CAA5D732ADAA23LL,1L,0xF75DF1563CFEBDABLL}},{{0L,(-1L),0x7D2AE857B1A09495LL,(-1L)},{0x162D50C857617C0ELL,0x16971DB95674643ALL,0x162D50C857617C0ELL,0x85C2F4FE10EF7D59LL},{(-4L),(-1L),0x7EA1B14898D08F49LL,0x162D50C857617C0ELL},{(-1L),0x3579942796A450C1LL,(-6L),0xAB56411D7FEAE215LL},{0L,0x8131DDA87B8B0D58LL,(-6L),0xF4CAA5D732ADAA23LL},{(-1L),0x85C2F4FE10EF7D59LL,0x3579942796A450C1LL,(-2L)},{1L,0L,0x0DCEE05243B9C309LL,0x162D50C857617C0ELL},{0x0DCEE05243B9C309LL,0x162D50C857617C0ELL,0x162D50C857617C0ELL,0x0DCEE05243B9C309LL}},{{(-1L),(-4L),(-2L),0x3579942796A450C1LL},{0L,0L,0xF4CAA5D732ADAA23LL,(-6L)},{0x7EA1B14898D08F49LL,0x85C2F4FE10EF7D59LL,0xAB56411D7FEAE215LL,(-6L)},{(-1L),0L,0x0DCEE05243B9C309LL,0x3579942796A450C1LL},{(-9L),(-4L),0L,0x0DCEE05243B9C309LL},{0x85C2F4FE10EF7D59LL,0x162D50C857617C0ELL,0x16971DB95674643ALL,0x162D50C857617C0ELL},{0L,0L,0xAC39B398EF544BD1LL,(-2L)},{0x7D2AE857B1A09495LL,0x85C2F4FE10EF7D59LL,(-4L),0xF4CAA5D732ADAA23LL}},{{0xF75DF1563CFEBDABLL,0x8131DDA87B8B0D58LL,0x0DCEE05243B9C309LL,0xAB56411D7FEAE215LL},{0xF75DF1563CFEBDABLL,0x3579942796A450C1LL,(-4L),0x0DCEE05243B9C309LL},{0x7D2AE857B1A09495LL,0xAB56411D7FEAE215LL,0xAC39B398EF544BD1LL,0L},{0L,(-1L),0x16971DB95674643ALL,0x16971DB95674643ALL},{0x85C2F4FE10EF7D59LL,0x85C2F4FE10EF7D59LL,0L,0xAC39B398EF544BD1LL},{(-9L),(-10L),0x0DCEE05243B9C309LL,(-4L)},{(-1L),0L,0xAB56411D7FEAE215LL,0x0DCEE05243B9C309LL},{0x7EA1B14898D08F49LL,0L,0xF4CAA5D732ADAA23LL,(-4L)}},{{0L,(-10L),(-2L),0xAC39B398EF544BD1LL},{(-1L),0x85C2F4FE10EF7D59LL,0x162D50C857617C0ELL,0x16971DB95674643ALL},{0x0DCEE05243B9C309LL,(-1L),0x0DCEE05243B9C309LL,0L},{1L,0xAB56411D7FEAE215LL,0x3579942796A450C1LL,0x0DCEE05243B9C309LL},{(-1L),0x3579942796A450C1LL,(-6L),0xAB56411D7FEAE215LL},{0L,0x8131DDA87B8B0D58LL,(-6L),0xF4CAA5D732ADAA23LL},{(-1L),0x85C2F4FE10EF7D59LL,0x3579942796A450C1LL,(-2L)},{1L,0L,0x0DCEE05243B9C309LL,0x162D50C857617C0ELL}},{{0x0DCEE05243B9C309LL,0x162D50C857617C0ELL,0x162D50C857617C0ELL,0x0DCEE05243B9C309LL},{(-1L),(-4L),(-2L),0x3579942796A450C1LL},{0L,0L,0xF4CAA5D732ADAA23LL,(-6L)},{0x7EA1B14898D08F49LL,0x85C2F4FE10EF7D59LL,0xAB56411D7FEAE215LL,(-6L)},{(-1L),0L,0x0DCEE05243B9C309LL,0x3579942796A450C1LL},{(-9L),(-4L),0L,0x0DCEE05243B9C309LL},{0x85C2F4FE10EF7D59LL,0x162D50C857617C0ELL,0x16971DB95674643ALL,0x162D50C857617C0ELL},{0L,0L,0xAC39B398EF544BD1LL,(-2L)}},{{0x7D2AE857B1A09495LL,0x85C2F4FE10EF7D59LL,(-4L),0xF4CAA5D732ADAA23LL},{0xF75DF1563CFEBDABLL,0x8131DDA87B8B0D58LL,0x0DCEE05243B9C309LL,0xAB56411D7FEAE215LL},{0xF75DF1563CFEBDABLL,0x3579942796A450C1LL,(-4L),0x0DCEE05243B9C309LL},{0x7D2AE857B1A09495LL,0xAB56411D7FEAE215LL,0xAC39B398EF544BD1LL,0L},{0L,(-1L),0x16971DB95674643ALL,0x16971DB95674643ALL},{0x85C2F4FE10EF7D59LL,0x85C2F4FE10EF7D59LL,0L,0xAC39B398EF544BD1LL},{(-9L),(-10L),0x0DCEE05243B9C309LL,(-4L)},{(-1L),0L,0xAB56411D7FEAE215LL,0x0DCEE05243B9C309LL}}};
static float g_68[6][8] = {{(-0x9.8p+1),(-0x9.8p+1),(-0x10.0p+1),0x7.3E8932p-19,0x4.0293E9p+72,0xD.024CA9p-11,0x2.6695BDp-81,0x1.Fp-1},{0x1.3p+1,0x2.6695BDp-81,0x1.7p-1,(-0x10.0p+1),0x1.Fp-1,0x7.3E8932p-19,0x0.Dp-1,0x9.DB650Fp-7},{0x7.3E8932p-19,0x1.3p+1,0x1.7p-1,(-0x1.2p+1),0x2.6695BDp-81,(-0x10.0p+1),(-0x10.0p+1),0x2.6695BDp-81},{(-0x9.8p+1),0x0.Dp-1,0x0.Dp-1,(-0x9.8p+1),0x2.6695BDp-81,0xE.7A53CCp+54,0x9.DB650Fp-7,(-0x10.0p+1)},{0x7.3E8932p-19,0x4.0293E9p+72,0xD.024CA9p-11,0x2.6695BDp-81,0x1.Fp-1,0x2.6695BDp-81,0xD.024CA9p-11,0x4.0293E9p+72},{0x0.Dp-1,0x4.0293E9p+72,(-0x10.0p+1),0xD.024CA9p-11,0x1.7p-1,0xE.7A53CCp+54,(-0x1.2p+1),(-0x1.2p+1)}};
static uint16_t g_84 = 0xF9F6L;
static int16_t g_121 = 0x4BD0L;
static const volatile int32_t g_124[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
static const volatile int32_t *g_123 = &g_124[1];
static const volatile int32_t ** volatile g_122 = &g_123;/* VOLATILE GLOBAL g_122 */
static float g_126 = 0x4.B0DFABp+12;
static uint64_t g_128 = 0xB3BFF73A4BFF4CCELL;
static int16_t g_129 = 0xC9A4L;
static uint16_t g_145 = 65535UL;
static int64_t g_150 = 1L;
static uint16_t *g_154 = &g_84;
static uint8_t g_191[5] = {255UL,255UL,255UL,255UL,255UL};
static const volatile int32_t ** const  volatile g_199 = &g_123;/* VOLATILE GLOBAL g_199 */
static volatile uint32_t g_217 = 0x70074BB8L;/* VOLATILE GLOBAL g_217 */
static volatile uint32_t *g_216[9] = {&g_217,&g_217,&g_217,&g_217,&g_217,&g_217,&g_217,&g_217,&g_217};
static volatile uint32_t * volatile * volatile g_215 = &g_216[6];/* VOLATILE GLOBAL g_215 */
static uint16_t g_223 = 0xF6D6L;
static int16_t g_226 = 0x3EE4L;
static volatile uint32_t g_227 = 1UL;/* VOLATILE GLOBAL g_227 */
static uint64_t g_255[9] = {0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL,0xD4BD7B55C8FD6D36LL};
static int32_t g_301 = 0xD7DE96F4L;
static int16_t g_303 = 0x6E62L;
static int8_t g_322 = 0x90L;
static int8_t g_324 = 0x4EL;
static uint64_t g_325 = 0xE32B3647BB7FCCB7LL;
static volatile int32_t g_327 = 0x43741902L;/* VOLATILE GLOBAL g_327 */
static volatile int32_t g_328[6] = {0xD35FB90BL,0xD35FB90BL,0xD35FB90BL,0xD35FB90BL,0xD35FB90BL,0xD35FB90BL};
static volatile float g_338 = 0xD.684781p-35;/* VOLATILE GLOBAL g_338 */
static volatile float *g_337 = &g_338;
static volatile float ** volatile g_336[3][7] = {{&g_337,&g_337,&g_337,&g_337,&g_337,&g_337,&g_337},{&g_337,(void*)0,&g_337,&g_337,(void*)0,&g_337,(void*)0},{&g_337,&g_337,&g_337,&g_337,&g_337,&g_337,&g_337}};
static volatile float ** volatile * volatile g_339 = &g_336[0][5];/* VOLATILE GLOBAL g_339 */
static int32_t g_354[4][3] = {{4L,4L,4L},{0x2F1ED42EL,0x2F1ED42EL,0x2F1ED42EL},{4L,4L,4L},{0x2F1ED42EL,0x2F1ED42EL,0x2F1ED42EL}};
static int32_t *g_353[5] = {&g_354[2][0],&g_354[2][0],&g_354[2][0],&g_354[2][0],&g_354[2][0]};
static float g_362 = 0x0.5BAAB0p+19;
static float *g_361 = &g_362;
static float **g_360 = &g_361;
static uint8_t g_446[2][9][8] = {{{7UL,255UL,0x05L,246UL,0xC8L,0x15L,1UL,0x89L},{0xCBL,0x25L,7UL,0x05L,246UL,255UL,2UL,0UL},{0x89L,1UL,0x25L,255UL,0x25L,1UL,0x89L,1UL},{0xC6L,0x89L,0xCBL,7UL,0x8CL,0xAEL,0x84L,255UL},{246UL,1UL,0x15L,0xABL,0x8CL,1UL,0xC1L,255UL},{0xC6L,0x97L,0x1CL,255UL,0x25L,2UL,246UL,246UL},{0x89L,246UL,0UL,0UL,246UL,0x89L,0x1CL,0x84L},{0xCBL,1UL,0x6AL,0x25L,0xC8L,0x84L,0xC6L,0xB2L},{7UL,0x29L,1UL,0x25L,0xE0L,0xC1L,0x8CL,0x84L}},{{0x8CL,0xE0L,0xAEL,0UL,0UL,0x6AL,0x84L,0x6AL},{0xC1L,246UL,1UL,246UL,0xC1L,0xABL,1UL,0x15L},{7UL,246UL,0x29L,0xC8L,0xB2L,1UL,0xE0L,246UL},{246UL,1UL,0x29L,0xE0L,255UL,2UL,1UL,0xCBL},{0xB2L,0x05L,1UL,0x15L,0x97L,0x84L,0x84L,0x97L},{0xAEL,0xC1L,0xC1L,0xAEL,0xCBL,1UL,2UL,255UL},{255UL,0xC6L,0xCBL,0x6AL,246UL,0xE0L,1UL,0xB2L},{0x84L,0xC6L,0xB2L,0x89L,0x15L,1UL,0xABL,0xC1L},{255UL,0xC1L,0x25L,0UL,0x6AL,0x84L,0x6AL,0UL}}};
static volatile int64_t ** volatile g_455 = (void*)0;/* VOLATILE GLOBAL g_455 */
static volatile int64_t g_458 = 0xE52CF93ED212E763LL;/* VOLATILE GLOBAL g_458 */
static volatile int64_t *g_457 = &g_458;
static volatile int64_t **g_456 = &g_457;
static float ***g_536[9][4][4] = {{{&g_360,&g_360,&g_360,&g_360},{&g_360,(void*)0,&g_360,&g_360},{(void*)0,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}},{{(void*)0,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{(void*)0,&g_360,&g_360,&g_360}},{{&g_360,&g_360,&g_360,&g_360},{(void*)0,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{(void*)0,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,&g_360,&g_360},{(void*)0,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}},{{&g_360,&g_360,(void*)0,&g_360},{(void*)0,&g_360,(void*)0,&g_360},{&g_360,&g_360,&g_360,&g_360},{&g_360,&g_360,&g_360,&g_360}}};
static float ****g_535 = &g_536[5][0][3];
static const volatile int32_t *g_557 = &g_328[5];
static volatile int32_t g_614 = 1L;/* VOLATILE GLOBAL g_614 */
static uint64_t g_667 = 18446744073709551615UL;
static int32_t ** const *g_711 = (void*)0;
static int32_t ** const **g_710 = &g_711;
static int32_t ** const *** volatile g_709 = &g_710;/* VOLATILE GLOBAL g_709 */
static uint32_t g_775[6][5] = {{0xC378AF0FL,1UL,0xC378AF0FL,0xC378AF0FL,1UL},{1UL,0xC378AF0FL,0xC378AF0FL,1UL,0xC378AF0FL},{1UL,1UL,1UL,1UL,1UL},{0xC378AF0FL,1UL,0xC378AF0FL,0xC378AF0FL,1UL},{1UL,0xC378AF0FL,0xC378AF0FL,1UL,0xC378AF0FL},{1UL,1UL,1UL,1UL,1UL}};
static uint16_t g_808 = 0x9A29L;
static float *****g_812 = &g_535;
static uint64_t *g_828 = &g_128;
static float g_886[2][10] = {{(-0x1.7p-1),0x1.7p-1,0x8.Dp+1,0x1.2p+1,0x8.Dp+1,0x1.7p-1,(-0x1.7p-1),(-0x1.7p-1),0x1.7p-1,0x8.Dp+1},{0x1.7p-1,(-0x1.7p-1),(-0x1.7p-1),0x1.7p-1,0x8.Dp+1,0x1.2p+1,0x8.Dp+1,0x1.7p-1,(-0x1.7p-1),(-0x1.7p-1)}};
static float g_889[2][7][7] = {{{0x0.FCA55Bp+38,(-0x5.1p-1),(-0x1.Cp+1),0xD.4DF68Fp-47,0x0.2p+1,(-0x1.Bp-1),0x0.Cp-1},{0x5.A33376p-57,(-0x10.5p+1),0x8.0p+1,0x0.4p+1,(-0x1.5p+1),0x0.2p+1,0x1.9p+1},{0x8.0p+1,0xC.F2D68Fp-28,(-0x1.Bp-1),(-0x1.5p+1),(-0x1.8p-1),0x5.A33376p-57,0x5.A33376p-57},{0x3.11A16Cp+95,0x2.2p+1,(-0x1.Bp-1),0x2.2p+1,0x3.11A16Cp+95,0x1.9p+1,0x0.2p+1},{0xC.F2D68Fp-28,(-0x8.Dp-1),0x8.0p+1,0x4.3p-1,0x0.4p+1,0x0.Cp-1,(-0x1.Bp-1)},{0x4.4p+1,0x0.2p-1,(-0x1.Cp+1),0x0.FCA55Bp+38,0xD.550ED7p+88,(-0x8.Dp-1),(-0x10.5p+1)},{0xC.F2D68Fp-28,0x4.3p-1,0xE.2D7D76p+82,0x0.A2EF4Bp+39,0x4.4p+1,(-0x1.5p+1),0xF.478355p-48}},{{0x3.11A16Cp+95,(-0x1.9p+1),(-0x1.5p+1),(-0x1.Cp+1),(-0x1.Cp+1),(-0x1.5p+1),(-0x1.9p+1)},{0x8.0p+1,0x5.A33376p-57,0xF.478355p-48,0x1.6p-1,0xD.4DF68Fp-47,(-0x8.Dp-1),(-0x1.Cp+1)},{0x5.A33376p-57,0x1.6p-1,0x0.A2EF4Bp+39,0x8.0p+1,0x2.2p+1,0x0.Cp-1,0x3.11A16Cp+95},{0x0.FCA55Bp+38,0xF.478355p-48,0xC.F2D68Fp-28,0x1.6p-1,0x0.Cp-1,0x1.9p+1,0xD.4DF68Fp-47},{0x7.648893p-32,0x3.11A16Cp+95,0x4.84CAC2p+55,(-0x1.Cp+1),0x9.49726Bp-76,0x5.A33376p-57,0x9.49726Bp-76},{0x0.A2EF4Bp+39,0x3.11A16Cp+95,0x3.11A16Cp+95,0x0.A2EF4Bp+39,0x0.2p-1,0x0.2p+1,0xD.550ED7p+88},{0x0.Cp-1,0xF.478355p-48,(-0x1.9p+1),0x0.FCA55Bp+38,0x8.0p+1,(-0x1.Bp-1),0xE.2D7D76p+82}}};
static volatile int8_t g_912 = 0xFDL;/* VOLATILE GLOBAL g_912 */
static int32_t ** volatile g_927 = &g_353[0];/* VOLATILE GLOBAL g_927 */
static int32_t ** volatile g_949 = (void*)0;/* VOLATILE GLOBAL g_949 */
static int32_t ** volatile g_950 = (void*)0;/* VOLATILE GLOBAL g_950 */
static int16_t *g_975 = &g_226;
static int16_t **g_974 = &g_975;
static int16_t *** volatile g_973 = &g_974;/* VOLATILE GLOBAL g_973 */
static int32_t **g_986 = &g_353[2];
static int32_t ***g_985 = &g_986;
static int32_t ****g_984[2][5][4] = {{{&g_985,&g_985,&g_985,&g_985},{&g_985,&g_985,(void*)0,&g_985},{&g_985,&g_985,(void*)0,&g_985},{&g_985,&g_985,&g_985,&g_985},{&g_985,&g_985,&g_985,&g_985}},{{(void*)0,&g_985,&g_985,&g_985},{&g_985,&g_985,&g_985,&g_985},{&g_985,&g_985,&g_985,&g_985},{&g_985,&g_985,&g_985,&g_985},{(void*)0,&g_985,&g_985,&g_985}}};
static int8_t g_1029 = 0x15L;
static int64_t g_1064 = 4L;
static int64_t g_1069 = 0xEE905CED23828191LL;
static int64_t g_1070 = (-1L);
static int64_t g_1071 = (-5L);
static uint8_t g_1136 = 0xAEL;
static uint32_t g_1198 = 0x3917F92CL;
static const uint32_t g_1200 = 1UL;
static volatile float g_1248 = (-0x7.Dp+1);/* VOLATILE GLOBAL g_1248 */


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static int32_t * func_2(int32_t * const  p_3);
static const int8_t  func_18(int32_t * p_19, int64_t  p_20);
static int32_t * func_21(int32_t * p_22, uint32_t  p_23);
static int32_t * func_24(int32_t  p_25, uint32_t  p_26, uint64_t  p_27, int8_t  p_28);
static int32_t * func_44(uint32_t  p_45);
static int64_t  func_52(uint32_t * p_53, int64_t  p_54, int64_t  p_55, int32_t  p_56, int32_t  p_57);
static uint32_t * func_58(int32_t ** p_59);
static uint64_t  func_69(int32_t  p_70);
static int32_t  func_74(uint32_t * p_75, float  p_76);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_985 g_986 g_353
 * writes: g_6 g_353
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_5[8][5] = {{&g_6,&g_6,&g_6,&g_6,&g_6},{&g_6,&g_6,&g_6,&g_6,&g_6},{&g_6,(void*)0,&g_6,(void*)0,&g_6},{&g_6,&g_6,(void*)0,&g_6,&g_6},{(void*)0,&g_6,&g_6,&g_6,&g_6},{&g_6,&g_6,&g_6,&g_6,&g_6},{&g_6,&g_6,&g_6,(void*)0,&g_6},{&g_6,&g_6,&g_6,(void*)0,&g_6}};
    int32_t **l_4 = &l_5[0][2];
    int64_t l_1292 = 0L;
    int i, j;
    (**g_985) = func_2(((*l_4) = (void*)0));
    return l_1292;
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_985 g_986 g_353
 * writes: g_6
 */
static int32_t * func_2(int32_t * const  p_3)
{ /* block id: 2 */
    int8_t l_41 = 0x63L;
    int32_t l_43 = (-1L);
    int32_t l_1285[1][10] = {{(-5L),0x00ADE3F0L,(-5L),0x00ADE3F0L,(-5L),0x00ADE3F0L,(-5L),0x00ADE3F0L,(-5L),0x00ADE3F0L}};
    int32_t l_1286[7];
    int32_t l_1287 = 0xD8F25A90L;
    int i, j;
    for (i = 0; i < 7; i++)
        l_1286[i] = 0x0B43EE29L;
    for (g_6 = 0; (g_6 < 13); g_6 = safe_add_func_uint64_t_u_u(g_6, 1))
    { /* block id: 5 */
        uint32_t *l_29[10] = {&g_30[1],&g_30[1],&g_30[1],&g_30[1],&g_30[1],&g_30[1],&g_30[1],&g_30[1],&g_30[1],&g_30[1]};
        int32_t l_31 = 0x8D5FEAF8L;
        int32_t *l_42[8][4][2] = {{{&g_6,&l_31},{&g_6,&g_6},{&g_6,&l_31},{&g_6,(void*)0}},{{&l_31,(void*)0},{&g_6,&l_31},{&g_6,&g_6},{&g_6,&l_31}},{{&g_6,(void*)0},{&l_31,(void*)0},{&g_6,&l_31},{&g_6,&g_6}},{{&g_6,&l_31},{&g_6,(void*)0},{&l_31,(void*)0},{&g_6,&l_31}},{{&g_6,&g_6},{&g_6,&l_31},{&g_6,(void*)0},{&l_31,(void*)0}},{{&g_6,&l_31},{&g_6,&g_6},{&g_6,&l_31},{&g_6,(void*)0}},{{&l_31,(void*)0},{&g_6,&l_31},{&g_6,&g_6},{&g_6,&l_31}},{{&g_6,(void*)0},{&l_31,(void*)0},{&g_6,&l_31},{&g_6,&g_6}}};
        int32_t *l_1288 = &l_31;
        int i, j, k;
    }
    return (**g_985);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int8_t  func_18(int32_t * p_19, int64_t  p_20)
{ /* block id: 607 */
    int16_t *** volatile *l_1284 = &g_973;
    l_1284 = &g_973;
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads : g_322 g_986 g_360 g_361 g_354 g_337 g_338 g_84 g_325 g_255 g_124 g_303 g_154 g_145 g_223 g_362 g_975 g_226 g_828 g_457 g_458 g_217
 * writes: g_322 g_223 g_353 g_362 g_338 g_1069 g_1070 g_84 g_354 g_128
 */
static int32_t * func_21(int32_t * p_22, uint32_t  p_23)
{ /* block id: 414 */
    int32_t l_901 = 0xA9644334L;
    int32_t l_913[3][9][2] = {{{0xC831CB14L,0L},{(-1L),1L},{0x6F1E5B18L,0x6F1E5B18L},{0xC831CB14L,0x6F1E5B18L},{0x6F1E5B18L,1L},{(-1L),0L},{0xC831CB14L,(-1L)},{0L,1L},{0L,(-1L)}},{{0xC831CB14L,0L},{(-1L),1L},{0x6F1E5B18L,0x6F1E5B18L},{0xC831CB14L,0x6F1E5B18L},{0x6F1E5B18L,1L},{(-1L),0L},{0xC831CB14L,(-1L)},{0L,1L},{0L,(-1L)}},{{0xC831CB14L,0L},{(-1L),1L},{0x6F1E5B18L,0x6F1E5B18L},{0xC831CB14L,0x6F1E5B18L},{0x6F1E5B18L,1L},{(-1L),0L},{0xC831CB14L,1L},{0x548BE03EL,(-1L)},{0x548BE03EL,1L}}};
    float ***l_969[6] = {&g_360,&g_360,&g_360,&g_360,&g_360,&g_360};
    int64_t l_1000 = 0xFB169BF8A09B9A2CLL;
    int64_t * const l_1063 = &g_1064;
    int64_t * const *l_1062[7] = {&l_1063,&l_1063,&l_1063,&l_1063,&l_1063,&l_1063,&l_1063};
    int32_t l_1072 = (-2L);
    int32_t **l_1170 = &g_353[0];
    int32_t l_1174 = (-4L);
    uint32_t l_1279 = 4294967295UL;
    uint16_t l_1280[7];
    int16_t *l_1281[2][7] = {{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_226,(void*)0,&g_226,(void*)0,&g_226,(void*)0,&g_226}};
    int8_t *l_1282 = &g_322;
    int32_t l_1283 = 0xD8407B8DL;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1280[i] = 65527UL;
    for (g_322 = 4; (g_322 >= 0); g_322 -= 1)
    { /* block id: 417 */
        int64_t l_907 = 0L;
        int32_t l_911 = 0xD226C7CBL;
        int32_t l_916 = 0x134087B2L;
        int32_t l_920 = 0x38382F23L;
        int32_t l_922 = 0x2DF4DD59L;
        int32_t l_923 = 0L;
        int64_t l_941 = (-7L);
        float **l_988 = &g_361;
        int32_t l_999[5][7] = {{(-6L),0x0D440A95L,0xD5F398AFL,0L,0L,0xD5F398AFL,0x0D440A95L},{(-6L),0x0D440A95L,0xD5F398AFL,0L,0L,0xD5F398AFL,0x0D440A95L},{(-6L),0x0D440A95L,0xD5F398AFL,0L,0L,0xD5F398AFL,0x0D440A95L},{(-6L),0x0D440A95L,0xD5F398AFL,0L,0L,0xD5F398AFL,0x0D440A95L},{(-6L),0x0D440A95L,0xD5F398AFL,0L,0L,0xD5F398AFL,0x0D440A95L}};
        uint8_t l_1001 = 0xA8L;
        int32_t *l_1004 = &l_911;
        int32_t *l_1006 = &l_920;
        const int16_t * const l_1112 = &g_129;
        const int16_t * const *l_1111[8][3][9] = {{{&l_1112,&l_1112,(void*)0,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112}},{{&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112},{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112}},{{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112},{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,(void*)0,&l_1112}},{{&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0},{(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,(void*)0},{&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112}},{{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,(void*)0}},{{&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112},{(void*)0,(void*)0,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112}},{{(void*)0,(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112},{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0},{(void*)0,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,(void*)0,&l_1112,&l_1112}},{{&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,(void*)0},{(void*)0,&l_1112,&l_1112,&l_1112,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112},{&l_1112,(void*)0,&l_1112,&l_1112,(void*)0,&l_1112,&l_1112,&l_1112,(void*)0}}};
        float l_1135 = 0x7.1AD75Dp+47;
        int16_t **l_1142 = (void*)0;
        int32_t **l_1230 = &l_1004;
        int64_t l_1247 = (-1L);
        int i, j, k;
        for (g_223 = 0; (g_223 <= 2); g_223 += 1)
        { /* block id: 420 */
            int32_t *l_904 = (void*)0;
            int32_t *l_906[3][2] = {{&g_301,&g_301},{&g_301,&g_301},{&g_301,&g_301}};
            int32_t l_915 = 0xFC0F80FFL;
            int32_t l_917 = 0xE71298BBL;
            int32_t l_918[4];
            uint64_t l_924 = 0x7E85ED3BADC09230LL;
            const volatile int32_t *l_928 = &g_327;
            uint32_t l_946 = 0xE016226FL;
            int32_t *l_1005 = &l_917;
            int32_t *l_1007 = (void*)0;
            int64_t *l_1060 = (void*)0;
            int64_t ** const l_1059 = &l_1060;
            int i, j;
            for (i = 0; i < 4; i++)
                l_918[i] = 0x87339DC0L;
        }
        (*g_986) = &l_911;
        (*g_337) = (safe_sub_func_float_f_f((+(l_901 = ((**g_360) = ((l_1142 != (void*)0) >= l_1072)))), ((l_913[1][8][0] = ((safe_sub_func_float_f_f(l_913[2][5][0], p_23)) < 0xC.E0E561p-50)) >= (safe_add_func_float_f_f((safe_add_func_float_f_f(0xE.1733BEp-19, (((safe_mul_func_int16_t_s_s((((safe_div_func_int32_t_s_s(((0x5AL | p_23) , (-1L)), (*p_22))) , p_23) & p_23), (*l_1004))) || 0x5D4DDA8CL) , (*l_1004)))), (*g_337))))));
        for (g_1069 = 0; (g_1069 <= 1); g_1069 += 1)
        { /* block id: 542 */
            int16_t *l_1162 = &g_121;
            uint8_t *l_1171 = &l_1001;
            uint8_t *l_1172[3][4] = {{&g_191[2],&g_191[2],(void*)0,(void*)0},{&g_191[4],(void*)0,&g_191[4],(void*)0},{&g_191[4],(void*)0,(void*)0,&g_191[4]}};
            int32_t l_1173 = 0x896C8FF0L;
            uint32_t *l_1176 = &g_775[2][0];
            uint32_t **l_1175 = &l_1176;
            int32_t l_1214 = 5L;
            int32_t l_1217 = 0xCF331F19L;
            int32_t ****l_1235[1];
            int i, j;
            for (i = 0; i < 1; i++)
                l_1235[i] = (void*)0;
            for (g_1070 = 1; (g_1070 >= 0); g_1070 -= 1)
            { /* block id: 545 */
                return p_22;
            }
            (*l_1170) = func_44((safe_div_func_int32_t_s_s(5L, ((safe_add_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u((safe_div_func_int32_t_s_s(((~(((((l_1162 != (void*)0) ^ (((((((*g_154) = (safe_sub_func_uint64_t_u_u(p_23, ((+(safe_add_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_u(((((void*)0 != l_1170) != ((((*l_1004) || (p_23 || ((l_1072 = ((*l_1006) = ((((*l_1171) |= g_84) < g_325) >= g_255[4]))) || g_124[1]))) , 18446744073709551615UL) , l_1173)) ^ g_303), 12)) != p_23), 0x13L))) <= 1L)))) || l_1173) <= 0x9BEA1E65L) , l_1162) != &g_145) || (*l_1004))) > p_23) , 0x4DBD106CL) >= 0xA60A63AAL)) != 0xE197FFDFF4D16712LL), (-1L))), l_1174)), p_23)) & g_145))));
            for (g_223 = 0; (g_223 <= 1); g_223 += 1)
            { /* block id: 555 */
                uint32_t l_1199 = 0x0B4CE21AL;
                int32_t l_1215 = (-1L);
                int32_t l_1216 = (-1L);
                int32_t l_1218 = (-5L);
                uint32_t l_1219 = 4294967295UL;
                const int32_t ****l_1236 = (void*)0;
                float *l_1240[10][4] = {{(void*)0,&g_68[4][2],&g_362,&g_362},{(void*)0,(void*)0,&g_68[4][2],&g_126},{&g_68[2][3],&g_362,(void*)0,&g_889[1][3][2]},{&g_68[4][2],&g_886[0][5],&g_886[1][1],(void*)0},{&g_886[0][5],&g_886[0][5],&g_886[0][5],&g_889[1][3][2]},{&g_886[0][5],&g_362,&g_886[0][5],&g_126},{&g_126,(void*)0,&g_68[4][2],&g_362},{&g_68[4][2],&g_68[4][2],&g_68[4][2],&g_68[4][2]},{&g_126,&g_889[1][3][2],&g_886[0][5],&g_886[0][5]},{&g_886[0][5],&g_126,&g_886[0][5],&g_68[4][2]}};
                int32_t l_1243 = 0L;
                int i, j;
                for (l_916 = 0; (l_916 <= 1); l_916 += 1)
                { /* block id: 558 */
                    (*p_22) = (*p_22);
                }
            }
        }
    }
    l_901 &= (p_23 > (safe_div_func_uint32_t_u_u(((safe_sub_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s(((*l_1282) = (((((safe_mod_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_s((*g_154), 15)), (((l_913[2][1][0] |= (safe_lshift_func_int16_t_s_s((((((safe_mul_func_float_f_f((0x0.Bp+1 <= (safe_sub_func_float_f_f((**g_360), ((*g_975) , (*g_337))))), (&l_913[0][4][1] != ((((*g_828) = p_23) > p_23) , &l_913[0][4][1])))) >= l_1279) , l_1280[6]) , 0xD2L) , p_23), p_23))) >= 0xFBC2L) , (*g_457)))) || 0x1FL) , g_217) , &g_974) == (void*)0)), l_1174)) <= l_1283), p_23)) > 0x6642L), (*p_22))) , p_23), 0x83ABCAB1L)));
    (*l_1170) = p_22;
    return p_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_34 g_6 g_84 g_30 g_122 g_123 g_124 g_129 g_128 g_121 g_150 g_154 g_145 g_191 g_68 g_199 g_215 g_223 g_227 g_255 g_226 g_217 g_303 g_324 g_325 g_336 g_339 g_360 g_354 g_301 g_328 g_361 g_362 g_216 g_446 g_455 g_456 g_535 g_337 g_457 g_458 g_557 g_536 g_667 g_614 g_709 g_322 g_828
 * writes: g_68 g_84 g_121 g_126 g_128 g_129 g_145 g_150 g_154 g_191 g_123 g_223 g_227 g_255 g_226 g_34 g_303 g_322 g_301 g_324 g_325 g_336 g_328 g_327 g_353 g_354 g_446 g_535 g_30 g_338 g_557 g_536 g_362 g_667 g_710 g_886 g_889
 */
static int32_t * func_24(int32_t  p_25, uint32_t  p_26, uint64_t  p_27, int8_t  p_28)
{ /* block id: 9 */
    int32_t *l_61 = &g_6;
    int32_t **l_60 = &l_61;
    int16_t l_359 = (-1L);
    float **l_363 = &g_361;
    float *l_882 = &g_68[4][7];
    float *l_883 = (void*)0;
    float *l_884 = &g_126;
    float *l_885 = &g_886[0][5];
    int32_t l_887[5][10] = {{1L,(-5L),(-1L),0L,(-1L),(-5L),1L,0xB9DE3436L,(-1L),0xD1A660B1L},{(-1L),0x183AF080L,0x163D887EL,5L,0xB9DE3436L,5L,0x626D8410L,0x626D8410L,5L,0xB9DE3436L},{(-1L),0x183AF080L,0x183AF080L,(-1L),5L,(-1L),1L,0xD1A660B1L,1L,7L},{0x163D887EL,(-5L),1L,1L,0x183AF080L,5L,0x183AF080L,1L,1L,(-5L)},{0x95065218L,(-1L),7L,0xB9DE3436L,0x183AF080L,0xCA30BEE8L,1L,5L,0L,0L}};
    float *l_888 = &g_889[1][3][2];
    int32_t l_890 = 0xA06C6064L;
    float l_891 = 0x1.5p-1;
    int64_t ***l_892 = (void*)0;
    int32_t *l_893 = &g_354[2][0];
    uint8_t l_894 = 0x46L;
    int i, j;
    (*l_60) = func_44((safe_rshift_func_int8_t_s_u((3UL == (safe_rshift_func_uint8_t_u_s(((((safe_div_func_int64_t_s_s(func_52(func_58(l_60), ((l_359 == (g_6 , (((l_363 = g_360) == (void*)0) != (((((g_30[0] >= (g_30[1] != 18446744073709551609UL)) < 0x73F0A9B692279F36LL) || 0xCF7D55AAL) & g_30[1]) != 1L)))) != g_6), g_354[3][1], p_27, g_30[1]), p_26)) & p_28) , (void*)0) == (void*)0), p_25))), 7)));
    (*g_337) = ((**g_360) = ((p_27 == (safe_lshift_func_uint8_t_u_s(252UL, (((safe_div_func_uint16_t_u_u(((p_25 == (safe_mul_func_float_f_f(((safe_sub_func_float_f_f(((((((**g_360) >= (safe_mul_func_float_f_f(((safe_add_func_float_f_f((*g_361), ((((((l_890 = ((p_28 > ((((*l_888) = (safe_sub_func_float_f_f(((l_887[2][5] = (safe_add_func_float_f_f(0x8.0p-1, (!((*l_885) = ((*l_884) = (safe_div_func_float_f_f(0x1.6p+1, ((*l_882) = (safe_sub_func_float_f_f((((((~0x2D4419FDL) , 0x31FC90BDL) , p_26) == (-0x4.6p-1)) , 0x9.71CC59p-53), 0xF.C816AEp-17))))))))))) <= (*g_361)), p_26))) , (*g_154)) , (*g_457))) == (*g_154))) , (*g_828)) > 4L) <= 1L) != 0x77L) , p_28))) == p_27), 0x7.F95F9Bp+84))) != l_359) , g_226) , (**g_360)) == p_25), (**g_360))) < 0x0.2p-1), p_26))) , (*g_154)), 1L)) > g_301) > p_26)))) , p_28));
    (*l_893) &= ((((void*)0 != l_892) & 1UL) & l_887[2][5]);
    ++l_894;
    return &g_354[2][0];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_44(uint32_t  p_45)
{ /* block id: 400 */
    int32_t *l_859 = (void*)0;
    return l_859;
}


/* ------------------------------------------ */
/* 
 * reads : g_154 g_84 g_223 g_199 g_123 g_124 g_191 g_301 g_34 g_324 g_354 g_145 g_122 g_328 g_30 g_361 g_362 g_215 g_216 g_217 g_446 g_455 g_456 g_150 g_325 g_128 g_6 g_535 g_337 g_360 g_129 g_227 g_255 g_457 g_458 g_226 g_557 g_121 g_536 g_303 g_667 g_614 g_709 g_322
 * writes: g_354 g_324 g_123 g_84 g_446 g_322 g_191 g_535 g_30 g_338 g_121 g_150 g_557 g_303 g_536 g_223 g_362 g_667 g_710
 */
static int64_t  func_52(uint32_t * p_53, int64_t  p_54, int64_t  p_55, int32_t  p_56, int32_t  p_57)
{ /* block id: 180 */
    uint32_t *l_365 = &g_30[1];
    uint32_t **l_364 = &l_365;
    int16_t l_366 = (-6L);
    uint32_t *l_367[10];
    int32_t l_368 = 0xED04191BL;
    int8_t *l_383[1];
    int32_t **l_400[7][7] = {{&g_353[0],&g_353[1],&g_353[0],&g_353[0],&g_353[0],&g_353[1],&g_353[0]},{(void*)0,&g_353[0],&g_353[1],&g_353[0],&g_353[0],&g_353[0],&g_353[1]},{&g_353[0],&g_353[0],(void*)0,&g_353[0],(void*)0,(void*)0,(void*)0},{&g_353[0],&g_353[1],&g_353[1],&g_353[0],&g_353[0],(void*)0,&g_353[0]},{(void*)0,(void*)0,&g_353[0],&g_353[0],(void*)0,(void*)0,&g_353[1]},{(void*)0,&g_353[0],(void*)0,&g_353[0],&g_353[0],(void*)0,&g_353[0]},{(void*)0,&g_353[1],(void*)0,(void*)0,&g_353[0],&g_353[0],(void*)0}};
    int32_t ***l_399[2][10] = {{&l_400[3][6],&l_400[3][5],&l_400[3][6],&l_400[3][6],&l_400[3][5],&l_400[3][6],&l_400[3][6],&l_400[3][5],&l_400[3][6],&l_400[3][6]},{(void*)0,(void*)0,(void*)0,&l_400[3][5],(void*)0,(void*)0,(void*)0,&l_400[3][5],(void*)0,(void*)0}};
    int32_t ****l_398 = &l_399[1][1];
    int32_t l_503 = 0x85740216L;
    uint16_t *l_505 = (void*)0;
    int16_t l_522 = 5L;
    uint32_t l_523 = 0x6A587B32L;
    float ***l_601 = &g_360;
    int32_t l_612 = 0L;
    int8_t l_615 = 0L;
    int i, j;
    for (i = 0; i < 10; i++)
        l_367[i] = &g_30[1];
    for (i = 0; i < 1; i++)
        l_383[i] = &g_322;
lbl_508:
    l_368 ^= (p_54 & (((*l_364) = (p_53 = p_53)) != ((l_366 ^ 4294967295UL) , l_367[8])));
    if (((p_56 <= (+(0xDB37D4CBDD056B5DLL < ((safe_add_func_int64_t_s_s(0x8792839816672525LL, (safe_lshift_func_uint8_t_u_s((safe_div_func_int8_t_s_s((((safe_lshift_func_int8_t_s_s(((((((0x279585D140373F57LL | (safe_mod_func_int64_t_s_s(p_57, ((safe_div_func_int64_t_s_s(p_55, (l_366 , (((safe_unary_minus_func_int8_t_s(((((*g_154) && ((l_368 &= ((p_54 , p_54) <= 1L)) || g_223)) >= 0x4496L) == 9L))) && 0x3B7FDE95L) | 0x9E56529D9DC93415LL)))) & 0x4772L)))) && (**g_199)) != g_223) <= 1L) > l_366) >= l_366), 5)) == g_191[4]) | p_54), g_191[4])), g_301)))) >= g_34[0][3][0])))) < p_54))
    { /* block id: 185 */
        return g_324;
    }
    else
    { /* block id: 187 */
        int32_t *l_384 = &g_354[2][0];
        int32_t *l_385 = &l_368;
        int32_t l_435 = 0xADFD7EACL;
        int32_t l_437[1];
        const float *l_544 = &g_68[1][0];
        const float ** const l_543 = &l_544;
        float ***l_623 = &g_360;
        int64_t * const l_653 = &g_150;
        int64_t * const * const l_652[1][5][3] = {{{&l_653,&l_653,&l_653},{&l_653,&l_653,&l_653},{(void*)0,&l_653,&l_653},{(void*)0,(void*)0,&l_653},{&l_653,&l_653,&l_653}}};
        const uint16_t *l_664 = &g_223;
        int8_t *l_739[6] = {&g_322,(void*)0,(void*)0,&g_322,(void*)0,(void*)0};
        uint32_t l_810 = 0x8E6ED13EL;
        float *l_837 = (void*)0;
        float ** const l_836 = &l_837;
        float ** const *l_835 = &l_836;
        float ** const **l_834 = &l_835;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_437[i] = 0xCAA5BAC8L;
        if (((*l_385) = ((*l_384) = l_368)))
        { /* block id: 190 */
            int8_t *l_388 = &g_324;
            uint32_t **l_393[6];
            int32_t l_401[5][6] = {{0L,(-1L),0L,(-1L),0L,(-1L)},{0xD75D45A3L,(-1L),0xD75D45A3L,(-1L),0xD75D45A3L,(-1L)},{0L,(-1L),0L,(-1L),0L,(-1L)},{0xD75D45A3L,(-1L),0xD75D45A3L,(-1L),0xD75D45A3L,(-1L)},{0L,(-1L),0L,(-1L),0L,(-1L)}};
            int i, j;
            for (i = 0; i < 6; i++)
                l_393[i] = (void*)0;
            (*l_384) = ((safe_div_func_uint16_t_u_u((((void*)0 != l_388) >= ((*l_388) = ((-10L) == ((*l_385) = (l_368 ^ ((((((g_34[5][6][2] != p_57) < ((((safe_sub_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((((p_53 = ((*l_364) = p_53)) == ((safe_mod_func_int16_t_s_s((safe_add_func_int8_t_s_s((l_398 != &l_399[1][1]), g_124[1])), g_354[2][0])) , l_384)) != p_56), 0x8C1EL)), p_54)) || g_354[3][1]) == l_401[3][4]) && (*l_384))) != 0x1DL) > (*l_385)) < 0x9561FC705CBADFCFLL) && p_57)))))), p_55)) , (*g_123));
            (*g_122) = ((safe_sub_func_uint8_t_u_u(g_145, (!(safe_lshift_func_uint8_t_u_u(0x64L, 7))))) , ((!(safe_rshift_func_uint8_t_u_s(0UL, 4))) , (void*)0));
        }
        else
        { /* block id: 197 */
            int8_t l_423 = (-1L);
            float l_428 = 0x5.42BDA7p+41;
            float ***l_430 = &g_360;
            int32_t l_433 = (-4L);
            int32_t l_441 = 7L;
            int32_t l_442 = 6L;
            int32_t l_443 = (-10L);
            int32_t l_444[10];
            int8_t l_519 = 0xDBL;
            int64_t l_531 = 0L;
            int32_t *l_553 = (void*)0;
            uint64_t l_564 = 5UL;
            int16_t *l_585 = &g_303;
            uint16_t l_616 = 0UL;
            int32_t ** const *l_707 = &l_400[3][6];
            int32_t ** const **l_706 = &l_707;
            int i;
            for (i = 0; i < 10; i++)
                l_444[i] = (-10L);
lbl_561:
            (*l_384) |= (safe_sub_func_uint64_t_u_u((!(((-1L) < (g_191[4] > ((0x563E01A304107595LL > (safe_lshift_func_uint8_t_u_u(p_54, 3))) > p_57))) | ((~((((void*)0 != &g_322) ^ (1L & (((*g_154) |= (safe_unary_minus_func_int32_t_s(((~0x1DL) > 0x7EL)))) <= 2L))) > g_328[4])) < p_54))), g_30[1]));
            if (g_145)
                goto lbl_631;
            for (g_324 = 0; (g_324 >= 18); g_324++)
            { /* block id: 202 */
                uint32_t l_431 = 0xA5BFBA87L;
                int32_t l_432 = 0xF935192AL;
                int32_t l_438 = 0x26903F35L;
                int32_t l_445 = 1L;
                float ***l_460 = (void*)0;
                int32_t l_511[7][3][8] = {{{0xA93E8386L,0x38F3F42CL,6L,0xA93E8386L,0x234940AAL,0L,0x27A0BAE5L,0x6D34E7B4L},{0xA93E8386L,(-1L),5L,(-4L),0x27A0BAE5L,0x6080111DL,0x4CF17241L,0x4CF17241L},{0x6D34E7B4L,0xD52962E6L,0x6DC99BBAL,0x6DC99BBAL,0xD52962E6L,0x6D34E7B4L,6L,(-4L)}},{{0x2480694EL,0x38F3F42CL,0x234940AAL,1L,0x6DC99BBAL,6L,0x2480694EL,0xE13BADAAL},{0xD52962E6L,0x27A0BAE5L,5L,1L,0x4CF17241L,5L,0xA93E8386L,(-4L)},{0xE13BADAAL,0x4CF17241L,0x38F3F42CL,0x6DC99BBAL,0x2480694EL,0x6DC99BBAL,0x38F3F42CL,0x4CF17241L}},{{(-1L),6L,0xE13BADAAL,(-4L),0x6DC99BBAL,0x234940AAL,(-4L),0x6D34E7B4L},{1L,0x2480694EL,0x6080111DL,0xA93E8386L,(-1L),1L,0x234940AAL,0x38F3F42CL},{(-4L),6L,(-2L),(-7L),0x1673F1BDL,0L,0L,0x1673F1BDL}},{{0x1673F1BDL,0L,0L,0x1673F1BDL,(-7L),(-2L),6L,(-4L)},{0x38F3F42CL,0x234940AAL,1L,0x6DC99BBAL,6L,0x2480694EL,0xE13BADAAL,0x6D34E7B4L},{(-4L),0x234940AAL,(-7L),0x9BEBD055L,0x234940AAL,(-2L),5L,0x6DC99BBAL}},{{0L,0L,0x9BEBD055L,0xE13BADAAL,0x9BEBD055L,0L,0L,(-2L)},{0x234940AAL,6L,1L,0L,0x6D34E7B4L,1L,0x38F3F42CL,0x50DBD52EL},{(-2L),0xE13BADAAL,5L,0x9BEBD055L,0x6D34E7B4L,(-7L),0L,0xE13BADAAL}},{{0x234940AAL,5L,(-4L),0x50DBD52EL,0x9BEBD055L,0x9BEBD055L,0x50DBD52EL,(-4L)},{0L,0L,0x2480694EL,0x38F3F42CL,0x234940AAL,1L,0x6DC99BBAL,6L},{(-4L),0x38F3F42CL,0x8FF595BCL,(-7L),6L,5L,0L,6L}},{{0x38F3F42CL,0L,0x6080111DL,0x38F3F42CL,(-7L),(-4L),0x1673F1BDL,(-4L)},{0x1673F1BDL,0x50DBD52EL,1L,0x50DBD52EL,0x1673F1BDL,0x2480694EL,0x6D34E7B4L,0xE13BADAAL},{(-4L),0x6DC99BBAL,0L,0x9BEBD055L,0x6DC99BBAL,0x8FF595BCL,5L,0x50DBD52EL}}};
                uint32_t l_560 = 0x3DFDACF3L;
                const volatile int32_t *l_613 = &g_327;
                int i, j, k;
                if ((!(safe_mod_func_int16_t_s_s((p_57 , 0x9611L), g_30[0]))))
                { /* block id: 203 */
                    int32_t l_429[4];
                    uint16_t *l_472 = &g_84;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_429[i] = 0xFF05F87BL;
                    if ((l_429[3] = ((1UL == (((*g_154) | (l_423 , 0x6144L)) && (safe_rshift_func_uint16_t_u_s(((((*g_361) , &g_336[2][1]) == (l_429[2] , l_430)) || ((**g_215) == l_431)), g_191[4])))) || l_429[2])))
                    { /* block id: 205 */
                        int32_t l_434[4] = {0L,0L,0L,0L};
                        int32_t l_436 = 0xBE354D0AL;
                        int32_t l_439 = 8L;
                        int32_t l_440[8] = {(-3L),9L,(-3L),9L,(-3L),9L,(-3L),9L};
                        int i;
                        --g_446[1][5][0];
                        l_385 = p_53;
                    }
                    else
                    { /* block id: 208 */
                        float ****l_459[2][10][8] = {{{&l_430,&l_430,(void*)0,&l_430,&l_430,&l_430,&l_430,&l_430},{(void*)0,(void*)0,&l_430,(void*)0,(void*)0,&l_430,(void*)0,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,(void*)0,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,(void*)0,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,(void*)0,&l_430,(void*)0,&l_430,&l_430,&l_430},{(void*)0,&l_430,&l_430,&l_430,(void*)0,&l_430,&l_430,&l_430},{&l_430,&l_430,(void*)0,&l_430,&l_430,&l_430,&l_430,(void*)0}},{{&l_430,(void*)0,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,(void*)0,&l_430,(void*)0,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,(void*)0,&l_430,(void*)0},{&l_430,&l_430,(void*)0,&l_430,&l_430,(void*)0,&l_430,&l_430},{&l_430,(void*)0,&l_430,&l_430,&l_430,&l_430,&l_430,&l_430},{&l_430,&l_430,&l_430,&l_430,&l_430,&l_430,(void*)0,&l_430}}};
                        int32_t l_461 = (-5L);
                        int64_t ** const l_469 = (void*)0;
                        uint16_t *l_473 = &g_223;
                        int i, j, k;
                        (*l_384) = (0x2073E9CAL ^ (safe_mod_func_uint8_t_u_u((((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u((((((g_455 != g_456) | (0x901CL && ((l_460 = &g_360) == &g_360))) , (((void*)0 != &g_216[1]) , (g_322 = (((*l_385) = ((-4L) != l_429[2])) & l_429[0])))) || l_461) > p_54), p_55)) | (*l_384)), g_84)) == 0x353D06A8L) != g_446[1][5][0]), 0xCBL)));
                        (*l_385) = ((safe_mod_func_int32_t_s_s(((p_54 | (-2L)) != ((safe_mul_func_int8_t_s_s(0xFAL, (~(safe_lshift_func_int8_t_s_s(l_429[0], (l_469 != (void*)0)))))) , p_57)), ((safe_lshift_func_uint16_t_u_u(((l_429[2] > (((l_472 != l_473) , p_56) || p_56)) && l_461), 9)) | 0xA57D0B8BL))) , (-1L));
                    }
                    l_385 = &l_444[5];
                }
                else
                { /* block id: 216 */
                    uint32_t l_492 = 0xB9E5621BL;
                    if (((safe_sub_func_float_f_f((p_54 < 0x1.Bp+1), (p_55 >= (safe_mul_func_float_f_f(((((g_324 & (safe_mul_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_u(0xBCL, (safe_lshift_func_int8_t_s_u((safe_sub_func_uint16_t_u_u(p_55, 0xA20AL)), (safe_rshift_func_uint16_t_u_s(((((safe_sub_func_uint32_t_u_u((safe_mod_func_uint16_t_u_u(0UL, ((*g_154) = p_57))), p_54)) & p_57) > l_492) >= 0x35L), 3)))))) | p_55), p_57))) < p_56) , l_431) > 0x0.679AABp+29), (*l_384)))))) , 0x3E3E66BAL))
                    { /* block id: 218 */
                        uint8_t *l_498 = &g_191[4];
                        uint32_t *l_504 = &l_431;
                        int32_t l_506 = 0x4066393BL;
                        (*l_384) = ((!((l_506 &= ((((*l_504) |= (((safe_sub_func_uint32_t_u_u((p_57 >= ((safe_rshift_func_uint8_t_u_u(((*l_498) |= g_124[0]), (g_150 || 65527UL))) && ((((safe_mod_func_int16_t_s_s((g_124[9] > ((safe_mul_func_int8_t_s_s((l_503 && g_325), p_56)) , ((g_128 > (*g_154)) ^ g_128))), p_56)) , 0UL) != 0x161C96B1157A0F71LL) , p_57))), p_55)) >= p_55) , 0xBD532876L)) , l_505) != (void*)0)) == l_492)) == 0xCF55E906L);
                        return l_442;
                    }
                    else
                    { /* block id: 224 */
                        (*l_385) &= (+((p_56 < 4294967295UL) ^ l_492));
                        if (p_56)
                            goto lbl_508;
                    }
                }
                for (l_445 = 0; (l_445 != 14); l_445 = safe_add_func_uint64_t_u_u(l_445, 7))
                { /* block id: 231 */
                    int8_t l_512 = 0xA0L;
                    int32_t l_513 = 0xCCB5D994L;
                    int32_t l_514 = 0x31998246L;
                    int32_t l_515 = 0L;
                    int32_t l_516 = (-1L);
                    int32_t l_517 = (-1L);
                    int32_t l_518 = (-10L);
                    int32_t l_520 = 0x072120BAL;
                    int32_t l_521[8] = {(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L)};
                    int i;
                    ++l_523;
                }
                (*l_385) |= (safe_add_func_uint32_t_u_u((0x46L && (p_54 >= (((safe_unary_minus_func_int32_t_s(((*l_384) = 0L))) | (l_511[1][1][1] , p_54)) , (safe_mul_func_int8_t_s_s(0xC8L, p_56))))), (**g_215)));
                if (l_531)
                { /* block id: 236 */
                    float ****l_533 = &l_430;
                    float *****l_534 = &l_533;
                    int16_t *l_545 = (void*)0;
                    int16_t *l_546 = (void*)0;
                    int16_t *l_547 = &g_121;
                    int32_t *l_548 = &l_503;
                    (*g_337) = (((l_441 == (g_6 == (p_54 > (+(7L || (&g_154 == (void*)0)))))) <= (p_55 , ((g_30[1] = ((((((*l_534) = l_533) != (g_535 = g_535)) , p_55) , p_56) , 0xA09EF43BL)) >= (**g_215)))) , l_442);
                    l_441 |= (((l_444[3] >= (safe_rshift_func_int16_t_s_s(((*l_547) = (g_191[2] < ((g_128 , (***l_534)) != l_543))), 13))) >= (((((*l_548) = (1L | 0xD9958D024D92F79DLL)) , ((g_30[1] = (l_432 = 4294967290UL)) != ((l_442 ^= (((safe_mod_func_uint64_t_u_u((((((l_553 = &l_511[1][1][1]) == &l_435) != p_55) , &l_423) != (void*)0), g_129)) | l_438) <= 0x7251FC11L)) < l_531))) > (*l_385)) , 0UL)) ^ 255UL);
                    for (g_150 = (-6); (g_150 >= 5); g_150 = safe_add_func_uint32_t_u_u(g_150, 3))
                    { /* block id: 250 */
                        const volatile int32_t **l_556[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        g_557 = (*g_122);
                        (*l_384) = (safe_mod_func_uint32_t_u_u(0x6CC57A27L, ((*l_553) = l_560)));
                    }
                }
                else
                { /* block id: 255 */
                    int32_t l_563 = 0x558127AFL;
                    uint8_t l_600 = 1UL;
                    if (l_431)
                        goto lbl_561;
                    if (((safe_unary_minus_func_uint8_t_u(g_324)) , (p_57 , (*l_384))))
                    { /* block id: 257 */
                        float l_569 = 0xE.4CE2EBp-86;
                        int16_t *l_586[10] = {&g_226,&l_366,&g_226,&g_226,&l_366,&g_226,&g_226,&l_366,&g_226,&g_226};
                        int32_t l_587 = 1L;
                        int i;
                        ++l_564;
                        (*l_385) = (((g_227 >= 251UL) , ((safe_sub_func_uint8_t_u_u(g_325, (((((0x0.0A9A87p-74 <= (**g_360)) <= (0x7.0C2121p+70 < (safe_mul_func_float_f_f(p_55, ((safe_mul_func_float_f_f((safe_mul_func_float_f_f((((safe_rshift_func_uint16_t_u_s((safe_sub_func_int32_t_s_s((*l_384), (safe_rshift_func_int8_t_s_u(((((((-(safe_mul_func_float_f_f((*l_385), l_563))) >= p_57) < p_54) , l_585) == l_586[2]) >= p_57), 2)))), 6)) , p_55) > p_57), l_587)), p_54)) < 0x8.1p+1))))) != (*g_361)) > l_587) , (*l_384)))) != g_354[2][2])) > g_255[0]);
                        return (*g_457);
                    }
                    else
                    { /* block id: 261 */
                        int32_t *l_588 = (void*)0;
                        l_588 = &l_442;
                        (*l_384) = ((safe_add_func_uint8_t_u_u((*l_588), (((safe_add_func_int16_t_s_s((*l_588), ((**g_215) == ((p_56 < ((safe_add_func_int16_t_s_s(((safe_add_func_uint64_t_u_u((((&g_123 != ((safe_sub_func_int32_t_s_s((((void*)0 != &g_303) & ((*l_585) = ((((((g_30[1] = (safe_unary_minus_func_int8_t_s((g_324 != (p_56 < 0x68L))))) , (*g_154)) && p_56) & (*l_385)) > 0x38AB42B117A61394LL) >= p_55))), (*l_588))) , (void*)0)) <= 1L) != l_600), p_57)) > p_57), (*g_154))) < g_226)) ^ 4UL)))) || l_432) < l_438))) || 0xBD6BL);
                        (*l_588) &= (((l_511[0][2][7] && l_563) , l_601) != (((((-3L) | (p_57 , 255UL)) && (safe_mul_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((safe_add_func_int32_t_s_s(((*l_384) = (safe_add_func_int64_t_s_s((safe_sub_func_uint8_t_u_u(p_54, (((((p_56 | p_55) | (*g_154)) == (*g_557)) && l_612) , g_354[2][0]))), 5UL))), 0xD9DB7477L)), g_129)), g_121))) , (*l_384)) , (*g_535)));
                        l_613 = (*g_199);
                    }
                    l_616--;
                    (*l_385) = 0x562A7567L;
                }
            }
lbl_631:
            (*l_384) ^= (safe_mod_func_int8_t_s_s(((safe_mul_func_int16_t_s_s(p_57, ((((*g_535) = l_623) != (void*)0) , ((((safe_mul_func_int8_t_s_s(p_57, 0x7AL)) & ((safe_div_func_uint8_t_u_u((+(g_255[5] != (g_324 |= (0xBE1B3732L < (65535UL && (safe_mul_func_int16_t_s_s(((0L > (*l_385)) & p_55), 0x023FL))))))), l_444[3])) | g_303)) | 0x4213L) >= 0x8E46L)))) , p_55), 9L));
            for (g_303 = 0; (g_303 >= (-18)); g_303 = safe_sub_func_int32_t_s_s(g_303, 4))
            { /* block id: 280 */
                uint32_t l_657 = 0x424B2860L;
                int32_t l_705 = 0x47C8F525L;
                uint64_t l_716 = 0x1FBCDBAA360C4AD0LL;
                int64_t *l_728[10] = {&g_150,&g_150,&g_34[3][3][2],&g_34[2][6][0],&g_34[3][3][2],&g_150,&g_150,&g_34[3][3][2],&g_34[2][6][0],&g_34[3][3][2]};
                int64_t ** const l_727 = &l_728[3];
                int32_t l_730 = (-5L);
                int i;
                for (g_121 = 0; (g_121 > (-18)); g_121 = safe_sub_func_uint16_t_u_u(g_121, 1))
                { /* block id: 283 */
                    uint32_t l_658[6][8][5] = {{{0x45F6A0C7L,4294967295UL,0x45F6A0C7L,4294967295UL,4294967295UL},{4294967290UL,0xC9966AA9L,1UL,1UL,4UL},{0x51338D01L,0x6749C41FL,4294967295UL,0x61497C64L,1UL},{0x9CBF16D7L,4UL,1UL,4UL,0x9CBF16D7L},{2UL,4294967290UL,0x45F6A0C7L,4294967287UL,0x61497C64L},{0xFC537342L,0xBE4C34CDL,0UL,4294967291UL,0x3332D77AL},{4294967295UL,0x45F6A0C7L,0x61497C64L,4294967290UL,0x61497C64L},{4294967291UL,4294967291UL,0xC9966AA9L,0UL,0x9CBF16D7L}},{{0x61497C64L,0xB49727DFL,4294967295UL,4294967295UL,1UL},{1UL,0x3A463316L,0x3332D77AL,0x2196302FL,4UL},{0xDE7180C3L,0xB49727DFL,0xB49727DFL,0xDE7180C3L,4294967295UL},{0xBE4C34CDL,4294967291UL,6UL,0x9CBF16D7L,0x62F1B5C4L},{4294967295UL,0x45F6A0C7L,5UL,4294967295UL,5UL},{4UL,6UL,3UL,3UL,6UL},{4294967295UL,0xDE7180C3L,0x6749C41FL,1UL,0xB49727DFL},{4294967295UL,4294967291UL,1UL,0x3A463316L,0x3332D77AL}},{{0xDE7180C3L,0x51338D01L,2UL,4294967295UL,4294967295UL},{4294967295UL,0x2196302FL,4294967295UL,0xBE4C34CDL,0xC9966AA9L},{4294967295UL,0x6749C41FL,4294967290UL,0xDE7180C3L,0x61497C64L},{4UL,0x3A463316L,0x9CBF16D7L,1UL,0UL},{0x45F6A0C7L,0x61497C64L,4294967290UL,0x61497C64L,0x45F6A0C7L},{6UL,0xFC537342L,4294967295UL,4294967291UL,1UL},{1UL,0xB49727DFL,2UL,4294967295UL,4294967295UL},{1UL,4294967295UL,1UL,0xFC537342L,1UL}},{{4294967295UL,4294967295UL,0x6749C41FL,2UL,0x45F6A0C7L},{1UL,0x3332D77AL,3UL,0x9CBF16D7L,0UL},{4294967290UL,4294967287UL,4294967295UL,0x51338D01L,0x61497C64L},{4294967290UL,0x3332D77AL,0x3332D77AL,4294967290UL,0xC9966AA9L},{0xB49727DFL,4294967295UL,5UL,0x45F6A0C7L,4294967295UL},{3UL,4294967295UL,0x62F1B5C4L,1UL,0x3332D77AL},{4294967287UL,0xB49727DFL,0x45F6A0C7L,0x45F6A0C7L,0xB49727DFL},{0UL,0xFC537342L,0x2196302FL,4294967290UL,6UL}},{{4294967295UL,0x61497C64L,4294967295UL,0x51338D01L,5UL},{0xFC537342L,0x3A463316L,0xBE4C34CDL,0x9CBF16D7L,0x9CBF16D7L},{4294967295UL,0x6749C41FL,4294967295UL,2UL,4294967295UL},{0UL,0x2196302FL,1UL,0xFC537342L,4294967291UL},{4294967287UL,0x51338D01L,4294967295UL,4294967295UL,4294967295UL},{3UL,4294967291UL,1UL,4294967291UL,3UL},{0xB49727DFL,0xDE7180C3L,4294967295UL,0x61497C64L,4294967295UL},{4294967290UL,6UL,0xBE4C34CDL,1UL,0x62F1B5C4L}},{{4294967290UL,4294967295UL,4294967295UL,0xDE7180C3L,4294967295UL},{1UL,1UL,0x2196302FL,0xBE4C34CDL,3UL},{4294967295UL,5UL,0x45F6A0C7L,4294967295UL,4294967295UL},{1UL,4UL,0x62F1B5C4L,0x3A463316L,4294967291UL},{1UL,5UL,5UL,1UL,4294967295UL},{6UL,1UL,0x3332D77AL,3UL,0x9CBF16D7L},{0x45F6A0C7L,4294967295UL,4294967295UL,4294967290UL,5UL},{4UL,6UL,3UL,3UL,6UL}}};
                    uint16_t **l_670 = &l_505;
                    const uint16_t l_675 = 0UL;
                    int32_t *l_676 = &l_437[0];
                    int32_t l_712 = 0xBD97286AL;
                    int32_t l_714[4] = {1L,1L,1L,1L};
                    int i, j, k;
                    for (g_223 = 0; (g_223 <= 1); g_223 += 1)
                    { /* block id: 286 */
                        int64_t *l_656 = &l_531;
                        int64_t **l_655 = &l_656;
                        int64_t ***l_654 = &l_655;
                        int i;
                        (***l_623) = (safe_add_func_float_f_f((safe_sub_func_float_f_f((g_30[g_223] != p_56), (safe_sub_func_float_f_f((safe_sub_func_float_f_f(((((p_57 == p_54) != ((safe_sub_func_float_f_f((safe_add_func_float_f_f((((((safe_lshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_u((g_30[1] == (l_652[0][3][2] == ((*l_654) = (void*)0))), ((((l_657 = (*l_384)) || (**g_456)) > (-6L)) != g_30[g_223]))), l_658[4][7][1])) >= 0x0EL) & p_55) == g_34[4][7][1]) , p_54), 0x0.0p-1)), (*g_361))) < p_57)) > l_658[0][1][1]) >= 0x0.BD590Dp-38), g_30[g_223])), g_30[g_223])))), (*g_361)));
                    }
                    if (l_657)
                        continue;
                    if ((((safe_mod_func_int8_t_s_s((g_30[1] > (+((safe_mod_func_int16_t_s_s(((l_664 != (g_446[1][5][0] , (p_55 , ((*l_670) = ((safe_sub_func_uint32_t_u_u((g_667--), (p_56 == 4294967295UL))) , &g_223))))) | (((*l_385) , (safe_rshift_func_uint16_t_u_s((safe_div_func_int64_t_s_s(l_675, 1UL)), 1))) | (*l_384))), 0x57ADL)) & (-9L)))), p_54)) & (*g_154)) < (*g_457)))
                    { /* block id: 294 */
                        int32_t l_704 = 0xEA85677CL;
                        int32_t ** const ***l_708 = &l_706;
                        l_676 = (void*)0;
                        l_705 ^= ((*l_385) ^= ((((0x3EL >= ((((((safe_rshift_func_uint8_t_u_u((safe_sub_func_int32_t_s_s((safe_mod_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u(l_657, (l_657 , (safe_rshift_func_uint16_t_u_u((*g_154), 6))))), ((safe_mul_func_uint8_t_u_u(g_191[4], ((*g_154) != (safe_rshift_func_uint16_t_u_u(((~p_55) > (safe_lshift_func_int8_t_s_u(((safe_mod_func_uint16_t_u_u(((((safe_add_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((((safe_mod_func_int64_t_s_s(((safe_add_func_uint8_t_u_u(p_54, p_57)) != 0x52FAE713A1C20FFELL), l_657)) < 0xACL) >= p_55), l_704)), (*g_154))) > 0L) != p_54) != 7L), p_56)) > p_54), 0))), p_56))))) ^ (*g_154)))), l_443)), g_614)) > g_255[7]) , &g_216[6]) == &g_216[6]) , p_54) && 0x71B7A8D2L)) > (*l_384)) != g_30[0]) <= 0xEBL));
                        (*g_709) = ((*l_708) = l_706);
                    }
                    else
                    { /* block id: 300 */
                        int8_t l_713[8] = {(-1L),8L,(-1L),8L,(-1L),8L,(-1L),8L};
                        int32_t l_715 = 0xD74373A4L;
                        int32_t l_729 = (-1L);
                        int i;
                        (*l_384) = (-1L);
                        l_716--;
                        if ((**g_122))
                            continue;
                        l_730 = (safe_lshift_func_int16_t_s_u(((safe_add_func_int64_t_s_s((*l_676), (((l_705 = ((safe_lshift_func_uint8_t_u_u((5UL | (++(**l_670))), 2)) && ((*l_505) ^= p_54))) < g_322) || (*l_676)))) < ((((((p_55 , (*g_215)) != &g_30[1]) , (l_727 == (void*)0)) != (*l_676)) | p_57) , l_729)), 14));
                    }
                    for (l_730 = 5; (l_730 >= 0); l_730 -= 1)
                    { /* block id: 311 */
                        int32_t l_737 = 9L;
                        int i;
                        (*l_385) = (safe_add_func_int32_t_s_s((g_328[l_730] != (-6L)), ((safe_add_func_int16_t_s_s((safe_mul_func_int16_t_s_s(l_737, (+(l_739[4] != (void*)0)))), (*g_154))) != (((safe_unary_minus_func_int32_t_s((safe_sub_func_uint16_t_u_u((0xDE4BL <= ((p_57 , g_191[4]) ^ 0xA2L)), l_737)))) <= g_301) , 2L))));
                    }
                }
            }
        }
        for (g_84 = 0; (g_84 != 35); g_84 = safe_add_func_uint64_t_u_u(g_84, 1))
        { /* block id: 319 */
            uint8_t *l_750 = &g_191[4];
            uint8_t **l_749 = &l_750;
            int32_t l_751 = 0xE6F860A3L;
            uint16_t *l_754 = &g_145;
            uint64_t *l_755 = &g_667;
            uint64_t *l_756[5];
            float l_757[4][8] = {{0x0.7p-1,0xF.C5FAB0p+68,0xF.C5FAB0p+68,0xF.C5FAB0p+68,0x6.9p-1,0x6.9p-1,0xF.C5FAB0p+68,0x6.9p-1},{0xF.C5FAB0p+68,0xF.C5FAB0p+68,0x0.7p-1,0xF.C5FAB0p+68,0xF.C5FAB0p+68,0x0.7p-1,0xF.C5FAB0p+68,0xF.C5FAB0p+68},{0x6.9p-1,0xF.C5FAB0p+68,0x6.9p-1,0x6.9p-1,0xF.C5FAB0p+68,0x6.9p-1,0x6.9p-1,0xF.C5FAB0p+68},{0xF.C5FAB0p+68,0x6.9p-1,0x6.9p-1,0xF.C5FAB0p+68,0x6.9p-1,0x6.9p-1,0xF.C5FAB0p+68,0x6.9p-1}};
            int32_t l_763 = (-9L);
            int32_t l_764 = 0xDEB3BCC9L;
            int32_t l_773 = 1L;
            int i, j;
            for (i = 0; i < 5; i++)
                l_756[i] = &g_255[0];
        }
    }
    return p_55;
}


/* ------------------------------------------ */
/* 
 * reads : g_34 g_6 g_84 g_30 g_122 g_123 g_124 g_129 g_128 g_121 g_150 g_154 g_145 g_191 g_68 g_199 g_215 g_223 g_227 g_255 g_226 g_217 g_303 g_324 g_325 g_336 g_339
 * writes: g_68 g_84 g_121 g_126 g_128 g_129 g_145 g_150 g_154 g_191 g_123 g_223 g_227 g_255 g_226 g_34 g_303 g_322 g_301 g_324 g_325 g_336 g_328 g_327 g_353
 */
static uint32_t * func_58(int32_t ** p_59)
{ /* block id: 10 */
    int32_t *l_66 = &g_6;
    int32_t **l_65[1];
    float *l_67[8][8][3] = {{{&g_68[2][7],&g_68[4][2],&g_68[4][2]},{&g_68[4][2],&g_68[4][2],(void*)0},{&g_68[4][2],&g_68[5][2],&g_68[4][2]},{&g_68[2][7],(void*)0,(void*)0},{&g_68[4][0],&g_68[5][3],&g_68[4][2]},{&g_68[4][2],&g_68[2][7],&g_68[2][7]},{&g_68[1][4],(void*)0,(void*)0},{&g_68[4][2],(void*)0,&g_68[3][3]}},{{(void*)0,&g_68[2][7],&g_68[3][1]},{&g_68[4][2],&g_68[5][3],&g_68[2][0]},{&g_68[1][2],(void*)0,&g_68[4][2]},{&g_68[4][2],&g_68[5][2],&g_68[4][0]},{&g_68[4][2],&g_68[4][2],&g_68[5][3]},{&g_68[4][2],&g_68[4][2],&g_68[4][2]},{(void*)0,&g_68[4][0],&g_68[3][3]},{&g_68[4][2],&g_68[2][7],&g_68[4][2]}},{{&g_68[4][2],&g_68[5][3],&g_68[0][2]},{&g_68[4][2],(void*)0,&g_68[4][2]},{&g_68[4][2],&g_68[4][2],&g_68[4][2]},{&g_68[4][2],&g_68[4][2],&g_68[1][6]},{&g_68[4][5],(void*)0,&g_68[4][2]},{&g_68[5][3],&g_68[4][2],&g_68[4][2]},{(void*)0,&g_68[1][2],&g_68[2][0]},{&g_68[5][3],&g_68[5][3],&g_68[4][2]}},{{&g_68[4][5],&g_68[4][2],&g_68[4][2]},{&g_68[4][2],(void*)0,(void*)0},{&g_68[4][2],&g_68[4][2],&g_68[4][1]},{&g_68[4][2],&g_68[4][0],&g_68[4][2]},{&g_68[4][2],&g_68[4][2],(void*)0},{&g_68[4][2],&g_68[5][1],&g_68[0][2]},{(void*)0,&g_68[5][3],&g_68[4][2]},{&g_68[4][2],&g_68[4][2],(void*)0}},{{&g_68[4][2],&g_68[4][0],(void*)0},{&g_68[4][2],&g_68[4][2],&g_68[4][2]},{&g_68[1][2],&g_68[4][2],&g_68[4][2]},{&g_68[4][2],(void*)0,&g_68[4][1]},{(void*)0,&g_68[5][1],&g_68[4][2]},{&g_68[4][2],&g_68[5][2],&g_68[4][2]},{&g_68[1][4],&g_68[4][2],&g_68[4][1]},{&g_68[4][2],&g_68[4][2],&g_68[4][2]}},{{&g_68[4][0],&g_68[4][2],&g_68[4][2]},{&g_68[2][7],&g_68[4][2],(void*)0},{&g_68[4][2],(void*)0,(void*)0},{&g_68[4][2],&g_68[1][2],&g_68[4][2]},{&g_68[2][7],&g_68[4][2],&g_68[0][2]},{&g_68[1][4],&g_68[4][2],(void*)0},{&g_68[4][2],&g_68[4][0],&g_68[4][2]},{(void*)0,&g_68[2][7],&g_68[4][1]}},{{&g_68[4][2],(void*)0,(void*)0},{&g_68[5][3],(void*)0,&g_68[4][2]},{(void*)0,&g_68[2][7],&g_68[4][2]},{&g_68[1][1],(void*)0,&g_68[2][0]},{&g_68[4][2],&g_68[3][0],&g_68[4][2]},{&g_68[4][0],(void*)0,&g_68[4][2]},{&g_68[0][0],(void*)0,&g_68[2][0]},{&g_68[4][2],(void*)0,&g_68[4][2]}},{{(void*)0,(void*)0,&g_68[2][0]},{&g_68[4][2],&g_68[4][2],&g_68[5][2]},{&g_68[4][2],&g_68[4][2],&g_68[4][1]},{&g_68[4][2],&g_68[4][2],(void*)0},{(void*)0,&g_68[4][7],(void*)0},{&g_68[4][2],(void*)0,&g_68[1][4]},{(void*)0,(void*)0,&g_68[3][0]},{&g_68[5][2],(void*)0,(void*)0}}};
    uint32_t *l_77 = (void*)0;
    uint32_t *l_130[10] = {(void*)0,&g_30[1],(void*)0,&g_30[1],(void*)0,&g_30[1],(void*)0,&g_30[1],(void*)0,&g_30[1]};
    int16_t l_131 = 0xAE24L;
    int64_t *l_197 = (void*)0;
    uint32_t * const *l_218 = &l_130[8];
    int64_t *l_307[10] = {&g_34[5][2][2],&g_34[5][6][3],&g_34[5][6][3],&g_34[5][2][2],&g_34[5][6][3],&g_34[5][6][3],&g_34[5][2][2],&g_34[5][6][3],&g_34[5][6][3],&g_34[5][2][2]};
    const volatile int32_t *l_326 = &g_327;
    uint16_t *l_349 = &g_84;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_65[i] = &l_66;
lbl_195:
    (*p_59) = (void*)0;
    if (((safe_mod_func_int8_t_s_s((((+(l_131 = ((((g_68[3][0] = (l_65[0] == &l_66)) < g_34[2][0][2]) , ((*l_66) < func_69((g_34[5][3][3] , 0L)))) ^ ((*l_66) , (safe_add_func_int16_t_s_s((func_69(func_74(l_77, (*l_66))) ^ g_34[5][2][2]), 0x58E7L)))))) , p_59) == &g_123), g_30[0])) , (*l_66)))
    { /* block id: 23 */
        uint16_t *l_143 = &g_84;
        uint16_t *l_144[1];
        int32_t l_146 = 0xB3B4D8F4L;
        uint64_t l_149 = 0x98E47A8F7DD28A49LL;
        uint16_t **l_155 = &g_154;
        uint32_t *l_180 = (void*)0;
        int i;
        for (i = 0; i < 1; i++)
            l_144[i] = &g_145;
        g_150 ^= (safe_div_func_uint64_t_u_u(g_128, (+(safe_add_func_int16_t_s_s(((safe_mul_func_int16_t_s_s(((safe_add_func_float_f_f(((safe_div_func_float_f_f(g_121, (((g_145 = ((*l_143) = g_124[1])) != (g_129 != l_146)) , ((safe_add_func_float_f_f(l_149, (((g_124[1] , ((5L == (&g_128 != &g_128)) != (-6L))) , p_59) != &g_123))) < 0x9.9B7D33p-98)))) == 0x2.A67DBEp+99), g_128)) , 0L), g_6)) < 1L), g_6)))));
        if ((safe_unary_minus_func_uint32_t_u((((safe_mul_func_uint16_t_u_u(((*l_66) <= (((*l_155) = (l_144[0] = g_154)) == (void*)0)), g_34[5][2][2])) == (safe_sub_func_uint64_t_u_u(g_124[8], l_149))) != g_145))))
        { /* block id: 29 */
            int8_t l_160[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            uint32_t **l_173 = &l_77;
            int64_t *l_181 = &g_150;
            int32_t l_182 = 0x5C755685L;
            int i;
lbl_221:
            if ((safe_rshift_func_int16_t_s_u((l_160[6] & ((safe_lshift_func_uint8_t_u_u(l_149, ((l_182 |= (safe_div_func_int8_t_s_s((((*l_181) = (safe_sub_func_int32_t_s_s(((safe_mul_func_int8_t_s_s((((safe_mod_func_uint8_t_u_u(((*l_66) && (((*l_173) = &g_30[1]) != (((g_129 && 1UL) & (&g_30[1] == l_67[4][0][2])) , ((safe_mod_func_int16_t_s_s(((safe_mod_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_u(255UL, g_30[1])), l_146)) > g_84), l_160[6])) , l_180)))), 4L)) && l_149) != 0UL), (-1L))) >= 0x9F2AL), 0L))) < l_149), 0xBAL))) & (*l_66)))) , l_146)), (*g_154))))
            { /* block id: 33 */
                uint16_t l_183 = 65533UL;
                l_183 ^= (18446744073709551609UL || l_149);
                if (g_145)
                    goto lbl_222;
                for (l_183 = 0; (l_183 <= 1); l_183 += 1)
                { /* block id: 37 */
                    int i;
                    if (g_30[l_183])
                        break;
                    for (g_121 = 0; (g_121 <= 1); g_121 += 1)
                    { /* block id: 41 */
                        uint8_t l_187 = 0x40L;
                        int32_t l_188 = 0xA1BD8FF2L;
                        g_191[4] |= ((l_188 = ((safe_sub_func_int8_t_s_s((~(((*l_181) = 0x0A3B53B9236ECDBELL) == (&g_145 == (void*)0))), (g_124[6] && l_187))) || ((void*)0 != &g_30[1]))) , ((((safe_rshift_func_int16_t_s_u(l_146, ((*g_123) != 0L))) || 1UL) >= l_146) > 0L));
                        if (l_149)
                            break;
                        (*p_59) = (*p_59);
                    }
                }
            }
            else
            { /* block id: 49 */
                int64_t * const l_196 = &g_34[5][2][2];
                for (g_84 = 0; (g_84 <= 9); g_84 += 1)
                { /* block id: 52 */
                    uint32_t l_192[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_192[i] = 0x0BFACF2AL;
                    l_192[1]--;
                    (*p_59) = l_130[g_84];
                    if (g_145)
                        goto lbl_222;
                    for (g_145 = 0; (g_145 <= 9); g_145 += 1)
                    { /* block id: 57 */
                        if (g_145)
                            goto lbl_195;
                    }
                    for (l_131 = 5; (l_131 >= 0); l_131 -= 1)
                    { /* block id: 62 */
                        int8_t l_198 = 0xB4L;
                        int i, j;
                        l_198 = ((l_196 == l_197) < 0x7.0p-1);
                        g_68[4][2] = g_68[l_131][(l_131 + 2)];
                        (*g_199) = (*g_122);
                    }
                }
            }
lbl_222:
            for (g_128 = 0; (g_128 <= 9); g_128 += 1)
            { /* block id: 71 */
                uint16_t l_200 = 65535UL;
                int16_t *l_219 = &g_121;
                int32_t l_220[9];
                int i;
                for (i = 0; i < 9; i++)
                    l_220[i] = 0x2A3349E9L;
                l_220[0] = (g_30[0] <= ((l_200 , ((((safe_mod_func_uint32_t_u_u((((*l_219) = ((l_182 = ((l_146 = l_149) == (safe_mul_func_uint16_t_u_u(65534UL, ((safe_mod_func_int64_t_s_s((((((*g_154) = ((*g_154) <= (safe_div_func_uint32_t_u_u(((l_130[g_128] != (void*)0) > ((safe_add_func_int32_t_s_s(((((safe_rshift_func_int8_t_s_s((((safe_div_func_float_f_f((l_200 > 0x1.Bp+1), 0x1.1E251Cp-10)) , g_215) != l_218), g_34[1][3][1])) < (-1L)) ^ g_6) < l_149), 0x8069DB77L)) , 0x8222L)), 0x82150C06L)))) != 0x2D4DL) >= 0x3F941D61AFEE84A3LL) >= l_182), l_200)) & l_200))))) | l_160[6])) >= 0L), 0x130ACF16L)) | 65535UL) > g_34[5][2][2]) == g_30[1])) > l_149));
                if (l_182)
                    goto lbl_221;
                if (g_84)
                    goto lbl_195;
            }
            g_223++;
        }
        else
        { /* block id: 83 */
            uint32_t l_242 = 5UL;
            uint8_t l_254 = 0xE7L;
            uint32_t *l_258 = (void*)0;
            for (l_131 = 9; (l_131 >= 0); l_131 -= 1)
            { /* block id: 86 */
                int32_t l_245 = (-6L);
                int i;
                for (g_129 = 0; (g_129 <= 1); g_129 += 1)
                { /* block id: 89 */
                    for (g_128 = 0; (g_128 <= 8); g_128 += 1)
                    { /* block id: 92 */
                        int i;
                        return l_130[(g_129 + 8)];
                    }
                }
                --g_227;
                g_255[5] ^= (safe_rshift_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s(((safe_mod_func_int64_t_s_s((safe_sub_func_uint32_t_u_u(((safe_div_func_int32_t_s_s(((((void*)0 != l_130[l_131]) < (safe_mul_func_uint16_t_u_u(((l_242 == ((l_130[l_131] == ((((((safe_mod_func_int16_t_s_s((0xEC05L <= ((l_245 && (((safe_div_func_int32_t_s_s((**g_199), 0x9BCD83B6L)) && ((safe_div_func_float_f_f(((g_68[4][2] = ((safe_add_func_float_f_f((g_126 = (safe_div_func_float_f_f(l_242, l_245))), 0x2.0F9016p-93)) != g_34[6][3][1])) >= 0x6.Fp+1), (-0x1.8p+1))) , g_34[2][5][1])) ^ l_242)) || 0L)), l_146)) != l_245) && 0UL) , g_223) , l_146) , (*g_122))) >= 1UL)) , 65528UL), 0xE3F5L))) && g_84), l_242)) || l_245), l_254)), l_254)) ^ g_191[1]), l_242)), l_254));
                for (g_223 = 0; (g_223 <= 5); g_223 += 1)
                { /* block id: 102 */
                    const volatile int32_t **l_256 = (void*)0;
                    const volatile int32_t **l_257[6] = {&g_123,&g_123,&g_123,&g_123,&g_123,&g_123};
                    int32_t l_280 = (-7L);
                    int i;
                    (*g_122) = (*g_122);
                    for (g_150 = 1; (g_150 >= 0); g_150 -= 1)
                    { /* block id: 106 */
                        return l_258;
                    }
                    for (g_129 = 0; (g_129 <= 1); g_129 += 1)
                    { /* block id: 111 */
                        int16_t *l_279[2];
                        int32_t l_281 = (-3L);
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_279[i] = &g_226;
                        l_281 = (((safe_add_func_float_f_f((((g_30[g_129] > ((safe_sub_func_float_f_f((safe_add_func_float_f_f((safe_add_func_float_f_f(g_68[g_129][g_223], (safe_sub_func_float_f_f((-(safe_sub_func_float_f_f(g_30[g_129], (l_146 = ((void*)0 == l_130[l_131]))))), (safe_sub_func_float_f_f(((+(safe_sub_func_float_f_f((((g_223 < (safe_add_func_float_f_f((((((-8L) >= (g_191[4] , ((g_226 ^= g_30[1]) < (*g_154)))) | 1L) , g_150) , 0x0.4p+1), 0x1.4p-1))) != g_223) , 0x1.Fp-1), g_124[0]))) == l_245), g_255[3])))))), 0x2.7p-1)), g_30[1])) == l_280)) >= 0x3.EA3133p-87) , g_84), g_6)) > l_245) == g_68[4][2]);
                    }
                }
            }
        }
        (*p_59) = &l_146;
        for (g_129 = 0; (g_129 <= 1); g_129 += 1)
        { /* block id: 122 */
            uint32_t l_290 = 0x3FE579FFL;
            int64_t *l_299 = &g_34[5][2][2];
            int32_t *l_300[10] = {&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301};
            int32_t l_302 = 0x05B46385L;
            int i;
            g_303 &= (safe_mod_func_uint8_t_u_u(0UL, ((safe_mod_func_int32_t_s_s((safe_sub_func_int64_t_s_s(((*l_299) = (((l_302 = (l_146 = ((((*g_154) = (((*p_59) == ((((safe_div_func_uint16_t_u_u(1UL, g_217)) >= l_290) >= (safe_add_func_int32_t_s_s((safe_lshift_func_uint8_t_u_u(l_290, 4)), ((safe_mul_func_int8_t_s_s((safe_mod_func_int16_t_s_s(((void*)0 != l_299), 0x9C13L)), l_290)) >= 0x39C3L)))) , (*g_199))) ^ l_290)) | 0xE83EL) , g_84))) , 1L) || 0x96L)), g_145)), 0xA4DD6817L)) & l_290)));
            (**p_59) = (safe_div_func_int8_t_s_s(0xA0L, (*l_66)));
        }
    }
    else
    { /* block id: 130 */
        int64_t **l_306 = &l_197;
        uint64_t *l_312 = &g_255[1];
        uint64_t l_313 = 18446744073709551615UL;
        uint16_t *l_320[5];
        int8_t *l_321 = &g_322;
        int32_t *l_323 = &g_301;
        int32_t l_330 = 0x8A4ACF00L;
        int32_t l_331 = (-1L);
        int32_t l_332 = 0L;
        int i;
        for (i = 0; i < 5; i++)
            l_320[i] = &g_223;
        if (l_131)
            goto lbl_195;
        g_324 |= ((((((*l_306) = l_197) != l_307[3]) , (((((*l_323) = ((safe_rshift_func_uint8_t_u_u((((safe_lshift_func_int16_t_s_u(((((*l_312) = 0x2927434CF9F92A51LL) , &l_131) == (void*)0), (l_313 == ((safe_mul_func_int8_t_s_s((((safe_rshift_func_uint16_t_u_u(l_313, 9)) < (safe_add_func_int8_t_s_s(((*l_321) = (&g_145 != l_320[1])), g_124[1]))) , 0L), (*l_66))) ^ l_313)))) , 0x51L) >= g_34[5][2][2]), g_30[1])) , (*l_66))) , g_191[4]) , 0x26EF9ED5F9CC0651LL) & 0x20FB83DA78BEC0A9LL)) & l_313) & g_6);
        g_325 = l_313;
        for (g_128 = 0; (g_128 <= 1); g_128 += 1)
        { /* block id: 140 */
            uint16_t **l_352 = &g_154;
            int i;
            if (g_30[g_128])
            { /* block id: 141 */
                int64_t l_329[6] = {0xCE6AA63A2ED1A2A8LL,0xCE6AA63A2ED1A2A8LL,0xCE6AA63A2ED1A2A8LL,0xCE6AA63A2ED1A2A8LL,0xCE6AA63A2ED1A2A8LL,0xCE6AA63A2ED1A2A8LL};
                int32_t * const *l_341 = &l_66;
                int32_t * const **l_340 = &l_341;
                int i;
                l_326 = (*g_199);
                if (g_30[g_128])
                    break;
                for (g_303 = 3; (g_303 >= 0); g_303 -= 1)
                { /* block id: 146 */
                    uint32_t l_350 = 3UL;
                    uint16_t **l_351 = &l_320[4];
                    for (g_325 = 0; (g_325 <= 3); g_325 += 1)
                    { /* block id: 149 */
                        uint8_t l_333 = 8UL;
                        int i, j, k;
                        l_333++;
                        if (g_34[(g_325 + 3)][(g_325 + 2)][g_325])
                            continue;
                        (*g_339) = g_336[0][5];
                        return &g_30[1];
                    }
                    if ((**g_199))
                    { /* block id: 155 */
                        int32_t * const ***l_342 = &l_340;
                        (*l_342) = l_340;
                        g_328[4] = (safe_mod_func_int32_t_s_s((safe_mod_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((((*l_312) = (g_30[g_128] < ((g_129 , &g_122) == (void*)0))) & (((l_350 = ((&g_303 == (void*)0) , (g_150 &= ((g_84 , ((l_349 != (void*)0) & g_223)) && (*l_326))))) && 0x3793D5AF8A9D95EALL) | g_30[1])), (-9L))), 0x15L)), g_30[g_128]));
                        g_327 = (l_351 != l_352);
                    }
                    else
                    { /* block id: 162 */
                        return l_67[6][4][2];
                    }
                }
            }
            else
            { /* block id: 166 */
                const uint32_t l_355 = 18446744073709551615UL;
                g_353[0] = &l_332;
                if (l_355)
                    break;
                for (g_303 = 0; (g_303 > 10); ++g_303)
                { /* block id: 171 */
                    uint64_t l_358[3][9][2] = {{{18446744073709551615UL,0x095A344A1A613836LL},{5UL,18446744073709551614UL},{0xEA915CF9A51A2D85LL,18446744073709551614UL},{0x204DF620D2C9EF17LL,18446744073709551613UL},{18446744073709551613UL,0x1CFEF05E6B33BDA3LL},{9UL,0UL},{0UL,1UL},{1UL,0x9A44CD39AF578687LL},{18446744073709551614UL,0xA2793B3B70A786B7LL}},{{0x1CFEF05E6B33BDA3LL,18446744073709551608UL},{0x9A44CD39AF578687LL,18446744073709551608UL},{0x1CFEF05E6B33BDA3LL,0xA2793B3B70A786B7LL},{18446744073709551614UL,0x9A44CD39AF578687LL},{1UL,1UL},{0UL,0UL},{9UL,0x1CFEF05E6B33BDA3LL},{18446744073709551613UL,18446744073709551613UL},{0x204DF620D2C9EF17LL,18446744073709551614UL}},{{0xEA915CF9A51A2D85LL,18446744073709551614UL},{5UL,0x095A344A1A613836LL},{18446744073709551615UL,5UL},{1UL,0xE81482354808C082LL},{1UL,5UL},{18446744073709551615UL,0x095A344A1A613836LL},{5UL,18446744073709551614UL},{0xEA915CF9A51A2D85LL,18446744073709551614UL},{0x204DF620D2C9EF17LL,18446744073709551613UL}}};
                    int i, j, k;
                    l_358[0][7][1] |= (**g_122);
                    return &g_30[1];
                }
            }
        }
    }
    return &g_30[0];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint64_t  func_69(int32_t  p_70)
{ /* block id: 13 */
    int32_t l_71 = 0x52248422L;
    return l_71;
}


/* ------------------------------------------ */
/* 
 * reads : g_84 g_30 g_6 g_122 g_34 g_123 g_124 g_129
 * writes: g_84 g_121 g_126 g_128 g_129
 */
static int32_t  func_74(uint32_t * p_75, float  p_76)
{ /* block id: 15 */
    uint32_t l_82[9] = {0x77E1F98DL,0x34388704L,0x34388704L,0x77E1F98DL,0x34388704L,0x34388704L,0x77E1F98DL,0x34388704L,0x34388704L};
    uint16_t *l_83 = &g_84;
    int32_t *l_114 = &g_6;
    int32_t **l_113[1];
    float *l_125 = &g_126;
    uint64_t *l_127 = &g_128;
    int i;
    for (i = 0; i < 1; i++)
        l_113[i] = &l_114;
    g_129 |= (safe_add_func_int32_t_s_s((safe_sub_func_uint64_t_u_u(((*l_127) = (((*l_83) = l_82[3]) , (((safe_mul_func_uint16_t_u_u((((((safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_add_func_float_f_f(((safe_div_func_float_f_f(((+l_82[0]) , (g_84 < (safe_mul_func_float_f_f((safe_sub_func_float_f_f(g_30[1], ((safe_add_func_float_f_f((safe_sub_func_float_f_f((!((*l_125) = (safe_add_func_float_f_f((((((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((safe_add_func_float_f_f(0x1.3p+1, (((l_82[3] , l_113[0]) == ((safe_mod_func_uint16_t_u_u((g_6 ^ (g_121 = ((safe_rshift_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((*l_114), 5)), 1)) == 0x34L))), 2L)) , g_122)) != (*l_114)))), g_34[2][2][1])) != g_30[0]), p_76)) >= p_76) < g_30[0]) == p_76) == p_76), 0x7.F46339p+40)))), g_34[5][2][2])), p_76)) == p_76))), 0x3.1p+1)))), (-0x5.1p-1))) > 0xD.357D0Dp+81), (-0x7.9p+1))), g_6)), g_30[1])) , (*g_123)) <= 0x5AC177D1L) , (*l_114)) != (*l_114)), 0xBD03L)) <= g_34[5][0][2]) >= g_30[1]))), 8UL)), g_34[4][3][2]));
    return (*l_114);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_6, "g_6", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_30[i], "g_30[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_34[i][j][k], "g_34[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc_bytes(&g_68[i][j], sizeof(g_68[i][j]), "g_68[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_84, "g_84", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_124[i], "g_124[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_126, sizeof(g_126), "g_126", print_hash_value);
    transparent_crc(g_128, "g_128", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    transparent_crc(g_145, "g_145", print_hash_value);
    transparent_crc(g_150, "g_150", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_191[i], "g_191[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_217, "g_217", print_hash_value);
    transparent_crc(g_223, "g_223", print_hash_value);
    transparent_crc(g_226, "g_226", print_hash_value);
    transparent_crc(g_227, "g_227", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_255[i], "g_255[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_301, "g_301", print_hash_value);
    transparent_crc(g_303, "g_303", print_hash_value);
    transparent_crc(g_322, "g_322", print_hash_value);
    transparent_crc(g_324, "g_324", print_hash_value);
    transparent_crc(g_325, "g_325", print_hash_value);
    transparent_crc(g_327, "g_327", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_328[i], "g_328[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_338, sizeof(g_338), "g_338", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_354[i][j], "g_354[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_362, sizeof(g_362), "g_362", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_446[i][j][k], "g_446[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_458, "g_458", print_hash_value);
    transparent_crc(g_614, "g_614", print_hash_value);
    transparent_crc(g_667, "g_667", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_775[i][j], "g_775[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_808, "g_808", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc_bytes(&g_886[i][j], sizeof(g_886[i][j]), "g_886[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc_bytes(&g_889[i][j][k], sizeof(g_889[i][j][k]), "g_889[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_912, "g_912", print_hash_value);
    transparent_crc(g_1029, "g_1029", print_hash_value);
    transparent_crc(g_1064, "g_1064", print_hash_value);
    transparent_crc(g_1069, "g_1069", print_hash_value);
    transparent_crc(g_1070, "g_1070", print_hash_value);
    transparent_crc(g_1071, "g_1071", print_hash_value);
    transparent_crc(g_1136, "g_1136", print_hash_value);
    transparent_crc(g_1198, "g_1198", print_hash_value);
    transparent_crc(g_1200, "g_1200", print_hash_value);
    transparent_crc_bytes (&g_1248, sizeof(g_1248), "g_1248", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 320
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 149
   depth: 2, occurrence: 36
   depth: 3, occurrence: 7
   depth: 4, occurrence: 1
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 9, occurrence: 2
   depth: 13, occurrence: 2
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 1
   depth: 19, occurrence: 2
   depth: 20, occurrence: 5
   depth: 22, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 4
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 32, occurrence: 1
   depth: 35, occurrence: 2
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 45, occurrence: 2

XXX total number of pointers: 269

XXX times a variable address is taken: 669
XXX times a pointer is dereferenced on RHS: 181
breakdown:
   depth: 1, occurrence: 148
   depth: 2, occurrence: 31
   depth: 3, occurrence: 2
XXX times a pointer is dereferenced on LHS: 160
breakdown:
   depth: 1, occurrence: 147
   depth: 2, occurrence: 10
   depth: 3, occurrence: 3
XXX times a pointer is compared with null: 23
XXX times a pointer is compared with address of another variable: 5
XXX times a pointer is compared with another pointer: 8
XXX times a pointer is qualified to be dereferenced: 4851

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 664
   level: 2, occurrence: 179
   level: 3, occurrence: 40
   level: 4, occurrence: 43
   level: 5, occurrence: 13
XXX number of pointers point to pointers: 98
XXX number of pointers point to scalars: 171
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 35.7
XXX average alias set size: 1.58

XXX times a non-volatile is read: 1050
XXX times a non-volatile is write: 511
XXX times a volatile is read: 97
XXX    times read thru a pointer: 46
XXX times a volatile is write: 17
XXX    times written thru a pointer: 6
XXX times a volatile is available for access: 726
XXX percentage of non-volatile access: 93.2

XXX forward jumps: 3
XXX backward jumps: 7

XXX stmts: 156
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 25
   depth: 1, occurrence: 15
   depth: 2, occurrence: 17
   depth: 3, occurrence: 24
   depth: 4, occurrence: 31
   depth: 5, occurrence: 44

XXX percentage a fresh-made variable is used: 15
XXX percentage an existing variable is used: 85
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2804225294
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint16_t g_19 = 0x45EEL;
static int32_t g_28 = 0x24891023L;
static int32_t g_32 = 4L;
static uint8_t g_48 = 0x6BL;
static int32_t *g_68 = &g_32;
static int32_t g_89 = 0x743E03EBL;
static uint64_t g_92 = 0xD559FE4D3D82F881LL;
static uint32_t g_103 = 18446744073709551615UL;
static float g_111[8] = {0x0.F45F4Dp-49,0x0.F45F4Dp-49,0x0.F45F4Dp-49,0x0.F45F4Dp-49,0x0.F45F4Dp-49,0x0.F45F4Dp-49,0x0.F45F4Dp-49,0x0.F45F4Dp-49};
static uint8_t *g_130 = &g_48;
static uint8_t **g_129[8] = {&g_130,&g_130,&g_130,&g_130,&g_130,&g_130,&g_130,&g_130};
static uint64_t g_153 = 9UL;
static float * volatile g_156 = &g_111[5];/* VOLATILE GLOBAL g_156 */
static uint8_t g_159 = 0x3DL;
static int32_t g_166 = 0x07F47E08L;
static uint64_t g_202[10][5] = {{5UL,5UL,4UL,18446744073709551607UL,7UL},{0x4816DB70B35F69D6LL,0xCE6E74F6A4189961LL,0xCE6E74F6A4189961LL,0x4816DB70B35F69D6LL,0xD3BC77A5DD5B360FLL},{0x42FEC1ED602CC8EELL,18446744073709551607UL,18446744073709551608UL,18446744073709551608UL,18446744073709551607UL},{0xD3BC77A5DD5B360FLL,0xCE6E74F6A4189961LL,0UL,0x5D44B987B1761CDCLL,0x5D44B987B1761CDCLL},{1UL,5UL,1UL,18446744073709551608UL,4UL},{1UL,0x4816DB70B35F69D6LL,0x5D44B987B1761CDCLL,0x4816DB70B35F69D6LL,1UL},{18446744073709551607UL,1UL,7UL,18446744073709551608UL,7UL},{0x5FA26938DC3F510ELL,0x5FA26938DC3F510ELL,0xD3BC77A5DD5B360FLL,0UL,0x4816DB70B35F69D6LL},{1UL,18446744073709551607UL,18446744073709551607UL,1UL,7UL},{0xCE6E74F6A4189961LL,0UL,0x5D44B987B1761CDCLL,0x5D44B987B1761CDCLL,0UL}};
static uint64_t g_232 = 3UL;
static int64_t g_243 = 0xF97BA1A1ADF4364DLL;
static int16_t g_246[4] = {0x9F06L,0x9F06L,0x9F06L,0x9F06L};
static float *g_287 = &g_111[5];
static uint32_t g_294 = 6UL;
static int8_t g_311[3][10][7] = {{{0xD4L,(-1L),0xB1L,0xD5L,1L,(-3L),(-1L)},{0x6EL,(-2L),(-1L),1L,(-1L),0xD5L,(-7L)},{0xA6L,0x86L,0x45L,0x6EL,1L,0xABL,0x0BL},{0x58L,0xA6L,0L,0x98L,0xD4L,0x33L,5L},{0xD5L,0x3DL,0x13L,0xD4L,1L,0xCDL,0xFEL},{0x8AL,0xCDL,0xABL,0xBEL,0x6DL,5L,(-10L)},{6L,(-7L),1L,0x4AL,0x6EL,0x02L,6L},{9L,0x6EL,9L,0xD3L,2L,0xDEL,6L},{0x62L,5L,1L,0xB1L,6L,0xF9L,(-10L)},{0x13L,0L,0x62L,0xBBL,0x5AL,(-1L),0xFEL}},{{1L,(-1L),0xD5L,0x45L,1L,(-2L),5L},{0L,0xF4L,0x10L,1L,4L,0x27L,0x0BL},{0x2FL,(-2L),0L,(-7L),0x39L,0x39L,(-7L)},{0x0BL,(-1L),0x0BL,(-3L),0x59L,0xBEL,(-1L)},{0L,(-6L),0x5AL,(-1L),0x13L,(-1L),0xDEL},{0x03L,(-1L),0xA9L,0x45L,0x58L,0xBEL,0xF2L},{0xCDL,1L,(-1L),(-6L),1L,0x39L,0x6DL},{0xF4L,1L,1L,1L,9L,0x27L,(-1L)},{0xF2L,0x04L,0x27L,0L,1L,(-2L),9L},{0xFEL,0x13L,0x3DL,0L,(-7L),(-1L),1L}},{{(-1L),1L,0x20L,2L,0x60L,0xF9L,0xB1L},{0x04L,(-1L),0L,0x39L,0x43L,0xDEL,0xCDL},{0x60L,(-3L),0x6DL,(-10L),0x43L,0x02L,0x9EL},{0x02L,0xD4L,(-1L),0x04L,0x60L,5L,0xDEL},{0x45L,6L,0x0BL,0x54L,0x64L,1L,6L},{0xABL,0x4AL,(-10L),0xDEL,0x0BL,0x13L,0L},{(-10L),0x82L,0x5AL,0xF9L,2L,(-1L),3L},{0x20L,1L,0xD4L,0xBBL,0xBBL,0xD4L,1L},{(-6L),(-1L),1L,0x13L,0xDEL,6L,0x9EL},{0x6EL,4L,(-2L),0x9EL,0x4AL,1L,0x47L}}};
static uint8_t g_334 = 0xF4L;
static volatile int8_t g_374[10] = {(-9L),(-9L),0x13L,(-9L),(-9L),0x13L,(-9L),(-9L),0x13L,(-9L)};
static volatile int64_t g_377 = 0x29D4AADC0C72F8FDLL;/* VOLATILE GLOBAL g_377 */
static volatile int32_t g_378 = 0x14EE652FL;/* VOLATILE GLOBAL g_378 */
static float g_381[9] = {0x2.7p+1,0x2.7p+1,0x2.7p+1,0x2.7p+1,0x2.7p+1,0x2.7p+1,0x2.7p+1,0x2.7p+1,0x2.7p+1};
static uint8_t g_382 = 0x62L;
static int64_t g_398 = 0x3C2BF1DB0326982ELL;
static volatile int16_t g_402 = 0L;/* VOLATILE GLOBAL g_402 */
static uint32_t g_403 = 4294967287UL;
static int64_t *g_415 = &g_398;
static volatile uint8_t g_419 = 7UL;/* VOLATILE GLOBAL g_419 */
static volatile uint8_t *g_418 = &g_419;
static volatile uint8_t * volatile * const  volatile g_417[8] = {&g_418,&g_418,&g_418,&g_418,&g_418,&g_418,&g_418,&g_418};
static float * volatile g_424[2][9] = {{(void*)0,&g_111[7],(void*)0,&g_111[7],(void*)0,&g_111[7],(void*)0,&g_111[7],(void*)0},{&g_381[5],(void*)0,(void*)0,&g_381[5],&g_381[5],(void*)0,(void*)0,&g_381[5],&g_381[5]}};
static int64_t g_449 = (-10L);
static volatile int8_t g_455 = 1L;/* VOLATILE GLOBAL g_455 */
static uint8_t g_458 = 0UL;
static uint16_t g_466 = 0x5044L;
static uint8_t g_500 = 250UL;
static int64_t g_522[4][2][4] = {{{1L,1L,0x5ECBCB07EB745F4ALL,1L},{1L,8L,8L,1L}},{{8L,1L,8L,8L},{1L,1L,0x5ECBCB07EB745F4ALL,1L}},{{1L,8L,8L,1L},{8L,1L,8L,8L}},{{1L,1L,0x5ECBCB07EB745F4ALL,1L},{1L,8L,8L,1L}}};
static int32_t ** volatile g_525 = &g_68;/* VOLATILE GLOBAL g_525 */
static int16_t * volatile g_540 = &g_246[0];/* VOLATILE GLOBAL g_540 */
static int16_t * volatile *g_539 = &g_540;
static int32_t ** volatile g_542 = &g_68;/* VOLATILE GLOBAL g_542 */
static int64_t g_569 = (-1L);
static volatile uint16_t g_570 = 0x60E0L;/* VOLATILE GLOBAL g_570 */
static uint64_t g_599 = 18446744073709551615UL;
static uint8_t * const *g_645 = (void*)0;
static uint8_t * const **g_644 = &g_645;
static uint32_t g_673 = 0xA3B46D12L;
static int16_t g_692 = 8L;
static volatile int8_t g_697[7][2] = {{0xBAL,0xBAL},{0xBAL,0xBAL},{0xBAL,0xBAL},{0xBAL,0xBAL},{0xBAL,0xBAL},{0xBAL,0xBAL},{0xBAL,0xBAL}};
static uint16_t g_698 = 65532UL;
static volatile uint32_t g_701 = 0UL;/* VOLATILE GLOBAL g_701 */
static volatile uint32_t g_706 = 0xEE2B75CBL;/* VOLATILE GLOBAL g_706 */
static const uint16_t *g_726 = &g_19;
static const uint16_t * volatile *g_725 = &g_726;
static const uint16_t * volatile * volatile * volatile g_724 = &g_725;/* VOLATILE GLOBAL g_724 */
static int32_t ** volatile g_764[6] = {&g_68,&g_68,&g_68,&g_68,&g_68,&g_68};
static volatile uint64_t g_784 = 18446744073709551606UL;/* VOLATILE GLOBAL g_784 */
static volatile int16_t g_807 = (-1L);/* VOLATILE GLOBAL g_807 */
static int32_t g_809 = 5L;
static float **g_999 = &g_287;
static float *** volatile g_998 = &g_999;/* VOLATILE GLOBAL g_998 */
static uint16_t *g_1029 = &g_698;
static uint16_t * const * const g_1105 = &g_1029;
static uint16_t * const * const *g_1104 = &g_1105;
static uint16_t * const * const **g_1103 = &g_1104;
static int32_t * volatile g_1160 = &g_89;/* VOLATILE GLOBAL g_1160 */
static int32_t ** volatile g_1212[2][6][4] = {{{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68}},{{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68}}};
static int32_t *** volatile g_1214[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static volatile int64_t ** volatile *g_1263[3] = {(void*)0,(void*)0,(void*)0};
static int32_t **g_1290 = (void*)0;
static int32_t ** const *g_1289 = &g_1290;
static int32_t ** const ** volatile g_1288[3] = {&g_1289,&g_1289,&g_1289};
static int32_t ** const ** volatile g_1291 = (void*)0;/* VOLATILE GLOBAL g_1291 */
static uint16_t g_1334 = 65529UL;
static int8_t g_1350 = 0x0EL;
static uint32_t g_1362 = 9UL;
static const int32_t *g_1399[4] = {&g_89,&g_89,&g_89,&g_89};
static const volatile uint64_t g_1432 = 0xD1D740DF34D719DBLL;/* VOLATILE GLOBAL g_1432 */
static uint16_t **g_1435[10][9][2] = {{{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029}},{{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029}},{{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029}},{{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029}},{{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0}},{{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029}},{{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029}},{{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0}},{{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029}},{{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,&g_1029},{&g_1029,(void*)0},{&g_1029,&g_1029}}};
static uint16_t ***g_1434 = &g_1435[1][0][1];
static int32_t ** volatile g_1436[4][10][2] = {{{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68}},{{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{&g_68,(void*)0},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,(void*)0},{&g_68,&g_68},{(void*)0,&g_68}},{{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68}},{{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{&g_68,(void*)0},{(void*)0,&g_68}}};
static int16_t g_1497[6][1][3] = {{{(-1L),(-7L),0xB260L}},{{1L,1L,0xB260L}},{{(-7L),(-1L),0x0705L}},{{0xBEADL,1L,0xBEADL}},{{0xBEADL,(-7L),1L}},{{(-7L),0xBEADL,0xBEADL}}};
static int32_t * volatile g_1515 = &g_89;/* VOLATILE GLOBAL g_1515 */
static int16_t * volatile * volatile *g_1544 = &g_539;
static int16_t * volatile * volatile * volatile * volatile g_1543 = &g_1544;/* VOLATILE GLOBAL g_1543 */
static uint32_t g_1587 = 1UL;
static int16_t g_1597 = 0x2B95L;
static int64_t g_1607 = 0x2C3E3B058C0D5740LL;
static uint8_t g_1687 = 255UL;
static const int32_t ** volatile g_1709[9][3] = {{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]},{&g_1399[0],&g_1399[0],&g_1399[0]}};
static uint64_t *g_1744 = &g_202[7][1];
static uint8_t ***g_1771[4] = {&g_129[7],&g_129[7],&g_129[7],&g_129[7]};
static uint8_t ****g_1770[2][7][4] = {{{(void*)0,&g_1771[2],&g_1771[0],&g_1771[3]},{&g_1771[3],&g_1771[3],&g_1771[3],&g_1771[3]},{&g_1771[0],&g_1771[2],(void*)0,(void*)0},{&g_1771[3],&g_1771[2],&g_1771[2],&g_1771[2]},{&g_1771[3],&g_1771[3],&g_1771[2],&g_1771[3]},{&g_1771[3],&g_1771[0],(void*)0,&g_1771[2]},{&g_1771[0],(void*)0,&g_1771[3],(void*)0}},{{&g_1771[3],(void*)0,&g_1771[0],&g_1771[2]},{(void*)0,&g_1771[0],&g_1771[3],&g_1771[3]},{&g_1771[2],&g_1771[3],&g_1771[3],&g_1771[2]},{&g_1771[2],&g_1771[2],&g_1771[3],(void*)0},{(void*)0,&g_1771[2],&g_1771[0],&g_1771[3]},{&g_1771[3],&g_1771[3],&g_1771[3],&g_1771[3]},{&g_1771[0],&g_1771[2],(void*)0,(void*)0}}};
static uint8_t *****g_1769 = &g_1770[0][0][0];
static uint64_t g_1790 = 0xB19A33C453313DA9LL;
static const volatile uint32_t *g_1799 = &g_701;
static const volatile uint32_t **g_1798 = &g_1799;
static const volatile uint32_t ***g_1797 = &g_1798;
static int64_t * const ***g_1819 = (void*)0;
static volatile int16_t g_1826 = 0x5645L;/* VOLATILE GLOBAL g_1826 */
static uint64_t g_1835 = 0x210F1A0D8BC6F044LL;
static int32_t ***g_1914 = &g_1290;
static int32_t ****g_1913 = &g_1914;
static const float ***g_1934 = (void*)0;
static uint8_t g_1951 = 1UL;
static float * volatile g_2117 = &g_381[5];/* VOLATILE GLOBAL g_2117 */
static volatile int16_t g_2191 = 0x9858L;/* VOLATILE GLOBAL g_2191 */
static uint8_t ****g_2220 = &g_1771[1];
static uint32_t *g_2235[3][6][2] = {{{&g_673,&g_673},{&g_673,&g_1587},{&g_1587,&g_673},{&g_1587,&g_673},{&g_294,&g_1587},{&g_1587,&g_1587}},{{&g_1587,&g_1587},{&g_294,&g_673},{&g_1587,&g_673},{&g_294,&g_1587},{&g_1587,&g_1587},{&g_1587,&g_1587}},{{&g_294,&g_673},{&g_1587,&g_673},{&g_294,&g_1587},{&g_1587,&g_1587},{&g_1587,&g_1587},{&g_294,&g_673}}};
static volatile uint64_t ***g_2330 = (void*)0;
static uint64_t **g_2332 = (void*)0;
static uint64_t ***g_2331 = &g_2332;
static int32_t g_2382 = 1L;
static float * volatile g_2383 = &g_111[6];/* VOLATILE GLOBAL g_2383 */
static volatile uint32_t g_2409 = 0xB9362B38L;/* VOLATILE GLOBAL g_2409 */
static volatile uint32_t *g_2408 = &g_2409;
static volatile uint32_t * volatile * volatile g_2407[7][10] = {{&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,(void*)0,(void*)0,&g_2408,&g_2408},{&g_2408,(void*)0,(void*)0,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408},{&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,(void*)0,(void*)0},{&g_2408,&g_2408,&g_2408,(void*)0,(void*)0,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408},{&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408},{&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,(void*)0,(void*)0,&g_2408,&g_2408},{&g_2408,(void*)0,(void*)0,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408,&g_2408}};
static volatile uint32_t * volatile * volatile * volatile g_2406 = &g_2407[4][3];/* VOLATILE GLOBAL g_2406 */
static volatile uint32_t * volatile * volatile * volatile *g_2405 = &g_2406;
static float * volatile g_2419 = &g_111[5];/* VOLATILE GLOBAL g_2419 */
static int64_t g_2439 = 0L;
static volatile uint32_t g_2500 = 0x4E226D98L;/* VOLATILE GLOBAL g_2500 */
static int32_t ** volatile g_2510 = &g_68;/* VOLATILE GLOBAL g_2510 */
static int32_t *g_2511 = (void*)0;
static uint8_t g_2519 = 8UL;
static volatile uint32_t **g_2556 = &g_2408;
static volatile uint32_t ***g_2555 = &g_2556;
static volatile int8_t *g_2577 = (void*)0;
static volatile int8_t ** volatile g_2576 = &g_2577;/* VOLATILE GLOBAL g_2576 */
static volatile int64_t g_2581[5] = {0x706D55BC43DD463ELL,0x706D55BC43DD463ELL,0x706D55BC43DD463ELL,0x706D55BC43DD463ELL,0x706D55BC43DD463ELL};
static int16_t g_2606 = 0x2E96L;
static float ***g_2637[3] = {&g_999,&g_999,&g_999};
static float ****g_2636 = &g_2637[1];
static float *****g_2672[4] = {&g_2636,&g_2636,&g_2636,&g_2636};
static const int16_t *g_2692 = &g_2606;
static const int16_t **g_2691 = &g_2692;
static uint32_t g_2829 = 0x2B49F38AL;
static uint32_t g_2853 = 0xA3153476L;
static float g_2920[10][4][6] = {{{0x6.Bp+1,0x7.6p-1,0xA.DB060Bp+66,(-0x2.Dp+1),0xA.3828E6p-68,(-0x1.7p+1)},{(-0x6.9p+1),0x1.3p-1,0x0.4p+1,0x2.67338Fp-75,(-0x8.1p+1),0x6.01F82Fp+99},{(-0x1.4p+1),0x4.9p+1,0x0.0p+1,0xD.ED793Ap-38,0xC.8150F9p+63,0xA.3828E6p-68},{0x6.88155Bp-66,(-0x1.6p+1),0xB.DA47BEp-46,0x6.01F82Fp+99,0xF.6D40C6p-80,(-0x7.Ap+1)}},{{0x0.4p-1,(-0x2.3p-1),0x1.8p-1,0xB.767C21p-40,0xE.F28D3Bp-97,0x4.38A0A4p+10},{0x6.Cp-1,(-0x2.Dp+1),0xE.F28D3Bp-97,0xB.DA47BEp-46,0x0.4p-1,0x2.D64CC0p-68},{0x7.EFF967p+5,(-0x6.9p+1),(-0x1.5p+1),0x0.4p-1,(-0x9.Ap-1),0x1.8p+1},{0x2.8F2432p+7,0x0.Dp+1,0x4.38A0A4p+10,(-0x6.Fp-1),0x1.3p-1,(-0x9.Ap-1)}},{{0x5.1p-1,0x7.EFF967p+5,0x7.9BBC0Fp+97,0x9.5186F5p+72,0x2.05A328p+64,(-0x1.Dp-1)},{0xB.2480F8p+93,(-0x6.8p-1),0x3.207141p+36,0x9.Fp+1,0x1.8p-1,0x4.2B7C4Cp+77},{0x8.6364E1p-69,0x3.05A61Bp+25,0x2.05A328p+64,0x2.05A328p+64,0x3.05A61Bp+25,0x8.6364E1p-69},{(-0x1.7p+1),(-0x9.Ap-1),(-0x6.Fp-1),(-0x4.7p+1),0x0.2F11ADp-80,0x7.6p-1}},{{(-0x6.8p-1),0x2.952CC7p-69,0x1.8p+1,0x1.Fp+1,(-0x2.0p+1),0x0.4p-1},{(-0x2.3p-1),0x2.D64CC0p-68,0x1.1p+1,(-0x6.Fp-1),0xB.DA47BEp-46,(-0x6.9p+1)},{0x0.2F11ADp-80,(-0x7.4p-1),0x9.A1A492p+28,0xE.27BD90p+97,0x2.67338Fp-75,0x5.4FC485p-91},{0x2.8F2432p+7,0x5.1p-1,0xD.ED793Ap-38,0xF.6D40C6p-80,(-0x9.Ap-1),0x2.B0A965p+13}},{{0x1.Fp+1,0xB.2F1375p+9,(-0x8.1p+1),(-0x1.6p+1),0x6.3p-1,0xA.DB060Bp+66},{(-0x7.Ap+1),0xF.6D40C6p-80,0x6.01F82Fp+99,0xB.DA47BEp-46,(-0x1.6p+1),0x6.88155Bp-66},{0x2.67338Fp-75,0x9.Fp+1,0x0.6p+1,(-0x1.Dp-1),0x6.Bp+1,0xD.ED793Ap-38},{0x1.9p-1,0xE.E8C599p-52,0x1.0p-1,0x8.6364E1p-69,0xF.0621CCp+36,0x0.7p-1}},{{0xE.D0B8B3p-90,0x1.0p-1,0x9.5186F5p+72,(-0x1.Ep+1),0x1.D355E0p-9,0xA.3828E6p-68},{(-0x1.Dp-1),0x1.Dp+1,(-0x6.8p-1),0x7.6p-1,0x2.E273B8p+52,0xE.D0B8B3p-90},{0x3.207141p+36,0x0.4p-1,0x6.Bp+1,0x0.4p+1,0x4.7p+1,0xB.3A6C0Dp-92},{0xB.3A6C0Dp-92,0xB.0839A5p-80,0x0.Cp+1,0x6.Ap+1,(-0x6.Fp-1),0x7.9BBC0Fp+97}},{{0x6.Bp+1,0xA.DB060Bp+66,0x4.38A0A4p+10,0x6.3p-1,0x1.1p+1,0x7.EFF967p+5},{(-0x2.Dp+1),0x1.Fp+1,0xE.F28D3Bp-97,0xE.E8C599p-52,(-0x1.Dp-1),0x1.8p-1},{(-0x6.8p-1),0xA.3828E6p-68,0x4.2B7C4Cp+77,(-0x7.4p-1),(-0x7.Ap+1),(-0x6.Fp-1)},{0xA.3828E6p-68,0x8.Ap+1,(-0x1.5p+1),0x4.2B7C4Cp+77,(-0x1.5p+1),0x8.Ap+1}},{{0xB.0839A5p-80,0xE.F28D3Bp-97,0x1.8p+1,(-0x1.4p+1),0x1.Fp+1,0x1.Dp+1},{0x6.88155Bp-66,(-0x6.Dp-1),0x4.1CA13Bp+50,0x2.05A328p+64,0x0.570968p+57,0x6.Bp+1},{(-0x1.Ep+1),(-0x6.Dp-1),0xE.C8C58Ap+51,0xC.26F6F6p+90,0x1.Fp+1,0x5.1p-1},{0x0.4p+1,0xE.F28D3Bp-97,(-0x1.7p+1),(-0x1.Ap-1),(-0x1.5p+1),0x2.952CC7p-69}},{{0xB.2F1375p+9,0x8.Ap+1,0xC.26F6F6p+90,0x5.4B310Ep-51,(-0x7.Ap+1),0x3.05A61Bp+25},{0xF.0621CCp+36,0xA.3828E6p-68,0x6.8C7BE6p+93,0x6.142D44p-71,(-0x1.Dp-1),0x1.Fp+1},{(-0x8.1p+1),0x1.Fp+1,(-0x1.Ep+1),(-0x6.9p+1),0x1.1p+1,0x3.207141p+36},{(-0x1.Ep+1),0xA.DB060Bp+66,0xB.767C21p-40,(-0x8.1p+1),(-0x6.Fp-1),0x5.5p-1}},{{(-0x6.Fp-1),0xB.0839A5p-80,0x6.Ap+1,0xE.F28D3Bp-97,0x4.7p+1,0x0.0p+1},{0x8.6364E1p-69,0x0.4p-1,0xB.0839A5p-80,0x5.4FC485p-91,0x2.E273B8p+52,(-0x1.Dp-1)},{0x6.Cp-1,0x1.Dp+1,0x2.05A328p+64,(-0x1.Ep+1),0x1.D355E0p-9,0xE.C8C58Ap+51},{0x0.Cp+1,0x1.0p-1,(-0x2.3p-1),0x4.1CA13Bp+50,0xF.0621CCp+36,0xC.8150F9p+63}}};
static uint8_t g_2926[7] = {0xF1L,0xF1L,249UL,0xF1L,0xF1L,249UL,0xF1L};
static uint32_t **g_2931 = &g_2235[2][3][1];
static uint32_t ***g_2930 = &g_2931;
static uint32_t * const *g_2936 = (void*)0;
static uint32_t * const **g_2935 = &g_2936;
static int32_t * const *g_2961 = &g_2511;
static int32_t * const * const *g_2960[2] = {&g_2961,&g_2961};
static int32_t * const * const **g_2959 = &g_2960[0];
static int32_t ** const  volatile g_3077 = &g_68;/* VOLATILE GLOBAL g_3077 */
static int16_t *g_3086 = &g_246[0];
static int64_t **g_3126 = &g_415;
static int64_t ***g_3125 = &g_3126;
static int64_t ****g_3124 = &g_3125;
static int64_t *****g_3123 = &g_3124;
static int8_t g_3156 = 5L;
static int32_t ** volatile g_3237[10][1] = {{&g_2511},{(void*)0},{&g_2511},{(void*)0},{&g_2511},{(void*)0},{&g_2511},{(void*)0},{&g_2511},{(void*)0}};
static int32_t ** volatile g_3238 = &g_2511;/* VOLATILE GLOBAL g_3238 */
static const uint32_t *g_3254 = &g_1362;
static const uint32_t **g_3253 = &g_3254;
static uint16_t g_3280 = 0UL;
static uint8_t g_3323 = 0UL;
static uint32_t **** const *g_3379 = (void*)0;
static volatile uint16_t g_3398 = 0xBBE6L;/* VOLATILE GLOBAL g_3398 */
static int32_t g_3433 = 0x2F1E66BBL;
static int64_t g_3462 = (-1L);
static const int16_t g_3502 = 0L;
static uint16_t * const *g_3533 = &g_1029;
static uint16_t * const **g_3532 = &g_3533;
static volatile uint32_t ****g_3548[8] = {&g_2555,&g_2555,&g_2555,&g_2555,&g_2555,&g_2555,&g_2555,&g_2555};
static volatile uint32_t ***** volatile g_3547 = &g_3548[2];/* VOLATILE GLOBAL g_3547 */
static int32_t *g_3567 = &g_28;
static int32_t ** volatile g_3573[4] = {&g_2511,&g_2511,&g_2511,&g_2511};


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static int32_t  func_2(uint16_t  p_3, uint64_t  p_4, uint32_t  p_5, int8_t  p_6);
static uint16_t  func_7(uint8_t  p_8);
static uint32_t  func_13(int32_t  p_14, const int16_t  p_15, int32_t  p_16, int64_t  p_17, int64_t  p_18);
static uint8_t  func_22(float  p_23);
static int32_t * func_33(int32_t * p_34, float  p_35, int32_t * p_36);
static int32_t * func_37(int32_t  p_38, const int16_t  p_39, int32_t  p_40, int32_t * p_41, uint16_t  p_42);
static uint8_t  func_49(uint32_t  p_50, int32_t  p_51, float  p_52, const int32_t * p_53, int32_t  p_54);
static uint8_t  func_62(int32_t * p_63, uint64_t  p_64, const int32_t  p_65);
static const int32_t  func_70(int32_t * p_71, int32_t * p_72);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_19 g_28 g_32 g_2829 g_89 g_1544 g_539 g_540 g_246 g_1515 g_809 g_202 g_726 g_311 g_1798 g_1799 g_701 g_1350 g_166 g_1362 g_398 g_1797 g_542 g_3077 g_2220 g_1771 g_129 g_130 g_48 g_2519 g_415 g_2959 g_2960 g_1913 g_1914 g_2636 g_2637 g_999 g_287 g_111 g_1497 g_68 g_2382 g_3123 g_724 g_725 g_1029 g_698 g_418 g_419 g_1744 g_3156 g_2692 g_2606 g_466 g_2691 g_3238 g_3253 g_3280 g_103 g_3323 g_998 g_1769 g_1770 g_599 g_3124 g_3125 g_3126 g_3379 g_3398 g_522 g_2331 g_2332 g_3433 g_3254 g_3462 g_458 g_3502 g_525 g_232 g_3532 g_1103 g_1104 g_1432 g_3547 g_1543
 * writes: g_28 g_32 g_2829 g_89 g_166 g_692 g_809 g_1687 g_466 g_1607 g_1951 g_673 g_1350 g_569 g_1362 g_1029 g_246 g_68 g_48 g_3086 g_3123 g_111 g_3156 g_19 g_202 g_2511 g_2382 g_3253 g_3280 g_103 g_3323 g_599 g_403 g_398 g_2920 g_232 g_3532 g_458 g_3548 g_311 g_3567
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int16_t l_24 = 9L;
    int8_t *l_3155 = &g_3156;
    uint32_t l_3551 = 0xADDAE00AL;
    uint16_t l_3575 = 7UL;
    int32_t l_3576 = 0x170188FDL;
    l_3575 = func_2(func_7(((((safe_div_func_int32_t_s_s(0x60D00585L, (safe_lshift_func_int8_t_s_u((func_13(g_19, ((6L != 0xE294L) < (0x23E93D0B5486B6FELL | (safe_lshift_func_int16_t_s_s(((func_22(l_24) <= (((safe_add_func_int8_t_s_s(l_24, ((*l_3155) &= (4UL && (*g_1744))))) , (*g_2692)) == (*g_726))) & 0xC35F2F4F306FE2BBLL), 12)))), l_24, (*g_415), l_24) >= l_3551), l_3551)))) ^ 0xB72EL) == l_3551) | (*g_2692))), l_3551, l_24, l_24);
    l_3576 ^= (l_3575 , l_3551);
    return (***g_724);
}


/* ------------------------------------------ */
/* 
 * reads : g_1543 g_1544 g_539 g_540 g_246 g_1515 g_89 g_809 g_202 g_726 g_19 g_311 g_1798 g_1799 g_701 g_28 g_1350 g_166 g_1362 g_398 g_1797 g_542
 * writes: g_692 g_809 g_1687 g_466 g_1607 g_1951 g_673 g_1350 g_569 g_1362 g_89 g_1029 g_246 g_68
 */
static int32_t  func_2(uint16_t  p_3, uint64_t  p_4, uint32_t  p_5, int8_t  p_6)
{ /* block id: 1604 */
    int64_t l_3568 = 0L;
    int32_t *l_3569 = &g_89;
    uint32_t l_3570 = 0xAF7EE03EL;
    int32_t **l_3574 = &l_3569;
    (*l_3574) = func_37(p_3, (****g_1543), l_3568, ((l_3568 > 0L) , l_3569), (++l_3570));
    return p_5;
}


/* ------------------------------------------ */
/* 
 * reads : g_1544 g_539 g_540 g_287 g_2692 g_2606 g_3125 g_3126 g_415 g_311 g_1744 g_202
 * writes: g_246 g_111 g_692 g_403 g_398 g_311 g_3567
 */
static uint16_t  func_7(uint8_t  p_8)
{ /* block id: 1595 */
    int32_t l_3562 = 8L;
    int16_t *l_3563 = &g_692;
    int64_t l_3564 = 1L;
    uint32_t *l_3565[1][5][5] = {{{(void*)0,(void*)0,&g_1362,&g_1362,&g_1362},{&g_1362,&g_1362,&g_1362,&g_1362,&g_1362},{&g_1362,(void*)0,&g_1362,(void*)0,&g_1362},{&g_1362,&g_1362,&g_1362,&g_1362,&g_1362},{&g_1362,(void*)0,(void*)0,&g_1362,&g_1362}}};
    int8_t *l_3566 = &g_311[2][2][2];
    int i, j, k;
    g_3567 = (((safe_mul_func_int16_t_s_s(p_8, (safe_mul_func_int16_t_s_s((safe_mod_func_int64_t_s_s(0x80954D874729A0ADLL, 0x52A28E76BB55FC14LL)), 0xD820L)))) < ((((((((*l_3566) |= ((safe_add_func_uint64_t_u_u(0x7444703DA7CEB2E3LL, ((***g_3125) = (p_8 == ((((g_403 = ((((***g_1544) = p_8) ^ ((*l_3563) = ((((*g_287) = (-0x1.Ap-1)) == ((safe_add_func_float_f_f((l_3562 > l_3562), l_3562)) >= p_8)) , (*g_2692)))) | l_3564)) == 0xB5442D1CL) >= (-2L)) != 0x356884E7624E27D3LL))))) , p_8)) && 0x20L) , p_8) == (-5L)) && (-5L)) != (*g_1744)) == l_3564)) , &l_3562);
    return l_3564;
}


/* ------------------------------------------ */
/* 
 * reads : g_19 g_466 g_1744 g_202 g_28 g_2220 g_1771 g_129 g_130 g_32 g_2691 g_2692 g_2606 g_48 g_1515 g_89 g_809 g_1544 g_539 g_726 g_311 g_1798 g_1799 g_701 g_1350 g_166 g_1362 g_398 g_1797 g_540 g_542 g_3238 g_999 g_287 g_2382 g_3253 g_3280 g_103 g_3323 g_998 g_111 g_1769 g_1770 g_599 g_3124 g_3125 g_3126 g_415 g_2636 g_2637 g_246 g_3379 g_3398 g_522 g_2331 g_2332 g_3433 g_3254 g_3462 g_724 g_725 g_458 g_3502 g_525 g_232 g_3532 g_1103 g_1104 g_1432 g_3547
 * writes: g_19 g_466 g_28 g_202 g_48 g_32 g_692 g_809 g_1687 g_1607 g_1951 g_673 g_1350 g_569 g_1362 g_89 g_1029 g_246 g_68 g_2511 g_111 g_2382 g_3253 g_3280 g_103 g_3323 g_599 g_403 g_398 g_2920 g_232 g_3532 g_458 g_3548
 */
static uint32_t  func_13(int32_t  p_14, const int16_t  p_15, int32_t  p_16, int64_t  p_17, int64_t  p_18)
{ /* block id: 1428 */
    int32_t *l_3160 = &g_2382;
    int32_t **l_3159 = &l_3160;
    int32_t l_3161 = (-7L);
    int32_t l_3162 = 7L;
    uint16_t *l_3163 = (void*)0;
    uint16_t *l_3164 = &g_19;
    int32_t l_3184 = 5L;
    int32_t l_3185 = 1L;
    int32_t l_3186 = 1L;
    int32_t l_3187[7] = {0x77980286L,0x77980286L,0x77980286L,0x77980286L,0x77980286L,0x77980286L,0x77980286L};
    int8_t l_3191 = 0L;
    int64_t l_3192 = (-10L);
    int8_t *l_3212 = &g_1350;
    int8_t **l_3211 = &l_3212;
    int8_t l_3297[8][8][4] = {{{0xC2L,3L,0L,0x8FL},{0x42L,1L,1L,(-9L)},{0x49L,0x78L,3L,0x57L},{3L,0x57L,(-9L),0xEFL},{0x7FL,0x3AL,4L,0x49L},{0xFEL,0L,(-10L),2L},{0x71L,(-10L),2L,0x3AL},{3L,0x21L,0x30L,7L}},{{0L,0x7FL,0x78L,(-1L)},{0xD0L,0L,0x1CL,(-1L)},{(-1L),0x23L,0L,0x71L},{1L,0xC1L,(-4L),0x38L},{0x2DL,0x65L,0x65L,0x2DL},{5L,0x1CL,0L,4L},{(-4L),0xD8L,0L,0x7FL},{0xF3L,0x38L,1L,0x7FL}},{{0x17L,0xD8L,0x21L,4L},{2L,0x1CL,0x49L,0x2DL},{1L,0x65L,0xC4L,0x38L},{5L,0xC1L,(-8L),0x71L},{0xE2L,0x23L,(-10L),(-1L)},{0x78L,0L,0x81L,(-1L)},{(-10L),0x7FL,0x54L,7L},{1L,0x21L,0L,0x3AL}},{{0x3AL,(-10L),1L,2L},{(-9L),0L,0x8FL,0x49L},{1L,0x3AL,0xE2L,0xEFL},{(-1L),0x57L,0x3AL,0x57L},{0L,0x78L,0x2DL,(-9L)},{(-10L),1L,1L,0x8FL},{(-1L),3L,4L,0x17L},{(-1L),0xC4L,1L,0xC1L}},{{(-10L),0x17L,0x2DL,(-8L)},{0L,5L,0x3AL,0x23L},{(-1L),4L,0xE2L,0L},{1L,0x42L,0x8FL,3L},{(-9L),0xF3L,1L,0x8DL},{0x3AL,0xDFL,0L,0xE2L},{1L,1L,0x54L,1L},{0xF3L,0xC4L,4L,0L}},{{1L,0x38L,0xF3L,1L},{1L,4L,0L,0x1CL},{1L,(-1L),0xFEL,0xFEL},{0x49L,0x49L,0x57L,0xD0L},{0L,0xC2L,0x7FL,(-8L)},{(-1L),0x30L,2L,0x7FL},{0x3AL,0x30L,(-9L),(-8L)},{0x30L,0xC2L,(-10L),0xD0L}},{{0x1CL,0x49L,0x2DL,0xFEL},{0L,(-1L),0x30L,0x1CL},{2L,4L,0x65L,1L},{0xDFL,0x38L,(-8L),0L},{1L,0xC4L,1L,0x49L},{3L,1L,0xC1L,1L},{0xE2L,3L,0L,0x71L},{0x54L,0x3AL,4L,1L}},{{(-1L),5L,1L,(-9L)},{0x42L,1L,0xC2L,0x81L},{1L,0x1CL,1L,0L},{0x57L,(-1L),0x8DL,0xD8L},{5L,0xFEL,3L,(-1L)},{(-1L),1L,3L,0x38L},{5L,0xEFL,0x8DL,0xC2L},{0x57L,1L,1L,0x3AL}}};
    int64_t l_3298 = 0L;
    uint16_t ***l_3328[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    const int16_t **l_3365 = &g_2692;
    float l_3391 = 0x0.8D5835p-34;
    int32_t ***l_3403 = &g_1290;
    uint64_t **l_3405 = &g_1744;
    float **l_3460 = &g_287;
    int i, j, k;
    if ((((safe_mul_func_int16_t_s_s((l_3161 = (((*l_3159) = &g_2382) != &g_2382)), ((*l_3164)++))) <= (((void*)0 == l_3164) || (l_3162 = l_3162))) < 0x1BL))
    { /* block id: 1433 */
        return p_18;
    }
    else
    { /* block id: 1435 */
        int32_t *l_3170 = &g_28;
        int32_t l_3182[1][6][3] = {{{0x4E467A5DL,0x5C7B2F73L,0x4E467A5DL},{0xA1088D44L,0xA1088D44L,0x176F4AFFL},{0x5083DF10L,0x5C7B2F73L,0x5083DF10L},{0xA1088D44L,0x176F4AFFL,0x176F4AFFL},{0x4E467A5DL,0x5C7B2F73L,0x4E467A5DL},{0xA1088D44L,0xA1088D44L,0x176F4AFFL}}};
        int32_t l_3183 = (-8L);
        uint64_t l_3188 = 0xD05AB52D9747493CLL;
        int16_t **l_3197 = &g_3086;
        int16_t ***l_3196[8] = {&l_3197,&l_3197,&l_3197,&l_3197,&l_3197,&l_3197,&l_3197,&l_3197};
        const int8_t *l_3210 = (void*)0;
        const int8_t **l_3209 = &l_3210;
        int64_t *** const *l_3235 = &g_3125;
        int64_t *** const **l_3234 = &l_3235;
        uint32_t l_3293 = 0x78B3DE19L;
        int i, j, k;
        for (g_466 = 0; (g_466 >= 56); g_466 = safe_add_func_int16_t_s_s(g_466, 1))
        { /* block id: 1438 */
            int32_t *l_3169 = &l_3162;
            l_3170 = l_3169;
        }
        (*l_3170) = p_14;
        if ((safe_add_func_uint16_t_u_u(((++(*g_1744)) != p_18), (safe_lshift_func_uint16_t_u_u(p_17, 2)))))
        { /* block id: 1443 */
            int16_t l_3177 = 0xE3AAL;
            int32_t *l_3178 = (void*)0;
            int32_t *l_3179 = &g_32;
            int32_t *l_3180 = &g_28;
            int32_t *l_3181[2];
            uint32_t l_3193 = 0xB7F57219L;
            int i;
            for (i = 0; i < 2; i++)
                l_3181[i] = &g_809;
            ++l_3188;
            (*l_3170) ^= p_16;
            l_3193++;
            (*l_3179) &= ((((*l_3170) > (((l_3196[5] == &l_3197) & (safe_add_func_int16_t_s_s(0L, (((((****g_2220) = (*l_3170)) > 9UL) ^ (safe_sub_func_uint32_t_u_u(4294967294UL, (safe_rshift_func_uint8_t_u_u((((+((((safe_sub_func_uint8_t_u_u((((((safe_mul_func_uint16_t_u_u(0xF6A9L, 0xCD1DL)) | 65530UL) , 0x12CC77E9L) , p_15) <= 6L), p_14)) , l_3209) == l_3211) || p_18)) == (*l_3170)) && p_16), 4))))) != 18446744073709551610UL)))) , (-1L))) <= p_18) <= (-7L));
        }
        else
        { /* block id: 1449 */
            uint32_t *l_3225 = &g_403;
            int32_t l_3233 = 8L;
            int32_t l_3236 = 0L;
            int32_t l_3276 = 0x2238864BL;
            int32_t l_3277 = 6L;
            int32_t l_3278[3];
            int32_t l_3279 = 0x79E624D4L;
            int32_t l_3292[8] = {(-9L),(-9L),0x0B1625B4L,(-9L),(-9L),0x0B1625B4L,(-9L),(-9L)};
            int i;
            for (i = 0; i < 3; i++)
                l_3278[i] = 0x902FB455L;
            (*g_3238) = func_37(l_3186, (*l_3170), (safe_rshift_func_uint16_t_u_u(((*l_3170) | (safe_div_func_uint16_t_u_u((((*g_1744) &= l_3191) && l_3185), (safe_mul_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(((((((((((safe_rshift_func_uint8_t_u_s(((l_3225 != (void*)0) == (((~(safe_lshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u((l_3233 = (((safe_div_func_int16_t_s_s(((0xD6L || 0x10L) == p_15), l_3233)) < l_3233) , 4UL)), p_14)), (*l_3170)))) >= 0x0701L) > p_16)), 6)) == (**g_2691)) > 0UL) != 0x68738CFDL) , (-8L)) | p_15) >= l_3162) , l_3234) == &g_1819) == p_17), l_3185)) , (****g_2220)), p_14))))), 11)), l_3225, l_3236);
            (**g_999) = (l_3236 >= (safe_sub_func_float_f_f((safe_mul_func_float_f_f((safe_mul_func_float_f_f((0x0.Bp+1 < p_17), (((((safe_div_func_int32_t_s_s(0x046B86BEL, (safe_rshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_s(0x62L, ((**l_3211) |= (6UL || ((((*l_3164) = 0x81F3L) < (p_17 , (*l_3170))) , p_17))))), p_14)))) <= p_14) , l_3233) == p_15) > 0x4.4FA22Ap+56))), l_3233)), (*l_3170))));
            for (g_2382 = 0; (g_2382 <= 18); g_2382++)
            { /* block id: 1458 */
                const uint32_t ***l_3255 = &g_3253;
                int32_t l_3266 = 0xA2902ECAL;
                uint32_t **l_3269 = &l_3225;
                uint32_t ***l_3270 = &l_3269;
                int32_t l_3274 = 0L;
                int32_t l_3275[8][8] = {{(-1L),0x7FD9AF6CL,0L,(-1L),0xEE3C912BL,0xEE3C912BL,(-1L),0L},{1L,1L,0x362928ABL,0x4B8F6371L,3L,0x7FD9AF6CL,(-1L),0x42EEECBBL},{0x6F903761L,0xE06983A7L,0xEE3C912BL,0x7FD9AF6CL,0xBC677CE1L,(-7L),(-1L),0x42EEECBBL},{0xE06983A7L,0L,(-1L),0x4B8F6371L,(-1L),0x4B8F6371L,(-1L),0L},{(-1L),0x362928ABL,(-1L),(-1L),3L,(-1L),0L,(-7L)},{(-1L),0xEE3C912BL,0x42EEECBBL,(-1L),(-1L),0xB8C8360EL,0L,3L},{(-6L),(-1L),(-1L),0x7FD9AF6CL,0x7FD9AF6CL,(-1L),(-1L),(-1L)},{0x7FD9AF6CL,(-1L),(-1L),(-1L),3L,(-7L),0x13DD4148L,(-1L)}};
                int16_t l_3299 = 5L;
                uint32_t l_3300 = 8UL;
                uint8_t *** const l_3305 = &g_129[7];
                int i, j;
                if ((((*l_3255) = g_3253) == ((*l_3270) = ((safe_unary_minus_func_uint64_t_u(((**g_1797) == l_3160))) , ((((p_14 , 0x9.250A92p+7) < (safe_sub_func_float_f_f(((safe_rshift_func_int16_t_s_u((!p_16), (safe_lshift_func_uint16_t_u_s(((safe_lshift_func_int16_t_s_u(l_3266, (safe_add_func_uint16_t_u_u(p_17, (((****g_2220) < l_3233) > l_3266))))) <= (*g_1744)), l_3186)))) , p_14), l_3187[1]))) >= p_14) , l_3269)))))
                { /* block id: 1461 */
                    int32_t *l_3271 = (void*)0;
                    int32_t *l_3272 = &l_3266;
                    int32_t *l_3273[2][6] = {{(void*)0,(void*)0,&g_32,(void*)0,(void*)0,&g_32},{(void*)0,(void*)0,&g_32,(void*)0,(void*)0,&g_32}};
                    int i, j;
                    g_3280--;
                    for (l_3266 = (-12); (l_3266 > 17); l_3266 = safe_add_func_int16_t_s_s(l_3266, 9))
                    { /* block id: 1465 */
                        if (p_18)
                            break;
                    }
                    if (p_15)
                        break;
                }
                else
                { /* block id: 1469 */
                    int32_t *l_3285 = &g_89;
                    int32_t *l_3286 = &l_3274;
                    int32_t *l_3287 = &l_3275[0][1];
                    int32_t *l_3288 = (void*)0;
                    int32_t *l_3289 = &l_3182[0][1][2];
                    int32_t *l_3290 = &l_3276;
                    int32_t *l_3291[7];
                    int i;
                    for (i = 0; i < 7; i++)
                        l_3291[i] = &l_3184;
                    l_3293++;
                }
                for (g_1350 = 0; (g_1350 <= 3); g_1350 += 1)
                { /* block id: 1474 */
                    int32_t *l_3296[6] = {&l_3292[3],&l_3292[3],&l_3292[3],&l_3292[3],&l_3292[3],&l_3292[3]};
                    int i;
                    l_3300++;
                    l_3275[4][2] = (safe_sub_func_float_f_f((g_111[(g_1350 + 3)] = ((void*)0 == l_3305)), (*l_3170)));
                }
                for (g_103 = 0; (g_103 == 40); g_103 = safe_add_func_uint64_t_u_u(g_103, 3))
                { /* block id: 1481 */
                    uint32_t l_3308 = 0x76F44CADL;
                    return l_3308;
                }
            }
        }
        l_3170 = (void*)0;
    }
    for (g_48 = 9; (g_48 < 59); g_48++)
    { /* block id: 1490 */
        int32_t *l_3311 = &l_3186;
        int32_t *l_3312 = &l_3162;
        int32_t *l_3313 = &l_3161;
        int32_t *l_3314 = &g_28;
        int32_t l_3315 = 0x8A72BE4EL;
        int32_t l_3316 = 0L;
        int32_t *l_3317 = &l_3316;
        int32_t *l_3318 = &l_3316;
        int32_t *l_3319 = &g_89;
        int32_t *l_3320 = &g_28;
        int32_t l_3321 = 7L;
        int32_t *l_3322[5];
        uint8_t ****l_3339 = &g_1771[3];
        uint64_t *l_3340 = &g_599;
        int64_t **l_3368[9];
        uint16_t l_3429 = 0x7AADL;
        int32_t *l_3503 = &g_809;
        uint64_t *** const l_3512 = (void*)0;
        int32_t *l_3517 = &l_3187[6];
        uint16_t ** const *l_3531 = &g_1435[1][0][1];
        int i;
        for (i = 0; i < 5; i++)
            l_3322[i] = &g_32;
        for (i = 0; i < 9; i++)
            l_3368[i] = &g_415;
        g_3323++;
        (*l_3313) &= ((safe_lshift_func_uint16_t_u_u(l_3192, ((*l_3312) && (((1UL != (((**l_3211) |= ((((void*)0 != l_3328[1]) && (*g_1744)) >= l_3187[1])) & (safe_mod_func_int8_t_s_s(p_18, ((((*l_3319) & p_17) , (***g_998)) , p_14))))) < 4294967295UL) , 0L)))) , (*l_3311));
        if ((((safe_mul_func_int8_t_s_s((*l_3318), (65534UL < (safe_div_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(((((*l_3340) ^= ((*g_1744) ^= (safe_rshift_func_int16_t_s_u((p_17 & ((*g_1769) == l_3339)), 11)))) && (7L & (((l_3191 | (safe_mul_func_int16_t_s_s((safe_div_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u(((*l_3313) != 1UL), (*l_3313))), l_3162)), (**g_2691)))) , 0x336FC1D6796ABD07LL) | p_17))) > (*l_3320)), 1L)), (*l_3320)))))) ^ (****g_3124)) <= l_3162))
        { /* block id: 1496 */
            return p_18;
        }
        else
        { /* block id: 1498 */
            int32_t l_3353 = 7L;
            uint64_t **l_3364[5][6] = {{&l_3340,(void*)0,&l_3340,&g_1744,&g_1744,&g_1744},{(void*)0,&l_3340,&l_3340,(void*)0,&l_3340,&g_1744},{&l_3340,&g_1744,&l_3340,&l_3340,&g_1744,&l_3340},{&l_3340,&l_3340,&l_3340,(void*)0,&g_1744,&l_3340},{&l_3340,&g_1744,&l_3340,&l_3340,&l_3340,&l_3340}};
            int16_t *l_3393 = &g_1497[2][0][1];
            const uint64_t l_3394 = 0UL;
            int32_t *l_3395 = &g_809;
            float l_3412 = 0x7.0A0C3Dp-64;
            int32_t l_3413 = 0x2D8AD304L;
            int32_t l_3536 = 0x8CCF6CEAL;
            int i, j;
            for (g_692 = (-15); (g_692 > (-30)); --g_692)
            { /* block id: 1501 */
                uint64_t * const *l_3375 = &g_1744;
                int32_t l_3376 = 8L;
                uint32_t ***l_3382 = &g_2931;
                uint32_t **** const l_3381 = &l_3382;
                uint32_t **** const *l_3380 = &l_3381;
                for (g_28 = (-5); (g_28 > 11); g_28 = safe_add_func_int64_t_s_s(g_28, 1))
                { /* block id: 1504 */
                    uint32_t *l_3363 = &g_403;
                    (****g_2636) = (*l_3313);
                    for (l_3315 = (-7); (l_3315 == 8); l_3315 = safe_add_func_uint32_t_u_u(l_3315, 2))
                    { /* block id: 1508 */
                        uint16_t l_3354 = 0x2D84L;
                        --l_3354;
                    }
                    p_16 = (((safe_div_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u((((*g_1544) != (((safe_rshift_func_int16_t_s_s((((*l_3363) = (0x38L < (-1L))) == (-8L)), (((p_17 || (-2L)) & (l_3364[2][0] != (l_3353 , (void*)0))) , p_15))) && (****g_3124)) , l_3365)) ^ 0xCDB468EFL), 15)) & 0xA5L), p_17)) <= p_16) , p_16);
                }
                for (l_3298 = 1; (l_3298 >= 0); l_3298 -= 1)
                { /* block id: 1516 */
                    int i;
                    if (p_18)
                        break;
                }
                if (((l_3187[1] = 0xFCL) ^ (((((safe_sub_func_uint32_t_u_u(((p_15 || ((((((1UL < (l_3368[7] == (void*)0)) != ((((p_17 <= (safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_s(((safe_sub_func_int8_t_s_s(((**l_3211) = p_15), ((((void*)0 != l_3375) & l_3376) >= (*l_3320)))) , (*l_3313)), 2)), p_15))) != 1UL) > p_17) >= (*g_1799))) || p_17) || l_3353) == l_3353) | l_3184)) != 0x657D9DC23BB7A55CLL), 3UL)) , 0x4AA8L) , (void*)0) == &g_3124) < 255UL)))
                { /* block id: 1521 */
                    int32_t l_3392 = 0x8636D809L;
                    (*l_3313) &= ((***g_1544) && ((((****g_3124) = (p_16 | 0xCBF82E4AL)) ^ 0x6C3689CF8E2BA5E5LL) == (safe_add_func_int16_t_s_s((((((l_3380 = g_3379) != &g_2405) < ((safe_sub_func_int32_t_s_s(p_16, (safe_mod_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_s(((***g_1544) = (p_18 > l_3392)), 11)), 1)), p_15)))) | 0x9B4BA76DL)) , l_3393) == (*g_2691)), l_3394))));
                    if (p_14)
                        break;
                }
                else
                { /* block id: 1527 */
                    for (g_89 = 2; (g_89 >= 0); g_89 -= 1)
                    { /* block id: 1530 */
                        int32_t l_3404 = 0L;
                        if (l_3353)
                            break;
                        l_3187[6] &= ((void*)0 != &g_2577);
                        l_3395 = &p_16;
                        (*l_3317) |= ((*l_3311) = (((((*g_1744) = ((safe_div_func_int16_t_s_s((((p_15 >= p_14) < p_17) ^ g_3398), (l_3376 | (p_16 , (safe_mul_func_uint16_t_u_u(0x96E6L, 0UL)))))) <= ((safe_add_func_uint64_t_u_u(((void*)0 != l_3403), l_3404)) , g_522[1][1][0]))) <= 0x1D52E7168531174ELL) == p_15) == p_15));
                    }
                }
            }
            if ((0x658A976BL < (((p_17 = p_17) <= ((l_3405 == (*g_2331)) >= (safe_sub_func_uint16_t_u_u((0xB7L != (safe_add_func_uint64_t_u_u((1UL && ((safe_add_func_uint64_t_u_u((*l_3395), (((((((*l_3395) , (**g_2691)) && 1L) && 2L) , p_18) <= p_14) && (*l_3395)))) ^ p_15)), l_3413))), (*l_3313))))) , 2L)))
            { /* block id: 1541 */
                uint8_t l_3431 = 255UL;
                int32_t l_3459 = 0xE94B6391L;
                float **l_3461 = &g_287;
                int8_t l_3463 = 0L;
                int32_t l_3464 = 0L;
                for (l_3298 = (-6); (l_3298 == (-5)); l_3298 = safe_add_func_uint32_t_u_u(l_3298, 2))
                { /* block id: 1544 */
                    int32_t l_3430 = 0L;
                    int32_t l_3432 = 0xC18E34B8L;
                    l_3432 &= (18446744073709551610UL <= (safe_mul_func_uint16_t_u_u(((((!((***g_3125) | (safe_mod_func_int8_t_s_s(p_17, (((((safe_mod_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s((-1L), (safe_div_func_uint64_t_u_u((((((**g_539) = 0x6404L) & (safe_add_func_int64_t_s_s(((*l_3395) = (l_3429 < ((**g_2220) != (void*)0))), ((*l_3312) && p_17)))) >= 2L) , (*l_3314)), l_3430)))), l_3191)) <= p_15) >= p_17) && p_14) , 255UL))))) <= 0x16C6L) != p_18) > 0xCCL), l_3431)));
                }
                (*l_3319) &= (g_3433 && ((safe_mod_func_int16_t_s_s((l_3464 = (p_14 == (((((((((safe_sub_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u(((((safe_unary_minus_func_uint64_t_u(((safe_sub_func_float_f_f((safe_mul_func_float_f_f(0x8.9C344Dp-99, (safe_add_func_float_f_f(0x2.0p-1, (((((((((safe_add_func_uint32_t_u_u(((safe_lshift_func_uint16_t_u_s((safe_mul_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u(((!4294967295UL) == (((safe_unary_minus_func_int64_t_s(0xDD792D9BB4CCD15DLL)) || (l_3459 = (0UL || (safe_div_func_int16_t_s_s(((((0xC669E9CBL <= (***g_1797)) >= p_16) >= 0x57L) == (**g_3126)), 65535UL))))) , p_17)), p_18)), 1UL)), 8)) , p_15), l_3431)) , p_18) < 0x61L) & 0xC06AL) ^ (**g_3253)) || (**g_3126)) , l_3460) != l_3461) != (*l_3395)))))), p_16)) , 0x214836AC5FEDDE4ALL))) , g_3462) <= (**g_3253)) , (***g_724)), (**g_725))), l_3463)) & g_458) >= 1L) > (*l_3395)) == l_3463) <= p_18) > p_15) , (*g_1799)) | 0x244AFEA3L))), p_14)) || 0x92L));
            }
            else
            { /* block id: 1552 */
                int32_t **l_3469 = &l_3322[3];
                int16_t l_3515[7][7] = {{0x9CA8L,0L,0x9CA8L,0L,0x9CA8L,0L,0x9CA8L},{(-2L),1L,1L,(-2L),(-2L),1L,1L},{0xAAA7L,0L,0xAAA7L,0L,0xAAA7L,0L,0xAAA7L},{(-2L),(-2L),1L,1L,(-2L),(-2L),1L},{0x9CA8L,0L,0x9CA8L,0L,0x9CA8L,0L,0x9CA8L},{(-2L),1L,1L,(-2L),(-2L),1L,1L},{0xAAA7L,0L,0xAAA7L,0L,0xAAA7L,0L,0xAAA7L}};
                int i, j;
                (*l_3311) ^= (safe_mod_func_uint16_t_u_u((safe_sub_func_int8_t_s_s(((*l_3212) = (*l_3395)), 0xF8L)), (-2L)));
                (*l_3469) = &p_16;
                for (g_673 = 0; (g_673 == 25); ++g_673)
                { /* block id: 1558 */
                    float l_3477 = 0x0.4p-1;
                    int32_t l_3486 = (-1L);
                    float *l_3499 = (void*)0;
                    float *l_3500 = &g_2920[6][1][3];
                    int32_t l_3501 = 1L;
                    int16_t **l_3513 = &g_3086;
                    uint8_t l_3514[8][4] = {{0xA4L,255UL,255UL,0xA4L},{255UL,0xA4L,255UL,255UL},{0xA4L,0xA4L,0UL,0xA4L},{0xA4L,255UL,255UL,0xA4L},{255UL,0xA4L,255UL,255UL},{0xA4L,0xA4L,0UL,0xA4L},{0xA4L,255UL,255UL,0xA4L},{255UL,0xA4L,255UL,255UL}};
                    int i, j;
                    (*l_3314) = ((((safe_add_func_int64_t_s_s((~(safe_sub_func_uint16_t_u_u(p_17, (safe_rshift_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(p_17, p_15)), 4))))), (((safe_mul_func_uint16_t_u_u((safe_add_func_uint64_t_u_u(l_3486, ((*g_1744) &= ((((((((safe_sub_func_int8_t_s_s((((safe_add_func_float_f_f((((****g_2636) = ((*g_287) == 0xD.CDFF00p-65)) <= l_3486), (safe_div_func_float_f_f(l_3486, ((safe_div_func_float_f_f(((l_3501 = (safe_add_func_float_f_f(((((*l_3500) = (safe_sub_func_float_f_f(((0x8.0p+1 != (*l_3395)) > (**l_3469)), (*l_3317)))) != p_15) <= p_18), (*l_3311)))) != l_3486), l_3486)) == 0x5.5p-1))))) , 4UL) <= 0x8761L), p_15)) , (**l_3469)) <= l_3486) | p_18) >= l_3486) != 0UL) >= 1L) < 4294967293UL)))), (*g_540))) <= g_3502) , p_16))) , 0x1FL) && (****g_2220)) , (*l_3395));
                    l_3503 = (*l_3469);
                    (*g_287) = (safe_add_func_float_f_f((safe_div_func_float_f_f(0x8.0852CDp+82, ((p_17 , (((safe_rshift_func_int16_t_s_u(0x7693L, 13)) != (((safe_div_func_int64_t_s_s((((p_18 <= l_3501) , (*g_1544)) != ((((((void*)0 != l_3512) ^ (p_17 == p_14)) <= p_14) | (**g_2691)) , l_3513)), p_17)) ^ p_16) == l_3514[3][3])) , p_16)) , l_3515[6][4]))), p_18));
                    (**l_3460) = (-p_14);
                }
                if ((**l_3469))
                    continue;
            }
            l_3395 = &p_16;
            if (p_18)
            { /* block id: 1571 */
                return (**g_3253);
            }
            else
            { /* block id: 1573 */
                int32_t l_3526[10];
                int32_t l_3546 = 0x07004314L;
                int i;
                for (i = 0; i < 10; i++)
                    l_3526[i] = 0x632936D0L;
                (*g_525) = l_3517;
                for (g_232 = (-2); (g_232 < 17); g_232++)
                { /* block id: 1577 */
                    uint16_t * const ***l_3534 = &g_3532;
                    const int32_t l_3535 = 0x08C86311L;
                    int64_t **l_3537 = &g_415;
                    if ((safe_add_func_uint8_t_u_u((safe_mod_func_int8_t_s_s(((((safe_mul_func_int16_t_s_s(((l_3526[4] && (safe_add_func_int32_t_s_s(((((*l_3395) = (((((**l_3211) = (p_14 & (p_16 <= ((((251UL | ((((((((((l_3531 == (void*)0) > 0xAFED3A1BL) && (((*l_3534) = g_3532) != (*g_1103))) <= (**g_725)) & p_16) , (void*)0) != l_3368[8]) == 1L) , 0x60L) < p_15)) < l_3526[0]) >= 0xB0L) | (*l_3395))))) & l_3535) ^ (*l_3517)) , p_16)) , 0x755A253A1FCAD699LL) | p_14), 0L))) && 0x4441BAB4CFB72704LL), l_3536)) , (**g_3126)) >= (*g_1744)) > 2UL), g_1432)), p_18)))
                    { /* block id: 1581 */
                        return p_14;
                    }
                    else
                    { /* block id: 1583 */
                        int64_t **l_3538[6][4][9] = {{{&g_415,(void*)0,&g_415,(void*)0,(void*)0,(void*)0,&g_415,(void*)0,&g_415},{&g_415,&g_415,&g_415,(void*)0,&g_415,(void*)0,&g_415,(void*)0,&g_415},{&g_415,&g_415,(void*)0,(void*)0,(void*)0,&g_415,&g_415,&g_415,(void*)0},{&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415}},{{&g_415,(void*)0,(void*)0,(void*)0,(void*)0,&g_415,(void*)0,&g_415,(void*)0},{&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415},{&g_415,&g_415,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_415},{&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415}},{{(void*)0,(void*)0,(void*)0,&g_415,(void*)0,&g_415,(void*)0,(void*)0,&g_415},{&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415},{(void*)0,&g_415,&g_415,&g_415,&g_415,&g_415,(void*)0,(void*)0,(void*)0},{&g_415,(void*)0,(void*)0,&g_415,&g_415,&g_415,(void*)0,(void*)0,&g_415}},{{&g_415,(void*)0,(void*)0,(void*)0,&g_415,&g_415,&g_415,(void*)0,(void*)0},{&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415},{&g_415,&g_415,&g_415,&g_415,(void*)0,(void*)0,&g_415,(void*)0,&g_415},{&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_415,&g_415,(void*)0,&g_415,&g_415,(void*)0,&g_415,&g_415,&g_415},{(void*)0,(void*)0,&g_415,(void*)0,&g_415,&g_415,&g_415,&g_415,(void*)0},{&g_415,(void*)0,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415}},{{&g_415,(void*)0,(void*)0,(void*)0,&g_415,(void*)0,&g_415,&g_415,&g_415},{(void*)0,&g_415,&g_415,&g_415,&g_415,&g_415,(void*)0,&g_415,&g_415},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415,&g_415}}};
                        int32_t l_3541 = 0x6410D605L;
                        uint8_t *l_3542 = (void*)0;
                        uint8_t *l_3543 = &g_458;
                        int32_t *l_3549 = &l_3413;
                        int16_t l_3550 = 0x06B3L;
                        int i, j, k;
                        (*g_3547) = (((p_18 >= (l_3546 = ((l_3537 == l_3538[1][2][3]) > (safe_sub_func_uint8_t_u_u(((*l_3543)++), p_17))))) >= 0xFC7E25252442125DLL) , &g_2555);
                        l_3549 = &p_16;
                        if (l_3550)
                            continue;
                    }
                }
            }
        }
    }
    return (*g_3254);
}


/* ------------------------------------------ */
/* 
 * reads : g_28 g_19 g_32 g_2829 g_89 g_1544 g_539 g_540 g_246 g_1515 g_809 g_202 g_726 g_311 g_1798 g_1799 g_701 g_1350 g_166 g_1362 g_398 g_1797 g_542 g_3077 g_2220 g_1771 g_129 g_130 g_48 g_2519 g_415 g_2959 g_2960 g_1913 g_1914 g_2636 g_2637 g_999 g_287 g_111 g_1497 g_68 g_2382 g_3123 g_724 g_725 g_1029 g_698 g_418 g_419
 * writes: g_28 g_32 g_2829 g_89 g_166 g_692 g_809 g_1687 g_466 g_1607 g_1951 g_673 g_1350 g_569 g_1362 g_1029 g_246 g_68 g_48 g_3086 g_3123 g_111
 */
static uint8_t  func_22(float  p_23)
{ /* block id: 1 */
    uint8_t l_25 = 1UL;
    int16_t l_1975 = 1L;
    int32_t l_3072 = 6L;
    int32_t *l_3078 = &l_3072;
    int64_t **l_3105 = &g_415;
    int64_t ***l_3104 = &l_3105;
    int64_t ****l_3103 = &l_3104;
    int8_t l_3129[4];
    int i;
    for (i = 0; i < 4; i++)
        l_3129[i] = 0xDDL;
    if (l_25)
    { /* block id: 2 */
        uint32_t l_46 = 0xBE46552DL;
        int64_t l_1972[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int32_t *l_3071 = &g_28;
        int i;
        for (l_25 = (-19); (l_25 == 21); l_25 = safe_add_func_uint8_t_u_u(l_25, 1))
        { /* block id: 5 */
            int32_t *l_1974 = &g_28;
            int32_t *l_3070 = &g_89;
            for (g_28 = (-25); (g_28 < (-20)); g_28++)
            { /* block id: 8 */
                int32_t *l_31 = &g_32;
                uint8_t *l_47 = &g_48;
                int8_t *l_1970 = &g_1350;
                int16_t *l_1971[7] = {&g_1497[4][0][0],&g_1597,&g_1597,&g_1497[4][0][0],&g_1597,&g_1597,&g_1497[4][0][0]};
                const int32_t l_1973[8] = {0xEEDADEE2L,0xE598C2FCL,0xEEDADEE2L,0xEEDADEE2L,0xE598C2FCL,0xEEDADEE2L,1L,0xEEDADEE2L};
                int32_t **l_3069 = &g_68;
                int i;
                (*l_31) |= g_19;
                if (g_19)
                    continue;
            }
            for (g_2829 = (-11); (g_2829 <= 55); g_2829 = safe_add_func_int16_t_s_s(g_2829, 8))
            { /* block id: 1389 */
                int16_t l_3075 = 0xA469L;
                int32_t *l_3076 = &g_166;
                (*l_3070) ^= 0x19CD6614L;
                (*g_3077) = func_37(l_3075, (***g_1544), ((*l_3076) = l_25), l_3070, l_25);
                return (*l_3070);
            }
        }
        l_3078 = l_3071;
        l_3071 = (void*)0;
    }
    else
    { /* block id: 1398 */
        int16_t *l_3085 = (void*)0;
        int32_t l_3087 = (-1L);
        int16_t *l_3088 = &l_1975;
        int32_t l_3095 = 0L;
        uint64_t l_3096 = 0xB8BCF016E66D1958LL;
        uint32_t *l_3097[3];
        uint32_t *l_3098 = (void*)0;
        uint32_t *l_3099[9] = {&g_1362,&g_1362,&g_1362,&g_1362,&g_1362,&g_1362,&g_1362,&g_1362,&g_1362};
        int32_t l_3100 = 0x189C3859L;
        uint32_t l_3136 = 4UL;
        int i;
        for (i = 0; i < 3; i++)
            l_3097[i] = &g_673;
        (*g_68) |= (safe_add_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u((((****g_2220) &= (*l_3078)) < (*l_3078)), (((*l_3088) = (l_3087 |= (safe_lshift_func_int16_t_s_u(((***g_1544) = ((l_3085 != (g_3086 = l_3085)) > (*l_3078))), 6)))) < (safe_add_func_uint32_t_u_u((g_1362 = (safe_rshift_func_int16_t_s_u(((((void*)0 == &g_1544) != 0xABAB713B28E71592LL) == (((l_3095 = (l_3096 |= ((((((safe_mul_func_uint16_t_u_u(((-7L) || (*l_3078)), l_3095)) && g_2519) ^ l_3095) & (*g_415)) , (*g_2959)) == (*g_1913)))) , (****g_2636)) , l_3096)), 15))), g_1497[4][0][2]))))) ^ l_3100), (*l_3078)));
        if (l_3087)
        { /* block id: 1408 */
            int64_t ****l_3106[9][7] = {{&l_3104,(void*)0,&l_3104,&l_3104,&l_3104,(void*)0,&l_3104},{&l_3104,&l_3104,&l_3104,&l_3104,(void*)0,&l_3104,&l_3104},{(void*)0,&l_3104,&l_3104,&l_3104,&l_3104,&l_3104,&l_3104},{&l_3104,&l_3104,&l_3104,(void*)0,&l_3104,&l_3104,(void*)0},{&l_3104,&l_3104,&l_3104,&l_3104,&l_3104,&l_3104,&l_3104},{(void*)0,(void*)0,&l_3104,(void*)0,&l_3104,&l_3104,(void*)0},{&l_3104,&l_3104,&l_3104,&l_3104,(void*)0,(void*)0,&l_3104},{&l_3104,(void*)0,&l_3104,&l_3104,&l_3104,&l_3104,&l_3104},{&l_3104,&l_3104,&l_3104,&l_3104,&l_3104,&l_3104,&l_3104}};
            int32_t *l_3115[1];
            int32_t l_3127 = (-1L);
            uint64_t l_3128 = 0x4900E86F831C4883LL;
            int32_t **l_3130 = (void*)0;
            int32_t *l_3132 = &g_2382;
            int32_t **l_3131 = &l_3132;
            int32_t l_3133 = 0x1E678C7BL;
            int i, j;
            for (i = 0; i < 1; i++)
                l_3115[i] = &g_166;
            l_3133 ^= (safe_sub_func_int32_t_s_s((((*g_287) = ((l_3103 != l_3106[7][1]) == (safe_mul_func_float_f_f((safe_div_func_float_f_f(((((((safe_mul_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((((l_3115[0] = &g_166) != ((*l_3131) = func_37((((**g_1798) >= (safe_rshift_func_int8_t_s_s((safe_mul_func_int8_t_s_s(((((!g_2382) & (safe_add_func_uint16_t_u_u((((((l_3087 = (((g_3123 = g_3123) == (void*)0) >= l_3100)) == l_3127) <= l_3128) | l_3095) == (*l_3078)), 0xC03EL))) , l_3129[2]) < l_3127), l_3127)), 4))) , (*l_3078)), l_3127, (*l_3078), &l_3095, (*l_3078)))) < (*l_3078)), 5)), l_3127)) == 1L) , 0x4.8AC0ADp-13) == 0x4.59CAF5p+26) == p_23) > p_23), 0x1.8p-1)), l_3128)))) , 0xF180109BL), (*l_3078)));
        }
        else
        { /* block id: 1415 */
            uint16_t l_3148 = 0x3689L;
            (*g_287) = l_3100;
            for (g_28 = (-6); (g_28 < (-20)); g_28 = safe_sub_func_uint16_t_u_u(g_28, 5))
            { /* block id: 1419 */
                int32_t l_3147 = 8L;
                l_3100 &= (l_3136 < ((safe_rshift_func_uint16_t_u_s((0xBAE2A5FDC75DEF8FLL != (((safe_div_func_int8_t_s_s((safe_add_func_int16_t_s_s((safe_mod_func_uint64_t_u_u((0xA20D54C1L < (((safe_lshift_func_uint8_t_u_u(((*g_130) = l_3147), 3)) == (*l_3078)) < (l_3095 , (***g_724)))), l_3148)), (safe_mul_func_int16_t_s_s(((safe_mod_func_uint16_t_u_u((*g_1029), 0x4AB0L)) , 6L), l_3148)))), (*l_3078))) >= 0UL) | l_3147)), 5)) != 0xF27A7A8CL));
                return (*g_418);
            }
        }
    }
    return (*g_418);
}


/* ------------------------------------------ */
/* 
 * reads : g_500 g_32 g_1790 g_1744 g_1544 g_539 g_540 g_130 g_48 g_1289 g_1290 g_243 g_156 g_111 g_2117 g_569 g_381 g_998 g_999 g_1543 g_246 g_202 g_725 g_726 g_19 g_415 g_398 g_809 g_153 g_2382 g_2383 g_1515 g_89 g_311 g_1798 g_1799 g_701 g_28 g_1350 g_166 g_1362 g_1797 g_542 g_2405 g_1160 g_673 g_2419 g_1432 g_2331 g_2332 g_334 g_2409 g_294 g_2220 g_1771 g_129 g_2500 g_1687 g_1770 g_2510 g_2511 g_2519 g_449 g_2555 g_103 g_2576 g_2581 g_522 g_418 g_419 g_2606 g_698
 * writes: g_32 g_166 g_1951 g_458 g_1362 g_202 g_246 g_48 g_1790 g_111 g_381 g_569 g_103 g_809 g_311 g_692 g_1687 g_466 g_1607 g_673 g_1350 g_89 g_1029 g_68 g_153 g_294 g_449 g_243 g_398 g_2636 g_698
 */
static int32_t * func_33(int32_t * p_34, float  p_35, int32_t * p_36)
{ /* block id: 948 */
    uint32_t *l_2051 = (void*)0;
    int32_t l_2058[10] = {7L,0L,7L,0L,7L,0L,7L,0L,7L,0L};
    uint8_t *l_2061 = &g_1951;
    int16_t l_2062 = (-1L);
    int32_t *l_2063 = &g_166;
    int64_t ***l_2069 = (void*)0;
    int64_t ****l_2068[7];
    int32_t l_2070 = 1L;
    int32_t *l_2079 = &g_809;
    uint8_t *****l_2096 = &g_1770[0][0][0];
    int32_t l_2133 = 0L;
    int16_t ***l_2154 = (void*)0;
    int16_t ****l_2153[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint8_t *****l_2160 = &g_1770[1][5][2];
    int8_t l_2188 = 0x4BL;
    const float l_2216 = 0x4.9D5BDEp-76;
    uint8_t ****l_2221 = &g_1771[3];
    uint16_t l_2251[10];
    const int32_t ***l_2275 = (void*)0;
    const int32_t ****l_2274 = &l_2275;
    int32_t l_2290 = (-2L);
    int8_t l_2294 = 6L;
    uint64_t **l_2313 = &g_1744;
    float l_2467 = 0xC.9814B9p-41;
    float l_2469 = 0x0.Ep-1;
    int32_t l_2480[10] = {7L,7L,7L,7L,7L,7L,7L,7L,7L,7L};
    int16_t l_2499 = 0x6F34L;
    uint64_t l_2502 = 9UL;
    uint8_t *l_2532[2][3][6] = {{{&g_458,&g_2519,&g_458,&g_1951,&g_48,&g_48},{&g_1687,&g_458,&g_458,&g_1687,&g_2519,(void*)0},{(void*)0,&g_1687,&g_48,&g_1687,(void*)0,&g_1951}},{{&g_1687,(void*)0,&g_1951,&g_1951,(void*)0,&g_1687},{&g_458,&g_1687,&g_2519,(void*)0,&g_2519,&g_1687},{&g_2519,&g_458,&g_1951,&g_48,&g_48,&g_1951}}};
    int8_t *l_2579 = &g_311[1][8][0];
    int8_t ** const l_2578 = &l_2579;
    int16_t l_2629 = 0x2439L;
    float ***l_2668[3][2] = {{&g_999,(void*)0},{(void*)0,&g_999},{(void*)0,(void*)0}};
    int16_t l_2713 = 0L;
    int8_t **l_2781 = (void*)0;
    int32_t l_2802 = 0x6A0188E4L;
    int64_t l_2848 = 0x75672D78C6C59E5FLL;
    int8_t l_2967 = (-4L);
    uint32_t **l_3064 = (void*)0;
    uint32_t ***l_3063[2][3][4] = {{{&l_3064,&l_3064,&l_3064,&l_3064},{&l_3064,(void*)0,&l_3064,&l_3064},{&l_3064,&l_3064,&l_3064,&l_3064}},{{&l_3064,&l_3064,&l_3064,&l_3064},{&l_3064,(void*)0,&l_3064,&l_3064},{&l_3064,&l_3064,&l_3064,&l_3064}}};
    uint32_t ****l_3062[6] = {&l_3063[0][1][3],&l_3063[0][1][3],&l_3063[0][1][3],&l_3063[0][1][3],&l_3063[0][1][3],&l_3063[0][1][3]};
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_2068[i] = &l_2069;
    for (i = 0; i < 10; i++)
        l_2251[i] = 0xA25AL;
    (*p_36) &= (safe_mul_func_uint8_t_u_u((l_2051 == (void*)0), (safe_rshift_func_uint16_t_u_s((safe_sub_func_int16_t_s_s((safe_div_func_uint64_t_u_u(l_2058[7], l_2058[0])), 0x8258L)), ((safe_mod_func_int8_t_s_s((l_2061 != l_2061), ((g_500 , l_2062) && 4294967295UL))) > 0x9480L)))));
lbl_2540:
    if ((l_2058[7] != (((((g_1790 , (((*l_2063) = l_2058[7]) , 0xD8L)) >= 0x3CL) , (l_2070 = (safe_lshift_func_uint16_t_u_u((((((safe_rshift_func_uint8_t_u_u((l_2068[4] == &l_2069), ((*l_2061) = 0UL))) , l_2058[0]) , 0x0AB69196L) < l_2058[7]) , l_2062), l_2058[2])))) & l_2058[7]) , l_2058[7])))
    { /* block id: 953 */
        uint64_t l_2074[6] = {18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL};
        int32_t l_2115 = 0xF52D114BL;
        int32_t l_2136 = (-1L);
        int32_t l_2137 = 0x5F45CCC3L;
        int32_t l_2138 = 0x0E0DADD7L;
        uint32_t l_2140 = 0UL;
        int32_t l_2184 = 1L;
        int32_t l_2186[9] = {0xB8FFD3D1L,0xB8FFD3D1L,0xB8FFD3D1L,0xB8FFD3D1L,0xB8FFD3D1L,0xB8FFD3D1L,0xB8FFD3D1L,0xB8FFD3D1L,0xB8FFD3D1L};
        uint16_t ****l_2201 = &g_1434;
        uint8_t ****l_2225 = &g_1771[3];
        int32_t l_2292 = (-1L);
        uint32_t **l_2321 = &g_2235[1][2][1];
        uint32_t ***l_2320 = &l_2321;
        uint32_t l_2348[7][5];
        uint32_t l_2381[10][9][1] = {{{0xB81BEB09L},{0x6D5E4C8CL},{18446744073709551610UL},{0x4F87E7D6L},{0UL},{18446744073709551613UL},{0x70FCE423L},{0x6D5E4C8CL},{0UL}},{{0x1C2BC097L},{0xCAB97CDDL},{18446744073709551613UL},{0xCAB97CDDL},{0x1C2BC097L},{0UL},{0x6D5E4C8CL},{0x70FCE423L},{18446744073709551613UL}},{{0x1FC9E2C9L},{0x1039DE0EL},{0xB81BEB09L},{0UL},{18446744073709551609UL},{0UL},{0xB81BEB09L},{0x1039DE0EL},{0x1FC9E2C9L}},{{18446744073709551613UL},{0x70FCE423L},{0x6D5E4C8CL},{0UL},{0x1C2BC097L},{0xCAB97CDDL},{18446744073709551613UL},{0xCAB97CDDL},{0x1C2BC097L}},{{0UL},{0x6D5E4C8CL},{0x70FCE423L},{18446744073709551613UL},{0x1FC9E2C9L},{0x1039DE0EL},{0xB81BEB09L},{0UL},{18446744073709551609UL}},{{0UL},{0xB81BEB09L},{0x1039DE0EL},{0x1FC9E2C9L},{18446744073709551613UL},{0x70FCE423L},{0x6D5E4C8CL},{0UL},{0x1C2BC097L}},{{0xCAB97CDDL},{18446744073709551613UL},{0xCAB97CDDL},{0x1C2BC097L},{0UL},{0x6D5E4C8CL},{0x70FCE423L},{18446744073709551613UL},{0x1FC9E2C9L}},{{0x1039DE0EL},{0xB81BEB09L},{0UL},{18446744073709551609UL},{0UL},{0xB81BEB09L},{0x1039DE0EL},{0x1FC9E2C9L},{18446744073709551613UL}},{{0x70FCE423L},{0x6D5E4C8CL},{0UL},{0x1C2BC097L},{0xCAB97CDDL},{18446744073709551613UL},{0xCAB97CDDL},{0x1C2BC097L},{0UL}},{{0x6D5E4C8CL},{0x70FCE423L},{18446744073709551613UL},{0x1FC9E2C9L},{0x1039DE0EL},{0xB81BEB09L},{0UL},{18446744073709551609UL},{0UL}}};
        uint32_t ****l_2411[10][1][8] = {{{&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,(void*)0,(void*)0}},{{&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320}},{{(void*)0,&l_2320,(void*)0,&l_2320,(void*)0,(void*)0,&l_2320,&l_2320}},{{&l_2320,&l_2320,&l_2320,(void*)0,(void*)0,(void*)0,&l_2320,&l_2320}},{{(void*)0,&l_2320,&l_2320,&l_2320,&l_2320,(void*)0,&l_2320,&l_2320}},{{&l_2320,(void*)0,&l_2320,&l_2320,&l_2320,(void*)0,(void*)0,(void*)0}},{{&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320}},{{(void*)0,&l_2320,&l_2320,&l_2320,(void*)0,&l_2320,&l_2320,&l_2320}},{{&l_2320,&l_2320,(void*)0,(void*)0,&l_2320,(void*)0,(void*)0,&l_2320}},{{&l_2320,&l_2320,(void*)0,&l_2320,&l_2320,&l_2320,&l_2320,&l_2320}}};
        uint32_t l_2449 = 0x814DEE65L;
        int32_t l_2468 = 0xAD0B0447L;
        uint32_t l_2470 = 0x099F4CE9L;
        int i, j, k;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 5; j++)
                l_2348[i][j] = 1UL;
        }
        if ((~l_2070))
        { /* block id: 954 */
            int32_t *l_2072 = (void*)0;
            int32_t *l_2073[6][5];
            uint8_t ** const ***l_2097 = (void*)0;
            int8_t * const *l_2218[2];
            int8_t * const **l_2217 = &l_2218[0];
            int64_t *****l_2255 = (void*)0;
            int i, j;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 5; j++)
                    l_2073[i][j] = (void*)0;
            }
            for (i = 0; i < 2; i++)
                l_2218[i] = (void*)0;
            l_2074[0]--;
            for (g_458 = 0; (g_458 < 51); g_458 = safe_add_func_int64_t_s_s(g_458, 7))
            { /* block id: 958 */
                int32_t l_2090 = 0x2FA25C20L;
                uint32_t l_2134 = 0x179F2A66L;
                int32_t l_2135 = 0L;
                int32_t l_2139 = 0x9FED9B65L;
                l_2079 = &l_2058[1];
                for (g_1362 = 0; (g_1362 < 24); g_1362++)
                { /* block id: 962 */
                    int8_t l_2091 = 0x6CL;
                    uint8_t ****l_2116 = &g_1771[3];
                    uint32_t *l_2129 = &g_673;
                    (*p_36) &= (safe_add_func_int64_t_s_s((((((*l_2079) = 0xA66C7711L) < 0xFEBABB07L) , (safe_add_func_int16_t_s_s(((***g_1544) = (safe_sub_func_uint64_t_u_u(((*g_1744) = 18446744073709551615UL), (safe_sub_func_int16_t_s_s(l_2090, l_2074[0]))))), (((0x09L ^ (++(*g_130))) & (safe_add_func_uint32_t_u_u((*l_2079), (l_2096 != (l_2097 = l_2097))))) < 4294967291UL)))) <= l_2091), 7UL));
                    for (g_1790 = 0; (g_1790 >= 18); g_1790++)
                    { /* block id: 971 */
                        uint64_t l_2104[10][3] = {{0xDFBF4EFD38733E28LL,0xAA601C580CF4D97DLL,0xDFBF4EFD38733E28LL},{0xD9D7E8D2BB322270LL,0x667DD3F76575664DLL,0x667DD3F76575664DLL},{0UL,0xAA601C580CF4D97DLL,0UL},{0xD9D7E8D2BB322270LL,0xD9D7E8D2BB322270LL,0x667DD3F76575664DLL},{0xDFBF4EFD38733E28LL,0xAA601C580CF4D97DLL,0xDFBF4EFD38733E28LL},{0xD9D7E8D2BB322270LL,0x667DD3F76575664DLL,0x667DD3F76575664DLL},{0UL,0xAA601C580CF4D97DLL,0UL},{0xD9D7E8D2BB322270LL,0xD9D7E8D2BB322270LL,0x667DD3F76575664DLL},{0xDFBF4EFD38733E28LL,0xAA601C580CF4D97DLL,0xDFBF4EFD38733E28LL},{0xD9D7E8D2BB322270LL,0x667DD3F76575664DLL,0x667DD3F76575664DLL}};
                        int i, j;
                        (*g_2117) = ((safe_div_func_int64_t_s_s(0x1EACBFE879FA2D9FLL, (safe_mul_func_uint8_t_u_u(((1L != l_2104[7][0]) <= l_2104[7][0]), (((*l_2079) &= (&p_36 == (*g_1289))) >= g_243))))) , (safe_add_func_float_f_f((safe_sub_func_float_f_f((((*g_156) = ((safe_lshift_func_uint16_t_u_s((safe_div_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((((((l_2091 , (l_2115 , (*l_2079))) , (*p_36)) , l_2116) == l_2116) < (*l_2079)), l_2091)), (*p_36))), l_2115)) , (*g_156))) <= 0x1.1p-1), p_35)), l_2091)));
                    }
                    for (g_569 = 6; (g_569 >= 0); g_569 -= 1)
                    { /* block id: 978 */
                        float **l_2120 = &g_287;
                        float ***l_2121[1];
                        float **l_2122[9][8][2] = {{{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_287,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,&g_287},{(void*)0,(void*)0}},{{&g_287,&g_287},{&g_287,(void*)0},{(void*)0,&g_287},{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_287,(void*)0}},{{(void*)0,(void*)0},{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,&g_287},{(void*)0,(void*)0},{&g_287,&g_287},{&g_287,(void*)0},{(void*)0,&g_287}},{{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_287,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_287},{&g_287,&g_287}},{{(void*)0,&g_287},{(void*)0,(void*)0},{&g_287,&g_287},{&g_287,(void*)0},{(void*)0,&g_287},{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,(void*)0}},{{(void*)0,(void*)0},{&g_287,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,&g_287},{(void*)0,(void*)0},{&g_287,&g_287}},{{&g_287,(void*)0},{(void*)0,&g_287},{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_287,(void*)0},{(void*)0,(void*)0}},{{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,&g_287},{(void*)0,(void*)0},{&g_287,&g_287},{&g_287,(void*)0},{(void*)0,&g_287},{(void*)0,&g_287}},{{&g_287,&g_287},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_287,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_287},{&g_287,&g_287},{(void*)0,&g_287}}};
                        uint64_t **l_2128[5][3][10] = {{{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744}},{{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744}},{{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744}},{{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744}},{{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744},{&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744,&g_1744}}};
                        int32_t l_2132[3][2][7] = {{{0xA29E1979L,0x3B459882L,0x40634B34L,0xA29E1979L,0x40634B34L,0x3B459882L,0xA29E1979L},{0xB184E427L,0xA29E1979L,0x3B459882L,0x40634B34L,0xA29E1979L,0x40634B34L,0x3B459882L}},{{0xA29E1979L,0xA29E1979L,0x605FCFFBL,1L,1L,0x605FCFFBL,1L},{1L,0x3B459882L,0x3B459882L,1L,0x40634B34L,0xB184E427L,1L}},{{0xB184E427L,1L,0x40634B34L,0x40634B34L,1L,0xB184E427L,0x3B459882L},{1L,1L,0x605FCFFBL,0xA29E1979L,0xA29E1979L,0x605FCFFBL,1L}}};
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                            l_2121[i] = &g_999;
                        g_381[(g_569 + 1)] = g_381[g_569];
                        p_35 = ((safe_mod_func_int16_t_s_s(((l_2120 != (l_2122[4][5][1] = (*g_998))) | (!0UL)), ((g_539 == ((((safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u((((((void*)0 == l_2128[0][1][7]) ^ ((((void*)0 != l_2129) , ((safe_lshift_func_int8_t_s_s((-9L), 7)) == l_2091)) == 0x46C4L)) , (*l_2079)) , l_2090), l_2115)), l_2132[1][1][2])) >= l_2091) , 0x759828F982563B7CLL) , g_539)) , l_2074[5]))) , l_2133);
                        return p_34;
                    }
                    (*l_2079) = l_2134;
                }
                ++l_2140;
                (*p_36) = (((**g_539) = (****g_1543)) == (((safe_div_func_uint32_t_u_u((4294967295UL | ((*l_2079) < ((((*g_1744) = (*g_1744)) < ((*l_2079) < 1UL)) & ((safe_mul_func_int16_t_s_s((safe_div_func_int64_t_s_s((safe_sub_func_int16_t_s_s((l_2153[0] == (((*l_2079) , (safe_div_func_int16_t_s_s((safe_rshift_func_int16_t_s_s(1L, 6)), (**g_725)))) , (void*)0)), (*l_2079))), 0xD31D169B56A995A4LL)), 0UL)) , (*g_415))))), 0x7CF0FDF5L)) < (*l_2079)) || (*g_1744)));
            }
        }
        else
        { /* block id: 1063 */
            int16_t l_2367[3];
            uint64_t l_2379[9];
            int8_t l_2399 = 0L;
            int32_t l_2414 = 0x21FF2FBBL;
            int32_t *l_2416 = &l_2137;
            uint16_t l_2431 = 0UL;
            int32_t l_2443 = 0xFDE571D5L;
            int32_t l_2444 = 0L;
            int32_t l_2446[5];
            int32_t *** const *l_2461[10] = {&g_1914,(void*)0,&g_1914,&g_1914,(void*)0,&g_1914,(void*)0,&g_1914,&g_1914,(void*)0};
            int16_t *l_2465 = &g_1497[4][0][0];
            int16_t **l_2464[5][2] = {{(void*)0,&l_2465},{(void*)0,&l_2465},{(void*)0,&l_2465},{(void*)0,&l_2465},{(void*)0,&l_2465}};
            int i, j;
            for (i = 0; i < 3; i++)
                l_2367[i] = 0x2B8FL;
            for (i = 0; i < 9; i++)
                l_2379[i] = 0xFBA284ABD643D75CLL;
            for (i = 0; i < 5; i++)
                l_2446[i] = 7L;
            for (g_103 = 0; (g_103 > 18); g_103++)
            { /* block id: 1066 */
                int16_t l_2338 = 0xC7B9L;
                int32_t l_2349 = 8L;
                uint32_t ****l_2410 = (void*)0;
                int64_t * const ****l_2415[6] = {&g_1819,&g_1819,&g_1819,&g_1819,&g_1819,&g_1819};
                int i;
                if (l_2338)
                    break;
                if ((((((*g_1744) = 0x62316A3CB14CF6BELL) == (((**g_539) >= l_2338) < (safe_rshift_func_int8_t_s_u(l_2184, (1L > (~(safe_add_func_int8_t_s_s((*l_2079), (*l_2079))))))))) & l_2338) <= (l_2349 |= ((safe_add_func_int8_t_s_s((safe_lshift_func_int8_t_s_s(l_2348[5][2], g_19)), g_153)) , 0x662DL))))
                { /* block id: 1070 */
                    int32_t l_2361 = 0L;
                    uint64_t *l_2362 = &g_1790;
                    int32_t l_2371 = 0x87C5F0BDL;
                    int8_t *l_2380[3];
                    int32_t *l_2404 = &l_2184;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_2380[i] = (void*)0;
                    (*g_2383) = (safe_sub_func_float_f_f((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((safe_sub_func_float_f_f(((safe_mul_func_float_f_f(((+((p_35 = (l_2361 , l_2338)) >= (((void*)0 != l_2362) >= ((((safe_add_func_uint64_t_u_u((safe_mod_func_int8_t_s_s((g_311[1][3][2] = ((*l_2079) = ((l_2349 < l_2367[0]) , (((safe_add_func_uint8_t_u_u((l_2371 &= ((*l_2061) = (~0xB6391FBAA4158B8FLL))), ((safe_lshift_func_int16_t_s_u((safe_mod_func_int16_t_s_s((+(safe_sub_func_uint64_t_u_u(18446744073709551615UL, (0x435DD59BL || l_2379[0])))), l_2338)), 5)) <= (*p_36)))) & l_2361) ^ l_2338)))), l_2361)), (*g_415))) , 0xD252L) , (-0x5.6p-1)) > l_2381[0][5][0])))) == 0xE.DEFAD6p+60), 0x1.9p+1)) >= g_2382), 0x0.9p-1)), 0xC.02004Ep-62)) > (-0x6.7p+1)), l_2361)), l_2379[0]));
                    for (l_2349 = 15; (l_2349 > 25); l_2349 = safe_add_func_int16_t_s_s(l_2349, 8))
                    { /* block id: 1079 */
                        int32_t *l_2386 = &g_89;
                        return l_2386;
                    }
                    l_2404 = func_37(((l_2367[1] ^ (l_2371 = (((**l_2313) = ((**g_725) ^ ((l_2184 = (safe_rshift_func_int16_t_s_u((safe_lshift_func_int16_t_s_u((l_2338 == (safe_rshift_func_int16_t_s_u((((safe_div_func_uint8_t_u_u((l_2379[0] < (safe_div_func_int32_t_s_s((safe_mod_func_int16_t_s_s(((**g_539) = (l_2399 && ((p_34 != (((**g_725) < (((safe_div_func_uint8_t_u_u(l_2115, (l_2367[0] ^ l_2367[2]))) > 65534UL) ^ 0UL)) , &g_378)) != 0xC242C107L))), l_2361)), 0x33A8F944L))), l_2138)) ^ (*g_726)) & l_2379[0]), l_2379[0]))), 6)), (*g_726)))) > l_2379[4]))) && l_2371))) || l_2348[1][1]), (*l_2079), l_2074[0], p_34, l_2137);
                }
                else
                { /* block id: 1087 */
                    float l_2412 = 0x1.4p+1;
                    int32_t l_2413 = (-1L);
                    p_36 = func_37(((*p_36) = ((*l_2079) = ((g_2405 == (l_2411[7][0][1] = l_2410)) , (-1L)))), ((((*g_1160) ^= 1L) >= g_28) , ((l_2414 = (l_2413 = (l_2349 = l_2367[2]))) | ((l_2367[0] , l_2415[0]) != &g_1819))), g_673, l_2416, l_2115);
                    return p_34;
                }
            }
            (*l_2416) |= (*p_36);
            for (g_153 = 0; (g_153 <= 2); g_153 += 1)
            { /* block id: 1102 */
                int32_t **l_2417 = &l_2416;
                uint64_t l_2418 = 0x0A93BE6D7F634716LL;
                const int32_t ****l_2428 = &l_2275;
                int32_t l_2437 = 0x0AE82302L;
                int32_t l_2440 = 0x87584307L;
                int32_t l_2441 = 0x04F597CFL;
                int32_t l_2442 = 1L;
                int32_t l_2445 = 0L;
                int32_t l_2447 = 4L;
                int32_t l_2448[9][2] = {{(-2L),(-2L)},{0xF80B051FL,(-2L)},{(-2L),0xC411E7F9L},{0xF49B7C8BL,0x36D8FDF6L},{0xF80B051FL,0xF49B7C8BL},{0x36D8FDF6L,0xC411E7F9L},{0x36D8FDF6L,0xF49B7C8BL},{0xF80B051FL,(-5L)},{0xC411E7F9L,0x1A23FD8BL}};
                uint8_t * const ***l_2463[10][5] = {{&g_644,&g_644,&g_644,&g_644,&g_644},{(void*)0,&g_644,(void*)0,&g_644,&g_644},{&g_644,&g_644,&g_644,&g_644,&g_644},{&g_644,&g_644,&g_644,&g_644,&g_644},{&g_644,&g_644,&g_644,&g_644,(void*)0},{&g_644,(void*)0,(void*)0,&g_644,&g_644},{&g_644,&g_644,&g_644,&g_644,&g_644},{&g_644,&g_644,&g_644,&g_644,&g_644},{&g_644,&g_644,(void*)0,&g_644,&g_644},{&g_644,&g_644,&g_644,&g_644,&g_644}};
                int32_t *l_2466 = &g_28;
                int i, j;
                (*l_2417) = p_36;
                (*g_2419) = (0x3.7p+1 != (0xB.368BF8p+13 > l_2418));
                if (((*l_2416) = (*p_36)))
                { /* block id: 1106 */
                    int8_t l_2432[5] = {1L,1L,1L,1L,1L};
                    int32_t l_2438[9];
                    int64_t ****l_2462[2];
                    int i;
                    for (i = 0; i < 9; i++)
                        l_2438[i] = 0x224C4DEDL;
                    for (i = 0; i < 2; i++)
                        l_2462[i] = &l_2069;
                    if ((((((l_2432[3] = ((*g_2117) = (safe_div_func_float_f_f(((safe_mul_func_uint8_t_u_u((((safe_div_func_int16_t_s_s((((***g_1544) > 0x9B70L) || (safe_sub_func_int8_t_s_s((&l_2275 != l_2428), (((((*g_1799) | (-1L)) != (g_673 || (l_2184 == (((((safe_rshift_func_int16_t_s_s((*l_2416), 10)) > 0xD8FBFE3C356F8D46LL) < 0x73DD27856D3206F6LL) , 65535UL) || l_2431)))) | 254UL) , g_1432)))), (**g_725))) , (*g_2331)) == (*g_2331)), (*l_2416))) , 0x1.Ap+1), p_35)))) <= p_35) , (*g_540)) >= l_2186[6]) ^ (**l_2417)))
                    { /* block id: 1109 */
                        int32_t *l_2433 = (void*)0;
                        int32_t *l_2434 = (void*)0;
                        int32_t *l_2435 = &l_2186[4];
                        int32_t *l_2436[9][4] = {{&l_2138,(void*)0,&l_2186[6],&l_2136},{&g_89,&g_32,&g_89,&l_2186[6]},{&l_2186[5],&l_2137,(void*)0,&l_2186[5]},{&l_2138,&l_2186[6],(void*)0,&l_2137},{&l_2186[6],&g_32,(void*)0,(void*)0},{&l_2138,&l_2138,(void*)0,&l_2136},{&l_2186[5],(void*)0,&g_89,&l_2137},{&g_89,&l_2137,&l_2186[6],&g_89},{&l_2138,&l_2137,&g_28,&l_2137}};
                        int i, j;
                        (*l_2416) = 0L;
                        l_2449++;
                    }
                    else
                    { /* block id: 1112 */
                        int32_t ****l_2460 = &g_1914;
                        (*l_2416) = ((**l_2417) && ((0x1430L <= (*g_726)) > (safe_rshift_func_int8_t_s_s((safe_mod_func_int8_t_s_s(((g_334 != ((((0xA.DFB800p+1 == (**l_2417)) , (((safe_mul_func_uint8_t_u_u(((safe_mod_func_uint16_t_u_u(((l_2460 == l_2461[7]) < l_2432[3]), 0xAC11L)) > l_2381[0][5][0]), l_2438[0])) , (void*)0) == l_2462[1])) , l_2463[9][4]) == (void*)0)) <= (*l_2416)), 0xD4L)), g_2409))));
                    }
                }
                else
                { /* block id: 1115 */
                    (*p_36) |= (l_2464[4][1] != (**g_1543));
                    return p_34;
                }
                return l_2466;
            }
            ++l_2470;
        }
    }
    else
    { /* block id: 1123 */
        uint32_t *l_2473 = &g_1362;
        uint32_t *l_2474 = &g_294;
        const int32_t l_2479[2] = {0x9D1839D2L,0x9D1839D2L};
        int64_t l_2501 = 0x77C0E7087BBE9DB1LL;
        float *l_2503 = &l_2467;
        int64_t l_2504 = 0xA029BDC366223BFALL;
        float *l_2505 = &g_111[5];
        const uint8_t *l_2509 = (void*)0;
        const uint8_t **l_2508 = &l_2509;
        const uint8_t ***l_2507 = &l_2508;
        const uint8_t *** const *l_2506 = &l_2507;
        int i;
        (*g_2510) = (((l_2506 = ((((void*)0 != l_2473) >= ((((*l_2079) & ((((--(*l_2474)) , (((p_35 == (-(((*l_2505) = (((((*l_2503) = (((~(l_2479[1] >= l_2480[3])) , (safe_div_func_float_f_f((((safe_div_func_uint8_t_u_u((((~(!(safe_rshift_func_uint16_t_u_s((safe_rshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((safe_add_func_uint64_t_u_u(l_2479[1], ((**l_2313) = ((safe_rshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u(((-1L) == ((****g_2220) ^= (((*l_2079) == (*l_2079)) > l_2479[1]))), l_2499)), g_2500)) == 0xC75EL)))) > (-8L)), g_1687)), l_2501)), l_2479[1])))) | l_2479[1]) >= 0xA5F6L), l_2502)) == 0x0F70L) , (*g_2383)), (-0x1.Cp+1)))) <= (*l_2079))) > p_35) > 0x7.27F2DFp+57) < l_2504)) != 0x9.6p-1))) , 2UL) >= g_311[1][7][1])) == l_2479[1]) >= 0UL)) & (*p_36)) < 0xAAL)) , l_2506)) == (*l_2096)) , p_34);
        return g_2511;
    }
    for (g_1350 = 0; (g_1350 <= (-14)); g_1350 = safe_sub_func_int8_t_s_s(g_1350, 3))
    { /* block id: 1135 */
        int16_t l_2516 = 0xF618L;
        int32_t l_2526[7][6][2] = {{{0xE4AE285BL,0x3F466AF8L},{0x01E72A55L,0x11C1FFDFL},{0x3F466AF8L,0xBC6CC6FBL},{0x01D9AED0L,7L},{0x7C228A46L,1L},{1L,0x01E72A55L}},{{0x025C6A47L,0x01E72A55L},{1L,1L},{0x7C228A46L,7L},{0x01D9AED0L,0xBC6CC6FBL},{0x3F466AF8L,0x11C1FFDFL},{0x01E72A55L,0x3F466AF8L}},{{0xE4AE285BL,0x0A6A4F61L},{0xE4AE285BL,0x3F466AF8L},{0x01E72A55L,0x11C1FFDFL},{0x3F466AF8L,0xBC6CC6FBL},{0x01D9AED0L,7L},{0x7C228A46L,1L}},{{1L,0x01E72A55L},{0x025C6A47L,0x01E72A55L},{1L,1L},{0x7C228A46L,7L},{0x01D9AED0L,0xBC6CC6FBL},{0x3F466AF8L,0x11C1FFDFL}},{{0x01E72A55L,0x3F466AF8L},{0xE4AE285BL,0x0A6A4F61L},{0xE4AE285BL,0x3F466AF8L},{0x01E72A55L,0x11C1FFDFL},{0x3F466AF8L,0xBC6CC6FBL},{0x01D9AED0L,7L}},{{0x7C228A46L,1L},{1L,0x01E72A55L},{0x025C6A47L,0x01E72A55L},{1L,1L},{0x7C228A46L,7L},{0x01D9AED0L,0xBC6CC6FBL}},{{0x3F466AF8L,0x11C1FFDFL},{0x01E72A55L,0x3F466AF8L},{0xE4AE285BL,0x0A6A4F61L},{0xE4AE285BL,0x3F466AF8L},{0x01E72A55L,0x11C1FFDFL},{0x3F466AF8L,0xBC6CC6FBL}}};
        uint16_t *l_2527 = &l_2251[4];
        uint64_t l_2531 = 2UL;
        uint8_t *l_2533 = &g_334;
        int32_t l_2554 = 0x24F72035L;
        const int32_t ****l_2604[9];
        uint32_t **l_2617 = &g_2235[2][4][1];
        uint32_t ***l_2616 = &l_2617;
        int64_t **l_2621 = &g_415;
        int64_t ***l_2620[1];
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_2604[i] = (void*)0;
        for (i = 0; i < 1; i++)
            l_2620[i] = &l_2621;
        (*l_2079) |= 0x7109A510L;
        p_34 = p_36;
        l_2526[3][1][1] ^= ((safe_sub_func_uint8_t_u_u(((*l_2079) | (*l_2079)), l_2516)) && (l_2516 >= (safe_rshift_func_uint8_t_u_u((((g_2519 != ((((safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u(0UL, (((*l_2079) ^= 1UL) >= (++(*l_2527))))), (((*p_36) = (-9L)) >= (!l_2516)))), l_2531)) , l_2532[1][2][5]) != l_2533) & g_2519)) >= 0x26L) >= 0x27L), l_2516))));
        for (g_48 = 2; (g_48 <= 7); g_48 += 1)
        { /* block id: 1144 */
            int32_t l_2534[7] = {0xE174C5E5L,0xE174C5E5L,0xE174C5E5L,0xE174C5E5L,0xE174C5E5L,0xE174C5E5L,0xE174C5E5L};
            uint64_t l_2537 = 0UL;
            int8_t l_2558 = 0x3CL;
            uint16_t l_2607[2];
            int32_t l_2631 = (-8L);
            float *l_2632 = &g_381[5];
            int i;
            for (i = 0; i < 2; i++)
                l_2607[i] = 0x6847L;
            for (g_89 = 5; (g_89 >= 0); g_89 -= 1)
            { /* block id: 1147 */
                int32_t *l_2535 = (void*)0;
                int32_t *l_2536[7][8][1] = {{{&l_2534[6]},{&l_2534[6]},{&g_89},{&l_2058[0]},{&g_89},{&l_2526[3][1][1]},{&g_89},{&l_2526[3][1][1]}},{{&g_89},{&g_89},{&g_28},{&l_2534[1]},{&l_2534[1]},{&g_28},{&g_89},{&g_89}},{{&l_2526[3][1][1]},{&g_89},{&l_2526[3][1][1]},{&g_89},{&g_89},{&g_28},{&l_2534[1]},{&l_2534[1]}},{{&g_28},{&g_89},{&g_89},{&l_2526[3][1][1]},{&g_89},{&l_2526[3][1][1]},{&g_89},{&g_89}},{{&g_28},{&l_2534[1]},{&l_2534[1]},{&g_28},{&g_89},{&g_89},{&l_2526[3][1][1]},{&g_89}},{{&l_2526[3][1][1]},{&g_89},{&g_89},{&g_28},{&l_2534[1]},{&l_2534[1]},{&g_28},{&g_89}},{{&g_89},{&l_2526[3][1][1]},{&g_89},{&l_2526[3][1][1]},{&g_89},{&g_89},{&g_28},{&l_2534[1]}}};
                const int32_t l_2557[2][10] = {{6L,0x410480A8L,6L,0x410480A8L,6L,0x410480A8L,6L,0x410480A8L,6L,0x410480A8L},{6L,0x410480A8L,6L,0x410480A8L,6L,0x410480A8L,6L,0x410480A8L,6L,0x410480A8L}};
                int i, j, k;
                ++l_2537;
                for (l_2516 = 5; (l_2516 >= 0); l_2516 -= 1)
                { /* block id: 1151 */
                    int8_t *l_2549 = &l_2294;
                    int32_t l_2550 = 0x65D9F298L;
                    uint32_t *l_2553 = (void*)0;
                    for (g_449 = 2; (g_449 <= 7); g_449 += 1)
                    { /* block id: 1154 */
                        if (g_153)
                            goto lbl_2540;
                        if (l_2526[1][5][0])
                            continue;
                        (*p_34) = ((safe_add_func_int8_t_s_s((-3L), g_569)) <= l_2534[0]);
                    }
                    (*p_34) = (((((safe_rshift_func_int16_t_s_s(((***g_1544) ^= 0x38D8L), 12)) ^ ((safe_mod_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_u(((*l_2549) = l_2534[2]), l_2550)) ^ 0xE49BD5E221AE320BLL), l_2537)) & l_2526[3][1][1])) != (l_2554 = (safe_rshift_func_int8_t_s_s((p_36 == p_34), 2)))) & (g_2555 != (void*)0)) ^ l_2557[0][6]);
                }
                if ((*p_36))
                    continue;
            }
            if (l_2558)
            { /* block id: 1166 */
                int16_t l_2584 = 3L;
                int32_t l_2588 = 0x78DE4649L;
                int32_t l_2589 = 0xAD7BF3ACL;
                for (g_103 = 0; (g_103 <= 7); g_103 += 1)
                { /* block id: 1169 */
                    uint32_t ** const *l_2562 = (void*)0;
                    uint32_t ** const **l_2561 = &l_2562;
                    int32_t l_2580 = 0x9D112F3FL;
                    for (g_673 = 0; (g_673 <= 7); g_673 += 1)
                    { /* block id: 1172 */
                        uint32_t l_2565 = 0xA5AE75EBL;
                        uint32_t *l_2566 = &l_2565;
                        const int32_t l_2582 = 0L;
                        int32_t l_2583 = 0L;
                        uint32_t ***l_2587[4];
                        uint32_t ****l_2586 = &l_2587[0];
                        uint32_t *****l_2585 = &l_2586;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_2587[i] = (void*)0;
                        (*p_36) = (l_2589 = (safe_rshift_func_uint16_t_u_s((0UL > (((l_2526[1][1][1] &= ((0UL || (l_2561 == ((*l_2585) = (((l_2584 = (safe_sub_func_uint32_t_u_u((((*l_2566) = l_2565) < (((((!(safe_mul_func_int8_t_s_s((safe_div_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(((safe_lshift_func_uint8_t_u_u(((((g_2576 != l_2578) , (l_2580 && ((((((l_2580 , (g_2581[4] & l_2580)) >= l_2580) < 0xD2C9AFF1L) && l_2580) , g_103) , l_2534[2]))) == g_19) <= g_522[1][1][2]), 1)) , l_2582), g_1350)), (-2L))), l_2534[2]))) , &p_34) != &p_36) > l_2583) && l_2583)), 8UL))) & 0x0DL) , (void*)0)))) || 0L)) && l_2588) >= l_2580)), 4)));
                    }
                }
                for (g_1362 = 0; (g_1362 <= 7); g_1362 += 1)
                { /* block id: 1183 */
                    for (g_243 = 0; (g_243 <= 2); g_243 += 1)
                    { /* block id: 1186 */
                        int i, j;
                        if ((*l_2079))
                            break;
                    }
                }
            }
            else
            { /* block id: 1190 */
                int32_t l_2590 = 1L;
                int32_t *l_2591 = &l_2526[5][3][0];
                int32_t *l_2592[4] = {&l_2590,&l_2590,&l_2590,&l_2590};
                uint32_t l_2593 = 0UL;
                const uint16_t *l_2605 = &g_1334;
                int i;
                --l_2593;
                l_2534[6] = ((((*l_2079) , (safe_mod_func_uint64_t_u_u((((*l_2079) = (safe_div_func_int32_t_s_s(((((safe_mul_func_int16_t_s_s((0x09L == (((0xF21E1D30L > ((*p_36) &= (((safe_mul_func_uint8_t_u_u((*g_418), (((l_2604[7] != &g_1289) >= (g_569 ^= (*g_415))) != ((void*)0 == l_2605)))) < l_2534[2]) < l_2537))) , (*l_2591)) > l_2534[2])), 0xFE4EL)) <= (*l_2591)) >= 246UL) != g_2606), 2L))) , (*g_1744)), 0x12BC4252D789868ELL))) == 0L) , (*p_34));
                if (l_2558)
                    continue;
                l_2534[2] |= l_2607[1];
            }
            (*l_2632) = ((safe_lshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((*g_418), (((*l_2079) ^= (l_2534[2] = (l_2616 != (void*)0))) >= l_2537))), (safe_mul_func_int16_t_s_s((l_2620[0] != ((safe_div_func_uint32_t_u_u(l_2607[0], (~(safe_rshift_func_uint16_t_u_s(((*l_2527) = (safe_sub_func_uint64_t_u_u(((*p_34) , l_2629), (safe_unary_minus_func_int32_t_s(((((((**l_2621) = (2UL > l_2607[1])) ^ 0x9AAC3606B92AD24FLL) >= l_2631) & 0x327FF870L) == l_2607[1])))))), l_2607[1]))))) , (void*)0)), l_2607[1])))), l_2607[0])), 1)) , 0x1.8p-1);
        }
    }
    for (l_2502 = 0; (l_2502 <= 2); l_2502 += 1)
    { /* block id: 1208 */
        float ***l_2634[9][3] = {{(void*)0,(void*)0,&g_999},{&g_999,&g_999,&g_999},{(void*)0,(void*)0,&g_999},{&g_999,&g_999,&g_999},{(void*)0,(void*)0,&g_999},{&g_999,&g_999,&g_999},{(void*)0,(void*)0,&g_999},{&g_999,&g_999,&g_999},{(void*)0,(void*)0,&g_999}};
        float ****l_2633 = &l_2634[5][2];
        float *****l_2635[4][5][3] = {{{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633}},{{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633}},{{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633}},{{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633},{&l_2633,&l_2633,&l_2633}}};
        int32_t l_2644 = 0xCED85BF2L;
        int64_t **l_2647 = &g_415;
        uint64_t * const l_2663 = &g_202[7][1];
        int64_t l_2694[7] = {0x3CE611127C44BEC8LL,0x3CE611127C44BEC8LL,0x3CE611127C44BEC8LL,0x3CE611127C44BEC8LL,0x3CE611127C44BEC8LL,0x3CE611127C44BEC8LL,0x3CE611127C44BEC8LL};
        int32_t l_2726 = (-1L);
        int32_t *l_2729 = &g_32;
        uint32_t **l_2732 = &l_2051;
        uint32_t ***l_2731 = &l_2732;
        uint32_t ****l_2730 = &l_2731;
        int16_t l_2801 = 0x79DAL;
        int64_t l_2803 = 0x70381B341F0755DELL;
        uint8_t l_2825 = 0xB7L;
        int32_t l_2878 = 0xC86BCFA3L;
        uint32_t **l_2884 = (void*)0;
        uint32_t ** const *l_2883[4][4] = {{(void*)0,&l_2884,(void*)0,&l_2884},{(void*)0,&l_2884,(void*)0,&l_2884},{(void*)0,&l_2884,(void*)0,&l_2884},{(void*)0,&l_2884,(void*)0,&l_2884}};
        int8_t **l_2955 = &l_2579;
        int32_t l_2968 = (-1L);
        uint32_t l_3000 = 18446744073709551615UL;
        uint8_t l_3001 = 0x69L;
        int i, j, k;
        (*p_36) |= (((((g_2636 = l_2633) == &g_1934) || (*l_2079)) >= ((safe_add_func_uint32_t_u_u((safe_rshift_func_int8_t_s_s(((l_2644 != (l_2644 <= (safe_mul_func_int8_t_s_s((l_2644 , (l_2644 ^ ((l_2647 == (void*)0) < 0x03E70134A6C7127DLL))), 0x46L)))) ^ (****g_2220)), l_2644)), (***g_1797))) != (*l_2079))) >= l_2644);
        (*l_2079) &= 0xD75FD1AAL;
        for (g_89 = 0; (g_89 <= 3); g_89 += 1)
        { /* block id: 1214 */
            float l_2655 = 0x9.2p-1;
            uint8_t l_2658 = 0xBFL;
            int32_t l_2664 = 0L;
            uint32_t l_2667 = 3UL;
            float ***l_2669 = &g_999;
            float *****l_2671[8] = {&g_2636,&g_2636,&g_2636,&g_2636,&g_2636,(void*)0,(void*)0,&g_2636};
            const uint16_t *l_2710[8] = {&l_2251[2],&l_2251[2],&l_2251[2],&l_2251[2],&l_2251[2],&l_2251[2],&l_2251[2],&l_2251[2]};
            int32_t l_2750 = 0L;
            uint16_t ****l_2756 = &g_1434;
            float l_2764[3][2][7] = {{{0x1.Bp+1,0x0.8p+1,0x3.7p-1,0x0.8p+1,0x1.Bp+1,0x9.Cp+1,0x1.Bp+1},{0x9.Dp+1,0x7.7CBFC6p+60,0x7.7CBFC6p+60,0x9.Dp+1,0x7.7CBFC6p+60,0x7.7CBFC6p+60,0x9.Dp+1}},{{0xA.480E0Cp+49,0x0.8p+1,0xA.480E0Cp+49,(-0x7.1p-1),0x1.Bp+1,(-0x7.1p-1),0xA.480E0Cp+49},{0x9.Dp+1,0x9.Dp+1,0x6.85E5B4p-61,0x9.Dp+1,0x9.Dp+1,0x6.85E5B4p-61,0x9.Dp+1}},{{0x1.Bp+1,(-0x7.1p-1),0xA.480E0Cp+49,0x0.8p+1,0xA.480E0Cp+49,(-0x7.1p-1),0x1.Bp+1},{0x7.7CBFC6p+60,0x9.Dp+1,0x7.7CBFC6p+60,0x7.7CBFC6p+60,0x9.Dp+1,0x7.7CBFC6p+60,0x7.7CBFC6p+60}}};
            uint32_t l_2776 = 0x3CF36B2CL;
            uint32_t l_2777 = 4294967295UL;
            int8_t l_2823 = 1L;
            uint64_t **l_2846 = &g_1744;
            uint32_t *l_2847 = &g_103;
            int i, j, k;
        }
        for (g_698 = 0; (g_698 <= 2); g_698 += 1)
        { /* block id: 1307 */
            const int16_t l_2859 = 1L;
            int8_t **l_2956 = &l_2579;
            int32_t *l_3004 = (void*)0;
            uint8_t **l_3044 = &g_130;
            int16_t l_3045[3];
            int16_t l_3046 = 0xF741L;
            int32_t l_3049 = 0xC6BE73B7L;
            uint32_t ****l_3060 = &l_2731;
            int i;
            for (i = 0; i < 3; i++)
                l_3045[i] = 0xD04EL;
        }
    }
    return p_36;
}


/* ------------------------------------------ */
/* 
 * reads : g_692 g_1687 g_466 g_1607 g_1515 g_89 g_809 g_1951 g_1544 g_539 g_202 g_726 g_19 g_311 g_1798 g_1799 g_701 g_28 g_1350 g_166 g_569 g_1362 g_398 g_1797 g_540 g_542
 * writes: g_692 g_809 g_1687 g_466 g_1607 g_1951 g_673 g_1350 g_569 g_1362 g_89 g_1029 g_246 g_68
 */
static int32_t * func_37(int32_t  p_38, const int16_t  p_39, int32_t  p_40, int32_t * p_41, uint16_t  p_42)
{ /* block id: 879 */
    int32_t *l_1986 = &g_28;
    float ***l_2003 = &g_999;
    float l_2016[8];
    int32_t l_2017 = 0x2BAF6106L;
    uint16_t *l_2040 = &g_466;
    int32_t *l_2048 = (void*)0;
    int i;
    for (i = 0; i < 8; i++)
        l_2016[i] = (-0x7.Dp+1);
    for (g_692 = (-14); (g_692 != 7); g_692 = safe_add_func_int64_t_s_s(g_692, 1))
    { /* block id: 882 */
        float l_1978 = 0x4.E30213p-58;
        int32_t *l_1979 = &g_809;
        int32_t l_1982 = 0x7ED8A746L;
        int8_t l_2030[4] = {5L,5L,5L,5L};
        int32_t ***l_2034[8][3] = {{&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290},{&g_1290,&g_1290,&g_1290}};
        int32_t *l_2046 = &l_1982;
        int i, j;
        p_41 = (void*)0;
        if (p_39)
            continue;
        if (((*l_1979) = 0x8B44A659L))
        { /* block id: 886 */
            uint64_t l_1983 = 1UL;
            int32_t l_2018 = 0L;
            int16_t * const *l_2020 = (void*)0;
            const float ****l_2037 = &g_1934;
            int32_t *l_2047 = &g_32;
            for (g_1687 = 0; (g_1687 < 32); ++g_1687)
            { /* block id: 889 */
                for (g_466 = 1; (g_466 <= 5); g_466 += 1)
                { /* block id: 892 */
                    for (g_1607 = 0; (g_1607 <= 5); g_1607 += 1)
                    { /* block id: 895 */
                        int i;
                        --l_1983;
                        (*l_1979) |= (*g_1515);
                    }
                    p_38 ^= 0x0A6BADE8L;
                    return l_1986;
                }
                if (l_1983)
                    continue;
                for (g_1951 = (-4); (g_1951 == 20); g_1951 = safe_add_func_int8_t_s_s(g_1951, 7))
                { /* block id: 905 */
                    p_38 ^= l_1983;
                }
            }
            for (p_40 = 18; (p_40 < (-29)); p_40 = safe_sub_func_int16_t_s_s(p_40, 3))
            { /* block id: 911 */
                uint32_t l_2010 = 0UL;
                int32_t *l_2045 = &l_2018;
                for (l_1983 = (-29); (l_1983 < 25); l_1983 = safe_add_func_uint32_t_u_u(l_1983, 1))
                { /* block id: 914 */
                    uint32_t *l_2013 = &g_673;
                    int32_t l_2019 = (-1L);
                    int8_t *l_2021 = &g_1350;
                    int32_t l_2022 = (-1L);
                    l_2022 ^= (((*l_2021) ^= ((*g_1544) == ((safe_mod_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_s(p_40, 5)) > (((safe_add_func_int32_t_s_s((l_2018 |= (safe_lshift_func_int16_t_s_s((safe_unary_minus_func_int64_t_s((((~(((*l_1979) , l_2003) == ((((safe_sub_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((l_2010 || ((safe_lshift_func_int16_t_s_s(((((((((*l_2013) = g_202[9][1]) , &g_1797) == (void*)0) <= ((l_2017 |= ((safe_div_func_uint16_t_u_u((*g_726), p_38)) <= (*l_1979))) == g_311[2][2][3])) <= l_2010) || p_42) && (**g_1798)), p_40)) | 18446744073709551614UL)), 7)), 4294967288UL)), 1L)) || 0xB5F6L) , 0x0C2A5ADAL) , l_2003))) ^ 9UL) || (*l_1986)))), 7))), 0xDCE920F6L)) || 0UL) != l_2019)), 0x8BA9B0DEL)) , l_2020))) == g_166);
                }
                if (l_2010)
                    continue;
                for (l_1983 = 0; (l_1983 <= 7); l_1983 += 1)
                { /* block id: 924 */
                    const int32_t l_2035 = 0L;
                    for (g_569 = 0; (g_569 <= 7); g_569 += 1)
                    { /* block id: 927 */
                        uint32_t *l_2025 = &g_1362;
                        int32_t l_2036 = (-1L);
                        uint16_t **l_2041 = &l_2040;
                        uint16_t **l_2042 = &g_1029;
                        int i;
                        g_89 ^= (l_2010 , ((*l_1979) = (((safe_lshift_func_int16_t_s_s(((--(*l_2025)) , ((&g_1934 != (((safe_mul_func_int8_t_s_s((l_2030[3] == ((safe_unary_minus_func_uint32_t_u((safe_mul_func_int32_t_s_s((((void*)0 == l_2034[0][0]) != (g_398 , (*l_1986))), (l_2035 >= p_42))))) ^ (***g_1797))), l_2036)) , (-0x8.9p-1)) , l_2037)) > l_2036)), l_2018)) , (*l_1986)) , (*l_1979))));
                        (*l_1979) = ((safe_sub_func_int16_t_s_s(p_40, (*l_1986))) && ((((*l_2041) = l_2040) != ((*l_2042) = (void*)0)) && (safe_div_func_int16_t_s_s(((***g_1544) = 1L), ((*l_2040) = 65535UL)))));
                        (*g_542) = &p_38;
                    }
                    return l_2047;
                }
            }
        }
        else
        { /* block id: 941 */
            if (p_42)
                break;
        }
    }
    l_1986 = &l_2017;
    return l_2048;
}


/* ------------------------------------------ */
/* 
 * reads : g_28 g_32 g_48 g_92 g_19 g_89 g_103 g_129 g_153 g_156 g_159 g_111 g_130 g_202 g_166 g_246 g_232 g_243 g_294 g_334 g_382 g_403 g_417 g_424 g_415 g_398 g_419 g_449 g_458 g_500 g_374 g_522 g_525 g_402 g_311 g_539 g_542 g_68 g_570 g_540 g_599 g_418 g_466 g_673 g_692 g_698 g_701 g_706 g_724 g_725 g_726 g_784 g_569 g_644 g_645 g_1103 g_807 g_1104 g_1105 g_1029 g_1160 g_809 g_1263 g_1334 g_1362 g_377 g_1432 g_998 g_999 g_287 g_1515 g_1543 g_1497 g_1587 g_1350 g_1607 g_455 g_1687 g_1544 g_697 g_1744 g_1790 g_1797 g_1826 g_1835 g_1798 g_1799 g_1913 g_1934 g_1951
 * writes: g_68 g_32 g_89 g_92 g_103 g_48 g_153 g_111 g_159 g_166 g_202 g_232 g_243 g_246 g_129 g_287 g_294 g_311 g_334 g_382 g_403 g_130 g_415 g_458 g_500 g_539 g_398 g_570 g_599 g_466 g_644 g_673 g_692 g_698 g_701 g_706 g_784 g_381 g_569 g_522 g_1103 g_1362 g_809 g_1399 g_1434 g_1334 g_1497 g_1587 g_1607 g_1597 g_1769 g_1790 g_1797 g_1819 g_1835 g_1687 g_1934 g_1826 g_807 g_1951
 */
static uint8_t  func_49(uint32_t  p_50, int32_t  p_51, float  p_52, const int32_t * p_53, int32_t  p_54)
{ /* block id: 12 */
    int8_t l_55[7];
    int16_t *l_1608 = (void*)0;
    int32_t *l_1651 = &g_89;
    int32_t l_1668 = 0xB7E3D89FL;
    int32_t l_1669 = (-9L);
    int32_t l_1672 = 5L;
    uint32_t *l_1722 = &g_673;
    const int8_t *l_1733 = &g_311[0][4][4];
    const int8_t **l_1732 = &l_1733;
    int32_t l_1776 = 0x2023D0F1L;
    int32_t l_1777 = 0x3BB00F5AL;
    int32_t l_1778 = (-1L);
    int32_t l_1779 = (-4L);
    int32_t l_1780 = 0L;
    int32_t l_1781 = 0x2E87A6EDL;
    int32_t l_1782 = 0L;
    int32_t l_1783 = 0L;
    int32_t l_1784 = 0x81B7A9A5L;
    int32_t l_1785 = 0L;
    int32_t l_1786 = 0x59728150L;
    int32_t l_1787 = 0x77F65AC0L;
    int32_t l_1788 = 0x87E5EB0EL;
    int32_t l_1789[2][2][9] = {{{0xD84ADA97L,0xB7ED64ABL,0xB7ED64ABL,0xD84ADA97L,0xB7ED64ABL,0xB7ED64ABL,0xD84ADA97L,0xB7ED64ABL,0xB7ED64ABL},{(-4L),0x5B1C9AD3L,0x5B1C9AD3L,(-4L),0x5B1C9AD3L,0x5B1C9AD3L,(-4L),0x5B1C9AD3L,0x5B1C9AD3L}},{{0xD84ADA97L,0xB7ED64ABL,0xB7ED64ABL,0xD84ADA97L,0xB7ED64ABL,0xB7ED64ABL,0xD84ADA97L,0xB7ED64ABL,0xB7ED64ABL},{(-4L),0x5B1C9AD3L,0x5B1C9AD3L,(-4L),0x5B1C9AD3L,0x5B1C9AD3L,(-4L),0x5B1C9AD3L,0x5B1C9AD3L}}};
    int64_t **l_1794 = (void*)0;
    int64_t *** const l_1793 = &l_1794;
    int64_t * const **l_1817 = (void*)0;
    int64_t * const ***l_1816 = &l_1817;
    int32_t l_1875 = (-3L);
    uint16_t ** const l_1907[9] = {&g_1029,&g_1029,&g_1029,&g_1029,&g_1029,&g_1029,&g_1029,&g_1029,&g_1029};
    float l_1969 = (-0x8.6p+1);
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_55[i] = 0x86L;
    for (p_50 = 0; (p_50 <= 6); p_50 += 1)
    { /* block id: 15 */
        int32_t *l_67 = &g_32;
        int32_t **l_66[8][2] = {{&l_67,&l_67},{(void*)0,&l_67},{&l_67,&l_67},{&l_67,&l_67},{&l_67,(void*)0},{&l_67,&l_67},{&l_67,&l_67},{&l_67,&l_67}};
        int16_t *l_1596 = &g_1597;
        int64_t *l_1606[3][3][1] = {{{&g_1607},{&g_449},{&g_1607}},{{&g_449},{&g_1607},{&g_449}},{{&g_1607},{&g_449},{&g_1607}}};
        uint16_t ** const *l_1639 = (void*)0;
        uint16_t ** const **l_1638 = &l_1639;
        uint64_t *l_1663 = &g_232;
        uint8_t l_1673 = 255UL;
        const int8_t *l_1721 = (void*)0;
        int8_t *l_1731 = (void*)0;
        int8_t **l_1730 = &l_1731;
        uint8_t *****l_1768 = (void*)0;
        int64_t l_1774 = (-8L);
        int64_t l_1775 = 0xB3DD3E37A8B19E30LL;
        int32_t l_1834 = 0x6F869A51L;
        int32_t ** const **l_1917 = &g_1289;
        uint32_t l_1946[8][1] = {{0xF7CD2BABL},{1UL},{1UL},{0xF7CD2BABL},{1UL},{1UL},{0xF7CD2BABL},{1UL}};
        int i, j, k;
        if ((safe_div_func_uint64_t_u_u(((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u(func_62((g_68 = (void*)0), (l_55[p_50] , g_28), l_55[1]), ((l_55[4] , (l_1596 != (((g_1607 &= (safe_add_func_uint64_t_u_u(((safe_mod_func_int8_t_s_s(((0xAC89L < ((safe_lshift_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s(((*g_726) , 0x441EL), 0L)) , l_55[5]), l_55[1])) , p_50)) ^ 1UL), g_1350)) || p_54), p_54))) , 0xDAL) , l_1608))) != l_55[0]))), 0x27L)) < g_449), l_55[3])))
        { /* block id: 704 */
            uint32_t l_1620 = 0xEFE0A8D4L;
            int32_t l_1670 = 0xCBB2E515L;
            int32_t l_1689 = (-1L);
            uint8_t *l_1752 = &g_500;
            int32_t *l_1772 = &g_89;
            int32_t *l_1773[6];
            int64_t ****l_1857 = (void*)0;
            float l_1908 = (-0x1.Ep-1);
            int i;
            for (i = 0; i < 6; i++)
                l_1773[i] = &l_1668;
            for (g_569 = 4; (g_569 >= 0); g_569 -= 1)
            { /* block id: 707 */
                int32_t l_1640 = (-2L);
                int32_t *l_1652 = &g_809;
                int32_t l_1665 = 0x2D005E3AL;
                int32_t l_1666 = 1L;
                int32_t l_1667 = (-1L);
                int32_t l_1671[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
                int i;
                for (g_1334 = 0; (g_1334 <= 3); g_1334 += 1)
                { /* block id: 710 */
                    int32_t ***l_1614[3][6] = {{&g_1290,&l_66[3][0],&l_66[3][0],&g_1290,&l_66[3][0],&l_66[3][0]},{&g_1290,&l_66[3][0],&l_66[3][0],&g_1290,&l_66[3][0],&l_66[3][0]},{&g_1290,&l_66[3][0],&l_66[3][0],&g_1290,&l_66[3][0],&l_66[3][0]}};
                    int32_t ****l_1613[6][5][4] = {{{&l_1614[0][3],&l_1614[0][3],&l_1614[1][0],&l_1614[0][3]},{&l_1614[0][3],&l_1614[0][3],&l_1614[0][3],(void*)0},{&l_1614[0][3],(void*)0,(void*)0,&l_1614[0][3]},{&l_1614[1][0],(void*)0,&l_1614[0][3],(void*)0},{(void*)0,&l_1614[0][3],&l_1614[0][3],&l_1614[0][3]}},{{&l_1614[1][0],&l_1614[1][0],(void*)0,&l_1614[0][3]},{&l_1614[0][3],&l_1614[0][3],&l_1614[0][3],(void*)0},{&l_1614[0][3],(void*)0,(void*)0,&l_1614[0][3]},{&l_1614[1][0],(void*)0,&l_1614[0][3],(void*)0},{(void*)0,&l_1614[0][3],&l_1614[0][3],&l_1614[0][3]}},{{&l_1614[1][0],&l_1614[1][0],(void*)0,&l_1614[0][3]},{&l_1614[0][3],&l_1614[0][3],&l_1614[0][3],(void*)0},{&l_1614[0][3],(void*)0,(void*)0,&l_1614[0][3]},{&l_1614[1][0],(void*)0,&l_1614[0][3],(void*)0},{(void*)0,&l_1614[0][3],&l_1614[0][3],&l_1614[0][3]}},{{&l_1614[1][0],&l_1614[1][0],(void*)0,&l_1614[0][3]},{&l_1614[0][3],&l_1614[0][3],&l_1614[0][3],(void*)0},{&l_1614[0][3],(void*)0,(void*)0,&l_1614[0][3]},{&l_1614[1][0],(void*)0,&l_1614[0][3],(void*)0},{(void*)0,&l_1614[0][3],&l_1614[0][3],&l_1614[0][3]}},{{&l_1614[1][0],&l_1614[1][0],(void*)0,&l_1614[0][3]},{&l_1614[0][3],&l_1614[0][3],&l_1614[0][3],(void*)0},{&l_1614[0][3],(void*)0,(void*)0,&l_1614[0][3]},{&l_1614[1][0],(void*)0,&l_1614[0][3],(void*)0},{(void*)0,&l_1614[0][3],&l_1614[0][3],&l_1614[0][3]}},{{&l_1614[1][0],&l_1614[1][0],(void*)0,&l_1614[0][3]},{&l_1614[0][3],&l_1614[0][3],&l_1614[0][3],(void*)0},{&l_1614[0][3],(void*)0,(void*)0,&l_1614[0][3]},{&l_1614[1][0],(void*)0,&l_1614[0][3],(void*)0},{(void*)0,&l_1614[0][3],&l_1614[0][3],&l_1614[0][3]}}};
                    int32_t *****l_1615 = &l_1613[0][4][2];
                    float *l_1629 = &g_381[5];
                    uint64_t *l_1664[9] = {&g_232,&g_232,&g_232,&g_232,&g_232,&g_232,&g_232,&g_232,&g_232};
                    int i, j, k;
                    if ((safe_sub_func_uint64_t_u_u(6UL, (((((safe_div_func_float_f_f((&g_1289 == ((*l_1615) = l_1613[4][1][2])), ((safe_div_func_float_f_f(((g_246[g_1334] , g_246[g_1334]) < (-0x1.2p-1)), (safe_div_func_float_f_f(l_1620, (safe_sub_func_float_f_f(((safe_sub_func_float_f_f((safe_sub_func_float_f_f((*g_156), (safe_mul_func_float_f_f(((*l_1629) = p_50), 0x6.D855F1p+29)))), l_55[3])) < p_54), (-0x5.7p-1))))))) >= p_50))) == (-0x1.0p+1)) >= 0xF.C7D830p+64) , 0x4CL) >= l_55[3]))))
                    { /* block id: 713 */
                        return (*l_67);
                    }
                    else
                    { /* block id: 715 */
                        uint64_t *l_1643 = &g_202[3][1];
                        uint8_t ***l_1649 = &g_129[5];
                        int32_t l_1650 = 0x1E892501L;
                        uint64_t **l_1660 = &l_1643;
                        uint64_t *l_1662 = &g_153;
                        uint64_t **l_1661 = &l_1662;
                        (*l_67) = ((p_50 & p_51) & (safe_add_func_int64_t_s_s((p_54 > (((safe_sub_func_uint32_t_u_u((safe_sub_func_int32_t_s_s((safe_sub_func_uint64_t_u_u(((void*)0 == l_1638), l_1640)), (safe_div_func_uint64_t_u_u(((*l_1643) = (p_50 & p_50)), (+((safe_div_func_int64_t_s_s((safe_lshift_func_uint16_t_u_s((l_1649 == (void*)0), 12)), (*l_67))) ^ l_1650)))))), l_1640)) < 8UL) <= 0x4132L)), 18446744073709551615UL)));
                        l_1652 = l_1651;
                        (*l_1651) ^= (safe_lshift_func_uint16_t_u_s(p_51, ((safe_lshift_func_uint16_t_u_u((**g_725), ((**g_1105) = (~(*p_53))))) < (safe_add_func_uint64_t_u_u(18446744073709551612UL, ((l_1663 = ((*l_1661) = ((*l_1660) = (void*)0))) != l_1664[0]))))));
                    }
                    if ((*l_1652))
                        continue;
                    --l_1673;
                }
                p_53 = &l_1669;
                for (l_1665 = 6; (l_1665 >= 0); l_1665 -= 1)
                { /* block id: 731 */
                    float l_1676 = 0x8.C8A4A6p+48;
                    uint32_t *l_1685 = &g_1362;
                    uint32_t **l_1686 = &l_1685;
                    uint64_t *l_1688 = (void*)0;
                    int32_t l_1690 = 8L;
                    int32_t **l_1703 = (void*)0;
                    int32_t l_1707 = 0xDDDB4184L;
                    (*l_67) ^= (((l_1689 ^= ((*l_1651) & ((((((g_103 && (p_50 | (p_50 == (*g_418)))) , ((safe_sub_func_uint16_t_u_u(((((((safe_sub_func_int8_t_s_s(0x2AL, (safe_sub_func_int64_t_s_s((((safe_rshift_func_int16_t_s_u(((*l_1596) = (g_455 >= ((((*l_1686) = l_1685) != p_53) ^ 1L))), 2)) || (*l_1652)) & g_1687), 0x6CC241F18E0C3A04LL)))) || p_54) , 5L) | l_1670) , (*p_53)) | 0x7D3A3918L), 3L)) , (***g_724))) ^ 3UL) ^ (*p_53)) == g_1334) < 0xDAF5L))) , (-8L)) > (*l_1652));
                    for (g_1597 = 0; (g_1597 <= 2); g_1597 += 1)
                    { /* block id: 738 */
                        (*l_1652) = (p_51 != (((****g_1543) && l_1690) != p_50));
                        if ((*l_1652))
                            break;
                    }
                    (*l_1651) &= ((*l_67) = 0x8CFB007AL);
                    for (l_1640 = 0; (l_1640 <= 2); l_1640 += 1)
                    { /* block id: 746 */
                        uint16_t l_1706[4];
                        int32_t l_1708[1][2][6] = {{{0xB6DE2392L,0x3C80F93DL,0xB6DE2392L,(-1L),(-1L),0xB6DE2392L},{0xAAAB27FFL,0xAAAB27FFL,(-1L),0xAD852191L,(-1L),0xAAAB27FFL}}};
                        const int32_t **l_1710 = &g_1399[0];
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                            l_1706[i] = 0x536CL;
                        l_1708[0][0][0] ^= ((safe_div_func_uint64_t_u_u((p_54 , (safe_rshift_func_int8_t_s_s(((0x36BFL < ((*p_53) != (l_1707 ^= (safe_sub_func_int16_t_s_s(0xCC3FL, (((safe_rshift_func_int16_t_s_u(p_51, (safe_sub_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s(0x09L, 2)), ((*l_1685) ^= (l_1703 == (void*)0)))))) ^ ((safe_mod_func_int8_t_s_s(g_599, p_51)) > l_1706[1])) , p_54)))))) == l_1670), 1))), l_1670)) <= (***g_1104));
                        (*l_1710) = p_53;
                    }
                }
            }
            for (l_1689 = 4; (l_1689 >= 1); l_1689 -= 1)
            { /* block id: 756 */
                int8_t *l_1719 = &l_55[p_50];
                int8_t **l_1720 = &l_1719;
                int32_t l_1736 = 0xE1265697L;
                int32_t * const l_1737 = &l_1670;
                int32_t *l_1741[2][2][2] = {{{&g_32,&g_32},{&g_32,&g_32}},{{&g_32,&g_32},{&g_32,&g_32}}};
                uint16_t ** const *l_1763[2][5][3] = {{{&g_1435[1][0][1],&g_1435[9][5][1],(void*)0},{&g_1435[5][5][0],&g_1435[1][0][1],&g_1435[1][3][0]},{&g_1435[5][5][0],&g_1435[5][5][0],&g_1435[1][0][1]},{&g_1435[1][0][1],&g_1435[1][0][1],&g_1435[1][0][1]},{&g_1435[1][0][1],&g_1435[9][5][1],&g_1435[1][3][0]}},{{&g_1435[1][0][1],&g_1435[9][5][1],(void*)0},{&g_1435[5][5][0],&g_1435[1][0][1],&g_1435[1][3][0]},{&g_1435[5][5][0],&g_1435[5][5][0],&g_1435[1][0][1]},{&g_1435[1][0][1],&g_1435[1][0][1],&g_1435[1][0][1]},{&g_1435[1][0][1],&g_1435[9][5][1],&g_1435[1][3][0]}}};
                int32_t ***l_1765 = &l_66[2][0];
                int i, j, k;
                if (((safe_mod_func_int16_t_s_s(((*l_1596) = (safe_sub_func_uint8_t_u_u(1UL, ((l_1651 = &l_1669) != p_53)))), (safe_sub_func_uint16_t_u_u(0x38F7L, (g_48 != (((safe_rshift_func_uint16_t_u_u(p_50, ((((*l_1720) = l_1719) == (l_1721 = &g_311[1][8][6])) < (&g_1587 != l_1722)))) ^ g_697[3][0]) < l_1620)))))) && 0UL))
                { /* block id: 761 */
                    int32_t l_1729[3][9] = {{1L,1L,1L,1L,1L,1L,1L,1L,1L},{0xD7312256L,0xD7312256L,1L,0xD7312256L,0xD7312256L,1L,0xD7312256L,0x5758E027L,0xD7312256L},{(-6L),(-6L),1L,(-6L),(-6L),1L,(-6L),(-6L),1L}};
                    int32_t l_1740[3];
                    int32_t l_1750 = 0x0BC23B8EL;
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1740[i] = 0x0CBFBA62L;
                    (*l_1651) &= ((+((safe_div_func_int64_t_s_s((safe_rshift_func_int8_t_s_s(((((((+(l_1729[1][7] != p_51)) , (0x3C68L == (((((l_1730 == (((void*)0 == &l_55[3]) , l_1732)) | (safe_add_func_uint16_t_u_u(p_54, (l_1736 || (*g_415))))) , 0xEA4B6F13L) >= g_809) || 0xEC53DBBAE1AA8B74LL))) , (*g_540)) && p_50) , l_1737) != p_53), 1)), g_522[0][1][3])) > g_449)) == p_54);
                    for (g_698 = 0; (g_698 <= 2); g_698 += 1)
                    { /* block id: 765 */
                        uint64_t **l_1745 = (void*)0;
                        uint64_t **l_1746 = &l_1663;
                        uint16_t ** const **l_1764 = &l_1763[1][3][1];
                        int i;
                        l_1750 = (((l_1740[0] = (((*g_130) = (0x17F91EF40A0F06FALL & (*l_1737))) == (p_51 < g_202[7][1]))) > ((void*)0 != l_1741[1][1][0])) || (((((((safe_sub_func_float_f_f((((*l_1746) = g_1744) != ((safe_rshift_func_uint8_t_u_s(((safe_unary_minus_func_int16_t_s((p_54 | (**g_725)))) & g_784), p_50)) , (void*)0)), g_92)) , 0UL) != 0x860EL) && (*l_1651)) , 0x8B9479A6L) & 4294967295UL) || p_50));
                        (*l_1737) &= (l_1729[1][5] != ((((safe_unary_minus_func_int32_t_s((l_1750 = (l_1752 != ((((***g_1104) && ((***g_1544) |= (safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_s((&p_53 == (void*)0), 15)), ((((safe_mod_func_int64_t_s_s((((l_1750 == 0x543EF373CE15EFE0LL) && (safe_sub_func_uint8_t_u_u(((*g_130) = p_50), ((safe_mod_func_int16_t_s_s((*l_1651), p_54)) <= p_54)))) != 0x47L), 0xEAD07C8073E4062ALL)) > 0xFE5B2F0DL) , (-10L)) || p_54))))) != 0x269955A005B19AE4LL) , (void*)0))))) > p_54) | g_698) ^ (-1L)));
                        (*l_67) &= l_1689;
                        (*l_1651) |= (((((*l_1638) = &g_1435[0][3][1]) == ((*l_1764) = l_1763[1][3][1])) && ((void*)0 == l_1765)) & (safe_rshift_func_uint8_t_u_s(p_54, 5)));
                    }
                }
                else
                { /* block id: 779 */
                    g_1769 = l_1768;
                }
                l_1773[2] = (l_1772 = (void*)0);
                g_1790++;
            }
            for (g_466 = 0; (g_466 <= 1); g_466 += 1)
            { /* block id: 788 */
                int32_t *l_1802 = &l_1689;
                uint64_t l_1825 = 0x7847C69126A6B6C8LL;
                int32_t l_1827 = (-2L);
                int32_t l_1828 = 6L;
                int32_t l_1829[4][6] = {{(-6L),(-8L),(-5L),0x618C75D1L,(-5L),(-8L)},{(-5L),(-6L),(-1L),0L,0L,(-1L)},{(-5L),(-5L),0L,0x618C75D1L,4L,0x618C75D1L},{(-6L),(-5L),(-6L),(-1L),0L,0L}};
                int64_t ***l_1863 = &l_1794;
                int64_t ****l_1862 = &l_1863;
                int64_t * const ***l_1864 = &l_1817;
                int i, j;
                for (g_1790 = 2; (g_1790 <= 6); g_1790 += 1)
                { /* block id: 791 */
                    int64_t ***l_1796 = (void*)0;
                    int64_t ****l_1795 = &l_1796;
                    const volatile uint32_t ****l_1800 = &g_1797;
                    for (l_1783 = 0; (l_1783 <= 2); l_1783 += 1)
                    { /* block id: 794 */
                        return p_54;
                    }
                    (*l_1795) = l_1793;
                    (*l_1800) = g_1797;
                }
                for (p_54 = 0; (p_54 <= 1); p_54 += 1)
                { /* block id: 802 */
                    int32_t *l_1801 = &l_1776;
                    int32_t l_1830 = (-9L);
                    int32_t l_1831 = 0x7B449768L;
                    int32_t l_1832 = 0x8A1DB3D8L;
                    int32_t l_1833[4][5] = {{0x46AC3832L,0x46AC3832L,0x46AC3832L,0x46AC3832L,0x46AC3832L},{0xE09FFDF0L,1L,0xE09FFDF0L,1L,0xE09FFDF0L},{0x46AC3832L,0x46AC3832L,0x46AC3832L,0x46AC3832L,0x46AC3832L},{0xE09FFDF0L,1L,0xE09FFDF0L,1L,0xE09FFDF0L}};
                    int i, j, k;
                    for (g_809 = 0; (g_809 <= 1); g_809 += 1)
                    { /* block id: 805 */
                        uint64_t *l_1809[3][3] = {{&g_599,&g_599,&g_599},{&g_1790,&g_92,&g_1790},{&g_599,&g_599,&g_599}};
                        int64_t * const ****l_1818[5][3][4] = {{{&l_1816,&l_1816,&l_1816,&l_1816},{&l_1816,&l_1816,&l_1816,&l_1816},{&l_1816,&l_1816,&l_1816,&l_1816}},{{&l_1816,&l_1816,(void*)0,&l_1816},{&l_1816,&l_1816,&l_1816,&l_1816},{&l_1816,&l_1816,(void*)0,&l_1816}},{{&l_1816,&l_1816,&l_1816,&l_1816},{&l_1816,&l_1816,&l_1816,(void*)0},{&l_1816,&l_1816,&l_1816,&l_1816}},{{(void*)0,&l_1816,&l_1816,&l_1816},{(void*)0,&l_1816,(void*)0,&l_1816},{&l_1816,&l_1816,(void*)0,&l_1816}},{{(void*)0,&l_1816,&l_1816,(void*)0},{(void*)0,&l_1816,&l_1816,(void*)0},{&l_1816,(void*)0,&l_1816,&l_1816}}};
                        int32_t l_1824 = 0L;
                        int i, j, k;
                        l_1802 = l_1801;
                        (*g_1160) = (safe_lshift_func_int16_t_s_u(((*l_1596) = ((safe_rshift_func_uint16_t_u_s((((safe_mod_func_uint64_t_u_u((g_1790 |= (*g_1744)), (safe_mul_func_uint8_t_u_u(p_54, 0x27L)))) , (*p_53)) && (6L != (0x3B4BL | ((safe_mul_func_int8_t_s_s(((safe_add_func_int8_t_s_s((&g_1263[0] == (g_1819 = l_1816)), (safe_div_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s(0x99C0L, l_1824)), l_1825)))) ^ (-7L)), p_54)) ^ g_1826)))), p_54)) != p_51)), 2));
                    }
                    ++g_1835;
                }
                for (l_1672 = 6; (l_1672 >= 0); l_1672 -= 1)
                { /* block id: 816 */
                    uint32_t l_1838 = 3UL;
                    int64_t *****l_1858 = &l_1857;
                    int64_t ***l_1861[1];
                    int64_t ****l_1860 = &l_1861[0];
                    int64_t *****l_1859[2][5];
                    int64_t * const ****l_1865[7] = {&l_1864,&l_1864,&l_1864,&l_1864,&l_1864,&l_1864,&l_1864};
                    uint32_t *l_1868 = (void*)0;
                    uint32_t *l_1869[7];
                    uint64_t l_1870 = 18446744073709551615UL;
                    uint32_t l_1871 = 0UL;
                    float l_1874 = 0x4.5BBD64p-82;
                    int32_t l_1876 = 0xAD7D1324L;
                    int32_t l_1877 = 0x83D45378L;
                    int32_t l_1878[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
                    int32_t l_1879 = (-1L);
                    int32_t l_1880 = 0xE2E6E4F5L;
                    int32_t l_1882 = 0xC8635964L;
                    int32_t l_1883 = 0x520F83EDL;
                    uint64_t l_1885 = 4UL;
                    int32_t ****l_1916 = &g_1914;
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_1861[i] = &l_1794;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_1859[i][j] = &l_1860;
                    }
                    for (i = 0; i < 7; i++)
                        l_1869[i] = &g_1362;
                    (*g_156) = l_1838;
                    l_1871 = (safe_div_func_int16_t_s_s(((l_55[(g_466 + 1)] = (safe_mod_func_int16_t_s_s((safe_mod_func_uint32_t_u_u((safe_lshift_func_int16_t_s_s(p_54, ((((*g_1744) = (safe_add_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((safe_div_func_int32_t_s_s((safe_div_func_uint16_t_u_u((**g_1105), 0x1D49L)), (-5L))), (safe_add_func_int64_t_s_s(((l_1862 = ((*l_1858) = l_1857)) == (l_1816 = l_1864)), (safe_mul_func_uint8_t_u_u(((*l_1802) = p_50), (((((*l_1651) = p_50) , (*l_1651)) || p_50) & g_166))))))), l_1870))) , 0x0CE5AD0CL) <= (**g_1798)))), 0x5C836CE6L)), l_1870))) ^ g_166), p_54));
                    for (l_1776 = 0; (l_1776 <= 0); l_1776 += 1)
                    { /* block id: 828 */
                        int32_t l_1872 = 8L;
                        int32_t l_1873 = 0xD77AB75DL;
                        int32_t l_1881 = 1L;
                        int32_t l_1884 = 4L;
                        uint8_t *l_1909 = &g_1687;
                        int i, j, k;
                        l_1885--;
                        if (l_1884)
                            continue;
                        l_1828 = (safe_add_func_uint16_t_u_u((l_1873 = ((safe_add_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(8UL, (((safe_sub_func_uint64_t_u_u((safe_rshift_func_int8_t_s_s((((*g_1029) = (((safe_mul_func_uint16_t_u_u((*l_1651), (safe_mod_func_int64_t_s_s(((*g_415) &= (((*l_1909) = ((safe_unary_minus_func_uint8_t_u(((*l_1752) = ((0xF3L & ((((safe_mul_func_uint8_t_u_u((--(*g_130)), ((***g_1544) != ((l_1872 , (((l_1884 != ((*l_1802) >= ((**g_1103) != l_1907[1]))) != l_1908) , (void*)0)) != (void*)0)))) , p_51) != (*l_67)) ^ 0x15DDL)) != 253UL)))) || (**g_1798))) || p_50)), 0x839FD211EF97BCB1LL)))) < 0xDDF85B02L) > 0x7DEF901CCB75A453LL)) > 0x21B3L), g_246[0])), l_1884)) == p_54) < p_51))), p_50)) , 0x6287L)), p_54));
                    }
                    if ((*l_1651))
                        break;
                    for (g_458 = 0; (g_458 <= 0); g_458 += 1)
                    { /* block id: 842 */
                        int32_t *****l_1915[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1915[i] = &g_1913;
                        (*l_1651) &= ((safe_mul_func_int16_t_s_s(((+((l_1916 = g_1913) != l_1917)) | ((*g_415) , 0UL)), (*g_726))) , (*l_67));
                    }
                }
            }
        }
        else
        { /* block id: 848 */
            uint32_t l_1920 = 1UL;
            float *l_1923 = (void*)0;
            float *l_1924 = &g_111[5];
            uint32_t *l_1928 = (void*)0;
            uint32_t *l_1929 = &g_1362;
            const float ****l_1935 = &g_1934;
            int32_t l_1941 = 1L;
            int32_t l_1944 = 3L;
            int32_t l_1949 = 0x5D1AFC43L;
            int32_t l_1950 = 0xA01EED76L;
            int8_t ***l_1956 = &l_1730;
            int i;
            (*l_67) = (safe_mul_func_int8_t_s_s((l_55[p_50] |= (((*l_1651) = l_1920) , ((*g_1799) ^ (*p_53)))), (((((safe_lshift_func_uint16_t_u_s(((p_51 < ((*l_1924) = l_1672)) , (safe_unary_minus_func_uint64_t_u((safe_div_func_uint32_t_u_u(((*l_1929) = 0UL), (l_1920 , (safe_mul_func_uint8_t_u_u(p_50, (safe_mod_func_int8_t_s_s(((((*l_1935) = g_1934) != &g_999) >= 1UL), 0x78L)))))))))), p_50)) , 18446744073709551615UL) , 0xEDACL) <= 0x8CF1L) ^ l_1920)));
            for (g_32 = 0; (g_32 > (-19)); --g_32)
            { /* block id: 857 */
                const int32_t **l_1940[8];
                int32_t l_1942 = 0xEE01E6E8L;
                int32_t l_1943 = (-4L);
                int32_t l_1945[3];
                int i;
                for (i = 0; i < 8; i++)
                    l_1940[i] = &g_1399[0];
                for (i = 0; i < 3; i++)
                    l_1945[i] = 0x672A6DFEL;
                for (g_232 = 0; (g_232 != 15); g_232++)
                { /* block id: 860 */
                    for (g_1826 = 0; g_1826 < 6; g_1826 += 1)
                    {
                        for (g_807 = 0; g_807 < 1; g_807 += 1)
                        {
                            for (g_398 = 0; g_398 < 3; g_398 += 1)
                            {
                                g_1497[g_1826][g_807][g_398] = 0x5507L;
                            }
                        }
                    }
                }
                p_53 = p_53;
                l_1946[4][0]--;
                ++g_1951;
            }
            l_1786 &= ((safe_sub_func_uint8_t_u_u((((*g_1744) |= (((((*l_1956) = &l_1731) != &l_1721) && (((*l_1924) = (safe_div_func_float_f_f((((*g_156) <= (-0x8.9p-1)) == (((safe_sub_func_float_f_f(((*l_1651) = ((((safe_div_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_s(((*g_130) = p_51), (safe_sub_func_int64_t_s_s(p_50, (-4L))))), 11)), (0xB84CL ^ p_51))) <= p_50) , 1UL) , (-0x1.2p-1))), p_50)) < p_54) >= (*l_67))), l_1941))) , (*p_53))) , (*l_1651))) || p_50), p_54)) ^ p_51);
        }
        return p_54;
    }
    return (*g_418);
}


/* ------------------------------------------ */
/* 
 * reads : g_48 g_28 g_92 g_19 g_32 g_89 g_103 g_129 g_153 g_156 g_159 g_111 g_130 g_202 g_166 g_246 g_232 g_243 g_294 g_334 g_382 g_403 g_417 g_424 g_415 g_398 g_419 g_449 g_458 g_500 g_374 g_522 g_525 g_402 g_311 g_539 g_542 g_68 g_570 g_540 g_599 g_418 g_466 g_673 g_692 g_698 g_701 g_706 g_724 g_725 g_726 g_784 g_569 g_644 g_645 g_1103 g_807 g_1104 g_1105 g_1029 g_1160 g_809 g_1263 g_1334 g_1362 g_377 g_1432 g_998 g_999 g_287 g_1515 g_1543 g_1497 g_1587
 * writes: g_32 g_89 g_92 g_103 g_48 g_153 g_111 g_159 g_166 g_202 g_68 g_232 g_243 g_246 g_129 g_287 g_294 g_311 g_334 g_382 g_403 g_130 g_415 g_458 g_500 g_539 g_398 g_570 g_599 g_466 g_644 g_673 g_692 g_698 g_701 g_706 g_784 g_381 g_569 g_522 g_1103 g_1362 g_809 g_1399 g_1434 g_1334 g_1497 g_1587
 */
static uint8_t  func_62(int32_t * p_63, uint64_t  p_64, const int32_t  p_65)
{ /* block id: 17 */
    int32_t l_69[4];
    uint16_t **l_1356 = (void*)0;
    const int32_t l_1393[10][10][2] = {{{0x707C15A1L,0x6311D817L},{0x525AF856L,6L},{1L,0x1DD1F13AL},{0L,0x1DD1F13AL},{1L,6L},{0x525AF856L,0x6311D817L},{0x707C15A1L,0L},{0x7F4C9886L,0xA174D2D7L},{6L,0x84946672L},{1L,0L}},{{1L,0x707C15A1L},{0x6311D817L,6L},{0xE2003B94L,0xA174D2D7L},{0L,0x1DB077E0L},{0x7F4C9886L,1L},{0x525AF856L,0x525AF856L},{0x6311D817L,0L},{0L,0x1DD1F13AL},{1L,0x84946672L},{0xE2003B94L,1L}},{{0x707C15A1L,1L},{0x707C15A1L,1L},{0xE2003B94L,0x84946672L},{1L,0x1DD1F13AL},{0L,0L},{0L,(-1L)},{(-1L),0L},{0x6EAFE57EL,0x3649F3BCL},{1L,1L},{8L,0xAC5497FBL}},{{0L,1L},{0x64DBCCA9L,1L},{(-6L),0x19F0F880L},{0xAC5497FBL,1L},{0x6EAFE57EL,0x8DE20457L},{1L,0L},{(-1L),0xAC5497FBL},{(-6L),0x6311D817L},{0x8DE20457L,0x6311D817L},{(-6L),0xAC5497FBL}},{{(-1L),0L},{1L,0x8DE20457L},{0x6EAFE57EL,1L},{0xAC5497FBL,0x19F0F880L},{(-6L),1L},{0x64DBCCA9L,1L},{0L,0xAC5497FBL},{8L,1L},{1L,0x3649F3BCL},{0x6EAFE57EL,0L}},{{(-1L),(-1L)},{0L,1L},{0x1F5F956DL,0x6311D817L},{0L,0x19F0F880L},{8L,0L},{1L,0x64DBCCA9L},{1L,0L},{8L,0x19F0F880L},{0L,0x6311D817L},{0x1F5F956DL,1L}},{{0L,(-1L)},{(-1L),0L},{0x6EAFE57EL,0x3649F3BCL},{1L,1L},{8L,0xAC5497FBL},{0L,1L},{0x64DBCCA9L,1L},{(-6L),0x19F0F880L},{0xAC5497FBL,1L},{0x6EAFE57EL,0x8DE20457L}},{{1L,0L},{(-1L),0xAC5497FBL},{(-6L),0x6311D817L},{0x8DE20457L,0x6311D817L},{(-6L),0xAC5497FBL},{(-1L),0L},{1L,0x8DE20457L},{0x6EAFE57EL,1L},{0xAC5497FBL,0x19F0F880L},{(-6L),1L}},{{0x64DBCCA9L,1L},{0L,0xAC5497FBL},{8L,1L},{1L,0x3649F3BCL},{0x6EAFE57EL,0L},{(-1L),(-1L)},{0L,1L},{0x1F5F956DL,0x6311D817L},{0L,0x19F0F880L},{8L,0L}},{{1L,0x64DBCCA9L},{1L,0L},{8L,0x19F0F880L},{0L,0x6311D817L},{0x1F5F956DL,1L},{0L,(-1L)},{(-1L),0L},{0x6EAFE57EL,0x3649F3BCL},{1L,1L},{8L,0xAC5497FBL}}};
    int32_t **l_1437 = &g_68;
    const uint8_t **l_1447 = (void*)0;
    const uint8_t ***l_1446 = &l_1447;
    const uint8_t *** const *l_1445 = &l_1446;
    const uint8_t *** const ** const l_1444 = &l_1445;
    const int16_t *l_1499 = (void*)0;
    const int16_t **l_1498[6];
    uint32_t *l_1506 = &g_1362;
    uint64_t l_1569 = 3UL;
    uint8_t l_1595 = 0x60L;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_69[i] = (-1L);
    for (i = 0; i < 6; i++)
        l_1498[i] = &l_1499;
lbl_1586:
    for (p_64 = 0; (p_64 <= 3); p_64 += 1)
    { /* block id: 20 */
        uint32_t *l_1361 = &g_1362;
        int32_t ***l_1365 = &g_1290;
        uint16_t l_1367[10] = {1UL,1UL,0xF564L,0UL,0xF564L,1UL,1UL,0xF564L,0UL,0xF564L};
        uint16_t ***l_1370 = &l_1356;
        uint16_t ****l_1369[3];
        uint16_t *****l_1368[6] = {&l_1369[0],&l_1369[0],&l_1369[0],&l_1369[0],&l_1369[0],&l_1369[0]};
        float l_1403 = 0x9.F197C0p+25;
        int16_t l_1412[4];
        int i;
        for (i = 0; i < 3; i++)
            l_1369[i] = &l_1370;
        for (i = 0; i < 4; i++)
            l_1412[i] = 0xDD2DL;
        if ((l_69[p_64] == ((func_70(p_63, &g_28) | (l_1356 != ((safe_rshift_func_int8_t_s_s(l_69[p_64], (safe_div_func_uint32_t_u_u(((*l_1361)++), (6L & ((void*)0 == l_1365)))))) , (*g_1104)))) , (-1L))))
        { /* block id: 572 */
            return p_64;
        }
        else
        { /* block id: 574 */
            int32_t l_1372 = 0x927DA57FL;
            int32_t l_1374[10];
            int32_t l_1375 = 1L;
            int32_t l_1376 = (-1L);
            uint16_t ***l_1433 = &l_1356;
            int i;
            for (i = 0; i < 10; i++)
                l_1374[i] = 0x41301D34L;
            for (g_809 = 3; (g_809 >= 0); g_809 -= 1)
            { /* block id: 577 */
                float *l_1366[8] = {&g_381[4],&g_381[5],&g_381[4],&g_381[5],&g_381[4],&g_381[5],&g_381[4],&g_381[5]};
                uint8_t l_1373 = 0xC2L;
                const int32_t *l_1398[2][1];
                float ***l_1426 = &g_999;
                int i, j;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1398[i][j] = &l_1393[0][6][0];
                }
                l_1367[6] = 0xC.690740p-78;
                if (((((void*)0 != l_1368[3]) != (((+(l_1376 = ((p_65 > l_1372) < ((*g_156) = (l_1375 = (l_1374[3] = (l_1373 <= p_65))))))) != (p_65 == p_65)) , ((safe_mul_func_int8_t_s_s((((safe_div_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((safe_div_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u(((safe_sub_func_int32_t_s_s((l_69[p_64] = (safe_add_func_int32_t_s_s((safe_rshift_func_uint8_t_u_s(0x7CL, 0)), l_1376))), l_1393[0][6][0])) || 1L), 6)) && 65528UL), p_64)), g_570)), (*g_130))) != l_1374[3]) <= p_64), (-1L))) | 0x8E0EFDFD2D9E8FD8LL))) <= 0UL))
                { /* block id: 584 */
                    for (l_1376 = 0; (l_1376 > (-25)); l_1376--)
                    { /* block id: 587 */
                        int32_t *l_1396 = &l_1376;
                        int32_t **l_1397 = &l_1396;
                        (*l_1397) = l_1396;
                        if (l_1374[3])
                            continue;
                    }
                    if ((((*g_415) = 2L) < p_64))
                    { /* block id: 592 */
                        int64_t l_1400 = 0L;
                        g_1399[0] = l_1398[1][0];
                        (*g_542) = p_63;
                        l_1374[3] = ((l_1400 = p_64) == (safe_mul_func_float_f_f(p_65, (l_1374[3] >= p_64))));
                        return p_64;
                    }
                    else
                    { /* block id: 598 */
                        int64_t l_1404[9][8][3] = {{{1L,(-1L),0x6A50D583329823A5LL},{0x3473A85E372B6963LL,0L,0x989D78310FF9D896LL},{(-1L),0L,0L},{0x41C9D3065BAA08C5LL,0x41C9D3065BAA08C5LL,0x95B64ABBFCDAB655LL},{0x37482E1E71465C91LL,(-1L),(-2L)},{0x6A50D583329823A5LL,(-1L),(-1L)},{(-1L),0L,8L},{1L,0x35DE555756486487LL,(-1L)}},{{(-7L),1L,0L},{(-3L),0L,(-1L)},{0x7A529F4D627C8B86LL,8L,0x3F0EF52134CFB767LL},{0L,(-2L),(-3L)},{(-1L),(-7L),0x35DE555756486487LL},{0xEA3F3B71948502DALL,(-1L),8L},{0xE8344845ECCA6689LL,(-1L),2L},{0x209EFA7B43C95A45LL,(-1L),0x065D1A2B0C4AAEF9LL}},{{(-3L),(-1L),0x41C9D3065BAA08C5LL},{6L,(-7L),0L},{(-1L),(-2L),0x95B64ABBFCDAB655LL},{1L,8L,1L},{2L,0L,8L},{0L,1L,(-1L)},{1L,0x35DE555756486487LL,0x566342A58DF0BD85LL},{(-3L),0L,(-4L)}},{{1L,0x209EFA7B43C95A45LL,(-8L)},{0L,(-2L),(-1L)},{2L,(-8L),0xEA3F3B71948502DALL},{1L,0xE8344845ECCA6689LL,8L},{(-1L),2L,(-1L)},{6L,(-1L),0x141F793AA69E9F5ELL},{(-3L),0x6A50D583329823A5LL,0x3473A85E372B6963LL},{0x209EFA7B43C95A45LL,1L,0x3473A85E372B6963LL}},{{0xE8344845ECCA6689LL,(-2L),0x141F793AA69E9F5ELL},{0xEA3F3B71948502DALL,0x7A529F4D627C8B86LL,(-1L)},{(-1L),1L,8L},{0L,0xEA3F3B71948502DALL,0xEA3F3B71948502DALL},{0x7A529F4D627C8B86LL,(-1L),(-1L)},{(-3L),0L,(-8L)},{(-7L),6L,(-4L)},{1L,(-2L),0x566342A58DF0BD85LL}},{{(-1L),6L,(-1L)},{0x35DE555756486487LL,0L,8L},{0x6A50D583329823A5LL,(-1L),1L},{(-8L),0xEA3F3B71948502DALL,0x95B64ABBFCDAB655LL},{(-3L),1L,0L},{8L,0x7A529F4D627C8B86LL,0x41C9D3065BAA08C5LL},{0L,(-2L),0x065D1A2B0C4AAEF9LL},{(-1L),1L,2L}},{{(-1L),0x6A50D583329823A5LL,8L},{0L,(-1L),0x35DE555756486487LL},{8L,2L,(-3L)},{(-3L),0xE8344845ECCA6689LL,0x3F0EF52134CFB767LL},{(-8L),(-8L),(-1L)},{0x6A50D583329823A5LL,(-2L),0L},{0x35DE555756486487LL,0x209EFA7B43C95A45LL,(-1L)},{(-1L),0L,8L}},{{1L,0x35DE555756486487LL,(-1L)},{(-7L),1L,0L},{(-3L),0L,(-1L)},{0x7A529F4D627C8B86LL,8L,0x3F0EF52134CFB767LL},{0L,(-2L),(-3L)},{(-1L),(-7L),0x35DE555756486487LL},{0xEA3F3B71948502DALL,(-1L),8L},{0xE8344845ECCA6689LL,(-1L),2L}},{{0x209EFA7B43C95A45LL,(-1L),0x065D1A2B0C4AAEF9LL},{(-3L),(-1L),0x41C9D3065BAA08C5LL},{6L,(-7L),0L},{(-1L),(-2L),0x95B64ABBFCDAB655LL},{1L,8L,1L},{2L,1L,0x37482E1E71465C91LL},{1L,0xCA50CB773451F4FALL,(-2L)},{1L,8L,0L}}};
                        int32_t *l_1405 = &l_1374[3];
                        int32_t *l_1427 = (void*)0;
                        int32_t *l_1428 = (void*)0;
                        int32_t *l_1429 = &l_1375;
                        int i, j, k;
                        (*l_1405) = l_1404[2][6][2];
                        (*l_1429) &= (safe_sub_func_uint8_t_u_u((((safe_add_func_float_f_f(0x5.79EED5p+46, (((*g_415) = ((((safe_div_func_uint16_t_u_u((**g_725), p_64)) && (l_1412[1] != (safe_rshift_func_int8_t_s_s((((safe_div_func_uint32_t_u_u(((safe_add_func_float_f_f(l_1393[2][5][1], (p_64 , ((safe_mul_func_float_f_f((((safe_add_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u(((~1L) >= (p_64 >= ((g_246[0] | g_377) && g_311[2][5][3]))), 2)), 0x865479C8L)) , p_64) < l_1393[0][6][0]), l_1393[0][6][0])) < p_65)))) , p_65), 0xB99F7645L)) != p_64) != (*g_415)), 7)))) , p_63) == &l_1375)) , l_69[0]))) , (void*)0) != l_1426), (*l_1405)));
                    }
                }
                else
                { /* block id: 603 */
                    l_69[p_64] = ((*g_1160) = (((g_1434 = ((safe_rshift_func_uint8_t_u_u((1L >= g_1432), 1)) , l_1433)) != (*g_1103)) == 6UL));
                    return (*g_130);
                }
                p_63 = p_63;
            }
            for (l_1372 = 0; l_1372 < 10; l_1372 += 1)
            {
                l_1374[l_1372] = 0x3054E823L;
            }
            (*g_542) = &l_69[p_64];
        }
        return l_1393[0][6][0];
    }
lbl_1529:
    (*l_1437) = &l_69[2];
    for (g_1334 = 0; (g_1334 != 14); ++g_1334)
    { /* block id: 619 */
        uint16_t ****l_1450[6] = {&g_1434,&g_1434,&g_1434,&g_1434,&g_1434,&g_1434};
        int32_t l_1458 = (-2L);
        int32_t l_1459[8][5][5] = {{{0x400FF370L,1L,0x6DA8DBCAL,0x6DA8DBCAL,1L},{(-7L),0x8D8552E6L,0x6DA8DBCAL,0x8A860B28L,0L},{(-1L),(-7L),1L,0x977861F7L,0x8A860B28L},{0x020D7777L,(-1L),0L,0x400FF370L,1L},{(-1L),0x400FF370L,0xEEF011EEL,1L,0xEEF011EEL}},{{(-7L),(-7L),0x979D2F05L,1L,0x83BBE8EEL},{0x400FF370L,(-1L),0x8D8552E6L,0x400FF370L,(-7L)},{(-1L),0x020D7777L,0x6DA8DBCAL,0x977861F7L,0x020D7777L},{(-7L),(-1L),1L,0x8A860B28L,0x8A860B28L},{0x8D8552E6L,(-7L),0x8D8552E6L,0x6DA8DBCAL,0x8A860B28L}},{{1L,0x400FF370L,0L,(-1L),0x020D7777L},{(-1L),(-1L),(-1L),1L,(-7L)},{0xEEF011EEL,(-7L),0L,0x020D7777L,0x83BBE8EEL},{(-1L),0x8D8552E6L,0x8D8552E6L,(-1L),0xEEF011EEL},{(-1L),1L,1L,0x977861F7L,1L}},{{0xEEF011EEL,(-1L),0x6DA8DBCAL,0L,0x8A860B28L},{(-1L),0xEEF011EEL,0x8D8552E6L,0x977861F7L,0L},{1L,(-1L),0x979D2F05L,(-1L),1L},{0x8D8552E6L,(-1L),0xEEF011EEL,0x020D7777L,(-7L)},{(-7L),0xEEF011EEL,0L,1L,(-1L)}},{{(-1L),(-1L),1L,(-1L),(-7L)},{0x400FF370L,1L,0x6DA8DBCAL,0x6DA8DBCAL,1L},{(-7L),0x8D8552E6L,0x6DA8DBCAL,0x8A860B28L,0L},{(-1L),(-7L),1L,0x977861F7L,0x8A860B28L},{0x020D7777L,(-1L),0L,0x400FF370L,1L}},{{(-1L),0x400FF370L,0xEEF011EEL,1L,0xEEF011EEL},{(-7L),(-7L),0x979D2F05L,1L,0x83BBE8EEL},{0x400FF370L,(-1L),0x8D8552E6L,0x400FF370L,(-7L)},{(-1L),0x020D7777L,0x6DA8DBCAL,0x977861F7L,0x020D7777L},{(-7L),(-1L),1L,0x8A860B28L,0x8A860B28L}},{{0x8D8552E6L,(-7L),0x8D8552E6L,0x6DA8DBCAL,0x8A860B28L},{1L,0x400FF370L,0L,(-1L),0x020D7777L},{(-1L),(-1L),(-1L),1L,(-7L)},{0xEEF011EEL,(-7L),0L,0x020D7777L,0x83BBE8EEL},{(-1L),0x8D8552E6L,0x8D8552E6L,(-1L),0xEEF011EEL}},{{(-1L),0x6DA8DBCAL,0x8A860B28L,0L,0x6DA8DBCAL},{0x55158F45L,0x020D7777L,0x979D2F05L,(-1L),0xEEF011EEL},{0x020D7777L,0x55158F45L,0xDBB64BE6L,0L,(-1L)},{0x6DA8DBCAL,0x8D8552E6L,(-7L),0x8D8552E6L,0x6DA8DBCAL},{0xDBB64BE6L,0x8D8552E6L,0x55158F45L,1L,(-1L)}}};
        uint8_t l_1473 = 249UL;
        int8_t l_1485 = 0L;
        int64_t **l_1505 = &g_415;
        int32_t * const *l_1547 = &g_68;
        uint8_t ** const **l_1592 = (void*)0;
        int i, j, k;
        for (g_692 = 29; (g_692 < 10); --g_692)
        { /* block id: 622 */
            uint32_t *l_1448 = (void*)0;
            uint32_t *l_1449 = &g_1362;
            int64_t l_1460 = (-8L);
            int32_t l_1466[5][7] = {{0x24E482DBL,0x24E482DBL,0x1C243047L,0x6D538406L,2L,0x6D538406L,0x1C243047L},{0x24E482DBL,0x24E482DBL,0x1C243047L,0x6D538406L,2L,0x6D538406L,0x1C243047L},{0x24E482DBL,0x24E482DBL,0x1C243047L,0x6D538406L,2L,0x6D538406L,0x1C243047L},{0x24E482DBL,0x24E482DBL,0x1C243047L,0x6D538406L,2L,0x6D538406L,0x1C243047L},{0x24E482DBL,0x24E482DBL,0x1C243047L,0x6D538406L,2L,0x6D538406L,0x1C243047L}};
            uint64_t l_1486 = 0xE8C32D84B5EC1B4CLL;
            int16_t * const l_1496 = &g_1497[4][0][0];
            int16_t * const *l_1495 = &l_1496;
            uint16_t *****l_1526 = &l_1450[4];
            uint8_t ** const *l_1591[1][6][3] = {{{&g_129[4],&g_129[4],&g_129[4]},{(void*)0,&g_129[4],(void*)0},{&g_129[4],&g_129[4],&g_129[4]},{&g_129[5],&g_129[4],&g_129[5]},{&g_129[4],&g_129[4],&g_129[4]},{(void*)0,&g_129[4],(void*)0}}};
            uint8_t ** const **l_1590 = &l_1591[0][0][2];
            int i, j, k;
            if ((safe_lshift_func_int8_t_s_u(g_166, ((((*l_1449) ^= ((void*)0 != l_1444)) , l_1450[4]) != l_1450[4]))))
            { /* block id: 624 */
                int32_t l_1453 = 0x41E53C88L;
                int32_t l_1461 = 0x80B0C185L;
                int32_t l_1462 = 5L;
                int32_t l_1463 = (-7L);
                int32_t l_1464 = 0x7C9A0416L;
                int32_t l_1467 = (-4L);
                int32_t l_1470 = 3L;
                int32_t l_1472 = 0x32DC8028L;
                int32_t *l_1476 = &g_32;
                int32_t *l_1477 = &l_1462;
                int32_t *l_1478 = &l_1458;
                int32_t *l_1479 = &g_89;
                int32_t *l_1480 = &g_32;
                int32_t *l_1481 = &l_1467;
                int32_t *l_1482 = (void*)0;
                int32_t *l_1483 = &l_1467;
                int32_t *l_1484[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int16_t * const l_1493 = &g_692;
                int16_t * const *l_1492 = &l_1493;
                int16_t * const **l_1494[7] = {&l_1492,&l_1492,&l_1492,&l_1492,&l_1492,&l_1492,&l_1492};
                int i;
                for (g_32 = 2; (g_32 != (-27)); --g_32)
                { /* block id: 627 */
                    int64_t l_1465[8][8] = {{0L,1L,0xDB5333C54B14838FLL,(-1L),(-9L),(-1L),0xDB5333C54B14838FLL,1L},{0L,0L,0x9D49A25E6400AF02LL,0xD56BA15C2CDD5736LL,0xDB5333C54B14838FLL,0xF8D136137FA84E0ELL,0xE543517AF55CD756LL,1L},{0xE543517AF55CD756LL,0xF06B58FF11808507LL,0L,(-1L),0L,0xF06B58FF11808507LL,0xE543517AF55CD756LL,0L},{(-1L),(-1L),0x9D49A25E6400AF02LL,0xF8D136137FA84E0ELL,0x926A83F1E8179D0ELL,0xF06B58FF11808507LL,0xDB5333C54B14838FLL,0xF06B58FF11808507LL},{0x926A83F1E8179D0ELL,0xF06B58FF11808507LL,0xDB5333C54B14838FLL,0xF06B58FF11808507LL,0x926A83F1E8179D0ELL,0xF8D136137FA84E0ELL,0x9D49A25E6400AF02LL,(-1L)},{(-1L),0L,0xE543517AF55CD756LL,0xF06B58FF11808507LL,0L,(-1L),0L,0xF06B58FF11808507LL},{0xE543517AF55CD756LL,1L,0xE543517AF55CD756LL,0xF8D136137FA84E0ELL,0xDB5333C54B14838FLL,0xD56BA15C2CDD5736LL,0x9D49A25E6400AF02LL,0L},{0L,1L,0xDB5333C54B14838FLL,(-1L),(-9L),(-1L),0xDB5333C54B14838FLL,1L}};
                    int32_t l_1468 = 0L;
                    int32_t l_1469 = 0xB0D1A175L;
                    int32_t l_1471 = (-1L);
                    int i, j;
                    (*g_68) = p_64;
                    (**l_1437) = l_1453;
                    for (g_92 = (-23); (g_92 >= 6); g_92 = safe_add_func_int16_t_s_s(g_92, 1))
                    { /* block id: 632 */
                        uint32_t l_1456 = 18446744073709551615UL;
                        int32_t *l_1457[6][7] = {{&g_89,&g_89,&g_32,&l_69[2],&g_28,&g_28,&g_89},{&g_89,&g_28,&l_69[1],&g_28,&l_69[1],&l_69[1],&g_28},{&g_809,&g_809,&g_809,&g_28,&g_28,&g_809,&g_32},{&g_28,&g_809,&g_32,&l_69[2],&g_809,&g_32,&g_809},{&g_809,&g_28,&g_28,&g_809,&g_32,&g_32,&g_809},{&g_32,&g_28,&g_32,&l_69[0],&g_809,&g_32,&g_32}};
                        int i, j;
                        (**g_525) = l_1456;
                        --l_1473;
                    }
                    return l_1469;
                }
                l_1486++;
                (*g_287) = ((safe_add_func_float_f_f((((-((l_1495 = l_1492) != ((l_1459[2][4][0] || 0x12L) , l_1498[3]))) < (***g_998)) >= ((0x0.A74DCEp-76 <= ((safe_add_func_float_f_f((((g_522[1][1][0] != ((+((((*g_999) == (void*)0) != g_246[0]) < 0x02L)) <= l_1459[5][1][4])) , (void*)0) == (void*)0), (**g_999))) != p_65)) >= (-0x1.0p-1))), 0x8.5p+1)) > p_64);
            }
            else
            { /* block id: 641 */
                uint16_t ***l_1527 = &l_1356;
                int32_t l_1528 = 0x131B6DB3L;
                int16_t ***l_1546 = (void*)0;
                int16_t ****l_1545 = &l_1546;
                int32_t l_1563 = 1L;
                int32_t l_1565 = 0xCDC7F958L;
                int8_t *l_1581[3][9][4] = {{{&g_1350,(void*)0,&g_311[2][0][2],&l_1485},{&l_1485,&g_311[2][1][4],&l_1485,(void*)0},{&g_1350,&g_1350,&g_311[1][6][3],&g_311[2][1][4]},{&l_1485,&g_311[1][6][5],&g_311[2][1][4],&g_1350},{&g_311[2][1][4],&g_311[0][6][5],&g_311[2][1][4],&l_1485},{&l_1485,(void*)0,&g_311[1][6][3],(void*)0},{&g_1350,&g_311[2][1][4],&l_1485,&g_311[2][2][2]},{&l_1485,&g_311[2][2][2],&g_311[2][0][2],&g_311[2][1][4]},{&g_1350,&g_311[2][1][4],(void*)0,&g_311[2][0][4]}},{{(void*)0,(void*)0,(void*)0,(void*)0},{&g_311[2][1][4],&l_1485,&l_1485,(void*)0},{&g_311[2][1][4],&l_1485,&g_1350,&g_1350},{&g_1350,(void*)0,&g_1350,&g_1350},{&g_311[2][1][4],&g_311[2][1][4],&l_1485,(void*)0},{&g_311[2][2][2],(void*)0,&g_311[0][6][5],&l_1485},{(void*)0,&g_1350,&l_1485,&g_311[0][6][5]},{&g_311[2][1][4],&g_1350,(void*)0,&l_1485},{&g_1350,(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_311[2][1][4],(void*)0,&g_1350},{&g_311[2][1][4],(void*)0,(void*)0,&g_1350},{&g_311[2][0][4],&l_1485,(void*)0,(void*)0},{(void*)0,&l_1485,(void*)0,(void*)0},{(void*)0,(void*)0,&g_1350,&g_311[2][0][4]},{&g_311[2][1][4],&g_311[2][1][4],(void*)0,&g_311[2][1][4]},{&g_311[2][1][4],&g_311[2][2][2],&g_311[2][1][4],&g_311[2][2][2]},{(void*)0,&g_311[2][1][4],(void*)0,(void*)0},{&l_1485,(void*)0,&g_311[2][1][4],&l_1485}}};
                int i, j, k;
                if ((safe_add_func_int8_t_s_s(((**l_1437) ^ (((**l_1437) , l_1505) == (void*)0)), ((&l_1459[5][0][2] != ((l_1506 == (void*)0) , (void*)0)) , ((void*)0 != &l_1473)))))
                { /* block id: 642 */
                    uint8_t * const ***l_1507 = &g_644;
                    uint8_t * const ****l_1508 = &l_1507;
                    (*l_1508) = l_1507;
                    for (g_153 = 0; (g_153 > 23); g_153 = safe_add_func_int32_t_s_s(g_153, 9))
                    { /* block id: 646 */
                        int64_t l_1511 = 0x0EDABB6F013E9394LL;
                        l_1511 &= (**l_1437);
                    }
                    for (g_466 = 10; (g_466 <= 56); ++g_466)
                    { /* block id: 651 */
                        int32_t *l_1514 = (void*)0;
                        (*g_1515) &= (**l_1437);
                    }
                }
                else
                { /* block id: 654 */
                    uint64_t l_1534 = 0x53653DAD1BDEAE52LL;
                    int32_t l_1564 = 1L;
                    uint32_t l_1566 = 4294967293UL;
                    if (((**l_1437) = (safe_mod_func_uint64_t_u_u((safe_sub_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((l_1528 = ((*g_130) = (((safe_add_func_int16_t_s_s((((((void*)0 == l_1526) , l_1527) != l_1527) && (((**g_999) = (l_1486 == (((0x985B2220L <= g_398) , p_65) >= (**l_1437)))) , 0xCD797916D572D7C1LL)), 0xA109L)) , (-9L)) < 0xC7DC428EL))), p_64)), 4)), 0xD08DA151A6DB041ELL)), l_1486))))
                    { /* block id: 659 */
                        int32_t *l_1530 = (void*)0;
                        int32_t *l_1531 = &l_1466[2][2];
                        int32_t *l_1532 = &l_1459[2][2][0];
                        int32_t *l_1533[5] = {&l_1458,&l_1458,&l_1458,&l_1458,&l_1458};
                        int i;
                        if (g_166)
                            goto lbl_1529;
                        (*l_1437) = (*l_1437);
                        l_1534++;
                    }
                    else
                    { /* block id: 663 */
                        (*g_287) = l_1528;
                        return p_64;
                    }
                    (**l_1437) = 0x1EB8DB1CL;
                    if ((((((((safe_sub_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(18446744073709551609UL, (safe_mod_func_uint8_t_u_u((g_1543 != l_1545), (((void*)0 != l_1547) , (((safe_sub_func_uint32_t_u_u(((((safe_div_func_uint16_t_u_u((p_65 ^ (safe_lshift_func_int16_t_s_u(((**l_1495) = (safe_add_func_int8_t_s_s(p_64, (p_65 | (((((**l_1547) | l_1534) , 0xC157E224L) < 0xF8AFCCAFL) , g_1497[4][0][0]))))), (****g_1103)))), 65535UL)) , p_65) & 0x8DFC719E7073AAF1LL) <= (****g_1103)), 0xB942BC66L)) ^ p_64) ^ l_1528)))))), 0x06D1L)) | (*g_130)) & g_103) & 0xC5E90705L) > 5UL) & g_243) , l_1528))
                    { /* block id: 669 */
                        int32_t *l_1556 = &g_89;
                        int32_t *l_1557 = &l_69[1];
                        int32_t *l_1558 = &l_1459[2][2][1];
                        int32_t *l_1559 = &g_809;
                        int32_t *l_1560 = &g_89;
                        int32_t *l_1561 = &l_1528;
                        int32_t *l_1562[4][6] = {{&l_1459[2][4][0],&l_1459[2][4][0],&l_1458,&l_1466[2][5],&l_1458,&l_1459[2][4][0]},{&l_1458,&l_1459[2][4][0],&l_1466[2][5],&l_1466[2][5],&l_1459[2][4][0],&l_1458},{&l_1459[2][4][0],&l_1458,&l_1466[2][5],&l_1458,&l_1459[2][4][0],&l_1459[2][4][0]},{&l_69[3],&l_1458,&l_1458,&l_69[3],&l_1459[2][4][0],&l_69[3]}};
                        int i, j;
                        l_1566++;
                    }
                    else
                    { /* block id: 671 */
                        int32_t *l_1570 = &g_809;
                        (*l_1570) = ((l_1569 , (l_1466[0][3] = ((**l_1437) &= 0x1CDF7E7FL))) <= p_65);
                    }
                }
                if ((p_65 | ((safe_mod_func_int64_t_s_s((safe_rshift_func_int8_t_s_s((((**l_1437) , (safe_add_func_uint8_t_u_u(l_1486, (safe_sub_func_int16_t_s_s((((-3L) & ((0x77L == ((**l_1547) ^= 0x07L)) >= 0x86L)) <= (safe_div_func_int16_t_s_s((**g_539), (safe_sub_func_uint8_t_u_u(((*g_130) |= 0x75L), l_1528))))), (**g_1105)))))) || 0xAE6C3048L), p_65)), 0x624BBE106EBC6C7BLL)) & l_1569)))
                { /* block id: 679 */
                    if (g_294)
                        goto lbl_1586;
                    return p_64;
                }
                else
                { /* block id: 682 */
                    g_1587 ^= (0x4C40B78938B557DBLL | ((void*)0 == &l_1445));
                    for (l_1563 = (-15); (l_1563 != 1); ++l_1563)
                    { /* block id: 686 */
                        if ((**g_525))
                            break;
                        if (l_1486)
                            continue;
                        return p_64;
                    }
                }
            }
            (**l_1547) ^= p_65;
            l_1592 = l_1590;
        }
    }
    for (l_1569 = 0; (l_1569 < 10); l_1569 = safe_add_func_uint64_t_u_u(l_1569, 4))
    { /* block id: 699 */
        l_1595 ^= 0xF81DA2ACL;
    }
    return (*g_130);
}


/* ------------------------------------------ */
/* 
 * reads : g_48 g_28 g_92 g_19 g_32 g_89 g_103 g_129 g_153 g_156 g_159 g_111 g_130 g_202 g_166 g_246 g_232 g_243 g_294 g_334 g_382 g_403 g_417 g_424 g_415 g_398 g_419 g_449 g_458 g_500 g_374 g_522 g_525 g_402 g_311 g_539 g_542 g_68 g_570 g_540 g_599 g_418 g_466 g_673 g_692 g_698 g_701 g_706 g_724 g_725 g_726 g_784 g_569 g_644 g_645 g_1103 g_807 g_1104 g_1105 g_1029 g_1160 g_809 g_1263 g_1334
 * writes: g_32 g_89 g_92 g_103 g_48 g_153 g_111 g_159 g_166 g_202 g_68 g_232 g_243 g_246 g_129 g_287 g_294 g_311 g_334 g_382 g_403 g_130 g_415 g_458 g_500 g_539 g_398 g_570 g_599 g_466 g_644 g_673 g_692 g_698 g_701 g_706 g_784 g_381 g_569 g_522 g_1103
 */
static const int32_t  func_70(int32_t * p_71, int32_t * p_72)
{ /* block id: 21 */
    int32_t l_85[8];
    int32_t *l_86 = &g_32;
    float l_90[1];
    float l_93 = 0x1.Ep-1;
    int32_t l_96 = 0x59069C78L;
    uint8_t *l_107[9][3][5] = {{{(void*)0,(void*)0,&g_48,&g_48,(void*)0},{&g_48,&g_48,&g_48,&g_48,&g_48},{(void*)0,&g_48,&g_48,&g_48,(void*)0}},{{&g_48,&g_48,(void*)0,&g_48,(void*)0},{(void*)0,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{(void*)0,&g_48,(void*)0,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{&g_48,&g_48,&g_48,(void*)0,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,(void*)0}},{{&g_48,(void*)0,(void*)0,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{&g_48,(void*)0,(void*)0,(void*)0,&g_48},{&g_48,(void*)0,&g_48,&g_48,&g_48},{&g_48,(void*)0,&g_48,&g_48,&g_48}},{{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,(void*)0,&g_48,(void*)0},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}},{{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48},{&g_48,&g_48,&g_48,&g_48,&g_48}}};
    int32_t l_124 = 0xE3A65848L;
    int8_t l_183 = 0x60L;
    uint32_t l_244 = 1UL;
    const float l_337[7][5][7] = {{{(-0x1.1p+1),0x8.8D4735p-19,(-0x1.1p+1),(-0x2.Cp-1),0xA.A44495p-13,(-0x5.Bp-1),0x7.4p+1},{(-0x10.Ap+1),0xE.2EFB70p+76,0x8.9p+1,0x8.9p+1,0xE.2EFB70p+76,(-0x10.Ap+1),0xE.2EFB70p+76},{(-0x1.1p+1),(-0x2.Cp-1),0xA.A44495p-13,(-0x5.Bp-1),0x7.4p+1,(-0x5.Bp-1),0xA.A44495p-13},{0x4.8p-1,0x4.8p-1,(-0x10.Ap+1),0x8.9p+1,(-0x10.Ap+1),0x4.8p-1,0x4.8p-1},{0x0.8p-1,(-0x2.Cp-1),(-0x7.Cp+1),(-0x2.Cp-1),0x0.8p-1,0xB.8D09D4p+61,0xA.A44495p-13}},{{(-0x5.0p-1),0xE.2EFB70p+76,(-0x5.0p-1),(-0x10.Ap+1),(-0x10.Ap+1),(-0x5.0p-1),0xE.2EFB70p+76},{0xA.A44495p-13,0x8.8D4735p-19,(-0x7.Cp+1),0x6.4p-1,0x7.4p+1,(-0x2.Cp-1),0x7.4p+1},{(-0x5.0p-1),(-0x10.Ap+1),(-0x10.Ap+1),(-0x5.0p-1),0xE.2EFB70p+76,(-0x5.0p-1),(-0x10.Ap+1)},{0x0.8p-1,0xB.8D09D4p+61,0xA.A44495p-13,0x6.4p-1,0xA.A44495p-13,0xB.8D09D4p+61,0x0.8p-1},{0x4.8p-1,(-0x10.Ap+1),0x8.9p+1,(-0x10.Ap+1),0x4.8p-1,0x4.8p-1,(-0x10.Ap+1)}},{{(-0x1.1p+1),0x8.8D4735p-19,(-0x1.1p+1),(-0x2.Cp-1),0xA.A44495p-13,(-0x5.Bp-1),0x7.4p+1},{(-0x10.Ap+1),0xE.2EFB70p+76,0x8.9p+1,0x8.9p+1,0xE.2EFB70p+76,(-0x10.Ap+1),0xE.2EFB70p+76},{(-0x1.1p+1),(-0x2.Cp-1),0xA.A44495p-13,(-0x5.Bp-1),0x7.4p+1,(-0x5.Bp-1),0xA.A44495p-13},{0x4.8p-1,0x4.8p-1,(-0x10.Ap+1),0x8.9p+1,(-0x10.Ap+1),0x4.8p-1,0x4.8p-1},{0x0.8p-1,(-0x2.Cp-1),0x7.4p+1,(-0x5.Bp-1),0xA.A44495p-13,(-0x2.Cp-1),(-0x1.1p+1)}},{{0x8.9p+1,0x4.8p-1,0x8.9p+1,(-0x5.0p-1),(-0x5.0p-1),0x8.9p+1,0x4.8p-1},{(-0x1.1p+1),0xB.8D09D4p+61,0x7.4p+1,0x8.8D4735p-19,0x0.8p-1,(-0x5.Bp-1),0x0.8p-1},{0x8.9p+1,(-0x5.0p-1),(-0x5.0p-1),0x8.9p+1,0x4.8p-1,0x8.9p+1,(-0x5.0p-1)},{0xA.A44495p-13,(-0x2.Cp-1),(-0x1.1p+1),0x8.8D4735p-19,(-0x1.1p+1),(-0x2.Cp-1),0xA.A44495p-13},{(-0x10.Ap+1),(-0x5.0p-1),0xE.2EFB70p+76,(-0x5.0p-1),(-0x10.Ap+1),(-0x10.Ap+1),(-0x5.0p-1)}},{{(-0x7.Cp+1),0xB.8D09D4p+61,(-0x7.Cp+1),(-0x5.Bp-1),(-0x1.1p+1),0x6.4p-1,0x0.8p-1},{(-0x5.0p-1),0x4.8p-1,0xE.2EFB70p+76,0xE.2EFB70p+76,0x4.8p-1,(-0x5.0p-1),0x4.8p-1},{(-0x7.Cp+1),(-0x5.Bp-1),(-0x1.1p+1),0x6.4p-1,0x0.8p-1,0x6.4p-1,(-0x1.1p+1)},{(-0x10.Ap+1),(-0x10.Ap+1),(-0x5.0p-1),0xE.2EFB70p+76,(-0x5.0p-1),(-0x10.Ap+1),(-0x10.Ap+1)},{0xA.A44495p-13,(-0x5.Bp-1),0x7.4p+1,(-0x5.Bp-1),0xA.A44495p-13,(-0x2.Cp-1),(-0x1.1p+1)}},{{0x8.9p+1,0x4.8p-1,0x8.9p+1,(-0x5.0p-1),(-0x5.0p-1),0x8.9p+1,0x4.8p-1},{(-0x1.1p+1),0xB.8D09D4p+61,0x7.4p+1,0x8.8D4735p-19,0x0.8p-1,(-0x5.Bp-1),0x0.8p-1},{0x8.9p+1,(-0x5.0p-1),(-0x5.0p-1),0x8.9p+1,0x4.8p-1,0x8.9p+1,(-0x5.0p-1)},{0xA.A44495p-13,(-0x2.Cp-1),(-0x1.1p+1),0x8.8D4735p-19,(-0x1.1p+1),(-0x2.Cp-1),0xA.A44495p-13},{(-0x10.Ap+1),(-0x5.0p-1),0xE.2EFB70p+76,(-0x5.0p-1),(-0x10.Ap+1),(-0x10.Ap+1),(-0x5.0p-1)}},{{(-0x7.Cp+1),0xB.8D09D4p+61,(-0x7.Cp+1),(-0x5.Bp-1),(-0x1.1p+1),0x6.4p-1,0x0.8p-1},{(-0x5.0p-1),0x4.8p-1,0xE.2EFB70p+76,0xE.2EFB70p+76,0x4.8p-1,(-0x5.0p-1),0x4.8p-1},{(-0x7.Cp+1),(-0x5.Bp-1),(-0x1.1p+1),0x6.4p-1,0x0.8p-1,0x6.4p-1,(-0x1.1p+1)},{(-0x10.Ap+1),(-0x10.Ap+1),(-0x5.0p-1),0xE.2EFB70p+76,(-0x5.0p-1),(-0x10.Ap+1),(-0x10.Ap+1)},{0xA.A44495p-13,(-0x5.Bp-1),0x7.4p+1,(-0x5.Bp-1),0xA.A44495p-13,(-0x2.Cp-1),(-0x1.1p+1)}}};
    uint64_t l_373 = 0xE77EDE5556FB1F53LL;
    int32_t l_379 = 0xCEDC32C6L;
    uint8_t *l_409 = &g_48;
    int32_t l_456 = (-5L);
    uint8_t *l_509 = &g_48;
    uint64_t l_537[10][6] = {{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL},{18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL,18446744073709551607UL}};
    uint16_t *l_714[7] = {&g_466,(void*)0,(void*)0,&g_466,(void*)0,(void*)0,&g_466};
    uint16_t **l_713 = &l_714[0];
    uint8_t ***l_719 = &g_129[5];
    uint8_t ****l_718[1];
    int16_t l_760 = (-4L);
    int32_t l_792[2][3] = {{0xED38C478L,0xED38C478L,0xED38C478L},{0L,0L,0L}};
    uint16_t l_812 = 0x8E74L;
    int64_t **l_868 = &g_415;
    int64_t ***l_867[5];
    int8_t l_909[8] = {1L,0x6FL,1L,0x6FL,1L,0x6FL,1L,0x6FL};
    uint16_t l_947 = 0xC067L;
    uint32_t l_1033 = 0x5218310AL;
    int32_t l_1043 = 0xD3A9CF81L;
    int64_t **l_1056[5] = {&g_415,&g_415,&g_415,&g_415,&g_415};
    uint16_t ** const **l_1147[2][1][2];
    int32_t **l_1217 = (void*)0;
    int32_t l_1271[8][10] = {{(-1L),7L,0L,0xF0289B2CL,0L,7L,(-1L),1L,(-3L),0x2B1173EAL},{1L,0x6EBB7D11L,0x2BF34FB9L,0xCF234905L,(-1L),(-1L),0xCF234905L,0x2BF34FB9L,0x6EBB7D11L,1L},{2L,0x6EBB7D11L,(-3L),0xFC95F0FFL,0xF0289B2CL,0x2B1173EAL,(-1L),0x2B1173EAL,0xF0289B2CL,0xFC95F0FFL},{0xFC95F0FFL,7L,0xFC95F0FFL,0x6EBB7D11L,0xF0289B2CL,(-1L),1L,0L,0L,1L},{0xF0289B2CL,(-1L),(-1L),(-1L),(-1L),0xF0289B2CL,2L,0L,0xCF234905L,0x2B1173EAL},{7L,0x2B1173EAL,0xFC95F0FFL,0x2BF34FB9L,0L,0x2BF34FB9L,0xFC95F0FFL,0x2B1173EAL,7L,0xF0289B2CL},{7L,(-1L),(-3L),2L,0x2BF34FB9L,0xF0289B2CL,0xF0289B2CL,0x2BF34FB9L,2L,(-3L)},{0xF0289B2CL,0xF0289B2CL,0x2BF34FB9L,2L,(-3L),(-1L),7L,1L,7L,(-1L)}};
    float l_1332 = 0xD.254A18p-10;
    uint64_t l_1339 = 0UL;
    int64_t l_1342 = (-1L);
    int32_t *l_1343 = &l_379;
    int32_t *l_1344 = &g_89;
    int32_t *l_1345 = (void*)0;
    int32_t *l_1346 = &l_792[1][1];
    int32_t *l_1347 = &l_379;
    int32_t *l_1348 = (void*)0;
    int32_t *l_1349[1][10][10] = {{{&g_32,&l_124,&l_379,&g_32,&l_792[0][1],&l_124,&l_792[1][0],&g_32,&l_379,&l_379},{&l_792[0][1],&l_124,&g_28,&g_809,&g_809,&g_28,&l_124,&l_792[0][1],&l_379,&l_379},{&g_32,&g_32,(void*)0,&g_32,&l_124,&l_792[1][2],&l_379,&l_124,&g_32,&g_809},{&l_792[1][0],&l_792[0][1],(void*)0,&l_124,&l_96,&l_124,(void*)0,&l_792[0][1],&l_792[1][0],(void*)0},{&g_32,&g_32,&g_28,&l_379,&g_32,&l_379,&g_809,&g_32,&l_792[1][0],&l_379},{&l_124,&l_792[1][0],&l_379,&l_379,(void*)0,(void*)0,&l_379,&l_379,&l_792[1][0],&l_124},{&l_379,&g_32,&l_792[1][0],&l_124,&l_792[0][1],&g_32,&l_379,&l_124,&g_32,&l_792[1][0]},{&l_379,&l_124,&g_32,&l_379,&l_792[1][1],&l_379,&l_792[1][1],&l_379,&g_32,&l_124},{&l_792[1][1],&l_792[1][0],&g_89,&l_792[1][0],(void*)0,&g_89,&l_124,&l_379,&l_792[1][0],(void*)0},{&g_28,&l_379,(void*)0,&l_379,&l_792[1][2],&g_89,&g_89,&l_792[1][2],&l_379,(void*)0}}};
    int16_t l_1351 = 0x2E94L;
    int8_t l_1352[6];
    uint32_t l_1353 = 0x3FE976C1L;
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_85[i] = 1L;
    for (i = 0; i < 1; i++)
        l_90[i] = 0xF.C796D6p-11;
    for (i = 0; i < 1; i++)
        l_718[i] = &l_719;
    for (i = 0; i < 5; i++)
        l_867[i] = &l_868;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
                l_1147[i][j][k] = (void*)0;
        }
    }
    for (i = 0; i < 6; i++)
        l_1352[i] = (-1L);
lbl_247:
    for (g_32 = 29; (g_32 <= (-20)); g_32--)
    { /* block id: 24 */
        int8_t l_79 = (-9L);
        uint64_t *l_91 = &g_92;
        int8_t *l_94 = &l_79;
        int32_t l_95 = 0x45D8D669L;
        l_95 = (((0xEE2BL & (safe_mul_func_int8_t_s_s(((*l_94) = ((safe_sub_func_uint32_t_u_u(g_48, (((*l_91) &= (l_79 >= ((((safe_mul_func_int8_t_s_s(g_48, (safe_rshift_func_int8_t_s_u(((safe_unary_minus_func_uint32_t_u(l_85[2])) | ((l_86 != p_71) < (safe_div_func_uint64_t_u_u((g_28 >= (g_89 = (l_79 == 0UL))), 0x53E45BD28040910BLL)))), l_79)))) && g_28) == 0UL) || g_48))) >= 0x51C8BDC662BFFD65LL))) , g_19)), 1L))) < (*l_86)) ^ 0xB7A4FD34L);
        l_96 ^= l_95;
    }
lbl_1089:
    for (g_92 = (-20); (g_92 != 25); ++g_92)
    { /* block id: 33 */
        uint64_t l_110 = 0x3B7D9658AA494CAALL;
        uint8_t **l_127 = (void*)0;
        int32_t l_133[10] = {9L,9L,(-6L),9L,9L,(-6L),9L,9L,(-6L),9L};
        uint8_t **l_252 = &l_107[3][0][4];
        int16_t l_270 = 0x80B0L;
        int32_t *l_272 = &l_133[8];
        float *l_288 = &l_93;
        int32_t l_291 = 0x5326B0D4L;
        int64_t *l_414 = &g_398;
        int8_t l_437[10][9][2] = {{{0xD3L,0x4CL},{0x0CL,0x02L},{(-6L),(-7L)},{(-9L),(-9L)},{(-7L),0xAEL},{(-7L),0xF4L},{0xC4L,1L},{7L,0xF0L},{(-10L),0xF0L}},{{7L,1L},{0xC4L,0xF4L},{(-7L),0xAEL},{(-7L),(-9L)},{(-9L),(-7L)},{(-6L),0x02L},{0x0CL,0x4CL},{0xD3L,0xF4L},{0xB3L,0L}},{{1L,1L},{(-6L),(-1L)},{0L,0L},{0x8EL,0xAEL},{0xE1L,(-1L)},{0xBCL,0x58L},{7L,1L},{0L,0xC8L},{0x58L,1L}},{{0xD9L,(-1L)},{0xAEL,0xAAL},{(-7L),(-10L)},{0L,0xFAL},{0x4CL,0x02L},{1L,0L},{0xD9L,7L},{0xB3L,1L},{0x0CL,(-7L)}},{{0x4CL,(-1L)},{0x30L,(-9L)},{1L,0xAAL},{0xE1L,1L},{0xC4L,0x8EL},{0x58L,1L},{0x1BL,0xF0L},{0xF4L,0x58L},{0xD9L,1L}},{{(-7L),(-7L)},{0x8EL,(-10L)},{0x30L,(-7L)},{1L,1L},{1L,1L},{0xD3L,0x58L},{0xD3L,1L},{1L,1L},{1L,(-7L)}},{{0x30L,(-10L)},{0x8EL,(-7L)},{(-7L),1L},{0xD9L,0x58L},{0xF4L,0xF0L},{0x1BL,1L},{0x58L,0x8EL},{0xC4L,1L},{0xE1L,0xAAL}},{{1L,(-9L)},{0x30L,(-1L)},{0x4CL,(-7L)},{0x0CL,1L},{0xB3L,7L},{0xD9L,0L},{1L,0x02L},{0x4CL,0xFAL},{0L,(-10L)}},{{(-7L),0xAAL},{0xAEL,(-1L)},{0xD9L,1L},{0x58L,0xF4L},{0xBCL,7L},{0x68L,0xF4L},{0x30L,7L},{0xDCL,0xB3L},{1L,0xBCL}},{{0x59L,0x68L},{0x50L,(-7L)},{(-6L),9L},{(-1L),0x80L},{(-10L),0L},{1L,0xE1L},{0x50L,0xC8L},{0x21L,0xC4L},{1L,0xB3L}}};
        int i, j, k;
        for (g_32 = (-6); (g_32 > (-7)); g_32--)
        { /* block id: 36 */
            int8_t l_101 = 0x3AL;
            int32_t *l_102[3];
            int32_t **l_175 = (void*)0;
            int32_t **l_176 = &l_102[0];
            int8_t *l_177 = &l_101;
            uint8_t **l_250 = &g_130;
            float *l_269 = &g_111[5];
            uint16_t l_290 = 2UL;
            int64_t *l_442 = &g_398;
            int8_t l_448 = 5L;
            int i;
            for (i = 0; i < 3; i++)
                l_102[i] = &l_96;
            g_103 = (g_89 = l_101);
            for (l_96 = 28; (l_96 == 6); l_96 = safe_sub_func_int32_t_s_s(l_96, 6))
            { /* block id: 41 */
                const int64_t l_123 = 0xEFC5DF5A5DA86A19LL;
                int32_t l_158 = 1L;
                for (l_101 = 0; (l_101 >= 0); l_101 -= 1)
                { /* block id: 44 */
                    int16_t l_112 = 0xF0B8L;
                    for (g_103 = 0; (g_103 <= 2); g_103 += 1)
                    { /* block id: 47 */
                        int8_t l_106 = 1L;
                        uint8_t *l_108 = &g_48;
                        uint8_t **l_109 = &l_108;
                        int i;
                        l_112 ^= (l_106 < ((g_89 > (l_107[3][0][4] == ((*l_109) = l_108))) && l_110));
                        g_89 &= g_28;
                    }
                    l_124 &= (((safe_mul_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((g_48 = (safe_mul_func_uint16_t_u_u(((*p_72) | (7UL < 0xC8B9E1FAL)), ((void*)0 == &g_48)))), (4294967294UL & ((safe_sub_func_int16_t_s_s(((*l_86) >= g_103), ((l_110 || l_110) ^ 0UL))) != 8UL)))) | (-6L)), l_123)) < (*l_86)) & (-1L));
                }
                for (g_103 = 22; (g_103 == 28); g_103 = safe_add_func_uint32_t_u_u(g_103, 1))
                { /* block id: 57 */
                    uint8_t ***l_128 = &l_127;
                    float *l_131 = (void*)0;
                    int32_t l_132[8][9][3] = {{{0x954279E9L,0x46AFD119L,0x6699B53DL},{0L,(-1L),(-1L)},{0L,9L,1L},{4L,(-10L),(-1L)},{(-7L),(-1L),(-9L)},{1L,0xE9465D98L,0x39F97B59L},{0x6DFE233BL,(-1L),0x6699B53DL},{4L,(-10L),1L},{0xBB48C356L,9L,0x6C92F557L}},{{0x65CA490DL,(-1L),9L},{(-7L),0x46AFD119L,(-1L)},{0L,(-1L),0x39F97B59L},{0L,9L,1L},{0L,(-10L),3L},{0x4C8FCCF9L,(-1L),1L},{0x65CA490DL,0xE9465D98L,0L},{(-1L),(-1L),(-1L)},{0xEDBCE3F7L,(-10L),0xD6ACD93DL}},{{0x6DFE233BL,9L,0x5FE3C0FCL},{0x6A152264L,(-1L),1L},{0x4C8FCCF9L,0x46AFD119L,7L},{4L,(-1L),0L},{2L,9L,(-9L)},{0L,(-10L),1L},{0x954279E9L,(-1L),1L},{0x6A152264L,0xE9465D98L,(-1L)},{0xBB48C356L,(-1L),7L}},{{1L,(-10L),9L},{(-1L),9L,0x654D6470L},{1L,(-1L),0xD6ACD93DL},{0x954279E9L,0x46AFD119L,0x6699B53DL},{0L,(-1L),(-1L)},{0L,9L,1L},{4L,(-10L),(-1L)},{(-7L),(-1L),(-9L)},{1L,0xE9465D98L,0x39F97B59L}},{{0x6DFE233BL,(-1L),0x6699B53DL},{4L,(-10L),1L},{0xBB48C356L,9L,0x6C92F557L},{0x65CA490DL,(-1L),9L},{(-7L),0x46AFD119L,(-1L)},{0L,(-1L),0x39F97B59L},{0L,9L,1L},{0L,(-10L),3L},{0x4C8FCCF9L,(-1L),1L}},{{0x65CA490DL,0xE9465D98L,0L},{(-1L),(-1L),(-1L)},{0xEDBCE3F7L,(-10L),0xD6ACD93DL},{0x6DFE233BL,9L,0x5FE3C0FCL},{0x6A152264L,(-1L),1L},{0x4C8FCCF9L,0x46AFD119L,7L},{4L,(-1L),0L},{2L,9L,(-9L)},{0L,(-10L),1L}},{{7L,0x21FA5157L,0xF5104884L},{(-1L),0xFC21787FL,0xB561CB1BL},{1L,0x21FA5157L,0xE0FD2558L},{1L,(-7L),9L},{(-9L),0L,1L},{0L,0xB561CB1BL,0L},{7L,0x0945A8A0L,0L},{1L,0xB561CB1BL,0xB561CB1BL},{0x5FE3C0FCL,0L,(-1L)}},{{0xD6ACD93DL,(-7L),0x034D0585L},{(-1L),0x21FA5157L,(-3L)},{0L,0xFC21787FL,0x9D979A8BL},{1L,0x21FA5157L,0L},{3L,(-7L),(-5L)},{1L,0L,3L},{0x39F97B59L,0xB561CB1BL,9L},{(-1L),0x0945A8A0L,0x21FA5157L},{9L,0xB561CB1BL,0x9D979A8BL}}};
                    int i, j, k;
                    l_133[8] = ((((*l_128) = l_127) == g_129[4]) > (l_132[4][8][0] = 0x6.184417p+51));
                    for (l_110 = 4; (l_110 < 57); ++l_110)
                    { /* block id: 63 */
                        int32_t l_138 = 0L;
                        uint64_t *l_152 = &g_153;
                        const uint8_t **l_155 = (void*)0;
                        const uint8_t ** const *l_154 = &l_155;
                        int32_t l_157 = 0x559E358FL;
                        float *l_165[5][8] = {{&l_90[0],&l_90[0],(void*)0,&l_93,&l_93,&l_93,(void*)0,&l_90[0]},{&l_90[0],&l_93,(void*)0,(void*)0,(void*)0,(void*)0,&l_93,&l_90[0]},{&l_93,&g_111[2],&l_90[0],&l_93,&l_90[0],&g_111[2],&l_93,&l_93},{&g_111[2],&l_93,(void*)0,(void*)0,&l_93,&g_111[2],(void*)0,&g_111[2]},{&l_93,&g_111[2],(void*)0,&g_111[2],&l_93,(void*)0,(void*)0,&l_93}};
                        int i, j;
                        (*g_156) = ((safe_mul_func_float_f_f(((*l_86) , ((l_138 || (safe_mul_func_uint16_t_u_u((l_123 == (safe_rshift_func_int16_t_s_s((-5L), 8))), (l_110 < ((safe_div_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((~18446744073709551613UL), 0)), ((((safe_mod_func_int32_t_s_s(((-1L) != ((*l_152) = (safe_div_func_uint8_t_u_u(((&g_89 == &g_89) ^ 4L), g_32)))), 4294967293UL)) , &g_129[4]) == l_154) && g_153))) , 0UL))))) , l_133[5])), l_123)) <= 0x3.61DA5Ap-68);
                        ++g_159;
                        l_132[4][8][0] = (safe_add_func_float_f_f((-((*g_156) != (g_166 = (0x0.A44408p-36 == l_158)))), (safe_sub_func_float_f_f((0x0.705393p+53 <= (g_28 , (safe_sub_func_float_f_f((((*l_128) = (*l_128)) != (l_138 , &g_130)), ((safe_mul_func_float_f_f((g_19 , g_111[5]), l_123)) >= (*l_86)))))), g_159))));
                    }
                }
            }
            if ((safe_mul_func_int8_t_s_s(((*l_177) |= (l_110 | (((*l_176) = &g_89) != (void*)0))), (*l_86))))
            { /* block id: 75 */
                int32_t *l_182[3][2] = {{(void*)0,&l_96},{&l_96,(void*)0},{&l_96,&l_96}};
                uint64_t *l_281[8] = {&g_202[6][0],&g_202[6][0],&g_202[6][0],&g_202[6][0],&g_202[6][0],&g_202[6][0],&g_202[6][0],&g_202[6][0]};
                float **l_286[8] = {&l_269,&l_269,&l_269,&l_269,&l_269,&l_269,&l_269,&l_269};
                const uint32_t l_356 = 0UL;
                int64_t l_434 = 0x38683411C583FD01LL;
                int i, j;
                if ((l_133[8] = (g_103 , (((((safe_mod_func_uint64_t_u_u(6UL, (safe_rshift_func_int16_t_s_u(l_110, ((void*)0 == &g_129[4]))))) & ((*l_86) & (l_182[1][1] != (void*)0))) ^ 0x5CL) == 0x654C1894117C0604LL) || g_32))))
                { /* block id: 77 */
                    uint32_t l_207[5];
                    int64_t *l_242 = &g_243;
                    int32_t l_245 = 0x1AA2D425L;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_207[i] = 0x7DA2B038L;
                    for (g_159 = 0; (g_159 <= 1); g_159 += 1)
                    { /* block id: 80 */
                        uint64_t *l_201[8] = {&l_110,&l_110,&l_110,&l_110,&l_110,&l_110,&l_110,&l_110};
                        int8_t l_208 = 0xF0L;
                        int i, j, k;
                        g_89 = l_183;
                        l_182[(g_159 + 1)][g_159] = &g_89;
                        (**l_176) = ((((safe_mod_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(g_89, 15)), ((((*g_130) ^= 255UL) || ((safe_sub_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s(g_153, 2)), 2)), (l_133[7] != ((safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u((safe_unary_minus_func_int64_t_s((safe_sub_func_uint64_t_u_u(1UL, (--g_202[7][1]))))), ((safe_div_func_uint8_t_u_u((*g_130), l_207[0])) & ((**l_176) <= l_208)))), g_32)) , l_133[4])))) , (-8L))) , l_133[9]))) | g_166) > 0xB081F9AEL) <= g_166);
                    }
                    g_68 = &g_89;
                    g_246[0] = (((safe_sub_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(0UL, ((((safe_add_func_int8_t_s_s((safe_add_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u(0xB175C3F9EEAFE74CLL, 18446744073709551609UL)), ((safe_rshift_func_uint8_t_u_s((((safe_sub_func_float_f_f(((safe_sub_func_float_f_f((((~((safe_mod_func_int16_t_s_s(((g_232 = ((((((**l_176) = (**l_176)) || l_110) == 0x696AL) ^ 0x14CDL) ^ g_92)) < (safe_add_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(((l_245 = (safe_lshift_func_int16_t_s_u((safe_mul_func_uint8_t_u_u(((((*l_242) = ((((((+((-1L) & (*l_86))) > l_207[4]) , g_202[7][2]) >= (*l_86)) > g_103) >= (*l_86))) > l_244) || 0x50L), g_202[7][1])), 9))) | g_202[7][1]), (*l_86))), 0x0068L))), g_159)) > 0UL)) , (*g_156)) >= 0x7.CF6DD3p-15), g_159)) > 0x3.3C6027p-25), 0x6.F036B3p+50)) , (*g_156)) , 0x05L), 1)) , l_110))) ^ g_202[7][1]), g_103)), g_32)), 247UL)) , (void*)0) != &g_202[7][3]) || g_166))), g_202[7][1])) > 255UL) != (*p_72));
                }
                else
                { /* block id: 93 */
                    uint8_t ***l_251 = &g_129[3];
                    uint8_t ***l_253 = &l_127;
                    int32_t l_254 = 7L;
                    float *l_263 = &l_93;
                    float **l_264 = &l_263;
                    int32_t l_265 = 0x6967E034L;
                    float *l_266[2][3][1];
                    float **l_267 = (void*)0;
                    float **l_268[4][9] = {{&l_266[0][2][0],(void*)0,(void*)0,&l_266[0][2][0],&l_266[0][2][0],(void*)0,(void*)0,&l_266[0][2][0],&l_266[0][2][0]},{&l_266[0][0][0],&l_266[1][0][0],&l_266[0][0][0],&l_266[1][0][0],&l_266[0][0][0],&l_266[1][0][0],&l_266[0][0][0],&l_266[1][0][0],&l_266[0][0][0]},{&l_266[0][2][0],&l_266[0][2][0],(void*)0,(void*)0,&l_266[0][2][0],&l_266[0][2][0],(void*)0,(void*)0,&l_266[0][2][0]},{(void*)0,&l_266[1][0][0],(void*)0,&l_266[1][0][0],(void*)0,&l_266[1][0][0],(void*)0,&l_266[1][0][0],(void*)0}};
                    float l_271 = 0x0.0p-1;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 3; j++)
                        {
                            for (k = 0; k < 1; k++)
                                l_266[i][j][k] = &l_90[0];
                        }
                    }
                    if (g_48)
                        goto lbl_247;
                    l_124 &= (safe_rshift_func_int8_t_s_s(((((((*l_251) = l_250) != ((*l_253) = l_252)) >= l_254) <= (-1L)) , ((safe_add_func_int8_t_s_s(((-2L) & ((safe_rshift_func_uint8_t_u_u(((((safe_mul_func_int16_t_s_s(((((((safe_div_func_uint16_t_u_u(((l_254 = g_246[1]) ^ (((*l_264) = (g_232 , l_263)) == (l_269 = (l_265 , l_266[0][0][0])))), g_202[2][1])) < (-2L)) > 251UL) != (*l_86)) , (*l_86)) && g_243), g_153)) , (*l_86)) < g_202[6][1]) < 0xC4637558L), l_265)) ^ l_270)), l_265)) > 1UL)), 1));
                    l_272 = p_72;
                    if ((*l_272))
                        continue;
                }
                if (((**l_176) = (safe_add_func_uint64_t_u_u(18446744073709551615UL, (((safe_mod_func_uint8_t_u_u(((safe_div_func_int8_t_s_s((safe_sub_func_int16_t_s_s(((*p_72) != 0xCD65A0D8L), (((g_153 = g_202[4][1]) < (((g_92 == (safe_div_func_int64_t_s_s(((safe_sub_func_uint64_t_u_u(((g_287 = p_71) == (g_89 , (l_288 = l_102[1]))), (~l_290))) , g_159), (*l_86)))) == 0x2547L) | (*l_272))) && l_291))), (-1L))) , (*g_130)), (*l_86))) & (*l_86)) , 0L)))))
                { /* block id: 108 */
                    int32_t l_292 = (-10L);
                    int32_t l_293[10][7][3] = {{{0xD4162190L,(-4L),0x9631A347L},{0xFCFB86C9L,1L,0x4347F6A0L},{0x9631A347L,0x168ACB36L,0x4C44AEB9L},{(-7L),0xB74A6212L,0x4347F6A0L},{(-9L),(-7L),0x9631A347L},{0x256DE87AL,0x82D32F51L,0x256DE87AL},{0x11AB2A09L,0xFC11A16BL,0L}},{{0xAEA7E00CL,(-9L),1L},{0xFC11A16BL,0x4C44AEB9L,0xD4162190L},{0xFF5C94A6L,(-1L),0L},{0x168ACB36L,0xDE7F64FBL,0x4C44AEB9L},{0x256DE87AL,0x85622054L,(-1L)},{9L,0xBDF71456L,0xD4162190L},{0xEDC4550EL,0xAC1F8926L,0xFCFB86C9L}},{{0xDD90FAD0L,0x11AB2A09L,0x9631A347L},{0x528E0214L,0x4348F841L,(-7L)},{0L,0x11AB2A09L,(-9L)},{0xFF5C94A6L,0xAC1F8926L,0x256DE87AL},{0xBDF71456L,0xBDF71456L,0x11AB2A09L},{(-7L),0x85622054L,0xAEA7E00CL},{(-9L),0xDE7F64FBL,0xFC11A16BL}},{{0xAEA7E00CL,0x539B9ACDL,0xFF5C94A6L},{0L,(-9L),0xFC11A16BL},{0L,(-2L),0xAEA7E00CL},{0x11AB2A09L,0x168ACB36L,0x11AB2A09L},{4L,(-1L),0x256DE87AL},{(-8L),0xF4FE8CBAL,(-9L)},{1L,0x2F378EE9L,(-7L)}},{{(-4L),(-9L),0x9631A347L},{1L,0x4240811FL,0xFCFB86C9L},{(-8L),0xD4162190L,0xD4162190L},{4L,0x55E803D2L,(-1L)},{0x11AB2A09L,(-4L),0x4C44AEB9L},{0L,0xAE44536BL,0x4EDF46E7L},{0L,0x18FCEAAEL,0xBDF71456L}},{{0xAEA7E00CL,0xAE44536BL,1L},{(-9L),(-4L),(-8L)},{(-7L),0x55E803D2L,0xEDC4550EL},{0xBDF71456L,0xD4162190L,0L},{0xFF5C94A6L,0x4240811FL,0xEF9F8DC6L},{0L,(-9L),(-9L)},{0x528E0214L,0x2F378EE9L,0xEF9F8DC6L}},{{0xDD90FAD0L,0xF4FE8CBAL,0L},{0xEDC4550EL,(-1L),0xEDC4550EL},{9L,0x168ACB36L,(-8L)},{0x256DE87AL,(-2L),1L},{0x168ACB36L,(-9L),0xBDF71456L},{1L,0x539B9ACDL,0x4EDF46E7L},{0x168ACB36L,0xDE7F64FBL,0x4C44AEB9L}},{{0x256DE87AL,0x85622054L,(-1L)},{9L,0xBDF71456L,0xD4162190L},{0xEDC4550EL,0xAC1F8926L,0xFCFB86C9L},{0xDD90FAD0L,0x11AB2A09L,0x9631A347L},{0x528E0214L,0x4348F841L,(-7L)},{0L,0x11AB2A09L,(-9L)},{0xFF5C94A6L,0xAC1F8926L,0x256DE87AL}},{{0xBDF71456L,0xBDF71456L,0x11AB2A09L},{(-7L),0x85622054L,0xAEA7E00CL},{(-9L),0xDE7F64FBL,0xFC11A16BL},{0xAEA7E00CL,0x539B9ACDL,0xFF5C94A6L},{0L,(-9L),0xFC11A16BL},{0L,(-2L),0xAEA7E00CL},{0x11AB2A09L,0x168ACB36L,0x11AB2A09L}},{{4L,(-1L),0x256DE87AL},{(-8L),0xF4FE8CBAL,(-9L)},{1L,0x2F378EE9L,(-7L)},{(-4L),(-9L),0x9631A347L},{1L,0x4240811FL,0xFCFB86C9L},{(-8L),0xD4162190L,0xD4162190L},{4L,0x55E803D2L,(-1L)}}};
                    uint8_t ***l_299[7][8][4] = {{{&g_129[4],&l_250,&l_252,&g_129[3]},{(void*)0,&l_250,&g_129[4],&l_250},{&l_252,(void*)0,&g_129[3],&g_129[3]},{&g_129[3],(void*)0,(void*)0,&l_127},{&g_129[4],(void*)0,&l_250,&g_129[3]},{&l_252,&g_129[3],&g_129[4],&l_252},{&g_129[2],(void*)0,&l_250,&l_250},{(void*)0,(void*)0,&l_252,&l_252}},{{&g_129[4],&g_129[3],&l_127,&g_129[3]},{(void*)0,(void*)0,&g_129[4],&l_127},{&l_252,(void*)0,&g_129[3],&g_129[3]},{(void*)0,&g_129[3],(void*)0,&l_252},{&l_127,(void*)0,&l_127,&l_250},{&l_252,(void*)0,&l_252,&l_252},{&l_252,&g_129[3],&l_250,&g_129[3]},{&g_129[3],(void*)0,(void*)0,&l_127}},{{&g_129[4],(void*)0,&l_250,&g_129[3]},{&l_252,&g_129[3],&g_129[4],&l_252},{&g_129[2],(void*)0,&l_250,&l_250},{(void*)0,(void*)0,&l_252,&l_252},{&g_129[4],&g_129[3],&l_127,&g_129[3]},{(void*)0,(void*)0,&g_129[4],&l_127},{&l_252,(void*)0,&g_129[3],&g_129[3]},{(void*)0,&g_129[3],(void*)0,&l_252}},{{&l_127,(void*)0,&l_127,&l_250},{&l_252,(void*)0,&l_252,&l_252},{&l_252,&g_129[3],&l_250,&g_129[3]},{&g_129[3],(void*)0,(void*)0,&l_127},{&g_129[4],(void*)0,&l_250,&g_129[3]},{&l_252,&g_129[3],&g_129[4],&l_252},{&g_129[2],(void*)0,&l_250,&l_250},{(void*)0,(void*)0,&l_252,&l_252}},{{&g_129[4],&g_129[3],&l_127,&g_129[3]},{(void*)0,(void*)0,&g_129[4],&l_127},{&l_252,(void*)0,&g_129[3],&g_129[3]},{(void*)0,&g_129[3],(void*)0,&l_252},{&l_127,(void*)0,&l_127,&l_250},{&l_252,(void*)0,&l_252,&l_252},{&l_252,&g_129[3],&l_250,&g_129[3]},{&g_129[3],(void*)0,(void*)0,&l_127}},{{&g_129[4],(void*)0,&l_250,&g_129[3]},{&l_252,&g_129[3],&g_129[4],&l_252},{&g_129[2],(void*)0,&l_250,&l_250},{(void*)0,(void*)0,&l_252,&l_252},{&g_129[4],&g_129[3],&l_127,&g_129[3]},{(void*)0,(void*)0,&g_129[4],&l_127},{&l_252,(void*)0,&g_129[3],&g_129[3]},{(void*)0,&g_129[3],(void*)0,&l_252}},{{&l_127,(void*)0,&l_127,&l_250},{&l_252,(void*)0,&l_252,&l_252},{&l_252,&g_129[3],&g_129[3],&l_250},{&l_250,&g_129[3],&l_252,&l_250},{&l_127,&g_129[3],&l_252,&l_250},{(void*)0,&l_250,&l_252,&l_127},{&l_252,&g_129[3],&g_129[3],&g_129[3]},{&g_129[3],&g_129[3],&g_129[3],&l_127}}};
                    const int8_t *l_333[7][1] = {{&g_311[2][1][4]},{&g_311[0][3][2]},{&g_311[2][1][4]},{&g_311[0][3][2]},{&g_311[2][1][4]},{&g_311[0][3][2]},{&g_311[2][1][4]}};
                    int64_t l_335 = 0L;
                    uint32_t *l_336[6];
                    float *l_348 = (void*)0;
                    int i, j, k;
                    for (i = 0; i < 6; i++)
                        l_336[i] = &g_103;
                    ++g_294;
                    for (g_243 = 29; (g_243 <= 5); --g_243)
                    { /* block id: 112 */
                        uint32_t l_304 = 8UL;
                        int32_t l_305 = 0x4D4A3C98L;
                        g_89 = ((void*)0 != l_299[6][3][3]);
                        g_311[2][1][4] = ((**l_176) &= ((g_153 ^ (safe_mod_func_int8_t_s_s(l_292, 0xA9L))) , ((safe_mod_func_uint64_t_u_u((g_202[7][1] = (((l_305 = l_304) == ((safe_div_func_int8_t_s_s((safe_add_func_int8_t_s_s(((g_246[3] < (g_19 != ((-1L) | (+(&l_288 != &g_156))))) == (*l_272)), (*l_86))), l_293[3][4][2])) == g_32)) | 255UL)), 1UL)) & 255UL)));
                        l_124 = ((**l_176) &= (safe_mod_func_uint16_t_u_u((*l_86), l_293[3][4][2])));
                        l_293[3][4][2] = ((((((*l_177) = ((*l_86) , (safe_mul_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((safe_mod_func_int32_t_s_s((*p_72), ((**l_176) = (*l_86)))) != (((~(((((safe_lshift_func_uint8_t_u_s(0UL, (safe_rshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((g_243 && (p_71 != ((g_334 |= ((safe_rshift_func_uint16_t_u_s((*l_86), 3)) ^ (((((((((*l_272) , l_333[1][0]) != &g_311[1][7][1]) == g_232) && (-1L)) <= l_293[7][5][1]) && (*l_86)) | 0x170D78081E67CEA3LL) < 0x4E5EF28340461B76LL))) , (void*)0))) , 0x12L), 2UL)), 0)), l_305)))) <= g_246[0]) , 18446744073709551612UL) <= (*l_86)) && (-7L))) != (*l_86)) || l_335)), (*l_272))), l_304)))) , (*l_86)) || 0x8A85L) <= (*l_272)) , 0xB.9D94C1p-68);
                    }
                    if (((g_294 = 0x8EEB5581L) , (-1L)))
                    { /* block id: 126 */
                        if ((*p_72))
                            break;
                        return l_335;
                    }
                    else
                    { /* block id: 129 */
                        int64_t *l_346 = &l_335;
                        int32_t l_353 = 0xF886CF4FL;
                        (*l_288) = (0x4.Bp-1 >= (*l_86));
                        g_68 = (((safe_add_func_uint16_t_u_u((1L != ((safe_mod_func_uint16_t_u_u(((&g_129[5] != (void*)0) & ((safe_lshift_func_uint8_t_u_s(((safe_lshift_func_int8_t_s_s((((*l_346) &= 1L) != ((((((+(g_89 = ((((((((void*)0 != p_71) != ((l_348 = p_72) != (void*)0)) > (safe_lshift_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u((*g_130), 1UL)), (*l_272)))) , (**l_176)) , 0UL) , (*l_86)) <= 0x9287L))) || g_32) < 0x2182E4B0C4D84C3ELL) , &l_348) != (void*)0) || (*l_86))), 6)) >= l_353), l_292)) <= 0xA888EF2FA366EE92LL)), (*l_272))) && l_335)), 0x1BDCL)) >= (*l_272)) , &g_89);
                        if (g_19)
                            goto lbl_247;
                    }
                }
                else
                { /* block id: 137 */
                    float l_372 = (-0x1.2p-1);
                    int32_t l_376 = 0L;
                    int64_t *l_386 = &g_243;
                    int64_t **l_385[6][8] = {{&l_386,(void*)0,&l_386,&l_386,&l_386,(void*)0,&l_386,&l_386},{&l_386,(void*)0,&l_386,&l_386,&l_386,(void*)0,&l_386,&l_386},{&l_386,(void*)0,&l_386,&l_386,&l_386,(void*)0,&l_386,&l_386},{&l_386,(void*)0,&l_386,&l_386,&l_386,(void*)0,&l_386,&l_386},{&l_386,(void*)0,&l_386,&l_386,&l_386,(void*)0,&l_386,&l_386},{&l_386,(void*)0,&l_386,&l_386,&l_386,(void*)0,&l_386,&l_386}};
                    int32_t l_396 = 0xCC374125L;
                    int32_t l_399 = 0x36D820D2L;
                    int32_t l_401 = 0x0BF8AFA2L;
                    uint8_t *l_408 = &g_48;
                    uint8_t *l_410 = &g_159;
                    int16_t *l_416 = &g_246[0];
                    int i, j;
                    if ((((l_373 = ((safe_rshift_func_uint8_t_u_u((p_71 == (void*)0), (*g_130))) >= (l_356 >= ((safe_lshift_func_int8_t_s_u(((safe_div_func_float_f_f((safe_div_func_float_f_f((0xD.45EBF2p+34 >= ((g_103 , ((safe_div_func_float_f_f(((((g_159 , (((safe_mul_func_float_f_f((safe_sub_func_float_f_f((+0xF.029325p+52), (safe_mul_func_float_f_f((*l_272), 0xF.D52D6Ap+70)))), (*l_272))) == (-0x1.4p-1)) > 0x5.Cp-1)) == (-0x5.3p-1)) == (*g_156)) != 0x1.Ap+1), (*l_86))) > (*l_86))) == (*l_272))), (*l_272))), (*l_86))) , (*l_86)), 0)) & g_334)))) && 0xFCC4D1D9BDEC7DB3LL) != g_48))
                    { /* block id: 139 */
                        int64_t l_375 = (-2L);
                        int32_t l_380 = 0x5BDCAAD3L;
                        int64_t ***l_387 = (void*)0;
                        int64_t ***l_388 = &l_385[2][0];
                        g_382--;
                        (*l_388) = l_385[0][2];
                    }
                    else
                    { /* block id: 142 */
                        int8_t l_393 = (-4L);
                        int32_t l_397 = 0x670A18DEL;
                        int32_t l_400 = 0x7E63A4FBL;
                        (**l_176) |= (safe_lshift_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(l_393, (safe_sub_func_uint32_t_u_u(4294967294UL, g_246[0])))), 4));
                        (*l_176) = (void*)0;
                        ++g_403;
                        l_133[8] = (-10L);
                    }
                    g_89 &= (*p_72);
                    (*l_288) = (+((*l_86) <= ((!((((((*l_250) = (((*l_386) &= (*l_272)) , l_408)) == ((*l_252) = (l_410 = l_409))) != l_401) > 0x2.4p-1) == (-((((g_311[1][3][2] = l_399) & (((safe_div_func_int16_t_s_s(((*l_416) &= (&g_377 != (g_415 = l_414))), 0xC496L)) == g_89) , (*l_86))) , g_417[0]) != (void*)0)))) != l_401)));
                    l_133[8] &= (((safe_mod_func_int8_t_s_s(0x72L, (safe_rshift_func_uint16_t_u_u(0x5FA2L, 14)))) , ((g_424[0][3] == p_71) , l_272)) == (void*)0);
                }
                for (g_232 = 0; (g_232 == 48); g_232++)
                { /* block id: 161 */
                    uint32_t l_431 = 18446744073709551615UL;
                    int32_t l_433 = (-1L);
                    int16_t *l_447[10][4][6] = {{{&l_270,&g_246[1],&l_270,&g_246[0],&g_246[0],&l_270},{(void*)0,(void*)0,&g_246[2],&g_246[0],&g_246[1],&l_270},{&l_270,&g_246[2],&g_246[1],&l_270,&g_246[1],&g_246[2]},{&g_246[0],&l_270,&g_246[1],&g_246[0],(void*)0,&l_270}},{{&l_270,&g_246[0],&g_246[2],&g_246[2],&g_246[0],&l_270},{&g_246[2],&g_246[0],&l_270,&g_246[0],(void*)0,&g_246[1]},{&g_246[1],&l_270,&g_246[0],&l_270,&g_246[1],&g_246[0]},{&g_246[1],&g_246[2],&l_270,&g_246[0],&g_246[1],&g_246[1]}},{{&g_246[2],(void*)0,(void*)0,&g_246[2],&g_246[0],&g_246[1]},{&l_270,&g_246[1],&l_270,&g_246[0],&g_246[0],&g_246[0]},{&g_246[0],(void*)0,&g_246[0],&l_270,&g_246[0],&g_246[1]},{&l_270,&g_246[1],&l_270,&g_246[0],&g_246[0],&l_270}},{{(void*)0,(void*)0,&g_246[2],&g_246[0],&g_246[1],&l_270},{&l_270,&g_246[2],&g_246[1],&l_270,&g_246[1],&g_246[2]},{&g_246[0],&l_270,&g_246[1],&g_246[0],(void*)0,&l_270},{&l_270,&g_246[0],&g_246[2],&g_246[2],&g_246[0],&l_270}},{{&g_246[2],&g_246[0],&l_270,&g_246[0],(void*)0,&g_246[1]},{&g_246[1],&l_270,&g_246[0],&l_270,&g_246[1],&g_246[0]},{&g_246[1],&g_246[2],&l_270,&g_246[0],&g_246[1],&g_246[1]},{(void*)0,&l_270,&l_270,(void*)0,&g_246[0],&g_246[1]}},{{&g_246[0],&g_246[1],(void*)0,&g_246[2],&g_246[1],&g_246[2]},{&g_246[0],&g_246[0],&g_246[0],&l_270,&g_246[1],&l_270},{(void*)0,&g_246[1],&g_246[0],&g_246[0],&g_246[0],&g_246[0]},{&l_270,&l_270,(void*)0,&g_246[0],&g_246[1],&l_270}},{{(void*)0,(void*)0,&l_270,&l_270,&l_270,(void*)0},{&g_246[0],(void*)0,&l_270,&g_246[2],&l_270,&l_270},{&g_246[0],&g_246[2],(void*)0,(void*)0,&g_246[2],&g_246[0]},{(void*)0,&g_246[2],&g_246[0],&g_246[1],&l_270,&l_270}},{{&l_270,(void*)0,&g_246[0],(void*)0,&l_270,&g_246[2]},{&l_270,(void*)0,(void*)0,&g_246[1],&g_246[1],&g_246[1]},{(void*)0,&l_270,&l_270,(void*)0,&g_246[0],&g_246[1]},{&g_246[0],&g_246[1],(void*)0,&g_246[2],&g_246[1],&g_246[2]}},{{&g_246[0],&g_246[0],&g_246[0],&l_270,&g_246[1],&l_270},{(void*)0,&g_246[1],&g_246[0],&g_246[0],&g_246[0],&g_246[0]},{&l_270,&l_270,(void*)0,&g_246[0],&g_246[1],&l_270},{(void*)0,(void*)0,&l_270,&l_270,&l_270,(void*)0}},{{&g_246[0],(void*)0,&l_270,&g_246[2],&l_270,&l_270},{&g_246[0],&g_246[2],(void*)0,(void*)0,&g_246[2],&g_246[0]},{(void*)0,&g_246[2],&g_246[0],&g_246[1],&l_270,&l_270},{&l_270,(void*)0,&g_246[0],(void*)0,&l_270,&g_246[2]}}};
                    int32_t l_450[10];
                    uint64_t l_451 = 0x3533F1D312E5505ELL;
                    int64_t **l_452[7];
                    int64_t ***l_453 = &l_452[3];
                    int i, j, k;
                    for (i = 0; i < 10; i++)
                        l_450[i] = (-1L);
                    for (i = 0; i < 7; i++)
                        l_452[i] = &g_415;
                    (*l_453) = ((((((safe_mul_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(l_431, 12)), 0xA2L)) && ((g_89 = (safe_unary_minus_func_uint64_t_u((l_437[5][3][1] ^= (g_202[7][3]--))))) , ((((*g_415) < ((((l_450[5] = (safe_rshift_func_int16_t_s_s(((((g_294 ^ ((void*)0 != l_442)) && 0x76F8FF9C4B21BEA8LL) , ((((((safe_mul_func_int16_t_s_s((l_433 ^= (l_96 = ((safe_lshift_func_int16_t_s_u(g_334, g_419)) || (*l_86)))), (*l_86))) , l_448) & l_431) ^ g_166) | 4UL) | g_449)) | g_202[8][0]), g_382))) | 0xC6778101BD1B4413LL) < l_431) <= l_451)) , 0x5C2A52FEF01E9E20LL) <= (*l_272)))) == l_451) < (*l_86)) > g_398) , l_452[5]);
                }
            }
            else
            { /* block id: 170 */
                int32_t *l_454 = &l_379;
                int32_t l_457[1][6][7] = {{{0xE938B8B6L,(-1L),0xC4A3E8A9L,0x6B2C2969L,(-7L),(-1L),0xE938B8B6L},{0x81F5BC65L,(-1L),9L,5L,(-7L),0xAEC7EDF9L,0x81F5BC65L},{0x81F5BC65L,(-1L),0xC4A3E8A9L,5L,0xC4A3E8A9L,(-1L),0x81F5BC65L},{0xE938B8B6L,(-1L),0xC4A3E8A9L,0x6B2C2969L,(-7L),(-1L),0xE938B8B6L},{0x81F5BC65L,(-1L),9L,5L,(-7L),0xAEC7EDF9L,0x81F5BC65L},{0x81F5BC65L,(-1L),0xC4A3E8A9L,5L,0xC4A3E8A9L,(-1L),0x81F5BC65L}}};
                int i, j, k;
                (*l_176) = l_454;
                g_458++;
                if ((*p_72))
                    break;
                if (g_166)
                    continue;
            }
        }
    }
    if ((*l_86))
    { /* block id: 178 */
        uint32_t l_463 = 8UL;
        uint64_t *l_464 = &g_202[7][1];
        uint16_t *l_465[7][6][6] = {{{&g_466,&g_19,&g_19,&g_19,&g_466,&g_466},{&g_19,&g_19,&g_19,&g_19,&g_19,&g_466},{&g_19,&g_466,&g_19,&g_19,(void*)0,(void*)0},{&g_466,&g_466,&g_466,&g_19,&g_19,&g_466},{(void*)0,&g_466,(void*)0,(void*)0,&g_466,&g_466},{(void*)0,&g_19,&g_19,&g_19,&g_19,(void*)0}},{{&g_19,(void*)0,(void*)0,&g_466,(void*)0,&g_466},{&g_19,(void*)0,&g_19,(void*)0,(void*)0,&g_466},{&g_466,(void*)0,&g_19,&g_466,&g_19,&g_466},{&g_19,&g_466,&g_19,&g_466,&g_19,(void*)0},{&g_466,&g_19,&g_19,(void*)0,&g_19,&g_19},{&g_466,(void*)0,(void*)0,(void*)0,&g_19,(void*)0}},{{(void*)0,&g_466,&g_19,&g_466,&g_19,(void*)0},{(void*)0,&g_466,&g_19,(void*)0,(void*)0,&g_19},{(void*)0,&g_466,&g_466,(void*)0,&g_466,&g_466},{(void*)0,(void*)0,(void*)0,&g_19,&g_19,&g_466},{&g_466,&g_466,&g_466,&g_466,&g_19,&g_19},{&g_466,&g_466,(void*)0,(void*)0,&g_19,&g_19}},{{&g_466,(void*)0,&g_466,&g_19,&g_466,&g_466},{&g_19,&g_466,&g_19,(void*)0,(void*)0,&g_19},{&g_466,&g_466,(void*)0,&g_19,&g_19,&g_466},{&g_466,&g_466,(void*)0,(void*)0,&g_19,(void*)0},{&g_19,(void*)0,(void*)0,&g_466,&g_19,&g_466},{&g_19,&g_19,&g_19,&g_466,&g_19,(void*)0}},{{(void*)0,&g_466,(void*)0,(void*)0,&g_19,&g_19},{(void*)0,(void*)0,(void*)0,&g_466,(void*)0,(void*)0},{&g_19,(void*)0,&g_19,&g_19,(void*)0,&g_19},{&g_466,(void*)0,&g_466,(void*)0,&g_19,&g_466},{(void*)0,&g_19,&g_466,&g_19,&g_466,(void*)0},{&g_466,&g_466,&g_19,&g_19,&g_19,&g_19}},{{&g_466,&g_466,&g_19,&g_19,&g_466,&g_466},{(void*)0,(void*)0,&g_466,(void*)0,&g_19,&g_19},{&g_19,(void*)0,&g_466,(void*)0,&g_466,&g_466},{&g_19,(void*)0,&g_19,&g_466,&g_19,&g_19},{&g_466,&g_19,&g_19,&g_19,&g_466,(void*)0},{&g_19,&g_466,&g_19,&g_19,&g_19,(void*)0}},{{&g_19,(void*)0,&g_19,&g_19,&g_466,&g_19},{&g_19,(void*)0,&g_466,&g_466,&g_466,(void*)0},{&g_466,(void*)0,&g_19,&g_466,&g_466,&g_19},{&g_466,(void*)0,&g_19,(void*)0,&g_19,&g_19},{&g_19,(void*)0,&g_19,(void*)0,(void*)0,(void*)0},{&g_466,&g_19,&g_19,&g_19,&g_466,(void*)0}}};
        int32_t l_467 = 0x65D40BDBL;
        int8_t *l_479[7][2][10] = {{{(void*)0,&g_311[2][1][4],(void*)0,&g_311[2][8][4],&g_311[2][1][4],&g_311[2][8][4],(void*)0,&g_311[2][1][4],(void*)0,&g_311[2][8][4]},{&g_311[2][4][4],&g_311[2][1][4],&l_183,&g_311[2][1][4],&g_311[2][4][4],&l_183,&g_311[2][4][4],&g_311[2][1][4],&l_183,&g_311[2][1][4]}},{{&g_311[2][1][4],(void*)0,(void*)0,&g_311[2][1][4],(void*)0,(void*)0,&g_311[2][1][4],(void*)0,(void*)0,&g_311[2][1][4]},{&l_183,&g_311[2][1][4],&l_183,&g_311[2][8][4],&g_311[2][4][4],(void*)0,&l_183,&g_311[2][1][4],&l_183,(void*)0}},{{&g_311[2][1][4],&g_311[2][1][4],&g_311[2][1][4],&g_311[2][1][4],&g_311[2][1][4],&l_183,&g_311[2][1][4],&g_311[2][1][4],&l_183,&g_311[2][1][4]},{&g_311[2][4][4],&g_311[2][8][4],&l_183,&g_311[2][1][4],&l_183,&g_311[2][8][4],&g_311[2][4][4],(void*)0,&l_183,&g_311[2][1][4]}},{{(void*)0,&g_311[2][1][4],(void*)0,(void*)0,&g_311[2][1][4],(void*)0,(void*)0,&g_311[2][1][4],(void*)0,(void*)0},{&g_311[2][4][4],&g_311[2][1][4],&l_183,&g_311[2][1][4],&g_311[2][4][4],&l_183,&g_311[2][4][4],&g_311[2][1][4],&l_183,&g_311[2][1][4]}},{{&g_311[2][1][4],&g_311[2][8][4],(void*)0,&g_311[2][1][4],(void*)0,&g_311[2][8][4],&g_311[2][1][4],&g_311[2][8][4],(void*)0,&g_311[2][1][4]},{&l_183,&g_311[2][1][4],&l_183,(void*)0,&g_311[2][4][4],&g_311[2][8][4],&l_183,&g_311[2][1][4],&l_183,&g_311[2][8][4]}},{{&g_311[2][1][4],&g_311[2][1][4],&g_311[2][1][4],&g_311[2][1][4],&g_311[2][1][4],&l_183,&g_311[2][1][4],&g_311[2][1][4],&l_183,&g_311[2][1][4]},{&g_311[2][4][4],(void*)0,&l_183,&g_311[2][1][4],&l_183,(void*)0,&g_311[2][4][4],&g_311[2][8][4],&l_183,&g_311[2][1][4]}},{{(void*)0,&g_311[2][1][4],(void*)0,&g_311[2][8][4],&g_311[2][1][4],&g_311[2][8][4],(void*)0,&g_311[2][1][4],(void*)0,&g_311[2][8][4]},{&g_311[2][4][4],&g_311[2][1][4],&l_183,&g_311[2][1][4],&g_311[2][4][4],&l_183,&g_311[2][4][4],&g_311[2][1][4],&l_183,&g_311[2][1][4]}}};
        int32_t l_487[9][6] = {{(-1L),(-1L),0x6D8661B1L,0x2B36794BL,0x6D8661B1L,(-1L)},{0x6D8661B1L,8L,0x2B36794BL,0x2B36794BL,8L,0xCE716D60L},{0x6D8661B1L,0xCE716D60L,8L,0xCE716D60L,0x6D8661B1L,0x6D8661B1L},{0x2B36794BL,0xCE716D60L,0xCE716D60L,0x2B36794BL,(-1L),0x2B36794BL},{0x2B36794BL,(-1L),0x2B36794BL,0xCE716D60L,0xCE716D60L,0x2B36794BL},{0x6D8661B1L,0x6D8661B1L,0xCE716D60L,8L,0xCE716D60L,0x6D8661B1L},{0xCE716D60L,(-1L),8L,8L,(-1L),0xCE716D60L},{0x6D8661B1L,0xCE716D60L,8L,0xCE716D60L,0x6D8661B1L,0x6D8661B1L},{0x2B36794BL,0xCE716D60L,0xCE716D60L,0x2B36794BL,(-1L),0x2B36794BL}};
        float *l_488 = &l_90[0];
        uint8_t l_489 = 0x23L;
        int64_t l_524 = 1L;
        int32_t l_559 = 1L;
        int32_t l_639 = 0x145240BBL;
        int16_t l_696 = 0x245EL;
        int64_t **l_741 = &g_415;
        uint8_t l_795 = 0x22L;
        uint16_t l_803 = 0xF2E6L;
        int64_t l_806 = 0x181872C8A30EEC2ELL;
        uint8_t l_835 = 0x97L;
        const int32_t l_838 = 0xD4D5C10BL;
        float l_883[3];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_883[i] = 0x9.A2E929p+57;
        if (((((safe_add_func_float_f_f(((*l_488) = (l_463 != ((l_464 == &l_373) != (((l_467 = (*l_86)) != (safe_div_func_uint16_t_u_u(65535UL, (((l_487[7][0] = ((safe_rshift_func_uint8_t_u_u(l_463, (safe_unary_minus_func_int8_t_s((safe_mod_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((((((((safe_add_func_float_f_f((((g_311[0][5][4] = l_463) , (((safe_mod_func_int64_t_s_s((*l_86), (safe_mul_func_uint8_t_u_u((!(safe_mod_func_uint16_t_u_u((0x3AAD1F59B6CD46E5LL == l_463), (*l_86)))), 1L)))) <= (*l_86)) | l_463)) , (*l_86)), (*l_86))) , 4294967293UL) != g_449) && l_463) == l_463) < 0xFC3DL) , g_32), l_463)), (*l_86))))))) && (*p_72))) | (*l_86)) ^ (*l_86))))) , (*l_86))))), l_463)) == 0x2.7p-1) , l_489) >= 0x15L))
        { /* block id: 183 */
            int64_t l_490 = 1L;
            uint16_t l_491 = 3UL;
            int32_t l_498 = 0xDB93C4E6L;
            int32_t l_499[2][1][1];
            const uint8_t ***l_521[1];
            int32_t **l_551 = &g_68;
            int64_t * const *l_585[2][2];
            uint32_t *l_611 = &g_294;
            uint8_t * const **l_643 = (void*)0;
            int32_t *l_679 = &l_499[0][0][0];
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_499[i][j][k] = (-8L);
                }
            }
            for (i = 0; i < 1; i++)
                l_521[i] = (void*)0;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 2; j++)
                    l_585[i][j] = (void*)0;
            }
            if ((l_490 , l_491))
            { /* block id: 184 */
                uint32_t *l_493 = &g_403;
                int32_t l_523 = 9L;
                int16_t *l_536 = &g_246[2];
                int32_t *l_538 = &l_523;
                int32_t l_568 = (-1L);
                (*l_86) &= ((safe_unary_minus_func_int32_t_s((*p_72))) ^ (((*l_493)--) == (18446744073709551615UL ^ 0x1BFB572EDA09387FLL)));
                for (l_456 = 5; (l_456 >= 1); l_456 -= 1)
                { /* block id: 189 */
                    int32_t *l_496 = &l_124;
                    int32_t *l_497[8][10][3] = {{{&g_28,&l_467,&l_467},{&l_467,&g_28,&g_32},{&l_487[7][0],&l_96,&g_89},{&g_28,&g_28,&l_124},{&l_467,&l_467,&l_487[7][0]},{&g_89,&g_89,&g_28},{&g_89,&g_28,&g_28},{&l_467,&l_96,&l_96},{&g_28,&l_124,&l_467},{&l_487[7][0],&l_467,&l_96}},{{&l_467,&l_379,&g_28},{&g_28,&l_96,&g_28},{(void*)0,&l_96,&l_487[7][0]},{&l_487[7][0],&l_379,&l_124},{&l_96,&l_467,&g_89},{&l_467,&l_124,&g_32},{&l_96,&l_96,&l_467},{&l_487[7][0],&g_28,&g_28},{(void*)0,&g_89,&g_28},{&g_28,&l_467,&l_467}},{{&l_467,&g_28,&g_32},{&l_487[7][0],&l_96,&g_89},{&g_28,&g_28,&l_124},{&l_467,&l_467,&l_487[7][0]},{&g_89,&g_89,&g_28},{&g_89,&g_28,&g_28},{&l_467,&l_96,&l_96},{&g_28,&l_124,&l_467},{&l_487[7][0],&l_467,&l_96},{&l_467,&l_379,&g_28}},{{&g_28,&l_96,&g_28},{(void*)0,&l_96,&l_487[7][0]},{&l_487[7][0],&l_379,&l_124},{&l_96,&l_467,&g_89},{&l_467,&l_124,&g_32},{&l_96,&l_96,&l_467},{&l_487[7][0],&g_28,&g_28},{(void*)0,&g_89,&g_28},{&g_28,&l_467,&l_467},{&l_467,&g_28,&g_32}},{{&l_487[7][0],&l_96,&g_89},{&g_28,&g_28,&l_124},{&l_467,&l_467,&l_487[7][0]},{&g_89,&g_89,&g_28},{&g_89,&g_28,&g_28},{&l_467,&l_96,&l_96},{&g_28,&l_124,&l_467},{&l_487[7][0],&l_467,&l_96},{&l_467,&l_379,&g_28},{&g_28,&l_124,(void*)0}},{{&l_96,&l_124,&g_28},{&g_28,&l_96,&l_467},{&l_124,&l_467,&l_487[7][0]},{&g_28,&l_467,&g_28},{&l_124,&l_467,&l_467},{&g_28,&g_89,&g_89},{&l_96,&l_487[7][0],&g_89},{(void*)0,&g_28,&l_467},{&g_32,(void*)0,&g_28},{(void*)0,&l_96,&l_487[7][0]}},{{&l_487[7][0],(void*)0,&l_467},{&l_467,&g_28,&g_28},{&l_487[7][0],&l_487[7][0],(void*)0},{&l_487[7][0],&g_89,&l_487[7][0]},{&l_467,&l_467,&l_96},{&l_487[7][0],&l_467,&l_467},{(void*)0,&l_467,&l_96},{&g_32,&l_96,&l_487[7][0]},{(void*)0,&l_124,(void*)0},{&l_96,&l_124,&g_28}},{{&g_28,&l_96,&l_467},{&l_124,&l_467,&l_487[7][0]},{&g_28,&l_467,&g_28},{&l_124,&l_467,&l_467},{&g_28,&g_89,&g_89},{&l_96,&l_487[7][0],&g_89},{(void*)0,&g_28,&l_467},{&g_32,(void*)0,&g_28},{(void*)0,&l_96,&l_487[7][0]},{&l_487[7][0],(void*)0,&l_467}}};
                    int i, j, k;
                    --g_500;
                    for (l_489 = 2; (l_489 <= 7); l_489 += 1)
                    { /* block id: 193 */
                        int32_t l_516 = (-2L);
                        int i;
                        (*l_496) = ((safe_div_func_int8_t_s_s(((safe_mod_func_uint64_t_u_u((safe_sub_func_int64_t_s_s((l_509 == ((safe_lshift_func_int16_t_s_s((safe_add_func_uint16_t_u_u(((((g_374[(l_489 + 2)] , &g_129[6]) == ((safe_div_func_uint64_t_u_u(l_516, (safe_mul_func_uint8_t_u_u(((!(l_487[7][0] = (0x3D9DED70L > (l_498 < (!g_246[0]))))) == 0x1C8EL), (g_19 > (*l_496)))))) , l_521[0])) && 65535UL) && 65529UL), g_522[1][1][0])), l_499[1][0][0])) , l_107[8][1][3])), (*g_415))), g_202[8][4])) < 0x7C4B1933237D9C39LL), 1UL)) < l_523);
                        if (l_524)
                            break;
                        if ((*p_72))
                            break;
                    }
                    (*l_496) = g_374[(l_456 + 3)];
                    (*g_525) = &g_89;
                }
                if ((((safe_rshift_func_uint16_t_u_u((((((((*l_86) & ((safe_mul_func_uint16_t_u_u(g_402, ((*l_536) = (safe_sub_func_uint64_t_u_u(0xE1480AFC95339341LL, (((*l_86) | g_232) ^ ((safe_lshift_func_uint8_t_u_u(((l_489 < ((((safe_div_func_uint32_t_u_u((l_523 , ((l_467 = 0xB0L) && (l_467 & l_523))), (*p_72))) != l_523) , l_523) || g_311[1][8][5])) & (*l_86)), 1)) == l_523))))))) , (-7L))) <= g_103) , 0x81L) , g_246[3]) , 1UL) & (*l_86)), l_537[2][3])) < (*g_415)) != l_463))
                { /* block id: 204 */
                    int64_t l_557[8][6] = {{0L,(-3L),0xF8D67716B19010ACLL,0x0091F6346946E5B2LL,0xF8D67716B19010ACLL,(-3L)},{0x19CE5C9B4B8B33C3LL,0xF91E71C99DF37147LL,0x0ACB233FFB6A3B96LL,1L,5L,0x0ACB233FFB6A3B96LL},{0x19CE5C9B4B8B33C3LL,0xF8D67716B19010ACLL,5L,0x0091F6346946E5B2LL,9L,0x7802D0260EBB5FD9LL},{0L,0xF8D67716B19010ACLL,0xF91E71C99DF37147LL,1L,5L,5L},{0xA1047E7917779D9ELL,0xF91E71C99DF37147LL,0xF91E71C99DF37147LL,0xA1047E7917779D9ELL,0xF8D67716B19010ACLL,0x7802D0260EBB5FD9LL},{0x0091F6346946E5B2LL,(-3L),5L,0xA1047E7917779D9ELL,0x7802D0260EBB5FD9LL,0x0ACB233FFB6A3B96LL},{0xA1047E7917779D9ELL,0x7802D0260EBB5FD9LL,0x0ACB233FFB6A3B96LL,1L,0x7802D0260EBB5FD9LL,(-3L)},{0L,(-3L),0xF8D67716B19010ACLL,0x0091F6346946E5B2LL,0xF8D67716B19010ACLL,(-3L)}};
                    int i, j;
lbl_543:
                    l_538 = p_71;
                    for (l_491 = 0; (l_491 <= 1); l_491 += 1)
                    { /* block id: 208 */
                        int16_t * volatile **l_541 = &g_539;
                        int32_t **l_546 = &l_86;
                        int32_t ***l_552 = &l_551;
                        uint64_t *l_558 = &g_92;
                        (*l_541) = g_539;
                        (*g_542) = &l_379;
                        if (g_48)
                            goto lbl_543;
                        l_559 |= (g_294 , ((((safe_mod_func_int64_t_s_s((((*l_546) = (*g_525)) != (void*)0), ((*l_558) &= ((safe_sub_func_int64_t_s_s((l_467 | (safe_rshift_func_uint8_t_u_s((*g_130), (((*g_415) |= (((((*l_552) = l_551) != (void*)0) == ((safe_add_func_int32_t_s_s(0x5BEA22D9L, (safe_mul_func_int16_t_s_s((((((*g_68) || l_557[1][0]) , (void*)0) != &g_130) , (-1L)), 65535UL)))) || l_487[7][0])) | l_96)) | l_183)))), 0xEE617969053A3550LL)) > g_202[7][1])))) | 0xBC2B9435L) < (*g_130)) , (*g_68)));
                    }
                }
                else
                { /* block id: 218 */
                    int32_t l_562 = 1L;
                    int32_t *l_563 = &l_559;
                    int32_t *l_564 = &g_89;
                    int32_t *l_565 = &l_523;
                    int32_t *l_566 = &l_559;
                    int32_t *l_567[3];
                    int16_t l_589 = (-1L);
                    int i;
                    for (i = 0; i < 3; i++)
                        l_567[i] = &l_523;
                    for (l_456 = 10; (l_456 >= (-10)); l_456 = safe_sub_func_uint64_t_u_u(l_456, 9))
                    { /* block id: 221 */
                        return (*p_72);
                    }
                    l_562 = ((*l_86) &= l_562);
                    g_570--;
                    if ((safe_rshift_func_uint16_t_u_s((safe_sub_func_int8_t_s_s((((((g_232 ^ ((g_382 < (((*l_564) < (safe_add_func_uint8_t_u_u((&g_415 != ((safe_add_func_uint16_t_u_u((((((3UL <= (safe_div_func_uint64_t_u_u((safe_mod_func_uint64_t_u_u((*l_565), 0xED94A2F449240F57LL)), (*l_564)))) , (*l_565)) , g_522[2][0][3]) & (*p_72)) == (*l_86)), (*l_86))) , l_585[1][1])), (*l_86)))) , (*l_538))) >= (**g_539))) , 0UL) >= 0xF9L) > (*l_538)) || (*l_564)), (*l_86))), 11)))
                    { /* block id: 227 */
                        uint64_t l_586 = 18446744073709551606UL;
                        (*l_551) = p_71;
                        l_586--;
                    }
                    else
                    { /* block id: 230 */
                        int16_t l_598 = (-2L);
                        (*l_551) = (*g_525);
                        (*l_551) = (l_487[0][5] , (l_589 , ((((*l_563) = (*p_72)) , ((&g_542 != &g_525) , (safe_mul_func_int16_t_s_s(((g_311[1][9][2] = ((*l_86) == (g_159 , (*l_538)))) <= ((safe_rshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_u((safe_mod_func_int16_t_s_s((**g_539), (-7L))), l_487[7][5])), g_32)) == g_246[0])), 65535UL)))) , (*g_542))));
                        --g_599;
                        (*l_565) = ((*l_566) = ((l_487[7][0] ^= (*l_538)) == (safe_mod_func_int16_t_s_s((!(safe_mod_func_uint16_t_u_u(((((safe_rshift_func_uint8_t_u_u((l_598 | (l_493 != l_611)), 7)) , &l_379) == ((((l_598 == (~(((*l_536) = (**g_539)) , 0UL))) , (((!(((((-1L) < g_522[1][1][0]) == (*l_566)) >= (*g_418)) || 0xE7992E8DE8D97E15LL)) , (-10L)) & (-4L))) & (-7L)) , (*g_525))) , (*l_564)), (-10L)))), (*l_538)))));
                    }
                }
            }
            else
            { /* block id: 242 */
                int16_t l_614 = 0x0527L;
                int32_t l_620[5][10] = {{0xE9EE20F9L,8L,(-1L),0xDC396F67L,0xDC396F67L,(-1L),8L,0xE9EE20F9L,8L,(-1L)},{0x82BD5820L,0x3DA04279L,0xDC396F67L,0x3DA04279L,0x82BD5820L,(-1L),(-1L),0x82BD5820L,0x3DA04279L,0xDC396F67L},{0xE9EE20F9L,0xE9EE20F9L,0xDC396F67L,0x82BD5820L,5L,0x82BD5820L,0xDC396F67L,0xE9EE20F9L,0xE9EE20F9L,0xDC396F67L},{0x3DA04279L,0x82BD5820L,(-1L),(-1L),0x82BD5820L,0x3DA04279L,0xDC396F67L,0x3DA04279L,0x82BD5820L,(-1L)},{8L,0xE9EE20F9L,8L,(-1L),0xDC396F67L,0xDC396F67L,(-1L),8L,0xE9EE20F9L,8L}};
                int32_t l_627 = (-2L);
                uint32_t *l_642 = &l_463;
                uint8_t ***l_647 = (void*)0;
                uint8_t ****l_646 = &l_647;
                int32_t *l_651 = (void*)0;
                int32_t *l_652 = &l_620[1][9];
                int32_t *l_653 = &l_379;
                int i, j;
                l_379 &= ((l_614 >= ((*l_86) = (*p_72))) && (+((safe_lshift_func_uint8_t_u_u(((l_620[1][9] = ((*l_409) = 255UL)) < (0xE497L != ((safe_div_func_int8_t_s_s(((safe_unary_minus_func_uint16_t_u(g_202[7][1])) >= 0L), (safe_add_func_uint16_t_u_u(l_524, (((~((l_627 ^= ((**g_539) = 0x1241L)) , (*l_86))) >= 0xB2B8CA537DBEFF98LL) || l_463))))) ^ 0xA47EA09EL))), (*l_86))) | (-7L))));
                for (g_294 = 0; (g_294 < 41); g_294 = safe_add_func_uint16_t_u_u(g_294, 1))
                { /* block id: 251 */
                    uint32_t l_630 = 8UL;
                    ++l_630;
                }
                (*l_653) |= (safe_add_func_uint32_t_u_u(((*l_642) = ((*l_86) = (safe_lshift_func_uint16_t_u_s(((*l_86) , (l_639 ^= (safe_mul_func_uint16_t_u_u((l_627 , ((*l_86) , (4294967289UL | 9UL))), (--g_466))))), 2)))), (((g_644 = l_643) != ((*l_646) = &g_129[4])) | (0xBBF45438L || ((*l_652) = ((~(safe_lshift_func_uint16_t_u_u(1UL, 9))) || l_559))))));
                for (g_500 = (-25); (g_500 < 58); g_500 = safe_add_func_int32_t_s_s(g_500, 7))
                { /* block id: 264 */
                    uint16_t l_660 = 1UL;
                    int32_t *l_663 = &l_499[0][0][0];
                    int32_t *l_664 = &l_499[0][0][0];
                    int32_t *l_665 = (void*)0;
                    int32_t *l_666 = &l_499[1][0][0];
                    int32_t *l_667 = &l_620[1][9];
                    int32_t l_668 = (-6L);
                    int32_t *l_669 = (void*)0;
                    int32_t *l_670 = (void*)0;
                    int32_t *l_671 = &l_559;
                    int32_t *l_672[6] = {&l_124,&l_124,&l_124,&l_124,&l_124,&l_124};
                    int i;
                    for (l_489 = (-22); (l_489 > 38); l_489++)
                    { /* block id: 267 */
                        return (*p_72);
                    }
                    for (g_382 = 0; (g_382 > 20); g_382++)
                    { /* block id: 272 */
                        l_660--;
                        return (*p_72);
                    }
                    --g_673;
                }
            }
            if (l_639)
            { /* block id: 279 */
                for (g_599 = 3; (g_599 < 22); g_599 = safe_add_func_int64_t_s_s(g_599, 5))
                { /* block id: 282 */
                    int32_t **l_678[8] = {&l_86,&l_86,&l_86,&l_86,&l_86,&l_86,&l_86,&l_86};
                    int i;
                    l_679 = ((*l_551) = (*l_551));
                }
            }
            else
            { /* block id: 286 */
                uint32_t l_686[9][6] = {{0x8C867A49L,18446744073709551612UL,18446744073709551615UL,18446744073709551615UL,18446744073709551612UL,0x8C867A49L},{0UL,0x8C867A49L,18446744073709551615UL,0x8C867A49L,0UL,0UL},{0xC3169FBAL,0x8C867A49L,0x8C867A49L,0xC3169FBAL,18446744073709551612UL,0xC3169FBAL},{0xC3169FBAL,18446744073709551612UL,0xC3169FBAL,0x8C867A49L,0x8C867A49L,0xC3169FBAL},{0UL,0UL,0x8C867A49L,18446744073709551615UL,0x8C867A49L,0UL},{0x8C867A49L,18446744073709551612UL,18446744073709551615UL,18446744073709551615UL,18446744073709551612UL,0x8C867A49L},{0UL,0x8C867A49L,18446744073709551615UL,0x8C867A49L,0UL,0UL},{0xC3169FBAL,0x8C867A49L,0x8C867A49L,0xC3169FBAL,18446744073709551612UL,0xC3169FBAL},{0xC3169FBAL,18446744073709551612UL,0xC3169FBAL,0x8C867A49L,0x8C867A49L,0xC3169FBAL}};
                int16_t *l_691[6][2] = {{&g_246[3],&g_246[0]},{&g_246[0],&g_246[3]},{&g_246[0],&g_246[0]},{&g_246[3],&g_246[0]},{&g_246[0],&g_246[3]},{&g_246[0],&g_246[0]}};
                int32_t l_693 = 0xFDC20A31L;
                int32_t l_705[5];
                int i, j;
                for (i = 0; i < 5; i++)
                    l_705[i] = 0L;
                if ((safe_mul_func_int8_t_s_s(g_246[0], ((((g_419 && (safe_lshift_func_uint16_t_u_s(((l_693 |= ((*l_679) != ((l_686[4][1] >= ((g_692 ^= ((g_311[2][1][4] &= (-1L)) | ((5L != (*l_679)) < (((((safe_mod_func_int32_t_s_s((*l_86), ((safe_lshift_func_uint8_t_u_u(((l_487[2][5] = ((*l_488) = 0x6.5EF864p-99)) , l_463), l_639)) , 9UL))) ^ l_559) | 0x2D8BFE60L) && (*p_72)) || (**g_539))))) > l_686[4][1])) ^ (-2L)))) || (-4L)), l_524))) , (**g_539)) && 0x3DC0L) & g_243))))
                { /* block id: 292 */
                    int32_t *l_694 = &g_89;
                    int32_t *l_695[9][6][4] = {{{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379}},{{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]}},{{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379}},{{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]}},{{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379}},{{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]}},{{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379}},{{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]}},{{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379},{&g_28,&l_693,&l_487[7][0],&l_693},{&g_28,&l_379,&g_89,&l_499[0][0][0]},{&g_28,&l_467,&l_487[7][0],&l_467},{&g_28,&l_499[0][0][0],&g_89,&l_379}}};
                    int i, j, k;
                    ++g_698;
                    g_701--;
                }
                else
                { /* block id: 295 */
                    int32_t *l_704[10][1][6] = {{{&l_96,&g_32,&l_96,&g_28,&g_32,&l_639}},{{&l_96,&l_487[1][2],&g_28,&g_28,&l_487[1][2],&l_96}},{{&l_96,(void*)0,&l_639,&g_28,(void*)0,&g_28}},{{&l_96,&g_32,&l_96,&g_28,&g_32,&l_639}},{{&l_96,&l_487[1][2],&g_28,&g_28,&l_487[1][2],&l_96}},{{&l_96,(void*)0,&l_639,&g_28,(void*)0,&g_28}},{{&l_96,&g_32,&l_96,&g_28,&g_32,&l_639}},{{&l_96,&l_487[1][2],&g_28,&g_28,&l_487[1][2],&l_96}},{{&l_96,(void*)0,&l_639,&g_28,(void*)0,&g_28}},{{&l_96,&g_32,&l_96,&g_28,&g_32,&l_639}}};
                    int i, j, k;
                    g_706--;
                }
                for (l_183 = 1; (l_183 <= 7); l_183 += 1)
                { /* block id: 300 */
                    return (*p_72);
                }
            }
            (*l_551) = (*l_551);
            for (g_458 = 0; (g_458 < 56); g_458 = safe_add_func_int16_t_s_s(g_458, 7))
            { /* block id: 307 */
                uint16_t l_731[3];
                const int8_t l_732 = 0xFAL;
                int64_t *l_733[9][2][10] = {{{&g_243,&g_449,&g_522[1][1][0],(void*)0,&g_522[3][0][2],&l_524,&l_490,&l_524,&g_398,&g_449},{&g_522[2][0][2],&g_398,(void*)0,&g_398,(void*)0,&g_522[2][0][2],&l_524,&l_490,&g_243,&g_449}},{{(void*)0,(void*)0,(void*)0,&g_398,&g_522[3][0][2],&g_398,&l_490,&g_569,&g_522[1][0][3],&g_522[0][0][0]},{&g_522[0][0][2],&g_243,&g_569,(void*)0,&l_524,&g_569,&g_243,&l_490,(void*)0,&g_522[1][1][0]}},{{&g_569,&g_243,&l_490,(void*)0,&g_522[1][1][0],(void*)0,&l_490,&g_243,&g_569,&l_524},{&g_243,&l_490,&l_490,&l_490,&g_522[0][0][0],&g_522[1][0][3],&g_569,&l_490,&l_490,&g_243}},{{&l_490,&g_398,&g_569,&l_490,(void*)0,&g_243,&g_398,&g_569,&g_569,&g_398},{(void*)0,&l_490,&g_522[2][0][2],(void*)0,(void*)0,(void*)0,(void*)0,&g_398,(void*)0,&g_243}},{{&g_522[1][1][0],&g_398,&l_490,(void*)0,&g_522[0][0][0],(void*)0,&g_569,(void*)0,&g_522[1][0][3],&l_524},{(void*)0,&l_490,&l_524,(void*)0,&g_522[1][1][0],&g_243,&g_569,&g_569,&g_243,&g_522[1][1][0]}},{{&l_490,&g_398,&g_398,&l_490,&l_524,&g_522[1][0][3],(void*)0,&g_569,(void*)0,&g_522[0][0][0]},{&g_243,&l_490,&l_524,&g_522[0][0][2],&g_243,(void*)0,&g_398,(void*)0,(void*)0,(void*)0}},{{&g_569,&g_398,&l_490,&l_490,&g_398,&g_569,&g_569,&g_398,&g_243,(void*)0},{&g_522[0][0][2],&l_490,&g_522[2][0][2],(void*)0,&g_243,&l_490,&l_490,&g_569,&g_522[1][0][3],&g_522[0][0][0]}},{{&g_522[0][0][2],&g_243,&g_569,(void*)0,&l_524,&g_569,&g_243,&l_490,(void*)0,&g_522[1][1][0]},{&g_569,&g_243,&l_490,(void*)0,&g_522[1][1][0],(void*)0,&l_490,&g_243,&g_569,&l_524}},{{&g_243,&l_490,&l_490,&l_490,&g_522[0][0][0],&g_522[1][0][3],&g_569,&l_490,&l_490,&g_243},{&l_490,&g_398,&g_569,&l_490,(void*)0,&g_243,&g_398,&g_569,&g_569,&g_398}}};
                int32_t *l_740[2][3];
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_731[i] = 0UL;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_740[i][j] = &l_498;
                }
                (*l_551) = p_72;
                for (g_243 = 0; (g_243 > (-1)); g_243--)
                { /* block id: 311 */
                    const uint8_t ****l_735 = (void*)0;
                    (*l_86) = (-3L);
                    for (l_373 = 1; (l_373 <= 7); l_373 += 1)
                    { /* block id: 315 */
                        uint16_t ***l_715 = &l_713;
                        float **l_723 = &l_488;
                        float ***l_722 = &l_723;
                        int32_t l_730 = (-3L);
                        uint32_t *l_734 = &l_463;
                        int i;
                        (*g_542) = p_71;
                        (*l_715) = l_713;
                        (*l_551) = p_72;
                        (*l_86) |= (g_334 > (safe_add_func_int32_t_s_s(((((((**g_539) = (l_718[0] != ((((safe_div_func_float_f_f(((((*l_722) = (void*)0) == (void*)0) > (((g_724 != (((safe_lshift_func_uint8_t_u_u((g_692 | ((*l_734) = ((safe_unary_minus_func_int8_t_s(0x5DL)) , (l_730 ^ (((((((l_731[2] & l_732) , &g_522[3][1][3]) != l_733[2][1][4]) && (**g_725)) , (*g_415)) != (*g_415)) , l_731[2]))))), l_731[2])) ^ (-6L)) , &g_725)) , 1L) , g_111[l_373])), 0x1.9p-1)) , 0xE961ED73L) , l_730) , l_735))) != 65534UL) , (*g_418)) != l_731[2]) == l_487[3][1]), l_731[2])));
                    }
                    (*l_488) = (safe_add_func_float_f_f((-0x1.Ap-1), l_731[2]));
                }
                if ((*g_68))
                { /* block id: 326 */
                    if ((*l_86))
                        break;
                    for (l_559 = (-27); (l_559 > (-21)); l_559 = safe_add_func_uint8_t_u_u(l_559, 8))
                    { /* block id: 330 */
                        (*l_551) = p_71;
                        (*l_551) = l_740[1][2];
                    }
                }
                else
                { /* block id: 334 */
                    (*l_86) = 2L;
                }
            }
        }
        else
        { /* block id: 338 */
            int64_t ***l_742 = &l_741;
            int32_t l_751 = (-10L);
            int64_t *l_761 = (void*)0;
            int32_t l_771[8][1][4] = {{{4L,0xF3B8012AL,4L,0xF3B8012AL}},{{4L,0xF3B8012AL,4L,0xF3B8012AL}},{{4L,0xF3B8012AL,4L,0xF3B8012AL}},{{4L,0xF3B8012AL,4L,0xF3B8012AL}},{{4L,0xF3B8012AL,4L,0xF3B8012AL}},{{4L,0xF3B8012AL,4L,0xF3B8012AL}},{{4L,0xF3B8012AL,4L,0xF3B8012AL}},{{4L,0xF3B8012AL,4L,0xF3B8012AL}}};
            uint8_t ****l_775 = &l_719;
            int32_t l_793 = (-1L);
            uint16_t **l_884 = &l_714[0];
            int8_t l_905[6][1][6] = {{{7L,6L,0x7CL,7L,0x4DL,0x4DL}},{{6L,0L,0L,6L,0x4DL,7L}},{{0x7CL,0L,0x4DL,0x7CL,0x4DL,0L}},{{(-1L),0L,7L,(-1L),0x4DL,0x4DL}},{{6L,0L,0L,6L,0x4DL,7L}},{{0x7CL,0L,0x4DL,0x7CL,0x4DL,0L}}};
            int32_t l_997 = 0L;
            float l_1026[1][7][10] = {{{0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1),0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1)},{0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1),0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1)},{0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1),0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1)},{0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1),0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1)},{0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1),0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1)},{0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1),0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1)},{0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1),0x0.887B72p-92,(-0x6.Bp+1),0xA.4309EAp+89,0xA.4309EAp+89,(-0x6.Bp+1)}}};
            uint32_t l_1034 = 4294967290UL;
            int32_t **l_1044[9][6] = {{&l_86,&g_68,&g_68,&g_68,&g_68,&l_86},{&g_68,&g_68,&l_86,(void*)0,&g_68,&l_86},{&l_86,(void*)0,&g_68,(void*)0,&l_86,&g_68},{&l_86,&g_68,(void*)0,(void*)0,(void*)0,(void*)0},{&g_68,&g_68,&g_68,&g_68,&g_68,(void*)0},{&l_86,(void*)0,(void*)0,&g_68,(void*)0,&g_68},{&g_68,(void*)0,&g_68,&l_86,(void*)0,&l_86},{(void*)0,(void*)0,&l_86,&g_68,&g_68,&l_86},{&g_68,&g_68,&g_68,&g_68,(void*)0,&l_86}};
            int i, j, k;
            if (((l_467 | ((l_696 || 0UL) >= ((((*l_742) = l_741) != ((safe_add_func_uint64_t_u_u(g_570, (safe_rshift_func_int8_t_s_u(((safe_unary_minus_func_int8_t_s((safe_div_func_int64_t_s_s((l_639 = ((*g_415) = (+(l_751 < (safe_mul_func_uint16_t_u_u(((safe_mod_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((-1L), l_760)), l_639)), 0xD1L)) == g_522[3][0][1]), l_751)))))), l_463)))) || (*l_86)), 6)))) , (void*)0)) != l_751))) <= (*p_72)))
            { /* block id: 342 */
                uint8_t l_772[3][8][5] = {{{0xCEL,0x0FL,255UL,0xCBL,255UL},{250UL,250UL,5UL,0x2DL,6UL},{0UL,0x21L,0xCEL,0x0FL,255UL},{1UL,0x2DL,249UL,249UL,0x2DL},{255UL,0x21L,1UL,248UL,0x22L},{0x4AL,250UL,0x4AL,249UL,5UL},{255UL,0x0FL,0x22L,0x0FL,255UL},{0x2DL,0x4AL,6UL,249UL,6UL}},{{0xC5L,5UL,255UL,0x6BL,0UL},{0x4AL,0x2DL,0x2DL,0x4AL,6UL},{0xCEL,0x6BL,0x22L,248UL,1UL},{6UL,0x2DL,5UL,250UL,250UL},{255UL,5UL,255UL,248UL,255UL},{249UL,0x4AL,250UL,0x4AL,249UL},{255UL,0x21L,0xC5L,0x6BL,0xC5L},{6UL,6UL,250UL,249UL,1UL}},{{0xCEL,0xCBL,255UL,0x21L,0xC5L},{0x4AL,249UL,5UL,5UL,249UL},{0xC5L,0xCBL,0x22L,255UL,255UL},{0x2DL,6UL,0x2DL,5UL,250UL},{1UL,0x21L,255UL,0x21L,1UL},{0x2DL,0x4AL,6UL,249UL,6UL},{0xC5L,5UL,255UL,0x6BL,0UL},{0x4AL,0x2DL,0x2DL,0x4AL,6UL}}};
                int i, j, k;
                for (l_96 = (-9); (l_96 > 5); ++l_96)
                { /* block id: 345 */
                    int32_t **l_765[9][7][1] = {{{&l_86},{&g_68},{&l_86},{(void*)0},{&g_68},{&g_68},{&g_68}},{{&g_68},{&g_68},{&g_68},{(void*)0},{&g_68},{(void*)0},{&g_68}},{{&g_68},{&g_68},{&g_68},{&g_68},{&g_68},{(void*)0},{&l_86}},{{&g_68},{&l_86},{(void*)0},{(void*)0},{&l_86},{&g_68},{&l_86}},{{(void*)0},{&g_68},{&g_68},{&g_68},{&g_68},{&g_68},{&g_68}},{{(void*)0},{&g_68},{(void*)0},{&g_68},{&g_68},{&g_68},{&g_68}},{{&g_68},{&g_68},{(void*)0},{&l_86},{&g_68},{&l_86},{(void*)0}},{{(void*)0},{&l_86},{&g_68},{&l_86},{(void*)0},{&g_68},{&g_68}},{{&g_68},{&g_68},{&g_68},{&g_68},{(void*)0},{&g_68},{(void*)0}}};
                    uint8_t *****l_776 = &l_718[0];
                    uint32_t l_782 = 0x0D40E97CL;
                    int i, j, k;
                    p_72 = &l_379;
                    (*l_488) = (!l_751);
                    if ((*p_72))
                        break;
                    l_487[1][3] = ((((0xB.E1ED88p+22 > ((*l_488) = (safe_sub_func_float_f_f((safe_add_func_float_f_f((l_771[0][0][3] != (l_772[1][5][0] >= l_772[1][5][0])), ((safe_add_func_float_f_f(((*g_156) < (l_524 > (((&g_644 == ((*l_776) = l_775)) != (((safe_rshift_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_u(((+((l_771[0][0][3] < l_696) <= l_771[0][0][3])) , l_524), 3)) || (*g_415)), 13)) , l_772[1][0][4]) , 0x8AFAL)) , (*l_86)))), (*l_86))) >= l_782))), 0x3.8p-1)))) > (*l_86)) < l_559) , (-0x9.Dp+1));
                }
            }
            else
            { /* block id: 353 */
                int32_t *l_783[10][5] = {{(void*)0,&l_379,&g_32,&g_32,&l_379},{&l_467,&l_751,&l_639,&l_751,&l_467},{&l_379,&g_32,&g_32,&l_379,(void*)0},{&g_28,&l_751,&g_28,&g_28,&g_28},{&l_379,&l_379,&l_639,&g_32,(void*)0},{&l_467,&g_28,&l_639,&g_28,&l_467},{(void*)0,&g_32,&l_639,&l_379,&l_379},{&g_28,&g_28,&g_28,&l_751,&g_28},{(void*)0,&l_379,&g_32,&g_32,&l_379},{&l_467,&l_751,&l_639,&l_751,&l_467}};
                int i, j;
                ++g_784;
            }
lbl_839:
            for (g_103 = (-30); (g_103 != 18); g_103 = safe_add_func_int32_t_s_s(g_103, 8))
            { /* block id: 358 */
                int8_t l_789 = (-1L);
                int32_t *l_790 = &g_32;
                int32_t *l_791[8];
                int64_t l_794 = 3L;
                int i;
                for (i = 0; i < 8; i++)
                    l_791[i] = (void*)0;
                if (l_789)
                    break;
                ++l_795;
            }
            if ((safe_div_func_int16_t_s_s((*l_86), 65533UL)))
            { /* block id: 362 */
                int32_t *l_802 = &l_639;
                int32_t l_808 = 0x86999B32L;
                int32_t l_810 = 0x4415BA99L;
                int32_t l_811[10] = {9L,9L,9L,9L,9L,9L,9L,9L,9L,9L};
                uint8_t *****l_823 = (void*)0;
                int i;
                for (g_673 = 0; (g_673 >= 53); g_673 = safe_add_func_uint16_t_u_u(g_673, 8))
                { /* block id: 365 */
                    l_802 = (void*)0;
                    if ((*l_86))
                        break;
                    (*l_86) |= (l_803 > 1L);
                }
                for (l_760 = 8; (l_760 >= 0); l_760 -= 1)
                { /* block id: 372 */
                    int32_t l_804 = (-1L);
                    int32_t *l_805[10][2][5] = {{{&l_804,&l_792[1][2],&l_804,&l_804,&l_792[1][2]},{&l_96,&g_28,&g_28,&l_96,&g_28}},{{&l_792[1][2],&l_792[1][2],(void*)0,&l_792[1][2],&l_792[1][2]},{&g_28,&l_96,&g_28,&g_28,&l_96}},{{&l_792[1][2],&l_804,&l_804,&l_792[1][2],&l_804},{&l_96,&l_96,&l_751,&l_96,&l_96}},{{&l_804,&l_792[1][2],&l_804,&l_804,&l_792[1][2]},{&l_96,&g_28,&g_28,&l_96,&g_28}},{{&l_792[1][2],&l_792[1][2],(void*)0,&l_792[1][2],&l_792[1][2]},{&g_28,&l_96,&g_28,&g_28,&l_96}},{{&l_792[1][2],&l_804,&l_804,&l_792[1][2],&l_804},{&l_96,&l_96,&l_751,&l_96,&l_96}},{{&l_804,&l_792[1][2],&l_804,&l_804,&l_792[1][2]},{&l_96,&g_28,&g_28,&l_96,&g_28}},{{&l_792[1][2],&l_792[1][2],(void*)0,&l_792[1][2],&l_792[1][2]},{&g_28,&l_96,&g_28,&g_28,&l_96}},{{&l_792[1][2],&l_804,&l_804,&l_792[1][2],&l_804},{&l_96,&l_96,&l_751,&l_96,&l_96}},{{&l_804,&l_792[1][2],&l_804,&l_804,&l_792[1][2]},{&l_96,&g_28,&g_28,&l_96,&g_28}}};
                    int16_t *l_828 = (void*)0;
                    int16_t *l_829[10][7] = {{(void*)0,&g_246[3],(void*)0,&g_246[2],(void*)0,&g_246[3],(void*)0},{&l_696,&g_246[0],(void*)0,(void*)0,(void*)0,&g_246[0],&l_696},{(void*)0,&g_246[3],(void*)0,&g_246[2],(void*)0,&g_246[3],(void*)0},{&l_696,&g_246[0],(void*)0,(void*)0,(void*)0,&g_246[0],&l_696},{(void*)0,&g_246[3],(void*)0,&g_246[2],(void*)0,&g_246[3],(void*)0},{&l_696,&g_246[0],(void*)0,(void*)0,(void*)0,&g_246[0],&l_696},{(void*)0,&g_246[3],(void*)0,&g_246[2],(void*)0,&g_246[3],(void*)0},{&l_696,&g_246[0],(void*)0,(void*)0,(void*)0,&g_246[0],&l_696},{(void*)0,&g_246[3],(void*)0,&g_246[2],(void*)0,&g_246[3],(void*)0},{&l_696,&g_246[0],(void*)0,(void*)0,(void*)0,&g_246[0],&l_696}};
                    int i, j, k;
                    l_812--;
                    (*l_488) = ((g_381[l_760] = (((((safe_rshift_func_int16_t_s_u((**g_539), ((*l_86) > ((safe_lshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_u(5L, 13)), 5)) , ((safe_mod_func_int64_t_s_s(((void*)0 == l_823), (((l_559 & l_810) >= (((safe_rshift_func_uint8_t_u_u((safe_mod_func_int16_t_s_s(((l_771[7][0][2] |= (l_811[0] ^ ((*g_726) < (**g_725)))) , g_19), (**g_725))), (*g_130))) == 1L) >= g_522[1][1][0])) || 1UL))) > (-1L)))))) || 1L) | l_463) != (**g_725)) , 0x0.Cp+1)) == (*l_86));
                    for (g_466 = 1; (g_466 <= 5); g_466 += 1)
                    { /* block id: 379 */
                        int32_t l_830 = 1L;
                        int32_t l_831 = 8L;
                        int32_t l_832 = 8L;
                        int32_t l_833 = 0x55DB9B1FL;
                        int32_t l_834[9][2] = {{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)},{(-4L),(-4L)}};
                        int i, j;
                        (*l_86) |= l_771[1][0][3];
                        l_835--;
                    }
                    for (l_456 = 0; (l_456 <= 1); l_456 += 1)
                    { /* block id: 385 */
                        if (l_838)
                            break;
                    }
                }
                if (g_449)
                    goto lbl_839;
                (*l_86) |= (-8L);
            }
            else
            { /* block id: 391 */
                const uint64_t l_856 = 0xBEE3B7D46F3871E0LL;
                int32_t l_900 = 0L;
                int32_t l_901 = 0x858487B2L;
                int32_t l_902 = (-2L);
                int32_t l_903 = 0x6378AC2DL;
                int32_t l_904 = 9L;
                float l_906[2];
                int32_t l_907 = 0x008011A5L;
                int32_t l_908 = 0x9F52CD28L;
                uint32_t l_910 = 0x17A00D9AL;
                int64_t **l_936 = &g_415;
                int32_t l_958 = 0x370FDB7CL;
                int i;
                for (i = 0; i < 2; i++)
                    l_906[i] = 0x0.8p-1;
                for (g_153 = 0; (g_153 >= 26); g_153++)
                { /* block id: 394 */
                    int16_t l_855 = (-4L);
                    int32_t l_857[4] = {0xA8D3BEB7L,0xA8D3BEB7L,0xA8D3BEB7L,0xA8D3BEB7L};
                    int64_t ***l_888 = &l_741;
                    int32_t l_891 = 5L;
                    int32_t *l_892 = &l_857[1];
                    int32_t *l_893 = &l_792[0][1];
                    int32_t *l_894 = &l_487[7][0];
                    int32_t *l_895 = &l_487[7][0];
                    int32_t *l_896 = &l_639;
                    int32_t *l_897 = &l_771[0][0][3];
                    int32_t *l_898 = &l_792[0][1];
                    int32_t *l_899[7][9][4] = {{{&l_487[7][0],&l_771[7][0][3],&l_124,(void*)0},{(void*)0,&l_487[7][0],&l_891,&g_809},{&g_28,&g_89,(void*)0,&l_771[6][0][2]},{&g_32,&l_891,&l_771[1][0][1],&l_857[1]},{&l_487[8][2],&l_891,&l_559,&l_771[0][0][3]},{&l_792[0][0],(void*)0,(void*)0,&l_792[1][2]},{&l_771[0][0][3],&l_771[6][0][2],&g_809,&l_487[7][0]},{&l_857[0],&l_857[3],&l_792[1][2],&l_792[0][1]},{&l_124,&l_792[0][0],&g_32,&g_28}},{{&l_857[3],&l_467,&l_891,&l_467},{(void*)0,(void*)0,&l_857[3],&l_857[1]},{(void*)0,&g_28,&l_559,&l_857[3]},{&l_379,&l_487[7][0],(void*)0,&l_891},{&l_379,&g_809,&l_559,&l_487[8][2]},{(void*)0,&l_891,&l_857[3],&l_96},{(void*)0,&g_28,&l_891,(void*)0},{&l_857[3],&l_487[7][0],&g_32,&l_379},{&l_124,&g_32,&l_792[1][2],&l_487[5][0]}},{{&l_857[0],&l_857[3],&g_809,(void*)0},{&l_771[0][0][3],&l_771[1][0][1],(void*)0,(void*)0},{&l_792[0][0],&l_487[5][0],&l_559,&g_32},{&l_487[8][2],&l_771[3][0][0],&l_771[1][0][1],&l_487[7][0]},{&g_32,&l_487[7][0],(void*)0,&l_792[0][1]},{&g_28,(void*)0,&l_891,&l_124},{(void*)0,&l_124,&l_124,(void*)0},{&l_487[7][0],&l_771[0][0][3],&l_487[7][0],&g_28},{(void*)0,&g_32,&g_28,&g_28}},{{&l_559,(void*)0,(void*)0,&g_28},{(void*)0,&g_32,&l_487[7][0],&g_28},{&l_857[3],&l_771[0][0][3],&l_771[3][0][0],(void*)0},{&l_771[7][0][3],&l_124,(void*)0,&l_124},{&l_891,(void*)0,&l_487[5][0],&l_792[0][1]},{&l_771[3][0][0],&l_487[7][0],&l_467,&l_487[7][0]},{&l_857[2],&l_771[3][0][0],&l_857[0],&g_32},{(void*)0,&l_487[5][0],&l_857[1],(void*)0},{&l_792[0][1],&l_771[1][0][1],&l_857[0],(void*)0}},{{(void*)0,&l_857[3],&l_771[6][0][2],&l_487[5][0]},{&g_28,&g_32,&g_28,&l_379},{&l_857[0],&l_487[7][0],&l_379,(void*)0},{&l_771[0][0][3],&g_28,&l_771[0][0][3],&l_96},{&g_32,&l_891,(void*)0,&l_487[8][2]},{(void*)0,&g_809,&l_891,&l_891},{&l_857[1],&l_487[7][0],&l_891,&l_771[0][0][3]},{&g_28,(void*)0,&l_124,&l_487[7][0]},{&l_467,(void*)0,&l_487[7][0],(void*)0}},{{&l_487[7][0],(void*)0,&l_857[0],&l_771[3][0][0]},{&g_32,&l_771[1][0][3],&l_487[7][0],(void*)0},{(void*)0,&l_857[2],&g_32,(void*)0},{&g_28,&g_32,&l_857[0],&l_792[0][1]},{(void*)0,(void*)0,&l_487[7][0],&l_487[8][2]},{&l_124,&l_771[6][0][2],&g_32,&l_379},{&l_559,(void*)0,(void*)0,&g_32},{&l_792[0][0],&g_28,&g_28,&l_487[5][0]},{&l_771[6][0][2],&l_891,(void*)0,(void*)0}},{{&l_891,&l_891,&l_792[0][0],&l_771[1][0][3]},{&l_857[2],&l_559,&l_379,&l_487[7][0]},{&l_792[0][1],&l_891,&g_28,&l_379},{(void*)0,&l_891,(void*)0,&l_487[7][0]},{&l_891,&l_559,&l_771[1][0][1],&l_771[1][0][3]},{&g_809,&l_891,&l_857[1],(void*)0},{&l_857[1],&l_891,(void*)0,&l_487[5][0]},{(void*)0,&g_28,&l_857[1],&g_32},{(void*)0,(void*)0,&g_89,&l_379}}};
                    int i, j, k;
                }
                (*l_86) = (0x685F8C55L & (safe_mod_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u((safe_sub_func_int64_t_s_s((0x09ABE2C92A710576LL >= ((((*l_86) , (*g_415)) && (((*l_86) >= (l_856 > (((void*)0 != &g_311[1][3][1]) != (safe_mul_func_uint16_t_u_u((*l_86), (-7L)))))) & (*g_540))) | 0L)), (*l_86))), 1L)) <= 0x8FL), 0x4CL)));
                for (g_398 = (-18); (g_398 >= 22); g_398++)
                { /* block id: 418 */
                    int32_t *l_925 = &l_908;
                    const int32_t *l_990 = &l_838;
                    const int32_t **l_989 = &l_990;
                    int8_t *l_1001[3];
                    int32_t **l_1042 = &g_68;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1001[i] = &l_909[5];
                }
                l_907 &= 0x4AEDE5C5L;
            }
            l_86 = (*g_525);
        }
    }
    else
    { /* block id: 460 */
        uint32_t *l_1049 = &l_1033;
        int32_t l_1055 = 0L;
        uint64_t l_1072 = 0x709670F0EFE29D6CLL;
        int32_t l_1073[7][7] = {{0x8B57128AL,0x8BE00D5BL,1L,0x8BE00D5BL,0x8B57128AL,0x8B57128AL,0x8BE00D5BL},{(-2L),1L,(-2L),0x325E04A1L,0x325E04A1L,(-2L),1L},{0x8BE00D5BL,0L,1L,1L,0L,0x8BE00D5BL,0L},{(-2L),0x325E04A1L,0x325E04A1L,(-2L),1L,(-2L),0x325E04A1L},{0x8B57128AL,0x8B57128AL,0x8BE00D5BL,1L,0x8BE00D5BL,0x8B57128AL,0x8B57128AL},{(-7L),0x325E04A1L,(-10L),0x325E04A1L,(-7L),(-7L),0x325E04A1L},{(-10L),0L,(-10L),0x8BE00D5BL,0x8BE00D5BL,(-10L),0L}};
        int64_t **l_1074 = &g_415;
        uint32_t l_1200 = 0x6B9DD79BL;
        int16_t **l_1257 = (void*)0;
        int16_t ***l_1256[6] = {&l_1257,&l_1257,&l_1257,&l_1257,&l_1257,&l_1257};
        int16_t ****l_1255 = &l_1256[0];
        uint8_t l_1275 = 0x39L;
        uint16_t **l_1279 = (void*)0;
        int16_t l_1281 = 0x075AL;
        const uint32_t l_1333 = 0xF21CECB6L;
        int32_t l_1338[10][1];
        int i, j;
        for (i = 0; i < 10; i++)
        {
            for (j = 0; j < 1; j++)
                l_1338[i][j] = 0xC7C66E7CL;
        }
        if (((safe_lshift_func_uint8_t_u_s(((safe_sub_func_uint32_t_u_u(((*l_1049) = 8UL), (((+(((safe_div_func_uint8_t_u_u(0x2DL, (safe_mul_func_int16_t_s_s(l_1055, (((**g_539) = (-4L)) ^ ((l_1056[1] == ((safe_sub_func_int64_t_s_s((g_522[3][1][0] |= (g_569 ^= ((safe_sub_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u(0UL, (safe_add_func_int16_t_s_s((((l_1073[1][0] &= (((safe_mod_func_int16_t_s_s((g_692 = ((((void*)0 == &g_466) ^ (safe_sub_func_int8_t_s_s((~((*l_86) && (*l_86))), l_1055))) && (*l_86))), (*l_86))) ^ g_294) , l_1072)) <= 0x58L) || (*l_86)), l_1055)))) <= 0L), l_1055)), 0x54L)) , (*g_415)))), 18446744073709551615UL)) , l_1074)) | 0x55452525L)))))) && (*g_415)) == (*l_86))) && (-1L)) ^ l_1072))) == g_92), g_398)) < l_1055))
        { /* block id: 467 */
            int32_t l_1079[5] = {3L,3L,3L,3L,3L};
            int32_t *l_1084 = &l_792[0][1];
            float l_1087 = 0x6.Fp-1;
            float *l_1088 = &g_381[5];
            int i;
            (*l_1088) = ((safe_div_func_float_f_f(0x2.337E22p+62, ((safe_div_func_float_f_f(((*l_86) = l_1055), l_1079[0])) >= (((safe_div_func_int8_t_s_s(2L, (safe_mul_func_uint8_t_u_u((0xB960L | (((((*l_1084) = l_537[8][3]) , (l_1073[1][0] || (safe_lshift_func_int8_t_s_s((*l_1084), (65526UL >= (*g_726)))))) != 0x2CA7L) >= l_1033)), l_1073[1][0])))) > (*g_415)) , l_1087)))) != l_1043);
            if (g_32)
                goto lbl_1089;
        }
        else
        { /* block id: 472 */
            int64_t l_1095 = 0xFA1F09598808D3D0LL;
            int16_t * const *l_1120 = (void*)0;
            int16_t * const **l_1119 = &l_1120;
            int32_t l_1203 = 0L;
            int64_t **l_1211[5];
            int32_t **l_1213[5] = {&l_86,&l_86,&l_86,&l_86,&l_86};
            int64_t *l_1220 = &g_522[1][1][0];
            int16_t l_1258 = (-2L);
            uint64_t l_1337[10][10] = {{0xB926BF11BAE15ABELL,0xA8D1E8135C0BB6ACLL,0UL,0xB926BF11BAE15ABELL,0UL,0xA8D1E8135C0BB6ACLL,0xB926BF11BAE15ABELL,1UL,1UL,0xB926BF11BAE15ABELL},{1UL,0x48D1A2DA2717AB30LL,0UL,0UL,0x48D1A2DA2717AB30LL,1UL,0xA8D1E8135C0BB6ACLL,0x48D1A2DA2717AB30LL,0xA8D1E8135C0BB6ACLL,1UL},{0x157E49A785440A42LL,0x48D1A2DA2717AB30LL,18446744073709551615UL,0x48D1A2DA2717AB30LL,0x157E49A785440A42LL,18446744073709551615UL,0xB926BF11BAE15ABELL,0xB926BF11BAE15ABELL,18446744073709551615UL,0x157E49A785440A42LL},{0x157E49A785440A42LL,0xA8D1E8135C0BB6ACLL,0xA8D1E8135C0BB6ACLL,0x157E49A785440A42LL,0UL,1UL,0x157E49A785440A42LL,1UL,0UL,0x157E49A785440A42LL},{1UL,0x157E49A785440A42LL,1UL,0UL,0x157E49A785440A42LL,18446744073709551615UL,18446744073709551615UL,0xA8D1E8135C0BB6ACLL,0UL,0xFAA431EF16AA032CLL},{1UL,1UL,0xB926BF11BAE15ABELL,0xA8D1E8135C0BB6ACLL,0UL,0xB926BF11BAE15ABELL,0UL,0xA8D1E8135C0BB6ACLL,0xB926BF11BAE15ABELL,1UL},{0UL,18446744073709551615UL,0xFAA431EF16AA032CLL,0UL,0UL,0UL,0UL,0xFAA431EF16AA032CLL,18446744073709551615UL,0UL},{0xFAA431EF16AA032CLL,1UL,18446744073709551615UL,0UL,1UL,0UL,18446744073709551615UL,1UL,0xFAA431EF16AA032CLL,0xFAA431EF16AA032CLL},{0UL,0xA8D1E8135C0BB6ACLL,0xB926BF11BAE15ABELL,1UL,1UL,0xB926BF11BAE15ABELL,0xA8D1E8135C0BB6ACLL,0UL,0xB926BF11BAE15ABELL,0UL},{1UL,18446744073709551615UL,0UL,1UL,0UL,18446744073709551615UL,1UL,0xFAA431EF16AA032CLL,0xFAA431EF16AA032CLL,1UL}};
            int i, j;
            for (i = 0; i < 5; i++)
                l_1211[i] = &g_415;
            if (((safe_mul_func_uint16_t_u_u((l_1055 = ((0L <= g_398) <= (*g_130))), (!(safe_sub_func_uint16_t_u_u(((*g_644) == (void*)0), (((l_1072 ^ (*l_86)) || ((void*)0 == &p_71)) <= l_1095)))))) <= 0x32L))
            { /* block id: 474 */
                uint16_t ***l_1101 = &l_713;
                uint16_t ****l_1100 = &l_1101;
                int32_t l_1139[1];
                const int8_t *l_1140 = (void*)0;
                int i;
                for (i = 0; i < 1; i++)
                    l_1139[i] = 0x070AEDC6L;
                for (l_1043 = 11; (l_1043 < 18); l_1043 = safe_add_func_uint64_t_u_u(l_1043, 3))
                { /* block id: 477 */
                    uint16_t *****l_1102 = &l_1100;
                    uint16_t * const * const ***l_1106 = &g_1103;
                    int16_t * const ***l_1121 = (void*)0;
                    int16_t * const ***l_1122[10][5][5] = {{{&l_1119,(void*)0,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,(void*)0,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,(void*)0,&l_1119}},{{(void*)0,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,(void*)0,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119}},{{&l_1119,(void*)0,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,(void*)0,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,(void*)0,&l_1119}},{{(void*)0,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,(void*)0,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119}},{{&l_1119,(void*)0,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,(void*)0,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,(void*)0,&l_1119}},{{(void*)0,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,(void*)0,&l_1119,(void*)0,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119}},{{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,(void*)0,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,(void*)0,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,(void*)0,&l_1119,&l_1119,&l_1119}},{{&l_1119,&l_1119,&l_1119,(void*)0,&l_1119},{(void*)0,(void*)0,&l_1119,(void*)0,&l_1119},{&l_1119,(void*)0,&l_1119,(void*)0,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119}},{{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,(void*)0,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,(void*)0,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,(void*)0,&l_1119,&l_1119,&l_1119}},{{&l_1119,&l_1119,&l_1119,(void*)0,&l_1119},{(void*)0,(void*)0,&l_1119,(void*)0,&l_1119},{&l_1119,(void*)0,&l_1119,(void*)0,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119},{&l_1119,&l_1119,&l_1119,&l_1119,&l_1119}}};
                    int32_t l_1127 = (-1L);
                    uint64_t *l_1136 = (void*)0;
                    uint64_t *l_1137 = (void*)0;
                    uint64_t *l_1138 = &g_92;
                    int64_t *l_1144 = &g_243;
                    float ***l_1186 = &g_999;
                    int32_t l_1196[1];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_1196[i] = 0x8DDF3D63L;
                    if ((((safe_rshift_func_uint16_t_u_u(((***g_1104) = ((((l_1095 <= (g_569 , ((*l_1049) = (((*l_1102) = l_1100) != ((*l_1106) = g_1103))))) <= (safe_sub_func_uint8_t_u_u(((safe_div_func_float_f_f(((*l_86) = (((safe_mul_func_float_f_f(((safe_mod_func_uint8_t_u_u((((((safe_add_func_uint32_t_u_u((safe_sub_func_int32_t_s_s((l_1095 >= ((l_1119 = l_1119) == ((((((safe_add_func_int64_t_s_s(((**l_868) &= ((safe_mul_func_uint8_t_u_u(l_1127, ((safe_lshift_func_uint16_t_u_u(((0x4CE5L | (safe_add_func_int16_t_s_s((((safe_lshift_func_int16_t_s_s((safe_add_func_uint64_t_u_u((g_202[7][1] ^= ((*l_1138) = ((0xC6L <= g_522[1][1][0]) && l_1127))), l_1095)), 10)) <= l_1127) & (*g_130)), (*l_86)))) | l_1127), 2)) , l_1095))) == g_232)), g_246[3])) ^ g_807) , (void*)0) == (*l_1074)) <= 0xE57A69705DED4976LL) , (void*)0))), (*l_86))), 0UL)) ^ (*l_86)) >= (***g_1104)) <= (*g_130)) & l_1095), g_246[0])) , 0xD.760145p+98), (*l_86))) > (*l_86)) == 0x1.Bp+1)), l_1127)) , l_1139[0]), 0x9AL))) | l_1073[1][0]) , 0xC2F2L)), 15)) , &g_697[3][0]) != l_1140))
                    { /* block id: 487 */
                        int64_t *l_1143 = (void*)0;
                        int32_t l_1159[10];
                        int i;
                        for (i = 0; i < 10; i++)
                            l_1159[i] = 1L;
                        (*g_1160) &= ((safe_mul_func_int8_t_s_s(l_1095, (((*l_868) = &g_243) != (l_1144 = l_1143)))) < ((safe_mod_func_uint16_t_u_u((((void*)0 != l_1147[0][0][1]) , (safe_rshift_func_int8_t_s_s((((l_1072 < ((***l_719)++)) || (safe_mul_func_int16_t_s_s(l_1127, (safe_add_func_uint16_t_u_u((****g_1103), (!(safe_lshift_func_int16_t_s_s((*l_86), l_1139[0])))))))) , (-4L)), 5))), l_1159[1])) == l_1095));
                    }
                    else
                    { /* block id: 492 */
                        int32_t **l_1161 = (void*)0;
                        int32_t **l_1162 = &l_86;
                        int32_t *l_1179 = &l_379;
                        uint16_t l_1195 = 0x195AL;
                        (*l_1162) = (void*)0;
                        (*l_1179) |= (safe_div_func_uint64_t_u_u(l_1139[0], (safe_add_func_int8_t_s_s((((((safe_sub_func_float_f_f((0x0.1p-1 >= ((((safe_add_func_float_f_f(l_1095, l_1127)) <= (safe_div_func_float_f_f(((safe_sub_func_float_f_f(l_1139[0], ((((-0x1.Dp+1) <= (0x0.Bp+1 > 0xD.F6AB78p-85)) <= (safe_add_func_float_f_f((safe_sub_func_float_f_f(0xF.262BDEp-28, 0xD.F24258p-33)), 0x1.Ep+1))) == l_1139[0]))) < l_1139[0]), l_1095))) <= l_1073[2][0]) != l_1072)), l_1095)) <= 0x6.4ECBA9p+56) != l_1127) >= 0x1.Ap+1) , l_1095), l_1095))));
                        (*l_1179) = ((~((l_1196[0] &= ((safe_div_func_uint8_t_u_u((((*g_540) | ((+(*g_415)) && l_1127)) != (safe_rshift_func_int8_t_s_u(((l_1186 != &g_999) >= (((((l_1095 || ((safe_add_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((safe_div_func_int32_t_s_s(((*l_1106) != (((void*)0 != (**g_1103)) , &g_1104)), 4UL)) && l_1139[0]), 0xED85806AL)), l_1095)) > l_1095)) , 1L) , l_1195) , (void*)0) != (void*)0)), (*g_130)))), l_1127)) && (*p_72))) , 0xD4L)) || l_1073[2][0]);
                    }
                    return (*p_72);
                }
                l_1055 = 0x2.880890p-10;
            }
            else
            { /* block id: 501 */
                int32_t *l_1218 = &l_792[0][0];
                uint16_t ***l_1221 = (void*)0;
                uint16_t ***l_1222 = &l_713;
                int32_t l_1231 = 0xBDAA1CCCL;
                int64_t ***l_1274 = &l_1074;
                for (l_1055 = 11; (l_1055 > (-4)); l_1055--)
                { /* block id: 504 */
                    int32_t *l_1199[8] = {&l_1055,&l_1055,&l_1055,&l_1055,&l_1055,&l_1055,&l_1055,&l_1055};
                    int i;
                    l_1200--;
                    for (l_1043 = 0; (l_1043 >= 0); l_1043 -= 1)
                    { /* block id: 508 */
                        uint32_t l_1204[3][3][8] = {{{0x7EFB03B0L,0x4163555AL,0x7197E8ABL,0x65CDFDBFL,0x4163555AL,0x65CDFDBFL,0x7197E8ABL,0x4163555AL},{0x4FDBF865L,0x7197E8ABL,0x7EFB03B0L,0x4FDBF865L,0x65CDFDBFL,0x65CDFDBFL,0x4FDBF865L,0x7EFB03B0L},{0x4163555AL,0x4163555AL,0xB96F14F9L,0x8254A445L,0x4FDBF865L,0xB96F14F9L,0x4FDBF865L,0x8254A445L}},{{0x7EFB03B0L,0x8254A445L,0x7EFB03B0L,0x65CDFDBFL,0x8254A445L,0x7197E8ABL,0x7197E8ABL,0x8254A445L},{0x8254A445L,0x7197E8ABL,0x7197E8ABL,0x8254A445L,0x65CDFDBFL,0x7EFB03B0L,0x8254A445L,0x7EFB03B0L},{0x8254A445L,0x4FDBF865L,0xB96F14F9L,0x4FDBF865L,0x8254A445L,0xB96F14F9L,0x4163555AL,0x4163555AL}},{{0x7EFB03B0L,0x4FDBF865L,0x65CDFDBFL,0x65CDFDBFL,0x4FDBF865L,0x7EFB03B0L,0x7197E8ABL,0x4FDBF865L},{0x4163555AL,0x7197E8ABL,0x65CDFDBFL,0x4163555AL,0x65CDFDBFL,0x7197E8ABL,0x4163555AL,0x7EFB03B0L},{0x4FDBF865L,0x8254A445L,0xB96F14F9L,0x4163555AL,0x7EFB03B0L,0x4163555AL,0x7197E8ABL,0x65CDFDBFL}}};
                        int32_t **l_1207 = &g_68;
                        int i, j, k;
                        ++l_1204[2][1][0];
                        (*l_1207) = p_71;
                        (*l_1207) = (*g_525);
                    }
                    for (l_1203 = 5; (l_1203 >= 0); l_1203 -= 1)
                    { /* block id: 515 */
                        int32_t **l_1208 = &l_1199[3];
                        (*l_1208) = p_71;
                    }
                    for (g_692 = 0; (g_692 > (-19)); g_692--)
                    { /* block id: 520 */
                        (*l_86) = (*p_72);
                    }
                }
                if ((l_1211[2] == &g_415))
                { /* block id: 524 */
lbl_1286:
                    (*g_542) = &l_124;
                }
                else
                { /* block id: 526 */
                    int32_t ***l_1215 = (void*)0;
                    int32_t ***l_1216[8];
                    int i;
                    for (i = 0; i < 8; i++)
                        l_1216[i] = (void*)0;
                    l_1217 = l_1213[2];
                    p_72 = (l_1218 = (*g_525));
                }
                (*l_86) = ((+(*l_86)) || ((l_1220 == &g_522[1][1][0]) < (((*g_1104) != ((*l_1222) = &l_714[0])) < (safe_lshift_func_uint8_t_u_u(((((safe_div_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(l_1073[2][0], (safe_rshift_func_int16_t_s_s((0xC14EL || (l_1231 & g_673)), 11)))), l_1073[1][0])) < g_706) > g_673) , 0UL), (*g_130))))));
                if (((*g_415) && (+(safe_rshift_func_int16_t_s_u((((safe_mod_func_uint8_t_u_u(((safe_sub_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u(((*l_86) | (safe_rshift_func_int16_t_s_s((((((safe_rshift_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_s((safe_div_func_uint64_t_u_u(0x361D5097CB0A677ALL, g_232)), 1)) || ((((safe_add_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s((((**g_539) >= 0x5B3FL) , (g_784 , ((safe_mod_func_int16_t_s_s(((void*)0 == (*l_1119)), l_1200)) <= l_1072))), g_294)) , (*g_540)), g_246[2])) , l_1255) == &l_1119) < 0x0FA702F5L)), 3)) < g_166) >= g_403) && 0x255DBCB8F37EA269LL) != g_809), 1))), 4)) && (*l_86)), 0x7806AD2EL)) ^ l_1258), 0x8BL)) < l_1200) , l_1055), l_1072)))))
                { /* block id: 533 */
                    int8_t l_1270[3][9] = {{(-1L),(-4L),0x8BL,0L,0L,0x8BL,(-4L),(-1L),(-4L)},{0xCDL,0xDBL,0x8BL,0x8BL,0xDBL,0xCDL,0L,0xCDL,0xDBL},{0xCDL,(-4L),(-4L),0xCDL,(-1L),0xDBL,(-1L),0xCDL,(-4L)}};
                    int i, j;
                    l_1218 = &l_1055;
                    (*l_86) = (((safe_div_func_uint64_t_u_u(0x81841739AE167809LL, (safe_div_func_uint8_t_u_u(((((g_1263[0] != (((safe_mod_func_int64_t_s_s((g_311[1][7][2] , ((((*l_1218) , (safe_mod_func_int16_t_s_s(0x57ADL, (*l_1218)))) || (safe_div_func_int64_t_s_s(l_1270[0][6], l_1271[3][0]))) != ((safe_rshift_func_int8_t_s_s((((**g_539) >= 0UL) || l_1270[0][6]), 6)) , l_1073[1][0]))), (-1L))) & (*l_1218)) , l_1274)) != l_1275) | g_522[1][1][0]) | 0x1490BE51L), (*l_86))))) , (*l_86)) >= (*l_1218));
                }
                else
                { /* block id: 536 */
                    float l_1280 = 0x1.9p+1;
                    int32_t ** const *l_1287 = (void*)0;
                    int32_t ** const **l_1292 = &l_1287;
                    for (l_373 = 0; (l_373 <= 2); l_373 += 1)
                    { /* block id: 539 */
                        uint8_t l_1278 = 0x2DL;
                        (*l_86) = (((safe_mul_func_uint64_t_u_u(0x7F2BFA93FB5FF958LL, (((*l_1222) = (l_1278 , &l_714[4])) != l_1279))) != (&l_1120 != &g_539)) < (l_1281 < (safe_mod_func_uint8_t_u_u((safe_div_func_int64_t_s_s(3L, (((*g_130) == g_673) && l_1200))), g_153))));
                        if (l_1072)
                            goto lbl_1286;
                    }
                    (*l_1292) = l_1287;
                }
            }
            for (l_1072 = 0; (l_1072 <= 7); l_1072 += 1)
            { /* block id: 549 */
                float l_1297 = 0x5.560A00p-34;
                const int32_t l_1327 = 0L;
                int8_t *l_1328 = &l_909[1];
                int8_t *l_1329 = &g_311[2][1][4];
                uint64_t l_1330[10][6][4] = {{{0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL,1UL},{0UL,1UL,0UL,1UL},{1UL,0UL,0x00FAEDF517B5DC40LL,1UL},{0x00FAEDF517B5DC40LL,1UL,1UL,1UL},{0x4AC34021FCE4CD37LL,0x4AC34021FCE4CD37LL,0UL,0UL},{0x4AC34021FCE4CD37LL,0UL,1UL,0x4AC34021FCE4CD37LL}},{{0x00FAEDF517B5DC40LL,0UL,0x00FAEDF517B5DC40LL,1UL},{1UL,0UL,0UL,0x4AC34021FCE4CD37LL},{0UL,0UL,0UL,0UL},{0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL,1UL},{0UL,1UL,0UL,1UL},{1UL,0UL,0x00FAEDF517B5DC40LL,1UL}},{{0x00FAEDF517B5DC40LL,1UL,1UL,1UL},{0x4AC34021FCE4CD37LL,0x4AC34021FCE4CD37LL,0UL,0UL},{0x4AC34021FCE4CD37LL,0UL,1UL,0x4AC34021FCE4CD37LL},{0x00FAEDF517B5DC40LL,0UL,0x00FAEDF517B5DC40LL,1UL},{1UL,0UL,0UL,0x4AC34021FCE4CD37LL},{0UL,0UL,0UL,0UL}},{{0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL,1UL},{0UL,1UL,0UL,1UL},{1UL,0UL,0x00FAEDF517B5DC40LL,1UL},{0x00FAEDF517B5DC40LL,1UL,1UL,1UL},{0x4AC34021FCE4CD37LL,0x4AC34021FCE4CD37LL,0UL,0UL},{0x4AC34021FCE4CD37LL,0UL,1UL,0x4AC34021FCE4CD37LL}},{{0x00FAEDF517B5DC40LL,0UL,0x00FAEDF517B5DC40LL,1UL},{1UL,0UL,0UL,0x4AC34021FCE4CD37LL},{0UL,0UL,0UL,0UL},{0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL,1UL},{0UL,1UL,0UL,1UL},{1UL,0UL,0x00FAEDF517B5DC40LL,1UL}},{{0x00FAEDF517B5DC40LL,1UL,1UL,1UL},{0x4AC34021FCE4CD37LL,0x4AC34021FCE4CD37LL,0UL,0UL},{0x4AC34021FCE4CD37LL,0UL,1UL,0x4AC34021FCE4CD37LL},{0x00FAEDF517B5DC40LL,0UL,0x00FAEDF517B5DC40LL,1UL},{1UL,0UL,0UL,0x4AC34021FCE4CD37LL},{0UL,0UL,0UL,0UL}},{{0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL,1UL},{0UL,1UL,0UL,1UL},{1UL,0UL,0x00FAEDF517B5DC40LL,1UL},{0x00FAEDF517B5DC40LL,1UL,1UL,1UL},{0x00FAEDF517B5DC40LL,0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL},{0x00FAEDF517B5DC40LL,0UL,18446744073709551606UL,0x00FAEDF517B5DC40LL}},{{0UL,0UL,0UL,18446744073709551606UL},{1UL,0UL,0x4AC34021FCE4CD37LL,0x00FAEDF517B5DC40LL},{0UL,0UL,0UL,0UL},{0UL,0x00FAEDF517B5DC40LL,0UL,18446744073709551606UL},{0UL,1UL,0x4AC34021FCE4CD37LL,1UL},{1UL,0UL,0UL,1UL}},{{0UL,1UL,18446744073709551606UL,18446744073709551606UL},{0x00FAEDF517B5DC40LL,0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL},{0x00FAEDF517B5DC40LL,0UL,18446744073709551606UL,0x00FAEDF517B5DC40LL},{0UL,0UL,0UL,18446744073709551606UL},{1UL,0UL,0x4AC34021FCE4CD37LL,0x00FAEDF517B5DC40LL},{0UL,0UL,0UL,0UL}},{{0UL,0x00FAEDF517B5DC40LL,0UL,18446744073709551606UL},{0UL,1UL,0x4AC34021FCE4CD37LL,1UL},{1UL,0UL,0UL,1UL},{0UL,1UL,18446744073709551606UL,18446744073709551606UL},{0x00FAEDF517B5DC40LL,0x00FAEDF517B5DC40LL,0x4AC34021FCE4CD37LL,0UL},{0x00FAEDF517B5DC40LL,0UL,18446744073709551606UL,0x00FAEDF517B5DC40LL}}};
                uint32_t *l_1331 = &g_294;
                float *l_1335 = (void*)0;
                float *l_1336 = &l_1297;
                int i, j, k;
                (*l_86) ^= 0x19724D92L;
                l_1338[6][0] = (((safe_add_func_float_f_f(((safe_sub_func_float_f_f((-0x1.Fp-1), ((((*l_1336) = ((((0x9D0569B7L <= l_1072) , ((((safe_div_func_uint8_t_u_u((((safe_unary_minus_func_int32_t_s((safe_add_func_int32_t_s_s((safe_div_func_uint8_t_u_u((*l_86), (safe_div_func_int64_t_s_s((safe_rshift_func_int8_t_s_s(((safe_sub_func_uint32_t_u_u((((*l_1331) &= (safe_mod_func_int8_t_s_s((((safe_mul_func_int8_t_s_s(((*l_1329) ^= (safe_add_func_uint32_t_u_u((((*l_1328) = ((safe_sub_func_int64_t_s_s((l_1055 = ((**l_868) = ((*l_1220) = (safe_div_func_int32_t_s_s((l_1073[3][3] = (safe_mod_func_uint8_t_u_u(l_1200, ((safe_sub_func_uint32_t_u_u(((safe_mul_func_uint16_t_u_u((((void*)0 != (*l_1074)) , (l_1072 & g_28)), 5UL)) > 0xD7F877A7L), l_1327)) , g_92)))), l_1327))))), 0x56A0AB3C8E5B901BLL)) == 0x7FF014D1L)) < (*g_130)), 4294967295UL))), l_1330[9][3][3])) , (*l_86)) , g_402), (*l_86)))) , (*l_86)), (*l_86))) <= 0x7CA8L), 1)), g_449)))), 1L)))) <= l_1333) >= l_1330[1][2][0]), l_1330[9][3][3])) , l_1333) < g_1334) == l_1327)) >= l_1327) <= l_1275)) != 0x0.Fp-1) == l_1275))) >= l_1337[8][4]), (*l_86))) == 0x1.00F4E2p+48) == (*l_86));
                for (l_1095 = 7; (l_1095 >= 2); l_1095 -= 1)
                { /* block id: 562 */
                    int i, j;
                    return l_1271[l_1072][l_1072];
                }
            }
            l_1339++;
        }
    }
    --l_1353;
    return (*l_1344);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_19, "g_19", print_hash_value);
    transparent_crc(g_28, "g_28", print_hash_value);
    transparent_crc(g_32, "g_32", print_hash_value);
    transparent_crc(g_48, "g_48", print_hash_value);
    transparent_crc(g_89, "g_89", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc_bytes(&g_111[i], sizeof(g_111[i]), "g_111[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_153, "g_153", print_hash_value);
    transparent_crc(g_159, "g_159", print_hash_value);
    transparent_crc(g_166, "g_166", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_202[i][j], "g_202[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_232, "g_232", print_hash_value);
    transparent_crc(g_243, "g_243", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_246[i], "g_246[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_294, "g_294", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_311[i][j][k], "g_311[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_334, "g_334", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_374[i], "g_374[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_377, "g_377", print_hash_value);
    transparent_crc(g_378, "g_378", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_381[i], sizeof(g_381[i]), "g_381[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_382, "g_382", print_hash_value);
    transparent_crc(g_398, "g_398", print_hash_value);
    transparent_crc(g_402, "g_402", print_hash_value);
    transparent_crc(g_403, "g_403", print_hash_value);
    transparent_crc(g_419, "g_419", print_hash_value);
    transparent_crc(g_449, "g_449", print_hash_value);
    transparent_crc(g_455, "g_455", print_hash_value);
    transparent_crc(g_458, "g_458", print_hash_value);
    transparent_crc(g_466, "g_466", print_hash_value);
    transparent_crc(g_500, "g_500", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_522[i][j][k], "g_522[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_569, "g_569", print_hash_value);
    transparent_crc(g_570, "g_570", print_hash_value);
    transparent_crc(g_599, "g_599", print_hash_value);
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc(g_692, "g_692", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_697[i][j], "g_697[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_698, "g_698", print_hash_value);
    transparent_crc(g_701, "g_701", print_hash_value);
    transparent_crc(g_706, "g_706", print_hash_value);
    transparent_crc(g_784, "g_784", print_hash_value);
    transparent_crc(g_807, "g_807", print_hash_value);
    transparent_crc(g_809, "g_809", print_hash_value);
    transparent_crc(g_1334, "g_1334", print_hash_value);
    transparent_crc(g_1350, "g_1350", print_hash_value);
    transparent_crc(g_1362, "g_1362", print_hash_value);
    transparent_crc(g_1432, "g_1432", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1497[i][j][k], "g_1497[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1587, "g_1587", print_hash_value);
    transparent_crc(g_1597, "g_1597", print_hash_value);
    transparent_crc(g_1607, "g_1607", print_hash_value);
    transparent_crc(g_1687, "g_1687", print_hash_value);
    transparent_crc(g_1790, "g_1790", print_hash_value);
    transparent_crc(g_1826, "g_1826", print_hash_value);
    transparent_crc(g_1835, "g_1835", print_hash_value);
    transparent_crc(g_1951, "g_1951", print_hash_value);
    transparent_crc(g_2191, "g_2191", print_hash_value);
    transparent_crc(g_2382, "g_2382", print_hash_value);
    transparent_crc(g_2409, "g_2409", print_hash_value);
    transparent_crc(g_2439, "g_2439", print_hash_value);
    transparent_crc(g_2500, "g_2500", print_hash_value);
    transparent_crc(g_2519, "g_2519", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2581[i], "g_2581[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2606, "g_2606", print_hash_value);
    transparent_crc(g_2829, "g_2829", print_hash_value);
    transparent_crc(g_2853, "g_2853", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_2920[i][j][k], sizeof(g_2920[i][j][k]), "g_2920[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2926[i], "g_2926[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3156, "g_3156", print_hash_value);
    transparent_crc(g_3280, "g_3280", print_hash_value);
    transparent_crc(g_3323, "g_3323", print_hash_value);
    transparent_crc(g_3398, "g_3398", print_hash_value);
    transparent_crc(g_3433, "g_3433", print_hash_value);
    transparent_crc(g_3462, "g_3462", print_hash_value);
    transparent_crc(g_3502, "g_3502", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 934
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 54
breakdown:
   depth: 1, occurrence: 519
   depth: 2, occurrence: 134
   depth: 3, occurrence: 6
   depth: 4, occurrence: 4
   depth: 5, occurrence: 5
   depth: 6, occurrence: 3
   depth: 7, occurrence: 3
   depth: 8, occurrence: 2
   depth: 9, occurrence: 2
   depth: 10, occurrence: 3
   depth: 11, occurrence: 2
   depth: 12, occurrence: 2
   depth: 13, occurrence: 2
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 4
   depth: 17, occurrence: 6
   depth: 18, occurrence: 5
   depth: 19, occurrence: 10
   depth: 20, occurrence: 8
   depth: 21, occurrence: 4
   depth: 22, occurrence: 10
   depth: 23, occurrence: 4
   depth: 24, occurrence: 3
   depth: 25, occurrence: 5
   depth: 26, occurrence: 4
   depth: 27, occurrence: 2
   depth: 28, occurrence: 4
   depth: 29, occurrence: 4
   depth: 30, occurrence: 2
   depth: 31, occurrence: 2
   depth: 32, occurrence: 2
   depth: 33, occurrence: 2
   depth: 34, occurrence: 1
   depth: 35, occurrence: 2
   depth: 36, occurrence: 3
   depth: 37, occurrence: 2
   depth: 38, occurrence: 2
   depth: 39, occurrence: 1
   depth: 40, occurrence: 1
   depth: 45, occurrence: 1
   depth: 46, occurrence: 1
   depth: 47, occurrence: 1
   depth: 50, occurrence: 1
   depth: 54, occurrence: 1

XXX total number of pointers: 765

XXX times a variable address is taken: 1856
XXX times a pointer is dereferenced on RHS: 670
breakdown:
   depth: 1, occurrence: 558
   depth: 2, occurrence: 75
   depth: 3, occurrence: 21
   depth: 4, occurrence: 16
XXX times a pointer is dereferenced on LHS: 451
breakdown:
   depth: 1, occurrence: 380
   depth: 2, occurrence: 50
   depth: 3, occurrence: 13
   depth: 4, occurrence: 8
XXX times a pointer is compared with null: 69
XXX times a pointer is compared with address of another variable: 17
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 14503

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2890
   level: 2, occurrence: 586
   level: 3, occurrence: 325
   level: 4, occurrence: 171
   level: 5, occurrence: 10
XXX number of pointers point to pointers: 395
XXX number of pointers point to scalars: 370
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.8
XXX average alias set size: 1.48

XXX times a non-volatile is read: 3141
XXX times a non-volatile is write: 1413
XXX times a volatile is read: 185
XXX    times read thru a pointer: 103
XXX times a volatile is write: 61
XXX    times written thru a pointer: 27
XXX times a volatile is available for access: 3.05e+03
XXX percentage of non-volatile access: 94.9

XXX forward jumps: 0
XXX backward jumps: 12

XXX stmts: 520
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 38
   depth: 2, occurrence: 59
   depth: 3, occurrence: 111
   depth: 4, occurrence: 151
   depth: 5, occurrence: 129

XXX percentage a fresh-made variable is used: 15.5
XXX percentage an existing variable is used: 84.5
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2458761745
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   float  f0;
   uint16_t  f1;
   unsigned f2 : 14;
   const volatile uint64_t  f3;
};

#pragma pack(push)
#pragma pack(1)
struct S1 {
   uint32_t  f0;
   const volatile signed f1 : 26;
   unsigned f2 : 17;
   int8_t  f3;
   volatile signed f4 : 31;
   volatile signed f5 : 9;
   volatile signed f6 : 18;
};
#pragma pack(pop)

union U2 {
   volatile float  f0;
   uint32_t  f1;
};

union U3 {
   int8_t  f0;
   const int16_t  f1;
   int16_t  f2;
   volatile int32_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_5[2] = {0x515FDC8DL,0x515FDC8DL};
static uint16_t g_36 = 0x82D3L;
static struct S0 g_39 = {0xF.312D9Ep-2,0xBD3CL,76,0UL};/* VOLATILE GLOBAL g_39 */
static int32_t *g_76 = &g_5[0];
static int32_t **g_75 = &g_76;
static int64_t g_79 = 0x4299DA232D690D09LL;
static uint64_t g_83[4][6][9] = {{{0x72F705CCF7F12930LL,0xE68E90F11B2BD449LL,18446744073709551612UL,4UL,18446744073709551615UL,2UL,4UL,0x6804FA82FB5A3837LL,0xEBD4AFBE21FB0408LL},{4UL,18446744073709551615UL,0xAC77CF8A12A9E8DCLL,0xFED21ADA0F0CF813LL,1UL,9UL,0x14382FC2C680C6B3LL,4UL,0xF1DE66CF567C8C73LL},{4UL,0xE68E90F11B2BD449LL,0x7861EF600149B903LL,0xE7EEA9CB02FEDD84LL,18446744073709551615UL,0x68E686C5625565FBLL,0xE7EEA9CB02FEDD84LL,0x6804FA82FB5A3837LL,18446744073709551615UL},{0x14382FC2C680C6B3LL,18446744073709551615UL,0xCA5F8AAA1069F5C4LL,18446744073709551615UL,1UL,0xCF80EE9E06837CBFLL,0x25711A96804C796BLL,4UL,0xF9EF08FD1E8302DELL},{0xE7EEA9CB02FEDD84LL,0xE68E90F11B2BD449LL,0x6804FA82FB5A3837LL,0x72F705CCF7F12930LL,18446744073709551615UL,18446744073709551615UL,0x72F705CCF7F12930LL,0x6804FA82FB5A3837LL,0x06E3EF0F463998E5LL},{1UL,0UL,18446744073709551615UL,0x56FD9218137C7680LL,0x741FA38B77653E0BLL,0UL,0x87665D8ACBC52F4FLL,0xE2B492AE4C48B7B4LL,3UL}},{{2UL,0x06E3EF0F463998E5LL,1UL,18446744073709551615UL,18446744073709551606UL,0xBA4DF932BF644599LL,18446744073709551615UL,1UL,0xA3CE1FE0044E9B00LL},{0x87665D8ACBC52F4FLL,0UL,18446744073709551606UL,0xCF80EE9E06837CBFLL,0x741FA38B77653E0BLL,0x005763FF8919A246LL,0x0DB40B985C477C23LL,0xE2B492AE4C48B7B4LL,18446744073709551613UL},{18446744073709551615UL,0x06E3EF0F463998E5LL,0xDB1C6A30DE6A78DCLL,0x68E686C5625565FBLL,18446744073709551606UL,8UL,0x68E686C5625565FBLL,1UL,0UL},{0x0DB40B985C477C23LL,0UL,0UL,9UL,0x741FA38B77653E0BLL,18446744073709551615UL,1UL,0xE2B492AE4C48B7B4LL,0xF0FED554AE0E0E78LL},{0x68E686C5625565FBLL,0x06E3EF0F463998E5LL,1UL,2UL,18446744073709551606UL,18446744073709551606UL,2UL,1UL,0x06E3EF0F463998E5LL},{1UL,0UL,18446744073709551615UL,0x56FD9218137C7680LL,0x741FA38B77653E0BLL,0UL,0x87665D8ACBC52F4FLL,0xE2B492AE4C48B7B4LL,3UL}},{{2UL,0x06E3EF0F463998E5LL,1UL,18446744073709551615UL,18446744073709551606UL,0xBA4DF932BF644599LL,18446744073709551615UL,1UL,0xA3CE1FE0044E9B00LL},{0x87665D8ACBC52F4FLL,0UL,18446744073709551606UL,0xCF80EE9E06837CBFLL,0x741FA38B77653E0BLL,0x005763FF8919A246LL,0x0DB40B985C477C23LL,0xE2B492AE4C48B7B4LL,18446744073709551613UL},{18446744073709551615UL,0x06E3EF0F463998E5LL,0xDB1C6A30DE6A78DCLL,0x68E686C5625565FBLL,18446744073709551606UL,8UL,0x68E686C5625565FBLL,1UL,0UL},{0x0DB40B985C477C23LL,0UL,0UL,9UL,0x741FA38B77653E0BLL,18446744073709551615UL,1UL,0xE2B492AE4C48B7B4LL,0xF0FED554AE0E0E78LL},{0x68E686C5625565FBLL,0x06E3EF0F463998E5LL,1UL,2UL,18446744073709551606UL,18446744073709551606UL,2UL,1UL,0x06E3EF0F463998E5LL},{1UL,0UL,18446744073709551615UL,0x56FD9218137C7680LL,0x741FA38B77653E0BLL,0UL,0x87665D8ACBC52F4FLL,0xE2B492AE4C48B7B4LL,3UL}},{{2UL,0x06E3EF0F463998E5LL,1UL,18446744073709551615UL,18446744073709551606UL,0xBA4DF932BF644599LL,18446744073709551615UL,1UL,0xA3CE1FE0044E9B00LL},{0x87665D8ACBC52F4FLL,0UL,18446744073709551606UL,0xCF80EE9E06837CBFLL,0x741FA38B77653E0BLL,0x005763FF8919A246LL,0x0DB40B985C477C23LL,0xE2B492AE4C48B7B4LL,18446744073709551613UL},{18446744073709551615UL,0x06E3EF0F463998E5LL,0xDB1C6A30DE6A78DCLL,0x68E686C5625565FBLL,18446744073709551606UL,8UL,0x68E686C5625565FBLL,1UL,0UL},{0x0DB40B985C477C23LL,0UL,0UL,9UL,0x741FA38B77653E0BLL,18446744073709551615UL,1UL,0xE2B492AE4C48B7B4LL,0xF0FED554AE0E0E78LL},{0x68E686C5625565FBLL,0x06E3EF0F463998E5LL,1UL,2UL,18446744073709551606UL,18446744073709551606UL,2UL,1UL,0x06E3EF0F463998E5LL},{1UL,0UL,18446744073709551615UL,0x56FD9218137C7680LL,0x741FA38B77653E0BLL,0UL,0x87665D8ACBC52F4FLL,0xE2B492AE4C48B7B4LL,3UL}}};
static int32_t g_87[9] = {0xB799871AL,0x8AEBE663L,0x8AEBE663L,0xB799871AL,0x8AEBE663L,0x8AEBE663L,0xB799871AL,0x8AEBE663L,0x8AEBE663L};
static int32_t g_104 = 1L;
static volatile union U2 g_113 = {0x0.6p+1};/* VOLATILE GLOBAL g_113 */
static int32_t ***g_135 = &g_75;
static int32_t **** volatile g_134[7][10] = {{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0},{&g_135,&g_135,&g_135,&g_135,(void*)0,(void*)0,&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,&g_135,(void*)0,&g_135,&g_135}};
static int64_t g_159 = 0x2CFDF216B6F3BF3CLL;
static volatile struct S1 g_181 = {0xBEA80B9EL,-7787,158,-1L,-24782,-12,317};/* VOLATILE GLOBAL g_181 */
static float g_208 = 0x2.Cp-1;
static volatile uint32_t g_213 = 0x07BED13AL;/* VOLATILE GLOBAL g_213 */
static volatile union U3 g_227 = {-7L};/* VOLATILE GLOBAL g_227 */
static const volatile union U3 g_228[10] = {{0x1EL},{0x1EL},{0x1EL},{0x1EL},{0x1EL},{0x1EL},{0x1EL},{0x1EL},{0x1EL},{0x1EL}};
static uint8_t g_230 = 6UL;
static int16_t g_271 = 0x72D7L;
static int8_t g_278 = (-1L);
static uint32_t g_282 = 0xE3A0155CL;
static int32_t *g_362 = &g_5[0];
static uint64_t *g_371 = &g_83[1][3][0];
static uint64_t * volatile * volatile g_370 = &g_371;/* VOLATILE GLOBAL g_370 */
static int32_t **g_378 = &g_362;
static uint32_t g_379 = 1UL;
static struct S1 g_415 = {0xF35AB57CL,1880,192,0x64L,36390,13,402};/* VOLATILE GLOBAL g_415 */
static int8_t g_452 = 0x2CL;
static const int8_t g_466 = 9L;
static uint32_t g_480 = 0UL;
static union U2 g_485 = {0x1.Bp+1};/* VOLATILE GLOBAL g_485 */
static union U2 * volatile g_486[8] = {&g_485,&g_485,&g_485,&g_485,&g_485,&g_485,&g_485,&g_485};
static struct S0 g_556 = {-0x10.Ap+1,0x44FCL,86,0xED4A835E1E76CAD3LL};/* VOLATILE GLOBAL g_556 */
static struct S1 g_560[4] = {{1UL,7412,17,-9L,-2003,14,188},{1UL,7412,17,-9L,-2003,14,188},{1UL,7412,17,-9L,-2003,14,188},{1UL,7412,17,-9L,-2003,14,188}};
static float g_569[9] = {0x5.4E1379p-14,0x5.4E1379p-14,0x5.4E1379p-14,0x5.4E1379p-14,0x5.4E1379p-14,0x5.4E1379p-14,0x5.4E1379p-14,0x5.4E1379p-14,0x5.4E1379p-14};
static struct S1 g_580[3] = {{0UL,-6410,236,-9L,-30988,11,99},{0UL,-6410,236,-9L,-30988,11,99},{0UL,-6410,236,-9L,-30988,11,99}};
static union U3 g_607 = {0xA6L};/* VOLATILE GLOBAL g_607 */
static volatile union U3 *g_666[3] = {&g_227,&g_227,&g_227};
static volatile union U3 ** const  volatile g_665[8][9] = {{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]},{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]},{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]},{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]},{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]},{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]},{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]},{&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0],&g_666[0]}};
static volatile union U3 g_668 = {0x4CL};/* VOLATILE GLOBAL g_668 */
static volatile uint32_t g_673 = 6UL;/* VOLATILE GLOBAL g_673 */
static int64_t g_679 = 0x6B8558A782F1EA2ALL;
static volatile union U3 g_680 = {0x87L};/* VOLATILE GLOBAL g_680 */
static struct S0 *g_682 = &g_556;
static struct S0 ** volatile g_681 = &g_682;/* VOLATILE GLOBAL g_681 */
static volatile uint8_t g_718[7][1] = {{7UL},{0x44L},{7UL},{0x44L},{7UL},{0x44L},{7UL}};
static volatile uint8_t * volatile g_717 = &g_718[3][0];/* VOLATILE GLOBAL g_717 */
static volatile uint8_t * volatile *g_716 = &g_717;
static volatile uint32_t **g_725 = (void*)0;
static volatile union U3 g_745 = {0xA6L};/* VOLATILE GLOBAL g_745 */
static volatile int8_t *g_756 = &g_227.f0;
static volatile int8_t **g_755 = &g_756;
static union U3 g_891 = {1L};/* VOLATILE GLOBAL g_891 */
static const int32_t *g_899 = &g_5[0];
static const int32_t ** volatile g_898[4][8][8] = {{{&g_899,&g_899,&g_899,&g_899,(void*)0,&g_899,&g_899,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0,&g_899,&g_899},{(void*)0,&g_899,&g_899,&g_899,&g_899,(void*)0,(void*)0,&g_899},{(void*)0,&g_899,&g_899,(void*)0,&g_899,&g_899,&g_899,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0},{(void*)0,&g_899,(void*)0,&g_899,(void*)0,(void*)0,&g_899,&g_899},{(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0,&g_899}},{{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899},{&g_899,(void*)0,(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899},{(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0},{(void*)0,&g_899,(void*)0,&g_899,(void*)0,(void*)0,&g_899,&g_899},{(void*)0,(void*)0,&g_899,&g_899,&g_899,(void*)0,(void*)0,&g_899},{(void*)0,&g_899,&g_899,(void*)0,&g_899,&g_899,&g_899,&g_899},{&g_899,&g_899,(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899},{(void*)0,(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899}},{{(void*)0,&g_899,&g_899,&g_899,(void*)0,&g_899,&g_899,&g_899},{(void*)0,&g_899,&g_899,&g_899,&g_899,(void*)0,(void*)0,&g_899},{(void*)0,&g_899,&g_899,(void*)0,&g_899,&g_899,&g_899,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0},{(void*)0,&g_899,(void*)0,&g_899,(void*)0,(void*)0,&g_899,&g_899},{(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899}},{{&g_899,(void*)0,(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899},{(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0},{&g_899,&g_899,&g_899,(void*)0,&g_899,&g_899,&g_899,&g_899},{(void*)0,&g_899,(void*)0,&g_899,(void*)0,&g_899,&g_899,&g_899},{&g_899,(void*)0,&g_899,&g_899,(void*)0,&g_899,&g_899,(void*)0},{(void*)0,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0,&g_899},{&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,&g_899,(void*)0}}};
static volatile union U3 g_906 = {1L};/* VOLATILE GLOBAL g_906 */
static volatile struct S1 g_924 = {0xCA9A0367L,5634,194,0x9DL,35292,-5,-440};/* VOLATILE GLOBAL g_924 */
static uint8_t g_953[4] = {8UL,8UL,8UL,8UL};
static uint64_t g_979 = 18446744073709551612UL;
static struct S1 g_1002 = {0x4F547AB0L,-7377,28,0xE2L,-20127,5,393};/* VOLATILE GLOBAL g_1002 */
static volatile struct S0 g_1008 = {0x6.253786p-62,0UL,0,0xAF271493D5F8A6C6LL};/* VOLATILE GLOBAL g_1008 */
static const int32_t ** volatile g_1016[4] = {&g_899,&g_899,&g_899,&g_899};
static int32_t g_1034 = 4L;
static const int32_t *g_1033 = &g_1034;
static const int32_t **g_1032 = &g_1033;
static const union U2 g_1060 = {-0x7.0p+1};/* VOLATILE GLOBAL g_1060 */
static union U3 g_1066 = {0x40L};/* VOLATILE GLOBAL g_1066 */
static uint64_t g_1070 = 1UL;
static struct S0 g_1083 = {0xB.294473p+1,1UL,103,0x1E563AC10BEB395ALL};/* VOLATILE GLOBAL g_1083 */
static uint8_t *g_1101 = &g_953[3];
static uint8_t **g_1100 = &g_1101;
static uint8_t ** const *g_1099[3][2][3] = {{{&g_1100,(void*)0,&g_1100},{(void*)0,(void*)0,(void*)0}},{{&g_1100,(void*)0,&g_1100},{(void*)0,(void*)0,(void*)0}},{{&g_1100,(void*)0,&g_1100},{(void*)0,(void*)0,(void*)0}}};
static volatile uint32_t g_1103 = 0xC39E3B26L;/* VOLATILE GLOBAL g_1103 */
static volatile struct S0 g_1107[1] = {{0x1.FDE11Dp+57,0x1EB2L,75,0xCB8D5E0F4FD82DA2LL}};
static volatile union U3 g_1108[8][9] = {{{0x6CL},{0x8CL},{-1L},{0x6CL},{0x80L},{1L},{1L},{0x56L},{1L}},{{9L},{0x72L},{0x6CL},{0x6CL},{0x72L},{9L},{0L},{0x8CL},{0x02L}},{{1L},{-8L},{9L},{0xC6L},{0x80L},{9L},{1L},{1L},{0x21L}},{{0x02L},{0x1FL},{1L},{0L},{-8L},{1L},{0L},{1L},{0x6CL}},{{0xC6L},{1L},{0x02L},{0x21L},{0x8CL},{1L},{1L},{0x8CL},{0x21L}},{{0xC6L},{0x0DL},{0xC6L},{1L},{0x3CL},{0x02L},{9L},{0x56L},{0x02L}},{{0x02L},{1L},{0xC6L},{1L},{0xF2L},{0x21L},{-1L},{0x39L},{1L}},{{1L},{0x1FL},{0x02L},{1L},{0xF2L},{0x6CL},{1L},{0x3CL},{1L}}};
static const union U2 *g_1121[6] = {&g_485,&g_485,&g_485,&g_485,&g_485,&g_485};
static const union U2 ** volatile g_1120 = &g_1121[2];/* VOLATILE GLOBAL g_1120 */
static volatile union U3 g_1149 = {-1L};/* VOLATILE GLOBAL g_1149 */
static volatile struct S1 g_1175 = {0x60BCE414L,7996,86,2L,9799,-15,-410};/* VOLATILE GLOBAL g_1175 */
static volatile struct S1 g_1181 = {0xA1FE0991L,-1706,35,0x7AL,-32890,-10,-381};/* VOLATILE GLOBAL g_1181 */
static float **g_1188 = (void*)0;
static float *** volatile g_1187 = &g_1188;/* VOLATILE GLOBAL g_1187 */
static volatile int8_t g_1209[4][3][7] = {{{0x55L,0L,1L,0L,(-1L),0xB9L,7L},{(-1L),0x15L,0L,0x8FL,0xB9L,1L,7L},{0x88L,1L,1L,(-1L),1L,0x98L,0x38L}},{{0xB4L,0xC6L,(-9L),1L,0xA0L,(-1L),0x8FL},{0x42L,1L,0L,0x98L,(-1L),(-1L),0L},{0xA0L,1L,3L,3L,1L,0xA0L,0L}},{{(-1L),0x42L,0L,1L,0L,0x6CL,1L},{(-6L),0x88L,7L,0L,(-1L),0x15L,0xB4L},{0x55L,0x42L,0xFDL,0x38L,0L,(-1L),(-1L)}},{{(-6L),1L,(-1L),7L,0x98L,0L,0x8FL},{(-9L),0x6CL,0x55L,(-5L),0x98L,1L,1L},{0L,0xFDL,(-5L),0xFDL,0L,(-6L),0L}}};
static volatile uint64_t g_1215 = 1UL;/* VOLATILE GLOBAL g_1215 */
static int8_t **g_1325 = (void*)0;
static union U3 g_1349 = {0xAEL};/* VOLATILE GLOBAL g_1349 */
static uint32_t g_1361[2] = {1UL,1UL};
static union U3 g_1364 = {0x7EL};/* VOLATILE GLOBAL g_1364 */
static int32_t g_1374 = 0x8CA8E8D0L;
static int32_t *g_1373 = &g_1374;
static union U3 *g_1411 = &g_1349;
static union U3 ** volatile g_1410 = &g_1411;/* VOLATILE GLOBAL g_1410 */
static uint8_t g_1421[4] = {255UL,255UL,255UL,255UL};
static union U3 g_1429 = {0xD7L};/* VOLATILE GLOBAL g_1429 */
static union U2 * volatile *g_1436 = &g_486[5];
static union U2 * volatile **g_1435[10][7][3] = {{{&g_1436,(void*)0,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,(void*)0},{&g_1436,&g_1436,&g_1436}},{{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0}},{{&g_1436,(void*)0,(void*)0},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436}},{{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,(void*)0},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,&g_1436}},{{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,(void*)0},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436}},{{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,(void*)0}},{{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,&g_1436},{&g_1436,&g_1436,(void*)0},{&g_1436,(void*)0,&g_1436},{(void*)0,&g_1436,&g_1436},{(void*)0,&g_1436,&g_1436},{(void*)0,&g_1436,(void*)0}},{{(void*)0,&g_1436,&g_1436},{(void*)0,(void*)0,&g_1436},{(void*)0,&g_1436,(void*)0},{(void*)0,&g_1436,&g_1436},{(void*)0,(void*)0,&g_1436},{(void*)0,(void*)0,&g_1436},{(void*)0,&g_1436,&g_1436}},{{(void*)0,&g_1436,&g_1436},{(void*)0,&g_1436,(void*)0},{(void*)0,&g_1436,&g_1436},{(void*)0,(void*)0,&g_1436},{(void*)0,&g_1436,(void*)0},{(void*)0,&g_1436,&g_1436},{(void*)0,(void*)0,&g_1436}},{{(void*)0,(void*)0,&g_1436},{(void*)0,&g_1436,&g_1436},{(void*)0,&g_1436,&g_1436},{(void*)0,&g_1436,(void*)0},{(void*)0,&g_1436,&g_1436},{(void*)0,(void*)0,&g_1436},{(void*)0,&g_1436,(void*)0}}};
static union U2 * volatile ** volatile * volatile g_1434 = &g_1435[9][1][2];/* VOLATILE GLOBAL g_1434 */
static struct S1 *g_1459[10][5] = {{&g_415,&g_560[0],&g_415,&g_415,&g_560[0]},{&g_580[1],(void*)0,(void*)0,&g_580[1],(void*)0},{&g_560[0],&g_560[0],&g_580[0],&g_560[0],&g_560[0]},{(void*)0,&g_580[1],(void*)0,(void*)0,&g_580[1]},{&g_560[0],&g_415,&g_415,&g_560[0],&g_415},{&g_580[1],&g_580[1],(void*)0,&g_580[1],&g_580[1]},{&g_415,&g_560[0],&g_415,&g_415,&g_560[0]},{&g_580[1],(void*)0,(void*)0,&g_580[1],(void*)0},{&g_560[0],&g_560[0],&g_580[0],&g_560[0],&g_560[0]},{(void*)0,&g_580[1],(void*)0,(void*)0,&g_580[1]}};
static struct S1 ** volatile g_1458[2] = {&g_1459[7][4],&g_1459[7][4]};
static struct S1 ** const  volatile g_1460[7][10] = {{&g_1459[4][0],&g_1459[7][4],&g_1459[4][0],(void*)0,&g_1459[8][2],(void*)0,&g_1459[7][4],&g_1459[7][4],(void*)0,&g_1459[8][2]},{&g_1459[7][4],&g_1459[7][4],&g_1459[7][4],&g_1459[7][4],&g_1459[4][0],&g_1459[7][3],(void*)0,&g_1459[7][4],(void*)0,&g_1459[7][3]},{(void*)0,&g_1459[2][1],&g_1459[4][0],&g_1459[2][1],(void*)0,&g_1459[6][2],&g_1459[7][4],(void*)0,(void*)0,&g_1459[7][4]},{&g_1459[7][4],&g_1459[6][2],&g_1459[7][4],&g_1459[7][4],&g_1459[6][2],&g_1459[7][4],&g_1459[8][2],&g_1459[7][4],(void*)0,&g_1459[7][4]},{&g_1459[2][1],&g_1459[7][4],(void*)0,(void*)0,(void*)0,&g_1459[7][4],&g_1459[2][1],&g_1459[8][2],&g_1459[7][3],&g_1459[7][3]},{&g_1459[2][1],&g_1459[7][3],&g_1459[7][4],&g_1459[4][0],&g_1459[4][0],&g_1459[7][4],&g_1459[7][3],&g_1459[2][1],&g_1459[6][2],&g_1459[8][2]},{&g_1459[7][4],&g_1459[7][3],&g_1459[2][1],&g_1459[6][2],&g_1459[7][3],&g_1459[7][4],&g_1459[7][4],(void*)0,&g_1459[4][0],&g_1459[6][2]}};
static struct S1 g_1462 = {0x2C49929AL,577,125,-6L,-18475,-4,195};/* VOLATILE GLOBAL g_1462 */
static uint32_t * const g_1519 = &g_485.f1;
static uint32_t * const  volatile *g_1518[2][1] = {{&g_1519},{&g_1519}};
static uint32_t **g_1520 = (void*)0;
static uint8_t g_1530 = 248UL;
static struct S0 g_1534[4][4] = {{{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL}},{{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL}},{{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL}},{{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL},{-0x1.3p+1,65528UL,18,0xC84EBA4B3F2F94C2LL}}};
static uint8_t g_1538 = 8UL;
static union U3 g_1541 = {-1L};/* VOLATILE GLOBAL g_1541 */
static union U2 ***g_1546 = (void*)0;
static union U3 g_1574[1] = {{4L}};
static const union U3 ***g_1578 = (void*)0;
static const union U3 **** volatile g_1577 = &g_1578;/* VOLATILE GLOBAL g_1577 */
static union U3 g_1585 = {-1L};/* VOLATILE GLOBAL g_1585 */
static union U3 g_1607 = {0xC6L};/* VOLATILE GLOBAL g_1607 */
static int8_t g_1673 = 0x25L;
static volatile uint16_t g_1678 = 0x2413L;/* VOLATILE GLOBAL g_1678 */
static union U3 **g_1681 = &g_1411;
static volatile struct S1 g_1691 = {1UL,1824,195,0xF4L,-32344,-0,413};/* VOLATILE GLOBAL g_1691 */
static volatile struct S1 g_1692 = {1UL,-4558,200,0x66L,26705,14,-317};/* VOLATILE GLOBAL g_1692 */
static int32_t g_1727[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
static volatile struct S1 g_1731 = {0xBC1A994DL,-1691,45,0x11L,40268,12,-131};/* VOLATILE GLOBAL g_1731 */
static int32_t g_1737 = 1L;
static union U2 *g_1760 = &g_485;
static union U2 **g_1759 = &g_1760;
static const volatile struct S1 g_1773 = {0x3B7A732FL,2556,148,0x7EL,-36824,-15,257};/* VOLATILE GLOBAL g_1773 */
static struct S1 ** volatile g_1784 = &g_1459[7][4];/* VOLATILE GLOBAL g_1784 */
static volatile union U2 g_1799 = {0x6.C283BBp-50};/* VOLATILE GLOBAL g_1799 */
static struct S1 g_1818 = {1UL,1584,201,3L,-34336,-5,5};/* VOLATILE GLOBAL g_1818 */
static volatile float g_1851[10] = {0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24,0xA.7D4543p+24};
static volatile float *g_1850 = &g_1851[3];
static volatile float ** volatile g_1849[5] = {&g_1850,&g_1850,&g_1850,&g_1850,&g_1850};
static volatile float ** volatile * volatile g_1848[4] = {&g_1849[0],&g_1849[0],&g_1849[0],&g_1849[0]};
static volatile float ** volatile * volatile *g_1847[7] = {&g_1848[1],&g_1848[1],&g_1848[1],&g_1848[1],&g_1848[1],&g_1848[1],&g_1848[1]};
static struct S1 ** volatile g_1855 = &g_1459[6][1];/* VOLATILE GLOBAL g_1855 */
static volatile union U3 g_1881[1][10] = {{{0xC5L},{0xC5L},{0xC5L},{0xC5L},{0xC5L},{0xC5L},{0xC5L},{0xC5L},{0xC5L},{0xC5L}}};
static int32_t **** const g_1885 = &g_135;
static int32_t **** const *g_1884 = &g_1885;
static union U2 g_1958 = {-0x1.Bp-1};/* VOLATILE GLOBAL g_1958 */
static const int32_t g_1988 = 0x442378E4L;
static union U3 g_1994 = {-10L};/* VOLATILE GLOBAL g_1994 */
static union U2 g_2000 = {0x0.1p-1};/* VOLATILE GLOBAL g_2000 */
static volatile union U3 g_2021 = {0xDAL};/* VOLATILE GLOBAL g_2021 */
static struct S1 g_2042 = {0x2FAA1E91L,-404,355,-5L,-38320,9,413};/* VOLATILE GLOBAL g_2042 */
static const int32_t g_2077 = 0x45C93C72L;
static const int32_t g_2079 = 0L;


/* --- FORWARD DECLARATIONS --- */
static union U3  func_1(void);
static int32_t * func_7(int32_t * p_8);
static int32_t * func_9(int32_t ** p_10, int32_t ** p_11, int32_t * p_12, int32_t * p_13);
static int32_t ** func_14(const int16_t  p_15, int16_t  p_16, int64_t  p_17, int16_t  p_18);
static int32_t * func_20(uint64_t  p_21, int32_t * const * p_22, int32_t * p_23, int32_t * p_24);
static struct S0  func_25(uint64_t  p_26, uint32_t  p_27, int32_t  p_28);
static int32_t * func_40(int32_t * const  p_41, int32_t * p_42, const int32_t ** p_43, uint8_t  p_44, uint64_t  p_45);
static int32_t * func_46(uint32_t  p_47, int16_t  p_48, int32_t * p_49);
static const int8_t  func_62(const int32_t * p_63);
static int32_t  func_67(int32_t ** p_68, int32_t ** p_69, int32_t ** p_70, const int32_t * p_71, int32_t ** p_72);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_1681 g_1411 g_1349
 * writes:
 */
static union U3  func_1(void)
{ /* block id: 0 */
    int32_t *l_4 = &g_5[0];
    int32_t l_1068 = (-7L);
    int32_t *l_1088[3][5] = {{&l_1068,&g_5[0],&g_5[0],&l_1068,&g_5[0]},{&g_1034,&g_1034,&g_5[1],&g_1034,&g_1034},{&g_5[0],&l_1068,&g_5[0],&g_5[0],&l_1068}};
    int32_t l_1090 = 0xE15B9174L;
    int32_t *l_1372 = &l_1068;
    int32_t l_1400[7];
    float l_1401 = 0x6.Ap+1;
    int8_t l_1402 = 0x49L;
    int64_t l_1418 = 0xDF662EFE13F6D791LL;
    uint64_t l_1426 = 1UL;
    float l_1450[10] = {0x1.Ep+1,0x1.Ep+1,0x0.Cp+1,0x9.FFFBA3p+26,0x0.Cp+1,0x1.Ep+1,0x1.Ep+1,0x0.Cp+1,0x9.FFFBA3p+26,0x0.Cp+1};
    struct S1 *l_1461 = &g_1462;
    union U2 **l_1545 = (void*)0;
    union U2 ***l_1544 = &l_1545;
    union U2 ****l_1547 = &g_1546;
    int16_t *l_1548 = &g_271;
    uint32_t *l_1558 = &g_1361[1];
    uint64_t l_1566 = 0x494085BABFE4C872LL;
    uint16_t l_1567 = 65526UL;
    const int16_t l_1568 = 0x058BL;
    int8_t ***l_1569 = &g_1325;
    int8_t *l_1570 = &g_452;
    uint32_t l_1571[7][10] = {{1UL,0xD33F18E5L,0xF2B478F5L,1UL,0x00952A0EL,0xF2B478F5L,0xF2B478F5L,0x00952A0EL,1UL,0xF2B478F5L},{0x00952A0EL,0x00952A0EL,18446744073709551608UL,0xD33F18E5L,0x00952A0EL,0xCD956C08L,0xD33F18E5L,0xD33F18E5L,0xCD956C08L,0x00952A0EL},{0x00952A0EL,0xF2B478F5L,0xF2B478F5L,0x00952A0EL,1UL,0xF2B478F5L,0xD33F18E5L,1UL,1UL,0xD33F18E5L},{1UL,0x00952A0EL,0xF2B478F5L,0xF2B478F5L,0x00952A0EL,1UL,0xF2B478F5L,0xD33F18E5L,1UL,1UL},{0x00952A0EL,0xD33F18E5L,18446744073709551608UL,0x00952A0EL,0x00952A0EL,18446744073709551608UL,0xD33F18E5L,0x00952A0EL,0xCD956C08L,0xD33F18E5L},{0x00952A0EL,1UL,0xF2B478F5L,0xD33F18E5L,1UL,1UL,0xD33F18E5L,0xF2B478F5L,1UL,0x00952A0EL},{1UL,0xD33F18E5L,0xF2B478F5L,1UL,0x00952A0EL,0xF2B478F5L,0xF2B478F5L,0x00952A0EL,1UL,0xF2B478F5L}};
    int16_t l_1580 = 0L;
    uint32_t l_1582 = 1UL;
    uint16_t *l_1588 = &g_39.f1;
    int16_t l_1601 = 0xBDC7L;
    uint16_t *l_1602[6][10] = {{(void*)0,&g_1083.f1,&g_1534[1][2].f1,&g_556.f1,&g_556.f1,&g_1534[1][2].f1,&g_1083.f1,(void*)0,&g_1534[1][2].f1,(void*)0},{&g_556.f1,&g_1534[1][2].f1,&g_556.f1,&g_556.f1,&g_556.f1,&g_1534[1][2].f1,&g_556.f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_556.f1},{&g_1534[1][2].f1,(void*)0,&g_556.f1,&g_556.f1,(void*)0,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_556.f1,&g_1534[1][2].f1,&g_36},{&g_1534[1][2].f1,&g_556.f1,&g_556.f1,&g_556.f1,&g_1534[1][2].f1,&g_556.f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_556.f1,&g_1534[1][2].f1},{&g_1534[1][2].f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_36,&g_1534[1][2].f1,&g_36,&g_1534[1][2].f1,&g_1534[1][2].f1},{&g_36,&g_1534[1][2].f1,&g_36,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_1534[1][2].f1,&g_36}};
    int64_t l_1624 = 0xC5506689F8590A2DLL;
    uint64_t l_1629 = 1UL;
    float l_1672 = 0x7.4p+1;
    int32_t l_1676 = (-1L);
    union U3 **l_1704 = (void*)0;
    int32_t *l_1729 = (void*)0;
    uint8_t l_1780 = 255UL;
    int32_t l_1782 = 4L;
    uint8_t l_1833 = 255UL;
    uint8_t *l_1838[10][7][3] = {{{&l_1833,&g_1421[1],&g_1421[1]},{&g_230,&g_1421[1],&g_230},{&l_1780,&g_953[1],&g_953[3]},{&g_1421[2],&l_1833,(void*)0},{&g_1421[1],&g_1421[1],&g_953[0]},{&g_1421[1],(void*)0,&g_1421[1]},{&g_953[3],&g_1530,&l_1780}},{{&l_1780,&g_1538,&g_1421[1]},{&g_953[0],&l_1833,&g_1421[1]},{&g_230,&l_1833,&g_1421[0]},{&g_953[0],&g_1421[3],&g_1421[1]},{&l_1780,&g_1530,&g_953[3]},{&g_953[3],(void*)0,&g_1538},{&g_1421[1],&g_953[2],&g_953[0]}},{{&g_1421[1],&g_1421[1],&g_1421[1]},{&g_1421[2],&g_1421[2],(void*)0},{&l_1780,(void*)0,&g_1421[1]},{&g_230,&g_953[0],&g_1421[1]},{&l_1833,&g_1421[1],&g_953[1]},{(void*)0,&g_230,&g_1421[1]},{&g_1421[1],&g_953[0],&g_1421[1]}},{{&l_1833,&g_1421[0],(void*)0},{&g_1421[3],(void*)0,&g_1421[1]},{&g_1421[1],&g_230,&g_953[0]},{&g_1538,&g_1538,&g_1538},{&g_1530,(void*)0,&g_953[3]},{&g_1538,&g_953[3],&g_1421[1]},{&g_953[2],(void*)0,&g_1421[0]}},{{&g_1421[1],&g_230,&g_1421[1]},{(void*)0,(void*)0,&g_1421[1]},{&l_1833,&g_953[3],&l_1780},{&g_230,(void*)0,&g_1421[1]},{(void*)0,&g_1538,&g_953[0]},{&l_1833,&g_230,(void*)0},{&g_953[3],(void*)0,&g_953[3]}},{{&g_1421[1],&g_1421[0],&g_230},{&g_1421[1],&g_953[0],&g_1421[1]},{(void*)0,&g_230,&g_953[2]},{(void*)0,&g_1421[1],&g_230},{(void*)0,&g_953[0],&g_1421[1]},{&g_1421[1],(void*)0,&g_230},{&g_1421[1],&g_1421[2],&l_1833}},{{&g_953[3],&g_1421[1],&g_1421[3]},{&l_1833,&g_953[2],&g_230},{(void*)0,(void*)0,&g_953[0]},{&g_230,&g_1530,&l_1833},{&l_1833,&g_1421[3],&g_1538},{(void*)0,&l_1833,(void*)0},{&g_1421[1],&l_1833,&g_1538}},{{&g_953[2],&g_1538,&l_1833},{&g_1538,&g_1530,&g_953[0]},{&g_1530,(void*)0,&g_230},{&g_1538,&g_1421[1],&g_230},{&g_953[0],(void*)0,(void*)0},{(void*)0,&g_953[3],(void*)0},{(void*)0,&g_1530,&g_1421[1]}},{{(void*)0,&g_1421[1],&g_953[2]},{&g_230,&g_1421[2],&g_230},{&g_953[0],&g_1421[1],&g_953[1]},{(void*)0,&g_1530,&g_1421[0]},{&g_230,&g_953[3],(void*)0},{&g_1421[3],(void*)0,&g_1421[1]},{&l_1780,&g_1530,&g_953[3]}},{{&g_1530,(void*)0,&g_1530},{&g_1421[1],&g_1421[1],&g_230},{&l_1833,(void*)0,(void*)0},{&g_1421[1],&g_953[0],(void*)0},{&g_1530,&g_230,(void*)0},{&g_1421[1],&g_230,&g_1538},{&l_1833,&g_230,&g_1421[2]}}};
    int8_t l_1876 = 0x5BL;
    float * const l_1999[10][10][2] = {{{&g_39.f0,(void*)0},{&g_556.f0,(void*)0},{&g_569[0],&g_556.f0},{&l_1450[3],&g_569[6]},{(void*)0,&g_556.f0},{&g_1534[1][2].f0,&l_1450[3]},{&g_1083.f0,&g_39.f0},{&l_1401,&l_1401},{&g_569[6],&g_208},{&g_569[0],&l_1450[3]}},{{&l_1672,&l_1450[3]},{&g_569[0],&g_208},{&g_569[6],&l_1401},{&l_1401,&g_39.f0},{&g_1083.f0,&l_1450[3]},{&g_1534[1][2].f0,&g_556.f0},{(void*)0,&g_569[6]},{&l_1450[3],&g_556.f0},{&g_569[0],(void*)0},{&g_556.f0,(void*)0}},{{&g_39.f0,&g_1083.f0},{(void*)0,&g_1083.f0},{&l_1401,&l_1450[1]},{(void*)0,&l_1401},{&g_208,&g_569[1]},{&g_208,&g_569[1]},{(void*)0,&g_1534[1][2].f0},{&g_569[1],&g_1083.f0},{&g_569[3],(void*)0},{&g_39.f0,&g_556.f0}},{{(void*)0,(void*)0},{&g_569[0],(void*)0},{&l_1450[3],&g_556.f0},{&g_1083.f0,&g_556.f0},{&l_1672,&g_1083.f0},{&g_1083.f0,&l_1450[3]},{&l_1401,&l_1401},{&g_556.f0,&g_208},{&g_569[0],&g_569[4]},{&l_1450[3],&l_1450[3]}},{{&g_569[5],&g_208},{&g_569[6],&g_569[6]},{&l_1401,&g_39.f0},{(void*)0,&g_1083.f0},{&g_1534[1][2].f0,&g_556.f0},{&g_1083.f0,&g_569[6]},{&g_569[4],(void*)0},{&g_569[0],&g_569[3]},{(void*)0,(void*)0},{&g_556.f0,(void*)0}},{{(void*)0,(void*)0},{&g_569[1],&l_1450[1]},{&g_556.f0,&g_569[1]},{&g_208,&l_1401},{&g_208,&g_569[1]},{&g_556.f0,&l_1450[1]},{&g_569[1],(void*)0},{(void*)0,(void*)0},{&g_556.f0,(void*)0},{(void*)0,&g_569[3]}},{{&g_569[0],(void*)0},{&g_569[4],&g_569[6]},{&g_1083.f0,&g_556.f0},{&g_1534[1][2].f0,&g_1083.f0},{(void*)0,&g_39.f0},{&l_1401,&g_569[6]},{&g_569[6],&g_208},{&g_569[5],&l_1450[3]},{&l_1450[3],&g_569[4]},{&g_569[6],&g_556.f0}},{{&g_208,&g_208},{&g_569[3],&g_208},{(void*)0,(void*)0},{(void*)0,&l_1672},{&l_1450[3],&g_208},{(void*)0,&g_1083.f0},{&l_1401,&g_569[5]},{&l_1672,(void*)0},{&l_1401,&g_569[0]},{&l_1672,&l_1450[3]}},{{&g_556.f0,&g_1083.f0},{&g_569[4],&g_556.f0},{&g_1083.f0,&g_556.f0},{&g_556.f0,&g_569[0]},{&g_569[4],&l_1401},{&g_569[0],&l_1450[3]},{&g_569[5],(void*)0},{&l_1401,&l_1672},{(void*)0,&g_569[5]},{&g_1534[1][2].f0,&g_569[0]}},{{(void*)0,&l_1672},{&g_39.f0,&l_1672},{&g_556.f0,(void*)0},{(void*)0,&l_1401},{&g_39.f0,&g_208},{&l_1672,&g_1083.f0},{&g_569[6],(void*)0},{&l_1450[3],(void*)0},{&g_569[6],&g_1083.f0},{&l_1672,&g_208}}};
    float * const *l_1998 = &l_1999[4][1][0];
    float * const **l_1997 = &l_1998;
    float * const ***l_1996 = &l_1997;
    int32_t *l_2027 = &l_1400[3];
    int32_t *l_2039 = &g_87[1];
    const int32_t **l_2040 = &g_899;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1400[i] = (-4L);
    return (**g_1681);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_7(int32_t * p_8)
{ /* block id: 550 */
    union U3 *l_1394[1];
    union U3 **l_1393[3][6];
    union U3 *l_1395 = (void*)0;
    int32_t *l_1396 = &g_5[0];
    int i, j;
    for (i = 0; i < 1; i++)
        l_1394[i] = &g_891;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
            l_1393[i][j] = &l_1394[0];
    }
    l_1395 = (void*)0;
    return l_1396;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_9(int32_t ** p_10, int32_t ** p_11, int32_t * p_12, int32_t * p_13)
{ /* block id: 546 */
    int64_t l_1381[1][8];
    int32_t ****l_1382 = &g_135;
    int32_t *l_1390 = &g_87[0];
    uint32_t *l_1392 = (void*)0;
    uint32_t **l_1391 = &l_1392;
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
            l_1381[i][j] = 0x5DF4D3AAC1E5036DLL;
    }
    (*p_12) = (((*l_1391) = l_1390) != p_13);
    return (*p_11);
}


/* ------------------------------------------ */
/* 
 * reads : g_1099 g_1103 g_679 g_1107 g_1108 g_756 g_227.f0 g_362 g_1032 g_1120 g_1108.f0 g_87 g_5 g_607.f2 g_1149 g_1070 g_1002.f5 g_953 g_556.f2 g_371 g_83 g_278 g_580.f0 g_1033 g_1034 g_452 g_39.f1 g_607.f0 g_1175 g_135 g_75 g_159 g_1083 g_891.f0 g_580.f3 g_560.f0 g_725 g_1181 g_1187 g_181.f0 g_1002.f2 g_1215 g_755 g_1101 g_1209 g_36 g_1002.f0 g_1325 g_717 g_718 g_1100 g_379 g_1008.f1 g_560.f3 g_466 g_104 g_1361 g_1066.f0 g_230
 * writes: g_1099 g_87 g_5 g_1033 g_1121 g_607.f2 g_953 g_271 g_79 g_1002.f0 g_75 g_378 g_39.f0 g_569 g_1083.f0 g_556.f0 g_1188 g_560.f0 g_1215 g_104 g_1181.f0 g_1034 g_83 g_36 g_891.f0 g_39.f1 g_76 g_362 g_379 g_1066.f0 g_1187
 */
static int32_t ** func_14(const int16_t  p_15, int16_t  p_16, int64_t  p_17, int16_t  p_18)
{ /* block id: 443 */
    uint8_t *l_1097[4];
    uint8_t **l_1096[10];
    uint8_t ***l_1095 = &l_1096[8];
    uint64_t *l_1098 = &g_979;
    uint8_t ** const **l_1102 = &g_1099[1][0][1];
    int32_t l_1106 = 0x5BDBCA1EL;
    int8_t *l_1112 = &g_1066.f0;
    int8_t **l_1111 = &l_1112;
    int32_t l_1118[4][2] = {{(-1L),(-1L)},{(-1L),(-1L)},{(-1L),(-1L)},{(-1L),(-1L)}};
    int32_t **l_1177 = &g_76;
    struct S0 **l_1183 = &g_682;
    uint16_t l_1247 = 0xE161L;
    int64_t l_1260 = (-4L);
    uint32_t l_1261 = 2UL;
    union U2 *l_1341 = &g_485;
    union U2 **l_1340[8] = {&l_1341,&l_1341,&l_1341,&l_1341,&l_1341,&l_1341,&l_1341,&l_1341};
    int16_t *l_1346 = &g_271;
    union U3 *l_1348 = &g_1349;
    union U3 **l_1347 = &l_1348;
    uint8_t l_1362 = 0xAEL;
    union U3 * const l_1363 = &g_1364;
    float ***l_1365 = (void*)0;
    float ****l_1366 = &l_1365;
    float ***l_1368[8] = {(void*)0,(void*)0,&g_1188,(void*)0,(void*)0,&g_1188,(void*)0,(void*)0};
    float ****l_1367[7] = {&l_1368[7],&l_1368[2],&l_1368[7],&l_1368[7],&l_1368[2],&l_1368[7],&l_1368[7]};
    float ***l_1369[3];
    int i, j;
    for (i = 0; i < 4; i++)
        l_1097[i] = &g_953[3];
    for (i = 0; i < 10; i++)
        l_1096[i] = &l_1097[1];
    for (i = 0; i < 3; i++)
        l_1369[i] = &g_1188;
    if (((safe_rshift_func_uint16_t_u_s(((safe_mul_func_uint16_t_u_u((l_1095 == ((*l_1102) = ((l_1098 == (void*)0) , g_1099[2][1][1]))), g_1103)) || ((0xFD35L == (p_15 , ((safe_mod_func_int32_t_s_s(0x3198A271L, l_1106)) ^ 0UL))) && l_1106)), g_679)) > 2UL))
    { /* block id: 445 */
        int8_t **l_1113 = &l_1112;
        const int32_t l_1114 = (-7L);
        uint32_t l_1115 = 4294967295UL;
        const union U2 *l_1119 = (void*)0;
        (*g_362) = (g_1107[0] , ((p_17 , g_1108[2][2]) , (l_1118[2][1] &= (safe_mod_func_int16_t_s_s((((*g_756) == (l_1111 == l_1113)) || ((l_1115 ^= (l_1114 , 253UL)) == (safe_mul_func_uint8_t_u_u(0x52L, l_1106)))), p_17)))));
        (*g_1032) = &l_1114;
        (*g_1120) = l_1119;
    }
    else
    { /* block id: 451 */
        int64_t l_1146 = (-4L);
        uint8_t **l_1199[5];
        int32_t l_1213 = (-2L);
        int32_t l_1237[1];
        int32_t *l_1330 = &l_1118[0][0];
        uint8_t l_1331 = 0xA6L;
        int i;
        for (i = 0; i < 5; i++)
            l_1199[i] = &g_1101;
        for (i = 0; i < 1; i++)
            l_1237[i] = 1L;
        (*g_362) |= (safe_sub_func_int16_t_s_s(g_1108[2][2].f0, p_18));
        if ((&g_134[3][6] != (void*)0))
        { /* block id: 453 */
            float l_1162 = 0xD.5EFB9Fp+91;
            int32_t l_1163 = 0x79C7D5C1L;
            int32_t l_1212 = 0x17491949L;
            int32_t l_1214 = 0xA7628883L;
            int32_t *l_1234 = &g_5[0];
            int32_t *l_1235 = &l_1212;
            int32_t *l_1236 = &l_1163;
            int32_t *l_1238 = &g_104;
            int32_t *l_1239 = &g_5[0];
            int32_t *l_1240 = &l_1214;
            int32_t *l_1241 = &l_1163;
            int32_t *l_1242 = &g_87[6];
            int32_t *l_1243 = &l_1106;
            int32_t *l_1244 = &l_1118[1][0];
            int32_t *l_1245 = &g_5[0];
            int32_t *l_1246 = &g_1034;
            for (g_607.f2 = 0; (g_607.f2 != 0); g_607.f2 = safe_add_func_int16_t_s_s(g_607.f2, 8))
            { /* block id: 456 */
                uint64_t * const * const l_1143[3][5][10] = {{{&g_371,&g_371,&g_371,(void*)0,&l_1098,&l_1098,&l_1098,&g_371,&l_1098,&g_371},{&l_1098,(void*)0,&g_371,&l_1098,&l_1098,&g_371,&l_1098,&g_371,&l_1098,&l_1098},{&l_1098,&g_371,&g_371,&g_371,&l_1098,(void*)0,&g_371,&g_371,&l_1098,&g_371},{(void*)0,&g_371,&l_1098,&g_371,&l_1098,&g_371,(void*)0,&g_371,&l_1098,&l_1098},{&l_1098,&g_371,&g_371,&g_371,&l_1098,&g_371,(void*)0,&g_371,&l_1098,&g_371}},{{&l_1098,&g_371,&g_371,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098,(void*)0,(void*)0},{&g_371,&g_371,(void*)0,&g_371,&g_371,&g_371,&g_371,&g_371,(void*)0,&g_371},{&l_1098,&g_371,(void*)0,&l_1098,&l_1098,(void*)0,&l_1098,&g_371,&l_1098,&g_371},{&g_371,(void*)0,&l_1098,&g_371,&g_371,(void*)0,&l_1098,&g_371,&l_1098,&l_1098},{&l_1098,&g_371,(void*)0,&g_371,(void*)0,&g_371,&l_1098,&l_1098,(void*)0,&g_371}},{{&g_371,&l_1098,&g_371,&l_1098,&g_371,&g_371,&l_1098,&l_1098,&l_1098,&g_371},{&l_1098,&g_371,&l_1098,&g_371,&g_371,&g_371,(void*)0,&g_371,(void*)0,&g_371},{(void*)0,&g_371,&l_1098,&g_371,&g_371,&g_371,&g_371,&g_371,(void*)0,&g_371},{&g_371,&g_371,&g_371,&l_1098,&l_1098,&l_1098,&g_371,&g_371,&g_371,&g_371},{&g_371,&g_371,(void*)0,&g_371,&g_371,(void*)0,(void*)0,&l_1098,&l_1098,&g_371}}};
                int16_t *l_1164 = &g_271;
                int64_t *l_1165 = &l_1146;
                int32_t *l_1166 = &l_1106;
                int16_t l_1172[3][1][7] = {{{1L,1L,1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L,1L,1L}}};
                uint32_t **l_1180 = (void*)0;
                float *l_1186 = (void*)0;
                float **l_1185 = &l_1186;
                uint8_t l_1207[2][4] = {{1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL}};
                int32_t l_1211 = 0x1064171DL;
                int32_t ****l_1231 = &g_135;
                int i, j, k;
                (*l_1166) |= (safe_mul_func_int16_t_s_s((((*l_1165) = (safe_sub_func_int64_t_s_s(((void*)0 == &g_282), (safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((safe_div_func_int32_t_s_s(((!(((*l_1164) = ((safe_rshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_u(((void*)0 == l_1143[1][1][7]), (((safe_add_func_uint64_t_u_u(((0x0.Ep-1 >= l_1146) , (safe_add_func_uint32_t_u_u((((*g_362) = (g_1149 , (safe_add_func_int8_t_s_s(((safe_lshift_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((safe_mod_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_s((0L || (l_1118[0][1] = (((safe_div_func_uint8_t_u_u((((***l_1095) |= ((g_1070 , g_1002.f5) || 6L)) < l_1118[2][1]), 7L)) && 0xC22FD5B78E17C780LL) >= 0UL))), g_556.f2)), (*g_371))), 1UL)), p_18)) < g_278), l_1163)))) && l_1163), g_580[0].f0))), 0x9678CAC5731258FELL)) & p_15) | 0UL))) >= p_18), 11)) | 255UL)) , (-1L))) , (*g_1033)), g_452)), p_15)), p_15)), 13))))) > (*g_371)), g_39.f1));
                for (g_79 = 0; (g_79 <= 3); g_79 += 1)
                { /* block id: 465 */
                    int32_t *l_1176 = &g_5[0];
                    uint8_t **l_1198 = &g_1101;
                    int32_t l_1202 = 0x87A9CF11L;
                    int32_t l_1210[10][10][2] = {{{1L,(-5L)},{(-8L),0xEC788675L},{0x361A3885L,(-1L)},{0x8AF0EB63L,(-1L)},{0x361A3885L,0xEC788675L},{(-8L),(-5L)},{1L,0x1A25F135L},{0x25F7B81BL,0xE762E140L},{0x03A78113L,0x8AE6A044L},{0L,0x6509AE2AL}},{{0xEAD1F438L,0x5CD08D35L},{1L,(-9L)},{0L,0x77363730L},{0x1A25F135L,(-1L)},{(-1L),0x8AF0EB63L},{0x5CD08D35L,0L},{(-8L),(-9L)},{0xC2F99786L,1L},{1L,0xE762E140L},{0L,(-1L)}},{{0x4CE7A255L,1L},{0xEAD1F438L,0x1A25F135L},{0xC2F99786L,0x6A5B44E2L},{(-4L),0x77363730L},{0x361A3885L,0x8AF0EB63L},{(-5L),(-5L)},{0x5CD08D35L,0xEC788675L},{0L,0x6A5B44E2L},{0x6509AE2AL,1L},{0x25F7B81BL,0x6509AE2AL}},{{0x4CE7A255L,0L},{0x4CE7A255L,0x6509AE2AL},{0x25F7B81BL,1L},{0x6509AE2AL,0x6A5B44E2L},{0L,0xEC788675L},{0x5CD08D35L,(-5L)},{(-5L),0x8AF0EB63L},{0x361A3885L,0x77363730L},{(-4L),0x6A5B44E2L},{0xC2F99786L,0x1A25F135L}},{{0xEAD1F438L,1L},{0x4CE7A255L,(-1L)},{0L,0xE762E140L},{1L,1L},{0xC2F99786L,(-9L)},{(-8L),0L},{0x5CD08D35L,0x8AF0EB63L},{(-1L),(-1L)},{0x1A25F135L,0x77363730L},{0L,(-9L)}},{{1L,0x5CD08D35L},{0xEAD1F438L,0x6509AE2AL},{0L,0x8AE6A044L},{0x03A78113L,0xE762E140L},{0x25F7B81BL,0x1A25F135L},{(-1L),1L},{0x03A78113L,0x361A3885L},{2L,0x3A6F4EC2L},{8L,0x3A6F4EC2L},{2L,0x361A3885L}},{{0x03A78113L,1L},{(-1L),(-1L)},{0x485F7E58L,0L},{1L,(-1L)},{9L,0x0E8AF403L},{(-4L),0x8AF0EB63L},{(-1L),0x25F7B81BL},{3L,0x5CD08D35L},{(-1L),0x3A6F4EC2L},{1L,8L}},{{0x8AF0EB63L,0x1A25F135L},{0x03A78113L,0x25F7B81BL},{0x8C427E2CL,(-5L)},{0L,0L},{9L,1L},{0xEC788675L,(-1L)},{(-4L),(-1L)},{0x8C427E2CL,0xEAD1F438L},{0L,0x5CD08D35L},{2L,8L}},{{0x9B9E2721L,0x9B9E2721L},{0x8AF0EB63L,0x361A3885L},{3L,0xEAD1F438L},{0x0E8AF403L,(-5L)},{0x485F7E58L,0x0E8AF403L},{0xEC788675L,(-1L)},{0xEC788675L,0x0E8AF403L},{0x485F7E58L,(-5L)},{0x0E8AF403L,0xEAD1F438L},{3L,0x361A3885L}},{{0x8AF0EB63L,0x9B9E2721L},{0x9B9E2721L,8L},{2L,0x5CD08D35L},{0L,0xEAD1F438L},{0x8C427E2CL,(-1L)},{(-4L),(-1L)},{0xEC788675L,1L},{9L,0L},{0L,(-5L)},{0x8C427E2CL,0x25F7B81BL}}};
                    int i, j, k;
                    for (g_1002.f0 = 0; (g_1002.f0 <= 3); g_1002.f0 += 1)
                    { /* block id: 468 */
                        int i, j, k;
                        (*g_1032) = func_20((safe_lshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s((((~((l_1172[0][0][4] | g_607.f0) < 0xF12CL)) | p_17) | ((*l_1164) = (safe_add_func_uint16_t_u_u(((g_1175 , (l_1118[3][1] = 0x9CE54E08L)) <= p_16), (l_1146 , g_278))))), 0x3E858FA4326A7FEFLL)), 0)), (*g_135), l_1176, &l_1106);
                        return l_1177;
                    }
                    if ((((safe_lshift_func_int16_t_s_s(0x8105L, (*l_1166))) , g_725) != l_1180))
                    { /* block id: 474 */
                        uint32_t l_1182 = 0xAE2FD7C1L;
                        float *l_1184 = &g_556.f0;
                        (*l_1184) = (((g_1181 , ((-9L) >= (0UL <= l_1182))) , l_1183) == &g_682);
                        (*g_1187) = l_1185;
                        l_1163 = (*l_1166);
                        if (p_15)
                            break;
                    }
                    else
                    { /* block id: 479 */
                        uint64_t l_1196[1];
                        uint32_t *l_1197 = &g_560[0].f0;
                        int32_t *l_1208[5];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1196[i] = 18446744073709551606UL;
                        for (i = 0; i < 5; i++)
                            l_1208[i] = (void*)0;
                        l_1207[0][3] = (((safe_sub_func_uint64_t_u_u((((*l_1197) = (safe_mul_func_uint8_t_u_u((~1L), ((safe_rshift_func_int16_t_s_s(g_181.f0, 3)) != l_1196[0])))) | ((l_1198 != l_1199[3]) >= g_1002.f2)), (((*l_1165) &= (safe_sub_func_uint8_t_u_u(l_1202, (p_15 ^ ((safe_rshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_u(0xDE46L, 0)), (*l_1166))) != p_17))))) >= p_18))) || p_17) , 0x568BE0C9L);
                        ++g_1215;
                    }
                    for (g_104 = 0; (g_104 <= 3); g_104 += 1)
                    { /* block id: 487 */
                        uint32_t l_1220 = 0x3260D012L;
                        int32_t ****l_1232 = (void*)0;
                        int32_t l_1233[1][5] = {{4L,4L,4L,4L,4L}};
                        int i, j;
                        (*g_362) &= p_17;
                        if (p_16)
                            break;
                        (*l_1166) = (((&g_1188 != (void*)0) & ((l_1220 = p_18) < p_15)) >= ((safe_div_func_int32_t_s_s((((((safe_mul_func_uint8_t_u_u((safe_div_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(0x23L, ((**l_1198) = (safe_mod_func_int64_t_s_s(((*l_1165) &= (5UL || ((&g_135 == (l_1232 = l_1231)) < (p_16 || (**g_755))))), l_1212))))), p_16)), l_1233[0][4])) ^ l_1214) , l_1213) | 0xE5L) >= p_16), g_580[0].f3)) != p_16));
                    }
                }
                for (g_1181.f0 = 0; g_1181.f0 < 4; g_1181.f0 += 1)
                {
                    for (g_79 = 0; g_79 < 6; g_79 += 1)
                    {
                        for (g_1034 = 0; g_1034 < 9; g_1034 += 1)
                        {
                            g_83[g_1181.f0][g_79][g_1034] = 1UL;
                        }
                    }
                }
            }
            ++l_1247;
        }
        else
        { /* block id: 500 */
            int32_t *l_1250 = &g_5[0];
            int32_t *l_1251 = &g_5[0];
            int32_t *l_1252 = &l_1118[2][1];
            int32_t *l_1253 = (void*)0;
            int32_t *l_1254 = &l_1118[2][1];
            int32_t *l_1255 = &l_1106;
            int32_t *l_1256 = &l_1237[0];
            int32_t *l_1257 = (void*)0;
            int32_t *l_1258 = &l_1237[0];
            int32_t *l_1259[8][7][4] = {{{&g_87[1],&g_87[1],&g_87[1],&g_5[1]},{&g_87[0],&g_1034,&l_1118[1][1],(void*)0},{(void*)0,&l_1237[0],&g_5[0],&l_1118[1][1]},{&g_87[1],&l_1237[0],(void*)0,(void*)0},{&l_1237[0],&g_1034,&l_1237[0],&g_5[1]},{&g_5[0],&g_87[1],&l_1118[1][1],(void*)0},{&g_104,&l_1106,&g_87[0],&l_1237[0]}},{{&g_5[0],&g_5[0],&l_1118[2][1],&g_104},{&l_1118[2][1],&g_87[2],(void*)0,&g_5[0]},{&l_1237[0],&l_1237[0],(void*)0,&g_5[1]},{&l_1106,&g_87[1],&l_1118[2][1],&g_87[1]},{(void*)0,&l_1118[2][1],&l_1118[1][1],&l_1118[1][1]},{&g_104,&l_1118[2][1],&g_5[0],&g_104},{(void*)0,&g_1034,&l_1237[0],&g_87[2]}},{{(void*)0,&l_1118[2][1],&g_1034,(void*)0},{&g_87[1],&g_87[1],(void*)0,&g_104},{&g_87[1],&l_1118[2][1],&l_1118[1][1],&g_87[2]},{&g_5[0],(void*)0,(void*)0,&l_1118[1][1]},{&g_87[1],&l_1118[2][1],&l_1106,&l_1118[2][1]},{&l_1237[0],(void*)0,&g_87[2],&g_87[1]},{&l_1237[0],&g_87[1],&g_1034,&g_1034}},{{&l_1237[0],&l_1237[0],&g_87[0],&l_1237[0]},{&l_1237[0],&l_1118[2][1],&g_1034,&g_104},{&l_1237[0],&l_1237[0],&g_87[2],&g_87[5]},{&g_1034,&g_87[1],&l_1106,(void*)0},{(void*)0,&g_87[1],&g_87[5],&l_1118[2][1]},{(void*)0,&l_1237[0],(void*)0,(void*)0},{(void*)0,&g_1034,&g_87[0],(void*)0}},{{&l_1118[2][1],&g_87[5],&l_1118[2][1],(void*)0},{&g_87[5],&l_1237[0],&g_5[0],&g_87[1]},{&g_1034,(void*)0,&g_104,(void*)0},{(void*)0,&g_87[1],(void*)0,&g_1034},{&l_1237[0],&g_5[0],&g_87[1],(void*)0},{&g_5[0],(void*)0,&l_1118[2][1],&g_5[0]},{&g_1034,&l_1237[0],&g_1034,&g_1034}},{{(void*)0,(void*)0,&g_87[1],&g_5[0]},{&l_1237[0],&g_5[0],&g_104,&l_1118[1][1]},{(void*)0,&g_87[0],&g_87[0],(void*)0},{&l_1118[1][0],(void*)0,&g_5[0],&g_87[1]},{&g_1034,&l_1118[2][1],&g_104,(void*)0},{&l_1118[2][1],&g_87[1],&g_87[1],(void*)0},{&l_1106,&l_1118[2][1],(void*)0,&g_87[1]}},{{&g_1034,(void*)0,&l_1237[0],(void*)0},{(void*)0,&g_87[0],&g_87[1],&l_1118[1][1]},{&g_87[5],&g_5[0],&g_87[1],&g_5[0]},{&l_1118[1][1],(void*)0,&g_5[0],&g_1034},{&g_87[2],&l_1237[0],(void*)0,&g_5[0]},{&g_5[0],(void*)0,(void*)0,(void*)0},{&l_1118[1][1],&g_5[0],&l_1118[2][1],&g_1034}},{{&g_1034,&g_87[1],&g_87[1],(void*)0},{&g_87[1],(void*)0,&g_87[5],&g_87[1]},{&g_1034,&l_1237[0],&g_104,(void*)0},{(void*)0,&g_87[5],&g_87[1],(void*)0},{&l_1106,&g_1034,&l_1118[2][1],(void*)0},{&g_1034,&l_1237[0],&l_1237[0],&l_1118[2][1]},{&g_1034,&g_87[1],&g_87[0],(void*)0}}};
            uint16_t *l_1270[7][7] = {{&g_1083.f1,(void*)0,(void*)0,&g_1083.f1,&g_39.f1,(void*)0,&g_39.f1},{&g_1083.f1,&g_1083.f1,&l_1247,&g_556.f1,&g_556.f1,(void*)0,&g_1083.f1},{&g_1083.f1,&g_39.f1,&g_39.f1,&g_36,&l_1247,&g_36,&g_39.f1},{&l_1247,&l_1247,&l_1247,&g_1083.f1,(void*)0,&g_36,&g_36},{(void*)0,&l_1247,&g_36,(void*)0,&g_1083.f1,(void*)0,&g_1083.f1},{&g_36,(void*)0,(void*)0,&g_36,(void*)0,(void*)0,&g_36},{&g_556.f1,&g_36,&g_1083.f1,&g_36,&l_1247,&g_1083.f1,(void*)0}};
            const int32_t * const **l_1304 = (void*)0;
            const int32_t * const ***l_1303 = &l_1304;
            int16_t l_1313 = 0x5C7EL;
            uint8_t ****l_1320 = (void*)0;
            int8_t **l_1327 = &l_1112;
            int i, j, k;
            l_1261--;
            if ((g_1209[3][2][3] == ((((l_1237[0] || ((safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_s((g_36 = ((void*)0 != l_1098)), 6)) , ((l_1237[0] |= ((p_18 | (++g_36)) > g_87[3])) & ((safe_sub_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u(p_18, (+l_1213))), p_15)) == 0xB91F744F7AEB750ALL))), 3L)), p_16)) >= p_15)) , p_15) , (*l_1250)) > l_1213)))
            { /* block id: 505 */
                int32_t l_1290 = 1L;
                int8_t ***l_1326[10][6] = {{&g_1325,&l_1111,&l_1111,&l_1111,&g_1325,&g_1325},{&l_1111,&g_1325,&g_1325,&g_1325,&g_1325,&l_1111},{&g_1325,&g_1325,&g_1325,&g_1325,&g_1325,&g_1325},{&g_1325,&g_1325,&l_1111,&g_1325,&g_1325,&l_1111},{&g_1325,&g_1325,&g_1325,&g_1325,&l_1111,&g_1325},{&g_1325,&g_1325,&g_1325,&l_1111,&g_1325,&g_1325},{&g_1325,&g_1325,&g_1325,&g_1325,&g_1325,&g_1325},{&g_1325,&g_1325,&g_1325,&g_1325,&g_1325,&l_1111},{&g_1325,&g_1325,&l_1111,&l_1111,&g_1325,&g_1325},{&g_1325,&g_1325,&g_1325,&g_1325,&g_1325,&g_1325}};
                uint16_t l_1328 = 0xB9D0L;
                int i, j;
                (*l_1251) = (safe_rshift_func_int8_t_s_s((safe_add_func_int32_t_s_s(0x7F373FEDL, (safe_rshift_func_uint16_t_u_s(p_15, ((-1L) || g_679))))), 2));
                (*l_1255) = p_16;
                for (g_891.f0 = 0; (g_891.f0 >= (-21)); g_891.f0--)
                { /* block id: 510 */
                    int32_t l_1294 = 0x2C0E78BBL;
                    for (g_39.f1 = 23; (g_39.f1 == 17); --g_39.f1)
                    { /* block id: 513 */
                        uint32_t *l_1291 = &g_1002.f0;
                        (*l_1250) &= (safe_sub_func_int32_t_s_s(l_1290, (p_15 != (l_1294 ^= ((*l_1291)++)))));
                        (*l_1258) = 0x69713A9FL;
                        (*l_1255) = (p_17 == (safe_mod_func_uint16_t_u_u(((g_83[2][5][2] != ((p_18 , (((safe_div_func_uint64_t_u_u((safe_add_func_int16_t_s_s(l_1294, (0L | (safe_add_func_uint32_t_u_u((0UL != ((((l_1303 == (void*)0) < (safe_unary_minus_func_uint32_t_u(((safe_mul_func_uint16_t_u_u((((0xC222L ^ p_16) <= 2L) , 0UL), p_18)) ^ l_1290)))) && l_1247) || l_1237[0])), p_16))))), 0x9DD5B6AE422BD5BCLL)) , p_15) != 65531UL)) ^ 0L)) > 65528UL), p_15)));
                    }
                }
                (*l_1252) &= ((safe_unary_minus_func_uint16_t_u((safe_sub_func_int32_t_s_s((safe_div_func_uint64_t_u_u(1UL, (l_1313 , (safe_rshift_func_int8_t_s_u((safe_sub_func_int64_t_s_s(((safe_div_func_uint8_t_u_u(((void*)0 == l_1320), ((safe_lshift_func_int8_t_s_u((((safe_div_func_int8_t_s_s(((l_1327 = g_1325) != (void*)0), (*g_717))) == ((void*)0 == l_1270[4][3])) | g_87[1]), 3)) , (**g_1100)))) > (*l_1251)), l_1328)), (**g_1100)))))), p_15)))) ^ 5L);
            }
            else
            { /* block id: 523 */
                int32_t **l_1329[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_1329[i] = &l_1259[1][1][3];
                return (*g_135);
            }
            (*g_75) = &l_1237[0];
        }
        --l_1331;
    }
    if (p_15)
    { /* block id: 530 */
        for (g_379 = 0; (g_379 <= 3); g_379 += 1)
        { /* block id: 533 */
            int32_t **l_1334 = &g_76;
            return l_1334;
        }
    }
    else
    { /* block id: 536 */
        int32_t **l_1335 = &g_76;
        return l_1335;
    }
    g_1187 = (l_1369[2] = ((*l_1366) = ((((safe_add_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u(((void*)0 != l_1340[4]), (safe_add_func_int16_t_s_s(((((*l_1346) = (safe_mul_func_uint16_t_u_u(g_1008.f1, 8UL))) | (((*l_1347) = (void*)0) != (((**l_1111) ^= (safe_rshift_func_uint16_t_u_u(((l_1098 != &g_83[1][3][0]) && (safe_mul_func_int8_t_s_s(((((((safe_lshift_func_uint16_t_u_u((((safe_unary_minus_func_int16_t_s(((safe_div_func_int8_t_s_s(((((g_560[0].f3 , (safe_rshift_func_int16_t_s_u((((g_466 || l_1260) < p_17) | p_15), 8))) && (*g_1101)) != g_104) >= l_1106), p_16)) , g_1361[1]))) , p_15) , g_953[1]), g_1034)) <= (-8L)) , (*g_371)) ^ 0x13BC044D1E4189D1LL) >= 0x93D2B3FEL) , 0L), l_1362))), p_16))) , l_1363))) & g_39.f1), g_230)))), l_1118[2][1])) & p_16) & 18446744073709551615UL) , l_1365)));
    return l_1177;
}


/* ------------------------------------------ */
/* 
 * reads : g_159 g_135 g_75 g_1083 g_891.f0 g_87 g_580.f3 g_560.f0
 * writes: g_75 g_378 g_39.f0 g_569 g_1083.f0
 */
static int32_t * func_20(uint64_t  p_21, int32_t * const * p_22, int32_t * p_23, int32_t * p_24)
{ /* block id: 431 */
    int32_t ***l_1071 = &g_378;
    int32_t l_1075 = 0x82A8C1FBL;
    uint32_t *l_1082 = &g_282;
    float *l_1084 = &g_39.f0;
    float *l_1085[5];
    const int32_t l_1086[5][6] = {{1L,0x975ACCCAL,0x5CACB39BL,4L,1L,4L},{0x0C0B3EB2L,1L,0x0C0B3EB2L,0L,1L,0L},{0x5CACB39BL,0x975ACCCAL,1L,0x0C0B3EB2L,0x0C0B3EB2L,1L},{(-1L),(-1L),(-8L),0x0C0B3EB2L,0x975ACCCAL,0L},{0x5CACB39BL,(-8L),0L,0L,0L,(-8L)}};
    int32_t l_1087 = 1L;
    int i, j;
    for (i = 0; i < 5; i++)
        l_1085[i] = &g_569[0];
    l_1087 = ((p_21 != (-0x1.2p+1)) < (g_1083.f0 = ((((l_1075 = (((((g_159 == (((*g_135) = (*g_135)) != ((*l_1071) = &p_23))) != (+(safe_sub_func_float_f_f(l_1075, ((safe_add_func_float_f_f((((g_569[0] = (safe_mul_func_float_f_f((safe_sub_func_float_f_f(((void*)0 != l_1082), ((((*l_1084) = (g_1083 , g_891.f0)) > l_1075) < g_87[1]))), 0xF.C13ADBp-41))) != 0xF.36C198p+28) != g_580[0].f3), 0x7.DBB5EFp+30)) == g_560[0].f0))))) != 0x2.BD1884p-13) < 0x7.6C72BAp+89) >= l_1075)) >= l_1086[0][2]) == p_21) == p_21)));
    return p_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_36 g_39
 * writes: g_36
 */
static struct S0  func_25(uint64_t  p_26, uint32_t  p_27, int32_t  p_28)
{ /* block id: 4 */
    int32_t *l_29 = &g_5[0];
    int32_t *l_30 = (void*)0;
    int32_t *l_31[4] = {&g_5[0],&g_5[0],&g_5[0],&g_5[0]};
    int32_t l_32[9] = {4L,4L,4L,4L,4L,4L,4L,4L,4L};
    int32_t l_33 = 0x20B4D66FL;
    int32_t l_34 = 0x9E88CDAFL;
    float l_35[8][8][4] = {{{0x8.6p+1,(-0x1.Bp+1),0x0.D768DBp+46,0xC.6D2ED0p-39},{0x1.Dp-1,0x1.Fp-1,0x0.D768DBp+46,(-0x4.Ep+1)},{0x8.6p+1,0xC.6D2ED0p-39,0x2.6p-1,0xC.6D2ED0p-39},{0x8.6p+1,(-0x4.Ep+1),0x0.D768DBp+46,0x1.Fp-1},{0x1.Dp-1,0xC.6D2ED0p-39,0x0.D768DBp+46,(-0x1.Bp+1)},{0x8.6p+1,0x1.Fp-1,0x2.6p-1,0x1.Fp-1},{0x8.6p+1,(-0x1.Bp+1),0x0.D768DBp+46,0xC.6D2ED0p-39},{0x1.Dp-1,0x1.Fp-1,0x0.D768DBp+46,(-0x4.Ep+1)}},{{0x8.6p+1,0xC.6D2ED0p-39,0x2.6p-1,0xC.6D2ED0p-39},{0x8.6p+1,(-0x4.Ep+1),0x0.D768DBp+46,0x1.Fp-1},{0x1.Dp-1,0xC.6D2ED0p-39,0x0.D768DBp+46,(-0x1.Bp+1)},{0x8.6p+1,0x1.Fp-1,0x2.6p-1,0x1.Fp-1},{0x8.6p+1,(-0x1.Bp+1),0x0.D768DBp+46,0xC.6D2ED0p-39},{0x1.Dp-1,0x1.Fp-1,0x0.D768DBp+46,(-0x4.Ep+1)},{0x8.6p+1,0xC.6D2ED0p-39,0x2.6p-1,0xC.6D2ED0p-39},{0x8.6p+1,(-0x4.Ep+1),0x0.D768DBp+46,0x1.Fp-1}},{{0x1.Dp-1,0xC.6D2ED0p-39,0x0.D768DBp+46,(-0x1.Bp+1)},{0x8.6p+1,0x1.Fp-1,0x2.6p-1,0x1.Fp-1},{0x8.6p+1,(-0x1.Bp+1),0x0.D768DBp+46,0xC.6D2ED0p-39},{0x1.Dp-1,0x1.Fp-1,0x0.D768DBp+46,(-0x4.Ep+1)},{0x8.6p+1,0xC.6D2ED0p-39,0x2.6p-1,0xC.6D2ED0p-39},{0x8.6p+1,(-0x4.Ep+1),0x0.D768DBp+46,0x1.Fp-1},{0x1.Dp-1,0xC.6D2ED0p-39,0x0.D768DBp+46,(-0x1.Bp+1)},{0x8.6p+1,0x1.Fp-1,0x2.6p-1,0x1.Fp-1}},{{0x8.6p+1,(-0x1.Bp+1),0x0.D768DBp+46,0xC.6D2ED0p-39},{0x1.Dp-1,(-0x4.Ep+1),0x2.6p-1,(-0x5.7p-1)},{0x1.Dp-1,(-0x1.Bp+1),0xF.8EF8B4p+32,(-0x1.Bp+1)},{0x1.Dp-1,(-0x5.7p-1),0x2.6p-1,(-0x4.Ep+1)},{0x0.9p+1,(-0x1.Bp+1),0x2.6p-1,0xF.D1F3BDp+61},{0x1.Dp-1,(-0x4.Ep+1),0xF.8EF8B4p+32,(-0x4.Ep+1)},{0x1.Dp-1,0xF.D1F3BDp+61,0x2.6p-1,(-0x1.Bp+1)},{0x0.9p+1,(-0x4.Ep+1),0x2.6p-1,(-0x5.7p-1)}},{{0x1.Dp-1,(-0x1.Bp+1),0xF.8EF8B4p+32,(-0x1.Bp+1)},{0x1.Dp-1,(-0x5.7p-1),0x2.6p-1,(-0x4.Ep+1)},{0x0.9p+1,(-0x1.Bp+1),0x2.6p-1,0xF.D1F3BDp+61},{0x1.Dp-1,(-0x4.Ep+1),0xF.8EF8B4p+32,(-0x4.Ep+1)},{0x1.Dp-1,0xF.D1F3BDp+61,0x2.6p-1,(-0x1.Bp+1)},{0x0.9p+1,(-0x4.Ep+1),0x2.6p-1,(-0x5.7p-1)},{0x1.Dp-1,(-0x1.Bp+1),0xF.8EF8B4p+32,(-0x1.Bp+1)},{0x1.Dp-1,(-0x5.7p-1),0x2.6p-1,(-0x4.Ep+1)}},{{0x0.9p+1,(-0x1.Bp+1),0x2.6p-1,0xF.D1F3BDp+61},{0x1.Dp-1,(-0x4.Ep+1),0xF.8EF8B4p+32,(-0x4.Ep+1)},{0x1.Dp-1,0xF.D1F3BDp+61,0x2.6p-1,(-0x1.Bp+1)},{0x0.9p+1,(-0x4.Ep+1),0x2.6p-1,(-0x5.7p-1)},{0x1.Dp-1,(-0x1.Bp+1),0xF.8EF8B4p+32,(-0x1.Bp+1)},{0x1.Dp-1,(-0x5.7p-1),0x2.6p-1,(-0x4.Ep+1)},{0x0.9p+1,(-0x1.Bp+1),0x2.6p-1,0xF.D1F3BDp+61},{0x1.Dp-1,(-0x4.Ep+1),0xF.8EF8B4p+32,(-0x4.Ep+1)}},{{0x1.Dp-1,0xF.D1F3BDp+61,0x2.6p-1,(-0x1.Bp+1)},{0x0.9p+1,(-0x4.Ep+1),0x2.6p-1,(-0x5.7p-1)},{0x1.Dp-1,(-0x1.Bp+1),0xF.8EF8B4p+32,(-0x1.Bp+1)},{0x1.Dp-1,(-0x5.7p-1),0x2.6p-1,(-0x4.Ep+1)},{0x0.9p+1,(-0x1.Bp+1),0x2.6p-1,0xF.D1F3BDp+61},{0x1.Dp-1,(-0x4.Ep+1),0xF.8EF8B4p+32,(-0x4.Ep+1)},{0x1.Dp-1,0xF.D1F3BDp+61,0x2.6p-1,(-0x1.Bp+1)},{0x0.9p+1,(-0x4.Ep+1),0x2.6p-1,(-0x5.7p-1)}},{{0x1.Dp-1,(-0x1.Bp+1),0xF.8EF8B4p+32,(-0x1.Bp+1)},{0x1.Dp-1,(-0x5.7p-1),0x2.6p-1,(-0x4.Ep+1)},{0x0.9p+1,(-0x1.Bp+1),0x2.6p-1,0xF.D1F3BDp+61},{0x1.Dp-1,(-0x4.Ep+1),0xF.8EF8B4p+32,(-0x4.Ep+1)},{0x1.Dp-1,0xF.D1F3BDp+61,0x2.6p-1,(-0x1.Bp+1)},{0x0.9p+1,(-0x4.Ep+1),0x2.6p-1,(-0x5.7p-1)},{0x1.Dp-1,(-0x1.Bp+1),0xF.8EF8B4p+32,(-0x1.Bp+1)},{0x1.Dp-1,(-0x5.7p-1),0x2.6p-1,(-0x4.Ep+1)}}};
    int i, j, k;
    g_36--;
    return g_39;
}


/* ------------------------------------------ */
/* 
 * reads : g_580.f6 g_371 g_83 g_271 g_452 g_1034
 * writes: g_83 g_607.f0 g_271
 */
static int32_t * func_40(int32_t * const  p_41, int32_t * p_42, const int32_t ** p_43, uint8_t  p_44, uint64_t  p_45)
{ /* block id: 423 */
    int8_t *l_1054 = &g_607.f0;
    const union U2 * const l_1059 = &g_1060;
    const union U2 * const *l_1058 = &l_1059;
    const union U2 * const **l_1057 = &l_1058;
    int32_t l_1061 = 0xA39BBCFCL;
    uint64_t l_1062 = 0xFABA09C7CD9D76ABLL;
    int16_t *l_1063 = &g_271;
    union U3 *l_1065 = &g_1066;
    union U3 **l_1064 = &l_1065;
    int32_t l_1067 = 0xF6E7FCB5L;
    l_1067 = (safe_mul_func_int16_t_s_s((safe_div_func_int64_t_s_s((p_44 != (safe_lshift_func_uint16_t_u_u(g_580[0].f6, (safe_add_func_uint64_t_u_u((safe_mod_func_uint8_t_u_u((((((((void*)0 == &g_716) , (safe_div_func_uint16_t_u_u(0UL, (((*l_1063) |= ((safe_sub_func_uint32_t_u_u((((safe_sub_func_int8_t_s_s(((*l_1054) = (p_45 > ((*g_371)--))), ((l_1061 = (safe_mod_func_int16_t_s_s(0L, (((((p_45 > (l_1057 != &l_1058)) ^ p_45) | p_44) != 0x8E8FC60DL) ^ 1L)))) == p_44))) > l_1062) | (-1L)), 0x14339D0FL)) | 0L)) | g_452)))) , l_1064) != &l_1065) < p_44) >= l_1062), p_44)), p_45))))), g_1034)), l_1062));
    return p_42;
}


/* ------------------------------------------ */
/* 
 * reads : g_39.f1 g_39.f2 g_5 g_75 g_79 g_39.f3 g_36 g_83 g_76 g_87 g_104 g_113 g_181 g_135 g_159 g_113.f1 g_213 g_227 g_228 g_230 g_228.f0 g_282 g_362 g_370 g_278 g_378 g_379 g_371 g_415 g_39 g_480 g_466 g_607 g_607.f0 g_560.f2 g_556.f1 g_560.f5 g_673 g_580.f3 g_679 g_452 g_680 g_681 g_560.f3 g_580.f2 g_716 g_556.f2 g_756 g_227.f0 g_560.f0 g_271 g_891.f0 g_717 g_718 g_1002 g_560.f4 g_1008 g_979
 * writes: g_39.f1 g_79 g_5 g_36 g_83 g_87 g_104 g_39.f0 g_213 g_230 g_208 g_271 g_278 g_282 g_76 g_75 g_379 g_159 g_362 g_480 g_486 g_560.f3 g_679 g_682 g_569 g_716 g_556.f1 g_891.f0 g_899
 */
static int32_t * func_46(uint32_t  p_47, int16_t  p_48, int32_t * p_49)
{ /* block id: 7 */
    uint8_t l_57[9][10] = {{0xD3L,246UL,0xD3L,0x4FL,246UL,255UL,255UL,246UL,0x4FL,0xD3L},{0x80L,0x80L,6UL,246UL,0x7DL,6UL,0x7DL,246UL,6UL,0x80L},{0x7DL,255UL,0xD3L,0x7DL,0x4FL,0x4FL,0x7DL,0xD3L,255UL,0x7DL},{0xD3L,0x80L,255UL,0x4FL,0x80L,0x4FL,6UL,0xD3L,7UL,7UL},{0x4FL,255UL,0x80L,0xD3L,0xD3L,0x80L,255UL,0x4FL,0x80L,0x4FL},{0xD3L,6UL,0UL,0xD3L,0UL,6UL,0xD3L,7UL,7UL,0xD3L},{7UL,0x4FL,0UL,0UL,0x4FL,7UL,6UL,0x4FL,6UL,7UL},{255UL,0x4FL,0x80L,0x4FL,255UL,0x80L,0xD3L,0xD3L,0x80L,255UL},{255UL,6UL,6UL,255UL,0UL,7UL,255UL,7UL,0UL,255UL}};
    int32_t *l_1031 = &g_87[4];
    int i, j;
    for (g_39.f1 = 0; (g_39.f1 <= 1); g_39.f1 += 1)
    { /* block id: 10 */
        int32_t *l_50 = &g_5[0];
        int32_t *l_51 = &g_5[1];
        int32_t *l_52 = &g_5[0];
        int32_t *l_53 = &g_5[1];
        int32_t *l_54 = &g_5[1];
        int32_t *l_55 = (void*)0;
        int32_t *l_56[5][4] = {{&g_5[1],&g_5[0],&g_5[1],(void*)0},{&g_5[1],&g_5[0],&g_5[g_39.f1],&g_5[0]},{&g_5[0],&g_5[0],&g_5[0],&g_5[0]},{&g_5[1],&g_5[0],&g_5[0],(void*)0},{&g_5[0],&g_5[1],&g_5[g_39.f1],&g_5[1]}};
        uint32_t l_1030[9];
        int i, j;
        for (i = 0; i < 9; i++)
            l_1030[i] = 0x6E42AFCFL;
        l_57[3][7]++;
        l_1030[1] |= ((safe_mul_func_int8_t_s_s(func_62(p_49), g_560[0].f0)) , ((*g_362) = ((*g_371) < p_48)));
        return l_1031;
    }
    return l_1031;
}


/* ------------------------------------------ */
/* 
 * reads : g_39.f2 g_5 g_75 g_79 g_39.f3 g_36 g_83 g_76 g_39.f1 g_87 g_104 g_113 g_181 g_135 g_159 g_113.f1 g_213 g_227 g_228 g_230 g_228.f0 g_282 g_362 g_370 g_278 g_378 g_379 g_371 g_415 g_39 g_480 g_466 g_607 g_607.f0 g_560.f2 g_556.f1 g_560.f5 g_673 g_580.f3 g_679 g_452 g_680 g_681 g_560.f3 g_580.f2 g_716 g_556.f2 g_756 g_227.f0 g_560.f0 g_271 g_891.f0 g_717 g_718 g_1002 g_560.f4 g_1008 g_979
 * writes: g_79 g_5 g_36 g_83 g_87 g_104 g_39.f0 g_213 g_230 g_208 g_271 g_278 g_282 g_76 g_75 g_379 g_159 g_362 g_480 g_486 g_560.f3 g_679 g_682 g_569 g_716 g_556.f1 g_891.f0 g_899
 */
static const int8_t  func_62(const int32_t * p_63)
{ /* block id: 12 */
    uint32_t l_66 = 4UL;
    int32_t *l_74 = &g_5[0];
    int32_t **l_73 = &l_74;
    int64_t *l_77 = (void*)0;
    int64_t *l_78 = &g_79;
    int32_t l_759 = (-1L);
    int32_t l_765[1];
    int16_t *l_831 = (void*)0;
    int8_t *l_835 = &g_452;
    struct S0 **l_852 = &g_682;
    const int8_t l_853 = (-8L);
    uint32_t *l_940[4][4];
    uint32_t **l_939 = &l_940[1][2];
    int64_t l_941 = 2L;
    uint32_t l_942 = 0xE0708B01L;
    uint64_t l_982[2];
    int32_t l_1003 = 0xBA18532CL;
    int32_t l_1012 = (-1L);
    int32_t l_1013[9][9] = {{(-1L),0L,(-1L),(-4L),(-6L),(-6L),(-4L),(-1L),0L},{0xF6961C4FL,0x4DD5CB1BL,(-1L),0xC2185B84L,0x4DD5CB1BL,(-1L),1L,(-1L),(-1L)},{(-1L),(-6L),0L,0L,0L,(-6L),(-1L),0L,(-1L)},{1L,0x4DD5CB1BL,0xB8493B73L,0xF6961C4FL,(-1L),0xB8493B73L,1L,0xB8493B73L,(-1L)},{0xAF1F6C48L,0L,0L,0xAF1F6C48L,0xBD13C0B7L,0xCF88EB08L,(-4L),0L,0xCF88EB08L},{0xD6A7C9CBL,(-1L),(-1L),0xF6961C4FL,0x20625EDDL,0x20625EDDL,0xF6961C4FL,(-1L),(-1L)},{(-4L),0xBD13C0B7L,(-1L),0L,0xBD13C0B7L,0L,(-1L),(-1L),(-1L)},{0xD6A7C9CBL,0x20625EDDL,(-1L),0xC2185B84L,(-1L),0x20625EDDL,0xD6A7C9CBL,(-1L),(-1L)},{0xAF1F6C48L,0xBD13C0B7L,0xCF88EB08L,(-4L),0L,0xCF88EB08L,(-1L),0xCF88EB08L,0L}};
    const int32_t **l_1017 = &g_899;
    int32_t *l_1018 = (void*)0;
    int32_t *l_1019 = &l_1013[2][5];
    int32_t *l_1020 = &l_765[0];
    int32_t *l_1021[5][7][6] = {{{&l_759,&l_1013[2][5],&g_5[0],&l_765[0],&g_87[1],&g_5[0]},{&l_759,&g_87[1],&g_87[1],&l_759,(void*)0,&g_5[0]},{&g_87[1],&g_87[1],&l_1013[2][5],&g_104,&g_87[1],&g_87[1]},{&g_5[0],&l_1013[2][5],&l_1013[2][5],&g_5[0],&g_87[1],&g_5[0]},{&l_759,&l_1013[2][5],&g_87[1],&g_5[0],&g_5[0],&g_5[0]},{&g_5[0],&g_5[0],&g_5[0],&g_104,&g_5[0],&g_5[1]},{&g_5[0],&g_5[1],&g_104,&g_5[0],&g_104,&g_5[1]}},{{(void*)0,&g_87[2],&l_1013[6][7],&g_87[1],&l_759,&l_1013[6][7]},{(void*)0,&g_104,&l_759,&g_5[0],&l_1012,&l_1012},{&g_5[0],&g_104,&g_87[2],&g_87[1],&l_759,&l_759},{&l_1013[2][5],&g_87[2],&g_87[2],&l_1013[2][5],&g_104,&l_1012},{&g_5[0],&g_5[1],&l_759,&l_1013[2][5],&l_1012,&l_1013[6][7]},{&l_1013[2][5],&l_1012,&l_1013[6][7],&g_87[1],&l_1012,&g_5[1]},{&g_5[0],&g_5[1],&g_104,&g_5[0],&g_104,&g_5[1]}},{{(void*)0,&g_87[2],&l_1013[6][7],&g_87[1],&l_759,&l_1013[6][7]},{(void*)0,&g_104,&l_759,&g_5[0],&l_1012,&l_1012},{&g_5[0],&g_104,&g_87[2],&g_87[1],&l_759,&l_759},{&l_1013[2][5],&g_87[2],&g_87[2],&l_1013[2][5],&g_104,&l_1012},{&g_5[0],&g_5[1],&l_759,&l_1013[2][5],&l_1012,&l_1013[6][7]},{&l_1013[2][5],&l_1012,&l_1013[6][7],&g_87[1],&l_1012,&g_5[1]},{&g_5[0],&g_5[1],&g_104,&g_5[0],&g_104,&g_5[1]}},{{(void*)0,&g_87[2],&l_1013[6][7],&g_87[1],&l_759,&l_1013[6][7]},{(void*)0,&g_104,&l_759,&g_5[0],&l_1012,&l_1012},{&g_5[0],&g_104,&g_87[2],&g_87[1],&l_759,&l_759},{&l_1013[2][5],&g_87[2],&g_87[2],&l_1013[2][5],&g_104,&l_1012},{&g_5[0],&g_5[1],&l_759,&l_1013[2][5],&l_1012,&l_1013[6][7]},{&l_1013[2][5],&l_1012,&l_1013[6][7],&g_87[1],&l_1012,&g_5[1]},{&g_5[0],&g_5[1],&g_104,&g_5[0],&g_104,&g_5[1]}},{{(void*)0,&g_87[2],&l_1013[6][7],&g_87[1],&l_759,&l_1013[6][7]},{(void*)0,&g_104,&l_759,&g_5[0],&l_1012,&l_1012},{&g_5[0],&g_104,&g_87[2],&g_87[1],&l_759,&l_759},{&l_1013[2][5],&g_87[2],&g_87[2],&l_1013[2][5],&g_104,&l_1012},{&g_5[0],&g_5[1],&l_759,&l_1013[2][5],&l_1012,&l_1013[6][7]},{&l_1013[2][5],&l_1012,&l_1013[6][7],&g_87[1],&l_1012,&g_5[1]},{&g_5[0],&g_5[1],&g_104,&g_5[0],&g_104,&g_5[1]}}};
    uint16_t l_1022 = 0xD9CDL;
    int16_t l_1025 = (-2L);
    int64_t l_1026 = (-1L);
    uint64_t l_1027 = 18446744073709551611UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_765[i] = 0xB5A242C3L;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
            l_940[i][j] = &g_282;
    }
    for (i = 0; i < 2; i++)
        l_982[i] = 0x4860F1AE1BD21330LL;
    if ((((safe_rshift_func_int16_t_s_s((((g_39.f2 != g_5[0]) != 0x7BL) , (l_66 , (func_67(l_73, &l_74, g_75, (((**l_73) = (((*l_78) ^= (*l_74)) & (safe_sub_func_int16_t_s_s(g_39.f3, g_5[0])))) , (*l_73)), &l_74) > 0x3F2A7929L))), g_556.f2)) || 0x7B73ECFDL) != l_66))
    { /* block id: 260 */
        int32_t *l_726[1];
        int16_t l_785[10][4] = {{0x156FL,0x341FL,0x341FL,0x156FL},{0x341FL,0x156FL,0x341FL,0x341FL},{0x156FL,0x156FL,0xFB33L,0x156FL},{0x156FL,0x341FL,0x341FL,0x156FL},{0xFB33L,0x341FL,0xFB33L,0xFB33L},{0x341FL,0x341FL,0x156FL,0x341FL},{0x341FL,0xFB33L,0xFB33L,0x341FL},{0xFB33L,0x341FL,0xFB33L,0xFB33L},{0x341FL,0x341FL,0x156FL,0x341FL},{0x341FL,0xFB33L,0xFB33L,0x341FL}};
        int64_t l_793 = (-1L);
        int8_t l_817 = (-8L);
        int8_t *l_836 = &g_607.f0;
        uint32_t l_881 = 0xD88A6399L;
        int32_t ****l_887 = (void*)0;
        int32_t *****l_886 = &l_887;
        uint32_t l_921 = 0xB99E64F0L;
        const uint32_t **l_925 = (void*)0;
        int i, j;
        for (i = 0; i < 1; i++)
            l_726[i] = (void*)0;
        for (g_556.f1 = 18; (g_556.f1 > 1); --g_556.f1)
        { /* block id: 263 */
            uint32_t **l_724 = (void*)0;
            int32_t l_730 = 0x972E596FL;
            int32_t l_757 = 0xE931ACA7L;
            int32_t l_764 = 0x7C1D52B2L;
            int32_t l_770 = 0x41881A46L;
            int32_t l_809[8][9][3] = {{{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)}},{{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L}},{{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)}},{{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L}},{{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)}},{{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L}},{{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)}},{{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L},{(-3L),(-3L),(-3L)},{0x74323085L,0x74323085L,0x74323085L}}};
            int32_t l_810 = 0x8D1AEA77L;
            int32_t l_814 = 0x181BAEC5L;
            int32_t l_816[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
            int64_t l_819[5];
            int32_t **l_893 = &g_76;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_819[i] = (-1L);
        }
    }
    else
    { /* block id: 386 */
        const union U2 *l_946 = &g_485;
        const union U2 **l_945[1][1];
        const union U2 ***l_947 = &l_945[0][0];
        uint8_t *l_949 = &g_230;
        uint8_t **l_948 = &l_949;
        uint8_t * const l_952 = &g_953[3];
        uint8_t * const *l_951 = &l_952;
        uint8_t * const **l_950 = &l_951;
        uint64_t **l_975 = &g_371;
        uint64_t ***l_974 = &l_975;
        uint64_t *l_978[9];
        int32_t l_980 = (-7L);
        int16_t l_981 = (-1L);
        uint64_t l_983 = 18446744073709551613UL;
        int8_t l_984[3][4] = {{7L,1L,7L,4L},{7L,4L,4L,7L},{(-3L),4L,0x7EL,4L}};
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_945[i][j] = &l_946;
        }
        for (i = 0; i < 9; i++)
            l_978[i] = &g_979;
        l_984[2][3] ^= (((*l_947) = l_945[0][0]) == (((l_948 == ((*l_950) = &l_949)) != (g_415.f2 ^ (0UL < (((safe_sub_func_int8_t_s_s((safe_sub_func_int8_t_s_s((safe_add_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s((((safe_rshift_func_int16_t_s_u((((((safe_rshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s((*g_756), 0x6CL)), 8)) != ((safe_add_func_uint8_t_u_u((((safe_mod_func_uint64_t_u_u((((*l_974) = (void*)0) != (((safe_lshift_func_int16_t_s_s((((l_980 &= (*g_371)) == 0xB382EE196CBFA34FLL) && (-1L)), g_415.f2)) <= 2UL) , (void*)0)), (*g_371))) , (void*)0) == g_716), g_607.f0)) != l_981)) != 0x037ADB3DL) > l_982[1]) || (*g_371)), l_981)) < 0x8F78CE93L) && l_980), l_981)), 18446744073709551615UL)), g_580[0].f2)), (-1L))) || g_560[0].f0) & l_983)))) , &g_486[6]));
        for (g_271 = 0; (g_271 > 11); ++g_271)
        { /* block id: 394 */
            int8_t *l_987 = &g_580[0].f3;
            int32_t l_995[2];
            int i;
            for (i = 0; i < 2; i++)
                l_995[i] = 0x67BA2B2EL;
            for (g_891.f0 = 0; (g_891.f0 <= 1); g_891.f0 += 1)
            { /* block id: 397 */
                int8_t *l_988 = &g_560[0].f3;
                int16_t *l_1009 = &g_607.f2;
                int16_t *l_1010 = &g_607.f2;
                int16_t *l_1011[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                int i;
                g_5[g_891.f0] &= (l_987 == l_988);
                if (g_5[g_891.f0])
                    break;
                l_1013[2][5] &= (l_1012 &= (((safe_mod_func_int8_t_s_s(((((((**g_716) , ((safe_lshift_func_int8_t_s_u(((safe_unary_minus_func_int8_t_s((safe_unary_minus_func_uint32_t_u((l_995[0] != (safe_lshift_func_uint16_t_u_s(l_984[2][3], (l_980 |= (safe_sub_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s(((g_1002 , (((((((((((((*g_371) = (*g_371)) | g_580[0].f2) & (l_1003 <= (safe_mod_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((l_984[2][3] , ((((g_560[0].f4 >= g_230) == g_159) ^ l_995[0]) == g_679)), g_5[g_891.f0])), g_36)))) && l_981) & (-1L)) ^ 0xE669L) || g_278) == 1UL) , g_1008) , l_984[2][3]) ^ g_36) > g_452)) && l_995[1]), 0)), l_995[0])))))))))) | l_995[0]), 5)) & 0x02L)) , g_5[g_891.f0]) > g_979) , (*g_717)) | 1UL), g_39.f2)) && 4294967295UL) == 1UL));
                l_980 ^= (**g_378);
            }
            if (l_981)
                break;
        }
        for (l_66 = (-29); (l_66 >= 10); ++l_66)
        { /* block id: 410 */
            return l_984[2][2];
        }
    }
    (*l_1017) = p_63;
    ++l_1022;
    l_1027--;
    return (*g_756);
}


/* ------------------------------------------ */
/* 
 * reads : g_36 g_5 g_83 g_75 g_76 g_39.f3 g_39.f1 g_87 g_104 g_79 g_113 g_181 g_135 g_159 g_113.f1 g_213 g_227 g_228 g_39.f2 g_230 g_228.f0 g_362 g_282 g_370 g_278 g_378 g_379 g_371 g_415 g_39 g_480 g_466 g_607 g_607.f0 g_560.f2 g_556.f1 g_560.f5 g_673 g_580.f3 g_679 g_452 g_680 g_681 g_560.f3 g_580.f2 g_716
 * writes: g_36 g_83 g_5 g_87 g_104 g_79 g_39.f0 g_213 g_230 g_208 g_271 g_278 g_282 g_76 g_75 g_379 g_159 g_362 g_480 g_486 g_560.f3 g_679 g_682 g_569 g_716
 */
static int32_t  func_67(int32_t ** p_68, int32_t ** p_69, int32_t ** p_70, const int32_t * p_71, int32_t ** p_72)
{ /* block id: 15 */
    const float l_90 = 0x1.5p+1;
    int32_t l_98[2][6] = {{(-1L),0xC66179C2L,(-1L),(-1L),0xC66179C2L,(-1L)},{(-1L),0xC66179C2L,(-1L),(-1L),0xC66179C2L,(-1L)}};
    int32_t l_99 = 0L;
    int32_t l_102 = 0xB7A4C9A1L;
    int32_t *l_105 = &l_102;
    int64_t l_152 = 0L;
    uint32_t l_161 = 1UL;
    uint64_t *l_202 = &g_83[1][3][0];
    uint64_t * const l_204 = &g_83[1][3][0];
    int32_t l_207 = 0x9939CF26L;
    int32_t *l_209 = &l_102;
    int32_t *l_210 = (void*)0;
    int32_t *l_211 = &g_104;
    int32_t *l_212[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t * const *l_224 = &l_209;
    int32_t * const **l_223 = &l_224;
    int32_t * const ***l_222 = &l_223;
    int32_t *l_254[7] = {&l_98[0][3],(void*)0,(void*)0,&l_98[0][3],(void*)0,(void*)0,&l_98[0][3]};
    int16_t l_350 = 0x96B5L;
    int64_t l_418 = 1L;
    int8_t *l_449[2];
    int8_t * const *l_448[4];
    union U2 *l_605[1];
    volatile union U3 *l_667 = &g_668;
    float l_714[5] = {0x9.0D80E1p+64,0x9.0D80E1p+64,0x9.0D80E1p+64,0x9.0D80E1p+64,0x9.0D80E1p+64};
    int i, j;
    for (i = 0; i < 2; i++)
        l_449[i] = &g_415.f3;
    for (i = 0; i < 4; i++)
        l_448[i] = &l_449[1];
    for (i = 0; i < 1; i++)
        l_605[i] = (void*)0;
    for (g_36 = 0; (g_36 <= 1); g_36 += 1)
    { /* block id: 18 */
        uint64_t *l_82 = &g_83[1][3][0];
        int32_t *l_86 = &g_87[1];
        int32_t *l_103 = &g_104;
        int i;
        (*l_86) = (g_5[g_36] = ((0xDD4F9EB425E41762LL == g_5[0]) == (0x114672487D748821LL != (++(*l_82)))));
        (*l_103) &= (((l_102 &= (safe_mod_func_int32_t_s_s((**g_75), ((safe_sub_func_uint32_t_u_u(((safe_add_func_int32_t_s_s((**p_72), (**g_75))) < ((~(!((g_39.f3 , &g_83[3][2][6]) == (void*)0))) & ((l_99 &= ((~0xBF23260E939A0984LL) , l_98[1][1])) , ((safe_mod_func_int16_t_s_s(g_39.f1, g_5[0])) | 0xAE82E7F0L)))), (*l_86))) | l_98[1][2])))) == 0xCE23668EL) >= 0x2D15554DL);
    }
    if (((*l_105) |= (*g_76)))
    { /* block id: 27 */
        uint32_t l_110 = 0x0FA5D7BFL;
        int32_t l_126 = 0xAAD204E7L;
        int32_t l_127 = 7L;
        int32_t l_143 = 0xAFFE66EDL;
        int32_t l_149 = 9L;
        int32_t l_150 = 0L;
        int32_t l_153 = 0x84FFCF1DL;
        int32_t l_154[9] = {0x14C34745L,0x14C34745L,0x14C34745L,0x14C34745L,0x14C34745L,0x14C34745L,0x14C34745L,0x14C34745L,0x14C34745L};
        int32_t *l_164 = (void*)0;
        int32_t *l_165 = &l_98[1][1];
        int32_t *l_166[3];
        uint16_t l_167 = 0x9D83L;
        int i;
        for (i = 0; i < 3; i++)
            l_166[i] = &l_153;
        for (g_79 = 29; (g_79 < 23); --g_79)
        { /* block id: 30 */
            int16_t l_115 = (-4L);
            int32_t l_137 = 0xE8E43806L;
            int32_t l_138[5][9] = {{0xD63BD277L,0x0B0314DCL,0xF202E733L,0x0B0314DCL,0xD63BD277L,0L,0x4A4612F2L,(-1L),5L},{(-1L),0x0B0314DCL,0L,5L,0xE0C65E12L,(-5L),0x41C64668L,(-5L),0xE0C65E12L},{0x4A4612F2L,0xE0C65E12L,0xE0C65E12L,0x4A4612F2L,0x52318AF5L,0L,0xF202E733L,0x41C64668L,0x05CF3AA0L},{0x4A4612F2L,2L,0x1052B527L,0L,0x05CF3AA0L,0x52318AF5L,0x52318AF5L,0x05CF3AA0L,0L},{(-1L),(-5L),(-1L),0x1052B527L,0x52318AF5L,0x41C64668L,5L,0x4FD85D12L,0L}};
            int32_t l_146[1][5][5] = {{{6L,6L,6L,6L,6L},{(-10L),(-10L),(-10L),(-10L),(-10L)},{6L,6L,6L,6L,6L},{(-10L),(-10L),(-10L),(-10L),(-10L)},{6L,6L,6L,6L,6L}}};
            uint32_t l_155 = 0x8F71FD4DL;
            float l_160[9] = {0xD.E3901Cp+37,0xD.E3901Cp+37,0xD.E3901Cp+37,0xD.E3901Cp+37,0xD.E3901Cp+37,0xD.E3901Cp+37,0xD.E3901Cp+37,0xD.E3901Cp+37,0xD.E3901Cp+37};
            int i, j, k;
            for (l_99 = 6; (l_99 != 13); ++l_99)
            { /* block id: 33 */
                if (l_110)
                    break;
                return (*p_71);
            }
            if ((g_87[8] |= ((safe_div_func_uint8_t_u_u(g_5[0], (*l_105))) ^ g_83[1][3][0])))
            { /* block id: 38 */
                int32_t l_139 = 1L;
                int32_t l_140 = 0xADD22AC3L;
                int32_t l_141 = (-1L);
                int32_t l_142 = 0xDED1B19EL;
                int32_t l_144 = 0xD8023FC2L;
                float l_145 = 0x4.D6AAB7p+48;
                int32_t l_147 = 1L;
                int32_t l_148 = 0x84388657L;
                int32_t l_151[9][3] = {{(-1L),0x0541D072L,0x9E310B34L},{0x0541D072L,0x0541D072L,(-1L)},{0x9E310B34L,(-1L),(-1L)},{(-1L),(-1L),0x5F283686L},{0x9E310B34L,(-1L),0x9E310B34L},{0x0541D072L,(-1L),0x5F283686L},{0x0541D072L,0x0541D072L,(-1L)},{0x9E310B34L,(-1L),(-1L)},{(-1L),(-1L),0x5F283686L}};
                int i, j;
                if (((g_113 , (void*)0) != (void*)0))
                { /* block id: 39 */
                    int32_t *l_114 = &l_99;
                    int32_t *l_116 = &g_87[3];
                    int32_t *l_117 = (void*)0;
                    int32_t *l_118 = &g_5[0];
                    int32_t *l_119 = &l_98[0][3];
                    int32_t *l_120 = (void*)0;
                    int32_t *l_121 = &g_5[1];
                    int32_t *l_122 = &l_102;
                    int32_t *l_123 = &l_98[1][1];
                    int32_t *l_124 = &l_99;
                    int32_t *l_125[9];
                    uint32_t l_128 = 0x3D9D43D6L;
                    float *l_131 = &g_39.f0;
                    int32_t ***l_136 = &g_75;
                    int i;
                    for (i = 0; i < 9; i++)
                        l_125[i] = &g_5[1];
                    --l_128;
                    (*l_131) = l_115;
                    for (l_99 = 0; (l_99 < 14); l_99++)
                    { /* block id: 44 */
                        l_136 = &g_75;
                    }
                    --l_155;
                }
                else
                { /* block id: 48 */
                    int32_t *l_158[10][4][3] = {{{(void*)0,(void*)0,&l_138[2][8]},{&l_154[8],&l_138[4][3],&l_98[0][1]},{&g_5[0],&l_127,&g_5[0]},{(void*)0,&l_127,(void*)0}},{{(void*)0,&l_138[4][3],&g_104},{&l_127,(void*)0,(void*)0},{(void*)0,&g_104,&l_151[0][1]},{&l_127,&l_138[4][1],&l_138[3][3]}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_5[0],(void*)0,&l_138[3][3]},{&l_154[8],&g_5[0],&l_151[0][1]}},{{(void*)0,&g_87[0],(void*)0},{&l_98[0][1],&g_5[0],&g_104},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_5[0]}},{{(void*)0,(void*)0,&l_98[0][1]},{(void*)0,&l_138[4][1],&l_138[2][8]},{&l_98[0][1],&g_104,(void*)0},{(void*)0,(void*)0,&l_138[2][8]}},{{&l_154[8],&l_138[4][3],&l_98[0][1]},{&g_5[0],&l_127,&g_5[0]},{(void*)0,&l_127,(void*)0},{(void*)0,&l_138[4][3],&g_104}},{{&l_127,(void*)0,(void*)0},{(void*)0,&g_104,&l_151[0][1]},{&l_127,&l_138[4][1],&l_138[3][3]},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{&g_5[0],(void*)0,&l_138[3][3]},{&l_154[8],&g_5[0],&l_151[0][1]},{(void*)0,&g_87[0],(void*)0}},{{&l_98[0][1],&g_5[0],&g_104},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_5[0]},{(void*)0,(void*)0,&l_98[0][1]}},{{(void*)0,&l_138[4][1],&l_138[2][8]},{&l_98[0][1],&g_104,(void*)0},{(void*)0,(void*)0,&l_138[2][8]},{&l_154[8],&l_138[4][3],&l_98[0][1]}}};
                    int i, j, k;
                    ++l_161;
                }
                (*p_72) = &l_138[4][1];
            }
            else
            { /* block id: 52 */
                (**p_70) ^= 0xE8D22055L;
            }
            if ((*g_76))
                continue;
        }
        --l_167;
    }
    else
    { /* block id: 58 */
        const float l_178 = 0x1.Dp+1;
        int32_t *l_182 = &g_87[1];
        uint64_t *l_183 = &g_83[0][0][2];
        uint64_t **l_203 = &l_202;
        (*g_76) = (g_87[0] < ((safe_mul_func_int16_t_s_s((((safe_lshift_func_uint16_t_u_s(((((safe_rshift_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u((*l_105), (safe_add_func_int64_t_s_s(((*g_75) != (g_181 , l_182)), ((*l_183) &= g_5[1]))))), (*l_182))) && ((safe_sub_func_int64_t_s_s((*l_105), (((*l_105) > 0x37L) , (-1L)))) != 4294967295UL)) ^ 0xAA6C780E02698774LL) & g_104), 4)) , g_76) == (void*)0), g_87[4])) || g_181.f5));
        (***g_135) = ((safe_add_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(((safe_sub_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(0x4D6B2067BBAE65E6LL, (safe_lshift_func_uint16_t_u_s(((*l_182) > (((*l_105) = (***g_135)) == (safe_sub_func_uint64_t_u_u((safe_sub_func_int64_t_s_s((((safe_sub_func_uint8_t_u_u(g_5[1], ((((*l_203) = l_202) == l_204) > (safe_sub_func_int16_t_s_s(l_161, ((1L != ((l_98[1][1] && g_159) != l_207)) , (*l_182))))))) <= g_113.f1) > (*l_182)), (*l_182))), g_159)))), g_104)))), (*l_182))) != (*l_182)), (**p_72))), l_98[1][4])) >= g_87[2]);
    }
    ++g_213;
    if ((safe_rshift_func_uint16_t_u_s((safe_add_func_int32_t_s_s(((*l_211) &= ((*g_76) = ((0x224FDA9CL && (**p_70)) < ((((safe_lshift_func_int8_t_s_u((l_222 == &g_135), 0)) > (safe_mul_func_int16_t_s_s(((7L <= (**l_224)) , (((g_227 , (g_228[5] , (((***l_223) | 7UL) == 0x2C949257L))) | g_5[0]) || 0x7ED15087L)), g_39.f2))) & 0x17DD8CD3L) | g_36)))), 0xFA8EDC44L)), 15)))
    { /* block id: 68 */
        uint8_t *l_229 = &g_230;
        float *l_234[8];
        int32_t l_241 = 0x08A6282CL;
        int32_t ***l_244 = &g_75;
        int32_t ** const * const l_245 = &g_75;
        int64_t l_246 = 0xED1E823F320E88ABLL;
        int32_t ** const *l_318 = &g_75;
        int32_t ** const **l_317 = &l_318;
        int32_t l_330 = 0x1C2A41C3L;
        int32_t l_331 = (-3L);
        int32_t l_332 = 1L;
        int32_t l_334 = 0x3D3B8DD7L;
        int32_t l_338[3][5] = {{0x3999C5EDL,(-1L),0x3999C5EDL,0L,0L},{0x3999C5EDL,(-1L),0x3999C5EDL,0L,0L},{0x3999C5EDL,(-1L),0x3999C5EDL,0L,0L}};
        uint64_t *l_382 = (void*)0;
        uint32_t l_404 = 0x05B8F7AFL;
        uint16_t l_423[9];
        int32_t *l_487 = &g_87[1];
        int32_t l_561 = 0L;
        uint32_t l_664 = 18446744073709551615UL;
        int16_t *l_677 = (void*)0;
        int i, j;
        for (i = 0; i < 8; i++)
            l_234[i] = &g_39.f0;
        for (i = 0; i < 9; i++)
            l_423[i] = 65535UL;
        if ((((((*l_229)++) < (((((**l_224) , (((+(g_208 = 0x7.D43EE3p-35)) != (safe_sub_func_float_f_f(((((safe_sub_func_float_f_f(g_228[5].f0, (safe_sub_func_float_f_f(l_241, l_241)))) >= (g_39.f0 = g_5[0])) <= (safe_sub_func_float_f_f(l_241, (l_244 == l_245)))) == 0xD.6EC189p+47), 0x1.Ap+1))) > l_246)) , g_39.f2) ^ 0x4EA68FB3AADD4B25LL) >= (***g_135))) , &l_246) != &g_159))
        { /* block id: 72 */
            uint16_t l_247 = 0xEEC0L;
            int16_t *l_270 = &g_271;
            int32_t l_276 = 0x39B585EEL;
            int8_t *l_277 = &g_278;
            uint32_t *l_281 = &g_282;
            int64_t *l_283 = &l_152;
            int64_t *l_284[4][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
            int32_t l_295 = 8L;
            int8_t l_314 = 0xA0L;
            int32_t ** const **l_319 = &l_318;
            int32_t l_321 = (-1L);
            int32_t l_333 = 0xE33EB2F2L;
            int32_t l_337[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            float l_352 = 0x0.5p+1;
            uint64_t l_427 = 18446744073709551615UL;
            int16_t l_439 = 0x1782L;
            uint32_t l_539 = 18446744073709551615UL;
            uint64_t *l_571 = &l_427;
            int32_t l_600 = 0xAE74AEF5L;
            int i, j;
            l_247++;
lbl_287:
            (***l_245) = (((--g_230) != (~(safe_unary_minus_func_int8_t_s((((**l_245) == (l_254[5] = (**l_245))) , g_5[0]))))) , (*l_209));
            if ((safe_div_func_uint64_t_u_u((safe_rshift_func_int16_t_s_s((!(((safe_add_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((safe_div_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(((*l_270) = (safe_mul_func_int16_t_s_s(g_87[8], 0x4FD1L))), l_247)), (safe_div_func_int32_t_s_s(((safe_div_func_int8_t_s_s(((*l_277) = l_276), (4UL & 0x64L))) & (l_276 >= ((***l_245) = ((*l_283) = (safe_rshift_func_int8_t_s_s((((*l_281) = ((***l_244) && (***l_244))) , 6L), 2)))))), g_83[1][3][0])))), 6)), (*l_209))) <= (**l_224)) || (-4L))), (****l_222))), g_83[1][3][0])))
            { /* block id: 82 */
                int8_t l_294 = 0x20L;
                int32_t l_320 = 6L;
                int32_t l_325 = 0x50D31762L;
                int32_t l_328 = 0L;
                int32_t l_335 = 0x49759B55L;
                float l_340 = 0x4.6598BCp+81;
                int32_t l_341 = 0L;
                int32_t l_349 = 0xB86D0D46L;
                int32_t l_353 = 0xD0E68FDFL;
                int32_t l_356[7][3][2] = {{{0x798E68DCL,0x915107CCL},{0x798E68DCL,0xB53FDD81L},{0x73DF16D8L,(-1L)}},{{0xB53FDD81L,0xC3E0866EL},{0xAFD6690BL,6L},{0x9C295ACAL,0x72CE9D82L}},{{0x72CE9D82L,7L},{(-2L),0x9C295ACAL},{0x915107CCL,0x140A6335L}},{{6L,0x140A6335L},{0x915107CCL,0x9C295ACAL},{0xAFD6690BL,6L}},{{0x798E68DCL,0x798E68DCL},{7L,(-2L)},{0xA8135809L,0x140A6335L}},{{1L,0x72CE9D82L},{0xB53FDD81L,1L},{6L,(-1L)}},{{6L,1L},{0xB53FDD81L,0x72CE9D82L},{1L,0x140A6335L}}};
                uint32_t l_359 = 0x8345C613L;
                int32_t l_369 = 0x9DD7C9ECL;
                uint16_t l_372 = 65535UL;
                int32_t * const ***l_402 = &l_223;
                uint32_t *l_405 = &l_404;
                uint32_t l_436 = 4294967290UL;
                uint32_t l_440[5][7][4] = {{{0xA12C8202L,18446744073709551609UL,1UL,0x339696A0L},{18446744073709551606UL,0x57779247L,0xDDD26CDAL,18446744073709551609UL},{0x5B0575B3L,0x2FE0A090L,0xDDD26CDAL,18446744073709551612UL},{18446744073709551606UL,0x21FE37D9L,1UL,2UL},{0xA12C8202L,0x01FBBC12L,18446744073709551612UL,0UL},{18446744073709551612UL,0UL,1UL,0x57779247L},{0UL,0x21FE37D9L,0xDBAE5DA3L,4UL}},{{18446744073709551609UL,0x5B0575B3L,0x5B0575B3L,18446744073709551609UL},{0xDDD26CDAL,2UL,0xB761592DL,0xF0434472L},{0UL,18446744073709551609UL,0x5A760E5CL,18446744073709551608UL},{4UL,1UL,18446744073709551612UL,18446744073709551608UL},{1UL,18446744073709551609UL,0x01FBBC12L,0xF0434472L},{18446744073709551606UL,2UL,0x9ACA6ABEL,18446744073709551609UL},{0x2FE0A090L,0x5B0575B3L,0xDDD26CDAL,4UL}},{{0xB761592DL,0x21FE37D9L,0x01FBBC12L,0x57779247L},{0xA12C8202L,0UL,0x9050A612L,0UL},{4UL,0x01FBBC12L,1UL,2UL},{0x01FBBC12L,0x21FE37D9L,0xB761592DL,18446744073709551612UL},{18446744073709551609UL,0x2FE0A090L,18446744073709551615UL,18446744073709551609UL},{18446744073709551609UL,0x57779247L,0xB761592DL,0x339696A0L},{0x01FBBC12L,18446744073709551609UL,1UL,0x12AC7278L}},{{4UL,0UL,0x9050A612L,18446744073709551608UL},{0xA12C8202L,0xDDD26CDAL,0x01FBBC12L,0x339696A0L},{0xB761592DL,2UL,0xDDD26CDAL,0xDDD26CDAL},{0x2FE0A090L,0x2FE0A090L,0x9ACA6ABEL,4UL},{18446744073709551606UL,0x0863E606L,0x01FBBC12L,2UL},{1UL,0UL,18446744073709551612UL,0x01FBBC12L},{4UL,0UL,0x5A760E5CL,2UL}},{{0UL,0x0863E606L,0xB761592DL,4UL},{0xDDD26CDAL,0x2FE0A090L,0x5B0575B3L,0xDDD26CDAL},{18446744073709551609UL,2UL,0xDBAE5DA3L,0x339696A0L},{0UL,0xDDD26CDAL,1UL,18446744073709551608UL},{18446744073709551612UL,0UL,18446744073709551612UL,0x12AC7278L},{0xA12C8202L,18446744073709551609UL,1UL,0x339696A0L},{18446744073709551606UL,0x57779247L,0xDDD26CDAL,18446744073709551609UL}}};
                int8_t * const l_451 = &g_452;
                int8_t * const *l_450 = &l_451;
                float l_476 = 0xC.7677DFp-22;
                int i, j, k;
lbl_430:
                for (l_241 = (-14); (l_241 == 19); l_241++)
                { /* block id: 85 */
                    int8_t *l_310 = &g_278;
                    int8_t **l_311 = &l_277;
                    int8_t *l_313 = &l_294;
                    int8_t **l_312 = &l_313;
                    const int32_t l_315 = 1L;
                    int32_t l_316 = 1L;
                    if (g_39.f2)
                        goto lbl_287;
                    l_316 = (g_208 = (((((((***l_223) < (safe_mul_func_float_f_f((safe_sub_func_float_f_f(((((safe_mul_func_float_f_f(l_247, (l_295 = l_294))) <= (((((safe_mul_func_uint16_t_u_u(g_113.f1, ((0xA405D672L == (safe_div_func_int32_t_s_s((((safe_div_func_int16_t_s_s((~(safe_add_func_int64_t_s_s((((safe_lshift_func_int8_t_s_s(((((*l_312) = ((*l_311) = (l_310 = ((safe_mul_func_int16_t_s_s((safe_unary_minus_func_int8_t_s(((void*)0 == (*l_223)))), l_294)) , &g_278)))) == (void*)0) < g_5[0]), 2)) , &g_278) == &g_278), l_247))), 65535UL)) | g_230) , 0x212BC34BL), 0xD5659A52L))) >= l_314))) && l_294) <= g_5[0]) >= g_83[3][3][7]) , (***l_244))) != (***l_244)) != 0x0.0p+1), 0x0.3p-1)), l_315))) < g_83[1][3][0]) != g_282) <= 0xE.D0E94Fp+59) < g_83[3][2][7]) != g_5[0]));
                }
                if (((l_319 = l_317) == (g_213 , (void*)0)))
                { /* block id: 95 */
                    uint16_t l_322 = 1UL;
                    l_322++;
                }
                else
                { /* block id: 97 */
                    int8_t l_327 = 0L;
                    int32_t l_342 = 0xBF9F11C0L;
                    int32_t l_346 = (-2L);
                    int32_t l_347 = 0x981C1CB0L;
                    int32_t l_348 = 0x0FB35F5CL;
                    int32_t l_351[9] = {0x2F0B213FL,0x2F0B213FL,0x2F0B213FL,0x2F0B213FL,0x2F0B213FL,0x2F0B213FL,0x2F0B213FL,0x2F0B213FL,0x2F0B213FL};
                    int64_t l_377 = 0xBFD380D2B9B9DC79LL;
                    int i;
                    for (l_246 = 0; (l_246 <= 3); l_246 += 1)
                    { /* block id: 100 */
                        float l_326 = 0x1.0p+1;
                        int32_t l_329 = 0xC3D7BF49L;
                        int32_t l_336 = 0x1077C2F8L;
                        int32_t l_339 = 0x3BA08297L;
                        int32_t l_343 = (-1L);
                        int32_t l_344 = 0L;
                        int32_t l_345 = (-1L);
                        int32_t l_354 = 0x41C29687L;
                        int32_t l_355 = 0x0C8D3A4BL;
                        int32_t l_357 = 0x4D32078EL;
                        int32_t l_358 = (-1L);
                        l_359++;
                        (*p_70) = ((*p_69) = g_362);
                        (*l_211) &= (safe_lshift_func_int8_t_s_s(((safe_sub_func_uint8_t_u_u(((*l_229)--), (l_369 < ((**p_70) = (***l_244))))) | (g_370 != (void*)0)), l_294));
                        ++l_372;
                    }
                    for (l_346 = 9; (l_346 <= (-27)); l_346 = safe_sub_func_uint8_t_u_u(l_346, 2))
                    { /* block id: 111 */
                        l_377 = g_278;
                        (**p_70) = (((****l_317) && (&p_71 != ((*l_244) = g_378))) >= ((-1L) == (g_181.f1 , (g_278 ^= 0x86L))));
                        (*l_105) = (****l_319);
                    }
                }
                ++g_379;
                if (((l_382 != l_202) , (safe_mod_func_int8_t_s_s(((*l_277) = (((*l_405) = ((safe_div_func_int64_t_s_s((safe_rshift_func_int8_t_s_u((safe_sub_func_int64_t_s_s((((****l_319) == (((&p_68 == &p_68) , (((g_36 == (safe_mod_func_uint32_t_u_u((safe_mod_func_int64_t_s_s((safe_mod_func_int16_t_s_s((!((safe_mul_func_uint8_t_u_u((((l_402 != l_319) <= (!((((l_202 != (void*)0) == g_5[0]) || (**g_370)) || 0x2EL))) != 0UL), (***l_245))) , 0xA91C581AL)), g_79)), l_404)), g_83[1][0][2]))) != 0x1CC7L) || (***l_245))) <= 0x3E38L)) , g_181.f6), (****l_319))), 5)), (-2L))) , (****l_319))) != (***g_135))), g_159))))
                { /* block id: 122 */
                    int8_t *l_416[9] = {&g_278,&g_278,&g_278,&g_278,&g_278,&g_278,&g_278,&g_278,&g_278};
                    int32_t *l_417 = &l_98[1][1];
                    int32_t l_419 = 0x829D64EFL;
                    float l_420 = 0xE.3D098Cp-4;
                    int32_t l_421 = (-1L);
                    int32_t l_422[8] = {0x603639A3L,0x603639A3L,0x603639A3L,0x603639A3L,0x603639A3L,0x603639A3L,0x603639A3L,0x603639A3L};
                    int i;
                    (***l_318) = ((safe_sub_func_float_f_f((-0x2.Fp-1), 0x1.2p+1)) == (+(safe_mul_func_float_f_f((safe_div_func_float_f_f(((****l_402) == (((g_39.f0 = ((***l_244) >= (safe_div_func_float_f_f((func_25((g_415 , ((l_277 = &g_278) != l_416[7])), (****l_402), (((g_159 = ((p_71 == l_234[3]) | (*g_371))) , (***l_245)) >= (****l_319))) , (****l_319)), g_79)))) , g_5[0]) < g_39.f1)), (****l_402))), 0xC.DAA8B1p+59))));
                    l_417 = (*g_378);
                    ++l_423[5];
                    if ((**g_378))
                    { /* block id: 129 */
                        int8_t l_426 = 0x5AL;
                        --l_427;
                        (*p_69) = (void*)0;
                    }
                    else
                    { /* block id: 132 */
                        (**g_135) = (*g_75);
                    }
                }
                else
                { /* block id: 135 */
                    int64_t l_431 = (-8L);
                    int32_t l_433 = 0x53653B32L;
                    int32_t l_435[4];
                    int32_t ****l_443[5][10][4] = {{{&l_244,&g_135,&g_135,&g_135},{&l_244,&l_244,&g_135,&l_244},{&l_244,&l_244,&l_244,&l_244},{&l_244,&l_244,&g_135,&l_244},{&g_135,&l_244,&g_135,&g_135},{&l_244,&l_244,&g_135,&g_135},{&l_244,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&l_244},{&l_244,&l_244,(void*)0,&g_135},{&l_244,&l_244,&g_135,&l_244}},{{&g_135,&l_244,&g_135,&g_135},{&l_244,&l_244,(void*)0,&l_244},{&l_244,&l_244,&g_135,&l_244},{&g_135,&l_244,&g_135,&l_244},{&l_244,&l_244,&g_135,&l_244},{&l_244,&g_135,&g_135,&l_244},{&g_135,&l_244,&g_135,&g_135},{&l_244,&l_244,&l_244,&g_135},{&l_244,&l_244,&g_135,&g_135},{&g_135,&l_244,&l_244,&g_135}},{{&l_244,&l_244,&l_244,&l_244},{&l_244,&g_135,&g_135,&l_244},{&g_135,&l_244,&l_244,&l_244},{&l_244,&l_244,&g_135,&l_244},{&l_244,&l_244,&g_135,&l_244},{&l_244,&l_244,&g_135,&g_135},{&l_244,&l_244,&g_135,&l_244},{&l_244,&l_244,&g_135,&g_135},{&l_244,&l_244,&g_135,&l_244},{&l_244,&g_135,&g_135,&g_135}},{{&l_244,&g_135,&l_244,&g_135},{&g_135,&l_244,&g_135,&g_135},{&l_244,&l_244,&l_244,&l_244},{&l_244,&l_244,&l_244,&l_244},{&g_135,&l_244,&g_135,&l_244},{&l_244,&l_244,&l_244,&l_244},{&l_244,&l_244,&g_135,&l_244},{&g_135,&l_244,&g_135,&g_135},{&l_244,&l_244,&g_135,&g_135},{&l_244,&g_135,&g_135,&g_135}},{{&g_135,&g_135,&g_135,&l_244},{&l_244,&l_244,(void*)0,&g_135},{&l_244,&l_244,&g_135,&l_244},{&g_135,&l_244,&g_135,&g_135},{&l_244,&l_244,(void*)0,&l_244},{&l_244,&l_244,&g_135,&l_244},{&g_135,&l_244,&g_135,&l_244},{&l_244,&l_244,&g_135,&l_244},{&l_244,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135}}};
                    uint32_t *l_444 = &l_404;
                    int32_t *** const *l_475[7] = {&l_244,&l_244,&l_244,&l_244,&l_244,&l_244,&l_244};
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_435[i] = 0x4ED938CBL;
                    for (g_282 = 0; (g_282 <= 8); g_282 += 1)
                    { /* block id: 138 */
                        int32_t l_432[5][5][3] = {{{0xEED0CCDFL,0xDDEA1BE2L,0xC68D558BL},{5L,0x215C90B1L,(-1L)},{(-7L),0L,(-8L)},{0xC68D558BL,(-8L),(-8L)},{0xB555D5DAL,1L,(-1L)}},{{0x24597E23L,7L,0xC68D558BL},{4L,0xD065A48FL,(-1L)},{(-6L),0L,0x215C90B1L},{1L,0xD065A48FL,0x5A3D138EL},{0xB555D5DAL,7L,0xD0DC103DL}},{{(-1L),1L,0xC68D558BL},{0L,(-8L),0xC1607425L},{0L,0L,0xB555D5DAL},{(-1L),0x215C90B1L,0xD065A48FL},{0xB555D5DAL,0xDDEA1BE2L,0x419B02CBL}},{{1L,0x133223F9L,0xC68D558BL},{(-6L),0xB555D5DAL,0x419B02CBL},{4L,0L,0xD065A48FL},{0x24597E23L,0x5A3D138EL,0xB555D5DAL},{0xB555D5DAL,0x832CC451L,0xC1607425L}},{{0xC68D558BL,0x832CC451L,0xC68D558BL},{(-7L),0x5A3D138EL,0xD0DC103DL},{5L,0L,0x5A3D138EL},{0xEED0CCDFL,0xB555D5DAL,0x215C90B1L},{0xB555D5DAL,0x133223F9L,(-1L)}}};
                        int64_t l_434 = 0x61D954F8D5CAD57BLL;
                        int i, j, k;
                        if (g_415.f2)
                            goto lbl_430;
                        l_436++;
                        ++l_440[0][5][0];
                        (****l_319) = g_87[g_282];
                    }
                    if ((0xB089B96EL > ((void*)0 == l_443[0][2][3])))
                    { /* block id: 144 */
                        uint32_t **l_445 = (void*)0;
                        uint32_t **l_446 = (void*)0;
                        uint32_t **l_447 = &l_444;
                        int32_t l_461 = 0L;
                        const int8_t *l_465 = &g_466;
                        const int8_t **l_464 = &l_465;
                        int32_t *****l_479 = &l_443[0][2][3];
                        (*l_211) ^= (((***l_223) , ((p_71 != ((*l_447) = l_444)) || (((l_450 = l_448[2]) == (void*)0) != (((****l_319) || (safe_mod_func_uint16_t_u_u((((***l_318) <= ((((((*l_281) = (((safe_lshift_func_uint16_t_u_u((safe_div_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u((****l_319), l_461)), (-4L))), 8)) , 1UL) != g_415.f1)) , 0xBAL) || g_181.f5) >= 0xF6315291AAE7A633LL) > 0x5EB3L)) <= (****l_319)), g_83[0][2][0]))) != 0UL)))) <= l_461);
                        (**l_224) = ((***g_135) = (***g_135));
                        (*l_105) = (safe_add_func_int32_t_s_s(((((*l_464) = &l_314) == &l_314) , (safe_mul_func_uint16_t_u_u((safe_div_func_uint32_t_u_u((((*l_229) &= (***l_245)) >= (g_415.f5 < (safe_rshift_func_int16_t_s_s((((((((((safe_rshift_func_uint8_t_u_u((****l_319), (((****l_402) != (l_475[0] == ((*l_479) = (((g_39.f2 && (safe_rshift_func_int8_t_s_u(((void*)0 != &g_371), (***l_223)))) != (*g_371)) , &l_244)))) > (****l_402)))) & g_415.f2) || g_415.f2) ^ (***g_135)) < (****l_402)) ^ (****l_402)) > 5L) <= (*g_371)) && (***l_245)), (****l_319))))), g_36)), g_39.f2))), (****l_317)));
                    }
                    else
                    { /* block id: 155 */
                        union U2 *l_484 = &g_485;
                        union U2 **l_483[8][1][4] = {{{(void*)0,(void*)0,(void*)0,&l_484}},{{&l_484,&l_484,&l_484,&l_484}},{{&l_484,&l_484,&l_484,&l_484}},{{&l_484,&l_484,&l_484,&l_484}},{{&l_484,&l_484,&l_484,&l_484}},{{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_484,&l_484,(void*)0}},{{&l_484,(void*)0,&l_484,(void*)0}}};
                        int i, j, k;
                        --g_480;
                        g_486[6] = (void*)0;
                    }
                    (*l_487) = (1UL | (((**p_70) ^= (****l_402)) && (((**l_245) = l_487) == (void*)0)));
                    (*g_362) |= 0xA69229FFL;
                }
            }
            else
            { /* block id: 164 */
                int16_t l_519 = 9L;
                int32_t l_522 = 7L;
                int32_t * const **l_533[9][2] = {{&l_224,&l_224},{(void*)0,&l_224},{&l_224,&l_224},{&l_224,&l_224},{(void*)0,&l_224},{&l_224,&l_224},{&l_224,&l_224},{(void*)0,&l_224},{&l_224,&l_224}};
                int32_t l_568 = 0x57298A71L;
                int8_t l_594 = 0x36L;
                int i, j;
                for (g_79 = 1; (g_79 <= 7); g_79 += 1)
                { /* block id: 167 */
                    const int32_t l_523 = 0x69B10AD6L;
                    uint32_t *l_529 = &g_415.f0;
                    int32_t l_532 = 0xBDAD0C7BL;
                    int32_t l_534 = (-5L);
                    int32_t l_535[6][8][5] = {{{(-8L),(-2L),1L,0xD5F96C70L,(-1L)},{(-6L),(-1L),(-2L),0x2117A4CEL,0x17F6FED5L},{0L,(-5L),(-6L),1L,1L},{0x587F1D1EL,0x723F1017L,0x39D51E3EL,0x72A25E73L,0x43CA985CL},{0x0D56D5ABL,1L,(-1L),0L,1L},{0x2F9D5CDEL,(-6L),(-6L),0x2F9D5CDEL,0x39D51E3EL},{(-10L),(-1L),1L,2L,0L},{0L,0L,0x2117A4CEL,(-1L),(-1L)}},{{(-2L),(-1L),0xAC51F746L,2L,0L},{0x9A745319L,0x2652B163L,(-5L),0x2F9D5CDEL,0L},{0x587F1D1EL,0x729C92BFL,0L,0L,0x78A59628L},{0L,(-1L),0x0D56D5ABL,1L,8L},{1L,0x72A25E73L,0x9F400FA3L,0L,0x307F9F6FL},{0x625C0E7AL,1L,(-6L),0x409122E5L,0x54638D5FL},{6L,0xD85DB3D0L,0x409122E5L,(-8L),1L},{1L,0x307F9F6FL,0x2C8835BCL,(-5L),1L}},{{0xC30C2108L,1L,(-5L),(-6L),0x54638D5FL},{0x9A745319L,(-6L),0L,0x876761E2L,0x307F9F6FL},{0xBC648A7CL,0L,(-1L),0xA9A08B39L,8L},{(-6L),(-1L),1L,0x76480D70L,0x78A59628L},{0x587F1D1EL,(-2L),0L,0L,(-2L)},{6L,0x2C8835BCL,(-6L),1L,(-5L)},{0x2C8835BCL,(-1L),0x9A745319L,(-1L),0x0D56D5ABL},{0x37A81801L,1L,(-1L),0L,1L}},{{0x2C8835BCL,0x4B926720L,(-3L),0x54638D5FL,6L},{6L,1L,(-1L),1L,(-5L)},{0x587F1D1EL,0L,0x2C8835BCL,(-2L),0L},{(-6L),0x37A81801L,0xD5F96C70L,0L,0x54638D5FL},{0xBC648A7CL,0x9F400FA3L,(-9L),1L,0L},{0x9A745319L,0L,0L,(-9L),0x0D56D5ABL},{0xC30C2108L,(-2L),0x39D51E3EL,0x76480D70L,0x37A81801L},{1L,(-2L),(-1L),0xC30C2108L,(-10L)}},{{6L,0L,0x0D56D5ABL,(-2L),0xF5CC5F0BL},{0x625C0E7AL,0x9F400FA3L,0x9A745319L,0L,0xBC648A7CL},{1L,0x37A81801L,0L,0x409122E5L,1L},{0L,0L,(-6L),(-1L),(-1L)},{0L,1L,0L,(-1L),1L},{0x2117A4CEL,0x4B926720L,0xD76F6C96L,(-2L),0x876761E2L},{0x9F400FA3L,1L,0x78A59628L,0xC30C2108L,6L},{0xBC648A7CL,(-1L),0xD76F6C96L,0x876761E2L,(-10L)}},{{0xAC51F746L,0x2C8835BCL,0L,0x625C0E7AL,0xBC648A7CL},{(-6L),(-2L),(-6L),0x17F6FED5L,0x37A81801L},{(-3L),(-1L),0L,(-1L),1L},{0L,0L,0x9A745319L,(-5L),(-5L)},{0xA9A08B39L,(-6L),0x0D56D5ABL,0L,5L},{0x72A25E73L,1L,(-1L),0x54638D5FL,0x307F9F6FL},{0L,0x307F9F6FL,0x39D51E3EL,0x54638D5FL,0x2652B163L},{0L,0xD85DB3D0L,0L,0L,0L}}};
                    const int16_t *l_577[4][6] = {{&l_350,&l_350,&l_350,&l_350,&l_350,&l_350},{&l_350,&l_350,&l_350,&l_350,&l_350,&l_350},{&l_350,&l_350,&l_350,&l_350,&l_350,&l_350},{&l_350,&l_350,&l_350,&l_350,&l_350,&l_350}};
                    int i, j, k;
                    for (l_247 = 0; (l_247 <= 2); l_247 += 1)
                    { /* block id: 170 */
                        uint16_t l_520 = 65535UL;
                        int16_t *l_521 = &l_519;
                        uint16_t *l_524 = &g_36;
                        int i, j;
                        l_338[l_247][l_247] = (((safe_mul_func_int16_t_s_s((~(safe_mul_func_uint16_t_u_u(((*l_524) |= ((safe_add_func_int64_t_s_s((***l_318), (safe_add_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((((safe_mul_func_int16_t_s_s(((safe_mod_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_s(0x38D2L, ((((safe_mod_func_uint8_t_u_u((g_466 , (((0L == 7UL) && g_230) == (safe_rshift_func_int16_t_s_s(((*l_222) != &p_72), 9)))), ((*l_277) = (safe_rshift_func_uint8_t_u_s((safe_add_func_int16_t_s_s((safe_add_func_int8_t_s_s((safe_add_func_int16_t_s_s(((*l_521) = ((*l_270) = (((l_519 ^ l_519) || l_520) , 0x56D0L))), (*l_487))), 255UL)), 0x5DFFL)), 5))))) < l_522) , 0x4C92DDD171794065LL) , (***l_245)))) , g_278), l_523)) , 0xBEAAL), l_523)) != (**g_370)) && 0x492BL), 1)), 0x9447L)))) && 0UL)), 1L))), l_523)) || (*l_487)) >= 0UL);
                        (*l_211) = 4L;
                    }
                }
            }
        }
        else
        { /* block id: 208 */
            union U2 **l_606 = &l_605[0];
            int32_t l_631[10] = {9L,0xD01D29ABL,0xD01D29ABL,9L,6L,9L,0xD01D29ABL,0xD01D29ABL,9L,6L};
            uint64_t l_688 = 0x507957BF77A3C083LL;
            int32_t l_715 = 3L;
            int i;
            (*l_606) = l_605[0];
            (*l_487) = (g_607 , (**p_70));
            for (l_246 = 0; (l_246 == (-23)); l_246 = safe_sub_func_uint16_t_u_u(l_246, 1))
            { /* block id: 213 */
                int32_t * const ****l_622 = &l_222;
                int32_t ****l_629 = &g_135;
                uint32_t l_634 = 1UL;
                uint32_t l_678 = 0xE4970F08L;
                for (l_161 = 0; (l_161 >= 10); l_161++)
                { /* block id: 216 */
                    int8_t l_619[5];
                    int32_t *****l_630 = &l_629;
                    int16_t *l_632[6][6][7] = {{{&l_350,&g_271,&g_271,&l_350,&g_271,&l_350,&g_271},{&g_271,&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271},{&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271,&g_271},{&l_350,&g_271,&l_350,&g_271,&g_271,&l_350,&g_271},{&g_271,&g_271,(void*)0,(void*)0,&g_271,&g_271,&g_271},{&l_350,&g_271,&g_271,&l_350,&g_271,&l_350,&g_271}},{{&g_271,&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271},{&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271,&g_271},{&l_350,&g_271,&l_350,&g_271,&g_271,&l_350,&g_271},{&g_271,&g_271,(void*)0,(void*)0,&g_271,&g_271,&g_271},{&l_350,&g_271,&g_271,&l_350,&g_271,&l_350,&g_271},{&g_271,&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271}},{{&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271,&g_271},{&l_350,&g_271,&l_350,&g_271,&g_271,&l_350,&g_271},{&g_271,&g_271,(void*)0,(void*)0,&g_271,&g_271,&g_271},{&l_350,&g_271,&g_271,&l_350,&g_271,&l_350,&g_271},{&g_271,&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271},{&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271,&g_271}},{{&l_350,&g_271,&l_350,&g_271,&g_271,&l_350,&g_271},{&g_271,&g_271,(void*)0,(void*)0,&g_271,&g_271,&g_271},{&l_350,&g_271,&g_271,&l_350,&g_271,&l_350,&g_271},{&g_271,&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271},{&g_271,&g_271,(void*)0,&g_271,&g_271,&g_271,&g_271},{(void*)0,&g_271,(void*)0,&l_350,&l_350,(void*)0,&g_271}},{{&l_350,&g_271,&g_271,&g_271,&g_271,&l_350,&g_271},{(void*)0,&l_350,&l_350,(void*)0,&g_271,(void*)0,&l_350},{&g_271,&g_271,&l_350,&g_271,&l_350,&g_271,&g_271},{&g_271,&l_350,&g_271,&l_350,&g_271,&g_271,&l_350},{(void*)0,&g_271,(void*)0,&l_350,&l_350,(void*)0,&g_271},{&l_350,&g_271,&g_271,&g_271,&g_271,&l_350,&g_271}},{{(void*)0,&l_350,&l_350,(void*)0,&g_271,(void*)0,&l_350},{&g_271,&g_271,&l_350,&g_271,&l_350,&g_271,&g_271},{&g_271,&l_350,&g_271,&l_350,&g_271,&g_271,&l_350},{(void*)0,&g_271,(void*)0,&l_350,&l_350,(void*)0,&g_271},{&l_350,&g_271,&g_271,&g_271,&g_271,&l_350,&g_271},{(void*)0,&l_350,&l_350,(void*)0,&g_271,(void*)0,&l_350}}};
                    int32_t l_633 = 0L;
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                        l_619[i] = (-6L);
                    if ((safe_mul_func_int16_t_s_s((l_633 |= ((safe_lshift_func_uint16_t_u_u((!(((((g_181.f5 ^ (((safe_mul_func_uint16_t_u_u((l_619[0] || (g_607.f0 || (safe_lshift_func_uint8_t_u_s(((((((l_622 != ((safe_rshift_func_int8_t_s_s((~0UL), 0)) , &l_222)) ^ (((l_619[4] <= (***l_244)) , ((safe_mul_func_uint16_t_u_u((~((g_560[0].f3 = (((*l_630) = l_629) == (void*)0)) | (****l_317))), 0x3DDBL)) ^ 0x25L)) ^ (***l_318))) || (*****l_630)) & l_631[8]) & (***l_245)) ^ g_560[0].f2), 4)))), 0x2DA9L)) , 0x0.Ep+1) , (***l_244))) > (*l_487)) || g_282) > (*g_371)) && g_181.f2)), (*l_105))) & g_5[1])), l_631[2])))
                    { /* block id: 220 */
                        (***l_629) = (g_556.f1 , &l_631[8]);
                        if ((***l_318))
                            break;
                        (**p_70) = (l_634 || ((*****l_630) | (((+(+(safe_unary_minus_func_int32_t_s((-3L))))) || (&g_271 != (void*)0)) >= 18446744073709551606UL)));
                    }
                    else
                    { /* block id: 224 */
                        uint16_t *l_649 = &g_36;
                        int32_t l_650 = 0x2BF58478L;
                        (*l_211) = ((((safe_lshift_func_uint8_t_u_s(((safe_lshift_func_uint16_t_u_u((safe_div_func_uint64_t_u_u(((!((func_25(((((safe_mul_func_uint16_t_u_u(((*l_649) = (safe_lshift_func_uint16_t_u_u(g_230, 1))), ((l_650 & (safe_lshift_func_uint8_t_u_u((*****l_630), ((((safe_mod_func_int32_t_s_s((0L ^ ((4294967293UL && (0L == ((safe_add_func_uint16_t_u_u((safe_unary_minus_func_int64_t_s(l_631[8])), ((g_39.f3 != (safe_rshift_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((**p_70), (*l_487))), 11))) < (*****l_630)))) <= l_664))) , (*g_76))), l_631[8])) > 1UL) <= 0x641FD031F44860BFLL) , g_560[0].f5)))) > (*g_76)))) ^ l_631[8]) > l_631[8]) != (*g_371)), (*l_487), (***l_223)) , l_631[8]) ^ (***l_223))) <= 1UL), 0x202E5EE9930A3E2ALL)), 15)) && (****l_629)), l_650)) || l_631[8]) || 18446744073709551608UL) , l_631[6]);
                    }
                    l_667 = &g_227;
                    return l_631[8];
                }
                (*l_211) ^= (safe_add_func_int32_t_s_s((**g_75), (safe_mod_func_int32_t_s_s(((***l_223) = ((g_673 == (((!g_79) >= g_580[0].f3) , (g_679 &= (safe_sub_func_uint8_t_u_u(((0x95D700B52A1D54D4LL > 0x47AC7FC2101C0BDFLL) | (***l_318)), (((((&g_271 != (((**l_622) == (void*)0) , l_677)) <= g_83[1][2][0]) == 0xCA08L) > l_678) , (****l_317))))))) & g_452)), g_83[3][1][8]))));
            }
            if ((0x93L ^ (((*l_606) = &g_485) == &g_485)))
            { /* block id: 236 */
                (*g_681) = (g_680 , &g_39);
            }
            else
            { /* block id: 238 */
                uint32_t l_685[6] = {0x7AA8DFF9L,0x596AFAEEL,0x596AFAEEL,0x7AA8DFF9L,0x596AFAEEL,0x596AFAEEL};
                uint64_t l_701 = 18446744073709551609UL;
                int32_t l_713[8][10][1] = {{{0x1A9E9C5BL},{0L},{0x28391DB4L},{0xD04F5C2DL},{1L},{9L},{(-6L)},{0xC27B5DD1L},{0xC27B5DD1L},{(-6L)}},{{9L},{1L},{0xD04F5C2DL},{0x28391DB4L},{0L},{0x1A9E9C5BL},{6L},{1L},{6L},{0x1A9E9C5BL}},{{0L},{0x28391DB4L},{0xD04F5C2DL},{1L},{9L},{(-6L)},{0xC27B5DD1L},{0xC27B5DD1L},{(-6L)},{9L}},{{1L},{0xD04F5C2DL},{0x28391DB4L},{0L},{0x1A9E9C5BL},{6L},{1L},{6L},{0x1A9E9C5BL},{0L}},{{0x28391DB4L},{0xD04F5C2DL},{1L},{9L},{(-6L)},{0xC27B5DD1L},{0xC27B5DD1L},{(-6L)},{9L},{1L}},{{0xD04F5C2DL},{0x28391DB4L},{0L},{0x1A9E9C5BL},{6L},{1L},{6L},{0x1A9E9C5BL},{0L},{0x28391DB4L}},{{0xD04F5C2DL},{1L},{9L},{(-6L)},{0xC27B5DD1L},{0xC27B5DD1L},{(-6L)},{9L},{1L},{0xD04F5C2DL}},{{0x28391DB4L},{0L},{0x1A9E9C5BL},{6L},{1L},{6L},{0x1A9E9C5BL},{0L},{0x28391DB4L},{0xD04F5C2DL}}};
                int i, j, k;
                (****l_317) = 1L;
                for (l_332 = (-4); (l_332 <= 1); l_332 = safe_add_func_uint8_t_u_u(l_332, 3))
                { /* block id: 242 */
                    (**g_135) = (**l_318);
                    if (l_685[5])
                        break;
                }
                l_715 = ((safe_mul_func_float_f_f(((***l_318) = g_181.f3), l_688)) != (safe_sub_func_float_f_f(((((safe_mul_func_float_f_f((safe_add_func_float_f_f(((safe_sub_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(((g_560[0].f2 | (-4L)) < (safe_add_func_int16_t_s_s((g_271 = ((((l_701 & ((safe_lshift_func_uint8_t_u_u((l_382 != ((safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_div_func_float_f_f((+0xC.FC72EEp-30), (safe_add_func_float_f_f((((l_713[0][0][0] = g_39.f0) == (g_569[6] = (-0x5.1p+1))) > g_104), 0x5.Ap+1)))), 0x9.2p+1)), g_560[0].f3)) , &l_701)), g_415.f0)) , 0xE8A46E4732835D78LL)) >= 0xB0L) , (****l_222)) , g_415.f2)), g_480))), l_685[5])), g_580[0].f2)) , g_415.f2), 0x0.Bp-1)), g_560[0].f2)) == l_685[5]) < 0xA.2DB309p-46) == l_714[1]), (-0x4.Bp-1))));
                (**l_224) = 0xCBA2B135L;
            }
        }
    }
    else
    { /* block id: 254 */
        volatile uint8_t * volatile **l_719 = &g_716;
        (*l_719) = g_716;
        (**g_135) = (*g_75);
        (*g_362) |= ((void*)0 != &g_134[5][7]);
    }
    return (*p_71);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_5[i], "g_5[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc_bytes (&g_39.f0, sizeof(g_39.f0), "g_39.f0", print_hash_value);
    transparent_crc(g_39.f1, "g_39.f1", print_hash_value);
    transparent_crc(g_39.f2, "g_39.f2", print_hash_value);
    transparent_crc(g_39.f3, "g_39.f3", print_hash_value);
    transparent_crc(g_79, "g_79", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_83[i][j][k], "g_83[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_87[i], "g_87[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc_bytes (&g_113.f0, sizeof(g_113.f0), "g_113.f0", print_hash_value);
    transparent_crc(g_113.f1, "g_113.f1", print_hash_value);
    transparent_crc(g_159, "g_159", print_hash_value);
    transparent_crc(g_181.f0, "g_181.f0", print_hash_value);
    transparent_crc(g_181.f1, "g_181.f1", print_hash_value);
    transparent_crc(g_181.f2, "g_181.f2", print_hash_value);
    transparent_crc(g_181.f3, "g_181.f3", print_hash_value);
    transparent_crc(g_181.f4, "g_181.f4", print_hash_value);
    transparent_crc(g_181.f5, "g_181.f5", print_hash_value);
    transparent_crc(g_181.f6, "g_181.f6", print_hash_value);
    transparent_crc_bytes (&g_208, sizeof(g_208), "g_208", print_hash_value);
    transparent_crc(g_213, "g_213", print_hash_value);
    transparent_crc(g_227.f0, "g_227.f0", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_228[i].f0, "g_228[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_230, "g_230", print_hash_value);
    transparent_crc(g_271, "g_271", print_hash_value);
    transparent_crc(g_278, "g_278", print_hash_value);
    transparent_crc(g_282, "g_282", print_hash_value);
    transparent_crc(g_379, "g_379", print_hash_value);
    transparent_crc(g_415.f0, "g_415.f0", print_hash_value);
    transparent_crc(g_415.f1, "g_415.f1", print_hash_value);
    transparent_crc(g_415.f2, "g_415.f2", print_hash_value);
    transparent_crc(g_415.f3, "g_415.f3", print_hash_value);
    transparent_crc(g_415.f4, "g_415.f4", print_hash_value);
    transparent_crc(g_415.f5, "g_415.f5", print_hash_value);
    transparent_crc(g_415.f6, "g_415.f6", print_hash_value);
    transparent_crc(g_452, "g_452", print_hash_value);
    transparent_crc(g_466, "g_466", print_hash_value);
    transparent_crc(g_480, "g_480", print_hash_value);
    transparent_crc_bytes (&g_485.f0, sizeof(g_485.f0), "g_485.f0", print_hash_value);
    transparent_crc(g_485.f1, "g_485.f1", print_hash_value);
    transparent_crc_bytes (&g_556.f0, sizeof(g_556.f0), "g_556.f0", print_hash_value);
    transparent_crc(g_556.f1, "g_556.f1", print_hash_value);
    transparent_crc(g_556.f2, "g_556.f2", print_hash_value);
    transparent_crc(g_556.f3, "g_556.f3", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_560[i].f0, "g_560[i].f0", print_hash_value);
        transparent_crc(g_560[i].f1, "g_560[i].f1", print_hash_value);
        transparent_crc(g_560[i].f2, "g_560[i].f2", print_hash_value);
        transparent_crc(g_560[i].f3, "g_560[i].f3", print_hash_value);
        transparent_crc(g_560[i].f4, "g_560[i].f4", print_hash_value);
        transparent_crc(g_560[i].f5, "g_560[i].f5", print_hash_value);
        transparent_crc(g_560[i].f6, "g_560[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc_bytes(&g_569[i], sizeof(g_569[i]), "g_569[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_580[i].f0, "g_580[i].f0", print_hash_value);
        transparent_crc(g_580[i].f1, "g_580[i].f1", print_hash_value);
        transparent_crc(g_580[i].f2, "g_580[i].f2", print_hash_value);
        transparent_crc(g_580[i].f3, "g_580[i].f3", print_hash_value);
        transparent_crc(g_580[i].f4, "g_580[i].f4", print_hash_value);
        transparent_crc(g_580[i].f5, "g_580[i].f5", print_hash_value);
        transparent_crc(g_580[i].f6, "g_580[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_607.f0, "g_607.f0", print_hash_value);
    transparent_crc(g_668.f0, "g_668.f0", print_hash_value);
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc(g_679, "g_679", print_hash_value);
    transparent_crc(g_680.f0, "g_680.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_718[i][j], "g_718[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_745.f0, "g_745.f0", print_hash_value);
    transparent_crc(g_891.f0, "g_891.f0", print_hash_value);
    transparent_crc(g_906.f0, "g_906.f0", print_hash_value);
    transparent_crc(g_924.f0, "g_924.f0", print_hash_value);
    transparent_crc(g_924.f1, "g_924.f1", print_hash_value);
    transparent_crc(g_924.f2, "g_924.f2", print_hash_value);
    transparent_crc(g_924.f3, "g_924.f3", print_hash_value);
    transparent_crc(g_924.f4, "g_924.f4", print_hash_value);
    transparent_crc(g_924.f5, "g_924.f5", print_hash_value);
    transparent_crc(g_924.f6, "g_924.f6", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_953[i], "g_953[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_979, "g_979", print_hash_value);
    transparent_crc(g_1002.f0, "g_1002.f0", print_hash_value);
    transparent_crc(g_1002.f1, "g_1002.f1", print_hash_value);
    transparent_crc(g_1002.f2, "g_1002.f2", print_hash_value);
    transparent_crc(g_1002.f3, "g_1002.f3", print_hash_value);
    transparent_crc(g_1002.f4, "g_1002.f4", print_hash_value);
    transparent_crc(g_1002.f5, "g_1002.f5", print_hash_value);
    transparent_crc(g_1002.f6, "g_1002.f6", print_hash_value);
    transparent_crc_bytes (&g_1008.f0, sizeof(g_1008.f0), "g_1008.f0", print_hash_value);
    transparent_crc(g_1008.f1, "g_1008.f1", print_hash_value);
    transparent_crc(g_1008.f2, "g_1008.f2", print_hash_value);
    transparent_crc(g_1008.f3, "g_1008.f3", print_hash_value);
    transparent_crc(g_1034, "g_1034", print_hash_value);
    transparent_crc_bytes (&g_1060.f0, sizeof(g_1060.f0), "g_1060.f0", print_hash_value);
    transparent_crc(g_1060.f1, "g_1060.f1", print_hash_value);
    transparent_crc(g_1066.f0, "g_1066.f0", print_hash_value);
    transparent_crc(g_1070, "g_1070", print_hash_value);
    transparent_crc_bytes (&g_1083.f0, sizeof(g_1083.f0), "g_1083.f0", print_hash_value);
    transparent_crc(g_1083.f1, "g_1083.f1", print_hash_value);
    transparent_crc(g_1083.f2, "g_1083.f2", print_hash_value);
    transparent_crc(g_1083.f3, "g_1083.f3", print_hash_value);
    transparent_crc(g_1103, "g_1103", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc_bytes(&g_1107[i].f0, sizeof(g_1107[i].f0), "g_1107[i].f0", print_hash_value);
        transparent_crc(g_1107[i].f1, "g_1107[i].f1", print_hash_value);
        transparent_crc(g_1107[i].f2, "g_1107[i].f2", print_hash_value);
        transparent_crc(g_1107[i].f3, "g_1107[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1108[i][j].f0, "g_1108[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1149.f0, "g_1149.f0", print_hash_value);
    transparent_crc(g_1175.f0, "g_1175.f0", print_hash_value);
    transparent_crc(g_1175.f1, "g_1175.f1", print_hash_value);
    transparent_crc(g_1175.f2, "g_1175.f2", print_hash_value);
    transparent_crc(g_1175.f3, "g_1175.f3", print_hash_value);
    transparent_crc(g_1175.f4, "g_1175.f4", print_hash_value);
    transparent_crc(g_1175.f5, "g_1175.f5", print_hash_value);
    transparent_crc(g_1175.f6, "g_1175.f6", print_hash_value);
    transparent_crc(g_1181.f0, "g_1181.f0", print_hash_value);
    transparent_crc(g_1181.f1, "g_1181.f1", print_hash_value);
    transparent_crc(g_1181.f2, "g_1181.f2", print_hash_value);
    transparent_crc(g_1181.f3, "g_1181.f3", print_hash_value);
    transparent_crc(g_1181.f4, "g_1181.f4", print_hash_value);
    transparent_crc(g_1181.f5, "g_1181.f5", print_hash_value);
    transparent_crc(g_1181.f6, "g_1181.f6", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_1209[i][j][k], "g_1209[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1215, "g_1215", print_hash_value);
    transparent_crc(g_1349.f0, "g_1349.f0", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1361[i], "g_1361[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1364.f0, "g_1364.f0", print_hash_value);
    transparent_crc(g_1374, "g_1374", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1421[i], "g_1421[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1429.f0, "g_1429.f0", print_hash_value);
    transparent_crc(g_1462.f0, "g_1462.f0", print_hash_value);
    transparent_crc(g_1462.f1, "g_1462.f1", print_hash_value);
    transparent_crc(g_1462.f2, "g_1462.f2", print_hash_value);
    transparent_crc(g_1462.f3, "g_1462.f3", print_hash_value);
    transparent_crc(g_1462.f4, "g_1462.f4", print_hash_value);
    transparent_crc(g_1462.f5, "g_1462.f5", print_hash_value);
    transparent_crc(g_1462.f6, "g_1462.f6", print_hash_value);
    transparent_crc(g_1530, "g_1530", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc_bytes(&g_1534[i][j].f0, sizeof(g_1534[i][j].f0), "g_1534[i][j].f0", print_hash_value);
            transparent_crc(g_1534[i][j].f1, "g_1534[i][j].f1", print_hash_value);
            transparent_crc(g_1534[i][j].f2, "g_1534[i][j].f2", print_hash_value);
            transparent_crc(g_1534[i][j].f3, "g_1534[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1538, "g_1538", print_hash_value);
    transparent_crc(g_1541.f0, "g_1541.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1574[i].f0, "g_1574[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1585.f0, "g_1585.f0", print_hash_value);
    transparent_crc(g_1607.f0, "g_1607.f0", print_hash_value);
    transparent_crc(g_1673, "g_1673", print_hash_value);
    transparent_crc(g_1678, "g_1678", print_hash_value);
    transparent_crc(g_1691.f0, "g_1691.f0", print_hash_value);
    transparent_crc(g_1691.f1, "g_1691.f1", print_hash_value);
    transparent_crc(g_1691.f2, "g_1691.f2", print_hash_value);
    transparent_crc(g_1691.f3, "g_1691.f3", print_hash_value);
    transparent_crc(g_1691.f4, "g_1691.f4", print_hash_value);
    transparent_crc(g_1691.f5, "g_1691.f5", print_hash_value);
    transparent_crc(g_1691.f6, "g_1691.f6", print_hash_value);
    transparent_crc(g_1692.f0, "g_1692.f0", print_hash_value);
    transparent_crc(g_1692.f1, "g_1692.f1", print_hash_value);
    transparent_crc(g_1692.f2, "g_1692.f2", print_hash_value);
    transparent_crc(g_1692.f3, "g_1692.f3", print_hash_value);
    transparent_crc(g_1692.f4, "g_1692.f4", print_hash_value);
    transparent_crc(g_1692.f5, "g_1692.f5", print_hash_value);
    transparent_crc(g_1692.f6, "g_1692.f6", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1727[i], "g_1727[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1731.f0, "g_1731.f0", print_hash_value);
    transparent_crc(g_1731.f1, "g_1731.f1", print_hash_value);
    transparent_crc(g_1731.f2, "g_1731.f2", print_hash_value);
    transparent_crc(g_1731.f3, "g_1731.f3", print_hash_value);
    transparent_crc(g_1731.f4, "g_1731.f4", print_hash_value);
    transparent_crc(g_1731.f5, "g_1731.f5", print_hash_value);
    transparent_crc(g_1731.f6, "g_1731.f6", print_hash_value);
    transparent_crc(g_1737, "g_1737", print_hash_value);
    transparent_crc(g_1773.f0, "g_1773.f0", print_hash_value);
    transparent_crc(g_1773.f1, "g_1773.f1", print_hash_value);
    transparent_crc(g_1773.f2, "g_1773.f2", print_hash_value);
    transparent_crc(g_1773.f3, "g_1773.f3", print_hash_value);
    transparent_crc(g_1773.f4, "g_1773.f4", print_hash_value);
    transparent_crc(g_1773.f5, "g_1773.f5", print_hash_value);
    transparent_crc(g_1773.f6, "g_1773.f6", print_hash_value);
    transparent_crc_bytes (&g_1799.f0, sizeof(g_1799.f0), "g_1799.f0", print_hash_value);
    transparent_crc(g_1799.f1, "g_1799.f1", print_hash_value);
    transparent_crc(g_1818.f0, "g_1818.f0", print_hash_value);
    transparent_crc(g_1818.f1, "g_1818.f1", print_hash_value);
    transparent_crc(g_1818.f2, "g_1818.f2", print_hash_value);
    transparent_crc(g_1818.f3, "g_1818.f3", print_hash_value);
    transparent_crc(g_1818.f4, "g_1818.f4", print_hash_value);
    transparent_crc(g_1818.f5, "g_1818.f5", print_hash_value);
    transparent_crc(g_1818.f6, "g_1818.f6", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc_bytes(&g_1851[i], sizeof(g_1851[i]), "g_1851[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1881[i][j].f0, "g_1881[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_1958.f0, sizeof(g_1958.f0), "g_1958.f0", print_hash_value);
    transparent_crc(g_1958.f1, "g_1958.f1", print_hash_value);
    transparent_crc(g_1988, "g_1988", print_hash_value);
    transparent_crc(g_1994.f0, "g_1994.f0", print_hash_value);
    transparent_crc_bytes (&g_2000.f0, sizeof(g_2000.f0), "g_2000.f0", print_hash_value);
    transparent_crc(g_2000.f1, "g_2000.f1", print_hash_value);
    transparent_crc(g_2021.f0, "g_2021.f0", print_hash_value);
    transparent_crc(g_2042.f0, "g_2042.f0", print_hash_value);
    transparent_crc(g_2042.f1, "g_2042.f1", print_hash_value);
    transparent_crc(g_2042.f2, "g_2042.f2", print_hash_value);
    transparent_crc(g_2042.f3, "g_2042.f3", print_hash_value);
    transparent_crc(g_2042.f4, "g_2042.f4", print_hash_value);
    transparent_crc(g_2042.f5, "g_2042.f5", print_hash_value);
    transparent_crc(g_2042.f6, "g_2042.f6", print_hash_value);
    transparent_crc(g_2077, "g_2077", print_hash_value);
    transparent_crc(g_2079, "g_2079", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 572
   depth: 1, occurrence: 20
XXX total union variables: 20

XXX non-zero bitfields defined in structs: 6
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 4
XXX structs with bitfields in the program: 24
breakdown:
   indirect level: 0, occurrence: 20
   indirect level: 1, occurrence: 2
   indirect level: 2, occurrence: 2
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 9
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 23
XXX times a single bitfield on LHS: 1
XXX times a single bitfield on RHS: 69

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 181
   depth: 2, occurrence: 35
   depth: 3, occurrence: 3
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
   depth: 6, occurrence: 5
   depth: 7, occurrence: 1
   depth: 8, occurrence: 2
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 23, occurrence: 4
   depth: 24, occurrence: 2
   depth: 29, occurrence: 2
   depth: 31, occurrence: 1
   depth: 33, occurrence: 3
   depth: 37, occurrence: 3
   depth: 38, occurrence: 1
   depth: 40, occurrence: 2
   depth: 42, occurrence: 1

XXX total number of pointers: 433

XXX times a variable address is taken: 1212
XXX times a pointer is dereferenced on RHS: 287
breakdown:
   depth: 1, occurrence: 146
   depth: 2, occurrence: 63
   depth: 3, occurrence: 38
   depth: 4, occurrence: 36
   depth: 5, occurrence: 4
XXX times a pointer is dereferenced on LHS: 277
breakdown:
   depth: 1, occurrence: 236
   depth: 2, occurrence: 25
   depth: 3, occurrence: 12
   depth: 4, occurrence: 4
XXX times a pointer is compared with null: 39
XXX times a pointer is compared with address of another variable: 12
XXX times a pointer is compared with another pointer: 15
XXX times a pointer is qualified to be dereferenced: 9415

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1127
   level: 2, occurrence: 347
   level: 3, occurrence: 152
   level: 4, occurrence: 88
   level: 5, occurrence: 29
XXX number of pointers point to pointers: 186
XXX number of pointers point to scalars: 223
XXX number of pointers point to structs: 4
XXX percent of pointers has null in alias set: 26.1
XXX average alias set size: 1.53

XXX times a non-volatile is read: 1751
XXX times a non-volatile is write: 838
XXX times a volatile is read: 142
XXX    times read thru a pointer: 40
XXX times a volatile is write: 14
XXX    times written thru a pointer: 2
XXX times a volatile is available for access: 5.47e+03
XXX percentage of non-volatile access: 94.3

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 168
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 27
   depth: 1, occurrence: 25
   depth: 2, occurrence: 19
   depth: 3, occurrence: 29
   depth: 4, occurrence: 28
   depth: 5, occurrence: 40

XXX percentage a fresh-made variable is used: 17
XXX percentage an existing variable is used: 83
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


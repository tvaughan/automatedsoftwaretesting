/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2728591761
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 0x2DB58E46L;
static uint64_t g_4[2][3] = {{18446744073709551615UL,0x90BE4438BEA12209LL,18446744073709551615UL},{18446744073709551615UL,0x90BE4438BEA12209LL,18446744073709551615UL}};
static int32_t g_38 = 0x9D66DE6BL;
static volatile int64_t g_43[2] = {1L,1L};
static volatile uint16_t g_44 = 0xECC3L;/* VOLATILE GLOBAL g_44 */
static volatile uint8_t g_52[2][1][4] = {{{246UL,246UL,246UL,246UL}},{{246UL,246UL,246UL,246UL}}};
static int8_t g_72 = 0x4EL;
static uint8_t g_98 = 248UL;
static volatile int32_t * volatile *g_101 = (void*)0;
static uint32_t g_117 = 4294967295UL;
static int8_t g_135 = 0L;
static uint16_t g_142 = 65530UL;
static uint16_t g_150 = 0xA24DL;
static uint16_t g_152[3] = {65535UL,65535UL,65535UL};
static int32_t g_164 = (-3L);
static uint32_t g_165 = 0x7E3D8B0AL;
static uint64_t g_168[4][5][2] = {{{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL}},{{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL}},{{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL}},{{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL},{0x26EC4865E95B6698LL,18446744073709551615UL}}};
static int32_t *g_171[1] = {&g_164};
static uint8_t g_182[10][10] = {{0x2DL,1UL,253UL,253UL,1UL,0x2DL,253UL,8UL,0x9AL,3UL},{0x2DL,253UL,8UL,0x9AL,3UL,0x2DL,0x90L,0x3DL,0x9CL,4UL},{0x2DL,0x90L,0x3DL,0x9CL,4UL,0x2DL,4UL,0x9CL,0x3DL,0x90L},{0x2DL,4UL,0x9CL,0x3DL,0x90L,0x2DL,3UL,0x9AL,8UL,253UL},{0x2DL,3UL,0x9AL,8UL,253UL,0x2DL,1UL,253UL,253UL,1UL},{0x2DL,1UL,253UL,253UL,1UL,0x2DL,253UL,8UL,0x9AL,3UL},{0x2DL,253UL,8UL,0x9AL,3UL,0x2DL,0x90L,0x3DL,0x9CL,4UL},{0x2DL,0x90L,0x3DL,0x9CL,4UL,0x2DL,4UL,0x9CL,0x3DL,0x90L},{0x2DL,4UL,0x9CL,0x3DL,0x90L,0x2DL,3UL,0x9AL,8UL,253UL},{0x2DL,3UL,0x9AL,8UL,253UL,0x2DL,1UL,253UL,253UL,1UL}};
static int32_t **g_253 = &g_171[0];
static float g_255 = 0xD.4AFBE2p+63;
static int64_t g_283[1][10][1] = {{{(-1L)},{(-4L)},{0L},{0L},{(-4L)},{(-1L)},{(-4L)},{0L},{0L},{(-4L)}}};
static float g_293 = 0x6.14B37Dp-35;
static volatile uint64_t g_303 = 0x2CC83618A206A817LL;/* VOLATILE GLOBAL g_303 */
static volatile uint64_t *g_302 = &g_303;
static volatile uint64_t **g_301 = &g_302;
static volatile int8_t g_316 = 1L;/* VOLATILE GLOBAL g_316 */
static volatile int8_t *g_315 = &g_316;
static volatile int8_t * volatile * const g_314[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint32_t g_318 = 0x6931B3F7L;
static uint32_t g_321 = 1UL;
static int32_t ** volatile g_337[7][6] = {{&g_171[0],&g_171[0],&g_171[0],&g_171[0],&g_171[0],(void*)0},{&g_171[0],&g_171[0],&g_171[0],&g_171[0],&g_171[0],&g_171[0]},{&g_171[0],&g_171[0],(void*)0,&g_171[0],&g_171[0],&g_171[0]},{&g_171[0],&g_171[0],&g_171[0],&g_171[0],&g_171[0],(void*)0},{&g_171[0],&g_171[0],&g_171[0],&g_171[0],&g_171[0],&g_171[0]},{&g_171[0],&g_171[0],(void*)0,&g_171[0],&g_171[0],&g_171[0]},{&g_171[0],&g_171[0],&g_171[0],&g_171[0],&g_171[0],(void*)0}};
static float *g_347 = (void*)0;
static const int32_t g_366 = 0L;
static const float g_414[1][10][6] = {{{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62},{0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62,0x3.8F4521p+62}}};
static const float *g_413 = &g_414[0][6][2];
static int32_t ***g_435 = (void*)0;
static int32_t g_461[7][7][5] = {{{0x664490FDL,0xFC3B148FL,7L,7L,0x70A8FE62L},{0xFC3B148FL,0x7E5C937FL,7L,1L,7L},{0x70A8FE62L,0x70A8FE62L,0x7E5C937FL,0x664490FDL,1L},{0xFC3B148FL,1L,0xA50E7086L,0x664490FDL,0x664490FDL},{0x664490FDL,0x0B0CA8C5L,0x664490FDL,1L,1L},{0xA50E7086L,1L,0xFC3B148FL,7L,1L},{0x7E5C937FL,0x70A8FE62L,0x70A8FE62L,0x7E5C937FL,0x664490FDL}},{{7L,0x7E5C937FL,0xFC3B148FL,1L,1L},{7L,0xFC3B148FL,0x664490FDL,0xFC3B148FL,7L},{0x7E5C937FL,7L,0xA50E7086L,1L,0x70A8FE62L},{0xA50E7086L,7L,0x7E5C937FL,0x7E5C937FL,7L},{0x664490FDL,0xFC3B148FL,7L,7L,0x70A8FE62L},{0xFC3B148FL,0x7E5C937FL,7L,1L,7L},{0x70A8FE62L,0x70A8FE62L,0x7E5C937FL,0x664490FDL,1L}},{{0xFC3B148FL,1L,0xA50E7086L,0x664490FDL,0x664490FDL},{0x664490FDL,0x0B0CA8C5L,0x664490FDL,1L,1L},{0xA50E7086L,1L,0xFC3B148FL,7L,1L},{0x7E5C937FL,0x70A8FE62L,0x70A8FE62L,0x7E5C937FL,0x664490FDL},{7L,0x7E5C937FL,0xFC3B148FL,1L,1L},{7L,0xFC3B148FL,0x664490FDL,0xFC3B148FL,7L},{0x7E5C937FL,7L,0xA50E7086L,1L,0x70A8FE62L}},{{0xA50E7086L,7L,0x7E5C937FL,0x7E5C937FL,7L},{0x664490FDL,0xFC3B148FL,7L,7L,0x70A8FE62L},{0xFC3B148FL,0x7E5C937FL,7L,1L,7L},{0x70A8FE62L,0x70A8FE62L,0x7E5C937FL,0x664490FDL,1L},{0xFC3B148FL,1L,0xA50E7086L,0x664490FDL,0x664490FDL},{0x664490FDL,0x0B0CA8C5L,0x664490FDL,1L,1L},{0xA50E7086L,1L,0xFC3B148FL,7L,1L}},{{0x7E5C937FL,0x70A8FE62L,0x70A8FE62L,0x7E5C937FL,0x664490FDL},{7L,0x70A8FE62L,0x0B0CA8C5L,1L,7L},{0xA50E7086L,0x0B0CA8C5L,7L,0x0B0CA8C5L,0xA50E7086L},{0x70A8FE62L,0x7E5C937FL,0x664490FDL,1L,1L},{0x664490FDL,0x7E5C937FL,0x70A8FE62L,0x70A8FE62L,0x7E5C937FL},{7L,0x0B0CA8C5L,0xA50E7086L,0x7E5C937FL,1L},{0x0B0CA8C5L,0x70A8FE62L,0xA50E7086L,0xFC3B148FL,0xA50E7086L}},{{1L,1L,0x70A8FE62L,7L,7L},{0x0B0CA8C5L,7L,0x664490FDL,7L,7L},{7L,1L,7L,0xFC3B148FL,1L},{0x664490FDL,7L,0x0B0CA8C5L,0x7E5C937FL,1L},{0x70A8FE62L,1L,1L,0x70A8FE62L,7L},{0xA50E7086L,0x70A8FE62L,0x0B0CA8C5L,1L,7L},{0xA50E7086L,0x0B0CA8C5L,7L,0x0B0CA8C5L,0xA50E7086L}},{{0x70A8FE62L,0x7E5C937FL,0x664490FDL,1L,1L},{0x664490FDL,0x7E5C937FL,0x70A8FE62L,0x70A8FE62L,0x7E5C937FL},{7L,0x0B0CA8C5L,0xA50E7086L,0x7E5C937FL,1L},{0x0B0CA8C5L,0x70A8FE62L,0xA50E7086L,0xFC3B148FL,0xA50E7086L},{1L,1L,0x70A8FE62L,7L,7L},{0x0B0CA8C5L,7L,0x664490FDL,7L,7L},{7L,1L,7L,0xFC3B148FL,1L}}};
static uint16_t g_485 = 0x8502L;
static uint64_t * volatile *g_500 = (void*)0;
static uint64_t * volatile **g_499 = &g_500;
static uint64_t * volatile *** volatile g_498 = &g_499;/* VOLATILE GLOBAL g_498 */
static uint64_t * volatile *** volatile *g_497 = &g_498;
static uint64_t *g_522 = &g_4[1][1];
static uint64_t **g_521[3][4] = {{&g_522,&g_522,&g_522,&g_522},{&g_522,&g_522,&g_522,&g_522},{&g_522,&g_522,&g_522,&g_522}};
static uint64_t ***g_520 = &g_521[0][3];
static uint64_t g_530 = 0x06B26CD83A0382D5LL;
static uint32_t *g_533 = &g_117;
static uint32_t * const *g_532 = &g_533;
static uint32_t * const * volatile * volatile g_531[6][9][4] = {{{&g_532,(void*)0,&g_532,(void*)0},{(void*)0,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{(void*)0,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,(void*)0,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{(void*)0,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532}},{{(void*)0,&g_532,&g_532,(void*)0},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,(void*)0,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,(void*)0,&g_532,&g_532}},{{(void*)0,(void*)0,&g_532,&g_532},{(void*)0,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,(void*)0,(void*)0,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{(void*)0,&g_532,&g_532,&g_532},{(void*)0,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532}},{{&g_532,&g_532,&g_532,&g_532},{&g_532,(void*)0,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,(void*)0,&g_532,(void*)0}},{{(void*)0,&g_532,&g_532,&g_532},{(void*)0,&g_532,&g_532,&g_532},{&g_532,(void*)0,&g_532,&g_532},{&g_532,&g_532,(void*)0,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{(void*)0,&g_532,(void*)0,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532}},{{&g_532,&g_532,(void*)0,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,(void*)0,(void*)0,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,(void*)0},{&g_532,(void*)0,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532},{&g_532,&g_532,&g_532,&g_532}}};
static float * volatile g_658 = &g_255;/* VOLATILE GLOBAL g_658 */
static float g_681 = 0x9.4p-1;
static uint16_t g_740 = 0x4656L;
static int32_t * volatile g_816 = &g_164;/* VOLATILE GLOBAL g_816 */
static int32_t *g_817 = &g_38;
static uint64_t g_892[10][6] = {{0x477AA8DFF9745B5CLL,0x477AA8DFF9745B5CLL,1UL,8UL,1UL,0x477AA8DFF9745B5CLL},{1UL,18446744073709551610UL,8UL,8UL,18446744073709551610UL,1UL},{0x477AA8DFF9745B5CLL,1UL,8UL,1UL,0x477AA8DFF9745B5CLL,0x477AA8DFF9745B5CLL},{0x91DCF52A44E999A8LL,1UL,1UL,0x91DCF52A44E999A8LL,18446744073709551610UL,0x91DCF52A44E999A8LL},{0x91DCF52A44E999A8LL,18446744073709551610UL,0x91DCF52A44E999A8LL,1UL,1UL,0x91DCF52A44E999A8LL},{0x477AA8DFF9745B5CLL,0x477AA8DFF9745B5CLL,1UL,8UL,1UL,0x477AA8DFF9745B5CLL},{1UL,18446744073709551610UL,8UL,8UL,18446744073709551610UL,1UL},{0x477AA8DFF9745B5CLL,1UL,8UL,1UL,0x477AA8DFF9745B5CLL,0x477AA8DFF9745B5CLL},{0x91DCF52A44E999A8LL,1UL,1UL,0x91DCF52A44E999A8LL,18446744073709551610UL,0x91DCF52A44E999A8LL},{0x91DCF52A44E999A8LL,0x477AA8DFF9745B5CLL,8UL,0x91DCF52A44E999A8LL,0x91DCF52A44E999A8LL,8UL}};
static uint32_t **g_932 = &g_533;
static int16_t g_936 = (-1L);
static int32_t g_958 = 6L;
static int64_t **g_971 = (void*)0;
static uint8_t * const  volatile ** volatile g_972 = (void*)0;/* VOLATILE GLOBAL g_972 */
static uint8_t * const  volatile ** volatile * volatile g_973 = &g_972;/* VOLATILE GLOBAL g_973 */
static int16_t g_1004 = 7L;
static int32_t * const g_1033 = &g_38;
static int64_t *** const  volatile g_1037 = (void*)0;/* VOLATILE GLOBAL g_1037 */
static uint32_t g_1076 = 4294967295UL;
static float g_1104 = 0x1.3AA117p-69;
static float g_1105[3] = {0xA.429079p-78,0xA.429079p-78,0xA.429079p-78};
static uint32_t g_1168[4] = {4294967292UL,4294967292UL,4294967292UL,4294967292UL};
static volatile uint32_t g_1185 = 0UL;/* VOLATILE GLOBAL g_1185 */
static volatile uint32_t g_1186 = 4294967295UL;/* VOLATILE GLOBAL g_1186 */
static volatile uint32_t g_1187[7][9] = {{9UL,0x60A9D6EDL,0x75280704L,9UL,0UL,0UL,9UL,0x75280704L,0x60A9D6EDL},{0xF57A53CEL,4294967295UL,4294967295UL,0xF57A53CEL,0x91636CCBL,0x91636CCBL,0xF57A53CEL,4294967295UL,4294967295UL},{9UL,0x60A9D6EDL,0x75280704L,9UL,0UL,0UL,9UL,0x75280704L,0x60A9D6EDL},{0xF57A53CEL,4294967295UL,4294967295UL,0xF57A53CEL,0x91636CCBL,0x91636CCBL,0xF57A53CEL,4294967295UL,4294967295UL},{9UL,0x60A9D6EDL,0x75280704L,9UL,0UL,0UL,9UL,0x75280704L,0x60A9D6EDL},{0xF57A53CEL,4294967295UL,4294967295UL,0xF57A53CEL,0x91636CCBL,0x91636CCBL,0xF57A53CEL,4294967295UL,4294967295UL},{9UL,0x60A9D6EDL,0x75280704L,9UL,0UL,0UL,9UL,0x75280704L,0x60A9D6EDL}};
static volatile uint32_t *g_1184[9] = {&g_1186,&g_1185,&g_1185,&g_1186,&g_1185,&g_1185,&g_1186,&g_1185,&g_1185};
static volatile uint32_t * const * volatile g_1183 = &g_1184[4];/* VOLATILE GLOBAL g_1183 */
static volatile uint32_t * const * volatile *g_1182 = &g_1183;
static volatile uint32_t * const * volatile **g_1181 = &g_1182;
static const uint32_t g_1219 = 0x1590A421L;
static uint8_t g_1236[6][9][4] = {{{0x52L,0x98L,0x8CL,3UL},{0x79L,9UL,0x52L,0x01L},{0x98L,0xD2L,0x7DL,0x8BL},{3UL,0x39L,9UL,4UL},{4UL,1UL,1UL,0x4DL},{9UL,0xCBL,0xFCL,0x90L},{253UL,0x7DL,0x79L,2UL},{0x52L,7UL,9UL,0UL},{0x40L,0x18L,0xCBL,0x9AL}},{{0xC6L,0x78L,0x9AL,0x8BL},{9UL,0UL,0x56L,0UL},{252UL,0x98L,246UL,0x98L},{0x8CL,0xE0L,0xFCL,9UL},{0x98L,0x63L,0x40L,253UL},{0xCBL,0xC6L,0x39L,4UL},{0xCBL,0x18L,0x40L,0x60L},{0x98L,4UL,0xFCL,0x84L},{0x8CL,0x53L,246UL,247UL}},{{252UL,7UL,0x56L,0x4DL},{9UL,0UL,0x9AL,2UL},{0xC6L,0xBAL,0xCBL,253UL},{0x40L,0UL,9UL,0UL},{0x52L,9UL,0x79L,0x24L},{253UL,3UL,0xFCL,0x01L},{9UL,0UL,1UL,0x63L},{4UL,0xC6L,9UL,7UL},{3UL,0UL,0x7DL,0UL}},{{0x7DL,0x78L,0UL,0xE0L},{0UL,1UL,255UL,0x24L},{0UL,246UL,249UL,0x5BL},{0x79L,0x9AL,9UL,9UL},{0xA4L,0xA4L,0x39L,0x01L},{0x8BL,0x52L,0x53L,0xCBL},{1UL,249UL,9UL,0x53L},{0UL,249UL,0UL,0xCBL},{249UL,0x52L,0x26L,0x01L}},{{0x78L,0xA4L,247UL,9UL},{0xBAL,0x9AL,246UL,0x5BL},{1UL,246UL,9UL,0x24L},{0x01L,1UL,0x18L,0xE0L},{1UL,0x78L,0UL,0UL},{0x79L,4UL,0x78L,0xBAL},{247UL,3UL,246UL,0x01L},{0x63L,0x36L,249UL,2UL},{0x40L,0xF8L,9UL,0UL}},{{255UL,1UL,9UL,0x9AL},{0xF8L,0x52L,0x8CL,0UL},{246UL,247UL,0x90L,0x39L},{0xBAL,4UL,0x63L,0x63L},{249UL,0xBAL,1UL,0x24L},{0UL,7UL,0UL,0x56L},{0x40L,1UL,0UL,0x5BL},{0x26L,0x1EL,0x39L,1UL},{0x84L,3UL,0x39L,0UL}}};
static volatile uint8_t g_1243[3] = {0x89L,0x89L,0x89L};
static uint64_t ****g_1259 = (void*)0;
static uint16_t g_1271 = 8UL;
static volatile int16_t g_1327[3] = {1L,1L,1L};
static float * volatile g_1344 = (void*)0;/* VOLATILE GLOBAL g_1344 */
static uint32_t * volatile g_1364 = (void*)0;/* VOLATILE GLOBAL g_1364 */
static uint32_t * volatile * volatile g_1363[2][6][7] = {{{&g_1364,(void*)0,(void*)0,&g_1364,&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364,&g_1364,&g_1364,&g_1364,(void*)0},{&g_1364,&g_1364,&g_1364,(void*)0,(void*)0,&g_1364,&g_1364},{&g_1364,(void*)0,&g_1364,&g_1364,&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364,&g_1364,&g_1364,(void*)0,(void*)0},{&g_1364,&g_1364,&g_1364,&g_1364,&g_1364,&g_1364,(void*)0}},{{(void*)0,(void*)0,&g_1364,(void*)0,&g_1364,(void*)0,(void*)0},{&g_1364,(void*)0,(void*)0,&g_1364,(void*)0,&g_1364,(void*)0},{(void*)0,(void*)0,&g_1364,&g_1364,&g_1364,&g_1364,&g_1364},{&g_1364,(void*)0,(void*)0,&g_1364,&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364,&g_1364,&g_1364,&g_1364,&g_1364},{&g_1364,(void*)0,(void*)0,&g_1364,&g_1364,(void*)0,(void*)0}}};


/* --- FORWARD DECLARATIONS --- */
static const int64_t  func_1(void);
static int32_t  func_9(int32_t * p_10, uint16_t  p_11, int32_t  p_12, int64_t  p_13);
static int32_t * func_14(int32_t * const  p_15);
static int32_t * func_16(int32_t * p_17, int32_t * p_18, const int32_t * p_19, int32_t * p_20);
static int32_t * func_21(int32_t * const  p_22, int32_t  p_23, uint16_t  p_24, const int16_t  p_25, const int32_t * p_26);
static int32_t * func_27(uint8_t  p_28, int32_t * p_29);
static uint8_t  func_30(int32_t  p_31, int32_t  p_32, int8_t  p_33);
static int32_t * func_35(uint8_t  p_36);
static uint8_t  func_80(int64_t  p_81, int32_t * p_82, int32_t  p_83, uint32_t  p_84);
static float  func_85(int32_t ** p_86, int8_t  p_87, int32_t  p_88);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_461 g_1076 g_658 g_255 g_413 g_414 g_1183 g_1184 g_1187 g_1186 g_1185 g_817 g_38 g_253 g_171 g_164 g_3 g_1236 g_1243 g_318 g_520 g_521 g_522 g_1033 g_533 g_117 g_936 g_135 g_530 g_892 g_315 g_316 g_971 g_1219 g_1271 g_150 g_301 g_302 g_303 g_932 g_1168 g_98 g_142 g_1327 g_1004 g_485 g_1363 g_958 g_44 g_321 g_52
 * writes: g_4 g_3 g_1105 g_936 g_892 g_38 g_1259 g_135 g_182 g_117 g_1004 g_164 g_171 g_165 g_142 g_485 g_44 g_52 g_255
 */
static const int64_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_2[5] = {&g_3,&g_3,&g_3,&g_3,&g_3};
    int32_t l_833 = (-1L);
    uint32_t *l_840 = &g_165;
    uint32_t **l_839 = &l_840;
    uint32_t *l_890 = &g_318;
    const uint32_t *l_891[10][6][1] = {{{&g_321},{&g_321},{&g_321},{&g_117},{&g_321},{&g_318}},{{&g_321},{&g_117},{&g_321},{&g_321},{&g_321},{&g_117}},{{&g_321},{&g_321},{&g_321},{&g_117},{&g_321},{&g_318}},{{&g_321},{&g_117},{&g_321},{&g_321},{&g_321},{&g_117}},{{&g_321},{&g_321},{&g_321},{&g_117},{&g_321},{&g_318}},{{&g_321},{&g_117},{&g_321},{&g_321},{&g_321},{&g_117}},{{&g_321},{&g_321},{&g_321},{&g_117},{&g_321},{&g_318}},{{&g_321},{&g_117},{&g_321},{&g_321},{&g_321},{&g_117}},{{&g_321},{&g_321},{&g_321},{&g_117},{&g_321},{&g_318}},{{&g_321},{&g_117},{&g_321},{&g_321},{&g_321},{&g_117}}};
    int32_t l_893[7];
    int32_t l_906 = (-1L);
    const int8_t l_922 = 0L;
    int64_t l_929 = 0xD37238BC2E5ED6E2LL;
    uint32_t **l_933 = &g_533;
    int64_t *l_970 = &l_929;
    int64_t **l_969 = &l_970;
    int32_t l_993 = (-5L);
    uint64_t l_994 = 18446744073709551615UL;
    uint8_t *l_1000 = &g_182[4][9];
    uint8_t **l_999 = &l_1000;
    uint8_t *** const l_998 = &l_999;
    uint8_t l_1003[3];
    int8_t l_1007 = 1L;
    int16_t l_1013 = (-1L);
    uint8_t ***l_1022 = &l_999;
    uint8_t **l_1047 = &l_1000;
    uint64_t ****l_1139 = (void*)0;
    int32_t l_1175 = (-1L);
    int32_t **l_1203[9] = {(void*)0,(void*)0,&l_2[0],(void*)0,(void*)0,&l_2[0],(void*)0,(void*)0,&l_2[0]};
    int8_t *l_1213 = &g_135;
    int8_t **l_1212 = &l_1213;
    int8_t ***l_1211[7] = {(void*)0,(void*)0,&l_1212,(void*)0,(void*)0,&l_1212,(void*)0};
    float l_1215 = 0x7.E59354p+68;
    int32_t l_1240 = 1L;
    uint32_t l_1247 = 0xDB8493B7L;
    int8_t l_1295 = 0x32L;
    int32_t l_1315 = (-8L);
    int32_t l_1328 = (-1L);
    int64_t l_1332 = 0L;
    uint32_t l_1389 = 0x3AED6C5FL;
    uint32_t l_1391 = 0UL;
    const uint32_t l_1392 = 0x5F6D9192L;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_893[i] = (-1L);
    for (i = 0; i < 3; i++)
        l_1003[i] = 248UL;
lbl_1221:
    g_4[1][1]--;
lbl_1298:
    for (g_3 = (-17); (g_3 > (-16)); g_3++)
    { /* block id: 4 */
        float l_34 = 0x0.Bp+1;
        int32_t l_668 = (-1L);
        int32_t *l_669 = &g_461[4][0][1];
        int32_t l_818 = 0xE213A353L;
        int32_t **l_831 = &l_2[2];
        uint32_t **l_841[1];
        const float l_924 = 0xB.4B0A47p-58;
        int64_t l_957 = (-5L);
        int32_t l_982 = 6L;
        int32_t l_1005 = 0L;
        int32_t l_1006 = 1L;
        int32_t l_1008 = 0x8F04AA1DL;
        int32_t l_1009 = 0xA96E46B3L;
        int32_t l_1010 = 0L;
        int32_t l_1011[7][3];
        int8_t l_1012 = (-10L);
        uint32_t l_1014 = 4294967288UL;
        uint8_t * const *l_1020 = &l_1000;
        uint8_t * const **l_1019[8] = {(void*)0,&l_1020,&l_1020,(void*)0,&l_1020,&l_1020,(void*)0,&l_1020};
        uint8_t * const ***l_1021 = &l_1019[3];
        float l_1031 = (-0x5.Bp-1);
        int64_t l_1073 = 0L;
        float l_1158 = (-0x7.0p+1);
        int32_t l_1166 = 0x5D8A6654L;
        int i, j;
        for (i = 0; i < 1; i++)
            l_841[i] = &l_840;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 3; j++)
                l_1011[i][j] = (-1L);
        }
    }
    for (l_994 = 0; (l_994 > 47); l_994++)
    { /* block id: 468 */
        const int32_t *l_1200 = &l_893[3];
        const int32_t **l_1199[5][1] = {{&l_1200},{&l_1200},{&l_1200},{&l_1200},{&l_1200}};
        int32_t **l_1201[8] = {&l_2[0],&l_2[0],&l_2[0],&l_2[0],&l_2[0],&l_2[0],&l_2[0],&l_2[0]};
        int32_t ***l_1202 = (void*)0;
        int32_t ***l_1204[9][3] = {{&l_1201[4],&l_1201[3],&l_1201[4]},{&l_1201[4],&l_1201[4],(void*)0},{(void*)0,&l_1201[3],&l_1201[3]},{(void*)0,&l_1201[1],&l_1201[4]},{(void*)0,&l_1201[4],(void*)0},{&l_1201[4],(void*)0,&l_1201[4]},{&l_1201[4],&l_1201[4],&l_1201[3]},{&l_1201[4],(void*)0,(void*)0},{&l_1201[3],&l_1201[4],&l_1201[4]}};
        int32_t **l_1205[6] = {(void*)0,&g_171[0],&g_171[0],(void*)0,&g_171[0],&g_171[0]};
        int8_t *l_1210 = &l_1007;
        int8_t **l_1209 = &l_1210;
        int8_t ***l_1208 = &l_1209;
        float *l_1214 = &g_1105[0];
        const int32_t l_1216 = 0x70BE6741L;
        const uint32_t *l_1218 = &g_1219;
        const uint32_t **l_1217 = &l_1218;
        int64_t l_1220 = 0x18571480AD5E3A28LL;
        uint32_t l_1248[2];
        int8_t l_1272 = 0L;
        int8_t ***l_1309 = &l_1209;
        int16_t *l_1310 = (void*)0;
        int16_t *l_1311 = &l_1013;
        uint16_t *l_1312 = &g_740;
        int i, j;
        for (i = 0; i < 2; i++)
            l_1248[i] = 2UL;
        if ((((((safe_rshift_func_int16_t_s_s(g_461[4][6][3], g_1076)) | 0x86654D58L) < ((((*l_1217) = ((safe_div_func_uint32_t_u_u(((-6L) < (((((((*l_1214) = (safe_add_func_float_f_f(((l_1199[2][0] != (l_1205[2] = (l_1203[6] = l_1201[4]))) >= ((*g_658) != ((safe_add_func_float_f_f((*g_413), (l_1208 == l_1211[4]))) == (*g_413)))), (*g_413)))) < (*l_1200)) , (**g_1183)) ^ (*l_1200)) | 65533UL) <= (*g_817))), l_1216)) , &g_165)) == (void*)0) >= 18446744073709551612UL)) || l_1220) , (**g_253)))
        { /* block id: 473 */
            uint32_t l_1222 = 18446744073709551615UL;
            int16_t *l_1246[8][9][3] = {{{&g_1004,&g_1004,(void*)0},{&g_1004,(void*)0,(void*)0},{&g_1004,&g_1004,&g_1004},{&l_1013,(void*)0,&g_936},{&g_936,&g_1004,&g_1004},{&l_1013,(void*)0,(void*)0},{&g_1004,&g_1004,(void*)0},{&l_1013,(void*)0,&g_1004},{&g_1004,&g_1004,&g_1004}},{{&l_1013,(void*)0,&l_1013},{&g_936,&g_1004,&g_1004},{&l_1013,(void*)0,(void*)0},{&g_1004,&g_1004,(void*)0},{&g_1004,(void*)0,(void*)0},{&g_1004,&g_1004,&g_1004},{&l_1013,(void*)0,&g_1004},{&g_936,&l_1013,&l_1013},{(void*)0,&g_936,&l_1013}},{{(void*)0,&g_1004,(void*)0},{&g_936,&g_936,&l_1013},{&g_1004,&l_1013,&g_1004},{(void*)0,&g_936,&g_1004},{&g_936,&g_1004,&l_1013},{(void*)0,&g_936,&g_936},{(void*)0,&l_1013,(void*)0},{&l_1013,&g_936,&g_936},{&g_1004,&g_1004,&g_1004}},{{&g_1004,&g_936,&g_1004},{&g_936,&l_1013,&l_1013},{(void*)0,&g_936,&l_1013},{(void*)0,&g_1004,(void*)0},{&g_936,&g_936,&l_1013},{&g_1004,&l_1013,&g_1004},{(void*)0,&g_936,&g_1004},{&g_936,&g_1004,&l_1013},{(void*)0,&g_936,&g_936}},{{(void*)0,&l_1013,(void*)0},{&l_1013,&g_936,&g_936},{&g_1004,&g_1004,&g_1004},{&g_1004,&g_936,&g_1004},{&g_936,&l_1013,&l_1013},{(void*)0,&g_936,&l_1013},{(void*)0,&g_1004,(void*)0},{&g_936,&g_936,&l_1013},{&g_1004,&l_1013,&g_1004}},{{(void*)0,&g_936,&g_1004},{&g_936,&g_1004,&l_1013},{(void*)0,&g_936,&g_936},{(void*)0,&l_1013,(void*)0},{&l_1013,&g_936,&g_936},{&g_1004,&g_1004,&g_1004},{&g_1004,&g_936,&g_1004},{&g_936,&l_1013,&l_1013},{(void*)0,&g_936,&l_1013}},{{(void*)0,&g_1004,(void*)0},{&g_936,&g_936,&l_1013},{&g_1004,&l_1013,&g_1004},{(void*)0,&g_936,&g_1004},{&g_936,&g_1004,&l_1013},{(void*)0,&g_936,&g_936},{(void*)0,&l_1013,(void*)0},{&l_1013,&g_936,&g_936},{&g_1004,&g_1004,&g_1004}},{{&g_1004,&g_936,&g_1004},{&g_936,&l_1013,&l_1013},{(void*)0,&g_936,&l_1013},{(void*)0,&g_1004,(void*)0},{&g_936,&g_936,&l_1013},{&g_1004,&l_1013,&g_1004},{(void*)0,&g_936,&g_1004},{&g_936,&g_1004,&l_1013},{(void*)0,&g_936,&g_936}}};
            uint64_t *l_1249 = (void*)0;
            uint64_t *l_1250 = &g_892[8][5];
            uint8_t l_1251 = 0xEEL;
            uint64_t *****l_1258[3];
            int32_t l_1262 = 8L;
            int64_t **l_1268 = &l_970;
            int64_t ***l_1269[9] = {&g_971,&g_971,&g_971,&g_971,&g_971,&g_971,&g_971,&g_971,&g_971};
            int32_t l_1270 = 0L;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1258[i] = &l_1139;
            if (g_3)
                goto lbl_1221;
            --l_1222;
            (*g_817) = (safe_add_func_uint64_t_u_u(((safe_add_func_uint16_t_u_u((((*l_1250) = (safe_sub_func_int64_t_s_s(((l_1222 || (safe_mod_func_int32_t_s_s(((((safe_lshift_func_uint16_t_u_u(g_38, (+g_1236[0][7][0]))) != 255UL) | (safe_mul_func_int16_t_s_s((g_936 &= (((safe_unary_minus_func_uint16_t_u(l_1240)) || ((***g_520) |= ((safe_lshift_func_uint8_t_u_s(g_1243[2], g_318)) , l_1222))) | ((((safe_add_func_int32_t_s_s(l_1222, (*g_1033))) < 0x9FA0L) > l_1222) , (*g_533)))), 1L))) > l_1247), (*l_1200)))) > l_1248[1]), 18446744073709551609UL))) || l_1251), g_135)) == l_1251), l_1222));
            (*l_1214) = ((safe_lshift_func_uint8_t_u_s(g_530, 3)) , (((safe_mul_func_uint16_t_u_u((((safe_lshift_func_int16_t_s_s(((g_1259 = &g_520) == l_1139), 3)) > (g_892[1][1] & ((((safe_rshift_func_uint8_t_u_u(l_1262, 7)) < ((safe_rshift_func_uint8_t_u_u((l_1270 = (((safe_mod_func_uint8_t_u_u((+l_1262), (*g_315))) ^ ((l_969 = l_1268) != g_971)) | 0x3DL)), g_1219)) | g_1271)) & l_1272) , 0xD64BL))) | 65535UL), 0x9860L)) , &g_971) == (void*)0));
        }
        else
        { /* block id: 484 */
            int32_t l_1273 = 0x66C0C817L;
            int16_t *l_1296[8][4][6] = {{{&g_936,&l_1013,&l_1013,&l_1013,&g_1004,&l_1013},{(void*)0,&l_1013,&g_1004,&g_936,&g_936,&g_1004},{&g_936,&g_936,&g_1004,&g_936,&g_1004,&g_1004},{&g_1004,(void*)0,&g_1004,&g_1004,(void*)0,&l_1013}},{{(void*)0,&l_1013,&g_1004,&l_1013,&g_936,&g_936},{&l_1013,&g_936,&g_936,&l_1013,&g_1004,&l_1013},{(void*)0,&l_1013,(void*)0,&g_1004,&g_1004,(void*)0},{&g_1004,&g_1004,&g_1004,&g_936,&g_1004,&g_936}},{{(void*)0,&l_1013,(void*)0,(void*)0,&g_1004,(void*)0},{&g_1004,&g_936,&l_1013,&l_1013,&g_936,(void*)0},{(void*)0,&l_1013,(void*)0,&g_1004,(void*)0,&g_936},{&l_1013,(void*)0,&g_1004,&g_1004,&g_1004,(void*)0}},{{&l_1013,&g_936,(void*)0,&g_1004,(void*)0,&l_1013},{(void*)0,(void*)0,&g_936,&l_1013,&l_1013,&g_936},{&g_1004,(void*)0,&g_1004,(void*)0,(void*)0,&l_1013},{(void*)0,&g_936,&g_1004,&g_936,&g_1004,&g_1004}},{{&g_1004,(void*)0,&g_1004,&g_1004,(void*)0,&l_1013},{(void*)0,&l_1013,&g_1004,&l_1013,&g_936,&g_936},{&l_1013,&g_936,&g_936,&l_1013,&g_1004,&l_1013},{(void*)0,&l_1013,(void*)0,&g_1004,&g_1004,(void*)0}},{{&g_1004,&g_1004,&g_1004,&g_936,&g_1004,&g_936},{(void*)0,&l_1013,(void*)0,(void*)0,&g_1004,(void*)0},{&g_1004,&g_936,&l_1013,&l_1013,&g_936,(void*)0},{(void*)0,&l_1013,(void*)0,&g_1004,(void*)0,&g_936}},{{&l_1013,(void*)0,&g_1004,&g_1004,&g_1004,(void*)0},{&l_1013,&g_936,(void*)0,&g_1004,(void*)0,&l_1013},{(void*)0,(void*)0,&g_936,&l_1013,&l_1013,&g_936},{&g_1004,(void*)0,&g_1004,(void*)0,(void*)0,&l_1013}},{{(void*)0,&g_936,&g_1004,&g_936,&g_1004,&g_1004},{&g_1004,(void*)0,&g_1004,&g_1004,(void*)0,&l_1013},{(void*)0,&l_1013,&g_1004,&l_1013,&g_936,&g_936},{&l_1013,&g_936,&g_936,&l_1013,&g_1004,&l_1013}}};
            int32_t l_1297 = 0x87F5D124L;
            int i, j, k;
            l_1297 &= (((***l_1208) &= ((**l_1212) = l_1273)) < (safe_add_func_int32_t_s_s(0x779C4E7DL, (((g_150 | (safe_rshift_func_uint8_t_u_u(((**l_999) = (((**g_301) > g_530) >= l_1273)), (((++(**g_932)) == ((safe_mod_func_uint8_t_u_u(((safe_sub_func_int32_t_s_s(((**g_253) ^= (safe_div_func_int32_t_s_s(((*g_522) == (safe_unary_minus_func_int64_t_s((((g_1004 = (safe_mod_func_int64_t_s_s((safe_sub_func_uint32_t_u_u((safe_unary_minus_func_int16_t_s(l_1273)), (!((((g_1168[2] && 0x00L) < 0UL) , 1UL) <= l_1295)))), (-1L)))) && 0xD03DL) , g_3)))), (*g_817)))), (-7L))) > g_98), l_1273)) && l_1273)) <= 0L)))) , 0x0FF9E465L) >= (*g_817)))));
            if (l_1273)
                goto lbl_1298;
        }
        (*g_253) = (*g_253);
    }
    if (((*g_817) = ((safe_sub_func_int32_t_s_s((((*l_840) = ((((*l_969) = (*l_969)) == (void*)0) & l_1315)) , ((**g_253) = 0x8DBD52B5L)), ((**g_932) = (((&g_337[4][4] != &g_337[4][5]) | g_38) ^ (safe_mod_func_int8_t_s_s((g_530 <= ((safe_lshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_u(((safe_lshift_func_uint16_t_u_u(((safe_unary_minus_func_int8_t_s((safe_mul_func_uint16_t_u_u((&l_1175 == &l_1315), g_3)))) ^ g_142), g_1327[2])) >= 0xA321L), 0)), g_1004)) | l_1328)), g_1219)))))) , (*g_817))))
    { /* block id: 504 */
        return g_485;
    }
    else
    { /* block id: 506 */
        uint16_t *l_1331 = &g_142;
        int32_t *l_1334[5];
        uint64_t *l_1340 = (void*)0;
        float *l_1366 = (void*)0;
        float *l_1367 = &l_1215;
        float *l_1368[7][1] = {{&g_293},{&g_1105[0]},{&g_293},{&g_293},{&g_1105[0]},{&g_293},{&g_293}};
        int64_t ***l_1379 = &l_969;
        float l_1384 = 0x1.0p+1;
        int8_t **l_1387 = &l_1213;
        int i, j;
        for (i = 0; i < 5; i++)
            l_1334[i] = (void*)0;
        if ((safe_mod_func_uint64_t_u_u(18446744073709551614UL, (((*l_969) != (void*)0) ^ ((((void*)0 == l_1331) || l_1332) < (~((0L > (((*g_253) = (*g_253)) == l_1334[2])) , (*g_817))))))))
        { /* block id: 508 */
            float *l_1341 = &g_1105[2];
            float *l_1345 = &l_1215;
            (*l_1345) = (safe_sub_func_float_f_f((safe_sub_func_float_f_f((-0x8.5p-1), ((*g_413) > ((*l_1341) = (-(l_1340 == (void*)0)))))), (safe_mul_func_float_f_f(0x4.3E53EEp-17, (*g_658)))));
        }
        else
        { /* block id: 511 */
            int32_t l_1352[6] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
            int64_t *l_1353 = &l_1332;
            int8_t *l_1354 = &l_1295;
            int64_t *l_1356 = &g_283[0][8][0];
            int64_t **l_1355 = &l_1356;
            uint64_t l_1357 = 1UL;
            uint16_t *l_1358 = &g_485;
            int16_t *l_1365 = &g_1004;
            int i;
            (*g_253) = func_35((safe_sub_func_uint16_t_u_u((g_1187[5][4] , ((((*l_1358) = (safe_rshift_func_uint16_t_u_s(((*l_1331) = (((((safe_sub_func_int64_t_s_s(((**l_969) = l_1352[2]), ((*l_1353) = l_1352[2]))) , l_1354) == (void*)0) , &g_43[1]) != ((*l_1355) = (*l_969)))), l_1357))) , ((safe_mod_func_int16_t_s_s(((*l_1365) = ((safe_rshift_func_int16_t_s_s(((void*)0 != g_1363[1][3][4]), 8)) != l_1352[3])), 0x0474L)) | g_958)) , g_1236[0][7][2])), l_1352[3])));
            (*g_817) &= 0x2241FB16L;
        }
        (*g_658) = ((*l_1367) = (*g_658));
        for (g_164 = 29; (g_164 < (-17)); g_164 = safe_sub_func_uint64_t_u_u(g_164, 4))
        { /* block id: 525 */
            uint32_t l_1374 = 0xC7966AA2L;
            int32_t l_1388 = 0xA9E776C5L;
            int32_t l_1390[10] = {8L,8L,8L,8L,8L,8L,8L,8L,8L,8L};
            int i;
            (*l_1367) = (safe_div_func_float_f_f((*g_658), (+(g_321 , l_1374))));
            l_1391 |= ((((*g_817) <= (safe_rshift_func_int8_t_s_u(l_1374, 7))) > ((l_1379 != (void*)0) & (((*g_302) , (safe_mod_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_u(g_52[0][0][2], ((**l_1047) = ((safe_add_func_int8_t_s_s((((void*)0 != l_1387) && 1L), l_1388)) || l_1389)))) | l_1390[5]), g_164))) != l_1390[5]))) , 1L);
        }
    }
    return l_1392;
}


/* ------------------------------------------ */
/* 
 * reads : g_98
 * writes: g_38
 */
static int32_t  func_9(int32_t * p_10, uint16_t  p_11, int32_t  p_12, int64_t  p_13)
{ /* block id: 350 */
    int8_t l_836 = (-1L);
    (*p_10) = (g_98 != (safe_mul_func_int8_t_s_s(0L, ((&g_337[4][3] != &g_337[6][3]) | 0x09L))));
    return l_836;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_14(int32_t * const  p_15)
{ /* block id: 348 */
    int32_t *l_832 = &g_38;
    return l_832;
}


/* ------------------------------------------ */
/* 
 * reads : g_38
 * writes: g_38
 */
static int32_t * func_16(int32_t * p_17, int32_t * p_18, const int32_t * p_19, int32_t * p_20)
{ /* block id: 343 */
    int8_t l_827[3];
    int32_t *l_830 = (void*)0;
    int i;
    for (i = 0; i < 3; i++)
        l_827[i] = 0L;
    (*p_18) |= ((*p_17) &= (safe_rshift_func_uint8_t_u_u((l_827[0] & (l_827[1] > (safe_unary_minus_func_uint16_t_u((!0x4EL))))), 3)));
    return l_830;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_21(int32_t * const  p_22, int32_t  p_23, uint16_t  p_24, const int16_t  p_25, const int32_t * p_26)
{ /* block id: 340 */
    int32_t *l_819 = &g_38;
    uint32_t l_820 = 0xAB450427L;
    --l_820;
    return l_819;
}


/* ------------------------------------------ */
/* 
 * reads : g_38 g_485 g_321 g_520 g_521 g_135 g_52 g_98 g_522 g_4 g_318 g_530 g_740 g_533 g_182 g_152 g_117 g_165 g_142 g_164 g_315 g_316 g_413 g_414 g_816 g_817
 * writes: g_303 g_152 g_485 g_321 g_98 g_117 g_182 g_164 g_283 g_4
 */
static int32_t * func_27(uint8_t  p_28, int32_t * p_29)
{ /* block id: 292 */
    const uint8_t *l_672 = &g_98;
    const uint8_t **l_671 = &l_672;
    uint64_t ****l_684 = &g_520;
    uint64_t ****l_686 = &g_520;
    float *l_692 = (void*)0;
    int64_t l_694 = 0xA6ABE3DB70349706LL;
    int32_t l_716 = 9L;
    int32_t l_751 = (-8L);
    int32_t l_755[1];
    uint32_t l_815 = 18446744073709551610UL;
    int i;
    for (i = 0; i < 1; i++)
        l_755[i] = 0L;
    if ((((l_671 != &l_672) > ((safe_sub_func_uint32_t_u_u((0xCBL < ((p_28 , 0x4F3A5A6FL) | ((+(p_28 & (~(safe_rshift_func_uint16_t_u_s((safe_rshift_func_int16_t_s_s(0x134EL, ((p_28 , 0xB1D4L) == 0x6B32L))), 10))))) < 7UL))), 4UL)) == g_38)) || 0x4A93B26EDE409829LL))
    { /* block id: 293 */
        uint64_t *****l_685[6][6][7] = {{{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{(void*)0,(void*)0,&l_684,(void*)0,(void*)0,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,(void*)0},{&l_684,&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684},{&l_684,(void*)0,&l_684,&l_684,&l_684,(void*)0,&l_684}},{{&l_684,&l_684,&l_684,(void*)0,(void*)0,&l_684,&l_684},{(void*)0,&l_684,&l_684,&l_684,&l_684,(void*)0,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,(void*)0,(void*)0},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,(void*)0,&l_684,&l_684}},{{(void*)0,&l_684,&l_684,&l_684,(void*)0,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,(void*)0,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,(void*)0,(void*)0},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{(void*)0,(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684}},{{&l_684,&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,(void*)0,&l_684,&l_684,(void*)0},{&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684},{(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684}},{{&l_684,&l_684,(void*)0,(void*)0,&l_684,&l_684,&l_684},{&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684},{(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,(void*)0,(void*)0,(void*)0,&l_684},{&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,(void*)0}},{{&l_684,(void*)0,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{(void*)0,&l_684,&l_684,&l_684,&l_684,(void*)0,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684},{&l_684,&l_684,&l_684,&l_684,&l_684,&l_684,&l_684}}};
        int64_t l_690 = 1L;
        int8_t *l_691[2][3];
        int64_t *l_693 = &g_283[0][7][0];
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 3; j++)
                l_691[i][j] = (void*)0;
        }
        p_29 = p_29;
        for (g_303 = 0; g_303 < 3; g_303 += 1)
        {
            g_152[g_303] = 0UL;
        }
    }
    else
    { /* block id: 301 */
        uint64_t l_699[1];
        int32_t l_742 = 1L;
        int32_t l_748 = 0L;
        int32_t l_750 = 0xE194E562L;
        int32_t l_752 = (-1L);
        int32_t l_753 = 1L;
        int32_t l_757 = 0x61F3323FL;
        int32_t l_759 = 0x452376D1L;
        int32_t l_760 = 7L;
        uint32_t **l_805 = &g_533;
        uint32_t ***l_804 = &l_805;
        uint32_t ****l_803 = &l_804;
        int64_t *l_812[2][7] = {{&g_283[0][0][0],&g_283[0][0][0],(void*)0,&g_283[0][0][0],&g_283[0][0][0],(void*)0,&g_283[0][0][0]},{&g_283[0][0][0],&g_283[0][8][0],&g_283[0][8][0],&g_283[0][0][0],&g_283[0][8][0],&g_283[0][8][0],&g_283[0][0][0]}};
        uint8_t *l_813 = &g_182[5][4];
        int i, j;
        for (i = 0; i < 1; i++)
            l_699[i] = 1UL;
        for (g_485 = 0; (g_485 == 1); g_485 = safe_add_func_uint8_t_u_u(g_485, 1))
        { /* block id: 304 */
            uint64_t ***l_714 = &g_521[2][0];
            int32_t l_743 = (-1L);
            int32_t l_746 = 1L;
            int32_t l_747 = 0x0326FC28L;
            int32_t l_754 = (-1L);
            int32_t l_756 = 0x743ADBFFL;
            int32_t l_758 = 2L;
            uint32_t l_761 = 0x948EBC85L;
            for (g_321 = 0; (g_321 > 17); g_321 = safe_add_func_uint32_t_u_u(g_321, 6))
            { /* block id: 307 */
                float l_700 = 0xA.F4C856p-13;
                uint64_t **l_711 = &g_522;
                uint8_t *l_715[5];
                int32_t l_739[4] = {0x7EAEFB27L,0x7EAEFB27L,0x7EAEFB27L,0x7EAEFB27L};
                int i;
                for (i = 0; i < 5; i++)
                    l_715[i] = (void*)0;
                if (((*p_29) = ((l_699[0] && p_28) != ((safe_sub_func_uint8_t_u_u(p_28, (l_716 ^= (g_98 = ((0x669452F5L | (safe_mod_func_int64_t_s_s((safe_rshift_func_int16_t_s_s((((safe_add_func_int16_t_s_s(((((((safe_sub_func_int16_t_s_s((l_711 == (**l_686)), ((safe_mod_func_int64_t_s_s((l_699[0] > l_699[0]), 1L)) && 8UL))) , 4UL) , l_714) != &l_711) & g_135) >= g_52[0][0][0]), g_98)) , (void*)0) != (void*)0), 15)), (***g_520)))) , g_318))))) ^ 0x5CA57A18783B3B27LL))))
                { /* block id: 311 */
                    const int16_t l_741 = 1L;
                    int32_t *l_744 = &l_743;
                    int32_t *l_745[1];
                    int16_t l_749 = 0x80BCL;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_745[i] = &g_164;
                    l_742 &= (((p_28 > 0xB59BL) || (safe_add_func_int32_t_s_s((safe_sub_func_uint16_t_u_u(g_485, (safe_rshift_func_int16_t_s_u(((safe_mul_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u(1UL, 0x4148L)) == (--p_28)), ((void*)0 != &g_498))) <= l_716), (safe_rshift_func_uint16_t_u_s((((safe_lshift_func_int8_t_s_s(((((*g_533) = ((safe_mod_func_int64_t_s_s((safe_add_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((0x443C00562185D25FLL == g_530), 1)), l_739[3])), g_740)) & 0x6D0CABD99FB93141LL)) , l_739[3]) ^ l_699[0]), 7)) < l_739[3]) == l_739[0]), l_741)))))), l_739[0]))) , (*p_29));
                    ++l_761;
                }
                else
                { /* block id: 316 */
                    uint64_t l_773 = 0UL;
                    for (l_753 = 27; (l_753 <= 17); l_753 = safe_sub_func_uint16_t_u_u(l_753, 5))
                    { /* block id: 319 */
                        uint32_t l_770 = 4294967286UL;
                        (*p_29) |= (safe_mod_func_int16_t_s_s(((safe_add_func_uint32_t_u_u(l_770, ((--g_182[5][4]) == l_773))) || (safe_lshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((((((void*)0 != p_29) > p_28) <= (safe_mul_func_uint8_t_u_u((8UL ^ ((((safe_lshift_func_uint16_t_u_u((((safe_mod_func_int64_t_s_s((18446744073709551615UL >= (safe_sub_func_uint8_t_u_u((l_716 &= (safe_add_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s(l_773, 13)), 0x253DL)), g_152[0])), p_28))), l_753))), p_28)) >= l_739[3]) , g_117), l_752)) >= l_755[0]) , g_165) , g_142)), p_28))) , g_318), l_773)), l_773))), l_747));
                    }
                    for (g_164 = (-24); (g_164 <= (-29)); g_164--)
                    { /* block id: 326 */
                        return p_29;
                    }
                }
            }
        }
        l_742 |= (0xA6AA2ED47946DA30LL & ((safe_mul_func_float_f_f((((*p_29) , ((****l_684) = ((((safe_unary_minus_func_uint32_t_u(4294967286UL)) , (((safe_mul_func_int16_t_s_s((safe_div_func_int32_t_s_s((&g_531[5][7][0] == (p_28 , l_803)), p_28)), ((((*l_813) = (l_694 | ((((!((safe_lshift_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((!(g_283[0][6][0] = (7UL < l_716))), l_760)) <= l_716), 4)) ^ 0xF0E9L)) < p_28) , p_28) || (*g_315)))) >= g_38) , p_28))) , l_751) , 65531UL)) , p_28) || l_748))) , (*g_413)), 0x8.5p+1)) , l_751));
        (*g_816) ^= ((*p_29) &= ((+0x1FL) >= l_815));
    }
    return g_817;
}


/* ------------------------------------------ */
/* 
 * reads : g_253
 * writes: g_171
 */
static uint8_t  func_30(int32_t  p_31, int32_t  p_32, int8_t  p_33)
{ /* block id: 289 */
    int32_t l_670 = 0x48E2A5E0L;
    (*g_253) = &p_32;
    return l_670;
}


/* ------------------------------------------ */
/* 
 * reads : g_44 g_253 g_171
 * writes: g_44 g_38 g_52
 */
static int32_t * func_35(uint8_t  p_36)
{ /* block id: 5 */
    int32_t *l_37 = &g_38;
    int32_t *l_39 = &g_38;
    int32_t *l_40 = &g_38;
    int32_t *l_41 = &g_38;
    int32_t *l_42[6][3] = {{&g_38,&g_38,&g_38},{(void*)0,(void*)0,&g_3},{&g_38,&g_38,&g_38},{(void*)0,(void*)0,&g_3},{&g_38,&g_38,&g_38},{(void*)0,(void*)0,&g_3}};
    int64_t l_322 = 7L;
    int32_t *** const l_407 = &g_253;
    int32_t ***l_432[7][9][4] = {{{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0,&g_253},{&g_253,(void*)0,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0}},{{&g_253,(void*)0,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,(void*)0,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0,&g_253},{&g_253,&g_253,(void*)0,&g_253}},{{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{(void*)0,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253}},{{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253}},{{&g_253,&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{(void*)0,&g_253,&g_253,&g_253}},{{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0},{(void*)0,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253}},{{(void*)0,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{(void*)0,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0,&g_253}}};
    uint64_t ***l_440 = (void*)0;
    float l_453 = 0xB.7EC7BDp-38;
    const int32_t l_490 = 7L;
    const int32_t *l_507 = (void*)0;
    const int32_t **l_506[2][10][8] = {{{&l_507,&l_507,&l_507,&l_507,&l_507,&l_507,&l_507,(void*)0},{(void*)0,(void*)0,&l_507,&l_507,(void*)0,&l_507,&l_507,&l_507},{&l_507,&l_507,&l_507,(void*)0,&l_507,(void*)0,&l_507,(void*)0},{&l_507,&l_507,&l_507,(void*)0,&l_507,(void*)0,&l_507,&l_507},{&l_507,&l_507,(void*)0,&l_507,&l_507,(void*)0,(void*)0,(void*)0},{&l_507,&l_507,(void*)0,&l_507,&l_507,&l_507,(void*)0,&l_507},{(void*)0,(void*)0,&l_507,(void*)0,&l_507,&l_507,&l_507,(void*)0},{&l_507,&l_507,&l_507,(void*)0,(void*)0,(void*)0,&l_507,&l_507},{&l_507,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_507},{(void*)0,(void*)0,&l_507,&l_507,&l_507,&l_507,&l_507,&l_507}},{{(void*)0,&l_507,&l_507,(void*)0,&l_507,&l_507,&l_507,&l_507},{(void*)0,(void*)0,(void*)0,&l_507,&l_507,(void*)0,&l_507,(void*)0},{&l_507,(void*)0,(void*)0,(void*)0,&l_507,(void*)0,&l_507,&l_507},{(void*)0,&l_507,&l_507,&l_507,&l_507,&l_507,&l_507,(void*)0},{&l_507,(void*)0,&l_507,(void*)0,&l_507,&l_507,&l_507,(void*)0},{&l_507,&l_507,&l_507,&l_507,(void*)0,(void*)0,&l_507,&l_507},{&l_507,&l_507,&l_507,(void*)0,&l_507,(void*)0,&l_507,&l_507},{&l_507,&l_507,(void*)0,&l_507,&l_507,(void*)0,&l_507,(void*)0},{&l_507,&l_507,&l_507,(void*)0,&l_507,&l_507,&l_507,&l_507},{&l_507,(void*)0,(void*)0,(void*)0,(void*)0,&l_507,&l_507,(void*)0}}};
    const int32_t ***l_505 = &l_506[1][7][3];
    const int32_t ****l_504 = &l_505;
    int16_t l_519[5] = {0L,0L,0L,0L,0L};
    int32_t *l_535 = &g_164;
    uint32_t l_564 = 0UL;
    int16_t l_602 = (-10L);
    int32_t l_612 = 0L;
    uint32_t **l_620 = &g_533;
    uint32_t ***l_619 = &l_620;
    int8_t ***l_646 = (void*)0;
    const int32_t l_651 = 0L;
    int i, j, k;
    g_44++;
    for (p_36 = 0; (p_36 != 27); p_36++)
    { /* block id: 9 */
        float l_70 = 0x0.6p+1;
        int32_t **l_89[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        uint32_t l_358[1][10][10] = {{{0x433BF013L,0UL,0x433BF013L,0xF74EFF49L,0x82979188L,0x77A634DEL,4UL,0xF52C6664L,3UL,0x7D35CA30L},{0xEEF8A137L,0x12F89EAAL,4UL,0x433BF013L,0x77A634DEL,1UL,0x8B57DF5FL,0xF52C6664L,0xAD6472F1L,1UL},{0x11C0D050L,0x49556DF8L,0x433BF013L,4294967292UL,3UL,0xAE0AA558L,4294967295UL,4UL,2UL,0x48B1C6D4L},{0x49556DF8L,0x82979188L,0x94EE1D85L,0xF52C6664L,5UL,5UL,0xF52C6664L,0x16843CFBL,4294967292UL,2UL},{0x94EE1D85L,0xEEF8A137L,0x82979188L,0x8B57DF5FL,4294967295UL,1UL,0x6598BCBCL,0xAD6472F1L,0x433BF013L,0x48B1C6D4L},{0UL,3UL,0x7D05925AL,0xEEF8A137L,4294967295UL,0x39A730BAL,0xD53832CBL,0x8B57DF5FL,0xF74EFF49L,2UL},{4294967295UL,0x6598BCBCL,2UL,2UL,3UL,0xEEF8A137L,0UL,5UL,1UL,5UL},{0x11C0D050L,0x1DB8ABF7L,0x7D35CA30L,0x7D05925AL,0x7D35CA30L,0x1DB8ABF7L,0x11C0D050L,0xEEF8A137L,0x12F89EAAL,4UL},{0xF52C6664L,0xF74EFF49L,0xAD23FF09L,0UL,4294967295UL,7UL,0xAD6472F1L,0xAE0AA558L,2UL,0xEEF8A137L},{0x7D05925AL,0xF74EFF49L,2UL,0x94EE1D85L,4294967292UL,1UL,0x11C0D050L,4UL,0x49556DF8L,0xAD23FF09L}}};
        int16_t l_388[2];
        uint64_t ***l_441 = (void*)0;
        int8_t l_451 = (-10L);
        uint32_t l_454 = 18446744073709551609UL;
        uint8_t l_561[2];
        int32_t *l_578[8][8][4] = {{{&g_3,&g_38,(void*)0,&g_38},{&g_38,&g_3,&g_3,(void*)0},{&g_164,(void*)0,&g_3,&g_38},{&g_164,(void*)0,(void*)0,&g_38},{&g_38,&g_3,&g_3,&g_164},{(void*)0,&g_3,&g_164,&g_164},{(void*)0,&g_164,&g_38,&g_38},{&g_38,&g_3,&g_38,(void*)0}},{{(void*)0,&g_3,(void*)0,(void*)0},{&g_3,&g_164,&g_38,&g_3},{&g_164,(void*)0,&g_38,(void*)0},{&g_3,&g_164,(void*)0,&g_3},{(void*)0,&g_38,&g_38,&g_3},{&g_38,&g_3,&g_38,&g_38},{(void*)0,(void*)0,&g_164,&g_38},{(void*)0,&g_3,&g_3,&g_164}},{{&g_38,(void*)0,(void*)0,&g_3},{&g_164,&g_3,&g_3,&g_164},{&g_164,&g_38,&g_3,(void*)0},{&g_38,(void*)0,(void*)0,&g_38},{&g_3,&g_38,&g_38,&g_38},{&g_3,(void*)0,&g_3,(void*)0},{&g_38,&g_38,&g_38,&g_164},{(void*)0,&g_3,&g_38,&g_3}},{{&g_38,(void*)0,&g_164,&g_164},{&g_38,&g_3,&g_164,&g_38},{(void*)0,(void*)0,&g_3,&g_38},{&g_38,&g_3,&g_164,&g_3},{&g_164,&g_38,&g_3,&g_3},{&g_3,&g_164,&g_3,(void*)0},{&g_38,(void*)0,&g_38,&g_3},{&g_38,&g_164,&g_3,(void*)0}},{{&g_164,&g_38,&g_164,(void*)0},{(void*)0,(void*)0,&g_3,(void*)0},{&g_3,&g_38,&g_3,&g_164},{&g_164,&g_3,&g_38,(void*)0},{(void*)0,&g_38,&g_3,&g_3},{&g_164,&g_3,(void*)0,(void*)0},{&g_38,&g_38,&g_38,(void*)0},{(void*)0,&g_38,&g_38,&g_38}},{{&g_3,&g_3,&g_38,&g_38},{&g_164,&g_3,&g_3,&g_38},{&g_3,&g_38,&g_3,(void*)0},{&g_38,&g_38,&g_164,(void*)0},{&g_3,&g_3,(void*)0,&g_3},{&g_38,&g_38,&g_164,(void*)0},{&g_38,&g_3,&g_38,&g_164},{(void*)0,&g_38,(void*)0,(void*)0}},{{&g_38,(void*)0,&g_38,(void*)0},{(void*)0,&g_38,&g_38,&g_3},{&g_38,&g_164,&g_3,&g_38},{&g_38,&g_3,&g_3,&g_164},{&g_38,&g_3,&g_38,&g_164},{(void*)0,&g_164,&g_38,&g_164},{&g_38,&g_164,(void*)0,&g_3},{(void*)0,(void*)0,&g_38,&g_164}},{{&g_38,&g_3,&g_164,&g_3},{&g_38,&g_38,(void*)0,(void*)0},{&g_3,&g_164,&g_164,&g_3},{&g_38,&g_38,&g_3,(void*)0},{&g_3,&g_164,&g_3,&g_3},{&g_164,&g_38,&g_38,&g_3},{&g_3,&g_164,&g_38,(void*)0},{(void*)0,&g_38,&g_38,&g_3}}};
        uint32_t ***l_618 = (void*)0;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_388[i] = 1L;
        for (i = 0; i < 2; i++)
            l_561[i] = 0x90L;
        for (g_38 = 0; (g_38 >= 7); g_38 = safe_add_func_int64_t_s_s(g_38, 8))
        { /* block id: 12 */
            float *l_51[4];
            int32_t *l_53 = &g_3;
            int i;
            for (i = 0; i < 4; i++)
                l_51[i] = (void*)0;
            g_52[0][0][0] = 0x1.9p-1;
            return l_53;
        }
    }
    return (**l_407);
}


/* ------------------------------------------ */
/* 
 * reads : g_38 g_117 g_314 g_165 g_4 g_142
 * writes: g_117 g_318 g_164
 */
static uint8_t  func_80(int64_t  p_81, int32_t * p_82, int32_t  p_83, uint32_t  p_84)
{ /* block id: 98 */
    int16_t l_309 = 0xFAD5L;
    uint32_t *l_317 = &g_318;
    int32_t *l_319 = &g_164;
    (*l_319) = (((safe_mod_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(0L, 0x20F8B623F7150EB7LL)), l_309)) | ((*l_317) = ((safe_rshift_func_int8_t_s_s(((*p_82) > p_83), 6)) >= ((g_117 |= 0x4C8185F2L) ^ (safe_sub_func_int16_t_s_s((((void*)0 == g_314[7]) & l_309), g_165)))))) > g_4[1][1]);
    return g_142;
}


/* ------------------------------------------ */
/* 
 * reads : g_98 g_101 g_4 g_117 g_38 g_142 g_135 g_165 g_168 g_182 g_150 g_3 g_152 g_301
 * writes: g_98 g_4 g_142 g_150 g_165 g_168 g_171 g_182 g_135 g_117 g_152 g_253 g_255 g_301
 */
static float  func_85(int32_t ** p_86, int8_t  p_87, int32_t  p_88)
{ /* block id: 22 */
    int64_t l_118[9][9] = {{0x70C77B72BA733A7ALL,6L,6L,0x70C77B72BA733A7ALL,0x3862D450074354AFLL,0x896031D569F207B6LL,0x896031D569F207B6LL,0x3862D450074354AFLL,0x70C77B72BA733A7ALL},{0L,(-9L),0L,0x474A89AEEB1CB69ALL,0L,(-9L),0L,0x474A89AEEB1CB69ALL,0L},{0x70C77B72BA733A7ALL,0x70C77B72BA733A7ALL,0x896031D569F207B6LL,6L,0x3862D450074354AFLL,0x3862D450074354AFLL,6L,0x896031D569F207B6LL,0x70C77B72BA733A7ALL},{0xE0A50B65BDA1241BLL,0x474A89AEEB1CB69ALL,0xCFDD8C90CC824F9DLL,0x474A89AEEB1CB69ALL,0xE0A50B65BDA1241BLL,0x474A89AEEB1CB69ALL,0xCFDD8C90CC824F9DLL,0x474A89AEEB1CB69ALL,0xE0A50B65BDA1241BLL},{0x3862D450074354AFLL,6L,0x896031D569F207B6LL,0x70C77B72BA733A7ALL,0x70C77B72BA733A7ALL,0x896031D569F207B6LL,6L,0x3862D450074354AFLL,0x3862D450074354AFLL},{0L,0x474A89AEEB1CB69ALL,0L,(-9L),0L,0x474A89AEEB1CB69ALL,0L,(-9L),0L},{0x3862D450074354AFLL,0x70C77B72BA733A7ALL,6L,6L,0x70C77B72BA733A7ALL,0x3862D450074354AFLL,0x896031D569F207B6LL,0x896031D569F207B6LL,0x3862D450074354AFLL},{0xE0A50B65BDA1241BLL,(-9L),0xCFDD8C90CC824F9DLL,(-9L),0xE0A50B65BDA1241BLL,(-9L),0xCFDD8C90CC824F9DLL,(-9L),0xE0A50B65BDA1241BLL},{0x70C77B72BA733A7ALL,6L,6L,0x70C77B72BA733A7ALL,0x3862D450074354AFLL,0x896031D569F207B6LL,0x896031D569F207B6LL,0x3862D450074354AFLL,0x70C77B72BA733A7ALL}};
    const int32_t l_119 = 0xF347F2DBL;
    int32_t l_153 = 0L;
    const int8_t *l_185 = &g_135;
    const int8_t **l_184 = &l_185;
    int32_t l_222 = (-1L);
    int32_t *l_229[8][1][4] = {{{&l_153,&l_153,&l_153,&l_153}},{{&l_153,&l_153,&l_153,&l_153}},{{&l_153,&l_153,&l_153,&l_153}},{{&l_153,&l_153,&l_153,&l_153}},{{&l_153,&l_153,&l_153,&l_153}},{{&l_153,&l_153,&l_153,&l_153}},{{&l_153,&l_153,&l_153,&l_153}},{{&l_153,&l_153,&l_153,&l_153}}};
    int32_t *l_230 = (void*)0;
    int8_t l_287 = (-1L);
    int32_t l_291[4][2][2] = {{{0xBCC63CCAL,3L},{0x290F09D0L,3L}},{{0xBCC63CCAL,3L},{0x290F09D0L,3L}},{{0xBCC63CCAL,3L},{0x290F09D0L,3L}},{{0xBCC63CCAL,3L},{0x290F09D0L,3L}}};
    int8_t l_294 = 0L;
    uint16_t l_299 = 0xE303L;
    volatile uint64_t ***l_304 = &g_301;
    int i, j, k;
lbl_300:
    for (p_88 = 0; (p_88 <= 1); p_88 += 1)
    { /* block id: 25 */
        int32_t l_91 = (-10L);
        int32_t *l_92 = &l_91;
        int32_t *l_93 = &l_91;
        int32_t *l_94 = &l_91;
        int32_t *l_95 = &l_91;
        int32_t *l_96 = &l_91;
        int32_t *l_97 = (void*)0;
        int8_t *l_136 = &g_135;
        int32_t l_261 = 0x0984C2C5L;
        int32_t l_263 = 0x83445F07L;
        int32_t l_265[9][10][2] = {{{0xC72A8FA4L,0xE97E0EC3L},{(-1L),0x5251D080L},{0xD86CF038L,0L},{0x36639851L,0x36639851L},{0xFA5D7BF6L,(-1L)},{8L,(-1L)},{1L,(-1L)},{0xE97E0EC3L,1L},{(-1L),0xC72A8FA4L},{(-1L),1L}},{{0xE97E0EC3L,(-1L)},{1L,(-1L)},{8L,(-1L)},{0xFA5D7BF6L,0x36639851L},{0x36639851L,0L},{0xD86CF038L,0x5251D080L},{(-1L),0xE97E0EC3L},{0xC72A8FA4L,0x5B589D5DL},{0xEEE85F25L,0xCADE12A7L},{(-1L),0x795231DAL}},{{(-8L),0x2AAD204EL},{0x481FF659L,0x5A3D644BL},{0xA76FBAC7L,(-1L)},{(-1L),0x3D9D43D6L},{1L,0x4B668E61L},{(-1L),1L},{0xCADE12A7L,4L},{7L,0x2FD1BDCFL},{0xC2310836L,1L},{0x0197DAF0L,0L}},{{0x4B668E61L,0xFA5D7BF6L},{0x20846C0FL,(-8L)},{(-1L),0xC2310836L},{0xB7820DD7L,0x96F88958L},{0x5F2D47BFL,0x6F4B5C2CL},{0x6134C1F5L,0xB7820DD7L},{0x5B589D5DL,0x2B921B1EL},{0x6CAE12ECL,0xD86CF038L},{0L,(-7L)},{0x795231DAL,0x25C80A1DL}},{{0x5251D080L,8L},{0L,0x481FF659L},{(-10L),0x481FF659L},{0L,8L},{0x5251D080L,0x25C80A1DL},{0x795231DAL,(-7L)},{0L,0xD86CF038L},{0x6CAE12ECL,0x2B921B1EL},{0x5B589D5DL,0xB7820DD7L},{0x6134C1F5L,0x6F4B5C2CL}},{{0x5F2D47BFL,0x96F88958L},{0xB7820DD7L,0xC2310836L},{(-1L),(-8L)},{0x20846C0FL,0xFA5D7BF6L},{0x4B668E61L,0L},{0x0197DAF0L,1L},{0xC2310836L,0x2FD1BDCFL},{7L,4L},{0xCADE12A7L,1L},{(-1L),0x4B668E61L}},{{1L,0x3D9D43D6L},{(-1L),(-1L)},{0xEEE85F25L,0x2AAD204EL},{0xB7820DD7L,0xE9086A25L},{0x5A3D644BL,(-7L)},{0x0197DAF0L,0x825CBD8DL},{(-8L),4L},{0x5F2D47BFL,0L},{0x2FD1BDCFL,0x3D9D43D6L},{3L,(-1L)}},{{0xC72A8FA4L,0xC72A8FA4L},{0x92BA6CF0L,0x5BAC6DDDL},{0x5251D080L,(-1L)},{0xA76FBAC7L,0L},{0L,0xA76FBAC7L},{0xD86CF038L,0x5F2D47BFL},{0xD86CF038L,0xA76FBAC7L},{0L,0L},{0xA76FBAC7L,(-1L)},{0x5251D080L,0x5BAC6DDDL}},{{0x92BA6CF0L,0xC72A8FA4L},{0xC72A8FA4L,(-1L)},{3L,0x3D9D43D6L},{0x2FD1BDCFL,0L},{0x5F2D47BFL,4L},{(-8L),0x825CBD8DL},{0x0197DAF0L,(-7L)},{0x5A3D644BL,0xE9086A25L},{0xB7820DD7L,0x2AAD204EL},{0xEEE85F25L,0xD86CF038L}}};
        int16_t l_271 = 0x4CC5L;
        int32_t l_280 = (-1L);
        int32_t l_292 = (-10L);
        uint32_t l_296 = 7UL;
        int i, j, k;
        g_98--;
        if (p_87)
            break;
        for (g_98 = 0; (g_98 <= 1); g_98 += 1)
        { /* block id: 30 */
            uint32_t *l_116[1];
            int32_t l_220 = 0x1ADA0F0CL;
            int32_t l_260 = 0L;
            int32_t l_270 = 0xF08E0E4DL;
            int32_t l_272 = 0xD8A633CFL;
            int32_t l_288 = 0x0F7D3465L;
            int32_t l_289[7];
            int8_t l_290 = (-2L);
            int64_t l_295 = 0x9E9538959A926157LL;
            int i, j;
            for (i = 0; i < 1; i++)
                l_116[i] = &g_117;
            for (i = 0; i < 7; i++)
                l_289[i] = 1L;
            if ((((void*)0 == g_101) || (g_4[p_88][(g_98 + 1)] , (((safe_div_func_uint16_t_u_u((safe_div_func_uint16_t_u_u((((safe_lshift_func_uint8_t_u_u((((safe_sub_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((safe_div_func_int32_t_s_s(g_4[p_88][p_88], (-1L))), 1)), ((safe_mod_func_uint16_t_u_u((0x9A64B00FL & (l_118[8][1] = ((g_4[p_88][g_98] ^ (0x03D5L < ((p_87 = g_4[p_88][(g_98 + 1)]) || 1UL))) && p_88))), l_119)) , g_117))) == 0xA7L) >= g_38), p_88)) | p_88) , g_98), (-6L))), l_119)) != g_4[p_88][p_88]) ^ l_119))))
            { /* block id: 33 */
                uint64_t *l_132 = &g_4[p_88][g_98];
                int8_t *l_134[10][3] = {{(void*)0,&g_135,(void*)0},{&g_135,(void*)0,&g_135},{&g_135,&g_135,(void*)0},{&g_135,(void*)0,(void*)0},{(void*)0,&g_135,&g_135},{&g_135,&g_135,&g_135},{&g_135,(void*)0,&g_135},{&g_135,&g_135,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_135,&g_135}};
                int8_t **l_133 = &l_134[1][1];
                uint16_t *l_141 = &g_142;
                uint16_t *l_149 = &g_150;
                uint16_t *l_151[10][1];
                int64_t *l_154 = &l_118[8][1];
                int16_t l_155 = 0xBD3CL;
                int32_t l_181 = 0x48C122CBL;
                int32_t l_183 = 1L;
                uint8_t *l_194 = &g_182[7][6];
                int32_t l_221 = 0xE398B15EL;
                int i, j;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_151[i][j] = &g_152[0];
                }
                if ((((2UL || (safe_sub_func_uint64_t_u_u((safe_add_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_u(((safe_unary_minus_func_int64_t_s(((*l_154) = (~(safe_div_func_int32_t_s_s(g_117, (((l_153 |= ((safe_mod_func_uint64_t_u_u(((*l_132) = g_98), (*l_95))) , ((((*l_133) = &p_87) != l_136) ^ (((safe_rshift_func_uint8_t_u_s((safe_div_func_int8_t_s_s((0xB00C1120L && ((((*l_141)++) >= (safe_lshift_func_uint16_t_u_u(g_4[p_88][g_98], ((*l_149) = (safe_sub_func_int8_t_s_s(p_87, 0x1DL)))))) & l_118[5][0])), (-6L))), 7)) <= p_87) && 248UL)))) , p_88) && 0xF7B0L))))))) , g_4[p_88][p_88]), g_98)) , p_88), l_155)), g_135))) , &g_117) != l_116[0]))
                { /* block id: 40 */
                    uint32_t **l_156 = &l_116[0];
                    uint32_t ***l_157 = &l_156;
                    int32_t *l_158 = &l_91;
                    int32_t *l_159 = &l_153;
                    int32_t *l_160 = (void*)0;
                    int32_t *l_161 = (void*)0;
                    int32_t *l_162 = &l_91;
                    int32_t *l_163[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_163[i] = &l_91;
                    (*l_157) = l_156;
                    --g_165;
                    --g_168[0][4][0];
                    g_171[0] = &l_91;
                }
                else
                { /* block id: 45 */
                    float * const l_179 = (void*)0;
                    (*l_95) ^= (~(((9L && (l_183 = (((*l_136) = ((safe_add_func_int64_t_s_s(g_168[3][0][1], ((65528UL != (g_182[5][4] = (safe_rshift_func_int8_t_s_u((l_181 &= (safe_div_func_uint32_t_u_u((l_179 != (void*)0), (~p_87)))), p_87)))) >= (0x31A7L < g_168[0][4][0])))) <= l_118[8][1])) == 255UL))) , 1UL) || 0x3E95E56FL));
                }
                l_181 = (((l_181 <= ((((l_184 == (p_87 , (void*)0)) >= (safe_mul_func_int16_t_s_s((safe_sub_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((g_135 &= (safe_rshift_func_uint8_t_u_s(((*l_194) ^= l_118[8][1]), p_88))), ((void*)0 == &g_168[2][3][0]))), 0UL)), g_38))) == (*l_94)) , p_88)) != g_98) >= 0x0.Fp+1);
                l_222 |= (safe_sub_func_uint32_t_u_u((safe_add_func_int64_t_s_s((!(safe_add_func_int64_t_s_s(p_88, (((safe_sub_func_int64_t_s_s(((((safe_mod_func_uint32_t_u_u((((safe_lshift_func_int8_t_s_s((-1L), 2)) <= 0x76L) <= (safe_add_func_uint32_t_u_u(p_88, (((l_136 == l_136) , (((l_220 = ((p_87 > (((((((safe_mul_func_int16_t_s_s((((++(*l_132)) & ((*l_154) &= (safe_mul_func_uint16_t_u_u((safe_div_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((0x9DL >= ((g_117 = (((((*l_92) , l_181) >= l_119) == g_142) >= p_88)) < l_153)), 65535UL)), p_87)), p_88)))) > 0x6970L), l_119)) >= 8L) != g_150) || (*l_93)) , 0x5181CDD84EC9872CLL) <= g_3) , 0UL)) , (*l_94))) || p_88) >= g_38)) || p_88)))), 0xAA1069F5L)) , p_86) == (void*)0) ^ 0x64L), g_182[5][4])) & l_221) != g_150)))), g_168[2][4][1])), 0x18137C76L));
            }
            else
            { /* block id: 60 */
                uint64_t l_223 = 0x6D6AB5507861EF60LL;
                int32_t l_269 = 0xDED1B19EL;
                int32_t l_273 = 0x6EF15EC3L;
                int32_t l_274 = (-5L);
                int32_t l_275 = 1L;
                int32_t l_276 = 0x814D2432L;
                uint32_t l_284[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_284[i] = 4294967286UL;
                if (l_223)
                    break;
                for (g_165 = (-29); (g_165 >= 41); g_165 = safe_add_func_uint64_t_u_u(g_165, 4))
                { /* block id: 64 */
                    for (g_117 = (-20); (g_117 == 58); g_117 = safe_add_func_uint16_t_u_u(g_117, 6))
                    { /* block id: 67 */
                        int32_t **l_228[7][2][4] = {{{&g_171[0],(void*)0,&g_171[0],&l_96},{&l_95,(void*)0,&l_97,(void*)0}},{{&l_95,&l_96,&g_171[0],(void*)0},{&g_171[0],(void*)0,&g_171[0],&l_96}},{{&l_95,(void*)0,&l_97,(void*)0},{&l_95,&l_96,&g_171[0],(void*)0}},{{&g_171[0],(void*)0,&g_171[0],&l_96},{&l_95,(void*)0,&l_97,(void*)0}},{{&l_95,&l_96,&g_171[0],(void*)0},{&g_171[0],(void*)0,&g_171[0],&l_96}},{{&l_95,(void*)0,&l_97,(void*)0},{&l_95,&l_96,&g_171[0],(void*)0}},{{&g_171[0],(void*)0,&g_171[0],&l_96},{&l_95,(void*)0,&l_97,(void*)0}}};
                        int i, j, k;
                        l_229[2][0][1] = (void*)0;
                    }
                    l_230 = &g_38;
                }
                for (l_222 = 0; (l_222 <= (-2)); --l_222)
                { /* block id: 74 */
                    uint16_t *l_235 = &g_152[0];
                    int32_t **l_246[2];
                    int32_t ***l_245 = &l_246[0];
                    int32_t **l_252 = &l_97;
                    int32_t ***l_251[8][5];
                    float *l_254 = &g_255;
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_246[i] = (void*)0;
                    for (i = 0; i < 8; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_251[i][j] = &l_252;
                    }
                    (*l_96) = (((safe_add_func_float_f_f(((--(*l_235)) , (!((safe_sub_func_float_f_f((safe_div_func_float_f_f(((p_87 , g_4[p_88][p_88]) , ((*l_254) = (((safe_add_func_float_f_f((((*l_245) = &g_171[0]) != p_86), ((safe_div_func_float_f_f((-(0x6.1A9CCBp-99 >= (+((0x9.Bp-1 > ((((g_253 = p_86) == &l_229[2][0][1]) == (0x5.237F14p+98 == p_87)) > 0xF.626814p-33)) < (-0x1.6p-1))))), l_220)) > 0x7.2p-1))) < 0x7.381F49p+53) < g_150))), (*l_93))), g_150)) <= p_87))), (*l_96))) , (void*)0) != &l_223);
                    if ((safe_mul_func_int16_t_s_s((-1L), 0xDD84L)))
                    { /* block id: 80 */
                        int16_t l_258 = 0x3E0BL;
                        int32_t l_259 = 0xB037CAC5L;
                        int32_t l_262 = (-2L);
                        int32_t l_264 = (-5L);
                        int32_t l_266 = 0x6708EC85L;
                        int32_t l_267 = (-1L);
                        int32_t l_268[1];
                        uint32_t l_277[1][10][5] = {{{0UL,0xBD1B5C31L,18446744073709551610UL,1UL,18446744073709551610UL},{0xAAEBE991L,0xAAEBE991L,1UL,18446744073709551606UL,1UL},{0UL,0xBD1B5C31L,18446744073709551610UL,1UL,18446744073709551610UL},{0xAAEBE991L,0xAAEBE991L,1UL,18446744073709551606UL,1UL},{0UL,0xBD1B5C31L,18446744073709551610UL,1UL,18446744073709551610UL},{0xAAEBE991L,0xAAEBE991L,1UL,18446744073709551606UL,1UL},{0UL,0xBD1B5C31L,18446744073709551610UL,1UL,18446744073709551610UL},{0xAAEBE991L,0xAAEBE991L,1UL,18446744073709551606UL,1UL},{0UL,0xBD1B5C31L,18446744073709551610UL,1UL,18446744073709551610UL},{0xAAEBE991L,0xAAEBE991L,1UL,18446744073709551606UL,0xA573045CL}}};
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                            l_268[i] = (-1L);
                        ++l_277[0][4][0];
                        if (l_275)
                            continue;
                    }
                    else
                    { /* block id: 83 */
                        int8_t l_281 = 0x49L;
                        int32_t l_282 = 0xD37AC70BL;
                        ++l_284[0];
                    }
                    (*l_252) = (void*)0;
                }
                (*l_96) = p_88;
            }
            ++l_296;
        }
    }
    l_299 &= (p_88 = (&g_101 != (void*)0));
    if (g_3)
        goto lbl_300;
    (*l_304) = g_301;
    return p_87;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_4[i][j], "g_4[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_38, "g_38", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_43[i], "g_43[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_44, "g_44", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_52[i][j][k], "g_52[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_72, "g_72", print_hash_value);
    transparent_crc(g_98, "g_98", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc(g_142, "g_142", print_hash_value);
    transparent_crc(g_150, "g_150", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_152[i], "g_152[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_164, "g_164", print_hash_value);
    transparent_crc(g_165, "g_165", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_168[i][j][k], "g_168[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_182[i][j], "g_182[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc_bytes (&g_255, sizeof(g_255), "g_255", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_283[i][j][k], "g_283[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_293, sizeof(g_293), "g_293", print_hash_value);
    transparent_crc(g_303, "g_303", print_hash_value);
    transparent_crc(g_316, "g_316", print_hash_value);
    transparent_crc(g_318, "g_318", print_hash_value);
    transparent_crc(g_321, "g_321", print_hash_value);
    transparent_crc(g_366, "g_366", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc_bytes(&g_414[i][j][k], sizeof(g_414[i][j][k]), "g_414[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_461[i][j][k], "g_461[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_485, "g_485", print_hash_value);
    transparent_crc(g_530, "g_530", print_hash_value);
    transparent_crc_bytes (&g_681, sizeof(g_681), "g_681", print_hash_value);
    transparent_crc(g_740, "g_740", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_892[i][j], "g_892[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_936, "g_936", print_hash_value);
    transparent_crc(g_958, "g_958", print_hash_value);
    transparent_crc(g_1004, "g_1004", print_hash_value);
    transparent_crc(g_1076, "g_1076", print_hash_value);
    transparent_crc_bytes (&g_1104, sizeof(g_1104), "g_1104", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc_bytes(&g_1105[i], sizeof(g_1105[i]), "g_1105[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1168[i], "g_1168[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1185, "g_1185", print_hash_value);
    transparent_crc(g_1186, "g_1186", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1187[i][j], "g_1187[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1219, "g_1219", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1236[i][j][k], "g_1236[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1243[i], "g_1243[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1271, "g_1271", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1327[i], "g_1327[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 353
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 44
breakdown:
   depth: 1, occurrence: 86
   depth: 2, occurrence: 16
   depth: 3, occurrence: 3
   depth: 5, occurrence: 2
   depth: 7, occurrence: 1
   depth: 11, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 4
   depth: 27, occurrence: 2
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 31, occurrence: 2
   depth: 44, occurrence: 1

XXX total number of pointers: 314

XXX times a variable address is taken: 802
XXX times a pointer is dereferenced on RHS: 138
breakdown:
   depth: 1, occurrence: 114
   depth: 2, occurrence: 19
   depth: 3, occurrence: 5
XXX times a pointer is dereferenced on LHS: 163
breakdown:
   depth: 1, occurrence: 148
   depth: 2, occurrence: 9
   depth: 3, occurrence: 5
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 24
XXX times a pointer is compared with address of another variable: 3
XXX times a pointer is compared with another pointer: 7
XXX times a pointer is qualified to be dereferenced: 4671

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 785
   level: 2, occurrence: 159
   level: 3, occurrence: 46
   level: 4, occurrence: 16
   level: 5, occurrence: 8
XXX number of pointers point to pointers: 138
XXX number of pointers point to scalars: 176
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.6
XXX average alias set size: 1.31

XXX times a non-volatile is read: 975
XXX times a non-volatile is write: 490
XXX times a volatile is read: 57
XXX    times read thru a pointer: 19
XXX times a volatile is write: 11
XXX    times written thru a pointer: 3
XXX times a volatile is available for access: 830
XXX percentage of non-volatile access: 95.6

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 85
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 26
   depth: 1, occurrence: 15
   depth: 2, occurrence: 16
   depth: 3, occurrence: 8
   depth: 4, occurrence: 14
   depth: 5, occurrence: 6

XXX percentage a fresh-made variable is used: 16.3
XXX percentage an existing variable is used: 83.7
********************* end of statistics **********************/


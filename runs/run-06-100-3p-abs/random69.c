/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1791909954
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   int16_t  f0;
   volatile int8_t * f1;
   volatile float  f2;
   uint8_t  f3;
   volatile float  f4;
};

union U1 {
   volatile int16_t  f0;
   uint32_t  f1;
   uint32_t  f2;
   const int32_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static int64_t g_4 = 0xE6D058A835A23E86LL;
static uint32_t g_6 = 0x80CA9F8DL;
static uint32_t g_19 = 0x3C3D2146L;
static uint32_t *g_18 = &g_19;
static int8_t *g_25 = (void*)0;
static int32_t g_27[2][6] = {{0x34ADC6F7L,7L,0xFAB8D232L,0xFAB8D232L,7L,0x34ADC6F7L},{(-7L),0x34ADC6F7L,0xFAB8D232L,0x34ADC6F7L,(-7L),(-7L)}};
static int8_t g_68 = 0x3BL;
static int32_t g_72 = 0xF9032B81L;
static volatile int32_t g_76 = 0x93878D32L;/* VOLATILE GLOBAL g_76 */
static uint16_t g_87 = 65528UL;
static uint16_t g_101 = 0x96F1L;
static uint16_t *g_100[8][10][3] = {{{&g_101,&g_101,&g_101},{(void*)0,&g_101,&g_101},{(void*)0,&g_101,&g_101},{(void*)0,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,(void*)0,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,(void*)0,&g_101},{&g_101,&g_101,(void*)0}},{{(void*)0,&g_101,&g_101},{(void*)0,&g_101,&g_101},{&g_101,&g_101,&g_101},{(void*)0,(void*)0,(void*)0},{&g_101,&g_101,&g_101},{(void*)0,(void*)0,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{(void*)0,&g_101,&g_101}},{{(void*)0,&g_101,&g_101},{&g_101,(void*)0,&g_101},{&g_101,&g_101,&g_101},{&g_101,(void*)0,&g_101},{(void*)0,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,(void*)0,&g_101},{&g_101,&g_101,(void*)0},{&g_101,&g_101,(void*)0}},{{&g_101,(void*)0,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,(void*)0},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,(void*)0},{&g_101,&g_101,&g_101},{&g_101,&g_101,(void*)0}},{{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,(void*)0,&g_101},{&g_101,(void*)0,(void*)0},{&g_101,&g_101,(void*)0},{&g_101,&g_101,&g_101},{&g_101,&g_101,(void*)0},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101}},{{&g_101,(void*)0,&g_101},{&g_101,&g_101,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_101,(void*)0,(void*)0},{&g_101,&g_101,&g_101},{&g_101,(void*)0,&g_101},{(void*)0,&g_101,(void*)0},{(void*)0,&g_101,(void*)0},{&g_101,(void*)0,(void*)0},{&g_101,&g_101,&g_101}},{{&g_101,&g_101,&g_101},{(void*)0,&g_101,(void*)0},{&g_101,&g_101,(void*)0},{(void*)0,&g_101,(void*)0},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{(void*)0,(void*)0,(void*)0},{&g_101,&g_101,&g_101},{&g_101,&g_101,(void*)0}},{{&g_101,&g_101,(void*)0},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{(void*)0,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101},{&g_101,&g_101,(void*)0}}};
static uint8_t g_104 = 0x7CL;
static float g_130 = 0xE.9996A1p+96;
static float * volatile g_129 = &g_130;/* VOLATILE GLOBAL g_129 */
static int32_t g_140 = 1L;
static int32_t g_143 = 0xB3886849L;
static int32_t *g_142[6] = {&g_143,&g_143,&g_143,&g_143,&g_143,&g_143};
static volatile int32_t * volatile * volatile *g_144 = (void*)0;
static volatile int32_t * volatile * volatile ** volatile g_145 = &g_144;/* VOLATILE GLOBAL g_145 */
static volatile union U1 g_158 = {0x9FAFL};/* VOLATILE GLOBAL g_158 */
static uint32_t g_170 = 4294967295UL;
static int32_t g_179[7] = {6L,6L,6L,6L,6L,6L,6L};
static volatile union U1 g_246 = {0x2EE7L};/* VOLATILE GLOBAL g_246 */
static const int32_t g_255[2] = {0L,0L};
static const int32_t *g_254 = &g_255[1];
static const int32_t **g_253 = &g_254;
static int8_t g_260[6][9] = {{0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L},{0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L},{0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L},{0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L},{0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L,0x52L},{0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L,0xC9L}};
static volatile union U1 g_271 = {4L};/* VOLATILE GLOBAL g_271 */
static uint32_t g_287 = 18446744073709551615UL;
static int64_t g_310 = 7L;
static float * volatile g_336 = &g_130;/* VOLATILE GLOBAL g_336 */
static int32_t **g_343 = &g_142[2];
static int32_t ***g_342 = &g_343;
static int32_t ****g_341 = &g_342;
static int32_t ***** volatile g_340 = &g_341;/* VOLATILE GLOBAL g_340 */
static int32_t **g_354 = (void*)0;
static float * volatile g_365 = &g_130;/* VOLATILE GLOBAL g_365 */
static uint64_t g_371[7][9][3] = {{{18446744073709551615UL,18446744073709551607UL,0x7FCC671885BE153DLL},{0xC2B7E83EE8106C00LL,0xE7BD56AAE49ED9ADLL,18446744073709551607UL},{7UL,0UL,0x4A64224211670AD5LL},{0xE4B67363E014B914LL,0x070F7F89DB1F5828LL,0UL},{18446744073709551615UL,0x68401850DD78E7BELL,18446744073709551613UL},{0xE7BD56AAE49ED9ADLL,18446744073709551610UL,18446744073709551615UL},{18446744073709551610UL,0xD8008BCAF08113AELL,0x718A494BD68177CFLL},{0xC7BBDCE94BC582DDLL,18446744073709551606UL,0xC2B7E83EE8106C00LL},{18446744073709551615UL,18446744073709551606UL,18446744073709551615UL}},{{0UL,0xD8008BCAF08113AELL,18446744073709551615UL},{0UL,18446744073709551610UL,0xE4B67363E014B914LL},{0x42C6116AD5E1A435LL,0x68401850DD78E7BELL,18446744073709551615UL},{0x070F7F89DB1F5828LL,0x070F7F89DB1F5828LL,0x263A5E84BC4E430CLL},{0x3F57216A0719E04ELL,0UL,1UL},{0x8699DD6A417C5D52LL,0xE7BD56AAE49ED9ADLL,0xD8008BCAF08113AELL},{18446744073709551615UL,18446744073709551607UL,3UL},{0x7FCC671885BE153DLL,0x8699DD6A417C5D52LL,0xD8008BCAF08113AELL},{0x51CD8B1141D881F4LL,0x51179202AC2AD42ALL,1UL}},{{0x68401850DD78E7BELL,1UL,0x263A5E84BC4E430CLL},{0xD8008BCAF08113AELL,0x7FCC671885BE153DLL,18446744073709551615UL},{9UL,18446744073709551615UL,0xE4B67363E014B914LL},{0x718A494BD68177CFLL,9UL,18446744073709551615UL},{0x4DC235FE0AB0D4C8LL,0xA2F4FB3A054CF859LL,18446744073709551615UL},{0x4A64224211670AD5LL,1UL,0xC2B7E83EE8106C00LL},{0x4A64224211670AD5LL,18446744073709551615UL,0x718A494BD68177CFLL},{0x4DC235FE0AB0D4C8LL,0x51CD8B1141D881F4LL,18446744073709551615UL},{0x718A494BD68177CFLL,0xAF469C46D6A43C1CLL,18446744073709551613UL}},{{9UL,0UL,0UL},{0xD8008BCAF08113AELL,0x42C6116AD5E1A435LL,0x4A64224211670AD5LL},{0x68401850DD78E7BELL,4UL,18446744073709551607UL},{0x51CD8B1141D881F4LL,3UL,0x7FCC671885BE153DLL},{0x7FCC671885BE153DLL,0x718A494BD68177CFLL,0xA2F4FB3A054CF859LL},{18446744073709551615UL,3UL,18446744073709551615UL},{0x8699DD6A417C5D52LL,4UL,0x51179202AC2AD42ALL},{0x3F57216A0719E04ELL,0x42C6116AD5E1A435LL,18446744073709551615UL},{0x070F7F89DB1F5828LL,18446744073709551615UL,3UL}},{{18446744073709551615UL,0x51CD8B1141D881F4LL,0UL},{18446744073709551615UL,0x718A494BD68177CFLL,0xE4B67363E014B914LL},{18446744073709551615UL,3UL,0x51CD8B1141D881F4LL},{0x3F57216A0719E04ELL,1UL,0x51CD8B1141D881F4LL},{0UL,18446744073709551615UL,0xE4B67363E014B914LL},{18446744073709551615UL,1UL,0UL},{18446744073709551610UL,0xE4B67363E014B914LL,3UL},{0x42C6116AD5E1A435LL,0xD8008BCAF08113AELL,0x42C6116AD5E1A435LL},{18446744073709551615UL,1UL,18446744073709551615UL}},{{0xE2F1878CFAC56C3ALL,18446744073709551615UL,0xC7BBDCE94BC582DDLL},{0x4A64224211670AD5LL,3UL,18446744073709551615UL},{18446744073709551613UL,0x070F7F89DB1F5828LL,0xD8008BCAF08113AELL},{0x4A64224211670AD5LL,18446744073709551610UL,0x070F7F89DB1F5828LL},{0xE2F1878CFAC56C3ALL,9UL,0x13AAD4235F6A586CLL},{18446744073709551615UL,0x4DC235FE0AB0D4C8LL,18446744073709551615UL},{0x42C6116AD5E1A435LL,0UL,4UL},{18446744073709551610UL,18446744073709551615UL,0xC2B7E83EE8106C00LL},{18446744073709551615UL,0xAF469C46D6A43C1CLL,0x885816FB09E31F1BLL}},{{0UL,2UL,0x4A64224211670AD5LL},{0x3F57216A0719E04ELL,2UL,0x610EC7228BB2A369LL},{18446744073709551615UL,0xAF469C46D6A43C1CLL,3UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,0UL,18446744073709551613UL},{0x4DC235FE0AB0D4C8LL,0x4DC235FE0AB0D4C8LL,7UL},{18446744073709551607UL,9UL,1UL},{3UL,18446744073709551610UL,0xAF469C46D6A43C1CLL},{0xC2B7E83EE8106C00LL,0x070F7F89DB1F5828LL,0x68401850DD78E7BELL}}};
static uint64_t * const  volatile g_370 = &g_371[3][8][0];/* VOLATILE GLOBAL g_370 */
static float * volatile g_376 = &g_130;/* VOLATILE GLOBAL g_376 */
static int32_t **** volatile g_377 = (void*)0;/* VOLATILE GLOBAL g_377 */
static int32_t **** volatile g_378 = &g_342;/* VOLATILE GLOBAL g_378 */
static volatile int32_t *g_397 = &g_76;
static volatile int32_t **g_396 = &g_397;
static volatile int32_t ** volatile *g_395 = &g_396;
static volatile int32_t ** volatile * volatile * volatile g_394 = &g_395;/* VOLATILE GLOBAL g_394 */
static volatile int32_t ** volatile * volatile * volatile * volatile g_393 = &g_394;/* VOLATILE GLOBAL g_393 */
static volatile union U0 g_407 = {0xA5C8L};/* VOLATILE GLOBAL g_407 */
static union U1 g_435 = {-1L};/* VOLATILE GLOBAL g_435 */
static volatile union U1 g_436 = {0x870CL};/* VOLATILE GLOBAL g_436 */
static float g_452[7] = {0xC.F20EEFp-66,0xC.F20EEFp-66,0xC.F20EEFp-66,0xC.F20EEFp-66,0xC.F20EEFp-66,0xC.F20EEFp-66,0xC.F20EEFp-66};
static volatile int8_t g_479[3] = {0xCDL,0xCDL,0xCDL};
static volatile int8_t *g_478 = &g_479[0];
static volatile int8_t **g_477 = &g_478;
static volatile int8_t *** volatile g_476 = &g_477;/* VOLATILE GLOBAL g_476 */
static volatile int8_t *** volatile * const  volatile g_480 = &g_476;/* VOLATILE GLOBAL g_480 */
static float * const  volatile g_483 = &g_452[2];/* VOLATILE GLOBAL g_483 */
static int64_t g_548 = (-1L);
static volatile uint32_t g_571 = 0x43FBEE75L;/* VOLATILE GLOBAL g_571 */
static int16_t g_592[2] = {0x4C0FL,0x4C0FL};
static union U1 g_599 = {0L};/* VOLATILE GLOBAL g_599 */
static union U1 *g_598 = &g_599;
static union U1 *g_601 = (void*)0;
static union U1 ** volatile g_600 = &g_601;/* VOLATILE GLOBAL g_600 */
static int16_t g_616 = 1L;
static union U1 g_617 = {0xE115L};/* VOLATILE GLOBAL g_617 */
static volatile union U1 g_653 = {-7L};/* VOLATILE GLOBAL g_653 */
static union U0 g_717 = {7L};/* VOLATILE GLOBAL g_717 */
static union U0 *g_719[3] = {&g_717,&g_717,&g_717};
static union U0 ** volatile g_718 = &g_719[2];/* VOLATILE GLOBAL g_718 */
static union U1 ** volatile *g_721 = (void*)0;
static union U1 ** volatile ** volatile g_720 = &g_721;/* VOLATILE GLOBAL g_720 */
static uint16_t g_741 = 65535UL;
static uint64_t * const  volatile g_792 = (void*)0;/* VOLATILE GLOBAL g_792 */
static uint64_t * const  volatile *g_791 = &g_792;
static volatile union U0 g_802 = {-10L};/* VOLATILE GLOBAL g_802 */
static union U1 g_829 = {1L};/* VOLATILE GLOBAL g_829 */
static uint8_t g_834 = 0x52L;
static int32_t ***g_842 = &g_354;
static int32_t *** const *g_841 = &g_842;
static int32_t *** const **g_840 = &g_841;
static int32_t **** const *g_931 = (void*)0;
static volatile int16_t *g_938 = &g_271.f0;
static volatile int16_t * const  volatile * volatile g_937 = &g_938;/* VOLATILE GLOBAL g_937 */
static int8_t *g_950[4] = {&g_68,&g_68,&g_68,&g_68};
static const int32_t ***g_955 = &g_253;
static const int32_t ****g_954 = &g_955;
static const int32_t *****g_953[7][6][4] = {{{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,&g_954,(void*)0},{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,&g_954},{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,(void*)0}},{{&g_954,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,(void*)0},{&g_954,&g_954,&g_954,&g_954},{(void*)0,&g_954,(void*)0,&g_954},{&g_954,&g_954,&g_954,(void*)0},{(void*)0,&g_954,&g_954,&g_954}},{{&g_954,&g_954,&g_954,(void*)0},{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,&g_954},{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,(void*)0},{&g_954,&g_954,&g_954,&g_954}},{{&g_954,&g_954,(void*)0,(void*)0},{&g_954,&g_954,&g_954,&g_954},{(void*)0,&g_954,(void*)0,&g_954},{&g_954,&g_954,&g_954,(void*)0},{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,&g_954,&g_954}},{{(void*)0,&g_954,&g_954,&g_954},{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,&g_954},{&g_954,&g_954,(void*)0,&g_954},{&g_954,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,&g_954}},{{&g_954,&g_954,(void*)0,&g_954},{&g_954,(void*)0,&g_954,&g_954},{(void*)0,&g_954,&g_954,&g_954},{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,&g_954,&g_954},{(void*)0,&g_954,&g_954,&g_954}},{{(void*)0,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,&g_954},{&g_954,&g_954,(void*)0,&g_954},{&g_954,&g_954,&g_954,&g_954},{&g_954,&g_954,(void*)0,&g_954},{&g_954,&g_954,(void*)0,&g_954}}};
static union U1 g_982 = {0x2616L};/* VOLATILE GLOBAL g_982 */
static union U1 g_1249 = {0xEA7CL};/* VOLATILE GLOBAL g_1249 */
static int16_t g_1313 = 0x503EL;
static int32_t ****g_1336 = (void*)0;
static int32_t *****g_1335[9][10] = {{&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336},{(void*)0,&g_1336,(void*)0,&g_1336,(void*)0,&g_1336,(void*)0,&g_1336,(void*)0,&g_1336},{&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336},{(void*)0,&g_1336,&g_1336,&g_1336,(void*)0,&g_1336,&g_1336,&g_1336,(void*)0,&g_1336},{&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336},{(void*)0,&g_1336,(void*)0,&g_1336,(void*)0,&g_1336,(void*)0,&g_1336,(void*)0,&g_1336},{&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336},{(void*)0,&g_1336,&g_1336,&g_1336,(void*)0,&g_1336,&g_1336,&g_1336,(void*)0,&g_1336},{&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336,&g_1336}};
static union U1 * const * const *g_1341 = (void*)0;
static union U1 * const * const **g_1340 = &g_1341;
static volatile union U1 g_1352 = {-1L};/* VOLATILE GLOBAL g_1352 */
static int32_t *****g_1353 = &g_341;
static union U1 ****g_1361 = (void*)0;
static union U1 **g_1365[5][1][2] = {{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}}};
static union U1 ***g_1364 = &g_1365[4][0][0];
static union U1 ****g_1363 = &g_1364;
static union U1 ***** volatile g_1362[9] = {&g_1363,&g_1363,&g_1363,&g_1363,&g_1363,&g_1363,&g_1363,&g_1363,&g_1363};
static union U1 ***** volatile g_1366 = &g_1363;/* VOLATILE GLOBAL g_1366 */
static union U0 g_1373 = {0x0252L};/* VOLATILE GLOBAL g_1373 */
static const int8_t g_1379[4][2][7] = {{{0x01L,(-9L),0x64L,(-5L),(-9L),(-9L),(-5L)},{0xE6L,2L,0xE6L,0xEDL,(-1L),0x64L,0xA1L}},{{(-5L),0x01L,3L,3L,0x01L,(-5L),0x01L},{0xE6L,0xEDL,(-1L),0x64L,0xA1L,0x64L,(-1L)}},{{(-9L),(-9L),(-5L),3L,(-5L),(-9L),(-9L)},{(-6L),0xEDL,0L,0xEDL,(-6L),4L,(-1L)}},{{0x64L,0x01L,0x64L,(-5L),(-5L),0x64L,0x01L},{(-1L),2L,0L,(-3L),0xA1L,0xEDL,0xA1L}}};
static const int8_t g_1381 = 0x5EL;
static const int8_t *g_1380 = &g_1381;
static volatile union U0 g_1394 = {0xDBD0L};/* VOLATILE GLOBAL g_1394 */
static union U0 g_1480 = {4L};/* VOLATILE GLOBAL g_1480 */
static union U0 **g_1486 = &g_719[2];
static union U0 ***g_1485 = &g_1486;
static uint64_t *g_1494[7][3] = {{(void*)0,(void*)0,&g_371[4][6][0]},{(void*)0,(void*)0,&g_371[4][6][0]},{(void*)0,(void*)0,&g_371[4][6][0]},{(void*)0,(void*)0,&g_371[4][6][0]},{(void*)0,(void*)0,&g_371[4][6][0]},{(void*)0,(void*)0,&g_371[4][6][0]},{(void*)0,(void*)0,&g_371[4][6][0]}};
static union U0 g_1495 = {0x26A8L};/* VOLATILE GLOBAL g_1495 */
static uint32_t **g_1499 = &g_18;
static uint32_t **g_1500[9][9] = {{(void*)0,&g_18,&g_18,(void*)0,&g_18,&g_18,&g_18,(void*)0,&g_18},{(void*)0,&g_18,(void*)0,(void*)0,&g_18,(void*)0,&g_18,&g_18,&g_18},{(void*)0,&g_18,(void*)0,&g_18,&g_18,(void*)0,&g_18,(void*)0,&g_18},{&g_18,(void*)0,(void*)0,&g_18,&g_18,&g_18,&g_18,(void*)0,(void*)0},{&g_18,(void*)0,&g_18,&g_18,&g_18,&g_18,&g_18,&g_18,&g_18},{&g_18,&g_18,&g_18,&g_18,&g_18,&g_18,(void*)0,(void*)0,&g_18},{&g_18,(void*)0,&g_18,(void*)0,&g_18,&g_18,&g_18,&g_18,&g_18},{(void*)0,(void*)0,&g_18,&g_18,&g_18,(void*)0,(void*)0,&g_18,&g_18},{(void*)0,&g_18,(void*)0,&g_18,&g_18,&g_18,(void*)0,&g_18,(void*)0}};
static union U1 g_1545 = {0xBE5CL};/* VOLATILE GLOBAL g_1545 */
static union U0 ****g_1556 = &g_1485;
static union U0 ***** volatile g_1555[2][4][1] = {{{&g_1556},{&g_1556},{&g_1556},{&g_1556}},{{&g_1556},{&g_1556},{&g_1556},{&g_1556}}};
static union U0 ***** volatile g_1557 = (void*)0;/* VOLATILE GLOBAL g_1557 */
static volatile union U1 g_1614 = {0xA0DAL};/* VOLATILE GLOBAL g_1614 */
static union U1 g_1617 = {-3L};/* VOLATILE GLOBAL g_1617 */
static volatile union U1 g_1626 = {0x3D77L};/* VOLATILE GLOBAL g_1626 */
static volatile union U0 g_1627 = {0L};/* VOLATILE GLOBAL g_1627 */
static float * volatile g_1633 = &g_130;/* VOLATILE GLOBAL g_1633 */
static uint32_t * volatile *g_1646[8] = {&g_18,&g_18,&g_18,&g_18,&g_18,&g_18,&g_18,&g_18};
static uint32_t * volatile * const *g_1645[8] = {&g_1646[6],&g_1646[6],&g_1646[6],&g_1646[6],&g_1646[6],&g_1646[6],&g_1646[6],&g_1646[6]};
static uint32_t * volatile * const **g_1644 = &g_1645[6];
static uint32_t g_1648 = 18446744073709551611UL;
static volatile int16_t *** volatile g_1651[2] = {(void*)0,(void*)0};
static volatile int16_t **g_1653 = &g_938;
static volatile int16_t *** volatile g_1652[5][8] = {{&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653},{&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653},{&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653},{&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653},{&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653,&g_1653}};
static union U1 g_1660 = {0x61D0L};/* VOLATILE GLOBAL g_1660 */
static volatile int32_t g_1726 = 0xC6476F81L;/* VOLATILE GLOBAL g_1726 */
static volatile int32_t g_1731 = 0xCBAA599CL;/* VOLATILE GLOBAL g_1731 */
static union U0 g_1741 = {1L};/* VOLATILE GLOBAL g_1741 */
static union U0 g_1743 = {-1L};/* VOLATILE GLOBAL g_1743 */
static int8_t * volatile *g_1751[7][1][8] = {{{&g_950[1],&g_950[1],&g_950[2],(void*)0,&g_950[0],&g_950[2],(void*)0,&g_950[1]}},{{(void*)0,&g_950[0],&g_950[1],&g_950[1],(void*)0,&g_950[1],&g_950[1],&g_950[0]}},{{&g_950[1],(void*)0,(void*)0,&g_950[1],&g_950[1],&g_950[1],&g_950[1],&g_25}},{{(void*)0,(void*)0,&g_25,(void*)0,&g_950[1],&g_950[2],&g_950[1],&g_950[1]}},{{&g_25,(void*)0,(void*)0,&g_950[1],&g_950[0],&g_950[1],&g_950[1],&g_950[0]}},{{&g_950[0],&g_950[1],&g_950[1],&g_950[0],&g_950[1],(void*)0,(void*)0,&g_25}},{{&g_950[1],&g_950[1],&g_950[2],&g_950[1],(void*)0,&g_25,(void*)0,(void*)0}}};
static int8_t * volatile **g_1750[1] = {&g_1751[1][0][5]};
static int8_t * volatile *** const g_1749[8] = {&g_1750[0],&g_1750[0],&g_1750[0],&g_1750[0],&g_1750[0],&g_1750[0],&g_1750[0],&g_1750[0]};
static const int16_t g_1792 = 0x34DAL;
static int64_t g_1794 = 0x1261F840AF3B689DLL;
static int8_t g_1856 = 0x0BL;
static int16_t g_1863 = (-10L);


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static uint64_t  func_12(uint32_t * p_13, uint32_t * p_14, int16_t  p_15, uint32_t * const  p_16, uint32_t * p_17);
static int16_t  func_22(int8_t * p_23, int32_t  p_24);
static int32_t * func_31(int8_t * const  p_32);
static int8_t * func_33(int32_t * p_34, uint64_t  p_35, uint32_t * p_36, uint64_t  p_37, uint32_t * p_38);
static int32_t * func_39(uint16_t  p_40);
static uint16_t  func_41(int32_t * p_42, uint16_t  p_43, int32_t  p_44);
static int32_t * func_45(int32_t * p_46, int32_t * p_47, uint32_t  p_48, const int8_t * p_49);
static int32_t * func_50(int32_t  p_51, int8_t * p_52);
static int8_t * func_54(uint64_t  p_55);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_4 g_6 g_18 g_25 g_27 g_19 g_938 g_271.f0 g_370 g_371 g_955 g_253 g_254 g_396 g_397 g_76 g_343 g_142 g_72 g_143 g_840 g_841 g_842 g_354 g_1340 g_617.f1 g_394 g_395 g_1352 g_1366 g_342 g_179 g_1353 g_341 g_1373 g_435 g_436 g_365 g_130 g_104 g_287 g_393 g_616 g_1394 g_260 g_478 g_479 g_829.f2 g_1313 g_378 g_834 g_476 g_477 g_101 g_617.f3 g_950 g_1249.f2 g_1480 g_617.f0 g_1485 g_717.f0 g_170 g_1499 g_592 g_954 g_87 g_68 g_1380 g_1381 g_600 g_601 g_1614 g_1617 g_1626 g_1627 g_802.f0 g_140 g_1633 g_937 g_1480.f3 g_1644 g_1648 g_548 g_310 g_741 g_1486 g_1749 g_340 g_717.f3 g_719 g_1556 g_255 g_1792 g_1794 g_435.f2 g_1741.f3 g_1545.f2
 * writes: g_6 g_27 g_19 g_1335 g_254 g_76 g_72 g_143 g_1340 g_617.f1 g_829.f1 g_1313 g_397 g_1353 g_340 g_87 g_1361 g_1363 g_548 g_140 g_287 g_101 g_142 g_452 g_130 g_1380 g_616 g_371 g_1373.f3 g_717.f0 g_834 g_592 g_68 g_260 g_1249.f2 g_1485 g_4 g_1480.f3 g_310 g_741 g_719 g_1617.f2 g_1660.f1 g_653.f0 g_1614.f1 g_435.f2 g_1545.f2
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    uint32_t *l_5 = &g_6;
    const int32_t l_11[6][10][1] = {{{0xC18AB9C2L},{0x4A90C6D2L},{0L},{0x4A90C6D2L},{0xC18AB9C2L},{0L},{0L},{0L},{0xC18AB9C2L},{0x4A90C6D2L}},{{0L},{0x4A90C6D2L},{0xC18AB9C2L},{0L},{0L},{0L},{0xC18AB9C2L},{0x4A90C6D2L},{0L},{0x4A90C6D2L}},{{0xC18AB9C2L},{0L},{0L},{0L},{0xC18AB9C2L},{0x4A90C6D2L},{0L},{0x4A90C6D2L},{0xC18AB9C2L},{0L}},{{0L},{0L},{0xC18AB9C2L},{0x4A90C6D2L},{0L},{0x4A90C6D2L},{0xC18AB9C2L},{0L},{0L},{0L}},{{0xC18AB9C2L},{0x4A90C6D2L},{0L},{0x4A90C6D2L},{0xC18AB9C2L},{0L},{0L},{0L},{0xC18AB9C2L},{0x4A90C6D2L}},{{0L},{0x4A90C6D2L},{0xC18AB9C2L},{0L},{0L},{0L},{0xC18AB9C2L},{0x4A90C6D2L},{0L},{0x4A90C6D2L}}};
    int32_t l_1339 = (-1L);
    int32_t l_1344[10][10][2] = {{{(-1L),4L},{7L,0x1E3CF879L},{0x99EFB632L,0xC08128DFL},{9L,0x99EFB632L},{0xCF55335CL,0xB64F4CF8L},{0xCF55335CL,0x99EFB632L},{9L,0xC08128DFL},{0x99EFB632L,0x1E3CF879L},{7L,4L},{(-1L),(-3L)}},{{(-3L),5L},{0L,0x181023E4L},{0L,2L},{0x129AB6F6L,0xC7943D01L},{0xF80DF10DL,0x3DC453B9L},{0xB1DE60BAL,(-1L)},{0L,(-1L)},{0x15B7C2D6L,0x51A42B54L},{(-1L),0xB64F4CF8L},{0x1E3CF879L,0x4D632F3CL}},{{0xE031D692L,7L},{1L,(-5L)},{0xB1DE60BAL,0x94894538L},{1L,0x770DDB8BL},{0xFABFB1D7L,0x15B7C2D6L},{7L,0x271109EEL},{0x181023E4L,0xA547D6D8L},{(-5L),(-1L)},{0x271109EEL,0x1E3CF879L},{0xC7943D01L,1L}},{{(-3L),0xA0CD876BL},{1L,0xA0CD876BL},{(-3L),1L},{0xC7943D01L,0x1E3CF879L},{0x271109EEL,(-1L)},{(-5L),0xA547D6D8L},{0x181023E4L,0x271109EEL},{7L,0x15B7C2D6L},{0xFABFB1D7L,0x770DDB8BL},{1L,0x94894538L}},{{0xB1DE60BAL,(-5L)},{1L,7L},{0xE031D692L,0x4D632F3CL},{0x1E3CF879L,0xB64F4CF8L},{(-1L),0x51A42B54L},{0xC2C070B6L,0x99EFB632L},{0L,0x6F985105L},{(-1L),0xC16BF1A1L},{0xCF55335CL,(-1L)},{0L,0L}},{{0x129AB6F6L,1L},{0xB64F4CF8L,0xB1DE60BAL},{(-1L),(-1L)},{0x6F985105L,0xF80DF10DL},{0xA547D6D8L,(-3L)},{1L,0xFABFB1D7L},{8L,1L},{2L,4L},{2L,1L},{8L,0xFABFB1D7L}},{{1L,(-3L)},{0xA547D6D8L,0xF80DF10DL},{0x6F985105L,(-1L)},{(-1L),0xB1DE60BAL},{0xB64F4CF8L,1L},{0x129AB6F6L,0L},{0L,(-1L)},{0xCF55335CL,0xC16BF1A1L},{(-1L),0x6F985105L},{0L,0x99EFB632L}},{{0xC2C070B6L,0x51A42B54L},{(-1L),0xB64F4CF8L},{0x1E3CF879L,0x4D632F3CL},{0xE031D692L,7L},{1L,(-5L)},{0xB1DE60BAL,0x94894538L},{1L,0x770DDB8BL},{0xFABFB1D7L,0x15B7C2D6L},{7L,0x271109EEL},{0x181023E4L,0xA547D6D8L}},{{(-5L),(-1L)},{0x271109EEL,0x1E3CF879L},{0xC7943D01L,1L},{(-3L),0xA0CD876BL},{1L,0xA0CD876BL},{(-3L),1L},{0xC7943D01L,0x1E3CF879L},{0x271109EEL,(-1L)},{(-5L),0xA547D6D8L},{0x181023E4L,0x271109EEL}},{{7L,0x15B7C2D6L},{0xFABFB1D7L,0x770DDB8BL},{1L,0x94894538L},{0xB1DE60BAL,(-5L)},{1L,7L},{0xE031D692L,0x4D632F3CL},{0x1E3CF879L,0xB64F4CF8L},{(-1L),0x51A42B54L},{0xC2C070B6L,0x99EFB632L},{0L,0x6F985105L}}};
    int32_t *****l_1354 = &g_341;
    int16_t l_1412 = 0L;
    int32_t l_1417 = 1L;
    uint64_t *l_1496 = &g_371[3][8][0];
    union U1 *** const **l_1527 = (void*)0;
    const uint32_t *l_1552[3][10] = {{&g_6,&g_170,&g_6,&g_170,&g_6,&g_170,&g_6,&g_170,&g_6,&g_170},{&g_6,&g_170,&g_6,&g_170,&g_6,&g_170,&g_6,&g_170,&g_6,&g_170},{&g_6,&g_170,&g_6,&g_170,&g_6,&g_170,&g_6,&g_170,&g_6,&g_170}};
    const uint32_t **l_1551 = &l_1552[0][1];
    const uint32_t ***l_1550 = &l_1551;
    int32_t ****l_1562 = (void*)0;
    union U0 ****l_1586 = &g_1485;
    int64_t l_1705[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    int32_t l_1718 = 0x5B1C00F6L;
    float l_1727 = 0x2.1C37F4p-46;
    float l_1729 = 0x4.F51006p+37;
    int16_t l_1730 = 0xB0F6L;
    union U0 *l_1742 = &g_1743;
    uint16_t l_1831 = 0x4194L;
    uint8_t l_1869 = 0x87L;
    int i, j, k;
    l_1339 = ((**g_343) &= ((safe_rshift_func_uint16_t_u_u(g_4, (((++(*l_5)) <= (safe_add_func_uint64_t_u_u(l_11[5][9][0], func_12(g_18, l_5, (safe_rshift_func_int16_t_s_s((l_11[0][1][0] ^ func_22(g_25, l_11[5][9][0])), 3)), &g_170, &g_170)))) > 1UL))) < 6L));
    if (((void*)0 == (***g_840)))
    { /* block id: 592 */
        union U1 * const * const ***l_1342[9];
        float l_1343 = 0x1.817471p+66;
        int32_t l_1347 = 0xCD9C359DL;
        int32_t l_1348 = 0x9A599990L;
        const int8_t *l_1376 = &g_68;
        int32_t l_1452 = 0xC64C0D03L;
        uint32_t l_1467 = 4294967295UL;
        uint64_t l_1474 = 1UL;
        union U0 **l_1482 = &g_719[2];
        union U0 ***l_1481 = &l_1482;
        int32_t l_1512 = 0L;
        int32_t l_1594 = 0x839966B2L;
        uint32_t l_1595 = 18446744073709551615UL;
        int32_t **** const *l_1601 = &g_341;
        volatile int16_t **l_1654 = &g_938;
        uint32_t **l_1679[6];
        int32_t l_1720 = (-1L);
        int32_t l_1721 = (-1L);
        int32_t l_1722[3][5][10] = {{{0x0AAA99F6L,0xE111378DL,1L,0xF534B8A8L,0x95BB9E5DL,0x0AAA99F6L,1L,0x2854B86EL,0x8CD3B9C5L,0x1AD9E5F2L},{1L,0x67774D37L,0x76DDEDDBL,0x7DCE2C6AL,1L,0x16411B21L,1L,0x7DCE2C6AL,0x76DDEDDBL,0x67774D37L},{1L,0x1AD9E5F2L,0x8CD3B9C5L,0x2854B86EL,1L,0x0AAA99F6L,0x95BB9E5DL,0xF534B8A8L,1L,0xE111378DL},{0x0AAA99F6L,0x95BB9E5DL,0xF534B8A8L,1L,0xE111378DL,0x0AAA99F6L,0xA1E5B6A6L,0L,4L,0xA1E5B6A6L},{1L,1L,0x1DB0E93DL,0L,1L,0x16411B21L,0xE111378DL,0xC605B2F1L,0x684F8E68L,0x95BB9E5DL}},{{1L,(-1L),1L,(-6L),0L,0x0AAA99F6L,0x67774D37L,(-1L),0x8CD3B9C5L,1L},{0x0AAA99F6L,0x67774D37L,(-1L),0x8CD3B9C5L,1L,0x0AAA99F6L,0x1AD9E5F2L,0x7DCE2C6AL,(-1L),1L},{1L,1L,(-1L),0x2854B86EL,0x67774D37L,0x16411B21L,0x95BB9E5DL,(-6L),0xE10EAEA9L,0xE111378DL},{1L,0L,0xF534B8A8L,0xC605B2F1L,(-1L),0x0AAA99F6L,1L,4L,4L,1L},{0x0AAA99F6L,1L,4L,4L,1L,0x0AAA99F6L,(-1L),0xC605B2F1L,0xF534B8A8L,0L}},{{1L,0xE111378DL,0xE10EAEA9L,(-6L),0x95BB9E5DL,0x16411B21L,0x67774D37L,0x2854B86EL,(-1L),1L},{1L,1L,(-1L),0x7DCE2C6AL,0x1AD9E5F2L,0x0AAA99F6L,1L,0x8CD3B9C5L,(-1L),0x67774D37L},{0x0AAA99F6L,1L,0x8CD3B9C5L,(-1L),0x67774D37L,0x0AAA99F6L,0L,(-6L),1L,(-1L)},{1L,0x95BB9E5DL,0x684F8E68L,0xC605B2F1L,0xE111378DL,0x16411B21L,1L,0L,0x1DB0E93DL,1L},{1L,0xA1E5B6A6L,4L,0L,0xA1E5B6A6L,0x0AAA99F6L,0xE111378DL,1L,0xF534B8A8L,0x95BB9E5DL}}};
        float l_1732 = 0xE.249657p-23;
        int32_t l_1733 = (-1L);
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_1342[i] = &g_1340;
        for (i = 0; i < 6; i++)
            l_1679[i] = &l_5;
        (**g_343) |= (65535UL >= ((g_1340 = g_1340) == &g_721));
        for (g_617.f1 = 0; (g_617.f1 <= 2); g_617.f1 += 1)
        { /* block id: 597 */
            uint32_t l_1349 = 0x36D9E36DL;
            int8_t **l_1367 = &g_950[3];
            int32_t *l_1375 = &l_1339;
            const int8_t *l_1378 = &g_1379[1][1][0];
            uint64_t l_1413[5][10][1] = {{{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL},{1UL}}};
            int64_t l_1465[2];
            int32_t l_1466[1];
            int8_t l_1488[7] = {0xEBL,0xEBL,0xEBL,0xEBL,0xEBL,0xEBL,0xEBL};
            uint64_t l_1542 = 0x319472C48DD930F2LL;
            union U1 *l_1544 = &g_1545;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1465[i] = 0x9DD32D279E8EEE77LL;
            for (i = 0; i < 1; i++)
                l_1466[i] = (-3L);
            for (g_829.f1 = 0; (g_829.f1 <= 2); g_829.f1 += 1)
            { /* block id: 600 */
                int32_t *l_1345 = (void*)0;
                int32_t *l_1346[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_1346[i] = &l_1339;
                l_1349--;
                for (g_1313 = 2; (g_1313 <= 6); g_1313 += 1)
                { /* block id: 604 */
                    union U1 **l_1357[9] = {&g_598,&g_598,&g_598,&g_598,&g_598,&g_598,&g_598,&g_598,&g_598};
                    union U1 ***l_1356[8] = {&l_1357[7],&l_1357[7],&l_1357[7],&l_1357[7],&l_1357[7],&l_1357[7],&l_1357[7],&l_1357[7]};
                    union U1 ****l_1355 = &l_1356[6];
                    int i;
                    (**g_395) = (***g_394);
                    g_340 = (l_1354 = (g_1352 , (g_1353 = &g_341)));
                    for (g_87 = 0; (g_87 <= 2); g_87 += 1)
                    { /* block id: 611 */
                        union U1 *****l_1358 = (void*)0;
                        union U1 *****l_1359 = (void*)0;
                        union U1 *****l_1360 = &l_1355;
                        (*g_1366) = (g_1361 = ((*l_1360) = l_1355));
                    }
                    (**g_343) ^= ((void*)0 != l_1367);
                }
                (***g_342) = 0xA7AF64F3L;
                for (g_548 = 0; (g_548 <= 2); g_548 += 1)
                { /* block id: 621 */
                    int32_t l_1368 = (-3L);
                    int i;
                    (*****g_1353) = g_179[(g_617.f1 + 3)];
                    for (g_140 = 0; (g_140 <= 2); g_140 += 1)
                    { /* block id: 625 */
                        uint32_t l_1369[8][10] = {{9UL,9UL,0x944E6A69L,18446744073709551608UL,0x6A92799AL,18446744073709551608UL,0x944E6A69L,9UL,9UL,0x944E6A69L},{0UL,18446744073709551608UL,9UL,9UL,18446744073709551608UL,0UL,0x944E6A69L,0UL,18446744073709551608UL,9UL},{0UL,9UL,0UL,9UL,0x944E6A69L,0x944E6A69L,9UL,0UL,9UL,0UL},{0UL,0UL,9UL,18446744073709551608UL,9UL,0UL,0UL,0UL,0UL,9UL},{0UL,0UL,0UL,0UL,9UL,18446744073709551608UL,9UL,0UL,0UL,0UL},{9UL,0UL,9UL,0x944E6A69L,0x944E6A69L,9UL,0UL,9UL,0UL,9UL},{18446744073709551608UL,0UL,0x944E6A69L,0UL,18446744073709551608UL,9UL,9UL,18446744073709551608UL,0UL,0x944E6A69L},{9UL,9UL,0x944E6A69L,18446744073709551608UL,0x6A92799AL,18446744073709551608UL,0x944E6A69L,9UL,9UL,0x944E6A69L}};
                        int i, j;
                        l_1369[5][6]++;
                    }
                    for (g_287 = 0; (g_287 <= 2); g_287 += 1)
                    { /* block id: 630 */
                        int16_t *l_1374[7][1] = {{&g_616},{&g_616},{&g_1373.f0},{&g_616},{&g_616},{&g_1373.f0},{&g_616}};
                        int i, j;
                        (*g_343) = func_45((**g_342), (****l_1354), (((*****l_1354) = (+(g_1373 , ((void*)0 != &g_101)))) ^ 0L), &g_68);
                    }
                }
            }
            if (g_179[(g_617.f1 + 1)])
                continue;
            for (g_143 = 2; (g_143 >= 0); g_143 -= 1)
            { /* block id: 639 */
                const int8_t **l_1377[2];
                union U1 *****l_1400 = &g_1363;
                int32_t l_1404[8][6] = {{4L,0L,0L,4L,0L,0L},{4L,0L,0L,4L,0L,0L},{4L,0L,0L,4L,0L,0L},{4L,0L,0L,4L,0L,0L},{4L,0L,0L,4L,0L,0L},{4L,0L,0L,4L,0L,0L},{4L,0L,0L,4L,0L,0L},{4L,0L,0L,4L,0L,0L}};
                uint32_t l_1541[5][1] = {{0xDE1159DDL},{1UL},{0xDE1159DDL},{1UL},{0xDE1159DDL}};
                union U0 ****l_1559 = &l_1481;
                int i, j;
                for (i = 0; i < 2; i++)
                    l_1377[i] = (void*)0;
                if ((*****g_393))
                    break;
                (*g_343) = func_45((***g_341), l_1375, ((*l_5) = l_1348), (g_1380 = (l_1378 = l_1376)));
                for (g_616 = 0; (g_616 <= 2); g_616 += 1)
                { /* block id: 647 */
                    union U1 **l_1401 = (void*)0;
                    uint8_t *l_1403[9][3][1] = {{{&g_834},{&g_1373.f3},{(void*)0}},{{&g_1373.f3},{(void*)0},{(void*)0}},{{(void*)0},{&g_1373.f3},{&g_834}},{{&g_1373.f3},{(void*)0},{(void*)0}},{{(void*)0},{&g_1373.f3},{&g_834}},{{&g_1373.f3},{(void*)0},{(void*)0}},{{(void*)0},{&g_1373.f3},{&g_834}},{{&g_1373.f3},{(void*)0},{(void*)0}},{{(void*)0},{&g_1373.f3},{&g_834}}};
                    float *l_1409 = (void*)0;
                    float *l_1410 = (void*)0;
                    float *l_1411 = &g_130;
                    int32_t l_1418[1];
                    uint16_t l_1421 = 0UL;
                    int32_t *l_1422 = &g_27[0][5];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_1418[i] = 0x3FE28A9DL;
                    for (g_287 = 0; (g_287 <= 2); g_287 += 1)
                    { /* block id: 650 */
                        int i, j, k;
                        return g_371[(g_143 + 2)][g_617.f1][g_616];
                    }
                    (***g_395) = (((safe_add_func_int16_t_s_s((g_717.f0 = (safe_rshift_func_uint16_t_u_u(((((safe_add_func_int16_t_s_s((0x496EEBE1L | ((((((((safe_sub_func_int64_t_s_s(((g_371[(g_616 + 2)][(g_617.f1 + 4)][g_143] , (safe_mod_func_int16_t_s_s(((g_371[g_616][(g_616 + 4)][g_617.f1]++) , (g_1394 , ((safe_sub_func_uint32_t_u_u((+((void*)0 == l_1400)), (l_1401 != (void*)0))) ^ ((!(((l_1404[5][5] = (g_1373.f3 = (l_1348 = (*****l_1354)))) != ((safe_mul_func_float_f_f(((safe_sub_func_float_f_f(((*l_1411) = 0x4.6F583Ap+76), 0x2.0p-1)) >= (-0x5.Dp+1)), 0x6.073E2Dp+6)) , g_371[(g_616 + 2)][(g_617.f1 + 4)][g_143])) && 0xD7B5F67BL)) >= l_1347)))), 0x2628L))) != 0x9F22D95EL), (*l_1375))) , g_371[g_616][(g_616 + 4)][g_617.f1]) != 0xFEL) , 0xE6E3EA19278A226FLL) && 18446744073709551610UL) < l_1412) <= g_260[5][7]) <= g_143)), 6L)) ^ 4294967295UL) & 0x5387DFACL) < l_1347), 12))), 0xA655L)) & l_1413[1][9][0]) >= (*****g_1353));
                    (*l_1422) |= (((*l_5) = (((((g_371[(g_616 + 2)][(g_143 + 4)][g_143] = ((safe_sub_func_uint32_t_u_u((l_1404[5][5] || (&g_18 != &g_18)), (-4L))) , (~l_1417))) != ((((*g_478) > l_1418[0]) || (0x65L && (safe_lshift_func_int8_t_s_u((((0xA.8AB19Cp+57 <= l_1418[0]) , (*l_1375)) , l_1347), g_829.f2)))) , l_1421)) , l_1418[0]) != g_1313) != 0x0B2909D10448653DLL)) & 1L);
                    if (((safe_unary_minus_func_uint32_t_u((*l_1422))) | (*l_1422)))
                    { /* block id: 663 */
                        (**g_955) = &l_1418[0];
                    }
                    else
                    { /* block id: 665 */
                        int16_t *l_1448 = (void*)0;
                        int16_t *l_1449 = &g_592[1];
                        const int32_t l_1450 = 0L;
                        uint16_t *l_1451[9][10][2] = {{{&g_87,&g_741},{&l_1421,&l_1421},{&g_87,(void*)0},{(void*)0,&l_1421},{&g_101,&g_101},{(void*)0,&g_101},{&g_87,&g_741},{&g_87,&g_101},{(void*)0,&g_101},{&g_101,&l_1421}},{{(void*)0,(void*)0},{&g_87,&l_1421},{&l_1421,&g_741},{&g_87,&g_87},{&g_741,(void*)0},{(void*)0,(void*)0},{&g_741,&g_87},{&g_87,&g_741},{&l_1421,&l_1421},{&g_87,(void*)0}},{{(void*)0,&l_1421},{&g_101,&g_101},{(void*)0,&g_101},{&g_87,&g_741},{&g_87,&g_101},{(void*)0,&g_101},{&g_101,&l_1421},{(void*)0,(void*)0},{&g_87,&l_1421},{&l_1421,&g_741}},{{&g_87,&g_87},{&g_741,(void*)0},{(void*)0,(void*)0},{&g_741,&g_87},{&g_87,&g_741},{&l_1421,&l_1421},{&g_87,(void*)0},{(void*)0,&l_1421},{&g_101,&g_101},{(void*)0,&g_101}},{{&g_87,&g_741},{&g_87,&g_101},{(void*)0,&g_101},{&g_101,&l_1421},{(void*)0,(void*)0},{&g_87,&l_1421},{&l_1421,&g_741},{&g_87,&g_87},{&g_741,(void*)0},{(void*)0,(void*)0}},{{&g_741,&g_87},{&g_87,&g_741},{&l_1421,&l_1421},{&g_87,(void*)0},{(void*)0,&l_1421},{&g_101,&g_101},{(void*)0,&g_101},{&g_87,&g_741},{&g_87,&g_101},{(void*)0,&g_101}},{{&g_101,&l_1421},{(void*)0,(void*)0},{&g_87,&l_1421},{&l_1421,&g_741},{&g_87,&g_87},{&g_741,(void*)0},{(void*)0,(void*)0},{&g_741,&g_87},{&g_87,&g_741},{&l_1421,&l_1421}},{{&g_87,(void*)0},{(void*)0,&l_1421},{&g_101,&g_101},{(void*)0,&g_101},{&g_87,&g_741},{&g_87,&g_101},{(void*)0,&g_101},{&g_101,&l_1421},{(void*)0,(void*)0},{&g_87,&l_1421}},{{&l_1421,&g_741},{&g_87,&g_87},{&g_741,(void*)0},{(void*)0,&g_741},{&g_101,&g_741},{(void*)0,&g_87},{&g_87,&g_87},{&g_741,&g_87},{&l_1421,(void*)0},{&g_101,&l_1421}}};
                        int32_t *l_1453 = &g_72;
                        int32_t l_1454 = (-6L);
                        int32_t *l_1455 = &l_1418[0];
                        int32_t *l_1456 = &l_1344[9][0][1];
                        int32_t *l_1457 = &l_1454;
                        int32_t *l_1458 = &l_1339;
                        int32_t *l_1459 = &l_1454;
                        int32_t *l_1460 = &g_72;
                        int32_t *l_1461 = &l_1339;
                        int32_t *l_1462 = &l_1344[5][2][1];
                        int32_t *l_1463 = &l_1454;
                        int32_t *l_1464[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_1464[i] = &l_1404[5][5];
                        (***g_395) = (****g_378);
                        (*l_1453) |= (l_1452 = (safe_lshift_func_int8_t_s_s(((g_834++) && (safe_div_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_s((safe_mul_func_int8_t_s_s((l_1347 , (l_1348 = (-4L))), (safe_lshift_func_uint16_t_u_s((((l_1404[3][1] >= (0xC3L != ((**l_1367) = (((*g_476) != (((*l_1375) = (((**g_955) = func_50((safe_mod_func_int64_t_s_s((safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((0xA6644D225271BC2CLL > ((safe_mul_func_int16_t_s_s(g_101, (safe_mod_func_int16_t_s_s(((*l_1449) = (l_1411 == ((safe_mod_func_int16_t_s_s(0xC39DL, g_617.f3)) , &l_1347))), l_1450)))) || g_6)) || 0L), (*l_1422))), 8UL)), 0x6C290CE54C0BDD47LL)), &g_68)) != (void*)0)) , (void*)0)) | (*g_18))))) , 0x85C3L) != 0xDBA0L), 9)))), 15)), 2L))), 3)));
                        l_1467++;
                        return l_1348;
                    }
                }
                for (g_1249.f2 = 0; (g_1249.f2 <= 2); g_1249.f2 += 1)
                { /* block id: 681 */
                    int32_t l_1477 = 0L;
                    uint64_t *l_1493 = &g_371[3][8][0];
                    uint64_t **l_1492[7];
                    const int32_t *l_1497 = &l_1347;
                    uint32_t ***l_1498[8];
                    uint8_t l_1540 = 247UL;
                    float l_1546 = 0x8.1p-1;
                    const uint32_t *l_1549 = &g_170;
                    const uint32_t **l_1548 = &l_1549;
                    const uint32_t *** const l_1547 = &l_1548;
                    union U0 *****l_1558[9][6] = {{(void*)0,&g_1556,&g_1556,&g_1556,&g_1556,&g_1556},{&g_1556,&g_1556,&g_1556,&g_1556,(void*)0,&g_1556},{&g_1556,(void*)0,&g_1556,(void*)0,&g_1556,&g_1556},{(void*)0,&g_1556,&g_1556,&g_1556,&g_1556,&g_1556},{&g_1556,&g_1556,&g_1556,&g_1556,(void*)0,&g_1556},{&g_1556,(void*)0,&g_1556,(void*)0,&g_1556,&g_1556},{(void*)0,&g_1556,&g_1556,&g_1556,&g_1556,&g_1556},{&g_1556,&g_1556,&g_1556,&g_1556,(void*)0,&g_1556},{&g_1556,(void*)0,&g_1556,(void*)0,&g_1556,&g_1556}};
                    int i, j;
                    for (i = 0; i < 7; i++)
                        l_1492[i] = &l_1493;
                    for (i = 0; i < 8; i++)
                        l_1498[i] = (void*)0;
                    for (g_6 = 0; (g_6 <= 1); g_6 += 1)
                    { /* block id: 684 */
                        int32_t *l_1470 = &g_27[1][3];
                        int32_t *l_1471 = &l_1339;
                        int32_t *l_1472 = &l_1466[0];
                        int32_t *l_1473[5] = {&l_1404[5][5],&l_1404[5][5],&l_1404[5][5],&l_1404[5][5],&l_1404[5][5]};
                        union U0 ***l_1483 = &l_1482;
                        union U0 ***l_1484 = &l_1482;
                        union U0 ****l_1487 = &g_1485;
                        uint8_t *l_1489 = &g_834;
                        int i;
                        if ((*l_1375))
                            break;
                        l_1474++;
                        (*l_1375) &= (l_1477 < (((*l_1489) |= (safe_sub_func_uint16_t_u_u(l_1404[6][1], (((g_1480 , g_617.f0) & (((l_1484 = (l_1483 = l_1481)) != ((*l_1487) = g_1485)) , (((((*****l_1354) == g_717.f0) < g_170) <= g_179[2]) == l_1488[6]))) , l_1477)))) , (*g_18)));
                    }
                }
            }
        }
        if ((((void*)0 != &l_1467) , (((*****l_1354) ^ l_1452) , (safe_div_func_uint8_t_u_u(((((*l_5) &= (l_1562 != (void*)0)) , (**g_1499)) , ((0xEEL ^ l_1347) || (*****l_1354))), g_592[1])))))
        { /* block id: 718 */
            (***g_954) = (****l_1354);
        }
        else
        { /* block id: 720 */
            int8_t l_1587 = 0x7CL;
            uint64_t l_1647 = 0x04C583C50A42AB7DLL;
            int32_t l_1650 = (-5L);
            union U0 *l_1659 = &g_717;
            int32_t *l_1714 = &l_1452;
            int32_t *l_1715 = &l_1594;
            int32_t *l_1716 = (void*)0;
            int32_t *l_1717[6] = {&g_140,&g_140,&g_140,&g_140,&g_140,&g_140};
            int32_t l_1719 = 0L;
            uint32_t l_1723 = 0xD5FA7CB6L;
            int16_t l_1728 = 5L;
            uint32_t l_1734 = 0x364200B4L;
            int i;
            for (g_87 = 1; (g_87 <= 5); g_87 += 1)
            { /* block id: 723 */
                int8_t l_1596 = 1L;
                for (l_1474 = 0; (l_1474 <= 5); l_1474 += 1)
                { /* block id: 726 */
                    int i, j;
                    for (g_68 = 5; (g_68 >= 0); g_68 -= 1)
                    { /* block id: 729 */
                        int32_t l_1588 = (-7L);
                        int i, j;
                        l_1588 |= ((safe_mod_func_uint32_t_u_u((safe_add_func_int16_t_s_s(g_260[l_1474][(g_68 + 2)], g_260[l_1474][(l_1474 + 1)])), (safe_sub_func_uint16_t_u_u(((~(safe_div_func_int64_t_s_s((((**g_1499) = (safe_div_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((!(+(l_1348 || ((safe_lshift_func_int8_t_s_u(l_1474, 5)) != (safe_add_func_int32_t_s_s(((****g_378) = (-1L)), (safe_div_func_int32_t_s_s(((void*)0 == (*g_841)), (*g_18))))))))), (safe_add_func_uint16_t_u_u(((l_1586 != l_1586) | 4294967286UL), l_1587)))), 1UL))) <= (-3L)), g_617.f1))) == (*g_1380)), 0xC751L)))) , (***g_342));
                    }
                    for (g_143 = 0; (g_143 >= 0); g_143 -= 1)
                    { /* block id: 736 */
                        int i, j, k;
                        (***g_395) &= ((safe_add_func_uint32_t_u_u((~g_260[g_87][(g_143 + 3)]), 0x9C8631C7L)) >= g_260[g_87][(g_143 + 3)]);
                        if ((**g_396))
                            continue;
                        l_1339 = (safe_lshift_func_uint8_t_u_u(g_104, 0));
                    }
                    (***g_342) ^= g_260[l_1474][(g_87 + 1)];
                    l_1594 |= (g_260[g_87][(g_87 + 3)] < 1UL);
                    for (l_1348 = 3; (l_1348 >= 0); l_1348 -= 1)
                    { /* block id: 745 */
                        int64_t *l_1606 = &g_4;
                        int32_t *l_1607 = &l_1344[5][2][1];
                        int i, j, k;
                        (****g_341) = l_1595;
                        (*l_1607) |= (l_1596 || (safe_sub_func_int64_t_s_s((safe_add_func_uint16_t_u_u(g_260[l_1474][(g_87 + 1)], (-1L))), ((*l_1496) = (((*l_1606) = ((l_1601 != (void*)0) < (l_1339 |= (safe_lshift_func_uint16_t_u_s(((void*)0 == (*g_600)), ((safe_rshift_func_int8_t_s_s((255UL < g_27[0][1]), 3)) < (*****l_1601))))))) , g_260[l_1474][(g_87 + 1)])))));
                        return g_260[l_1474][(g_87 + 1)];
                    }
                }
                (****g_1353) = (****l_1354);
                for (g_72 = 0; (g_72 <= 3); g_72 += 1)
                { /* block id: 757 */
                    int64_t *l_1622 = (void*)0;
                    int64_t *l_1623[1][8];
                    int32_t l_1628[3][4][9] = {{{0x0DB0A3E3L,0L,0x0CF27725L,0x0CF27725L,0L,0x0DB0A3E3L,1L,0L,1L},{0xFC8D3C9BL,0x43AC5472L,(-6L),(-6L),0x43AC5472L,0xFC8D3C9BL,(-1L),0x43AC5472L,(-1L)},{0x0DB0A3E3L,0L,0x0CF27725L,0x0CF27725L,0L,0x0DB0A3E3L,1L,0L,1L},{0xFC8D3C9BL,0x43AC5472L,(-6L),(-6L),0x43AC5472L,0xFC8D3C9BL,(-1L),0x43AC5472L,(-1L)}},{{0x0DB0A3E3L,0L,0x0CF27725L,0x0CF27725L,0L,0x0DB0A3E3L,1L,0L,1L},{0xFC8D3C9BL,0x43AC5472L,(-6L),(-6L),0x43AC5472L,0xFC8D3C9BL,(-1L),0x43AC5472L,(-1L)},{0x0DB0A3E3L,0L,0x0CF27725L,0x0CF27725L,0L,0x0DB0A3E3L,1L,0L,1L},{0xFC8D3C9BL,0x43AC5472L,(-6L),(-6L),0x43AC5472L,0xFC8D3C9BL,(-1L),0x43AC5472L,(-1L)}},{{0x0DB0A3E3L,0L,0x0CF27725L,0x0CF27725L,0L,0x0DB0A3E3L,1L,0L,1L},{0xFC8D3C9BL,0x43AC5472L,(-6L),(-6L),0x43AC5472L,0xFC8D3C9BL,(-1L),0x43AC5472L,(-1L)},{0x0DB0A3E3L,0L,0x0CF27725L,0x0CF27725L,0L,0x0DB0A3E3L,1L,0L,1L},{0xFC8D3C9BL,0x43AC5472L,(-6L),(-6L),0x43AC5472L,0xFC8D3C9BL,(-1L),0x43AC5472L,(-1L)}}};
                    uint16_t l_1629 = 0UL;
                    int16_t *l_1630[4];
                    int32_t l_1631 = 1L;
                    const uint32_t l_1632 = 0UL;
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 8; j++)
                            l_1623[i][j] = (void*)0;
                    }
                    for (i = 0; i < 4; i++)
                        l_1630[i] = (void*)0;
                    (*g_1633) = ((safe_add_func_float_f_f(((*****l_1601) , (0x1.8p+1 <= ((((safe_mul_func_int16_t_s_s(1L, (((safe_mul_func_int8_t_s_s(l_1596, (((*l_5) = ((g_592[1] = (g_1614 , (((safe_add_func_int8_t_s_s(((((g_1617 , ((safe_div_func_uint16_t_u_u((((safe_add_func_int64_t_s_s((-5L), ((g_4 |= g_179[2]) == ((safe_div_func_uint8_t_u_u((((g_1626 , 0UL) , g_1627) , 0x91L), l_1628[0][0][7])) , l_1587)))) < 0UL) && l_1596), l_1629)) || g_802.f0)) < (*****l_1354)) <= (-3L)) , (*g_1380)), l_1587)) | l_1587) , (*g_938)))) != l_1631)) && 7L))) < l_1631) , 0x8FE3L))) , l_1632) <= l_1596) <= g_140))), g_140)) > 0x2.8F4E26p+14);
                    for (g_287 = 0; (g_287 <= 3); g_287 += 1)
                    { /* block id: 764 */
                        uint8_t *l_1636[9] = {&g_834,(void*)0,&g_834,(void*)0,&g_834,(void*)0,&g_834,(void*)0,&g_834};
                        int32_t l_1639 = (-1L);
                        int32_t l_1649 = 0x02D8E87EL;
                        int i, j;
                        l_1650 = ((**g_937) < ((((l_1649 |= (safe_add_func_int16_t_s_s(((g_1480.f3 ^= 255UL) && (((safe_mul_func_int8_t_s_s((g_260[(g_72 + 1)][(g_87 + 1)] = l_1639), ((((((safe_mul_func_int16_t_s_s(l_1596, ((l_1587 , l_1628[0][1][4]) > (((g_717.f0 ^= (0UL || l_1639)) | (safe_mul_func_uint8_t_u_u(((((((l_1647 &= (g_1644 == (void*)0)) , 0x7462B0A3L) , (void*)0) == l_1376) , 0xF.64E55Bp+69) , l_1596), g_1648))) == 0x8589L)))) | 0x84077A7B3970F8FBLL) == 0x5EF1L) <= (-6L)) | 18446744073709551615UL) || (*g_18)))) != 2L) == 0x3FL)), 0xB74DL))) == l_1587) , &g_938) != &g_938));
                    }
                    for (g_548 = 0; (g_548 <= 3); g_548 += 1)
                    { /* block id: 774 */
                        l_1654 = &g_938;
                    }
                }
            }
            for (g_310 = 0; (g_310 > 17); g_310 = safe_add_func_uint32_t_u_u(g_310, 1))
            { /* block id: 781 */
                int32_t l_1673 = (-4L);
                uint8_t *l_1675 = &g_717.f3;
                uint8_t **l_1674 = &l_1675;
                uint16_t *l_1676 = &g_87;
                float *l_1677 = &g_452[2];
                uint32_t l_1678 = 0x3F945B1BL;
                uint32_t **l_1680 = &g_18;
                float *l_1681 = &l_1343;
                float *l_1682 = (void*)0;
                float *l_1683 = (void*)0;
                float *l_1684[4] = {&g_130,&g_130,&g_130,&g_130};
                union U0 *l_1685 = &g_1495;
                int32_t *l_1711 = &l_1417;
                int i;
            }
            --l_1723;
            ++l_1734;
        }
    }
    else
    { /* block id: 815 */
        union U0 *l_1739 = &g_1480;
        uint64_t *l_1752 = &g_371[0][4][1];
        int32_t l_1753 = 0x536869F6L;
        union U1 **l_1756 = &g_598;
        int32_t l_1832[3][1];
        union U1 ****l_1833 = &g_1364;
        float l_1854 = 0xD.EE833Ap+75;
        uint8_t l_1860 = 0x93L;
        uint8_t l_1864 = 0UL;
        int i, j;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_1832[i][j] = (-4L);
        }
        (***g_395) &= (-9L);
        for (g_741 = 0; (g_741 <= 7); g_741 += 1)
        { /* block id: 819 */
            union U0 *l_1740 = &g_1741;
            int32_t l_1748[1];
            uint32_t l_1754 = 0xE0753D9EL;
            uint16_t l_1793[10][3] = {{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL},{0UL,0UL,0UL}};
            int32_t *l_1795 = &l_1718;
            volatile int16_t * const  volatile * volatile **l_1802 = (void*)0;
            volatile int16_t * const  volatile * volatile *l_1804 = &g_937;
            volatile int16_t * const  volatile * volatile **l_1803 = &l_1804;
            int i, j;
            for (i = 0; i < 1; i++)
                l_1748[i] = (-7L);
            (****g_341) = (safe_div_func_int16_t_s_s((((*g_938) , ((**g_1485) = l_1739)) == (l_1742 = l_1740)), (((((((((safe_lshift_func_uint8_t_u_s(((safe_mul_func_uint8_t_u_u((l_1748[0] && (g_1480.f3 , (g_1749[0] != (void*)0))), (l_1752 == (void*)0))) != l_1753), 0)) > l_1753) ^ 0x9D53L) , l_1754) || 0x7EL) > l_1753) , (*****l_1354)) <= l_1748[0]) & (*****l_1354))));
            for (g_1617.f2 = 0; (g_1617.f2 <= 7); g_1617.f2 += 1)
            { /* block id: 825 */
                union U1 **l_1755[5][9] = {{&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601},{(void*)0,&g_598,&g_601,&g_598,(void*)0,&g_601,(void*)0,&g_598,&g_601},{&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601},{(void*)0,&g_598,&g_601,&g_598,(void*)0,&g_601,(void*)0,&g_598,&g_601},{&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601,&g_601}};
                int64_t *l_1759 = &l_1705[5];
                int16_t *l_1764 = &g_592[1];
                int32_t l_1791 = 2L;
                uint16_t *l_1797 = (void*)0;
                uint16_t *l_1798 = &l_1793[1][2];
                int i, j;
                for (g_1660.f1 = 0; (g_1660.f1 <= 7); g_1660.f1 += 1)
                { /* block id: 828 */
                    (**g_343) = (l_1755[2][1] == l_1756);
                }
                l_1795 = func_45((****g_340), ((safe_div_func_int16_t_s_s((((*l_1759) = l_1754) <= (safe_add_func_int16_t_s_s((*****l_1354), ((l_1748[0] | (safe_rshift_func_int16_t_s_s(((*l_1764) ^= g_371[3][6][2]), 9))) || ((((safe_rshift_func_uint8_t_u_u((safe_div_func_int8_t_s_s((0x055E3F2FL ^ (safe_rshift_func_int8_t_s_u((safe_add_func_int16_t_s_s((safe_div_func_uint8_t_u_u(g_717.f3, ((safe_mod_func_uint32_t_u_u(((((*****l_1354) , (safe_add_func_int16_t_s_s((safe_mod_func_int32_t_s_s(((((((safe_add_func_int16_t_s_s((((safe_mul_func_int8_t_s_s((((safe_lshift_func_int8_t_s_s((safe_div_func_uint64_t_u_u(((*l_1752) ^= (safe_lshift_func_uint8_t_u_u(((*g_1486) == (***g_1556)), g_179[0]))), g_255[0])), 0)) > l_1753) , 0x39L), g_68)) <= 0L) >= (-1L)), 65535UL)) || l_1791) || 0x85ADL) , (void*)0) == (void*)0) ^ l_1791), (****g_341))), 0L))) >= l_1791) || 0x7CA5211D1EB2330CLL), 3UL)) , 0x30L))), g_1792)), 6))), 0x31L)), 3)) < l_1748[0]) & l_1753) & (*****l_1354)))))), l_1793[9][1])) , (****g_1353)), g_1794, &g_260[3][8]);
                (**g_396) |= 0L;
                (**g_343) = (safe_unary_minus_func_int16_t_s(((**g_937) && ((*l_1798) = 0UL))));
            }
            if ((*l_1795))
            { /* block id: 839 */
                for (g_653.f0 = 0; g_653.f0 < 9; g_653.f0 += 1)
                {
                    for (g_1614.f1 = 0; g_1614.f1 < 10; g_1614.f1 += 1)
                    {
                        g_1335[g_653.f0][g_1614.f1] = &g_1336;
                    }
                }
            }
            else
            { /* block id: 841 */
                uint16_t l_1799 = 9UL;
                l_1799++;
            }
            (*l_1803) = &g_937;
        }
        for (g_435.f2 = 0; (g_435.f2 >= 14); ++g_435.f2)
        { /* block id: 848 */
            uint64_t l_1818 = 0x35DB8CA7E3B43CDDLL;
            int64_t l_1836[6];
            int32_t l_1855 = 6L;
            int32_t l_1857 = 0x5873C8FFL;
            int32_t l_1858 = 0x2227B660L;
            int32_t l_1859 = (-1L);
            int i;
            for (i = 0; i < 6; i++)
                l_1836[i] = 0x6FAD68F2AE2E8E96LL;
            if (((((g_1741.f3 ^ (**g_477)) | ((*g_18)--)) , 0x98L) < (*****l_1354)))
            { /* block id: 850 */
                int32_t *l_1809 = &l_1718;
                union U1 ****l_1810[8][3] = {{&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364},{&g_1364,&g_1364,&g_1364}};
                int i, j;
                (*g_253) = l_1809;
                (****g_341) |= (****g_394);
                for (g_1545.f2 = 0; (g_1545.f2 <= 2); g_1545.f2 += 1)
                { /* block id: 855 */
                    int32_t l_1817[6];
                    int8_t *l_1819[6][3] = {{&g_260[5][7],&g_260[5][7],&g_68},{(void*)0,&g_260[5][7],&g_68},{&g_260[5][7],(void*)0,&g_68},{&g_260[5][7],&g_260[5][7],&g_68},{(void*)0,&g_260[5][7],&g_68},{&g_260[5][7],(void*)0,&g_68}};
                    int32_t l_1829 = 1L;
                    int16_t l_1830 = 9L;
                    union U0 ****l_1834[9];
                    int i, j;
                    for (i = 0; i < 6; i++)
                        l_1817[i] = 0L;
                    for (i = 0; i < 9; i++)
                        l_1834[i] = &g_1485;
                    (*****g_1353) = ((((l_1810[7][2] != ((safe_mul_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u((((safe_rshift_func_int16_t_s_s((*****l_1354), ((*****l_1354) && l_1817[0]))) , ((l_1817[0] = l_1818) & (safe_add_func_int64_t_s_s(((safe_lshift_func_int8_t_s_s((**g_477), (*l_1809))) == (((safe_lshift_func_uint8_t_u_s(g_1545.f2, (safe_div_func_uint16_t_u_u((((+(((g_179[2] > l_1829) == l_1829) <= l_1830)) , (*g_18)) != 0x7056BE5EL), l_1831)))) != l_1832[1][0]) < g_101)), g_27[0][1])))) , l_1818), 3)), g_287)) , l_1833)) != 0x2.136016p-59) , l_1834[7]) != (void*)0);
                }
            }
            else
            { /* block id: 859 */
                int32_t *l_1835 = &g_27[0][2];
                int32_t *l_1837 = &l_1718;
                int32_t *l_1838 = &g_72;
                int32_t *l_1839 = (void*)0;
                int32_t *l_1840 = (void*)0;
                int32_t *l_1841 = &l_1344[5][2][1];
                int32_t *l_1842 = &g_140;
                int32_t l_1843 = (-1L);
                int32_t *l_1844 = &l_1718;
                int32_t *l_1845 = (void*)0;
                int32_t *l_1846 = &g_72;
                int32_t *l_1847 = &l_1344[5][2][1];
                int32_t *l_1848 = &g_27[0][1];
                int32_t *l_1849 = &l_1339;
                int32_t *l_1850 = (void*)0;
                int32_t *l_1851 = &l_1339;
                int32_t *l_1852 = &g_27[0][1];
                int32_t *l_1853[8];
                int i;
                for (i = 0; i < 8; i++)
                    l_1853[i] = &g_27[0][2];
                --l_1860;
                l_1864--;
                return (*l_1835);
            }
        }
    }
    for (g_287 = 20; (g_287 <= 44); g_287 = safe_add_func_int16_t_s_s(g_287, 8))
    { /* block id: 868 */
        for (g_1545.f2 = 0; (g_1545.f2 <= 8); g_1545.f2 += 1)
        { /* block id: 871 */
            return l_1869;
        }
    }
    (**g_395) = (****g_393);
    return (*g_18);
}


/* ------------------------------------------ */
/* 
 * reads : g_18 g_19 g_370 g_371 g_955 g_253 g_254 g_396 g_397 g_76
 * writes: g_1335 g_254 g_76
 */
static uint64_t  func_12(uint32_t * p_13, uint32_t * p_14, int16_t  p_15, uint32_t * const  p_16, uint32_t * p_17)
{ /* block id: 580 */
    int32_t l_1331[8] = {(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L)};
    int32_t ****l_1334 = &g_842;
    int32_t *****l_1333[2][5][2] = {{{&l_1334,&l_1334},{&l_1334,&l_1334},{&l_1334,&l_1334},{&l_1334,&l_1334},{&l_1334,&l_1334}},{{&l_1334,&l_1334},{&l_1334,&l_1334},{&l_1334,&l_1334},{&l_1334,&l_1334},{&l_1334,&l_1334}}};
    int32_t *****l_1337 = &l_1334;
    int8_t **l_1338 = &g_950[1];
    int i, j, k;
    if ((safe_rshift_func_int16_t_s_u((1L < (safe_sub_func_uint32_t_u_u((*g_18), (safe_div_func_int64_t_s_s(((safe_mul_func_int8_t_s_s((!0xBCA9L), ((((safe_rshift_func_int16_t_s_s((((safe_rshift_func_int16_t_s_s((((safe_lshift_func_uint16_t_u_u(l_1331[4], 11)) && (((safe_unary_minus_func_uint8_t_u(0xD8L)) ^ ((*g_370) != ((g_1335[1][0] = l_1333[1][4][0]) == (l_1337 = &g_1336)))) , 1L)) , p_15), l_1331[4])) , l_1338) == (void*)0), p_15)) , 0x38L) && l_1331[3]) != p_15))) | p_15), 9L))))), l_1331[1])))
    { /* block id: 583 */
        (*g_253) = (**g_955);
    }
    else
    { /* block id: 585 */
        (**g_396) ^= p_15;
        return l_1331[6];
    }
    return p_15;
}


/* ------------------------------------------ */
/* 
 * reads : g_27 g_19 g_938 g_271.f0
 * writes: g_27 g_19
 */
static int16_t  func_22(int8_t * p_23, int32_t  p_24)
{ /* block id: 2 */
    int32_t *l_26 = &g_27[0][1];
    int32_t l_69 = 9L;
    const int8_t *l_421 = &g_260[5][7];
    int32_t *l_1315 = &g_72;
lbl_30:
    (*l_26) &= (p_24 , 0x60D743CCL);
    for (g_19 = (-26); (g_19 >= 47); ++g_19)
    { /* block id: 6 */
        int32_t *l_53 = (void*)0;
        int32_t **l_56 = &l_53;
        int32_t l_66 = (-1L);
        int8_t *l_67[2][6] = {{&g_68,&g_68,&g_68,&g_68,&g_68,&g_68},{&g_68,&g_68,&g_68,&g_68,&g_68,&g_68}};
        uint64_t l_70[1][6];
        uint32_t *l_481 = &g_19;
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 6; j++)
                l_70[i][j] = 3UL;
        }
        if (p_24)
            goto lbl_30;
    }
    return (*g_938);
}


/* ------------------------------------------ */
/* 
 * reads : g_396 g_397 g_376 g_130 g_68 g_27 g_310 g_179 g_18 g_19 g_104 g_341 g_342 g_343 g_371 g_254 g_72 g_143 g_395 g_76 g_129 g_452 g_435 g_436 g_253 g_142 g_365 g_287
 * writes: g_397 g_310 g_452 g_104 g_343 g_371 g_548 g_101 g_254 g_142 g_130
 */
static int32_t * func_31(int8_t * const  p_32)
{ /* block id: 209 */
    uint32_t l_501 = 0xF6E468ACL;
    int32_t l_502 = (-10L);
    int32_t l_506[4][3] = {{0L,0L,0L},{0x2309E625L,0L,0L},{0L,0x67BCEBC8L,(-7L)},{0x2309E625L,0x67BCEBC8L,0x2309E625L}};
    float l_507 = 0x1.1p+1;
    int64_t l_508 = 1L;
    uint32_t l_509[1][4][8];
    const int32_t **l_521 = &g_254;
    int64_t l_546 = 0x18E5C6F9AD18978BLL;
    const int8_t *l_550 = &g_260[5][7];
    uint32_t *l_580 = &g_170;
    int32_t l_627 = 0xF29CADA3L;
    uint64_t l_689 = 18446744073709551615UL;
    float l_746 = 0x1.2p+1;
    uint32_t l_750 = 0xE57095BAL;
    int32_t l_788[2][7] = {{1L,5L,1L,0L,1L,5L,1L},{1L,5L,1L,0L,1L,5L,0L}};
    int32_t *l_793 = &l_506[0][2];
    union U1 **l_819 = &g_601;
    int32_t *l_822 = &l_506[0][2];
    int8_t *** const l_836 = (void*)0;
    int32_t ****l_853 = &g_342;
    float l_860[3][9][9] = {{{(-0x3.Ep-1),0x4.A5560Fp-64,(-0x6.3p-1),0x7.05679Cp-29,0x5.0p-1,(-0x7.1p-1),0x2.817DFCp+94,0xA.E5542Cp+48,0x3.4p-1},{0x8.9p-1,0x4.0p+1,0x0.9p-1,0x9.E827C1p+76,0xB.75C3BBp+26,0x0.09C278p-20,0x7.05679Cp-29,0x7.05679Cp-29,0x0.09C278p-20},{0x0.6p-1,0x9.E827C1p+76,(-0x8.4p+1),0x9.E827C1p+76,0x0.6p-1,0x0.9p-1,0x9.6E3277p-24,0x4.A5560Fp-64,(-0x2.7p+1)},{0xE.FDA470p-85,0x0.Ep-1,0x5.Fp+1,0x7.05679Cp-29,0x9.E827C1p+76,0x9.Cp+1,0x8.9p-1,0x2.0F5AD6p+62,0x5.49DB6Cp-79},{0x5.Cp+1,0xC.DAB6A7p-90,0xB.AD73CDp+87,0xE.FDA470p-85,0x0.7p-1,0x0.9p-1,0xA.E5542Cp+48,0x3.3504ADp+94,0x5.1p-1},{0xA.EFE70Bp+49,0x3.3504ADp+94,0xB.6E969Fp+70,0xC.DAB6A7p-90,0x1.Bp-1,0x0.09C278p-20,0x4.A5560Fp-64,0x3.3504ADp+94,(-0x6.3p-1)},{0x5.68FDFBp+37,0xB.75C3BBp+26,0x1.B274F2p-20,0x5.0p-1,0x0.952283p+2,(-0x7.1p-1),0x0.6p-1,0x2.0F5AD6p+62,0x2.5p+1},{0x3.3504ADp+94,0x4.91A5D8p-29,(-0x7.1p-1),0x5.Cp+1,0x5.68FDFBp+37,0x1.9p-1,0x0.Ep-1,0x4.A5560Fp-64,0x3.Bp+1},{0x5.68FDFBp+37,0x0.952283p+2,0x9.4p+1,0x1.Ep-1,0x7.0p+1,0x1.Cp+1,0x4.0p+1,0x7.05679Cp-29,(-0x7.1p-1)}},{{0xA.EFE70Bp+49,0x0.6p-1,0x9.4p+1,0xA.EFE70Bp+49,0x4.91A5D8p-29,0x4.85FB17p-17,0x5.68FDFBp+37,0xA.E5542Cp+48,0x9.Cp+1},{0x5.Cp+1,0x0.6p-1,(-0x7.1p-1),0xA.E5542Cp+48,0x9.6E3277p-24,(-0x8.4p+1),0x0.7p-1,0x5.0p-1,0x1.5p+1},{0xE.FDA470p-85,0x2.817DFCp+94,0x1.B274F2p-20,0x4.0p+1,0x4.91A5D8p-29,0x2.5p+1,(-0x3.5p-1),0xB.75C3BBp+26,0xB.6E969Fp+70},{0x0.6p-1,(-0x3.5p-1),0xB.6E969Fp+70,0x7.0p+1,0x7.0p+1,0xB.6E969Fp+70,(-0x3.5p-1),0x0.6p-1,0x1.9p-1},{0x8.9p-1,0xA.E5542Cp+48,0xB.AD73CDp+87,(-0x3.Ep-1),0x5.68FDFBp+37,0x9.4p+1,0x0.7p-1,0x9.E827C1p+76,(-0x9.Bp+1)},{(-0x3.Ep-1),0xE.FDA470p-85,0x5.Fp+1,0xB.75C3BBp+26,0x0.952283p+2,(-0x2.Cp-1),0x5.68FDFBp+37,0x0.7p-1,0x1.9p-1},{0x2.0F5AD6p+62,0xC.AF9D30p+62,(-0x8.4p+1),0x4.A5560Fp-64,0x1.Bp-1,0x3.Bp+1,0x4.0p+1,0x1.Bp-1,0xB.6E969Fp+70},{0x0.1p-1,0xC.AF9D30p+62,0x0.9p-1,0x2.817DFCp+94,0x0.7p-1,0x1.E4DAEEp+55,0x0.Ep-1,0x0.952283p+2,0x1.5p+1},{0x4.0p+1,0xE.FDA470p-85,(-0x6.3p-1),0x2.0F5AD6p+62,0x9.E827C1p+76,0x8.4233E2p+59,0x0.6p-1,0x5.68FDFBp+37,0x9.Cp+1}},{{0x0.7p-1,0xA.E5542Cp+48,0x0.09C278p-20,0x2.817DFCp+94,0x0.6p-1,0x0.6p+1,0x4.A5560Fp-64,0x7.0p+1,(-0x7.1p-1)},{0xA.E5542Cp+48,(-0x3.5p-1),0x1.5p+1,0x4.A5560Fp-64,0xB.75C3BBp+26,0x0.6p+1,0xA.E5542Cp+48,0x4.91A5D8p-29,0x3.Bp+1},{0xC.AF9D30p+62,0x2.817DFCp+94,(-0x9.Bp+1),0xB.75C3BBp+26,0x5.0p-1,0x8.4233E2p+59,0x8.9p-1,0x9.6E3277p-24,0x2.5p+1},{0x9.E827C1p+76,0x4.3B8F1Dp+49,0xE.FDA470p-85,0x6.321B78p+80,0x0.157DC1p-72,0xC.DAB6A7p-90,0x1.Ep+1,0x3.DC7F70p-23,0x0.6p-1},{0xF.CAD25Cp-49,0x9.B8547Dp-89,0xB.75C3BBp+26,0x7.Dp-1,0x6.FDEAA1p-12,0x0.7p-1,0x6.FDEAA1p-12,0x7.Dp-1,0xB.75C3BBp+26},{0xF.CAD25Cp-49,0xF.CAD25Cp-49,0x0.952283p+2,0xC.B75095p-67,0x0.0p+1,0x8.9p-1,0x5.69F609p-49,(-0x6.1p+1),0x5.0p-1},{0xE.CD1CCEp+0,0x3.DC7F70p-23,0x4.0p+1,0x0.157DC1p-72,0x8.61F46Ap+26,0x0.Ep-1,0x7.Dp-1,0xF.CAD25Cp-49,0xA.E5542Cp+48},{(-0x1.7p+1),(-0x8.6p+1),0x0.952283p+2,0x9.6p+1,0x8.08259Bp+50,0x7.0p+1,0x0.Fp+1,(-0x10.4p+1),0x7.05679Cp-29},{0x0.157DC1p-72,0x8.08259Bp+50,0xB.75C3BBp+26,0x2.1p-1,0x8.08259Bp+50,0x9.E827C1p+76,(-0x6.Dp-1),(-0x1.Dp-1),0x4.A5560Fp-64}}};
    int32_t l_866 = 0x4F249BA5L;
    uint16_t l_877 = 0x8981L;
    int16_t *l_897 = &g_592[0];
    int32_t l_905 = 8L;
    uint8_t l_1007 = 253UL;
    uint32_t l_1016 = 0x878F18D1L;
    int32_t *l_1025 = &g_179[2];
    int32_t * const *l_1024 = &l_1025;
    int32_t * const **l_1023[10][4] = {{&l_1024,&l_1024,&l_1024,(void*)0},{&l_1024,(void*)0,(void*)0,&l_1024},{(void*)0,(void*)0,&l_1024,(void*)0},{(void*)0,&l_1024,&l_1024,&l_1024},{(void*)0,(void*)0,(void*)0,&l_1024},{&l_1024,&l_1024,&l_1024,(void*)0},{&l_1024,(void*)0,(void*)0,&l_1024},{(void*)0,(void*)0,&l_1024,(void*)0},{(void*)0,&l_1024,&l_1024,&l_1024},{(void*)0,(void*)0,(void*)0,&l_1024}};
    int16_t l_1082 = 0x13A9L;
    uint32_t l_1083 = 18446744073709551608UL;
    int32_t l_1162 = (-1L);
    int64_t l_1176[1];
    uint8_t l_1272 = 255UL;
    int32_t *l_1314 = (void*)0;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 8; k++)
                l_509[i][j][k] = 3UL;
        }
    }
    for (i = 0; i < 1; i++)
        l_1176[i] = 0x233AED5338856C34LL;
    (*g_396) = (*g_396);
    for (g_310 = 0; (g_310 >= 18); ++g_310)
    { /* block id: 213 */
        int32_t l_486 = (-10L);
        float *l_495 = &g_452[2];
        int8_t **l_499 = (void*)0;
        int32_t l_500 = 0x7EB5A85BL;
        int32_t *l_503 = &g_143;
        int32_t *l_504 = &l_502;
        int32_t *l_505[5][3][6] = {{{(void*)0,&g_27[1][0],&l_502,&g_143,&g_27[0][1],&l_500},{(void*)0,&l_500,&g_143,&g_72,&l_502,&g_27[0][1]},{&g_27[0][1],(void*)0,(void*)0,(void*)0,&g_27[0][1],&l_502}},{{&g_27[0][1],&g_27[0][1],&g_143,&g_27[1][0],&l_486,(void*)0},{(void*)0,&l_502,&g_143,&g_27[0][1],&l_486,(void*)0},{&g_143,&g_143,&g_143,&g_27[0][1],&l_502,&l_502}},{{&l_486,(void*)0,(void*)0,&l_486,&g_143,&g_27[0][1]},{&g_27[0][1],&g_143,&g_143,&g_143,&g_72,&l_500},{&g_27[0][1],&g_143,&l_502,(void*)0,&g_72,&g_27[0][1]}},{{&g_27[1][0],&g_143,&g_27[0][1],&g_27[0][1],&g_143,&g_27[0][1]},{(void*)0,(void*)0,(void*)0,&g_27[0][1],&l_502,&g_72},{&g_72,&g_143,&l_500,(void*)0,&l_486,&g_143}},{{&g_143,&l_502,&g_27[1][0],(void*)0,&l_486,&g_27[0][1]},{&g_72,&g_27[0][1],&g_143,&g_27[0][1],&g_27[0][1],&g_143},{(void*)0,(void*)0,(void*)0,&g_27[0][1],&l_502,&g_143}}};
        uint8_t *l_518 = &g_104;
        int32_t ***l_528 = &g_354;
        int32_t ****l_527 = &l_528;
        int32_t *****l_526 = &l_527;
        uint64_t *l_536 = &g_371[3][8][0];
        int64_t *l_547[2];
        uint32_t l_549 = 0x594737F5L;
        int32_t **l_551 = &g_142[1];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_547[i] = &l_546;
        l_500 = (l_486 , (safe_add_func_uint16_t_u_u((safe_add_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u((((((((safe_add_func_float_f_f((((*l_495) = (*g_376)) == (((0x2B73L && l_486) >= (*p_32)) , (g_27[0][1] < 0xD.272D4Cp+87))), ((((!((safe_mul_func_float_f_f((l_499 == (void*)0), l_500)) != g_310)) > g_179[2]) <= 0x1.Bp+1) == l_501))) != l_501) == l_501) , 4UL) & 0x4982683FL) <= 1L) > (*g_18)), l_500)), l_501)), 0x3242L)));
        l_509[0][2][3]++;
        (*g_253) = ((*l_551) = func_45((((((safe_sub_func_uint32_t_u_u(((g_27[0][1] , ((safe_add_func_uint64_t_u_u(((((++(*l_518)) && (l_521 == ((*g_342) = (**g_341)))) , (safe_lshift_func_int8_t_s_u((safe_sub_func_int8_t_s_s((l_526 != &l_527), (((((((g_548 = (+(safe_rshift_func_int16_t_s_s(((((safe_mod_func_int64_t_s_s((((safe_add_func_uint64_t_u_u(((*l_536)--), 18446744073709551609UL)) > ((((+(safe_rshift_func_uint16_t_u_s((**l_521), (safe_div_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u((*l_503), (((((*p_32) | (*p_32)) , l_505[3][0][4]) != (void*)0) && (*l_504)))), (-4L)))))) == 7L) , 0xC4L) && (**l_521))) > l_546), (**l_521))) & (***g_395)) && (*l_504)) ^ 0xFCAD1DB9L), 6)))) , (*g_129)) == g_452[2]) , 0xBEB51E9AF835EF3BLL) > 18446744073709551607UL) >= (**l_521)) == 0x8F5C557AL))), 1))) | (*l_503)), 1L)) ^ l_549)) , 6UL), 0x6563E275L)) && (**l_521)) <= (*l_504)) ^ (**l_521)) , &l_506[2][2]), &l_486, (**l_521), l_550));
    }
    return l_1314;
}


/* ------------------------------------------ */
/* 
 * reads : g_483
 * writes: g_452
 */
static int8_t * func_33(int32_t * p_34, uint64_t  p_35, uint32_t * p_36, uint64_t  p_37, uint32_t * p_38)
{ /* block id: 206 */
    int64_t l_482[8][5] = {{0xFCF05DBD966DF15CLL,0x4F5BECC7A56D2BBCLL,0x4F5BECC7A56D2BBCLL,0xFCF05DBD966DF15CLL,0x4F5BECC7A56D2BBCLL},{0xFCF05DBD966DF15CLL,0xFCF05DBD966DF15CLL,1L,0x4F5BECC7A56D2BBCLL,0x4F5BECC7A56D2BBCLL},{1L,0x4F5BECC7A56D2BBCLL,1L,1L,0x4F5BECC7A56D2BBCLL},{0x4F5BECC7A56D2BBCLL,1L,1L,0x4F5BECC7A56D2BBCLL,1L},{0x4F5BECC7A56D2BBCLL,0x4F5BECC7A56D2BBCLL,0xFCF05DBD966DF15CLL,0x4F5BECC7A56D2BBCLL,0x4F5BECC7A56D2BBCLL},{1L,0x4F5BECC7A56D2BBCLL,1L,1L,0x4F5BECC7A56D2BBCLL},{0x4F5BECC7A56D2BBCLL,1L,1L,0x4F5BECC7A56D2BBCLL,1L},{0x4F5BECC7A56D2BBCLL,0x4F5BECC7A56D2BBCLL,0xFCF05DBD966DF15CLL,0x4F5BECC7A56D2BBCLL,0x4F5BECC7A56D2BBCLL}};
    int i, j;
    (*g_483) = (l_482[2][0] <= l_482[2][0]);
    return &g_68;
}


/* ------------------------------------------ */
/* 
 * reads : g_476 g_480 g_343 g_142
 * writes: g_476
 */
static int32_t * func_39(uint16_t  p_40)
{ /* block id: 202 */
    float l_466 = 0x4.F43DC1p+40;
    int32_t l_467[9][9] = {{0L,0L,0x503C1DFEL,1L,(-1L),0x08F29C20L,0L,1L,0x7CA87C93L},{9L,1L,0xCF49092BL,0L,9L,(-1L),1L,0x25EAA3A3L,(-1L)},{9L,0x7CA87C93L,0x503C1DFEL,0L,0x559B9958L,0L,0x25EAA3A3L,0x30C26756L,0xA8FBE99FL},{(-1L),0x503C1DFEL,1L,(-1L),0x30C26756L,0x7CA87C93L,0x7CA87C93L,0x30C26756L,(-1L)},{0L,0L,0L,1L,9L,9L,0xCF49092BL,0x25EAA3A3L,0xC51A6070L},{0xBE86FD6CL,(-7L),(-9L),0x559B9958L,(-1L),0xC51A6070L,0x4381999DL,1L,0x70DB61EDL},{(-8L),0L,(-1L),1L,0x7CA87C93L,1L,9L,(-7L),0x08F29C20L},{0x3FB1A51AL,0xC51A6070L,1L,(-1L),0x08F29C20L,0xCF49092BL,0xA8FBE99FL,0xCF49092BL,0x08F29C20L},{0L,0x4381999DL,0x4381999DL,0L,0x25EAA3A3L,9L,1L,0x3FB1A51AL,0x70DB61EDL}};
    int32_t *l_468 = &g_140;
    int32_t *l_469 = &g_27[0][1];
    int32_t *l_470 = (void*)0;
    int32_t *l_471 = &g_27[0][1];
    int32_t *l_472[5] = {&g_143,&g_143,&g_143,&g_143,&g_143};
    uint32_t l_473 = 1UL;
    int i, j;
    l_473++;
    (*g_480) = g_476;
    return (*g_343);
}


/* ------------------------------------------ */
/* 
 * reads : g_341 g_342 g_378 g_343 g_142 g_72 g_143
 * writes: g_72 g_143
 */
static uint16_t  func_41(int32_t * p_42, uint16_t  p_43, int32_t  p_44)
{ /* block id: 196 */
    int32_t ***l_458 = (void*)0;
    int32_t *l_459 = &g_140;
    int32_t *l_460 = &g_140;
    int32_t *l_461[10][4][2] = {{{&g_143,&g_140},{&g_27[0][3],&g_72},{&g_72,&g_143},{&g_27[0][1],&g_72}},{{&g_27[0][1],&g_27[1][0]},{&g_27[0][1],&g_72},{&g_27[0][1],&g_143},{&g_72,&g_72}},{{&g_27[0][3],&g_140},{&g_143,(void*)0},{(void*)0,(void*)0},{&g_143,&g_140}},{{&g_27[0][3],&g_72},{&g_72,&g_143},{&g_27[0][1],&g_72},{&g_27[0][1],&g_27[1][0]}},{{&g_27[0][1],&g_72},{&g_27[0][1],&g_143},{&g_72,&g_72},{&g_27[0][3],&g_140}},{{&g_143,(void*)0},{(void*)0,(void*)0},{&g_143,&g_140},{&g_27[0][3],&g_72}},{{&g_72,&g_143},{&g_27[0][1],&g_72},{&g_27[0][1],&g_27[1][0]},{&g_27[0][1],&g_72}},{{&g_27[0][1],&g_143},{&g_72,&g_72},{&g_27[0][3],&g_140},{&g_143,(void*)0}},{{(void*)0,(void*)0},{&g_143,&g_140},{&g_27[0][3],&g_72},{&g_72,&g_143}},{{&g_27[0][1],&g_27[1][0]},{&g_140,(void*)0},{&g_140,&g_27[1][0]},{&g_27[0][3],(void*)0}}};
    float l_462 = 0xC.670354p-99;
    uint32_t l_463[10] = {1UL,1UL,4294967293UL,0xE6F3CDE0L,4294967293UL,1UL,1UL,4294967293UL,0xE6F3CDE0L,4294967293UL};
    int i, j, k;
    (**g_343) |= (!((l_458 = (*g_341)) != (*g_378)));
    ++l_463[2];
    return (***l_458);
}


/* ------------------------------------------ */
/* 
 * reads : g_101 g_435 g_436 g_253 g_343 g_142 g_341 g_342 g_365 g_130 g_179 g_104 g_287
 * writes: g_101 g_254 g_142 g_452 g_130
 */
static int32_t * func_45(int32_t * p_46, int32_t * p_47, uint32_t  p_48, const int8_t * p_49)
{ /* block id: 184 */
    int8_t l_427 = 5L;
    int32_t l_437 = 0x92557BF3L;
    for (g_101 = 0; (g_101 > 12); g_101 = safe_add_func_uint64_t_u_u(g_101, 2))
    { /* block id: 187 */
        uint16_t l_434 = 0UL;
        int8_t *l_438 = (void*)0;
        float *l_451 = &g_452[2];
        int32_t *****l_455 = &g_341;
        float *l_456[10][9] = {{&g_130,&g_130,(void*)0,&g_130,&g_130,&g_130,&g_130,&g_130,(void*)0},{&g_130,&g_130,&g_130,(void*)0,&g_130,&g_130,&g_130,&g_130,&g_130},{&g_130,&g_130,&g_130,&g_130,(void*)0,&g_130,(void*)0,&g_130,(void*)0},{&g_130,&g_130,&g_130,&g_130,(void*)0,(void*)0,(void*)0,(void*)0,&g_130},{&g_130,&g_130,&g_130,&g_130,(void*)0,&g_130,(void*)0,(void*)0,&g_130},{&g_130,&g_130,(void*)0,(void*)0,&g_130,(void*)0,&g_130,(void*)0,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130,(void*)0,(void*)0,&g_130},{&g_130,(void*)0,(void*)0,&g_130,&g_130,&g_130,&g_130,(void*)0,(void*)0},{&g_130,&g_130,&g_130,&g_130,&g_130,&g_130,&g_130,(void*)0,(void*)0},{(void*)0,&g_130,(void*)0,(void*)0,&g_130,(void*)0,(void*)0,&g_130,(void*)0}};
        int i, j;
        (***g_341) = ((!(safe_add_func_int16_t_s_s(l_427, 0x8732L))) , func_50(((safe_mod_func_uint64_t_u_u((p_48 || 0x48L), p_48)) , ((safe_add_func_uint8_t_u_u((p_48 == p_48), (safe_mul_func_uint16_t_u_u(((((l_434 , (l_437 = (((((g_435 , p_49) == (void*)0) , l_434) , g_436) , 6L))) , l_427) ^ p_48) >= 65533UL), 0UL)))) || 0x94L)), l_438));
        (*g_365) = (safe_div_func_float_f_f((safe_sub_func_float_f_f((l_437 == l_434), (safe_sub_func_float_f_f((p_48 != (((safe_div_func_float_f_f(((*g_365) >= (((safe_div_func_float_f_f(((safe_sub_func_float_f_f(((*l_451) = (g_179[2] < p_48)), (g_104 >= (safe_div_func_float_f_f(((l_455 = (void*)0) != (void*)0), p_48))))) > l_434), l_437)) >= l_437) > 0xE.E8E537p+56)), g_104)) , (void*)0) != &l_437)), p_48)))), g_287));
        return (*g_343);
    }
    return p_46;
}


/* ------------------------------------------ */
/* 
 * reads : g_253 g_343 g_142
 * writes: g_254
 */
static int32_t * func_50(int32_t  p_51, int8_t * p_52)
{ /* block id: 181 */
    int32_t *l_420 = &g_27[0][1];
    (*g_253) = l_420;
    return (*g_343);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t * func_54(uint64_t  p_55)
{ /* block id: 11 */
    int32_t *l_71 = &g_72;
    int32_t *l_73 = &g_72;
    int32_t *l_74[7][1][9] = {{{(void*)0,&g_72,&g_72,(void*)0,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,(void*)0,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,(void*)0,&g_72,&g_72,&g_72,&g_72,(void*)0,&g_72}},{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}},{{(void*)0,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72,&g_72}},{{&g_72,&g_72,&g_72,(void*)0,&g_72,&g_72,&g_72,&g_72,&g_72}}};
    int32_t l_75 = 0x80214363L;
    uint64_t l_77[10][3][3] = {{{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL}},{{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL}},{{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL}},{{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL}},{{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL}},{{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL}},{{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL}},{{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL}},{{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL}},{{7UL,7UL,7UL},{0x36AA128409D7FF73LL,0x36AA128409D7FF73LL,0x36AA128409D7FF73LL},{7UL,7UL,7UL}}};
    int8_t *l_82 = &g_68;
    int32_t **l_83[4][9][3] = {{{&l_73,(void*)0,&l_74[5][0][6]},{&l_73,&l_73,&l_71},{&l_73,&l_73,&l_73},{&l_74[0][0][4],&l_74[2][0][7],&l_71},{&l_74[0][0][4],&l_74[0][0][4],&l_74[5][0][6]},{&l_74[5][0][8],&l_73,&l_74[2][0][7]},{&l_74[0][0][4],&l_73,(void*)0},{&l_74[5][0][8],(void*)0,&l_74[1][0][1]},{&l_74[0][0][4],(void*)0,(void*)0}},{{&l_74[0][0][4],&l_74[0][0][4],&l_74[2][0][7]},{&l_73,(void*)0,&l_74[0][0][4]},{&l_73,(void*)0,&l_71},{&l_73,&l_73,&l_73},{&l_74[0][0][4],&l_73,&l_71},{&l_73,&l_74[0][0][4],&l_74[0][0][4]},{&l_74[5][0][8],&l_74[2][0][7],&l_74[2][0][7]},{&l_74[5][0][6],&l_73,(void*)0},{&l_74[5][0][8],&l_73,&l_74[1][0][1]}},{{&l_73,(void*)0,(void*)0},{&l_74[0][0][4],&l_74[4][0][0],&l_74[2][0][7]},{&l_73,(void*)0,&l_74[5][0][6]},{&l_73,&l_73,&l_71},{&l_73,&l_73,&l_73},{&l_74[0][0][4],&l_74[2][0][7],&l_71},{&l_74[0][0][4],&l_74[0][0][4],&l_74[5][0][6]},{&l_74[5][0][8],&l_73,&l_74[2][0][7]},{&l_74[0][0][4],&l_73,(void*)0}},{{&l_74[5][0][8],(void*)0,&l_74[1][0][1]},{&l_74[0][0][4],(void*)0,(void*)0},{&l_74[0][0][4],&l_74[0][0][4],&l_74[2][0][7]},{&l_73,(void*)0,&l_74[0][0][4]},{&l_73,(void*)0,&l_71},{&l_73,&l_73,&l_73},{&l_74[0][0][4],&l_73,&l_71},{&l_73,&l_74[0][0][4],&l_74[0][0][4]},{&l_74[5][0][8],&l_74[2][0][7],&l_74[2][0][7]}}};
    int32_t *l_84 = &l_75;
    uint16_t *l_85 = (void*)0;
    uint16_t *l_86[10];
    uint16_t l_102[2][3][7] = {{{0x9449L,0xD4D0L,0x9449L,0xD4D0L,0x9449L,0xD4D0L,0x9449L},{0UL,0UL,0UL,0UL,0UL,0UL,0UL},{0x9449L,0xD4D0L,0x9449L,0xD4D0L,0x9449L,0xD4D0L,0x9449L}},{{0UL,0UL,0UL,0UL,0UL,0UL,0UL},{0x9449L,0xD4D0L,0x9449L,0xD4D0L,0x9449L,0xD4D0L,0x9449L},{0UL,0UL,0UL,0UL,0UL,0UL,0UL}}};
    uint8_t *l_103[9][3] = {{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104},{&g_104,&g_104,&g_104}};
    uint16_t l_127 = 6UL;
    int32_t *l_157 = &l_75;
    uint32_t l_180 = 1UL;
    float l_226 = 0xF.810B9Fp-70;
    int32_t *l_404 = (void*)0;
    int32_t *l_414 = &g_179[2];
    int32_t **l_413 = &l_414;
    int32_t ***l_415 = &g_354;
    int32_t ***l_416 = &l_413;
    uint64_t l_419[8] = {0UL,0xB1EA3303BF7EEAD8LL,0xB1EA3303BF7EEAD8LL,0UL,0xB1EA3303BF7EEAD8LL,0xB1EA3303BF7EEAD8LL,0UL,0xB1EA3303BF7EEAD8LL};
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_86[i] = &g_87;
    ++l_77[1][1][2];
    return l_103[4][0];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_19, "g_19", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_27[i][j], "g_27[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_68, "g_68", print_hash_value);
    transparent_crc(g_72, "g_72", print_hash_value);
    transparent_crc(g_76, "g_76", print_hash_value);
    transparent_crc(g_87, "g_87", print_hash_value);
    transparent_crc(g_101, "g_101", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc_bytes (&g_130, sizeof(g_130), "g_130", print_hash_value);
    transparent_crc(g_140, "g_140", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    transparent_crc(g_158.f0, "g_158.f0", print_hash_value);
    transparent_crc(g_170, "g_170", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_179[i], "g_179[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_246.f0, "g_246.f0", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_255[i], "g_255[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_260[i][j], "g_260[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_271.f0, "g_271.f0", print_hash_value);
    transparent_crc(g_287, "g_287", print_hash_value);
    transparent_crc(g_310, "g_310", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_371[i][j][k], "g_371[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_407.f0, "g_407.f0", print_hash_value);
    transparent_crc(g_407.f3, "g_407.f3", print_hash_value);
    transparent_crc(g_435.f0, "g_435.f0", print_hash_value);
    transparent_crc(g_436.f0, "g_436.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc_bytes(&g_452[i], sizeof(g_452[i]), "g_452[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_479[i], "g_479[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_548, "g_548", print_hash_value);
    transparent_crc(g_571, "g_571", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_592[i], "g_592[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_599.f0, "g_599.f0", print_hash_value);
    transparent_crc(g_616, "g_616", print_hash_value);
    transparent_crc(g_617.f0, "g_617.f0", print_hash_value);
    transparent_crc(g_653.f0, "g_653.f0", print_hash_value);
    transparent_crc(g_717.f0, "g_717.f0", print_hash_value);
    transparent_crc(g_717.f3, "g_717.f3", print_hash_value);
    transparent_crc(g_741, "g_741", print_hash_value);
    transparent_crc(g_802.f0, "g_802.f0", print_hash_value);
    transparent_crc(g_802.f3, "g_802.f3", print_hash_value);
    transparent_crc(g_829.f0, "g_829.f0", print_hash_value);
    transparent_crc(g_834, "g_834", print_hash_value);
    transparent_crc(g_982.f0, "g_982.f0", print_hash_value);
    transparent_crc(g_1249.f0, "g_1249.f0", print_hash_value);
    transparent_crc(g_1313, "g_1313", print_hash_value);
    transparent_crc(g_1352.f0, "g_1352.f0", print_hash_value);
    transparent_crc(g_1373.f3, "g_1373.f3", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_1379[i][j][k], "g_1379[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1381, "g_1381", print_hash_value);
    transparent_crc(g_1394.f0, "g_1394.f0", print_hash_value);
    transparent_crc(g_1394.f3, "g_1394.f3", print_hash_value);
    transparent_crc(g_1480.f3, "g_1480.f3", print_hash_value);
    transparent_crc(g_1495.f0, "g_1495.f0", print_hash_value);
    transparent_crc(g_1495.f3, "g_1495.f3", print_hash_value);
    transparent_crc(g_1545.f0, "g_1545.f0", print_hash_value);
    transparent_crc(g_1614.f0, "g_1614.f0", print_hash_value);
    transparent_crc(g_1617.f0, "g_1617.f0", print_hash_value);
    transparent_crc(g_1626.f0, "g_1626.f0", print_hash_value);
    transparent_crc(g_1627.f0, "g_1627.f0", print_hash_value);
    transparent_crc(g_1627.f3, "g_1627.f3", print_hash_value);
    transparent_crc(g_1648, "g_1648", print_hash_value);
    transparent_crc(g_1660.f0, "g_1660.f0", print_hash_value);
    transparent_crc(g_1726, "g_1726", print_hash_value);
    transparent_crc(g_1731, "g_1731", print_hash_value);
    transparent_crc(g_1741.f0, "g_1741.f0", print_hash_value);
    transparent_crc(g_1741.f3, "g_1741.f3", print_hash_value);
    transparent_crc(g_1743.f0, "g_1743.f0", print_hash_value);
    transparent_crc(g_1743.f3, "g_1743.f3", print_hash_value);
    transparent_crc(g_1792, "g_1792", print_hash_value);
    transparent_crc(g_1794, "g_1794", print_hash_value);
    transparent_crc(g_1856, "g_1856", print_hash_value);
    transparent_crc(g_1863, "g_1863", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 482
XXX total union variables: 22

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 50
breakdown:
   depth: 1, occurrence: 122
   depth: 2, occurrence: 39
   depth: 3, occurrence: 4
   depth: 4, occurrence: 2
   depth: 6, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 12, occurrence: 1
   depth: 15, occurrence: 1
   depth: 17, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 1
   depth: 26, occurrence: 1
   depth: 33, occurrence: 1
   depth: 36, occurrence: 2
   depth: 40, occurrence: 1
   depth: 46, occurrence: 1
   depth: 50, occurrence: 1

XXX total number of pointers: 500

XXX times a variable address is taken: 1316
XXX times a pointer is dereferenced on RHS: 340
breakdown:
   depth: 1, occurrence: 176
   depth: 2, occurrence: 60
   depth: 3, occurrence: 48
   depth: 4, occurrence: 18
   depth: 5, occurrence: 38
XXX times a pointer is dereferenced on LHS: 225
breakdown:
   depth: 1, occurrence: 171
   depth: 2, occurrence: 27
   depth: 3, occurrence: 14
   depth: 4, occurrence: 7
   depth: 5, occurrence: 6
XXX times a pointer is compared with null: 30
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 8
XXX times a pointer is qualified to be dereferenced: 6709

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1107
   level: 2, occurrence: 258
   level: 3, occurrence: 187
   level: 4, occurrence: 94
   level: 5, occurrence: 129
XXX number of pointers point to pointers: 173
XXX number of pointers point to scalars: 314
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 24.4
XXX average alias set size: 1.41

XXX times a non-volatile is read: 1788
XXX times a non-volatile is write: 752
XXX times a volatile is read: 112
XXX    times read thru a pointer: 44
XXX times a volatile is write: 49
XXX    times written thru a pointer: 27
XXX times a volatile is available for access: 4.69e+03
XXX percentage of non-volatile access: 94

XXX forward jumps: 0
XXX backward jumps: 11

XXX stmts: 124
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 27
   depth: 1, occurrence: 17
   depth: 2, occurrence: 14
   depth: 3, occurrence: 23
   depth: 4, occurrence: 22
   depth: 5, occurrence: 21

XXX percentage a fresh-made variable is used: 17.6
XXX percentage an existing variable is used: 82.4
********************* end of statistics **********************/


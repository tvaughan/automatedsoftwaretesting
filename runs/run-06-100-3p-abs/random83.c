/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1210739749
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   volatile int16_t  f0;
   volatile unsigned f1 : 6;
   uint64_t  f2;
   int16_t  f3;
   int64_t  f4;
   volatile uint64_t  f5;
   const volatile uint32_t  f6;
   int16_t  f7;
   volatile uint64_t  f8;
   const volatile uint32_t  f9;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static uint32_t g_2[3] = {0x3250E6DAL,0x3250E6DAL,0x3250E6DAL};
static int32_t g_3 = 0xB274BF90L;
static volatile int32_t g_4 = 7L;/* VOLATILE GLOBAL g_4 */
static int32_t g_5 = 0xE58BFC4FL;
static int32_t g_12 = 0xB3BFA32AL;
static int32_t g_15 = 1L;
static volatile int32_t g_18 = 0xD66C4A87L;/* VOLATILE GLOBAL g_18 */
static volatile int32_t g_19 = 5L;/* VOLATILE GLOBAL g_19 */
static int32_t g_20[5][10][5] = {{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}}};
static int32_t g_21[7] = {0x0FAAB39EL,0x0FAAB39EL,0x0FAAB39EL,0x0FAAB39EL,0x0FAAB39EL,0x0FAAB39EL,0x0FAAB39EL};
static float g_48[5][10] = {{(-0x1.3p+1),0xC.7E9069p+61,(-0x1.3p+1),0xA.DBEFC7p+47,0x6.CE2E7Cp+78,0xF.3F8502p-52,0xB.1AA435p-11,0x0.Ep+1,(-0x8.Cp-1),0xA.83CD0Cp+34},{0xA.83CD0Cp+34,0xF.3F8502p-52,0x6.5879A2p+6,0x0.1p+1,0x0.Ep+1,0x2.C75278p+79,0x2.C75278p+79,0x0.Ep+1,0x0.1p+1,0x6.5879A2p+6},{0xF.3B1AD3p-40,0xF.3B1AD3p-40,(-0x1.3p+1),(-0x3.5p-1),(-0x8.Cp-1),0xC.7E9069p+61,0x2.6AE69Ep+16,0x4.F2CA12p+62,0x0.Ep+1,0x1.1p-1},{0x2.C75278p+79,0xB.1AA435p-11,0xF.3B1AD3p-40,0x6.93CE85p-45,0x2.6AE69Ep+16,0x6.CE2E7Cp+78,0x2.6AE69Ep+16,0x6.93CE85p-45,0xF.3B1AD3p-40,0xB.1AA435p-11},{0x0.Ep+1,0xF.3B1AD3p-40,0x0.1p+1,0xA.83CD0Cp+34,0x1.1p-1,0xA.DBEFC7p+47,0x2.C75278p+79,0x6.CE2E7Cp+78,(-0x3.5p-1),0x4.F2CA12p+62}};
static float *g_79[9][10] = {{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]},{&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[4][0],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]}};
static float **g_78 = &g_79[0][9];
static float *** volatile g_80 = &g_78;/* VOLATILE GLOBAL g_80 */
static int32_t g_87 = 0x3A44ADEFL;
static int32_t *g_86 = &g_87;
static uint64_t g_90 = 0xA3C80536D0B4E555LL;
static int32_t g_106 = 0xEB01B519L;
static float g_113 = 0x3.FBE49Ep+32;
static int64_t g_114 = 0x54D451FECA717395LL;
static uint32_t g_116 = 0xD4CACFD2L;
static uint64_t g_119 = 0UL;
static uint16_t g_129 = 0xE8C2L;
static volatile struct S0 g_131 = {0L,5,0xD3D22307A3C17D6ELL,7L,9L,0UL,7UL,0x89C8L,1UL,0x920A84A5L};/* VOLATILE GLOBAL g_131 */
static int8_t g_141 = 0x69L;
static uint8_t g_174 = 0xCEL;
static int16_t g_204 = (-1L);
static int8_t g_207 = 1L;
static int32_t g_230 = 0L;
static uint16_t *g_289 = &g_129;
static uint8_t g_301 = 255UL;
static volatile int32_t g_302[2] = {(-5L),(-5L)};
static float * volatile g_303[6][10][4] = {{{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,(void*)0,&g_113},{(void*)0,&g_113,(void*)0,&g_113}},{{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,(void*)0,&g_113},{(void*)0,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,(void*)0,&g_113},{&g_113,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,(void*)0,&g_113},{&g_113,&g_113,(void*)0,(void*)0},{&g_113,&g_113,&g_113,&g_113}},{{&g_113,&g_113,&g_113,&g_113},{(void*)0,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,(void*)0,&g_113},{(void*)0,&g_113,(void*)0,(void*)0},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,(void*)0,&g_113}},{{(void*)0,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,(void*)0,&g_113},{(void*)0,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,(void*)0,&g_113},{&g_113,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,(void*)0,&g_113},{&g_113,&g_113,(void*)0,(void*)0}},{{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{(void*)0,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,(void*)0,&g_113},{(void*)0,&g_113,(void*)0,(void*)0},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,&g_113,&g_113}},{{&g_113,(void*)0,(void*)0,&g_113},{(void*)0,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_113,&g_113},{&g_113,&g_113,(void*)0,&g_113},{&g_113,&g_113,&g_113,&g_113},{(void*)0,(void*)0,&g_113,(void*)0},{&g_113,&g_113,&g_113,(void*)0},{&g_113,&g_113,&g_113,&g_113},{&g_113,(void*)0,&g_113,(void*)0},{&g_113,&g_113,&g_113,&g_113}}};
static volatile uint32_t g_305 = 0xE7D89B47L;/* VOLATILE GLOBAL g_305 */
static int16_t g_322 = 0x748FL;
static int16_t g_324 = (-5L);
static const uint16_t g_328 = 65533UL;
static int64_t g_331 = 0x3FDFB1FC32AD577ELL;
static volatile struct S0 g_336 = {0x480FL,2,18446744073709551615UL,0L,0x4274B08A3A49C99BLL,0x29E2CB7D202A4483LL,4294967287UL,0xE29CL,18446744073709551609UL,18446744073709551615UL};/* VOLATILE GLOBAL g_336 */
static int16_t g_355 = 1L;
static int32_t g_357[1][6] = {{1L,1L,1L,1L,1L,1L}};
static float * volatile * volatile * volatile g_372 = (void*)0;/* VOLATILE GLOBAL g_372 */
static float * volatile * volatile * volatile *g_371[4][7][4] = {{{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372}},{{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372}},{{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372}},{{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372},{&g_372,&g_372,&g_372,&g_372}}};
static float * volatile * volatile * volatile **g_370 = &g_371[0][0][3];
static float * volatile g_444 = (void*)0;/* VOLATILE GLOBAL g_444 */
static float * volatile g_445 = &g_48[3][6];/* VOLATILE GLOBAL g_445 */
static uint16_t **g_514[10][4] = {{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289},{&g_289,&g_289,&g_289,&g_289}};
static int32_t ** volatile g_525 = (void*)0;/* VOLATILE GLOBAL g_525 */
static struct S0 g_536 = {0x90C7L,2,0x4D990C2477CC2462LL,0L,0L,0x49F9E8E7A7D9FDAALL,4294967295UL,1L,0x69381AE926B2A162LL,0xF59A86B9L};/* VOLATILE GLOBAL g_536 */
static const int32_t **g_588 = (void*)0;
static volatile struct S0 g_595 = {0xAF55L,5,18446744073709551612UL,0xB201L,0x818AF3D08A5053CDLL,18446744073709551615UL,0UL,0x7768L,18446744073709551615UL,0UL};/* VOLATILE GLOBAL g_595 */
static uint16_t ***g_601 = &g_514[9][1];
static uint16_t ****g_600 = &g_601;
static volatile uint32_t g_639 = 4UL;/* VOLATILE GLOBAL g_639 */
static float ***g_646 = (void*)0;
static volatile int64_t g_691 = (-3L);/* VOLATILE GLOBAL g_691 */
static uint8_t *g_906[8] = {&g_174,&g_174,&g_174,&g_174,&g_174,&g_174,&g_174,&g_174};
static uint8_t *g_908 = &g_301;
static uint16_t ** const *g_979 = &g_514[7][0];
static uint16_t ** const **g_978 = &g_979;
static uint16_t ** const ***g_977 = &g_978;
static volatile int32_t g_1006 = 1L;/* VOLATILE GLOBAL g_1006 */
static struct S0 g_1017 = {0L,7,0x19230540B616F1FALL,0x5FF0L,-1L,0xE2BC3C5A8988AB23LL,1UL,0x7B7EL,0UL,0x8BE5978AL};/* VOLATILE GLOBAL g_1017 */
static int32_t ** volatile g_1057 = (void*)0;/* VOLATILE GLOBAL g_1057 */
static int8_t *g_1088[1] = {&g_207};
static int8_t **g_1087 = &g_1088[0];
static struct S0 g_1103[1][3] = {{{0L,7,0x1C5D58697E94DA3ELL,0L,0x15D5B2F63835A833LL,0x92B7C474DD1D5574LL,0x48DCFB81L,5L,0xBC08280476AE99B1LL,18446744073709551609UL},{0L,7,0x1C5D58697E94DA3ELL,0L,0x15D5B2F63835A833LL,0x92B7C474DD1D5574LL,0x48DCFB81L,5L,0xBC08280476AE99B1LL,18446744073709551609UL},{0L,7,0x1C5D58697E94DA3ELL,0L,0x15D5B2F63835A833LL,0x92B7C474DD1D5574LL,0x48DCFB81L,5L,0xBC08280476AE99B1LL,18446744073709551609UL}}};
static uint16_t *****g_1130 = (void*)0;
static uint64_t g_1140 = 0x50ACE5098C1DD916LL;
static int8_t g_1158 = 2L;
static int32_t ** volatile g_1182 = (void*)0;/* VOLATILE GLOBAL g_1182 */
static uint32_t g_1205 = 0xA03FC923L;
static volatile struct S0 *g_1253 = (void*)0;
static volatile int8_t ****g_1281 = (void*)0;
static uint32_t g_1288 = 7UL;
static int32_t ** volatile g_1295[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t ** volatile g_1296 = &g_86;/* VOLATILE GLOBAL g_1296 */
static int64_t *g_1351[10] = {&g_331,(void*)0,&g_331,&g_114,&g_114,&g_331,(void*)0,&g_331,&g_114,&g_114};
static volatile struct S0 g_1360 = {4L,2,0x9ABE94057466A5C9LL,0x54EEL,0xBEF84D5E3E8F421ELL,0xFEA895B205F169B0LL,0UL,0x73C5L,0x8A04E73562506FA8LL,0x736A01F0L};/* VOLATILE GLOBAL g_1360 */
static uint16_t g_1412 = 0x27FFL;
static uint8_t g_1487 = 0xC4L;
static struct S0 *g_1539 = (void*)0;
static struct S0 g_1541 = {0x06F0L,7,0x1A454BDB1BA539C8LL,0xCBBFL,0x8A7717FC3762908BLL,8UL,6UL,0xEE57L,0xA7DDF0F368172B33LL,1UL};/* VOLATILE GLOBAL g_1541 */
static uint16_t g_1557 = 0x8757L;
static uint64_t g_1560 = 0x5CBFF5F47947AEDCLL;
static volatile struct S0 g_1575 = {-8L,7,0xB0989412565373D9LL,0x2B46L,0xDA0FCEE35B182A1CLL,18446744073709551606UL,0xEFB51C10L,3L,18446744073709551615UL,0x1D84FB68L};/* VOLATILE GLOBAL g_1575 */
static float * volatile g_1621 = (void*)0;/* VOLATILE GLOBAL g_1621 */
static float * volatile g_1622 = &g_48[3][6];/* VOLATILE GLOBAL g_1622 */
static uint16_t * const *g_1660[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint16_t * const **g_1659 = &g_1660[2];
static uint16_t * const ***g_1658[8][3][4] = {{{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,(void*)0,&g_1659}},{{&g_1659,&g_1659,(void*)0,&g_1659},{&g_1659,(void*)0,&g_1659,(void*)0},{&g_1659,&g_1659,&g_1659,(void*)0}},{{&g_1659,(void*)0,&g_1659,&g_1659},{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,&g_1659,&g_1659}},{{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,(void*)0,&g_1659}},{{&g_1659,&g_1659,(void*)0,&g_1659},{&g_1659,(void*)0,&g_1659,(void*)0},{&g_1659,&g_1659,&g_1659,(void*)0}},{{&g_1659,(void*)0,&g_1659,&g_1659},{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,&g_1659,&g_1659}},{{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,&g_1659,&g_1659},{&g_1659,&g_1659,(void*)0,&g_1659}},{{&g_1659,&g_1659,(void*)0,&g_1659},{&g_1659,(void*)0,&g_1659,(void*)0},{&g_1659,&g_1659,&g_1659,(void*)0}}};
static uint16_t * const ****g_1657 = &g_1658[6][2][3];
static const uint64_t g_1664[1][9] = {{1UL,18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL}};
static int8_t ***g_1679[2][6] = {{&g_1087,&g_1087,&g_1087,&g_1087,&g_1087,&g_1087},{&g_1087,&g_1087,&g_1087,&g_1087,&g_1087,&g_1087}};
static int8_t ****g_1678 = &g_1679[1][3];
static int8_t *****g_1677 = &g_1678;
static struct S0 g_1708[4] = {{0xA812L,3,5UL,0xAC04L,0x03D6D89114B546DFLL,18446744073709551615UL,4294967295UL,-5L,0xBE1DAF94CC233F53LL,18446744073709551610UL},{0xA812L,3,5UL,0xAC04L,0x03D6D89114B546DFLL,18446744073709551615UL,4294967295UL,-5L,0xBE1DAF94CC233F53LL,18446744073709551610UL},{0xA812L,3,5UL,0xAC04L,0x03D6D89114B546DFLL,18446744073709551615UL,4294967295UL,-5L,0xBE1DAF94CC233F53LL,18446744073709551610UL},{0xA812L,3,5UL,0xAC04L,0x03D6D89114B546DFLL,18446744073709551615UL,4294967295UL,-5L,0xBE1DAF94CC233F53LL,18446744073709551610UL}};
static int64_t **g_1728 = &g_1351[4];
static int64_t *** volatile g_1727 = &g_1728;/* VOLATILE GLOBAL g_1727 */
static int32_t ** volatile g_1750 = &g_86;/* VOLATILE GLOBAL g_1750 */


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static int32_t  func_8(int32_t  p_9);
static int32_t * func_22(int32_t * p_23, int16_t  p_24, int32_t * p_25, int8_t  p_26, int32_t * p_27);
static int16_t  func_28(int32_t * p_29);
static int32_t * func_30(uint32_t  p_31, uint16_t  p_32, int32_t  p_33, uint8_t  p_34, uint32_t  p_35);
static int64_t  func_36(uint32_t  p_37);
static uint64_t  func_42(int32_t * p_43);
static int32_t * func_44(uint32_t  p_45);
static int32_t  func_60(int32_t * p_61);
static int32_t * func_62(uint32_t  p_63, float * p_64, const uint64_t  p_65, float * p_66);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_1750 g_1296 g_86 g_106 g_322 g_141 g_87
 * writes: g_3 g_5 g_86 g_106 g_87 g_141
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_1744 = 4294967295UL;
    uint32_t l_1747 = 0xB95700C7L;
    float *l_1756[8] = {&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6],&g_48[3][6]};
    int32_t l_1757 = 0x7324D0E9L;
    int i;
    for (g_3 = 0; (g_3 <= 2); g_3 += 1)
    { /* block id: 3 */
        int16_t l_1746 = (-8L);
        int32_t *l_1749 = &g_106;
        for (g_5 = 2; (g_5 >= 0); g_5 -= 1)
        { /* block id: 6 */
            uint8_t **l_1736 = &g_906[7];
            uint8_t ***l_1735 = &l_1736;
            int16_t *l_1737[9][7][4] = {{{&g_1541.f3,&g_1541.f7,&g_1541.f7,&g_1017.f7},{&g_1017.f7,&g_1708[0].f7,(void*)0,&g_1017.f3},{(void*)0,&g_1541.f3,&g_1103[0][2].f7,&g_1017.f7},{&g_1103[0][2].f3,&g_1103[0][2].f3,&g_1541.f3,&g_1541.f7},{&g_1541.f7,(void*)0,&g_1017.f7,&g_1541.f7},{&g_1541.f3,&g_1017.f7,&g_1541.f3,&g_1103[0][2].f3},{&g_1103[0][2].f7,&g_1017.f7,&g_1103[0][2].f3,&g_1103[0][2].f7}},{{&g_1017.f3,(void*)0,&g_1103[0][2].f3,&g_1541.f3},{(void*)0,&g_1103[0][2].f3,&g_1541.f7,(void*)0},{(void*)0,&g_1541.f7,&g_1541.f7,&g_1017.f3},{&g_1541.f7,(void*)0,&g_1708[0].f3,(void*)0},{&g_1103[0][2].f3,&g_1708[0].f7,&g_1708[0].f3,&g_1103[0][2].f7},{&g_1103[0][2].f3,&g_1017.f3,(void*)0,&g_1103[0][2].f3},{&g_1103[0][2].f7,&g_1541.f7,&g_1541.f7,&g_1103[0][2].f3}},{{&g_1708[0].f3,&g_1017.f3,&g_1708[0].f7,(void*)0},{&g_1017.f7,&g_1708[0].f3,&g_1103[0][2].f7,&g_1017.f7},{&g_1017.f3,&g_1017.f3,&g_1708[0].f3,&g_1017.f7},{&g_1017.f7,&g_1017.f7,&g_1103[0][2].f3,(void*)0},{&g_1017.f3,(void*)0,(void*)0,&g_1708[0].f3},{&g_1103[0][2].f3,&g_1708[0].f7,&g_1541.f7,(void*)0},{&g_1017.f7,&g_1708[0].f7,&g_1541.f7,&g_1103[0][2].f3}},{{(void*)0,&g_1103[0][2].f3,&g_1017.f7,&g_1103[0][2].f3},{&g_1103[0][2].f3,&g_1103[0][2].f3,&g_1017.f7,(void*)0},{&g_1541.f3,&g_1541.f7,&g_1017.f7,(void*)0},{&g_1103[0][2].f7,(void*)0,&g_1017.f7,&g_1017.f7},{&g_1103[0][2].f7,&g_1103[0][2].f3,&g_1017.f7,&g_1017.f3},{&g_1541.f3,&g_1017.f7,&g_1017.f7,&g_1017.f3},{&g_1103[0][2].f3,(void*)0,&g_1017.f7,&g_1017.f7}},{{(void*)0,&g_1103[0][2].f3,&g_1541.f7,&g_1017.f7},{&g_1017.f7,&g_1017.f7,&g_1541.f7,&g_1103[0][2].f3},{&g_1103[0][2].f3,&g_1017.f7,(void*)0,(void*)0},{&g_1017.f3,&g_1017.f7,&g_1103[0][2].f3,&g_1103[0][2].f7},{&g_1017.f7,(void*)0,&g_1708[0].f3,&g_1017.f7},{&g_1017.f3,(void*)0,&g_1103[0][2].f7,&g_1541.f7},{&g_1017.f7,(void*)0,&g_1708[0].f7,&g_1103[0][2].f7}},{{&g_1708[0].f3,&g_1017.f7,&g_1541.f7,(void*)0},{&g_1103[0][2].f7,&g_1103[0][2].f3,(void*)0,(void*)0},{&g_1103[0][2].f3,&g_1103[0][2].f3,&g_1708[0].f3,&g_1017.f7},{&g_1103[0][2].f3,&g_1708[0].f7,&g_1708[0].f3,(void*)0},{&g_1541.f7,&g_1017.f7,&g_1541.f7,(void*)0},{(void*)0,&g_1103[0][2].f3,&g_1541.f7,(void*)0},{&g_1017.f7,&g_1103[0][2].f7,(void*)0,&g_1017.f7}},{{&g_1017.f7,&g_1708[0].f3,(void*)0,&g_1103[0][2].f7},{&g_1017.f7,(void*)0,&g_1541.f7,&g_1708[0].f7},{&g_1017.f7,&g_1541.f3,(void*)0,&g_1541.f3},{(void*)0,&g_1541.f3,&g_1708[0].f7,&g_1708[0].f7},{&g_1708[0].f7,&g_1708[0].f7,&g_1541.f3,&g_1541.f3},{&g_1103[0][2].f7,(void*)0,(void*)0,&g_1017.f3},{&g_1541.f7,&g_1103[0][2].f3,(void*)0,(void*)0}},{{&g_1708[0].f3,&g_1103[0][2].f3,&g_1017.f3,&g_1017.f3},{&g_1103[0][2].f3,(void*)0,&g_1103[0][2].f3,&g_1541.f3},{&g_1708[0].f7,&g_1708[0].f7,&g_1017.f7,&g_1708[0].f7},{&g_1708[0].f7,&g_1541.f3,&g_1103[0][2].f3,&g_1541.f3},{&g_1708[0].f3,&g_1541.f3,&g_1708[0].f7,&g_1708[0].f7},{&g_1017.f3,(void*)0,&g_1017.f7,&g_1103[0][2].f7},{&g_1017.f3,&g_1708[0].f3,&g_1017.f3,&g_1017.f7}},{{&g_1103[0][2].f3,&g_1103[0][2].f3,&g_1541.f7,&g_1017.f7},{(void*)0,&g_1103[0][2].f3,&g_1017.f7,(void*)0},{&g_1017.f3,&g_1708[0].f7,&g_1103[0][2].f3,(void*)0},{&g_1017.f3,&g_1103[0][2].f7,(void*)0,&g_1103[0][2].f3},{&g_1017.f7,&g_1017.f7,(void*)0,&g_1017.f7},{(void*)0,(void*)0,&g_1017.f7,(void*)0},{&g_1017.f3,&g_1017.f3,&g_1017.f7,&g_1541.f7}}};
            float *l_1748 = &g_113;
            int i, j, k;
        }
        (*g_1750) = l_1749;
        if ((**g_1296))
            continue;
        (*g_86) = ((*l_1749) > g_322);
    }
    (*g_86) = 0x8AA99C2FL;
    for (g_141 = (-13); (g_141 == 16); g_141++)
    { /* block id: 796 */
        uint16_t l_1755 = 0x8FEFL;
        for (g_87 = 26; (g_87 == (-18)); g_87 = safe_sub_func_int32_t_s_s(g_87, 1))
        { /* block id: 799 */
            if (l_1755)
                break;
            if (l_1755)
                continue;
        }
    }
    l_1757 = 0xE.1FDA93p+67;
    return l_1747;
}


/* ------------------------------------------ */
/* 
 * reads : g_12 g_20 g_2 g_3 g_21 g_78 g_80 g_18 g_86 g_87 g_48 g_15 g_116 g_119 g_131 g_141 g_4 g_129 g_174 g_204 g_207 g_106 g_79 g_230 g_114 g_302 g_305 g_324 g_331 g_336 g_5 g_355 g_357 g_370 g_536.f7 g_536.f3 g_977 g_908 g_536.f1 g_322 g_1017 g_595.f3 g_301 g_536.f4 g_906 g_1087 g_1103 g_536.f9 g_1360 g_1088 g_90 g_1296 g_536.f2 g_445 g_1412 g_1158 g_289 g_1575 g_1541.f6 g_1622
 * writes: g_12 g_15 g_20 g_21 g_78 g_86 g_90 g_106 g_48 g_116 g_119 g_129 g_141 g_114 g_4 g_87 g_174 g_204 g_207 g_230 g_79 g_113 g_289 g_301 g_305 g_322 g_324 g_355 g_357 g_536.f3 g_536.f4 g_331 g_646 g_1087 g_536.f7 g_2 g_1351 g_536.f2 g_1158
 */
static int32_t  func_8(int32_t  p_9)
{ /* block id: 7 */
    int64_t *l_1386 = (void*)0;
    int32_t l_1406 = 0x77135E07L;
    int32_t l_1413 = 5L;
    int32_t l_1421 = (-1L);
    int32_t l_1424 = 0xA0A3CA1AL;
    int32_t l_1426 = 0x7F74E6B7L;
    int32_t l_1427 = (-8L);
    int32_t l_1428 = 0x1E52F759L;
    int32_t l_1430[6] = {(-1L),0x8EC9A549L,(-1L),(-1L),0x8EC9A549L,(-1L)};
    int32_t * const *l_1504 = &g_86;
    int64_t l_1520 = 0L;
    uint32_t l_1585 = 0xEDB91947L;
    int8_t l_1587 = 0x9BL;
    float l_1592 = 0xC.4275DEp+32;
    uint64_t l_1599 = 9UL;
    uint16_t l_1604[6] = {1UL,4UL,1UL,1UL,4UL,1UL};
    float l_1619 = (-0x9.Cp+1);
    uint8_t **l_1652 = &g_906[7];
    uint8_t ***l_1651[3];
    uint16_t * const ****l_1661 = (void*)0;
    int8_t ** const ***l_1680[2];
    uint64_t l_1691[3][10][5] = {{{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL}},{{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL}},{{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL},{7UL,0x3FF8544B78C80DCBLL,0x5D10E977F711D065LL,0x5D10E977F711D065LL,0x3FF8544B78C80DCBLL},{0x22A36B490734835ELL,0x60F85C3A4DE7BC8BLL,0UL,0UL,0x60F85C3A4DE7BC8BLL}}};
    int32_t l_1722 = 0x607DECDEL;
    float l_1731 = 0x8.976F91p+74;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1651[i] = &l_1652;
    for (i = 0; i < 2; i++)
        l_1680[i] = (void*)0;
    for (p_9 = (-26); (p_9 <= (-2)); p_9++)
    { /* block id: 10 */
        uint8_t l_49 = 0x49L;
        int32_t l_351 = 0L;
        int8_t *l_1396 = &g_207;
        int8_t **l_1395 = &l_1396;
        for (g_12 = (-3); (g_12 < 3); g_12 = safe_add_func_uint16_t_u_u(g_12, 2))
        { /* block id: 13 */
            int32_t *l_353 = &g_15;
            int16_t l_940 = 0x2403L;
            for (g_15 = 6; (g_15 != (-2)); g_15--)
            { /* block id: 16 */
                float *l_77 = &g_48[3][6];
                uint64_t l_358 = 0xB1C8B130256894C6LL;
                for (g_20[2][0][2] = 2; (g_20[2][0][2] >= 0); g_20[2][0][2] -= 1)
                { /* block id: 19 */
                    int32_t *l_352 = &g_5;
                    int32_t *l_1381 = (void*)0;
                    int i;
                    for (g_21[4] = 2; (g_21[4] >= 0); g_21[4] -= 1)
                    { /* block id: 22 */
                        float *l_46 = (void*)0;
                        float *l_47[5];
                        uint8_t *l_350 = &g_174;
                        int16_t *l_354 = &g_355;
                        int32_t *l_356 = &g_357[0][4];
                        int8_t **l_1392 = &g_1088[0];
                        int8_t ***l_1391 = &l_1392;
                        int8_t **l_1394 = &g_1088[0];
                        int8_t ***l_1393[7][10][3] = {{{&g_1087,&l_1394,&l_1394},{&l_1394,&g_1087,&g_1087},{&g_1087,&l_1394,&l_1394},{&g_1087,&g_1087,&l_1394},{(void*)0,&l_1394,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,(void*)0,&l_1394},{(void*)0,&l_1394,&g_1087},{&l_1394,&g_1087,&l_1394},{&l_1394,&l_1394,&l_1394}},{{&l_1394,&g_1087,(void*)0},{(void*)0,&g_1087,&l_1394},{&g_1087,(void*)0,&l_1394},{&l_1394,&g_1087,&l_1394},{(void*)0,(void*)0,&l_1394},{&g_1087,&g_1087,&g_1087},{&g_1087,&g_1087,&l_1394},{&l_1394,&l_1394,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,&l_1394,&g_1087}},{{&g_1087,(void*)0,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,&l_1394,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,&l_1394,(void*)0},{&g_1087,&g_1087,&l_1394},{&l_1394,&l_1394,&l_1394},{&l_1394,&g_1087,&g_1087},{&g_1087,&l_1394,&l_1394},{&g_1087,&g_1087,&l_1394}},{{(void*)0,&l_1394,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,(void*)0,&l_1394},{(void*)0,&l_1394,&g_1087},{&l_1394,&g_1087,&l_1394},{&l_1394,&l_1394,&l_1394},{&l_1394,&g_1087,(void*)0},{(void*)0,&g_1087,&l_1394},{&g_1087,(void*)0,&l_1394},{&l_1394,&g_1087,&l_1394}},{{(void*)0,(void*)0,&l_1394},{&g_1087,&g_1087,&g_1087},{&g_1087,&g_1087,&l_1394},{&l_1394,&l_1394,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,&l_1394,&g_1087},{&g_1087,(void*)0,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,&l_1394,&l_1394},{&l_1394,&g_1087,&l_1394}},{{&g_1087,&l_1394,(void*)0},{&g_1087,&g_1087,&l_1394},{&l_1394,&l_1394,&l_1394},{&l_1394,&g_1087,&g_1087},{&g_1087,&l_1394,&l_1394},{&g_1087,&g_1087,&l_1394},{(void*)0,&l_1394,&l_1394},{&l_1394,&g_1087,&l_1394},{&g_1087,(void*)0,&l_1394},{(void*)0,&l_1394,&g_1087}},{{&l_1394,&g_1087,&l_1394},{&l_1394,&l_1394,&l_1394},{&l_1394,&g_1087,(void*)0},{(void*)0,&g_1087,&l_1394},{(void*)0,&l_1394,&g_1087},{&l_1394,&g_1087,(void*)0},{&l_1394,&l_1394,&g_1087},{&g_1087,&l_1394,&l_1394},{&g_1087,&l_1394,&l_1394},{&g_1087,&l_1394,&l_1394}}};
                        int8_t l_1397 = 0xFEL;
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                            l_47[i] = &g_48[3][6];
                        l_1381 = func_22(&g_21[4], func_28(func_30((func_36(((((void*)0 == &g_20[2][4][3]) , (safe_mod_func_int16_t_s_s((safe_add_func_uint64_t_u_u(func_42(func_44(((((*l_77) = (((l_49 = 0x6.A687E9p-64) == (((g_2[g_20[2][0][2]] | ((*l_356) ^= (safe_rshift_func_int16_t_s_s((-8L), ((*l_354) |= (((safe_rshift_func_int16_t_s_s((((((*l_350) = (safe_mod_func_uint64_t_u_u(((safe_div_func_int32_t_s_s(((func_60(func_62((safe_div_func_uint16_t_u_u(((((p_9 , 5UL) && ((safe_lshift_func_int8_t_s_u(((safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s(0x01L, p_9)) | p_9), 65530UL)), p_9)) & 0xECE9L), 6)) || g_3)) ^ 1L) || p_9), 0x9AABL)), l_47[2], g_21[4], l_77)) , g_2[g_20[2][0][2]]) != g_5), p_9)) | 0L), g_15))) , (**g_80)) != l_77) || 0xB3D1L), l_351)) , l_352) != l_353)))))) , p_9) <= 0x9.2p+1)) != l_358)) , 0x26L) ^ p_9))), 1UL)), 0x2265L))) , p_9)) != 0x838FA34CE3421BABLL), l_940, l_358, p_9, p_9)), l_352, g_20[4][9][1], &g_21[4]);
                        l_351 = (((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((l_1386 == (void*)0) , (safe_sub_func_float_f_f(p_9, (safe_div_func_float_f_f((((void*)0 == (*g_1087)) > l_351), 0x1.A7E8DCp-69))))), (((*l_1391) = (void*)0) != (l_1395 = ((0xBBL != p_9) , (void*)0))))), 0x3.23532Ap-44)) < (*l_352)) > l_1397);
                    }
                    if (g_2[g_20[2][0][2]])
                        break;
                    return g_2[g_20[2][0][2]];
                }
                if (l_358)
                    continue;
            }
        }
        return p_9;
    }
    for (g_536.f2 = (-4); (g_536.f2 != 48); g_536.f2 = safe_add_func_uint32_t_u_u(g_536.f2, 2))
    { /* block id: 674 */
        uint64_t l_1402 = 2UL;
        const float *l_1409 = &g_113;
        int32_t l_1419[9][3][6] = {{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}},{{0L,(-1L),0x28A5592BL,0x28A5592BL,(-1L),0L},{0L,0x5B911004L,0x581B97D3L,0x28A5592BL,0x5B911004L,0x28A5592BL},{0L,0x88D4406AL,0L,0x28A5592BL,0x88D4406AL,0x581B97D3L}}};
        int32_t l_1420 = 6L;
        uint8_t **l_1441 = &g_908;
        uint16_t l_1470[3];
        float ***l_1477 = &g_78;
        const struct S0 *l_1515 = &g_1103[0][2];
        float ****l_1517 = &g_646;
        float *****l_1516 = &l_1517;
        struct S0 *l_1540 = &g_1541;
        const uint32_t l_1542[3][9] = {{0x334656D5L,4294967290UL,4294967290UL,0x334656D5L,0xB6E3AFA3L,0xD40AE375L,6UL,1UL,6UL},{1UL,0x334656D5L,4294967290UL,4294967290UL,0x334656D5L,0xB6E3AFA3L,0xD40AE375L,6UL,1UL},{0x57B0F97EL,0xA6C7693DL,0xB6E3AFA3L,1UL,1UL,0xB6E3AFA3L,0xA6C7693DL,0x57B0F97EL,0x334656D5L}};
        int32_t l_1719 = 0x8E3BCE9EL;
        int32_t l_1720[3][2] = {{0x265595D1L,0x265595D1L},{0x265595D1L,0x265595D1L},{0x265595D1L,0x265595D1L}};
        int32_t l_1721 = 0L;
        int32_t *l_1729 = &l_1430[2];
        int32_t *l_1730[6];
        uint32_t l_1732 = 0x71D0B5EAL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1470[i] = 0x0EF5L;
        for (i = 0; i < 6; i++)
            l_1730[i] = &g_357[0][3];
        l_1413 &= (safe_sub_func_uint64_t_u_u(l_1402, (((((*g_445) , (safe_rshift_func_uint8_t_u_u((((((safe_unary_minus_func_int8_t_s(l_1406)) >= ((safe_div_func_uint16_t_u_u((((void*)0 != l_1409) > ((&g_1281 == &g_1281) ^ (l_1406 <= 1L))), p_9)) & p_9)) < 1UL) , p_9) <= 0UL), 3))) , g_131.f2) && (-1L)) == g_1412)));
        for (g_141 = 0; (g_141 < 3); g_141 = safe_add_func_uint64_t_u_u(g_141, 2))
        { /* block id: 678 */
            float l_1422 = 0x7.5AB9C8p+14;
            int32_t l_1423 = (-5L);
            int32_t l_1429 = (-1L);
            int32_t l_1431 = 0x2C7F4477L;
            int32_t l_1432 = 1L;
            int32_t l_1433 = (-1L);
            int32_t l_1434 = 4L;
            uint32_t l_1435 = 4294967292UL;
            int32_t l_1459 = 1L;
            int32_t l_1460 = 8L;
            int32_t l_1462 = (-1L);
            int32_t l_1463 = (-10L);
            int32_t l_1464 = 0xD6F6BBD7L;
            int32_t l_1465 = (-4L);
            int32_t l_1466 = 0xF68AE531L;
            int32_t l_1468 = 9L;
            int32_t l_1469 = 0x4925EB7BL;
            uint32_t l_1482 = 4294967293UL;
            struct S0 *l_1514 = &g_1103[0][2];
            float *****l_1544 = &l_1517;
            int64_t l_1559 = 0L;
            uint16_t ****l_1626 = &g_601;
            uint32_t l_1639[5];
            uint64_t *l_1662 = &g_1103[0][2].f2;
            const uint64_t *l_1663 = &g_1664[0][4];
            uint32_t l_1724 = 0x9FB40EE3L;
            int i;
            for (i = 0; i < 5; i++)
                l_1639[i] = 18446744073709551615UL;
            for (g_1158 = 4; (g_1158 > (-28)); g_1158--)
            { /* block id: 681 */
                int32_t *l_1418[5] = {&g_357[0][4],&g_357[0][4],&g_357[0][4],&g_357[0][4],&g_357[0][4]};
                int16_t l_1425[5];
                uint8_t **l_1440 = (void*)0;
                int32_t l_1458 = 0xC4CC63DAL;
                int32_t l_1461 = 1L;
                int8_t *l_1473[6] = {&g_141,&g_141,&g_141,&g_141,&g_141,&g_141};
                int32_t * const *l_1505 = &l_1418[2];
                int32_t **l_1625 = &g_86;
                int i;
                for (i = 0; i < 5; i++)
                    l_1425[i] = 0xDC11L;
                l_1435++;
                for (l_1427 = (-16); (l_1427 <= (-29)); l_1427 = safe_sub_func_int64_t_s_s(l_1427, 8))
                { /* block id: 685 */
                    float *l_1442[2];
                    int32_t l_1443 = 0x7EB3038FL;
                    int32_t l_1444 = 0L;
                    int32_t l_1452[8] = {0x2A9D9802L,0xF13FBCDBL,0xF13FBCDBL,0x2A9D9802L,0xF13FBCDBL,0xF13FBCDBL,0x2A9D9802L,0xF13FBCDBL};
                    float l_1467 = (-0x1.Cp-1);
                    const int8_t l_1519 = 0x5EL;
                    float *****l_1543 = &l_1517;
                    uint32_t l_1550[1];
                    const uint16_t *l_1558[8];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1442[i] = &g_48[2][0];
                    for (i = 0; i < 1; i++)
                        l_1550[i] = 0xD6508B8AL;
                    for (i = 0; i < 8; i++)
                        l_1558[i] = &g_1557;
                    l_1430[1] = (l_1440 == l_1441);
                    if (p_9)
                    { /* block id: 687 */
                        uint32_t l_1445 = 0xD3828A7CL;
                        ++l_1445;
                        return p_9;
                    }
                    else
                    { /* block id: 690 */
                        int16_t l_1448[1];
                        int32_t l_1449 = 0x64E0E23BL;
                        int32_t l_1450 = 0x9FDE71F7L;
                        int32_t l_1451 = 0L;
                        int32_t l_1453 = 0L;
                        int32_t l_1454 = 0L;
                        int32_t l_1455 = 0x0ACE45CAL;
                        int32_t l_1456 = (-10L);
                        int32_t l_1457[9];
                        int64_t *l_1476[9] = {&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114};
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1448[i] = 1L;
                        for (i = 0; i < 9; i++)
                            l_1457[i] = 1L;
                        --l_1470[0];
                        l_1451 = (((void*)0 != l_1473[5]) || (safe_rshift_func_uint8_t_u_u(((**l_1441) = 0xB9L), (((l_1419[4][0][2] = l_1423) && (((void*)0 != l_1477) | l_1457[6])) >= (safe_sub_func_uint16_t_u_u(0x5CAAL, ((safe_sub_func_int64_t_s_s((l_1427 & p_9), 0L)) , 1UL)))))));
                        --l_1482;
                        (*g_86) |= 1L;
                    }
                }
                for (g_536.f4 = 8; (g_536.f4 != 18); g_536.f4 = safe_add_func_int8_t_s_s(g_536.f4, 5))
                { /* block id: 728 */
                    float l_1570 = 0x5.055C39p+79;
                    int32_t l_1584 = 0x6F919551L;
                    int32_t l_1588 = (-1L);
                    int32_t l_1589 = 0x0CF80717L;
                    int32_t l_1590 = 0xAFB6CD2BL;
                    int32_t l_1591 = (-1L);
                    int32_t l_1594 = 1L;
                    int32_t l_1596 = 1L;
                    int32_t l_1597 = 0x5E9DACC6L;
                    int32_t l_1598 = 4L;
                    if (((((safe_lshift_func_uint16_t_u_s((safe_div_func_int32_t_s_s((safe_mod_func_uint32_t_u_u(g_536.f4, ((-2L) & (*g_289)))), (*g_86))), 8)) <= (safe_div_func_int64_t_s_s((l_1419[0][2][0] = (safe_div_func_uint32_t_u_u(((g_1575 , (*g_86)) && (safe_mod_func_int8_t_s_s((l_1585 |= ((((p_9 || p_9) >= (((safe_rshift_func_uint8_t_u_s((((safe_div_func_int32_t_s_s((safe_div_func_uint8_t_u_u((l_1584 = (0x20L | 0L)), l_1419[0][0][5])), l_1419[1][2][4])) < 0x3F828D54L) >= (**l_1504)), (**g_1087))) >= g_1541.f6) || 5L)) | 0x92AFL) , 0xCAL)), l_1420))), p_9))), p_9))) & (**l_1505)) < l_1459))
                    { /* block id: 732 */
                        int8_t l_1586[4][4] = {{0xEAL,0x8DL,0xEAL,0x8DL},{0xEAL,0x8DL,0xEAL,0x8DL},{0xEAL,0x8DL,0xEAL,0x8DL},{0xEAL,0x8DL,0xEAL,0x8DL}};
                        int32_t l_1593 = 0xA805913AL;
                        int32_t l_1595 = 0x2DE2C18DL;
                        float *l_1620 = (void*)0;
                        int i, j;
                        l_1599--;
                        l_1419[0][2][0] = ((safe_div_func_float_f_f(p_9, (l_1464 , p_9))) < (l_1433 = 0x0.7p+1));
                        (*g_1622) = (l_1604[5] <= ((safe_mul_func_float_f_f((safe_add_func_float_f_f(0xC.500DB7p+39, 0x1.2p-1)), 0x8.Dp+1)) < (l_1589 = (0x5.B57BB2p-74 > (+((safe_sub_func_float_f_f(0x1.3p+1, (l_1470[2] <= (+((safe_div_func_float_f_f(((safe_lshift_func_uint16_t_u_s(0x63BDL, ((safe_rshift_func_uint8_t_u_s(0x92L, l_1470[1])) >= l_1470[2]))) , l_1619), p_9)) != (-0x1.Fp+1)))))) <= l_1542[2][1]))))));
                        if (p_9)
                            continue;
                    }
                    else
                    { /* block id: 739 */
                        (**l_1504) = (safe_rshift_func_int8_t_s_s(0L, 6));
                    }
                }
                (*l_1625) = (*l_1504);
            }
            l_1626 = (void*)0;
            for (l_1599 = 0; (l_1599 <= 19); l_1599 = safe_add_func_uint8_t_u_u(l_1599, 9))
            { /* block id: 748 */
                const uint32_t l_1633 = 1UL;
                struct S0 **l_1634[4][3] = {{&l_1514,&l_1514,&l_1514},{&l_1514,&l_1540,&l_1540},{&l_1514,&l_1514,&l_1514},{&l_1514,&l_1540,&l_1540}};
                int32_t **l_1640 = &g_86;
                float *l_1693 = &l_1422;
                int32_t l_1726 = 0xE8E8C6ADL;
                int i, j;
            }
        }
        --l_1732;
    }
    return (**l_1504);
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_86 g_87 g_48 g_336.f5 g_78 g_79 g_355 g_336.f0 g_536.f7 g_536.f3 g_977 g_908 g_536.f1 g_204 g_2 g_3 g_322 g_1017 g_114 g_21 g_595.f3 g_301 g_336 g_141 g_131.f9 g_357 g_324 g_174 g_536.f4 g_906 g_1087 g_1103 g_131.f0 g_536.f9 g_119 g_1360 g_116 g_1088 g_90 g_1296 g_331
 * writes: g_87 g_48 g_536.f3 g_174 g_322 g_301 g_355 g_114 g_536.f4 g_331 g_646 g_1087 g_536.f7 g_2 g_116 g_1351 g_207
 */
static int32_t * func_22(int32_t * p_23, int16_t  p_24, int32_t * p_25, int8_t  p_26, int32_t * p_27)
{ /* block id: 447 */
    float ****l_953[10];
    float *****l_952 = &l_953[8];
    float ***l_956 = &g_78;
    float ***l_957[2][4];
    float **** const l_955[6] = {&l_957[1][3],&l_957[1][3],&l_957[1][3],&l_957[1][3],&l_957[1][3],&l_957[1][3]};
    float **** const *l_954 = &l_955[3];
    int32_t l_958[9] = {0x930C343FL,0xADD3D4FBL,0x930C343FL,0xADD3D4FBL,0x930C343FL,0xADD3D4FBL,0x930C343FL,0xADD3D4FBL,0x930C343FL};
    int16_t *l_961[7][9][2] = {{{(void*)0,&g_204},{&g_536.f3,&g_204},{&g_204,&g_355},{&g_204,&g_355},{&g_204,&g_204},{&g_536.f3,&g_204},{(void*)0,&g_355},{&g_324,&g_355},{&g_355,&g_324}},{{&g_204,&g_536.f7},{&g_204,&g_324},{&g_355,&g_355},{&g_324,&g_355},{(void*)0,&g_204},{&g_536.f3,&g_204},{&g_204,&g_355},{&g_204,&g_355},{&g_204,&g_204}},{{&g_536.f3,&g_204},{(void*)0,&g_355},{&g_324,&g_355},{&g_355,&g_324},{&g_204,&g_536.f7},{&g_204,&g_324},{&g_355,&g_355},{&g_324,&g_355},{(void*)0,&g_204}},{{&g_536.f3,&g_204},{&g_204,&g_355},{&g_204,&g_355},{&g_204,&g_204},{&g_536.f3,&g_204},{(void*)0,&g_355},{&g_324,&g_355},{&g_355,&g_324},{&g_204,&g_536.f7}},{{&g_204,&g_324},{&g_355,&g_355},{&g_324,&g_355},{(void*)0,&g_204},{&g_536.f3,&g_204},{&g_204,&g_355},{&g_204,&g_355},{&g_204,&g_204},{&g_536.f3,&g_204}},{{(void*)0,&g_355},{&g_324,&g_355},{&g_355,&g_324},{&g_204,&g_324},{&g_355,&g_536.f3},{&g_204,&g_324},{&g_536.f3,&g_355},{&g_204,&g_355},{&g_204,&g_536.f7}},{{&g_536.f7,&g_204},{&g_204,&g_204},{&g_536.f7,&g_536.f7},{&g_204,&g_355},{&g_204,&g_355},{&g_536.f3,&g_324},{&g_204,&g_536.f3},{&g_355,&g_324},{&g_355,&g_536.f3}}};
    uint32_t *l_962[1][1][7] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
    int32_t l_963[1];
    int32_t l_964 = 0xF70DD79AL;
    uint16_t l_1008 = 65531UL;
    uint16_t *****l_1129 = &g_600;
    int64_t *l_1194 = &g_1103[0][2].f4;
    int8_t l_1248[4] = {(-5L),(-5L),(-5L),(-5L)};
    int8_t **l_1334 = &g_1088[0];
    int32_t *l_1380 = &g_21[4];
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_953[i] = &g_646;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
            l_957[i][j] = &g_78;
    }
    for (i = 0; i < 1; i++)
        l_963[i] = (-1L);
lbl_1084:
    (*g_86) |= (*p_25);
    if (((safe_add_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u(18446744073709551615UL, (safe_div_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_s(((7UL > (safe_lshift_func_int16_t_s_u(g_336.f5, ((l_952 != (l_954 = &l_953[8])) & (((*g_78) == (*g_78)) || ((l_958[0] , (((((((l_963[0] = ((safe_mul_func_int16_t_s_s((l_958[2] = (p_24 || l_958[0])), g_355)) , p_26)) | l_964) <= 4294967293UL) , l_958[0]) , 0xE8362950L) , 0L) > (-5L))) | l_964)))))) || g_336.f0), p_26)), (*g_86))))), p_26)) && g_536.f7))
    { /* block id: 452 */
        uint16_t l_966 = 0UL;
        uint16_t *****l_981 = &g_600;
        int32_t l_989 = 0xD44CC285L;
        int32_t l_991 = (-2L);
        int32_t l_994 = 0x2AD84187L;
        int32_t l_996 = (-1L);
        int32_t l_1000 = 0L;
        int32_t l_1003 = 1L;
        int32_t l_1004[6] = {1L,1L,1L,1L,1L,1L};
        float ***l_1081 = (void*)0;
        int i;
        for (g_536.f3 = 0; (g_536.f3 <= 8); g_536.f3 += 1)
        { /* block id: 455 */
            int32_t *l_965[8] = {&g_357[0][4],&l_963[0],&g_357[0][4],&l_963[0],&g_357[0][4],&l_963[0],&g_357[0][4],&l_963[0]};
            float ****l_1053 = &l_957[1][3];
            int i;
            ++l_966;
            for (g_174 = 0; (g_174 <= 3); g_174 += 1)
            { /* block id: 459 */
                int32_t l_990 = 0x69688DDDL;
                int32_t l_992 = (-2L);
                int32_t l_993 = 0x7370BF00L;
                int32_t l_995 = 1L;
                int32_t l_997 = (-1L);
                int32_t l_998 = 1L;
                int32_t l_999 = 0x917C4C69L;
                int32_t l_1001 = (-9L);
                int32_t l_1002[3];
                int64_t l_1005 = (-1L);
                int32_t l_1007 = (-1L);
                uint16_t **l_1018 = &g_289;
                int32_t l_1024[2][9];
                int i, j;
                for (i = 0; i < 3; i++)
                    l_1002[i] = 0xC42C6B07L;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 9; j++)
                        l_1024[i][j] = 0L;
                }
                l_963[0] = l_958[g_536.f3];
                for (g_322 = 1; (g_322 >= 0); g_322 -= 1)
                { /* block id: 463 */
                    uint16_t ** const ***l_980 = &g_978;
                    int32_t l_988 = 0x1F29030DL;
                    int i, j, k;
                    (*g_86) = (l_958[g_322] = (+((+(safe_lshift_func_int16_t_s_s((safe_lshift_func_int16_t_s_u((((((*g_908) = ((safe_mul_func_uint8_t_u_u((&g_600 != (l_980 = g_977)), ((l_981 = l_981) != (void*)0))) != (-4L))) || (safe_lshift_func_uint8_t_u_s((safe_sub_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((g_355 &= (p_26 , g_536.f1)), g_204)), l_966)), g_2[1]))) & l_988) != g_3), l_966)), p_24))) && 0xA4L)));
                }
                --l_1008;
                l_958[8] = (safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_mul_func_float_f_f(((&l_955[3] == &g_371[0][0][3]) < p_26), p_26)), ((-0x1.1p+1) <= p_26))), ((g_1017 , l_1018) != (void*)0)));
                for (l_989 = 6; (l_989 >= 0); l_989 -= 1)
                { /* block id: 475 */
                    int64_t *l_1031 = &l_1005;
                    int64_t *l_1032 = (void*)0;
                    int32_t l_1047 = 0L;
                    int i;
                    if ((safe_unary_minus_func_uint32_t_u((safe_rshift_func_uint16_t_u_s((((l_965[4] == p_23) & (safe_mod_func_int16_t_s_s(p_24, ((((((l_1024[0][2] = ((*g_78) == (void*)0)) || (safe_lshift_func_uint8_t_u_s(((((((((safe_div_func_int32_t_s_s((safe_div_func_uint32_t_u_u((((p_26 , (g_114 ^= ((*l_1031) &= p_26))) , (safe_add_func_int64_t_s_s((safe_rshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u((((safe_div_func_int16_t_s_s(((((safe_add_func_uint64_t_u_u(((safe_lshift_func_uint8_t_u_u(((*g_908) = ((l_958[1] <= (*p_27)) ^ p_26)), p_24)) , g_595.f3), l_1002[1])) == 0xDAECL) <= 0x37B4L) >= p_26), l_963[0])) == l_989) != l_993), l_964)) == 0x0D7DC84BL), 15)) || l_1008), l_1008)), 1L))) < 0xD0L), 0xA7CA1FA7L)), (*p_27))) || 0xD6B7L) | (*p_27)) , p_26) > 0x9.CE7C07p-4) == l_958[4]) <= l_1047) , (*g_908)), l_1047))) > l_1001) & l_1047) != p_26) ^ p_24)))) , 0xFDF6L), p_26)))))
                    { /* block id: 480 */
                        int8_t l_1052 = 0xF4L;
                        l_958[3] ^= ((l_1047 , (((*p_23) >= (!(g_595.f3 , ((p_24 < ((safe_rshift_func_uint8_t_u_u((g_336 , ((*g_908) = (l_998 & (((g_141 != 3L) && (+((((*l_1031) = 0xD474890D21554853LL) , 0x800BAA97E39E2849LL) == 0xBCD18CE9EC38893CLL))) | 0xC3491B6434051AEFLL)))), p_26)) ^ g_21[4])) | 0xB3L)))) && l_991)) > (*g_86));
                        if (l_1052)
                            break;
                    }
                    else
                    { /* block id: 485 */
                        l_1047 ^= 0xDD28CF61L;
                    }
                }
            }
            for (p_24 = 6; (p_24 >= 0); p_24 -= 1)
            { /* block id: 492 */
                int32_t l_1054 = (-10L);
                float l_1055 = 0x0.5D3A98p-15;
                int32_t **l_1056 = &g_86;
                int i;
                if (l_958[g_536.f3])
                    break;
            }
            for (g_536.f4 = 3; (g_536.f4 >= 0); g_536.f4 -= 1)
            { /* block id: 503 */
                uint8_t l_1078 = 0x7EL;
                int32_t l_1079 = 7L;
                for (g_331 = 0; (g_331 <= 3); g_331 += 1)
                { /* block id: 506 */
                    uint64_t l_1074 = 0xEDB25FF95A2A176ALL;
                    uint8_t *l_1077[3][3] = {{&g_174,(void*)0,&g_174},{&g_174,(void*)0,&g_174},{&g_174,(void*)0,&g_174}};
                    uint16_t l_1080 = 0x7CD2L;
                    float ***l_1082[10] = {&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78,&g_78};
                    int i, j;
                    l_1004[5] = (safe_add_func_uint64_t_u_u(g_131.f9, (safe_mul_func_uint8_t_u_u(((((*p_27) & (*p_27)) != (safe_sub_func_int32_t_s_s((safe_rshift_func_int16_t_s_s(l_958[3], (safe_mod_func_int32_t_s_s((((safe_sub_func_int16_t_s_s(l_994, ((((((safe_lshift_func_int16_t_s_u((p_24 = ((safe_rshift_func_uint8_t_u_s(((l_1074 != p_26) >= (safe_add_func_uint64_t_u_u(p_24, ((((l_1078 = ((*g_908) = (g_357[0][4] < g_324))) , l_963[0]) < 0xA959L) == g_5)))), 4)) <= g_324)), l_964)) >= g_174) , (*p_25)) != p_26) & (*g_86)) , l_1074))) ^ l_1079) && l_1074), l_1080)))), 1L))) == (-1L)), 0L))));
                    for (l_996 = 1; (l_996 <= 7); l_996 += 1)
                    { /* block id: 513 */
                        int8_t l_1083 = 0x3BL;
                        l_1083 |= (l_1081 != ((**l_954) = l_1082[5]));
                        if (g_536.f3)
                            goto lbl_1084;
                    }
                    if (((safe_mod_func_uint32_t_u_u(((0L > (((void*)0 == g_906[(g_536.f4 + 2)]) , 0x305CL)) < 18446744073709551615UL), g_595.f3)) || p_26))
                    { /* block id: 518 */
                        return p_25;
                    }
                    else
                    { /* block id: 520 */
                        if (l_996)
                            goto lbl_1084;
                    }
                }
            }
            for (g_301 = 0; (g_301 <= 3); g_301 += 1)
            { /* block id: 527 */
                int8_t ***l_1089 = &g_1087;
                (*l_1089) = g_1087;
            }
        }
    }
    else
    { /* block id: 531 */
        uint32_t l_1100 = 0x779E776EL;
        int8_t ****l_1104 = (void*)0;
        int8_t ***l_1105 = &g_1087;
        int32_t l_1108[6];
        uint32_t *l_1111 = &g_2[0];
        int32_t l_1122[6] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int32_t l_1254 = 3L;
        uint16_t l_1276 = 65535UL;
        const uint64_t *l_1300 = (void*)0;
        int32_t l_1337 = 2L;
        int32_t l_1338[4][6][7] = {{{(-8L),0xDDC85D05L,(-2L),0xB59C2E9DL,0L,0L,0xFD778992L},{0xDDC85D05L,(-1L),0x46E55730L,0x8B35BB2CL,0xB558F64BL,(-7L),(-8L)},{0x345D6653L,0L,(-7L),2L,0x676CFDADL,(-5L),0x46855549L},{0x24F9264AL,0L,0xD8888D4DL,0xF18B762DL,0xF18B762DL,0xD8888D4DL,0L},{0x2B90A9CCL,(-1L),0x345D6653L,(-8L),0x971FDB3AL,(-1L),0x24F9264AL},{0xAEBEFB06L,0xDDC85D05L,0xB558F64BL,0x345D6653L,0xD7D6CB0BL,0x46855549L,0xAEBEFB06L}},{{0x24F9264AL,0xB59C2E9DL,1L,(-8L),0L,0L,0x2B90A9CCL},{0L,0xAEBEFB06L,(-5L),0xF18B762DL,0xAEBEFB06L,0x345D6653L,0x24F9264AL},{0x46855549L,0xF18B762DL,0x971FDB3AL,2L,0x8B35BB2CL,0x345D6653L,0x345D6653L},{(-8L),0x8B35BB2CL,0xA1E17ACFL,0x8B35BB2CL,(-8L),0L,0xDDC85D05L},{0xFD778992L,(-5L),0x345D6653L,0xB59C2E9DL,0x46E55730L,0x46855549L,(-8L)},{0x0A409D4AL,0x2B90A9CCL,0xAEBEFB06L,0x0A409D4AL,0x8B35BB2CL,(-1L),0L}},{{0xFD778992L,0xB59C2E9DL,0x39F83BFEL,0L,0x2B90A9CCL,0xD8888D4DL,0L},{(-8L),0x46855549L,0x46E55730L,0xB59C2E9DL,0x345D6653L,(-5L),0xFD778992L},{0x46855549L,0x4526D2FFL,0x46E55730L,(-7L),0xD7D6CB0BL,(-7L),0x46E55730L},{0L,0L,0x39F83BFEL,0x24F9264AL,0x676CFDADL,0L,0xDDC85D05L},{(-1L),0x39F83BFEL,0x79349100L,0xAEBEFB06L,0xD8888D4DL,0x79349100L,(-7L)},{0x79349100L,0x971FDB3AL,0x39F83BFEL,0L,0xB558F64BL,0L,6L}},{{0x0A409D4AL,2L,0xFD778992L,(-7L),0L,0x24F9264AL,0x0A409D4AL},{(-1L),0L,0xA1E17ACFL,(-5L),0x39F83BFEL,1L,0x0A409D4AL},{0x39F83BFEL,0x0A409D4AL,0xD8888D4DL,0xD8888D4DL,0x0A409D4AL,0x39F83BFEL,6L},{2L,0xAEBEFB06L,0xD7D6CB0BL,(-1L),0x46855549L,0x8B35BB2CL,(-7L)},{(-5L),0x037BC83CL,0xCDE0CB51L,0x46855549L,0L,1L,2L},{(-2L),0xAEBEFB06L,0x39F83BFEL,0L,(-5L),0x9B265A8DL,0L}}};
        uint8_t **l_1379[6][6] = {{&g_908,(void*)0,&g_906[7],&g_908,&g_908,&g_906[7]},{&g_908,&g_908,&g_906[7],(void*)0,&g_908,&g_906[7]},{(void*)0,&g_908,&g_906[7],&g_908,(void*)0,&g_906[7]},{&g_908,(void*)0,&g_906[7],&g_908,&g_908,&g_906[7]},{&g_908,&g_908,&g_906[7],(void*)0,&g_908,&g_906[7]},{(void*)0,&g_908,&g_906[7],&g_908,(void*)0,&g_906[7]}};
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_1108[i] = 0x5378BD12L;
        if (g_204)
            goto lbl_1084;
        (*g_86) |= (((safe_mod_func_int16_t_s_s(((((safe_div_func_float_f_f((-(safe_mul_func_float_f_f(((l_964 = (!(safe_div_func_float_f_f(l_963[0], (((*l_1111) = ((((l_1100--) , (((g_1103[0][2] , l_963[0]) > ((l_1105 = (g_1017.f3 , &g_1087)) != (void*)0)) > (safe_rshift_func_int16_t_s_s(l_1108[5], (g_536.f7 = (safe_mul_func_uint8_t_u_u((l_963[0] < (65535UL > 0xB45AL)), l_1108[5]))))))) < l_963[0]) , p_24)) , p_26))))) < l_1108[0]), l_1108[5]))), l_958[2])) , g_131.f0) ^ g_324) , 0x6F19L), p_24)) ^ l_1108[2]) , (*p_23));
        (*g_86) &= (g_174 || ((safe_div_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(l_958[6], (safe_add_func_int16_t_s_s(((safe_add_func_uint8_t_u_u(l_1100, ((&g_86 != &p_27) <= 0x624492D73DEAD1A7LL))) || ((p_24 = ((safe_add_func_int16_t_s_s(g_536.f9, p_26)) != (l_1122[1] , g_119))) < p_26)), l_964)))), p_26)) , 0UL));
        for (g_536.f4 = 1; (g_536.f4 >= 0); g_536.f4 -= 1)
        { /* block id: 543 */
            uint64_t l_1146 = 9UL;
            int32_t l_1147[10] = {1L,0x917AC313L,0x92FD74F1L,0x917AC313L,1L,1L,0x917AC313L,0x92FD74F1L,0x917AC313L,1L};
            uint16_t * const *l_1164 = &g_289;
            float **** const l_1193 = (void*)0;
            float **** const *l_1192 = &l_1193;
            uint8_t * const l_1196 = (void*)0;
            int32_t *l_1206[6][3] = {{&g_87,&g_87,&g_87},{&g_230,&g_230,&g_230},{&g_87,&g_87,&g_87},{&g_230,&g_230,&g_230},{&g_87,&g_87,&g_87},{&g_230,&g_230,&g_230}};
            uint64_t l_1228 = 0x47614DDD92E9E23ELL;
            uint8_t **l_1318[2];
            int8_t * const ***l_1344 = (void*)0;
            int8_t * const l_1348 = (void*)0;
            int8_t * const *l_1347 = &l_1348;
            int8_t * const **l_1346 = &l_1347;
            int8_t * const ***l_1345 = &l_1346;
            uint16_t l_1368 = 0x8F61L;
            int i, j;
            for (i = 0; i < 2; i++)
                l_1318[i] = &g_908;
            for (g_116 = 0; (g_116 <= 1); g_116 += 1)
            { /* block id: 546 */
                uint16_t *****l_1127 = &g_600;
                uint16_t *****l_1128 = &g_600;
                int32_t l_1169 = 0x8C24B4A8L;
                float ****l_1174 = &l_956;
                int32_t *l_1184 = &l_963[0];
                uint64_t *l_1189 = &g_1017.f2;
                int32_t l_1225 = 0L;
                int32_t l_1339 = 0x45C0EA8BL;
                uint32_t l_1340 = 0xB1B7AC8BL;
            }
            if (((l_1105 = &l_1334) != ((*l_1345) = (void*)0)))
            { /* block id: 640 */
                int64_t *l_1349 = &g_1017.f4;
                int64_t **l_1350[4];
                int32_t l_1358 = 0xEAF53077L;
                int32_t l_1361 = (-9L);
                int32_t l_1367 = 0x179578F0L;
                int i;
                for (i = 0; i < 4; i++)
                    l_1350[i] = (void*)0;
                l_958[0] = (((l_1194 = (g_1351[1] = l_1349)) == (void*)0) , ((safe_mul_func_float_f_f((safe_add_func_float_f_f((l_1254 = (safe_div_func_float_f_f(l_1100, (l_1358 = l_1122[2])))), ((l_963[0] = (!(l_1361 = (g_1360 , ((void*)0 != (*l_1345)))))) > (((safe_mul_func_float_f_f((safe_div_func_float_f_f(((!(((**l_1334) = (((l_1367 != p_24) || 0xD060L) <= g_116)) , l_958[0])) < l_1122[1]), 0x2.Cp+1)), p_24)) > l_1368) > p_26)))), l_1338[0][4][1])) == 0xE.B5DADCp+35));
                if (l_958[0])
                    break;
                return p_23;
            }
            else
            { /* block id: 651 */
                uint64_t *l_1375 = &l_1228;
                int32_t l_1376 = 0xEB25C850L;
                (*g_86) ^= (safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((l_1108[1] , 0L), (safe_mul_func_uint16_t_u_u((((*l_1375) = l_1122[1]) <= ((l_1376 , ((g_90 , 0x2701L) >= 0x6D86L)) || ((safe_rshift_func_uint16_t_u_s((l_1379[0][2] == &g_906[0]), p_26)) , p_26))), 0x1A6CL)))), l_1254));
                return l_1380;
            }
        }
    }
    return (*g_1296);
}


/* ------------------------------------------ */
/* 
 * reads : g_336.f1
 * writes:
 */
static int16_t  func_28(int32_t * p_29)
{ /* block id: 445 */
    return g_336.f1;
}


/* ------------------------------------------ */
/* 
 * reads : g_86
 * writes: g_87 g_48
 */
static int32_t * func_30(uint32_t  p_31, uint16_t  p_32, int32_t  p_33, uint8_t  p_34, uint32_t  p_35)
{ /* block id: 442 */
    int32_t *l_941 = (void*)0;
    (*g_86) = p_35;
    return l_941;
}


/* ------------------------------------------ */
/* 
 * reads : g_357
 * writes:
 */
static int64_t  func_36(uint32_t  p_37)
{ /* block id: 439 */
    int32_t *l_939 = (void*)0;
    l_939 = &g_357[0][4];
    return (*l_939);
}


/* ------------------------------------------ */
/* 
 * reads : g_204 g_4
 * writes: g_204
 */
static uint64_t  func_42(int32_t * p_43)
{ /* block id: 167 */
    int64_t l_403 = 9L;
    int32_t l_404 = 0x10D65EF1L;
    int32_t l_460[3];
    uint16_t **l_520 = (void*)0;
    uint16_t ***l_519 = &l_520;
    int16_t *l_561 = &g_322;
    int8_t l_628 = 0xACL;
    int32_t l_629 = 0xAF0FDFF1L;
    int32_t l_632 = (-5L);
    int32_t l_633[4][6][1] = {{{0x0E3037BFL},{0x78748CA6L},{0x78748CA6L},{0x0E3037BFL},{1L},{0xD91E8C78L}},{{(-3L)},{(-8L)},{(-3L)},{0xD91E8C78L},{1L},{0x0E3037BFL}},{{0x78748CA6L},{0x78748CA6L},{0x0E3037BFL},{1L},{0xD91E8C78L},{(-3L)}},{{(-8L)},{(-3L)},{0xD91E8C78L},{1L},{0x0E3037BFL},{0x78748CA6L}}};
    uint32_t l_655 = 0xC7342BA4L;
    uint16_t l_688 = 2UL;
    int32_t **l_696 = &g_86;
    int32_t **l_697[5] = {&g_86,&g_86,&g_86,&g_86,&g_86};
    uint64_t l_707[8][1][6] = {{{0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL}},{{0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL}},{{0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL}},{{0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL}},{{0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL}},{{0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL}},{{0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0UL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL,0xCAECA83207FBB77BLL}},{{18446744073709551615UL,18446744073709551615UL,0xCAECA83207FBB77BLL,18446744073709551615UL,18446744073709551615UL,0xCAECA83207FBB77BLL}}};
    float * const *l_732 = &g_79[7][3];
    uint32_t l_734[9] = {0x37ED5ABBL,0x37ED5ABBL,0xAD162AB7L,0x37ED5ABBL,0x37ED5ABBL,0xAD162AB7L,0x37ED5ABBL,0x37ED5ABBL,0xAD162AB7L};
    int32_t l_739 = 0x86EEFA19L;
    int16_t l_740 = (-4L);
    uint8_t * const l_774[8] = {&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301};
    uint8_t l_791 = 246UL;
    int64_t l_822 = 0xF40F77FBE1AF2B60LL;
    int64_t l_828 = 0L;
    uint16_t l_835 = 65526UL;
    uint16_t l_902[3][8][8] = {{{0x6447L,0xE26EL,0x3C10L,0x7299L,0xBDF1L,0x7AC4L,65535UL,0x675AL},{0xB670L,65534UL,0UL,0x93D0L,0xAD9EL,0xC964L,0xEF00L,7UL},{0x93D0L,0x2CBBL,0xBDF1L,0xEE07L,65531UL,0x60BFL,0xA625L,0x6656L},{65535UL,0xCFC2L,65535UL,0x2CBBL,0UL,0xDE2FL,0x9B39L,0x23DDL},{0UL,0xDA4CL,4UL,65531UL,0x9807L,0xE26EL,0x5C5AL,2UL},{0x60BFL,0x3C10L,0x55ADL,0xE920L,0x67F5L,1UL,0x67F5L,0xE920L},{0xE222L,0xAFD0L,0xE222L,0x675AL,65531UL,0x67F5L,1UL,0xC9AEL},{65535UL,65535UL,65531UL,0xCB3FL,2UL,65535UL,65531UL,0xC964L}},{{65535UL,65535UL,1UL,65531UL,65531UL,0xBDF1L,7UL,65535UL},{0xE222L,0x55ADL,0xB813L,65528UL,0x67F5L,0x3695L,0x7AC4L,65535UL},{0x60BFL,0x5C5AL,1UL,0xD968L,0x9807L,0xDA4CL,0xE26EL,0xBDF1L},{0UL,0x675AL,0x6482L,0x23DDL,0UL,0x9097L,1UL,0x78C3L},{65535UL,0xE222L,0xD968L,0x9B39L,65531UL,0x3C10L,0x23DDL,1UL},{0x93D0L,1UL,0x9289L,0xAFD0L,0xAD9EL,0xB670L,0x847DL,0xE26EL},{0xB670L,65531UL,0xC9B8L,65535UL,0xBDF1L,0xAB80L,0x9807L,1UL},{0x6447L,65532UL,0x675AL,0xE6F2L,65534UL,7UL,0xB813L,65535UL}},{{0x67F5L,0x5EE0L,0x9807L,65535UL,65531UL,0x7299L,0x7299L,65531UL},{1UL,0UL,0UL,1UL,1UL,65535UL,65528UL,0xEF00L},{0x7299L,65535UL,0xAFD0L,0xDEFCL,4UL,0xC9AEL,65535UL,0x3B68L},{65534UL,65535UL,0xAA37L,65535UL,0xE9C1L,65535UL,0xEE07L,0x32DAL},{0x1C2CL,0UL,0x7AC4L,0xA625L,0x6482L,0x7299L,0xCFC2L,0xC9B8L},{0x9807L,0xC964L,0x0E6FL,0x51D6L,65535UL,0xE6F2L,0x9289L,0x80F3L},{0x675AL,65531UL,0x8877L,0xB670L,0xE6F2L,0xC9AEL,65535UL,0x6482L},{0xC9B8L,0x3C10L,0xE6F2L,0x60BFL,0xEE07L,7UL,0x55ADL,0x8877L}}};
    uint32_t l_918 = 0x02076C51L;
    uint64_t l_936 = 0x5EB0D82BB530F166LL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_460[i] = 1L;
    for (g_204 = 0; (g_204 != 10); g_204 = safe_add_func_uint64_t_u_u(g_204, 5))
    { /* block id: 170 */
        int32_t *l_402[10][7][1] = {{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}},{{&g_230},{&g_230},{&g_5},{&g_20[2][0][2]},{(void*)0},{&g_20[2][0][2]},{&g_5}}};
        uint32_t l_405 = 0xAAA16B57L;
        int32_t l_506 = 2L;
        uint16_t **l_512 = &g_289;
        int32_t **l_526[2][10][7] = {{{&l_402[3][5][0],&l_402[3][5][0],&l_402[5][6][0],(void*)0,(void*)0,&l_402[5][6][0],(void*)0},{(void*)0,&l_402[9][6][0],&l_402[9][6][0],(void*)0,&l_402[6][6][0],&l_402[9][6][0],(void*)0},{&l_402[9][6][0],(void*)0,&l_402[6][6][0],&l_402[6][6][0],(void*)0,&l_402[9][6][0],&l_402[9][6][0]},{(void*)0,(void*)0,&l_402[5][6][0],&l_402[3][5][0],&l_402[3][5][0],&l_402[5][6][0],(void*)0},{(void*)0,&l_402[9][6][0],&l_402[9][6][0],(void*)0,&l_402[6][6][0],&l_402[6][6][0],(void*)0},{&l_402[9][6][0],(void*)0,&l_402[9][6][0],&l_402[6][6][0],(void*)0,&l_402[9][6][0],&l_402[9][6][0]},{(void*)0,(void*)0,&l_402[5][6][0],(void*)0,(void*)0,&l_402[5][6][0],&l_402[3][5][0]},{&l_402[3][5][0],&l_402[9][6][0],&l_402[6][6][0],&l_402[3][5][0],&l_402[6][6][0],&l_402[9][6][0],&l_402[3][5][0]},{&l_402[9][6][0],&l_402[3][5][0],&l_402[9][6][0],&l_402[6][6][0],&l_402[3][5][0],&l_402[6][6][0],&l_402[9][6][0]},{&l_402[3][5][0],&l_402[3][5][0],&l_402[5][6][0],(void*)0,(void*)0,&l_402[5][6][0],(void*)0}},{{(void*)0,&l_402[9][6][0],&l_402[9][6][0],(void*)0,&l_402[6][6][0],&l_402[9][6][0],(void*)0},{&l_402[9][6][0],(void*)0,&l_402[6][6][0],&l_402[6][6][0],(void*)0,&l_402[9][6][0],&l_402[9][6][0]},{(void*)0,(void*)0,&l_402[5][6][0],&l_402[3][5][0],&l_402[3][5][0],&l_402[5][6][0],(void*)0},{(void*)0,&l_402[9][6][0],&l_402[9][6][0],(void*)0,&l_402[6][6][0],&l_402[4][1][0],&l_402[6][6][0]},{(void*)0,&l_402[9][6][0],(void*)0,&l_402[4][1][0],&l_402[9][6][0],&l_402[5][6][0],&l_402[5][6][0]},{&l_402[9][6][0],&l_402[6][6][0],&l_402[3][5][0],&l_402[6][6][0],&l_402[9][6][0],&l_402[3][5][0],&l_402[9][6][0]},{&l_402[9][6][0],&l_402[5][6][0],&l_402[4][1][0],&l_402[9][6][0],&l_402[4][1][0],&l_402[5][6][0],&l_402[9][6][0]},{(void*)0,&l_402[9][6][0],&l_402[5][6][0],&l_402[4][1][0],&l_402[9][6][0],&l_402[4][1][0],&l_402[5][6][0]},{&l_402[9][6][0],&l_402[9][6][0],&l_402[3][5][0],&l_402[9][6][0],&l_402[6][6][0],&l_402[3][5][0],&l_402[6][6][0]},{&l_402[9][6][0],&l_402[5][6][0],&l_402[5][6][0],&l_402[9][6][0],&l_402[4][1][0],(void*)0,&l_402[9][6][0]}}};
        int8_t *l_529 = &g_141;
        float *l_533 = &g_48[0][6];
        uint64_t l_570 = 0x488B10890B29E48ALL;
        int8_t l_721[8] = {0x3CL,0x3CL,0x3CL,0x3CL,0x3CL,0x3CL,0x3CL,0x3CL};
        float ****l_723 = &g_646;
        float l_733 = 0x3.2774F1p+68;
        int32_t *l_751 = &g_87;
        float *** const *l_763[4];
        float *** const **l_762 = &l_763[2];
        uint8_t *l_773 = &g_301;
        const int16_t l_779 = (-1L);
        float l_808 = 0x9.E28013p+53;
        uint16_t l_841 = 0x2BEDL;
        int32_t l_874[6][5] = {{(-5L),5L,0xF7705B19L,0xA541B193L,0xA541B193L},{5L,(-5L),5L,0xF7705B19L,0xA541B193L},{(-2L),0xE1ADEF73L,0xA541B193L,0xE1ADEF73L,(-2L)},{5L,0xE1ADEF73L,(-5L),(-2L),(-5L)},{(-5L),(-5L),0xA541B193L,(-2L),1L},{0xE1ADEF73L,5L,5L,0xE1ADEF73L,(-5L)}};
        uint8_t l_890 = 0x22L;
        uint8_t l_930 = 0xDDL;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_763[i] = (void*)0;
        l_405--;
    }
    l_936++;
    return g_4;
}


/* ------------------------------------------ */
/* 
 * reads : g_90 g_129 g_336.f3 g_370 g_86
 * writes: g_90 g_129 g_87 g_48
 */
static int32_t * func_44(uint32_t  p_45)
{ /* block id: 155 */
    uint64_t l_368[10][3][1] = {{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}},{{18446744073709551609UL},{1UL},{18446744073709551609UL}}};
    int32_t l_392 = (-1L);
    int32_t l_393 = 0x7A03BF86L;
    int32_t l_394 = 0L;
    int32_t l_395 = 2L;
    int32_t l_396[1][7][6] = {{{1L,0x5853FA60L,1L,0x671A5FC1L,0x671A5FC1L,1L},{(-4L),(-4L),0x671A5FC1L,0x670F25A0L,0x671A5FC1L,(-4L)},{0x671A5FC1L,0x5853FA60L,0x670F25A0L,0x670F25A0L,0x5853FA60L,0x671A5FC1L},{(-4L),0x671A5FC1L,0x670F25A0L,0x671A5FC1L,(-4L),(-4L)},{1L,0x671A5FC1L,0x671A5FC1L,1L,0x5853FA60L,1L},{1L,0x5853FA60L,1L,0x671A5FC1L,0x671A5FC1L,1L},{(-4L),(-4L),0x671A5FC1L,0x670F25A0L,0x671A5FC1L,(-4L)}}};
    int i, j, k;
    for (g_90 = 24; (g_90 < 34); g_90++)
    { /* block id: 158 */
        uint16_t *l_365 = &g_129;
        int32_t l_380[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int8_t *l_381 = (void*)0;
        int8_t *l_382 = (void*)0;
        int8_t *l_383[9][2] = {{&g_141,&g_207},{&g_207,&g_207},{&g_141,&g_207},{&g_207,&g_207},{&g_141,&g_207},{&g_207,&g_207},{&g_141,&g_207},{&g_207,&g_207},{&g_141,&g_207}};
        int32_t l_384 = 0x277BD725L;
        int32_t l_385 = 0x4593456AL;
        int32_t *l_386 = &l_384;
        int32_t *l_387 = &l_384;
        int32_t *l_388 = &g_357[0][3];
        int32_t *l_389 = &l_384;
        int32_t *l_390 = &g_87;
        int32_t *l_391[6][4] = {{&g_12,&l_380[5],&g_12,&g_3},{&g_357[0][4],&l_380[5],(void*)0,&l_380[5]},{&g_357[0][4],&g_3,&g_12,&l_380[5]},{(void*)0,&g_3,(void*)0,(void*)0},{&g_12,&g_3,&g_357[0][4],&g_3},{&g_12,(void*)0,(void*)0,&g_3}};
        uint16_t l_397 = 0x6B11L;
        int i, j;
        (*g_86) = (((+(((((!(safe_lshift_func_uint16_t_u_u(((*l_365)--), 5))) > l_368[4][0][0]) || g_336.f3) || ((~(g_370 == &g_371[0][0][3])) <= (safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(0x1FL, p_45)), (l_384 = (safe_mul_func_uint16_t_u_u((l_380[5] = (~((l_368[4][0][0] == 0x351583004FE34421LL) != 0UL))), p_45))))))) & 0x0FL)) , 0xC154L) ^ l_385);
        --l_397;
        return &g_106;
    }
    return &g_20[0][6][4];
}


/* ------------------------------------------ */
/* 
 * reads : g_204 g_80 g_78 g_207 g_324 g_230 g_119 g_331 g_174 g_48 g_106 g_336 g_3 g_5
 * writes: g_204 g_86 g_322 g_324 g_129 g_106
 */
static int32_t  func_60(int32_t * p_61)
{ /* block id: 133 */
    uint16_t l_313 = 0x059FL;
    int32_t l_330 = 0x2FB07238L;
    int32_t l_349 = 0xDE906DD1L;
    for (g_204 = 0; (g_204 != (-22)); g_204 = safe_sub_func_int16_t_s_s(g_204, 2))
    { /* block id: 136 */
        int32_t **l_310 = &g_86;
        float **l_314 = &g_79[8][4];
        int16_t *l_321 = &g_322;
        int16_t *l_323 = &g_324;
        const uint16_t **l_325 = (void*)0;
        const uint16_t *l_327 = &g_328;
        const uint16_t **l_326 = &l_327;
        uint16_t *l_329 = &g_129;
        float l_332 = (-0x1.Ap+1);
        int32_t *l_333 = &g_106;
        (*l_310) = (void*)0;
        (*l_333) ^= ((((safe_lshift_func_int16_t_s_s(l_313, (l_314 == (*g_80)))) ^ (((safe_add_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_u((l_313 || (g_207 , (l_330 |= (safe_rshift_func_uint8_t_u_s((((*l_329) = ((((((*l_326) = (((((*l_323) &= ((*l_321) = ((0x4B13L || ((0x18A6E41CB4023847LL != l_313) == 0x9E9F541C69828AE5LL)) && 0x0D98L))) , g_230) > l_313) , l_323)) == (void*)0) < g_230) , g_119) == 0xD9C43994B56A9D76LL)) >= 2UL), 0))))), g_331)) <= g_174), l_313)) ^ (-8L)) || 0x55643CAA11183E08LL)) || (*p_61)) < (*p_61));
        l_349 ^= (((safe_div_func_int32_t_s_s((((g_336 , (((*l_323) = (safe_rshift_func_int8_t_s_u((((((*l_329) = (0x9CBDF80CL < (g_336.f6 <= l_313))) | ((*l_321) = (safe_mul_func_int16_t_s_s((0xB4A36ACEL ^ 0L), ((((g_3 == (safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u(((*l_333) , (safe_add_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((0x2163L && l_313), g_207)), g_174))), 8)), g_5))) , 0x01076E64L) , 1L) != l_330))))) , (-5L)) == 0x659D2678L), 4))) && 0UL)) && l_330) <= g_331), (*p_61))) , l_313) ^ (*l_333));
        (*l_310) = &g_87;
    }
    return (*p_61);
}


/* ------------------------------------------ */
/* 
 * reads : g_78 g_80 g_18 g_20 g_86 g_87 g_48 g_15 g_21 g_116 g_119 g_131 g_141 g_4 g_129 g_114 g_90 g_174 g_204 g_207 g_106 g_3 g_79 g_230 g_2 g_302 g_305
 * writes: g_78 g_86 g_90 g_106 g_48 g_116 g_119 g_129 g_141 g_114 g_4 g_87 g_174 g_204 g_207 g_230 g_79 g_113 g_289 g_301 g_305
 */
static int32_t * func_62(uint32_t  p_63, float * p_64, const uint64_t  p_65, float * p_66)
{ /* block id: 24 */
    int64_t l_81 = 0L;
    int32_t *l_88 = &g_87;
    uint64_t *l_89 = &g_90;
    uint64_t l_98 = 0UL;
    int32_t l_111 = 0x5DBD19AAL;
    int32_t l_112[8] = {1L,0L,1L,0L,1L,0L,1L,0L};
    int32_t l_115[10][3][4] = {{{0xFA81E3AFL,0xC34014C2L,0xC9851B6DL,(-1L)},{0x83AED21BL,0xC3633543L,(-1L),(-1L)},{0xD5419F28L,0xC34014C2L,(-6L),0x5E22BCF9L}},{{0x9E55FE40L,8L,(-5L),0x47AAFFF4L},{6L,0xA5C4F2F5L,0x4AF182F5L,(-6L)},{6L,6L,0x27BAF6ACL,0x83AED21BL}},{{0xA5C4F2F5L,6L,(-3L),0xBC1C087CL},{0x27BAF6ACL,0xD5419F28L,6L,0xB82127F5L},{0x5B974B4EL,0xC3633543L,0L,1L}},{{(-1L),1L,3L,0x9E55FE40L},{0x9E55FE40L,0x5B974B4EL,1L,0x5B974B4EL},{0x1D411996L,0xC9851B6DL,0xB82127F5L,(-6L)}},{{8L,0xEA078BFDL,(-1L),6L},{0xC9851B6DL,0x47AAFFF4L,(-3L),0xC3633543L},{0xC9851B6DL,(-1L),(-1L),(-1L)}},{{8L,0xC3633543L,0xB82127F5L,0L},{0x1D411996L,(-3L),1L,6L},{0x9E55FE40L,0x83AED21BL,3L,8L}},{{(-1L),0x27BAF6ACL,0L,(-6L)},{0x5B974B4EL,(-1L),6L,0L},{0x27BAF6ACL,8L,(-3L),1L}},{{0xA5C4F2F5L,0x1D411996L,0x27BAF6ACL,0x277B54E5L},{6L,0xC3633543L,0x4AF182F5L,0x4AF182F5L},{6L,6L,(-5L),0L}},{{0x9E55FE40L,0L,(-6L),8L},{0xD5419F28L,0xFA81E3AFL,(-1L),(-6L)},{0x83AED21BL,0xFA81E3AFL,0xC9851B6DL,8L}},{{0xFA81E3AFL,0L,(-3L),0L},{6L,6L,0xA5C4F2F5L,0x4AF182F5L},{0x47AAFFF4L,0xC3633543L,(-1L),0x277B54E5L}}};
    int32_t l_130 = 0xC174B887L;
    float ***l_136 = (void*)0;
    const float **l_143[2];
    const float ***l_142[3][5];
    uint32_t l_242[4][9][3] = {{{4UL,18446744073709551606UL,0UL},{0x1A8FB665L,0x1797122EL,0x9F6FEFC9L},{18446744073709551615UL,18446744073709551606UL,0x7442A286L},{0x1A8FB665L,0x9F6FEFC9L,0x9F6FEFC9L},{4UL,18446744073709551606UL,0UL},{0x1A8FB665L,0x1797122EL,0x9F6FEFC9L},{18446744073709551615UL,18446744073709551606UL,0x7442A286L},{0x1A8FB665L,0x9F6FEFC9L,0x9F6FEFC9L},{4UL,18446744073709551606UL,0UL}},{{0x1A8FB665L,0x1797122EL,0x9F6FEFC9L},{18446744073709551615UL,18446744073709551606UL,0x7442A286L},{0x1A8FB665L,0x9F6FEFC9L,0x9F6FEFC9L},{4UL,18446744073709551606UL,0UL},{0x1A8FB665L,0x1797122EL,0x9F6FEFC9L},{18446744073709551615UL,18446744073709551606UL,0x7442A286L},{0x1A8FB665L,0x9F6FEFC9L,0x9F6FEFC9L},{4UL,18446744073709551606UL,0x62C57F3EL},{0x1797122EL,18446744073709551607UL,0x03B10FF5L}},{{0x7442A286L,4UL,18446744073709551611UL},{0x1797122EL,0x03B10FF5L,0x03B10FF5L},{0UL,4UL,0x62C57F3EL},{0x1797122EL,18446744073709551607UL,0x03B10FF5L},{0x7442A286L,4UL,18446744073709551611UL},{0x1797122EL,0x03B10FF5L,0x03B10FF5L},{0UL,4UL,0x62C57F3EL},{0x1797122EL,18446744073709551607UL,0x03B10FF5L},{0x7442A286L,4UL,18446744073709551611UL}},{{0x1797122EL,0x03B10FF5L,0x03B10FF5L},{0UL,4UL,0x62C57F3EL},{0x1797122EL,18446744073709551607UL,0x03B10FF5L},{0x7442A286L,4UL,18446744073709551611UL},{0x1797122EL,0x03B10FF5L,0x03B10FF5L},{0UL,4UL,0x62C57F3EL},{0x1797122EL,18446744073709551607UL,0x03B10FF5L},{0x7442A286L,4UL,18446744073709551611UL},{0x1797122EL,0x03B10FF5L,0x03B10FF5L}}};
    const float ** const **l_247 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_143[i] = (void*)0;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
            l_142[i][j] = &l_143[0];
    }
    (*g_80) = g_78;
    if ((g_18 ^ (l_81 && (safe_rshift_func_int16_t_s_s(6L, (safe_rshift_func_int16_t_s_s(((p_63 > ((*l_89) = ((l_88 = (g_86 = p_64)) != p_66))) >= ((p_63 <= ((((void*)0 == &g_12) <= p_65) != (-2L))) && g_20[1][6][4])), 14)))))))
    { /* block id: 29 */
        float *l_99 = (void*)0;
        uint8_t l_100 = 0xCFL;
        float ***l_102 = &g_78;
        float ****l_101 = &l_102;
        int32_t l_103[2];
        int32_t *l_104 = (void*)0;
        int32_t *l_105 = &g_106;
        int i;
        for (i = 0; i < 2; i++)
            l_103[i] = 0xA23C0F0EL;
        (*p_66) = ((safe_sub_func_int32_t_s_s((*g_86), ((*l_105) = (safe_mul_func_int8_t_s_s(((l_103[1] = ((safe_div_func_int16_t_s_s(((((+((l_98 <= ((l_100 = (*p_66)) != (&g_78 != ((*l_101) = (void*)0)))) , (((void*)0 == &g_90) ^ (p_65 | g_15)))) && (-7L)) > g_21[4]) || l_100), p_63)) != (*l_88))) <= p_63), g_87))))) , (*p_64));
        for (l_81 = 0; (l_81 == 11); ++l_81)
        { /* block id: 37 */
            int32_t *l_109 = &g_106;
            int32_t *l_110[3];
            int i;
            for (i = 0; i < 3; i++)
                l_110[i] = &g_87;
            --g_116;
            ++g_119;
        }
    }
    else
    { /* block id: 41 */
        int32_t **l_122 = (void*)0;
        int32_t *l_123[9][4] = {{(void*)0,&g_20[2][0][2],&l_112[1],(void*)0},{&g_20[1][2][3],&g_12,&g_20[1][2][3],&l_112[1]},{&g_20[2][0][2],&g_12,&l_112[6],(void*)0},{&g_12,&g_20[2][0][2],&g_20[2][0][2],&g_12},{&g_20[1][2][3],(void*)0,&g_20[2][0][2],&l_112[1]},{&g_12,&g_20[2][0][2],&l_112[6],&g_20[2][0][2]},{&g_20[2][0][2],&g_20[2][0][2],&g_20[1][2][3],&g_20[2][0][2]},{&g_20[1][2][3],&g_20[2][0][2],&l_112[1],&l_112[1]},{(void*)0,(void*)0,&l_112[6],&g_12}};
        uint16_t *l_128[9][1] = {{(void*)0},{&g_129},{(void*)0},{(void*)0},{&g_129},{(void*)0},{(void*)0},{&g_129},{(void*)0}};
        float *l_234 = (void*)0;
        const float ***l_235 = (void*)0;
        int16_t l_280[1];
        int i, j;
        for (i = 0; i < 1; i++)
            l_280[i] = 0x4E12L;
        l_123[6][2] = p_64;
        if ((safe_mul_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(0x8CE80C63ED81A4CDLL, ((g_129 = (*l_88)) || l_130))), ((g_131 , 0UL) && g_15))))
        { /* block id: 44 */
            int64_t l_139 = 0xFBDBECFBBAAEE789LL;
            int32_t *l_161 = &g_5;
            int32_t l_167 = 0x98820683L;
            int32_t l_168[2][8][1] = {{{0xFCDAD931L},{0xAA69C249L},{1L},{1L},{0xAA69C249L},{0xFCDAD931L},{0xAA69C249L},{1L}},{{1L},{0xAA69C249L},{0xFCDAD931L},{0xAA69C249L},{1L},{1L},{0xAA69C249L},{0xFCDAD931L}}};
            int i, j, k;
            for (l_81 = 0; (l_81 != 24); l_81 = safe_add_func_uint32_t_u_u(l_81, 1))
            { /* block id: 47 */
                float ****l_137 = &l_136;
                int32_t l_138 = 0xC62979EAL;
                int8_t *l_140 = &g_141;
                const float ****l_144 = &l_142[2][2];
                uint16_t *l_162 = &g_129;
                if ((safe_sub_func_int8_t_s_s(((((*l_137) = l_136) == (l_138 , ((0xA4EB9ED4L != (((*l_140) ^= (g_131.f3 == l_139)) <= 246UL)) , ((*l_144) = l_142[0][4])))) && (safe_mul_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s((g_4 >= ((safe_mod_func_uint32_t_u_u((g_116 = ((safe_add_func_int64_t_s_s(((safe_unary_minus_func_uint32_t_u((safe_mod_func_uint64_t_u_u(18446744073709551615UL, p_63)))) | l_139), 0L)) & p_63)), (*l_88))) & 0x8102L)), g_129)) == g_20[3][6][1]), 65535UL))), p_63)))
                { /* block id: 52 */
                    for (g_114 = (-28); (g_114 < 11); ++g_114)
                    { /* block id: 55 */
                        (*p_66) = 0x2.A718D1p-63;
                    }
                }
                else
                { /* block id: 58 */
                    uint16_t *l_160 = &g_129;
                    int32_t l_172 = 1L;
                    int32_t l_173[2][7][9] = {{{0x9A6307D7L,0x952B729EL,5L,0x221738ADL,0L,0L,0x221738ADL,5L,0x952B729EL},{0xE08B5220L,0xB3A7F255L,(-1L),0xD2700C1AL,0xD06BC657L,0x8D88053DL,2L,(-7L),0x9BD45106L},{0x10CCC691L,1L,0x102025CEL,0L,0L,1L,0L,0x73B9E9AAL,0x9E39CDA5L},{0xF2F5FB29L,0xB3A7F255L,1L,(-3L),0x52D96C67L,(-1L),0x9BD45106L,0x9BD45106L,(-1L)},{(-7L),0x952B729EL,0x20AD96D9L,0x952B729EL,(-7L),0x9A6307D7L,7L,1L,0L},{0xD2700C1AL,0x3784CA32L,(-6L),0x9BD45106L,(-3L),0x5A8641CAL,0xF2F5FB29L,0xE08B5220L,8L},{0x19A83302L,0x221738ADL,0x6B115645L,0x9E39CDA5L,0xC90B6C89L,0x9A6307D7L,0x73B9E9AAL,1L,0xB99ADBD7L}},{{0x15293983L,0x1818C57DL,0x6DA4907FL,(-1L),0xEE803E18L,(-1L),0x6DA4907FL,0x1818C57DL,0x15293983L},{0x221738ADL,1L,0xB99ADBD7L,0x73B9E9AAL,(-4L),(-1L),0x038DD451L,7L,0x952B729EL},{0xE3571BFDL,0L,0xEE803E18L,(-7L),(-1L),(-6L),0xF2F5FB29L,2L,1L},{0x221738ADL,(-4L),0x038DD451L,5L,(-1L),0x4ADD0DCDL,0xC90B6C89L,0x4ADD0DCDL,(-1L)},{1L,0x3784CA32L,0x3784CA32L,1L,0L,0xFF6E787AL,(-1L),(-3L),0x09930230L},{0L,0L,(-1L),0x952B729EL,(-8L),(-1L),0x10CCC691L,0x73B9E9AAL,1L},{0x6DA4907FL,(-6L),0xE3571BFDL,1L,0L,(-3L),0xD2700C1AL,0xA4678B7DL,2L}}};
                    int i, j, k;
                    g_4 &= (((p_65 , p_66) == ((safe_lshift_func_uint16_t_u_u(((0x1308BAC569388203LL == (((void*)0 == l_160) , p_63)) ^ (*l_88)), 4)) , l_161)) != (l_162 == (void*)0));
                    for (g_129 = 0; (g_129 < 7); g_129++)
                    { /* block id: 62 */
                        if (p_65)
                            break;
                        (*g_86) = (*l_88);
                        if (l_138)
                            continue;
                    }
                    for (g_90 = (-11); (g_90 != 41); g_90++)
                    { /* block id: 69 */
                        uint32_t l_169 = 18446744073709551614UL;
                        --l_169;
                    }
                    ++g_174;
                }
            }
        }
        else
        { /* block id: 75 */
            const int32_t *l_193[9][3];
            float ****l_194 = &l_136;
            int8_t *l_195 = &g_141;
            int16_t *l_202 = (void*)0;
            int16_t *l_203 = &g_204;
            int8_t *l_205 = (void*)0;
            int8_t *l_206 = &g_207;
            float l_208 = (-0x5.1p+1);
            uint8_t *l_209 = &g_174;
            int32_t l_236 = (-2L);
            int32_t l_249 = 0L;
            int64_t l_279 = 0L;
            uint16_t **l_290 = &l_128[6][0];
            uint8_t *l_298 = (void*)0;
            uint8_t *l_299 = (void*)0;
            uint8_t *l_300[3][10] = {{&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301},{&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301},{&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301,&g_301}};
            float *l_304 = &g_113;
            int i, j;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 3; j++)
                    l_193[i][j] = (void*)0;
            }
            for (g_90 = (-3); (g_90 <= 57); g_90 = safe_add_func_uint8_t_u_u(g_90, 8))
            { /* block id: 78 */
                return p_64;
            }
            if ((p_63 ^ (((*l_209) = ((safe_div_func_float_f_f(((*p_66) = 0x1.C92445p-38), ((safe_div_func_float_f_f(((((safe_rshift_func_int16_t_s_u((((*l_206) &= ((((safe_sub_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((safe_div_func_int64_t_s_s((p_65 | (safe_rshift_func_uint8_t_u_u((((l_193[1][1] != (((*l_195) = (((*l_194) = &g_78) != &l_143[0])) , p_66)) | p_63) ^ (safe_add_func_int16_t_s_s((safe_mul_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_u(((*l_203) ^= ((1UL ^ p_63) | 0UL)), p_63)) != g_131.f9), g_116)), g_20[3][0][3]))), 6))), g_21[6])), 0xF14BL)), 1UL)) , 0xD58CC530L) && p_63) < g_20[2][3][4])) , g_106), g_3)) , g_204) , p_65) == l_115[9][0][0]), l_111)) >= l_208))) , g_116)) , g_18)))
            { /* block id: 87 */
                int32_t l_215 = 0x5A8E8F59L;
                int32_t l_228 = 0xB007968BL;
                for (g_207 = 0; (g_207 < 18); g_207 = safe_add_func_uint32_t_u_u(g_207, 5))
                { /* block id: 90 */
                    uint16_t l_227 = 0x7749L;
                    int32_t l_229[1][3];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_229[i][j] = 9L;
                    }
                    g_230 |= (l_229[0][1] |= (g_106 ^= (safe_lshift_func_int16_t_s_s(((+(l_215 , ((void*)0 == (**g_80)))) , (((l_228 = ((g_18 == l_215) > ((p_63 & (!(((safe_div_func_int8_t_s_s((safe_div_func_int32_t_s_s((*l_88), 4294967295UL)), (--g_174))) , (safe_add_func_int16_t_s_s(((safe_mod_func_uint32_t_u_u(l_227, (-4L))) && p_63), g_20[0][9][4]))) ^ p_65))) , l_215))) > p_65) && 1UL)), 8))));
                }
            }
            else
            { /* block id: 97 */
                uint8_t *l_233 = &g_174;
                int32_t l_250 = 0xAACB86EDL;
                for (g_106 = 26; (g_106 > 26); ++g_106)
                { /* block id: 100 */
                    (*g_86) ^= (((g_20[2][0][2] , (p_65 != (((*g_78) = p_66) == (((0x10B4DC2EF21BAC02LL != ((void*)0 == l_233)) , g_114) , (l_234 = p_66))))) , (void*)0) != l_235);
                    if ((*g_86))
                    { /* block id: 104 */
                        uint8_t l_241 = 0xF1L;
                        const float ** const ***l_248 = &l_247;
                        if ((*g_86))
                            break;
                        l_236 ^= ((&p_64 != &p_66) > g_2[0]);
                        (*g_86) = ((safe_add_func_int64_t_s_s((safe_sub_func_int8_t_s_s(g_21[4], (l_241 <= (((l_236 |= l_242[0][4][1]) != (((p_65 == ((-1L) == ((safe_lshift_func_int16_t_s_s(g_116, (safe_rshift_func_int8_t_s_s(((*l_206) = (g_131.f1 || (((l_249 = (((*l_248) = l_247) == (((p_65 , 4294967295UL) , 8L) , &g_80))) > 7L) ^ p_65))), p_63)))) >= 0xF79196BF7D5F75E5LL))) & l_250) | g_20[4][9][3])) , 0x205A4E5D210BA1ACLL)))), g_20[2][0][2])) , 0x7E19047CL);
                    }
                    else
                    { /* block id: 112 */
                        uint16_t l_251[2][7] = {{0xCF1FL,0xCF1FL,0xCF1FL,0xCF1FL,0xCF1FL,0xCF1FL,0xCF1FL},{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL}};
                        float *l_272 = &g_113;
                        uint64_t l_273 = 0x94C8D8B33F109BE1LL;
                        int i, j;
                        (*l_234) = ((&g_78 != (g_131 , ((*l_194) = (l_251[1][3] , &g_78)))) <= ((safe_sub_func_float_f_f(((*p_66) >= ((safe_div_func_float_f_f(((*l_272) = (safe_sub_func_float_f_f((safe_mul_func_float_f_f(l_251[1][0], ((safe_add_func_float_f_f((*p_64), ((safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((safe_add_func_float_f_f(0x6.A9D7FDp-91, ((safe_mul_func_float_f_f((*p_66), (*p_64))) != (-0x2.1p-1)))) <= (*p_64)), (*p_64))), 0x1.6p-1)), l_250)) > (*p_66)))) > (*p_66)))), 0x8.3p+1))), (*p_64))) == 0x1.Bp-1)), l_273)) == (*p_64)));
                        if (p_65)
                            continue;
                    }
                }
            }
            l_112[5] = ((((safe_sub_func_int64_t_s_s(((+(p_65 > 0x01DBL)) >= ((*g_86) = (safe_mul_func_int16_t_s_s(((g_131.f2 , ((&g_86 != (void*)0) , l_279)) ^ (*l_88)), (g_129 && (((*p_66) < (*p_66)) , (*l_88))))))), g_21[4])) , (*g_86)) , 9UL) | 0xAA810893L);
            (*l_304) = ((((*p_64) = l_280[0]) != (((safe_add_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((safe_add_func_uint32_t_u_u(p_65, (safe_div_func_int16_t_s_s((((g_87 , l_203) != ((*l_290) = (g_289 = (void*)0))) , ((~(g_301 = ((((void*)0 != &l_193[1][1]) == ((*l_89) = ((safe_mul_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((7UL & ((*l_209)--)), p_63)) >= p_63), 0UL)) , g_204))) , g_131.f8))) == p_63)), p_65)))), 0L)), 0x521BL)) , g_114) , g_302[1])) == p_63);
        }
        ++g_305;
    }
    return p_64;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    transparent_crc(g_19, "g_19", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_20[i][j][k], "g_20[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_21[i], "g_21[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc_bytes(&g_48[i][j], sizeof(g_48[i][j]), "g_48[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_87, "g_87", print_hash_value);
    transparent_crc(g_90, "g_90", print_hash_value);
    transparent_crc(g_106, "g_106", print_hash_value);
    transparent_crc_bytes (&g_113, sizeof(g_113), "g_113", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    transparent_crc(g_131.f0, "g_131.f0", print_hash_value);
    transparent_crc(g_131.f1, "g_131.f1", print_hash_value);
    transparent_crc(g_131.f2, "g_131.f2", print_hash_value);
    transparent_crc(g_131.f3, "g_131.f3", print_hash_value);
    transparent_crc(g_131.f4, "g_131.f4", print_hash_value);
    transparent_crc(g_131.f5, "g_131.f5", print_hash_value);
    transparent_crc(g_131.f6, "g_131.f6", print_hash_value);
    transparent_crc(g_131.f7, "g_131.f7", print_hash_value);
    transparent_crc(g_131.f8, "g_131.f8", print_hash_value);
    transparent_crc(g_131.f9, "g_131.f9", print_hash_value);
    transparent_crc(g_141, "g_141", print_hash_value);
    transparent_crc(g_174, "g_174", print_hash_value);
    transparent_crc(g_204, "g_204", print_hash_value);
    transparent_crc(g_207, "g_207", print_hash_value);
    transparent_crc(g_230, "g_230", print_hash_value);
    transparent_crc(g_301, "g_301", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_302[i], "g_302[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_305, "g_305", print_hash_value);
    transparent_crc(g_322, "g_322", print_hash_value);
    transparent_crc(g_324, "g_324", print_hash_value);
    transparent_crc(g_328, "g_328", print_hash_value);
    transparent_crc(g_331, "g_331", print_hash_value);
    transparent_crc(g_336.f0, "g_336.f0", print_hash_value);
    transparent_crc(g_336.f1, "g_336.f1", print_hash_value);
    transparent_crc(g_336.f2, "g_336.f2", print_hash_value);
    transparent_crc(g_336.f3, "g_336.f3", print_hash_value);
    transparent_crc(g_336.f4, "g_336.f4", print_hash_value);
    transparent_crc(g_336.f5, "g_336.f5", print_hash_value);
    transparent_crc(g_336.f6, "g_336.f6", print_hash_value);
    transparent_crc(g_336.f7, "g_336.f7", print_hash_value);
    transparent_crc(g_336.f8, "g_336.f8", print_hash_value);
    transparent_crc(g_336.f9, "g_336.f9", print_hash_value);
    transparent_crc(g_355, "g_355", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_357[i][j], "g_357[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_536.f0, "g_536.f0", print_hash_value);
    transparent_crc(g_536.f1, "g_536.f1", print_hash_value);
    transparent_crc(g_536.f2, "g_536.f2", print_hash_value);
    transparent_crc(g_536.f3, "g_536.f3", print_hash_value);
    transparent_crc(g_536.f4, "g_536.f4", print_hash_value);
    transparent_crc(g_536.f5, "g_536.f5", print_hash_value);
    transparent_crc(g_536.f6, "g_536.f6", print_hash_value);
    transparent_crc(g_536.f7, "g_536.f7", print_hash_value);
    transparent_crc(g_536.f8, "g_536.f8", print_hash_value);
    transparent_crc(g_536.f9, "g_536.f9", print_hash_value);
    transparent_crc(g_595.f0, "g_595.f0", print_hash_value);
    transparent_crc(g_595.f1, "g_595.f1", print_hash_value);
    transparent_crc(g_595.f2, "g_595.f2", print_hash_value);
    transparent_crc(g_595.f3, "g_595.f3", print_hash_value);
    transparent_crc(g_595.f4, "g_595.f4", print_hash_value);
    transparent_crc(g_595.f5, "g_595.f5", print_hash_value);
    transparent_crc(g_595.f6, "g_595.f6", print_hash_value);
    transparent_crc(g_595.f7, "g_595.f7", print_hash_value);
    transparent_crc(g_595.f8, "g_595.f8", print_hash_value);
    transparent_crc(g_595.f9, "g_595.f9", print_hash_value);
    transparent_crc(g_639, "g_639", print_hash_value);
    transparent_crc(g_691, "g_691", print_hash_value);
    transparent_crc(g_1006, "g_1006", print_hash_value);
    transparent_crc(g_1017.f0, "g_1017.f0", print_hash_value);
    transparent_crc(g_1017.f1, "g_1017.f1", print_hash_value);
    transparent_crc(g_1017.f2, "g_1017.f2", print_hash_value);
    transparent_crc(g_1017.f3, "g_1017.f3", print_hash_value);
    transparent_crc(g_1017.f4, "g_1017.f4", print_hash_value);
    transparent_crc(g_1017.f5, "g_1017.f5", print_hash_value);
    transparent_crc(g_1017.f6, "g_1017.f6", print_hash_value);
    transparent_crc(g_1017.f7, "g_1017.f7", print_hash_value);
    transparent_crc(g_1017.f8, "g_1017.f8", print_hash_value);
    transparent_crc(g_1017.f9, "g_1017.f9", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_1103[i][j].f0, "g_1103[i][j].f0", print_hash_value);
            transparent_crc(g_1103[i][j].f1, "g_1103[i][j].f1", print_hash_value);
            transparent_crc(g_1103[i][j].f2, "g_1103[i][j].f2", print_hash_value);
            transparent_crc(g_1103[i][j].f3, "g_1103[i][j].f3", print_hash_value);
            transparent_crc(g_1103[i][j].f4, "g_1103[i][j].f4", print_hash_value);
            transparent_crc(g_1103[i][j].f5, "g_1103[i][j].f5", print_hash_value);
            transparent_crc(g_1103[i][j].f6, "g_1103[i][j].f6", print_hash_value);
            transparent_crc(g_1103[i][j].f7, "g_1103[i][j].f7", print_hash_value);
            transparent_crc(g_1103[i][j].f8, "g_1103[i][j].f8", print_hash_value);
            transparent_crc(g_1103[i][j].f9, "g_1103[i][j].f9", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1140, "g_1140", print_hash_value);
    transparent_crc(g_1158, "g_1158", print_hash_value);
    transparent_crc(g_1205, "g_1205", print_hash_value);
    transparent_crc(g_1288, "g_1288", print_hash_value);
    transparent_crc(g_1360.f0, "g_1360.f0", print_hash_value);
    transparent_crc(g_1360.f1, "g_1360.f1", print_hash_value);
    transparent_crc(g_1360.f2, "g_1360.f2", print_hash_value);
    transparent_crc(g_1360.f3, "g_1360.f3", print_hash_value);
    transparent_crc(g_1360.f4, "g_1360.f4", print_hash_value);
    transparent_crc(g_1360.f5, "g_1360.f5", print_hash_value);
    transparent_crc(g_1360.f6, "g_1360.f6", print_hash_value);
    transparent_crc(g_1360.f7, "g_1360.f7", print_hash_value);
    transparent_crc(g_1360.f8, "g_1360.f8", print_hash_value);
    transparent_crc(g_1360.f9, "g_1360.f9", print_hash_value);
    transparent_crc(g_1412, "g_1412", print_hash_value);
    transparent_crc(g_1487, "g_1487", print_hash_value);
    transparent_crc(g_1541.f0, "g_1541.f0", print_hash_value);
    transparent_crc(g_1541.f1, "g_1541.f1", print_hash_value);
    transparent_crc(g_1541.f2, "g_1541.f2", print_hash_value);
    transparent_crc(g_1541.f3, "g_1541.f3", print_hash_value);
    transparent_crc(g_1541.f4, "g_1541.f4", print_hash_value);
    transparent_crc(g_1541.f5, "g_1541.f5", print_hash_value);
    transparent_crc(g_1541.f6, "g_1541.f6", print_hash_value);
    transparent_crc(g_1541.f7, "g_1541.f7", print_hash_value);
    transparent_crc(g_1541.f8, "g_1541.f8", print_hash_value);
    transparent_crc(g_1541.f9, "g_1541.f9", print_hash_value);
    transparent_crc(g_1557, "g_1557", print_hash_value);
    transparent_crc(g_1560, "g_1560", print_hash_value);
    transparent_crc(g_1575.f0, "g_1575.f0", print_hash_value);
    transparent_crc(g_1575.f1, "g_1575.f1", print_hash_value);
    transparent_crc(g_1575.f2, "g_1575.f2", print_hash_value);
    transparent_crc(g_1575.f3, "g_1575.f3", print_hash_value);
    transparent_crc(g_1575.f4, "g_1575.f4", print_hash_value);
    transparent_crc(g_1575.f5, "g_1575.f5", print_hash_value);
    transparent_crc(g_1575.f6, "g_1575.f6", print_hash_value);
    transparent_crc(g_1575.f7, "g_1575.f7", print_hash_value);
    transparent_crc(g_1575.f8, "g_1575.f8", print_hash_value);
    transparent_crc(g_1575.f9, "g_1575.f9", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1664[i][j], "g_1664[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1708[i].f0, "g_1708[i].f0", print_hash_value);
        transparent_crc(g_1708[i].f1, "g_1708[i].f1", print_hash_value);
        transparent_crc(g_1708[i].f2, "g_1708[i].f2", print_hash_value);
        transparent_crc(g_1708[i].f3, "g_1708[i].f3", print_hash_value);
        transparent_crc(g_1708[i].f4, "g_1708[i].f4", print_hash_value);
        transparent_crc(g_1708[i].f5, "g_1708[i].f5", print_hash_value);
        transparent_crc(g_1708[i].f6, "g_1708[i].f6", print_hash_value);
        transparent_crc(g_1708[i].f7, "g_1708[i].f7", print_hash_value);
        transparent_crc(g_1708[i].f8, "g_1708[i].f8", print_hash_value);
        transparent_crc(g_1708[i].f9, "g_1708[i].f9", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 483
   depth: 1, occurrence: 9
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 1
XXX structs with bitfields in the program: 14
breakdown:
   indirect level: 0, occurrence: 9
   indirect level: 1, occurrence: 5
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 4
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 14
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 4

XXX max expression depth: 62
breakdown:
   depth: 1, occurrence: 134
   depth: 2, occurrence: 40
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
   depth: 7, occurrence: 2
   depth: 9, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 1
   depth: 16, occurrence: 4
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 1
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 2
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 37, occurrence: 1
   depth: 46, occurrence: 1
   depth: 62, occurrence: 1

XXX total number of pointers: 320

XXX times a variable address is taken: 778
XXX times a pointer is dereferenced on RHS: 172
breakdown:
   depth: 1, occurrence: 134
   depth: 2, occurrence: 30
   depth: 3, occurrence: 5
   depth: 4, occurrence: 3
XXX times a pointer is dereferenced on LHS: 160
breakdown:
   depth: 1, occurrence: 143
   depth: 2, occurrence: 13
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 40
XXX times a pointer is compared with address of another variable: 7
XXX times a pointer is compared with another pointer: 16
XXX times a pointer is qualified to be dereferenced: 6599

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 905
   level: 2, occurrence: 160
   level: 3, occurrence: 71
   level: 4, occurrence: 42
   level: 5, occurrence: 68
XXX number of pointers point to pointers: 152
XXX number of pointers point to scalars: 163
XXX number of pointers point to structs: 5
XXX percent of pointers has null in alias set: 34.1
XXX average alias set size: 1.45

XXX times a non-volatile is read: 1344
XXX times a non-volatile is write: 627
XXX times a volatile is read: 77
XXX    times read thru a pointer: 0
XXX times a volatile is write: 11
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 4.13e+03
XXX percentage of non-volatile access: 95.7

XXX forward jumps: 0
XXX backward jumps: 7

XXX stmts: 146
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 26
   depth: 1, occurrence: 28
   depth: 2, occurrence: 20
   depth: 3, occurrence: 23
   depth: 4, occurrence: 19
   depth: 5, occurrence: 30

XXX percentage a fresh-made variable is used: 16.8
XXX percentage an existing variable is used: 83.2
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


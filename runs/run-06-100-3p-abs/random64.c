/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3227998639
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_4[9][7][4] = {{{0xEF9D7865L,0x06695998L,0x6EC6E54EL,0L},{0xF29855EEL,0x6EC6E54EL,0xF2C36544L,0x86524F20L},{0xF29855EEL,0xCFAF425FL,0x6EC6E54EL,1L},{0xEF9D7865L,0x86524F20L,0x86524F20L,0xEF9D7865L},{0x368A9017L,0x9949DFCFL,0xEF9D7865L,0L},{0xF2C36544L,1L,1L,(-1L)},{0x1C2A2C7CL,0L,1L,(-1L)}},{{0x9949DFCFL,1L,0xDD8FB8CEL,0L},{0xCFAF425FL,0x9949DFCFL,0xCFAF425FL,0xEF9D7865L},{1L,0x86524F20L,0xF29855EEL,1L},{(-1L),0xCFAF425FL,0L,0x86524F20L},{1L,0x6EC6E54EL,0xF29855EEL,1L},{0L,0L,0xEF9D7865L,0xEF9D7865L},{0x1C2A2C7CL,0x1C2A2C7CL,0x368A9017L,0x86524F20L}},{{0x368A9017L,0x86524F20L,0xF2C36544L,0x6EC6E54EL},{0x6EC6E54EL,1L,0x1C2A2C7CL,0xF2C36544L},{(-4L),1L,0x9949DFCFL,0x6EC6E54EL},{1L,0x86524F20L,0xCFAF425FL,0x86524F20L},{1L,0x1C2A2C7CL,1L,0xEF9D7865L},{0xCFAF425FL,0L,(-1L),1L},{0xEF9D7865L,(-1L),1L,1L}},{{0xEF9D7865L,0x368A9017L,(-1L),0xDD8FB8CEL},{0xCFAF425FL,1L,1L,0xCFAF425FL},{1L,0x6EC6E54EL,0xCFAF425FL,0xF29855EEL},{1L,0xDD8FB8CEL,0x9949DFCFL,0L},{(-4L),1L,0x1C2A2C7CL,0L},{0x6EC6E54EL,0xDD8FB8CEL,0xF2C36544L,0xF29855EEL},{0x368A9017L,0x6EC6E54EL,0x368A9017L,0xCFAF425FL}},{{0x1C2A2C7CL,1L,0xEF9D7865L,0xDD8FB8CEL},{0L,0x368A9017L,0xF29855EEL,1L},{0x86524F20L,(-1L),0xF29855EEL,1L},{0L,0L,0xEF9D7865L,0xEF9D7865L},{0x1C2A2C7CL,0x1C2A2C7CL,0x368A9017L,0x86524F20L},{0x368A9017L,0x86524F20L,0xF2C36544L,0x6EC6E54EL},{0x6EC6E54EL,1L,0x1C2A2C7CL,0xF2C36544L}},{{(-4L),1L,0x9949DFCFL,0x6EC6E54EL},{1L,0x86524F20L,0xCFAF425FL,0x86524F20L},{1L,0x1C2A2C7CL,1L,0xEF9D7865L},{0xCFAF425FL,0L,(-1L),1L},{0xEF9D7865L,(-1L),1L,1L},{0xEF9D7865L,0x368A9017L,(-1L),0xDD8FB8CEL},{0xCFAF425FL,1L,1L,0xCFAF425FL}},{{1L,0x6EC6E54EL,0xCFAF425FL,0xF29855EEL},{1L,0xDD8FB8CEL,0x9949DFCFL,0L},{(-4L),1L,0x1C2A2C7CL,0L},{0x6EC6E54EL,0xDD8FB8CEL,0xF2C36544L,0xF29855EEL},{0x368A9017L,0x6EC6E54EL,0x368A9017L,0xCFAF425FL},{0x1C2A2C7CL,1L,0xEF9D7865L,0xDD8FB8CEL},{0L,0x368A9017L,0xF29855EEL,1L}},{{0x86524F20L,(-1L),0xF29855EEL,1L},{0L,0L,0xEF9D7865L,0xEF9D7865L},{0x1C2A2C7CL,0x1C2A2C7CL,0x368A9017L,0x86524F20L},{0x368A9017L,0x86524F20L,0xF2C36544L,0x6EC6E54EL},{0x6EC6E54EL,1L,0x1C2A2C7CL,0xF2C36544L},{(-4L),1L,0x9949DFCFL,0x6EC6E54EL},{1L,0x86524F20L,0xCFAF425FL,0x86524F20L}},{{1L,0x1C2A2C7CL,1L,0xEF9D7865L},{0xCFAF425FL,0L,(-1L),1L},{0xEF9D7865L,(-1L),1L,1L},{0xEF9D7865L,1L,0L,0xF2C36544L},{0x368A9017L,0x1C2A2C7CL,0x1C2A2C7CL,0x368A9017L},{0xDD8FB8CEL,(-1L),0x368A9017L,0xEF9D7865L},{0x86524F20L,0xF2C36544L,0x6EC6E54EL,0xF29855EEL}}};
static int32_t * volatile g_3 = &g_4[2][4][2];/* VOLATILE GLOBAL g_3 */
static int32_t g_7 = 0L;


/* --- FORWARD DECLARATIONS --- */
static const int16_t  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_4 g_7
 * writes: g_4 g_7
 */
static const int16_t  func_1(void)
{ /* block id: 0 */
    int32_t l_2 = 2L;
    int32_t *l_6 = &g_7;
    (*g_3) &= l_2;
    (*l_6) ^= (safe_unary_minus_func_int32_t_s((*g_3)));
    return (*l_6);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_4[i][j][k], "g_4[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_7, "g_7", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 1
breakdown:
   depth: 1, occurrence: 5

XXX total number of pointers: 2

XXX times a variable address is taken: 2
XXX times a pointer is dereferenced on RHS: 2
breakdown:
   depth: 1, occurrence: 2
XXX times a pointer is dereferenced on LHS: 2
breakdown:
   depth: 1, occurrence: 2
XXX times a pointer is compared with null: 0
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 3

XXX max dereference level: 1
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 7
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 2
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 0
XXX average alias set size: 1

XXX times a non-volatile is read: 4
XXX times a non-volatile is write: 3
XXX times a volatile is read: 1
XXX    times read thru a pointer: 0
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 1
XXX percentage of non-volatile access: 77.8

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 3
XXX max block depth: 0
breakdown:
   depth: 0, occurrence: 3

XXX percentage a fresh-made variable is used: 33.3
XXX percentage an existing variable is used: 66.7
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


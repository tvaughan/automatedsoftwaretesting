/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      175322757
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int8_t g_6 = 7L;/* VOLATILE GLOBAL g_6 */
static int32_t g_12 = 0xAD2086BDL;
static uint32_t g_14 = 0x62DAAB72L;
static int64_t g_39 = 0L;
static int64_t *g_38 = &g_39;
static int32_t g_42 = 0x1434A938L;
static int64_t g_44 = (-1L);
static int16_t g_45 = 0x7527L;
static uint16_t g_47 = 0UL;
static uint32_t g_77[2][6] = {{0UL,0UL,0UL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL,0UL,0UL}};
static int8_t g_97[8][1][4] = {{{0xD1L,(-1L),(-1L),0xD1L}},{{0xA4L,(-1L),0x39L,0xD1L}},{{0xD1L,(-1L),(-1L),0xD1L}},{{0xA4L,(-1L),0x39L,0xD1L}},{{0xD1L,(-1L),(-1L),0xD1L}},{{0xA4L,(-1L),0x39L,0xD1L}},{{0xD1L,(-1L),(-1L),0xD1L}},{{0xA4L,(-1L),0x39L,0xD1L}}};
static uint32_t g_101[2] = {0xE555501DL,0xE555501DL};
static uint64_t g_112[7] = {0x762B02D3770B94A9LL,0x762B02D3770B94A9LL,0x762B02D3770B94A9LL,0x762B02D3770B94A9LL,0x762B02D3770B94A9LL,0x762B02D3770B94A9LL,0x762B02D3770B94A9LL};
static volatile uint64_t g_120 = 0xF66B510C19ED05CELL;/* VOLATILE GLOBAL g_120 */
static volatile uint64_t *g_119 = &g_120;
static volatile uint64_t * volatile *g_118 = &g_119;
static uint8_t g_135[9][2] = {{0UL,3UL},{0UL,0UL},{3UL,0UL},{0UL,3UL},{0UL,0UL},{3UL,0UL},{0UL,3UL},{0UL,0UL},{3UL,0UL}};
static int32_t * volatile g_145 = (void*)0;/* VOLATILE GLOBAL g_145 */
static int32_t * volatile *g_144 = &g_145;
static int8_t g_146 = 0x93L;
static int32_t g_161 = 1L;
static int32_t g_166 = (-1L);
static int32_t *g_165 = &g_166;
static uint16_t g_168 = 0xB6CAL;
static uint16_t g_174 = 2UL;
static float g_175[5] = {0x5.Bp+1,0x5.Bp+1,0x5.Bp+1,0x5.Bp+1,0x5.Bp+1};
static int16_t g_177[1] = {0L};
static int16_t g_179 = 0x1199L;
static const int16_t g_190 = 1L;
static int64_t g_233 = (-1L);
static uint8_t g_237 = 0x85L;
static int32_t g_241 = (-1L);
static int8_t g_248 = 0x03L;
static int32_t g_249 = 1L;
static uint32_t g_250 = 0x95FF6E78L;
static uint8_t g_287 = 0UL;
static uint8_t g_310 = 3UL;
static uint64_t g_328 = 0UL;
static int16_t *g_351 = &g_177[0];
static int16_t **g_350[5][5][2] = {{{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351}},{{&g_351,&g_351},{&g_351,&g_351},{&g_351,(void*)0},{&g_351,&g_351},{&g_351,&g_351}},{{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351}},{{&g_351,(void*)0},{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351}},{{&g_351,&g_351},{&g_351,&g_351},{&g_351,&g_351},{&g_351,(void*)0},{&g_351,(void*)0}}};
static int16_t **g_352 = &g_351;
static int32_t g_368 = (-1L);
static int32_t g_369 = 0xC322C723L;
static uint32_t g_372 = 4294967295UL;
static uint8_t g_414[1] = {0x99L};
static float g_417[3] = {(-0x1.Bp-1),(-0x1.Bp-1),(-0x1.Bp-1)};
static uint32_t *g_433 = (void*)0;
static int16_t g_440 = 0xC27FL;
static uint64_t g_450 = 18446744073709551613UL;
static uint32_t g_459 = 0x3A7DBBFAL;
static float g_461[4][9][5] = {{{0x0.1p-1,0x2.Ep-1,0x7.4p-1,0x4.1p-1,(-0x5.6p-1)},{0x0.1E2FD9p+86,0x6.D67468p+22,0x6.7E2228p-11,0x8.1D43D3p+28,0x0.73DC19p+13},{0x1.EC5A4Dp-10,0xB.8F56AAp+32,0x1.7p+1,0xA.0AB8EFp+69,0x1.Bp+1},{(-0x1.1p+1),0x0.73DC19p+13,0x0.8p-1,(-0x5.7p+1),0x0.6p-1},{0x2.2199A4p-35,(-0x1.9p+1),(-0x7.2p-1),0x7.3BCDCAp+79,0x3.456A58p-93},{0xC.AD7FDCp+91,0x5.158300p+87,0x8.1D43D3p+28,(-0x1.1p+1),(-0x6.7p-1)},{(-0x1.Fp+1),0x1.4p+1,0xD.06CBE3p-13,0x1.4p+1,(-0x1.Fp+1)},{(-0x10.4p+1),0x0.8p-1,0xE.C5F407p+39,0xD.FE44C7p+87,(-0x4.7p+1)},{0xD.06CBE3p-13,0x4.1p-1,0x9.215799p-49,0x1.F34A63p+57,(-0x4.Ep-1)}},{{0x5.158300p+87,0x0.6p-1,0x7.8p+1,0x0.8p-1,(-0x4.7p+1)},{0x1.7p+1,0x1.F34A63p+57,(-0x9.1p+1),0x2.Ep-1,(-0x1.Fp+1)},{(-0x4.7p+1),0x5.A46EA1p+52,0x8.9A9044p+84,0xE.C5F407p+39,(-0x6.7p-1)},{0xA.0AB8EFp+69,0x6.3p-1,(-0x1.Fp+1),(-0x2.Ep+1),0x3.456A58p-93},{0xE.8ED07Ap-3,0x8.1D43D3p+28,(-0x5.7p+1),(-0x10.9p-1),0x0.6p-1},{0x6.DB7AEBp+86,(-0x5.6p-1),0xA.7408F1p+83,0x1.Bp+1,0x1.Bp+1},{0x0.Fp-1,0xC.55CD05p-13,0x0.Fp-1,0x1.14C7D5p+80,0x0.73DC19p+13},{0x7.C93A24p+7,0x2.871C56p-40,0xF.44F254p+3,0xC.717064p+27,(-0x5.6p-1)},{0xE.73E674p-7,0x1.4D71A7p+51,0x5.158300p+87,0x9.112BF3p+30,(-0x5.7p+1)}},{{0x6.3p-1,0x7.3BCDCAp+79,0xF.44F254p+3,(-0x5.6p-1),0xD.06CBE3p-13},{0x1.14C7D5p+80,0x0.1E2FD9p+86,0x0.Fp-1,0x5.EAEFBCp-32,0x0.6p-1},{0x5.2p+1,0x7.4p-1,0xA.7408F1p+83,(-0x1.Fp+1),0xA.0AB8EFp+69},{0x4.44388Cp-58,0x1.4D71A7p+51,0x8.9A9044p+84,0x1.14C7D5p+80,0x9.35DADFp+45},{0x5.2p+1,(-0x9.1p+1),0xC.04676Bp-12,0xA.0AB8EFp+69,0xB.8F56AAp+32},{0x6.7E2228p-11,0x8.1p+1,0x0.6p-1,0x0.6p-1,0x8.1p+1},{0xA.0AB8EFp+69,(-0x2.Ep+1),0x7.C93A24p+7,0xD.06CBE3p-13,0x4.1p-1},{0xA.EAAAD8p+80,0x1.14C7D5p+80,0x6.7E2228p-11,(-0x5.7p+1),(-0x10.4p+1)},{0x1.F34A63p+57,0x7.C93A24p+7,(-0x1.Dp-1),(-0x5.6p-1),(-0x4.Ep-1)}},{{0xA.EAAAD8p+80,0x8.9A9044p+84,0x5.343FA8p-63,0x0.73DC19p+13,0x8.1D43D3p+28},{0xA.0AB8EFp+69,0x5.2p+1,(-0x2.Ep+1),0x1.Bp+1,0x1.4p+1},{0x6.7E2228p-11,0x6.D67468p+22,0x0.1E2FD9p+86,0x0.6p-1,0xE.8ED07Ap-3},{0x5.2p+1,0xD.06CBE3p-13,(-0x10.Bp-1),0x3.456A58p-93,0xA.7408F1p+83},{0xE.C5F407p+39,0x5.A46EA1p+52,0x0.Cp-1,(-0x6.7p-1),0x5.EAEFBCp-32},{(-0x1.Fp+1),(-0x1.5p-1),(-0x1.5p-1),(-0x1.Fp+1),(-0x1.Dp-1)},{0x9.35DADFp+45,0xE.8ED07Ap-3,0x1.14C7D5p+80,(-0x4.7p+1),0x7.8p+1},{0x1.7p+1,0x6.3p-1,0x2.8E3145p+46,(-0x4.Ep-1),0x1.EC5A4Dp-10},{0x0.73DC19p+13,(-0x5.7p+1),0x5.158300p+87,(-0x4.7p+1),0x3.992D61p-67}}};
static int32_t g_464 = 1L;
static uint64_t ** volatile g_536[2][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static uint64_t ** volatile *g_535 = &g_536[1][4];
static uint64_t ** volatile **g_534 = &g_535;
static int16_t g_661[2][2] = {{(-2L),(-2L)},{(-2L),(-2L)}};
static uint32_t g_669 = 0x6A5C7B7FL;
static uint16_t g_751 = 0x7A90L;
static int32_t *g_766 = &g_161;
static int32_t **g_765 = &g_766;
static int32_t g_809 = (-1L);
static int32_t g_810[6][5] = {{1L,(-1L),(-1L),(-1L),(-1L)},{1L,(-1L),(-1L),(-1L),(-1L)},{1L,(-1L),(-1L),(-1L),(-1L)},{1L,(-1L),(-1L),(-1L),(-1L)},{1L,(-1L),(-1L),(-1L),(-1L)},{1L,(-1L),(-1L),(-1L),(-1L)}};
static int8_t g_811 = 6L;
static uint8_t g_812 = 0xB9L;
static const float *g_846 = &g_417[2];
static const float **g_845[3] = {&g_846,&g_846,&g_846};
static uint32_t ** volatile g_906 = (void*)0;/* VOLATILE GLOBAL g_906 */
static uint32_t ** volatile *g_905[2] = {&g_906,&g_906};
static volatile uint8_t g_998 = 0x5AL;/* VOLATILE GLOBAL g_998 */
static volatile uint8_t *g_997 = &g_998;
static volatile uint8_t **g_996 = &g_997;
static uint32_t *g_1034 = &g_372;
static int32_t g_1073[3][2] = {{1L,1L},{1L,1L},{1L,1L}};
static int32_t g_1081 = 3L;
static float g_1114 = 0xA.679E3Fp-26;
static volatile uint16_t g_1271 = 1UL;/* VOLATILE GLOBAL g_1271 */
static volatile uint16_t *g_1270 = &g_1271;
static volatile uint16_t **g_1269 = &g_1270;
static uint32_t g_1287[3] = {4294967295UL,4294967295UL,4294967295UL};
static const uint32_t g_1343[9][6] = {{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL},{8UL,0x4FE67BBCL,8UL,0x4FE67BBCL,8UL,0x4FE67BBCL}};
static int16_t * const *g_1348 = &g_351;
static int16_t * const * const *g_1347[2][8] = {{&g_1348,&g_1348,&g_1348,&g_1348,&g_1348,&g_1348,&g_1348,&g_1348},{&g_1348,&g_1348,&g_1348,&g_1348,&g_1348,&g_1348,&g_1348,&g_1348}};
static int16_t * const * const **g_1346[4] = {&g_1347[0][6],&g_1347[0][6],&g_1347[0][6],&g_1347[0][6]};
static uint32_t **g_1382 = &g_433;
static uint32_t ***g_1381 = &g_1382;
static int16_t ** const *g_1503 = &g_350[3][4][1];
static int16_t ** const **g_1502 = &g_1503;
static int8_t *g_1649 = &g_811;
static uint16_t g_1831 = 0xDB1CL;
static int32_t *g_1891[5][7] = {{&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081},{&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081},{&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081},{&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081},{&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081,&g_1081}};
static float g_1912 = 0x8.Ep-1;
static int16_t g_2016 = 0x1566L;
static int32_t g_2041 = 0x06A06E3FL;
static float *g_2072[2] = {&g_461[2][1][2],&g_461[2][1][2]};
static const volatile float g_2105 = (-0x1.3p+1);/* VOLATILE GLOBAL g_2105 */
static uint64_t *g_2112[9][8][3] = {{{&g_450,(void*)0,&g_450},{&g_112[0],&g_112[3],&g_112[0]},{&g_450,&g_450,&g_450},{&g_112[1],(void*)0,(void*)0},{&g_112[5],&g_450,&g_450},{&g_328,&g_112[3],&g_328},{&g_112[5],&g_450,&g_112[0]},{&g_112[1],&g_450,&g_328}},{{&g_450,&g_450,&g_450},{&g_112[0],&g_450,&g_328},{&g_450,&g_450,&g_450},{&g_450,&g_112[3],&g_450},{&g_450,&g_450,&g_450},{&g_112[1],(void*)0,&g_328},{&g_112[5],&g_450,&g_450},{(void*)0,&g_112[3],&g_328}},{{&g_112[5],(void*)0,&g_112[0]},{&g_112[1],&g_450,&g_328},{&g_450,&g_450,&g_450},{&g_450,&g_450,(void*)0},{&g_450,(void*)0,&g_450},{&g_112[0],&g_112[3],&g_112[0]},{&g_450,&g_450,&g_450},{&g_112[1],(void*)0,(void*)0}},{{&g_112[5],&g_450,&g_450},{&g_328,&g_112[3],&g_328},{&g_112[5],&g_450,&g_112[0]},{&g_112[1],&g_450,&g_328},{&g_450,&g_450,&g_450},{&g_112[0],&g_450,&g_328},{&g_450,&g_450,&g_450},{&g_450,&g_112[3],&g_450}},{{&g_450,&g_450,&g_450},{&g_112[1],(void*)0,&g_328},{&g_112[5],&g_450,&g_450},{(void*)0,&g_112[3],&g_328},{&g_112[5],(void*)0,&g_112[0]},{&g_112[1],&g_450,&g_328},{&g_450,&g_450,&g_450},{&g_450,&g_450,(void*)0}},{{&g_450,(void*)0,&g_450},{&g_112[0],&g_112[3],&g_112[0]},{&g_450,&g_450,&g_450},{&g_112[1],(void*)0,(void*)0},{&g_112[5],&g_450,&g_450},{&g_328,&g_112[3],&g_328},{&g_328,&g_450,&g_450},{(void*)0,&g_450,&g_328}},{{&g_450,&g_450,&g_112[0]},{&g_328,&g_450,&g_112[0]},{&g_112[5],&g_450,(void*)0},{&g_328,(void*)0,&g_328},{&g_450,&g_112[0],(void*)0},{&g_328,&g_450,&g_112[0]},{&g_328,&g_112[5],&g_112[0]},{&g_450,(void*)0,&g_328}},{{&g_328,&g_450,&g_450},{&g_328,&g_450,&g_328},{&g_450,&g_450,&g_112[0]},{&g_328,&g_450,&g_450},{&g_112[5],&g_450,(void*)0},{&g_328,(void*)0,&g_328},{&g_450,&g_112[5],(void*)0},{(void*)0,&g_450,&g_450}},{{&g_328,&g_112[0],&g_112[0]},{&g_112[0],(void*)0,&g_328},{&g_328,&g_450,&g_450},{(void*)0,&g_450,&g_328},{&g_450,&g_450,&g_112[0]},{&g_328,&g_450,&g_112[0]},{&g_112[5],&g_450,(void*)0},{&g_328,(void*)0,&g_328}}};
static uint64_t **g_2111 = &g_2112[1][0][2];
static int32_t **g_2131 = &g_165;
static int8_t g_2195 = 6L;
static int64_t **g_2247[9][6][2] = {{{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,(void*)0},{&g_38,(void*)0},{&g_38,(void*)0}},{{&g_38,&g_38},{&g_38,&g_38},{(void*)0,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38}},{{(void*)0,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38}},{{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{(void*)0,&g_38},{(void*)0,&g_38},{(void*)0,&g_38}},{{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38}},{{&g_38,&g_38},{&g_38,&g_38},{(void*)0,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38}},{{(void*)0,&g_38},{&g_38,&g_38},{&g_38,(void*)0},{&g_38,(void*)0},{&g_38,(void*)0},{&g_38,&g_38}},{{&g_38,&g_38},{&g_38,&g_38},{&g_38,&g_38},{&g_38,(void*)0},{(void*)0,&g_38},{&g_38,(void*)0}},{{&g_38,&g_38},{&g_38,(void*)0},{&g_38,&g_38},{(void*)0,(void*)0},{&g_38,&g_38},{&g_38,&g_38}}};
static int64_t ***g_2246[10][3] = {{&g_2247[4][5][0],&g_2247[4][5][1],(void*)0},{&g_2247[4][5][0],&g_2247[0][4][1],&g_2247[0][4][1]},{&g_2247[4][5][0],(void*)0,&g_2247[4][5][0]},{&g_2247[6][4][0],&g_2247[4][5][1],&g_2247[0][3][0]},{(void*)0,&g_2247[4][5][1],&g_2247[4][5][0]},{&g_2247[4][5][0],(void*)0,&g_2247[4][5][0]},{&g_2247[4][5][0],&g_2247[4][5][0],&g_2247[4][5][0]},{&g_2247[4][5][0],(void*)0,&g_2247[4][5][0]},{(void*)0,&g_2247[0][4][1],&g_2247[4][5][0]},{&g_2247[6][4][0],&g_2247[4][5][0],&g_2247[4][5][0]}};
static int16_t g_2253 = 0x647FL;
static int8_t g_2276 = 7L;
static uint32_t g_2303 = 0xF464A646L;
static int16_t ***g_2322 = (void*)0;
static int16_t ****g_2321[10][9][2] = {{{&g_2322,&g_2322},{(void*)0,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322}},{{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,&g_2322}},{{(void*)0,(void*)0},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,&g_2322},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,(void*)0}},{{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322}},{{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{(void*)0,&g_2322}},{{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,(void*)0},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,&g_2322}},{{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,(void*)0},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,&g_2322}},{{(void*)0,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,&g_2322}},{{(void*)0,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,(void*)0}},{{&g_2322,&g_2322},{&g_2322,(void*)0},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,&g_2322},{(void*)0,&g_2322},{&g_2322,&g_2322},{&g_2322,(void*)0},{&g_2322,(void*)0}}};
static int16_t **** const *g_2320 = &g_2321[8][6][1];
static uint16_t g_2343 = 0x9D9DL;
static uint8_t g_2428 = 1UL;
static uint32_t g_2529 = 1UL;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t  func_7(uint16_t  p_8, int64_t  p_9, uint16_t  p_10, const int16_t  p_11);
static uint32_t  func_17(uint32_t  p_18, int64_t * p_19, float  p_20);
static int16_t  func_32(uint32_t  p_33, uint32_t  p_34, int64_t * p_35, int64_t * p_36, int16_t  p_37);
static int32_t  func_54(int32_t  p_55, const uint64_t  p_56);
static uint64_t  func_63(int32_t * p_64, int32_t * p_65);
static uint16_t  func_83(float  p_84, float  p_85, uint32_t  p_86, int64_t * p_87, float  p_88);
static uint8_t  func_98(uint32_t  p_99);
static int8_t  func_104(uint32_t  p_105, int32_t * p_106, int32_t * p_107, float  p_108, int8_t * const  p_109);
static uint16_t  func_115(const uint32_t  p_116);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_12 g_38 g_47 g_44 g_42 g_812 g_248 g_39 g_168 g_414 g_135 g_351 g_177 g_751 g_352 g_765 g_766 g_161 g_846 g_417 g_1649 g_1270 g_1271 g_810 g_534 g_535 g_1034 g_1287 g_372 g_1348 g_996 g_997 g_998 g_369 g_144 g_1912 g_174 g_1343 g_440 g_165 g_1073 g_145 g_1382 g_433 g_119 g_120 g_310 g_241 g_809 g_112 g_811 g_2041 g_1269 g_1831 g_2111 g_661 g_233 g_166 g_2131 g_669 g_2016 g_2253 g_536 g_287 g_2303 g_2320 g_2343 g_2276 g_118 g_2428 g_249 g_101 g_1891 g_1081 g_450 g_2529
 * writes: g_14 g_47 g_44 g_45 g_166 g_369 g_165 g_669 g_42 g_175 g_812 g_414 g_372 g_811 g_1034 g_1891 g_145 g_1831 g_12 g_1287 g_174 g_177 g_1073 g_433 g_1114 g_310 g_39 g_112 g_1912 g_2072 g_2111 g_2131 g_233 g_2016 g_2195 g_2246 g_248 g_2320 g_237 g_161 g_2276 g_97 g_249 g_168 g_1081 g_2529
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int64_t *l_13[4];
    uint32_t l_21 = 0x0A3531F1L;
    float *l_815[5][10][4] = {{{&g_461[0][1][1],&g_461[2][1][2],(void*)0,(void*)0},{&g_417[1],(void*)0,&g_175[0],&g_461[0][8][2]},{&g_175[3],&g_417[2],&g_175[3],(void*)0},{&g_417[2],(void*)0,(void*)0,&g_175[4]},{&g_175[0],(void*)0,(void*)0,&g_175[3]},{&g_461[1][5][2],&g_175[0],&g_461[3][8][4],&g_461[2][1][2]},{&g_461[2][1][2],(void*)0,&g_175[0],&g_417[2]},{&g_461[2][1][2],&g_417[2],&g_417[1],&g_461[2][1][2]},{&g_461[2][1][2],(void*)0,&g_175[0],(void*)0},{(void*)0,(void*)0,&g_417[2],&g_461[2][6][3]}},{{&g_461[3][8][4],(void*)0,&g_461[2][6][3],&g_461[0][6][4]},{(void*)0,&g_175[4],(void*)0,&g_175[0]},{&g_417[2],&g_417[1],&g_461[2][1][2],&g_417[2]},{&g_417[2],&g_417[2],&g_417[2],&g_417[1]},{&g_175[0],&g_175[3],&g_417[2],(void*)0},{&g_417[2],(void*)0,&g_461[2][1][2],&g_175[0]},{&g_417[2],&g_175[1],(void*)0,(void*)0},{(void*)0,(void*)0,&g_461[2][6][3],(void*)0},{&g_461[3][8][4],&g_417[2],&g_417[2],&g_461[2][1][2]},{(void*)0,&g_417[1],&g_175[0],(void*)0}},{{&g_461[2][1][2],&g_175[0],&g_417[1],&g_417[1]},{&g_461[2][1][2],&g_175[3],&g_175[0],(void*)0},{&g_461[2][1][2],&g_417[2],&g_461[3][8][4],&g_417[1]},{&g_461[1][5][2],&g_175[3],(void*)0,&g_175[0]},{&g_175[0],&g_417[2],(void*)0,&g_417[2]},{&g_417[2],(void*)0,&g_175[3],&g_175[3]},{&g_175[3],&g_175[3],&g_175[0],&g_417[2]},{&g_417[1],&g_461[1][5][2],(void*)0,(void*)0},{&g_461[0][1][1],&g_417[2],&g_461[2][1][2],(void*)0},{&g_175[0],&g_417[2],(void*)0,(void*)0}},{{&g_417[2],&g_461[1][5][2],&g_461[2][3][2],&g_417[2]},{&g_175[3],&g_175[3],&g_417[2],&g_175[3]},{&g_461[0][8][2],(void*)0,&g_175[3],&g_417[2]},{(void*)0,&g_417[2],&g_461[0][8][2],&g_175[0]},{(void*)0,&g_175[3],&g_417[2],&g_417[1]},{&g_417[2],&g_417[2],&g_461[1][6][2],(void*)0},{&g_417[2],&g_175[3],&g_461[0][6][4],&g_417[1]},{(void*)0,&g_175[0],&g_417[2],(void*)0},{&g_417[1],&g_417[1],&g_417[1],&g_461[2][1][2]},{(void*)0,&g_417[2],&g_417[2],(void*)0}},{{(void*)0,(void*)0,&g_417[1],(void*)0},{(void*)0,&g_175[1],&g_417[1],&g_175[0]},{&g_175[3],(void*)0,&g_175[0],(void*)0},{&g_461[2][6][3],&g_175[3],&g_461[2][1][2],&g_417[1]},{&g_417[1],(void*)0,&g_461[0][1][1],&g_175[1]},{&g_461[2][1][2],(void*)0,&g_461[1][5][2],(void*)0},{&g_417[1],&g_175[0],(void*)0,(void*)0},{(void*)0,&g_417[2],(void*)0,&g_417[1]},{&g_461[2][1][2],&g_175[3],&g_175[4],(void*)0},{&g_461[3][8][4],(void*)0,(void*)0,(void*)0}}};
    float l_1853 = 0x1.Fp-1;
    int64_t l_1864[10] = {8L,8L,8L,8L,8L,8L,8L,8L,8L,8L};
    int64_t l_1892[7] = {0x366B8F190BA8E89ELL,0x366B8F190BA8E89ELL,0x366B8F190BA8E89ELL,0x366B8F190BA8E89ELL,0x366B8F190BA8E89ELL,0x366B8F190BA8E89ELL,0x366B8F190BA8E89ELL};
    int16_t l_1920 = 1L;
    int32_t l_1922 = 0L;
    int16_t l_1923 = 0xBE05L;
    uint32_t l_1944 = 0xB24D8F8DL;
    int32_t l_2007 = (-1L);
    int32_t l_2009 = 0x2C944F3BL;
    int32_t l_2014 = (-6L);
    int32_t l_2018[5];
    const int16_t ***l_2044 = (void*)0;
    const int16_t *** const *l_2043 = &l_2044;
    const int16_t *** const **l_2042[10] = {&l_2043,&l_2043,(void*)0,&l_2043,&l_2043,(void*)0,&l_2043,&l_2043,(void*)0,&l_2043};
    float l_2047 = 0x9.Ep+1;
    uint32_t *** const l_2144 = &g_1382;
    uint16_t * const *l_2162 = (void*)0;
    int64_t **l_2184 = (void*)0;
    int64_t ***l_2183 = &l_2184;
    uint8_t l_2200 = 255UL;
    const int8_t *l_2219 = (void*)0;
    const int8_t **l_2218[8];
    uint32_t l_2324[10][5] = {{18446744073709551615UL,18446744073709551615UL,6UL,0UL,0x4B172ED1L},{1UL,0UL,0UL,1UL,18446744073709551615UL},{1UL,0UL,0xB98033C1L,0xB98033C1L,0UL},{18446744073709551615UL,0UL,0xB98033C1L,6UL,6UL},{0UL,18446744073709551615UL,0UL,0xB98033C1L,6UL},{0UL,1UL,6UL,1UL,0UL},{0UL,1UL,18446744073709551615UL,0UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,6UL,0UL,0x4B172ED1L},{1UL,0UL,0UL,1UL,18446744073709551615UL},{1UL,0UL,0xB98033C1L,0xB98033C1L,0UL}};
    uint64_t l_2346 = 18446744073709551606UL;
    int32_t *l_2377 = &l_2014;
    int32_t **l_2386 = &g_1891[0][1];
    int16_t l_2458 = 1L;
    float l_2468 = 0x8.F79AE6p+23;
    int32_t l_2482[3][3][3] = {{{0xE93573FEL,0xE93573FEL,7L},{0x1AC8D460L,0x1AC8D460L,1L},{0xE93573FEL,0xE93573FEL,7L}},{{0x1AC8D460L,0x1AC8D460L,1L},{0xE93573FEL,0xE93573FEL,7L},{0x1AC8D460L,0x1AC8D460L,1L}},{{0xE93573FEL,0xE93573FEL,7L},{0x1AC8D460L,0x1AC8D460L,1L},{0xE93573FEL,0xE93573FEL,7L}}};
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_13[i] = (void*)0;
    for (i = 0; i < 5; i++)
        l_2018[i] = 0x90237899L;
    for (i = 0; i < 8; i++)
        l_2218[i] = &l_2219;
    if (((!((safe_sub_func_float_f_f((+(((g_6 > (func_7(g_12, (g_14 = g_12), (((safe_lshift_func_uint16_t_u_u(0x4C03L, 14)) >= func_17(l_21, l_13[1], (safe_add_func_float_f_f((-0x1.7p+1), (safe_add_func_float_f_f((g_175[0] = (safe_sub_func_float_f_f((((safe_sub_func_float_f_f(((safe_sub_func_float_f_f((func_32(l_21, l_21, g_38, l_13[1], g_12) , 0x0.4p-1), (-0x2.Bp+1))) > g_248), 0xD.FEFE0Bp-73)) != 0x4.Fp-1) != g_39), l_21))), g_168)))))) >= l_21), (**g_352)) , 0x2.E9D285p+92)) <= l_21) >= (*g_846))), l_21)) > l_21)) , l_21))
    { /* block id: 868 */
        uint32_t l_1856 = 0xB12807BEL;
        int16_t * const *l_1862 = &g_351;
        uint64_t ***l_1863 = (void*)0;
        uint32_t **l_1869 = (void*)0;
        uint32_t **l_1870 = &g_1034;
        uint32_t *l_1871[6][1] = {{&g_1287[0]},{&g_372},{&g_1287[0]},{&g_372},{&g_1287[0]},{&g_372}};
        uint32_t *l_1873 = &l_1856;
        uint32_t **l_1872 = &l_1873;
        int16_t l_1874 = 0x3315L;
        uint8_t *l_1888 = (void*)0;
        int32_t l_1889 = 0L;
        uint64_t l_1983 = 0x98F23C84425E66BDLL;
        int8_t l_2006 = 0xB4L;
        int32_t l_2023 = 8L;
        int32_t l_2025 = (-1L);
        int32_t l_2026 = 0L;
        int32_t l_2029 = 0x80DEEA91L;
        int32_t l_2049 = 0x60953231L;
        int32_t l_2053 = 8L;
        int32_t **l_2129 = &g_165;
        int64_t l_2172 = 0x71530C0E06F73EFDLL;
        float l_2182 = (-0x3.0p-1);
        uint32_t l_2199 = 18446744073709551612UL;
        float **l_2217[5][2][6] = {{{&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3]},{&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0]}},{{&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3]},{&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0]}},{{&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3]},{&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0]}},{{&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3]},{&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0]}},{{&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3]},{&l_815[4][2][0],&l_815[4][2][0],&l_815[3][9][3],&l_815[3][9][3],&l_815[4][2][0],&l_815[4][2][0]}}};
        int32_t **l_2378 = &g_766;
        int i, j, k;
lbl_2146:
        l_1864[4] |= (((safe_sub_func_int16_t_s_s(l_1856, (((*g_1649) = 0xFAL) <= ((*g_1270) == (safe_sub_func_int64_t_s_s((safe_lshift_func_uint16_t_u_s((0x9591L >= g_810[2][4]), (!((((l_1862 != l_1862) & (((l_1863 != (((*g_846) , l_1856) , (*g_534))) == l_21) > (*g_1034))) == l_1856) <= l_21)))), 0x50326B3FCA0267F7LL)))))) < 1UL) && l_1856);
        if ((((l_1856 ^ ((1L || (*g_38)) | ((safe_mod_func_uint8_t_u_u(((l_1871[5][0] = ((*l_1870) = &g_1287[1])) == ((*l_1872) = l_815[3][7][3])), l_1874)) >= (safe_sub_func_uint64_t_u_u(((l_1889 = (+(safe_mod_func_int8_t_s_s((safe_div_func_uint32_t_u_u((l_1864[2] | (g_414[0] = (((safe_mul_func_uint16_t_u_u((((safe_add_func_int8_t_s_s((safe_add_func_int32_t_s_s(l_21, 0x82E9B1A2L)), 255UL)) > (**g_1348)) != (*g_38)), 1L)) , 0xA104F925771080D8LL) && l_1874))), (-1L))), (**g_996))))) && l_1856), l_1864[4]))))) <= l_1864[4]) != g_369))
        { /* block id: 876 */
            int32_t **l_1890[3][10] = {{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165},{&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165,&g_165}};
            uint16_t *l_1893 = &g_1831;
            uint64_t l_1905 = 1UL;
            float *l_1909 = &g_1114;
            int8_t l_2054 = 0x7DL;
            uint8_t *l_2086 = &g_414[0];
            int32_t l_2139[10][3][8] = {{{0xC5585CD6L,8L,(-1L),(-1L),8L,0xC5585CD6L,7L,0L},{(-1L),7L,0L,0xDA245669L,0L,0x9BE3A5C8L,0x8F132AADL,2L},{(-1L),7L,0x548C8047L,0xDA245669L,0x42BAFB6CL,0x0D97C251L,6L,0L}},{{0xC4275DE0L,0x42BAFB6CL,1L,(-1L),0x548C8047L,0x3B35E5D1L,0x9F487CA3L,0L},{(-7L),0x77CDEAD4L,1L,0x9C6FA183L,1L,1L,(-1L),0x9BE3A5C8L},{0L,0xB6DCD4A3L,0x1DD784F3L,8L,0xBA94D472L,5L,7L,0L}},{{0xE6627106L,0L,0L,0L,1L,0xDA245669L,1L,0L},{0x48213418L,0xBA94D472L,0x48213418L,(-1L),(-1L),0x42BAFB6CL,0xDC07F66EL,0x0D97C251L},{0L,2L,0xF3E43737L,0x03B5352AL,0x11A18668L,(-10L),(-1L),(-1L)}},{{0L,6L,0x77CDEAD4L,0xB1604F82L,(-1L),0L,(-1L),0x9C6FA183L},{0x48213418L,0x1DD784F3L,0xC4275DE0L,0xE6627106L,1L,0x11A18668L,0x3B35E5D1L,(-10L)},{0xE6627106L,0x48213418L,0x8F132AADL,0x42BAFB6CL,0xBA94D472L,0xB6DCD4A3L,0xC17C49E9L,0x03B5352AL}},{{0L,0x36386259L,0x11A18668L,0x9BE3A5C8L,1L,0xF3E43737L,0L,0xBA94D472L},{(-7L),0xB1604F82L,7L,0x548C8047L,0x548C8047L,7L,0xB1604F82L,(-7L)},{0xC4275DE0L,0xDA245669L,0L,0L,0x42BAFB6CL,8L,1L,1L}},{{(-1L),0L,0x9C6FA183L,(-10L),0L,8L,0x4B0AB393L,0xC5585CD6L},{(-1L),0xDA245669L,(-1L),0x9F487CA3L,8L,7L,1L,0x36386259L},{0xC5585CD6L,0xB1604F82L,0x42BAFB6CL,0x4B0AB393L,0xDC07F66EL,0xF3E43737L,0x36386259L,0xC17C49E9L}},{{0L,0x36386259L,0xDC07F66EL,0x3B35E5D1L,2L,0xB6DCD4A3L,0x11A18668L,0xE6627106L},{1L,0x48213418L,(-10L),1L,(-1L),0x11A18668L,0L,(-1L)},{0xF3E43737L,0L,0L,0L,0xF3E43737L,8L,(-1L),0xC17C49E9L}},{{0L,7L,5L,0xBA94D472L,8L,0x1DD784F3L,0xB6DCD4A3L,0L},{0x42BAFB6CL,0L,5L,0L,0xC17C49E9L,1L,(-1L),0L},{8L,0L,0L,0xC17C49E9L,0x48213418L,0x11A18668L,0L,0xB1604F82L}},{{0x03B5352AL,0x4B0AB393L,0x1DD784F3L,(-1L),0xB6DCD4A3L,0xE6627106L,(-1L),2L},{0x8F132AADL,(-1L),(-1L),(-10L),0x548C8047L,0xC17C49E9L,7L,0x03B5352AL},{(-1L),0x48213418L,1L,5L,0x36386259L,0x36386259L,5L,1L}},{{1L,1L,2L,0x48213418L,0L,0xC5585CD6L,0xDC07F66EL,7L},{(-10L),0xBA94D472L,0x3B35E5D1L,0x77CDEAD4L,0x0D97C251L,1L,0x03B5352AL,7L},{0xBA94D472L,0x42BAFB6CL,0x8F132AADL,0x48213418L,0xE6627106L,(-7L),0x9BE3A5C8L,1L}}};
            int16_t l_2275 = 0x57F8L;
            uint32_t l_2278 = 2UL;
            uint64_t ****l_2292[8][1] = {{&l_1863},{&l_1863},{&l_1863},{&l_1863},{&l_1863},{&l_1863},{&l_1863},{&l_1863}};
            const int64_t **l_2332 = (void*)0;
            const int64_t ***l_2331 = &l_2332;
            int8_t l_2342[5][1] = {{0x49L},{3L},{0x49L},{3L},{0x49L}};
            int64_t l_2473 = (-4L);
            const int64_t l_2474 = 0x07E6178E72998841LL;
            int i, j, k;
            (*g_144) = (g_1891[0][3] = (g_165 = &g_1073[0][0]));
            if ((((*l_1893) = l_1892[4]) , (l_1874 ^ (248UL == (l_1864[4] <= (!0x0FL))))))
            { /* block id: 881 */
                int32_t l_1906 = 0xA7DC7E18L;
                int32_t l_2008[7];
                uint64_t l_2045 = 0x648A1B5393096AE7LL;
                int64_t l_2050 = (-2L);
                int64_t l_2087 = (-7L);
                int32_t ***l_2130[2];
                int32_t l_2145 = 8L;
                int i;
                for (i = 0; i < 7; i++)
                    l_2008[i] = 0x0B5427CCL;
                for (i = 0; i < 2; i++)
                    l_2130[i] = &l_1890[0][1];
                for (g_12 = 0; (g_12 != (-27)); g_12 = safe_sub_func_uint8_t_u_u(g_12, 1))
                { /* block id: 884 */
                    float *l_1907 = &g_175[0];
                    float **l_1908 = &l_815[2][6][1];
                    int32_t l_1919 = 0L;
                    uint16_t *l_1921 = &g_174;
                    int32_t l_2005 = 0x17F41BBBL;
                    int32_t l_2010 = 0xB9E18064L;
                    int32_t l_2012 = (-10L);
                    int32_t l_2019 = 0x59CABAD8L;
                    int32_t l_2020 = 0xAD65F9E8L;
                    int32_t l_2021 = 0xBA37A01DL;
                    int32_t l_2028 = 0L;
                    uint16_t l_2030[10][7] = {{65530UL,65533UL,65533UL,65530UL,0x0303L,65530UL,65533UL},{1UL,1UL,65533UL,0x3DC3L,65533UL,1UL,1UL},{1UL,65533UL,0x3DC3L,65533UL,1UL,1UL,65533UL},{65530UL,0x0303L,65530UL,65533UL,65533UL,65530UL,0x0303L},{65533UL,0x0303L,0x3DC3L,0x3DC3L,0x0303L,65533UL,0x0303L},{65530UL,65533UL,65533UL,65530UL,0x0303L,65530UL,65533UL},{1UL,1UL,65533UL,0x3DC3L,65533UL,1UL,1UL},{1UL,65533UL,0x3DC3L,65533UL,1UL,1UL,65533UL},{65530UL,0x0303L,65530UL,65533UL,65533UL,65530UL,0x0303L},{65533UL,0x0303L,0x3DC3L,0x3DC3L,0x0303L,65533UL,0x0303L}};
                    int16_t l_2052 = 4L;
                    uint32_t l_2088 = 1UL;
                    int i, j;
                    if ((((l_1922 = (((*l_1921) ^= ((*l_1893) = (safe_lshift_func_int16_t_s_u(((((safe_div_func_int16_t_s_s(0x02B4L, (*g_1270))) < (l_1905 ^ (((**l_1870) = (((l_1906 , ((*l_1908) = l_1907)) == l_1909) || l_1889)) < (safe_sub_func_int16_t_s_s(((((*l_1907) = g_1912) , (safe_rshift_func_int8_t_s_s((safe_mod_func_int64_t_s_s((safe_add_func_int64_t_s_s(7L, l_1919)), 0xE34FBF89B4FA3879LL)), l_21))) == l_1919), l_1892[4]))))) && l_1920) , l_21), l_1919)))) ^ (*g_351))) != l_1889) >= l_1923))
                    { /* block id: 891 */
                        uint64_t l_1933 = 1UL;
                        uint64_t l_1945 = 0x3D80AC993321AF8ELL;
                        l_1945 |= ((safe_div_func_uint32_t_u_u((*g_1034), 0x3532A700L)) , ((safe_rshift_func_uint16_t_u_s(((safe_lshift_func_uint8_t_u_s((safe_div_func_int16_t_s_s((+((((0xCC22L == (l_1889 = l_1933)) == ((safe_add_func_uint8_t_u_u(((**g_996) & (safe_mod_func_uint8_t_u_u((safe_mod_func_int8_t_s_s((safe_mod_func_int16_t_s_s(((**l_1862) |= (safe_sub_func_int16_t_s_s(l_1906, ((l_1933 , (l_1856 <= ((**g_765) , 0xC1069FB4B5B71121LL))) , g_1343[7][5])))), l_1919)), 0x83L)), 0x49L))), l_1919)) != 7L)) & l_1944) || l_1919)), g_440)), 7)) != (*g_165)), 15)) || 4294967295UL));
                        (**g_144) = (safe_lshift_func_uint16_t_u_u((++(*l_1921)), 12));
                        return (*g_766);
                    }
                    else
                    { /* block id: 898 */
                        (*g_165) = (*g_165);
                        if (l_1889)
                            break;
                        (*l_1909) = (((*g_1382) = (*g_1382)) == ((safe_div_func_int32_t_s_s((safe_div_func_int32_t_s_s(0xB95B9110L, 0x64D4838EL)), ((((0xAA97BC34L <= ((l_1889 = (safe_div_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_s((l_1919 & 0UL), (**g_1348))), l_1864[1]))) | (((**l_1870) = ((safe_div_func_float_f_f(((-0x2.Bp+1) <= l_1919), l_1864[4])) , (*g_1034))) != 0xD8898AE3L))) || l_1906) >= (*g_351)) , (*g_165)))) , l_1907));
                        if (l_1919)
                            break;
                    }
                    if ((**g_144))
                    { /* block id: 907 */
                        uint8_t *l_1984 = (void*)0;
                        uint8_t *l_1985 = &g_310;
                        int32_t l_1990 = 0x2FD77467L;
                        uint64_t *l_2004 = &g_112[0];
                        int32_t l_2011 = (-8L);
                        int32_t l_2013 = 4L;
                        int32_t l_2015 = 0L;
                        int32_t l_2017 = 0xEEA76E7EL;
                        int32_t l_2022 = 0xF297B48AL;
                        int64_t l_2024[10][9][2] = {{{0xEAD0F85B510F78C6LL,8L},{8L,0xEAD0F85B510F78C6LL},{1L,0xCC2BD6F6BBD75329LL},{(-1L),4L},{8L,0L},{4L,1L},{0L,1L},{4L,0L},{8L,4L}},{{(-1L),0xCC2BD6F6BBD75329LL},{1L,0xEAD0F85B510F78C6LL},{8L,8L},{0xEAD0F85B510F78C6LL,0xEAD0F85B510F78C6LL},{0x915DDB6E76BA8261LL,0xB223D1D1A764CDFALL},{0L,0L},{0xAEF0B1510F07D5E0LL,0L},{0xEAD0F85B510F78C6LL,0L},{0xEAD0F85B510F78C6LL,0L}},{{0xAEF0B1510F07D5E0LL,0L},{0L,0xB223D1D1A764CDFALL},{0x915DDB6E76BA8261LL,0xEAD0F85B510F78C6LL},{4L,0L},{0L,4L},{0xEAD0F85B510F78C6LL,0x915DDB6E76BA8261LL},{0xB223D1D1A764CDFALL,0L},{0L,0xAEF0B1510F07D5E0LL},{0L,0xEAD0F85B510F78C6LL}},{{0L,0xEAD0F85B510F78C6LL},{0L,0xAEF0B1510F07D5E0LL},{0L,0L},{0xB223D1D1A764CDFALL,0x915DDB6E76BA8261LL},{0xEAD0F85B510F78C6LL,4L},{0L,0L},{4L,0xEAD0F85B510F78C6LL},{0x915DDB6E76BA8261LL,0xB223D1D1A764CDFALL},{0L,0L}},{{0xAEF0B1510F07D5E0LL,0L},{0xEAD0F85B510F78C6LL,0L},{0xEAD0F85B510F78C6LL,0L},{0xAEF0B1510F07D5E0LL,0L},{0L,0xB223D1D1A764CDFALL},{0x915DDB6E76BA8261LL,0xEAD0F85B510F78C6LL},{4L,0L},{0L,4L},{0xEAD0F85B510F78C6LL,0x915DDB6E76BA8261LL}},{{0xB223D1D1A764CDFALL,0L},{0L,0xAEF0B1510F07D5E0LL},{0L,0xEAD0F85B510F78C6LL},{0L,0xEAD0F85B510F78C6LL},{0L,0xAEF0B1510F07D5E0LL},{0L,0L},{0xB223D1D1A764CDFALL,0x915DDB6E76BA8261LL},{0xEAD0F85B510F78C6LL,4L},{0L,0L}},{{4L,0xEAD0F85B510F78C6LL},{0x915DDB6E76BA8261LL,0xB223D1D1A764CDFALL},{0L,0L},{0xAEF0B1510F07D5E0LL,0L},{0xEAD0F85B510F78C6LL,0L},{0xEAD0F85B510F78C6LL,0L},{0xAEF0B1510F07D5E0LL,0L},{0L,0xB223D1D1A764CDFALL},{0x915DDB6E76BA8261LL,0xEAD0F85B510F78C6LL}},{{4L,0L},{0L,4L},{0xEAD0F85B510F78C6LL,0x915DDB6E76BA8261LL},{0xB223D1D1A764CDFALL,0L},{0L,0xAEF0B1510F07D5E0LL},{0L,0xEAD0F85B510F78C6LL},{0L,0xEAD0F85B510F78C6LL},{0L,0xAEF0B1510F07D5E0LL},{0L,0L}},{{0xB223D1D1A764CDFALL,0x915DDB6E76BA8261LL},{0xEAD0F85B510F78C6LL,4L},{0L,0L},{4L,0xEAD0F85B510F78C6LL},{0x915DDB6E76BA8261LL,0xB223D1D1A764CDFALL},{0L,0L},{0xAEF0B1510F07D5E0LL,0L},{0xEAD0F85B510F78C6LL,0L},{0xEAD0F85B510F78C6LL,0L}},{{0xAEF0B1510F07D5E0LL,0L},{0L,0xB223D1D1A764CDFALL},{0x915DDB6E76BA8261LL,0xEAD0F85B510F78C6LL},{4L,0L},{0L,4L},{0xEAD0F85B510F78C6LL,0x915DDB6E76BA8261LL},{0xB223D1D1A764CDFALL,0L},{0L,0xAEF0B1510F07D5E0LL},{0L,0xEAD0F85B510F78C6LL}}};
                        int32_t l_2027[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_2027[i] = 0x21EA84EAL;
                        (*g_165) &= (~((+((&g_38 == &g_38) , (((((safe_sub_func_uint64_t_u_u(((safe_mod_func_int8_t_s_s((safe_add_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((((safe_div_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((((l_1919 = (+((safe_add_func_uint64_t_u_u(((*l_2004) ^= ((*g_119) > ((safe_div_func_int64_t_s_s((safe_add_func_uint32_t_u_u((l_1983 >= (((++(*l_1985)) , (safe_sub_func_uint8_t_u_u((((l_1990 != ((*g_1034) ^ ((safe_add_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(l_1990, (((safe_mul_func_int16_t_s_s((-6L), ((safe_lshift_func_int16_t_s_u(((safe_lshift_func_uint16_t_u_u((~((*g_38) = (safe_lshift_func_int8_t_s_u(l_1923, g_241)))), 4)) > 0xBCDAL), 7)) >= l_1990))) | 0xF61312C2L) && l_1906))), (-1L))) || l_1906))) <= 0x2DC7L) , l_1856), g_809))) ^ l_1906)), 0UL)), l_1906)) || l_1922))), l_1892[4])) < (-1L)))) , l_1919) , 18446744073709551614UL), 0xBF976F83B8EB5263LL)), l_2005)) <= 0L) , 1UL), 18446744073709551609UL)), (*g_351))), 255UL)) , l_1919), 0x25C8E3B3FC2AEF0FLL)) != l_1889) >= (*g_1649)) && l_2006) || l_1906))) , l_2007));
                        (*l_1907) = (g_1912 = (-0x4.Ap+1));
                        ++l_2030[5][2];
                        l_2028 = ((safe_mul_func_int8_t_s_s((l_2008[0] = (*g_1649)), (((l_2045 = (safe_lshift_func_int16_t_s_s((247UL < (&g_846 != &g_846)), (!((~((((g_2041 , (((*g_38) && ((*g_1649) , l_1990)) > (**g_144))) | 0xAB04L) , (void*)0) != l_2042[9])) < (*g_1649)))))) > (-5L)) , (-9L)))) || (-1L));
                    }
                    else
                    { /* block id: 919 */
                        int32_t l_2046 = 0x31B03A5FL;
                        int32_t l_2048 = 0L;
                        int32_t l_2051[7] = {1L,0x034253B2L,1L,1L,0x034253B2L,1L,1L};
                        uint8_t l_2055 = 3UL;
                        uint8_t l_2073 = 255UL;
                        int i;
                        l_2055--;
                        (**g_144) = ((((safe_add_func_int32_t_s_s(((safe_rshift_func_int16_t_s_u((safe_lshift_func_int16_t_s_u(l_2048, (((*g_351) = ((((**l_1870) = ((((safe_rshift_func_int16_t_s_u((l_2073 |= (((safe_add_func_uint64_t_u_u(4UL, 0x79BB10B4E857B0F9LL)) >= (*g_38)) ^ (safe_rshift_func_uint16_t_u_u(((*l_1921) = (safe_rshift_func_uint16_t_u_u(((g_2072[0] = l_1909) != l_1909), 8))), 12)))), 15)) , (safe_sub_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(l_2008[4], (safe_rshift_func_int8_t_s_s((safe_div_func_uint32_t_u_u(((((safe_mod_func_uint32_t_u_u((l_1944 ^ (((safe_mod_func_int32_t_s_s(((0x68C9L || l_2051[2]) , l_2055), 1UL)) , (-7L)) , (*g_38))), 1UL)) , l_2086) != (void*)0) , 9UL), l_2030[0][2])), (*g_1649))))), (**g_1269)))) <= 0x0FL) , l_2008[6])) < l_1919) , (*g_351))) && 0x8269L))), 8)) > 1L), l_2087)) & (*g_165)) & (-4L)) <= l_2088);
                    }
                    if ((safe_div_func_uint32_t_u_u((*g_1034), (safe_div_func_int16_t_s_s((safe_div_func_uint32_t_u_u((*g_1034), ((((((safe_sub_func_uint8_t_u_u((((*l_1921) = ((safe_div_func_int16_t_s_s(0x6C1BL, ((*g_351) = (-1L)))) > ((l_2029 && (((safe_rshift_func_uint16_t_u_s((*g_1270), 9)) ^ 0UL) >= (g_1073[1][0] < ((*l_1893) &= (safe_mod_func_uint64_t_u_u(((safe_rshift_func_int16_t_s_s(0L, 6)) < g_161), l_2014)))))) != l_2023))) , (**g_996)), l_1856)) & l_2014) || (*g_1270)) != l_1874) && (**g_144)) , l_2088))), g_1287[0])))))
                    { /* block id: 931 */
                        uint64_t l_2106 = 0x9412565373D992B4LL;
                        --l_2106;
                    }
                    else
                    { /* block id: 933 */
                        uint64_t ***l_2113 = &g_2111;
                        uint64_t **l_2115[5][6] = {{&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2],&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2]},{&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2],&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2]},{&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2],&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2]},{&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2],&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2]},{&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[6][5][2],&g_2112[1][0][2],&g_2112[1][0][2],&g_2112[1][0][2]}};
                        uint64_t ***l_2114 = &l_2115[4][3];
                        int32_t l_2118 = 8L;
                        int32_t l_2126 = 0L;
                        int i, j;
                        l_2018[0] ^= (*g_165);
                        l_2126 |= (safe_add_func_uint64_t_u_u((((void*)0 != &l_13[0]) , (((*l_2113) = g_2111) != ((*l_2114) = &g_2112[1][0][2]))), (safe_mul_func_int8_t_s_s(((*g_1649) = (((l_2118 & (!((0UL >= (255UL | ((safe_add_func_int16_t_s_s(((safe_div_func_int32_t_s_s((((*g_165) = l_2020) && (((*l_1907) = (safe_add_func_float_f_f(((void*)0 != &g_1287[0]), 0x6.8p-1))) , 0L)), l_2118)) , (**g_1348)), l_2050)) && (*g_1649)))) == (*g_1649)))) > (*g_1034)) <= l_2026)), g_661[1][1]))));
                    }
                }
                (*l_1909) = ((safe_add_func_float_f_f((l_2129 == (g_2131 = &g_1891[0][0])), (safe_div_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f((((**l_2129) == ((!l_2139[7][2][6]) <= (**l_2129))) > ((safe_mul_func_float_f_f((l_2014 < (safe_mul_func_float_f_f((&g_906 == ((*g_165) , l_2144)), (**l_2129)))), (**l_2129))) == l_2145)), 0x1.Cp-1)), (-0x5.Bp-1))), (-0x1.Ap-1))))) > (**l_2129));
                return (*g_766);
            }
            else
            { /* block id: 946 */
                int32_t ***l_2165[9] = {&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129};
                int32_t ***l_2167 = &g_2131;
                int32_t ****l_2166 = &l_2167;
                int i;
                if (l_2007)
                    goto lbl_2146;
                (*l_1909) = ((((+((safe_lshift_func_int16_t_s_u(0L, 11)) > l_1892[4])) | (safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_s((-1L), 10)), (safe_lshift_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(((*l_2086) |= (((18446744073709551613UL == ((safe_div_func_uint64_t_u_u((((void*)0 != l_2162) , (safe_sub_func_uint8_t_u_u(((l_2165[0] == ((*l_2166) = &l_2129)) ^ (safe_lshift_func_uint8_t_u_u((((((((safe_add_func_int64_t_s_s((g_233 ^= ((*g_38) = 0x3FF2E05826C7DC18LL)), g_809)) , l_2172) | (*g_997)) < 2UL) , (*g_1034)) < 4294967292UL) == 0x7BL), 2))), 7UL))), (**l_2129))) | 0UL)) , (**g_996)) == (*g_1649))), 0L)), g_166)))), (*g_1649)))) >= (**g_2131)) , (**l_2129));
                for (g_669 = 0; (g_669 <= 43); g_669++)
                { /* block id: 955 */
                    uint64_t l_2179 = 0xE63D7388F10154BALL;
                    int64_t l_2196 = 1L;
                    int32_t l_2197 = 7L;
                    for (g_2016 = 3; (g_2016 >= 0); g_2016 -= 1)
                    { /* block id: 958 */
                        int32_t l_2194 = (-1L);
                        int i, j;
                        (***l_2167) = (**l_2129);
                        l_2197 = (l_2196 = (((***l_2167) = ((((safe_mul_func_float_f_f(((safe_sub_func_float_f_f((*g_846), (l_2179 > (safe_div_func_float_f_f((g_2195 = (l_2182 <= (((void*)0 == l_2183) , (safe_add_func_float_f_f(((safe_mul_func_float_f_f((safe_sub_func_float_f_f((0xF.F7533Cp+61 < (((*l_1909) = (-0x1.Ep+1)) == (((safe_lshift_func_int8_t_s_s((****l_2166), (((!0x2A53L) & 0UL) != (***l_2167)))) < 0x0B9AL) , l_2014))), (*g_846))), l_1864[4])) >= (-0x2.4p+1)), l_2194))))), l_2179))))) != 0x1.Bp-1), l_1944)) > l_2018[0]) <= (****l_2166)) == l_1864[4])) >= l_2018[0]));
                    }
                    if ((***l_2167))
                        break;
                }
            }
            if ((**l_2129))
            { /* block id: 969 */
                uint64_t l_2213 = 0xAED04D79A75DC6F7LL;
                float **l_2216 = &l_815[1][2][2];
                uint32_t l_2220 = 0xDA2C17D1L;
                int32_t l_2274 = (-9L);
                int8_t *l_2315[7][4][3] = {{{&g_97[3][0][0],&g_2276,&g_97[3][0][0]},{&g_2195,&g_2195,&l_2006},{&g_2195,&g_2276,&g_2195},{&g_2195,&l_2006,&l_2006}},{{&g_97[3][0][0],&g_2276,&g_97[3][0][0]},{&g_2195,&g_2195,&l_2006},{&g_2195,&g_2276,&g_2195},{&g_2195,&l_2006,&l_2006}},{{&g_97[3][0][0],&g_2276,&g_97[3][0][0]},{&g_2195,&g_2195,&l_2006},{&g_2195,&g_2276,&g_2195},{&g_2195,&l_2006,&l_2006}},{{&g_97[3][0][0],&g_2276,&g_97[3][0][0]},{&g_2195,&g_2195,&l_2006},{&g_2195,&g_2276,&g_2195},{&g_2195,&l_2006,&l_2006}},{{&g_97[3][0][0],&g_2276,&g_97[3][0][0]},{&g_2195,&g_2195,&l_2006},{&g_2195,&g_2276,&g_2195},{&g_2195,&l_2006,&l_2006}},{{&g_97[3][0][0],&g_2276,&g_97[3][0][0]},{&g_2195,&g_2195,&l_2006},{&g_2195,&g_2276,&g_2195},{&g_2195,&l_2006,&l_2006}},{{&g_97[3][0][0],&g_2276,&g_97[3][0][0]},{&g_2195,&g_2195,&l_2006},{&g_2195,&g_2276,&g_2195},{&g_2195,&l_2006,&l_2006}}};
                uint32_t *l_2323[5][1] = {{&g_669},{&g_669},{&g_669},{&g_669},{&g_669}};
                int64_t l_2341 = 0x249F0DBD04A5836DLL;
                int16_t *****l_2344 = (void*)0;
                int32_t l_2345 = 0x95732DEBL;
                int i, j, k;
                (**g_144) = ((((**l_2129) <= ((+(l_2200 = (l_2018[0] <= l_2199))) != ((((((((*l_2086) &= 1UL) || (safe_add_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((safe_div_func_int16_t_s_s((((void*)0 != l_2144) >= (safe_sub_func_uint64_t_u_u(0UL, ((((((safe_add_func_uint64_t_u_u(((safe_div_func_uint16_t_u_u(l_2213, (safe_lshift_func_uint16_t_u_s(((void*)0 == &g_144), 7)))) >= (*g_1649)), (-10L))) > (-1L)) & (*g_38)) , l_2216) == l_2217[1][0][5]) , (**l_2129))))), 0xD4E8L)), (**g_1269))), (**l_2129)))) , l_2213) >= (*g_1649)) , (void*)0) == l_2218[6]) >= l_2220))) != (**l_2129)) , (**g_2131));
                if ((**g_2131))
                { /* block id: 973 */
                    uint64_t ****l_2225 = &l_1863;
                    float ***l_2230 = &l_2216;
                    float ****l_2231 = (void*)0;
                    float ****l_2232 = &l_2230;
                    int32_t l_2233 = 0x3AEF594DL;
                    const int64_t ***l_2252 = (void*)0;
                    int32_t l_2277 = 1L;
                    if ((safe_sub_func_int64_t_s_s((safe_add_func_uint64_t_u_u(((void*)0 != l_2225), ((safe_lshift_func_uint16_t_u_u(l_2014, 2)) && (safe_sub_func_uint8_t_u_u(1UL, (*g_1649)))))), (&g_845[2] != ((*l_2232) = l_2230)))))
                    { /* block id: 975 */
                        uint32_t l_2234 = 0xDC49A2EBL;
                        int64_t ***l_2245 = (void*)0;
                        float l_2254 = 0xE.D68736p-61;
                        (*l_2129) = (*g_2131);
                        l_2233 ^= (**g_144);
                        l_2233 &= (*g_165);
                        l_2014 = (((l_2234 != (-0x3.2p+1)) > (((((safe_sub_func_float_f_f(((safe_mul_func_float_f_f(((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((safe_div_func_float_f_f(((((**l_2129) > l_2220) != (((l_2234 , (*g_846)) , ((g_2246[8][0] = l_2245) != ((((((safe_lshift_func_int16_t_s_u(((safe_sub_func_uint32_t_u_u(4294967292UL, 2UL)) == 0xE134L), 10)) == (*g_1649)) , 1UL) ^ (*g_997)) != (**l_2129)) , l_2252))) , (*g_846))) > 0x9.4p-1), l_2007)), 0x5.B1964Ap-3)) >= 0x2.D1B687p-83), l_2233)) == g_2253), l_2254)) <= l_2213), 0x7.99976Cp-70)) != 0x5.0B9153p-52) == 0x7.Ep-1) >= (-0x1.Fp+1)) == 0x1.515A36p-53)) < l_2018[0]);
                    }
                    else
                    { /* block id: 981 */
                        uint16_t l_2259 = 5UL;
                        uint64_t ***l_2270 = &g_2111;
                        int32_t l_2273 = (-3L);
                        (*l_1909) = (l_1922 = ((l_2018[1] = (*g_846)) != (safe_div_func_float_f_f(l_1864[5], ((safe_div_func_float_f_f((l_2259 = 0x0.6p-1), (safe_sub_func_float_f_f(l_1923, (safe_div_func_float_f_f((((((*g_846) > ((**l_2129) != (safe_mul_func_float_f_f(((safe_add_func_float_f_f((safe_add_func_float_f_f((((*l_2270) = (void*)0) == (**g_534)), (safe_mul_func_float_f_f(((0xA2L > (*g_1649)) , l_2233), l_2014)))), l_2273)) != (*g_846)), l_2274)))) >= 0xF.807F21p-15) <= l_2275) <= l_2273), 0x5.3051C6p+65)))))) != (**l_2129))))));
                    }
                    l_2278--;
                }
                else
                { /* block id: 989 */
                    float l_2285 = 0x8.9D01F9p-37;
                    uint64_t ****l_2293 = &l_1863;
                    uint64_t *****l_2294 = &l_2293;
                    uint64_t l_2304[5];
                    uint16_t l_2305 = 0x2119L;
                    uint8_t **l_2308 = (void*)0;
                    int i;
                    for (i = 0; i < 5; i++)
                        l_2304[i] = 0x6D21CEBE2275C518LL;
                    l_2014 = ((safe_div_func_uint16_t_u_u(((safe_div_func_int64_t_s_s(l_2220, (safe_mul_func_uint16_t_u_u(((((safe_mod_func_int32_t_s_s(((safe_div_func_uint32_t_u_u(((l_2292[4][0] != ((*l_2294) = l_2293)) & ((-9L) != (safe_lshift_func_int16_t_s_u((((**g_144) = ((safe_lshift_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(0x0C7AL, 65535UL)), (((((*l_1893) = ((((safe_sub_func_uint64_t_u_u(l_2274, (((*g_1034) || (((*g_997) , 0xAA99L) && (**g_1269))) || (*g_38)))) < (**l_2129)) || (*g_165)) >= l_1864[4])) & g_177[0]) , &g_846) == &g_846))) ^ l_2007)) < (*g_1034)), g_287)))), 9L)) <= g_2303), (*g_1034))) && l_2200) && 0x5B96AEA1L) | l_2304[0]), (**g_1348))))) == l_1944), (*g_351))) , l_1920);
                    l_2305--;
                    l_2308 = &l_2086;
                }
                l_2346 &= (safe_div_func_uint32_t_u_u(((safe_div_func_uint8_t_u_u((((**g_144) < (safe_add_func_int32_t_s_s(((*g_1649) >= (g_248 &= (*g_1649))), l_1864[4]))) || (((g_812--) && ((*g_1649) = (safe_mul_func_uint16_t_u_u(0x320FL, (l_2345 = ((**g_1348) = ((g_2320 = g_2320) == ((--l_2324[7][0]) , ((((safe_sub_func_uint8_t_u_u(((&l_1862 == ((((safe_div_func_int8_t_s_s(((l_2331 != (((((safe_mul_func_uint16_t_u_u((((safe_add_func_uint32_t_u_u(((((*l_1893) &= ((safe_add_func_int32_t_s_s(((((*l_2086) = 255UL) ^ 0L) | (**l_2129)), (**l_2129))) ^ 1L)) != l_2341) , (**l_2129)), (*g_165))) & (-1L)) != l_2213), l_2342[0][0])) >= 0UL) <= (*g_119)) & (**l_2129)) , &g_2247[2][1][0])) < 3L), g_2343)) != 0xFC14L) && (*g_1034)) , &l_1862)) | 0x39L), 0xE9L)) != l_2341) && (**l_2129)) , l_2344))))))))) != (**g_2131))), 0x92L)) < l_2220), (**l_2129)));
            }
            else
            { /* block id: 1007 */
                uint32_t l_2351 = 0x998F251DL;
                int32_t l_2352[1];
                uint64_t ** const *l_2407 = &g_2111;
                int64_t l_2427 = 5L;
                int i;
                for (i = 0; i < 1; i++)
                    l_2352[i] = 0L;
                for (l_1905 = 0; (l_1905 >= 31); ++l_1905)
                { /* block id: 1010 */
                    if (l_2172)
                        goto lbl_2146;
                    if ((**g_144))
                        break;
                }
                l_2352[0] ^= ((safe_mul_func_uint8_t_u_u((**l_2129), (l_2351 < l_2351))) || (*g_1649));
                for (l_2053 = 18; (l_2053 >= (-1)); l_2053--)
                { /* block id: 1017 */
                    uint16_t l_2375 = 65526UL;
                    int32_t *l_2376[2][3];
                    uint64_t l_2381 = 1UL;
                    int8_t *l_2384 = &g_248;
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_2376[i][j] = &l_1922;
                    }
                    for (l_1905 = 0; (l_1905 == 18); l_1905 = safe_add_func_uint32_t_u_u(l_1905, 7))
                    { /* block id: 1020 */
                        int64_t l_2359 = 0x3863CAD83417B985LL;
                        l_2375 |= ((18446744073709551615UL == (((l_2359 & ((safe_add_func_int32_t_s_s(0x88115BFBL, ((((l_21 >= ((*g_145) == ((l_2023 = ((((*l_1893) = (safe_mul_func_uint8_t_u_u((g_237 = ((*l_2086)++)), (*g_1649)))) && (safe_mod_func_int64_t_s_s((safe_mod_func_int64_t_s_s(((((**l_1870) = (safe_unary_minus_func_uint32_t_u(4294967295UL))) || (l_2352[0] ^ (safe_rshift_func_uint8_t_u_s(((((safe_mul_func_float_f_f(l_2359, l_2359)) == 0x8.0B9380p+64) , l_2018[4]) && (*g_1034)), l_2359)))) && l_2352[0]), 1L)), l_1864[4]))) | l_1922)) > 2UL))) == g_287) != 2L) <= l_2324[2][2]))) != l_21)) != 0x98EBEF6CL) ^ 0xB6F1AA82L)) ^ 1UL);
                    }
                    l_2377 = ((*g_2131) = l_2376[0][0]);
                    (**g_144) &= (l_2378 != (((*g_38) ^= (((-8L) != ((*l_1893) ^= (**l_2129))) >= ((((**l_2129) == (l_2352[0] , (-10L))) , &g_1034) == ((safe_add_func_uint32_t_u_u(l_2381, (((*g_351) = (safe_add_func_int8_t_s_s(((*l_2384) ^= ((*g_1649) = (*g_1649))), l_2351))) , (*g_1034)))) , &g_1034)))) , (void*)0));
                }
                for (g_372 = 0; (g_372 <= 2); g_372 += 1)
                { /* block id: 1039 */
                    int32_t ***l_2385[7][3][5] = {{{&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129},{&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1]},{&l_2129,&l_2129,&l_1890[0][1],&l_2129,&l_2129}},{{&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129},{&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1]},{&l_2129,&l_2129,&l_1890[0][1],&l_2129,&l_2129}},{{&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129},{&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1]},{&l_2129,&l_2129,&l_1890[0][1],&l_2129,&l_2129}},{{&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129},{&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1]},{&l_2129,&l_2129,&l_1890[0][1],&l_2129,&l_2129}},{{&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129},{&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1]},{&l_2129,&l_2129,&l_1890[0][1],&l_2129,&l_2129}},{{&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129},{&l_2129,&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1]},{&l_2129,&l_2129,&l_1890[0][1],&l_2129,&l_2129}},{{&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1],&l_1890[0][1]},{&l_1890[0][1],&l_1890[0][1],&l_1890[0][1],&l_1890[0][1],&l_1890[0][1]},{&l_1890[0][1],&l_1890[0][1],&l_2129,&l_1890[0][1],&l_1890[0][1]}}};
                    uint16_t l_2411[2][5][3] = {{{1UL,65533UL,1UL},{65535UL,65535UL,65535UL},{1UL,65533UL,1UL},{65535UL,65535UL,65535UL},{1UL,65533UL,1UL}},{{65535UL,65535UL,65535UL},{1UL,65533UL,1UL},{65535UL,65535UL,65535UL},{1UL,65533UL,1UL},{65535UL,65535UL,65535UL}}};
                    uint32_t l_2429 = 1UL;
                    uint32_t **l_2442 = &g_433;
                    uint32_t l_2456 = 0x4460FB23L;
                    int16_t l_2457 = 4L;
                    int i, j, k;
                    (**l_2129) = 0xC5BE3610L;
                    l_2386 = (void*)0;
                    for (g_369 = 0; (g_369 <= 0); g_369 += 1)
                    { /* block id: 1044 */
                        const uint64_t *l_2405 = &g_112[4];
                        const uint64_t **l_2404[5];
                        const uint64_t ***l_2403 = &l_2404[3];
                        const uint64_t ****l_2406 = &l_2403;
                        int8_t *l_2408 = &g_2276;
                        int8_t *l_2409 = (void*)0;
                        int8_t *l_2410 = &g_97[3][0][0];
                        uint32_t *l_2420 = &l_2278;
                        int i;
                        for (i = 0; i < 5; i++)
                            l_2404[i] = &l_2405;
                        if (g_177[g_369])
                            break;
                        (*g_144) = (((safe_add_func_int64_t_s_s(0x73E0DE3DC712D17FLL, ((65526UL && ((**g_996) <= ((*l_2086)--))) > ((safe_div_func_uint32_t_u_u((*g_1034), (*g_165))) > (((*l_2410) = ((*l_2408) &= ((safe_sub_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s(((*g_1649) ^= (safe_div_func_int64_t_s_s(((((safe_rshift_func_uint8_t_u_s((safe_rshift_func_uint8_t_u_s((&g_997 == (void*)0), 1)), 3)) , (**l_2129)) , ((*g_766) ^= (((*l_2406) = l_2403) != l_2407))) , (*l_2377)), 0x1C951EFC36D7AFAELL))), 0UL)) || (*g_165)), 1UL)) != 0x6D98D3EDL))) ^ (-2L)))))) || 0x59E64B09L) , (*g_144));
                        l_2023 ^= ((l_2411[0][2][0] ^= (**g_118)) <= ((safe_lshift_func_uint8_t_u_s((safe_add_func_int8_t_s_s(((*l_2408) |= ((safe_lshift_func_uint16_t_u_s(g_2041, 5)) < ((*g_1649) |= ((l_2352[0] , ((((safe_div_func_int32_t_s_s((*g_165), ((--(*l_2420)) , (**g_2131)))) | (safe_lshift_func_uint16_t_u_u(l_2352[0], 5))) , (*g_165)) || (((+(((*g_165) | (~(**g_1348))) < (*l_2377))) <= l_2427) , g_2428))) != (-8L))))), 0x8EL)), g_174)) , l_2351));
                    }
                    l_2429--;
                    for (g_249 = 2; (g_249 >= 0); g_249 -= 1)
                    { /* block id: 1062 */
                        int32_t l_2441[6] = {(-1L),0L,(-1L),(-1L),0L,(-1L)};
                        int32_t l_2459 = (-1L);
                        int i;
                        l_2459 &= (safe_sub_func_uint8_t_u_u((!(safe_sub_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_u(l_2441[2], 0)) | (-5L)), 14)), ((void*)0 == l_2442)))), (0L && ((l_2441[2] >= (safe_add_func_int32_t_s_s(((safe_lshift_func_uint8_t_u_u(((*g_997) , (**g_996)), ((safe_sub_func_uint32_t_u_u((safe_unary_minus_func_uint8_t_u((((((safe_mul_func_float_f_f((safe_mul_func_float_f_f(((safe_sub_func_float_f_f(((((((*l_1909) = (-0x8.Ep+1)) <= g_417[g_372]) , l_2456) <= (**l_2129)) < l_2441[5]), l_2351)) < l_2457), (*g_846))), l_2441[3])) > (-0x1.2p+1)) , 0x76CAL) > g_2276) ^ (*g_1034)))), (*g_1034))) & l_2458))) ^ 1L), 0x041FC8BCL))) || (**g_1269)))));
                    }
                }
            }
            for (g_812 = 10; (g_812 <= 8); g_812 = safe_sub_func_int64_t_s_s(g_812, 7))
            { /* block id: 1070 */
                int32_t l_2475 = (-5L);
                int32_t *l_2480[3][8][3] = {{{&l_2475,&l_2009,&l_2475},{&l_2007,&l_2053,&g_810[5][0]},{&l_2007,&l_2475,&l_2009},{&l_2475,&l_2053,&l_2009},{&l_2009,&l_2009,&g_810[5][0]},{&l_2475,&l_2009,&l_2475},{&l_2007,&l_2053,&g_810[5][0]},{&l_2007,&l_2475,&l_2009}},{{&l_2475,&l_2053,&l_2009},{&l_2009,&l_2009,&g_810[5][0]},{&l_2475,&l_2009,&l_2475},{&l_2007,&l_2053,&g_810[5][0]},{&l_2007,&l_2475,&l_2009},{&l_2475,&l_2053,&l_2009},{&l_2009,&l_2009,&g_810[5][0]},{&l_2475,&l_2009,&l_2475}},{{&l_2007,&l_2053,&g_810[5][0]},{&l_2007,&l_2475,&l_2009},{&l_2475,&l_2053,&l_2009},{&l_2009,&l_2009,&g_810[5][0]},{&l_2475,&l_2009,&l_2475},{&l_2007,&l_2053,&g_810[5][0]},{&l_2007,&l_2475,&l_2009},{&l_2475,&l_2053,&l_2009}}};
                int32_t l_2481 = 0xC3BEF55DL;
                int i, j, k;
                l_2018[2] ^= (((l_2482[2][0][2] &= (safe_sub_func_int16_t_s_s(((safe_add_func_int64_t_s_s((safe_div_func_uint16_t_u_u(((*g_38) & (safe_sub_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u((l_2473 & (l_2474 , (((((l_2475 &= (*g_1034)) , ((safe_sub_func_uint8_t_u_u((((((*l_2377) <= l_2475) == 65530UL) != ((safe_div_func_uint8_t_u_u(((*g_765) != l_2480[0][6][2]), (**l_2129))) & (**l_2129))) != 5L), (*g_1649))) >= 0x36E99583381FA6F4LL)) >= 0x96L) & l_2481) < (**g_1269)))), g_669)), 255UL))), g_101[1])), 0xD28AF00052B10B3FLL)) ^ (**g_352)), 0xCE7FL))) || (-1L)) < l_2481);
            }
        }
        else
        { /* block id: 1075 */
            uint16_t l_2483 = 65530UL;
            int16_t *****l_2484 = (void*)0;
            int32_t l_2503 = 0xF3DD5893L;
            int32_t l_2505[7][7][1] = {{{0x7433ED6FL},{1L},{1L},{1L},{1L},{0x7433ED6FL},{1L}},{{1L},{1L},{1L},{0x7433ED6FL},{1L},{1L},{1L}},{{1L},{0x7433ED6FL},{1L},{1L},{1L},{1L},{0x7433ED6FL}},{{1L},{1L},{1L},{1L},{0x7433ED6FL},{1L},{1L}},{{1L},{1L},{0x7433ED6FL},{1L},{1L},{1L},{1L}},{{0x7433ED6FL},{1L},{1L},{1L},{1L},{0x7433ED6FL},{1L}},{{1L},{1L},{1L},{0x7433ED6FL},{1L},{1L},{1L}}};
            uint32_t ****l_2513 = (void*)0;
            int i, j, k;
            for (g_168 = 0; (g_168 <= 1); g_168 += 1)
            { /* block id: 1078 */
                uint32_t l_2504 = 0xBA980638L;
                int64_t l_2515 = 0xC4D0E0DA9697C724LL;
                (**l_2386) &= (((l_2483 , l_2484) != (void*)0) & (*g_997));
                for (l_1920 = 0; (l_1920 <= 1); l_1920 += 1)
                { /* block id: 1082 */
                    uint32_t ****l_2486 = &g_1381;
                    uint32_t *****l_2485 = &l_2486;
                    int32_t l_2492[2][10] = {{0xEC74D594L,0xB3C7C1D4L,(-1L),2L,(-1L),0xB3C7C1D4L,0xEC74D594L,0xEC74D594L,0xB3C7C1D4L,(-1L)},{0xB3C7C1D4L,0xEC74D594L,0xEC74D594L,0xB3C7C1D4L,(-1L),2L,(-1L),0xB3C7C1D4L,0xEC74D594L,0xEC74D594L}};
                    uint8_t l_2514 = 255UL;
                    int i, j;
                    (*l_2485) = (void*)0;
                    l_2505[4][3][0] = (l_2492[0][1] = (safe_mod_func_uint8_t_u_u((safe_unary_minus_func_uint8_t_u(((--(*g_1034)) > ((((l_2492[0][1] ^ (((!(&g_1382 != l_2144)) < (safe_sub_func_int8_t_s_s(0x7EL, (0x6992L > ((*g_1270) ^ (((safe_add_func_int64_t_s_s((((l_2483 != (safe_rshift_func_int8_t_s_s((safe_unary_minus_func_uint32_t_u((((safe_lshift_func_uint16_t_u_s(g_1073[2][1], ((g_369 <= g_1343[0][1]) <= (-1L)))) | l_2492[0][1]) , g_42))), (*g_1649)))) == l_2503) || g_450), l_2504)) , l_2504) < l_2483)))))) , 5L)) != 251UL) | l_2492[0][1]) == (-7L))))), 0xCDL)));
                    (**l_2386) &= (safe_lshift_func_int8_t_s_u((+((*l_2377) = ((((+(*g_1649)) <= (0x03ACCD0FL || ((+6UL) && (((((*g_1034) < (safe_mod_func_uint32_t_u_u((*g_1034), (4294967295UL & ((l_2492[0][1] >= (l_2513 != (void*)0)) > l_2503))))) | l_2514) < (*g_1034)) || l_2483)))) | l_2504) , 0L))), (*g_997)));
                    for (g_12 = 1; (g_12 >= 0); g_12 -= 1)
                    { /* block id: 1091 */
                        uint8_t l_2516 = 7UL;
                        if (l_2515)
                            break;
                        return l_2516;
                    }
                }
            }
        }
    }
    else
    { /* block id: 1098 */
        const int64_t l_2519 = 0xD70EB4318D619F04LL;
        int32_t l_2527 = 0xF73067ABL;
        uint32_t ****l_2528[7];
        int i;
        for (i = 0; i < 7; i++)
            l_2528[i] = &g_1381;
        g_2529 ^= (safe_mod_func_int64_t_s_s((1L == (((&g_905[1] != ((l_2519 ^ ((safe_sub_func_int16_t_s_s((((safe_mod_func_uint64_t_u_u(0xC49A1F188FC2A810LL, (*l_2377))) , ((1UL & (safe_rshift_func_int16_t_s_s((l_2519 != (((*g_351) = (l_2527 = ((*l_2377) , (+(2UL == 0x87L))))) > 6L)), 15))) ^ 0x5932L)) > 0UL), (**l_2386))) || (*l_2377))) , l_2528[1])) | (*g_1034)) ^ (*g_1034))), 0x3AB39076B69D4A09LL));
        return (*g_766);
    }
    (*g_144) = (*g_144);
    return (**g_765);
}


/* ------------------------------------------ */
/* 
 * reads : g_765 g_766 g_161
 * writes:
 */
static int32_t  func_7(uint16_t  p_8, int64_t  p_9, uint16_t  p_10, const int16_t  p_11)
{ /* block id: 364 */
    uint64_t l_836 = 0UL;
    float *l_844[8][4][6] = {{{&g_461[2][1][2],&g_175[0],&g_417[2],&g_417[2],&g_417[1],&g_461[2][1][2]},{&g_417[2],&g_175[1],&g_417[2],&g_417[2],&g_175[1],&g_417[2]},{&g_461[2][1][2],&g_417[1],&g_417[2],&g_417[2],&g_175[0],&g_461[2][1][2]},{(void*)0,&g_417[1],&g_417[2],(void*)0,&g_175[1],(void*)0}},{{(void*)0,&g_175[1],(void*)0,&g_417[2],&g_417[1],(void*)0},{&g_461[2][1][2],&g_175[0],&g_417[2],&g_417[2],&g_417[1],&g_461[2][1][2]},{&g_417[2],&g_175[1],&g_417[2],&g_417[2],&g_175[1],&g_417[2]},{&g_461[2][1][2],&g_417[1],&g_417[2],&g_417[2],&g_175[0],&g_461[2][1][2]}},{{(void*)0,&g_417[1],&g_417[2],(void*)0,&g_175[1],(void*)0},{(void*)0,&g_175[1],(void*)0,&g_417[2],&g_417[1],(void*)0},{&g_461[2][1][2],&g_175[0],&g_417[2],&g_417[2],&g_417[1],&g_461[2][1][2]},{&g_417[2],&g_175[1],&g_417[2],&g_417[2],&g_175[1],&g_417[2]}},{{&g_461[2][1][2],&g_417[1],&g_417[2],&g_417[2],&g_175[0],&g_461[2][1][2]},{(void*)0,&g_417[1],&g_417[2],(void*)0,&g_175[1],(void*)0},{(void*)0,&g_175[1],(void*)0,&g_417[2],&g_417[1],(void*)0},{&g_461[2][1][2],&g_175[0],&g_417[2],&g_417[2],&g_417[1],&g_461[2][1][2]}},{{&g_417[2],&g_175[1],&g_417[2],&g_417[2],&g_175[1],&g_417[2]},{&g_461[2][1][2],&g_417[1],&g_417[2],&g_417[2],&g_175[0],&g_461[2][1][2]},{(void*)0,&g_417[1],&g_417[2],(void*)0,&g_175[1],(void*)0},{(void*)0,&g_175[1],(void*)0,&g_417[2],&g_417[1],(void*)0}},{{&g_461[2][1][2],&g_175[0],&g_417[2],&g_417[2],&g_417[1],&g_461[2][1][2]},{&g_417[2],&g_175[1],&g_417[2],&g_417[2],&g_175[1],&g_417[2]},{&g_461[2][1][2],&g_417[1],&g_417[2],&g_417[2],&g_175[0],&g_461[2][1][2]},{(void*)0,&g_417[1],&g_417[2],(void*)0,&g_175[1],(void*)0}},{{(void*)0,&g_175[1],(void*)0,&g_417[2],&g_417[1],(void*)0},{&g_461[2][1][2],&g_175[0],&g_417[2],&g_417[2],&g_417[1],&g_461[2][1][2]},{&g_417[2],&g_175[1],&g_417[2],&g_417[2],&g_175[1],&g_417[2]},{&g_461[2][1][2],&g_417[1],&g_417[2],&g_417[2],&g_175[0],&g_461[2][1][2]}},{{(void*)0,&g_417[1],&g_417[2],(void*)0,&g_175[1],(void*)0},{(void*)0,&g_175[1],(void*)0,&g_417[2],&g_417[1],(void*)0},{&g_461[2][1][2],&g_175[0],&g_417[2],&g_417[2],&g_417[1],&g_461[2][1][2]},{&g_417[2],&g_175[1],&g_417[2],&g_417[2],&g_175[1],&g_417[2]}}};
    float **l_843 = &l_844[5][1][3];
    const float ***l_847 = (void*)0;
    const float **l_848 = &g_846;
    uint32_t **l_854 = &g_433;
    uint32_t ***l_853 = &l_854;
    int32_t l_855 = 0xA0588A8AL;
    int32_t **l_856 = &g_165;
    uint16_t *l_863 = &g_168;
    int32_t l_885 = 0x53BFF9F7L;
    uint64_t *l_886[3][6][8] = {{{&l_836,(void*)0,&l_836,(void*)0,&l_836,&g_112[3],&g_112[1],&g_112[0]},{&g_112[1],(void*)0,&g_450,(void*)0,&l_836,&l_836,(void*)0,(void*)0},{&g_450,&l_836,&g_450,&l_836,&g_328,(void*)0,&g_112[1],&g_450},{&l_836,(void*)0,&l_836,&g_112[0],(void*)0,&g_450,&g_328,(void*)0},{&g_112[0],&g_328,&l_836,&g_450,&g_450,&l_836,&g_328,&g_112[0]},{(void*)0,(void*)0,(void*)0,&g_450,(void*)0,&g_112[1],&g_450,&g_328}},{{&g_112[1],&l_836,&g_112[1],&g_450,&g_450,&g_112[1],&l_836,(void*)0},{&l_836,(void*)0,&g_450,&g_450,&g_112[0],&l_836,&g_112[1],&g_450},{(void*)0,&g_328,&g_328,(void*)0,(void*)0,&g_450,(void*)0,(void*)0},{&g_450,(void*)0,&g_450,&g_112[1],&g_450,(void*)0,&g_112[1],&g_450},{(void*)0,&l_836,&l_836,&g_450,&g_450,&l_836,&g_450,(void*)0},{(void*)0,(void*)0,&g_112[1],&l_836,&g_450,&g_112[3],(void*)0,&l_836}},{{&g_450,(void*)0,&l_836,&l_836,(void*)0,&g_450,&g_450,&g_450},{(void*)0,&g_112[0],&g_450,&g_450,&g_112[0],(void*)0,&g_328,(void*)0},{&l_836,&l_836,(void*)0,(void*)0,&g_450,&l_836,&g_112[1],&l_836},{&g_112[1],&g_450,&g_328,(void*)0,(void*)0,&l_836,&l_836,(void*)0},{(void*)0,(void*)0,&g_450,&g_450,&g_450,&l_836,&g_112[3],&g_450},{&g_112[0],&l_836,&g_450,&l_836,(void*)0,&l_836,&g_450,&l_836}}};
    int32_t l_909 = (-3L);
    uint8_t *l_913 = &g_135[0][1];
    int32_t l_932 = 0x5AD0B53AL;
    int16_t **l_977 = &g_351;
    uint32_t l_995 = 0x6F5BB28CL;
    uint8_t l_1007 = 2UL;
    int32_t l_1092 = 0xF0C275F0L;
    int32_t l_1112 = 0x3BFEEB27L;
    int32_t l_1113 = 0x1DDEB5FAL;
    int32_t l_1121 = 0xC4F57EDFL;
    int32_t l_1124 = (-8L);
    uint16_t **l_1268 = &l_863;
    int16_t ***l_1315 = &g_350[4][1][1];
    int16_t ****l_1314 = &l_1315;
    int64_t l_1320 = 0L;
    uint16_t l_1388[5] = {1UL,1UL,1UL,1UL,1UL};
    int32_t l_1398 = 0xC04CF8C1L;
    uint64_t **l_1436 = &l_886[1][0][2];
    uint64_t ***l_1435 = &l_1436;
    uint64_t ****l_1434 = &l_1435;
    uint64_t l_1553 = 0x633F7EE2832FBAB4LL;
    uint32_t l_1555 = 0xABCF6FC0L;
    int32_t l_1572[3];
    int64_t l_1573 = 0L;
    uint8_t l_1622 = 0UL;
    uint16_t l_1685 = 0x1A37L;
    float l_1730 = (-0x5.8p+1);
    int8_t **l_1749[1][2][6] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
    uint32_t l_1791 = 0xD74C2F53L;
    int64_t **l_1843 = &g_38;
    int64_t ***l_1844 = &l_1843;
    int64_t **l_1845 = &g_38;
    int32_t *l_1852 = &g_42;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1572[i] = 0L;
    return (**g_765);
}


/* ------------------------------------------ */
/* 
 * reads : g_414 g_135 g_351 g_177 g_751 g_42
 * writes: g_414 g_372 g_166 g_42
 */
static uint32_t  func_17(uint32_t  p_18, int64_t * p_19, float  p_20)
{ /* block id: 354 */
    uint8_t *l_816 = &g_414[0];
    int32_t l_821[7][4][1] = {{{(-9L)},{0L},{0xA7F1EFE3L},{0L}},{{(-9L)},{3L},{0L},{(-7L)}},{{(-7L)},{0L},{3L},{(-9L)}},{{0L},{0xA7F1EFE3L},{0L},{(-9L)}},{{3L},{0L},{(-7L)},{(-7L)}},{{0L},{3L},{(-9L)},{0L}},{{0xA7F1EFE3L},{0L},{(-9L)},{3L}}};
    uint32_t *l_826 = &g_372;
    float l_827 = 0x7.26C9F5p-40;
    int i, j, k;
    g_166 = (((++(*l_816)) == ((((-1L) & p_18) & ((safe_rshift_func_uint16_t_u_s(((void*)0 != &g_535), 3)) < (l_821[5][3][0] , (safe_mul_func_int16_t_s_s((0L & (l_821[5][3][0] || ((((*l_826) = ((g_135[5][0] & ((safe_mul_func_int16_t_s_s((*g_351), 0L)) , l_821[0][3][0])) | g_751)) > l_821[0][1][0]) & g_177[0]))), l_821[5][3][0]))))) > 0x9FL)) <= p_18);
    for (g_42 = 0; (g_42 == 12); ++g_42)
    { /* block id: 360 */
        return l_821[1][2][0];
    }
    return l_821[5][3][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_44 g_42 g_812
 * writes: g_47 g_44 g_45 g_166 g_369 g_165 g_669 g_42 g_175 g_812
 */
static int16_t  func_32(uint32_t  p_33, uint32_t  p_34, int64_t * p_35, int64_t * p_36, int16_t  p_37)
{ /* block id: 2 */
    int16_t l_40 = 0xDF86L;
    int32_t *l_41 = &g_42;
    int32_t *l_43[3][6] = {{&g_12,&g_12,&g_12,&g_12,&g_12,&g_12},{&g_42,&g_12,&g_42,&g_12,&g_42,&g_12},{&g_12,&g_12,&g_12,&g_12,&g_12,&g_12}};
    float l_46 = (-0x3.7p-1);
    uint32_t l_561 = 1UL;
    int64_t l_668[10] = {0x47B6FDB0506BF248LL,0x1BCFEB9623361F96LL,0x47B6FDB0506BF248LL,0xAB3C352F68489E27LL,0xAB3C352F68489E27LL,0x47B6FDB0506BF248LL,0x1BCFEB9623361F96LL,0x47B6FDB0506BF248LL,0xAB3C352F68489E27LL,0xAB3C352F68489E27LL};
    uint64_t * const l_758 = &g_450;
    int32_t **l_801 = (void*)0;
    int32_t **l_802 = &g_165;
    int i, j;
    ++g_47;
    for (g_44 = 0; (g_44 <= (-10)); g_44 = safe_sub_func_uint64_t_u_u(g_44, 9))
    { /* block id: 6 */
        int32_t *l_66 = &g_12;
        int32_t l_563 = (-1L);
        uint32_t l_588 = 0x9CD9F86EL;
        const uint16_t * const *l_608 = (void*)0;
        int16_t **l_627 = &g_351;
        int32_t l_660[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int32_t *l_724 = (void*)0;
        int i;
        if (g_47)
            break;
        for (g_45 = 0; (g_45 < 15); g_45++)
        { /* block id: 10 */
            const int64_t *l_80 = &g_39;
            int32_t l_89 = 6L;
            uint64_t *l_486 = (void*)0;
            uint64_t *l_487 = (void*)0;
            uint64_t *l_488 = &g_450;
            uint64_t *l_489 = &g_328;
            uint64_t *l_490[1];
            uint64_t *l_491 = &g_112[0];
            int32_t l_566[1][7][1] = {{{1L},{0xAB147159L},{1L},{1L},{0xAB147159L},{1L},{1L}}};
            uint16_t **l_609 = (void*)0;
            const uint64_t l_610 = 0x2878E20FFFFA6E6CLL;
            uint16_t *l_614 = &g_168;
            float l_619 = 0xE.9D2E33p+60;
            const uint64_t ***l_654 = (void*)0;
            const uint64_t ****l_653 = &l_654;
            uint64_t l_662[5] = {0x7C357E5D2B0CC41ELL,0x7C357E5D2B0CC41ELL,0x7C357E5D2B0CC41ELL,0x7C357E5D2B0CC41ELL,0x7C357E5D2B0CC41ELL};
            int32_t l_768 = 0x6B2B0678L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_490[i] = &g_112[0];
        }
        for (g_166 = 4; (g_166 <= 18); g_166 = safe_add_func_int8_t_s_s(g_166, 3))
        { /* block id: 320 */
            int32_t *l_793 = (void*)0;
            if (p_34)
                break;
            for (g_369 = 0; (g_369 <= 1); g_369 += 1)
            { /* block id: 324 */
                int32_t **l_794 = (void*)0;
                int32_t **l_795[7][10][2] = {{{&l_43[1][2],(void*)0},{&l_66,(void*)0},{&l_724,&l_793},{(void*)0,&l_793},{&l_724,(void*)0},{&l_66,(void*)0},{&l_43[1][2],&l_43[1][2]},{(void*)0,(void*)0},{&l_41,&l_43[0][2]},{&l_724,&l_43[1][3]}},{{(void*)0,&l_724},{&l_66,&l_724},{&l_66,&l_724},{(void*)0,&l_43[1][3]},{&l_724,&l_43[0][2]},{&l_41,(void*)0},{(void*)0,&l_43[1][2]},{&l_43[1][2],(void*)0},{&l_66,(void*)0},{&l_724,&l_793}},{{(void*)0,&l_793},{&l_724,(void*)0},{&l_66,(void*)0},{&l_43[1][2],&l_43[1][2]},{(void*)0,(void*)0},{&l_41,&l_43[0][2]},{&l_724,&l_43[1][3]},{(void*)0,&l_724},{&l_66,&l_724},{&l_66,&l_724}},{{(void*)0,&l_43[1][3]},{&l_724,&l_43[0][2]},{&l_41,(void*)0},{(void*)0,&l_43[1][2]},{&l_43[1][2],(void*)0},{&l_66,(void*)0},{&l_724,&l_793},{(void*)0,&l_793},{&l_724,(void*)0},{&l_66,(void*)0}},{{&l_43[1][2],&l_43[1][2]},{(void*)0,(void*)0},{&l_41,&l_43[0][2]},{&l_724,&l_43[1][3]},{(void*)0,&l_724},{&l_66,&l_724},{&l_66,&l_724},{(void*)0,&l_43[1][3]},{&l_724,&l_43[0][2]},{&l_41,(void*)0}},{{(void*)0,&l_43[1][2]},{&l_43[1][2],(void*)0},{&l_66,(void*)0},{&l_724,&l_793},{(void*)0,&l_793},{&l_724,(void*)0},{&l_66,(void*)0},{&l_43[1][2],&l_43[1][2]},{(void*)0,(void*)0},{&l_41,&l_43[0][2]}},{{&l_724,&l_43[1][3]},{(void*)0,&l_724},{&l_66,&l_724},{&l_66,&l_724},{(void*)0,&l_43[1][3]},{&l_724,&l_43[0][2]},{&l_41,(void*)0},{(void*)0,&l_43[1][2]},{&l_43[1][2],(void*)0},{&l_66,(void*)0}}};
                int i, j, k;
                g_165 = l_793;
                l_66 = l_793;
                for (g_669 = 0; (g_669 <= 1); g_669 += 1)
                { /* block id: 329 */
                    l_41 = &g_368;
                }
                if (p_34)
                    break;
            }
            for (g_42 = (-26); (g_42 <= 9); ++g_42)
            { /* block id: 336 */
                float *l_800 = &g_175[0];
                (*l_800) = (safe_div_func_float_f_f(0x1.Dp-1, p_37));
                if (p_34)
                    break;
            }
        }
    }
    if ((((*l_802) = (void*)0) != &g_464))
    { /* block id: 343 */
        uint8_t l_805[4] = {0xB4L,0xB4L,0xB4L,0xB4L};
        int i;
        for (g_42 = 0; (g_42 == (-12)); g_42 = safe_sub_func_uint8_t_u_u(g_42, 3))
        { /* block id: 346 */
            l_805[0]++;
        }
    }
    else
    { /* block id: 349 */
        int32_t l_808[7] = {0x6A035C70L,0x04759BE9L,0x04759BE9L,0x5348D72DL,0x04759BE9L,0x04759BE9L,0x5348D72DL};
        int i;
        ++g_812;
    }
    return p_34;
}


/* ------------------------------------------ */
/* 
 * reads : g_328 g_351 g_165 g_464 g_450 g_534 g_372
 * writes: g_146 g_417 g_177 g_166 g_464 g_450 g_179 g_165 g_534 g_372
 */
static int32_t  func_54(int32_t  p_55, const uint64_t  p_56)
{ /* block id: 183 */
    uint16_t l_496 = 0UL;
    int64_t l_513 = 7L;
    int32_t *l_514[10][4] = {{&g_161,&g_161,&g_161,&g_369},{&g_369,&g_161,(void*)0,&g_161},{(void*)0,&g_369,(void*)0,&g_369},{&g_369,&g_161,&g_161,&g_369},{&g_161,(void*)0,(void*)0,&g_249},{(void*)0,&g_249,&g_161,&g_369},{(void*)0,&g_369,&g_369,&g_369},{&g_369,&g_249,(void*)0,&g_249},{(void*)0,(void*)0,&g_369,&g_369},{&g_161,&g_161,&g_369,&g_369}};
    int32_t l_515 = (-1L);
    uint32_t *l_516 = &g_250;
    uint32_t *l_517 = &g_77[0][0];
    int8_t *l_518[8][5][4] = {{{&g_97[1][0][3],(void*)0,&g_97[4][0][1],(void*)0},{&g_97[1][0][3],&g_146,&g_146,&g_97[1][0][3]},{(void*)0,(void*)0,&g_97[2][0][2],&g_146},{(void*)0,(void*)0,&g_97[7][0][0],(void*)0},{(void*)0,(void*)0,&g_146,(void*)0}},{{&g_248,(void*)0,&g_248,&g_146},{&g_97[1][0][3],(void*)0,&g_97[0][0][0],&g_97[1][0][3]},{(void*)0,&g_146,&g_97[2][0][2],(void*)0},{&g_146,(void*)0,&g_97[2][0][2],&g_97[2][0][2]},{(void*)0,(void*)0,&g_97[0][0][0],(void*)0}},{{&g_97[1][0][3],&g_248,&g_248,(void*)0},{&g_248,(void*)0,&g_146,&g_248},{(void*)0,(void*)0,&g_97[7][0][0],(void*)0},{(void*)0,&g_248,&g_97[2][0][2],(void*)0},{(void*)0,(void*)0,&g_146,&g_97[2][0][2]}},{{&g_97[1][0][3],(void*)0,&g_97[4][0][1],(void*)0},{&g_97[1][0][3],&g_146,&g_146,&g_97[1][0][3]},{(void*)0,(void*)0,&g_97[2][0][2],&g_146},{(void*)0,(void*)0,&g_97[7][0][0],(void*)0},{(void*)0,(void*)0,&g_146,(void*)0}},{{&g_248,(void*)0,&g_248,&g_146},{&g_97[1][0][3],(void*)0,&g_97[0][0][0],&g_97[1][0][3]},{(void*)0,&g_146,&g_97[2][0][2],(void*)0},{&g_146,(void*)0,&g_97[2][0][2],&g_97[2][0][2]},{(void*)0,(void*)0,&g_97[0][0][0],(void*)0}},{{&g_97[1][0][3],&g_248,&g_248,(void*)0},{&g_248,(void*)0,&g_146,&g_248},{(void*)0,(void*)0,&g_97[7][0][0],(void*)0},{(void*)0,&g_248,&g_97[2][0][2],(void*)0},{(void*)0,(void*)0,&g_146,&g_97[2][0][2]}},{{&g_97[1][0][3],(void*)0,&g_97[4][0][1],(void*)0},{&g_97[1][0][3],&g_146,&g_146,&g_97[1][0][3]},{(void*)0,(void*)0,&g_97[2][0][2],&g_146},{(void*)0,(void*)0,&g_97[7][0][0],(void*)0},{(void*)0,(void*)0,&g_146,(void*)0}},{{&g_248,(void*)0,&g_248,&g_146},{&g_97[2][0][2],&g_248,&g_97[1][0][3],&g_97[2][0][2]},{&g_146,&g_97[4][0][1],&g_97[4][0][3],&g_248},{&g_97[4][0][1],(void*)0,&g_97[4][0][3],&g_97[4][0][3]},{&g_146,&g_146,&g_97[1][0][3],&g_248}}};
    int32_t *l_519 = (void*)0;
    int32_t *l_520 = &g_464;
    int i, j, k;
    for (g_146 = 0; g_146 < 3; g_146 += 1)
    {
        g_417[g_146] = (-0x1.2p-1);
    }
    (*l_520) |= ((safe_mul_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(l_496, p_56)), 0x6EL)) || (safe_rshift_func_uint16_t_u_u(((g_146 = ((g_328 | (safe_rshift_func_int8_t_s_u((safe_mul_func_uint16_t_u_u((l_496 | (safe_div_func_int16_t_s_s((((safe_sub_func_int32_t_s_s(((*g_165) = ((safe_div_func_int8_t_s_s((((l_515 &= ((safe_rshift_func_uint16_t_u_s((safe_mod_func_int32_t_s_s((((*g_351) = (-9L)) <= 0x61F8L), 1UL)), l_513)) & 65528UL)) , l_516) != l_517), 0xCEL)) , 0x994FB0CAL)), 0xE1BF947EL)) > p_55) != l_496), l_513))), l_496)), 7))) <= 246UL)) != l_513), 2)));
    if ((*l_520))
    { /* block id: 190 */
        int32_t *l_521[9][4] = {{&g_166,&g_12,&g_464,&g_166},{(void*)0,&g_241,&l_515,(void*)0},{&g_464,&g_166,(void*)0,(void*)0},{&g_464,&g_464,&l_515,&g_368},{(void*)0,(void*)0,&g_464,&g_241},{&g_166,(void*)0,&g_464,&g_464},{&g_166,(void*)0,&g_166,&g_241},{(void*)0,(void*)0,&g_368,&g_368},{&g_368,&g_464,&g_166,(void*)0}};
        int32_t **l_522 = &l_521[2][0];
        int i, j;
        (*l_522) = l_521[2][2];
        (*g_165) = ((*l_520) = 0x416F6E3BL);
    }
    else
    { /* block id: 194 */
        uint32_t l_531 = 0x8BFF18C8L;
        for (g_450 = 0; (g_450 >= 44); g_450 = safe_add_func_int32_t_s_s(g_450, 7))
        { /* block id: 197 */
            uint64_t ** volatile ***l_537 = &g_534;
            for (g_179 = 0; (g_179 == 6); ++g_179)
            { /* block id: 200 */
                int32_t *l_527 = &g_241;
                int32_t **l_528 = (void*)0;
                int32_t **l_529 = &g_165;
                int32_t *l_530[9][3] = {{&g_368,&l_515,&g_368},{&g_464,&g_464,&g_464},{&g_241,&l_515,&g_241},{&g_464,&g_464,&g_464},{&g_368,&l_515,&g_368},{&g_464,&g_464,&g_464},{&g_241,&l_515,&g_241},{&g_464,&g_464,&g_464},{&g_368,&l_515,&g_368}};
                int i, j;
                (*l_529) = (l_527 = &l_515);
                l_531--;
            }
            (*l_537) = g_534;
            for (g_372 = 0; (g_372 == 33); g_372++)
            { /* block id: 208 */
                return p_56;
            }
        }
    }
    return (*l_520);
}


/* ------------------------------------------ */
/* 
 * reads : g_77 g_42
 * writes: g_77
 */
static uint64_t  func_63(int32_t * p_64, int32_t * p_65)
{ /* block id: 11 */
    int16_t l_67 = 0xC3D3L;
    int32_t *l_68 = &g_42;
    int32_t *l_69 = &g_42;
    int32_t l_70 = 0x91CD3C6EL;
    int32_t *l_71 = &l_70;
    int32_t *l_72 = &l_70;
    int32_t *l_73 = &l_70;
    int32_t *l_74 = &l_70;
    int32_t *l_75 = &l_70;
    int32_t *l_76[5] = {&l_70,&l_70,&l_70,&l_70,&l_70};
    int i;
    g_77[0][0]--;
    return (*l_68);
}


/* ------------------------------------------ */
/* 
 * reads : g_47
 * writes: g_47
 */
static uint16_t  func_83(float  p_84, float  p_85, uint32_t  p_86, int64_t * p_87, float  p_88)
{ /* block id: 14 */
    int8_t l_457 = 1L;
    int64_t l_473 = (-3L);
    int32_t *l_480 = &g_241;
    int32_t *l_481 = (void*)0;
    int32_t *l_482[5][2] = {{&g_12,&g_464},{&g_12,&g_464},{&g_12,&g_464},{&g_12,&g_464},{&g_12,&g_464}};
    uint32_t l_483 = 0xC6677179L;
    int i, j;
    for (g_47 = 5; (g_47 != 40); g_47 = safe_add_func_uint64_t_u_u(g_47, 7))
    { /* block id: 17 */
        int8_t *l_96 = &g_97[3][0][0];
        uint32_t *l_100 = &g_101[1];
        int32_t l_443 = 0L;
        int32_t l_448[10] = {(-1L),(-9L),(-1L),(-9L),(-1L),(-9L),(-1L),(-9L),(-1L),(-9L)};
        uint64_t *l_449 = &g_450;
        uint32_t *l_458[10][9] = {{(void*)0,(void*)0,&g_459,&g_459,(void*)0,&g_459,&g_459,(void*)0,(void*)0},{&g_459,(void*)0,(void*)0,&g_459,(void*)0,(void*)0,&g_459,&g_459,(void*)0},{&g_459,&g_459,&g_459,&g_459,&g_459,(void*)0,(void*)0,&g_459,&g_459},{&g_459,(void*)0,&g_459,&g_459,&g_459,&g_459,&g_459,&g_459,(void*)0},{(void*)0,&g_459,(void*)0,&g_459,&g_459,(void*)0,&g_459,(void*)0,&g_459},{(void*)0,&g_459,&g_459,&g_459,&g_459,(void*)0,&g_459,(void*)0,&g_459},{&g_459,&g_459,&g_459,&g_459,(void*)0,&g_459,(void*)0,&g_459,&g_459},{(void*)0,(void*)0,&g_459,&g_459,&g_459,&g_459,&g_459,(void*)0,(void*)0},{&g_459,&g_459,(void*)0,&g_459,(void*)0,&g_459,&g_459,&g_459,&g_459},{&g_459,(void*)0,&g_459,(void*)0,&g_459,&g_459,&g_459,&g_459,&g_459}};
        float *l_460[1][9][5] = {{{&g_461[2][1][2],&g_461[2][6][0],&g_461[2][1][2],&g_461[0][3][2],&g_461[0][8][3]},{(void*)0,&g_461[2][7][2],&g_461[2][7][2],(void*)0,&g_461[2][7][2]},{&g_461[0][8][3],&g_461[2][6][0],&g_461[2][2][4],&g_461[2][6][0],&g_461[0][8][3]},{&g_461[2][7][2],(void*)0,&g_461[2][7][2],&g_461[2][7][2],(void*)0},{&g_461[0][8][3],&g_461[0][3][2],&g_461[2][1][2],&g_461[2][6][0],&g_461[2][1][2]},{(void*)0,(void*)0,&g_461[2][1][2],(void*)0,(void*)0},{&g_461[2][1][2],&g_461[2][6][0],&g_461[2][1][2],&g_461[0][3][2],&g_461[0][8][3]},{(void*)0,&g_461[2][7][2],&g_461[2][7][2],(void*)0,&g_461[2][7][2]},{&g_461[0][8][3],&g_461[2][6][0],&g_461[2][2][4],&g_461[2][6][0],&g_461[0][8][3]}}};
        int32_t l_462 = 0x064D76B6L;
        int32_t *l_463 = &g_464;
        int64_t **l_476 = &g_38;
        int64_t *l_477 = &g_233;
        int32_t l_478 = 0L;
        uint32_t l_479 = 0xE8F233E4L;
        int i, j, k;
    }
    l_483++;
    return p_86;
}


/* ------------------------------------------ */
/* 
 * reads : g_101 g_112 g_118 g_38 g_97 g_44 g_39 g_144 g_146 g_161 g_168 g_165 g_12 g_45 g_135 g_166 g_177 g_237 g_179 g_250 g_233 g_249 g_287 g_42 g_328 g_190 g_241 g_351 g_372 g_47 g_310 g_414
 * writes: g_112 g_118 g_39 g_135 g_146 g_165 g_168 g_174 g_177 g_166 g_237 g_179 g_250 g_233 g_287 g_241 g_175 g_310 g_328 g_350 g_352 g_97 g_372 g_368 g_417 g_433
 */
static uint8_t  func_98(uint32_t  p_99)
{ /* block id: 21 */
    uint64_t *l_111[10] = {&g_112[3],&g_112[0],&g_112[3],&g_112[3],&g_112[0],&g_112[3],&g_112[3],&g_112[0],&g_112[3],&g_112[3]};
    const int32_t l_117 = 0xE1F63711L;
    int32_t l_125[3][3] = {{0x2762B6CBL,0x2762B6CBL,0x79EA335EL},{0x2762B6CBL,0x2762B6CBL,0x79EA335EL},{0x2762B6CBL,0x2762B6CBL,0x79EA335EL}};
    uint8_t *l_134 = &g_135[0][1];
    uint64_t l_136[5][4] = {{4UL,0x28A62AF0730FA287LL,4UL,0x28A62AF0730FA287LL},{4UL,0x28A62AF0730FA287LL,4UL,0x28A62AF0730FA287LL},{4UL,0x28A62AF0730FA287LL,4UL,0x28A62AF0730FA287LL},{4UL,0x28A62AF0730FA287LL,4UL,0x28A62AF0730FA287LL},{4UL,0x28A62AF0730FA287LL,4UL,0x28A62AF0730FA287LL}};
    int32_t *l_137 = &g_12;
    int32_t *l_393[5];
    int32_t l_406 = 0xECDA24C8L;
    float *l_413 = &g_175[0];
    int16_t *l_415 = &g_179;
    float *l_416 = &g_417[2];
    uint32_t *l_428 = &g_372;
    uint32_t *l_432 = (void*)0;
    uint32_t **l_431[4] = {&l_432,&l_432,&l_432,&l_432};
    int16_t *l_439[4][3][9] = {{{(void*)0,&g_440,&g_440,&g_440,(void*)0,&g_45,(void*)0,&g_440,&g_440},{&g_45,&g_440,&g_45,&g_45,&g_45,&g_45,&g_440,&g_45,&g_440},{&g_45,&g_440,(void*)0,&g_440,&g_45,&g_45,&g_45,&g_440,(void*)0}},{{&g_45,&g_45,&g_45,&g_440,&g_45,&g_440,&g_440,&g_440,&g_440},{(void*)0,&g_440,&g_440,&g_440,(void*)0,&g_45,(void*)0,&g_440,&g_440},{&g_45,&g_440,&g_45,&g_45,&g_45,&g_45,&g_440,&g_45,&g_440}},{{&g_45,&g_440,(void*)0,&g_440,&g_45,&g_45,&g_45,&g_440,(void*)0},{&g_45,&g_45,&g_45,&g_440,&g_45,&g_440,&g_440,&g_440,&g_440},{(void*)0,&g_440,&g_440,&g_440,(void*)0,&g_45,(void*)0,&g_440,&g_440}},{{&g_45,&g_440,&g_45,&g_45,&g_45,&g_45,&g_440,&g_45,&g_440},{&g_45,&g_440,(void*)0,&g_440,&g_45,&g_45,&g_45,&g_440,(void*)0},{&g_45,(void*)0,&g_440,(void*)0,(void*)0,&g_45,(void*)0,(void*)0,&g_45}}};
    uint32_t l_441 = 1UL;
    uint32_t l_442 = 4294967295UL;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_393[i] = &g_368;
    g_368 = (func_104(((~((g_112[3] &= g_101[0]) || (safe_mul_func_uint16_t_u_u(((func_115(l_117) || (safe_sub_func_uint8_t_u_u(((*l_134) = (+(((((((*g_38) = p_99) ^ (l_125[0][1] = p_99)) , (safe_lshift_func_uint8_t_u_s((safe_div_func_int32_t_s_s((safe_mod_func_int32_t_s_s(l_125[0][1], ((-4L) & ((safe_rshift_func_uint16_t_u_u(g_97[3][0][0], g_44)) >= l_125[0][1])))), g_101[1])), 0))) , 0x1BL) , l_125[1][0]) , g_39))), 3L))) , l_136[3][1]), 6L)))) , g_39), l_137, &g_12, g_44, l_134) == p_99);
    (*l_416) = (safe_mul_func_float_f_f((&l_136[3][1] != ((((safe_lshift_func_int8_t_s_u((((safe_lshift_func_int16_t_s_u(0x4B47L, ((g_168 != ((safe_sub_func_int64_t_s_s((&g_233 == ((p_99 != (safe_sub_func_int8_t_s_s((((safe_sub_func_uint16_t_u_u(l_406, ((g_310 && (safe_rshift_func_int8_t_s_u(((((*l_415) &= ((*g_351) = (safe_mul_func_uint8_t_u_u((((*l_413) = ((safe_add_func_float_f_f((*l_137), g_177[0])) > (-0x5.4p-1))) , g_146), g_414[0])))) , 0xB234A05AL) == p_99), 1))) & p_99))) == (-1L)) , 9L), 1UL))) , (void*)0)), p_99)) & 0L)) & g_146))) , p_99) <= p_99), 6)) || g_190) > p_99) , (void*)0)), 0x2.145808p-19));
    l_442 ^= ((safe_mul_func_uint16_t_u_u((((*l_137) , (-6L)) <= ((*l_134) &= (p_99 && (((safe_div_func_uint32_t_u_u((safe_sub_func_int16_t_s_s(6L, (safe_div_func_int16_t_s_s((l_441 = (safe_sub_func_int64_t_s_s((*g_38), (((*l_428)++) && ((g_433 = l_416) != (((&l_415 != ((((*l_415) = ((*g_351) |= ((((((((*l_137) || (safe_mul_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(((safe_unary_minus_func_int8_t_s(((6L == p_99) , g_168))) < (*g_38)), 1L)), 0x54F0L))) && 0x2EB42526L) >= p_99) ^ 6UL) < p_99) < p_99) || (*g_38)))) != (*l_137)) , &g_351)) >= g_168) , &g_101[1])))))), 1L)))), 0x6E64F549L)) == p_99) >= 0UL)))), g_47)) >= (*l_137));
    return (*l_137);
}


/* ------------------------------------------ */
/* 
 * reads : g_39 g_144 g_146 g_161 g_97 g_168 g_165 g_12 g_45 g_135 g_101 g_166 g_44 g_177 g_237 g_179 g_250 g_233 g_249 g_287 g_112 g_42 g_38 g_328 g_190 g_241 g_351 g_372 g_47
 * writes: g_39 g_146 g_165 g_168 g_174 g_177 g_166 g_237 g_179 g_250 g_233 g_287 g_241 g_175 g_310 g_328 g_350 g_352 g_97 g_112 g_372
 */
static int8_t  func_104(uint32_t  p_105, int32_t * p_106, int32_t * p_107, float  p_108, int8_t * const  p_109)
{ /* block id: 29 */
    int32_t *l_138 = (void*)0;
    int32_t **l_139 = (void*)0;
    int32_t **l_140 = (void*)0;
    int32_t **l_141 = &l_138;
    const uint64_t l_164 = 1UL;
    uint16_t *l_167 = &g_168;
    uint16_t *l_173[7][6][1] = {{{&g_47},{&g_47},{&g_47},{&g_47},{&g_174},{&g_174}},{{&g_174},{&g_47},{&g_47},{&g_47},{&g_47},{&g_174}},{{&g_174},{&g_174},{&g_47},{&g_47},{&g_47},{&g_47}},{{&g_174},{&g_174},{&g_174},{&g_47},{&g_47},{&g_47}},{{&g_47},{&g_174},{&g_174},{&g_174},{&g_47},{&g_47}},{{&g_47},{&g_47},{&g_174},{&g_174},{&g_174},{&g_47}},{{&g_47},{&g_47},{&g_47},{&g_174},{&g_174},{&g_174}}};
    int16_t *l_176 = &g_177[0];
    int16_t *l_178[3];
    int32_t l_180 = 0x4CACFD22L;
    uint32_t l_203 = 0x96BFC592L;
    int32_t l_228 = (-8L);
    int32_t l_236[2][5][7] = {{{4L,(-5L),0xC6D75AD0L,(-1L),0x5287C32AL,0xA3E7E255L,0xA3E7E255L},{0L,0x1C0100DBL,0x5BA54898L,0x1C0100DBL,0L,0x23D9690AL,0x4898B4B1L},{0L,6L,7L,0x417017BCL,0x15593264L,5L,0x6365C1E2L},{1L,0xB246ADE1L,(-1L),0x0C57F21AL,0x0C57F21AL,(-1L),0xB246ADE1L},{0L,0x417017BCL,6L,4L,(-5L),0xC6D75AD0L,(-1L)}},{{0L,0x9A4C43A8L,(-1L),0x5BA54898L,(-1L),0x0C57F21AL,(-1L)},{4L,1L,1L,4L,0xA3E7E255L,(-1L),7L},{0x23D9690AL,(-1L),0L,0x0C57F21AL,0xAE42E39EL,2L,0x1C0100DBL},{7L,(-1L),(-1L),0x417017BCL,(-1L),(-1L),7L},{(-1L),1L,0x9A4C43A8L,0x1C0100DBL,0x23D9690AL,0xB246ADE1L,(-1L)}}};
    uint64_t l_242 = 0x79DBC9DE42899F2ALL;
    int16_t l_357 = 0xFAFCL;
    int16_t l_370 = 0L;
    int32_t l_371 = (-4L);
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_178[i] = &g_179;
    (*l_141) = l_138;
    for (g_39 = (-9); (g_39 != (-1)); g_39 = safe_add_func_uint64_t_u_u(g_39, 8))
    { /* block id: 33 */
        g_146 |= ((void*)0 != g_144);
    }
    if ((g_166 = (safe_div_func_int16_t_s_s((-4L), (safe_unary_minus_func_int16_t_s((l_180 |= ((*l_176) = (safe_add_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((((safe_sub_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_u(((+(-5L)) || ((safe_mod_func_uint64_t_u_u(((((g_161 , ((g_174 = (((g_97[3][0][0] , (safe_add_func_uint8_t_u_u(0x2DL, (((g_165 = (l_164 , &g_12)) != ((*l_141) = (*l_141))) | (--(*l_167)))))) || (safe_mul_func_int8_t_s_s((p_105 < (*g_165)), (-3L)))) != 1UL)) | g_45)) , g_135[3][1]) & 0xBE4C4CDAL) | g_101[0]), p_105)) || g_97[3][0][0])), 3)) != g_166), 0xA6L)) ^ 8L) || (*p_109)), (-9L))), 0UL))))))))))
    { /* block id: 43 */
        int32_t *l_181 = (void*)0;
        int32_t *l_182 = (void*)0;
        int32_t *l_183 = &l_180;
        const int16_t *l_186[2];
        uint16_t *l_201 = &g_168;
        int32_t l_209 = 0x6ECED94FL;
        int32_t l_230 = 0L;
        int32_t l_231 = (-1L);
        int32_t l_234 = (-1L);
        int32_t l_235 = 4L;
        int32_t *l_240 = &g_42;
        int16_t *l_260 = &g_177[0];
        int16_t *l_261 = &g_45;
        int64_t *l_265 = (void*)0;
        int8_t *l_267 = &g_248;
        int8_t l_320 = (-8L);
        int i;
        for (i = 0; i < 2; i++)
            l_186[i] = &g_177[0];
        (*l_183) &= (*g_165);
        for (g_39 = 0; (g_39 >= 0); g_39 -= 1)
        { /* block id: 47 */
            const int16_t **l_187 = &l_186[0];
            const int16_t *l_189 = &g_190;
            const int16_t **l_188 = &l_189;
            uint16_t **l_202 = &l_173[1][4][0];
            int32_t l_227 = 0x0E318C20L;
            int32_t l_229 = (-1L);
            int32_t l_232 = 0xE789F30DL;
            int i;
            if (((safe_add_func_int32_t_s_s(g_101[g_39], (p_105 , (((((*l_187) = l_186[0]) != ((*l_188) = l_167)) || g_44) , ((p_105 == ((safe_add_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u(p_105, 12)), ((*l_176) = (safe_rshift_func_int8_t_s_u((g_166 > ((((safe_div_func_int16_t_s_s((((safe_rshift_func_int8_t_s_u((((*l_202) = l_201) != l_167), l_203)) <= 0x5F9BL) > 0xC179A001L), 0x4398L)) <= g_168) | g_177[0]) == 1L)), p_105))))) >= (*p_107))) >= (-1L)))))) == (*p_109)))
            { /* block id: 52 */
                return (*p_109);
            }
            else
            { /* block id: 54 */
                int32_t *l_204 = &l_180;
                int32_t *l_205 = &l_180;
                int32_t *l_206 = &l_180;
                int32_t *l_207 = &l_180;
                int32_t *l_208 = (void*)0;
                int32_t *l_210 = &g_166;
                int32_t *l_211 = (void*)0;
                int32_t *l_212 = &l_180;
                int32_t *l_213 = &l_180;
                int32_t *l_214 = &l_209;
                int32_t *l_215 = (void*)0;
                int32_t *l_216 = (void*)0;
                int32_t *l_217 = &l_209;
                int32_t *l_218 = &l_180;
                int32_t l_219 = 0xF0C8DDAEL;
                int32_t *l_220 = &g_166;
                int32_t l_221 = (-1L);
                int32_t *l_222 = &l_219;
                int32_t *l_223 = &l_180;
                int32_t *l_224 = (void*)0;
                int32_t l_225 = 0x4C2ECEAEL;
                int32_t *l_226[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_226[i] = &l_225;
                for (g_146 = 0; g_146 < 2; g_146 += 1)
                {
                    l_186[g_146] = &g_45;
                }
                ++g_237;
                g_165 = (void*)0;
                (*l_183) |= 0L;
            }
            for (l_229 = 0; (l_229 <= 0); l_229 += 1)
            { /* block id: 62 */
                return (*p_109);
            }
        }
        (*l_141) = l_240;
        for (g_179 = 0; (g_179 <= 1); g_179 += 1)
        { /* block id: 69 */
            int32_t l_247[7][6][6] = {{{0x5D5CCFC8L,(-4L),0xC9CCB98CL,0x44FB7190L,0x44FB7190L,0xC9CCB98CL},{9L,9L,(-4L),(-9L),0xD7152396L,2L},{0x047C511BL,0x44FB7190L,0x9ECE0AAEL,0x281A18FFL,0L,(-4L)},{4L,0x047C511BL,0x9ECE0AAEL,2L,9L,2L},{(-4L),2L,(-4L),3L,(-10L),0xC9CCB98CL},{3L,(-10L),0xC9CCB98CL,0L,0x91BF880EL,0xD7152396L}},{{(-9L),0xC9CCB98CL,0L,0L,3L,3L},{3L,0L,0L,3L,0x047C511BL,0x44FB7190L},{(-4L),(-9L),0xD7152396L,2L,0L,0x9ECE0AAEL},{4L,0L,9L,0x281A18FFL,0L,(-10L)},{0x047C511BL,(-9L),3L,(-9L),0x047C511BL,0x281A18FFL},{9L,0L,0x5D5CCFC8L,0x44FB7190L,(-4L),(-1L)}},{{3L,0x281A18FFL,(-9L),9L,0x44FB7190L,(-1L)},{0L,0L,3L,3L,0L,0L},{0x44FB7190L,0xC9CCB98CL,(-4L),0x5D5CCFC8L,4L,0L},{0xC9CCB98CL,(-9L),4L,0x047C511BL,0x9ECE0AAEL,2L},{0xC9CCB98CL,0xD7152396L,0x047C511BL,0x5D5CCFC8L,0x047C511BL,0xD7152396L},{0x44FB7190L,4L,9L,3L,0xD7152396L,(-4L)}},{{0L,0x91BF880EL,0x5D5CCFC8L,9L,(-1L),0x047C511BL},{3L,0x91BF880EL,0x281A18FFL,0xD7152396L,0xD7152396L,0x281A18FFL},{4L,4L,0x91BF880EL,0L,0x047C511BL,0xC9CCB98CL},{(-9L),0xD7152396L,2L,0L,0x9ECE0AAEL,0x91BF880EL},{(-1L),(-9L),2L,0xC9CCB98CL,4L,0xC9CCB98CL},{0x91BF880EL,0xC9CCB98CL,0x91BF880EL,(-4L),0L,0x281A18FFL}},{{(-4L),0L,0x281A18FFL,0x9ECE0AAEL,0x44FB7190L,0x047C511BL},{0L,0x281A18FFL,0x5D5CCFC8L,0x9ECE0AAEL,(-4L),(-4L)},{(-4L),9L,9L,(-4L),(-9L),0xD7152396L},{0x91BF880EL,0L,0x047C511BL,0xC9CCB98CL,0x5D5CCFC8L,2L},{(-1L),0x9ECE0AAEL,4L,0L,0x5D5CCFC8L,0L},{(-9L),0L,(-4L),0L,(-9L),0L}},{{4L,9L,3L,0xD7152396L,(-4L),(-1L)},{3L,0x281A18FFL,(-9L),9L,0x44FB7190L,(-1L)},{0L,0L,3L,3L,0L,0L},{0x44FB7190L,0xC9CCB98CL,(-4L),0x5D5CCFC8L,4L,0L},{0xC9CCB98CL,(-9L),4L,0x047C511BL,0x9ECE0AAEL,2L},{0xC9CCB98CL,0xD7152396L,0x047C511BL,0x5D5CCFC8L,0x047C511BL,0xD7152396L}},{{0x44FB7190L,4L,9L,3L,0xD7152396L,(-4L)},{0L,0x91BF880EL,0x5D5CCFC8L,9L,(-1L),0x047C511BL},{3L,0x91BF880EL,0x281A18FFL,0xD7152396L,0xD7152396L,0x281A18FFL},{4L,4L,0x91BF880EL,0L,0x047C511BL,0xC9CCB98CL},{(-9L),0xD7152396L,2L,0L,0x9ECE0AAEL,0x91BF880EL},{(-1L),(-9L),2L,0xC9CCB98CL,4L,0xC9CCB98CL}}};
            uint32_t l_253 = 0x54E90A01L;
            uint64_t l_264 = 0xE6F1A90540248992LL;
            uint16_t l_355 = 65535UL;
            int32_t *l_358 = &l_234;
            int32_t *l_359 = &l_180;
            int32_t *l_360 = &l_234;
            int32_t *l_361 = &l_230;
            int32_t *l_362 = (void*)0;
            int32_t *l_363 = &l_247[4][0][1];
            int32_t *l_364 = &l_234;
            int32_t *l_365 = &l_247[4][2][2];
            int32_t *l_366 = (void*)0;
            int32_t *l_367[9][8] = {{&g_166,&l_228,&g_166,(void*)0,(void*)0,&l_228,&l_180,&l_228},{&l_231,&g_166,&l_209,&g_166,&g_166,&g_166,&l_230,(void*)0},{&g_166,&l_230,&g_166,&l_231,(void*)0,&l_228,&g_166,&g_166},{&l_228,(void*)0,&g_166,&g_166,(void*)0,&l_228,&l_230,(void*)0},{(void*)0,(void*)0,&l_209,(void*)0,&g_166,&l_231,&l_228,&g_166},{&l_209,&l_231,&g_166,(void*)0,&g_166,&l_231,&l_209,(void*)0},{(void*)0,&g_166,&l_230,&g_166,&l_231,(void*)0,&l_228,&g_166},{(void*)0,&l_180,&g_42,&l_231,&l_231,&g_42,&l_180,(void*)0},{(void*)0,(void*)0,&l_231,&g_166,&g_166,&l_180,&l_230,(void*)0}};
            int i, j, k;
            (*l_183) = (*p_106);
            for (l_180 = 1; (l_180 >= 0); l_180 -= 1)
            { /* block id: 73 */
                int32_t l_245 = (-1L);
                int32_t *l_246[4];
                float l_258 = 0x5.2B729Ep+49;
                int16_t *l_259 = (void*)0;
                int i;
                for (i = 0; i < 4; i++)
                    l_246[i] = &l_234;
                ++l_242;
                l_245 ^= g_177[0];
                g_250++;
                l_253 |= (*p_106);
                for (g_233 = 0; (g_233 <= 1); g_233 += 1)
                { /* block id: 80 */
                    for (l_231 = 1; (l_231 >= 0); l_231 -= 1)
                    { /* block id: 83 */
                        int8_t *l_262 = &g_146;
                        int32_t l_263 = (-9L);
                        int i, j, k;
                        g_166 &= ((safe_div_func_int16_t_s_s((((l_236[g_233][(l_180 + 1)][(l_180 + 3)] == p_105) < (*p_109)) | 0xD319A833L), (l_263 |= (safe_mul_func_int32_t_s_s((g_179 > l_247[6][4][0]), (((*l_262) = ((0x3983L ^ (((l_260 = l_259) != l_261) < p_105)) > p_105)) <= g_177[0])))))) , (*p_106));
                    }
                }
            }
            if (((void*)0 != l_186[0]))
            { /* block id: 91 */
                int16_t * const *l_266 = &l_176;
                int32_t l_269 = (-7L);
                const uint64_t *l_270 = &l_164;
                l_264 = 0xCFD49A66L;
                if (((*l_183) |= ((0x4C89L | ((l_265 == &g_233) || (&g_97[3][0][0] == (((void*)0 == l_266) , l_267)))) ^ (((((((((+1UL) || p_105) < l_269) > 0xF338L) , g_249) < 0x9C38L) <= g_44) , l_270) == l_265))))
                { /* block id: 94 */
                    uint8_t l_296 = 0UL;
                    int32_t l_297 = 1L;
                    for (p_105 = 0; (p_105 <= 5); p_105 += 1)
                    { /* block id: 97 */
                        int32_t *l_271 = &l_247[4][5][2];
                        int32_t *l_272 = &l_180;
                        int32_t *l_273 = (void*)0;
                        int32_t *l_274 = &l_247[4][2][2];
                        int32_t *l_275 = &g_166;
                        int32_t *l_276 = (void*)0;
                        int32_t *l_277 = &l_231;
                        int32_t *l_278 = &l_230;
                        int32_t *l_279 = &l_247[0][1][3];
                        int32_t *l_280 = &l_235;
                        int32_t *l_281 = (void*)0;
                        int32_t *l_282 = &l_269;
                        int32_t *l_283 = &l_247[1][3][3];
                        int32_t *l_284 = &g_241;
                        int32_t *l_285 = &g_241;
                        int32_t *l_286[10];
                        uint8_t *l_304 = &g_237;
                        int i;
                        for (i = 0; i < 10; i++)
                            l_286[i] = &l_236[0][1][5];
                        --g_287;
                        (*l_141) = &l_234;
                        (*l_280) &= (g_179 , (l_269 = ((((safe_sub_func_int8_t_s_s(((*p_109) > ((*l_304) = ((safe_sub_func_int64_t_s_s((((l_296 || 3UL) != ((l_297 = (g_97[5][0][0] < g_97[2][0][0])) , (safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s(((safe_rshift_func_uint8_t_u_s(((&p_107 != (p_105 , &g_165)) < (*l_183)), g_166)) > g_233), 1UL)), 4L)))) || 65535UL), p_105)) > 0xB5L))), p_105)) < p_105) || l_269) == p_105)));
                        if ((*p_106))
                            continue;
                    }
                    for (g_241 = 0; (g_241 > (-5)); g_241 = safe_sub_func_uint16_t_u_u(g_241, 6))
                    { /* block id: 108 */
                        return g_161;
                    }
                    for (l_180 = (-24); (l_180 == (-5)); l_180++)
                    { /* block id: 113 */
                        g_165 = (p_107 = &l_269);
                    }
                }
                else
                { /* block id: 117 */
                    for (g_287 = 0; (g_287 <= 1); g_287 += 1)
                    { /* block id: 120 */
                        float *l_309[8];
                        int i, j;
                        for (i = 0; i < 8; i++)
                            l_309[i] = &g_175[4];
                        g_165 = &l_247[1][0][5];
                        if (g_135[(g_179 + 6)][g_179])
                            continue;
                        g_310 = (g_175[2] = g_112[(g_287 + 2)]);
                    }
                    l_247[4][2][2] &= (safe_add_func_uint32_t_u_u(p_105, ((safe_sub_func_uint64_t_u_u(g_45, ((((*p_107) || (safe_add_func_int32_t_s_s(((safe_div_func_uint32_t_u_u((((&p_106 != (void*)0) >= ((*l_240) > 1UL)) , (p_105 ^ (+0xC90EL))), 0x5370F3B8L)) | g_177[0]), g_177[0]))) , p_105) , (*g_38)))) <= 6L)));
                }
                return l_320;
            }
            else
            { /* block id: 129 */
                uint64_t l_321 = 18446744073709551615UL;
                int32_t l_324 = 1L;
                int32_t *l_325 = &l_228;
                int32_t *l_326 = &g_241;
                int32_t *l_327[4] = {&l_236[0][4][3],&l_236[0][4][3],&l_236[0][4][3],&l_236[0][4][3]};
                uint32_t *l_333[9] = {&g_250,(void*)0,&g_250,(void*)0,&g_250,(void*)0,&g_250,(void*)0,&g_250};
                int16_t **l_349 = &l_260;
                int16_t ***l_348[6][9] = {{&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349},{&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349},{&l_349,&l_349,(void*)0,&l_349,&l_349,&l_349,(void*)0,(void*)0,&l_349},{&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,(void*)0,&l_349},{&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349,&l_349},{&l_349,(void*)0,&l_349,&l_349,(void*)0,&l_349,&l_349,&l_349,&l_349}};
                uint64_t *l_356 = &g_112[0];
                int i, j;
                l_321--;
                (*l_141) = (void*)0;
                g_328++;
                (*l_326) = (((*l_356) = ((l_247[4][2][2] = (safe_lshift_func_uint16_t_u_s(((*l_201) = (*l_240)), 11))) , ((safe_rshift_func_uint16_t_u_s((((safe_div_func_uint8_t_u_u(((safe_sub_func_int8_t_s_s(((((safe_lshift_func_uint16_t_u_u(0x7122L, 4)) != (safe_rshift_func_int8_t_s_u((g_97[1][0][1] = ((safe_rshift_func_uint8_t_u_s(((safe_lshift_func_int8_t_s_u(((g_350[4][1][1] = (void*)0) == (g_352 = &l_261)), ((safe_mod_func_int8_t_s_s((g_190 && g_190), (l_247[5][5][2] = l_253))) && ((void*)0 == &g_328)))) != 0x6EC7L), l_253)) ^ (*l_326))), 4))) & 65533UL) || (*g_351)), g_112[0])) > g_44), l_355)) , (void*)0) == &g_190), 13)) == 0xF59413205A4E5D21LL))) ^ 0xB5957E19047CAE5DLL);
            }
            g_372--;
        }
    }
    else
    { /* block id: 144 */
        uint64_t *l_382[6] = {&g_112[2],&g_112[2],&g_112[2],&g_112[2],&g_112[2],&g_112[2]};
        uint64_t **l_381 = &l_382[1];
        int16_t **l_386 = &l_178[1];
        int32_t l_391 = 0x726A70D8L;
        int32_t *l_392 = &l_236[1][1][0];
        int i;
        (*l_392) &= (p_105 | (safe_sub_func_int64_t_s_s(((g_42 , ((safe_lshift_func_int8_t_s_s((safe_mul_func_int16_t_s_s(((*g_351) = (&g_112[0] == ((*l_381) = (void*)0))), (safe_lshift_func_uint8_t_u_s((safe_unary_minus_func_int8_t_s((l_386 == (void*)0))), 7)))), 7)) >= p_105)) & p_105), ((safe_mod_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u(((255UL < (-8L)) ^ g_47), 0)), l_391)) && p_105))));
        return g_12;
    }
    return (*p_109);
}


/* ------------------------------------------ */
/* 
 * reads : g_118
 * writes: g_118
 */
static uint16_t  func_115(const uint32_t  p_116)
{ /* block id: 23 */
    volatile uint64_t * volatile **l_121 = &g_118;
    (*l_121) = g_118;
    return p_116;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_39, "g_39", print_hash_value);
    transparent_crc(g_42, "g_42", print_hash_value);
    transparent_crc(g_44, "g_44", print_hash_value);
    transparent_crc(g_45, "g_45", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_77[i][j], "g_77[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_97[i][j][k], "g_97[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_101[i], "g_101[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_112[i], "g_112[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_120, "g_120", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_135[i][j], "g_135[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_146, "g_146", print_hash_value);
    transparent_crc(g_161, "g_161", print_hash_value);
    transparent_crc(g_166, "g_166", print_hash_value);
    transparent_crc(g_168, "g_168", print_hash_value);
    transparent_crc(g_174, "g_174", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc_bytes(&g_175[i], sizeof(g_175[i]), "g_175[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_177[i], "g_177[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_179, "g_179", print_hash_value);
    transparent_crc(g_190, "g_190", print_hash_value);
    transparent_crc(g_233, "g_233", print_hash_value);
    transparent_crc(g_237, "g_237", print_hash_value);
    transparent_crc(g_241, "g_241", print_hash_value);
    transparent_crc(g_248, "g_248", print_hash_value);
    transparent_crc(g_249, "g_249", print_hash_value);
    transparent_crc(g_250, "g_250", print_hash_value);
    transparent_crc(g_287, "g_287", print_hash_value);
    transparent_crc(g_310, "g_310", print_hash_value);
    transparent_crc(g_328, "g_328", print_hash_value);
    transparent_crc(g_368, "g_368", print_hash_value);
    transparent_crc(g_369, "g_369", print_hash_value);
    transparent_crc(g_372, "g_372", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_414[i], "g_414[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc_bytes(&g_417[i], sizeof(g_417[i]), "g_417[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_440, "g_440", print_hash_value);
    transparent_crc(g_450, "g_450", print_hash_value);
    transparent_crc(g_459, "g_459", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc_bytes(&g_461[i][j][k], sizeof(g_461[i][j][k]), "g_461[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_464, "g_464", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_661[i][j], "g_661[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_669, "g_669", print_hash_value);
    transparent_crc(g_751, "g_751", print_hash_value);
    transparent_crc(g_809, "g_809", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_810[i][j], "g_810[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_811, "g_811", print_hash_value);
    transparent_crc(g_812, "g_812", print_hash_value);
    transparent_crc(g_998, "g_998", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1073[i][j], "g_1073[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1081, "g_1081", print_hash_value);
    transparent_crc_bytes (&g_1114, sizeof(g_1114), "g_1114", print_hash_value);
    transparent_crc(g_1271, "g_1271", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1287[i], "g_1287[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_1343[i][j], "g_1343[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1831, "g_1831", print_hash_value);
    transparent_crc_bytes (&g_1912, sizeof(g_1912), "g_1912", print_hash_value);
    transparent_crc(g_2016, "g_2016", print_hash_value);
    transparent_crc(g_2041, "g_2041", print_hash_value);
    transparent_crc_bytes (&g_2105, sizeof(g_2105), "g_2105", print_hash_value);
    transparent_crc(g_2195, "g_2195", print_hash_value);
    transparent_crc(g_2253, "g_2253", print_hash_value);
    transparent_crc(g_2276, "g_2276", print_hash_value);
    transparent_crc(g_2303, "g_2303", print_hash_value);
    transparent_crc(g_2343, "g_2343", print_hash_value);
    transparent_crc(g_2428, "g_2428", print_hash_value);
    transparent_crc(g_2529, "g_2529", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 576
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 183
   depth: 2, occurrence: 45
   depth: 3, occurrence: 3
   depth: 4, occurrence: 2
   depth: 6, occurrence: 1
   depth: 9, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 3
   depth: 18, occurrence: 3
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 3
   depth: 26, occurrence: 3
   depth: 27, occurrence: 3
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 32, occurrence: 3
   depth: 33, occurrence: 3
   depth: 34, occurrence: 2
   depth: 37, occurrence: 1
   depth: 40, occurrence: 1
   depth: 48, occurrence: 2

XXX total number of pointers: 481

XXX times a variable address is taken: 1205
XXX times a pointer is dereferenced on RHS: 369
breakdown:
   depth: 1, occurrence: 254
   depth: 2, occurrence: 111
   depth: 3, occurrence: 2
   depth: 4, occurrence: 2
XXX times a pointer is dereferenced on LHS: 312
breakdown:
   depth: 1, occurrence: 271
   depth: 2, occurrence: 39
   depth: 3, occurrence: 2
XXX times a pointer is compared with null: 46
XXX times a pointer is compared with address of another variable: 10
XXX times a pointer is compared with another pointer: 14
XXX times a pointer is qualified to be dereferenced: 6827

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1027
   level: 2, occurrence: 459
   level: 3, occurrence: 73
   level: 4, occurrence: 42
   level: 5, occurrence: 8
XXX number of pointers point to pointers: 180
XXX number of pointers point to scalars: 301
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.9
XXX average alias set size: 1.42

XXX times a non-volatile is read: 2033
XXX times a non-volatile is write: 998
XXX times a volatile is read: 37
XXX    times read thru a pointer: 35
XXX times a volatile is write: 8
XXX    times written thru a pointer: 8
XXX times a volatile is available for access: 320
XXX percentage of non-volatile access: 98.5

XXX forward jumps: 0
XXX backward jumps: 7

XXX stmts: 182
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 20
   depth: 2, occurrence: 18
   depth: 3, occurrence: 43
   depth: 4, occurrence: 31
   depth: 5, occurrence: 40

XXX percentage a fresh-made variable is used: 17.8
XXX percentage an existing variable is used: 82.2
********************* end of statistics **********************/


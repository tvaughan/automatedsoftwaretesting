/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      1402122663
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int16_t g_2 = 3L;/* VOLATILE GLOBAL g_2 */
static int32_t g_4 = (-6L);
static volatile uint32_t g_5 = 18446744073709551615UL;/* VOLATILE GLOBAL g_5 */
static int16_t g_31 = 0L;
static int16_t g_33[7] = {0xB75DL,0xB75DL,0xB75DL,0xB75DL,0xB75DL,0xB75DL,0xB75DL};
static int16_t g_66 = 2L;
static uint8_t g_119[8][8] = {{0xEAL,0x70L,0x32L,0x70L,0xEAL,0x32L,0x28L,0x28L},{248UL,0x70L,6UL,6UL,0x70L,248UL,0x82L,0x70L},{0x28L,0x82L,6UL,0x28L,6UL,0x82L,0x28L,248UL},{0x70L,0xEAL,0x32L,0x28L,0x28L,0x32L,0xEAL,0x70L},{248UL,0x28L,0x82L,6UL,0x28L,6UL,0x82L,248UL},{6UL,0x32L,1UL,6UL,1UL,1UL,6UL,1UL},{248UL,248UL,0x28L,0x82L,6UL,0x28L,6UL,0x82L},{1UL,0x82L,1UL,1UL,0x82L,0x32L,0x32L,0x82L}};
static const int32_t *g_148 = &g_4;
static const int32_t **g_147 = &g_148;
static uint16_t g_156 = 65531UL;
static float g_168 = (-0x7.5p+1);
static int8_t g_171 = 0x88L;
static uint32_t g_172[2] = {0xEBA21342L,0xEBA21342L};
static int32_t g_174 = 0x5A347877L;
static int32_t g_180 = 0xBDCD1592L;
static int64_t g_181 = 0L;
static int32_t g_182 = 0x773156C3L;
static uint16_t g_183 = 0xC06EL;
static int32_t g_191[3] = {0xC52FFF79L,0xC52FFF79L,0xC52FFF79L};
static uint32_t g_224[7][7][2] = {{{0x8042E1AEL,18446744073709551614UL},{0x11363E69L,0x5E212201L},{0x5E212201L,0x11363E69L},{18446744073709551614UL,0x8042E1AEL},{18446744073709551614UL,0x11363E69L},{0x5E212201L,0x5E212201L},{0x11363E69L,18446744073709551614UL}},{{0x8042E1AEL,18446744073709551614UL},{0x11363E69L,0x5E212201L},{0x5E212201L,0x11363E69L},{18446744073709551614UL,0x8042E1AEL},{18446744073709551614UL,0x11363E69L},{0x5E212201L,0x5E212201L},{0x11363E69L,18446744073709551614UL}},{{0x8042E1AEL,18446744073709551614UL},{0x11363E69L,0x5E212201L},{0x5E212201L,0x11363E69L},{18446744073709551614UL,0x8042E1AEL},{18446744073709551614UL,0x11363E69L},{0x5E212201L,0x5E212201L},{0x11363E69L,18446744073709551614UL}},{{0x8042E1AEL,18446744073709551614UL},{0x11363E69L,0x5E212201L},{0x5E212201L,0x11363E69L},{18446744073709551614UL,0x8042E1AEL},{18446744073709551614UL,0x11363E69L},{0x5E212201L,0x5E212201L},{0x11363E69L,18446744073709551614UL}},{{0x8042E1AEL,18446744073709551614UL},{0x11363E69L,0x5E212201L},{0x5E212201L,0x11363E69L},{18446744073709551614UL,0x8042E1AEL},{18446744073709551614UL,0x11363E69L},{0x5E212201L,0x5E212201L},{0x11363E69L,18446744073709551614UL}},{{0x8042E1AEL,18446744073709551614UL},{0x11363E69L,0x5E212201L},{0x5E212201L,0x11363E69L},{18446744073709551614UL,0x8042E1AEL},{18446744073709551614UL,0x11363E69L},{0x5E212201L,0x5E212201L},{0x11363E69L,18446744073709551614UL}},{{0x8042E1AEL,18446744073709551614UL},{0x11363E69L,0x5E212201L},{0x5E212201L,0x11363E69L},{18446744073709551614UL,0x8042E1AEL},{18446744073709551614UL,0x11363E69L},{0x5E212201L,0x5E212201L},{0x11363E69L,18446744073709551614UL}}};
static int16_t g_245[5][9][5] = {{{0xFDF0L,(-8L),0L,0x2CF0L,1L},{0L,0xA974L,0x2228L,0L,(-4L)},{0L,(-8L),(-1L),(-8L),(-1L)},{0xA974L,0xA974L,0x3C17L,(-5L),(-7L)},{0x8938L,(-8L),0xF954L,1L,5L},{0L,0xA974L,(-7L),0xB5D4L,0x2228L},{2L,(-8L),1L,0x11B2L,0xF954L},{0xBB65L,0xA974L,0L,9L,0L},{(-8L),(-8L),5L,0xA83DL,0L}},{{0xB183L,0xA974L,(-4L),(-1L),0x3C17L},{0xFDF0L,(-8L),0L,0x2CF0L,1L},{0L,0xA974L,0x2228L,0L,(-4L)},{0L,(-8L),(-1L),1L,0x73F6L},{0x3C17L,0x3C17L,0x9D96L,0x2FD4L,1L},{(-1L),5L,0x1F65L,0xC9F1L,0x56BCL},{0x2228L,0x3C17L,1L,(-5L),0x520DL},{0L,5L,0xEFD1L,6L,0x1F65L},{(-4L),0x3C17L,0x896DL,0x8793L,0x896DL}},{{5L,5L,0x56BCL,(-1L),8L},{0L,0x3C17L,(-5L),0L,0x9D96L},{1L,5L,8L,0xE75DL,0xEFD1L},{(-7L),0x3C17L,0x520DL,0L,(-5L)},{0xF954L,5L,0x73F6L,1L,0x73F6L},{0x3C17L,0x3C17L,0x9D96L,0x2FD4L,1L},{(-1L),5L,0x1F65L,0xC9F1L,0x56BCL},{0x2228L,0x3C17L,1L,(-5L),0x520DL},{0L,5L,0xEFD1L,6L,0x1F65L}},{{(-4L),0x3C17L,0x896DL,0x8793L,0x896DL},{5L,5L,0x56BCL,(-1L),8L},{0L,0x3C17L,(-5L),0L,0x9D96L},{1L,5L,8L,0xE75DL,0xEFD1L},{(-7L),0x3C17L,0x520DL,0L,(-5L)},{0xF954L,5L,0x73F6L,1L,0x73F6L},{0x3C17L,0x3C17L,0x9D96L,0x2FD4L,1L},{(-1L),5L,0x1F65L,0xC9F1L,0x56BCL},{0x2228L,0x3C17L,1L,(-5L),0x520DL}},{{0L,5L,0xEFD1L,6L,0x1F65L},{(-4L),0x3C17L,0x896DL,0x8793L,0x896DL},{5L,5L,0x56BCL,(-1L),8L},{0L,0x3C17L,(-5L),0L,0x9D96L},{1L,5L,8L,0xE75DL,0xEFD1L},{(-7L),0x3C17L,0x520DL,0L,(-5L)},{0xF954L,5L,0x73F6L,1L,0x73F6L},{0x3C17L,0x3C17L,0x9D96L,0x2FD4L,1L},{(-1L),5L,0x1F65L,0xC9F1L,0x56BCL}}};
static uint16_t *g_260[10][3][2] = {{{&g_183,&g_156},{(void*)0,&g_156},{&g_183,&g_156}},{{(void*)0,&g_156},{&g_183,&g_156},{(void*)0,&g_156}},{{&g_183,&g_156},{(void*)0,&g_156},{&g_183,&g_156}},{{(void*)0,&g_156},{&g_183,&g_156},{(void*)0,&g_156}},{{&g_183,&g_156},{(void*)0,&g_156},{&g_183,&g_156}},{{(void*)0,&g_156},{&g_183,&g_156},{(void*)0,&g_156}},{{&g_183,&g_156},{(void*)0,&g_156},{&g_183,&g_156}},{{(void*)0,&g_156},{&g_183,&g_156},{(void*)0,&g_156}},{{&g_183,&g_156},{(void*)0,&g_156},{&g_183,&g_156}},{{(void*)0,&g_156},{&g_183,&g_156},{(void*)0,&g_156}}};
static uint8_t g_281 = 0xA4L;
static float g_305 = 0xA.B85963p+24;
static uint64_t g_321[9][6] = {{1UL,18446744073709551608UL,7UL,0UL,18446744073709551607UL,9UL},{0x2BE139EDC8300414LL,0x6D32FED8E15CF69DLL,1UL,9UL,9UL,1UL},{0x2DE78825F0558DB3LL,0x2DE78825F0558DB3LL,0UL,0x9F691A5FC7A70CB1LL,18446744073709551612UL,1UL},{7UL,0x4DC901E218672574LL,18446744073709551612UL,0x21FA9E638077705ELL,18446744073709551615UL,0UL},{0x6D32FED8E15CF69DLL,7UL,18446744073709551612UL,1UL,0x2DE78825F0558DB3LL,1UL},{0xE343D0A16C710281LL,1UL,0UL,0x2D250F800D05015ELL,0UL,1UL},{0x2D250F800D05015ELL,0UL,1UL,0xE343D0A16C710281LL,3UL,9UL},{1UL,18446744073709551612UL,7UL,0x6D32FED8E15CF69DLL,0x21FA9E638077705ELL,18446744073709551606UL},{0x21FA9E638077705ELL,18446744073709551612UL,0x4DC901E218672574LL,7UL,3UL,0x2BE139EDC8300414LL}};
static int8_t g_323 = 0x70L;
static uint64_t g_397 = 0xA1FEE238F7E52FACLL;
static int32_t g_403 = 0xED370A6AL;
static int16_t g_404[3][2] = {{0x5A69L,0x5A69L},{0x5A69L,0x5A69L},{0x5A69L,0x5A69L}};
static int8_t g_405 = 0xBBL;
static uint32_t g_406 = 0x0895CBA5L;
static float *g_413 = &g_305;
static int16_t g_422 = (-2L);
static int32_t g_464 = 1L;
static float g_510 = 0x0.Cp-1;
static uint8_t g_511[7] = {255UL,249UL,255UL,255UL,249UL,255UL,255UL};
static int16_t *g_530[2][6] = {{&g_422,&g_422,&g_66,&g_422,&g_422,&g_66},{&g_422,&g_422,&g_66,&g_422,&g_422,&g_66}};
static uint64_t *g_543 = (void*)0;
static uint64_t **g_542 = &g_543;
static uint8_t g_580 = 255UL;
static uint32_t g_598 = 4294967295UL;
static uint64_t g_603[6][1][2] = {{{0x5EA57A1EFF69008CLL,0x5EA57A1EFF69008CLL}},{{0x5EA57A1EFF69008CLL,0x5EA57A1EFF69008CLL}},{{0x5EA57A1EFF69008CLL,0x5EA57A1EFF69008CLL}},{{0x5EA57A1EFF69008CLL,0x5EA57A1EFF69008CLL}},{{0x5EA57A1EFF69008CLL,0x5EA57A1EFF69008CLL}},{{0x5EA57A1EFF69008CLL,0x5EA57A1EFF69008CLL}}};
static int32_t *g_628[5] = {&g_403,&g_403,&g_403,&g_403,&g_403};
static int32_t **g_627 = &g_628[1];
static uint64_t g_668 = 18446744073709551608UL;
static const int64_t g_726 = 0L;
static uint16_t g_833[7][3][8] = {{{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL},{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL},{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL}},{{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL},{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL},{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL}},{{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL},{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL},{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL}},{{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL},{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL},{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL}},{{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL},{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL},{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL}},{{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL},{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL},{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL}},{{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL},{0xA199L,0x8D5DL,0x5B3AL,0x8D5DL,0xA199L,65535UL,0x5B3AL,65535UL},{0xA199L,65535UL,0x5B3AL,65535UL,0xA199L,0x8D5DL,0x5B3AL,0x8D5DL}}};
static int64_t *g_859 = &g_181;
static int64_t g_952[5][7][7] = {{{(-1L),0xC0C36839F7A50D0DLL,0xE96586E983D65EB3LL,0xF84DA40ADF32BCBDLL,(-10L),0x3653E60B6CC8B770LL,0x605266222EC1753ELL},{0xF84DA40ADF32BCBDLL,0x605266222EC1753ELL,0x199966202390ECD4LL,0x542A5A0DB0D6CED9LL,0x92F637620C55FC05LL,1L,0x4BCCA59E8593B52BLL},{0xC0C36839F7A50D0DLL,5L,1L,1L,0x3EE552AF8955E680LL,0x300C969ABBC3C426LL,0xF84DA40ADF32BCBDLL},{0x3653E60B6CC8B770LL,0xD5A4B3732DC89A02LL,1L,0x34EBE21A7B8D75BALL,0x34EBE21A7B8D75BALL,1L,0xD5A4B3732DC89A02LL},{0x0A63EC8A2D9085EDLL,7L,0x199966202390ECD4LL,0xE024E516CDFAFAEELL,0xC0C36839F7A50D0DLL,0x542A5A0DB0D6CED9LL,1L},{0x6E3718CA66D780B8LL,(-5L),0xE96586E983D65EB3LL,5L,(-1L),0xD5A4B3732DC89A02LL,0x023358CC8F8694FBLL},{0x24C4E9AC21205D7ALL,0x6E3718CA66D780B8LL,0x34EBE21A7B8D75BALL,0xE024E516CDFAFAEELL,0x6F44D803291F1E42LL,0xC0C36839F7A50D0DLL,0x199966202390ECD4LL}},{{1L,0x023358CC8F8694FBLL,7L,0x34EBE21A7B8D75BALL,(-5L),0x92F637620C55FC05LL,(-1L)},{7L,0x6F44D803291F1E42LL,5L,1L,(-5L),(-5L),1L},{0x605266222EC1753ELL,(-10L),0x605266222EC1753ELL,0x542A5A0DB0D6CED9LL,0x6F44D803291F1E42LL,0x0A63EC8A2D9085EDLL,0xE024E516CDFAFAEELL},{(-5L),5L,0x24C4E9AC21205D7ALL,0xF84DA40ADF32BCBDLL,(-1L),0x75FB50876D107D60LL,0x6E3718CA66D780B8LL},{0x4BCCA59E8593B52BLL,0x3653E60B6CC8B770LL,(-1L),0L,0xC0C36839F7A50D0DLL,0x0A63EC8A2D9085EDLL,0xF1BF23F58D7CC361LL},{0xE024E516CDFAFAEELL,1L,1L,0xC0C36839F7A50D0DLL,0xF84DA40ADF32BCBDLL,0xD5A4B3732DC89A02LL,0x4BCCA59E8593B52BLL},{(-1L),0xE024E516CDFAFAEELL,7L,0x24C4E9AC21205D7ALL,0xE96586E983D65EB3LL,0x4BCCA59E8593B52BLL,0x4BCCA59E8593B52BLL}},{{0x4BCCA59E8593B52BLL,0xF1BF23F58D7CC361LL,0x605266222EC1753ELL,0xF1BF23F58D7CC361LL,0x4BCCA59E8593B52BLL,0x75FB50876D107D60LL,7L},{0x300C969ABBC3C426LL,0xDE812F79A9ECDA58LL,(-5L),0x4BCCA59E8593B52BLL,1L,0x92F637620C55FC05LL,0x542A5A0DB0D6CED9LL},{0x3653E60B6CC8B770LL,0x3EE552AF8955E680LL,0x4BCCA59E8593B52BLL,0x199966202390ECD4LL,(-10L),5L,5L},{0x300C969ABBC3C426LL,0x4BCCA59E8593B52BLL,0xE024E516CDFAFAEELL,0x75FB50876D107D60LL,(-5L),0xCB5A1670ABF5610FLL,4L},{0x4BCCA59E8593B52BLL,0x023358CC8F8694FBLL,0x199966202390ECD4LL,0xD5A4B3732DC89A02LL,0x34EBE21A7B8D75BALL,0xF1BF23F58D7CC361LL,(-10L)},{(-1L),0x023358CC8F8694FBLL,0x92F637620C55FC05LL,0x6E3718CA66D780B8LL,0x92F637620C55FC05LL,0x023358CC8F8694FBLL,(-1L)},{5L,0x4BCCA59E8593B52BLL,0x6F44D803291F1E42LL,1L,0xCB5A1670ABF5610FLL,(-1L),1L}},{{0x199966202390ECD4LL,0x3EE552AF8955E680LL,5L,0x0A63EC8A2D9085EDLL,0xEDB0FF6EAAFB427FLL,0x24C4E9AC21205D7ALL,0x023358CC8F8694FBLL},{0xD5A4B3732DC89A02LL,0xDE812F79A9ECDA58LL,0x6F44D803291F1E42LL,0x3EE552AF8955E680LL,4L,0x605266222EC1753ELL,0x92F637620C55FC05LL},{0x0A63EC8A2D9085EDLL,0xF1BF23F58D7CC361LL,0x92F637620C55FC05LL,7L,0x6F44D803291F1E42LL,5L,1L},{0xC0C36839F7A50D0DLL,0xE024E516CDFAFAEELL,0x199966202390ECD4LL,7L,0x0A63EC8A2D9085EDLL,7L,0x199966202390ECD4LL},{0xCB5A1670ABF5610FLL,0xCB5A1670ABF5610FLL,0xE024E516CDFAFAEELL,0x3EE552AF8955E680LL,(-1L),0x34EBE21A7B8D75BALL,0x0A63EC8A2D9085EDLL},{0x6E3718CA66D780B8LL,(-1L),0x4BCCA59E8593B52BLL,0x0A63EC8A2D9085EDLL,0x3EE552AF8955E680LL,0xE96586E983D65EB3LL,0x300C969ABBC3C426LL},{0x542A5A0DB0D6CED9LL,0x3653E60B6CC8B770LL,(-5L),1L,(-1L),0x199966202390ECD4LL,0xE96586E983D65EB3LL}},{{0xEDB0FF6EAAFB427FLL,1L,0x605266222EC1753ELL,0x6E3718CA66D780B8LL,0x0A63EC8A2D9085EDLL,1L,0xF1BF23F58D7CC361LL},{(-1L),0x300C969ABBC3C426LL,7L,0xD5A4B3732DC89A02LL,0x6F44D803291F1E42LL,1L,0x6F44D803291F1E42LL},{0x75FB50876D107D60LL,1L,1L,0x75FB50876D107D60LL,4L,0x199966202390ECD4LL,0xDE812F79A9ECDA58LL},{1L,0x542A5A0DB0D6CED9LL,0x24C4E9AC21205D7ALL,0x199966202390ECD4LL,0xEDB0FF6EAAFB427FLL,0xE96586E983D65EB3LL,(-1L)},{(-10L),0xD5A4B3732DC89A02LL,0x6E3718CA66D780B8LL,0x4BCCA59E8593B52BLL,0xCB5A1670ABF5610FLL,0x34EBE21A7B8D75BALL,0xDE812F79A9ECDA58LL},{0L,0xC0C36839F7A50D0DLL,0x0A63EC8A2D9085EDLL,0xF1BF23F58D7CC361LL,0x92F637620C55FC05LL,7L,0x6F44D803291F1E42LL},{0x605266222EC1753ELL,0x92F637620C55FC05LL,0x3653E60B6CC8B770LL,0x24C4E9AC21205D7ALL,0x34EBE21A7B8D75BALL,5L,0xF1BF23F58D7CC361LL}}};
static int16_t **g_1061 = &g_530[1][5];
static int16_t ***g_1060 = &g_1061;
static volatile int32_t g_1101 = 2L;/* VOLATILE GLOBAL g_1101 */
static volatile int32_t *g_1100 = &g_1101;
static volatile int32_t ** volatile g_1099 = &g_1100;/* VOLATILE GLOBAL g_1099 */
static volatile int32_t ** volatile *g_1098 = &g_1099;
static volatile int32_t ** volatile * volatile *g_1097 = &g_1098;
static int64_t g_1179 = 0xA448AFCAF241BCD6LL;
static int16_t g_1197 = 1L;
static int16_t * const g_1196 = &g_1197;
static int16_t * const *g_1195 = &g_1196;
static int16_t **g_1198 = &g_530[1][5];
static int32_t ** const g_1234 = (void*)0;
static int32_t ** const *g_1233 = &g_1234;
static uint8_t *g_1412 = &g_281;
static uint8_t **g_1411 = &g_1412;
static int32_t *g_1429 = &g_191[0];
static int32_t * volatile * const g_1428 = &g_1429;
static int32_t ***g_1458 = &g_627;
static int32_t ****g_1457 = &g_1458;
static int32_t ****g_1459 = (void*)0;
static int32_t g_1532 = (-2L);
static float g_1641 = 0x1.9p-1;
static int32_t g_1756 = 0x385F84BEL;
static const int32_t g_1783 = 0xBA420A10L;
static uint16_t **g_1813 = (void*)0;
static uint16_t **g_1814[10] = {&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1],&g_260[9][0][1]};
static uint32_t g_1815 = 0x01ED3EE3L;
static int16_t g_1871 = 0xF59EL;
static int32_t g_1906 = 1L;
static uint32_t g_1908 = 5UL;
static int8_t *g_1916 = (void*)0;
static const int16_t ***g_1990 = (void*)0;
static float *g_2100[10] = {&g_510,(void*)0,&g_510,&g_1641,&g_1641,&g_510,(void*)0,&g_510,&g_1641,&g_1641};
static int32_t *****g_2122 = &g_1457;
static const int16_t *g_2175 = &g_404[0][1];
static const int16_t * const *g_2174 = &g_2175;
static uint16_t g_2178 = 1UL;
static uint32_t *g_2419 = &g_172[0];
static uint32_t **g_2418 = &g_2419;
static float * volatile g_2500 = &g_1641;/* VOLATILE GLOBAL g_2500 */
static uint32_t *g_2517 = &g_172[0];
static volatile float g_2527 = 0x0.9p-1;/* VOLATILE GLOBAL g_2527 */


/* --- FORWARD DECLARATIONS --- */
static const uint32_t  func_1(void);
static int32_t  func_8(int32_t * p_9);
static int32_t * func_10(int32_t * p_11, uint64_t  p_12, int32_t * p_13);
static int32_t * func_34(int32_t * p_35, int16_t * p_36, int16_t  p_37, int64_t  p_38);
static int16_t * func_40(uint64_t  p_41, uint64_t  p_42, uint32_t  p_43);
static const uint16_t  func_67(const uint64_t  p_68, int32_t ** p_69, int8_t  p_70);
static const uint64_t  func_71(uint64_t  p_72, int32_t * p_73, const int16_t * p_74);
static uint8_t  func_84(int32_t ** p_85, int32_t  p_86);
static const int16_t  func_97(uint32_t  p_98, int32_t  p_99, uint32_t  p_100, int16_t * p_101);
static int32_t * func_126(uint64_t  p_127, uint32_t  p_128, int16_t * p_129, float  p_130);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_4
 * writes: g_5
 */
static const uint32_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_3[3];
    int16_t *l_30 = &g_31;
    int16_t *l_32[10][2] = {{&g_33[5],&g_33[6]},{&g_33[5],&g_33[5]},{&g_33[6],&g_33[6]},{&g_33[6],&g_33[5]},{&g_33[5],&g_33[6]},{&g_33[5],&g_33[6]},{&g_33[5],&g_33[5]},{&g_33[6],&g_33[5]},{&g_33[5],(void*)0},{&g_33[6],&g_33[0]}};
    int32_t **l_39 = &l_3[2];
    int32_t l_55[8] = {0x6E4944C1L,0x6E4944C1L,0x6E4944C1L,0x6E4944C1L,0x6E4944C1L,0x6E4944C1L,0x6E4944C1L,0x6E4944C1L};
    const float l_1934 = 0x1.9p+1;
    const int32_t l_1935 = 1L;
    uint64_t *l_1936 = &g_603[1][0][1];
    float l_2522[1];
    uint32_t *l_2523 = (void*)0;
    uint16_t l_2524[2];
    uint16_t l_2530 = 65526UL;
    int i, j;
    for (i = 0; i < 3; i++)
        l_3[i] = &g_4;
    for (i = 0; i < 1; i++)
        l_2522[i] = 0x7.85DC78p-72;
    for (i = 0; i < 2; i++)
        l_2524[i] = 65531UL;
    g_5++;
    return (**l_39);
}


/* ------------------------------------------ */
/* 
 * reads : g_181 g_174 g_4 g_1196 g_1197
 * writes: g_181 g_174 g_4
 */
static int32_t  func_8(int32_t * p_9)
{ /* block id: 946 */
    uint32_t l_2425[5][1][1];
    int32_t l_2430[1];
    int16_t *l_2435 = (void*)0;
    int32_t l_2446[5][1][4] = {{{1L,1L,1L,1L}},{{1L,1L,1L,1L}},{{1L,1L,1L,1L}},{{1L,1L,1L,1L}},{{1L,1L,1L,1L}}};
    int i, j, k;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
                l_2425[i][j][k] = 0x44C66FAFL;
        }
    }
    for (i = 0; i < 1; i++)
        l_2430[i] = (-1L);
    for (g_181 = 0; (g_181 <= 1); g_181 += 1)
    { /* block id: 949 */
        float l_2427 = 0xC.D1830Dp+20;
        int32_t l_2428[9];
        int32_t *l_2445 = &g_1906;
        int16_t ** const ***l_2449 = (void*)0;
        int i;
        for (i = 0; i < 9; i++)
            l_2428[i] = (-9L);
        for (g_174 = 0; (g_174 <= 1); g_174 += 1)
        { /* block id: 952 */
            int32_t l_2426 = 0x952A29BBL;
            int32_t l_2429[9] = {0x8CEB5E55L,0x8CEB5E55L,0x29E98DE5L,0x8CEB5E55L,0x8CEB5E55L,0x29E98DE5L,0x8CEB5E55L,0x8CEB5E55L,0x29E98DE5L};
            int16_t *l_2436 = (void*)0;
            int16_t ****l_2448[9] = {&g_1060,&g_1060,&g_1060,&g_1060,&g_1060,&g_1060,&g_1060,&g_1060,&g_1060};
            int16_t *****l_2447 = &l_2448[2];
            int i;
            l_2430[0] &= (safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s((((*p_9) &= ((l_2425[3][0][0] ^ 3UL) >= l_2426)) & l_2425[3][0][0]), l_2428[1])), (l_2429[0] = ((-1L) & l_2426))));
            (*p_9) = (safe_lshift_func_int8_t_s_s((((safe_add_func_uint16_t_u_u(0xED66L, l_2429[2])) ^ ((l_2435 == l_2436) || (safe_add_func_uint16_t_u_u(l_2430[0], ((l_2447 = ((safe_mul_func_int16_t_s_s((*g_1196), ((l_2446[4][0][3] = (safe_rshift_func_uint16_t_u_u((l_2428[1] = ((safe_rshift_func_int16_t_s_u((-7L), (l_2445 != p_9))) != (-2L))), 5))) | l_2430[0]))) , (void*)0)) != l_2449))))) , l_2428[1]), 4));
        }
        return (*p_9);
    }
    return l_2430[0];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_10(int32_t * p_11, uint64_t  p_12, int32_t * p_13)
{ /* block id: 721 */
    int32_t l_1939 = 0xA1858892L;
    uint64_t *l_1948 = &g_603[5][0][0];
    uint64_t *l_1949 = &g_603[3][0][1];
    uint64_t *l_1955 = &g_603[3][0][0];
    int32_t l_1960 = 0L;
    int64_t **l_1965 = &g_859;
    int32_t l_1975[8];
    uint32_t *l_1983 = (void*)0;
    uint32_t **l_1982 = &l_1983;
    uint32_t ***l_1981[8][2][10] = {{{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982},{&l_1982,&l_1982,&l_1982,&l_1982,(void*)0,&l_1982,&l_1982,&l_1982,(void*)0,&l_1982}},{{(void*)0,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982},{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982}},{{&l_1982,&l_1982,(void*)0,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,(void*)0,&l_1982},{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,(void*)0,(void*)0}},{{&l_1982,&l_1982,&l_1982,(void*)0,&l_1982,&l_1982,&l_1982,(void*)0,&l_1982,&l_1982},{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982}},{{(void*)0,&l_1982,&l_1982,(void*)0,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982},{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982}},{{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982},{&l_1982,(void*)0,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982}},{{&l_1982,&l_1982,(void*)0,(void*)0,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982},{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,(void*)0,&l_1982,&l_1982}},{{&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982,&l_1982},{&l_1982,&l_1982,&l_1982,&l_1982,(void*)0,&l_1982,&l_1982,(void*)0,(void*)0,(void*)0}}};
    const int16_t *l_1986 = &g_422;
    const int16_t **l_1985 = &l_1986;
    const int16_t ***l_1984 = &l_1985;
    int32_t l_2101 = 0L;
    uint32_t l_2143 = 0x799B5C0FL;
    uint32_t l_2289 = 3UL;
    uint8_t * const l_2292 = &g_511[1];
    const int16_t ** const *l_2316 = &l_1985;
    const int16_t ** const **l_2315 = &l_2316;
    const int16_t ** const ***l_2314 = &l_2315;
    int32_t * const ** const *l_2325[7];
    int32_t * const ** const **l_2324 = &l_2325[5];
    int64_t l_2350 = 3L;
    uint16_t l_2355 = 0UL;
    int32_t *l_2420 = &g_4;
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_1975[i] = (-4L);
    for (i = 0; i < 7; i++)
        l_2325[i] = (void*)0;
    return l_2420;
}


/* ------------------------------------------ */
/* 
 * reads : g_1908 g_1457 g_1458 g_627 g_156 g_66 g_172 g_859 g_181 g_147
 * writes: g_1908 g_628 g_168 g_171 g_172 g_1916 g_148
 */
static int32_t * func_34(int32_t * p_35, int16_t * p_36, int16_t  p_37, int64_t  p_38)
{ /* block id: 705 */
    int32_t **l_1892 = &g_1429;
    int32_t ***l_1891[3];
    int32_t l_1898 = 0xE930637BL;
    int32_t l_1905[7];
    int8_t *l_1914 = &g_405;
    int8_t **l_1915[7][2][8] = {{{&l_1914,(void*)0,(void*)0,&l_1914,(void*)0,(void*)0,&l_1914,(void*)0},{&l_1914,(void*)0,&l_1914,&l_1914,&l_1914,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_1914,&l_1914,&l_1914,&l_1914,(void*)0,&l_1914,&l_1914},{&l_1914,&l_1914,&l_1914,(void*)0,(void*)0,&l_1914,&l_1914,&l_1914}},{{&l_1914,(void*)0,(void*)0,(void*)0,&l_1914,&l_1914,(void*)0,&l_1914},{(void*)0,&l_1914,(void*)0,&l_1914,(void*)0,(void*)0,(void*)0,(void*)0}},{{&l_1914,(void*)0,(void*)0,&l_1914,&l_1914,&l_1914,(void*)0,(void*)0},{(void*)0,&l_1914,(void*)0,&l_1914,&l_1914,&l_1914,&l_1914,&l_1914}},{{&l_1914,&l_1914,&l_1914,(void*)0,(void*)0,&l_1914,&l_1914,&l_1914},{&l_1914,(void*)0,&l_1914,(void*)0,(void*)0,(void*)0,(void*)0,&l_1914}},{{&l_1914,&l_1914,&l_1914,&l_1914,(void*)0,&l_1914,&l_1914,(void*)0},{&l_1914,(void*)0,(void*)0,&l_1914,&l_1914,&l_1914,(void*)0,(void*)0}},{{(void*)0,&l_1914,&l_1914,&l_1914,&l_1914,(void*)0,&l_1914,&l_1914},{&l_1914,&l_1914,(void*)0,(void*)0,(void*)0,(void*)0,&l_1914,&l_1914}}};
    uint16_t *l_1921[1];
    int64_t l_1922 = 1L;
    int64_t l_1923 = 0x6168FCE8F159A7A4LL;
    float l_1924 = 0x7.8E6009p+30;
    uint16_t l_1925 = 0xDCC3L;
    int32_t * const ***l_1933 = (void*)0;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1891[i] = &l_1892;
    for (i = 0; i < 7; i++)
        l_1905[i] = 0L;
    for (i = 0; i < 1; i++)
        l_1921[i] = &g_183;
    if ((l_1891[2] == &g_1428))
    { /* block id: 706 */
        int32_t *l_1893 = &g_180;
        int32_t *l_1894 = &g_403;
        int32_t *l_1895 = &g_403;
        int32_t l_1896[9][8] = {{2L,0xC28394FEL,0xE8A3CC3CL,0L,0xC28394FEL,0L,0xE8A3CC3CL,0xC28394FEL},{0xAEDE07A7L,0xE8A3CC3CL,2L,0xAEDE07A7L,0L,0L,0xAEDE07A7L,2L},{0xC28394FEL,0xC28394FEL,0L,0x934BF1C5L,0xAEDE07A7L,0L,0xAEDE07A7L,0x934BF1C5L},{2L,0x934BF1C5L,2L,0L,0x934BF1C5L,0xE8A3CC3CL,0xE8A3CC3CL,0x934BF1C5L},{0x934BF1C5L,0xE8A3CC3CL,0xE8A3CC3CL,0x934BF1C5L,0L,2L,0x934BF1C5L,2L},{0x934BF1C5L,0xAEDE07A7L,0L,0xAEDE07A7L,0x934BF1C5L,0L,0xC28394FEL,0xC28394FEL},{2L,0xAEDE07A7L,0L,0L,0xAEDE07A7L,2L,0xE8A3CC3CL,0L},{2L,0L,0xAF4D00C7L,2L,0xAF4D00C7L,0L,2L,0xC589A770L},{0L,0xE8A3CC3CL,0xC28394FEL,2L,2L,0xC28394FEL,0xE8A3CC3CL,0L}};
        int32_t *l_1897 = &l_1896[0][1];
        int32_t *l_1899 = (void*)0;
        int32_t *l_1900 = &l_1898;
        int32_t *l_1901 = &g_403;
        int32_t *l_1902 = &g_403;
        int32_t *l_1903 = &l_1896[4][6];
        int32_t *l_1904[6][10] = {{&l_1896[1][3],&g_180,&l_1896[1][3],&g_180,&l_1896[1][3],&g_180,&l_1898,&g_403,&g_4,&l_1898},{&g_180,&l_1898,&g_403,&g_4,&l_1898,(void*)0,&g_174,&g_174,(void*)0,&l_1898},{&l_1896[1][3],&g_4,&g_4,&l_1896[1][3],&l_1896[1][3],&l_1896[4][3],(void*)0,&g_180,&g_174,&l_1898},{&l_1896[6][0],&l_1896[1][3],&l_1896[1][7],(void*)0,&g_174,(void*)0,&g_4,(void*)0,&g_174,(void*)0},{&l_1898,&l_1896[6][0],&l_1898,&l_1896[1][3],&g_174,&g_4,&g_174,&l_1896[5][5],(void*)0,(void*)0},{(void*)0,&g_4,&l_1896[1][3],&g_4,&l_1896[5][5],&l_1898,&l_1898,&l_1896[5][5],&g_4,&l_1896[1][3]}};
        float l_1907 = 0x0.Cp-1;
        int i, j;
        ++g_1908;
    }
    else
    { /* block id: 708 */
        uint16_t l_1911 = 0xF912L;
        int32_t *l_1912 = &l_1905[2];
        int16_t *l_1913 = &g_1197;
        (*l_1912) |= l_1911;
        (***g_1457) = p_35;
        (**g_1458) = func_126(p_38, p_38, l_1913, (*l_1912));
    }
    l_1898 |= (((((g_1916 = l_1914) == &g_323) , ((((*g_859) , 0xCB97L) , (safe_rshift_func_int16_t_s_s((p_37 , ((safe_add_func_uint16_t_u_u((((++l_1925) ^ (safe_mod_func_int64_t_s_s((((void*)0 != &l_1914) & 1L), ((~(safe_mod_func_uint16_t_u_u(((void*)0 != l_1933), 2UL))) ^ (*g_859))))) <= 0x90858682E495F642LL), l_1905[6])) < 8L)), l_1905[4]))) == (*g_859))) | (*g_859)) && p_37);
    (*g_147) = &l_1905[6];
    return p_35;
}


/* ------------------------------------------ */
/* 
 * reads : g_1411 g_1412 g_281 g_859 g_181 g_4 g_726 g_627 g_628 g_1457 g_1458 g_403 g_1532 g_156 g_66 g_172 g_603 g_833 g_464 g_952 g_580 g_1196 g_1197 g_542 g_413 g_305 g_1179 g_119 g_422 g_245 g_180 g_224 g_598 g_1060 g_1061 g_530 g_1195 g_1756 g_1233 g_1234 g_1198 g_1783 g_397 g_1428 g_1429 g_191 g_1815 g_147 g_148 g_543 g_1871 g_171
 * writes: g_281 g_403 g_168 g_171 g_172 g_181 g_603 g_952 g_598 g_543 g_1197 g_580 g_305 g_180 g_323 g_628 g_1532 g_1179 g_156 g_397 g_191 g_404 g_1813 g_1814 g_1195
 */
static int16_t * func_40(uint64_t  p_41, uint64_t  p_42, uint32_t  p_43)
{ /* block id: 5 */
    int32_t l_64[9][2] = {{0x5738F5E6L,0x3783EA6DL},{0x21BA827CL,0x3783EA6DL},{0x5738F5E6L,0x21BA827CL},{0L,0L},{0L,0x21BA827CL},{0x5738F5E6L,0x3783EA6DL},{0x21BA827CL,0x3783EA6DL},{0x5738F5E6L,0x21BA827CL},{0L,0L}};
    int32_t *l_88 = &g_4;
    int32_t **l_87 = &l_88;
    uint8_t l_1227 = 0UL;
    int32_t l_1262 = 0xC07FD365L;
    int8_t l_1264 = 0x59L;
    int8_t l_1267 = 0xB5L;
    int32_t *l_1276[8][8][4] = {{{&g_180,&g_403,&g_180,(void*)0},{&l_64[0][0],&l_64[4][1],&g_174,&g_4},{&g_180,&g_180,&g_180,&l_64[4][1]},{&g_4,&g_403,&g_180,&g_4},{&g_180,&g_4,&g_174,&l_64[5][1]},{&l_64[0][0],&l_64[5][1],&g_180,&g_4},{&g_180,&g_4,(void*)0,&g_403},{&g_403,&g_403,&l_64[7][1],&g_403}},{{&g_174,&g_403,&l_64[5][1],&g_403},{&g_403,&g_4,(void*)0,&g_4},{&l_64[5][1],&l_64[0][0],&g_174,&l_64[5][1]},{(void*)0,&g_4,&l_64[5][1],&g_403},{(void*)0,&g_403,&g_403,&g_4},{&l_64[5][1],&g_180,(void*)0,&g_4},{(void*)0,&g_4,&g_4,(void*)0},{&g_4,&g_403,(void*)0,&g_180}},{{&g_403,&g_4,(void*)0,&g_180},{&g_174,&g_4,&l_64[5][1],&g_180},{&g_403,&g_4,(void*)0,&g_180},{&g_180,&g_403,&g_180,(void*)0},{&l_64[0][0],&g_4,(void*)0,&g_4},{&g_180,&g_180,&g_180,&g_4},{&g_4,&g_403,&l_64[2][1],&g_403},{&g_180,&g_4,(void*)0,&l_64[5][1]}},{{(void*)0,(void*)0,&g_180,&g_174},{&g_4,&g_4,&g_403,&l_64[5][1]},{&l_64[5][1],&l_64[5][1],(void*)0,&g_403},{&g_4,&g_4,&g_174,(void*)0},{&g_4,&g_4,&g_180,&g_4},{(void*)0,(void*)0,&l_64[7][1],(void*)0},{&g_180,&g_4,(void*)0,(void*)0},{(void*)0,&g_180,&g_4,&l_64[5][1]}},{{(void*)0,&g_180,(void*)0,(void*)0},{&g_180,&l_64[5][1],&l_64[7][1],&g_180},{(void*)0,&l_64[5][1],&g_180,(void*)0},{&g_4,&l_64[5][1],&g_174,&g_180},{&g_4,&l_64[5][1],(void*)0,&l_64[2][1]},{&l_64[5][1],(void*)0,&g_403,(void*)0},{&g_4,(void*)0,&g_180,&g_180},{(void*)0,&l_64[5][1],&g_180,&l_64[5][1]}},{{&g_180,&g_4,&l_64[5][1],&l_64[5][1]},{&g_4,(void*)0,&g_174,&l_64[5][1]},{&l_64[2][1],&g_4,&g_180,&g_180},{(void*)0,(void*)0,&l_64[5][1],&g_4},{&g_4,&g_174,&l_64[4][1],&l_64[5][1]},{(void*)0,&g_4,(void*)0,&l_64[4][1]},{(void*)0,&g_4,&g_403,&l_64[5][1]},{&g_4,&g_174,&g_180,&g_4}},{{&g_174,(void*)0,&l_64[7][1],&g_180},{&g_180,&g_4,&g_180,&l_64[5][1]},{(void*)0,(void*)0,&l_64[5][1],&l_64[5][1]},{&g_180,&g_4,(void*)0,&l_64[5][1]},{&g_180,&l_64[5][1],&l_64[0][0],&g_180},{(void*)0,(void*)0,&g_180,(void*)0},{&l_64[5][1],(void*)0,&g_174,&l_64[2][1]},{(void*)0,&l_64[5][1],&g_174,&g_180}},{{&l_64[5][1],&l_64[5][1],&l_64[4][1],(void*)0},{&g_180,&l_64[5][1],&g_180,&g_180},{(void*)0,&l_64[5][1],(void*)0,(void*)0},{&g_180,&g_180,&g_174,&l_64[5][1]},{&g_174,&g_180,&g_174,(void*)0},{&g_180,&g_4,(void*)0,(void*)0},{(void*)0,(void*)0,&g_180,&g_4},{&g_180,&g_4,&l_64[4][1],(void*)0}}};
    int16_t *l_1418[3];
    int16_t *l_1420 = &g_404[0][1];
    const int32_t *l_1431 = &g_191[0];
    const int32_t **l_1430 = &l_1431;
    uint64_t l_1480 = 0UL;
    int8_t l_1486 = 0x9FL;
    float l_1530 = 0x3.2p-1;
    int8_t l_1531 = (-8L);
    int8_t l_1689 = 0x5DL;
    uint32_t l_1706 = 0UL;
    float l_1717 = 0xD.8D454Cp-33;
    const uint32_t l_1721 = 0UL;
    uint64_t * const *l_1742[5] = {&g_543,&g_543,&g_543,&g_543,&g_543};
    uint64_t * const ** const l_1741 = &l_1742[0];
    const uint64_t *l_1745 = (void*)0;
    const uint64_t **l_1744 = &l_1745;
    const uint64_t ***l_1743 = &l_1744;
    int64_t **l_1749 = &g_859;
    uint16_t **l_1876 = &g_260[0][1][0];
    int8_t *l_1887 = &g_171;
    int64_t l_1890[4];
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1418[i] = &g_1197;
    for (i = 0; i < 4; i++)
        l_1890[i] = 0x76FF81E289D65201LL;
    for (p_42 = 8; (p_42 != 4); p_42 = safe_sub_func_uint16_t_u_u(p_42, 2))
    { /* block id: 8 */
        int16_t *l_65 = &g_66;
        float l_91[7];
        const int16_t **l_1221 = (void*)0;
        const int16_t ***l_1220 = &l_1221;
        int32_t l_1224 = 0x55057118L;
        int32_t l_1235 = 0xE4E0C746L;
        int32_t l_1260 = (-6L);
        int32_t l_1263 = 0x3F899808L;
        int32_t l_1265 = 0L;
        int32_t l_1266 = 0x4CCCE9DFL;
        int32_t l_1268 = (-7L);
        int32_t l_1270 = 0L;
        int32_t l_1272 = (-10L);
        uint64_t l_1374 = 0x2DD46FC780D808EELL;
        int32_t *l_1375 = &l_1268;
        const uint64_t *l_1450[5][7][7] = {{{(void*)0,&g_321[6][0],&g_668,&l_1374,&l_1374,&g_668,&g_321[6][0]},{&l_1374,&g_603[1][0][1],&g_603[5][0][0],&l_1374,&g_321[6][0],&g_603[1][0][0],&g_603[1][0][1]},{&g_603[1][0][1],(void*)0,&g_668,&g_397,&l_1374,&g_668,&g_668},{&g_603[4][0][1],&g_321[1][2],&l_1374,&l_1374,&g_397,&g_321[6][0],&g_603[2][0][1]},{&g_321[6][0],&g_668,&g_603[1][0][1],&g_397,&g_321[6][0],&g_668,&l_1374},{&g_603[1][0][1],(void*)0,&g_603[1][0][1],&g_321[6][0],&l_1374,&g_397,&l_1374},{&g_321[6][0],&g_668,&g_668,&g_321[6][0],&g_321[7][1],&g_397,&g_603[1][0][1]}},{{&g_668,&g_397,&g_603[4][0][1],(void*)0,&g_321[6][0],&g_397,&g_603[1][0][0]},{&g_668,&g_321[7][1],&g_603[1][0][1],&g_668,&g_321[6][0],&g_668,&g_603[1][0][1]},{&g_603[1][0][1],&g_603[1][0][1],&l_1374,&g_603[5][0][0],&g_397,&g_603[2][0][1],&l_1374},{&g_603[1][0][1],&g_603[2][0][1],(void*)0,&g_668,&g_668,(void*)0,&l_1374},{&g_603[2][0][1],&g_668,&g_397,&l_1374,&g_397,&g_668,&g_603[2][0][1]},{&l_1374,&g_668,&g_321[6][0],(void*)0,&g_321[6][0],&l_1374,&g_668},{&g_603[1][0][0],&g_603[4][0][1],&g_603[1][0][1],&g_603[1][0][1],&g_321[6][0],&g_603[1][0][1],&g_603[1][0][1]}},{{&g_321[7][1],&g_668,&g_321[6][0],&g_668,&g_321[7][1],&g_603[1][0][1],&g_603[2][0][1]},{&l_1374,&g_603[5][0][0],&g_397,&g_603[2][0][1],&l_1374,&g_321[6][0],&g_321[6][0]},{&g_668,(void*)0,(void*)0,&g_668,&g_321[6][0],&g_321[6][0],&g_668},{&l_1374,&g_603[2][0][1],&l_1374,&g_668,&g_397,&g_603[4][0][1],(void*)0},{&g_321[7][1],&g_397,&g_603[1][0][1],&g_603[1][0][1],&g_668,&g_603[2][0][1],&l_1374},{&g_603[1][0][0],&l_1374,&g_603[4][0][1],&g_668,&g_668,&g_603[4][0][1],&l_1374},{&l_1374,&g_321[6][0],&g_668,&g_603[1][0][1],&g_397,&g_321[6][0],&g_668}},{{&g_603[2][0][1],&g_603[1][0][1],&g_603[1][0][1],&g_397,(void*)0,&g_321[6][0],&g_603[1][0][1]},{&g_603[1][0][1],&l_1374,&g_603[1][0][1],&g_603[1][0][1],&l_1374,&g_603[1][0][1],&g_668},{&g_603[1][0][1],&g_603[1][0][1],&g_603[1][0][0],&g_668,&g_668,&g_603[1][0][1],&g_397},{&g_668,&g_668,&g_321[6][0],&g_603[1][0][1],(void*)0,&l_1374,(void*)0},{&g_668,&g_603[1][0][1],&g_603[1][0][1],&g_668,&g_603[1][0][1],&g_668,&g_321[6][0]},{&g_321[6][0],&l_1374,(void*)0,&g_668,&l_1374,(void*)0,&g_603[1][0][1]},{&g_603[1][0][1],&g_603[1][0][1],&g_321[6][0],&g_603[2][0][1],&g_603[1][0][1],&g_603[2][0][1],&g_321[6][0]}},{{&g_321[6][0],&g_321[6][0],&g_603[2][0][1],&g_668,(void*)0,&g_668,(void*)0},{&g_321[6][0],&l_1374,&l_1374,&g_603[1][0][1],&g_603[1][0][1],&g_397,&g_397},{&g_668,&g_397,(void*)0,(void*)0,(void*)0,&g_397,(void*)0},{&g_668,&g_603[4][0][1],&l_1374,&g_603[1][0][0],&g_603[1][0][1],(void*)0,&g_603[1][0][1]},{&g_321[6][0],&g_603[1][0][1],&g_668,&g_321[6][0],&g_397,&l_1374,&l_1374},{&g_603[5][0][0],&g_603[1][0][1],&l_1374,&g_603[1][0][1],&g_603[5][0][0],&l_1374,&g_321[6][0]},{&l_1374,&g_668,&g_321[7][1],(void*)0,&g_603[1][0][1],&g_668,&g_397}}};
        const uint64_t **l_1449 = &l_1450[1][5][3];
        const uint64_t ***l_1448 = &l_1449;
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_91[i] = 0x0.6p-1;
    }
    if ((l_1486 || ((((((safe_rshift_func_uint8_t_u_u(0x19L, ((**g_1411)--))) , p_42) && ((((safe_mod_func_uint64_t_u_u((((safe_mod_func_int64_t_s_s((*g_859), (safe_rshift_func_uint8_t_u_s((safe_div_func_int16_t_s_s((!0L), p_43)), 5)))) , (safe_rshift_func_uint16_t_u_s((0xACA2L ^ (0x4073EFA9B1D1067FLL == ((((safe_mul_func_float_f_f((-0x10.Ep-1), 0xD.F73FD8p+30)) < (*l_88)) , p_41) , (**l_87)))), p_42))) & 0xF314L), 0x6ED8C70F3C628E7DLL)) > p_41) ^ p_42) && g_181)) & (*g_859)) || g_726) < p_43)))
    { /* block id: 540 */
        int16_t l_1514 = 0xCFE1L;
        int32_t l_1515 = 1L;
        uint16_t **l_1550[4];
        uint32_t *l_1563 = &g_172[0];
        int32_t l_1567 = 0x75EA0C72L;
        uint8_t l_1596[8][8] = {{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL},{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL},{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL},{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL},{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL},{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL},{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL},{254UL,254UL,0xDDL,0xDDL,254UL,254UL,0xDDL,0xDDL}};
        uint64_t *l_1612 = &g_397;
        int32_t l_1635 = (-1L);
        int32_t l_1636 = 0x49C5E2E3L;
        float l_1637 = 0xA.ACB947p-69;
        int32_t l_1639 = (-3L);
        int32_t l_1642[7][7][5] = {{{1L,1L,(-1L),0L,0xEBDE7974L},{0x8B16E830L,0x2E8A7F9EL,(-1L),0x0FA1D982L,7L},{7L,(-1L),9L,(-1L),7L},{0L,0x2E8A7F9EL,0xEBDE7974L,7L,1L},{0L,1L,7L,2L,2L},{7L,(-1L),7L,0x2E8A7F9EL,1L},{0x8B16E830L,2L,1L,0x2E8A7F9EL,7L}},{{1L,7L,2L,2L,7L},{7L,0x74BB47CCL,1L,7L,0xEBDE7974L},{(-1L),0x74BB47CCL,7L,(-1L),9L},{0x0FA1D982L,7L,7L,0x0FA1D982L,(-1L)},{(-1L),2L,0xEBDE7974L,0L,(-1L)},{7L,(-1L),9L,1L,9L},{1L,1L,(-1L),0L,0xEBDE7974L}},{{0x8B16E830L,0x2E8A7F9EL,(-1L),0x0FA1D982L,7L},{7L,(-1L),9L,(-1L),7L},{0L,0x2E8A7F9EL,0xEBDE7974L,7L,1L},{0L,1L,7L,2L,2L},{7L,(-1L),7L,0x2E8A7F9EL,1L},{0x8B16E830L,2L,1L,0x74BB47CCL,2L},{(-1L),0x0FA1D982L,7L,7L,0x0FA1D982L}},{{0x0FA1D982L,0xEBDE7974L,(-1L),2L,9L},{0L,0xEBDE7974L,2L,(-1L),0x8B16E830L},{1L,0x0FA1D982L,0x0FA1D982L,1L,(-1L)},{0L,7L,9L,0x2E8A7F9EL,(-1L)},{0x0FA1D982L,0L,0x8B16E830L,(-1L),0x8B16E830L},{(-1L),(-1L),(-1L),0x2E8A7F9EL,9L},{7L,0x74BB47CCL,(-1L),1L,0x0FA1D982L}},{{2L,(-1L),0x8B16E830L,(-1L),2L},{0x2E8A7F9EL,0x74BB47CCL,9L,2L,(-1L)},{0x2E8A7F9EL,(-1L),0x0FA1D982L,7L,7L},{2L,0L,2L,0x74BB47CCL,(-1L)},{7L,7L,(-1L),0x74BB47CCL,2L},{(-1L),0x0FA1D982L,7L,7L,0x0FA1D982L},{0x0FA1D982L,0xEBDE7974L,(-1L),2L,9L}},{{0L,0xEBDE7974L,2L,(-1L),0x8B16E830L},{1L,0x0FA1D982L,0x0FA1D982L,1L,(-1L)},{0L,7L,9L,0x2E8A7F9EL,(-1L)},{0x0FA1D982L,0L,0x8B16E830L,(-1L),0x8B16E830L},{(-1L),(-1L),(-1L),0x2E8A7F9EL,9L},{7L,0x74BB47CCL,(-1L),1L,0x0FA1D982L},{2L,(-1L),0x8B16E830L,(-1L),2L}},{{0x2E8A7F9EL,0x74BB47CCL,9L,2L,(-1L)},{0x2E8A7F9EL,(-1L),0x0FA1D982L,7L,7L},{2L,0L,2L,0x74BB47CCL,(-1L)},{7L,7L,(-1L),0x74BB47CCL,2L},{(-1L),0x0FA1D982L,7L,7L,0x0FA1D982L},{0x0FA1D982L,0xEBDE7974L,(-1L),2L,9L},{0L,0xEBDE7974L,2L,(-1L),0x8B16E830L}}};
        int16_t *l_1656 = &g_245[2][1][3];
        float l_1657 = 0x0.9p-1;
        uint8_t l_1695 = 0x79L;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1550[i] = &g_260[9][0][1];
        (*l_87) = func_126(p_42, (safe_div_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u((((safe_mod_func_uint8_t_u_u((safe_mod_func_int64_t_s_s(((((safe_sub_func_int64_t_s_s((*g_859), (p_41 <= (l_1515 = l_1514)))) == (safe_lshift_func_int16_t_s_s(((safe_mul_func_int8_t_s_s(((safe_mod_func_int16_t_s_s(0xCE22L, (safe_mul_func_int16_t_s_s(((safe_add_func_int64_t_s_s(((p_42 & (((-3L) >= (l_1514 != (((**g_627) = (safe_lshift_func_int8_t_s_u(((safe_sub_func_uint64_t_u_u(l_1514, l_1514)) , 0x25L), l_1514))) , (****g_1457)))) ^ p_41)) , 0x25F684E96853BC89LL), p_41)) && l_1531), p_43)))) , 0xA6L), l_1514)) | 4294967295UL), 2))) , 1UL) < p_42), g_1532)), g_181)) != (-1L)) != (*g_859)), p_41)) <= 4294967289UL), 251UL)), l_1418[2], p_41);
        for (g_181 = 0; (g_181 == 24); g_181 = safe_add_func_uint64_t_u_u(g_181, 7))
        { /* block id: 546 */
            uint64_t *l_1535 = &g_603[5][0][0];
            int16_t *l_1551 = (void*)0;
            int8_t *l_1561 = &l_1531;
            uint16_t l_1562 = 0x3490L;
            int64_t *l_1564[10] = {&g_952[1][3][0],&g_1179,&g_1179,&g_952[1][3][0],&g_1179,&g_1179,&g_952[1][3][0],&g_1179,&g_1179,&g_952[1][3][0]};
            int i;
            (****g_1457) ^= ((((--(*l_1535)) == (g_952[1][3][0] |= (safe_sub_func_int64_t_s_s((safe_rshift_func_int16_t_s_u(((safe_mul_func_uint8_t_u_u(255UL, (safe_sub_func_int8_t_s_s(((safe_rshift_func_uint8_t_u_u(((**g_1411) = p_43), (((safe_sub_func_uint16_t_u_u((l_1550[1] != (void*)0), p_43)) >= (((&l_1514 != l_1551) <= (safe_lshift_func_int8_t_s_s((safe_div_func_int64_t_s_s(0xF31E1AC29B8B4E40LL, ((+(l_1515 = ((((safe_add_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_u(((*l_1561) &= ((((g_833[4][0][1] | l_1514) || 4294967290UL) , (void*)0) != (void*)0)), (**l_87))) , p_41), l_1562)) , l_1563) != (void*)0) , (*g_859)))) , p_42))), g_464))) >= (*l_88))) > 18446744073709551612UL))) != l_1514), p_42)))) > (*g_859)), 13)), p_41)))) != p_41) == 0x8BF3L);
        }
        for (l_1486 = 6; (l_1486 > 3); l_1486 = safe_sub_func_int16_t_s_s(l_1486, 4))
        { /* block id: 556 */
            uint32_t l_1568 = 4294967293UL;
            uint8_t *l_1579 = &g_119[6][6];
            int32_t l_1580 = 0x7F714DE8L;
            uint16_t l_1605 = 0x809EL;
            int8_t l_1613 = 0xAAL;
            uint64_t l_1614[10];
            int32_t l_1625 = 0xB3F88110L;
            int32_t l_1626 = 0x448C8F2BL;
            int32_t l_1629 = 0xBC4BA62DL;
            int32_t l_1633 = 5L;
            int32_t l_1634[5][8] = {{0x4E18B96EL,0xAFF99305L,0x36AC4D98L,0L,0x36AC4D98L,0xAFF99305L,0x4E18B96EL,0L},{0xAFF99305L,(-1L),0xC648153CL,0xB0E38BC8L,0x4233CC0DL,0L,0L,0x4233CC0DL},{0x0BFA6F4CL,0x4E18B96EL,0x4E18B96EL,0x0BFA6F4CL,0x4233CC0DL,(-7L),0L,0L},{0xAFF99305L,(-4L),3L,0x4233CC0DL,0x36AC4D98L,0x4233CC0DL,3L,(-4L)},{0x4E18B96EL,0xC648153CL,0x4233CC0DL,(-1L),0x36AC4D98L,(-4L),(-7L),(-7L)}};
            uint32_t l_1645[8] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
            uint32_t l_1661[4][4] = {{0x477A9BA5L,0x803C0E48L,0x477A9BA5L,0x477A9BA5L},{0x803C0E48L,0x803C0E48L,0UL,0x803C0E48L},{0x803C0E48L,0x477A9BA5L,0x477A9BA5L,0x803C0E48L},{0x477A9BA5L,0x803C0E48L,0x477A9BA5L,0x477A9BA5L}};
            int i, j;
            for (i = 0; i < 10; i++)
                l_1614[i] = 0xCE363D71CB18B481LL;
            l_1568--;
            for (g_598 = (-18); (g_598 != 34); g_598++)
            { /* block id: 560 */
                int16_t ***l_1590[8][5] = {{&g_1198,&g_1061,(void*)0,&g_1061,&g_1198},{&g_1061,&g_1198,(void*)0,&g_1198,&g_1198},{&g_1198,(void*)0,(void*)0,&g_1198,&g_1198},{&g_1061,&g_1198,(void*)0,&g_1198,&g_1198},{&g_1061,&g_1198,&g_1061,&g_1198,&g_1198},{&g_1198,(void*)0,&g_1198,&g_1198,&g_1198},{&g_1198,&g_1198,(void*)0,&g_1198,&g_1061},{&g_1061,&g_1061,&g_1198,&g_1198,&g_1061}};
                int16_t ****l_1591 = &l_1590[1][0];
                const int32_t l_1597[9][4] = {{0x193BF483L,1L,0x5295C074L,0x5295C074L},{0x62D8FA7DL,0x62D8FA7DL,0x193BF483L,0x5295C074L},{0xFB4E4097L,1L,0xFB4E4097L,0x193BF483L},{0xFB4E4097L,0x193BF483L,0x193BF483L,0xFB4E4097L},{0x62D8FA7DL,0x193BF483L,0x5295C074L,0x193BF483L},{0x193BF483L,1L,0x5295C074L,0x5295C074L},{0x62D8FA7DL,0x62D8FA7DL,0x193BF483L,0x5295C074L},{0xFB4E4097L,1L,0xFB4E4097L,0x193BF483L},{0xFB4E4097L,0x193BF483L,0x193BF483L,0xFB4E4097L}};
                int32_t l_1616 = 0x93451013L;
                int32_t l_1624 = 0x4F38B594L;
                int32_t l_1630[6] = {(-2L),(-5L),(-5L),(-2L),(-5L),(-5L)};
                uint8_t l_1658 = 0x24L;
                int8_t *l_1680 = &l_1264;
                int8_t *l_1681 = &l_1531;
                int i, j;
                if ((safe_sub_func_int32_t_s_s(p_42, ((safe_div_func_uint32_t_u_u((((safe_lshift_func_uint8_t_u_u(((l_1580 ^= (l_1579 == &l_1227)) || (safe_sub_func_int64_t_s_s((safe_add_func_int16_t_s_s((((safe_add_func_int16_t_s_s((+(((((safe_lshift_func_uint16_t_u_s((((((&g_1195 != ((*l_1591) = l_1590[4][0])) <= (((((safe_lshift_func_int8_t_s_u((p_41 ^ (safe_div_func_uint64_t_u_u(p_41, l_1568))), 3)) , (l_1596[3][3] &= (((*g_859) = (0xACL != 249UL)) <= (**l_87)))) | (*l_88)) , 0x666F17C2L) >= g_580)) & (*g_1196)) == p_41) || g_172[1]), (*l_88))) , l_1596[1][0]) > (-1L)) , p_42) , p_41)), 0x0AADL)) <= 0xC191D76BL) || (*g_859)), (**l_87))), p_43))), l_1597[3][3])) != l_1597[3][3]) ^ 1L), p_41)) , p_43))))
                { /* block id: 565 */
                    uint8_t l_1615[1][3][4] = {{{251UL,1UL,1UL,251UL},{1UL,251UL,1UL,1UL},{251UL,251UL,0x69L,251UL}}};
                    int i, j, k;
                    l_1616 |= (safe_mul_func_uint8_t_u_u(((*g_1412) = (!l_1597[4][2])), (!(((((*l_88) > ((((!(((safe_div_func_int64_t_s_s((l_1605 , (l_1605 & (((*l_1563) = ((((((*g_1196) = (0x88E32E7D6BDEDE15LL || (((safe_rshift_func_uint16_t_u_u(((safe_div_func_int8_t_s_s(l_1596[7][1], (safe_div_func_int32_t_s_s(((****g_1457) = (((*g_542) = l_1612) == (void*)0)), p_43)))) , p_43), l_1613)) > g_726) > (-9L)))) <= p_43) > 3UL) & 0UL) >= l_1614[7])) ^ l_1596[3][4]))), p_43)) && (*g_1196)) < 0x2B94B9B00D86C24CLL)) & 0L) >= l_1615[0][1][0]) >= p_43)) , 0x2E194B11B065D6A7LL) , g_833[5][2][0]) != (*l_88)))));
                }
                else
                { /* block id: 572 */
                    float l_1620 = 0x4.5p+1;
                    int32_t l_1621 = 0L;
                    int32_t l_1623[7][9] = {{0xF2974550L,0xD5812EEEL,0x8B27D329L,0xDFCAE12CL,7L,0x71F1CC94L,7L,0xDFCAE12CL,0x8B27D329L},{0xB7B0185EL,0xB7B0185EL,0x25430D49L,5L,0xF2974550L,0xD5812EEEL,0x8B27D329L,0xDFCAE12CL,7L},{(-1L),2L,0x71F1CC94L,0xB7B0185EL,(-3L),(-3L),0xB7B0185EL,0x71F1CC94L,2L},{(-3L),0x5E4DA348L,0x25430D49L,(-1L),0L,5L,0xB7B0185EL,0x8B27D329L,0L},{0xDFCAE12CL,(-1L),0x8B27D329L,2L,0x1BD31F1EL,2L,0x8B27D329L,(-1L),0xDFCAE12CL},{0L,0x5E4DA348L,0xD5812EEEL,0x1BD31F1EL,0xB7B0185EL,2L,7L,0x25430D49L,(-1L)},{0x5E4DA348L,2L,5L,0x71F1CC94L,0x71F1CC94L,5L,2L,0x5E4DA348L,(-1L)}};
                    int16_t l_1644 = 8L;
                    uint32_t *l_1652 = &g_224[1][6][1];
                    uint32_t **l_1651 = &l_1652;
                    int i, j;
                    for (g_580 = 0; (g_580 > 48); ++g_580)
                    { /* block id: 575 */
                        uint16_t ***l_1619 = &l_1550[1];
                        int64_t l_1622[10][10] = {{0xB3C9C7EAC68B0437LL,0xF53919EB36F82E76LL,0xF53919EB36F82E76LL,0x2F99FA4515B887DCLL,0xB19481273C8C9194LL,4L,0xACA770EC4C9601F3LL,1L,4L,0x77320C8D7A08E35FLL},{4L,0xACA770EC4C9601F3LL,1L,4L,0x77320C8D7A08E35FLL,4L,1L,0xACA770EC4C9601F3LL,4L,0xB19481273C8C9194LL},{0x2F99FA4515B887DCLL,0xACA770EC4C9601F3LL,0xB3C9C7EAC68B0437LL,0x2F99FA4515B887DCLL,0x77320C8D7A08E35FLL,4L,0xACA770EC4C9601F3LL,0xACA770EC4C9601F3LL,4L,0x77320C8D7A08E35FLL},{0x2F99FA4515B887DCLL,1L,1L,0x2F99FA4515B887DCLL,0xB19481273C8C9194LL,4L,0xACA770EC4C9601F3LL,1L,4L,0x77320C8D7A08E35FLL},{4L,0xACA770EC4C9601F3LL,1L,4L,0x77320C8D7A08E35FLL,4L,1L,0xACA770EC4C9601F3LL,4L,0xB19481273C8C9194LL},{0x2F99FA4515B887DCLL,0xACA770EC4C9601F3LL,0xB3C9C7EAC68B0437LL,0x2F99FA4515B887DCLL,0x77320C8D7A08E35FLL,4L,0xACA770EC4C9601F3LL,0xACA770EC4C9601F3LL,4L,0x77320C8D7A08E35FLL},{0x2F99FA4515B887DCLL,1L,1L,0x2F99FA4515B887DCLL,0xB19481273C8C9194LL,4L,0xACA770EC4C9601F3LL,1L,4L,0x77320C8D7A08E35FLL},{4L,0xACA770EC4C9601F3LL,1L,4L,0x77320C8D7A08E35FLL,4L,1L,0xACA770EC4C9601F3LL,4L,0xB19481273C8C9194LL},{0x2F99FA4515B887DCLL,0xACA770EC4C9601F3LL,0xB3C9C7EAC68B0437LL,0x2F99FA4515B887DCLL,0x77320C8D7A08E35FLL,4L,0xACA770EC4C9601F3LL,0xACA770EC4C9601F3LL,4L,0x77320C8D7A08E35FLL},{0x2F99FA4515B887DCLL,1L,1L,0x2F99FA4515B887DCLL,0xB19481273C8C9194LL,4L,0xACA770EC4C9601F3LL,1L,4L,0x77320C8D7A08E35FLL}};
                        int32_t l_1627 = 7L;
                        int32_t l_1628 = 0xF0DBD1A5L;
                        int32_t l_1631 = 0x26AD7011L;
                        int32_t l_1632 = 1L;
                        int64_t l_1638[5] = {0x7FAEC13D7E000C19LL,0x7FAEC13D7E000C19LL,0x7FAEC13D7E000C19LL,0x7FAEC13D7E000C19LL,0x7FAEC13D7E000C19LL};
                        int32_t l_1640[1][1][4] = {{{9L,9L,9L,9L}}};
                        int64_t l_1643 = 0x926CBC99E2D8CD91LL;
                        int i, j, k;
                        (*l_1619) = &g_260[9][0][1];
                        if (p_42)
                            break;
                        (****g_1457) ^= p_42;
                        l_1645[2]++;
                    }
                    (*g_413) = (-((-0x6.5p-1) != (safe_sub_func_float_f_f((*g_413), (((*l_1651) = &p_43) == &g_224[5][4][1])))));
                    for (g_180 = 0; (g_180 != 26); ++g_180)
                    { /* block id: 585 */
                        int32_t *l_1655 = &l_1642[6][2][0];
                        l_1655 = &l_1567;
                        return l_1656;
                    }
                    l_1658--;
                }
                ++l_1661[0][2];
                l_1639 &= ((**g_627) = 1L);
                (**g_1458) = (((*g_859) &= (l_1645[2] != (g_323 = ((safe_add_func_int8_t_s_s((safe_mul_func_uint8_t_u_u(l_1568, ((*l_1681) = (safe_mod_func_int64_t_s_s(((safe_mod_func_int8_t_s_s(((*l_1680) &= (safe_lshift_func_int16_t_s_s((((!(*g_1412)) | (0x24E1L == (((p_43 < ((((safe_unary_minus_func_uint32_t_u(p_43)) , (((*g_413) = (safe_add_func_float_f_f(((void*)0 != &g_1428), ((safe_lshift_func_uint8_t_u_s(p_43, g_603[1][0][1])) , (*l_88))))) < l_1624)) , l_1645[2]) <= 18446744073709551606UL)) == 0xEE9DD1F686A3D348LL) >= p_42))) , l_1596[3][3]), p_42))), (*g_1412))) > p_41), l_1597[3][3]))))), l_1616)) <= l_1635)))) , &l_1642[3][4][3]);
            }
        }
        for (l_1486 = (-8); (l_1486 != 8); ++l_1486)
        { /* block id: 604 */
            int32_t l_1684 = 0x9194F458L;
            int32_t l_1685 = 0x413B5066L;
            int32_t l_1686 = 0xB7721082L;
            int32_t l_1687 = 0xC88DFF74L;
            int32_t l_1688 = 1L;
            int32_t l_1690 = 0x1F4233F0L;
            int32_t l_1691 = 0x9A20C0A4L;
            int32_t l_1692 = 5L;
            int32_t l_1693 = 0x82F6FE11L;
            int32_t l_1694 = 0xB1A9BC6BL;
            l_1695++;
        }
    }
    else
    { /* block id: 607 */
        for (g_1532 = 0; (g_1532 >= 4); g_1532 = safe_add_func_int64_t_s_s(g_1532, 3))
        { /* block id: 610 */
            int16_t *l_1700 = &g_404[0][1];
            return l_1700;
        }
    }
    for (g_1179 = 4; (g_1179 >= 1); g_1179 -= 1)
    { /* block id: 616 */
        int16_t l_1703 = 4L;
        int32_t ****l_1711 = &g_1458;
        int32_t ****l_1712 = &g_1458;
        const int8_t l_1718 = 0x5DL;
        int8_t *l_1722 = &l_1267;
        int32_t l_1729 = 1L;
        int16_t l_1767 = 0xF974L;
        int32_t l_1782[2];
        int8_t *l_1792 = &l_1264;
        int16_t **l_1834 = (void*)0;
        uint8_t l_1862 = 0xFAL;
        int i;
        for (i = 0; i < 2; i++)
            l_1782[i] = 0xF49EAB4FL;
        for (g_156 = 0; (g_156 <= 7); g_156 += 1)
        { /* block id: 619 */
            int32_t l_1704 = 0xC0BECF72L;
            int16_t *l_1705 = &g_422;
            int i, j;
            l_1703 |= (((safe_rshift_func_int8_t_s_s((-2L), 2)) , (void*)0) == &g_182);
            l_1704 |= g_119[g_156][g_156];
            return l_1705;
        }
        l_1706--;
        l_1729 = (((((l_1703 > (((safe_rshift_func_uint16_t_u_s((0L | (((*l_1722) &= ((l_1711 == l_1712) , (safe_mod_func_int32_t_s_s(p_41, (safe_mul_func_uint16_t_u_u(l_1718, (safe_mod_func_uint8_t_u_u(l_1721, 0x75L)))))))) && (+(!((((*g_413) = (safe_sub_func_float_f_f((((safe_lshift_func_int8_t_s_s(g_422, 0)) , (*g_413)) <= (*g_413)), p_41))) < p_42) , p_43))))), 0)) , (-9L)) == (*g_1412))) == g_245[2][1][3]) ^ 18446744073709551612UL) & g_172[0]) <= 1L);
        for (g_181 = 0; (g_181 <= 7); g_181 += 1)
        { /* block id: 630 */
            uint64_t l_1751[7][8] = {{0x00C834C7EC62CEE3LL,0x3C0D8D80035F261BLL,18446744073709551615UL,3UL,0x4EC78117BC44E4F2LL,3UL,18446744073709551615UL,0x3C0D8D80035F261BLL},{0xA18E26BCCDB60AFALL,4UL,1UL,0x85B8DA032703472CLL,1UL,0xB699EB669A370CD8LL,0xF7964DCFCC3B4119LL,18446744073709551615UL},{0x3C0D8D80035F261BLL,3UL,18446744073709551615UL,0xF7964DCFCC3B4119LL,0xA18E26BCCDB60AFALL,0xA18E26BCCDB60AFALL,0xF7964DCFCC3B4119LL,18446744073709551615UL},{0xF7964DCFCC3B4119LL,0xF7964DCFCC3B4119LL,1UL,0xB699EB669A370CD8LL,0x076C49994C40AF7BLL,0xBA2CE322727B96D2LL,18446744073709551615UL,0x00C834C7EC62CEE3LL},{0x076C49994C40AF7BLL,0xBA2CE322727B96D2LL,18446744073709551615UL,0x00C834C7EC62CEE3LL,0x4263471EDD003684LL,18446744073709551615UL,0x4263471EDD003684LL,0x00C834C7EC62CEE3LL},{0xBA2CE322727B96D2LL,1UL,0xBA2CE322727B96D2LL,0xB699EB669A370CD8LL,18446744073709551614UL,0x3C0D8D80035F261BLL,3UL,18446744073709551615UL},{0x85B8DA032703472CLL,1UL,0xB699EB669A370CD8LL,0xF7964DCFCC3B4119LL,18446744073709551615UL,18446744073709551614UL,18446744073709551614UL,18446744073709551615UL}};
            uint8_t l_1754 = 0UL;
            int16_t *l_1755 = &g_404[0][1];
            int32_t **l_1757 = &g_628[3];
            int16_t l_1799 = 0xC8BAL;
            float l_1801[4] = {(-0x1.4p+1),(-0x1.4p+1),(-0x1.4p+1),(-0x1.4p+1)};
            const int32_t l_1806 = 1L;
            int32_t l_1836 = 9L;
            int32_t l_1843 = 0x8F1B85A5L;
            uint16_t l_1848 = 0xA5F7L;
            const uint8_t l_1873 = 0x10L;
            int i, j;
            for (g_180 = 4; (g_180 >= 0); g_180 -= 1)
            { /* block id: 633 */
                const uint64_t ***l_1746 = &l_1744;
                int32_t l_1759 = 5L;
                int16_t * const *l_1833[4] = {&l_1418[2],&l_1418[2],&l_1418[2],&l_1418[2]};
                int16_t *l_1837 = (void*)0;
                int i, j, k;
                if ((safe_mul_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(((((g_603[0][0][0] = (safe_lshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u((+g_119[g_181][g_181]), g_245[g_1179][g_1179][g_180])), (safe_sub_func_int8_t_s_s(((p_41 ^= 0x04A7D8D4478F173FLL) == 0L), (l_1741 == (l_1746 = l_1743))))))) , (p_42 , p_42)) | (((safe_lshift_func_int8_t_s_s((-1L), p_42)) != g_224[5][4][1]) >= (**l_87))) >= g_598), 0)), 2L)))
                { /* block id: 637 */
                    return (**g_1060);
                }
                else
                { /* block id: 639 */
                    uint8_t l_1760[5][10][2] = {{{246UL,0x37L},{0UL,6UL},{1UL,4UL},{1UL,9UL},{0xC1L,0xAFL},{250UL,1UL},{0xDEL,0xAAL},{0xC1L,0x67L},{0xE8L,0xE8L},{0UL,1UL}},{{0xAFL,0xD3L},{1UL,250UL},{255UL,1UL},{0x37L,9UL},{0x37L,1UL},{255UL,250UL},{1UL,0xD3L},{6UL,1UL},{0UL,0xDEL},{0xDEL,0xE8L}},{{9UL,250UL},{0UL,1UL},{1UL,6UL},{0xC1L,0UL},{255UL,0x67L},{1UL,0UL},{246UL,4UL},{255UL,0xC1L},{0UL,246UL},{2UL,255UL}},{{0x78L,1UL},{1UL,0UL},{250UL,0UL},{1UL,1UL},{0x78L,255UL},{2UL,246UL},{0UL,0xC1L},{255UL,4UL},{246UL,0UL},{1UL,0x67L}},{{255UL,0UL},{0xC1L,6UL},{1UL,1UL},{0UL,250UL},{9UL,0xE8L},{0xDEL,0xDEL},{0UL,1UL},{6UL,0xD3L},{1UL,250UL},{255UL,1UL}}};
                    int16_t *l_1768 = (void*)0;
                    uint16_t *l_1773 = &g_156;
                    int64_t *l_1798 = &g_952[2][0][6];
                    int32_t l_1809 = (-5L);
                    uint64_t *l_1818 = &l_1480;
                    int i, j, k;
                    for (l_1689 = 1; (l_1689 <= 7); l_1689 += 1)
                    { /* block id: 642 */
                        int64_t **l_1750 = &g_859;
                        uint16_t **l_1753 = &g_260[9][0][1];
                        uint16_t ***l_1752 = &l_1753;
                        l_1751[5][4] = (l_1749 != l_1750);
                        l_1754 = (((**g_1195) , &g_260[9][0][1]) == ((*l_1752) = &g_260[9][0][1]));
                        return l_1755;
                    }
                    if (g_1756)
                    { /* block id: 648 */
                        l_1757 = (*g_1233);
                    }
                    else
                    { /* block id: 650 */
                        int32_t l_1758[8] = {0x764AC8EEL,0x764AC8EEL,0x764AC8EEL,0x764AC8EEL,0x764AC8EEL,0x764AC8EEL,0x764AC8EEL,0x764AC8EEL};
                        int i;
                        (*g_413) = l_1758[3];
                        l_1759 = p_42;
                        l_1760[3][7][1] = p_43;
                    }
                    if ((safe_add_func_int8_t_s_s(((l_1759 |= (+((g_397 = (safe_unary_minus_func_int64_t_s(l_1767))) ^ (((*l_1722) ^= g_598) <= (((l_1768 = (**g_1060)) != ((&g_1179 != ((((*l_1773) = (safe_div_func_int32_t_s_s(p_43, (safe_mul_func_int16_t_s_s((p_43 < 0x6EL), p_43))))) , ((safe_mul_func_int16_t_s_s((((safe_lshift_func_int16_t_s_s((safe_rshift_func_uint16_t_u_s((safe_add_func_uint64_t_u_u((g_833[1][1][4] > p_41), l_1782[0])), p_42)), 4)) , p_41) & 0xE00B458EL), 65535UL)) != 0x12L)) , &g_1179)) , (*g_1198))) <= p_42))))) > g_1783), p_42)))
                    { /* block id: 660 */
                        uint32_t l_1800 = 0UL;
                        (*l_87) = func_126((safe_mod_func_int64_t_s_s((safe_mod_func_uint64_t_u_u((g_397--), (safe_mul_func_uint16_t_u_u((((*g_1061) == (void*)0) < p_42), (((void*)0 != l_1792) >= (safe_add_func_uint64_t_u_u((p_41 = (((*g_1412) & ((*l_1722) = ((&g_260[9][0][1] != (((**g_1428) &= (((safe_div_func_uint32_t_u_u((safe_unary_minus_func_uint32_t_u(((6L > ((void*)0 == l_1798)) || p_42))), 0x52BB98B7L)) == l_1759) == 0xB2FC1FB89FC3F9C7LL)) , (void*)0)) <= l_1799))) | 0UL)), l_1800))))))), (*l_88))), g_119[7][6], l_1768, (*g_413));
                        return l_1773;
                    }
                    else
                    { /* block id: 667 */
                        uint32_t *l_1810[8] = {&l_1706,&l_1706,&l_1706,&l_1706,&l_1706,&l_1706,&l_1706,&l_1706};
                        uint16_t **l_1811[9];
                        uint16_t ***l_1812[2];
                        int32_t l_1835[9][2][3] = {{{0x5ACCD8E4L,(-1L),(-1L)},{0x5ACCD8E4L,0x5ACCD8E4L,(-1L)}},{{0xB8C9E9BFL,(-1L),(-1L)},{(-1L),9L,(-1L)}},{{0xB8C9E9BFL,9L,0xB8C9E9BFL},{0x5ACCD8E4L,(-1L),(-1L)}},{{0x5ACCD8E4L,0x5ACCD8E4L,(-1L)},{0xB8C9E9BFL,(-1L),(-1L)}},{{(-1L),9L,(-1L)},{0xB8C9E9BFL,9L,0xB8C9E9BFL}},{{0x5ACCD8E4L,(-1L),(-1L)},{0x5ACCD8E4L,0x5ACCD8E4L,(-1L)}},{{0xB8C9E9BFL,(-1L),(-1L)},{(-1L),9L,(-1L)}},{{0xB8C9E9BFL,9L,0xB8C9E9BFL},{0x5ACCD8E4L,(-1L),(-1L)}},{{0x5ACCD8E4L,0x5ACCD8E4L,(-1L)},{0xB8C9E9BFL,(-1L),(-1L)}}};
                        int i, j, k;
                        for (i = 0; i < 9; i++)
                            l_1811[i] = &g_260[0][1][0];
                        for (i = 0; i < 2; i++)
                            l_1812[i] = &l_1811[1];
                        l_1759 |= (safe_rshift_func_uint8_t_u_u(((p_41 & (((safe_mul_func_int16_t_s_s(l_1806, 0xF6E0L)) == ((g_598 = (l_1760[0][9][1] < ((*l_1420) = ((l_1729 = (((void*)0 == &g_260[9][0][1]) , (l_1809 = 0x642C193CL))) < 0xD71FB737L)))) , ((((g_1814[0] = (g_1813 = l_1811[4])) == &g_260[4][0][0]) >= (-9L)) , g_1815))) | (**l_87))) & (*g_859)), p_42));
                        l_1836 = ((((*l_1722) = (safe_lshift_func_uint16_t_u_s(((((*g_542) = &p_42) == l_1818) , ((*l_1773) ^= ((safe_mul_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(18446744073709551606UL, (p_41 &= 0x714420EFAE8DC1CBLL))), (safe_div_func_uint8_t_u_u(0xF3L, (-1L))))), (safe_lshift_func_uint8_t_u_u(((safe_add_func_uint16_t_u_u((((((((((-4L) != ((g_1195 = (((safe_sub_func_uint64_t_u_u(((*g_859) <= (p_42 = ((**g_147) | g_245[g_1179][g_1179][g_180]))), l_1760[3][7][1])) | (**l_87)) , l_1833[0])) == l_1834)) && (**g_542)) >= (*g_1196)) & g_1815) && p_42) > l_1835[8][1][1]) < (-1L)) & p_43), p_43)) != l_1760[2][0][0]), p_43)))) || 0x0D15FBA7FB90CEBBLL))), 2))) != 0x37L) & p_43);
                        return l_1837;
                    }
                }
            }
            for (l_1767 = 6; (l_1767 >= 0); l_1767 -= 1)
            { /* block id: 688 */
                uint8_t l_1838 = 255UL;
                uint16_t *l_1841[10][4][3] = {{{&g_833[4][2][7],&g_833[1][2][1],&g_833[1][2][3]},{(void*)0,&g_183,&g_833[1][2][1]},{&g_833[6][1][3],&g_833[4][2][7],&g_833[1][2][3]},{&g_183,&g_183,&g_183}},{{&g_833[0][2][5],(void*)0,&g_156},{&g_833[0][2][5],&g_156,&g_833[6][1][3]},{&g_183,&g_183,&g_156},{&g_833[6][1][3],(void*)0,(void*)0}},{{(void*)0,&g_183,&g_833[1][2][1]},{&g_833[4][2][7],&g_156,&g_833[4][2][7]},{&g_183,(void*)0,&g_833[4][2][7]},{&g_833[1][2][3],&g_183,&g_833[1][2][1]}},{{&g_833[1][2][1],&g_833[4][2][7],(void*)0},{(void*)0,&g_183,&g_156},{&g_833[4][2][7],&g_833[1][2][3],(void*)0},{&g_833[6][1][3],&g_833[6][1][3],&g_183}},{{&g_183,&g_833[6][1][3],(void*)0},{&g_156,&g_833[1][2][3],&g_833[6][1][3]},{&g_833[1][2][1],(void*)0,&g_833[1][2][1]},{(void*)0,&g_156,&g_833[6][1][3]}},{{&g_833[0][2][5],(void*)0,(void*)0},{&g_156,&g_183,&g_183},{&g_156,&g_183,(void*)0},{&g_833[0][2][5],&g_183,&g_183}},{{(void*)0,&g_833[1][2][1],&g_833[1][2][1]},{&g_833[1][2][1],&g_183,&g_833[4][2][7]},{&g_156,&g_183,&g_156},{&g_183,&g_183,&g_156}},{{&g_833[6][1][3],(void*)0,&g_833[4][2][7]},{&g_833[4][2][7],&g_156,&g_833[1][2][1]},{&g_183,(void*)0,&g_183},{&g_833[4][2][7],&g_833[1][2][3],(void*)0}},{{&g_833[6][1][3],&g_833[6][1][3],&g_183},{&g_183,&g_833[6][1][3],(void*)0},{&g_156,&g_833[1][2][3],&g_833[6][1][3]},{&g_833[1][2][1],(void*)0,&g_833[1][2][1]}},{{(void*)0,&g_156,&g_833[6][1][3]},{&g_833[0][2][5],(void*)0,(void*)0},{&g_156,&g_183,&g_183},{&g_156,&g_183,(void*)0}}};
                int32_t l_1842 = 0xD99516FBL;
                int32_t l_1844 = 0xBFEFF599L;
                int32_t l_1845 = 0x2A0EBBE0L;
                int32_t l_1846 = 0xC226A0B8L;
                int32_t l_1847 = 1L;
                const int32_t *l_1853 = &g_464;
                int64_t *l_1872 = &g_952[0][3][1];
                int i, j, k;
                l_1838--;
                l_1845 = ((l_1846 = (l_1848--)) , (l_1843 = ((((**g_1195) = ((((((*g_1429) &= ((safe_lshift_func_uint16_t_u_s((l_1853 != (((safe_sub_func_int64_t_s_s(((*l_1872) = ((((**g_1411) & (safe_add_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u((((0x5A2AL == (p_41 | 0x4B77L)) <= (safe_mod_func_uint8_t_u_u(0x80L, ((*l_1722) = l_1862)))) , ((safe_rshift_func_uint16_t_u_s(((safe_sub_func_int32_t_s_s((safe_div_func_int16_t_s_s((0L == ((safe_mul_func_int8_t_s_s(p_43, l_1846)) <= 0x9547F5FA6CEE4D9CLL)), (*l_88))), 1L)) ^ 0xDF393758L), (*g_1196))) != p_41)), g_1871)), 0x8BEB2FE7AE1A05ADLL))) && p_41) < 0x7F48L)), 0x30C6ADCBFAD4A1C1LL)) | g_171) , (void*)0)), (**g_1195))) , (**l_87))) , l_1873) == (*g_1412)) <= p_42) ^ l_1847)) , l_1838) == 0xB5BEEF2AL)));
            }
        }
    }
    l_1890[0] = (safe_rshift_func_int8_t_s_u((l_1876 == ((**l_87) , (((*l_88) >= (safe_rshift_func_int8_t_s_u((((-1L) != (((((**g_1411)--) != (safe_mul_func_int16_t_s_s((p_41 , (**l_87)), (safe_div_func_int64_t_s_s(((safe_rshift_func_int16_t_s_u((((*l_1887) = (1L <= p_41)) || (safe_div_func_int8_t_s_s((p_43 | (**l_87)), 0x4CL))), p_41)) | 0x3FF2L), p_42))))) ^ 65535UL) ^ g_952[1][3][0])) >= p_43), (*l_88)))) , &g_260[6][0][0]))), 7));
    return l_1420;
}


/* ------------------------------------------ */
/* 
 * reads : g_603 g_119 g_224 g_181 g_66 g_1097 g_627 g_628 g_403 g_422 g_281 g_598 g_859 g_833 g_147 g_405 g_580 g_171 g_183 g_1198 g_1060 g_413 g_305
 * writes: g_603 g_598 g_66 g_403 g_628 g_281 g_171 g_148 g_422 g_464 g_542 g_181 g_119 g_580 g_1195 g_1061 g_305
 */
static const uint16_t  func_67(const uint64_t  p_68, int32_t ** p_69, int8_t  p_70)
{ /* block id: 210 */
    uint64_t *l_629 = &g_603[1][0][1];
    int32_t l_642 = (-3L);
    uint32_t *l_645 = &g_598;
    uint64_t l_656 = 1UL;
    const int32_t ***l_706[3][6] = {{&g_147,(void*)0,&g_147,&g_147,(void*)0,&g_147},{&g_147,&g_147,(void*)0,&g_147,&g_147,&g_147},{&g_147,(void*)0,&g_147,&g_147,(void*)0,(void*)0}};
    const int32_t ****l_705 = &l_706[1][1];
    float l_751 = 0x5.FC07F9p+67;
    int32_t l_762 = 0x7BB1E311L;
    int32_t l_767 = 1L;
    int32_t l_769[9][9][2] = {{{0x1B01ADD6L,(-7L)},{0xED3F9DB1L,0xD8CC1535L},{0x128E26C9L,0L},{0x07F4BA0AL,0L},{9L,0xF056A81FL},{0x128E26C9L,(-7L)},{(-1L),(-7L)},{0x128E26C9L,0xF056A81FL},{9L,0L}},{{0x07F4BA0AL,0L},{0x128E26C9L,0xD8CC1535L},{0xED3F9DB1L,(-7L)},{0x1B01ADD6L,0L},{9L,1L},{9L,0L},{0x1B01ADD6L,(-7L)},{0xED3F9DB1L,0xD8CC1535L},{0x128E26C9L,0L}},{{0x07F4BA0AL,0L},{9L,0xF056A81FL},{0x128E26C9L,(-7L)},{(-1L),(-7L)},{0x128E26C9L,0xF056A81FL},{9L,0L},{0x07F4BA0AL,0L},{0x128E26C9L,0xD8CC1535L},{0xED3F9DB1L,(-7L)}},{{0x1B01ADD6L,0L},{9L,1L},{9L,0L},{0x1B01ADD6L,(-7L)},{0xED3F9DB1L,0xD8CC1535L},{0x128E26C9L,0L},{0x07F4BA0AL,0L},{9L,0xF056A81FL},{0x128E26C9L,(-7L)}},{{(-1L),(-7L)},{0x128E26C9L,0xF056A81FL},{9L,0L},{0x07F4BA0AL,0L},{0x128E26C9L,0xD8CC1535L},{0xED3F9DB1L,(-7L)},{0x1B01ADD6L,0L},{9L,1L},{9L,0L}},{{0x1B01ADD6L,(-7L)},{0xED3F9DB1L,0xD8CC1535L},{(-1L),0xCF0A00A3L},{0xCD5F946CL,0xF056A81FL},{(-1L),0x19E50391L},{(-1L),1L},{0L,1L},{(-1L),0x19E50391L},{(-1L),0xF056A81FL}},{{0xCD5F946CL,0xCF0A00A3L},{(-1L),0x3139F844L},{0x1B01ADD6L,1L},{(-1L),0xCF0A00A3L},{(-1L),2L},{(-1L),0xCF0A00A3L},{(-1L),1L},{0x1B01ADD6L,0x3139F844L},{(-1L),0xCF0A00A3L}},{{0xCD5F946CL,0xF056A81FL},{(-1L),0x19E50391L},{(-1L),1L},{0L,1L},{(-1L),0x19E50391L},{(-1L),0xF056A81FL},{0xCD5F946CL,0xCF0A00A3L},{(-1L),0x3139F844L},{0x1B01ADD6L,1L}},{{(-1L),0xCF0A00A3L},{(-1L),2L},{(-1L),0xCF0A00A3L},{(-1L),1L},{0x1B01ADD6L,0x3139F844L},{(-1L),0xCF0A00A3L},{0xCD5F946CL,0xF056A81FL},{(-1L),0x19E50391L},{(-1L),1L}}};
    uint16_t l_807 = 0x30ECL;
    int16_t l_907[2][9][1] = {{{0L},{0xF9ADL},{0xF9ADL},{0L},{(-7L)},{(-1L)},{0L},{(-1L)},{(-7L)}},{{0L},{0xF9ADL},{0xF9ADL},{0L},{(-7L)},{(-1L)},{0L},{(-1L)},{(-7L)}}};
    uint8_t l_953 = 1UL;
    int32_t l_1017 = 0x0624FA84L;
    uint8_t *l_1094 = &g_281;
    uint8_t **l_1093 = &l_1094;
    uint64_t l_1095[1];
    int32_t * const l_1113 = &l_642;
    uint32_t l_1181 = 1UL;
    uint8_t l_1190 = 248UL;
    int16_t * const *l_1193 = &g_530[1][5];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1095[i] = 9UL;
    if ((((*l_629)++) && (0x871015D1F0CB5AAALL && ((safe_add_func_int16_t_s_s((g_119[7][6] <= (((g_603[0][0][0] , (safe_div_func_int64_t_s_s((((safe_sub_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((&l_629 == &l_629), (safe_add_func_uint32_t_u_u(((*l_645) = (l_642 || (l_642 > (safe_add_func_uint8_t_u_u(0xE7L, p_70))))), 1L)))), l_642)) || g_224[5][4][1]) && p_70), g_181))) < p_70) , 0x39L)), l_642)) , 1L))))
    { /* block id: 213 */
        uint64_t l_652 = 0x65B6BB652712750BLL;
        int16_t **l_671 = &g_530[1][2];
        int16_t l_768 = 6L;
        int32_t l_770 = (-1L);
        int16_t l_776 = 0x7C1DL;
        int32_t l_778 = 0x01CC4D2AL;
        int32_t **l_809 = &g_628[0];
        uint64_t * const *l_821 = &g_543;
        int32_t ** const *l_841 = &g_627;
        int32_t ** const **l_840 = &l_841;
        int64_t *l_860 = (void*)0;
        int32_t l_886[7][2][2] = {{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}},{{1L,1L},{1L,1L}}};
        const uint32_t *l_1008 = &g_598;
        uint8_t l_1059 = 255UL;
        const int8_t *l_1090 = &g_323;
        int32_t *l_1102 = &l_886[6][1][0];
        int i, j, k;
        for (g_66 = 0; (g_66 != (-25)); g_66 = safe_sub_func_uint8_t_u_u(g_66, 5))
        { /* block id: 216 */
            uint64_t l_666 = 8UL;
            uint64_t **l_680 = (void*)0;
            float l_683 = (-0x1.Dp+1);
            int32_t l_764 = 0L;
            int32_t l_771 = 1L;
            int32_t l_773 = (-6L);
            int32_t l_774 = 0x8E69B795L;
            int32_t l_775 = 1L;
            int32_t l_777[9] = {0xF4DF749DL,0xF4DF749DL,0x9D7D599FL,0xF4DF749DL,0xF4DF749DL,0x9D7D599FL,0xF4DF749DL,0xF4DF749DL,0x9D7D599FL};
            int32_t ****l_814 = (void*)0;
            int32_t *****l_813 = &l_814;
            int8_t *l_834 = &g_323;
            float l_835 = (-0x1.2p-1);
            const int32_t ** const *l_843 = &g_147;
            const int32_t ** const **l_842 = &l_843;
            int64_t *l_861 = (void*)0;
            uint32_t l_892 = 0x80D23678L;
            uint64_t l_975 = 0xFB60F5EF18BB6DA0LL;
            const int64_t l_993 = (-4L);
            int16_t *l_1040 = &g_245[3][5][3];
            int i;
        }
        (***l_841) ^= (~((void*)0 == g_1097));
        (**l_841) = l_1102;
    }
    else
    { /* block id: 371 */
        int32_t **l_1105 = &g_628[1];
        (**p_69) = (safe_lshift_func_uint8_t_u_u(((void*)0 != l_1105), ((g_422 >= (g_171 = (+(safe_div_func_int8_t_s_s(((++(**l_1093)) & 0x23L), (l_767 = 5UL)))))) ^ (((g_598 , 0xB5D85CC73C110007LL) , (*g_859)) != ((safe_mul_func_int8_t_s_s(0L, g_833[1][2][1])) & 0x2908E87AL)))));
    }
    (*g_147) = (p_70 , l_1113);
    for (g_422 = 0; (g_422 != (-10)); g_422 = safe_sub_func_uint16_t_u_u(g_422, 5))
    { /* block id: 380 */
        uint16_t l_1135 = 0xBB21L;
        int64_t l_1145 = 1L;
        int32_t l_1165 = 0x291D506FL;
        int32_t l_1166 = (-4L);
        int32_t l_1167[6][6][7] = {{{0L,1L,(-8L),0xDA5518BFL,1L,0L,0x4CD36DE9L},{0x03965818L,(-6L),0x7CF33AD7L,0xD3772662L,1L,0xEC835A30L,1L},{(-1L),(-1L),0x80C4B18BL,(-5L),0x496D946EL,0L,0x9B93687CL},{1L,0L,(-1L),0x8D704880L,0x469ACB15L,0xF15C631BL,(-1L)},{0x7A49DCE9L,0x28070BC8L,0L,0L,0xC0408668L,(-1L),(-1L)},{0xA584AD6BL,(-8L),(-1L),0x110D0A1EL,(-1L),0x3D47C01EL,0x9B93687CL}},{{0x2EC68C38L,(-4L),0x013BFDDFL,0L,0xDC0D2C20L,(-8L),1L},{5L,0x3D47C01EL,(-4L),0x496D946EL,0L,0x75208654L,0x4CD36DE9L},{(-4L),(-3L),1L,0L,0L,0x110D0A1EL,0x110D0A1EL},{0x469ACB15L,0x85A3E3FFL,(-8L),0x85A3E3FFL,0x469ACB15L,0x2EC68C38L,1L},{0x80C4B18BL,1L,(-3L),1L,0L,0x149EF961L,0L},{(-8L),0x7CF33AD7L,0xCA349CD5L,(-6L),(-1L),1L,(-8L)}},{{0x80C4B18BL,1L,(-8L),0xEC835A30L,3L,(-5L),(-5L)},{0x3D47C01EL,3L,0x013BFDDFL,0xC0408668L,0xCA349CD5L,0x013BFDDFL,0xD0CED95CL},{(-3L),0x1E32ACB2L,0xF9BC40F5L,0x7C14674AL,0xFAC8010BL,1L,0xCA349CD5L},{0x1E32ACB2L,0L,0x469ACB15L,0x75208654L,0x28070BC8L,0xD3772662L,0x3D47C01EL},{0xF1249BEDL,0xF9BC40F5L,0x403DF484L,0L,0x149EF961L,0L,0xFAC8010BL},{7L,0x3D47C01EL,0x984F987AL,0xCA349CD5L,(-4L),0xBD658EE0L,(-4L)}},{{0xEC835A30L,0x3D47C01EL,4L,0xEC835A30L,0x7CF33AD7L,(-4L),(-1L)},{(-1L),0xF9BC40F5L,3L,0xF1249BEDL,(-3L),0xF15C631BL,0xA584AD6BL},{(-2L),0L,(-9L),(-7L),1L,(-8L),(-1L)},{0L,0x1E32ACB2L,0x2EC68C38L,0x28070BC8L,1L,0x58DBFBE3L,(-4L)},{0xFAC8010BL,3L,(-7L),0L,(-7L),3L,0xFAC8010BL},{(-1L),1L,(-8L),0L,1L,0xCA349CD5L,1L}},{{0x28070BC8L,0x7CF33AD7L,5L,0xFAC8010BL,0x75208654L,4L,0xC0408668L},{0L,1L,(-8L),0x1E32ACB2L,(-9L),1L,0xD0CED95CL},{0L,0x28070BC8L,(-7L),0xF9BC40F5L,(-4L),(-4L),0xF9BC40F5L},{0xAFC56190L,0x80C4B18BL,0x2EC68C38L,0xFE89E206L,(-1L),0L,1L},{0L,(-8L),(-9L),(-4L),0xED0CE13EL,(-2L),0L},{0xED0CE13EL,0x9B93687CL,3L,(-2L),0xFAC8010BL,(-1L),(-8L)}},{{(-1L),1L,4L,0x79F8D735L,0xC0408668L,0x22D6A4DBL,0x1E32ACB2L},{0xFAC8010BL,(-8L),0x984F987AL,0x79F8D735L,(-8L),1L,0xAFC56190L},{0xF15C631BL,0xFAC8010BL,0x403DF484L,(-2L),(-1L),1L,0x149EF961L},{0xA584AD6BL,0L,0x469ACB15L,(-4L),(-6L),0L,0xFE89E206L},{(-1L),1L,0xF9BC40F5L,0xFE89E206L,1L,3L,0L},{0L,3L,0x013BFDDFL,0xF9BC40F5L,0x110D0A1EL,0xF9BC40F5L,0x013BFDDFL}}};
        int32_t l_1186[4] = {1L,1L,1L,1L};
        int32_t *l_1189[3];
        int16_t * const **l_1194[5] = {&l_1193,&l_1193,&l_1193,&l_1193,&l_1193};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1189[i] = (void*)0;
        for (g_464 = 0; (g_464 >= 0); g_464 -= 1)
        { /* block id: 383 */
            int32_t l_1126[1];
            int32_t l_1162 = (-1L);
            int32_t l_1163 = 0x490A0817L;
            int32_t l_1164 = (-7L);
            int32_t l_1168 = 0xA4887A31L;
            int32_t l_1169 = 0x5C5CFC78L;
            int32_t l_1172 = 0x228E201AL;
            int32_t l_1173 = 5L;
            int32_t l_1174 = 0x8A3EFCB7L;
            int32_t l_1175[2];
            int64_t l_1177 = 0L;
            int16_t l_1180 = 0xD401L;
            int i;
            for (i = 0; i < 1; i++)
                l_1126[i] = 1L;
            for (i = 0; i < 2; i++)
                l_1175[i] = (-1L);
            for (l_767 = 0; (l_767 <= 0); l_767 += 1)
            { /* block id: 386 */
                uint64_t ***l_1127 = &g_542;
                int32_t *l_1128 = (void*)0;
                int32_t l_1129 = 0xAD0CEFF0L;
                int32_t *l_1130 = &l_762;
                int32_t *l_1131 = &l_769[7][1][1];
                int32_t *l_1132 = &l_1126[0];
                int32_t *l_1133 = &g_180;
                int32_t *l_1134[7];
                int i;
                for (i = 0; i < 7; i++)
                    l_1134[i] = &l_642;
                (*l_1113) |= (safe_lshift_func_uint8_t_u_s((0xA971659905E853E1LL || (safe_lshift_func_uint16_t_u_u((safe_mod_func_int16_t_s_s(((((((safe_add_func_int64_t_s_s(((p_70 > (((*l_1127) = (l_1126[0] , &l_629)) == &l_629)) | 0xDCC6F306L), 5UL)) != 0UL) || 0L) != p_68) , 0xB5AEA587L) , p_70), p_70)), 15))), 1));
                --l_1135;
            }
            for (g_181 = 0; (g_181 <= 0); g_181 += 1)
            { /* block id: 393 */
                int16_t *l_1144[1];
                int32_t l_1146[9][7][1] = {{{0x6910FF42L},{0xBF1D4603L},{(-1L)},{0xDF82B18BL},{0x6910FF42L},{0xDF82B18BL},{(-1L)}},{{0xBF1D4603L},{0x6910FF42L},{0xBF1D4603L},{(-1L)},{0xDF82B18BL},{0x6910FF42L},{0xDF82B18BL}},{{(-1L)},{0xBF1D4603L},{0x6910FF42L},{0xBF1D4603L},{(-1L)},{0xDF82B18BL},{0x6910FF42L}},{{0xDF82B18BL},{(-1L)},{0xBF1D4603L},{0x6910FF42L},{0xBF1D4603L},{(-1L)},{0xDF82B18BL}},{{0x6910FF42L},{0xDF82B18BL},{(-1L)},{0xBF1D4603L},{0x6910FF42L},{0xBF1D4603L},{(-1L)}},{{0xDF82B18BL},{0x6910FF42L},{0xDF82B18BL},{(-1L)},{0xBF1D4603L},{0x6910FF42L},{0xBF1D4603L}},{{(-1L)},{0xDF82B18BL},{0x6910FF42L},{0xDF82B18BL},{(-1L)},{0xBF1D4603L},{0x6910FF42L}},{{0xBF1D4603L},{(-1L)},{0xDF82B18BL},{0x6910FF42L},{0xDF82B18BL},{(-1L)},{0xBF1D4603L}},{{0x6910FF42L},{0xBF1D4603L},{(-1L)},{0xDF82B18BL},{0x6910FF42L},{0xDF82B18BL},{(-1L)}}};
                uint16_t *l_1161[9] = {&g_833[1][2][1],&g_833[1][2][1],&g_833[5][0][6],&g_833[1][2][1],&g_833[1][2][1],&g_833[5][0][6],&g_833[1][2][1],&g_833[1][2][1],&g_833[5][0][6]};
                int32_t l_1170 = (-1L);
                int32_t l_1171[8][9][3] = {{{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL}},{{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L}},{{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL}},{{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L}},{{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL}},{{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L}},{{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL}},{{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L},{0xEF30C96FL,(-8L),0xEF30C96FL},{0x41B3E3CBL,6L,6L},{0x320BD5ADL,(-8L),0x320BD5ADL},{0x41B3E3CBL,0x41B3E3CBL,6L}}};
                int16_t l_1176 = 0x0F49L;
                int32_t l_1178 = 0xB3B79AE2L;
                int32_t *l_1187 = (void*)0;
                int32_t *l_1188 = &l_767;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_1144[i] = &g_404[0][1];
                (*l_1188) &= ((safe_rshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s((safe_mul_func_int16_t_s_s((l_1126[0] = (-4L)), (l_1145 | l_1146[4][5][0]))), (safe_div_func_int8_t_s_s(((safe_sub_func_uint32_t_u_u(((safe_div_func_int16_t_s_s(p_68, ((*l_1113) = p_68))) > (safe_add_func_uint64_t_u_u((249UL ^ (safe_mul_func_int8_t_s_s(((p_68 == (safe_sub_func_int16_t_s_s((safe_mod_func_int64_t_s_s((((g_119[7][6] = 254UL) , (((g_580 &= (((--l_1181) & ((safe_div_func_uint8_t_u_u(g_405, l_1186[0])) >= 255UL)) , p_68)) <= g_171) < 1UL)) && 253UL), (*g_859))), l_1167[2][1][1]))) ^ l_1167[2][4][6]), g_183))), p_68))), 0x10E22048L)) || 0x28L), p_68)))), 8)) , (-9L));
            }
        }
        --l_1190;
        (*l_1113) |= ((g_1195 = l_1193) == ((*g_1060) = g_1198));
    }
    (*g_413) = ((*g_413) < (-0x8.Cp+1));
    return p_70;
}


/* ------------------------------------------ */
/* 
 * reads : g_580 g_403 g_224 g_413 g_305 g_156 g_66 g_172 g_147 g_148 g_4
 * writes: g_580 g_404 g_168 g_171 g_172 g_148
 */
static const uint64_t  func_71(uint64_t  p_72, int32_t * p_73, const int16_t * p_74)
{ /* block id: 204 */
    int16_t l_609 = 0x53D2L;
    uint8_t *l_616 = (void*)0;
    uint8_t *l_617 = &g_580;
    int16_t *l_618 = &g_404[0][0];
    uint16_t l_625 = 65535UL;
    int32_t l_626 = 2L;
    (*g_147) = func_126(l_609, (p_72 >= (l_609 || ((safe_mod_func_int32_t_s_s((safe_div_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s(((*l_617) |= p_72), g_403)), ((*l_618) = l_609))), (safe_sub_func_int8_t_s_s(p_72, g_224[1][1][0])))) >= (safe_div_func_int64_t_s_s(((safe_rshift_func_int8_t_s_s(l_609, p_72)) || 0xCEEB5F0A4D8FB16ELL), l_625))))), &l_609, (*g_413));
    l_626 |= (*g_148);
    return l_609;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_84(int32_t ** p_85, int32_t  p_86)
{ /* block id: 13 */
    int32_t l_123 = (-1L);
    int32_t l_272[4][6][4] = {{{0x1DF0D93DL,0L,0x8151B2E9L,(-8L)},{0x1DF0D93DL,(-5L),0x76EB01D0L,0L},{0x76EB01D0L,0L,0x76EB01D0L,(-5L)},{0x1DF0D93DL,(-8L),0x8151B2E9L,0L},{0x1DF0D93DL,0xBCB60050L,0x76EB01D0L,(-8L)},{0x76EB01D0L,(-8L),0x76EB01D0L,0xBCB60050L}},{{0x1DF0D93DL,0L,0x8151B2E9L,(-8L)},{0x1DF0D93DL,(-5L),0x76EB01D0L,0L},{0x76EB01D0L,0L,0x76EB01D0L,(-5L)},{0x1DF0D93DL,(-8L),0x8151B2E9L,0L},{0x1DF0D93DL,0xBCB60050L,0x76EB01D0L,(-8L)},{0x76EB01D0L,(-8L),0x76EB01D0L,0xBCB60050L}},{{0x1DF0D93DL,0L,0x8151B2E9L,(-8L)},{0x1DF0D93DL,(-5L),0x76EB01D0L,0L},{0x76EB01D0L,0L,0x76EB01D0L,(-5L)},{0x1DF0D93DL,(-8L),0x8151B2E9L,0L},{0x1DF0D93DL,0xBCB60050L,0x76EB01D0L,(-8L)},{0x76EB01D0L,(-8L),0x76EB01D0L,0xBCB60050L}},{{0x1DF0D93DL,0L,0x8151B2E9L,(-8L)},{0x1DF0D93DL,(-5L),0x76EB01D0L,0L},{0x76EB01D0L,0L,0x76EB01D0L,(-5L)},{0x1DF0D93DL,(-8L),0x8151B2E9L,0L},{0x1DF0D93DL,0xBCB60050L,0x76EB01D0L,(-5L)},{0x8151B2E9L,(-5L),0x8151B2E9L,1L}}};
    uint16_t l_292[10][7][3] = {{{65530UL,0x2699L,1UL},{65535UL,0x8CB9L,0x116CL},{0xCBF2L,0x72D9L,0xD842L},{0xAF2CL,0xC1E2L,65535UL},{0UL,0x4690L,0x4690L},{0x4690L,0x6183L,0xAA20L},{65526UL,3UL,0x6A66L}},{{0x46A0L,0x76EDL,0x7EEBL},{0xF97DL,65535UL,65535UL},{0x6A66L,0x76EDL,1UL},{0xB63DL,3UL,1UL},{65534UL,0x6183L,0x5AE9L},{3UL,0x4690L,65526UL},{0xF97DL,0xC1E2L,65530UL}},{{65535UL,0x72D9L,0x6ADBL},{65535UL,0x8CB9L,0xD842L},{65535UL,0x2699L,65535UL},{6UL,0x4690L,0x7EEBL},{0xAF2CL,9UL,0x76EDL},{65526UL,65535UL,0x76EDL},{0xF1C7L,0x2CD2L,0x7EEBL}},{{0x5AE9L,0x26F0L,65535UL},{3UL,0x76EDL,0xD842L},{0xC1E2L,65535UL,0x6ADBL},{65534UL,0xE36BL,65530UL},{0x76EDL,0xB9FBL,65526UL},{0x5AE9L,0x2699L,0x5AE9L},{0x46A0L,0x72D9L,1UL}},{{0xCBF2L,65529UL,1UL},{65535UL,0xB63DL,65535UL},{0x2920L,0xB9FBL,0x7EEBL},{65535UL,0x6183L,0x6A66L},{0xCBF2L,65535UL,0xAA20L},{0x46A0L,3UL,0x4690L},{0x5AE9L,1UL,65535UL}},{{0x76EDL,0x2CD2L,0xD842L},{65534UL,3UL,0x116CL},{0xC1E2L,0xE36BL,1UL},{3UL,0xAF2CL,65535UL},{0x5AE9L,0xB63DL,65530UL},{0xF1C7L,0x8CB9L,1UL},{65526UL,0x8CB9L,0x567DL}},{{0xAF2CL,0xB63DL,2UL},{6UL,0xAF2CL,0x4690L},{65535UL,0xE36BL,0x76EDL},{65535UL,3UL,0xAA20L},{65535UL,0x2CD2L,65535UL},{0xF97DL,1UL,2UL},{3UL,3UL,1UL}},{{65534UL,65535UL,0x6ADBL},{0xB63DL,0x6183L,1UL},{0x6A66L,0xB9FBL,65535UL},{0xF97DL,0xB63DL,1UL},{0x46A0L,65529UL,0x6ADBL},{65526UL,0x72D9L,1UL},{0x4690L,0x2699L,2UL}},{{0UL,0xB9FBL,65535UL},{0xAF2CL,0xE36BL,0xAA20L},{0xCBF2L,65535UL,0x76EDL},{65535UL,0x76EDL,0x4690L},{65530UL,0x26F0L,2UL},{0x6A66L,0x2CD2L,0x567DL},{0xF1C7L,65535UL,65528UL}},{{0xF1C7L,0x6ADBL,65535UL},{0xD842L,0UL,65535UL},{65535UL,0x46A0L,3UL},{0xD9E4L,9UL,2UL},{65530UL,3UL,0x2699L},{0x2920L,0xF1C7L,65526UL},{0xAA20L,0UL,0UL}}};
    int16_t *l_332 = &g_66;
    uint64_t *l_361 = &g_321[6][0];
    float *l_412[2];
    uint8_t *l_449 = &g_119[5][4];
    int32_t *l_497 = &l_272[2][1][2];
    int64_t l_504 = 0xC7B2E89AD5E69F15LL;
    int8_t l_551 = (-10L);
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_412[i] = &g_305;
    return p_86;
}


/* ------------------------------------------ */
/* 
 * reads : g_147 g_119 g_4 g_66 g_156 g_172 g_183 g_148 g_180 g_191 g_174 g_224 g_171 g_182
 * writes: g_156 g_168 g_171 g_172 g_183 g_191 g_148 g_224 g_180 g_260 g_174
 */
static const int16_t  func_97(uint32_t  p_98, int32_t  p_99, uint32_t  p_100, int16_t * p_101)
{ /* block id: 19 */
    uint32_t l_124 = 0x126B21CEL;
    uint32_t l_125 = 0x8F1CEF17L;
    int32_t l_157 = 3L;
    int16_t *l_158 = &g_66;
    int32_t *l_173[7] = {&g_4,&g_4,&g_174,&g_4,&g_4,&g_174,&g_4};
    int32_t * const *l_243 = (void*)0;
    const int32_t l_263 = 4L;
    int i;
    if (l_124)
    { /* block id: 20 */
        const int32_t **l_150 = &g_148;
        int32_t l_175 = 5L;
        int32_t l_176 = 0x9BB11877L;
        const int32_t ***l_187 = &g_147;
        const int32_t **** const l_186 = &l_187;
        int32_t l_222 = 1L;
        int32_t l_223[2];
        float l_232[8][6][5] = {{{0x2.7C866Fp-27,0x4.9CAA41p+24,0x9.179F1Ep+79,0x5.7F89DCp-27,0x6.4D1AD4p+51},{0x2.6p+1,0x0.1FEF54p+24,0x1.5p-1,0x8.1DB17Cp-95,(-0x1.2p+1)},{0x3.Cp-1,0x9.179F1Ep+79,(-0x1.Bp-1),(-0x3.Ep+1),0x7.9p-1},{0x1.3p+1,0x8.8D6541p+32,0x6.Ep-1,0x2.6p+1,0x1.0p+1},{0x3.Cp-1,0x7.2D96CCp+72,(-0x1.1p-1),0x1.8p-1,0xE.1E6137p-0},{0x2.6p+1,0xA.7D1E30p-75,0x0.9p+1,0x8.286BBEp-28,0x1.3p-1}},{{0x2.7C866Fp-27,0x7.2D96CCp+72,0xE.A4509Cp+90,0xD.0B409Ap-39,(-0x8.9p-1)},{0xF.4248B4p+67,0x8.8D6541p+32,0x8.8D6541p+32,0xF.4248B4p+67,0x0.4p-1},{0x1.Cp-1,0x9.179F1Ep+79,0xE.A4509Cp+90,0x9.2E1E26p+7,(-0x5.9p-1)},{(-0x1.8p+1),0x0.1FEF54p+24,0x0.9p+1,0xB.27D908p+70,(-0x1.Ep+1)},{0x5.7F89DCp-27,0x4.9CAA41p+24,(-0x1.1p-1),0x9.2E1E26p+7,0x3.239615p+10},{0xA.2A0CF3p-27,(-0x1.Dp+1),0x6.Ep-1,0xF.4248B4p+67,(-0x10.2p+1)}},{{0x1.8p-1,0xE.A4509Cp+90,(-0x1.Bp-1),0xD.0B409Ap-39,0x3.239615p+10},{0xB.27D908p+70,0x6.Ep-1,0x1.5p-1,0x8.286BBEp-28,(-0x1.Ep+1)},{0x1.Cp+1,0x0.7p-1,0x9.179F1Ep+79,0x1.8p-1,(-0x5.9p-1)},{0xB.27D908p+70,0x1.E7EE25p-38,(-0x6.2p+1),0x2.6p+1,0x0.4p-1},{0x1.8p-1,0xE.AA71E2p-88,0x1.4p+1,(-0x3.Ep+1),(-0x8.9p-1)},{0xA.2A0CF3p-27,0x1.E7EE25p-38,0x0.1FEF54p+24,0x8.1DB17Cp-95,0x1.3p-1}},{{0x5.7F89DCp-27,0x0.7p-1,0x0.7p-1,0x5.7F89DCp-27,0xE.1E6137p-0},{(-0x1.8p+1),0x6.Ep-1,0x0.1FEF54p+24,(-0x7.Ap-1),0x1.0p+1},{0x1.Cp-1,0xE.A4509Cp+90,0x1.4p+1,0x3.Cp-1,0x7.9p-1},{0xF.4248B4p+67,(-0x1.Dp+1),(-0x6.2p+1),(-0x7.Ap-1),(-0x1.2p+1)},{0x2.7C866Fp-27,0x4.9CAA41p+24,0x9.179F1Ep+79,0x5.7F89DCp-27,0x6.4D1AD4p+51},{0x2.6p+1,0x0.1FEF54p+24,0x1.5p-1,0x8.1DB17Cp-95,(-0x1.2p+1)}},{{0x3.Cp-1,0x9.179F1Ep+79,(-0x1.Bp-1),(-0x3.Ep+1),0x7.9p-1},{0x1.3p+1,0x8.8D6541p+32,0x6.Ep-1,0x2.6p+1,0x1.0p+1},{0x3.Cp-1,0x7.2D96CCp+72,(-0x1.1p-1),0x1.8p-1,0xE.1E6137p-0},{0x2.6p+1,0xA.7D1E30p-75,0x0.9p+1,0x8.286BBEp-28,0x1.3p-1},{0x2.7C866Fp-27,0x7.2D96CCp+72,0xE.A4509Cp+90,0xD.0B409Ap-39,(-0x8.9p-1)},{0xF.4248B4p+67,0x8.8D6541p+32,0x8.8D6541p+32,0xF.4248B4p+67,0x0.4p-1}},{{0x1.Cp-1,0x9.179F1Ep+79,0xE.A4509Cp+90,0x9.2E1E26p+7,(-0x5.9p-1)},{(-0x1.8p+1),0x0.1FEF54p+24,0x0.9p+1,0xB.27D908p+70,0x2.B1258Cp+71},{(-0x7.5p-1),0x5.7F89DCp-27,0x1.8p-1,(-0x1.Fp-1),0xD.307A31p+33},{0x8.38739Bp+12,0xF.4248B4p+67,0x1.3p+1,0x0.5p+1,(-0x10.5p+1)},{0x1.6p-1,(-0x2.7p+1),0x2.7C866Fp-27,0x0.Fp+1,0xD.307A31p+33},{0x1.54AAB1p-94,0x1.3p+1,0xA.2A0CF3p-27,0x0.BC5657p-56,0x2.B1258Cp+71}},{{0xB.D3E61Dp+60,(-0x3.Ep+1),0x1.Cp+1,0x1.6p-1,0xF.1EC5AFp+61},{0x1.54AAB1p-94,(-0x1.8p+1),0x2.6p+1,(-0x8.Cp+1),0x8.F789EDp-27},{0x1.6p-1,0x9.2E1E26p+7,0xD.0B409Ap-39,0x6.D58AA3p+90,0xB.74B672p-91},{0x8.38739Bp+12,(-0x1.8p+1),0x3.A7D8D9p+46,(-0x2.6p+1),0x0.9p+1},{(-0x7.5p-1),(-0x3.Ep+1),(-0x3.Ep+1),(-0x7.5p-1),0x0.974D9Ep-70},{0x5.7p-1,0x1.3p+1,0x3.A7D8D9p+46,0x1.5F4F15p-93,0x0.6p+1}},{{(-0x3.2p+1),(-0x2.7p+1),0xD.0B409Ap-39,0xF.382EBAp-48,0x7.1146A4p+56},{0x0.5p+1,0xF.4248B4p+67,0x2.6p+1,0x1.5F4F15p-93,0x1.Dp+1},{0x5.DBDCA4p+34,0x5.7F89DCp-27,0x1.Cp+1,(-0x7.5p-1),0x0.24E2F9p-16},{(-0x8.Cp+1),0x3.A7D8D9p+46,0xA.2A0CF3p-27,(-0x2.6p+1),0x1.Dp+1},{0xF.382EBAp-48,0x1.Cp+1,0x2.7C866Fp-27,0x6.D58AA3p+90,0x7.1146A4p+56},{0x9.71FE1Fp+14,0x8.286BBEp-28,0x1.3p+1,(-0x8.Cp+1),0x0.6p+1}}};
        uint32_t *l_236 = &l_125;
        int16_t *l_244[1];
        uint16_t l_246 = 65529UL;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_223[i] = (-7L);
        for (i = 0; i < 1; i++)
            l_244[i] = &g_245[2][1][3];
        if (l_125)
        { /* block id: 21 */
            int32_t *l_146 = &g_4;
            int32_t **l_145 = &l_146;
            const int32_t ***l_149[2][3][1] = {{{&g_147},{&g_147},{&g_147}},{{&g_147},{&g_147},{&g_147}}};
            int16_t *l_151[10][5][5] = {{{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66}},{{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66}},{{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66}},{{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66}},{{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66}},{{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66}},{{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66}},{{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66}},{{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66}},{{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66}}};
            uint16_t *l_154 = (void*)0;
            uint16_t *l_155 = &g_156;
            int32_t l_177 = (-10L);
            int32_t l_178 = 0xD1F2F80BL;
            int32_t l_179[3][10] = {{0x6262BB9AL,(-1L),0x6262BB9AL,9L,9L,0x6262BB9AL,(-1L),0x6262BB9AL,9L,9L},{0x6262BB9AL,(-1L),0x6262BB9AL,9L,9L,0x6262BB9AL,(-1L),0x6262BB9AL,9L,9L},{0x6262BB9AL,(-1L),0x6262BB9AL,9L,9L,0x6262BB9AL,(-1L),0x6262BB9AL,9L,9L}};
            const int32_t *****l_188 = (void*)0;
            const int32_t ****l_190 = (void*)0;
            const int32_t *****l_189 = &l_190;
            uint16_t l_214 = 0x90D7L;
            int i, j, k;
            l_173[1] = func_126(p_98, (0x06L != ((p_100 != (safe_rshift_func_uint16_t_u_s(((*l_155) = (safe_add_func_uint32_t_u_u((safe_mul_func_int16_t_s_s((((safe_mod_func_int32_t_s_s(((((safe_mul_func_int16_t_s_s(0x4AB2L, (safe_rshift_func_int16_t_s_s((l_145 == (l_150 = g_147)), 6)))) <= (l_151[1][4][0] != (((safe_mod_func_uint16_t_u_u(g_119[7][6], l_124)) | p_99) , (void*)0))) == g_4) & 0x7BL), (-1L))) == 4294967295UL) && g_4), 0x2A02L)), (-1L)))), (*p_101)))) != l_157)), l_158, p_99);
            g_183--;
            (*l_189) = l_186;
            (*l_150) = func_126(((g_156 = ((g_191[0] = g_172[1]) , ((g_156 == (+(((((((**l_150) != (((safe_lshift_func_uint8_t_u_s(((safe_rshift_func_uint8_t_u_u((*****l_189), ((safe_mul_func_uint8_t_u_u((p_99 & p_98), ((safe_sub_func_uint32_t_u_u(((safe_sub_func_uint64_t_u_u((((safe_mul_func_int8_t_s_s((((((safe_mod_func_uint8_t_u_u((safe_mod_func_int64_t_s_s(((p_100 != (!(safe_rshift_func_uint16_t_u_u(((((safe_rshift_func_int8_t_s_u(0x74L, (((l_176 = (-3L)) && 0x7AF9L) && 0xE6285BD8L))) , p_100) , (*p_101)) != (*p_101)), 14)))) != p_99), (****l_186))), p_99)) , (*g_148)) <= 2L) == (***l_187)) > (****l_190)), g_66)) < p_100) ^ g_180), l_214)) <= p_99), g_191[0])) >= g_156))) , p_98))) | p_99), (****l_186))) || p_99) >= g_119[7][6])) < g_174) == (-1L)) > (*p_101)) & p_99) < p_98))) != g_4))) >= (****l_190)), p_100, p_101, p_99);
        }
        else
        { /* block id: 37 */
            (***l_186) = func_126((***l_187), g_191[2], p_101, (0x2.77EB8Ep-63 > ((safe_div_func_float_f_f(((safe_sub_func_float_f_f((safe_sub_func_float_f_f(p_100, p_98)), (g_66 != 0x7.Ep+1))) == ((+p_100) > p_99)), 0x0.Fp+1)) == 0x1.AC1E00p-77)));
        }
        g_224[5][4][1]++;
        for (l_222 = 0; (l_222 > 13); l_222 = safe_add_func_int32_t_s_s(l_222, 7))
        { /* block id: 43 */
            uint64_t l_229 = 18446744073709551608UL;
            ++l_229;
        }
        g_180 = ((&g_148 == (**l_186)) <= (((**l_150) ^ 255UL) == ((safe_sub_func_uint64_t_u_u(g_172[1], (!(((*l_236) = 0x3E12D531L) > (safe_div_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u((((l_223[0] |= (((((safe_rshift_func_uint8_t_u_s((g_4 != ((l_243 != &l_173[1]) || g_191[1])), (***l_187))) || (-1L)) == g_171) || g_172[0]) == p_98)) , l_246) | g_182), p_99)), (****l_186))))))) , (**g_147))));
    }
    else
    { /* block id: 49 */
        uint32_t l_251[2][9] = {{9UL,0xE6EE9E94L,9UL,1UL,1UL,9UL,0xE6EE9E94L,9UL,1UL},{9UL,1UL,1UL,9UL,0xE6EE9E94L,9UL,9UL,9UL,0UL}};
        uint16_t *l_258 = &g_183;
        uint16_t **l_259[2];
        int i, j;
        for (i = 0; i < 2; i++)
            l_259[i] = (void*)0;
        g_174 = ((p_99 | (((safe_div_func_int32_t_s_s(l_251[1][1], (safe_div_func_int16_t_s_s((*p_101), 1L)))) | l_251[1][1]) != g_191[2])) ^ (safe_mod_func_uint16_t_u_u((safe_div_func_uint32_t_u_u((p_101 == (g_260[9][0][1] = l_258)), ((((safe_mod_func_uint16_t_u_u(l_251[0][4], g_191[0])) | (*p_101)) ^ 1L) , (*g_148)))), g_172[0])));
    }
    return l_263;
}


/* ------------------------------------------ */
/* 
 * reads : g_156 g_66 g_172
 * writes: g_168 g_171 g_172
 */
static int32_t * func_126(uint64_t  p_127, uint32_t  p_128, int16_t * p_129, float  p_130)
{ /* block id: 24 */
    uint16_t l_161[3][7] = {{0UL,0UL,0UL,0UL,0UL,0UL,0UL},{0x5C32L,0x5C32L,0xD2F6L,0x5C32L,0x5C32L,0xD2F6L,0x5C32L},{0UL,0UL,0UL,0UL,0UL,0UL,0UL}};
    float *l_166 = (void*)0;
    float *l_167[3];
    int32_t *l_170[1];
    int32_t **l_169 = &l_170[0];
    int i, j;
    for (i = 0; i < 3; i++)
        l_167[i] = &g_168;
    for (i = 0; i < 1; i++)
        l_170[i] = &g_4;
    g_172[0] |= (((-4L) < p_128) | ((g_171 = (((g_168 = (l_161[0][1] , (safe_div_func_float_f_f(0xC.DDE2BAp+12, ((((safe_mul_func_float_f_f((p_130 = l_161[0][3]), ((void*)0 == l_169))) != (p_127 , (0x0.A8C717p+46 >= g_156))) < p_127) == g_156))))) , g_66) == g_66)) && 0UL));
    return &g_4;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_31, "g_31", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_33[i], "g_33[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_66, "g_66", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_119[i][j], "g_119[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_156, "g_156", print_hash_value);
    transparent_crc_bytes (&g_168, sizeof(g_168), "g_168", print_hash_value);
    transparent_crc(g_171, "g_171", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_172[i], "g_172[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_174, "g_174", print_hash_value);
    transparent_crc(g_180, "g_180", print_hash_value);
    transparent_crc(g_181, "g_181", print_hash_value);
    transparent_crc(g_182, "g_182", print_hash_value);
    transparent_crc(g_183, "g_183", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_191[i], "g_191[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_224[i][j][k], "g_224[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_245[i][j][k], "g_245[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_281, "g_281", print_hash_value);
    transparent_crc_bytes (&g_305, sizeof(g_305), "g_305", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_321[i][j], "g_321[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_323, "g_323", print_hash_value);
    transparent_crc(g_397, "g_397", print_hash_value);
    transparent_crc(g_403, "g_403", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_404[i][j], "g_404[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_405, "g_405", print_hash_value);
    transparent_crc(g_406, "g_406", print_hash_value);
    transparent_crc(g_422, "g_422", print_hash_value);
    transparent_crc(g_464, "g_464", print_hash_value);
    transparent_crc_bytes (&g_510, sizeof(g_510), "g_510", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_511[i], "g_511[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_580, "g_580", print_hash_value);
    transparent_crc(g_598, "g_598", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_603[i][j][k], "g_603[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_668, "g_668", print_hash_value);
    transparent_crc(g_726, "g_726", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_833[i][j][k], "g_833[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_952[i][j][k], "g_952[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1101, "g_1101", print_hash_value);
    transparent_crc(g_1179, "g_1179", print_hash_value);
    transparent_crc(g_1197, "g_1197", print_hash_value);
    transparent_crc(g_1532, "g_1532", print_hash_value);
    transparent_crc_bytes (&g_1641, sizeof(g_1641), "g_1641", print_hash_value);
    transparent_crc(g_1756, "g_1756", print_hash_value);
    transparent_crc(g_1783, "g_1783", print_hash_value);
    transparent_crc(g_1815, "g_1815", print_hash_value);
    transparent_crc(g_1871, "g_1871", print_hash_value);
    transparent_crc(g_1906, "g_1906", print_hash_value);
    transparent_crc(g_1908, "g_1908", print_hash_value);
    transparent_crc(g_2178, "g_2178", print_hash_value);
    transparent_crc_bytes (&g_2527, sizeof(g_2527), "g_2527", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 599
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 111
   depth: 2, occurrence: 28
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
   depth: 9, occurrence: 1
   depth: 13, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 1
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 34, occurrence: 1
   depth: 36, occurrence: 2
   depth: 37, occurrence: 1
   depth: 39, occurrence: 1
   depth: 48, occurrence: 1

XXX total number of pointers: 466

XXX times a variable address is taken: 1169
XXX times a pointer is dereferenced on RHS: 348
breakdown:
   depth: 1, occurrence: 251
   depth: 2, occurrence: 73
   depth: 3, occurrence: 11
   depth: 4, occurrence: 12
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 338
breakdown:
   depth: 1, occurrence: 282
   depth: 2, occurrence: 40
   depth: 3, occurrence: 12
   depth: 4, occurrence: 4
XXX times a pointer is compared with null: 49
XXX times a pointer is compared with address of another variable: 9
XXX times a pointer is compared with another pointer: 10
XXX times a pointer is qualified to be dereferenced: 9077

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1278
   level: 2, occurrence: 399
   level: 3, occurrence: 144
   level: 4, occurrence: 107
   level: 5, occurrence: 19
XXX number of pointers point to pointers: 174
XXX number of pointers point to scalars: 292
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 27.7
XXX average alias set size: 1.42

XXX times a non-volatile is read: 2257
XXX times a non-volatile is write: 1025
XXX times a volatile is read: 14
XXX    times read thru a pointer: 9
XXX times a volatile is write: 11
XXX    times written thru a pointer: 9
XXX times a volatile is available for access: 340
XXX percentage of non-volatile access: 99.2

XXX forward jumps: 0
XXX backward jumps: 4

XXX stmts: 111
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 27
   depth: 1, occurrence: 27
   depth: 2, occurrence: 20
   depth: 3, occurrence: 10
   depth: 4, occurrence: 9
   depth: 5, occurrence: 18

XXX percentage a fresh-made variable is used: 14.5
XXX percentage an existing variable is used: 85.5
********************* end of statistics **********************/


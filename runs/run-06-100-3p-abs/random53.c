/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      74983057
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   const volatile int32_t  f0;
   uint32_t  f1;
   int16_t  f2;
   volatile int8_t  f3;
};

union U1 {
   int16_t  f0;
   int8_t * f1;
};

/* --- GLOBAL VARIABLES --- */
static volatile int8_t g_3 = 0xBEL;/* VOLATILE GLOBAL g_3 */
static volatile int8_t * volatile g_2 = &g_3;/* VOLATILE GLOBAL g_2 */
static const int8_t g_5 = 0x6CL;
static float g_7 = (-0x1.6p-1);
static float * volatile g_6 = &g_7;/* VOLATILE GLOBAL g_6 */
static uint32_t g_30 = 1UL;
static union U0 g_40[7] = {{-7L},{-4L},{-7L},{-7L},{-4L},{-7L},{-7L}};
static int64_t g_42 = 0xEF3CF68729207965LL;
static volatile int64_t g_60 = 0L;/* VOLATILE GLOBAL g_60 */
static volatile int64_t *g_59 = &g_60;
static uint64_t g_64 = 0x9978317D239F94ADLL;
static uint32_t g_65 = 1UL;
static int8_t g_78 = 0x45L;
static uint64_t g_93 = 0x19AFCA6F77A31244LL;
static int64_t g_97 = 1L;
static int32_t g_100[6] = {0x1D77F595L,0x1D77F595L,0x1D77F595L,0x1D77F595L,0x1D77F595L,0x1D77F595L};
static volatile int32_t g_110 = (-7L);/* VOLATILE GLOBAL g_110 */
static const volatile int32_t g_111 = 0x094D0414L;/* VOLATILE GLOBAL g_111 */
static const volatile int32_t *g_109[1][6] = {{&g_111,&g_111,&g_111,&g_111,&g_111,&g_111}};
static uint8_t g_115 = 0xFCL;
static int32_t g_118 = 5L;
static float *g_136 = (void*)0;
static float **g_135 = &g_136;
static int32_t *g_181[2] = {&g_100[0],&g_100[0]};
static volatile int32_t g_193[3] = {(-3L),(-3L),(-3L)};
static volatile int32_t *g_192 = &g_193[1];
static volatile int32_t **g_191 = &g_192;
static volatile int32_t *** const g_190 = &g_191;
static int32_t ** const *g_220 = (void*)0;
static int32_t *g_223 = &g_118;
static int32_t ** const g_222 = &g_223;
static int32_t ** const *g_221 = &g_222;
static int32_t **g_227 = &g_223;
static int32_t ***g_226[8] = {&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227};
static uint32_t g_233 = 0UL;
static volatile uint32_t g_240 = 5UL;/* VOLATILE GLOBAL g_240 */
static volatile uint32_t *g_239 = &g_240;
static volatile uint32_t ** volatile g_238 = &g_239;/* VOLATILE GLOBAL g_238 */
static volatile uint32_t ** volatile *g_237 = &g_238;
static uint8_t g_250[1][1] = {{0x5BL}};
static int8_t g_256[5][7] = {{0x14L,(-1L),0xE6L,(-8L),0xE6L,(-1L),0x14L},{(-1L),1L,0x51L,0x94L,(-9L),0xE6L,(-8L)},{(-9L),(-8L),(-8L),(-9L),1L,0xE6L,0x14L},{0x6EL,0xE6L,(-1L),1L,1L,(-1L),0xE6L},{1L,0x6EL,0x51L,(-1L),(-8L),0x14L,0x14L}};
static uint32_t g_316 = 1UL;
static int16_t g_336 = 0x7419L;
static union U1 g_406 = {9L};
static uint32_t g_458[5] = {0xA3983209L,0xA3983209L,0xA3983209L,0xA3983209L,0xA3983209L};
static uint16_t g_487 = 1UL;
static int32_t * const **g_497 = (void*)0;
static int32_t * const ***g_496 = &g_497;
static int64_t g_526[4] = {0x5963997383FC009DLL,0x5963997383FC009DLL,0x5963997383FC009DLL,0x5963997383FC009DLL};
static int64_t *g_525 = &g_526[0];
static const float *g_553[4][5] = {{&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7,&g_7}};
static const float **g_552 = &g_553[1][0];
static const float ***g_551 = &g_552;
static uint32_t g_568 = 0UL;
static uint64_t *g_653 = &g_64;
static uint64_t * volatile *g_652 = &g_653;
static const volatile uint64_t g_677 = 18446744073709551615UL;/* VOLATILE GLOBAL g_677 */
static const volatile uint64_t *g_676 = &g_677;
static const int32_t *g_684 = &g_118;
static union U0 g_702 = {0xC5357231L};/* VOLATILE GLOBAL g_702 */
static union U0 g_716[8] = {{0xE020399AL},{0xE020399AL},{3L},{0xE020399AL},{0xE020399AL},{3L},{0xE020399AL},{0xE020399AL}};
static const int32_t *g_723[8] = {&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118};
static const int32_t ** volatile g_722 = &g_723[4];/* VOLATILE GLOBAL g_722 */
static union U0 g_729 = {-5L};/* VOLATILE GLOBAL g_729 */
static union U0 g_730 = {0x55984CCFL};/* VOLATILE GLOBAL g_730 */
static float ***g_751 = &g_135;
static int16_t * volatile *g_843 = (void*)0;
static volatile uint64_t g_866 = 18446744073709551615UL;/* VOLATILE GLOBAL g_866 */
static volatile uint64_t *g_865[7] = {(void*)0,&g_866,&g_866,(void*)0,&g_866,&g_866,(void*)0};
static volatile uint64_t * const *g_864 = &g_865[0];
static volatile union U0 g_883 = {-1L};/* VOLATILE GLOBAL g_883 */
static union U1 *g_929[1] = {&g_406};
static const uint32_t g_953 = 0x1722F779L;
static volatile int8_t g_957[5][6] = {{(-1L),(-1L),(-1L),(-1L),(-1L),0L},{5L,(-1L),0x15L,5L,0L,0x9FL},{5L,0L,0x9FL,(-1L),(-1L),0x9FL},{(-1L),(-1L),0x15L,(-7L),(-1L),0L},{(-1L),0L,(-1L),(-7L),0L,0x15L}};
static uint32_t g_988[7] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static uint16_t g_1007 = 0x08CBL;
static volatile uint16_t g_1025 = 0UL;/* VOLATILE GLOBAL g_1025 */
static float g_1067 = (-0x2.1p-1);
static union U0 ** volatile g_1098 = (void*)0;/* VOLATILE GLOBAL g_1098 */
static union U0 *g_1099[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile union U0 g_1104[5] = {{0x1493D5F8L},{0x1493D5F8L},{0x1493D5F8L},{0x1493D5F8L},{0x1493D5F8L}};
static union U0 g_1105 = {0xC51F3F36L};/* VOLATILE GLOBAL g_1105 */
static const union U1 *g_1165 = (void*)0;
static uint16_t *g_1175[4] = {&g_1007,&g_1007,&g_1007,&g_1007};
static union U0 g_1181 = {-1L};/* VOLATILE GLOBAL g_1181 */
static int16_t *g_1193[6][9] = {{(void*)0,&g_1105.f2,&g_730.f2,&g_730.f2,&g_1181.f2,&g_716[2].f2,&g_1181.f2,&g_730.f2,&g_730.f2},{&g_1105.f2,&g_1105.f2,&g_1181.f2,&g_406.f0,&g_1181.f2,&g_1105.f2,&g_1105.f2,&g_1181.f2,&g_406.f0},{&g_336,&g_1105.f2,&g_336,&g_716[2].f2,(void*)0,&g_336,&g_1181.f2,&g_336,(void*)0},{(void*)0,&g_1181.f2,&g_1181.f2,(void*)0,&g_40[1].f2,(void*)0,&g_1181.f2,&g_1181.f2,(void*)0},{&g_1181.f2,&g_716[2].f2,&g_730.f2,&g_716[2].f2,&g_1181.f2,&g_730.f2,(void*)0,&g_730.f2,(void*)0},{&g_1181.f2,&g_40[1].f2,&g_406.f0,&g_406.f0,&g_40[1].f2,&g_1181.f2,&g_40[1].f2,&g_406.f0,&g_406.f0}};
static union U0 g_1215 = {-1L};/* VOLATILE GLOBAL g_1215 */
static union U0 g_1245[1] = {{0xBABAEDCEL}};
static const uint8_t *g_1278 = &g_250[0][0];
static const uint8_t ** volatile g_1277[7][3][5] = {{{(void*)0,&g_1278,&g_1278,&g_1278,(void*)0},{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278}},{{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278},{(void*)0,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,(void*)0,&g_1278,&g_1278}},{{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,(void*)0}},{{&g_1278,&g_1278,(void*)0,&g_1278,&g_1278},{(void*)0,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278}},{{(void*)0,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,(void*)0}},{{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,(void*)0},{&g_1278,&g_1278,(void*)0,&g_1278,&g_1278}},{{(void*)0,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278},{&g_1278,&g_1278,&g_1278,&g_1278,&g_1278}}};
static volatile union U0 g_1317[2] = {{9L},{9L}};
static union U0 g_1328 = {0x36B020A0L};/* VOLATILE GLOBAL g_1328 */
static int16_t g_1380 = 0x42ADL;
static uint16_t **g_1483 = &g_1175[0];
static uint16_t ***g_1482[4][5] = {{&g_1483,&g_1483,&g_1483,&g_1483,&g_1483},{&g_1483,&g_1483,&g_1483,&g_1483,&g_1483},{&g_1483,(void*)0,&g_1483,&g_1483,&g_1483},{&g_1483,&g_1483,&g_1483,&g_1483,&g_1483}};
static volatile union U0 g_1578 = {0x82B4C730L};/* VOLATILE GLOBAL g_1578 */
static uint64_t **g_1594[1][3] = {{&g_653,&g_653,&g_653}};
static uint64_t ** const *g_1593 = &g_1594[0][1];
static uint64_t ** const ** volatile g_1592 = &g_1593;/* VOLATILE GLOBAL g_1592 */
static union U0 ***g_1711 = (void*)0;
static const union U0 g_1714[5][5] = {{{1L},{-1L},{-1L},{-1L},{-1L}},{{1L},{-1L},{-1L},{-1L},{-1L}},{{1L},{-1L},{-1L},{-1L},{-1L}},{{1L},{-1L},{-1L},{-1L},{-1L}},{{1L},{-1L},{-1L},{-1L},{-1L}}};
static union U0 *g_1717 = (void*)0;
static uint8_t *g_1737 = &g_250[0][0];
static uint8_t **g_1736[7][9][3] = {{{(void*)0,(void*)0,&g_1737},{&g_1737,(void*)0,&g_1737},{(void*)0,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,(void*)0,&g_1737},{&g_1737,&g_1737,&g_1737},{(void*)0,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737}},{{(void*)0,&g_1737,(void*)0},{(void*)0,&g_1737,&g_1737},{&g_1737,(void*)0,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737}},{{(void*)0,&g_1737,&g_1737},{&g_1737,&g_1737,(void*)0},{&g_1737,(void*)0,&g_1737},{(void*)0,&g_1737,&g_1737},{(void*)0,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,(void*)0,&g_1737}},{{&g_1737,&g_1737,&g_1737},{(void*)0,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,(void*)0,(void*)0},{&g_1737,&g_1737,&g_1737},{&g_1737,(void*)0,&g_1737},{&g_1737,&g_1737,&g_1737},{(void*)0,&g_1737,&g_1737}},{{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,(void*)0},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,(void*)0,&g_1737},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_1737},{&g_1737,&g_1737,&g_1737},{&g_1737,(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_1737},{&g_1737,(void*)0,(void*)0},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,(void*)0},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,(void*)0},{&g_1737,&g_1737,(void*)0},{&g_1737,&g_1737,&g_1737},{(void*)0,(void*)0,(void*)0}},{{(void*)0,&g_1737,&g_1737},{&g_1737,(void*)0,&g_1737},{&g_1737,&g_1737,(void*)0},{&g_1737,&g_1737,&g_1737},{&g_1737,&g_1737,(void*)0},{(void*)0,&g_1737,(void*)0},{(void*)0,(void*)0,&g_1737},{&g_1737,&g_1737,(void*)0},{&g_1737,&g_1737,&g_1737}}};
static volatile union U0 g_1741 = {1L};/* VOLATILE GLOBAL g_1741 */
static int8_t g_1759[9] = {0x6EL,0x6EL,1L,0x6EL,0x6EL,1L,0x6EL,0x6EL,1L};
static volatile int64_t g_1841 = 1L;/* VOLATILE GLOBAL g_1841 */
static const uint8_t ***g_1891 = (void*)0;
static const uint8_t ****g_1890 = &g_1891;
static float **g_1992 = &g_136;
static float *** const g_1991 = &g_1992;
static float *** const *g_1990 = &g_1991;
static volatile int32_t g_2075 = 0x73DF2D9AL;/* VOLATILE GLOBAL g_2075 */
static uint64_t ***g_2165[10][7] = {{&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][2],&g_1594[0][1]},{&g_1594[0][1],&g_1594[0][0],&g_1594[0][1],&g_1594[0][1],(void*)0,&g_1594[0][1],&g_1594[0][1]},{&g_1594[0][0],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1]},{&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][2],&g_1594[0][1]},{&g_1594[0][1],&g_1594[0][0],&g_1594[0][1],&g_1594[0][1],(void*)0,&g_1594[0][1],&g_1594[0][1]},{&g_1594[0][0],(void*)0,&g_1594[0][1],(void*)0,(void*)0,(void*)0,&g_1594[0][1]},{(void*)0,(void*)0,&g_1594[0][1],&g_1594[0][1],(void*)0,&g_1594[0][1],&g_1594[0][0]},{(void*)0,&g_1594[0][0],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],(void*)0,&g_1594[0][0]},{&g_1594[0][0],(void*)0,&g_1594[0][1],(void*)0,(void*)0,(void*)0,&g_1594[0][1]},{(void*)0,(void*)0,&g_1594[0][1],&g_1594[0][1],(void*)0,&g_1594[0][1],&g_1594[0][0]}};
static uint64_t ****g_2164 = &g_2165[3][0];
static uint64_t *****g_2163 = &g_2164;
static const union U1 **g_2206 = &g_1165;
static const union U1 *** volatile g_2205 = &g_2206;/* VOLATILE GLOBAL g_2205 */
static int32_t g_2207 = 0L;
static uint16_t * const *** const *g_2249 = (void*)0;
static float g_2298 = 0x8.92175Bp-7;
static uint64_t g_2342 = 18446744073709551615UL;
static union U0 ** volatile g_2353 = &g_1099[1];/* VOLATILE GLOBAL g_2353 */
static int64_t g_2357 = 0x16F625E6B0F8BA13LL;
static uint8_t ***g_2404 = (void*)0;
static uint8_t ****g_2403 = &g_2404;
static uint8_t ***** volatile g_2402 = &g_2403;/* VOLATILE GLOBAL g_2402 */
static uint32_t *g_2429 = &g_1105.f1;
static uint32_t **g_2428 = &g_2429;
static volatile int32_t g_2518 = 1L;/* VOLATILE GLOBAL g_2518 */
static volatile uint16_t *g_2544 = &g_1025;
static volatile uint16_t ** volatile g_2543 = &g_2544;/* VOLATILE GLOBAL g_2543 */
static union U0 g_2559 = {0xAD470340L};/* VOLATILE GLOBAL g_2559 */
static int32_t *g_2621 = (void*)0;
static int32_t * volatile *g_2620 = &g_2621;
static union U0 g_2723 = {0x8B66ADD9L};/* VOLATILE GLOBAL g_2723 */
static volatile union U0 g_2800 = {-2L};/* VOLATILE GLOBAL g_2800 */
static volatile uint8_t g_2933[3] = {0xABL,0xABL,0xABL};
static union U1 * volatile g_2942 = &g_406;/* VOLATILE GLOBAL g_2942 */
static uint32_t g_2958[6] = {0xC53C3764L,0xC53C3764L,0xC53C3764L,0xC53C3764L,0xC53C3764L,0xC53C3764L};
static volatile union U0 g_2992[7][5] = {{{0L},{-8L},{-8L},{0L},{0L}},{{0L},{-8L},{-8L},{0L},{0L}},{{0L},{-8L},{-8L},{0L},{0L}},{{0L},{-8L},{-8L},{0L},{0L}},{{0L},{-8L},{-8L},{0L},{0L}},{{0L},{-8L},{-8L},{0L},{0L}},{{0L},{-8L},{-8L},{0L},{0L}}};
static int32_t ** volatile g_3036 = (void*)0;/* VOLATILE GLOBAL g_3036 */
static union U0 g_3047 = {0xB58872C8L};/* VOLATILE GLOBAL g_3047 */
static union U0 ****g_3058 = &g_1711;
static union U0 *****g_3057 = &g_3058;
static uint32_t * volatile *g_3062 = &g_2429;
static uint32_t * volatile ** volatile g_3061 = &g_3062;/* VOLATILE GLOBAL g_3061 */
static uint32_t * volatile ** volatile * volatile g_3060 = &g_3061;/* VOLATILE GLOBAL g_3060 */
static uint32_t * volatile ** volatile * volatile *g_3059 = &g_3060;
static union U0 **g_3066 = &g_1099[4];
static union U0 ** const *g_3065[2] = {&g_3066,&g_3066};
static union U0 ** const **g_3064 = &g_3065[1];
static union U0 ** const ***g_3063 = &g_3064;
static volatile uint32_t g_3104 = 0x5F080BC9L;/* VOLATILE GLOBAL g_3104 */
static int8_t *g_3113 = &g_78;
static int32_t * const  volatile g_3114[9] = {&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118,&g_118};
static uint32_t *****g_3125 = (void*)0;
static uint16_t * const ****g_3127 = (void*)0;
static uint16_t ****g_3134[2][7][1] = {{{&g_1482[1][2]},{(void*)0},{(void*)0},{&g_1482[1][2]},{&g_1482[0][3]},{&g_1482[1][2]},{&g_1482[0][3]}},{{&g_1482[1][2]},{(void*)0},{(void*)0},{&g_1482[1][2]},{&g_1482[0][3]},{&g_1482[1][2]},{&g_1482[0][3]}}};
static uint16_t *****g_3133[9][3][8] = {{{&g_3134[0][5][0],&g_3134[0][0][0],(void*)0,&g_3134[0][3][0],&g_3134[0][1][0],&g_3134[0][3][0],&g_3134[0][1][0],&g_3134[0][3][0]},{&g_3134[1][1][0],&g_3134[1][3][0],&g_3134[1][1][0],(void*)0,&g_3134[0][3][0],&g_3134[0][3][0],(void*)0,(void*)0},{(void*)0,&g_3134[0][0][0],&g_3134[0][5][0],&g_3134[0][5][0],&g_3134[0][0][0],(void*)0,&g_3134[0][3][0],&g_3134[0][1][0]}},{{(void*)0,(void*)0,&g_3134[0][0][0],&g_3134[0][0][0],&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[0][0][0],(void*)0},{&g_3134[1][1][0],&g_3134[0][0][0],&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[0][1][0],(void*)0,(void*)0,&g_3134[0][1][0]},{&g_3134[0][5][0],&g_3134[0][1][0],&g_3134[0][1][0],&g_3134[0][5][0],&g_3134[1][1][0],(void*)0,(void*)0,(void*)0}},{{&g_3134[0][0][0],&g_3134[0][5][0],&g_3134[0][3][0],(void*)0,&g_3134[0][3][0],&g_3134[0][5][0],&g_3134[0][0][0],&g_3134[0][3][0]},{&g_3134[0][3][0],&g_3134[0][5][0],&g_3134[0][0][0],&g_3134[0][3][0],(void*)0,(void*)0,&g_3134[0][3][0],&g_3134[0][0][0]},{&g_3134[0][1][0],&g_3134[0][1][0],&g_3134[0][5][0],&g_3134[1][1][0],(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[1][1][0],&g_3134[0][0][0],&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[0][1][0],(void*)0},{&g_3134[0][0][0],(void*)0,(void*)0,&g_3134[1][1][0],&g_3134[1][1][0],(void*)0,(void*)0,&g_3134[1][3][0]},{&g_3134[0][1][0],&g_3134[0][5][0],&g_3134[1][1][0],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_3134[0][0][0],&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[0][0][0],(void*)0,(void*)0,&g_3134[1][1][0],&g_3134[1][1][0]},{&g_3134[1][1][0],&g_3134[0][5][0],&g_3134[0][1][0],&g_3134[0][1][0],&g_3134[0][5][0],&g_3134[1][1][0],(void*)0,(void*)0},{&g_3134[1][1][0],&g_3134[0][3][0],&g_3134[1][3][0],&g_3134[0][5][0],(void*)0,&g_3134[0][5][0],&g_3134[1][3][0],&g_3134[0][3][0]}},{{&g_3134[0][0][0],&g_3134[1][3][0],(void*)0,&g_3134[0][5][0],(void*)0,&g_3134[0][0][0],&g_3134[0][0][0],(void*)0},{&g_3134[0][1][0],(void*)0,(void*)0,&g_3134[0][1][0],&g_3134[0][0][0],&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[1][1][0]},{&g_3134[1][3][0],&g_3134[0][1][0],(void*)0,&g_3134[0][0][0],(void*)0,&g_3134[0][1][0],&g_3134[1][3][0],(void*)0}},{{(void*)0,&g_3134[0][1][0],&g_3134[1][3][0],(void*)0,&g_3134[0][3][0],&g_3134[0][3][0],(void*)0,&g_3134[1][3][0]},{(void*)0,(void*)0,&g_3134[0][1][0],&g_3134[0][0][0],&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[1][1][0],&g_3134[0][0][0]},{(void*)0,&g_3134[1][3][0],&g_3134[0][0][0],&g_3134[1][3][0],(void*)0,&g_3134[0][5][0],(void*)0,&g_3134[0][0][0]}},{{&g_3134[1][3][0],&g_3134[0][3][0],&g_3134[1][1][0],&g_3134[0][0][0],&g_3134[0][0][0],&g_3134[1][1][0],&g_3134[0][3][0],&g_3134[1][3][0]},{&g_3134[0][1][0],&g_3134[0][5][0],&g_3134[1][1][0],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_3134[0][0][0],&g_3134[0][3][0],&g_3134[0][0][0],&g_3134[0][0][0],(void*)0,(void*)0,&g_3134[1][1][0],&g_3134[1][1][0]}},{{&g_3134[1][1][0],&g_3134[0][5][0],&g_3134[0][1][0],&g_3134[0][1][0],&g_3134[0][5][0],&g_3134[1][1][0],(void*)0,(void*)0},{&g_3134[1][1][0],&g_3134[0][3][0],&g_3134[1][3][0],&g_3134[0][5][0],(void*)0,&g_3134[0][5][0],&g_3134[1][3][0],&g_3134[0][3][0]},{&g_3134[0][0][0],&g_3134[1][3][0],(void*)0,&g_3134[0][5][0],(void*)0,&g_3134[0][0][0],&g_3134[0][0][0],(void*)0}}};
static uint32_t g_3161 = 0UL;
static union U0 g_3220 = {0L};/* VOLATILE GLOBAL g_3220 */
static union U0 ***g_3240 = &g_3066;
static union U0 **** const g_3239 = &g_3240;
static union U0 **** const *g_3238[3][10][4] = {{{&g_3239,&g_3239,&g_3239,(void*)0},{&g_3239,(void*)0,&g_3239,&g_3239},{&g_3239,(void*)0,(void*)0,(void*)0},{(void*)0,&g_3239,(void*)0,&g_3239},{&g_3239,&g_3239,&g_3239,(void*)0},{&g_3239,&g_3239,&g_3239,(void*)0},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,(void*)0,(void*)0,&g_3239},{(void*)0,&g_3239,(void*)0,&g_3239},{&g_3239,&g_3239,&g_3239,&g_3239}},{{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,(void*)0,&g_3239,&g_3239},{&g_3239,&g_3239,(void*)0,(void*)0},{&g_3239,&g_3239,(void*)0,(void*)0},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,&g_3239,(void*)0,&g_3239},{(void*)0,&g_3239,&g_3239,(void*)0},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,&g_3239,&g_3239,&g_3239}},{{(void*)0,&g_3239,&g_3239,&g_3239},{(void*)0,&g_3239,&g_3239,(void*)0},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,&g_3239,&g_3239,&g_3239},{(void*)0,&g_3239,(void*)0,&g_3239},{(void*)0,&g_3239,&g_3239,(void*)0},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,&g_3239,&g_3239,&g_3239},{&g_3239,&g_3239,&g_3239,&g_3239}}};
static union U0 **** const g_3242 = (void*)0;
static union U0 **** const *g_3241 = &g_3242;
static float * volatile g_3255 = &g_1067;/* VOLATILE GLOBAL g_3255 */


/* --- FORWARD DECLARATIONS --- */
static int64_t  func_1(void);
static int16_t  func_12(const uint32_t  p_13, float * p_14, int8_t  p_15, int64_t  p_16, float  p_17);
static const uint32_t  func_18(float * p_19, float * p_20, float  p_21, float * p_22);
static float * func_24(uint32_t  p_25, int8_t * p_26);
static int8_t * func_31(union U1  p_32, int8_t * p_33);
static uint32_t  func_34(uint16_t  p_35, int64_t  p_36, int8_t * const  p_37, int8_t * p_38, uint32_t * p_39);
static union U0  func_44(int16_t  p_45);
static int16_t  func_50(uint32_t * p_51, const int8_t * p_52, int8_t * p_53);
static uint32_t * func_54(uint8_t  p_55, uint32_t * p_56, int64_t * p_57);
static const int8_t  func_79(int64_t  p_80, uint32_t  p_81);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_6 g_5 g_30 g_40 g_42 g_40.f3 g_40.f2 g_59 g_65 g_64 g_100 g_93 g_40.f1 g_118 g_7 g_135 g_115 g_190 g_223 g_97 g_237 g_250 g_233 g_78 g_222 g_227 g_676 g_525 g_526 g_652 g_653 g_568 g_729.f1 g_458 g_702.f1 g_552 g_553 g_192 g_193 g_957 g_3 g_988 g_551 g_136 g_256 g_1025 g_336 g_729 g_487 g_716.f2 g_677 g_191 g_1104 g_1105 g_1007 g_684 g_702.f0 g_729.f2 g_1181 g_406 g_716.f1 g_238 g_239 g_240 g_1215 g_1215.f3 g_1245 g_1277 g_1278 g_1317 g_60 g_1328 g_716 g_722 g_316 g_1759 g_2543 g_2544 g_1483 g_1175 g_1215.f2 g_2933 g_1992 g_2942 g_2207 g_2723.f2 g_1990 g_1991 g_2958 g_1714.f1 g_2992 g_2723.f1 g_3047 g_2428 g_2429 g_1105.f1 g_1592 g_1593 g_1594 g_3059 g_3063 g_3060 g_3061 g_3114 g_3125 g_3062 g_3127 g_2164 g_2165 g_3161 g_406.f0 g_3113 g_1181.f2 g_751 g_2723 g_3255 g_1737
 * writes: g_7 g_30 g_42 g_64 g_78 g_100 g_109 g_118 g_115 g_181 g_220 g_221 g_226 g_233 g_65 g_97 g_250 g_93 g_223 g_406.f0 g_568 g_729.f1 g_929 g_702.f1 g_487 g_256 g_730.f2 g_1007 g_136 g_193 g_336 g_1099 g_192 g_1165 g_1193 g_723 g_1067 g_1759 g_526 g_1215.f2 g_2933 g_2207 g_2958 g_2723.f1 g_653 g_3057 g_3133 g_1105.f1 g_1992 g_135 g_3238 g_3241
 */
static int64_t  func_1(void)
{ /* block id: 0 */
    const int8_t *l_4 = &g_5;
    float *l_23 = &g_7;
    uint32_t *l_29 = &g_30;
    int64_t *l_41 = &g_42;
    int32_t l_61 = (-1L);
    uint64_t *l_62 = (void*)0;
    uint64_t *l_63 = &g_64;
    uint32_t *l_254 = (void*)0;
    uint32_t **l_253 = &l_254;
    int8_t *l_255 = &g_256[2][3];
    int16_t *l_678 = &g_406.f0;
    uint16_t l_1342 = 0x0EA7L;
    int64_t l_1343[9][4][5] = {{{0L,0x03520CE54AA0B436LL,0L,0L,1L},{0x83AF2BFFD5FE91F1LL,0x8F204C4E528AA904LL,0x5BFDE36C83DB3C0DLL,0L,0L},{(-9L),(-8L),9L,(-1L),0xB3C22FD5B78E17C7LL},{(-1L),0x4A367FE8C1F6FF0ALL,0x5BFDE36C83DB3C0DLL,0L,(-1L)}},{{0xFC10BAE36D0D93CBLL,4L,0L,0xAAD0CCAA97462CDELL,(-9L)},{0x165D329AB9243E36LL,0L,0L,(-1L),0xB6E5988FD053AA08LL},{0xE1A0B75F3D65ED98LL,0xD200A916AF6FFD6DLL,0xB6E5988FD053AA08LL,0x2C4032753AC4E1C5LL,0x83AF2BFFD5FE91F1LL},{0x8F204C4E528AA904LL,0x624593C0BECD0655LL,0xABBC9CEDA07C2D48LL,0x4A367FE8C1F6FF0ALL,0x0D19A98F31050FD2LL}},{{0L,0x9ED77690646FC76ALL,0L,(-1L),0x0D19A98F31050FD2LL},{(-1L),0xE5F8208EF81CF263LL,(-1L),0xA8BE8BF1431EDC13LL,0x83AF2BFFD5FE91F1LL},{0xD200A916AF6FFD6DLL,0xA8BE8BF1431EDC13LL,0x165D329AB9243E36LL,(-1L),0xB6E5988FD053AA08LL},{0xABBC9CEDA07C2D48LL,0x5BFDE36C83DB3C0DLL,(-9L),0L,(-9L)}},{{0x844EDCF8F3E55C1CLL,0x844EDCF8F3E55C1CLL,0x624593C0BECD0655LL,0x15A0E84DDD5C7D95LL,(-1L)},{0x9ED77690646FC76ALL,0L,0x27802B3FC4FDD8D8LL,0xF608EF2957F1498ALL,0xB3C22FD5B78E17C7LL},{0x27802B3FC4FDD8D8LL,0x13BD766A2BA22590LL,(-6L),1L,0L},{(-3L),0L,0x13BD766A2BA22590LL,0L,1L}},{{(-8L),0x844EDCF8F3E55C1CLL,0x3D368CF97880265CLL,0x9ED77690646FC76ALL,0xE1A0B75F3D65ED98LL},{0xF608EF2957F1498ALL,0x5BFDE36C83DB3C0DLL,0xFC10BAE36D0D93CBLL,(-9L),0x624593C0BECD0655LL},{0x624593C0BECD0655LL,0xA8BE8BF1431EDC13LL,0xAAD0CCAA97462CDELL,(-8L),(-5L)},{(-6L),0xE5F8208EF81CF263LL,0L,0xB6E5988FD053AA08LL,0x869971DFA7B72394LL}},{{0x15A0E84DDD5C7D95LL,0x9ED77690646FC76ALL,0L,0xB6E5988FD053AA08LL,0xFC10BAE36D0D93CBLL},{(-5L),0x624593C0BECD0655LL,1L,(-8L),1L},{0L,0xD200A916AF6FFD6DLL,1L,(-9L),0x8F204C4E528AA904LL},{0L,0L,0x9ED77690646FC76ALL,0x9ED77690646FC76ALL,0L}},{{0xB3C22FD5B78E17C7LL,4L,0x83AF2BFFD5FE91F1LL,0L,1L},{0xD74F969CED59AB1ELL,0x4A367FE8C1F6FF0ALL,0xF0312C75B2E9AE7ALL,1L,0x2C4032753AC4E1C5LL},{0L,(-8L),0xE5F8208EF81CF263LL,(-1L),0L},{0L,1L,1L,0x0A4758933156A90ELL,0xA8BE8BF1431EDC13LL}},{{0x83AF2BFFD5FE91F1LL,0L,0xF608EF2957F1498ALL,0x5BFDE36C83DB3C0DLL,0xFC10BAE36D0D93CBLL},{0xE5F8208EF81CF263LL,0x83AF2BFFD5FE91F1LL,0L,0x3D368CF97880265CLL,0x8074B646EE9B8C80LL},{0x869971DFA7B72394LL,0x8F204C4E528AA904LL,0xFC10BAE36D0D93CBLL,(-3L),0xB6E5988FD053AA08LL},{4L,0xE5F8208EF81CF263LL,1L,(-5L),0x27802B3FC4FDD8D8LL}},{{0x0A4758933156A90ELL,0L,1L,(-6L),0x13BD766A2BA22590LL},{0xF0312C75B2E9AE7ALL,(-1L),0xFC10BAE36D0D93CBLL,0L,0L},{0x15A0E84DDD5C7D95LL,0x0D19A98F31050FD2LL,0L,0x2C4032753AC4E1C5LL,0x625783D57EEE3691LL},{(-1L),0x13BD766A2BA22590LL,0xF608EF2957F1498ALL,0x13BD766A2BA22590LL,(-1L)}}};
    union U1 l_2671 = {7L};
    uint32_t *l_2957 = &g_2958[0];
    uint8_t l_2960 = 0xBCL;
    uint16_t ** const l_2973 = (void*)0;
    uint16_t ** const *l_2972 = &l_2973;
    uint16_t ** const **l_2971[8][2];
    uint64_t l_2979 = 18446744073709551615UL;
    int32_t ** const l_2995 = (void*)0;
    union U0 **l_3007 = &g_1099[4];
    union U0 ***l_3006 = &l_3007;
    uint32_t l_3014 = 0x02A1C34FL;
    int32_t l_3017 = 0xC0913CE5L;
    int32_t l_3019 = (-1L);
    int32_t l_3020 = 0L;
    int32_t l_3022 = 0x93B6FC76L;
    int32_t l_3024 = (-4L);
    int32_t l_3026 = 0L;
    int32_t l_3027[3][1][8] = {{{0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L}},{{0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L}},{{0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L,0xFD99D8E3L}}};
    int64_t l_3029 = 0L;
    uint16_t l_3031 = 65529UL;
    union U1 **l_3073 = &g_929[0];
    int32_t l_3111 = 1L;
    uint16_t * const **l_3130 = (void*)0;
    uint16_t * const ***l_3129[6][8] = {{(void*)0,(void*)0,&l_3130,&l_3130,&l_3130,&l_3130,&l_3130,(void*)0},{(void*)0,&l_3130,(void*)0,&l_3130,&l_3130,(void*)0,&l_3130,(void*)0},{&l_3130,&l_3130,(void*)0,&l_3130,(void*)0,&l_3130,&l_3130,&l_3130},{&l_3130,&l_3130,(void*)0,(void*)0,&l_3130,&l_3130,&l_3130,&l_3130},{&l_3130,&l_3130,&l_3130,&l_3130,&l_3130,(void*)0,(void*)0,&l_3130},{&l_3130,&l_3130,&l_3130,&l_3130,(void*)0,&l_3130,(void*)0,&l_3130}};
    uint16_t * const ****l_3128 = &l_3129[3][2];
    int16_t l_3136 = 0xBBD5L;
    const float ****l_3154[3];
    uint8_t l_3297 = 1UL;
    int16_t l_3300[9] = {0x1315L,0x1315L,0x1315L,0x1315L,0x1315L,0x1315L,0x1315L,0x1315L,0x1315L};
    float l_3317 = 0x7.6p-1;
    int64_t *l_3319 = &l_3029;
    float *l_3320 = &g_2298;
    float **l_3321 = &l_23;
    int i, j, k;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
            l_2971[i][j] = &l_2972;
    }
    for (i = 0; i < 3; i++)
        l_3154[i] = &g_551;
    (*g_6) = (g_2 != l_4);
    if (((safe_lshift_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(func_12(((*l_2957) &= func_18(l_23, func_24(((*l_29) ^= (safe_rshift_func_int16_t_s_s(g_5, 4))), func_31((func_34((g_40[1] , (((*l_41) &= 0xE6B9338886A6C65BLL) > ((safe_unary_minus_func_uint16_t_u((l_23 != (func_44((safe_div_func_int32_t_s_s(((g_40[1].f3 & (0x94CCL || (safe_mul_func_int16_t_s_s(((*l_678) = func_50(((*l_253) = func_54(((safe_unary_minus_func_uint64_t_u(((g_40[1].f2 != ((*l_63) = ((g_59 == l_41) , l_61))) > l_61))) != g_65), l_23, l_63)), l_4, l_255)), (-1L))))) ^ l_61), l_61))) , (*g_135))))) && l_1342))), l_1343[7][3][2], l_255, l_255, &g_316) , l_2671), &g_1759[2])), l_2671.f0, l_29)), l_2957, g_1714[1][2].f1, l_1343[7][3][2], l_1343[3][1][0]), 11)), l_1343[7][3][2])) && 0x500AB690L))
    { /* block id: 1218 */
        uint16_t ** const ***l_2965 = (void*)0;
        uint16_t *l_2970 = &g_1007;
        uint16_t ** const l_2969 = &l_2970;
        uint16_t ** const *l_2968 = &l_2969;
        uint16_t ** const **l_2967[7];
        uint16_t ** const ***l_2966[3][6] = {{&l_2967[6],&l_2967[6],&l_2967[6],&l_2967[4],&l_2967[4],&l_2967[6]},{&l_2967[6],&l_2967[6],&l_2967[4],&l_2967[1],&l_2967[2],&l_2967[1]},{&l_2967[6],&l_2967[6],&l_2967[6],&l_2967[6],&l_2967[4],&l_2967[4]}};
        int32_t l_2978 = 1L;
        uint8_t *l_2981 = &g_115;
        uint32_t *l_2996 = &g_988[0];
        union U1 l_2997 = {0x856CL};
        uint32_t l_3015 = 4294967287UL;
        int32_t l_3021 = 0xB329DDABL;
        int32_t l_3023 = 0xF2DF78B2L;
        int32_t l_3025 = 4L;
        int32_t l_3028 = 0xBAD3ACC6L;
        int32_t l_3030[4][5] = {{0xB1EBBDCCL,(-6L),0x99EB6BB6L,0xB1EBBDCCL,0x99EB6BB6L},{0xB1EBBDCCL,0xB1EBBDCCL,0xF45C8A07L,0x83FFA652L,(-10L)},{(-6L),(-10L),0x99EB6BB6L,0x99EB6BB6L,(-10L)},{(-10L),(-6L),(-6L),(-10L),0x99EB6BB6L}};
        const uint64_t *l_3046 = &l_2979;
        uint8_t ****l_3083 = &g_2404;
        uint32_t ***l_3124[5][3] = {{&g_2428,&g_2428,&g_2428},{&l_253,&l_253,&l_253},{&g_2428,&g_2428,&g_2428},{&l_253,&l_253,&l_253},{&g_2428,&g_2428,&g_2428}};
        uint32_t ****l_3123 = &l_3124[1][1];
        uint32_t *****l_3122 = &l_3123;
        uint16_t ****l_3132 = &g_1482[1][4];
        uint16_t *****l_3131 = &l_3132;
        uint64_t ** const * const l_3135[10] = {&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1],&g_1594[0][1]};
        uint64_t *****l_3200 = &g_2164;
        const uint32_t l_3203[5][5][8] = {{{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L},{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL},{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL},{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L},{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL}},{{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL},{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L},{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL},{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL},{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L}},{{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL},{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL},{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L},{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL},{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL}},{{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L},{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL},{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL},{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L},{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL}},{{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL},{9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L},{9UL,9UL,0x0728A07CL,9UL,9UL,0x0728A07CL,9UL,9UL},{0xE409C3A6L,9UL,0xE409C3A6L,0xE409C3A6L,9UL,0xE409C3A6L,0x0728A07CL,0xE409C3A6L},{0xE409C3A6L,0x0728A07CL,0x0728A07CL,0xE409C3A6L,0x0728A07CL,0x0728A07CL,0xE409C3A6L,0x0728A07CL}}};
        const uint8_t l_3206 = 0UL;
        int32_t *l_3214[1];
        int64_t l_3272[6][8][5] = {{{0x8E322F1965CD07EFLL,0x32BD9AF2C2571192LL,0xF410CB10D6D0D7C6LL,0x3E53D27913639274LL,0L},{(-10L),0x6DBD03AA67958E61LL,1L,0xD281496EEBE160FALL,0xC65E2C39E866FD9DLL},{(-1L),(-10L),4L,(-7L),(-8L)},{(-7L),1L,(-1L),4L,(-6L)},{0xAD3E32B2A1E0CA1FLL,0x3E53D27913639274LL,0x1C55A6A0883AE26DLL,5L,0x6DBD03AA67958E61LL},{0L,0x3E53D27913639274LL,0x48BC4B3EBAF2DEF8LL,0xAD3E32B2A1E0CA1FLL,0xD3CCAC2C6AB9449FLL},{(-3L),1L,(-6L),(-1L),0xC26AC481A7FD696ALL},{0x7E4FBB0AA580FAEELL,(-10L),(-7L),0x673BE293221905C7LL,(-1L)}},{{7L,0x6DBD03AA67958E61LL,0xD281496EEBE160FALL,0x621E2A80734E35BALL,0x18C860C2FB023D76LL},{1L,0x32BD9AF2C2571192LL,0L,0x3C1AF6E246C836F1LL,0x3C1AF6E246C836F1LL},{(-1L),1L,(-1L),0xA14071F31DFF66AELL,(-7L)},{1L,(-9L),0xD40B74D87A613AACLL,0x3E53D27913639274LL,2L},{(-10L),7L,(-1L),0xAD3E32B2A1E0CA1FLL,(-3L)},{(-1L),0x48BC4B3EBAF2DEF8LL,0xD40B74D87A613AACLL,2L,1L},{1L,0xF410CB10D6D0D7C6LL,(-1L),0x7E4FBB0AA580FAEELL,(-1L)},{(-10L),(-1L),0L,7L,0x6DBD03AA67958E61LL}},{{0xDA26A559345017E6LL,0L,0xD281496EEBE160FALL,0x112A11D4E654DA28LL,0x522E54C639E57805LL},{(-1L),1L,(-7L),8L,(-1L)},{0x97B6062AE6A96C2BLL,(-10L),(-6L),0x673BE293221905C7LL,0x287166709BFC8A81LL},{0x6870F465DEF5D10FLL,7L,0x48BC4B3EBAF2DEF8LL,0xC65E2C39E866FD9DLL,0L},{1L,0L,0x1C55A6A0883AE26DLL,0xC65E2C39E866FD9DLL,(-7L)},{0xC6D6C64A4FD462FBLL,0xAF94BE8E5864EBD0LL,(-1L),0x673BE293221905C7LL,0L},{0x8E322F1965CD07EFLL,0x18C860C2FB023D76LL,4L,8L,2L},{0x48BC4B3EBAF2DEF8LL,0x6870F465DEF5D10FLL,1L,0x112A11D4E654DA28LL,0x3C1AF6E246C836F1LL}},{{(-7L),0x48BC4B3EBAF2DEF8LL,0xF410CB10D6D0D7C6LL,7L,(-8L)},{0x287166709BFC8A81LL,0x7E4FBB0AA580FAEELL,0x3C1AF6E246C836F1LL,0x7E4FBB0AA580FAEELL,0x287166709BFC8A81LL},{(-10L),0x3E53D27913639274LL,0xDA26A559345017E6LL,2L,(-1L)},{0xDA26A559345017E6LL,0xD3CCAC2C6AB9449FLL,0x112A11D4E654DA28LL,0xAD3E32B2A1E0CA1FLL,0xE08896A925AC60DFLL},{0xC6D6C64A4FD462FBLL,0xF410CB10D6D0D7C6LL,(-7L),0x3E53D27913639274LL,(-1L)},{0x7E4FBB0AA580FAEELL,0xAD3E32B2A1E0CA1FLL,0x6D474F643278031CLL,0xA14071F31DFF66AELL,0x287166709BFC8A81LL},{(-1L),0x6870F465DEF5D10FLL,0xD281496EEBE160FALL,0x3C1AF6E246C836F1LL,(-8L)},{0xF410CB10D6D0D7C6LL,0L,0L,0x621E2A80734E35BALL,0x3C1AF6E246C836F1LL}},{{0xC65E2C39E866FD9DLL,0xB0930C42D46B024DLL,0x3C1AF6E246C836F1LL,0x673BE293221905C7LL,2L},{0L,(-9L),1L,(-1L),0L},{0x48BC4B3EBAF2DEF8LL,(-1L),(-1L),0xAD3E32B2A1E0CA1FLL,(-7L)},{1L,(-10L),0xF410CB10D6D0D7C6LL,5L,0L},{0L,2L,0x522E54C639E57805LL,0x536AA39EEB0125AFLL,0L},{0xC26AC481A7FD696ALL,0xAD3E32B2A1E0CA1FLL,0x3C1AF6E246C836F1LL,0L,0x0234978A155C3F98LL},{0xC65E2C39E866FD9DLL,2L,0x6DBD03AA67958E61LL,(-1L),0x48BC4B3EBAF2DEF8LL},{8L,0xAF94BE8E5864EBD0LL,0xDA26A559345017E6LL,(-10L),0xA14071F31DFF66AELL}},{{0xFC72B0C6363C915ELL,0x6870F465DEF5D10FLL,(-1L),4L,(-1L)},{0x3EAA27DD767D018CLL,0x3EAA27DD767D018CLL,(-1L),8L,1L},{0xAF94BE8E5864EBD0LL,0x287166709BFC8A81LL,0xC65E2C39E866FD9DLL,0L,0xE08896A925AC60DFLL},{0L,7L,0x522E54C639E57805LL,0xD40B74D87A613AACLL,(-3L)},{(-7L),0x287166709BFC8A81LL,0x536AA39EEB0125AFLL,0x48BC4B3EBAF2DEF8LL,0L},{0x6DBD03AA67958E61LL,0x3EAA27DD767D018CLL,0L,(-1L),0xD3CCAC2C6AB9449FLL},{0L,0x6870F465DEF5D10FLL,1L,0x32BD9AF2C2571192LL,0x6D474F643278031CLL},{1L,0xAF94BE8E5864EBD0LL,0xE08896A925AC60DFLL,2L,(-1L)}}};
        const union U1 ***l_3283 = (void*)0;
        const union U1 ****l_3282 = &l_3283;
        int32_t *l_3316 = &l_3025;
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_2967[i] = &l_2968;
        for (i = 0; i < 1; i++)
            l_3214[i] = &g_2207;
        (*g_191) = (*g_191);
        (*g_222) = func_24((safe_mul_func_uint16_t_u_u(l_2671.f0, l_1343[7][3][2])), func_31(((((safe_lshift_func_int8_t_s_s((((safe_sub_func_int16_t_s_s(((safe_div_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u(0UL, (l_2978 , (g_2992[0][3] , (safe_add_func_uint8_t_u_u((l_2995 == (void*)0), ((&g_988[4] != l_2996) , l_2978))))))), l_2978)) >= l_2979), l_2978)) || 4294967286UL) & l_2978), 7)) <= 0x4EL) > l_2978) , l_2997), l_255));
        if ((safe_sub_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((((safe_mul_func_uint16_t_u_u(((**g_1483)++), 0xFAFBL)) & (((l_3006 = (void*)0) == &l_3007) && (safe_sub_func_int64_t_s_s((l_2978 == l_1343[7][3][2]), (safe_mul_func_int16_t_s_s((safe_div_func_int32_t_s_s((((((1UL || (5UL <= l_2978)) >= l_3014) == (*g_525)) , l_3015) <= l_2979), 0x65893FF3L)), 5L)))))) , (*g_2)), l_3015)), l_2978)))
        { /* block id: 1227 */
            int32_t *l_3016[1][3];
            int64_t l_3018 = 0x82FC3992BB49041CLL;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_3016[i][j] = &g_2207;
            }
            l_3031--;
            if ((safe_mul_func_uint16_t_u_u(l_3028, 0L)))
            { /* block id: 1229 */
                int32_t **l_3037 = &l_3016[0][0];
                (*l_3037) = ((*g_227) = l_3016[0][0]);
            }
            else
            { /* block id: 1232 */
                for (g_2723.f1 = 10; (g_2723.f1 == 19); ++g_2723.f1)
                { /* block id: 1235 */
                    return (*g_59);
                }
            }
        }
        else
        { /* block id: 1239 */
            const float l_3053 = (-0x1.4p-1);
            union U0 ** const ***l_3067[9] = {&g_3064,&g_3064,&g_3064,&g_3064,&g_3064,&g_3064,&g_3064,&g_3064,&g_3064};
            int32_t l_3070[4] = {0x9F18A70AL,0x9F18A70AL,0x9F18A70AL,0x9F18A70AL};
            uint8_t ****l_3082 = &g_2404;
            uint32_t l_3098 = 1UL;
            float ** const *l_3156 = &g_135;
            float ** const **l_3155 = &l_3156;
            int64_t l_3168[3][10] = {{(-1L),0x3374C0A17E59F997LL,0x3374C0A17E59F997LL,(-1L),0x3374C0A17E59F997LL,0x3374C0A17E59F997LL,(-1L),0x3374C0A17E59F997LL,0x3374C0A17E59F997LL,(-1L)},{0x3374C0A17E59F997LL,(-1L),0x3374C0A17E59F997LL,0x3374C0A17E59F997LL,(-1L),0x3374C0A17E59F997LL,0x3374C0A17E59F997LL,(-1L),0x3374C0A17E59F997LL,0x3374C0A17E59F997LL},{(-1L),(-1L),0x7EBADC7DDB63FA31LL,(-1L),(-1L),0x7EBADC7DDB63FA31LL,(-1L),(-1L),0x7EBADC7DDB63FA31LL,(-1L)}};
            uint64_t **l_3189 = &g_653;
            uint8_t **l_3244[5];
            const union U1 l_3261[10] = {{1L},{1L},{1L},{1L},{1L},{1L},{1L},{1L},{1L},{1L}};
            int32_t * const ****l_3273 = (void*)0;
            uint32_t l_3293 = 0UL;
            int i, j;
            for (i = 0; i < 5; i++)
                l_3244[i] = &g_1737;
            for (g_64 = (-22); (g_64 == 53); g_64++)
            { /* block id: 1242 */
                float l_3068 = 0x0.4p-1;
                int32_t l_3069 = 0x7882AF5AL;
                for (g_65 = 0; (g_65 == 12); ++g_65)
                { /* block id: 1245 */
                    uint64_t *l_3048 = &g_2342;
                    union U0 *****l_3056 = (void*)0;
                    (*g_191) = ((safe_sub_func_uint16_t_u_u(0UL, (l_3046 == (g_3047 , l_3048)))) , ((safe_lshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s(((*l_678) = ((**g_2428) , (((safe_mul_func_uint8_t_u_u((((((((***g_1592) = (***g_1592)) != (((g_3057 = l_3056) != (l_3067[5] = (((g_3059 == (l_3028 , (void*)0)) , g_256[2][3]) , g_3063))) , l_3048)) , 1L) , l_3069) , (****g_1592)) == l_3070[1]), l_3030[2][1])) ^ 0UL) == l_2978))), l_3023)), 2)) , (**g_190)));
                    if (l_3069)
                        continue;
                }
            }
            if (((l_2978 = ((safe_div_func_int64_t_s_s(((((void*)0 != l_3073) && ((((safe_sub_func_int8_t_s_s((((~0x2DB5L) & (l_3070[1] ^= (~(*g_1278)))) | (*g_653)), (safe_div_func_uint32_t_u_u(((*g_3060) == &g_2428), (safe_mod_func_uint32_t_u_u(l_3021, (l_2997.f0 , (**g_2428)))))))) <= l_2978) , l_3082) == l_3083)) != l_2978), 0xD516E8A7B152990FLL)) , 0x93C1B705L)) , l_3070[1]))
            { /* block id: 1256 */
                return (*g_59);
            }
            else
            { /* block id: 1258 */
                int32_t *l_3084 = &l_3070[3];
                int32_t *l_3085 = &l_3070[1];
                int32_t *l_3086 = &l_3027[1][0][5];
                int32_t *l_3087 = &l_3026;
                int32_t *l_3088 = &l_3028;
                int32_t *l_3089 = &l_3025;
                int32_t *l_3090 = &l_3021;
                int32_t *l_3091 = &l_3070[3];
                int32_t *l_3092 = &l_3023;
                int32_t *l_3093 = (void*)0;
                int32_t *l_3094 = (void*)0;
                int32_t *l_3095 = &l_3026;
                int32_t *l_3096 = &l_3027[1][0][2];
                int32_t *l_3097[2][3][5] = {{{&l_3021,&l_3019,&g_100[0],&g_100[0],&l_3019},{&g_100[5],&l_3017,(void*)0,(void*)0,&l_3017},{&l_3021,&l_3019,&g_100[0],&g_100[0],&l_3019}},{{&g_100[5],&l_3017,(void*)0,(void*)0,&l_3017},{&l_3021,&l_3019,&g_100[0],&g_100[0],&l_3019},{&g_100[5],&l_3017,(void*)0,(void*)0,&l_3017}}};
                int8_t l_3112 = (-7L);
                int32_t l_3175 = 1L;
                uint64_t **l_3190 = (void*)0;
                int i, j, k;
                l_3098++;
                for (l_3022 = 3; (l_3022 >= 0); l_3022 -= 1)
                { /* block id: 1262 */
                    int32_t * volatile *l_3115 = &l_3093;
                    uint32_t *****l_3126 = (void*)0;
                    uint64_t **l_3188 = &l_63;
                    int i;
                    (*l_3115) = g_3114[3];
                    for (l_3017 = 5; (l_3017 >= 0); l_3017 -= 1)
                    { /* block id: 1266 */
                        int i;
                        l_3030[2][1] = (safe_mul_func_float_f_f((safe_div_func_float_f_f((safe_add_func_float_f_f((((l_3122 = l_3122) == (l_3126 = g_3125)) == (((l_3128 = ((*****g_3059) , (l_3030[3][3] , g_3127))) != (g_3133[0][0][4] = l_3131)) != (((*l_23) = ((((((*g_525) ^ ((g_100[(l_3022 + 1)] = 0xA5F710C5L) , (l_3070[1] , (*g_525)))) , (*g_2164)) == l_3135[1]) , (*l_3087)) == l_3070[3])) >= l_3136))), l_2978)), 0x1.1p-1)), 0xA.068E5Cp+96));
                        g_100[l_3017] |= l_3098;
                    }
                    (*l_3096) &= (((l_3070[1] & 0x5FCF1428L) >= (safe_div_func_int8_t_s_s((safe_mul_func_int8_t_s_s(((safe_add_func_int8_t_s_s(((((safe_mul_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(l_3028, ((safe_sub_func_int8_t_s_s((safe_add_func_uint8_t_u_u((safe_unary_minus_func_uint8_t_u(((((**g_2428)++) <= (l_3154[1] != l_3155)) | (safe_mul_func_int16_t_s_s((((*l_63) = (g_3161 < (((*l_678) &= (safe_lshift_func_int16_t_s_u(6L, 13))) <= (safe_div_func_int16_t_s_s(((safe_add_func_uint8_t_u_u(l_3168[1][8], (safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((*g_3113) = ((*l_255) &= (safe_mod_func_uint32_t_u_u(0x9578DB24L, l_3175)))), 0x3CL)), 0x22BAL)))) != (*g_2)), l_3025))))) && 0xA68BD1E320BAC4A4LL), 7L))))), (-5L))), g_1181.f2)) | l_3070[2]))), l_3028)) , (*l_3091)) >= 1UL) , (*g_2)), g_97)) <= (*l_3087)), 0x3FL)), l_3070[1]))) && (**l_3115));
                    for (l_2978 = 3; (l_2978 >= 0); l_2978 -= 1)
                    { /* block id: 1284 */
                        int32_t ****l_3191 = &g_226[6];
                        int i, j;
                        l_3030[l_2978][(l_3022 + 1)] |= (((((safe_rshift_func_int8_t_s_u((*g_3113), (safe_rshift_func_uint16_t_u_s((safe_sub_func_int64_t_s_s((safe_sub_func_int8_t_s_s((safe_div_func_uint32_t_u_u((**g_238), (safe_sub_func_uint16_t_u_u(((***g_551) , ((l_3188 == (l_3190 = l_3189)) && ((void*)0 == l_3191))), l_3070[0])))), (&g_988[6] == (void*)0))), (*g_525))), 15)))) >= (**g_1483)) & (*g_653)) != 3UL) & (*g_525));
                        if (l_3030[3][1])
                            continue;
                    }
                }
            }
            if (((((((((**l_3189) = l_3168[2][3]) , (safe_mul_func_uint16_t_u_u((safe_div_func_int8_t_s_s(0xE4L, (safe_mul_func_int16_t_s_s((l_3015 < (l_3200 == (void*)0)), (safe_mod_func_uint32_t_u_u((*****g_3059), l_3203[2][0][3])))))), (safe_mul_func_uint16_t_u_u(((((((*g_1278) , ((*l_255) = ((*g_3113) = (((*l_41) = (l_3028 , (*g_525))) <= l_2997.f0)))) < l_3070[1]) < l_3070[1]) | l_3168[1][8]) >= (*g_1278)), 0x645FL))))) ^ (*g_525)) > (*g_525)) , l_3206) , (*g_2)) , l_3168[1][1]))
            { /* block id: 1295 */
                int32_t *l_3207 = (void*)0;
                int32_t *l_3208 = &l_3017;
                int32_t *l_3209 = (void*)0;
                int32_t *l_3210[10][2][6] = {{{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0},{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0}},{{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0},{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0}},{{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0},{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0}},{{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0},{(void*)0,&l_3027[2][0][5],(void*)0,(void*)0,&l_3027[2][0][5],(void*)0}},{{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]},{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]}},{{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]},{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]}},{{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]},{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]}},{{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]},{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]}},{{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]},{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]}},{{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]},{&l_3070[1],(void*)0,&l_3070[1],&l_3070[1],(void*)0,&l_3070[1]}}};
                uint32_t l_3211 = 9UL;
                int i, j, k;
                l_3211++;
                l_3214[0] = &l_3021;
                l_3070[1] &= (***g_190);
            }
            else
            { /* block id: 1299 */
                uint32_t ***l_3217 = &l_253;
                union U0 *l_3219[2];
                int32_t l_3226 = 0x1DE60BACL;
                union U0 *****l_3243 = &g_3058;
                int i;
                for (i = 0; i < 2; i++)
                    l_3219[i] = &g_3220;
                for (l_3098 = 0; (l_3098 <= 5); l_3098 += 1)
                { /* block id: 1302 */
                    uint8_t l_3215 = 0x26L;
                    float **l_3235 = &l_23;
                    int32_t l_3236 = 0x96487357L;
                    uint64_t l_3254 = 18446744073709551615UL;
                    union U0 ** const ***l_3271 = &g_3064;
                    if (l_2671.f0)
                        break;
                    if (l_3215)
                    { /* block id: 1304 */
                        int16_t l_3216 = 9L;
                        (*l_23) = (l_3216 != ((*l_3123) == l_3217));
                        return (*g_59);
                    }
                    else
                    { /* block id: 1307 */
                        union U0 *l_3218 = &g_2723;
                        const uint16_t *l_3224[8][7] = {{&g_487,&l_3031,&g_487,&l_3031,&g_487,&l_3031,&g_487},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_3031,&l_3031,&l_3031,&l_3031,&l_3031,&l_3031,&l_3031},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_487,&l_3031,&g_487,&l_3031,&g_487,&l_3031,&g_487},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_3031,&l_3031,&l_3031,&l_3031,&l_3031,&l_3031,&l_3031},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                        const uint16_t ** const l_3223 = &l_3224[5][2];
                        uint8_t l_3225 = 255UL;
                        int i, j;
                        l_3219[0] = l_3218;
                        l_3236 &= ((safe_div_func_int8_t_s_s(((l_3223 == (void*)0) <= (l_3215 , l_3225)), l_3225)) ^ (((*g_751) = ((*g_1991) = (l_3226 , (*g_1991)))) != ((safe_div_func_int16_t_s_s((safe_mod_func_uint32_t_u_u((safe_div_func_uint32_t_u_u(((((*l_3218) , (safe_div_func_uint32_t_u_u((l_3226 & 0UL), l_3215))) && l_3070[2]) , 1UL), l_3226)), (**g_2428))), l_3098)) , l_3235)));
                    }
                    for (g_702.f1 = 1; (g_702.f1 <= 5); g_702.f1 += 1)
                    { /* block id: 1315 */
                        int32_t l_3237 = (-2L);
                        const union U1 ***l_3251 = &g_2206;
                        const union U1 ****l_3252 = &l_3251;
                        int32_t l_3253 = 4L;
                        int i, j;
                        (**l_3235) = l_3237;
                        (*g_3255) = (((g_3241 = (g_3238[2][3][2] = &g_3058)) == l_3243) , (((l_3244[0] == &l_2981) != ((safe_div_func_float_f_f((l_3253 = (0x0.C1B154p-31 < (((safe_sub_func_float_f_f(((safe_mul_func_float_f_f(l_3226, ((*l_23) = ((l_3237 , (((*l_41) ^= (((*l_3252) = l_3251) == (void*)0)) == 18446744073709551615UL)) , l_3236)))) != 0x0.7p-1), l_3168[1][6])) >= 0x6.437237p-97) == l_3237))), l_3254)) <= 0x8.641642p+15)) == l_3254));
                        (*g_192) ^= ((safe_unary_minus_func_int8_t_s((safe_rshift_func_int16_t_s_u((l_3272[5][4][1] = (((safe_div_func_float_f_f((0x0.2p+1 <= (l_3261[9] , ((((safe_mul_func_uint16_t_u_u(0xCE9FL, (+((*g_525) = (safe_rshift_func_int8_t_s_u((safe_add_func_uint16_t_u_u(((l_3226 , ((l_3237 , ((l_3070[1] = (safe_div_func_uint64_t_u_u((((*g_1737) = 0xC0L) > ((0xDB22L >= (l_3253 || l_3261[9].f0)) & (*g_653))), 3UL))) , 3L)) , l_3271)) != (void*)0), 0x2EE3L)), 7)))))) , 0xE.D5F09Dp-88) > l_3226) >= l_3261[9].f0))), (***g_551))) , 0x5.60C620p-39) , l_3070[2])), 6)))) , 6L);
                        l_3273 = &g_496;
                    }
                }
            }
            for (l_3031 = 0; (l_3031 > 43); l_3031 = safe_add_func_uint16_t_u_u(l_3031, 5))
            { /* block id: 1335 */
                int64_t l_3286 = 6L;
                int32_t l_3295 = 0x7CB0CBCEL;
                int32_t l_3296 = 0xA9DB4317L;
                int32_t l_3304 = 0x90CE54C0L;
                int32_t l_3309 = 3L;
                int32_t l_3311 = 0L;
                int32_t l_3312[10] = {0x10FA8CACL,(-1L),0x4568959AL,(-1L),0x10FA8CACL,0x10FA8CACL,(-1L),0x4568959AL,(-1L),0x10FA8CACL};
                uint16_t l_3313 = 8UL;
                int i;
                for (l_2979 = 28; (l_2979 == 49); l_2979 = safe_add_func_int64_t_s_s(l_2979, 5))
                { /* block id: 1338 */
                    uint16_t l_3290 = 0xCA33L;
                    int32_t l_3301 = 0L;
                    int32_t l_3303 = (-10L);
                    int32_t l_3306 = 0xA4119BF7L;
                    int32_t l_3307 = (-9L);
                    int32_t l_3308 = 0x02B7E776L;
                    int32_t l_3310 = 0L;
                    for (l_3028 = 0; (l_3028 != 29); ++l_3028)
                    { /* block id: 1341 */
                        int8_t l_3289 = (-5L);
                        int32_t l_3294 = 0x6A38B522L;
                        int32_t l_3302 = (-1L);
                        int32_t l_3305[7][7] = {{0x4FCBD925L,1L,0xC3021C33L,0xC3021C33L,1L,0x4FCBD925L,(-10L)},{0L,(-2L),0x0D595303L,0x0D595303L,(-2L),0L,0xBF1A6544L},{0x4FCBD925L,1L,0xC3021C33L,0xC3021C33L,1L,0x4FCBD925L,(-10L)},{0L,(-2L),0x0D595303L,0x0D595303L,(-2L),0L,0xBF1A6544L},{0x4FCBD925L,1L,0xC3021C33L,0xC3021C33L,1L,0x4FCBD925L,(-10L)},{0L,(-2L),0x0D595303L,0x0D595303L,(-2L),0L,0xBF1A6544L},{0x4FCBD925L,1L,0xC3021C33L,0xC3021C33L,1L,0x4FCBD925L,(-10L)}};
                        int i, j;
                        l_3294 &= (safe_lshift_func_uint8_t_u_s((l_3282 != (((safe_mod_func_uint64_t_u_u(((l_3289 = (l_3286 < (safe_add_func_uint8_t_u_u(0xECL, 1L)))) == (l_3290 = ((****g_1592) , 5L))), 0x7F4817181A5387DFLL)) || ((safe_lshift_func_int16_t_s_u(l_3290, 6)) > (((*g_59) != (*g_525)) ^ l_3293))) , &g_2205)), 3));
                        l_3297++;
                        l_3313++;
                    }
                }
            }
        }
        (*g_222) = l_3316;
    }
    else
    { /* block id: 1352 */
        int32_t l_3318[6][1][1] = {{{(-6L)}},{{1L}},{{(-6L)}},{{1L}},{{(-6L)}},{{1L}}};
        int i, j, k;
        (**g_190) = (**g_190);
        return l_3318[1][0][0];
    }
    (**g_191) &= ((*g_135) != ((*l_3321) = l_3320));
    return (*g_59);
}


/* ------------------------------------------ */
/* 
 * reads : g_191 g_192 g_193
 * writes: g_193
 */
static int16_t  func_12(const uint32_t  p_13, float * p_14, int8_t  p_15, int64_t  p_16, float  p_17)
{ /* block id: 1215 */
    uint64_t l_2959 = 0x4A53ACAD0650B56ELL;
    (**g_191) |= 0x602EC428L;
    return l_2959;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const uint32_t  func_18(float * p_19, float * p_20, float  p_21, float * p_22)
{ /* block id: 1212 */
    const int8_t l_2956 = 0x30L;
    return l_2956;
}


/* ------------------------------------------ */
/* 
 * reads : g_78 g_1483 g_1175 g_227 g_722 g_1215.f2 g_702.f1 g_684 g_118 g_223 g_222 g_2933 g_1992 g_136 g_2942 g_2207 g_40.f3 g_250 g_458 g_233 g_552 g_553 g_7 g_6 g_192 g_193 g_957 g_729.f1 g_2 g_3 g_97 g_988 g_653 g_64 g_652 g_525 g_526 g_551 g_135 g_256 g_1025 g_336 g_729 g_487 g_716.f2 g_676 g_677 g_190 g_191 g_1104 g_1105 g_93 g_702.f0 g_729.f2 g_1181 g_406 g_716.f1 g_237 g_238 g_239 g_240 g_1215 g_1215.f3 g_1245 g_1277 g_1278 g_1007 g_1317 g_59 g_60 g_1328 g_716 g_2723.f2 g_1990 g_1991
 * writes: g_1007 g_223 g_723 g_1215.f2 g_702.f1 g_336 g_78 g_100 g_2933 g_568 g_729.f1 g_929 g_93 g_487 g_256 g_406.f0 g_730.f2 g_136 g_193 g_7 g_1099 g_192 g_97 g_1165 g_1193 g_64 g_250 g_2207
 */
static float * func_24(uint32_t  p_25, int8_t * p_26)
{ /* block id: 1087 */
    int16_t l_2695 = (-1L);
    union U1 l_2721 = {0xF7B0L};
    int64_t *l_2729 = &g_2357;
    int32_t **l_2734 = &g_2621;
    int32_t l_2755 = 1L;
    int32_t l_2767 = 0x858B2DC4L;
    float *l_2769 = &g_7;
    int32_t * const ***l_2773 = &g_497;
    uint8_t l_2787 = 0x2EL;
    float l_2791 = 0x9.F6A77Dp-57;
    int32_t l_2814 = 0xAE5BE2BBL;
    int32_t l_2815[2];
    uint8_t **** const *l_2819 = &g_2403;
    int8_t l_2869[1];
    int16_t l_2873 = 0xCBE8L;
    int32_t *l_2902 = &g_2207;
    int32_t l_2923 = 0x087C07C9L;
    int64_t l_2955 = 0xFC41E34934E3E1EFLL;
    int i;
    for (i = 0; i < 2; i++)
        l_2815[i] = 0xC81CC004L;
    for (i = 0; i < 1; i++)
        l_2869[i] = 0x20L;
    if ((safe_lshift_func_uint8_t_u_u(p_25, (p_25 , (safe_mul_func_int8_t_s_s(l_2695, (safe_mod_func_uint64_t_u_u((safe_add_func_uint16_t_u_u(l_2695, (((((safe_div_func_int16_t_s_s(0xA7E0L, (l_2695 , ((safe_div_func_int64_t_s_s((safe_mod_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u(((+((**g_1483) = (safe_div_func_uint16_t_u_u(l_2695, (safe_lshift_func_int8_t_s_s(((6UL < (safe_sub_func_uint16_t_u_u((safe_div_func_uint64_t_u_u((((~l_2695) >= 5L) != p_25), l_2695)), 0x671DL))) , (*p_26)), l_2695)))))) ^ p_25), p_25)), p_25)), p_25)) | p_25)))) & 0xD7L) > l_2695) <= p_25) | 0x24L))), 0x39180B725C738EDELL))))))))
    { /* block id: 1089 */
        float *l_2718 = &g_7;
        (*g_227) = (void*)0;
        return l_2718;
    }
    else
    { /* block id: 1092 */
        union U1 *l_2722 = &l_2721;
        int32_t l_2728[9] = {(-9L),(-9L),9L,(-9L),(-9L),9L,(-9L),(-9L),9L};
        int i;
        (*g_722) = &l_2728[8];
    }
    for (g_1215.f2 = 0; (g_1215.f2 <= 1); g_1215.f2 += 1)
    { /* block id: 1098 */
        uint16_t l_2735 = 65535UL;
        int32_t l_2736 = 0x86CA25D1L;
        int32_t l_2766 = 0x23826C4AL;
        uint16_t * const **l_2780 = (void*)0;
        int8_t *l_2781 = (void*)0;
        int8_t *l_2782 = &g_1759[1];
        int16_t *l_2790 = &g_2559.f2;
        uint16_t l_2792 = 0x0D09L;
        uint32_t *l_2793 = &g_988[4];
        int32_t **l_2799 = (void*)0;
        uint8_t *l_2810 = &g_250[0][0];
        uint16_t l_2816[4] = {0x71FDL,0x71FDL,0x71FDL,0x71FDL};
        uint8_t *****l_2823 = &g_2403;
        const union U1 l_2876[7][7] = {{{0x938FL},{0xE144L},{-1L},{-1L},{0xBE87L},{-1L},{0x4E7CL}},{{0L},{0x4E7CL},{-1L},{0xBE87L},{-1L},{-1L},{0xE144L}},{{0x938FL},{0x4E7CL},{0x004AL},{-1L},{-1L},{0x004AL},{0x4E7CL}},{{0x938FL},{0xE144L},{-1L},{-1L},{0xBE87L},{-1L},{0x4E7CL}},{{0L},{0x4E7CL},{-1L},{0xBE87L},{-1L},{-1L},{0xE144L}},{{0x938FL},{0x4E7CL},{0x004AL},{-1L},{-1L},{0x004AL},{0x4E7CL}},{{0x938FL},{0xE144L},{-1L},{-1L},{0xBE87L},{-1L},{0x4E7CL}}};
        int32_t l_2897[10][2][9] = {{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}},{{0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL,0xFDE5ABFAL},{0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L,0x5AFB9D58L}}};
        int i, j, k;
    }
    for (p_25 = 0; (p_25 > 19); p_25 = safe_add_func_uint64_t_u_u(p_25, 7))
    { /* block id: 1192 */
        uint64_t l_2913 = 0x75CDAAB82E2D7292LL;
        int32_t l_2921[9][8][3] = {{{0x7A496C53L,(-5L),0x1D3E159FL},{0x405606B7L,0xAFDE14D1L,0x012D58F7L},{1L,(-1L),(-1L)},{(-1L),1L,(-1L)},{(-6L),0xD9D96644L,0x012D58F7L},{1L,(-5L),0x1D3E159FL},{0xF7C2F50BL,1L,0x5BBA3BB0L},{0xCB47AD15L,(-1L),0x13E9488DL}},{{0xD9D96644L,(-10L),1L},{0xD8E037AAL,0xCF14E8B1L,0xD8E037AAL},{0x114BC287L,0xD8E037AAL,(-8L)},{1L,0x7A496C53L,1L},{(-1L),0xDD7E90C2L,1L},{(-1L),0x114BC287L,0xD9D96644L},{(-1L),0x0AC0DB4AL,0xA9D5B2BAL},{1L,0x42AB5169L,(-5L)}},{{0x114BC287L,8L,0x38664417L},{0xD8E037AAL,0L,2L},{0xD9D96644L,0L,(-1L)},{0xCB47AD15L,0xF7C2F50BL,(-1L)},{0xF7C2F50BL,0x1B57FCB2L,(-1L)},{1L,0x1D3E159FL,0x4A5ACC5BL},{(-6L),(-1L),(-1L)},{(-1L),(-1L),(-1L)}},{{1L,0x1D3E159FL,0x6AF7B2CAL},{0x405606B7L,0x1B57FCB2L,0L},{0x7A496C53L,0xF7C2F50BL,0xCC1F3B4CL},{0x5A320338L,0L,0x1B57FCB2L},{(-5L),0L,0x42AB5169L},{8L,8L,0xAFDE14D1L},{0x13E9488DL,0x42AB5169L,(-6L)},{0xDD7E90C2L,0x0AC0DB4AL,0x5A320338L}},{{2L,0x114BC287L,0x4DF21E82L},{1L,0xDD7E90C2L,0x5A320338L},{0x2E158350L,0x7A496C53L,(-6L)},{(-5L),0xD8E037AAL,0xAFDE14D1L},{0L,0xCF14E8B1L,0x42AB5169L},{0x5BBA3BB0L,(-10L),0x1B57FCB2L},{0x012D58F7L,(-1L),0xCC1F3B4CL},{0xAFDE14D1L,1L,0L}},{{0x4A5ACC5BL,(-5L),0x6AF7B2CAL},{0x89A44F1CL,0xD9D96644L,(-1L)},{0x0471B36EL,1L,(-1L)},{0x0471B36EL,(-1L),0x4A5ACC5BL},{0x89A44F1CL,0xAFDE14D1L,(-1L)},{0x4A5ACC5BL,(-5L),0x7F7A5DFAL},{(-1L),0xEBD70BC0L,0x7A496C53L},{0xF7C2F50BL,1L,0x4A5ACC5BL}},{{(-1L),(-6L),(-6L)},{(-5L),0x5BBA3BB0L,0x405606B7L},{0x405606B7L,(-1L),1L},{0xCF14E8B1L,(-1L),0x1D3E159FL},{(-1L),0xCB47AD15L,0x5A320338L},{0x4A5ACC5BL,(-1L),(-1L)},{0x1B57FCB2L,(-1L),(-1L)},{0L,0x5BBA3BB0L,(-8L)}},{{0x42AB5169L,(-6L),0x4DF21E82L},{1L,1L,0L},{(-1L),0xEBD70BC0L,(-1L)},{(-5L),0x405606B7L,0x13E9488DL},{(-1L),(-1L),0xF7C2F50BL},{0x89A44F1CL,0xAA5B6E71L,0xA9D5B2BAL},{7L,0x4DF21E82L,0xA9D5B2BAL},{0xCB47AD15L,0x1D3E159FL,0xF7C2F50BL}},{{0x5A320338L,1L,0x13E9488DL},{1L,0x2E158350L,(-1L)},{0x0AC0DB4AL,7L,0L},{0x1D3E159FL,0xD9D96644L,0x4DF21E82L},{(-8L),1L,(-8L)},{0x6AF7B2CAL,(-8L),(-1L)},{0x4DF21E82L,(-5L),(-1L)},{0x0471B36EL,0x1B57FCB2L,0x5A320338L}}};
        int i, j, k;
        for (g_702.f1 = 0; (g_702.f1 <= 2); g_702.f1 += 1)
        { /* block id: 1195 */
            int32_t *l_2905 = (void*)0;
            int32_t *l_2906 = &g_100[4];
            int32_t *l_2907 = (void*)0;
            int32_t *l_2908 = &l_2815[1];
            int32_t *l_2909 = (void*)0;
            int32_t *l_2910 = &l_2755;
            int32_t *l_2911 = (void*)0;
            int32_t *l_2912 = &l_2767;
            int32_t l_2928 = 0x8D33F200L;
            int32_t l_2929 = 0x0AD01FD1L;
            int32_t l_2930 = 0L;
            int32_t l_2931 = 0xEE19EEE0L;
            int32_t l_2932 = 0x5C871FD4L;
            int i;
            l_2913++;
            for (g_336 = 2; (g_336 >= 0); g_336 -= 1)
            { /* block id: 1199 */
                int8_t l_2918 = 0x46L;
                int32_t *l_2919 = &l_2815[1];
                int32_t **l_2920 = &l_2910;
                int32_t l_2922[2][5] = {{0xDEAFABE1L,0xDEAFABE1L,0xDEAFABE1L,0xDEAFABE1L,0xDEAFABE1L},{0x0BC33A65L,0x0BC33A65L,0x0BC33A65L,0x0BC33A65L,0x0BC33A65L}};
                int32_t *l_2924 = &l_2755;
                int32_t *l_2925 = &g_2207;
                int32_t *l_2926 = (void*)0;
                int32_t *l_2927[6][4] = {{(void*)0,(void*)0,&l_2814,&l_2767},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&l_2922[0][4]},{(void*)0,(void*)0,(void*)0,&l_2814},{(void*)0,&l_2922[0][4],&l_2814,&l_2814},{(void*)0,(void*)0,&l_2767,&l_2922[0][4]}};
                int i, j;
                (*g_223) = ((*g_684) & (safe_rshift_func_int8_t_s_s(l_2918, ((*p_26) = 1L))));
                (*l_2920) = ((*g_222) = l_2919);
                if ((*l_2919))
                    break;
                g_2933[1]++;
            }
        }
        return (*g_1992);
    }
    (*l_2902) = (safe_mul_func_uint8_t_u_u(((((safe_mul_func_uint8_t_u_u((func_44(((p_25 | (safe_div_func_int64_t_s_s(((g_2942 == &l_2721) == (*l_2902)), (safe_rshift_func_int8_t_s_s((*p_26), 2))))) < p_25)) , ((safe_div_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(255UL, (safe_div_func_uint64_t_u_u((safe_sub_func_uint64_t_u_u((safe_div_func_uint32_t_u_u(((func_44(p_25) , p_25) && (*l_2902)), 0xAD66E1CAL)), (*l_2902))), p_25)))), p_25)) >= g_2723.f2)), 0x84L)) , (*l_2902)) & 0x02DD1EA3L) == l_2955), 0x3CL));
    return (***g_1990);
}


/* ------------------------------------------ */
/* 
 * reads : g_222 g_100 g_1759 g_191 g_192 g_2543 g_2544 g_1025 g_525 g_552 g_553 g_7 g_256
 * writes: g_223 g_193 g_1759 g_526 g_97 g_100 g_256
 */
static int8_t * func_31(union U1  p_32, int8_t * p_33)
{ /* block id: 1078 */
    int32_t *l_2672 = &g_100[0];
    int8_t *l_2673 = &g_78;
    int64_t *l_2690 = &g_97;
    (*g_222) = l_2672;
    (**g_191) = ((p_32.f0 , (((((void*)0 == l_2673) , 0x4EL) && p_32.f0) ^ (*l_2672))) && (safe_add_func_uint16_t_u_u((((*l_2672) ^ (*p_33)) , 0xF7ECL), 0xD58DL)));
    (*l_2672) = ((((safe_mul_func_int8_t_s_s(((*p_33) &= (safe_add_func_uint8_t_u_u((*l_2672), (safe_lshift_func_uint16_t_u_u((**g_2543), 11))))), 255UL)) & p_32.f0) ^ (((((safe_lshift_func_int16_t_s_u((safe_div_func_uint8_t_u_u((1UL > 0xF7L), (safe_add_func_uint64_t_u_u(p_32.f0, ((*l_2690) = ((*g_525) = ((safe_rshift_func_int16_t_s_s((*l_2672), 8)) < p_32.f0))))))), (*l_2672))) , (void*)0) != l_2673) == (*l_2672)) , (*l_2672))) != 0xAC8F5E67L);
    (*l_2672) = (**g_552);
    return l_2673;
}


/* ------------------------------------------ */
/* 
 * reads : g_676 g_677 g_552 g_553 g_7 g_256 g_525 g_526 g_722 g_316
 * writes: g_1067 g_723
 */
static uint32_t  func_34(uint16_t  p_35, int64_t  p_36, int8_t * const  p_37, int8_t * p_38, uint32_t * p_39)
{ /* block id: 581 */
    int32_t l_1349 = 0x03B066CDL;
    int32_t l_1358 = 0x7F91C472L;
    float *l_1359 = &g_1067;
    uint8_t **l_1365 = (void*)0;
    int16_t **l_1366 = &g_1193[2][2];
    int32_t l_1378 = 0x71BC65A0L;
    int32_t l_1432[5];
    int64_t l_1447[3][5][6] = {{{9L,(-8L),1L,1L,0x2B58143CDAD3BAC2LL,6L},{(-1L),0xE3CB4B5420E9483ELL,6L,(-1L),0x7F09D9A16879EEDDLL,0xCB3BF636E03D8C16LL},{(-10L),0xE3CB4B5420E9483ELL,0xEB8D763C82F1554ELL,0xBF6D65334EA8B0D7LL,0x2B58143CDAD3BAC2LL,9L},{0L,(-8L),(-1L),0x04FFBB39CCD418E0LL,1L,0xC3B015D5FC8D992ALL},{6L,0L,(-10L),6L,0xF843C10813F3F05CLL,0x04FFBB39CCD418E0LL}},{{0xE3CB4B5420E9483ELL,1L,4L,0x934541935B187BDDLL,0x934541935B187BDDLL,4L},{4L,4L,0xECBC0951054A3CDBLL,0x7F09D9A16879EEDDLL,1L,0xF843C10813F3F05CLL},{6L,3L,(-1L),0xB248102CF6107A56LL,0xEB8D763C82F1554ELL,0xECBC0951054A3CDBLL},{(-1L),6L,(-1L),6L,4L,0xF843C10813F3F05CLL},{1L,6L,0xECBC0951054A3CDBLL,0x2B58143CDAD3BAC2LL,0xB515739D6BB3DFF4LL,4L}},{{0x2B58143CDAD3BAC2LL,0xB515739D6BB3DFF4LL,4L,0x93AA192DD1F9DB19LL,0xCB3BF636E03D8C16LL,0x04FFBB39CCD418E0LL},{(-8L),0xECBC0951054A3CDBLL,(-10L),0xC3B015D5FC8D992ALL,(-1L),0xC3B015D5FC8D992ALL},{(-1L),0x439F1193DED6E568LL,(-1L),6L,1L,9L},{0x93AA192DD1F9DB19LL,6L,(-1L),1L,0xC86D3DD22C185C0ALL,0x8631D5B0B31778ACLL},{0xE3CB4B5420E9483ELL,0xCB3BF636E03D8C16LL,(-10L),1L,0x439F1193DED6E568LL,0xB515739D6BB3DFF4LL}}};
    int32_t *l_1497[1][7][1];
    union U1 l_1539 = {0x70EAL};
    float ** const *l_1543[9][4] = {{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135},{&g_135,&g_135,&g_135,&g_135}};
    int32_t ***l_1546 = (void*)0;
    int32_t ***l_1548 = &g_227;
    uint64_t * const ****l_1635 = (void*)0;
    uint32_t l_1661 = 0x9E981D04L;
    union U0 * const *l_1710 = &g_1099[6];
    union U0 * const **l_1709 = &l_1710;
    uint16_t l_1763 = 65535UL;
    const uint16_t *l_1781 = &g_1007;
    const uint16_t **l_1780 = &l_1781;
    const uint16_t ***l_1779 = &l_1780;
    const uint16_t ****l_1778 = &l_1779;
    uint8_t ***l_1889 = (void*)0;
    uint8_t *** const *l_1888 = &l_1889;
    int64_t l_1983 = 1L;
    uint64_t l_2061[9][3][1] = {{{0x2321210CBEDFA90ELL},{1UL},{18446744073709551615UL}},{{0x8F7CF8FB6F85A06CLL},{0x8F7CF8FB6F85A06CLL},{18446744073709551615UL}},{{1UL},{0x2321210CBEDFA90ELL},{1UL}},{{18446744073709551615UL},{0x8F7CF8FB6F85A06CLL},{0x8F7CF8FB6F85A06CLL}},{{18446744073709551615UL},{1UL},{0x2321210CBEDFA90ELL}},{{1UL},{18446744073709551615UL},{0x8F7CF8FB6F85A06CLL}},{{0x8F7CF8FB6F85A06CLL},{18446744073709551615UL},{1UL}},{{0x2321210CBEDFA90ELL},{1UL},{18446744073709551615UL}},{{0x8F7CF8FB6F85A06CLL},{0x8F7CF8FB6F85A06CLL},{18446744073709551615UL}}};
    float l_2076[6] = {0x8.C1B86Bp-74,0x8.C1B86Bp-74,0x8.C1B86Bp-74,0x8.C1B86Bp-74,0x8.C1B86Bp-74,0x8.C1B86Bp-74};
    uint64_t l_2114 = 0xE0867DB8501E2E48LL;
    uint16_t l_2141 = 0x7ED1L;
    int64_t l_2143 = 0x3C8202B1803394B1LL;
    int8_t l_2144 = 2L;
    int32_t l_2155 = 7L;
    uint64_t *****l_2166[10][1][10] = {{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}},{{(void*)0,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,&g_2164,(void*)0,&g_2164}}};
    uint32_t l_2167[8];
    const uint64_t l_2186 = 0x1196864273B3ADBELL;
    int16_t l_2194 = (-9L);
    float l_2241 = 0x7.3EAB09p+22;
    union U1 *l_2309 = (void*)0;
    const int32_t **l_2322 = &g_723[0];
    const int32_t ***l_2321 = &l_2322;
    uint32_t **l_2433 = (void*)0;
    float ****l_2521 = &g_751;
    float *****l_2520 = &l_2521;
    uint16_t **l_2534[8];
    uint32_t l_2586[4][8] = {{0x050E3CBDL,0x5394916FL,0xE6D90466L,4294967292UL,0xE6D90466L,0x5394916FL,0x050E3CBDL,0x5394916FL},{0x050E3CBDL,4294967292UL,0x050E3CBDL,0x5394916FL,0xE6D90466L,4294967292UL,0xE6D90466L,0x5394916FL},{0xEC3F1204L,0x5394916FL,0xEC3F1204L,0xC590C1ACL,0xE6D90466L,0xC590C1ACL,0xEC3F1204L,0x5394916FL},{0xE6D90466L,0xC590C1ACL,0xEC3F1204L,0x5394916FL,0xEC3F1204L,0xC590C1ACL,0xE6D90466L,0xC590C1ACL}};
    float l_2587[10][8][1] = {{{0x9.897AAFp+59},{0xF.C0F599p-16},{0x9.897AAFp+59},{0x2.04F4B9p-72},{0xE.E14066p+6},{0x6.62BABAp+29},{0xB.9D8365p-47},{0x6.62BABAp+29}},{{0xE.E14066p+6},{0x2.04F4B9p-72},{0x9.897AAFp+59},{0xF.C0F599p-16},{0x9.897AAFp+59},{0x2.04F4B9p-72},{0xE.E14066p+6},{0x6.62BABAp+29}},{{0xB.9D8365p-47},{0x6.62BABAp+29},{0xE.E14066p+6},{0x2.04F4B9p-72},{0x9.897AAFp+59},{0xF.C0F599p-16},{0x9.897AAFp+59},{0x2.04F4B9p-72}},{{0xE.E14066p+6},{0x6.62BABAp+29},{0xB.9D8365p-47},{0x6.62BABAp+29},{0xE.E14066p+6},{0x2.04F4B9p-72},{0x9.897AAFp+59},{0xF.C0F599p-16}},{{0x9.897AAFp+59},{0x2.04F4B9p-72},{0xE.E14066p+6},{0x6.62BABAp+29},{0xB.9D8365p-47},{0x6.62BABAp+29},{0xE.E14066p+6},{0x2.04F4B9p-72}},{{0x9.897AAFp+59},{0xF.C0F599p-16},{0x9.897AAFp+59},{0x2.04F4B9p-72},{0xE.E14066p+6},{0x6.62BABAp+29},{0xB.9D8365p-47},{0x6.62BABAp+29}},{{0xE.E14066p+6},{0x2.04F4B9p-72},{0x9.897AAFp+59},{0xF.C0F599p-16},{0x9.897AAFp+59},{0x2.04F4B9p-72},{0xE.E14066p+6},{0x6.62BABAp+29}},{{0xB.9D8365p-47},{0x6.62BABAp+29},{0xE.E14066p+6},{0x2.04F4B9p-72},{0x9.897AAFp+59},{0xF.C0F599p-16},{0x9.897AAFp+59},{0x2.04F4B9p-72}},{{0xE.E14066p+6},{0x6.62BABAp+29},{0xB.9D8365p-47},{0x6.62BABAp+29},{0xE.E14066p+6},{0x2.04F4B9p-72},{0x9.897AAFp+59},{0xF.C0F599p-16}},{{0x9.897AAFp+59},{0x2.04F4B9p-72},{0xE.E14066p+6},{0x6.62BABAp+29},{0xB.9D8365p-47},{0x6.62BABAp+29},{0xE.E14066p+6},{0x2.04F4B9p-72}}};
    int32_t l_2599[6][7] = {{8L,(-4L),8L,0x798981F9L,8L,(-4L),8L},{(-2L),0L,(-1L),0xE1B92351L,0xE1B92351L,(-1L),0L},{0xEFAD4589L,(-4L),(-4L),(-4L),0xEFAD4589L,(-4L),(-4L)},{0xE1B92351L,0xE1B92351L,(-1L),0L,(-2L),(-2L),0L},{8L,0x798981F9L,8L,(-4L),8L,0x798981F9L,8L},{0xE1B92351L,0L,0L,0xE1B92351L,(-2L),(-1L),(-1L)}};
    union U1 **l_2602[2][1];
    int32_t l_2610[7][3][8] = {{{(-4L),0x1BA02539L,(-4L),0x026F8550L,0x1BA02539L,0x00DEEDDAL,0x00DEEDDAL,0x1BA02539L},{0x1BA02539L,0x00DEEDDAL,0x00DEEDDAL,0x1BA02539L,0x026F8550L,(-4L),0x1BA02539L,(-4L)},{0x1BA02539L,0x9BF1F069L,0x3190B131L,0x9BF1F069L,0x1BA02539L,0x3190B131L,0xFFF2979FL,0xFFF2979FL}},{{(-4L),0x9BF1F069L,0x026F8550L,0x026F8550L,0x9BF1F069L,(-4L),0x00DEEDDAL,0x9BF1F069L},{0xFFF2979FL,0x00DEEDDAL,0x026F8550L,0xFFF2979FL,0x026F8550L,0x00DEEDDAL,0xFFF2979FL,(-4L)},{0x9BF1F069L,0x1BA02539L,0x3190B131L,0xFFF2979FL,0xFFF2979FL,0x3190B131L,0x00DEEDDAL,0x026F8550L}},{{0xFFB88F5CL,(-4L),0x3190B131L,0x1A443C4DL,(-4L),0x1A443C4DL,0x3190B131L,(-4L)},{0x026F8550L,0x3190B131L,0xFFB88F5CL,0x026F8550L,0x1A443C4DL,0x1A443C4DL,0x026F8550L,0xFFB88F5CL},{(-4L),(-4L),0xFFF2979FL,0x00DEEDDAL,0x026F8550L,0xFFF2979FL,0x026F8550L,0x00DEEDDAL}},{{0xFFB88F5CL,0x00DEEDDAL,0xFFB88F5CL,0x1A443C4DL,0x00DEEDDAL,0x3190B131L,0x3190B131L,0x00DEEDDAL},{0x00DEEDDAL,0x3190B131L,0x3190B131L,0x00DEEDDAL,0x1A443C4DL,0xFFB88F5CL,0x00DEEDDAL,0xFFB88F5CL},{0x00DEEDDAL,0x026F8550L,0xFFF2979FL,0x026F8550L,0x00DEEDDAL,0xFFF2979FL,(-4L),(-4L)}},{{0xFFB88F5CL,0x026F8550L,0x1A443C4DL,0x1A443C4DL,0x026F8550L,0xFFB88F5CL,0x3190B131L,0x026F8550L},{(-4L),0x3190B131L,0x1A443C4DL,(-4L),0x1A443C4DL,0x3190B131L,(-4L),0xFFB88F5CL},{0x026F8550L,0x00DEEDDAL,0xFFF2979FL,(-4L),(-4L),0xFFF2979FL,0x00DEEDDAL,0x026F8550L}},{{0xFFB88F5CL,(-4L),0x3190B131L,0x1A443C4DL,(-4L),0x1A443C4DL,0x3190B131L,(-4L)},{0x026F8550L,0x3190B131L,0xFFB88F5CL,0x026F8550L,0x1A443C4DL,0x1A443C4DL,0x026F8550L,0xFFB88F5CL},{(-4L),(-4L),0xFFF2979FL,0x00DEEDDAL,0x026F8550L,0xFFF2979FL,0x026F8550L,0x00DEEDDAL}},{{0xFFB88F5CL,0x00DEEDDAL,0xFFB88F5CL,0x1A443C4DL,0x00DEEDDAL,0x3190B131L,0x3190B131L,0x00DEEDDAL},{0x00DEEDDAL,0x3190B131L,0x3190B131L,0x00DEEDDAL,0x1A443C4DL,0xFFB88F5CL,0x00DEEDDAL,0xFFB88F5CL},{0x00DEEDDAL,0x026F8550L,0xFFF2979FL,0x026F8550L,0x00DEEDDAL,0xFFF2979FL,(-4L),(-4L)}}};
    float l_2636 = 0x6.9p-1;
    uint32_t l_2664 = 4294967290UL;
    int32_t *l_2670 = &g_100[0];
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_1432[i] = 6L;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 1; k++)
                l_1497[i][j][k] = &g_100[0];
        }
    }
    for (i = 0; i < 8; i++)
        l_2167[i] = 18446744073709551615UL;
    for (i = 0; i < 8; i++)
        l_2534[i] = &g_1175[0];
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_2602[i][j] = &g_929[0];
    }
    if ((((((safe_div_func_int16_t_s_s((safe_sub_func_int16_t_s_s(((!0x60129A5091839678LL) & l_1349), ((((*g_676) , l_1349) == (p_35 >= ((((0xC.12B1CCp-64 != (safe_div_func_float_f_f((safe_div_func_float_f_f(l_1349, ((*l_1359) = (((safe_sub_func_float_f_f((safe_div_func_float_f_f(((l_1349 == 0x4.CDD580p-99) != p_35), p_36)), (**g_552))) != l_1358) > l_1358)))), l_1358))) >= l_1349) , 1L) , g_256[2][3]))) >= l_1358))), 65535UL)) < (*g_525)) >= l_1349) ^ p_35) == 0x1CB285CAL))
    { /* block id: 583 */
        int32_t l_1371 = 6L;
        float l_1379 = (-0x7.Dp+1);
        int64_t *l_1381 = (void*)0;
        const uint32_t ***l_1394 = (void*)0;
        int32_t l_1401[7] = {0xA2426B7CL,0x495C6595L,0xA2426B7CL,0xA2426B7CL,0x495C6595L,0xA2426B7CL,0xA2426B7CL};
        uint64_t **l_1420[1];
        int32_t *l_1438 = &l_1401[3];
        uint16_t * const *l_1468[10][4] = {{&g_1175[2],&g_1175[2],&g_1175[0],&g_1175[2]},{&g_1175[0],&g_1175[3],&g_1175[2],&g_1175[3]},{&g_1175[0],(void*)0,&g_1175[0],&g_1175[2]},{&g_1175[2],(void*)0,&g_1175[0],&g_1175[3]},{&g_1175[2],&g_1175[2],&g_1175[3],&g_1175[2]},{&g_1175[2],&g_1175[1],&g_1175[0],&g_1175[0]},{&g_1175[2],&g_1175[2],&g_1175[0],&g_1175[0]},{&g_1175[0],&g_1175[0],&g_1175[2],(void*)0},{&g_1175[0],&g_1175[0],&g_1175[0],&g_1175[2]},{&g_1175[2],&g_1175[0],&g_1175[2],(void*)0}};
        union U1 l_1507 = {0xB678L};
        float ** const **l_1544 = &l_1543[7][3];
        const int16_t l_1545 = 0x09CDL;
        int32_t ****l_1547 = &g_226[0];
        int i, j;
        for (i = 0; i < 1; i++)
            l_1420[i] = &g_653;
    }
    else
    { /* block id: 639 */
        const uint16_t l_1552 = 0x73E2L;
        uint32_t l_1579 = 18446744073709551615UL;
        uint32_t *l_1580 = &g_568;
        int16_t l_1581 = 0xA236L;
        int32_t l_1629[3][1];
        int32_t *l_1663 = &g_118;
        union U1 l_1683 = {0x9AE4L};
        uint8_t ***l_1685 = (void*)0;
        union U0 *l_1716 = &g_1245[0];
        int32_t ***l_1728 = (void*)0;
        int32_t l_1738 = 1L;
        int8_t *l_1753 = &g_256[2][3];
        const uint16_t ****l_1944 = (void*)0;
        int8_t l_1970[4][3][7] = {{{0x29L,9L,(-1L),0L,9L,9L,0L},{(-1L),1L,(-1L),(-4L),0x06L,(-4L),(-1L)},{0x29L,0L,0xFDL,0x29L,0x29L,0xFDL,0L}},{{0x26L,8L,(-10L),(-4L),(-10L),8L,0L},{(-7L),9L,0xFDL,0xFDL,9L,(-7L),0xFDL},{(-1L),(-4L),0x06L,(-4L),(-1L),1L,(-1L)}},{{9L,0xFDL,0xFDL,9L,(-7L),0xFDL,(-1L)},{(-10L),(-4L),(-10L),8L,0L,8L,(-10L)},{9L,9L,0L,(-1L),9L,0x29L,(-1L)}},{{(-1L),8L,1L,(-4L),1L,8L,(-1L)},{(-7L),(-1L),0xFDL,(-7L),9L,0xFDL,0xFDL},{0L,(-4L),0x26L,(-4L),0L,1L,0L}}};
        int32_t l_1974 = 0x2C931E2CL;
        int16_t **l_2090 = &g_1193[2][2];
        int32_t l_2195 = (-4L);
        int16_t l_2213[7];
        uint16_t * const *** const *l_2250 = (void*)0;
        int16_t l_2328[5][7] = {{0x711FL,0xE749L,0L,1L,0xE749L,0xC6D5L,0x101DL},{0x22DEL,0x711FL,0xC6D5L,0x22DEL,0x101DL,0x22DEL,0xC6D5L},{0x6F9AL,0x6F9AL,1L,1L,0x711FL,0xEB39L,0x6F9AL},{0x6F9AL,0xC6D5L,0L,0x711FL,0x6DBFL,0x6DBFL,0x711FL},{0x22DEL,0x101DL,0x22DEL,0xC6D5L,0x711FL,0x22DEL,0xE749L}};
        uint32_t l_2331 = 0x800AF2D9L;
        int32_t l_2356 = 5L;
        uint32_t l_2358 = 0xB37BD88DL;
        uint64_t l_2445 = 3UL;
        uint16_t **l_2535[8][6][5] = {{{&g_1175[3],(void*)0,&g_1175[2],&g_1175[1],&g_1175[3]},{(void*)0,&g_1175[0],&g_1175[2],&g_1175[0],&g_1175[0]},{&g_1175[2],&g_1175[0],&g_1175[0],&g_1175[0],&g_1175[3]},{&g_1175[3],&g_1175[3],&g_1175[0],(void*)0,&g_1175[0]},{(void*)0,&g_1175[0],&g_1175[0],&g_1175[0],&g_1175[3]},{&g_1175[0],&g_1175[3],&g_1175[0],&g_1175[0],(void*)0}},{{&g_1175[0],&g_1175[0],&g_1175[0],&g_1175[2],&g_1175[0]},{&g_1175[0],&g_1175[2],&g_1175[2],&g_1175[0],(void*)0},{&g_1175[0],(void*)0,&g_1175[3],&g_1175[2],(void*)0},{(void*)0,&g_1175[3],&g_1175[0],&g_1175[0],(void*)0},{&g_1175[3],(void*)0,&g_1175[1],&g_1175[3],&g_1175[0]},{&g_1175[2],&g_1175[0],&g_1175[3],&g_1175[0],&g_1175[3]}},{{(void*)0,&g_1175[0],&g_1175[0],&g_1175[0],&g_1175[0]},{&g_1175[3],&g_1175[3],&g_1175[0],(void*)0,&g_1175[0]},{&g_1175[0],&g_1175[1],(void*)0,&g_1175[1],&g_1175[0]},{(void*)0,(void*)0,&g_1175[0],&g_1175[0],&g_1175[1]},{(void*)0,&g_1175[0],(void*)0,&g_1175[0],&g_1175[2]},{&g_1175[0],&g_1175[0],&g_1175[0],(void*)0,&g_1175[1]}},{{&g_1175[3],&g_1175[0],&g_1175[0],&g_1175[0],&g_1175[0]},{&g_1175[0],&g_1175[2],&g_1175[0],(void*)0,&g_1175[3]},{&g_1175[2],(void*)0,(void*)0,&g_1175[0],&g_1175[0]},{&g_1175[0],&g_1175[0],&g_1175[0],(void*)0,(void*)0},{&g_1175[0],&g_1175[0],&g_1175[1],&g_1175[3],&g_1175[0]},{&g_1175[0],&g_1175[1],(void*)0,(void*)0,&g_1175[0]}},{{&g_1175[0],&g_1175[3],&g_1175[0],(void*)0,&g_1175[0]},{&g_1175[2],&g_1175[0],(void*)0,&g_1175[0],&g_1175[1]},{&g_1175[2],&g_1175[3],&g_1175[3],&g_1175[1],(void*)0},{&g_1175[1],&g_1175[1],(void*)0,&g_1175[1],&g_1175[1]},{&g_1175[0],&g_1175[1],&g_1175[0],&g_1175[0],&g_1175[0]},{&g_1175[0],&g_1175[0],&g_1175[3],(void*)0,&g_1175[0]}},{{&g_1175[0],&g_1175[1],&g_1175[2],(void*)0,(void*)0},{&g_1175[2],&g_1175[2],&g_1175[3],&g_1175[3],&g_1175[2]},{(void*)0,(void*)0,&g_1175[3],(void*)0,&g_1175[0]},{(void*)0,&g_1175[2],&g_1175[0],&g_1175[0],(void*)0},{&g_1175[2],&g_1175[0],&g_1175[3],(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1175[1],&g_1175[0]}},{{&g_1175[3],&g_1175[0],&g_1175[0],&g_1175[0],(void*)0},{&g_1175[1],&g_1175[0],&g_1175[0],&g_1175[3],(void*)0},{&g_1175[0],&g_1175[0],&g_1175[0],(void*)0,&g_1175[0]},{&g_1175[0],&g_1175[0],(void*)0,&g_1175[0],&g_1175[0]},{&g_1175[0],&g_1175[0],&g_1175[3],&g_1175[0],&g_1175[0]},{&g_1175[1],&g_1175[3],&g_1175[0],&g_1175[0],&g_1175[0]}},{{&g_1175[2],&g_1175[3],&g_1175[3],&g_1175[1],(void*)0},{&g_1175[3],&g_1175[0],&g_1175[3],(void*)0,&g_1175[0]},{(void*)0,&g_1175[0],&g_1175[2],&g_1175[1],&g_1175[0]},{(void*)0,(void*)0,&g_1175[3],&g_1175[0],&g_1175[0]},{&g_1175[0],(void*)0,&g_1175[0],&g_1175[0],(void*)0},{&g_1175[0],&g_1175[0],(void*)0,&g_1175[0],&g_1175[2]}}};
        uint64_t l_2560 = 1UL;
        union U0 ****l_2561 = &g_1711;
        int64_t l_2584 = 0L;
        float ***l_2585[9][9] = {{(void*)0,&g_1992,&g_135,&g_1992,(void*)0,&g_1992,&g_135,&g_1992,(void*)0},{(void*)0,&g_135,&g_135,&g_135,&g_1992,&g_1992,(void*)0,&g_1992,&g_1992},{&g_1992,&g_1992,&g_1992,&g_1992,&g_1992,(void*)0,&g_135,(void*)0,(void*)0},{(void*)0,&g_1992,(void*)0,(void*)0,(void*)0,(void*)0,&g_1992,(void*)0,&g_1992},{(void*)0,&g_135,(void*)0,(void*)0,&g_1992,(void*)0,(void*)0,&g_1992,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1992,(void*)0,(void*)0,&g_135,&g_1992,&g_1992},{&g_1992,(void*)0,&g_135,&g_1992,&g_135,(void*)0,&g_1992,&g_135,(void*)0},{&g_135,(void*)0,(void*)0,&g_1992,(void*)0,(void*)0,(void*)0,&g_1992,(void*)0},{&g_1992,&g_1992,(void*)0,(void*)0,(void*)0,&g_1992,(void*)0,&g_135,(void*)0}};
        int32_t **l_2622[8] = {&g_2621,&g_2621,&g_2621,&g_2621,&g_2621,&g_2621,&g_2621,&g_2621};
        const uint16_t *****l_2667 = &l_1778;
        int i, j, k;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 1; j++)
                l_1629[i][j] = 0x68B24717L;
        }
        for (i = 0; i < 7; i++)
            l_2213[i] = 0x207EL;
    }
    (*g_722) = &l_1378;
    for (l_1378 = 0; (l_1378 == 15); l_1378 = safe_add_func_int32_t_s_s(l_1378, 8))
    { /* block id: 1073 */
        (**l_2321) = (void*)0;
    }
    (**l_2321) = l_2670;
    return (*p_39);
}


/* ------------------------------------------ */
/* 
 * reads : g_40.f3 g_250 g_458 g_702.f1 g_233 g_552 g_553 g_7 g_6 g_192 g_193 g_957 g_729.f1 g_2 g_3 g_97 g_988 g_653 g_64 g_652 g_525 g_526 g_551 g_135 g_136 g_256 g_1025 g_336 g_729 g_487 g_222 g_716.f2 g_676 g_677 g_190 g_191 g_227 g_1104 g_1105 g_1007 g_93 g_684 g_118 g_223 g_702.f0 g_729.f2 g_1181 g_406 g_716.f1 g_237 g_238 g_239 g_240 g_1215 g_1215.f3 g_1245 g_1277 g_1278 g_1317 g_59 g_60 g_1328 g_716 g_568
 * writes: g_568 g_729.f1 g_929 g_702.f1 g_93 g_487 g_256 g_406.f0 g_730.f2 g_1007 g_136 g_193 g_7 g_223 g_336 g_1099 g_192 g_97 g_1165 g_1193 g_64 g_250 g_723 g_100
 */
static union U0  func_44(int16_t  p_45)
{ /* block id: 271 */
    uint8_t *l_692 = (void*)0;
    uint8_t **l_691 = &l_692;
    int32_t l_694 = 0xDC0F8F2BL;
    uint8_t l_777[9] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    float l_818 = (-0x1.1p-1);
    int32_t ****l_824 = &g_226[3];
    int32_t **** const *l_823 = &l_824;
    int32_t *l_855 = (void*)0;
    union U1 *l_939 = &g_406;
    union U1 ** const l_938 = &l_939;
    int16_t l_940[1];
    int64_t *l_949 = &g_526[0];
    uint16_t *l_954 = &g_487;
    int32_t l_965 = 0L;
    uint8_t l_1022 = 0x7CL;
    int32_t l_1073 = 0x1BD21330L;
    int64_t *l_1081 = &g_97;
    union U0 *l_1110 = &g_716[2];
    uint64_t **l_1133 = &g_653;
    uint64_t l_1200 = 0xBAB1362BB3467E6ALL;
    uint32_t *l_1211 = &g_568;
    uint32_t **l_1210 = &l_1211;
    int8_t *l_1339[9][4][7] = {{{&g_256[2][3],&g_256[2][3],&g_78,(void*)0,&g_256[2][3],&g_78,&g_256[3][0]},{&g_256[3][3],&g_256[2][0],&g_256[4][6],&g_256[4][6],&g_256[2][0],&g_256[3][3],&g_256[2][0]},{&g_256[2][3],&g_78,&g_256[2][3],&g_256[4][5],&g_256[2][3],&g_256[2][3],&g_256[4][5]},{&g_78,&g_78,&g_78,&g_256[2][3],&g_256[2][0],&g_256[2][3],&g_256[3][3]}},{{&g_256[2][3],&g_78,&g_256[2][3],&g_78,&g_256[2][3],&g_78,&g_256[2][3]},{&g_256[4][6],&g_256[2][0],&g_256[3][3],&g_256[2][0],&g_78,&g_256[2][3],&g_78},{&g_78,&g_256[2][3],&g_256[2][3],&g_78,(void*)0,&g_256[2][3],&g_78},{&g_256[1][4],(void*)0,&g_256[3][3],&g_78,&g_78,&g_256[3][3],(void*)0}},{{(void*)0,&g_256[2][3],&g_256[2][3],&g_78,&g_256[2][3],&g_78,&g_78},{&g_256[2][3],&g_256[1][4],&g_78,&g_256[1][4],&g_256[2][3],&g_256[2][0],&g_78},{&g_256[3][0],&g_256[2][3],&g_256[2][3],&g_78,&g_256[4][5],&g_78,&g_256[2][3]},{&g_78,&g_78,&g_256[4][6],&g_78,(void*)0,&g_256[2][3],&g_256[3][3]}},{{&g_256[3][0],&g_78,&g_78,&g_78,&g_78,&g_256[3][0],&g_256[4][5]},{&g_256[2][3],&g_256[4][6],&g_256[1][4],&g_256[2][0],(void*)0,(void*)0,&g_256[2][0]},{(void*)0,&g_78,(void*)0,&g_78,&g_256[4][5],&g_256[2][3],&g_256[3][0]},{&g_256[1][4],&g_256[4][6],&g_256[2][3],&g_256[2][3],&g_256[2][3],&g_256[4][6],&g_256[1][4]}},{{&g_78,&g_78,&g_256[3][0],&g_256[4][5],&g_256[2][3],&g_256[2][3],&g_256[2][3]},{&g_256[4][6],&g_78,&g_78,&g_256[4][6],&g_78,(void*)0,&g_256[2][3]},{&g_256[2][3],&g_256[2][3],&g_256[3][0],(void*)0,(void*)0,&g_256[3][0],&g_256[2][3]},{&g_78,&g_256[1][4],&g_256[2][3],&g_256[2][0],&g_256[2][3],&g_256[1][4],&g_256[1][4]}},{{&g_256[3][0],&g_78,&g_78,&g_78,&g_256[3][0],&g_78,&g_78},{&g_78,&g_256[2][3],&g_78,&g_256[4][6],(void*)0,&g_256[4][6],&g_78},{&g_78,&g_78,&g_256[2][3],&g_78,&g_256[2][3],&g_256[2][3],(void*)0},{&g_78,&g_256[4][6],&g_78,&g_78,&g_256[4][6],&g_78,(void*)0}},{{&g_256[3][0],&g_256[2][3],&g_78,&g_256[2][3],&g_256[2][3],&g_256[2][3],&g_256[2][3]},{&g_256[2][0],&g_256[2][0],&g_256[2][0],&g_256[1][4],(void*)0,&g_256[3][3],&g_78},{&g_78,&g_256[2][3],&g_256[3][0],&g_256[2][3],&g_256[3][0],&g_256[2][3],&g_78},{&g_78,&g_256[4][6],&g_78,(void*)0,&g_256[2][3],&g_256[3][3],&g_256[2][3]}},{{&g_256[2][3],&g_78,&g_78,&g_256[2][3],&g_78,&g_256[2][3],&g_256[2][3]},{&g_78,&g_256[2][3],&g_78,&g_256[2][0],&g_256[2][0],&g_78,&g_256[2][3]},{&g_78,&g_78,&g_256[3][0],&g_78,&g_78,&g_256[2][3],&g_256[2][3]},{&g_256[3][3],&g_78,&g_256[2][0],&g_78,&g_256[3][3],&g_256[4][6],&g_256[2][3]}},{{(void*)0,&g_256[2][3],&g_78,&g_78,&g_256[2][3],&g_78,&g_78},{&g_256[2][3],&g_256[2][3],&g_78,&g_256[2][0],&g_256[2][3],&g_256[1][4],&g_78},{(void*)0,&g_78,&g_256[2][3],&g_256[2][3],&g_78,(void*)0,&g_256[2][3]},{&g_256[3][3],&g_78,&g_78,(void*)0,&g_256[2][3],&g_256[2][3],(void*)0}}};
    uint64_t l_1340[7][3] = {{0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL},{5UL,5UL,5UL},{0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL},{5UL,5UL,5UL},{0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL},{5UL,5UL,5UL},{0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL,0xA835BD0C1CB6F23DLL}};
    int32_t *l_1341 = &g_100[0];
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_940[i] = 0x11E5L;
lbl_958:
    for (g_568 = 0; (g_568 == 46); g_568 = safe_add_func_uint64_t_u_u(g_568, 5))
    { /* block id: 274 */
        uint8_t l_690 = 1UL;
        int32_t l_706 = 0xDF04DC4CL;
        union U1 l_737 = {0x6615L};
        uint16_t l_748 = 65535UL;
        float ***l_752 = &g_135;
        int8_t l_808 = 1L;
        int32_t l_810 = (-10L);
        int32_t *l_879 = &g_100[2];
        int64_t *l_910 = &g_97;
    }
    for (g_729.f1 = 20; (g_729.f1 != 21); g_729.f1++)
    { /* block id: 401 */
        int8_t l_922[6][8] = {{0xF2L,(-1L),(-1L),0xF2L,0x6CL,(-1L),0x67L,(-4L)},{1L,0x07L,(-1L),0x2AL,(-1L),0xD2L,(-4L),0xD2L},{3L,0x07L,0x67L,0x07L,3L,(-1L),0xF2L,6L},{(-1L),(-1L),3L,0x8FL,0x58L,0x6CL,0x07L,0x07L},{(-4L),(-7L),3L,3L,(-7L),(-4L),0xF2L,0x58L},{0x58L,0xD2L,0x67L,6L,0x07L,0x8FL,(-4L),(-1L)}};
        union U1 *l_927 = &g_406;
        union U1 **l_928 = (void*)0;
        uint32_t *l_934 = &g_702.f1;
        uint32_t ** const l_933 = &l_934;
        uint32_t ** const *l_932 = &l_933;
        int32_t l_941 = 0x7B916D71L;
        const uint32_t **l_950 = (void*)0;
        const uint32_t *l_952 = &g_953;
        const uint32_t **l_951 = &l_952;
        uint16_t **l_955[4][2];
        int32_t l_956 = (-1L);
        int32_t l_961 = 5L;
        uint32_t l_967 = 18446744073709551615UL;
        int32_t *l_1009 = (void*)0;
        uint8_t *l_1057[8][3] = {{&l_1022,&l_1022,&l_777[8]},{&g_250[0][0],&l_777[4],&l_777[4]},{&l_777[8],&l_1022,&l_1022},{&l_1022,&g_250[0][0],&l_1022},{&l_777[8],&l_777[4],&l_1022},{&l_777[4],&l_777[4],&g_250[0][0]},{&l_777[8],&l_777[4],&l_777[4]},{&g_250[0][0],&g_250[0][0],&g_250[0][0]}};
        int32_t l_1062 = 0x9C752807L;
        int32_t *l_1120[5];
        uint64_t **l_1132 = &g_653;
        uint32_t l_1161[5];
        uint32_t l_1262 = 0x982513B7L;
        int i, j;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 2; j++)
                l_955[i][j] = &l_954;
        }
        for (i = 0; i < 5; i++)
            l_1120[i] = &g_118;
        for (i = 0; i < 5; i++)
            l_1161[i] = 1UL;
        l_941 |= (l_694 = (safe_mul_func_uint16_t_u_u((safe_unary_minus_func_uint16_t_u(g_40[1].f3)), ((safe_div_func_uint16_t_u_u(((safe_sub_func_uint8_t_u_u(l_922[3][0], (safe_lshift_func_int8_t_s_u(((g_250[0][0] ^ (((((safe_div_func_int16_t_s_s(((g_929[0] = l_927) == &g_406), (safe_rshift_func_uint16_t_u_s(((void*)0 == l_932), 3)))) > ((safe_lshift_func_int16_t_s_s((+(g_458[4] < ((void*)0 != l_938))), 10)) > p_45)) != p_45) > 0xD8L) & 0x37F93CF1L)) | p_45), 7)))) > l_940[0]), p_45)) ^ l_922[3][2]))));
        l_956 = ((safe_mul_func_float_f_f((l_941 = (((((safe_mul_func_uint16_t_u_u((+(--(*l_934))), (((((*l_933) == ((*l_951) = &g_316)) ^ l_922[3][0]) , ((l_954 = l_954) != &g_487)) , ((p_45 ^ (-2L)) != l_922[3][0])))) == g_233) == 0xAC4DL) , (**g_552)) >= (*g_6))), l_956)) != (-0x5.Bp-1));
        if ((0x57DFEFAEL ^ (*g_192)))
        { /* block id: 410 */
            int64_t l_962 = (-1L);
            float *l_1021[2][8][4] = {{{&g_7,(void*)0,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,(void*)0,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,(void*)0,&g_7},{&g_7,(void*)0,&g_7,&g_7}},{{&g_7,&g_7,&g_7,&g_7},{(void*)0,(void*)0,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,(void*)0,&g_7},{&g_7,(void*)0,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7}}};
            int32_t l_1026 = 0xA4C26859L;
            int32_t *l_1041 = (void*)0;
            int32_t l_1042 = 0x450FCC68L;
            int8_t *l_1043[6][5] = {{&l_922[2][4],&l_922[2][4],(void*)0,&l_922[2][4],&l_922[2][4]},{&l_922[4][3],&l_922[2][4],&l_922[4][3],&l_922[4][3],&l_922[2][4]},{&l_922[2][4],&l_922[4][3],&l_922[4][3],&l_922[2][4],&l_922[4][3]},{&l_922[2][4],&l_922[2][4],(void*)0,&l_922[2][4],&l_922[2][4]},{&l_922[4][3],&l_922[2][4],&l_922[4][3],&l_922[4][3],&l_922[2][4]},{&l_922[2][4],&l_922[4][3],&l_922[4][3],&l_922[2][4],&l_922[4][3]}};
            int i, j, k;
            if (p_45)
            { /* block id: 411 */
                float l_964[1];
                int32_t l_966 = 0x160D0E04L;
                int i;
                for (i = 0; i < 1; i++)
                    l_964[i] = (-0x3.Bp-1);
                for (g_93 = 0; (g_93 <= 4); g_93 += 1)
                { /* block id: 414 */
                    int i;
                    if (g_957[4][5])
                        break;
                }
                if (l_956)
                    goto lbl_958;
                for (l_941 = 0; (l_941 == (-14)); --l_941)
                { /* block id: 420 */
                    int32_t *l_963[8][8] = {{(void*)0,(void*)0,&g_100[3],&l_941,&g_118,&g_100[0],(void*)0,(void*)0},{&g_100[3],&l_941,&g_100[1],&g_100[1],&l_941,&g_100[3],(void*)0,(void*)0},{&l_941,&g_100[3],(void*)0,(void*)0,&g_118,&g_100[1],&l_956,&g_118},{(void*)0,(void*)0,&g_100[2],(void*)0,&g_118,&l_956,&g_118,(void*)0},{&g_118,&g_118,&g_118,&g_100[1],&l_956,(void*)0,&g_100[1],(void*)0},{&g_118,&g_118,(void*)0,&l_941,&g_100[4],&g_118,&l_956,&g_118},{&g_118,(void*)0,(void*)0,&l_956,&l_956,(void*)0,(void*)0,&g_118},{&g_118,(void*)0,&g_118,&g_118,&g_118,&g_100[3],&g_118,(void*)0}};
                    int8_t *l_977 = &g_256[1][2];
                    union U1 l_984 = {0x8669L};
                    int16_t *l_985 = &g_406.f0;
                    int16_t *l_989 = (void*)0;
                    float *l_992 = &l_818;
                    float *l_1006[9][4] = {{&l_964[0],(void*)0,(void*)0,(void*)0},{(void*)0,&l_964[0],(void*)0,(void*)0},{&l_964[0],&l_964[0],(void*)0,(void*)0},{&l_964[0],&l_964[0],&l_964[0],&l_964[0]},{(void*)0,&l_964[0],&l_964[0],(void*)0},{(void*)0,&l_964[0],&l_964[0],&l_964[0]},{&l_964[0],&l_964[0],&l_964[0],&l_964[0]},{(void*)0,(void*)0,&l_964[0],&l_964[0]},{(void*)0,&l_964[0],(void*)0,&l_964[0]}};
                    int i, j;
                    ++l_967;
                    for (g_487 = 0; (g_487 >= 39); g_487 = safe_add_func_int32_t_s_s(g_487, 4))
                    { /* block id: 424 */
                        if (p_45)
                            break;
                    }
                    l_965 &= ((g_730.f2 = (safe_sub_func_int8_t_s_s(((((safe_rshift_func_int8_t_s_s(((((((*l_977) = (safe_unary_minus_func_uint32_t_u(l_967))) < g_729.f1) >= (*g_2)) == (safe_sub_func_int64_t_s_s(1L, ((safe_mod_func_uint8_t_u_u((((g_97 ^ (safe_sub_func_uint16_t_u_u(1UL, (l_984 , ((*l_985) = p_45))))) && (safe_mul_func_int8_t_s_s((((((l_962 || g_988[4]) & g_988[4]) < (*g_653)) < 0x18FF4B4E715FA559LL) , p_45), 0x53L))) != (**g_652)), 0x8FL)) < p_45)))) >= p_45), 1)) , l_966) == 5UL) | (*g_525)), (-1L)))) , l_966);
                    l_966 = (safe_div_func_float_f_f(((*l_992) = p_45), (((((p_45 < (safe_add_func_float_f_f((safe_div_func_float_f_f(p_45, (safe_mul_func_float_f_f((-0x1.Bp-1), ((*g_6) == (safe_div_func_float_f_f(((*g_525) , (safe_div_func_float_f_f(l_941, ((g_1007 = (-((((((0x7.Ep+1 > (safe_add_func_float_f_f(0x3.0B5CAEp-16, l_962))) < 0x1.4p+1) == p_45) <= p_45) == 0xE.64F0A6p-1) <= l_966))) <= p_45)))), 0xF.216F5Fp-61))))))), l_966))) == (***g_551)) < l_966) != l_961) < 0x4.9539F7p-73)));
                }
                if (l_962)
                    continue;
            }
            else
            { /* block id: 436 */
                int32_t *l_1008 = (void*)0;
                int8_t *l_1023 = &g_256[2][3];
                int32_t l_1024 = 0x7A3CAA48L;
                l_1009 = l_1008;
                l_1024 |= (safe_rshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s(((*l_1023) &= ((((*g_653) > ((void*)0 != &g_5)) == (~((p_45 | (safe_lshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((~(((*g_135) = (*g_135)) != ((0x309804E9L && ((!g_193[1]) , 4294967295UL)) , l_1021[0][5][2]))), l_1022)), p_45))) && p_45))) && p_45)), p_45)), 6));
                l_1026 &= (((void*)0 != &l_939) , g_1025);
            }
            (*g_192) = ((l_962 || ((safe_mod_func_int16_t_s_s((((g_256[4][4] = (safe_div_func_int64_t_s_s(((0xE538FC50855AD12ALL <= (safe_mul_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u(g_233, g_957[4][5])) & 0x41FCL), (0x6FL && ((safe_rshift_func_int16_t_s_u((p_45 && (((l_1042 = ((l_1026 = (safe_div_func_uint16_t_u_u((safe_mod_func_uint16_t_u_u(0x23C9L, g_336)), l_962))) > p_45)) | p_45) >= (*g_653))), 1)) < p_45))))) <= (-1L)), 0x71A9E432F1E25EE2LL))) , g_729) , 3L), 1L)) | (-6L))) == 0x57L);
        }
        else
        { /* block id: 447 */
            int64_t l_1064 = (-3L);
            int32_t l_1070[3];
            uint32_t l_1074 = 1UL;
            uint64_t *l_1118 = &g_64;
            union U1 l_1136[3] = {{0x0639L},{0x0639L},{0x0639L}};
            int16_t *l_1192 = (void*)0;
            int32_t l_1216 = 1L;
            uint32_t **l_1267[8] = {&l_1211,&l_934,&l_1211,&l_934,&l_1211,&l_934,&l_1211,&l_934};
            uint32_t l_1300 = 4UL;
            int32_t *l_1330 = &g_118;
            int i;
            for (i = 0; i < 3; i++)
                l_1070[i] = 0xCB382EE1L;
            for (g_487 = 0; (g_487 <= 1); g_487 += 1)
            { /* block id: 450 */
                int32_t l_1069 = 0x122DA807L;
                int32_t l_1072[3][10];
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 10; j++)
                        l_1072[i][j] = 9L;
                }
                if ((l_777[(g_487 + 5)] || ((g_988[1] >= p_45) >= 0x5161088536C9C7ABLL)))
                { /* block id: 451 */
                    uint64_t l_1046 = 5UL;
                    float *l_1063 = &g_7;
                    float *l_1065 = &l_818;
                    float *l_1066[10][10] = {{&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067},{&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067},{&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067},{&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067},{&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067},{&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067},{&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067},{&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067},{&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067,&g_1067},{&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067,&g_1067,(void*)0,&g_1067}};
                    int32_t l_1068 = 0L;
                    int i, j;
                    ++l_1046;
                    l_1069 = ((safe_div_func_float_f_f((safe_div_func_float_f_f((l_777[(g_487 + 5)] > (&g_751 == (void*)0)), (safe_div_func_float_f_f((safe_mul_func_float_f_f(p_45, ((-0x10.9p-1) < ((((*l_691) = &g_115) == l_1057[5][0]) <= p_45)))), (((l_1068 = ((*l_1065) = (safe_sub_func_float_f_f(((safe_add_func_float_f_f(((*l_1063) = l_1062), 0xE.96CC5Dp-84)) <= l_1064), l_777[(g_487 + 5)])))) < l_777[(g_487 + 5)]) >= 0x2.A62F57p+13))))), 0xD.584777p+88)) <= p_45);
                    (*g_222) = (void*)0;
                }
                else
                { /* block id: 459 */
                    int32_t *l_1071[10] = {&g_100[5],&l_965,&l_965,&g_100[5],&l_965,&l_965,&g_100[5],&l_965,&l_965,&g_100[5]};
                    int16_t *l_1084[9] = {&g_730.f2,&g_730.f2,&g_730.f2,&g_730.f2,&g_730.f2,&g_730.f2,&g_730.f2,&g_730.f2,&g_730.f2};
                    int32_t ****l_1089 = (void*)0;
                    int32_t *****l_1090 = &l_824;
                    uint16_t l_1091 = 65535UL;
                    int i;
                    l_1074++;
                    l_1091 &= (((g_716[2].f2 != (safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(((((l_1081 != (void*)0) ^ l_1069) , ((safe_mul_func_int16_t_s_s((l_1072[0][2] = (p_45 = ((*g_676) > 0x437D10A4A4C0239ALL))), (safe_mul_func_uint8_t_u_u((((((*l_1090) = l_1089) == (void*)0) || (((&g_256[2][3] != &l_922[3][0]) & l_777[(g_487 + 5)]) <= (*g_653))) && (*g_653)), l_922[1][4])))) ^ 0x7959DFEFA5133A8DLL)) || p_45), l_1069)), g_250[0][0]))) | g_526[0]) || (*g_2));
                    (***g_190) ^= (safe_mod_func_int8_t_s_s(l_1069, (safe_rshift_func_uint16_t_u_u(0x6F2FL, (((((*l_934) = 0x8E2DEBB7L) <= 0xBCBFF353L) , (l_965 = (safe_div_func_uint8_t_u_u(l_1070[0], 0x7CL)))) == l_1072[0][7])))));
                }
                if (p_45)
                    continue;
            }
            if (p_45)
                continue;
            for (g_336 = 1; (g_336 <= 5); g_336 += 1)
            { /* block id: 474 */
                union U0 **l_1111 = &g_1099[4];
                union U1 l_1121[8][4][1] = {{{{0xA7C9L}},{{0x1C4FL}},{{0x47C2L}},{{0x73D8L}}},{{{0x47C2L}},{{0x1C4FL}},{{0xA7C9L}},{{0x1C4FL}}},{{{0x47C2L}},{{0x73D8L}},{{0x47C2L}},{{0x1C4FL}}},{{{0xA7C9L}},{{0x1C4FL}},{{0x47C2L}},{{0x73D8L}}},{{{0x47C2L}},{{0x1C4FL}},{{0xA7C9L}},{{0x1C4FL}}},{{{0x47C2L}},{{0x73D8L}},{{0x47C2L}},{{0x1C4FL}}},{{{0xA7C9L}},{{0x1C4FL}},{{0x47C2L}},{{0x73D8L}}},{{{0x47C2L}},{{0x1C4FL}},{{0xA7C9L}},{{0x1C4FL}}}};
                int32_t *l_1135 = (void*)0;
                uint16_t *l_1176 = &g_487;
                int i, j, k;
                (*g_227) = &l_1070[0];
                g_1099[4] = &g_40[1];
                if (((safe_div_func_int64_t_s_s(((safe_sub_func_uint32_t_u_u((((g_1104[4] , g_1105) , (l_922[g_336][(g_336 + 2)] , (safe_div_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((((*l_1111) = l_1110) == (l_1070[1] , l_1110)), (((safe_div_func_int16_t_s_s(((++(*l_954)) < g_93), ((**g_191) & (safe_lshift_func_int16_t_s_u((p_45 < p_45), 4))))) , l_1118) == (void*)0))) , p_45), p_45)))) & 18446744073709551615UL), 4294967288UL)) ^ 1L), (*g_525))) == p_45))
                { /* block id: 479 */
                    int32_t *l_1119 = &g_100[0];
                    union U0 **l_1137 = &g_1099[4];
                    l_1120[0] = l_1119;
                    if (p_45)
                        continue;
                    for (l_967 = 0; (l_967 <= 7); l_967 += 1)
                    { /* block id: 484 */
                        float **l_1123 = &g_136;
                        float ***l_1122 = &l_1123;
                        int32_t *l_1134[8][9][2] = {{{&l_1070[0],&l_941},{&l_956,&g_100[4]},{&g_100[5],&l_941},{&l_694,(void*)0},{&g_100[0],&l_694},{&l_956,&g_100[0]},{&l_1070[2],&l_694},{&l_956,&l_956},{&l_694,&g_100[0]}},{{(void*)0,&g_100[4]},{&l_1070[2],&l_941},{&l_694,&l_1070[1]},{&l_956,(void*)0},{&l_956,(void*)0},{&l_694,&l_956},{&l_956,&l_1070[2]},{&l_941,&l_694},{(void*)0,&l_694}},{{&l_941,&l_1070[2]},{&l_956,&l_956},{&l_694,(void*)0},{&l_956,(void*)0},{&l_956,&l_1070[1]},{&l_694,&l_941},{&l_1070[2],&g_100[4]},{(void*)0,&g_100[0]},{&l_694,&l_956}},{{&l_956,&l_694},{&l_1070[2],&g_100[0]},{&l_956,&l_694},{&g_100[0],&g_118},{&l_941,&l_941},{&l_956,&l_956},{&l_694,(void*)0},{&l_694,&l_694},{&l_1070[0],&l_694}},{{(void*)0,&g_118},{(void*)0,&l_1070[2]},{&g_100[0],(void*)0},{&l_1070[0],&l_941},{&g_100[5],&l_956},{&g_118,&l_1070[0]},{&g_100[3],&g_100[3]},{&l_1070[1],&g_100[1]},{&l_956,&l_1070[2]}},{{&l_965,&l_1070[0]},{(void*)0,&l_965},{&g_100[0],&g_100[0]},{&g_100[0],&l_965},{(void*)0,&l_1070[0]},{&l_965,&l_1070[2]},{&l_956,&g_100[1]},{&l_1070[1],&g_100[3]},{&g_100[3],&l_1070[0]}},{{&g_118,&l_956},{&g_100[5],&l_941},{&l_1070[0],(void*)0},{&g_100[0],&l_1070[2]},{(void*)0,&g_118},{(void*)0,&l_694},{&l_1070[0],&l_694},{&l_694,(void*)0},{&l_694,&l_956}},{{&l_956,&l_941},{&l_941,&g_118},{&g_100[0],&l_694},{&l_956,&g_100[0]},{&l_1070[2],&l_694},{&l_956,&l_956},{&l_694,&g_100[0]},{(void*)0,&g_100[4]},{&l_1070[2],&l_941}}};
                        int i, j, k;
                        l_1135 = l_1134[2][5][1];
                        if ((*g_684))
                            break;
                        l_1070[1] = (l_1136[2] , (l_1137 != (void*)0));
                        (**g_191) |= l_1136[2].f0;
                    }
                }
                else
                { /* block id: 491 */
                    (**g_190) = (**g_190);
                }
                if (l_1074)
                { /* block id: 494 */
                    uint8_t l_1153 = 247UL;
                    int32_t l_1158 = 0L;
                    (***g_190) = (**g_227);
                    for (g_97 = 7; (g_97 >= 1); g_97 -= 1)
                    { /* block id: 498 */
                        int8_t l_1148 = 0xF9L;
                        int32_t l_1159 = 0x71C385B4L;
                        int32_t l_1160 = 0x45F6939EL;
                        const union U1 * const l_1164 = &l_1121[3][3][0];
                        int i;
                        (***g_190) = ((**g_227) = (safe_lshift_func_int8_t_s_s(p_45, (safe_mul_func_int8_t_s_s(((safe_mod_func_uint8_t_u_u((((-1L) == (((safe_add_func_uint16_t_u_u((l_1148 && ((g_702.f0 ^ ((((*l_954) = (safe_add_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_s(((*g_653) < 1UL), 10)), l_1153))) <= p_45) , (l_1158 &= (safe_div_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(g_233, g_729.f2)), g_118))))) == 1UL)), l_1136[2].f0)) , l_1148) == g_729.f1)) > 18446744073709551615UL), 0xC4L)) , (-1L)), p_45)))));
                        if (l_1148)
                            continue;
                        l_1161[3]++;
                        g_1165 = l_1164;
                    }
                    for (l_1064 = 7; (l_1064 >= 1); l_1064 -= 1)
                    { /* block id: 509 */
                        int16_t *l_1179[4][3] = {{&g_716[2].f2,&g_40[1].f2,&g_40[1].f2},{&g_729.f2,(void*)0,(void*)0},{&g_716[2].f2,&g_40[1].f2,&g_40[1].f2},{&g_729.f2,(void*)0,(void*)0}};
                        int32_t l_1180 = 0x66385A05L;
                        int i, j;
                    }
                }
                else
                { /* block id: 519 */
                    return g_1181;
                }
            }
            if (((l_1070[1] >= ((p_45 , (l_1136[2].f0 <= (safe_add_func_int64_t_s_s(((&g_496 != &g_496) ^ ((safe_div_func_int32_t_s_s((safe_add_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s((((*l_927) , (g_1193[2][2] = l_1192)) == &p_45), (safe_mod_func_int32_t_s_s((safe_add_func_uint8_t_u_u((safe_mod_func_int16_t_s_s(((l_954 = &g_1007) == &g_1007), p_45)), g_957[0][0])), l_1200)))), g_64)), p_45)) < 0x448DL)), 1UL)))) == g_716[2].f1)) > (-1L)))
            { /* block id: 525 */
                int32_t l_1203 = 0x1F6ABFA4L;
                uint32_t **l_1213 = &l_1211;
                for (g_93 = 25; (g_93 != 37); g_93 = safe_add_func_uint8_t_u_u(g_93, 4))
                { /* block id: 528 */
                    uint32_t ***l_1212 = &l_1210;
                    union U1 l_1217[7] = {{0xB265L},{0xB265L},{0xB265L},{0xB265L},{0xB265L},{0xB265L},{0xB265L}};
                    uint8_t ***l_1244[3];
                    uint8_t ****l_1243 = &l_1244[2];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1244[i] = &l_691;
                    (***g_190) &= l_1203;
                    if ((l_1203 = (((((*l_954) = l_1203) | (((0xAE58FFA7C8F63FA7LL ^ 18446744073709551610UL) > (safe_rshift_func_int16_t_s_s(((p_45 , (safe_sub_func_uint32_t_u_u((safe_mod_func_uint64_t_u_u((((((*l_1212) = l_1210) != l_1213) , (safe_unary_minus_func_uint16_t_u((l_1064 && p_45)))) < (&p_45 != &p_45)), l_1136[2].f0)), 0xF81E3622L))) == 0L), 4))) >= (***g_237))) , 9L) , 9L)))
                    { /* block id: 533 */
                        return g_1215;
                    }
                    else
                    { /* block id: 535 */
                        uint32_t l_1225[6] = {0x76317E88L,1UL,0x76317E88L,0x76317E88L,1UL,0x76317E88L};
                        float * const l_1228 = &l_818;
                        uint32_t *l_1229 = &g_316;
                        int i;
                        if (l_1216)
                            break;
                        (*g_222) = &l_1070[0];
                    }
                    l_1216 ^= (safe_rshift_func_int16_t_s_u((safe_sub_func_uint32_t_u_u((((safe_mul_func_int16_t_s_s((safe_div_func_int16_t_s_s(((&g_1175[0] == (void*)0) | ((**l_1133) = (0xCAL && ((4UL == ((0x1495437008B2B95ALL | (*g_525)) != l_1217[6].f0)) != (+(p_45 = (0x7BL != (safe_mul_func_int8_t_s_s((((void*)0 == &g_1007) , p_45), g_1215.f3))))))))), l_1070[2])), 65529UL)) , 0x65L) != g_526[0]), 2UL)), l_1217[6].f0));
                    (*l_1243) = &l_691;
                }
                return g_1245[0];
            }
            else
            { /* block id: 547 */
                uint32_t *l_1254 = &l_1161[1];
                int32_t l_1255 = 0x23D10AAFL;
                int8_t *l_1256 = &g_256[0][0];
                int32_t l_1257 = 0x95D61412L;
                int32_t l_1304 = 0x23213E35L;
                union U1 l_1324[5][9][5] = {{{{1L},{0x4CF2L},{1L},{0x2BD1L},{0x2BD1L}},{{0x98B1L},{0xDA74L},{0x98B1L},{0xB5EFL},{0xB5EFL}},{{1L},{0x4CF2L},{1L},{0x2BD1L},{0x2BD1L}},{{0x98B1L},{0xDA74L},{0x98B1L},{0xB5EFL},{0xB5EFL}},{{1L},{0x4CF2L},{1L},{0x2BD1L},{0x2BD1L}},{{0x98B1L},{0xDA74L},{0x98B1L},{0xB5EFL},{0xB5EFL}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}}},{{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}}},{{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}}},{{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}}},{{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{8L},{0xB27DL},{8L},{1L},{1L}},{{-1L},{-1L},{-1L},{0x98B1L},{0x98B1L}},{{0x4CF2L},{0x2BD1L},{0x4CF2L},{8L},{8L}}}};
                int i, j, k;
                l_1216 ^= (safe_sub_func_uint16_t_u_u((p_45 , ((1UL && (safe_sub_func_int64_t_s_s(((safe_sub_func_int32_t_s_s(0x5581AAE0L, (safe_div_func_int32_t_s_s(((*g_238) != l_1254), (((((((255UL || ((*l_1256) |= l_1255)) <= (g_250[0][0]++)) , (safe_rshift_func_uint16_t_u_u(0UL, (p_45 , p_45)))) , p_45) ^ p_45) || 1L) || p_45))))) , p_45), p_45))) | p_45)), p_45));
                ++l_1262;
                for (l_1062 = 6; (l_1062 >= 0); l_1062 -= 1)
                { /* block id: 554 */
                    uint32_t **l_1265 = &l_934;
                    uint32_t ***l_1266[2];
                    int32_t *l_1268 = (void*)0;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_1266[i] = &l_1265;
                    l_1267[4] = l_1265;
                    g_723[l_1062] = (l_1268 = &l_1255);
                    if ((*g_192))
                        break;
                    if (((((*l_1118) ^= (((safe_mod_func_uint8_t_u_u((safe_div_func_int16_t_s_s((((safe_add_func_uint64_t_u_u((safe_add_func_uint64_t_u_u((*l_1268), p_45)), (p_45 != ((((void*)0 == g_1277[2][2][3]) == (safe_rshift_func_int16_t_s_u(((p_45 != ((0x59DFL <= (((safe_add_func_float_f_f(((safe_mul_func_float_f_f((+0x0.7p-1), 0x1.2p+1)) >= p_45), 0xD.9B80CCp+58)) , l_1257) , p_45)) == 1L)) | (*g_1278)), p_45))) & g_487)))) | 4294967295UL) , 0xA13DL), (-8L))), p_45)) != 0xE692L) , 0UL)) , l_1136[2]) , p_45))
                    { /* block id: 560 */
                        const uint32_t l_1303[5][10][4] = {{{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L},{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL},{4294967292UL,0x973ABD22L,0xCD9D76ABL,0x3437FF00L},{0x973ABD22L,0x325BE481L,0x67CC9A5EL,0x3437FF00L},{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L},{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL}},{{4294967292UL,0x973ABD22L,0xCD9D76ABL,0x3437FF00L},{0x973ABD22L,0x325BE481L,0x67CC9A5EL,0x3437FF00L},{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L},{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL},{4294967292UL,0x973ABD22L,0xCD9D76ABL,0x3437FF00L},{0x973ABD22L,0x325BE481L,0x67CC9A5EL,0x3437FF00L},{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L}},{{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL},{4294967292UL,0x973ABD22L,0xCD9D76ABL,0x3437FF00L},{0x973ABD22L,0x325BE481L,0x67CC9A5EL,0x3437FF00L},{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L},{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL},{4294967292UL,0x973ABD22L,0xCD9D76ABL,0x3437FF00L},{0x973ABD22L,0x325BE481L,0x67CC9A5EL,0x3437FF00L}},{{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L},{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL},{4294967292UL,0x973ABD22L,0xCD9D76ABL,0x3437FF00L},{0x973ABD22L,0x325BE481L,0x67CC9A5EL,0x3437FF00L},{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L},{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL}},{{4294967292UL,0x973ABD22L,0xCD9D76ABL,0x3437FF00L},{0x973ABD22L,0x325BE481L,0x67CC9A5EL,0x3437FF00L},{0x3A25978EL,0x973ABD22L,0x6C551EABL,0x6C551EABL},{0x973ABD22L,0x973ABD22L,7UL,0x3437FF00L},{4294967292UL,0x325BE481L,0x6C551EABL,0x3437FF00L},{0x325BE481L,0x973ABD22L,0x67CC9A5EL,0x6C551EABL},{4294967292UL,0x973ABD22L,0x3437FF00L,0x6C551EABL},{0x325BE481L,2UL,7UL,0x6C551EABL},{2UL,0x325BE481L,0xCD9D76ABL,0xCD9D76ABL},{0x325BE481L,0x325BE481L,4294967290UL,0x6C551EABL}}};
                        uint8_t *l_1325 = (void*)0;
                        int32_t l_1326 = (-1L);
                        float *l_1327 = (void*)0;
                        int i, j, k;
                        l_1304 |= (safe_rshift_func_uint8_t_u_s((safe_div_func_uint16_t_u_u(((*l_954) |= 0xE56FL), (safe_sub_func_int32_t_s_s((safe_lshift_func_int16_t_s_u((safe_sub_func_uint64_t_u_u(((**g_652) , (((8L > (*l_1268)) , ((safe_mul_func_uint16_t_u_u(((safe_sub_func_float_f_f(p_45, ((*l_1268) = ((l_1300 > (*g_6)) == (safe_add_func_float_f_f(((l_1257 < (0x1.Bp+1 != 0x1.Ap+1)) <= 0x3.6p-1), p_45)))))) , p_45), 0xAB53L)) , p_45)) && l_1303[3][3][0])), l_1303[0][8][3])), l_1257)), 4294967295UL)))), l_1300));
                        (*g_6) = ((l_1070[0] = ((((((((safe_add_func_int8_t_s_s(((safe_lshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_u(((((void*)0 == &g_526[3]) ^ (safe_add_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((0x991D05308F244B8BLL && (safe_rshift_func_uint8_t_u_u((l_1326 = ((g_1317[1] , (safe_rshift_func_int16_t_s_u(((p_45 & p_45) <= ((((safe_div_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u(((((l_1324[1][1][4] , l_1325) == (void*)0) , p_45) || 1L), 0x70L)), p_45)) == (-2L)) <= l_1303[3][3][0]) > (*g_59))), p_45))) >= (*g_653))), (*g_1278)))) < p_45), p_45)), p_45))) || 9UL), p_45)), 11)) != 0x39EA0BC9L), l_1303[1][1][0])) <= p_45) == l_1136[2].f0) , p_45) < 0x8.6BDA80p-3) >= 0x1.Cp+1) == 0x0.1p-1) != l_1303[1][2][1])) < l_1303[3][3][0]);
                        return g_1328;
                    }
                    else
                    { /* block id: 568 */
                        int32_t *l_1329 = &l_1216;
                        (**g_191) = (l_1324[1][1][4].f0 > ((&l_941 == (l_1330 = l_1329)) <= ((l_1070[2] = p_45) && (safe_add_func_int8_t_s_s(l_1257, ((*g_684) >= (*g_239)))))));
                    }
                }
            }
        }
    }
    (***g_190) = (p_45 , ((*l_1341) = ((l_1340[4][0] = (safe_mul_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u(((&g_496 == (((((*l_1110) , (safe_add_func_int16_t_s_s(5L, ((void*)0 != (**g_237))))) && p_45) == (*g_525)) , &l_824)) > 0xA4E3B3A7L), 9)), (*g_1278)))) >= 0x6CL)));
    return (*l_1110);
}


/* ------------------------------------------ */
/* 
 * reads : g_64 g_65 g_233 g_40.f1 g_7 g_78 g_97 g_223 g_118 g_5 g_222 g_227 g_40.f2 g_676 g_525 g_526 g_652 g_653
 * writes: g_65 g_97 g_78 g_118 g_93 g_223
 */
static int16_t  func_50(uint32_t * p_51, const int8_t * p_52, int8_t * p_53)
{ /* block id: 82 */
    int32_t l_259 = 0x240C9C9EL;
    int16_t l_267 = 0xB31CL;
    union U1 l_269 = {0xFF69L};
    uint32_t **l_282[3];
    int32_t l_303 = 7L;
    int32_t l_304 = 0x2A585A5FL;
    uint8_t *l_325[6] = {&g_250[0][0],&g_250[0][0],&g_250[0][0],&g_250[0][0],&g_250[0][0],&g_250[0][0]};
    uint8_t **l_324 = &l_325[0];
    int32_t l_343 = 1L;
    int32_t l_349 = 0xA087F769L;
    int32_t l_350 = 0L;
    int32_t l_354 = 0xBBAE65E6L;
    int32_t l_356[4][10][6] = {{{(-8L),0x3D4D2CE4L,2L,3L,1L,(-3L)},{(-1L),(-1L),0xF2211BC0L,(-1L),1L,(-10L)},{0L,0x51BD0BF2L,1L,0xD11A3A0CL,0x72AD7945L,1L},{(-8L),(-3L),(-9L),0x3868164DL,(-6L),0L},{1L,(-1L),0L,(-1L),(-3L),1L},{0x72AD7945L,0x2097EB6BL,(-10L),(-1L),2L,0x76DAE3B9L},{(-1L),(-1L),0x3ECA1C97L,0x4EAF48ABL,0x12EE13CEL,0xF1A3C8B6L},{0xF48BA525L,(-7L),0x4EAF48ABL,(-7L),0xF48BA525L,0xFB69E625L},{0x954BABEFL,2L,1L,0L,0L,1L},{1L,1L,(-1L),2L,0x7FAADBBCL,1L}},{{6L,0xBEAC97DAL,1L,0xD30B71DFL,0L,0xFB69E625L},{0x7FAADBBCL,0x2FB22B93L,0x4EAF48ABL,(-1L),(-3L),0xF1A3C8B6L},{0L,0x92A074DEL,0x3ECA1C97L,(-9L),0x3D4D2CE4L,0x76DAE3B9L},{0xA2B2D58DL,0x20F8E480L,(-10L),1L,0xEED7DAFBL,1L},{0x3868164DL,0x12EE13CEL,0L,0xF1A3C8B6L,0x7FAADBBCL,0L},{0x2097EB6BL,0x76DAE3B9L,(-9L),0x90FCD752L,0xF1A3C8B6L,1L},{2L,1L,1L,0x69A0E57AL,(-7L),(-10L)},{(-8L),(-7L),0xF2211BC0L,(-9L),0xBEAC97DAL,(-3L)},{0x5464B650L,(-10L),2L,0x87C23023L,1L,0xA2B2D58DL},{1L,(-9L),(-8L),0xD11A3A0CL,(-3L),1L}},{{6L,0x72AD7945L,(-9L),0xF2211BC0L,0xD42AAD9CL,0xE686AA94L},{0x92A074DEL,(-1L),(-1L),(-1L),(-1L),0x92A074DEL},{0x72AD7945L,0x51BD0BF2L,1L,(-7L),0xFB69E625L,0x76DAE3B9L},{0L,(-10L),0xD42AAD9CL,0x5464B650L,0x12EE13CEL,(-1L)},{0L,(-3L),0x5464B650L,(-7L),(-1L),0x3868164DL},{0x72AD7945L,0xFB69E625L,1L,(-1L),0xF1A3C8B6L,0L},{0x92A074DEL,1L,0x90FCD752L,0xF2211BC0L,0x077F8951L,0x7FAADBBCL},{6L,1L,0x2C63D11BL,0xD11A3A0CL,0xE686AA94L,0xFB69E625L},{1L,0x20F8E480L,0x87C23023L,0xD30B71DFL,0xFB69E625L,(-1L)},{(-1L),0x3D4D2CE4L,0x90FCD752L,1L,0x3868164DL,0x92E5CFBAL}},{{0L,(-9L),0L,(-1L),(-8L),(-7L)},{0x5464B650L,0x5914214FL,(-1L),(-7L),1L,(-8L)},{0xD11A3A0CL,1L,(-1L),0L,(-1L),(-3L)},{0x4EAF48ABL,0x5464B650L,(-7L),0x879D84CDL,(-8L),0x5F7BA5FAL},{0x5F7BA5FAL,0xFB69E625L,0x4EAF48ABL,1L,0xCF92B8A4L,3L},{0x51BD0BF2L,0L,0x5464B650L,0x407BED13L,0x5464B650L,0L},{1L,6L,1L,0x077F8951L,0xBEAC97DAL,0x954BABEFL},{0x7FAADBBCL,0x8D3BB893L,(-1L),0x5464B650L,0xF1A3C8B6L,0x20F8E480L},{(-1L),0x8D3BB893L,0xA2B2D58DL,1L,0xBEAC97DAL,(-1L)},{0x2C63D11BL,6L,0x5F7BA5FAL,0x3868164DL,0x5464B650L,0x92E5CFBAL}}};
    uint64_t l_367 = 0xAEF6A1A9C6E4E7EBLL;
    float **l_371 = &g_136;
    uint64_t *l_374 = &g_93;
    int32_t l_376 = (-4L);
    float l_377 = (-0x3.Cp-1);
    int8_t l_378 = 0x6CL;
    int8_t l_379[7][10][3] = {{{1L,0xFFL,0xA0L},{0x41L,(-2L),0L},{0x96L,0x28L,8L},{0x25L,0xFBL,(-10L)},{0xFFL,0xA0L,8L},{0x41L,0xFFL,0L},{8L,(-1L),0xA0L},{0x96L,0xE8L,0x87L},{1L,0xE8L,(-10L)},{0x71L,(-1L),(-10L)}},{{1L,0xFFL,(-1L)},{0x90L,0xA0L,(-2L)},{0x96L,0xFBL,0x5DL},{0x90L,0x28L,(-10L)},{1L,(-2L),0x30L},{0x71L,0xFFL,0L},{1L,0L,0L},{0x96L,0x2CL,0x30L},{8L,1L,(-10L)},{0x41L,0L,0x5DL}},{{0xFFL,0xFFL,(-2L)},{0x25L,0L,(-1L)},{0x96L,1L,(-10L)},{0x41L,0x2CL,(-10L)},{1L,0L,0x87L},{1L,0xFFL,0xA0L},{0x41L,(-2L),0L},{0x96L,0x28L,8L},{0x25L,0xFBL,(-10L)},{0xFFL,0xA0L,8L}},{{0x41L,0xFFL,0L},{8L,(-1L),0xA0L},{0x96L,0xE8L,0x87L},{1L,0xE8L,(-10L)},{0x71L,(-1L),(-10L)},{1L,0xFFL,(-1L)},{0x90L,0xA0L,(-2L)},{0x96L,0xFBL,0x5DL},{0x90L,0x28L,(-10L)},{1L,(-2L),0x30L}},{{0x71L,0L,(-1L)},{0x5DL,(-1L),(-1L)},{0xA8L,0x8FL,0xA8L},{8L,0x47L,0L},{(-2L),0xC1L,0L},{0L,0L,0xB0L},{0x30L,0xC1L,0x28L},{0xA8L,0x47L,0x81L},{(-10L),0x8FL,0L},{0xA0L,(-1L),0x32L}},{{0xA0L,0L,0x80L},{(-10L),0xB0L,0xC1L},{0xA8L,2L,0L},{0x30L,0x05L,0L},{0L,0x80L,0L},{(-2L),0L,0xC1L},{8L,0x28L,0x80L},{0xA8L,0x22L,0x32L},{0x5DL,0x22L,0L},{(-1L),0x28L,0x81L}},{{0L,0L,0x28L},{0x87L,0x80L,0xB0L},{0xA8L,0x05L,0L},{0x87L,2L,0L},{0L,0xB0L,0xA8L},{(-1L),0L,(-1L)},{0x5DL,(-1L),(-1L)},{0xA8L,0x8FL,0xA8L},{8L,0x47L,0L},{(-2L),0xC1L,0L}}};
    uint32_t l_380 = 18446744073709551615UL;
    int32_t *l_647 = &g_118;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_282[i] = (void*)0;
lbl_385:
    for (g_65 = 0; (g_65 > 36); g_65++)
    { /* block id: 85 */
        union U1 *l_270 = &l_269;
        int32_t l_271 = 0x82E84388L;
        int32_t l_347[4][3];
        int i, j;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 3; j++)
                l_347[i][j] = 0x4536F9A5L;
        }
        if (l_259)
            break;
        if (((safe_div_func_int32_t_s_s(((safe_mod_func_int32_t_s_s(((0x1055516A13217A38LL && (!(safe_sub_func_uint8_t_u_u(l_267, (safe_unary_minus_func_int16_t_s(l_259)))))) == (((*l_270) = l_269) , l_271)), g_64)) , (safe_add_func_int32_t_s_s((((g_65 & (*p_51)) && g_78) & g_97), (*g_223)))), 0xF1AEBCF4L)) && 0xDEF38617D44FB6F4LL))
        { /* block id: 88 */
            uint64_t l_291 = 0UL;
            for (g_97 = (-7); (g_97 >= (-12)); g_97--)
            { /* block id: 91 */
                uint32_t *l_284 = (void*)0;
                uint32_t **l_283[2];
                float l_286 = 0x8.62EAF8p+89;
                int16_t *l_287 = (void*)0;
                int16_t *l_288 = (void*)0;
                int16_t *l_289 = (void*)0;
                int16_t *l_290 = (void*)0;
                int i;
                for (i = 0; i < 2; i++)
                    l_283[i] = &l_284;
                for (g_78 = 0; (g_78 == (-28)); g_78 = safe_sub_func_uint8_t_u_u(g_78, 2))
                { /* block id: 94 */
                    return l_271;
                }
                l_271 |= (((**g_222) ^= (safe_mul_func_uint16_t_u_u(0x3495L, ((safe_div_func_uint8_t_u_u((&g_239 != (l_283[1] = l_282[1])), (*p_52))) >= ((l_259 = (g_40[1].f1 || (!(-3L)))) || 0UL))))) , l_291);
                return l_259;
            }
            if (l_271)
                break;
            (**g_227) &= l_271;
        }
        else
        { /* block id: 105 */
            float l_292 = 0x8.5CEBB0p+31;
            int32_t l_293[4][6] = {{(-7L),1L,1L,(-7L),(-5L),(-1L)},{(-1L),(-7L),9L,(-7L),(-1L),0x7BA4540DL},{(-7L),(-1L),0x7BA4540DL,0x7BA4540DL,(-1L),(-7L)},{1L,(-7L),(-5L),(-1L),(-5L),(-7L)}};
            int32_t *l_294 = &g_100[0];
            int32_t *l_295 = &g_118;
            int32_t *l_296 = &g_100[0];
            int32_t *l_297 = &l_259;
            int32_t *l_298 = &g_118;
            int32_t *l_299 = &l_293[1][3];
            int32_t *l_300 = &g_100[0];
            int32_t *l_301 = &g_100[0];
            int32_t *l_302[6][2];
            uint8_t l_305[8][4][7] = {{{0xCAL,0xAAL,0x77L,0x8CL,0x64L,249UL,251UL},{0xF2L,0x07L,0x2BL,1UL,0x07L,0x05L,0x31L},{6UL,0x07L,0x77L,0xF2L,0x05L,0xA2L,0x8CL},{0x4BL,0x44L,6UL,251UL,6UL,1UL,0x77L}},{{0UL,251UL,249UL,250UL,0x4CL,0UL,0x77L},{3UL,0x8CL,1UL,0x77L,0x77L,1UL,0x8CL},{0x07L,0x2CL,0x07L,0x0FL,0UL,0UL,0x31L},{0xBDL,6UL,255UL,255UL,1UL,0x31L,251UL}},{{0x2CL,0UL,250UL,0x0FL,3UL,251UL,255UL},{0x64L,251UL,255UL,0x77L,0xA3L,250UL,0x4BL},{0x4CL,0xCAL,0x07L,250UL,0UL,0xBDL,0x2BL},{0x4CL,251UL,255UL,0xA3L,0x44L,0xA3L,255UL}},{{0x8CL,0x8CL,0x4CL,3UL,0x31L,0x2CL,251UL},{0x31L,0x07L,0x0FL,1UL,250UL,0xA1L,0x4BL},{251UL,0UL,0xA2L,0UL,0x31L,0x2BL,6UL},{255UL,1UL,0x31L,0x77L,0x44L,1UL,0xBDL}},{{0x4BL,255UL,255UL,0x4CL,0x05L,251UL,0xA1L},{0x2BL,255UL,0x2CL,6UL,0xF2L,0xF2L,6UL},{0xAAL,1UL,0xAAL,0x05L,0x4BL,8UL,0xF2L},{0xBDL,0UL,250UL,0x07L,0xCAL,0x4CL,249UL}},{{3UL,0x07L,0x2BL,0x64L,5UL,8UL,0x0FL},{0x07L,0x8CL,0xA3L,6UL,251UL,0xF2L,0UL},{6UL,251UL,6UL,1UL,0x77L,251UL,0x07L},{1UL,0x07L,6UL,255UL,0x64L,1UL,0x64L}},{{0xA1L,0xA3L,0xA3L,0xA1L,0x0FL,0x2BL,0UL},{0xA3L,5UL,0x2BL,0x8CL,255UL,0xA1L,1UL},{255UL,0xBDL,250UL,255UL,0x8CL,0x2CL,0UL},{0x05L,0x31L,0xAAL,255UL,0xBDL,0xA3L,0x64L}},{{250UL,0UL,0x2CL,1UL,1UL,251UL,0x07L},{0xCAL,0x44L,255UL,1UL,6UL,0UL,0UL},{6UL,255UL,0x31L,255UL,6UL,0x44L,0x0FL},{1UL,255UL,0xA2L,255UL,0UL,1UL,249UL}}};
            int i, j, k;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 2; j++)
                    l_302[i][j] = &l_259;
            }
            --l_305[3][0][5];
        }
        for (l_303 = 0; (l_303 != 14); l_303++)
        { /* block id: 110 */
            uint8_t *l_311 = &g_250[0][0];
            uint8_t **l_310 = &l_311;
            int32_t l_313 = 3L;
            int32_t l_315 = 1L;
            int32_t l_352[7] = {0L,0L,0L,0L,0L,0L,0L};
            int16_t l_360[8][2][2] = {{{0xB41EL,0xB41EL},{0xB41EL,0xC19AL}},{{0x2347L,0x3A5FL},{0xC19AL,0x3A5FL}},{{0x2347L,0xC19AL},{0xB41EL,0xB41EL}},{{0xB41EL,0xC19AL},{0x2347L,0x3A5FL}},{{0xC19AL,0x3A5FL},{0x2347L,0xC19AL}},{{0xB41EL,0xB41EL},{0xB41EL,0xC19AL}},{{0x2347L,0x3A5FL},{0xC19AL,0x3A5FL}},{{0x2347L,0xC19AL},{0xB41EL,0xB41EL}}};
            int8_t l_364 = 0x25L;
            const float **l_370 = (void*)0;
            float **l_373 = (void*)0;
            int i, j, k;
        }
    }
    if (((g_97 = g_40[1].f2) & ((*l_374) = g_64)))
    { /* block id: 137 */
        int32_t *l_375[5];
        int i;
        for (i = 0; i < 5; i++)
            l_375[i] = &l_349;
        --l_380;
        (*g_222) = &l_304;
        (*g_223) &= 0xCE23DE00L;
        for (l_378 = (-22); (l_378 == (-29)); l_378--)
        { /* block id: 143 */
            if (g_118)
                goto lbl_385;
        }
    }
    else
    { /* block id: 146 */
        uint32_t l_397 = 0xC99CDD21L;
        int32_t l_416 = 0L;
        int32_t l_417 = 0L;
        int32_t l_418 = 0xBD7BC4E3L;
        int32_t l_423 = 0xA6D8A958L;
        int32_t l_426 = 0xD55EE3B5L;
        int32_t l_427[4];
        int16_t l_431 = 0x2468L;
        uint8_t *l_450 = (void*)0;
        uint64_t l_451 = 0xF1C5159B95AF5EEDLL;
        int16_t l_563 = 1L;
        float l_564 = 0x0.3p+1;
        int32_t l_565 = 9L;
        int16_t l_567 = 0x208EL;
        int32_t ***l_585[9] = {&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227,&g_227};
        union U1 *l_662 = &g_406;
        int i;
        for (i = 0; i < 4; i++)
            l_427[i] = 0x8B25852AL;
        (**g_222) = l_349;
        if (g_233)
            goto lbl_663;
lbl_663:
        for (l_378 = 29; (l_378 == 20); --l_378)
        { /* block id: 150 */
            uint8_t l_388 = 0x9EL;
            union U1 l_391[8] = {{1L},{1L},{1L},{1L},{1L},{1L},{1L},{1L}};
            uint32_t l_408 = 0xCD2C7F21L;
            int32_t l_415[2][8] = {{0x6760D9B6L,0L,1L,9L,9L,1L,0L,0x6760D9B6L},{0L,0x3B342081L,0x6760D9B6L,1L,0x6760D9B6L,0x3B342081L,0L,0L}};
            float l_474 = 0x0.5729C9p+70;
            int16_t l_475 = (-3L);
            int8_t l_557 = 0xD4L;
            const float l_623 = 0x1.1p-1;
            uint32_t ***l_633 = (void*)0;
            union U1 **l_659 = (void*)0;
            union U1 *l_661[3][1][7] = {{{&l_391[3],&l_391[3],&l_391[2],&l_391[3],&l_391[3],&l_391[2],&l_391[3]}},{{&l_391[3],&l_269,&l_269,&l_391[3],&l_269,&l_269,&l_391[3]}},{{&l_269,&l_391[3],&l_269,&l_269,&l_391[3],&l_269,&l_269}}};
            union U1 **l_660[10] = {(void*)0,&l_661[0][0][4],(void*)0,&l_661[0][0][4],(void*)0,&l_661[0][0][4],(void*)0,&l_661[0][0][4],(void*)0,&l_661[0][0][4]};
            int i, j, k;
        }
        (**g_227) = (l_354 |= ((((safe_unary_minus_func_uint16_t_u((!g_118))) ^ ((safe_rshift_func_int16_t_s_s(((safe_lshift_func_int16_t_s_s(((safe_lshift_func_uint8_t_u_u((((safe_add_func_uint32_t_u_u((*l_647), ((((((safe_sub_func_int64_t_s_s(0x8644C82A2D3C646DLL, ((((*l_647) == (g_676 == &l_451)) > g_64) < (&l_585[7] == (void*)0)))) & 18446744073709551610UL) <= 255UL) , (*g_525)) != (**g_652)) <= g_5))) || 0x18A3L) ^ g_233), (*l_647))) != 6UL), 10)) , 1L), 13)) ^ 0xF22EL)) ^ 0xEE9F21EFL) == (*g_653)));
    }
    return (*l_647);
}


/* ------------------------------------------ */
/* 
 * reads : g_64 g_40.f2 g_65 g_100 g_93 g_40.f1 g_118 g_7 g_135 g_115 g_190 g_223 g_97 g_237 g_250 g_526 g_316 g_2357 g_1105.f1 g_702.f1 g_233
 * writes: g_64 g_78 g_100 g_109 g_118 g_7 g_115 g_181 g_220 g_221 g_226 g_233 g_65 g_97 g_250 g_316 g_1105.f1 g_702.f1
 */
static uint32_t * func_54(uint8_t  p_55, uint32_t * p_56, int64_t * p_57)
{ /* block id: 5 */
    int32_t l_76 = 0L;
    float l_95 = 0xF.F5F9F7p-8;
    int32_t l_98 = 1L;
    union U1 l_126 = {-9L};
    int32_t l_167 = (-1L);
    int32_t *l_180 = (void*)0;
    int32_t l_183 = 0x4BAE2B49L;
    int32_t l_184 = 0xED21ADA0L;
    int32_t l_185 = 0xDACBD30BL;
    int32_t l_186[9] = {0x507861EFL,9L,0x507861EFL,9L,0x507861EFL,9L,0x507861EFL,9L,0x507861EFL};
    int32_t ** const l_218 = &l_180;
    int32_t ** const *l_217[9] = {&l_218,&l_218,&l_218,&l_218,&l_218,&l_218,&l_218,&l_218,&l_218};
    int32_t ** const **l_219[1][10] = {{&l_217[1],&l_217[2],&l_217[1],&l_217[2],&l_217[1],&l_217[2],&l_217[1],&l_217[2],&l_217[1],&l_217[2]}};
    int32_t **l_225 = &g_223;
    int32_t ***l_224[7][4][1] = {{{&l_225},{&l_225},{&l_225},{&l_225}},{{&l_225},{&l_225},{&l_225},{&l_225}},{{&l_225},{&l_225},{&l_225},{&l_225}},{{&l_225},{&l_225},{&l_225},{&l_225}},{{&l_225},{&l_225},{&l_225},{&l_225}},{{&l_225},{&l_225},{&l_225},{&l_225}},{{&l_225},{&l_225},{&l_225},{&l_225}}};
    uint32_t *l_232 = &g_233;
    uint32_t *l_234 = &g_65;
    uint32_t **l_236 = &l_234;
    uint32_t ***l_235 = &l_236;
    float l_249 = (-0x4.Cp+1);
    int i, j, k;
    for (p_55 = 0; (p_55 == 34); ++p_55)
    { /* block id: 8 */
        int16_t l_88 = 0x3A8AL;
        uint64_t *l_92[8][9][1] = {{{&g_64},{&g_93},{&g_93},{&g_64},{&g_93},{&g_93},{&g_93},{&g_64},{&g_93}},{{&g_93},{&g_64},{&g_93},{&g_64},{&g_93},{&g_93},{&g_93},{&g_93},{&g_93}},{{&g_93},{&g_64},{&g_93},{&g_64},{&g_93},{&g_93},{&g_64},{&g_93},{&g_93}},{{&g_93},{&g_64},{&g_93},{&g_93},{&g_64},{&g_93},{&g_64},{&g_93},{&g_93}},{{&g_93},{&g_93},{&g_93},{&g_93},{&g_64},{&g_93},{&g_64},{&g_93},{&g_93}},{{&g_64},{&g_93},{&g_93},{&g_93},{&g_64},{&g_93},{&g_93},{&g_64},{&g_93}},{{&g_64},{&g_93},{&g_93},{&g_93},{&g_93},{&g_93},{&g_93},{&g_64},{&g_93}},{{&g_64},{&g_93},{&g_93},{&g_64},{&g_93},{&g_93},{&g_93},{&g_64},{&g_93}}};
        uint64_t *l_94 = &g_93;
        int32_t l_113[10][10] = {{0L,0x47F2DBD2L,0xA5CA4386L,0x47F2DBD2L,0x0F3F00C7L,0x47F2DBD2L,0x0F3F00C7L,1L,0L,1L},{0x81FA89BFL,0xBF60C7DCL,(-6L),1L,(-6L),0xBF60C7DCL,0x81FA89BFL,0xBF60C7DCL,(-6L),1L},{0xA5CA4386L,1L,0xA5CA4386L,0xBF60C7DCL,0x0F3F00C7L,0xBF60C7DCL,0xA5CA4386L,1L,0xA5CA4386L,0xBF60C7DCL},{0x81FA89BFL,1L,(-1L),1L,0x81FA89BFL,0x47F2DBD2L,0x81FA89BFL,1L,(-1L),1L},{0x0F3F00C7L,0xBF60C7DCL,0xA5CA4386L,1L,0xA5CA4386L,0xBF60C7DCL,0x0F3F00C7L,0xBF60C7DCL,0xA5CA4386L,1L},{(-6L),1L,(-6L),0xBF60C7DCL,0x81FA89BFL,0xBF60C7DCL,(-6L),1L,(-6L),0xBF60C7DCL},{0x0F3F00C7L,1L,0L,1L,0x0F3F00C7L,0x47F2DBD2L,0x0F3F00C7L,1L,0L,1L},{0x81FA89BFL,0xBF60C7DCL,(-6L),1L,(-6L),0xBF60C7DCL,0x81FA89BFL,0xBF60C7DCL,(-6L),1L},{0xA5CA4386L,1L,0xA5CA4386L,0xBF60C7DCL,0x0F3F00C7L,0xBF60C7DCL,0xA5CA4386L,1L,0xA5CA4386L,0xBF60C7DCL},{0x81FA89BFL,1L,(-1L),1L,0x81FA89BFL,0x47F2DBD2L,0x81FA89BFL,1L,(-1L),1L}};
        int32_t l_116[4][8];
        float **l_137 = &g_136;
        int16_t l_170 = (-2L);
        uint16_t l_194[1][10] = {{65527UL,1UL,65527UL,2UL,2UL,65527UL,1UL,65527UL,2UL,2UL}};
        uint32_t *l_197 = &g_40[1].f1;
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 8; j++)
                l_116[i][j] = 6L;
        }
        for (g_64 = 24; (g_64 <= 36); g_64++)
        { /* block id: 11 */
            int8_t *l_77 = &g_78;
            uint64_t *l_91[4] = {&g_64,&g_64,&g_64,&g_64};
            int64_t *l_96[10] = {&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97,&g_97};
            int32_t l_112 = 0L;
            uint8_t *l_114[3];
            int32_t *l_117 = &g_118;
            float *l_122[6] = {&l_95,&l_95,&l_95,&l_95,&l_95,&l_95};
            float **l_121 = &l_122[3];
            float *l_123 = &g_7;
            int32_t **l_129 = (void*)0;
            int32_t **l_130 = &l_117;
            uint32_t *l_133[10][8][3] = {{{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_65,&g_65,&g_65},{&g_65,&g_65,(void*)0},{(void*)0,&g_65,&g_65},{&g_65,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,(void*)0,&g_65},{&g_40[1].f1,(void*)0,(void*)0}},{{(void*)0,&g_40[1].f1,&g_65},{(void*)0,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_65,&g_65},{&g_65,&g_65,&g_40[1].f1},{(void*)0,&g_65,(void*)0},{&g_65,&g_40[1].f1,(void*)0},{&g_65,(void*)0,&g_40[1].f1}},{{&g_40[1].f1,(void*)0,&g_65},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_65,&g_65,&g_65},{&g_65,&g_65,(void*)0},{(void*)0,&g_65,&g_40[1].f1},{&g_65,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1}},{{(void*)0,&g_65,&g_65},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,(void*)0,&g_65},{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_65,(void*)0,(void*)0},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_40[1].f1,&g_40[1].f1}},{{&g_40[1].f1,&g_65,(void*)0},{(void*)0,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_65,&g_65},{(void*)0,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_40[1].f1,&g_65},{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_65,&g_65,&g_40[1].f1}},{{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_65,&g_65},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,(void*)0,&g_65},{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_65,(void*)0,(void*)0},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1}},{{(void*)0,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_65,(void*)0},{(void*)0,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_65,&g_65},{(void*)0,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_40[1].f1,&g_65},{&g_40[1].f1,&g_65,&g_40[1].f1}},{{&g_65,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_65,&g_65},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,(void*)0,&g_65},{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_65,(void*)0,(void*)0}},{{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_65,(void*)0},{(void*)0,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_65,&g_65},{(void*)0,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_40[1].f1,&g_65}},{{&g_40[1].f1,&g_65,&g_40[1].f1},{&g_65,&g_65,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,&g_65,&g_65},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{&g_40[1].f1,&g_40[1].f1,&g_40[1].f1},{(void*)0,(void*)0,&g_65},{&g_40[1].f1,&g_65,&g_40[1].f1}}};
            uint32_t **l_132 = &l_133[7][0][2];
            uint32_t ***l_131 = &l_132;
            int32_t l_144 = 0x962FF4F2L;
            int32_t l_145 = 0x015B29D1L;
            uint32_t l_172 = 0x486E1DBDL;
            uint32_t l_187[9][5] = {{0x5CCF7F12L,0UL,1UL,0x5CCF7F12L,1UL},{0xD449D27BL,0xD449D27BL,4294967294UL,8UL,0UL},{4UL,4294967287UL,1UL,1UL,4294967287UL},{0x0AB4F1E6L,4294967294UL,4294967287UL,0x0AB4F1E6L,4294967295UL},{0UL,1UL,0x5CCF7F12L,1UL,0UL},{4294967287UL,4294967295UL,4294967294UL,4294967295UL,4294967295UL},{0UL,4294967293UL,4294967293UL,0UL,0x6114F9EEL},{0x0AB4F1E6L,1UL,0xD449D27BL,4294967295UL,4294967295UL},{1UL,0UL,1UL,0x6114F9EEL,0UL}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_114[i] = &g_115;
            (*l_117) &= ((safe_rshift_func_int16_t_s_s(p_55, (safe_lshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_u(255UL, 2)), ((*l_77) = l_76))))) >= (l_116[3][2] = ((l_113[7][2] = ((func_79((l_98 = (safe_sub_func_uint64_t_u_u((g_40[1].f2 != ((g_65 & (safe_lshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_s((((l_88 != ((safe_sub_func_int8_t_s_s(g_64, ((l_92[2][3][0] = l_91[2]) != l_94))) < (*p_57))) || g_40[1].f2) <= 0x7F41B6ADL), p_55)), p_55))) , 65535UL)), (*p_57)))), p_55) ^ 0x8CL) && l_112)) > g_40[1].f1)));
            (*l_123) = ((p_55 >= ((safe_mul_func_float_f_f(g_7, (((*l_121) = (void*)0) != l_123))) <= (safe_mul_func_float_f_f(g_65, (p_56 == ((*l_130) = (l_126 , ((safe_div_func_int8_t_s_s((((void*)0 == &l_123) > p_55), l_116[2][6])) , &l_116[3][3])))))))) <= g_100[0]);
            (*l_131) = &p_56;
            if (p_55)
            { /* block id: 27 */
                float ***l_134 = &l_121;
                int32_t l_143 = 0L;
                int32_t l_165 = 0x6E713408L;
                int32_t l_168 = 2L;
                (*l_123) = (((**l_130) = (p_55 > (((*l_134) = &l_123) != (l_137 = g_135)))) , l_98);
                for (l_88 = 0; (l_88 > 10); l_88 = safe_add_func_int8_t_s_s(l_88, 8))
                { /* block id: 34 */
                    int32_t *l_140 = &l_98;
                    int32_t *l_141 = &l_98;
                    int32_t *l_142[6] = {&l_112,&g_100[5],&g_100[5],&l_112,&g_100[5],&g_100[5]};
                    uint16_t l_146 = 0x57B2L;
                    int32_t ***l_153 = &l_129;
                    uint16_t *l_164[4][9] = {{&l_146,&l_146,&l_146,(void*)0,&l_146,&l_146,&l_146,&l_146,&l_146},{(void*)0,&l_146,&l_146,&l_146,(void*)0,&l_146,&l_146,(void*)0,&l_146},{&l_146,&l_146,&l_146,&l_146,&l_146,&l_146,&l_146,&l_146,&l_146},{&l_146,&l_146,&l_146,&l_146,&l_146,&l_146,&l_146,&l_146,&l_146}};
                    int i, j;
                    ++l_146;
                    (**l_130) = (safe_unary_minus_func_uint16_t_u(((+(g_115 = (0x9E8BD095E97E3D8BLL != ((l_116[2][2] && ((((*l_153) = (void*)0) != (void*)0) != ((l_113[7][2] = (safe_lshift_func_uint16_t_u_u(((!(safe_mul_func_uint16_t_u_u((l_143 = (l_98 ^= (safe_mul_func_uint8_t_u_u((((4294967289UL <= 8L) , 0UL) , (safe_add_func_uint32_t_u_u(4294967295UL, ((!p_55) , 4294967295UL)))), 0x21L)))), g_100[1]))) | p_55), 10))) || g_100[1]))) > l_165)))) > g_118)));
                    if ((~0xDC82L))
                    { /* block id: 42 */
                        int16_t l_169 = 0xB22BL;
                        int32_t l_171[4] = {0xECA23E10L,0xECA23E10L,0xECA23E10L,0xECA23E10L};
                        int i;
                        g_118 ^= g_100[3];
                        l_172--;
                    }
                    else
                    { /* block id: 45 */
                        const uint64_t *l_179 = &g_93;
                        (*l_123) = p_55;
                        (*l_140) ^= (((((safe_mul_func_int8_t_s_s((l_113[3][6] | 18446744073709551609UL), (g_115 != ((((safe_lshift_func_uint16_t_u_s((l_179 != (void*)0), 7)) | (((g_93 , (p_55 , &p_56)) != &p_56) , g_93)) , p_55) , g_40[1].f1)))) < (*p_56)) <= 0x8AL) , p_55) > p_55);
                    }
                }
                g_181[1] = (l_180 = ((*l_130) = p_56));
            }
            else
            { /* block id: 53 */
                int32_t *l_182[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_182[i] = &g_100[0];
                l_187[0][1]++;
                l_186[5] = (l_113[7][2] = ((**l_130) ^= ((void*)0 == g_190)));
                --l_194[0][9];
            }
        }
        return l_197;
    }
    if ((((~((((--(*p_56)) | (((((((*l_234) = (((safe_add_func_uint32_t_u_u(g_65, (safe_rshift_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u((((((*l_232) = (g_40[1].f1 >= (safe_rshift_func_int16_t_s_u((p_55 | (safe_rshift_func_uint16_t_u_u((((((safe_lshift_func_int8_t_s_u(((g_221 = (g_220 = l_217[0])) == (g_226[0] = l_224[1][1][0])), 4)) != (&g_136 != (void*)0)) <= (((((safe_mul_func_int16_t_s_s(((safe_sub_func_int64_t_s_s(0x0E939A0984C2C580LL, g_64)) >= p_55), p_55)) && (**l_225)) >= g_40[1].f1) != 0xCEAAE59B509E7058LL) <= g_40[1].f2)) >= 0x37FBEDF6B5423CF2LL) , 65535UL), p_55))), p_55)))) == p_55) ^ 0UL) && p_55), (-2L))) < g_115), g_100[0])))) <= g_93) | p_55)) ^ g_97) >= 1L) < g_118) || 2UL) <= g_97)) , (*p_57)) > 0x0025C80A1DA36639LL)) , l_235) != g_237))
    { /* block id: 69 */
        for (g_97 = 0; (g_97 > 21); g_97 = safe_add_func_int64_t_s_s(g_97, 1))
        { /* block id: 72 */
            (*g_223) = (safe_mod_func_uint32_t_u_u(g_97, 0xA2B921B1L));
        }
    }
    else
    { /* block id: 75 */
        int64_t l_245 = 0x96F8895823D9D43DLL;
        uint8_t l_246 = 255UL;
        --l_246;
        return &g_233;
    }
    g_250[0][0]--;
    return p_56;
}


/* ------------------------------------------ */
/* 
 * reads : g_100 g_93
 * writes: g_100 g_109
 */
static const int8_t  func_79(int64_t  p_80, uint32_t  p_81)
{ /* block id: 15 */
    int32_t *l_99 = &g_100[0];
    int32_t *l_101 = &g_100[0];
    int32_t l_102 = 0xA64B00F5L;
    int32_t *l_103 = (void*)0;
    int32_t l_104[5][1] = {{(-10L)},{8L},{(-10L)},{8L},{(-10L)}};
    uint32_t l_105 = 0x03D53AF4L;
    int32_t **l_108[6][1] = {{&l_101},{&l_103},{&l_101},{&l_101},{&l_103},{&l_101}};
    int i, j;
    (*l_99) |= (65527UL | 1L);
    --l_105;
    g_109[0][4] = (void*)0;
    return g_93;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc_bytes (&g_7, sizeof(g_7), "g_7", print_hash_value);
    transparent_crc(g_30, "g_30", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_40[i].f0, "g_40[i].f0", print_hash_value);
        transparent_crc(g_40[i].f1, "g_40[i].f1", print_hash_value);
        transparent_crc(g_40[i].f2, "g_40[i].f2", print_hash_value);
        transparent_crc(g_40[i].f3, "g_40[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_42, "g_42", print_hash_value);
    transparent_crc(g_60, "g_60", print_hash_value);
    transparent_crc(g_64, "g_64", print_hash_value);
    transparent_crc(g_65, "g_65", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_93, "g_93", print_hash_value);
    transparent_crc(g_97, "g_97", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_100[i], "g_100[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc(g_115, "g_115", print_hash_value);
    transparent_crc(g_118, "g_118", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_193[i], "g_193[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_233, "g_233", print_hash_value);
    transparent_crc(g_240, "g_240", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_250[i][j], "g_250[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_256[i][j], "g_256[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_316, "g_316", print_hash_value);
    transparent_crc(g_336, "g_336", print_hash_value);
    transparent_crc(g_406.f0, "g_406.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_458[i], "g_458[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_487, "g_487", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_526[i], "g_526[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_568, "g_568", print_hash_value);
    transparent_crc(g_677, "g_677", print_hash_value);
    transparent_crc(g_702.f0, "g_702.f0", print_hash_value);
    transparent_crc(g_702.f1, "g_702.f1", print_hash_value);
    transparent_crc(g_702.f2, "g_702.f2", print_hash_value);
    transparent_crc(g_702.f3, "g_702.f3", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_716[i].f0, "g_716[i].f0", print_hash_value);
        transparent_crc(g_716[i].f1, "g_716[i].f1", print_hash_value);
        transparent_crc(g_716[i].f2, "g_716[i].f2", print_hash_value);
        transparent_crc(g_716[i].f3, "g_716[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_729.f0, "g_729.f0", print_hash_value);
    transparent_crc(g_729.f1, "g_729.f1", print_hash_value);
    transparent_crc(g_729.f2, "g_729.f2", print_hash_value);
    transparent_crc(g_729.f3, "g_729.f3", print_hash_value);
    transparent_crc(g_730.f2, "g_730.f2", print_hash_value);
    transparent_crc(g_730.f3, "g_730.f3", print_hash_value);
    transparent_crc(g_866, "g_866", print_hash_value);
    transparent_crc(g_883.f0, "g_883.f0", print_hash_value);
    transparent_crc(g_883.f1, "g_883.f1", print_hash_value);
    transparent_crc(g_883.f2, "g_883.f2", print_hash_value);
    transparent_crc(g_883.f3, "g_883.f3", print_hash_value);
    transparent_crc(g_953, "g_953", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_957[i][j], "g_957[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_988[i], "g_988[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1007, "g_1007", print_hash_value);
    transparent_crc(g_1025, "g_1025", print_hash_value);
    transparent_crc_bytes (&g_1067, sizeof(g_1067), "g_1067", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1104[i].f0, "g_1104[i].f0", print_hash_value);
        transparent_crc(g_1104[i].f1, "g_1104[i].f1", print_hash_value);
        transparent_crc(g_1104[i].f2, "g_1104[i].f2", print_hash_value);
        transparent_crc(g_1104[i].f3, "g_1104[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1105.f0, "g_1105.f0", print_hash_value);
    transparent_crc(g_1105.f1, "g_1105.f1", print_hash_value);
    transparent_crc(g_1105.f2, "g_1105.f2", print_hash_value);
    transparent_crc(g_1105.f3, "g_1105.f3", print_hash_value);
    transparent_crc(g_1181.f0, "g_1181.f0", print_hash_value);
    transparent_crc(g_1181.f1, "g_1181.f1", print_hash_value);
    transparent_crc(g_1181.f2, "g_1181.f2", print_hash_value);
    transparent_crc(g_1181.f3, "g_1181.f3", print_hash_value);
    transparent_crc(g_1215.f2, "g_1215.f2", print_hash_value);
    transparent_crc(g_1215.f3, "g_1215.f3", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1245[i].f0, "g_1245[i].f0", print_hash_value);
        transparent_crc(g_1245[i].f1, "g_1245[i].f1", print_hash_value);
        transparent_crc(g_1245[i].f2, "g_1245[i].f2", print_hash_value);
        transparent_crc(g_1245[i].f3, "g_1245[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1317[i].f0, "g_1317[i].f0", print_hash_value);
        transparent_crc(g_1317[i].f1, "g_1317[i].f1", print_hash_value);
        transparent_crc(g_1317[i].f2, "g_1317[i].f2", print_hash_value);
        transparent_crc(g_1317[i].f3, "g_1317[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1328.f0, "g_1328.f0", print_hash_value);
    transparent_crc(g_1328.f1, "g_1328.f1", print_hash_value);
    transparent_crc(g_1328.f2, "g_1328.f2", print_hash_value);
    transparent_crc(g_1328.f3, "g_1328.f3", print_hash_value);
    transparent_crc(g_1380, "g_1380", print_hash_value);
    transparent_crc(g_1578.f0, "g_1578.f0", print_hash_value);
    transparent_crc(g_1578.f1, "g_1578.f1", print_hash_value);
    transparent_crc(g_1578.f2, "g_1578.f2", print_hash_value);
    transparent_crc(g_1578.f3, "g_1578.f3", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1714[i][j].f0, "g_1714[i][j].f0", print_hash_value);
            transparent_crc(g_1714[i][j].f1, "g_1714[i][j].f1", print_hash_value);
            transparent_crc(g_1714[i][j].f2, "g_1714[i][j].f2", print_hash_value);
            transparent_crc(g_1714[i][j].f3, "g_1714[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1741.f0, "g_1741.f0", print_hash_value);
    transparent_crc(g_1741.f1, "g_1741.f1", print_hash_value);
    transparent_crc(g_1741.f2, "g_1741.f2", print_hash_value);
    transparent_crc(g_1741.f3, "g_1741.f3", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1759[i], "g_1759[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1841, "g_1841", print_hash_value);
    transparent_crc(g_2075, "g_2075", print_hash_value);
    transparent_crc(g_2207, "g_2207", print_hash_value);
    transparent_crc_bytes (&g_2298, sizeof(g_2298), "g_2298", print_hash_value);
    transparent_crc(g_2342, "g_2342", print_hash_value);
    transparent_crc(g_2357, "g_2357", print_hash_value);
    transparent_crc(g_2518, "g_2518", print_hash_value);
    transparent_crc(g_2559.f0, "g_2559.f0", print_hash_value);
    transparent_crc(g_2559.f1, "g_2559.f1", print_hash_value);
    transparent_crc(g_2559.f2, "g_2559.f2", print_hash_value);
    transparent_crc(g_2559.f3, "g_2559.f3", print_hash_value);
    transparent_crc(g_2723.f0, "g_2723.f0", print_hash_value);
    transparent_crc(g_2723.f1, "g_2723.f1", print_hash_value);
    transparent_crc(g_2723.f2, "g_2723.f2", print_hash_value);
    transparent_crc(g_2723.f3, "g_2723.f3", print_hash_value);
    transparent_crc(g_2800.f0, "g_2800.f0", print_hash_value);
    transparent_crc(g_2800.f1, "g_2800.f1", print_hash_value);
    transparent_crc(g_2800.f2, "g_2800.f2", print_hash_value);
    transparent_crc(g_2800.f3, "g_2800.f3", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2933[i], "g_2933[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2958[i], "g_2958[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_2992[i][j].f0, "g_2992[i][j].f0", print_hash_value);
            transparent_crc(g_2992[i][j].f1, "g_2992[i][j].f1", print_hash_value);
            transparent_crc(g_2992[i][j].f2, "g_2992[i][j].f2", print_hash_value);
            transparent_crc(g_2992[i][j].f3, "g_2992[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3047.f0, "g_3047.f0", print_hash_value);
    transparent_crc(g_3047.f1, "g_3047.f1", print_hash_value);
    transparent_crc(g_3047.f2, "g_3047.f2", print_hash_value);
    transparent_crc(g_3047.f3, "g_3047.f3", print_hash_value);
    transparent_crc(g_3104, "g_3104", print_hash_value);
    transparent_crc(g_3161, "g_3161", print_hash_value);
    transparent_crc(g_3220.f0, "g_3220.f0", print_hash_value);
    transparent_crc(g_3220.f1, "g_3220.f1", print_hash_value);
    transparent_crc(g_3220.f2, "g_3220.f2", print_hash_value);
    transparent_crc(g_3220.f3, "g_3220.f3", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 906
XXX total union variables: 45

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 218
   depth: 2, occurrence: 46
   depth: 3, occurrence: 5
   depth: 4, occurrence: 4
   depth: 7, occurrence: 1
   depth: 9, occurrence: 2
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 3
   depth: 19, occurrence: 3
   depth: 20, occurrence: 5
   depth: 21, occurrence: 2
   depth: 22, occurrence: 3
   depth: 23, occurrence: 5
   depth: 24, occurrence: 3
   depth: 25, occurrence: 4
   depth: 26, occurrence: 2
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 35, occurrence: 1
   depth: 37, occurrence: 1
   depth: 44, occurrence: 1
   depth: 52, occurrence: 1

XXX total number of pointers: 710

XXX times a variable address is taken: 1762
XXX times a pointer is dereferenced on RHS: 464
breakdown:
   depth: 1, occurrence: 375
   depth: 2, occurrence: 60
   depth: 3, occurrence: 24
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 395
breakdown:
   depth: 1, occurrence: 307
   depth: 2, occurrence: 62
   depth: 3, occurrence: 24
   depth: 4, occurrence: 1
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 76
XXX times a pointer is compared with address of another variable: 24
XXX times a pointer is compared with another pointer: 24
XXX times a pointer is qualified to be dereferenced: 13494

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 3466
   level: 2, occurrence: 872
   level: 3, occurrence: 380
   level: 4, occurrence: 141
   level: 5, occurrence: 52
XXX number of pointers point to pointers: 325
XXX number of pointers point to scalars: 357
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31
XXX average alias set size: 1.51

XXX times a non-volatile is read: 2730
XXX times a non-volatile is write: 1248
XXX times a volatile is read: 189
XXX    times read thru a pointer: 114
XXX times a volatile is write: 68
XXX    times written thru a pointer: 49
XXX times a volatile is available for access: 6.12e+03
XXX percentage of non-volatile access: 93.9

XXX forward jumps: 1
XXX backward jumps: 8

XXX stmts: 211
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 37
   depth: 1, occurrence: 31
   depth: 2, occurrence: 24
   depth: 3, occurrence: 42
   depth: 4, occurrence: 42
   depth: 5, occurrence: 35

XXX percentage a fresh-made variable is used: 15.7
XXX percentage an existing variable is used: 84.3
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      44797543
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_3 = 0x8D4081C2L;/* VOLATILE GLOBAL g_3 */
static volatile int32_t * const  volatile g_2 = &g_3;/* VOLATILE GLOBAL g_2 */
static volatile int32_t * volatile g_5 = &g_3;/* VOLATILE GLOBAL g_5 */
static int32_t g_6 = 0x62751C76L;
static int32_t g_9 = 0x8E1B61BAL;
static volatile int32_t g_12 = 0x434678DCL;/* VOLATILE GLOBAL g_12 */
static int32_t g_13 = 0x2A10AA43L;
static int8_t g_30[7][8] = {{1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L,1L,1L,1L}};
static int32_t g_44 = 0x618DE0F4L;
static int32_t g_46 = 0x070FEEA3L;
static int32_t *g_84 = &g_44;
static uint16_t g_87 = 65529UL;
static uint16_t g_92 = 65534UL;
static int32_t g_104 = (-1L);
static int16_t g_105 = 8L;
static uint64_t g_106[5][7][7] = {{{0xF3D1A1A39BFC11C9LL,0x86B8A71A37F4EAFELL,18446744073709551610UL,0UL,0x5DDCF39A3708275CLL,3UL,1UL},{0UL,1UL,0x1CD28735F4C61C4CLL,0x25D181DD90CDDE2BLL,0x27BBAC67CE96EA65LL,18446744073709551615UL,0xEDF41299D89CF69DLL},{0UL,18446744073709551612UL,0UL,2UL,0UL,0UL,2UL},{1UL,1UL,1UL,0x197363D2F63B645CLL,0xFA6E79906C8188D8LL,0UL,0x5B8C608F1A9DB3BFLL},{0x60B5F8E08A2E4B19LL,0UL,0xB3148BB49DA01FE0LL,1UL,0xDAE866A738CF3A2ALL,1UL,0xF01ADD9D0C5B6348LL},{0UL,0x25D181DD90CDDE2BLL,18446744073709551613UL,0x7BD3F66FFE49794ELL,18446744073709551612UL,0UL,1UL},{0UL,0x5B8C608F1A9DB3BFLL,0x7BD3F66FFE49794ELL,1UL,0x1CD28735F4C61C4CLL,0UL,0xDAE866A738CF3A2ALL}},{{0x4EABE0F6012C46B2LL,0xDAE866A738CF3A2ALL,0xA31C558FEE062075LL,0UL,0xBF0D081C3794E43DLL,18446744073709551615UL,0x59640E1577BE735BLL},{0UL,18446744073709551609UL,0x25D181DD90CDDE2BLL,0x4EBAA9A2C06FE3A5LL,18446744073709551615UL,3UL,0xDEF93A2F2406E9E1LL},{18446744073709551609UL,0xEDF41299D89CF69DLL,0xDEF93A2F2406E9E1LL,18446744073709551608UL,0x4854C362A02F9331LL,0x59640E1577BE735BLL,0UL},{0xBB5DFEAAC53A0744LL,0x4854C362A02F9331LL,0xDEF93A2F2406E9E1LL,0UL,1UL,0x4EABE0F6012C46B2LL,8UL},{0xAF0C93A8B8E48459LL,0x79D6528A1895B12CLL,0x25D181DD90CDDE2BLL,0UL,3UL,2UL,18446744073709551609UL},{0x4854C362A02F9331LL,1UL,0xA31C558FEE062075LL,0xDEF93A2F2406E9E1LL,18446744073709551615UL,2UL,18446744073709551615UL},{1UL,0x7BD3F66FFE49794ELL,0x7BD3F66FFE49794ELL,1UL,0x4EBAA9A2C06FE3A5LL,18446744073709551615UL,0xA06E20FE2C0A09E9LL}},{{0UL,0UL,18446744073709551613UL,1UL,18446744073709551609UL,1UL,0x5DDCF39A3708275CLL},{0x59640E1577BE735BLL,0x60B5F8E08A2E4B19LL,0xB3148BB49DA01FE0LL,0x5DDCF39A3708275CLL,0x5B8C608F1A9DB3BFLL,0xAF0C93A8B8E48459LL,0xA06E20FE2C0A09E9LL},{0UL,0x7A36CEB12FA99DB6LL,1UL,18446744073709551609UL,1UL,0UL,18446744073709551615UL},{0x27BBAC67CE96EA65LL,0UL,0UL,0xE00E1E3E4B84AAFBLL,0x7A36CEB12FA99DB6LL,0UL,18446744073709551609UL},{0x79D6528A1895B12CLL,0xB3148BB49DA01FE0LL,0x1CD28735F4C61C4CLL,1UL,0x2C01D27E8BA164AALL,1UL,8UL},{18446744073709551613UL,18446744073709551615UL,18446744073709551610UL,0UL,0UL,0xDEF93A2F2406E9E1LL,0UL},{0xFA6E79906C8188D8LL,2UL,18446744073709551615UL,0UL,0xA06E20FE2C0A09E9LL,0x7A36CEB12FA99DB6LL,0xDEF93A2F2406E9E1LL}},{{0xDEF93A2F2406E9E1LL,0xC678F9D625AE4031LL,0UL,1UL,1UL,0xF01ADD9D0C5B6348LL,0x59640E1577BE735BLL},{0xF01ADD9D0C5B6348LL,2UL,0x5B8C608F1A9DB3BFLL,0xE00E1E3E4B84AAFBLL,0x25D181DD90CDDE2BLL,0xDAE866A738CF3A2ALL,0xDAE866A738CF3A2ALL},{1UL,18446744073709551609UL,0UL,18446744073709551609UL,1UL,0UL,1UL},{0UL,3UL,18446744073709551615UL,0x5DDCF39A3708275CLL,0x6138F517126B21CELL,0x79D6528A1895B12CLL,0xF01ADD9D0C5B6348LL},{1UL,18446744073709551608UL,18446744073709551609UL,1UL,2UL,0x197363D2F63B645CLL,2UL},{0xC678F9D625AE4031LL,18446744073709551614UL,0x4854C362A02F9331LL,0xFA6E79906C8188D8LL,2UL,18446744073709551615UL,0UL},{0x4854C362A02F9331LL,0xF3D1A1A39BFC11C9LL,1UL,18446744073709551607UL,0x8AB48F0B38F1CEF1LL,0x86B8A71A37F4EAFELL,0x5B8C608F1A9DB3BFLL}},{{0UL,0xBF0D081C3794E43DLL,0x70527DF68D3435D6LL,18446744073709551615UL,1UL,18446744073709551610UL,0xF3D1A1A39BFC11C9LL},{18446744073709551607UL,0x59640E1577BE735BLL,0x27BBAC67CE96EA65LL,1UL,0UL,18446744073709551615UL,0x60B5F8E08A2E4B19LL},{0x4FAC92380C53E4ABLL,0x25D181DD90CDDE2BLL,1UL,0xA31C558FEE062075LL,0UL,0x27BBAC67CE96EA65LL,0xB3148BB49DA01FE0LL},{0x86B8A71A37F4EAFELL,0xF01ADD9D0C5B6348LL,1UL,0xDAE866A738CF3A2ALL,1UL,0xB3148BB49DA01FE0LL,0UL},{0UL,1UL,0x4FAC92380C53E4ABLL,2UL,0x8AB48F0B38F1CEF1LL,0x5B8C608F1A9DB3BFLL,18446744073709551609UL},{0xDEF93A2F2406E9E1LL,2UL,0x2C01D27E8BA164AALL,0x2C01D27E8BA164AALL,2UL,0xDEF93A2F2406E9E1LL,0xF01ADD9D0C5B6348LL},{1UL,18446744073709551612UL,1UL,0x5DDCF39A3708275CLL,0UL,1UL,0x4FAC92380C53E4ABLL}}};
static int8_t *g_111[8] = {&g_30[4][6],&g_30[4][6],&g_30[4][6],&g_30[4][6],&g_30[4][6],&g_30[4][6],&g_30[4][6],&g_30[4][6]};
static int32_t **g_113[8][5] = {{(void*)0,&g_84,(void*)0,&g_84,(void*)0},{&g_84,&g_84,&g_84,&g_84,&g_84},{&g_84,&g_84,&g_84,&g_84,&g_84},{&g_84,&g_84,&g_84,&g_84,&g_84},{(void*)0,&g_84,(void*)0,&g_84,(void*)0},{&g_84,&g_84,(void*)0,&g_84,&g_84},{&g_84,&g_84,&g_84,&g_84,&g_84},{&g_84,&g_84,(void*)0,&g_84,&g_84}};
static int32_t ***g_132 = &g_113[7][3];
static int64_t g_142 = 0x4615860905539D5BLL;
static uint32_t g_160[10] = {4294967291UL,4294967291UL,2UL,4294967291UL,4294967291UL,2UL,4294967291UL,4294967291UL,2UL,4294967291UL};
static uint32_t g_162 = 0xD9216BE7L;
static uint8_t g_190 = 250UL;
static uint64_t g_192 = 0x666B455545A2E4B3LL;
static float g_203 = 0x2.Bp-1;
static float g_213 = 0x3.7p+1;
static int8_t g_266 = 0x9DL;
static float g_269 = (-0x1.Fp+1);
static int64_t g_304[3][8] = {{0L,0L,0L,0x9CC4C8BBC05D2A46LL,0L,0L,0L,0L},{1L,0L,0L,1L,0x870BC3DA878EA642LL,1L,0L,0L},{0L,0x870BC3DA878EA642LL,0x9CC4C8BBC05D2A46LL,0x9CC4C8BBC05D2A46LL,0x870BC3DA878EA642LL,0L,0x870BC3DA878EA642LL,0x9CC4C8BBC05D2A46LL}};
static int64_t *g_303 = &g_304[1][2];
static float *g_427 = &g_269;
static float **g_426[1][5][9] = {{{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427},{&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427,&g_427}}};
static uint16_t g_459 = 0x0563L;
static int16_t g_467 = 0x79F6L;
static int32_t g_468 = 0xA251597FL;
static uint32_t *g_566 = &g_160[3];
static uint16_t *g_578 = &g_459;
static uint8_t g_617 = 1UL;
static int32_t *g_694 = &g_13;
static uint8_t g_751 = 0xA5L;
static int32_t *g_771 = &g_44;
static int32_t *g_798 = &g_44;
static int32_t *g_802[9] = {&g_9,&g_13,&g_13,&g_9,&g_13,&g_13,&g_9,&g_13,&g_13};
static int32_t g_855 = 0x8C62B81BL;
static uint32_t g_898[8] = {8UL,8UL,8UL,8UL,8UL,8UL,8UL,8UL};
static uint64_t *g_980 = &g_106[3][0][3];
static uint64_t **g_979 = &g_980;
static uint8_t *g_1078 = &g_751;
static uint8_t **g_1077 = &g_1078;
static uint32_t g_1109 = 4294967295UL;
static uint32_t g_1120 = 6UL;
static volatile int64_t ** volatile *g_1151 = (void*)0;
static float g_1185 = 0x8.0p+1;
static int32_t g_1365 = 0x90B47CB2L;
static int32_t g_1396[5][1][6] = {{{1L,1L,0x08C88F8AL,1L,1L,0x08C88F8AL}},{{1L,1L,0x08C88F8AL,1L,1L,0x08C88F8AL}},{{1L,1L,0x08C88F8AL,1L,1L,0x08C88F8AL}},{{1L,1L,0x08C88F8AL,1L,1L,0x08C88F8AL}},{{1L,1L,0x08C88F8AL,1L,1L,0x08C88F8AL}}};
static const int8_t *g_1478 = &g_30[6][0];
static const int8_t **g_1477[9][2][7] = {{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}},{{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0},{&g_1478,&g_1478,&g_1478,&g_1478,&g_1478,(void*)0,(void*)0}}};
static uint32_t g_1499 = 0x848F9B77L;
static uint8_t * volatile *g_1532 = &g_1078;
static uint8_t * volatile * volatile * volatile g_1531 = &g_1532;/* VOLATILE GLOBAL g_1531 */
static uint8_t * volatile * volatile * volatile *g_1530[2] = {&g_1531,&g_1531};
static volatile int64_t * volatile g_1539 = (void*)0;/* VOLATILE GLOBAL g_1539 */
static volatile int64_t * volatile *g_1538 = &g_1539;
static volatile int64_t * volatile **g_1537 = &g_1538;
static uint64_t *g_1554 = &g_106[3][0][3];
static uint32_t **g_1582 = &g_566;
static uint32_t ***g_1581 = &g_1582;
static volatile float g_1704[6][9][4] = {{{(-0x1.8p+1),0x2.Fp-1,(-0x1.8p+1),0x1.4p+1},{0x8.993213p-65,(-0x1.7p-1),0x1.4p+1,0x6.1p+1},{0x1.3p-1,0x2.4FBAD3p+63,0x1.B7E712p-34,(-0x1.7p-1)},{(-0x1.7p-1),0x3.7p-1,0x1.B7E712p-34,0x4.21CB98p+99},{0x1.6F5FA8p-99,(-0x1.7p-1),(-0x9.2p+1),0x2.Fp-1},{0x7.B2125Ep+52,(-0x1.7p-1),0x1.E19FF0p+95,(-0x1.8p+1)},{0x1.E19FF0p+95,(-0x1.8p+1),0x1.Ep+1,(-0x1.5p-1)},{(-0x1.7p-1),0x1.Ep+1,(-0x6.Cp+1),(-0x9.2p+1)},{(-0x1.8p+1),(-0x5.Ep+1),(-0x9.3p-1),(-0x9.3p-1)}},{{0x1.6F5FA8p-99,0x1.6F5FA8p-99,0x4.9E1B59p+6,(-0x2.6p-1)},{(-0x1.5p-1),0x1.8p-1,(-0x6.7p+1),(-0x1.8p+1)},{(-0x1.2p-1),(-0x1.2p-1),(-0x9.2p+1),(-0x6.7p+1)},{0xA.4FAC0Ap-74,(-0x1.2p-1),(-0x6.Cp+1),(-0x1.8p+1)},{(-0x1.2p-1),0x1.8p-1,0x7.4DAD58p-31,(-0x2.6p-1)},{(-0x1.7p-1),0x1.6F5FA8p-99,(-0x1.2p-1),(-0x9.3p-1)},{0x7.B2125Ep+52,(-0x5.Ep+1),0x1.B7E712p-34,(-0x9.2p+1)},{(-0x1.2p-1),0x1.Ep+1,0x4.9E1B59p+6,(-0x1.5p-1)},{0x1.4p+1,(-0x1.8p+1),0x2.Fp-1,(-0x1.8p+1)}},{{0x1.Ep+1,(-0x1.7p-1),(-0x9.3p-1),0x2.Fp-1},{0xA.4FAC0Ap-74,(-0x1.7p-1),(-0x1.2p-1),0x4.21CB98p+99},{0x1.E19FF0p+95,0x1.8p-1,(-0x1.2p-1),0x6.1p+1},{0x1.E19FF0p+95,0x1.Ep+1,(-0x1.2p-1),0x1.B7E712p-34},{0xA.4FAC0Ap-74,0x6.1p+1,(-0x9.3p-1),(-0x9.2p+1)},{0x1.Ep+1,(-0x1.3p+1),0x2.Fp-1,(-0x2.6p-1)},{0x1.4p+1,0xA.4FAC0Ap-74,0x4.9E1B59p+6,0x4.21CB98p+99},{(-0x1.2p-1),(-0x1.7p-1),0x1.B7E712p-34,0x4.9E1B59p+6},{0x7.B2125Ep+52,(-0x1.2p-1),(-0x1.2p-1),0x7.B2125Ep+52}},{{(-0x1.7p-1),(-0x1.8p+1),0x7.4DAD58p-31,0x6.1p+1},{(-0x1.2p-1),(-0x1.3p+1),(-0x6.Cp+1),(-0x9.3p-1)},{0xA.4FAC0Ap-74,0x1.4p+1,(-0x9.2p+1),(-0x9.3p-1)},{(-0x1.2p-1),(-0x1.3p+1),(-0x6.7p+1),0x6.1p+1},{(-0x1.5p-1),(-0x1.8p+1),0x4.9E1B59p+6,0x7.B2125Ep+52},{0x1.6F5FA8p-99,(-0x1.2p-1),(-0x9.3p-1),0x4.9E1B59p+6},{(-0x1.8p+1),(-0x1.7p-1),(-0x6.Cp+1),0x4.21CB98p+99},{(-0x1.7p-1),0xA.4FAC0Ap-74,0x1.Ep+1,(-0x2.6p-1)},{0x1.E19FF0p+95,(-0x1.3p+1),0x1.E19FF0p+95,(-0x9.2p+1)}},{{0x7.B2125Ep+52,0x6.1p+1,(-0x9.2p+1),0x1.B7E712p-34},{0x1.6F5FA8p-99,0x1.Ep+1,0x2.Fp-1,0x6.1p+1},{0x6.1p+1,0x1.8p-1,0x2.Fp-1,0x4.21CB98p+99},{0x1.6F5FA8p-99,(-0x1.7p-1),(-0x9.2p+1),0x2.Fp-1},{0x7.B2125Ep+52,(-0x1.7p-1),0x1.E19FF0p+95,(-0x1.8p+1)},{0x1.E19FF0p+95,(-0x1.8p+1),0x1.Ep+1,(-0x1.5p-1)},{(-0x1.7p-1),0x1.Ep+1,(-0x6.Cp+1),(-0x9.2p+1)},{(-0x1.8p+1),(-0x5.Ep+1),(-0x9.3p-1),(-0x9.3p-1)},{0x1.6F5FA8p-99,0x1.6F5FA8p-99,0x4.9E1B59p+6,(-0x2.6p-1)}},{{(-0x1.5p-1),0x1.8p-1,(-0x6.7p+1),(-0x1.8p+1)},{(-0x1.2p-1),(-0x1.2p-1),(-0x9.2p+1),(-0x6.7p+1)},{0xA.4FAC0Ap-74,(-0x1.2p-1),(-0x6.Cp+1),(-0x1.8p+1)},{(-0x1.2p-1),0x1.8p-1,0x7.4DAD58p-31,(-0x2.6p-1)},{(-0x1.7p-1),0x1.6F5FA8p-99,(-0x1.2p-1),(-0x9.3p-1)},{0x7.B2125Ep+52,(-0x5.Ep+1),0x1.B7E712p-34,(-0x9.2p+1)},{(-0x1.2p-1),0x1.Ep+1,(-0x1.2p-1),(-0x9.3p-1)},{(-0x9.2p+1),0x1.E19FF0p+95,(-0x1.3p+1),0x1.E19FF0p+95},{0x9.A2184Ap+55,0x1.4p+1,0x1.3p-1,(-0x1.3p+1)}}};
static volatile float * volatile g_1703 = &g_1704[0][7][3];/* VOLATILE GLOBAL g_1703 */
static volatile float * volatile *g_1702 = &g_1703;
static volatile float * volatile ** volatile g_1701 = &g_1702;/* VOLATILE GLOBAL g_1701 */
static volatile float * volatile ** volatile *g_1700 = &g_1701;
static uint16_t * volatile * volatile *g_1824 = (void*)0;
static uint8_t g_1832[7] = {253UL,253UL,253UL,253UL,253UL,253UL,253UL};
static int32_t *g_1836 = &g_468;
static int32_t **g_1835[3][3][4] = {{{&g_1836,&g_1836,&g_1836,&g_1836},{&g_1836,&g_1836,&g_1836,&g_1836},{&g_1836,&g_1836,&g_1836,&g_1836}},{{&g_1836,&g_1836,&g_1836,&g_1836},{&g_1836,&g_1836,&g_1836,&g_1836},{&g_1836,&g_1836,&g_1836,&g_1836}},{{&g_1836,&g_1836,&g_1836,&g_1836},{&g_1836,&g_1836,&g_1836,&g_1836},{&g_1836,&g_1836,&g_1836,&g_1836}}};
static int32_t **g_1837[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t g_1896 = 0x2A13295DL;
static uint32_t *g_1926 = &g_898[3];
static int16_t g_1968 = 0x6FA4L;
static int16_t *g_2028 = &g_1968;
static float ***g_2122 = &g_426[0][3][0];
static const uint32_t *g_2125 = (void*)0;
static const uint32_t **g_2124 = &g_2125;
static int32_t g_2144 = 1L;
static uint64_t g_2290 = 1UL;
static uint32_t g_2380[5][1][4] = {{{0UL,0UL,0UL,0UL}},{{0UL,0UL,0UL,0UL}},{{0UL,0UL,0UL,0UL}},{{0UL,0UL,0UL,0UL}},{{0UL,0UL,0UL,0UL}}};
static uint32_t **g_2635 = (void*)0;
static int8_t g_2696[6] = {8L,8L,8L,8L,8L,8L};
static const uint16_t g_2705 = 0xB799L;
static int8_t g_2711 = 1L;
static uint8_t g_2791 = 0xD1L;
static uint8_t g_2817[3][8] = {{255UL,255UL,0x34L,0x34L,255UL,255UL,0x34L,0x34L},{255UL,255UL,0x34L,0x34L,255UL,255UL,0x34L,0x34L},{255UL,255UL,0x34L,0x34L,255UL,255UL,0x34L,0x34L}};
static const int16_t g_2848[7][2] = {{0L,0L},{(-1L),0L},{0L,(-1L)},{0L,0L},{(-1L),0L},{0L,(-1L)},{0L,0L}};
static uint32_t g_2849 = 0x2413C0FBL;
static uint32_t g_2927[1] = {0xD5B8B081L};
static volatile int64_t ** volatile *** volatile g_3128 = (void*)0;/* VOLATILE GLOBAL g_3128 */
static volatile int64_t ** volatile *** volatile g_3129[3] = {(void*)0,(void*)0,(void*)0};
static volatile int64_t ** volatile **g_3131 = (void*)0;
static volatile int64_t ** volatile *** volatile g_3130[5][4][4] = {{{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131},{(void*)0,&g_3131,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131}},{{&g_3131,&g_3131,&g_3131,(void*)0},{&g_3131,&g_3131,&g_3131,&g_3131},{(void*)0,(void*)0,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131}},{{&g_3131,&g_3131,(void*)0,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,(void*)0,&g_3131,&g_3131}},{{&g_3131,&g_3131,&g_3131,(void*)0},{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,(void*)0,(void*)0,&g_3131}},{{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131},{&g_3131,&g_3131,&g_3131,&g_3131}}};
static const uint8_t g_3168 = 255UL;
static const uint32_t g_3199 = 18446744073709551615UL;
static const uint32_t *g_3198 = &g_3199;
static const uint32_t **g_3197[5][8] = {{&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_3198,&g_3198,(void*)0,&g_3198,(void*)0},{&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,(void*)0},{&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,&g_3198,&g_3198},{(void*)0,&g_3198,&g_3198,(void*)0,(void*)0,(void*)0,&g_3198,&g_3198}};
static volatile uint32_t g_3252 = 18446744073709551615UL;/* VOLATILE GLOBAL g_3252 */
static int32_t ** volatile g_3260 = &g_694;/* VOLATILE GLOBAL g_3260 */
static uint64_t ***g_3264[8] = {&g_979,&g_979,&g_979,&g_979,&g_979,&g_979,&g_979,&g_979};
static uint64_t ****g_3263[5] = {&g_3264[0],&g_3264[0],&g_3264[0],&g_3264[0],&g_3264[0]};
static int32_t ** volatile g_3297 = (void*)0;/* VOLATILE GLOBAL g_3297 */
static int32_t ** volatile g_3403[8] = {&g_802[7],(void*)0,(void*)0,&g_802[7],(void*)0,(void*)0,&g_802[7],(void*)0};
static int32_t ** volatile g_3404 = &g_84;/* VOLATILE GLOBAL g_3404 */
static int64_t *****g_3416 = (void*)0;
static int32_t ** const  volatile g_3448[2] = {&g_771,&g_771};
static int32_t ** const  volatile g_3553 = &g_771;/* VOLATILE GLOBAL g_3553 */
static int32_t ** volatile g_3567 = &g_84;/* VOLATILE GLOBAL g_3567 */
static const int32_t *g_3596 = (void*)0;


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static uint8_t  func_15(int64_t  p_16, uint32_t  p_17);
static uint8_t  func_18(uint32_t  p_19);
static uint32_t  func_20(uint16_t  p_21);
static int16_t  func_32(float  p_33, uint8_t  p_34, uint32_t  p_35, int8_t * p_36, int32_t  p_37);
static float  func_38(const uint32_t  p_39, int8_t  p_40, int64_t  p_41);
static const uint32_t  func_47(int32_t * p_48, int32_t ** p_49, uint64_t  p_50, int32_t ** p_51, int32_t * p_52);
static int32_t * func_53(const int64_t  p_54);
static const int64_t  func_55(int8_t * const  p_56, int32_t * p_57, int32_t ** p_58, const uint32_t  p_59, int32_t * const  p_60);
static int32_t  func_76(int32_t ** p_77, int8_t * p_78);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_6 g_9 g_13 g_1478 g_30
 * writes: g_5 g_6 g_9 g_13
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    volatile int32_t * volatile *l_4[5] = {&g_5,&g_5,&g_5,&g_5,&g_5};
    int32_t *l_3554[3];
    int64_t **l_3563[4][3] = {{&g_303,&g_303,&g_303},{(void*)0,&g_303,&g_303},{(void*)0,(void*)0,&g_303},{&g_303,&g_303,&g_303}};
    int64_t ** const *l_3562 = &l_3563[3][1];
    int64_t ** const **l_3561[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int64_t ** const ** const *l_3560 = &l_3561[0];
    uint32_t l_3577 = 0xD80C2C9FL;
    int16_t l_3593 = 0xB84BL;
    int i, j;
    for (i = 0; i < 3; i++)
        l_3554[i] = (void*)0;
    g_5 = g_2;
lbl_3602:
    for (g_6 = 0; (g_6 != (-23)); --g_6)
    { /* block id: 4 */
        int32_t l_3050 = 0x1BB00509L;
        int32_t l_3571 = (-4L);
        int32_t l_3572[10][1] = {{0L},{0x1E7C5AC5L},{0x1E7C5AC5L},{0L},{0x1E7C5AC5L},{0x1E7C5AC5L},{0L},{0x1E7C5AC5L},{0x1E7C5AC5L},{0L}};
        int i, j;
        for (g_9 = 27; (g_9 >= 3); g_9 = safe_sub_func_int32_t_s_s(g_9, 4))
        { /* block id: 7 */
            uint32_t l_14 = 4294967286UL;
            int64_t ** const ** const l_3559 = (void*)0;
            int64_t ** const ** const *l_3558 = &l_3559;
            uint32_t l_3568 = 0UL;
            int32_t l_3573 = 0x67378BEEL;
            int32_t l_3575 = 0L;
            uint8_t l_3601 = 249UL;
            for (g_13 = 1; (g_13 <= 4); g_13 += 1)
            { /* block id: 10 */
                uint32_t *l_3555 = (void*)0;
                uint32_t *l_3556 = &g_1109;
                int32_t l_3557 = 1L;
                int32_t l_3574 = 1L;
                int32_t l_3576 = 0x840D1541L;
                int32_t **l_3585 = &g_802[7];
                int32_t ***l_3584 = &l_3585;
                int8_t *l_3592 = &g_30[6][0];
                const int32_t **l_3595[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_3595[i] = (void*)0;
            }
            if (g_13)
                goto lbl_3602;
        }
    }
    return (*g_1478);
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_3 g_578 g_459 g_1703 g_1704 g_1531 g_1532 g_1078 g_751 g_1701 g_1702 g_427 g_2290 g_1120 g_1478 g_30 g_1554 g_106 g_1396 g_467 g_1832 g_771 g_44 g_192 g_617 g_2711 g_798 g_13 g_46 g_979 g_980 g_84 g_3553
 * writes: g_269 g_1704 g_459 g_2290 g_1120 g_1396 g_467 g_192 g_44 g_3131 g_617 g_2711 g_798 g_46 g_106 g_771
 */
static uint8_t  func_15(int64_t  p_16, uint32_t  p_17)
{ /* block id: 1293 */
    int32_t l_3062[8] = {0x217CD8A7L,0x217CD8A7L,0x217CD8A7L,0x217CD8A7L,0x217CD8A7L,0x217CD8A7L,0x217CD8A7L,0x217CD8A7L};
    int8_t *l_3071 = &g_266;
    int8_t *l_3072 = (void*)0;
    int32_t l_3077 = (-9L);
    const int16_t l_3079[8] = {0x242EL,1L,0x242EL,0x242EL,1L,0x242EL,0x242EL,1L};
    int32_t l_3082[10][10] = {{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L},{0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L,0xCD91C2DEL,0L}};
    int8_t l_3103[7][1][9] = {{{0x44L,0L,0x44L,(-10L),0xC2L,(-10L),0x44L,0L,0x44L}},{{0x0CL,(-4L),0x0CL,0x0CL,(-4L),0x0CL,0x0CL,(-4L),0x0CL}},{{0x44L,0L,0x44L,(-10L),0xC2L,(-10L),0x44L,0L,0x44L}},{{0x0CL,(-4L),0x0CL,0x0CL,(-4L),0x0CL,0x0CL,(-4L),0x0CL}},{{0x44L,0L,0x44L,(-10L),0xC2L,(-10L),0x44L,0L,0x44L}},{{0x0CL,(-4L),0x0CL,0x0CL,(-4L),0x0CL,0x0CL,(-4L),0x0CL}},{{0x44L,0L,0x44L,(-10L),0xC2L,(-10L),0x44L,0L,0x44L}}};
    uint32_t l_3159 = 0x6A6459B2L;
    uint16_t **l_3317 = &g_578;
    uint16_t ***l_3316[1];
    uint32_t l_3334[1];
    uint32_t l_3363 = 0xBD125B06L;
    int64_t l_3410 = 0x28831BC44CD98995LL;
    int64_t * const *l_3428[3][4][8] = {{{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303}},{{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303}},{{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303}}};
    uint8_t l_3445 = 254UL;
    int8_t l_3485 = 0L;
    uint64_t **l_3501[4][1][8] = {{{&g_1554,(void*)0,(void*)0,&g_1554,&g_1554,&g_1554,(void*)0,(void*)0}},{{(void*)0,&g_1554,&g_1554,&g_1554,&g_1554,(void*)0,&g_1554,&g_1554}},{{&g_1554,&g_1554,&g_1554,(void*)0,(void*)0,&g_1554,&g_1554,&g_1554}},{{&g_1554,(void*)0,&g_1554,(void*)0,&g_1554,&g_1554,(void*)0,&g_1554}}};
    uint32_t *l_3548[7] = {&l_3334[0],&g_1120,&g_1120,&l_3334[0],&g_1120,&g_1120,&l_3334[0]};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_3316[i] = &l_3317;
    for (i = 0; i < 1; i++)
        l_3334[i] = 18446744073709551610UL;
    if ((*g_5))
    { /* block id: 1294 */
        uint16_t *l_3057[4][9][5] = {{{&g_87,&g_92,&g_87,&g_92,(void*)0},{&g_459,&g_92,&g_459,(void*)0,&g_459},{&g_92,&g_87,&g_92,&g_92,&g_87},{(void*)0,&g_92,&g_459,&g_87,&g_87},{&g_87,&g_92,&g_87,&g_87,&g_459},{&g_87,&g_92,(void*)0,&g_92,&g_87},{&g_87,&g_87,&g_87,&g_92,&g_92},{&g_87,&g_92,&g_459,&g_87,&g_459},{&g_87,&g_92,&g_92,&g_87,&g_92}},{{(void*)0,&g_87,&g_92,&g_87,&g_87},{&g_92,&g_92,(void*)0,&g_87,&g_459},{&g_459,(void*)0,(void*)0,&g_87,&g_87},{&g_87,&g_87,(void*)0,&g_92,&g_87},{&g_87,&g_92,&g_92,&g_92,&g_459},{(void*)0,&g_87,&g_92,&g_87,(void*)0},{(void*)0,(void*)0,&g_459,&g_87,&g_459},{(void*)0,&g_92,&g_87,&g_92,&g_459},{&g_87,&g_87,(void*)0,(void*)0,&g_459}},{{&g_87,&g_92,&g_87,&g_92,(void*)0},{&g_459,&g_92,&g_459,(void*)0,&g_459},{&g_92,&g_87,&g_92,&g_92,&g_87},{(void*)0,&g_92,&g_459,&g_87,&g_87},{&g_87,&g_92,&g_87,&g_87,&g_459},{&g_87,&g_92,(void*)0,&g_92,&g_87},{&g_87,&g_87,&g_87,&g_92,&g_92},{&g_87,&g_92,&g_459,&g_87,&g_459},{&g_87,&g_92,&g_92,&g_87,&g_92}},{{(void*)0,&g_87,&g_92,&g_87,&g_87},{&g_92,(void*)0,(void*)0,&g_459,(void*)0},{(void*)0,(void*)0,&g_459,&g_92,&g_87},{&g_87,&g_87,(void*)0,&g_459,&g_92},{&g_87,&g_459,(void*)0,&g_92,&g_92},{&g_92,&g_87,(void*)0,&g_87,&g_92},{&g_92,(void*)0,&g_459,&g_92,(void*)0},{&g_92,(void*)0,&g_87,(void*)0,(void*)0},{&g_87,&g_92,&g_459,(void*)0,(void*)0}}};
        int8_t **l_3069 = &g_111[2];
        int8_t **l_3070[5];
        int32_t l_3075 = 0xBDB82640L;
        float *l_3076[5][9][1];
        int32_t l_3078[3][4] = {{(-3L),1L,(-3L),(-3L)},{1L,1L,(-1L),1L},{1L,(-3L),(-3L),1L}};
        volatile int64_t ** volatile ***l_3132 = &g_3131;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_3070[i] = &g_111[5];
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 9; j++)
            {
                for (k = 0; k < 1; k++)
                    l_3076[i][j][k] = &g_213;
            }
        }
        (**g_1702) = (safe_mul_func_float_f_f((safe_mul_func_float_f_f((l_3077 = (safe_sub_func_float_f_f((l_3057[3][8][0] == &g_92), (l_3075 = (safe_add_func_float_f_f((safe_div_func_float_f_f(l_3062[4], (-0x1.Ep+1))), ((*g_427) = ((safe_mod_func_uint64_t_u_u(((safe_div_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(0xACL, ((l_3071 = (void*)0) != l_3072))), (*g_578))) || ((safe_mul_func_uint8_t_u_u(((*g_1703) , (***g_1531)), l_3062[1])) > p_16)), l_3075)) , (***g_1701))))))))), l_3078[0][3])), l_3079[5]));
        for (g_459 = 0; (g_459 >= 54); g_459++)
        { /* block id: 1302 */
            uint32_t l_3087[4][9] = {{1UL,4294967286UL,1UL,8UL,4294967293UL,0xCF9A4630L,0xCF9A4630L,4294967293UL,8UL},{1UL,4294967295UL,1UL,0xCF9A4630L,0xACFA1903L,1UL,4294967288UL,4294967288UL,1UL},{1UL,4294967286UL,1UL,4294967286UL,1UL,8UL,4294967293UL,0xCF9A4630L,0xCF9A4630L},{4294967286UL,0xF32CDA1AL,1UL,0xCF9A4630L,1UL,0xF32CDA1AL,4294967286UL,0xACFA1903L,0x85487D2BL}};
            int32_t l_3101 = 0x0FCC3DE8L;
            int32_t l_3106[2][9];
            int i, j;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 9; j++)
                    l_3106[i][j] = 3L;
            }
            for (g_2290 = 0; (g_2290 <= 0); g_2290 += 1)
            { /* block id: 1305 */
                int32_t *l_3083 = &l_3078[0][2];
                int32_t *l_3084 = (void*)0;
                int32_t *l_3085 = &g_1396[4][0][5];
                int32_t *l_3086[9];
                int64_t **l_3125 = (void*)0;
                int64_t ***l_3124[3][6][1] = {{{&l_3125},{&l_3125},{(void*)0},{(void*)0},{&l_3125},{&l_3125}},{{(void*)0},{&l_3125},{&l_3125},{(void*)0},{(void*)0},{&l_3125}},{{&l_3125},{(void*)0},{&l_3125},{&l_3125},{(void*)0},{(void*)0}}};
                int i, j, k;
                for (i = 0; i < 9; i++)
                    l_3086[i] = &l_3077;
                ++l_3087[3][4];
                for (g_1120 = 0; (g_1120 <= 0); g_1120 += 1)
                { /* block id: 1309 */
                    int32_t l_3098 = (-1L);
                    int32_t l_3102 = 0xF61408FDL;
                    int32_t l_3104 = 0x29A8F96BL;
                    int32_t l_3105 = 0xBBB1194BL;
                    int32_t l_3107 = 0x2832E17AL;
                    int32_t l_3108 = (-10L);
                    int32_t l_3109 = (-7L);
                    int32_t l_3110 = 0L;
                    uint32_t l_3111 = 4294967295UL;
                    int64_t ***l_3127 = &l_3125;
                    int64_t ****l_3126 = &l_3127;
                    if ((safe_add_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(((((((l_3075 && (4L ^ (-5L))) && p_16) ^ ((*g_1478) , p_16)) == (((*g_1554) > p_17) , p_17)) < 0L) & 6L), (*g_578))), (*l_3083))))
                    { /* block id: 1310 */
                        int16_t l_3096 = (-6L);
                        int32_t l_3097 = 0L;
                        int32_t l_3099 = 0x94221AA3L;
                        int32_t l_3100[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_3100[i] = 0x2B474F36L;
                        ++l_3111;
                        if (l_3078[1][0])
                            break;
                    }
                    else
                    { /* block id: 1313 */
                        return p_16;
                    }
                    (*l_3085) = (safe_add_func_uint16_t_u_u(((safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u((0xFDL == (0UL && (safe_lshift_func_uint8_t_u_s((0x58710855L < (p_16 , ((l_3087[3][4] , (safe_add_func_int64_t_s_s(((l_3124[2][3][0] == ((*l_3126) = &l_3125)) | ((0UL <= (*g_1554)) , p_16)), l_3103[2][0][5]))) > (*l_3085)))), 4)))), 15)), 0x45A3E977L)) != (*g_578)), 1UL));
                    for (g_467 = 6; (g_467 >= 0); g_467 -= 1)
                    { /* block id: 1320 */
                        int i;
                        return g_1832[g_467];
                    }
                    (*l_3083) &= (*g_771);
                    for (g_192 = 0; (g_192 <= 0); g_192 += 1)
                    { /* block id: 1326 */
                        return (*g_1078);
                    }
                }
            }
            (*g_771) |= (l_3101 = 0xEAA66875L);
        }
        (*l_3132) = &g_1151;
        (*g_771) = (p_17 == 0x76L);
    }
    else
    { /* block id: 1336 */
        uint64_t l_3138[9][9] = {{0UL,0UL,0xE220B65FB5B09C03LL,0UL,0x78E6680FE5813357LL,0x72D1C3B1CD9254ADLL,1UL,0x72D1C3B1CD9254ADLL,0x78E6680FE5813357LL},{0x7158734130DB0CEBLL,0x6125E9F251CC7F59LL,0x6125E9F251CC7F59LL,0x7158734130DB0CEBLL,1UL,18446744073709551615UL,5UL,0x1148B81A26038A15LL,5UL},{1UL,0x646BBBF4D98B74C5LL,0xE220B65FB5B09C03LL,0xE220B65FB5B09C03LL,0x646BBBF4D98B74C5LL,1UL,1UL,0UL,9UL},{0x27783CE34DFFA692LL,18446744073709551615UL,0x1148B81A26038A15LL,1UL,1UL,0x1148B81A26038A15LL,18446744073709551615UL,0x27783CE34DFFA692LL,2UL},{9UL,0xE220B65FB5B09C03LL,0x4A5F9A1D059969E1LL,1UL,0x78E6680FE5813357LL,0x78E6680FE5813357LL,1UL,0x4A5F9A1D059969E1LL,0xE220B65FB5B09C03LL},{1UL,0x27783CE34DFFA692LL,0x4D370B75324DBF30LL,2UL,0x6125E9F251CC7F59LL,5UL,5UL,0x6125E9F251CC7F59LL,2UL},{0UL,0x00BEF7A4C00BD590LL,0UL,0x72D1C3B1CD9254ADLL,1UL,9UL,1UL,1UL,9UL},{0x4D370B75324DBF30LL,0x27783CE34DFFA692LL,1UL,0x27783CE34DFFA692LL,0x4D370B75324DBF30LL,2UL,0x6125E9F251CC7F59LL,5UL,5UL},{0x4A5F9A1D059969E1LL,0xE220B65FB5B09C03LL,9UL,0x72D1C3B1CD9254ADLL,9UL,0xE220B65FB5B09C03LL,0x4A5F9A1D059969E1LL,1UL,0x78E6680FE5813357LL}};
        int32_t l_3156 = 9L;
        int32_t l_3158[4] = {(-1L),(-1L),(-1L),(-1L)};
        const uint32_t **l_3196 = (void*)0;
        uint64_t ** const *l_3248 = &g_979;
        uint32_t ***l_3307 = &g_1582;
        int8_t l_3351 = 0x24L;
        int64_t l_3400 = (-1L);
        float l_3498 = (-0x1.Cp+1);
        int i, j;
lbl_3537:
        for (g_617 = 0; (g_617 >= 12); ++g_617)
        { /* block id: 1339 */
            int32_t *l_3135 = &l_3082[8][2];
            l_3135 = &l_3082[5][8];
        }
        if ((safe_lshift_func_int8_t_s_s(p_17, p_16)))
        { /* block id: 1342 */
            int32_t l_3157 = 0x7B91AC3BL;
            int8_t **l_3180 = (void*)0;
            int32_t l_3181 = 0L;
            int32_t l_3182 = 0x654F692AL;
            int32_t *l_3183 = &g_46;
            int16_t * const l_3216 = &g_105;
            float l_3220 = 0x4.6p+1;
            uint32_t l_3223 = 0xD1B9AD0EL;
            const uint64_t l_3253 = 0x716F9C8463980D76LL;
            uint64_t ****l_3261 = (void*)0;
            int8_t l_3278[10][3][2];
            uint16_t l_3299[3][2][7] = {{{0x7A05L,0x8C5AL,0x8C5AL,0x7A05L,0x1334L,0x7A05L,0x8C5AL},{0UL,0UL,0x8C5AL,3UL,0x8C5AL,0UL,0UL}},{{0UL,0x8C5AL,3UL,0x8C5AL,0UL,0UL,0x8C5AL},{0x7A05L,0x1334L,0x7A05L,0x8C5AL,0x8C5AL,0x7A05L,0x1334L}},{{0x8C5AL,0x1334L,3UL,3UL,0x1334L,0x8C5AL,0x1334L},{0x7A05L,0x8C5AL,0x8C5AL,0x7A05L,0x1334L,0x7A05L,0x8C5AL}}};
            float l_3321 = 0x1.Bp-1;
            int32_t l_3327 = (-6L);
            int32_t l_3357 = 0x403535C4L;
            int32_t l_3359 = 0L;
            int32_t l_3362 = 0x726F1E69L;
            int64_t **l_3430[4][10] = {{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303},{&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303,&g_303}};
            float l_3524 = (-0x8.2p-1);
            int i, j, k;
            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    for (k = 0; k < 2; k++)
                        l_3278[i][j][k] = 0x24L;
                }
            }
            l_3138[7][2]--;
            for (g_2711 = 3; (g_2711 >= 0); g_2711 -= 1)
            { /* block id: 1346 */
                int64_t **l_3144 = &g_303;
                int64_t ***l_3143 = &l_3144;
                int32_t l_3169 = 0x781C9FD5L;
                int32_t l_3174 = 0x7D78156EL;
                uint32_t *l_3201 = (void*)0;
                uint32_t **l_3200 = &l_3201;
                int8_t ***l_3221 = &l_3180;
                uint32_t l_3256[10][2] = {{8UL,0UL},{8UL,8UL},{0UL,8UL},{8UL,0UL},{8UL,8UL},{0UL,8UL},{8UL,0UL},{8UL,8UL},{0UL,8UL},{8UL,0UL}};
                int8_t **l_3294 = &l_3072;
                uint16_t ***l_3318[7][7] = {{&l_3317,&l_3317,(void*)0,&l_3317,&l_3317,(void*)0,&l_3317},{&l_3317,&l_3317,&l_3317,&l_3317,&l_3317,&l_3317,&l_3317},{(void*)0,&l_3317,&l_3317,&l_3317,&l_3317,(void*)0,&l_3317},{&l_3317,&l_3317,&l_3317,&l_3317,&l_3317,&l_3317,&l_3317},{&l_3317,&l_3317,(void*)0,&l_3317,&l_3317,&l_3317,&l_3317},{&l_3317,&l_3317,&l_3317,&l_3317,&l_3317,&l_3317,&l_3317},{&l_3317,&l_3317,(void*)0,&l_3317,&l_3317,(void*)0,&l_3317}};
                int8_t l_3348 = 1L;
                int32_t l_3349 = (-1L);
                int32_t l_3352 = 0xABAE6136L;
                int32_t l_3353 = (-1L);
                int32_t l_3354 = 0x54F695A0L;
                int32_t l_3355 = 1L;
                int32_t l_3356 = 0x23CED470L;
                int32_t l_3358 = 0x411DBCF6L;
                int32_t l_3361[10][1];
                int16_t l_3366 = 2L;
                int64_t ****l_3418 = (void*)0;
                int64_t *****l_3417 = &l_3418;
                int32_t l_3433[2][5][3] = {{{0L,0xC7F8D685L,0xC7F8D685L},{0xB888F79DL,0x6DEFD52BL,0x6DEFD52BL},{0L,0xC7F8D685L,0xC7F8D685L},{0xB888F79DL,0x6DEFD52BL,0x6DEFD52BL},{0L,0xC7F8D685L,0xC7F8D685L}},{{0xB888F79DL,0x6DEFD52BL,0x6DEFD52BL},{0L,0xC7F8D685L,0xC7F8D685L},{0xB888F79DL,0x6DEFD52BL,0x6DEFD52BL},{0L,0xC7F8D685L,0xC7F8D685L},{0xB888F79DL,0x6DEFD52BL,0x6DEFD52BL}}};
                int8_t l_3434 = 0xA6L;
                int i, j, k;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_3361[i][j] = 0x4EE9E92DL;
                }
            }
            for (p_16 = 0; (p_16 <= (-10)); p_16--)
            { /* block id: 1490 */
                uint64_t *****l_3464 = &l_3261;
                int32_t l_3507 = 0L;
            }
        }
        else
        { /* block id: 1530 */
            if (g_459)
                goto lbl_3537;
        }
        (*g_84) ^= (safe_div_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(((l_3156 &= ((**g_979) = ((safe_lshift_func_int16_t_s_u(p_16, 14)) != ((safe_rshift_func_int8_t_s_u(1L, 0)) & ((&l_3334[0] == (l_3548[3] = func_53(((safe_mul_func_int16_t_s_s(p_17, l_3158[0])) <= 0x190BL)))) | (((**l_3317) = (safe_rshift_func_uint16_t_u_s((*g_578), 2))) , ((((safe_rshift_func_uint8_t_u_s(3UL, p_16)) || 0x53E4L) <= l_3158[0]) , p_17))))))) >= l_3400), l_3158[0])), p_16));
        (*g_3553) = &l_3158[3];
    }
    return p_16;
}


/* ------------------------------------------ */
/* 
 * reads : g_303 g_304 g_2696 g_1554 g_106 g_1926 g_1077 g_1078 g_578 g_459 g_980 g_566 g_160 g_1365 g_2711 g_751 g_84 g_1896 g_30 g_9 g_1532 g_979 g_190 g_798 g_13 g_46 g_771 g_2791 g_1581 g_1582 g_2144 g_427 g_269 g_2817 g_44 g_2705 g_2849 g_87 g_105 g_2122 g_426 g_1530 g_92 g_2927 g_1968 g_1824 g_855
 * writes: g_898 g_751 g_1365 g_44 g_459 g_30 g_2696 g_46 g_105 g_798 g_190 g_92 g_160 g_269 g_304 g_802 g_87 g_2144 g_2927 g_1968 g_106 g_203 g_2635 g_855
 */
static uint8_t  func_18(uint32_t  p_19)
{ /* block id: 1115 */
    uint8_t l_2689[10][10][1] = {{{1UL},{1UL},{0x2FL},{1UL},{1UL},{255UL},{0x88L},{5UL},{1UL},{0x5EL}},{{5UL},{0x2FL},{0x83L},{255UL},{0x7DL},{5UL},{0xD9L},{1UL},{0x2FL},{0x52L}},{{0x52L},{0x2FL},{1UL},{0xD9L},{5UL},{0x7DL},{255UL},{0x83L},{0x2FL},{5UL}},{{0xC2L},{5UL},{0x5EL},{255UL},{0x52L},{0x7DL},{5UL},{0UL},{5UL},{0x7DL}},{{0x52L},{255UL},{0x5EL},{5UL},{0xC2L},{5UL},{0x2FL},{0x83L},{255UL},{0x7DL}},{{5UL},{0xD9L},{1UL},{0x2FL},{0x52L},{0x52L},{0x2FL},{1UL},{0xD9L},{5UL}},{{0x7DL},{255UL},{0x83L},{0x2FL},{5UL},{0xC2L},{5UL},{0x5EL},{255UL},{0x52L}},{{0x7DL},{5UL},{0UL},{5UL},{0x7DL},{0x52L},{255UL},{0x5EL},{5UL},{0xC2L}},{{5UL},{0x2FL},{0x83L},{255UL},{0x7DL},{5UL},{0xD9L},{1UL},{0x2FL},{0x52L}},{{0x52L},{0x2FL},{1UL},{0xD9L},{5UL},{0x7DL},{255UL},{0x83L},{0x2FL},{5UL}}};
    int64_t *l_2724 = &g_304[1][2];
    int32_t *l_2777 = &g_2144;
    int16_t **l_2861 = &g_2028;
    const uint32_t ** const l_2901 = (void*)0;
    uint16_t *l_2923 = &g_459;
    uint32_t ****l_2954[6];
    int8_t *l_2964[2][4] = {{(void*)0,&g_30[1][2],(void*)0,(void*)0},{&g_30[1][2],&g_30[1][2],&g_2696[1],&g_30[1][2]}};
    int32_t l_2993 = 6L;
    int32_t l_2995[5][1][1] = {{{(-1L)}},{{(-1L)}},{{(-1L)}},{{(-1L)}},{{(-1L)}}};
    int8_t **l_3017 = &l_2964[1][3];
    int8_t ***l_3016[8][8] = {{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017},{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017},{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017},{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017},{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017},{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017},{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017},{&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017,&l_3017}};
    float l_3028 = 0x1.F1A9F0p-99;
    uint8_t * volatile * volatile * volatile **l_3029 = &g_1530[0];
    int64_t l_3031[6][6][2] = {{{0xD0AC58D3D2AB0B30LL,0x5A127C5353AFA310LL},{(-1L),0x7771766FFF16868FLL},{0xCF7EAED6E0CA9C65LL,1L},{1L,(-1L)},{(-1L),(-1L)},{0x5A127C5353AFA310LL,1L}},{{0x8807ED5930FADD3BLL,0xAAC5FF70F32B3B63LL},{(-1L),1L},{(-4L),(-1L)},{(-6L),0x5CC43BC124A680D3LL},{(-6L),(-1L)},{(-4L),1L}},{{0x5CC43BC124A680D3LL,0xCC2EE4D830D44B2BLL},{(-4L),0x5A127C5353AFA310LL},{7L,0L},{0L,0xF7B112D75D8DEB3DLL},{(-1L),0x5A127C5353AFA310LL},{(-1L),(-4L)}},{{0x5CC43BC124A680D3LL,7L},{0xCF7EAED6E0CA9C65LL,0x5CC43BC124A680D3LL},{0xFECE996C4569E5F4LL,0x66EEC14EAA1EC64CLL},{0xB5EECE36A8A15CCCLL,1L},{0xCF7EAED6E0CA9C65LL,(-1L)},{1L,(-4L)}},{{(-4L),0xFE15CF8F831D1B1ELL},{(-1L),0L},{0xF7B112D75D8DEB3DLL,0L},{(-1L),0xFE15CF8F831D1B1ELL},{(-4L),(-4L)},{1L,(-1L)}},{{0xCF7EAED6E0CA9C65LL,1L},{0xB5EECE36A8A15CCCLL,0x66EEC14EAA1EC64CLL},{0xFECE996C4569E5F4LL,0x5CC43BC124A680D3LL},{0xCF7EAED6E0CA9C65LL,7L},{0x5CC43BC124A680D3LL,(-4L)},{(-1L),0x5A127C5353AFA310LL}}};
    uint32_t **l_3034 = (void*)0;
    uint32_t ***l_3035 = &g_2635;
    uint64_t l_3036 = 0x33CD5B8B61F1B2CBLL;
    uint16_t **l_3042 = &l_2923;
    uint16_t ***l_3041 = &l_3042;
    int8_t *l_3043[6] = {&g_266,&g_266,&g_266,&g_266,&g_266,&g_266};
    int32_t l_3044[7][2] = {{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L},{1L,1L}};
    uint64_t l_3047 = 0x9863F5842EFFE01BLL;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_2954[i] = &g_1581;
    l_2689[1][4][0]--;
    if (p_19)
    { /* block id: 1117 */
        int64_t *l_2692[10][8] = {{&g_142,&g_142,&g_304[1][2],&g_304[1][2],&g_142,&g_142,&g_304[0][6],(void*)0},{&g_142,&g_142,&g_304[1][2],&g_142,&g_304[0][6],&g_142,&g_304[1][2],&g_142},{(void*)0,&g_304[1][2],(void*)0,&g_142,(void*)0,&g_304[0][0],&g_304[0][0],(void*)0},{&g_304[1][2],(void*)0,(void*)0,&g_304[1][2],(void*)0,&g_142,&g_304[0][0],&g_142},{&g_304[1][2],&g_304[1][2],(void*)0,&g_304[0][0],(void*)0,&g_304[1][2],&g_304[1][2],&g_304[0][6]},{(void*)0,&g_304[1][2],&g_304[1][2],&g_304[0][6],&g_142,&g_142,&g_304[0][6],&g_304[1][2]},{(void*)0,(void*)0,&g_304[1][2],(void*)0,&g_142,&g_304[0][0],&g_142,&g_304[0][0]},{(void*)0,&g_304[1][2],(void*)0,&g_304[1][2],(void*)0,&g_142,(void*)0,&g_304[0][0]},{&g_304[1][2],&g_142,&g_142,(void*)0,(void*)0,&g_142,&g_142,&g_304[1][2]},{&g_304[1][2],&g_142,&g_142,&g_304[0][6],(void*)0,(void*)0,(void*)0,&g_304[0][6]}};
        int32_t l_2693 = (-6L);
        int32_t l_2702 = 0xB9D86AEEL;
        const uint16_t *l_2704[8][4] = {{&g_2705,&g_2705,(void*)0,&g_2705},{(void*)0,&g_2705,&g_2705,(void*)0},{&g_2705,(void*)0,&g_2705,(void*)0},{&g_2705,&g_2705,&g_2705,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_2705,&g_2705,&g_2705,(void*)0},{(void*)0,(void*)0,&g_2705,(void*)0},{&g_2705,&g_2705,(void*)0,&g_2705}};
        const uint16_t **l_2703 = &l_2704[4][0];
        const uint16_t *l_2707 = &g_2705;
        const uint16_t **l_2706 = &l_2707;
        int32_t ***l_2710 = &g_113[5][3];
        int32_t l_2787 = (-1L);
        int16_t l_2788 = 0L;
        uint64_t l_2914 = 0x0CD1830DB0D563B0LL;
        int32_t l_2997 = 0x728D326FL;
        uint16_t *l_3020[4][6] = {{&g_87,&g_87,&g_87,&g_87,&g_87,(void*)0},{(void*)0,&g_87,&g_87,&g_87,(void*)0,&g_87},{&g_87,(void*)0,&g_87,&g_87,(void*)0,&g_87},{(void*)0,&g_87,&g_87,(void*)0,&g_87,&g_87}};
        int i, j;
        if (((l_2693 |= (*g_303)) >= ((((safe_div_func_uint8_t_u_u(((**g_1077) = ((((3L > (g_2696[1] < p_19)) , ((+(*g_1554)) < (safe_rshift_func_uint16_t_u_s(((((*g_1926) = p_19) == (safe_lshift_func_int16_t_s_u((((l_2702 ^ (((*l_2706) = ((*l_2703) = &g_87)) == ((safe_mul_func_int8_t_s_s(((*g_1077) != (void*)0), l_2702)) , &g_92))) > 0x4580L) == 0x9BFEL), (*g_578)))) | (*g_980)), p_19)))) ^ (*g_566)) || p_19)), 0xAAL)) , l_2710) == l_2710) && 0x4C0EL)))
        { /* block id: 1123 */
            uint64_t l_2712 = 18446744073709551608UL;
            const uint8_t *l_2742 = &g_1832[4];
            const uint8_t **l_2741[8] = {&l_2742,&l_2742,&l_2742,&l_2742,&l_2742,&l_2742,&l_2742,&l_2742};
            const uint8_t ***l_2740 = &l_2741[3];
            int32_t l_2744 = 0xE71FCBE8L;
            int16_t l_2773 = 8L;
            uint32_t ***l_2873[2];
            uint8_t ***l_2920 = (void*)0;
            uint8_t **** const l_2919 = &l_2920;
            uint16_t *l_2921 = &g_87;
            int16_t *l_2922 = &g_105;
            int8_t *l_2924[4][4] = {{&g_2696[1],&g_2696[1],&g_2696[1],&g_2696[1]},{&g_2696[1],&g_2696[1],&g_2696[1],&g_2696[1]},{&g_2696[1],&g_2696[1],&g_2696[1],&g_2696[1]},{&g_2696[1],&g_2696[1],&g_2696[1],&g_2696[1]}};
            uint16_t *l_2925 = &g_92;
            uint8_t **l_2926[5] = {&g_1078,&g_1078,&g_1078,&g_1078,&g_1078};
            int i, j;
            for (i = 0; i < 2; i++)
                l_2873[i] = &g_1582;
lbl_2892:
            for (g_1365 = 1; (g_1365 >= 0); g_1365 -= 1)
            { /* block id: 1126 */
                uint8_t l_2715 = 253UL;
                int8_t l_2743 = 0xC6L;
                int8_t *l_2753 = (void*)0;
                int8_t *l_2754 = (void*)0;
                int8_t *l_2755 = &g_30[6][5];
                int8_t *l_2756 = &g_2696[4];
                float *l_2781[7];
                int16_t *l_2845[5];
                int32_t l_2877 = 0x67853DA0L;
                int i;
                for (i = 0; i < 7; i++)
                    l_2781[i] = &g_269;
                for (i = 0; i < 5; i++)
                    l_2845[i] = &g_105;
                (*g_84) = ((l_2712 = g_2711) , (safe_div_func_int32_t_s_s((l_2715 & (l_2715 | (safe_sub_func_int8_t_s_s((((void*)0 == l_2692[1][6]) == ((safe_mod_func_uint16_t_u_u((l_2712 > l_2689[9][6][0]), (*g_578))) , ((safe_sub_func_uint8_t_u_u((((0L >= (*g_1554)) , l_2692[7][6]) == l_2724), (**g_1077))) < l_2715))), (-1L))))), p_19)));
                l_2744 ^= (safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_u(65526UL, 14)), (safe_sub_func_int32_t_s_s(0L, ((((-8L) < (+(safe_mul_func_uint8_t_u_u(((l_2712 < ((safe_lshift_func_uint16_t_u_s(((safe_lshift_func_uint8_t_u_u((&l_2710 != (((safe_rshift_func_uint16_t_u_u(((*g_578) |= 0xE94AL), ((void*)0 != l_2740))) < ((0x4A4FL ^ p_19) != l_2712)) , &g_132)), 5)) , l_2743), g_1896)) , 0xE6L)) , 246UL), l_2689[1][4][0])))) != l_2689[1][4][0]) > p_19)))));
                if ((safe_div_func_uint16_t_u_u(((*g_578) &= (safe_lshift_func_int16_t_s_s(((((*l_2756) = (safe_sub_func_uint64_t_u_u((((-4L) & 0x0AL) == (safe_mod_func_int16_t_s_s((l_2689[1][4][0] , (((1UL >= (p_19 || (0x39L & l_2689[1][4][0]))) && p_19) > ((*l_2755) &= 0x4FL))), p_19))), (*g_303)))) && 0xFAL) , g_9), p_19))), l_2689[8][3][0])))
                { /* block id: 1134 */
                    int8_t * const *l_2785[10][7] = {{(void*)0,&l_2756,&l_2754,&g_111[5],&l_2754,&g_111[4],&l_2755},{&l_2753,(void*)0,&g_111[7],&l_2756,&g_111[7],(void*)0,&l_2753},{&l_2753,&l_2754,&l_2753,&l_2756,(void*)0,&l_2753,&g_111[7]},{(void*)0,(void*)0,&g_111[5],&l_2755,&l_2754,(void*)0,(void*)0},{&l_2754,&l_2754,&l_2753,&g_111[0],(void*)0,&l_2754,(void*)0},{&g_111[5],&g_111[7],&g_111[7],&g_111[5],(void*)0,&g_111[5],&l_2756},{&g_111[2],(void*)0,&l_2754,&g_111[4],&l_2754,&l_2756,&l_2753},{&l_2756,&l_2754,&g_111[4],&l_2754,(void*)0,&g_111[2],&l_2756},{&g_111[5],(void*)0,&g_111[5],&g_111[7],&g_111[7],&g_111[5],(void*)0},{&l_2754,(void*)0,&g_111[0],&l_2753,&l_2754,&l_2754,(void*)0}};
                    int32_t *l_2794 = (void*)0;
                    uint8_t **l_2805[1][2];
                    const uint8_t l_2816 = 247UL;
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 2; j++)
                            l_2805[i][j] = &g_1078;
                    }
                    for (g_46 = 0; (g_46 <= 1); g_46 += 1)
                    { /* block id: 1137 */
                        int32_t l_2761 = 1L;
                        int32_t l_2771 = 0x9E84E6DCL;
                        int16_t *l_2772 = &g_105;
                        int i, j;
                        (*g_84) = (((*l_2756) = (safe_lshift_func_uint8_t_u_s((**g_1077), ((((((*g_303) < (((*l_2772) = (l_2771 |= ((safe_rshift_func_uint16_t_u_u((l_2761 , ((safe_mod_func_int16_t_s_s(l_2689[7][9][0], l_2744)) == (safe_mod_func_uint32_t_u_u((((l_2744 == (**g_1532)) & ((~0xA1BA3C89F3F2C7F3LL) > (safe_add_func_int32_t_s_s(((p_19 , l_2761) , p_19), 0xCB1E2F85L)))) , l_2744), l_2712)))), (*g_578))) <= (**g_979)))) , 0x66DC81968356E7F1LL)) , l_2771) || 0UL) , p_19) & (*g_1078))))) <= p_19);
                        if (p_19)
                            continue;
                    }
                    if (p_19)
                    { /* block id: 1144 */
                        return l_2773;
                    }
                    else
                    { /* block id: 1146 */
                        uint64_t l_2776 = 0x5E256860EB540D96LL;
                        int32_t **l_2778 = &g_798;
                        (*l_2778) = (((safe_rshift_func_uint8_t_u_s((**g_1532), 1)) , (l_2776 , (*g_1554))) , l_2777);
                    }
                    if (l_2712)
                        goto lbl_3030;
                    for (g_190 = 2; (g_190 <= 7); g_190 += 1)
                    { /* block id: 1151 */
                        int32_t ****l_2783 = &l_2710;
                        int32_t *****l_2782 = &l_2783;
                        uint16_t *l_2786 = &g_92;
                        uint8_t ***l_2806 = &l_2805[0][1];
                        int16_t *l_2809 = &l_2788;
                        int i;
                        (*g_771) = ((((safe_mul_func_int16_t_s_s(((l_2781[1] = l_2777) != ((((&g_132 == ((*l_2782) = &l_2710)) ^ (+(*g_578))) == ((*l_2786) = (l_2773 == (l_2785[6][1] == &g_1478)))) , func_53(p_19))), l_2787)) | l_2788) , p_19) || p_19);
                        if (l_2787)
                            goto lbl_2892;
                        if (p_19)
                            continue;
                        l_2794 = func_53(((safe_mul_func_int16_t_s_s(g_2791, 0x603BL)) , (l_2715 || ((*g_1581) == (*g_1581)))));
                        (*g_427) = ((safe_sub_func_float_f_f(((safe_div_func_float_f_f((((*l_2777) <= ((((((safe_sub_func_int8_t_s_s((((((safe_sub_func_int32_t_s_s((safe_mul_func_uint8_t_u_u(((**g_1532) = (((*l_2806) = l_2805[0][0]) != (void*)0)), l_2715)), ((*g_566) = ((safe_mul_func_int16_t_s_s(((*l_2809) = p_19), p_19)) && (safe_rshift_func_uint16_t_u_u(p_19, ((safe_mod_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((p_19 , p_19), p_19)), 4294967288UL)) == p_19))))))) , l_2744) <= l_2744) && l_2712) > (*g_980)), 0x0BL)) && l_2712) , l_2816) >= (*g_427)) == p_19) != g_2817[2][1])) >= 0xA.564DF0p+20), 0xE.2E698Bp+77)) != (*l_2777)), (-0x8.Fp+1))) >= 0xE.95F5B1p+46);
                    }
                }
                else
                { /* block id: 1164 */
                    int8_t l_2826 = 0xCBL;
                    const int16_t *l_2847 = &g_2848[3][1];
                    const int16_t **l_2846 = &l_2847;
                    (*g_771) = (((*l_2755) = ((safe_add_func_uint32_t_u_u(((safe_rshift_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u(0UL, (((*g_1926) = ((*g_566)--)) != ((safe_mod_func_uint16_t_u_u(((safe_add_func_int64_t_s_s((safe_lshift_func_int16_t_s_s(p_19, ((((safe_add_func_uint64_t_u_u((*g_1554), 0UL)) <= ((*g_303) = (*g_303))) & ((safe_sub_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s((((safe_add_func_int32_t_s_s((safe_add_func_uint16_t_u_u((l_2845[1] != ((*l_2846) = &l_2773)), ((*g_578) = (((*g_84) <= (0UL || p_19)) && p_19)))), p_19)) ^ l_2744) && p_19), p_19)), p_19)) | l_2826)) & g_2705))), (**g_979))) != p_19), (-3L))) , 1L)))), p_19)), 4)) <= 0x35L), g_2849)) > l_2826)) | 0x6AL);
                }
                for (l_2743 = 0; (l_2743 <= 1); l_2743 += 1)
                { /* block id: 1175 */
                    int32_t **l_2859 = (void*)0;
                    int32_t **l_2860 = &g_802[0];
                    (*l_2860) = func_53((((((*l_2777) , (((*g_771) &= (safe_mod_func_int8_t_s_s(((*l_2755) ^= ((!(p_19 ^ (254UL != (**g_1532)))) > 0x7A1B7E46297C236BLL)), 1L))) | (((safe_rshift_func_uint16_t_u_u(((*g_578) |= ((safe_sub_func_int16_t_s_s((((*g_1926) = ((*g_566) = (p_19 >= (((l_2743 & ((((safe_mul_func_float_f_f((p_19 , 0xF.C59D79p-81), p_19)) , 0x0.1p+1) , 3L) < 4L)) , 0xAD0A0DC0L) | l_2712)))) >= 0x948430A9L), 0UL)) != 1UL)), 9)) , (**g_979)) && (*g_303)))) <= (*l_2777)) & (-1L)) && 3L));
                    for (g_87 = 1; (g_87 <= 4); g_87 += 1)
                    { /* block id: 1184 */
                        int i, j, k;
                        if (g_106[g_87][(g_87 + 2)][(l_2743 + 5)])
                            break;
                        if (g_2817[g_1365][(g_1365 + 5)])
                            break;
                    }
                    for (g_105 = 4; (g_105 >= 0); g_105 -= 1)
                    { /* block id: 1190 */
                        int32_t l_2862 = 4L;
                        uint32_t ***l_2874[8] = {&g_1582,&g_1582,&g_1582,&g_1582,&g_1582,&g_1582,&g_1582,&g_1582};
                        int i, j, k;
                        (*g_771) ^= ((void*)0 == l_2861);
                        l_2744 = ((((0x8.DB680Bp+76 == (l_2862 = g_106[(g_1365 + 2)][(l_2743 + 5)][(l_2743 + 4)])) > (safe_sub_func_float_f_f((g_106[(g_1365 + 2)][(l_2743 + 4)][(l_2743 + 3)] != (*l_2777)), g_106[(g_1365 + 2)][(l_2743 + 5)][(l_2743 + 4)]))) <= (safe_div_func_float_f_f((safe_add_func_float_f_f(((safe_sub_func_float_f_f((((safe_div_func_float_f_f(((***g_2122) = (l_2873[1] == (l_2874[7] = &g_1582))), (l_2877 = (safe_sub_func_float_f_f((p_19 , (p_19 != l_2773)), p_19))))) == g_106[(g_1365 + 2)][(l_2743 + 5)][(l_2743 + 4)]) >= p_19), l_2743)) <= 0xA.8CA5EAp+83), p_19)), p_19))) , 0x4.D6BBBBp-54);
                        (*g_427) = (((safe_sub_func_uint32_t_u_u(((p_19 || (~p_19)) || (safe_rshift_func_int8_t_s_s(p_19, (*l_2777)))), (((~((*g_303) = (*g_303))) ^ p_19) , ((*g_303) ^ ((safe_lshift_func_int8_t_s_u((safe_add_func_uint8_t_u_u((safe_add_func_int8_t_s_s((((safe_add_func_float_f_f(((0xD89CF8096CDF6C1BLL & (1L == 0xAEB0EC205DFD9B37LL)) , p_19), p_19)) , p_19) , (-1L)), p_19)), (**g_1532))), 2)) ^ (*g_980)))))) , 0x0.0p-1) < (-0x4.8p-1));
                        (***g_2122) = p_19;
                    }
                }
            }
            (***g_2122) = (((((((*l_2925) |= (~((*l_2777) = (((safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s(((((*g_566) = ((*l_2777) ^ (safe_lshift_func_uint8_t_u_s((((((!((l_2901 != (void*)0) , ((safe_rshift_func_uint8_t_u_s((*l_2777), (safe_sub_func_uint16_t_u_u((safe_add_func_uint64_t_u_u((safe_add_func_int32_t_s_s((((safe_mod_func_int16_t_s_s(l_2773, ((safe_sub_func_int64_t_s_s((l_2914 && ((*g_1926) = ((((*l_2922) = (safe_sub_func_uint32_t_u_u((((*l_2921) ^= ((*g_578) <= (safe_lshift_func_int8_t_s_s((l_2919 == g_1530[0]), 0)))) | p_19), 0x9ED660DDL))) || 0L) ^ (*g_578)))), l_2773)) & p_19))) , p_19) >= 8L), 0L)), p_19)), p_19)))) == p_19))) < 0x69L) || p_19) , (void*)0) != l_2923), (*l_2777))))) < 0x0582363DL) >= 0x78L), 0)), p_19)) , p_19) , l_2773)))) < (-9L)) , p_19) , l_2926[2]) != (void*)0) != l_2744);
            (*g_84) &= p_19;
        }
        else
        { /* block id: 1212 */
            int32_t l_2940[1];
            uint32_t ****l_2953 = (void*)0;
            uint32_t l_2956 = 0x00980E7CL;
            const int8_t l_2987[7][4][4] = {{{0x5FL,0L,0xADL,0L},{0L,0L,0xB0L,0L},{0xB0L,0L,(-1L),(-1L)},{0L,0L,0xADL,0x5FL}},{{0L,0L,(-1L),0L},{0xB0L,0x5FL,0xB0L,(-1L)},{0L,0x5FL,0xADL,0L},{0x5FL,0L,0L,0x5FL}},{{0xB0L,0L,0L,(-1L)},{0x5FL,0L,0xADL,0L},{0L,0L,0xB0L,0L},{0xB0L,0L,(-1L),(-1L)}},{{0L,0L,0xADL,0x5FL},{0L,0L,(-1L),0L},{0xB0L,0x5FL,0xB0L,(-1L)},{0L,0x5FL,0xADL,0L}},{{0x5FL,0L,0L,0x5FL},{0xB0L,0L,0xADL,0L},{0L,(-1L),0L,(-1L)},{(-1L),0xADL,0xD8L,(-1L)}},{{0xD8L,(-1L),0L,0L},{0xB0L,0xB0L,0L,0L},{0xB0L,0xADL,0L,0xB0L},{0xD8L,0L,0xD8L,0L}},{{(-1L),0L,0L,0xB0L},{0L,0xADL,0xADL,0L},{0xD8L,0xB0L,0xADL,0L},{0L,(-1L),0L,(-1L)}}};
            int32_t l_3000 = 0L;
            uint32_t l_3003 = 0x59B7D033L;
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_2940[i] = 0xAD9BC32FL;
            --g_2927[0];
            for (g_1968 = 0; (g_1968 <= 1); g_1968 += 1)
            { /* block id: 1216 */
                const int32_t l_2933 = 0x0F3F57A4L;
                int i;
                (*l_2777) |= (((+((*g_1554) = (safe_mul_func_uint8_t_u_u(l_2933, ((**g_1532) = p_19))))) | (-8L)) != l_2933);
            }
            for (g_1968 = 0; (g_1968 <= (-12)); g_1968--)
            { /* block id: 1223 */
                uint64_t l_2955 = 0xC1EC72C255438575LL;
                int32_t l_2990 = 0x977CFEF7L;
                int32_t l_2992 = 0xA2ECBFB9L;
                int32_t l_2994 = (-1L);
                int32_t l_2996 = 0xB47E1CDAL;
                int32_t l_2998 = 1L;
                int32_t l_3001 = 0x56C9A583L;
                int32_t l_3002[6][7][6] = {{{0x8DAD375EL,(-1L),(-5L),(-8L),0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),(-8L),(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),0xC8D8F40BL,0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),0xC8D8F40BL,(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),(-8L),0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),(-8L),(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),0xC8D8F40BL,0xD8370251L,(-1L)}},{{0x338C87ECL,(-1L),(-1L),0xC8D8F40BL,(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),(-8L),0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),(-8L),(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),0xC8D8F40BL,0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),0xC8D8F40BL,(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),(-8L),0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),(-8L),(-1L),(-1L)}},{{0x8DAD375EL,(-1L),(-5L),0xC8D8F40BL,0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),0xC8D8F40BL,(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),(-8L),0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),(-8L),(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),0xC8D8F40BL,0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),0xC8D8F40BL,(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),(-8L),0xD8370251L,(-1L)}},{{0x338C87ECL,(-1L),(-1L),(-8L),(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),0xC8D8F40BL,0xD8370251L,(-1L)},{0x338C87ECL,(-1L),(-1L),0xC8D8F40BL,(-1L),(-1L)},{0x8DAD375EL,(-1L),(-5L),(-8L),0xD8370251L,0xE7E88E1DL},{(-1L),0x120C59CBL,0x7B63E823L,(-1L),0x7B63E823L,0x120C59CBL},{0xD8370251L,0x120C59CBL,0x89FBE6B9L,(-1L),0x90FD2605L,0x120C59CBL},{(-1L),0xE7E88E1DL,0x7B63E823L,(-1L),0x7B63E823L,0xE7E88E1DL}},{{0xD8370251L,0xE7E88E1DL,0x89FBE6B9L,(-1L),0x90FD2605L,0xE7E88E1DL},{(-1L),0x120C59CBL,0x7B63E823L,(-1L),0x7B63E823L,0x120C59CBL},{0xD8370251L,0x120C59CBL,0x89FBE6B9L,(-1L),0x90FD2605L,0x120C59CBL},{(-1L),0xE7E88E1DL,0x7B63E823L,(-1L),0x7B63E823L,0xE7E88E1DL},{0xD8370251L,0xE7E88E1DL,0x89FBE6B9L,(-1L),0x90FD2605L,0xE7E88E1DL},{(-1L),0x120C59CBL,0x7B63E823L,(-1L),0x7B63E823L,0x120C59CBL},{0xD8370251L,0x120C59CBL,0x89FBE6B9L,(-1L),0x90FD2605L,0x120C59CBL}},{{(-1L),0xE7E88E1DL,0x7B63E823L,(-1L),0x7B63E823L,0xE7E88E1DL},{0xD8370251L,0xE7E88E1DL,0x89FBE6B9L,(-1L),0x90FD2605L,0xE7E88E1DL},{(-1L),0x120C59CBL,0x7B63E823L,(-1L),0x7B63E823L,0x120C59CBL},{0xD8370251L,0x120C59CBL,0x89FBE6B9L,(-1L),0x90FD2605L,0x120C59CBL},{(-1L),0xE7E88E1DL,0x7B63E823L,(-1L),0x7B63E823L,0xE7E88E1DL},{0xD8370251L,0xE7E88E1DL,0x89FBE6B9L,(-1L),0x90FD2605L,0xE7E88E1DL},{(-1L),0x120C59CBL,0x7B63E823L,(-1L),0x7B63E823L,0x120C59CBL}}};
                int32_t **l_3009 = &l_2777;
                int i, j, k;
            }
            if (p_19)
            { /* block id: 1262 */
                float *l_3012 = &g_203;
                float *l_3013 = (void*)0;
                float *l_3014[10][1] = {{&g_1185},{&g_213},{&g_1185},{&g_1185},{&g_213},{&g_1185},{&g_1185},{&g_213},{&g_1185},{&g_1185}};
                int i, j;
                l_2940[0] = (safe_add_func_float_f_f(((*l_3012) = ((*g_427) = (((void*)0 != &g_87) != (*l_2777)))), p_19));
            }
            else
            { /* block id: 1266 */
                uint16_t l_3015 = 1UL;
                (*l_2777) = l_3015;
            }
        }
        (*l_2777) &= ((*g_771) = 0x84D669DAL);
        (*g_84) |= (((*l_2777) = ((&g_1477[4][1][1] != l_3016[2][0]) <= (++(*l_2923)))) != (l_2995[4][0][0] ^= (!((safe_rshift_func_uint8_t_u_u(((**g_1077) = (safe_lshift_func_uint16_t_u_s(((safe_mod_func_uint16_t_u_u(65527UL, p_19)) , (g_87 = p_19)), (((*g_566) > ((p_19 , &l_2704[1][0]) != &l_2923)) == g_2817[0][7])))), p_19)) >= p_19))));
    }
    else
    { /* block id: 1278 */
lbl_3030:
        l_3029 = &g_1530[1];
        (*l_2777) = l_3031[4][3][0];
        (*g_84) = (((safe_mul_func_int8_t_s_s((l_3034 != ((*l_3035) = l_3034)), (l_3036 == (safe_lshift_func_int8_t_s_u(((safe_mod_func_int64_t_s_s(((g_1824 != l_3041) , ((void*)0 == l_3043[3])), l_3044[5][1])) | ((((*l_3017) = (((safe_mul_func_int8_t_s_s(0xF7L, 0xB5L)) & 0x24L) , &g_30[5][1])) == (void*)0) || 18446744073709551615UL)), l_3047))))) , 0xB93D4EF9L) , 0x15A5DDB5L);
    }
    for (g_855 = 21; (g_855 == (-21)); g_855 = safe_sub_func_uint32_t_u_u(g_855, 5))
    { /* block id: 1288 */
        (*g_84) ^= (*l_2777);
        (*g_427) = 0x6.001A17p+61;
    }
    return p_19;
}


/* ------------------------------------------ */
/* 
 * reads : g_459 g_798 g_44 g_426 g_427 g_13 g_104 g_1530 g_771 g_142 g_304 g_92 g_751 g_1824 g_980 g_106 g_1832 g_303 g_1554 g_1582 g_566 g_160 g_192 g_84 g_1077 g_1078 g_266 g_46 g_30 g_269 g_578 g_1581 g_467 g_2028 g_1968 g_1532 g_979 g_694 g_1478 g_2124 g_2144 g_2122 g_617 g_468 g_2290 g_1926 g_898 g_105 g_132 g_113 g_2380 g_2635
 * writes: g_459 g_190 g_44 g_269 g_30 g_142 g_92 g_751 g_798 g_46 g_2028 g_106 g_304 g_213 g_467 g_2122 g_1582 g_2124 g_160 g_132 g_694 g_617 g_468 g_111 g_1968 g_105 g_192 g_2380 g_802 g_898
 */
static uint32_t  func_20(uint16_t  p_21)
{ /* block id: 11 */
    int32_t *l_23 = &g_13;
    int32_t * const *l_22 = &l_23;
    int8_t *l_79 = &g_30[6][0];
    int64_t l_1186 = 0xEBA658CDACF5D0EDLL;
    int8_t l_1187 = 9L;
    int32_t l_1436 = 0xACF87AC5L;
    int32_t l_1437 = 0x73EFA9B1L;
    int32_t l_1438 = 1L;
    uint64_t l_1459 = 0x2E864DD7E9E57C2DLL;
    const uint8_t l_1501[9][9][3] = {{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}},{{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L},{0x24L,0x24L,0x24L}}};
    uint64_t **l_1504 = &g_980;
    uint8_t ** const *l_1534[8] = {&g_1077,&g_1077,&g_1077,&g_1077,&g_1077,&g_1077,&g_1077,&g_1077};
    uint8_t ** const **l_1533[3][4] = {{(void*)0,&l_1534[7],(void*)0,(void*)0},{&l_1534[4],&l_1534[4],(void*)0,(void*)0},{&l_1534[7],&l_1534[7],&l_1534[7],(void*)0}};
    uint64_t l_1541 = 0x3FCB5B933049E2E8LL;
    int8_t l_1542[2][1];
    uint32_t **l_1579 = (void*)0;
    uint32_t ***l_1578[7] = {&l_1579,&l_1579,&l_1579,&l_1579,&l_1579,&l_1579,&l_1579};
    uint32_t ***l_1583[8] = {&l_1579,&l_1579,&l_1579,&l_1579,&l_1579,&l_1579,&l_1579,&l_1579};
    uint32_t l_1652 = 1UL;
    float ***l_1773 = &g_426[0][0][1];
    float ****l_1772 = &l_1773;
    int64_t **l_1780 = (void*)0;
    int64_t *** const l_1779 = &l_1780;
    int32_t l_1808[3][9] = {{(-1L),0xB5EC15C5L,(-1L),0xB5EC15C5L,(-1L),0xB5EC15C5L,(-1L),0xB5EC15C5L,(-1L)},{7L,7L,7L,7L,7L,7L,7L,7L,7L},{(-1L),0xB5EC15C5L,(-1L),0xB5EC15C5L,(-1L),0xB5EC15C5L,(-1L),0xB5EC15C5L,(-1L)}};
    int64_t l_1817 = 0x90F9330847939F24LL;
    int16_t l_1833[3][9] = {{0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL},{0xCEE3L,0xCEE3L,0xCEE3L,0xCEE3L,0xCEE3L,0xCEE3L,0xCEE3L,0xCEE3L,0xCEE3L},{0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL,0x834CL}};
    uint32_t l_1834 = 0x076C4999L;
    int32_t l_1966[3];
    uint32_t l_2010[2][8][4] = {{{0xBF3F3CE7L,0x38D976E1L,6UL,4294967295UL},{0xBF3F3CE7L,6UL,0xBF3F3CE7L,0xCD875119L},{0x38D976E1L,4294967295UL,0xCD875119L,0xCD875119L},{6UL,6UL,0x4E07336DL,4294967295UL},{4294967295UL,0x38D976E1L,0x4E07336DL,0x38D976E1L},{6UL,0xBF3F3CE7L,0xCD875119L,0x4E07336DL},{0x38D976E1L,0xBF3F3CE7L,0xBF3F3CE7L,0x38D976E1L},{0xBF3F3CE7L,0x38D976E1L,6UL,4294967295UL}},{{0xBF3F3CE7L,6UL,0xBF3F3CE7L,0xCD875119L},{0x38D976E1L,4294967295UL,0xCD875119L,0xCD875119L},{6UL,6UL,0x4E07336DL,4294967295UL},{4294967295UL,0x38D976E1L,0x4E07336DL,0x38D976E1L},{6UL,0xBF3F3CE7L,0xCD875119L,0x4E07336DL},{0x38D976E1L,0xBF3F3CE7L,0xBF3F3CE7L,0x38D976E1L},{0xBF3F3CE7L,0x38D976E1L,6UL,4294967295UL},{0xBF3F3CE7L,6UL,0xBF3F3CE7L,0xCD875119L}}};
    uint32_t *l_2039 = &g_1120;
    uint32_t **l_2038 = &l_2039;
    const uint32_t *l_2040 = &g_1120;
    uint16_t *l_2069 = &g_92;
    uint32_t l_2088 = 0x8E393B28L;
    float l_2089 = 0xB.CB9713p+64;
    int8_t l_2108 = 0x45L;
    float l_2181 = (-0x10.Dp+1);
    int32_t l_2299 = 0x6237DDC4L;
    uint64_t l_2320 = 0UL;
    volatile float * volatile ** volatile **l_2363 = &g_1700;
    uint32_t *l_2400 = (void*)0;
    uint32_t l_2452 = 7UL;
    int16_t **l_2563 = &g_2028;
    uint64_t * const *l_2591 = (void*)0;
    uint64_t * const **l_2590 = &l_2591;
    uint64_t * const ***l_2589[2][6][8] = {{{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590}},{{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590},{&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590,&l_2590}}};
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_1542[i][j] = 5L;
    }
    for (i = 0; i < 3; i++)
        l_1966[i] = 0x6FC587A0L;
    if (((void*)0 != l_22))
    { /* block id: 12 */
        int8_t * const l_61 = (void*)0;
        int32_t l_90 = 0L;
        int32_t **l_93 = &l_23;
        uint16_t l_1414 = 0x55FEL;
        int16_t l_1421 = 0xB4A6L;
        int32_t ***l_1428 = &g_113[7][3];
        uint32_t **l_1443 = &g_566;
        int32_t l_1449 = 0xAF8B12DFL;
        const int8_t **l_1479 = &g_1478;
        const int64_t l_1511 = (-1L);
        int32_t l_1560 = 8L;
        int32_t l_1561 = 0x0BC9756DL;
        int32_t l_1565 = 0xF1E3217EL;
        int32_t l_1603[1];
        uint16_t **l_1664 = &g_578;
        int64_t l_1775 = 0x8B4875813F24372DLL;
        const uint32_t *l_1797 = &g_160[7];
        const uint32_t **l_1796 = &l_1797;
        const uint32_t ***l_1795 = &l_1796;
        const uint32_t ****l_1794 = &l_1795;
        int i;
        for (i = 0; i < 1; i++)
            l_1603[i] = (-3L);
        for (p_21 = 0; (p_21 > 11); p_21 = safe_add_func_uint16_t_u_u(p_21, 4))
        { /* block id: 15 */
            int8_t *l_28 = (void*)0;
            int8_t *l_29[1][6] = {{&g_30[6][0],&g_30[6][0],&g_30[6][0],&g_30[6][0],&g_30[6][0],&g_30[6][0]}};
            int32_t l_31 = 0xD6937652L;
            int32_t *l_42 = (void*)0;
            int32_t *l_43 = &g_44;
            int32_t *l_45 = &g_46;
            uint16_t *l_91 = &g_92;
            int32_t *l_1395 = &g_1396[4][0][5];
            int32_t *l_1434 = &g_104;
            float l_1439 = 0xD.195251p+26;
            int32_t l_1450 = 0xFB71C5DAL;
            int32_t l_1452 = (-1L);
            int32_t l_1453 = 0xBAF5F073L;
            int32_t l_1454 = (-4L);
            int32_t l_1458 = (-1L);
            uint64_t ***l_1503[2];
            int i, j;
            for (i = 0; i < 2; i++)
                l_1503[i] = (void*)0;
        }
        for (g_459 = (-12); (g_459 > 54); g_459 = safe_add_func_uint32_t_u_u(g_459, 1))
        { /* block id: 638 */
            uint8_t ***l_1517 = &g_1077;
            uint8_t *** const *l_1516 = &l_1517;
            uint32_t l_1535 = 0UL;
            int16_t *l_1536 = &g_105;
            uint16_t *l_1540[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            int32_t l_1556 = 0x211DBAC8L;
            int32_t l_1562 = 0xD74B5BC7L;
            int32_t *l_1625 = &g_104;
            int32_t ** const l_1624 = &l_1625;
            int32_t l_1651 = 0x39BA01EFL;
            uint16_t * const *l_1765 = (void*)0;
            uint16_t * const **l_1764 = &l_1765;
            uint32_t *l_1774[9][7][2] = {{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}},{{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120},{&g_1120,&g_1120}}};
            int i, j, k;
        }
        for (g_459 = 0; (g_459 >= 58); g_459 = safe_add_func_uint32_t_u_u(g_459, 6))
        { /* block id: 758 */
            uint32_t l_1787[1];
            int i;
            for (i = 0; i < 1; i++)
                l_1787[i] = 8UL;
            for (g_190 = 0; (g_190 <= 4); g_190 += 1)
            { /* block id: 761 */
                uint32_t l_1778 = 1UL;
                int32_t l_1781[6] = {7L,7L,7L,7L,7L,7L};
                int32_t *l_1798 = &l_1781[4];
                int i;
                (*g_798) &= l_1778;
                for (p_21 = 0; (p_21 <= 4); p_21 += 1)
                { /* block id: 765 */
                    int64_t l_1782 = (-1L);
                    int32_t l_1785 = (-4L);
                    int32_t l_1786 = 0L;
                    for (l_1459 = 0; (l_1459 <= 4); l_1459 += 1)
                    { /* block id: 768 */
                        int16_t l_1783 = (-3L);
                        int32_t *l_1784[1][2];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_1784[i][j] = &l_1561;
                        }
                        (***l_1773) = ((void*)0 != l_1779);
                        if (p_21)
                            break;
                        if (p_21)
                            break;
                        l_1787[0]--;
                    }
                }
                (*g_771) = (((*l_79) = ((p_21 >= ((0x1023L == p_21) , (1L ^ (**l_93)))) >= ((safe_mul_func_uint16_t_u_u(((((safe_rshift_func_int8_t_s_u(((0x82C1L || (l_1794 == (((g_104 , g_1530[0]) == g_1530[0]) , &l_1795))) | 4L), 6)) <= (-2L)) > 0xAFL) == (*l_23)), 0x34B3L)) ^ p_21))) && p_21);
                l_1798 = ((*l_93) = (((void*)0 != &g_1581) , &l_1438));
                for (g_142 = 0; (g_142 <= 1); g_142 += 1)
                { /* block id: 781 */
                    for (l_1449 = 4; (l_1449 >= 0); l_1449 -= 1)
                    { /* block id: 784 */
                        if (p_21)
                            break;
                    }
                }
            }
        }
        return g_304[0][6];
    }
    else
    { /* block id: 791 */
        int8_t l_1799 = 8L;
        int32_t l_1802 = (-1L);
        int32_t l_1803 = (-6L);
        int32_t l_1804 = 0xFB9FEE9DL;
        int32_t l_1805 = (-1L);
        int32_t l_1806 = 0L;
        int32_t l_1807 = 0x6C8BE73BL;
        int32_t l_1809 = 0x5B2D3A82L;
        float l_1810 = 0xF.742F19p+91;
        int32_t l_1811 = (-1L);
        int32_t l_1812 = 0x528388AEL;
        int32_t l_1813[10] = {6L,9L,6L,0x8C5C18DCL,0x8C5C18DCL,6L,9L,6L,0x8C5C18DCL,0x8C5C18DCL};
        uint32_t l_1814 = 0UL;
        int32_t ***l_1845 = &g_113[3][1];
        uint16_t *l_1881 = &g_87;
        uint64_t l_1916 = 0x8267984C2DB32C96LL;
        int32_t l_1967 = 0L;
        int64_t l_2008 = 0x1A0F9CAA1BB7B3C5LL;
        int16_t *l_2009 = &l_1833[0][1];
        int32_t **l_2011 = &l_23;
        int i;
        for (g_92 = 0; (g_92 <= 4); g_92 += 1)
        { /* block id: 794 */
            int32_t *l_1800 = &g_1396[4][0][5];
            int32_t *l_1801[9][9] = {{&g_855,(void*)0,&g_1396[4][0][5],&g_46,&g_1396[2][0][5],&g_1396[3][0][2],&g_1396[4][0][5],&g_855,&g_1396[4][0][5]},{&g_1396[4][0][5],&g_46,&l_1437,&g_44,&g_46,&g_855,&g_46,&g_44,&l_1437},{&l_1437,&l_1437,&g_855,&g_1396[4][0][5],(void*)0,&g_46,&g_46,&g_6,&g_1396[2][0][5]},{&g_1396[3][0][2],&g_44,(void*)0,(void*)0,&g_1396[2][0][5],&g_1396[4][0][5],&g_1396[4][0][5],&g_1396[2][0][5],(void*)0},{&g_855,&g_46,&g_855,(void*)0,&g_855,&g_44,&g_46,&g_1396[2][0][5],(void*)0},{&g_46,(void*)0,&l_1437,(void*)0,&g_46,&g_6,(void*)0,&g_6,&g_46},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_6,&g_6,&g_44,&g_46},{&g_44,&g_855,&g_46,(void*)0,&g_1396[4][0][5],(void*)0,&g_855,&g_855,(void*)0},{(void*)0,&g_1396[4][0][5],(void*)0,&g_1396[4][0][5],(void*)0,&g_44,&g_855,&g_46,(void*)0}};
            const float **** const *l_1825 = (void*)0;
            float l_1839[2][10] = {{(-0x5.Fp+1),0x8.Cp+1,0x8.Cp+1,(-0x5.Fp+1),0x2.7BBDF4p-99,0x7.423F32p+74,(-0x5.Fp+1),0x7.423F32p+74,0x2.7BBDF4p-99,(-0x5.Fp+1)},{0x7.423F32p+74,(-0x5.Fp+1),0x7.423F32p+74,0x2.7BBDF4p-99,(-0x5.Fp+1),0x8.Cp+1,0x8.Cp+1,(-0x5.Fp+1),0x2.7BBDF4p-99,0x7.423F32p+74}};
            int32_t l_1859 = (-1L);
            int64_t l_1871 = 0xF1FB6213C97DAF52LL;
            uint32_t *l_1925 = &g_1109;
            int i, j;
            ++l_1814;
            l_1817 &= 0xDFA27114L;
            for (g_751 = 1; (g_751 <= 7); g_751 += 1)
            { /* block id: 799 */
                const float **** const l_1827 = (void*)0;
                const float **** const *l_1826 = &l_1827;
                int32_t l_1838 = 4L;
                int32_t l_1840 = 0L;
                int32_t l_1841 = 0x6504AAC1L;
                float l_1870 = 0x1.Dp+1;
                uint32_t **l_1878 = (void*)0;
                uint32_t l_1882 = 0xC0D3251DL;
                int32_t l_1913 = 1L;
                int32_t l_1915 = 1L;
                int i, j;
                l_1834 = (safe_add_func_uint32_t_u_u((((safe_div_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(((g_1824 == (void*)0) , (((&l_1813[2] != (*l_22)) , &g_1700) != (l_1826 = l_1825))), (((*l_79) = (((*g_980) >= (safe_rshift_func_uint8_t_u_u(p_21, 4))) > ((safe_rshift_func_int16_t_s_u((((l_1804 |= ((((void*)0 == &g_1538) ^ 0UL) <= 4UL)) != p_21) , p_21), l_1813[6])) != 0x17A2D12FL))) <= 0x71L))) , g_1832[4]), 0x83L)) | (**l_22)) | l_1833[0][1]), 0L));
            }
        }
        (*l_2011) = func_53(((safe_div_func_uint32_t_u_u((((((void*)0 == &g_979) , ((*l_79) = (!(safe_sub_func_int32_t_s_s(((safe_rshift_func_int16_t_s_u((((((-6L) | (((safe_rshift_func_uint16_t_u_s(((((((**l_22) != 2UL) < (safe_mod_func_uint8_t_u_u(((**g_1077) ^= (0xF38DL != ((safe_lshift_func_int16_t_s_u(((*l_2009) = (((safe_sub_func_uint32_t_u_u((0x770CA6FF1B2C49EBLL <= (((safe_add_func_int8_t_s_s(p_21, ((*g_303) == l_2008))) , (*g_1554)) == (*g_303))), (**g_1582))) && g_192) == (*g_84))), 0)) , (-1L)))), (**l_22)))) <= 0xF2A2L) , 0xE4L) > (**l_22)), 4)) , &g_142) == (void*)0)) && 0xF6909412D0F0C283LL) || p_21) || l_1814), p_21)) <= g_266), p_21))))) <= 0L) | p_21), l_2010[1][0][3])) == p_21));
    }
    (*g_771) &= p_21;
    if ((safe_unary_minus_func_uint8_t_u((0x44L && (safe_rshift_func_int16_t_s_s(((((safe_rshift_func_uint16_t_u_s(((safe_lshift_func_int16_t_s_s((((*l_2038) = ((((*g_303) = ((safe_lshift_func_uint8_t_u_u((safe_add_func_uint8_t_u_u((~((safe_sub_func_uint64_t_u_u(0xE0F0A361EF09F44DLL, (safe_div_func_int16_t_s_s(g_30[6][0], (((&g_1968 == (g_2028 = &l_1833[0][1])) , (safe_div_func_int64_t_s_s(((((((*g_427) , ((safe_lshift_func_uint16_t_u_s((p_21 <= (*g_578)), 3)) & (+((**l_1504) = (safe_rshift_func_uint16_t_u_s(((safe_div_func_uint32_t_u_u(p_21, p_21)) > 0L), 4)))))) != 249UL) == 0xE95D6CADL) | (*g_578)) , 0xC66CA4D719F153A9LL), l_1542[1][0]))) ^ 0x8F7EL))))) != p_21)), p_21)), 2)) || (***g_1581))) < 18446744073709551615UL) , l_23)) == l_2040), 2)) < g_467), 3)) , p_21) , 0x7AL) ^ p_21), 12))))))
    { /* block id: 899 */
        int32_t ** const * const l_2049 = &g_1835[2][1][3];
        const int32_t l_2056 = (-5L);
        const uint16_t *l_2068 = &g_92;
        float *l_2090 = &g_213;
        float *l_2091 = &l_2089;
        (*g_771) = (safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((((safe_mod_func_uint64_t_u_u(((&g_1835[2][1][2] == l_2049) <= ((safe_add_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(l_2056, (safe_mul_func_uint16_t_u_u((((*g_578)++) , (p_21 , (*g_578))), ((0x4148L > (-1L)) == ((~((*l_2069) = (((safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((((safe_rshift_func_uint16_t_u_u(p_21, 0)) | (((l_2068 != l_2069) ^ 0UL) & (*g_2028))) != 0x1402L), (*g_1078))), 0x79ECL)) , 0xAAFC6B60L) >= 9UL))) <= (*g_303))))))), (**g_1077))), 0xE91AL)) < l_2056)), p_21)) , 0xCC436DB15F81A5DALL) < l_2056), p_21)), p_21));
        (*l_2091) = (safe_div_func_float_f_f((p_21 , ((*l_2090) = (((***l_1773) = ((*g_427) < (((p_21 > p_21) , &g_1538) != (void*)0))) <= ((!(((safe_rshift_func_int8_t_s_u(((safe_sub_func_int8_t_s_s((((+(*g_566)) == ((*l_79) = ((safe_mod_func_uint64_t_u_u(((**g_979) = (safe_sub_func_uint16_t_u_u((((((((*g_578) = ((safe_mul_func_uint8_t_u_u(((**g_1532) = ((safe_div_func_int64_t_s_s(((l_2056 > (l_2056 >= l_2088)) | 0L), (*g_303))) , p_21)), l_1436)) | 0x3FL)) ^ 0x67BCL) , 0x5FE2L) , (*g_303)) >= (*g_303)) == 0UL), l_2056))), 0xEAEDA024E5DFE674LL)) > l_2056))) != p_21), l_2056)) != 4294967295UL), 4)) & p_21) ^ 0xD91E5831L)) , p_21)))), 0x0.5p-1));
    }
    else
    { /* block id: 910 */
        uint8_t l_2100 = 0x0FL;
        int16_t *l_2107 = &g_467;
        float *l_2109 = &l_2089;
        float *l_2110 = &g_213;
        int32_t l_2113[8][6] = {{1L,1L,(-3L),1L,1L,(-3L)},{1L,1L,(-3L),1L,1L,(-3L)},{1L,1L,(-3L),1L,1L,(-3L)},{1L,1L,(-3L),1L,1L,(-3L)},{1L,1L,(-3L),1L,1L,(-3L)},{1L,1L,(-3L),1L,1L,(-3L)},{1L,1L,(-3L),1L,1L,(-3L)},{1L,1L,(-3L),1L,1L,(-3L)}};
        float ***l_2119[7] = {&g_426[0][0][1],&g_426[0][0][1],&g_426[0][0][1],&g_426[0][0][1],&g_426[0][0][1],&g_426[0][0][1],&g_426[0][0][1]};
        float ***l_2121 = &g_426[0][1][5];
        float ****l_2120[3][10][4] = {{{&l_2121,&l_2121,&l_2121,(void*)0},{&l_2121,(void*)0,&l_2121,&l_2121},{&l_2121,(void*)0,&l_2121,(void*)0},{&l_2121,&l_2121,&l_2121,(void*)0},{&l_2121,(void*)0,&l_2121,&l_2121},{&l_2121,(void*)0,&l_2121,(void*)0},{&l_2121,&l_2121,&l_2121,(void*)0},{&l_2121,(void*)0,&l_2121,&l_2121},{&l_2121,(void*)0,&l_2121,(void*)0},{&l_2121,&l_2121,&l_2121,(void*)0}},{{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121}},{{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121},{&l_2121,&l_2121,&l_2121,&l_2121}}};
        uint32_t **l_2123[1][6][8] = {{{&g_1926,&g_1926,&g_1926,(void*)0,(void*)0,&g_1926,&g_1926,&g_1926},{&g_566,&g_1926,&g_566,&g_1926,&g_566,&g_1926,&g_566,&g_566},{&g_1926,(void*)0,&g_566,(void*)0,&g_566,(void*)0,&g_1926,&g_1926},{&g_1926,&g_566,&g_566,&g_566,&g_566,&g_1926,&g_566,&g_566},{&g_566,&g_1926,&g_566,&g_566,&g_1926,(void*)0,&g_1926,&g_1926},{&g_566,&g_1926,&g_1926,&g_566,&g_1926,&g_1926,&g_566,&g_566}}};
        int32_t ***l_2150 = &g_113[5][3];
        int8_t *l_2243 = &l_1542[1][0];
        int64_t *l_2423 = &l_1817;
        uint32_t l_2430 = 18446744073709551607UL;
        float l_2449 = 0x4.9A10EBp+30;
        int32_t *l_2488 = (void*)0;
        int16_t l_2520 = (-1L);
        const uint64_t *l_2588[2];
        const uint64_t **l_2587 = &l_2588[0];
        const uint64_t ***l_2586[10][4][3] = {{{&l_2587,(void*)0,&l_2587},{(void*)0,&l_2587,&l_2587},{(void*)0,(void*)0,&l_2587},{(void*)0,(void*)0,(void*)0}},{{&l_2587,&l_2587,&l_2587},{&l_2587,(void*)0,&l_2587},{&l_2587,&l_2587,&l_2587},{&l_2587,&l_2587,&l_2587}},{{&l_2587,&l_2587,&l_2587},{&l_2587,&l_2587,(void*)0},{&l_2587,&l_2587,&l_2587},{&l_2587,(void*)0,&l_2587}},{{&l_2587,&l_2587,&l_2587},{&l_2587,&l_2587,&l_2587},{&l_2587,&l_2587,&l_2587},{&l_2587,&l_2587,(void*)0}},{{&l_2587,&l_2587,&l_2587},{&l_2587,(void*)0,(void*)0},{(void*)0,&l_2587,(void*)0},{(void*)0,&l_2587,&l_2587}},{{(void*)0,&l_2587,(void*)0},{&l_2587,(void*)0,(void*)0},{&l_2587,(void*)0,(void*)0},{&l_2587,&l_2587,&l_2587}},{{&l_2587,(void*)0,&l_2587},{&l_2587,&l_2587,&l_2587},{&l_2587,&l_2587,&l_2587},{(void*)0,(void*)0,&l_2587}},{{(void*)0,&l_2587,&l_2587},{&l_2587,(void*)0,&l_2587},{(void*)0,(void*)0,&l_2587},{&l_2587,&l_2587,&l_2587}},{{(void*)0,&l_2587,&l_2587},{&l_2587,&l_2587,&l_2587},{&l_2587,&l_2587,&l_2587},{(void*)0,&l_2587,&l_2587}},{{&l_2587,&l_2587,(void*)0},{(void*)0,&l_2587,(void*)0},{&l_2587,&l_2587,&l_2587},{(void*)0,&l_2587,(void*)0}}};
        const uint64_t ****l_2585 = &l_2586[1][0][0];
        uint16_t **l_2669 = &g_578;
        uint64_t l_2682[6][9] = {{18446744073709551610UL,0x99D08253E9C7D38CLL,18446744073709551610UL,0xCD710A78536657B7LL,0x7AC3E4CD29A4E533LL,0x8E7595150329AF88LL,0x200F242D7324796ALL,0UL,0UL},{0UL,0x99D08253E9C7D38CLL,0UL,0x7AC3E4CD29A4E533LL,0UL,0x99D08253E9C7D38CLL,0UL,0x200F242D7324796ALL,18446744073709551615UL},{0x200F242D7324796ALL,0x8E7595150329AF88LL,0x7AC3E4CD29A4E533LL,0xCD710A78536657B7LL,18446744073709551610UL,0x99D08253E9C7D38CLL,18446744073709551610UL,0xCD710A78536657B7LL,0x7AC3E4CD29A4E533LL},{0x7AC3E4CD29A4E533LL,0x7AC3E4CD29A4E533LL,18446744073709551615UL,0UL,18446744073709551615UL,0x8E7595150329AF88LL,0x99D08253E9C7D38CLL,0x200F242D7324796ALL,0x99D08253E9C7D38CLL},{0x7AC3E4CD29A4E533LL,0UL,0x8E7595150329AF88LL,0x8E7595150329AF88LL,0UL,0x7AC3E4CD29A4E533LL,0UL,0UL,18446744073709551615UL},{0x200F242D7324796ALL,0xCD710A78536657B7LL,18446744073709551615UL,18446744073709551615UL,0UL,0UL,18446744073709551615UL,18446744073709551615UL,0xCD710A78536657B7LL}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2588[i] = &l_1459;
        (*l_2110) = ((*l_2109) = (safe_div_func_float_f_f((safe_sub_func_float_f_f((p_21 <= ((safe_mul_func_float_f_f((safe_mul_func_float_f_f((((*g_2028) , (((*g_427) = l_2100) >= l_2100)) >= ((safe_add_func_int32_t_s_s(p_21, ((1UL < ((*l_2107) = ((l_2100 == p_21) , (safe_lshift_func_uint8_t_u_s(((safe_rshift_func_int8_t_s_s((((*g_694) ^ (*g_566)) , 3L), 2)) < 0x38A0L), (*g_1478)))))) > (-1L)))) , p_21)), l_2108)), l_2100)) >= l_2100)), p_21)), p_21)));
        if (((*g_84) |= (safe_rshift_func_int16_t_s_s((((*g_303) ^ (l_2113[5][2] = p_21)) != (safe_lshift_func_int8_t_s_u((-4L), 2))), ((safe_sub_func_uint8_t_u_u(0x27L, (+(((*l_1772) = (l_2119[2] = l_2119[3])) != (g_2122 = &g_426[0][0][1]))))) < p_21)))))
        { /* block id: 920 */
            const uint32_t ***l_2126 = (void*)0;
            const uint32_t ***l_2127 = &g_2124;
            int32_t l_2137[3];
            int16_t *l_2147[10] = {&g_1968,&g_1968,&g_1968,&g_1968,&g_1968,&g_1968,&g_1968,&g_1968,&g_1968,&g_1968};
            uint64_t ***l_2215 = (void*)0;
            int32_t l_2246[7] = {5L,1L,5L,5L,1L,5L,5L};
            int i;
            for (i = 0; i < 3; i++)
                l_2137[i] = 1L;
            if (((((((*g_1581) = l_2123[0][2][3]) == ((*l_2127) = g_2124)) || (*g_2028)) != (safe_rshift_func_uint16_t_u_u(((*g_980) , (safe_sub_func_uint32_t_u_u(((*g_566) |= (p_21 , (safe_add_func_uint64_t_u_u((safe_unary_minus_func_int32_t_s(p_21)), ((safe_rshift_func_uint8_t_u_u((**g_1532), 2)) <= l_2137[2]))))), (safe_mul_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_s((safe_add_func_uint64_t_u_u((*g_980), p_21)), 2)) , g_2144), p_21))))), 8))) >= p_21))
            { /* block id: 924 */
                int64_t l_2145[6][6][7] = {{{0x9EE0098A10954E60LL,0x38B5F5638EF33003LL,1L,(-8L),0xDCC30C7A34217A90LL,0x09E78242E24C8E73LL,0xD7A161598D587BE9LL},{1L,0xEF821A65B4321F30LL,0x809CCBD35F74CA15LL,0x2DEF6FED64E93922LL,(-1L),(-9L),0x5489272C5F27B14BLL},{(-1L),0xD1BEBD800ECC638ELL,0x38B5F5638EF33003LL,0x38B5F5638EF33003LL,0xD1BEBD800ECC638ELL,(-1L),(-2L)},{0L,1L,0x647EFAACF0444300LL,0x775E9B1F18D7AD66LL,0x87459847A3C57ECDLL,0x2DEF6FED64E93922LL,0x7B5988150C8E4B81LL},{1L,0x25A04BC57448400DLL,(-1L),(-1L),1L,1L,1L},{0xED9C64CE3F9EF728LL,1L,(-7L),0x2E492C89E818F108LL,0L,0x3C2763338AB4B3AELL,0L}},{{0x25A04BC57448400DLL,0xD1BEBD800ECC638ELL,0x535040BA3D91CFD2LL,0x23AF2C5F8BAB19E9LL,(-2L),(-8L),(-1L)},{7L,0xEF821A65B4321F30LL,0x933FFCEC1639904ELL,0x647EFAACF0444300LL,0x647EFAACF0444300LL,0x933FFCEC1639904ELL,0xEF821A65B4321F30LL},{0x8E96D0BBCC8F6DDFLL,0x38B5F5638EF33003LL,0L,0x291CD51B5FEAEC36LL,0x25A04BC57448400DLL,0xE278E60097B08D92LL,0x8682E495F6428F03LL},{0x2E492C89E818F108LL,0xEF821A65B4321F30LL,0x87459847A3C57ECDLL,0L,2L,0L,0x933FFCEC1639904ELL},{0xA3A52AAE0EA7F939LL,0xE278E60097B08D92LL,(-1L),0L,(-8L),0xF777974D9F5B5AF8LL,0x9EE0098A10954E60LL},{1L,0x933FFCEC1639904ELL,(-3L),0xED9C64CE3F9EF728LL,0xEF821A65B4321F30LL,0L,0xE0BA43E344A33D4ALL}},{{0x25A04BC57448400DLL,(-8L),1L,0xF711231AE11E7A15LL,0xDCC30C7A34217A90LL,0xDCC30C7A34217A90LL,0xF711231AE11E7A15LL},{5L,0x3C2763338AB4B3AELL,5L,1L,0x5489272C5F27B14BLL,9L,0x809CCBD35F74CA15LL},{0xDCC30C7A34217A90LL,1L,0xA3A52AAE0EA7F939LL,0x23AF2C5F8BAB19E9LL,0x2F273096ACB86A2DLL,0x291CD51B5FEAEC36LL,0xE278E60097B08D92LL},{(-7L),0x2DEF6FED64E93922LL,2L,(-1L),0L,9L,0x2CD5D47864AA7CACLL},{0L,(-1L),0x535040BA3D91CFD2LL,0x25A04BC57448400DLL,(-1L),0xDCC30C7A34217A90LL,(-4L)},{0L,(-9L),0x2CD5D47864AA7CACLL,2L,0L,0L,0L}},{{(-4L),0x09E78242E24C8E73LL,1L,0x09E78242E24C8E73LL,(-4L),0xF777974D9F5B5AF8LL,0x38B5F5638EF33003LL},{0x5489272C5F27B14BLL,(-1L),0L,0L,0x3C2763338AB4B3AELL,0L,0x2E492C89E818F108LL},{1L,(-2L),(-4L),0L,1L,0xD1BEBD800ECC638ELL,0L},{0x5489272C5F27B14BLL,0L,(-9L),0L,0L,1L,0x5FD1FB68C75B6427LL},{(-4L),1L,0L,0xDCC30C7A34217A90LL,0x8FA185889229120ALL,0x09E78242E24C8E73LL,1L},{0L,0xCEE4B3815F00E4ECLL,0L,0x6686F7B1440F9A95LL,0L,0xCEE4B3815F00E4ECLL,0L}},{{0L,(-4L),6L,0x8682E495F6428F03LL,(-1L),(-1L),0x535040BA3D91CFD2LL},{(-7L),0x6FD59A5B737B9467LL,0x809CCBD35F74CA15LL,5L,9L,2L,0xCEE4B3815F00E4ECLL},{0xDCC30C7A34217A90LL,0x95348B348BF20BDBLL,6L,(-2L),0xF711231AE11E7A15LL,1L,2L},{5L,0x597973FCE10487C7LL,0L,0x2CD5D47864AA7CACLL,0x7B5988150C8E4B81LL,0x809CCBD35F74CA15LL,0xD492D71728409E17LL},{0x25A04BC57448400DLL,0x291CD51B5FEAEC36LL,0L,0x38B5F5638EF33003LL,0x8E96D0BBCC8F6DDFLL,0x38B5F5638EF33003LL,0L},{1L,1L,(-9L),0x6FD59A5B737B9467LL,0L,0x647EFAACF0444300LL,5L}},{{0xA3A52AAE0EA7F939LL,(-1L),(-4L),0x8E96D0BBCC8F6DDFLL,(-2L),(-1L),(-8L)},{0x2E492C89E818F108LL,7L,0L,0x3C2763338AB4B3AELL,0L,(-7L),0L},{0xD7A161598D587BE9LL,0x8682E495F6428F03LL,1L,0xA3A52AAE0EA7F939LL,0x8E96D0BBCC8F6DDFLL,0x535040BA3D91CFD2LL,0x09E78242E24C8E73LL},{0x2DEF6FED64E93922LL,0x5489272C5F27B14BLL,0x2CD5D47864AA7CACLL,0xEF821A65B4321F30LL,0x7B5988150C8E4B81LL,0x933FFCEC1639904ELL,0x7B5988150C8E4B81LL},{0xF777974D9F5B5AF8LL,0x535040BA3D91CFD2LL,0x535040BA3D91CFD2LL,0xF777974D9F5B5AF8LL,0xF711231AE11E7A15LL,0L,0x95348B348BF20BDBLL},{0xD492D71728409E17LL,0x2E492C89E818F108LL,2L,(-7L),9L,0L,0x87459847A3C57ECDLL}}};
                int32_t * const l_2151 = (void*)0;
                uint8_t l_2182[6] = {250UL,5UL,250UL,5UL,5UL,250UL};
                int32_t *l_2183 = &g_855;
                uint8_t **l_2185 = &g_1078;
                int i, j, k;
                if (l_2145[3][1][2])
                { /* block id: 925 */
                    uint64_t **l_2146 = &g_1554;
                    int32_t l_2184[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_2184[i] = 0x77220B8AL;
                    if ((l_2146 == (void*)0))
                    { /* block id: 926 */
                        int16_t **l_2148 = (void*)0;
                        int16_t **l_2149 = (void*)0;
                        l_2113[5][2] = p_21;
                        (*g_771) |= ((g_2028 = l_2147[2]) == (void*)0);
                        g_132 = l_2150;
                    }
                    else
                    { /* block id: 931 */
                        int32_t **l_2152 = &g_694;
                        int64_t *l_2172 = &g_142;
                        int64_t **l_2173 = &l_2172;
                        int32_t *l_2178 = &l_2137[2];
                        (*l_2152) = l_2151;
                        (*g_771) |= (safe_sub_func_int16_t_s_s(((safe_add_func_int8_t_s_s(((*l_79) = ((safe_lshift_func_int8_t_s_u(((((safe_sub_func_float_f_f((((safe_div_func_int64_t_s_s(((*g_1478) & ((safe_add_func_float_f_f(((void*)0 == (*g_1581)), (safe_div_func_float_f_f((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((((***g_2122) = (+(((*l_2173) = l_2172) != &l_2145[3][1][2]))) > (safe_mul_func_float_f_f(((4294967295UL == (safe_mul_func_uint16_t_u_u((l_2178 == ((((**g_1532) & (safe_lshift_func_uint16_t_u_u(((*g_578) != (*g_578)), (*g_578)))) ^ l_2182[3]) , l_2183)), 0x9663L))) , p_21), l_2184[0]))), p_21)) == p_21), p_21)), p_21)))) , l_2137[2])), p_21)) || (*g_578)) , p_21), 0x0.7B0D6Ep+38)) , l_2185) == (void*)0) > 0x83390B39364AE77CLL), 0)) != 0x69L)), l_2137[0])) <= p_21), p_21));
                        (*l_2152) = func_53((safe_mod_func_uint16_t_u_u((l_2184[0] || (((safe_sub_func_int32_t_s_s((l_2184[0] | (safe_rshift_func_int8_t_s_s((((p_21 , (p_21 < (*g_2028))) || ((safe_add_func_uint16_t_u_u(l_2137[2], ((safe_add_func_uint32_t_u_u(((p_21 | (safe_lshift_func_int8_t_s_s(p_21, 5))) || ((**g_979) = (**g_979))), p_21)) >= 0x316AB16DD72ADE05LL))) <= 0xB3F644C730EFCD7ELL)) <= l_2184[0]), (*g_1478)))), p_21)) > p_21) ^ p_21)), (*g_578))));
                    }
                }
                else
                { /* block id: 940 */
                    int32_t l_2206 = 0x159F7D9BL;
                    uint64_t ***l_2212 = &l_1504;
                    uint64_t ***l_2214[4][4] = {{&l_1504,&l_1504,&l_1504,&l_1504},{&l_1504,&l_1504,&l_1504,&l_1504},{&l_1504,&l_1504,&l_1504,&l_1504},{&l_1504,&l_1504,&l_1504,&l_1504}};
                    uint64_t ****l_2213[3][9];
                    int32_t l_2221 = (-1L);
                    int i, j;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 9; j++)
                            l_2213[i][j] = &l_2214[2][2];
                    }
                    l_2221 = (safe_add_func_uint8_t_u_u((**g_1532), ((safe_mul_func_uint16_t_u_u((safe_div_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((l_2206 , ((p_21 , (safe_div_func_int16_t_s_s((!0L), ((((*l_2069) = (safe_mul_func_int8_t_s_s((((***l_2212) = (((l_2212 != (l_2215 = &g_979)) & ((safe_div_func_float_f_f(p_21, (safe_div_func_float_f_f(l_2206, ((((+p_21) > l_2137[1]) == p_21) > (*g_427)))))) , 0L)) < p_21)) , (*g_1478)), p_21))) || (*g_2028)) , (*g_578))))) > 0x8860L)), 0)), 0x07L)), (*g_2028))) > 0x91L)));
                    for (g_617 = 1; (g_617 >= 44); g_617 = safe_add_func_uint32_t_u_u(g_617, 5))
                    { /* block id: 947 */
                        int32_t *l_2224 = &g_44;
                        int32_t **l_2225 = &l_2224;
                        (*l_2225) = l_2224;
                    }
                }
            }
            else
            { /* block id: 951 */
                int8_t *l_2244 = (void*)0;
                int32_t l_2245 = (-1L);
                int32_t **l_2247 = &g_694;
                (*l_2247) = func_53(((safe_mul_func_int8_t_s_s(0x68L, ((l_2246[4] &= (safe_mod_func_uint64_t_u_u((safe_mod_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(p_21, (safe_mul_func_uint8_t_u_u((safe_unary_minus_func_int64_t_s(((safe_add_func_uint64_t_u_u(((safe_mul_func_int16_t_s_s((0L < (g_1832[4] , l_2137[2])), ((safe_mod_func_uint64_t_u_u(p_21, ((l_2243 != l_2244) , (l_2245 && l_2245)))) != p_21))) < (*g_1078)), 0x41444F99D1A543F9LL)) , p_21))), l_2137[1])))), (-1L))), p_21))) && (*g_1478)))) ^ (*g_2028)));
            }
        }
        else
        { /* block id: 955 */
            uint16_t l_2248 = 0xB817L;
            uint32_t l_2257 = 0x528232C2L;
            int32_t l_2258 = (-1L);
            int32_t l_2259 = 0xC2039807L;
            int32_t l_2317 = 0xB0392260L;
            int64_t l_2338[6] = {2L,2L,2L,2L,2L,2L};
            int32_t *l_2340 = &l_1437;
            int64_t **l_2410 = &g_303;
            int32_t **l_2413[4] = {&g_771,&g_771,&g_771,&g_771};
            int i;
            if ((l_2259 = (l_2258 = (((*g_566) <= 0xD069D85FL) , ((*g_84) = (((l_2248 , (((safe_rshift_func_uint16_t_u_u(((safe_div_func_uint32_t_u_u((((((**g_1077) = (**g_1077)) , (*g_427)) <= ((+p_21) , (safe_sub_func_float_f_f((-0x7.0p+1), (l_2248 >= ((*l_2109) = ((!p_21) < (***g_2122)))))))) , (**g_1582)), 0x3A7F85B4L)) || (**g_1582)), p_21)) , l_2257) && p_21)) <= (*g_578)) <= p_21))))))
            { /* block id: 961 */
                int8_t *l_2285 = &g_30[3][3];
                int16_t l_2331 = 1L;
                int32_t *l_2339 = &l_1808[2][3];
                uint16_t l_2361 = 0UL;
                volatile float * volatile ** volatile **l_2362 = &g_1700;
                uint8_t ***l_2388 = &g_1077;
                const int32_t l_2391 = 1L;
                for (g_468 = 6; (g_468 >= 0); g_468 -= 1)
                { /* block id: 964 */
                    int32_t *l_2263 = &l_2113[3][5];
                    int8_t *l_2287 = &l_1542[0][0];
                    int8_t l_2300 = 0xEDL;
                    uint16_t **l_2310 = &g_578;
                    uint16_t ***l_2309 = &l_2310;
                    int32_t l_2318 = (-4L);
                    int32_t l_2319 = 0L;
                    float *****l_2357 = &l_2120[1][4][3];
                    uint8_t ***l_2359[10][1];
                    uint64_t *l_2385[10][2][4] = {{{&g_2290,&l_1459,&g_192,&g_192},{&g_192,&l_1541,&l_1541,&g_192}},{{&l_1541,&l_1459,(void*)0,&g_192},{&l_2320,&l_1541,&l_1459,&g_192}},{{&g_2290,&l_1459,&g_192,&g_192},{&g_192,&l_1541,&l_1541,&g_192}},{{&l_1541,&l_1459,(void*)0,&g_192},{&l_2320,&l_1541,&l_1459,&g_192}},{{&g_2290,&l_1459,&g_192,&g_192},{&g_192,&l_1541,&l_1541,&g_192}},{{&l_1541,&l_1459,(void*)0,&g_192},{&l_2320,&l_1541,&l_1459,&g_192}},{{&g_2290,&l_1459,&g_192,&g_192},{&g_192,&l_1541,&l_1541,&g_192}},{{&l_1541,&l_1459,(void*)0,&g_192},{&l_2320,&l_1541,&l_1459,&g_192}},{{&g_2290,&l_1459,&g_192,&g_192},{&g_192,&l_1541,&l_1541,&g_192}},{{&l_1541,&l_1459,(void*)0,&g_192},{&l_2320,&l_1541,&l_1459,&g_192}}};
                    int i, j, k;
                    for (i = 0; i < 10; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_2359[i][j] = &g_1077;
                    }
                    for (g_44 = 1; (g_44 >= 0); g_44 -= 1)
                    { /* block id: 967 */
                        const int32_t *l_2262[7][4][7] = {{{&l_2258,(void*)0,&g_46,&l_1808[2][8],&g_6,&l_1437,&l_1808[2][6]},{(void*)0,&g_46,&l_1808[2][6],&l_1808[2][6],&g_46,(void*)0,&l_1437},{&l_1808[2][6],(void*)0,(void*)0,&g_855,&g_1396[4][0][5],&l_2113[5][0],&g_9},{&l_1437,(void*)0,&l_1966[1],(void*)0,&l_2113[5][2],&g_2144,&g_44}},{{&l_1808[2][6],(void*)0,(void*)0,&l_1966[1],&l_1966[1],&l_1436,&l_1808[2][6]},{&l_1436,&g_46,&l_2259,&l_2113[0][3],&g_46,&l_1808[2][6],&l_1966[1]},{&l_2113[5][2],(void*)0,&g_1396[4][0][5],&g_855,&g_2144,&g_9,&l_1966[2]},{(void*)0,&l_1808[2][6],&l_1966[1],&l_1966[1],&l_1966[1],(void*)0,&l_1966[2]}},{{&g_855,&g_6,&g_9,(void*)0,&l_1966[2],&g_1396[4][0][5],&l_1966[1]},{&g_44,&g_13,&l_1808[2][6],&l_1808[2][6],&g_1396[4][0][5],&l_1808[2][6],&l_1808[2][6]},{&l_1808[2][6],&l_1808[2][6],&g_6,&g_13,&l_1808[1][0],&l_2113[0][5],&g_44},{&l_2113[5][0],&l_2113[0][3],(void*)0,&l_1966[1],&l_1966[1],&l_2113[5][0],&g_44}},{{&l_1808[2][8],&l_1966[1],&g_9,&l_1966[1],&g_855,&l_1966[2],&l_1966[0]},{(void*)0,&l_1966[1],&l_1966[1],&l_1436,&l_1808[2][6],&g_44,&g_6},{&g_2144,&l_1808[2][6],&l_1966[1],&l_1808[2][6],(void*)0,&l_1808[1][0],&g_1396[4][0][5]},{&g_1396[4][0][5],&l_1808[2][6],&l_1966[2],&g_6,&l_1808[2][6],&l_2113[0][3],&g_2144}},{{&l_1966[1],&g_44,&l_1966[2],&g_1396[4][0][5],&g_46,(void*)0,&l_1808[2][6]},{&l_1808[2][6],&l_2113[5][0],&l_1966[1],&l_1437,&g_6,&g_855,&g_855},{&l_1808[1][0],(void*)0,&l_1966[1],(void*)0,&l_1808[1][0],(void*)0,&l_1436},{(void*)0,&l_1966[2],&g_9,(void*)0,&l_1966[1],&l_1966[1],&l_1437}},{{&g_855,&l_2113[0][3],&g_1396[4][0][5],&l_1808[1][0],&g_2144,&g_46,&g_2144},{(void*)0,(void*)0,&g_13,&l_1966[1],(void*)0,(void*)0,&l_1808[2][6]},{&l_1808[1][0],&g_9,(void*)0,&l_2258,&g_13,(void*)0,&l_2259},{&l_1808[2][6],(void*)0,&g_44,&g_44,(void*)0,&g_9,&g_855}},{{&l_1966[1],&g_1396[4][0][5],&g_855,&g_855,&g_6,&g_9,(void*)0},{&g_1396[4][0][5],&l_1808[1][0],&l_1808[2][6],&l_2113[5][2],&l_1966[1],(void*)0,&l_2113[5][0]},{&g_2144,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1436,(void*)0,&g_855,(void*)0,&g_46,&l_1808[2][6]}}};
                        const int32_t **l_2261 = &l_2262[0][2][3];
                        const int32_t *** const l_2260 = &l_2261;
                        int i, j, k;
                        l_2113[(g_468 + 1)][(g_44 + 1)] = ((void*)0 == l_2260);
                        if (l_2113[g_468][(g_44 + 3)])
                            continue;
                    }
                    for (l_2088 = 2; (l_2088 <= 6); l_2088 += 1)
                    { /* block id: 973 */
                        int32_t **l_2264 = (void*)0;
                        int32_t **l_2265 = (void*)0;
                        int32_t **l_2266 = &l_2263;
                        int8_t **l_2286 = &l_79;
                        (*l_2266) = l_2263;
                        (*g_84) |= (safe_mul_func_int16_t_s_s((safe_sub_func_uint16_t_u_u((0UL || (safe_add_func_uint64_t_u_u(0x883800955205300CLL, ((safe_sub_func_uint8_t_u_u(((((((safe_rshift_func_int16_t_s_u((*g_2028), 1)) || (p_21 , (safe_sub_func_int16_t_s_s(p_21, (((safe_mod_func_uint32_t_u_u((safe_mod_func_int64_t_s_s(((safe_rshift_func_int16_t_s_u(p_21, ((l_2287 = ((*l_2286) = l_2285)) != (g_111[1] = ((((safe_sub_func_uint32_t_u_u(p_21, (((*l_1772) = &g_426[0][2][6]) == &g_1702))) , 0xFBL) != 0xBEL) , l_2243))))) <= 0x1D4416DAL), p_21)), (***g_1581))) | (*g_578)) , p_21))))) & p_21) , p_21) , l_2248) < 0xF7398071081B2706LL), p_21)) | (-5L))))), p_21)), g_2290));
                    }
                }
            }
            else
            { /* block id: 1016 */
                int32_t l_2405 = 0xCD2B34E3L;
                l_2405 = (((void*)0 == l_2400) != (safe_div_func_float_f_f(((***l_1773) = ((***g_2122) != p_21)), (safe_div_func_float_f_f(p_21, p_21)))));
            }
            l_23 = func_53((safe_lshift_func_uint8_t_u_u((safe_add_func_uint8_t_u_u((l_2410 == (void*)0), ((safe_add_func_int64_t_s_s((((0x33F1638CL | p_21) | p_21) != ((-9L) == 0x75E7F418L)), (((&l_1583[6] != &g_1581) >= p_21) == (-2L)))) != (*g_1478)))), 7)));
        }
        if ((l_1808[2][6] ^= ((*g_84) |= (((*l_2107) = (((safe_mul_func_int8_t_s_s(l_2113[5][0], (((((*g_566) = (safe_mul_func_int8_t_s_s((safe_unary_minus_func_uint16_t_u(p_21)), (safe_mod_func_int64_t_s_s((g_142 |= (*g_303)), (safe_lshift_func_uint16_t_u_s(((*g_1926) | ((((*g_303) < ((*l_2423) = p_21)) >= ((*g_2028) ^= (-2L))) <= (&p_21 != ((safe_sub_func_int64_t_s_s(((((safe_sub_func_int32_t_s_s(0xCC3E7F26L, p_21)) ^ 0x6D1AL) || 0xFDF78E13ECE3592ELL) == 0UL), l_2113[3][0])) , &g_92)))), 5))))))) | p_21) ^ l_2430) <= (*g_1478)))) < p_21) == 0x05E15677L)) , l_2113[5][2]))))
        { /* block id: 1029 */
            const int32_t l_2441 = 0x2D05F5F8L;
            int16_t l_2450 = 0xA69DL;
            const int32_t ***l_2453 = (void*)0;
            uint32_t l_2543 = 0x11AE36C6L;
            int32_t l_2575[1];
            int8_t l_2641[1][9][9] = {{{0x6BL,(-1L),0x74L,0x6BL,0xDEL,0x74L,0x74L,0xDEL,0x6BL},{0x99L,0L,0x99L,6L,0xDCL,6L,0x99L,0L,0x99L},{0x6BL,0xDEL,0x74L,0x74L,0xDEL,0x6BL,0x74L,(-1L),0x6BL},{0x3AL,0L,0x3AL,6L,0xE7L,6L,0x3AL,0L,0x3AL},{0x6BL,(-1L),0x74L,0x6BL,0xDEL,0x74L,0x74L,0xDEL,0x6BL},{0x99L,0L,0x99L,6L,0xDCL,6L,0x99L,0L,0x99L},{0x6BL,0xDEL,0x74L,0x74L,0xDEL,0x6BL,0x74L,(-1L),0x6BL},{0x3AL,0L,0x3AL,6L,0xE7L,6L,0x3AL,0L,0x3AL},{0x6BL,(-1L),0x74L,0x4BL,0x6BL,0x89L,0x89L,0x6BL,0x4BL}}};
            uint32_t l_2650[9] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
            int32_t *l_2670 = &l_1438;
            int32_t **l_2680 = &g_798;
            int32_t *l_2681[10] = {&g_2144,&l_2299,&g_2144,&l_2299,&l_2299,&g_2144,&l_2299,&g_2144,&l_2299,&l_2299};
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_2575[i] = 1L;
            for (g_192 = 0; (g_192 <= 2); g_192 += 1)
            { /* block id: 1032 */
                int16_t l_2432 = 0x1BE6L;
                int32_t l_2448 = 0x5395DFB6L;
                uint8_t ***l_2455 = &g_1077;
                uint8_t ****l_2454 = &l_2455;
                int16_t l_2486 = 0x7909L;
                int32_t *l_2489 = &g_1365;
                int8_t *l_2511 = &l_2108;
                int32_t l_2611 = (-1L);
                int32_t l_2619 = 0xD8F18454L;
                int32_t l_2620 = 0x20DFC0DDL;
                int32_t l_2621[3][9] = {{0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L},{0x23462242L,0x23462242L,0x23462242L,0x23462242L,0x23462242L,0x23462242L,0x23462242L,0x23462242L,0x23462242L},{0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L,0x5FD38649L}};
                int i, j;
                l_2450 |= (((((~((*g_566) = l_2432)) | (((safe_mul_func_int8_t_s_s((safe_mod_func_uint64_t_u_u(((safe_sub_func_int16_t_s_s(((((*g_771) = 1L) , (((l_2448 ^= ((((safe_div_func_int16_t_s_s(l_2441, (safe_sub_func_int32_t_s_s(p_21, p_21)))) , 0x428BL) || (-9L)) >= ((safe_mul_func_uint8_t_u_u((**g_1532), (safe_div_func_int64_t_s_s((-7L), (*g_1554))))) < l_2441))) ^ 0xBBL) < p_21)) || p_21), 1L)) < p_21), p_21)), 0x93L)) <= 9UL) < p_21)) ^ l_2441) || 0L) , p_21);
                for (p_21 = 0; (p_21 <= 2); p_21 += 1)
                { /* block id: 1039 */
                    uint32_t *l_2451 = &g_2380[3][0][0];
                    uint8_t **l_2501 = &g_1078;
                    uint32_t l_2532 = 4294967286UL;
                    int16_t **l_2584 = &g_2028;
                    int32_t l_2608 = 0xAEA6EA95L;
                    int32_t l_2618 = 1L;
                    int32_t l_2622[5][8][6] = {{{0x80A36DBCL,0x4BA82179L,0xE7C90FF4L,0x02E03701L,0xCD57E038L,0x80A36DBCL},{0xB5D90F70L,0L,0x655A909AL,0x80A36DBCL,8L,8L},{0xE0484801L,0x0773055DL,0x0773055DL,0xE0484801L,2L,(-6L)},{(-1L),0x3A4CB9A5L,0xE0484801L,0L,0x7126209FL,9L},{0x0DC1767EL,8L,0L,0xE7C90FF4L,0x7126209FL,(-1L)},{0x80A36DBCL,0x3A4CB9A5L,0L,0x655A909AL,2L,0x80A36DBCL},{8L,0x0773055DL,(-7L),0x0773055DL,8L,(-9L)},{(-6L),0L,0x7126209FL,0xE0484801L,0xCD57E038L,0xC56E569AL}},{{9L,0x4BA82179L,0xE0484801L,0L,0x80A36DBCL,0xC56E569AL},{(-1L),8L,0x7126209FL,0L,0L,(-9L)},{0x80A36DBCL,0L,(-7L),(-7L),0L,0x80A36DBCL},{(-9L),0L,0L,0x7126209FL,8L,(-1L)},{0xC56E569AL,0x80A36DBCL,0L,0xE0484801L,0x4BA82179L,9L},{0xC56E569AL,0xCD57E038L,0xE0484801L,0x7126209FL,0L,(-6L)},{(-9L),8L,0x0773055DL,(-7L),0x0773055DL,8L},{0x80A36DBCL,2L,0x655A909AL,0L,0x3A4CB9A5L,0x80A36DBCL}},{{(-1L),0x7126209FL,0xE7C90FF4L,0L,8L,0x0DC1767EL},{9L,0x7126209FL,0L,0xE0484801L,0x3A4CB9A5L,(-1L)},{(-6L),2L,0xE0484801L,0x0773055DL,0x0773055DL,0xE0484801L},{8L,8L,0x80A36DBCL,0x655A909AL,0L,0xB5D90F70L},{0x80A36DBCL,0xCD57E038L,0x02E03701L,0xE7C90FF4L,0x4BA82179L,0x80A36DBCL},{0x0DC1767EL,0x80A36DBCL,0x02E03701L,0L,8L,0xB5D90F70L},{(-1L),0L,0x80A36DBCL,0xE0484801L,0L,0xE0484801L},{0xE7C90FF4L,8L,0xE7C90FF4L,9L,(-6L),0x02E03701L}},{{0x0773055DL,0x80A36DBCL,(-6L),0x3A4CB9A5L,9L,0x7126209FL},{9L,0x0DC1767EL,2L,0x3A4CB9A5L,(-1L),9L},{0x0773055DL,(-1L),0xCD57E038L,9L,0x80A36DBCL,0x80A36DBCL},{0xE7C90FF4L,0xE0484801L,0xE0484801L,0xE7C90FF4L,(-9L),0L},{0x02E03701L,0xB5D90F70L,0xE7C90FF4L,(-6L),0xC56E569AL,0x655A909AL},{0x7126209FL,0x80A36DBCL,(-1L),2L,0xC56E569AL,0L},{9L,0xB5D90F70L,0x4BA82179L,0xCD57E038L,(-9L),9L},{0x80A36DBCL,0xE0484801L,0L,0xE0484801L,0x80A36DBCL,0L}},{{0L,(-1L),0xC56E569AL,0xE7C90FF4L,(-1L),(-7L)},{0x655A909AL,0x0DC1767EL,0xE7C90FF4L,(-1L),9L,(-7L)},{0L,0x80A36DBCL,0xC56E569AL,0x4BA82179L,(-6L),0L},{9L,8L,0L,0L,8L,9L},{0L,(-6L),0x4BA82179L,0xC56E569AL,0x80A36DBCL,0L},{(-7L),9L,(-1L),0xE7C90FF4L,0x0DC1767EL,0x655A909AL},{(-7L),(-1L),0xE7C90FF4L,0xC56E569AL,(-1L),0L},{0L,0x80A36DBCL,0xE0484801L,0L,0xE0484801L,0x80A36DBCL}}};
                    int32_t *l_2646[1];
                    int32_t *l_2654[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_2646[i] = &g_468;
                    if ((((((((void*)0 == l_2451) > (l_2450 == ((-0x10.3p-1) == l_2452))) < (l_2150 != l_2453)) , (void*)0) != l_2454) <= p_21))
                    { /* block id: 1040 */
                        uint64_t l_2456 = 0xD99D97B6011ED2F5LL;
                        uint32_t *l_2464 = &l_2452;
                        uint64_t l_2484 = 1UL;
                        uint16_t l_2485 = 0x4E21L;
                        int32_t **l_2487[7] = {&g_1836,&g_1836,&g_1836,&g_1836,&g_1836,&g_1836,&g_1836};
                        int32_t *l_2490 = &l_2448;
                        int i;
                        (*g_771) &= (l_2456 | (safe_add_func_int16_t_s_s((~(safe_lshift_func_uint8_t_u_u(p_21, 2))), ((*l_2107) &= (safe_rshift_func_uint16_t_u_u((l_2441 , (((((++(*l_2464)) , ((safe_rshift_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u((safe_rshift_func_int16_t_s_u((((l_2488 = (p_21 , (((*l_2451) = (safe_rshift_func_uint16_t_u_s(((-3L) ^ ((((+(((*l_2109) = (((0x785C46A8985A425BLL && ((*g_303) |= l_2456)) , (((safe_mul_func_float_f_f(((*l_2110) = (p_21 != ((safe_sub_func_float_f_f((-(+((*g_427) = ((safe_mul_func_int8_t_s_s(((((p_21 != 0x7B25FE38C59D0019LL) , (void*)0) != (**l_1773)) >= p_21), (**g_1532))) , (*g_427))))), l_2484)) <= p_21))), p_21)) != p_21) <= p_21)) >= l_2485)) , l_2486)) & p_21) >= 0xC8L) && 5L)), 0))) , l_2039))) != l_2489) , (*g_2028)), l_2484)), 0xFA1EA543L)) , p_21), 2)) >= l_2484)) , 0x1BL) | p_21) , 65535UL)), 4))))));
                        if ((*g_771))
                            continue;
                        l_2490 = func_53((*g_303));
                        return g_467;
                    }
                    else
                    { /* block id: 1053 */
                        int32_t **l_2491 = &g_802[7];
                        if ((*g_84))
                            break;
                        (*l_2491) = (*l_22);
                    }
                    if ((safe_lshift_func_int8_t_s_u((*g_1478), 3)))
                    { /* block id: 1057 */
                        uint8_t **l_2500 = &g_1078;
                        uint64_t *l_2502 = (void*)0;
                        uint64_t *l_2503 = &l_1459;
                        int32_t *l_2504 = &l_1438;
                        const int8_t *l_2509[3][6];
                        int i, j;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 6; j++)
                                l_2509[i][j] = (void*)0;
                        }
                        l_2504 = func_53((((*l_2503) = ((**g_979) = (safe_mul_func_uint8_t_u_u(((((p_21 <= (safe_unary_minus_func_uint64_t_u((safe_add_func_uint16_t_u_u(0xC809L, ((*l_2107) |= 8L)))))) > (safe_unary_minus_func_uint64_t_u(p_21))) != p_21) & ((l_2500 == l_2501) >= ((((*g_578) && ((*g_2028) = p_21)) == (*g_980)) , (*g_1078)))), 0L)))) || p_21));
                        (****l_1772) = (p_21 == (safe_div_func_float_f_f(((((((safe_lshift_func_int16_t_s_u(0xBA93L, (((void*)0 == &g_132) < (l_2509[0][2] != ((~((0x4D6AL <= p_21) , 0x65L)) , l_2511))))) >= (*g_2028)) <= p_21) , p_21) , &g_1478) == &l_2243), p_21)));
                    }
                    else
                    { /* block id: 1064 */
                        const int32_t *l_2512 = (void*)0;
                        const int32_t **l_2513 = &l_2512;
                        int32_t l_2533 = 1L;
                        uint8_t l_2534[2][8][6] = {{{0x7BL,246UL,0UL,0x4DL,0x2AL,1UL},{0x4DL,0x2AL,1UL,246UL,0x78L,1UL},{255UL,3UL,0UL,1UL,246UL,7UL},{0x78L,0x91L,0xD8L,0x23L,0x2AL,0UL},{255UL,7UL,0xD8L,246UL,3UL,7UL},{0x65L,0x78L,0UL,0x7BL,0x91L,1UL},{0x7BL,0x91L,1UL,7UL,7UL,1UL},{255UL,255UL,0UL,4UL,0x78L,7UL}},{{246UL,255UL,0xD8L,1UL,0x91L,0UL},{3UL,246UL,0xD8L,7UL,255UL,7UL},{0x4DL,7UL,0UL,0x65L,255UL,1UL},{0x65L,255UL,1UL,0x78L,246UL,1UL},{3UL,255UL,0UL,0x23L,7UL,7UL},{5UL,0x39L,0x2AL,0UL,255UL,0x4DL},{255UL,0x49L,0x2AL,0x49L,255UL,7UL},{0x12L,0UL,0x4DL,0UL,0x39L,0x23L}}};
                        int8_t **l_2583 = &g_111[5];
                        int8_t ***l_2582 = &l_2583;
                        int i, j, k;
                        (*l_2513) = l_2512;
                        l_2448 = (safe_add_func_float_f_f((safe_mul_func_float_f_f(((safe_add_func_float_f_f((*g_427), p_21)) <= ((*l_2110) = 0x8.86D37Cp+83)), (p_21 < l_2520))), (safe_add_func_float_f_f((safe_add_func_float_f_f(((~(l_2533 = (((safe_rshift_func_int8_t_s_u(((safe_lshift_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(((*l_2069) ^= (*g_578)), p_21)), p_21)) | ((void*)0 != (*g_132))), 4)) == 1L) && l_2532))) , p_21), 0x9.0p+1)), l_2534[0][6][0]))));
                        (*g_771) = ((!((safe_lshift_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(p_21, ((0xE1A13B96L <= (((l_2543 = ((~((*g_1554) = (*g_980))) || l_2532)) < 0x8CL) > ((safe_rshift_func_int16_t_s_u(p_21, 0)) >= (safe_lshift_func_int8_t_s_u(p_21, ((****l_2454) = 251UL)))))) ^ (((+(((safe_mod_func_uint8_t_u_u((safe_sub_func_uint8_t_u_u(p_21, p_21)), 255UL)) & p_21) & l_2486)) ^ l_2532) | (-1L))))), p_21)) , 1UL), 3)) , p_21)) ^ l_2532);
                        (*l_2513) = func_53((safe_div_func_int32_t_s_s(((safe_rshift_func_int8_t_s_s((safe_add_func_int32_t_s_s((((safe_mod_func_int64_t_s_s(((((*l_2069) = 0xC259L) >= ((((*g_980) , 254UL) , ((l_2533 |= (safe_lshift_func_int16_t_s_u(((**l_2584) = (((*l_2107) |= (l_2563 == ((safe_mod_func_int16_t_s_s((!(((safe_unary_minus_func_uint32_t_u((*g_566))) && (p_21 >= (safe_sub_func_uint8_t_u_u((safe_add_func_uint64_t_u_u((((((safe_sub_func_int8_t_s_s((+(--(*l_2451))), ((safe_mod_func_int8_t_s_s(((-2L) >= (safe_add_func_uint16_t_u_u(((&l_2243 != ((*l_2582) = &l_79)) || 0xB5A1B66CL), p_21))), (*g_1478))) >= 0L))) , (-3L)) & p_21) , (*g_566)) , (*g_980)), (-4L))), (-3L))))) ^ 0x75L)), (*g_2028))) , l_2584))) <= p_21)), 13))) , p_21)) <= p_21)) & 1UL), 18446744073709551615UL)) , l_2585) != l_2589[0][0][2]), p_21)), p_21)) ^ 0xC33572A64BF8E8E5LL), 0xC2E0762BL)));
                    }
                    if ((*g_771))
                    { /* block id: 1082 */
                        int8_t l_2594[5] = {(-9L),(-9L),(-9L),(-9L),(-9L)};
                        int32_t *l_2595 = &l_2113[5][2];
                        int32_t *l_2596 = &l_1436;
                        int32_t *l_2597 = &g_855;
                        int32_t *l_2598 = &l_1438;
                        int32_t *l_2599 = &l_2448;
                        int32_t *l_2600 = &g_46;
                        int32_t *l_2601 = (void*)0;
                        int32_t *l_2602 = &l_1808[0][7];
                        int32_t *l_2603 = &l_2113[5][2];
                        int32_t *l_2604 = (void*)0;
                        int32_t *l_2605 = &l_1437;
                        int32_t *l_2606 = &g_1396[4][0][5];
                        int32_t *l_2607 = &l_2113[1][5];
                        int32_t *l_2609 = &l_1808[2][6];
                        int32_t l_2610 = 0xC7FB6934L;
                        int32_t *l_2612 = &g_44;
                        int32_t *l_2613 = (void*)0;
                        int32_t *l_2614 = &l_2608;
                        int32_t *l_2615 = &g_1396[4][0][5];
                        int32_t *l_2616 = &l_2575[0];
                        int32_t *l_2617[2];
                        uint32_t l_2623 = 4294967295UL;
                        uint32_t ***l_2636 = &l_2038;
                        int32_t **l_2645[3][10] = {{&l_2488,&l_2489,&l_2488,&l_2488,&l_2489,&l_2488,&l_2489,&g_1836,&l_2488,&g_1836},{(void*)0,&l_2488,&l_2488,&l_2488,&l_2488,&l_2488,(void*)0,&l_2489,&l_2488,&l_2488},{(void*)0,&l_2488,&l_2488,(void*)0,(void*)0,&l_2488,&l_2488,(void*)0,&l_2489,&l_2489}};
                        int32_t l_2649 = 0L;
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_2617[i] = &l_1808[2][6];
                        l_2594[3] ^= (safe_rshift_func_uint8_t_u_u(p_21, 3));
                        ++l_2623;
                        (*g_427) = (-((safe_sub_func_float_f_f(((safe_mul_func_float_f_f((((((*l_2110) = ((***g_2122) == (-0x2.1p-1))) <= (((safe_div_func_float_f_f((-0x1.7p+1), l_2448)) != (safe_sub_func_float_f_f((***g_2122), l_2430))) < ((((*l_2636) = g_2635) != &l_2039) , ((safe_sub_func_float_f_f(((safe_div_func_float_f_f((p_21 , p_21), 0x1.7p-1)) < (*g_427)), l_2641[0][8][6])) > (-0x1.Bp-1))))) != 0xB.702C7Ap-93) < l_2448), l_2619)) < 0x5.703788p-62), p_21)) < 0xA.20F047p+49));
                        (*g_771) &= ((safe_rshift_func_uint16_t_u_s((safe_unary_minus_func_uint16_t_u(0xE5DBL)), (*g_2028))) > ((((l_2646[0] = func_53((*l_2616))) != l_2109) > p_21) <= (((*g_980) = ((*g_566) && (safe_add_func_uint64_t_u_u(l_2649, (((((((*l_2451) = ((*g_1926) = (*g_1926))) | l_2650[3]) & 0x9FA1166EL) || 4L) != 0x66E4L) , (*g_303)))))) != 18446744073709551609UL)));
                    }
                    else
                    { /* block id: 1093 */
                        int64_t *l_2651 = &g_142;
                        int32_t **l_2652 = (void*)0;
                        int32_t **l_2653 = &g_694;
                        uint16_t **l_2664 = &g_578;
                        uint16_t ***l_2663 = &l_2664;
                        l_2654[3] = ((*l_2653) = func_53(((*l_2651) ^= (*g_303))));
                        (***g_2122) = (safe_mul_func_float_f_f(0xD.F1CC40p-17, (safe_mul_func_float_f_f((safe_mul_func_float_f_f(p_21, p_21)), ((+((p_21 != ((-0x3.Fp-1) == 0x7.Dp+1)) >= (+((((*l_2663) = &g_578) == (((safe_rshift_func_int8_t_s_s(9L, 2)) ^ (safe_lshift_func_int16_t_s_u(p_21, l_2621[0][6]))) , l_2669)) , p_21)))) >= l_2619)))));
                        (*l_2653) = l_2670;
                    }
                    return p_21;
                }
            }
            (*l_2670) ^= ((safe_sub_func_int64_t_s_s((safe_rshift_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(p_21, ((*g_84) = (3UL || (safe_div_func_uint16_t_u_u(((*l_2069) = 0x4B0FL), (p_21 ^ (((!p_21) == 0x3383L) & p_21)))))))), (p_21 >= 1L))), (*g_1554))) || p_21);
            (*l_2680) = (*l_22);
            --l_2682[5][6];
        }
        else
        { /* block id: 1109 */
            int32_t *l_2685 = &l_1437;
            int32_t l_2688[4] = {0L,0L,0L,0L};
            int i;
            l_2685 = func_53((*g_303));
            l_2688[0] ^= (safe_lshift_func_uint16_t_u_s(0UL, p_21));
        }
    }
    return g_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_192 g_751 g_566 g_160 g_30 g_13 g_303 g_304 g_980 g_106 g_855 g_898 g_467 g_84 g_9 g_111 g_979 g_46 g_266 g_105 g_87 g_617 g_427 g_269 g_1120 g_1077 g_1078 g_1109 g_6
 * writes: g_192 g_751 g_160 g_459 g_304 g_142 g_467 g_30 g_106 g_266 g_269 g_213 g_1120 g_1109
 */
static int16_t  func_32(float  p_33, uint8_t  p_34, uint32_t  p_35, int8_t * p_36, int32_t  p_37)
{ /* block id: 518 */
    const int64_t l_1200 = 1L;
    int32_t *l_1206 = &g_13;
    int32_t l_1243 = 0L;
    uint64_t l_1244 = 0xC3C241DC37A3413ELL;
    int32_t l_1303 = 0L;
    int32_t l_1305[7][1][3] = {{{1L,1L,1L}},{{(-1L),(-1L),(-1L)}},{{1L,1L,1L}},{{(-1L),(-1L),(-1L)}},{{1L,1L,1L}},{{(-1L),(-1L),(-1L)}},{{1L,1L,1L}}};
    uint64_t *l_1316 = &g_106[3][0][3];
    uint64_t **l_1315 = &l_1316;
    const uint32_t l_1319[7][3][10] = {{{8UL,0x5AF5D6B0L,0xFCC22C84L,4294967292UL,4294967288UL,0x2A05F870L,0xB2AB5D11L,0x31345134L,2UL,4294967292UL},{8UL,0x8750A487L,5UL,4294967292UL,4294967295UL,0xE346C5C8L,0xB2AB5D11L,0xE346C5C8L,4294967295UL,4294967292UL},{5UL,0x5AF5D6B0L,5UL,4294967293UL,4294967288UL,0xE346C5C8L,1UL,0x31345134L,4294967295UL,4294967293UL}},{{8UL,0x5AF5D6B0L,0xFCC22C84L,4294967292UL,4294967288UL,0x2A05F870L,0xB2AB5D11L,0x31345134L,2UL,4294967292UL},{8UL,0x8750A487L,5UL,4294967292UL,4294967295UL,0xE346C5C8L,0xB2AB5D11L,0xE346C5C8L,4294967295UL,4294967292UL},{5UL,0x5AF5D6B0L,5UL,4294967293UL,4294967288UL,0xE346C5C8L,1UL,0x31345134L,4294967295UL,4294967293UL}},{{8UL,0x5AF5D6B0L,0xFCC22C84L,4294967292UL,4294967288UL,0x2A05F870L,0xB2AB5D11L,0x31345134L,2UL,4294967292UL},{8UL,0x8750A487L,5UL,4294967292UL,4294967295UL,0xE346C5C8L,0xB2AB5D11L,0xE346C5C8L,4294967295UL,4294967292UL},{5UL,0x5AF5D6B0L,5UL,4294967293UL,4294967288UL,0xE346C5C8L,1UL,0x31345134L,4294967295UL,4294967293UL}},{{8UL,0x5AF5D6B0L,0xFCC22C84L,4294967292UL,4294967288UL,0x2A05F870L,0xB2AB5D11L,0x31345134L,2UL,4294967292UL},{8UL,0x8750A487L,5UL,4294967292UL,4294967295UL,0xE346C5C8L,0xB2AB5D11L,0xE346C5C8L,4294967295UL,4294967292UL},{5UL,0UL,4294967295UL,0x2A05F870L,5UL,0UL,4294967295UL,0x01C619C3L,0xFCC22C84L,0x2A05F870L}},{{1UL,0UL,0UL,0xE346C5C8L,5UL,0xFD30688BL,4UL,0x01C619C3L,8UL,0xE346C5C8L},{1UL,0xC811EBADL,4294967295UL,0xE346C5C8L,0xFCC22C84L,0UL,4UL,0UL,0xFCC22C84L,0xE346C5C8L},{4294967295UL,0UL,4294967295UL,0x2A05F870L,5UL,0UL,4294967295UL,0x01C619C3L,0xFCC22C84L,0x2A05F870L}},{{1UL,0UL,0UL,0xE346C5C8L,5UL,0xFD30688BL,4UL,0x01C619C3L,8UL,0xE346C5C8L},{1UL,0xC811EBADL,4294967295UL,0xE346C5C8L,0xFCC22C84L,0UL,4UL,0UL,0xFCC22C84L,0xE346C5C8L},{4294967295UL,0UL,4294967295UL,0x2A05F870L,5UL,0UL,4294967295UL,0x01C619C3L,0xFCC22C84L,0x2A05F870L}},{{1UL,0UL,0UL,0xE346C5C8L,5UL,0xFD30688BL,4UL,0x01C619C3L,8UL,0xE346C5C8L},{1UL,0xC811EBADL,4294967295UL,0xE346C5C8L,0xFCC22C84L,0UL,4UL,0UL,0xFCC22C84L,0xE346C5C8L},{4294967295UL,0UL,4294967295UL,0x2A05F870L,5UL,0UL,4294967295UL,0x01C619C3L,0xFCC22C84L,0x2A05F870L}}};
    float *l_1320 = &g_213;
    int i, j, k;
    for (g_192 = 6; (g_192 > 50); g_192++)
    { /* block id: 521 */
        uint32_t l_1234 = 0x33B88080L;
        int32_t l_1298 = 6L;
        int32_t l_1299 = 0x46A05653L;
        int32_t l_1300 = 0xF60D13F3L;
        int32_t l_1302 = 0L;
        int32_t l_1304 = 1L;
        int32_t l_1306 = 0L;
        uint32_t l_1307[10][10] = {{0x037C8C8BL,18446744073709551611UL,0xBCB26BF7L,1UL,0xA4EB6561L,0xA4EB6561L,1UL,0xBCB26BF7L,18446744073709551611UL,0x037C8C8BL},{0xD74CA8A4L,0x037C8C8BL,0UL,18446744073709551615UL,0xEB2D44F5L,1UL,0x52236348L,0x8C8EB256L,18446744073709551610UL,0xDAD087F2L},{0UL,0x25A4A11EL,18446744073709551615UL,0x037C8C8BL,0xEB2D44F5L,18446744073709551611UL,0UL,18446744073709551611UL,0xEB2D44F5L,0x037C8C8BL},{0xEB2D44F5L,0x52236348L,0xEB2D44F5L,0xD74CA8A4L,0xA4EB6561L,18446744073709551615UL,0xDAD087F2L,18446744073709551615UL,3UL,0x69F8DB88L},{0x52236348L,0xA4EB6561L,0UL,0UL,0xDAD087F2L,18446744073709551614UL,0xBCB26BF7L,18446744073709551615UL,18446744073709551615UL,0xBCB26BF7L},{18446744073709551610UL,3UL,0xEB2D44F5L,0xEB2D44F5L,3UL,18446744073709551610UL,18446744073709551615UL,18446744073709551611UL,18446744073709551615UL,0UL},{0x25A4A11EL,0x9372920CL,18446744073709551615UL,0x52236348L,0x4F5D3C21L,3UL,18446744073709551614UL,0x8C8EB256L,1UL,0UL},{0x25A4A11EL,0xBCB26BF7L,0UL,18446744073709551610UL,0x037C8C8BL,18446744073709551610UL,0UL,0xBCB26BF7L,0x25A4A11EL,18446744073709551615UL},{18446744073709551610UL,0UL,0xBCB26BF7L,0x25A4A11EL,18446744073709551615UL,18446744073709551614UL,0xA4EB6561L,0xEB2D44F5L,0x69F8DB88L,0x8C8EB256L},{0x52236348L,18446744073709551615UL,0x25A4A11EL,18446744073709551615UL,0x4F5D3C21L,0UL,0UL,0x4F5D3C21L,18446744073709551615UL,0x25A4A11EL}};
        int i, j;
        for (g_751 = 0; (g_751 < 56); g_751 = safe_add_func_uint32_t_u_u(g_751, 2))
        { /* block id: 524 */
            int8_t **l_1197 = &g_111[1];
            int32_t l_1233 = 0x49C584F9L;
            uint16_t *l_1235 = &g_459;
            int32_t l_1236[7][2] = {{6L,0xBA3EE937L},{1L,0xBA3EE937L},{6L,1L},{1L,1L},{1L,1L},{6L,0xBA3EE937L},{1L,0xBA3EE937L}};
            int32_t l_1237 = 0x93B9A6E6L;
            uint32_t ** const l_1264 = &g_566;
            int32_t ** const l_1279 = &g_694;
            float l_1297 = (-0x10.0p+1);
            int i, j;
            if ((safe_mul_func_uint16_t_u_u((((~(((safe_add_func_int32_t_s_s((l_1197 != (void*)0), ((0x4B311C921C547FB8LL == (safe_mul_func_int16_t_s_s(l_1200, (l_1237 &= (((((safe_add_func_int8_t_s_s((safe_sub_func_int16_t_s_s((((~((&p_37 != l_1206) , (--(*g_566)))) != 3L) , (safe_mod_func_uint16_t_u_u(((g_142 = (safe_div_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u(((safe_mod_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u((*p_36), (((*g_303) = (((safe_rshift_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(((safe_div_func_uint8_t_u_u((safe_div_func_uint16_t_u_u(((*l_1235) = (safe_mod_func_int64_t_s_s((safe_div_func_int32_t_s_s(((safe_sub_func_int8_t_s_s((((*l_1206) || 0x3D47BA5CL) | l_1233), l_1233)) | l_1234), p_34)), (*g_303)))), l_1234)), (*p_36))) != l_1234), 1UL)), (*l_1206))) , (*g_980)) && 0x42998ADE0A4DC4F2LL)) >= (*g_980)))), 1UL)) != p_34), 0x2CL)), (*g_566)))) > p_35), l_1233))), p_37)), 0x99L)) == l_1236[3][1]) == l_1234) & g_855) > (-1L)))))) != l_1236[3][0]))) ^ 0xF2L) , 4UL)) == (*p_36)) | 0x7F6C6965L), p_34)))
            { /* block id: 530 */
                int32_t l_1240 = 0x7BABAEC6L;
                int32_t *l_1242[3];
                float **l_1282 = &g_427;
                uint8_t l_1289 = 1UL;
                int i;
                for (i = 0; i < 3; i++)
                    l_1242[i] = &g_46;
                l_1244 &= (safe_div_func_int32_t_s_s(l_1240, (l_1243 &= (safe_unary_minus_func_uint16_t_u(g_898[5])))));
                for (g_467 = (-19); (g_467 >= (-24)); g_467--)
                { /* block id: 535 */
                    int8_t l_1266 = 0L;
                    uint64_t l_1283 = 0UL;
                    int32_t l_1301 = 0xAC975E30L;
                    for (l_1243 = 13; (l_1243 >= 27); l_1243 = safe_add_func_uint16_t_u_u(l_1243, 3))
                    { /* block id: 538 */
                        uint32_t * const *l_1265 = (void*)0;
                        int32_t **l_1278 = &g_694;
                        int8_t *l_1294 = (void*)0;
                        int8_t *l_1295 = (void*)0;
                        int8_t *l_1296 = &g_266;
                        p_37 = ((safe_sub_func_int8_t_s_s(0x48L, ((safe_rshift_func_int16_t_s_u((((((*p_36) , (((+p_35) || ((safe_add_func_int8_t_s_s((1L != ((safe_div_func_uint64_t_u_u(((65532UL | 0x314EL) ^ p_37), ((safe_add_func_uint32_t_u_u(4294967294UL, ((safe_add_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s(((l_1264 == l_1265) != p_35), 3)), 0xFEA0BE0A915DCD40LL)) > 4294967289UL))) , (*g_303)))) , (*p_36))), l_1266)) == 255UL)) == (*g_980))) == (*g_303)) > (*g_84)) , p_35), p_35)) && (*l_1206)))) , 1L);
                        p_37 |= ((safe_lshift_func_uint16_t_u_u((*l_1206), 7)) & (~(safe_div_func_int8_t_s_s((7L && ((**l_1197) = (safe_add_func_int8_t_s_s(0x03L, ((safe_mul_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((2L <= ((p_34 , l_1278) == l_1279)), (safe_add_func_int32_t_s_s((&g_427 == l_1282), l_1266)))), 0x09L)) < (*g_303)))))), l_1283))));
                        l_1233 ^= ((safe_div_func_uint64_t_u_u(((((**g_979) = 0x0B1C5CE2C4638D76LL) ^ (*g_303)) < ((+(g_46 && (&l_1200 == ((((safe_lshift_func_uint8_t_u_s((p_34 &= p_37), 0)) , l_1289) , (safe_add_func_int64_t_s_s(0L, ((safe_add_func_int8_t_s_s((*p_36), ((*l_1296) |= (p_37 ^ l_1234)))) && p_37)))) , (void*)0)))) >= g_105)), p_35)) > g_87);
                    }
                    l_1307[9][6]--;
                }
                for (l_1304 = 4; (l_1304 >= 1); l_1304 -= 1)
                { /* block id: 551 */
                    return g_617;
                }
            }
            else
            { /* block id: 554 */
                uint16_t l_1310 = 0xB88BL;
                l_1310--;
            }
            l_1298 |= p_37;
        }
    }
lbl_1394:
    (*l_1320) = ((*g_427) = (safe_div_func_float_f_f((0x5.D8FAEDp-42 >= ((*g_979) != ((*l_1315) = &l_1244))), (((safe_mul_func_uint16_t_u_u((((0x673C6775L < ((*l_1206) >= 0xD836L)) == l_1319[5][0][5]) > p_35), (&l_1315 != (void*)0))) , (*l_1206)) == (*g_427)))));
    for (g_1120 = 0; (g_1120 <= 6); g_1120 += 1)
    { /* block id: 565 */
        int32_t l_1336 = 0L;
        int32_t l_1337 = 0x8BE6373AL;
        int32_t l_1338 = 0xC71E8343L;
        int32_t l_1339 = 0xEE624B99L;
        int32_t l_1340[8] = {0x50973A34L,0x50973A34L,0x50973A34L,0x50973A34L,0x50973A34L,0x50973A34L,0x50973A34L,0x50973A34L};
        float l_1367 = 0x0.8p-1;
        uint32_t l_1372 = 18446744073709551615UL;
        int32_t l_1391[6][9] = {{9L,0L,0x0186ED9CL,0xFAC963A5L,0xB57A757EL,(-1L),3L,0x03F66B12L,0x03F66B12L},{1L,0x169C21A0L,(-1L),(-1L),(-1L),0x169C21A0L,1L,3L,0x0186ED9CL},{0xDEAA0527L,0x03F66B12L,(-1L),1L,(-1L),(-1L),0xB57A757EL,0x169C21A0L,0xB7156D6AL},{3L,0xDEAA0527L,0x0186ED9CL,0x169C21A0L,0x169C21A0L,0x0186ED9CL,0xDEAA0527L,3L,0L},{0L,2L,3L,0x169C21A0L,9L,0L,0L,0x03F66B12L,(-1L)},{0L,0L,(-1L),1L,0L,1L,(-1L),0L,0L}};
        int i, j;
        if (p_35)
        { /* block id: 566 */
            int32_t *l_1321 = (void*)0;
            int32_t *l_1322 = (void*)0;
            int32_t *l_1323 = (void*)0;
            int32_t *l_1324 = &l_1305[2][0][2];
            int32_t *l_1325 = &l_1305[3][0][2];
            int32_t *l_1326 = &g_44;
            int32_t *l_1327 = &g_855;
            int32_t *l_1328 = &l_1305[5][0][0];
            int32_t *l_1329 = &l_1243;
            int32_t *l_1330 = &g_46;
            int32_t *l_1331 = &l_1305[6][0][0];
            int32_t *l_1332 = &g_46;
            int32_t *l_1333 = (void*)0;
            int32_t *l_1334 = &l_1305[5][0][1];
            int32_t *l_1335[2];
            int64_t l_1341 = 0xFCAB3E570382BBDBLL;
            uint32_t l_1342 = 0UL;
            int i;
            for (i = 0; i < 2; i++)
                l_1335[i] = &l_1305[2][0][2];
            l_1342++;
        }
        else
        { /* block id: 568 */
            int64_t l_1353 = 0L;
            int32_t l_1360 = (-6L);
            int32_t l_1361 = 0x079764D3L;
            int32_t l_1366 = 0xB6518529L;
            int32_t l_1368 = 0x5F45766DL;
            int32_t l_1369 = 3L;
            int32_t l_1370 = (-1L);
            int32_t l_1371[6] = {0x72CE2F1FL,0L,0x72CE2F1FL,0x72CE2F1FL,0L,0x72CE2F1FL};
            const uint32_t **l_1385 = (void*)0;
            int i;
            for (l_1303 = 4; (l_1303 >= 2); l_1303 -= 1)
            { /* block id: 571 */
                int8_t *l_1352[9] = {&g_266,&g_30[0][5],&g_266,&g_266,&g_30[0][5],&g_266,&g_266,&g_30[0][5],&g_266};
                int16_t *l_1362[7][10] = {{(void*)0,(void*)0,&g_467,&g_105,(void*)0,&g_105,&g_467,(void*)0,(void*)0,(void*)0},{&g_467,&g_467,&g_105,(void*)0,(void*)0,&g_105,&g_467,&g_467,&g_105,&g_467},{(void*)0,&g_467,&g_105,(void*)0,&g_105,&g_467,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&g_467,&g_105,&g_105,&g_467,(void*)0,&g_467,&g_467,&g_467,(void*)0},{&g_467,&g_467,&g_105,&g_467,&g_467,&g_105,(void*)0,(void*)0,&g_105,&g_467},{&g_467,&g_467,&g_467,&g_467,&g_105,(void*)0,&g_467,(void*)0,&g_105,&g_467},{(void*)0,&g_467,(void*)0,&g_105,&g_467,&g_467,&g_467,&g_467,&g_105,(void*)0}};
                int32_t l_1363 = 0x98A8C7A9L;
                int32_t *l_1364[2];
                uint8_t ****l_1386 = (void*)0;
                uint8_t ***l_1388 = &g_1077;
                uint8_t ****l_1387 = &l_1388;
                int i, j;
                for (i = 0; i < 2; i++)
                    l_1364[i] = &l_1338;
                l_1363 |= (((((l_1338 |= ((safe_sub_func_int8_t_s_s((l_1353 &= (safe_add_func_uint8_t_u_u((!(safe_mul_func_uint8_t_u_u((**g_1077), (*p_36)))), 0x34L))), (*g_1078))) >= (l_1360 = (safe_mod_func_uint32_t_u_u(((*g_566) = (safe_sub_func_uint64_t_u_u(0x52E3D9AE04D24AEALL, (((-1L) > (safe_lshift_func_uint16_t_u_u((l_1360 != (*g_566)), ((l_1361 = (*p_36)) > l_1337)))) , (*g_980))))), p_35))))) > (*l_1206)) , (void*)0) != (void*)0) != (*p_36));
                l_1372--;
                if ((safe_mod_func_uint32_t_u_u(((safe_add_func_float_f_f(((*g_427) != ((safe_add_func_float_f_f(((safe_sub_func_float_f_f(0x1.2p-1, (safe_div_func_float_f_f(p_37, (((*l_1387) = ((l_1385 == (void*)0) , &g_1077)) != (void*)0))))) == (safe_div_func_float_f_f((*l_1206), ((((*g_303) == p_37) , p_33) > 0x2.18B060p-88)))), 0x1.EABECFp-45)) > (*l_1206))), l_1391[4][4])) , 0UL), p_37)))
                { /* block id: 580 */
                    for (g_1109 = 16; (g_1109 <= 21); g_1109 = safe_add_func_int64_t_s_s(g_1109, 9))
                    { /* block id: 583 */
                        return g_1120;
                    }
                }
                else
                { /* block id: 586 */
                    return p_35;
                }
            }
        }
        if (l_1337)
            break;
    }
    if (g_192)
        goto lbl_1394;
    return g_6;
}


/* ------------------------------------------ */
/* 
 * reads : g_13 g_46 g_92 g_468 g_87 g_566 g_160 g_142 g_751 g_980 g_106 g_898 g_30 g_44 g_304 g_9 g_427 g_269 g_303 g_467 g_1151 g_104 g_266 g_798
 * writes: g_303 g_92 g_87 g_160 g_751 g_304 g_694 g_46 g_269 g_113 g_467 g_105 g_798
 */
static float  func_38(const uint32_t  p_39, int8_t  p_40, int64_t  p_41)
{ /* block id: 434 */
    int16_t l_996 = 0x8D7BL;
    int64_t *l_997 = &g_304[1][2];
    int64_t **l_998 = &l_997;
    int64_t **l_999 = (void*)0;
    int64_t **l_1000 = &g_303;
    int32_t l_1001[5][5][8] = {{{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}},{{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}},{{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}},{{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}},{{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}}};
    int32_t **l_1004 = &g_694;
    uint16_t *l_1005[1];
    uint8_t *l_1033 = &g_751;
    int32_t *l_1036 = (void*)0;
    int32_t l_1037[3];
    int64_t l_1118 = 0x146B58506A628365LL;
    float **l_1161 = &g_427;
    int16_t l_1162[9][2][6] = {{{0x8F23L,0x3689L,0L,0xD7E8L,(-9L),0xDFE3L},{9L,(-1L),1L,1L,(-1L),9L}},{{(-9L),0x6587L,(-4L),0x2C62L,9L,0x6E73L},{1L,0x59DFL,9L,0xB099L,(-9L),(-1L)}},{{1L,0x8F23L,0xB099L,0x2C62L,9L,0xD7E8L},{(-9L),9L,0xF26AL,(-1L),0xF26AL,9L}},{{0x59DFL,0xDFE3L,0x6541L,(-4L),0xCA14L,0x59DFL},{0x6E73L,0x2C62L,0xFBF8L,9L,9L,0L}},{{(-1L),0x2C62L,0x6587L,0xB099L,0xCA14L,0xBB76L},{0xD7E8L,0xDFE3L,0xB099L,0xF26AL,0xF26AL,0xB099L}},{{9L,9L,0x59DFL,0x6541L,9L,0x4AD6L},{0x59DFL,0x8F23L,0x5067L,0xFBF8L,(-9L),0x59DFL}},{{0L,0x59DFL,0x5067L,0x6587L,9L,0x4AD6L},{0xBB76L,0x6587L,0x59DFL,0xB099L,0x3689L,0xB099L}},{{0xB099L,0x3689L,0xB099L,0x59DFL,0x6587L,0xBB76L},{0x4AD6L,9L,0x6587L,0x5067L,0x59DFL,0L}},{{0x59DFL,(-9L),0xFBF8L,0x5067L,0x8F23L,0x59DFL},{0x4AD6L,9L,0x6541L,0x59DFL,9L,9L}}};
    uint64_t l_1182[10][10] = {{18446744073709551607UL,0x969F73DEB9543717LL,18446744073709551615UL,18446744073709551615UL,0x969F73DEB9543717LL,18446744073709551607UL,0x56B8ECC2E94731DBLL,0x956167A315A3546FLL,2UL,18446744073709551610UL},{0x956167A315A3546FLL,0x2CF6D2B0716D566ALL,0xA26209D112DC4D2CLL,18446744073709551607UL,0x0C7BF828808E1654LL,0UL,8UL,0xF6652A82BA10EE49LL,8UL,0UL},{0x956167A315A3546FLL,0UL,0x2EE36E2A02F7C7FCLL,0UL,0x956167A315A3546FLL,18446744073709551607UL,0UL,0xB4AD4D194ADC86D2LL,18446744073709551615UL,18446744073709551615UL},{18446744073709551607UL,0UL,0xB4AD4D194ADC86D2LL,18446744073709551615UL,18446744073709551615UL,18446744073709551610UL,0x969F73DEB9543717LL,0x969F73DEB9543717LL,18446744073709551610UL,18446744073709551615UL},{0x2EE36E2A02F7C7FCLL,18446744073709551615UL,18446744073709551615UL,0x2EE36E2A02F7C7FCLL,0x956167A315A3546FLL,0x2CF6D2B0716D566ALL,0xA26209D112DC4D2CLL,18446744073709551607UL,0x0C7BF828808E1654LL,0UL},{0x56B8ECC2E94731DBLL,0x956167A315A3546FLL,2UL,18446744073709551610UL,0x0C7BF828808E1654LL,0xA26209D112DC4D2CLL,18446744073709551615UL,0xA26209D112DC4D2CLL,0x0C7BF828808E1654LL,18446744073709551610UL},{0UL,0x56B8ECC2E94731DBLL,0UL,0x2EE36E2A02F7C7FCLL,0x969F73DEB9543717LL,0xF6652A82BA10EE49LL,0x0C7BF828808E1654LL,8UL,18446744073709551610UL,18446744073709551615UL},{18446744073709551615UL,0xF6652A82BA10EE49LL,0x956167A315A3546FLL,18446744073709551615UL,8UL,18446744073709551615UL,18446744073709551615UL,8UL,18446744073709551615UL,0x956167A315A3546FLL},{2UL,2UL,0UL,0UL,18446744073709551610UL,0x56B8ECC2E94731DBLL,0xB4AD4D194ADC86D2LL,0xA26209D112DC4D2CLL,8UL,0x2CF6D2B0716D566ALL},{18446744073709551615UL,0x0C7BF828808E1654LL,2UL,18446744073709551607UL,0xB4AD4D194ADC86D2LL,0x0C7BF828808E1654LL,18446744073709551615UL,0xB4AD4D194ADC86D2LL,18446744073709551615UL,0xA26209D112DC4D2CLL}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1005[i] = &g_92;
    for (i = 0; i < 3; i++)
        l_1037[i] = 0L;
    l_1001[0][2][6] = (((safe_mul_func_uint16_t_u_u(p_40, g_13)) | (((safe_lshift_func_uint16_t_u_u(p_41, 10)) , l_996) ^ (((*l_1000) = ((*l_998) = l_997)) != &g_304[1][2]))) > g_46);
    if (((!(safe_unary_minus_func_uint64_t_u(((g_92 |= (l_1004 == (void*)0)) && (l_1037[1] &= (safe_rshift_func_int8_t_s_u((0UL == (((((((safe_mul_func_uint16_t_u_u((l_1001[3][0][1] = (g_87 |= g_468)), (((*g_566)--) ^ ((-4L) && (((*l_997) &= (safe_rshift_func_int16_t_s_u((!(safe_div_func_int64_t_s_s(((((0x3.2p-1 <= (safe_add_func_float_f_f((((0x170CL != ((safe_mul_func_int16_t_s_s(g_142, ((safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((safe_div_func_int8_t_s_s(((((safe_lshift_func_int16_t_s_u(((safe_mul_func_uint16_t_u_u((safe_add_func_uint64_t_u_u(((((*l_1033)--) >= ((((l_1036 == &g_104) && 1L) | (*g_980)) && p_40)) > 6L), 0x44BC65A84099CB98LL)), l_996)) && p_41), g_142)) >= 0x34L) <= 4294967291UL) != g_898[5]), p_40)), g_30[3][6])), g_44)) , l_996))) == 0x3493L)) , 0x0.4p-1) < 0xF.67DE27p-33), p_40))) , 3UL) & l_996) ^ p_41), p_39))), 11))) > g_46))))) , 0x33L) && 0xA7L) != (*g_980)) && 1L) & 65535UL) > g_9)), p_40))))))) >= p_40))
    { /* block id: 445 */
        (*l_1004) = (void*)0;
    }
    else
    { /* block id: 447 */
        int32_t l_1050 = 0xFFD245DAL;
        int32_t l_1058[4];
        uint8_t **l_1079[5] = {&g_1078,&g_1078,&g_1078,&g_1078,&g_1078};
        uint64_t **l_1095[9];
        int i;
        for (i = 0; i < 4; i++)
            l_1058[i] = 0xE39CCCB5L;
        for (i = 0; i < 9; i++)
            l_1095[i] = (void*)0;
        for (g_46 = 0; (g_46 != 16); g_46++)
        { /* block id: 450 */
            int32_t l_1043 = 1L;
            int64_t **l_1064 = &g_303;
            int32_t l_1105 = 1L;
            int32_t l_1114 = 1L;
            int32_t l_1115 = 1L;
            int32_t l_1117 = 0xAAB36A9EL;
            int32_t l_1119[6][10] = {{0xAB70B8E2L,(-1L),1L,1L,0x42CBA42DL,0xB6F7612AL,0x7FCD42C1L,(-1L),0x7FCD42C1L,0xB6F7612AL},{0L,0x0A75F0CDL,0x42CBA42DL,0x0A75F0CDL,0L,0xB6F7612AL,1L,0x04052CBBL,0x4EB9367CL,1L},{0xAB70B8E2L,(-1L),0x42CBA42DL,0x04052CBBL,0x4C6CC6D4L,0x04052CBBL,0x42CBA42DL,(-1L),0xAB70B8E2L,1L},{0x4EB9367CL,0x04052CBBL,1L,0xB6F7612AL,0L,0x0A75F0CDL,0x42CBA42DL,0x0A75F0CDL,0L,0xB6F7612AL},{0x7FCD42C1L,(-1L),0x7FCD42C1L,0xB6F7612AL,0x42CBA42DL,1L,1L,(-1L),0xAB70B8E2L,(-1L)},{0x7FCD42C1L,0x0A75F0CDL,0xAB70B8E2L,0x04052CBBL,0xAB70B8E2L,0x0A75F0CDL,0x7FCD42C1L,(-1L),0x4EB9367CL,(-1L)}};
            int i, j;
            for (p_41 = 0; (p_41 <= 2); p_41 += 1)
            { /* block id: 453 */
                uint8_t l_1053[6][7][3] = {{{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL}},{{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L}},{{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L},{0UL,250UL,0UL}},{{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L}},{{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL}},{{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L},{0x1FL,250UL,0x1FL},{1UL,0x11L,0x11L},{0UL,250UL,0UL},{1UL,1UL,0x11L}}};
                uint16_t l_1059[10] = {65535UL,0xFDA0L,65535UL,0x90B8L,0x90B8L,65535UL,0xFDA0L,65535UL,0x90B8L,0x90B8L};
                uint8_t **l_1075 = &l_1033;
                int32_t *l_1111 = &l_1058[0];
                int32_t *l_1112 = &l_1001[1][3][2];
                int32_t *l_1113[1][4] = {{&l_1001[0][2][6],&l_1001[0][2][6],&l_1001[0][2][6],&l_1001[0][2][6]}};
                float l_1116 = 0x0.2p+1;
                int i, j, k;
            }
            (*g_427) = ((-l_1058[3]) <= (*g_427));
        }
    }
    for (l_1118 = 0; (l_1118 >= 28); ++l_1118)
    { /* block id: 500 */
        int32_t l_1144 = 1L;
        int32_t l_1163 = 0x98A039CFL;
        int32_t l_1171 = (-1L);
        int32_t l_1172 = 0x73B98945L;
        int32_t l_1174 = 4L;
        int32_t l_1175 = 0xB8855344L;
        int32_t l_1176 = 1L;
        int32_t l_1177 = (-1L);
        int32_t l_1178 = 1L;
        int32_t l_1179 = 0x582AFD00L;
        int32_t l_1180 = (-8L);
        int32_t l_1181[9][3][9] = {{{(-6L),0xECA4A9D1L,2L,(-2L),0L,0x9E0C970AL,8L,0x38B9CBCAL,0xC329E879L},{1L,0x35547EE3L,0x45C81E91L,0xED976487L,(-1L),1L,0xC0316FB2L,(-1L),0xA8331930L},{0x1BFC82D7L,(-1L),0xC329E879L,0L,(-1L),0L,0xC329E879L,(-1L),0x1BFC82D7L}},{{1L,0xDCCC82F8L,(-3L),8L,(-8L),7L,(-1L),1L,0x35547EE3L},{0xBC5371BBL,(-2L),1L,0x5FB63BF0L,8L,(-1L),(-1L),1L,0xBF24FFB5L},{1L,(-8L),(-1L),0L,0x4538BA33L,0xDCCC82F8L,0x86D8E539L,9L,0x5DB10CA7L}},{{0x1BFC82D7L,0x0EC06FA8L,0x56A14464L,(-10L),(-4L),0xC329E879L,0x9E0C970AL,0xF466470BL,(-1L)},{1L,0x1C0883E6L,(-6L),0x4573E401L,0xA8331930L,1L,0xF1AB5794L,(-1L),0xA703EB7AL},{(-6L),0L,(-2L),(-1L),(-10L),0L,(-1L),(-1L),0L}},{{1L,0xF8C1268BL,0xA476C8E6L,0xF8C1268BL,1L,0x4538BA33L,0x4DD172FBL,0xD17F997BL,0L},{0xB0709ACBL,(-2L),8L,0x56CD5C9DL,(-10L),0xECA4A9D1L,0x50FD11BEL,1L,(-1L)},{1L,8L,1L,(-6L),0xF49F9322L,0x4538BA33L,0x45C81E91L,0x64B643B0L,0L}},{{0xF7D43CD0L,(-1L),0L,0x54219631L,0x56A14464L,0L,(-4L),0L,1L},{(-8L),1L,(-5L),0L,0x4CFF0628L,1L,0xED976487L,0x86D8E539L,(-8L)},{0xABFD3E54L,0xB3B79AE2L,1L,(-4L),0xB4D53EAAL,0xC329E879L,0x3A2A5EC9L,(-1L),0L}},{{0xC0316FB2L,0x5147C8F4L,0x1C0883E6L,(-5L),0xDCCC82F8L,0xDCCC82F8L,(-5L),0x1C0883E6L,0x5147C8F4L},{0L,0x9AE7F4C1L,(-4L),0xBC5371BBL,0x56CD5C9DL,(-1L),(-7L),7L,0xF7D43CD0L},{1L,1L,0L,0L,8L,7L,0x68380AA6L,0x35547EE3L,0x453D6C32L}},{{0xC329E879L,0x9AE7F4C1L,1L,(-2L),9L,0L,(-6L),(-3L),6L},{0xD17F997BL,(-1L),(-6L),0x4CFF0628L,0L,0x86D8E539L,0xCD65AF7CL,0xF1AB5794L,1L},{3L,0x3A2A5EC9L,1L,0L,(-3L),(-1L),6L,0xF466470BL,0x56A14464L}},{{9L,0x86D8E539L,0x4CFF0628L,8L,9L,1L,1L,0L,1L},{(-1L),1L,(-10L),(-10L),1L,(-1L),0xF0161FD0L,0xB0709ACBL,(-4L)},{0xCD65AF7CL,0xA8331930L,0x0420077AL,0x86D8E539L,1L,1L,8L,1L,(-6L)}},{{0xABFD3E54L,(-4L),0xD28621ACL,(-7L),3L,2L,0xF0161FD0L,1L,0L},{(-3L),1L,(-8L),0xC0316FB2L,0xF1AB5794L,0x0420077AL,1L,0x86D8E539L,0xF49F9322L},{0L,0L,0x3A2A5EC9L,9L,7L,0xB3B79AE2L,6L,(-2L),0xAA56AFCBL}}};
        int i, j, k;
        if (p_41)
        { /* block id: 501 */
            int32_t **l_1137[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int32_t ***l_1138 = &g_113[7][3];
            int16_t *l_1145 = &g_467;
            int16_t *l_1146 = (void*)0;
            int16_t *l_1147 = &g_105;
            int i;
            l_1037[1] = ((safe_rshift_func_uint8_t_u_s(((p_41 <= 0x31B9L) || ((safe_div_func_int16_t_s_s(((*l_1147) = ((*l_1145) &= (((*g_566) = (safe_div_func_int64_t_s_s(p_40, (*g_303)))) || (!(safe_div_func_int16_t_s_s((l_1004 != ((*l_1138) = l_1137[8])), (safe_mod_func_uint8_t_u_u((p_40 >= (((safe_mod_func_int32_t_s_s((!p_39), l_1144)) < p_41) && p_41)), l_1144)))))))), p_40)) , p_39)), 1)) , p_39);
        }
        else
        { /* block id: 507 */
            int64_t ***l_1150 = &l_1000;
            int32_t *l_1164 = &l_1001[0][4][4];
            int32_t *l_1165 = (void*)0;
            int32_t *l_1166 = &l_1163;
            int32_t *l_1167 = &l_1163;
            int32_t *l_1168 = (void*)0;
            int32_t *l_1169 = (void*)0;
            int32_t *l_1170[4];
            int16_t l_1173 = 0xB017L;
            int i;
            for (i = 0; i < 4; i++)
                l_1170[i] = &l_1037[1];
            l_1163 &= (safe_mul_func_int8_t_s_s((((((l_1144 >= (246UL >= (p_40 &= (l_1150 == g_1151)))) , p_40) , p_41) , func_53(((p_41 = ((**l_998) &= (safe_mod_func_int64_t_s_s((p_40 > (((+(((((safe_sub_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_s((((0x31L || (safe_mul_func_int8_t_s_s(1L, p_40))) , &g_427) == l_1161), 6)), 0x33D26FC7L)) != p_41) <= g_104) != p_41) & g_266)) , 7UL) | 0L)), l_1162[5][0][3])))) <= p_40))) != (void*)0), 4UL));
            l_1182[5][4]--;
        }
        if (p_40)
            continue;
    }
    return (*g_427);
}


/* ------------------------------------------ */
/* 
 * reads : g_427 g_269 g_87 g_44 g_30 g_303 g_304 g_566 g_160 g_192 g_798 g_13 g_46 g_9 g_855 g_106 g_617 g_898 g_190 g_467 g_468 g_84 g_92 g_979
 * writes: g_269 g_190 g_467 g_798 g_46 g_694 g_898 g_855 g_617 g_192 g_160 g_44
 */
static const uint32_t  func_47(int32_t * p_48, int32_t ** p_49, uint64_t  p_50, int32_t ** p_51, int32_t * p_52)
{ /* block id: 370 */
    int32_t l_806 = (-9L);
    uint8_t **l_853 = (void*)0;
    int32_t l_857 = 0L;
    int32_t l_869 = 0x37781343L;
    int32_t l_875 = 1L;
    int32_t l_876 = 0x84CE2844L;
    int32_t l_877 = 1L;
    int32_t l_880 = 0xE9914651L;
    int32_t l_882 = 0x6A47DE33L;
    int32_t l_883 = 0xEAC33C10L;
    int32_t l_885 = 0xF74E2335L;
    int32_t l_886 = 1L;
    int32_t l_888[2][8][5] = {{{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L},{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L},{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L},{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L}},{{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L},{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L},{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L},{(-3L),(-7L),0L,0x1FD32FFBL,0L},{0x4DFE8146L,0x4DFE8146L,1L,6L,1L}}};
    int32_t l_895 = (-1L);
    int64_t **l_912[3];
    uint16_t *l_952 = (void*)0;
    int32_t l_961 = 0L;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_912[i] = (void*)0;
lbl_833:
    (*g_427) = ((*g_427) <= (-0x1.1p-1));
    for (g_190 = 0; (g_190 <= 0); g_190 += 1)
    { /* block id: 374 */
        uint32_t l_803 = 0xF1FBCA8BL;
        const uint32_t l_811[7][1][8] = {{{0xBBC5E195L,0xC1F0C4C9L,0UL,0xC1F0C4C9L,0xBBC5E195L,1UL,0xBBC5E195L,0xC1F0C4C9L}},{{0x994BBD28L,0xC1F0C4C9L,0x994BBD28L,0x80317A55L,0xBBC5E195L,0x80317A55L,0x994BBD28L,0xC1F0C4C9L}},{{0xBBC5E195L,0x80317A55L,0x994BBD28L,0xC1F0C4C9L,0x994BBD28L,0x80317A55L,0xBBC5E195L,0x80317A55L}},{{0xBBC5E195L,0xC1F0C4C9L,0UL,0xC1F0C4C9L,0xBBC5E195L,1UL,0x994BBD28L,0x80317A55L}},{{0UL,0x80317A55L,0UL,1UL,0x994BBD28L,1UL,0UL,0x80317A55L}},{{0x994BBD28L,1UL,0UL,0x80317A55L,0UL,1UL,0x994BBD28L,1UL}},{{0x994BBD28L,0x80317A55L,0xBBC5E195L,0x80317A55L,0x994BBD28L,0xC1F0C4C9L,0x994BBD28L,0x80317A55L}}};
        const uint8_t l_832 = 0x71L;
        int32_t l_836 = 8L;
        uint8_t **l_854 = (void*)0;
        uint16_t *l_856 = &g_87;
        int32_t l_873 = 1L;
        int32_t l_874 = 0x613042E3L;
        int32_t l_878 = 0L;
        int32_t l_879 = (-1L);
        int32_t l_881 = 0x878F37B6L;
        int32_t l_884[5];
        int32_t l_892[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
        int32_t l_894 = 0x09BF2A99L;
        uint8_t l_927 = 0xCDL;
        float **l_968 = &g_427;
        int64_t * const *l_990 = &g_303;
        int64_t * const **l_989 = &l_990;
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_884[i] = (-5L);
        l_803++;
        l_806 &= l_803;
        for (g_467 = 0; (g_467 <= 0); g_467 += 1)
        { /* block id: 379 */
            uint16_t *l_824[2][5] = {{&g_459,&g_459,&g_459,&g_459,&g_459},{&g_92,&g_92,&g_92,&g_92,&g_92}};
            int32_t l_831 = (-1L);
            int32_t l_872 = 0x119D99C7L;
            int32_t l_887 = 0x291DFB5BL;
            int32_t l_889 = 0xF88AFE55L;
            int32_t l_890 = 0xCEABAA91L;
            int32_t l_891[7][5][7] = {{{9L,2L,(-1L),0xB351B921L,0x640F1983L,0x640F1983L,0xB351B921L},{0xD37CE518L,0x64839B04L,0xD37CE518L,0xC95B98B0L,0x61600CA7L,0x64839B04L,0x61600CA7L},{2L,0xB351B921L,0x86800C54L,9L,0x640F1983L,(-1L),(-1L)},{0x387FE4ACL,1L,0x7B12A550L,1L,0x387FE4ACL,0x64839B04L,0x7B12A550L},{0xA89500CFL,2L,0xB351B921L,0x86800C54L,9L,0x640F1983L,(-1L)}},{{0x61600CA7L,0xC95B98B0L,0xD37CE518L,0x64839B04L,0xD37CE518L,0xC95B98B0L,0x61600CA7L},{0xA89500CFL,0x86800C54L,(-1L),9L,2L,(-1L),0xB351B921L},{0x387FE4ACL,0xC95B98B0L,(-1L),0L,0x387FE4ACL,0L,(-1L)},{2L,2L,(-1L),0xB351B921L,0xA89500CFL,0x640F1983L,0x86800C54L},{0xD37CE518L,1L,0xD37CE518L,0L,0x61600CA7L,1L,0x61600CA7L}},{{9L,0xB351B921L,0xB351B921L,9L,0xA89500CFL,(-1L),(-1L)},{0x387FE4ACL,0x64839B04L,0x7B12A550L,0x64839B04L,0x387FE4ACL,1L,0x7B12A550L},{0x640F1983L,2L,0x86800C54L,0x86800C54L,2L,0x640F1983L,(-1L)},{0x61600CA7L,0L,0xD37CE518L,1L,0xD37CE518L,0L,0x61600CA7L},{0x640F1983L,0x86800C54L,(-1L),9L,9L,(-1L),0x86800C54L}},{{0x387FE4ACL,0L,(-1L),0xC95B98B0L,0x387FE4ACL,0xC95B98B0L,(-1L)},{9L,2L,(-1L),0xB351B921L,0x640F1983L,0x640F1983L,0xB351B921L},{0xD37CE518L,0x64839B04L,0xD37CE518L,0xC95B98B0L,0x61600CA7L,0x64839B04L,0x61600CA7L},{2L,0xB351B921L,0x86800C54L,9L,0x640F1983L,(-1L),(-1L)},{0x387FE4ACL,1L,0x7B12A550L,1L,0x387FE4ACL,0x64839B04L,0x7B12A550L}},{{0xA89500CFL,2L,0xB351B921L,0x86800C54L,9L,0x640F1983L,(-1L)},{0x61600CA7L,0xC95B98B0L,0xD37CE518L,0x64839B04L,0xD37CE518L,0xC95B98B0L,0x61600CA7L},{0xA89500CFL,0x86800C54L,(-1L),9L,2L,(-1L),0xB351B921L},{0x387FE4ACL,0xC95B98B0L,(-1L),0L,0x387FE4ACL,0L,(-1L)},{2L,2L,(-1L),0xB351B921L,0xA89500CFL,0x640F1983L,0x86800C54L}},{{0xD37CE518L,1L,0xD37CE518L,0L,0x61600CA7L,1L,0x61600CA7L},{9L,0xB351B921L,0xB351B921L,9L,0xA89500CFL,(-1L),(-1L)},{0x387FE4ACL,0x64839B04L,0x7B12A550L,0x64839B04L,0x387FE4ACL,1L,0x7B12A550L},{0x640F1983L,2L,0x86800C54L,0x86800C54L,2L,0x640F1983L,(-1L)},{0x61600CA7L,0L,0xD37CE518L,1L,0xD37CE518L,0L,0x61600CA7L}},{{0x640F1983L,0x86800C54L,(-1L),9L,9L,(-1L),0x86800C54L},{0x387FE4ACL,0L,(-1L),0xC95B98B0L,0x387FE4ACL,0xC95B98B0L,(-1L)},{9L,2L,(-1L),0xB351B921L,0x640F1983L,0x640F1983L,0xB351B921L},{0xD37CE518L,0x64839B04L,0xD37CE518L,0xC95B98B0L,0x61600CA7L,0x64839B04L,0x61600CA7L},{2L,0xB351B921L,0x86800C54L,9L,0x640F1983L,(-1L),(-1L)}}};
            int64_t l_893 = 1L;
            uint32_t l_924 = 18446744073709551615UL;
            uint64_t l_925[9] = {0xC8E656BBE6F382A1LL,18446744073709551615UL,0xC8E656BBE6F382A1LL,0xC8E656BBE6F382A1LL,18446744073709551615UL,0xC8E656BBE6F382A1LL,0xC8E656BBE6F382A1LL,18446744073709551615UL,0xC8E656BBE6F382A1LL};
            int i, j, k;
            (*p_51) = func_53(((safe_lshift_func_int8_t_s_u((safe_div_func_int64_t_s_s((((l_811[1][0][4] > (safe_mul_func_uint16_t_u_u((((safe_sub_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((safe_sub_func_int16_t_s_s(((g_87 == (((((((safe_mod_func_uint16_t_u_u(0x52F7L, 0x1D40L)) , (p_50 , p_50)) , (void*)0) == l_824[1][2]) ^ (safe_lshift_func_uint16_t_u_s((safe_rshift_func_int8_t_s_u((safe_rshift_func_uint16_t_u_u(p_50, l_831)), g_44)), g_30[2][4]))) , (*g_303)) >= 18446744073709551614UL)) , l_811[1][0][5]), l_806)), 0xD0L)), l_832)) > p_50), 1L)) | 0L) & 0x76B39DFD6B4DE22ALL), p_50))) | 18446744073709551615UL) != (*g_566)), g_192)), p_50)) ^ 0xFF1290CBL));
            if (p_50)
                goto lbl_833;
            (*p_49) = ((((safe_div_func_uint32_t_u_u(p_50, (((l_836 = (((0xD52DL >= 0x1BE5L) < (*g_566)) <= p_50)) ^ (((safe_rshift_func_uint8_t_u_u(((l_831 ^ (*p_52)) ^ ((safe_sub_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u((safe_sub_func_int32_t_s_s(((safe_mod_func_int16_t_s_s((((l_824[1][2] != (((safe_rshift_func_int8_t_s_u((safe_mul_func_int8_t_s_s((safe_div_func_uint64_t_u_u(((((l_853 == l_854) | g_855) != p_50) || 5L), 6UL)), l_832)), 4)) ^ 4L) , l_856)) == (*g_566)) != p_50), p_50)) & l_831), p_50)), 0UL)), l_806)) > g_160[5])), g_106[3][0][3])) , p_50) || (*g_303))) , (*g_566)))) >= l_803) , g_617) , (void*)0);
            for (l_831 = 0; (l_831 >= 0); l_831 -= 1)
            { /* block id: 386 */
                int32_t *l_858 = &l_857;
                int32_t *l_859 = &l_857;
                int32_t *l_860 = (void*)0;
                int32_t *l_861 = &g_855;
                int32_t *l_862 = &g_46;
                int32_t *l_863 = &g_44;
                int32_t *l_864 = &g_44;
                int32_t *l_865 = &l_836;
                int32_t *l_866 = &g_855;
                int32_t *l_867 = (void*)0;
                int32_t *l_868 = &l_857;
                int32_t l_870 = (-10L);
                int32_t *l_871[6][7][6] = {{{(void*)0,&g_13,&g_46,&g_855,&l_870,(void*)0},{&g_46,(void*)0,&l_857,&g_6,&l_870,(void*)0},{(void*)0,&g_13,&g_855,&g_855,&g_9,&l_870},{&g_46,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_13,&g_13,&g_13,&g_46,&g_6,&l_869},{(void*)0,&g_855,(void*)0,(void*)0,(void*)0,(void*)0},{&l_869,&l_870,&l_869,(void*)0,(void*)0,&g_46}},{{(void*)0,(void*)0,(void*)0,&g_46,&l_870,&l_836},{&g_13,&l_857,&g_9,(void*)0,(void*)0,&g_9},{&g_46,&g_46,(void*)0,&g_855,&l_869,&l_857},{(void*)0,&g_9,(void*)0,&g_6,(void*)0,(void*)0},{&g_46,(void*)0,(void*)0,&g_855,&g_46,&l_857},{(void*)0,&g_855,(void*)0,(void*)0,&l_836,&g_9},{(void*)0,&l_836,&g_9,&g_855,(void*)0,&g_855}},{{&g_13,&g_13,&g_46,&g_6,&l_869,&l_870},{&l_869,&l_836,&g_9,(void*)0,&l_870,&g_9},{&g_855,&l_836,&g_6,(void*)0,&l_869,(void*)0},{&l_869,&g_13,(void*)0,&l_869,(void*)0,&g_855},{&g_855,&g_855,&g_9,&g_9,&g_855,&g_855},{(void*)0,&g_46,&l_869,(void*)0,&l_870,(void*)0},{&l_870,(void*)0,&l_869,(void*)0,&g_9,&g_13}},{{&l_870,(void*)0,(void*)0,(void*)0,(void*)0,&g_13},{(void*)0,&l_870,(void*)0,&g_9,&g_855,&g_6},{&g_855,&l_869,&g_855,&l_869,&g_855,&l_857},{&l_869,(void*)0,&g_855,(void*)0,(void*)0,(void*)0},{&g_855,&g_855,&g_13,(void*)0,&g_13,(void*)0},{&l_869,&g_46,&g_855,&g_6,&g_13,&l_857},{&g_13,(void*)0,&g_855,&l_869,&g_6,&g_6}},{{&g_9,(void*)0,(void*)0,&g_9,&l_857,&g_13},{(void*)0,(void*)0,(void*)0,&g_46,(void*)0,&g_13},{&l_836,&g_46,&l_869,&g_13,(void*)0,(void*)0},{(void*)0,(void*)0,&l_869,&g_46,&l_857,&g_855},{&l_870,(void*)0,&g_9,&g_855,&g_6,&g_855},{(void*)0,(void*)0,(void*)0,&l_870,&g_13,(void*)0},{(void*)0,&g_46,&g_6,(void*)0,&g_13,&g_9}},{{(void*)0,&g_855,&g_9,(void*)0,(void*)0,&l_870},{(void*)0,(void*)0,&g_46,&l_870,&g_855,&g_855},{(void*)0,&l_869,(void*)0,&g_855,&g_855,(void*)0},{&l_870,&l_870,&l_870,&g_46,(void*)0,&l_869},{(void*)0,(void*)0,&g_9,&g_13,&g_9,&l_870},{&l_836,(void*)0,&g_9,&g_46,&l_870,&l_869},{(void*)0,&g_46,&l_870,&g_9,&g_855,(void*)0}}};
                float l_896 = (-0x9.Fp-1);
                float l_897 = 0xA.8D7C40p+70;
                const int64_t *l_914 = &g_142;
                const int64_t **l_913 = &l_914;
                uint16_t l_923 = 0xC216L;
                uint32_t l_943 = 0xA66D780BL;
                int16_t l_969 = 0x0F5EL;
                uint16_t l_991 = 65535UL;
                int i, j, k;
                --g_898[5];
                for (l_882 = 0; (l_882 <= 0); l_882 += 1)
                { /* block id: 390 */
                    int64_t l_901 = 2L;
                    int64_t ***l_932 = &l_912[1];
                    uint8_t *l_950 = &g_617;
                    int32_t *l_951[7][9] = {{&l_892[6],(void*)0,&g_104,&g_104,(void*)0,&l_892[6],(void*)0,&g_104,&g_104},{&g_104,&g_104,&g_104,&l_892[7],&g_104,&g_104,&g_104,&g_104,&l_892[7]},{&l_806,(void*)0,&l_806,&l_806,&l_806,&g_104,&l_806,&g_104,&l_806},{&l_892[7],&l_892[0],&l_892[0],&l_892[7],&g_104,&l_892[7],&l_892[0],&l_892[0],&l_892[7]},{&l_892[6],&l_806,(void*)0,&l_806,&l_892[6],&l_892[6],&l_806,(void*)0,&l_806},{&l_892[0],&g_104,&l_892[3],&l_892[3],&g_104,&l_892[0],&g_104,&l_892[3],&l_892[3]},{&l_892[6],&l_892[6],&l_806,(void*)0,&l_806,&l_892[6],&l_892[6],&l_806,(void*)0}};
                    int8_t *l_959[2];
                    int32_t l_960[3];
                    int i, j;
                    for (i = 0; i < 2; i++)
                        l_959[i] = (void*)0;
                    for (i = 0; i < 3; i++)
                        l_960[i] = 0x937C682EL;
                    if (l_901)
                        break;
                    for (g_855 = 0; (g_855 <= 6); g_855 += 1)
                    { /* block id: 394 */
                        uint8_t *l_905 = &g_617;
                        uint64_t *l_908 = &g_192;
                        int32_t l_926 = 4L;
                        int i, j, k;
                        l_884[3] &= (+(g_106[(g_190 + 3)][g_855][(g_467 + 6)] , (safe_div_func_int8_t_s_s(g_30[g_855][(l_831 + 4)], (p_50 ^ (((*l_905)++) , ((*l_908) |= ((0x35FF1CBDBD8C9ADDLL > (*g_303)) , g_468))))))));
                        l_925[0] &= (safe_lshift_func_int8_t_s_u((((l_901 == 0x5DL) ^ (((safe_unary_minus_func_int16_t_s((1L != ((l_912[2] == l_913) > (((*g_566) = ((0xDC7CL == l_901) , ((((*l_858) = (safe_sub_func_int32_t_s_s((safe_lshift_func_uint8_t_u_s(((*g_84) , (safe_div_func_int16_t_s_s(((safe_div_func_uint32_t_u_u(((-1L) || p_50), l_884[3])) || g_304[1][2]), (*l_862)))), p_50)), l_890))) < l_923) == l_901))) >= 0x3FADAB4FL))))) != l_924) <= 0L)) || l_806), 6));
                        --l_927;
                    }
                    (*g_427) = ((((l_881 = (l_806 >= ((((((safe_mul_func_int16_t_s_s((((*l_932) = &g_303) != &g_303), (safe_div_func_uint64_t_u_u(((0x1BL | ((safe_rshift_func_int8_t_s_u((l_888[1][5][3] |= (safe_lshift_func_uint16_t_u_s(((((p_50 , (safe_div_func_uint64_t_u_u(((p_50 < (safe_add_func_uint8_t_u_u(l_943, ((((((safe_mul_func_int8_t_s_s((((safe_sub_func_int16_t_s_s(((*l_866) | ((safe_mul_func_uint8_t_u_u((((*l_950) ^= (l_887 = g_106[2][4][3])) >= g_304[1][2]), p_50)) | g_160[3])), g_855)) , (-3L)) ^ l_877), 0x6EL)) || g_468) , 0xB6L) != g_87) , 65535UL) <= p_50)))) , 0x9F7164FC6BB1E670LL), (*g_303)))) , (void*)0) == (void*)0) <= g_468), 2))), p_50)) , p_50)) || (*g_303)), g_106[2][4][1])))) != g_106[3][2][5]) <= p_50) || 0UL) , 3L) | g_92))) , l_952) == l_856) <= 0x1.A22CD1p-42);
                    l_961 &= (safe_mul_func_int8_t_s_s((~0UL), (l_891[0][2][6] |= (l_960[2] &= ((safe_rshift_func_int8_t_s_u((+(l_951[0][7] != (void*)0)), g_304[2][1])) , (((void*)0 != p_52) != (*l_858)))))));
                }
                for (g_44 = 0; (g_44 <= 0); g_44 += 1)
                { /* block id: 415 */
                    uint16_t l_970 = 65535UL;
                    (*l_868) = ((((*l_865) < (p_50 > p_50)) != (l_803 , (safe_div_func_uint32_t_u_u(((((safe_lshift_func_int8_t_s_u(p_50, (safe_rshift_func_int8_t_s_s(((l_884[3] , l_968) == (void*)0), 6)))) < (*g_566)) & (*p_52)) ^ 0x3B851A0FD121A816LL), (*p_52))))) <= p_50);
                    if (l_887)
                    { /* block id: 417 */
                        (*g_427) = (*l_862);
                    }
                    else
                    { /* block id: 419 */
                        if (l_891[0][2][6])
                            break;
                        if (l_803)
                            break;
                        if (l_969)
                            continue;
                        --l_970;
                    }
                    (*l_858) &= (safe_add_func_uint32_t_u_u((safe_sub_func_int16_t_s_s(((((65529UL ^ 0x62D5L) , &g_84) == (void*)0) || 0x3645CEE3L), g_30[6][0])), (((((*l_866) = (p_50 <= ((*g_303) != (((*g_427) , g_979) == &g_980)))) >= 0UL) , 0x58L) || (-10L))));
                    l_888[1][1][4] = (0xF.171421p-47 < (((**l_968) = (l_970 >= (safe_div_func_float_f_f((safe_sub_func_float_f_f(((*g_427) == (((l_925[0] , l_925[1]) > ((safe_sub_func_float_f_f((0xA.30288Ep-18 < ((p_50 == (safe_sub_func_float_f_f(((void*)0 == l_989), (*g_427)))) != 0xE.EBBC93p-31)), p_50)) < p_50)) != 0x5.547E0Ap+42)), p_50)), l_991)))) >= l_893));
                }
            }
        }
    }
    return l_876;
}


/* ------------------------------------------ */
/* 
 * reads : g_798 g_13 g_46
 * writes: g_798 g_46
 */
static int32_t * func_53(const int64_t  p_54)
{ /* block id: 364 */
    int32_t *l_797 = &g_13;
    int32_t **l_799 = &g_798;
    int32_t *l_800 = &g_46;
    int32_t *l_801[8] = {&g_9,&g_6,&g_9,&g_9,&g_6,&g_9,&g_9,&g_6};
    int i;
    g_798 = l_797;
    (*l_799) = l_797;
    (*l_800) &= (*g_798);
    (*l_799) = l_801[6];
    return (*l_799);
}


/* ------------------------------------------ */
/* 
 * reads : g_92 g_106 g_84 g_9 g_30 g_104 g_13 g_46 g_44 g_142 g_105 g_87 g_459 g_427 g_269 g_303 g_304 g_160 g_6 g_132 g_113 g_162 g_566 g_190 g_578 g_617 g_266 g_694 g_192
 * writes: g_92 g_106 g_111 g_113 g_104 g_132 g_142 g_105 g_459 g_44 g_269 g_566 g_160 g_578 g_190 g_617 g_304 g_30 g_266 g_192 g_87 g_303 g_751 g_771
 */
static const int64_t  func_55(int8_t * const  p_56, int32_t * p_57, int32_t ** p_58, const uint32_t  p_59, int32_t * const  p_60)
{ /* block id: 26 */
    int32_t l_94 = 3L;
    int32_t l_102 = 1L;
    int32_t l_114 = 0x90D02F7EL;
    int32_t ** const *l_115 = &g_113[7][3];
    int32_t ** const **l_129 = &l_115;
    int32_t ***l_131 = &g_113[4][3];
    int32_t ****l_130[9] = {&l_131,&l_131,&l_131,&l_131,&l_131,&l_131,&l_131,&l_131,&l_131};
    int64_t *l_141 = &g_142;
    int16_t *l_143 = &g_105;
    uint64_t l_163 = 0x973355EA1934B4E1LL;
    uint32_t l_245 = 0xF28909F1L;
    int16_t l_275 = 0x7701L;
    uint32_t l_281[2];
    uint8_t *l_286 = &g_190;
    float *l_298[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    float **l_297 = &l_298[4];
    uint16_t l_336 = 0xAB8DL;
    uint32_t l_351 = 0x74943235L;
    int64_t l_383 = 0L;
    int8_t l_385 = 0x23L;
    int32_t l_409 = 0xE7AED57EL;
    int16_t l_458 = 1L;
    int32_t l_500 = 0x8E9C54DFL;
    int32_t l_502 = 0xFE3EF0EDL;
    int8_t l_517 = 0x20L;
    uint32_t l_579[1];
    int16_t l_584 = 0x9D9AL;
    uint32_t *l_662 = &g_160[3];
    const uint64_t l_663 = 8UL;
    const int8_t *l_727 = &g_30[2][6];
    const int8_t **l_726 = &l_727;
    int8_t l_788 = 0xDEL;
    uint32_t l_794 = 0xFB930586L;
    int i;
    for (i = 0; i < 2; i++)
        l_281[i] = 0x33EE4FA2L;
    for (i = 0; i < 1; i++)
        l_579[i] = 0UL;
    for (g_92 = 0; (g_92 <= 6); g_92 += 1)
    { /* block id: 29 */
        int32_t *l_95 = (void*)0;
        int32_t *l_96 = &l_94;
        int32_t *l_97 = &g_46;
        int32_t *l_98 = (void*)0;
        int32_t l_99 = 0x75BA80B3L;
        int32_t l_100 = 0x0C1829B6L;
        int32_t *l_101 = (void*)0;
        int32_t *l_103[4][3] = {{&l_99,&l_99,&l_99},{&g_6,&g_6,&g_6},{&l_99,&l_99,&l_99},{&g_6,&g_6,&g_6}};
        int8_t *l_110 = (void*)0;
        int8_t **l_109 = &l_110;
        int32_t ***l_112 = (void*)0;
        int i, j;
        g_106[3][0][3]--;
        l_114 &= (((p_59 < (((((*l_109) = &g_30[1][4]) == (g_111[5] = &g_30[4][4])) != ((g_92 , p_56) == ((&l_97 != (g_113[7][3] = &g_84)) , p_56))) ^ (((((*g_84) , l_102) <= l_94) | 0x0AA37635L) <= 0xD5L))) | g_30[1][0]) ^ p_59);
        for (g_104 = 6; (g_104 >= 0); g_104 -= 1)
        { /* block id: 37 */
            int32_t ** const **l_116 = (void*)0;
            int32_t ** const **l_117 = (void*)0;
            int32_t ** const **l_118[1];
            int i;
            for (i = 0; i < 1; i++)
                l_118[i] = (void*)0;
            l_115 = l_115;
        }
        return p_59;
    }
    l_114 = (**p_58);
    if ((((safe_mul_func_int16_t_s_s(((*l_143) &= (safe_sub_func_int64_t_s_s(((*l_141) &= (g_46 || (safe_lshift_func_int8_t_s_s((l_102 , (0x479FL < (((safe_div_func_float_f_f(0xB.95B8FDp-71, ((-0x2.Bp+1) == ((safe_mul_func_int8_t_s_s((((*l_129) = &p_58) == (g_132 = &p_58)), (safe_div_func_int64_t_s_s((safe_mul_func_int8_t_s_s((safe_add_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(((void*)0 != p_56), p_59)), g_44)), 8L)), 0x765F4E788166B7E9LL)))) , g_106[3][0][3])))) == 0xA.327EC8p-28) , p_59))), 7)))), g_13))), p_59)) | g_87) || p_59))
    { /* block id: 47 */
        uint32_t l_148[2];
        int32_t l_193 = (-1L);
        int32_t l_225 = (-1L);
        int32_t l_243 = 0xFC331715L;
        int32_t l_244 = 0xF5727A9AL;
        int32_t l_273 = 0xE96C51E9L;
        int32_t l_274 = 1L;
        int32_t l_276 = 0x3294FB15L;
        int32_t l_277[4] = {(-9L),(-9L),(-9L),(-9L)};
        int64_t *l_319 = (void*)0;
        const int64_t l_337 = 0x174DFCF0DD83AAD2LL;
        int8_t l_352 = 0x8FL;
        int8_t l_365 = 0x87L;
        int32_t l_386 = 1L;
        uint8_t l_387[3];
        uint32_t l_455[6][7] = {{18446744073709551606UL,0x741467CFL,7UL,7UL,0x9F9CC467L,18446744073709551611UL,18446744073709551606UL},{18446744073709551606UL,18446744073709551615UL,0x8AAB3CDEL,7UL,0x8AAB3CDEL,18446744073709551615UL,18446744073709551606UL},{0xBD30FE8CL,0x741467CFL,0x8AAB3CDEL,18446744073709551615UL,0x9F9CC467L,18446744073709551615UL,0xBD30FE8CL},{18446744073709551606UL,0x741467CFL,7UL,7UL,0x9F9CC467L,18446744073709551611UL,18446744073709551606UL},{18446744073709551606UL,18446744073709551615UL,0x8AAB3CDEL,7UL,0x8AAB3CDEL,18446744073709551615UL,18446744073709551606UL},{0xBD30FE8CL,0x741467CFL,0x8AAB3CDEL,18446744073709551615UL,0x9F9CC467L,18446744073709551615UL,0xBD30FE8CL}};
        uint64_t l_494 = 0xEBB07E3D37425BA7LL;
        uint8_t l_501 = 3UL;
        int32_t *l_504 = &l_225;
        int i, j;
        for (i = 0; i < 2; i++)
            l_148[i] = 0UL;
        for (i = 0; i < 3; i++)
            l_387[i] = 0x8DL;
        for (l_114 = (-19); (l_114 <= (-13)); l_114++)
        { /* block id: 50 */
            int8_t l_149 = 0x9AL;
            uint16_t *l_150[1][5][7] = {{{&g_92,&g_87,&g_92,&g_87,&g_92,&g_87,&g_92},{&g_92,(void*)0,&g_87,&g_92,&g_87,(void*)0,&g_92},{&g_92,&g_87,&g_92,&g_87,&g_92,&g_87,&g_92},{&g_92,(void*)0,&g_87,&g_92,&g_87,(void*)0,&g_92},{&g_92,&g_87,&g_92,&g_87,&g_92,&g_87,&g_92}}};
            int32_t l_151 = 1L;
            uint32_t *l_159 = &g_160[3];
            uint32_t *l_161 = &g_162;
            int8_t *l_164 = &g_30[1][5];
            const float l_220[9] = {0xC.F5C828p-96,0x8.4A78F7p+61,0xC.F5C828p-96,0x8.4A78F7p+61,0xC.F5C828p-96,0x8.4A78F7p+61,0xC.F5C828p-96,0x8.4A78F7p+61,0xC.F5C828p-96};
            int32_t *l_228[8][7][1] = {{{&l_225},{&l_102},{&g_13},{&g_6},{&g_13},{&l_102},{&l_225}},{{&l_102},{&g_13},{&g_6},{&g_13},{&l_102},{&l_225},{&l_102}},{{&g_13},{&g_6},{&g_13},{&l_102},{&l_225},{&g_6},{&l_225}},{{&g_44},{&l_225},{&g_6},{&g_9},{&g_6},{&l_225},{&g_44}},{{&l_225},{&g_6},{&g_9},{&g_6},{&l_225},{&g_44},{&l_225}},{{&g_6},{&g_9},{&g_6},{&l_225},{&g_44},{&l_225},{&g_6}},{{&g_9},{&g_6},{&l_225},{&g_44},{&l_225},{&g_6},{&g_9}},{{&g_6},{&l_225},{&g_44},{&l_225},{&g_6},{&g_9},{&g_6}}};
            int16_t l_240 = 8L;
            int32_t *l_320 = &l_244;
            uint8_t **l_354[3];
            int32_t l_445 = (-8L);
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_354[i] = &l_286;
        }
        ++g_459;
        (*p_57) = (~0x61857EDE482C3C41LL);
        if ((!l_277[2]))
        { /* block id: 179 */
            int16_t l_469 = 0x5EB4L;
            for (l_94 = 0; (l_94 < (-10)); l_94--)
            { /* block id: 182 */
                return p_59;
            }
            for (l_386 = 7; (l_386 >= 2); l_386 -= 1)
            { /* block id: 187 */
                int32_t l_466 = 0L;
                if ((**p_58))
                { /* block id: 188 */
                    uint32_t l_470 = 0xC39EC429L;
                    ++l_470;
                }
                else
                { /* block id: 190 */
                    uint32_t l_473[7][3][9] = {{{0x79766797L,7UL,1UL,8UL,0xF476E670L,0xD9EEAE14L,0xB761C147L,4294967295UL,1UL},{4294967288UL,1UL,0x79766797L,8UL,1UL,4294967295UL,5UL,0xE33B5475L,0x6187F734L},{0x6AC31E08L,4294967295UL,1UL,0UL,0xC734B7BAL,0xDC547D12L,7UL,4UL,0x0E1F2B49L}},{{4294967286UL,0x2A392CF7L,0xAEDEFA6EL,0x8F46643FL,0x8F46643FL,0xAEDEFA6EL,0x2A392CF7L,4294967286UL,0x00FE0485L},{0xFE6C8EF1L,4294967286UL,4294967290UL,5UL,4UL,9UL,0UL,4294967287UL,0x8D047683L},{0x4F18BEC3L,4294967295UL,8UL,3UL,5UL,4294967290UL,0xFBAF2102L,0x7B19BAE8L,0x00FE0485L}},{{0x662860BDL,0xC734B7BAL,0x24120D5FL,0x6AC31E08L,9UL,0x2A392CF7L,0xE8054638L,0xD9EEAE14L,0x0E1F2B49L},{0xC734B7BAL,1UL,0xE8054638L,0x24120D5FL,4294967295UL,4294967287UL,0x871B90EEL,4294967295UL,0x6187F734L},{4294967294UL,0xFBAF2102L,1UL,7UL,4294967295UL,0x8F46643FL,8UL,0x662860BDL,1UL}},{{0xDC547D12L,0x8D047683L,0UL,0xC0127614L,4294967295UL,0xFE6C8EF1L,0x7B19BAE8L,7UL,0xFBAF2102L},{4294967295UL,0xD9EEAE14L,3UL,9UL,4294967295UL,4294967295UL,0x00FE0485L,0x8D047683L,0x4F18BEC3L},{0UL,0x5A66CA30L,7UL,0x8593839AL,9UL,0x8593839AL,7UL,0x5A66CA30L,0UL}},{{7UL,3UL,4294967293UL,0UL,5UL,1UL,0x6AC31E08L,0xE8054638L,0x8EA6B741L},{9UL,0x88EBCD65L,5UL,0x8D047683L,4UL,0x6187F734L,0x8F46643FL,0x6AC31E08L,0xC734B7BAL},{7UL,5UL,0x6AC31E08L,0xB761C147L,0x8F46643FL,1UL,0xA4060EC5L,4294967290UL,0x8593839AL}},{{0UL,0xF476E670L,0x8EA6B741L,4294967295UL,0xC734B7BAL,4294967286UL,0x8593839AL,0x19280C0DL,0xE33B5475L},{4294967295UL,0x8593839AL,1UL,4294967293UL,1UL,4294967294UL,0x6187F734L,4294967294UL,4294967295UL},{0xDC547D12L,4294967287UL,0xC734B7BAL,4294967295UL,0x2A392CF7L,0x88EBCD65L,0xC0127614L,1UL,0x19280C0DL}},{{0x88EBCD65L,9UL,4294967294UL,0x8D047683L,1UL,0x79766797L,0xE33B5475L,0UL,7UL},{0x0E1F2B49L,5UL,1UL,0x4F18BEC3L,4294967286UL,4294967286UL,0x4F18BEC3L,1UL,5UL},{1UL,4294967286UL,4294967295UL,0x00FE0485L,0x662860BDL,7UL,0xB761C147L,4294967293UL,0xAEDEFA6EL}}};
                    int i, j, k;
                    --l_473[5][1][6];
                }
            }
            (*p_57) |= (-4L);
        }
        else
        { /* block id: 195 */
            int32_t ****l_478 = &l_131;
            uint32_t l_503 = 18446744073709551609UL;
            l_243 |= ((safe_lshift_func_uint8_t_u_u((l_478 != &g_132), g_44)) >= 0x3DL);
            l_503 = (safe_div_func_float_f_f(((p_59 != (safe_mul_func_float_f_f((((*g_427) , (safe_div_func_float_f_f((l_501 = ((safe_sub_func_float_f_f(0xC.B4E515p-8, (safe_div_func_float_f_f((safe_mul_func_float_f_f((+(l_500 = (((*g_303) , ((*g_427) = 0x6.3C663Fp+4)) != (safe_mul_func_float_f_f((l_494 , (safe_add_func_float_f_f((p_59 , ((+(p_59 , (safe_div_func_float_f_f(p_59, p_59)))) > 0x9.Fp-1)), p_59))), 0x1.F5BFCEp+70))))), p_59)), (-0x2.Cp+1))))) <= 0x0.AD85CFp+12)), 0xA.8E4D87p-74))) != l_502), p_59))) != 0x6.5B79D9p+19), p_59));
            l_504 = ((*p_58) = (*p_58));
        }
    }
    else
    { /* block id: 204 */
        int16_t l_507[9][2][7] = {{{0x3A80L,1L,0x0EF9L,7L,0xA5A5L,0xA5A5L,7L},{(-1L),5L,(-1L),1L,(-8L),(-5L),0x376EL}},{{7L,0x3A80L,0x3066L,(-1L),0x3FF2L,(-8L),0xAE28L},{0xA94DL,0x376EL,1L,0x62F0L,0x2A79L,(-5L),0xA94DL}},{{(-1L),0xA698L,0L,0xA5A5L,0xAE28L,0xA5A5L,0L},{0x62F0L,0x62F0L,0L,(-8L),0xA2D7L,(-1L),(-3L)}},{{0x3FF2L,0L,1L,0x3FF2L,0xE882L,0L,7L},{(-1L),0xA2D7L,1L,0x2A79L,0xA2D7L,0x1C7EL,1L}},{{(-1L),(-5L),0x3066L,0xAE28L,0xAE28L,0x3066L,(-5L)},{(-3L),1L,0x1C7EL,0xA2D7L,0x2A79L,1L,0xA2D7L}},{{0x0EF9L,7L,0L,0xE882L,0x3FF2L,1L,0L},{3L,(-3L),(-1L),0xA2D7L,(-8L),0L,0x62F0L}},{{0xA698L,0L,0xA5A5L,0xAE28L,0xA5A5L,0L,0xA698L},{0x1C7EL,0xA94DL,(-5L),0x2A79L,0x62F0L,1L,0x376EL}},{{0L,0xAE28L,(-8L),0x3FF2L,(-1L),0x3066L,0x3A80L},{(-8L),0x376EL,(-5L),(-8L),1L,(-1L),5L}},{{0x0EF9L,7L,0xA5A5L,0xA5A5L,7L,0x0EF9L,1L},{0xA2D7L,3L,(-1L),0x62F0L,0xA94DL,(-1L),(-8L)}}};
        uint32_t l_514 = 0xF3461FB7L;
        int32_t **l_515 = (void*)0;
        int16_t l_580 = 0x5E80L;
        int64_t l_620 = 0x563B8E7DEECD70EDLL;
        uint8_t l_621 = 2UL;
        int32_t l_631 = 4L;
        int32_t l_633 = 0x5C1C305DL;
        int32_t l_639 = (-8L);
        int32_t l_641 = 0xC36A85D4L;
        int32_t l_647 = (-5L);
        int32_t l_651 = 1L;
        int i, j, k;
        for (g_92 = 0; (g_92 >= 28); g_92 = safe_add_func_uint32_t_u_u(g_92, 1))
        { /* block id: 207 */
            int32_t **l_516 = &g_84;
            (*g_427) = (*g_427);
            (*p_57) &= (g_160[2] == p_59);
            (*p_57) |= ((l_507[5][1][5] ^ (safe_mul_func_uint8_t_u_u((((((((safe_rshift_func_int8_t_s_u(((**p_58) , (((*l_143) = (-5L)) , p_59)), 7)) ^ (safe_mod_func_uint64_t_u_u((g_30[6][0] , l_514), 0xA57A1EFF69008CA3LL))) < ((((((0x6CF44ED7F2A1B3A7LL <= ((l_515 == l_516) , g_6)) && (-1L)) , p_59) >= (**l_516)) , (*g_303)) != 2UL)) && p_59) < (**l_516)) ^ p_59) >= g_9), p_59))) , l_517);
        }
lbl_664:
        for (l_245 = (-28); (l_245 >= 23); l_245 = safe_add_func_uint16_t_u_u(l_245, 5))
        { /* block id: 215 */
            uint32_t l_533[8];
            int32_t l_534 = (-7L);
            int32_t l_581 = 0L;
            int32_t l_591 = 0L;
            int32_t *** const l_612 = &g_113[2][3];
            uint8_t l_618 = 0x5BL;
            uint64_t l_622 = 0x012088B5780A55FDLL;
            int32_t l_637[7][1][7] = {{{0xA9EFCD6BL,1L,0x3802D5ADL,1L,0xA9EFCD6BL,0x7BB1E311L,0x7BB1E311L}},{{(-5L),0x4018C8FAL,0x949759AFL,0x4018C8FAL,(-5L),(-2L),(-2L)}},{{0xA9EFCD6BL,1L,0x3802D5ADL,1L,0xA9EFCD6BL,0x7BB1E311L,0x7BB1E311L}},{{(-5L),0x4018C8FAL,0x949759AFL,0x4018C8FAL,(-5L),(-2L),(-2L)}},{{0xA9EFCD6BL,1L,2L,9L,0x7BB1E311L,0x3802D5ADL,0x3802D5ADL}},{{(-2L),(-2L),0xC4F0BA4DL,(-2L),(-2L),0x949759AFL,0x949759AFL}},{{0x7BB1E311L,9L,2L,9L,0x7BB1E311L,0x3802D5ADL,0x3802D5ADL}}};
            uint16_t l_652 = 1UL;
            int i, j, k;
            for (i = 0; i < 8; i++)
                l_533[i] = 18446744073709551615UL;
            if ((safe_rshift_func_uint16_t_u_u(g_46, ((g_105 | ((g_104 = (safe_sub_func_int64_t_s_s((safe_add_func_uint64_t_u_u(g_30[2][5], p_59)), (safe_mul_func_int16_t_s_s((g_304[1][2] || ((p_59 || (safe_mul_func_int8_t_s_s((l_533[7] = ((safe_mod_func_int64_t_s_s((*g_303), p_59)) , (((safe_unary_minus_func_int64_t_s(1L)) > 0xFBA4E223383F2871LL) || 0xCB5AL))), 0x20L))) < 0x7DL)), l_534))))) , g_142)) <= 0xFDEC0C18L))))
            { /* block id: 218 */
                const uint32_t l_535 = 4294967295UL;
                int32_t ***l_562 = &g_113[4][2];
                uint32_t *l_567 = &l_514;
                uint16_t *l_575 = &g_87;
                uint16_t **l_574 = &l_575;
                uint16_t *l_577 = &g_92;
                uint16_t **l_576[7][5][7] = {{{(void*)0,&l_577,&l_577,(void*)0,&l_577,&l_577,&l_577},{(void*)0,&l_577,&l_577,&l_577,&l_577,(void*)0,(void*)0},{&l_577,&l_577,(void*)0,(void*)0,&l_577,(void*)0,&l_577},{(void*)0,&l_577,&l_577,(void*)0,(void*)0,(void*)0,(void*)0},{&l_577,&l_577,&l_577,&l_577,&l_577,&l_577,&l_577}},{{&l_577,&l_577,&l_577,(void*)0,(void*)0,&l_577,&l_577},{&l_577,&l_577,(void*)0,&l_577,&l_577,&l_577,(void*)0},{&l_577,(void*)0,&l_577,&l_577,&l_577,&l_577,(void*)0},{&l_577,&l_577,&l_577,&l_577,&l_577,(void*)0,&l_577},{(void*)0,&l_577,(void*)0,&l_577,&l_577,(void*)0,&l_577}},{{&l_577,&l_577,&l_577,&l_577,&l_577,&l_577,(void*)0},{(void*)0,(void*)0,(void*)0,&l_577,(void*)0,&l_577,&l_577},{(void*)0,&l_577,&l_577,(void*)0,(void*)0,&l_577,(void*)0},{&l_577,(void*)0,&l_577,&l_577,&l_577,&l_577,&l_577},{&l_577,(void*)0,&l_577,&l_577,&l_577,(void*)0,(void*)0}},{{&l_577,(void*)0,(void*)0,&l_577,&l_577,(void*)0,(void*)0},{(void*)0,&l_577,&l_577,&l_577,&l_577,&l_577,&l_577},{&l_577,&l_577,(void*)0,&l_577,(void*)0,&l_577,(void*)0},{&l_577,&l_577,(void*)0,&l_577,&l_577,&l_577,&l_577},{&l_577,&l_577,&l_577,&l_577,&l_577,&l_577,&l_577}},{{&l_577,&l_577,&l_577,&l_577,&l_577,(void*)0,&l_577},{&l_577,&l_577,&l_577,&l_577,&l_577,(void*)0,&l_577},{&l_577,&l_577,&l_577,&l_577,&l_577,(void*)0,&l_577},{&l_577,&l_577,&l_577,&l_577,&l_577,&l_577,&l_577},{&l_577,&l_577,&l_577,&l_577,&l_577,&l_577,&l_577}},{{(void*)0,&l_577,&l_577,(void*)0,&l_577,&l_577,&l_577},{&l_577,&l_577,&l_577,&l_577,&l_577,&l_577,&l_577},{(void*)0,&l_577,&l_577,&l_577,&l_577,&l_577,&l_577},{&l_577,(void*)0,&l_577,&l_577,&l_577,&l_577,&l_577},{&l_577,(void*)0,(void*)0,&l_577,&l_577,&l_577,&l_577}},{{&l_577,&l_577,&l_577,(void*)0,&l_577,&l_577,&l_577},{&l_577,&l_577,(void*)0,&l_577,(void*)0,&l_577,&l_577},{(void*)0,&l_577,&l_577,&l_577,&l_577,(void*)0,&l_577},{&l_577,&l_577,&l_577,&l_577,(void*)0,&l_577,&l_577},{(void*)0,(void*)0,&l_577,&l_577,&l_577,&l_577,&l_577}}};
                int i, j, k;
                if (((*p_57) = (l_535 >= l_535)))
                { /* block id: 220 */
                    uint64_t l_542 = 1UL;
                    (*p_57) &= (6L >= (((p_59 <= (safe_div_func_float_f_f(0xC.C394D7p-77, (((safe_unary_minus_func_int32_t_s((!l_542))) , (((*g_427) = ((*g_427) > p_59)) == ((((safe_add_func_float_f_f(0x1.Cp+1, (((-0x1.2p+1) <= ((safe_div_func_float_f_f(((l_534 = l_535) <= l_542), p_59)) == p_59)) < 0x1.Cp+1))) >= p_59) < 0x8.CF70ABp-14) != 0x1.4p-1))) <= l_535)))) , &g_304[1][2]) == (void*)0));
                    if ((safe_lshift_func_int8_t_s_s(l_542, 4)))
                    { /* block id: 224 */
                        uint8_t l_559 = 0x1FL;
                        int8_t *l_560[9][6] = {{&g_30[6][0],(void*)0,&l_385,&l_385,&g_266,(void*)0},{(void*)0,&l_385,&g_266,&g_266,&g_266,&l_385},{(void*)0,(void*)0,&g_266,&l_385,&l_385,(void*)0},{&g_30[6][0],&g_266,(void*)0,&l_385,&l_385,(void*)0},{&g_266,&g_266,&g_30[6][0],&g_30[6][0],&l_385,&l_385},{&l_385,(void*)0,&l_385,&g_30[6][0],&g_266,&g_30[6][0]},{&l_385,&l_385,&l_385,(void*)0,&g_266,&l_385},{&g_266,(void*)0,&g_30[6][0],&g_30[6][0],(void*)0,(void*)0},{&g_30[6][0],(void*)0,(void*)0,&g_30[6][0],&g_30[6][0],(void*)0}};
                        int32_t l_561 = (-5L);
                        int i, j;
                        (*l_131) = ((safe_div_func_float_f_f(p_59, ((*g_427) = ((safe_sub_func_float_f_f(l_533[5], (safe_mul_func_float_f_f((((safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s(p_59, (l_561 = (0x53AEL || l_559)))), 0xD6L)) > (&p_58 != (p_59 , l_562))) , 0xE.86AE2Bp+56), l_542)))) <= (*g_427))))) , (*g_132));
                    }
                    else
                    { /* block id: 228 */
                        uint8_t l_563[6][4] = {{2UL,0x9EL,0xDCL,0x9EL},{0x9EL,0xE2L,0xDCL,0xDCL},{2UL,2UL,0x9EL,0xDCL},{246UL,0xE2L,246UL,0x9EL},{246UL,0x9EL,0x9EL,246UL},{2UL,0x9EL,0xDCL,0x9EL}};
                        int i, j;
                        l_563[4][3] ^= l_534;
                    }
                }
                else
                { /* block id: 231 */
                    return (*g_303);
                }
                l_581 &= ((((((g_162 > (((safe_mul_func_uint8_t_u_u(255UL, ((((*l_567) = (l_534 &= ((g_566 = &g_160[3]) != &l_535))) || (safe_add_func_int64_t_s_s(((((**p_58) == ((*g_566) = ((*l_567) = p_59))) , (safe_div_func_int32_t_s_s(((safe_rshift_func_uint8_t_u_s(((*l_286) = ((((*l_574) = &g_92) == (g_578 = &l_336)) == ((((p_59 | 0L) != 0xC017L) != 18446744073709551615UL) >= l_579[0]))), l_533[7])) | l_533[7]), 1L))) , 0x9BA28E84503CEB7DLL), 0x30ABCFF50843E1A0LL))) < g_162))) , l_580) >= (*p_57))) || (*g_303)) , p_59) , (void*)0) == (void*)0) , (*g_84));
            }
            else
            { /* block id: 243 */
                float l_590 = (-0x4.Ap+1);
                int32_t ** const *l_613 = &l_515;
                int32_t l_636 = 0x7FA5158FL;
                int32_t l_638 = 1L;
                int32_t l_642[8][6][5] = {{{2L,0xD72DC826L,7L,0x95D8F6D0L,(-1L)},{0x4CBFF2D1L,0x5B0DE157L,0x4F639156L,(-1L),0x07AA667FL},{(-6L),9L,0x006C090BL,0x5A50B2D8L,7L},{(-1L),0L,0x5A50B2D8L,0xD7060BD7L,0xC8C9D620L},{0xBF0E4A2BL,0x07AA667FL,1L,0x7E613A3DL,0x07AA667FL},{(-1L),0x7E613A3DL,0L,7L,(-1L)}},{{0xD7060BD7L,1L,1L,0L,7L},{0xE1B9C0F5L,(-1L),0x54D2A14EL,2L,0x8899B08FL},{(-6L),(-1L),0L,0x122687A1L,1L},{0xC8C9D620L,0xFAF0B633L,0xBD302361L,5L,0L},{(-6L),0xD5510A5CL,(-1L),1L,(-8L)},{(-8L),0xD5510A5CL,0xE7851DA5L,0x4E31ABA8L,1L}},{{(-1L),0xFAF0B633L,0L,8L,0x284DB795L},{0xBB54DE95L,(-1L),0xD7060BD7L,0x71CBB042L,0L},{0x32F6C7C6L,0xE81B5E91L,1L,0L,0xC8C9D620L},{0x00E7F2F4L,0x3A984FA0L,0xEAA6EEBDL,0L,0x7AB20305L},{1L,(-1L),0x114C7EB9L,0x5A50B2D8L,0xD5510A5CL},{0x4F639156L,0xC9EFA26DL,0xE7851DA5L,0x83863083L,0x4D427B10L}},{{(-1L),0x284DB795L,0x3A984FA0L,1L,(-6L)},{0x71CBB042L,0x5A50B2D8L,0x928B3D1DL,0x5A50B2D8L,0x71CBB042L},{1L,0x71CBB042L,0L,0xFAF0B633L,0L},{5L,(-8L),0x08DF0045L,(-1L),0x122687A1L},{0x8899B08FL,0x006C090BL,0xEA5EEC75L,0x71CBB042L,0L},{0xB36136FFL,(-1L),0x7AB20305L,0x52329993L,0x71CBB042L}},{{0L,0x3C0E74F0L,9L,1L,(-6L)},{(-1L),7L,0x7E613A3DL,1L,0x4D427B10L},{0x7AB20305L,0xAA2CD948L,0x54D2A14EL,0x211B6A6DL,0xD5510A5CL},{0L,0xE7851DA5L,(-1L),0xE81B5E91L,0x7AB20305L},{(-2L),0x00E7F2F4L,0x57E9432AL,0L,0xC8C9D620L},{0x93A310CFL,2L,1L,0xB36136FFL,0L}},{{5L,0xBD11887AL,0x4E31ABA8L,1L,0x284DB795L},{1L,0L,0xB4E672CFL,(-1L),1L},{0x08DF0045L,0xAA2CD948L,0x32F6C7C6L,(-6L),(-8L)},{(-1L),(-1L),0x32F6C7C6L,0x157B768EL,0L},{2L,(-7L),0xB4E672CFL,2L,1L},{(-1L),(-1L),0x4E31ABA8L,(-6L),0x8899B08FL}},{{0x00E7F2F4L,(-6L),1L,0xC9EFA26DL,0xE11B7FE4L},{0L,7L,0x57E9432AL,(-1L),2L},{0x284DB795L,0x71CBB042L,(-1L),0x4F639156L,0xC9EFA26DL},{(-1L),1L,0x54D2A14EL,0x157B768EL,0L},{7L,0xBB54DE95L,0x7E613A3DL,0x006C090BL,0L},{0xBD302361L,0xC9EFA26DL,9L,9L,0xC9EFA26DL}},{{0xC8C9D620L,9L,0x7AB20305L,1L,0x4F639156L},{0x006C090BL,0xD72DC826L,0xEA5EEC75L,(-2L),1L},{0x3A984FA0L,0xE81B5E91L,0x08DF0045L,0x32F6C7C6L,0x8899B08FL},{0x006C090BL,(-6L),0L,0xE81B5E91L,(-6L)},{0L,0x383E9C24L,(-1L),0xBD11887AL,(-1L)},{0x114C7EB9L,0x08DF0045L,0L,0xEA5EEC75L,0x4E31ABA8L}}};
                int i, j, k;
                l_591 |= (((safe_rshift_func_uint16_t_u_u(p_59, (l_581 , l_584))) > (((safe_unary_minus_func_uint8_t_u(((*l_286) |= ((p_59 && l_534) < ((void*)0 != &l_534))))) , (((*g_427) = (((*p_57) <= (((*g_578) ^= ((safe_lshift_func_uint8_t_u_u(((safe_unary_minus_func_int8_t_s((!g_13))) >= (*g_566)), 4)) < 0x37D2L)) <= p_59)) , p_59)) , 0L)) , g_160[3])) == p_59);
                for (l_591 = 0; (l_591 == 27); l_591++)
                { /* block id: 250 */
                    if ((*p_60))
                        break;
                }
                for (l_385 = 0; (l_385 != (-30)); l_385--)
                { /* block id: 255 */
                    uint8_t *l_616 = &g_617;
                    int32_t l_619 = 0xD24F4D39L;
                    uint16_t **l_623 = &g_578;
                    int16_t l_625 = 0x70A0L;
                    int32_t l_646 = 0x7F27B21FL;
                    uint32_t l_655 = 0xC6C1D8BFL;
                    if ((l_622 = ((*g_566) > (safe_lshift_func_int16_t_s_u(((safe_mul_func_int16_t_s_s((l_619 = ((safe_add_func_int32_t_s_s(2L, ((((*l_141) = (p_59 <= (g_190 == 0L))) > ((safe_mod_func_uint8_t_u_u(((((safe_sub_func_int8_t_s_s((safe_sub_func_uint16_t_u_u((*g_578), (((safe_div_func_int64_t_s_s(((*g_303) ^= ((((*l_616) |= ((l_612 == l_613) >= ((safe_mod_func_uint16_t_u_u(p_59, 0x3694L)) <= 7L))) > l_618) || 0xCBE4L)), l_619)) , 0xE1L) >= p_59))), l_620)) | (*g_566)) > 18446744073709551615UL) == (-1L)), g_30[1][4])) , g_106[3][4][1])) | p_59))) | p_59)), p_59)) | l_621), 3)))))
                    { /* block id: 261 */
                        (*p_57) &= (l_623 != &g_578);
                        if ((*p_60))
                            continue;
                        if ((*g_84))
                            break;
                        l_534 |= ((*p_57) = (*p_60));
                    }
                    else
                    { /* block id: 267 */
                        int32_t l_624[4];
                        int32_t l_626 = (-9L);
                        int32_t l_627 = 0x1176A0A6L;
                        int32_t l_628 = 1L;
                        int32_t l_629 = 0x9C2FC10DL;
                        int32_t l_630 = 0L;
                        int32_t l_632 = (-5L);
                        int32_t l_634 = 0x234EE096L;
                        int32_t l_635 = 0xCF64106AL;
                        int32_t l_640 = (-1L);
                        int32_t l_643 = 1L;
                        int32_t l_644 = 0xBD68A164L;
                        int32_t l_645 = 2L;
                        int32_t l_648 = 0x5FC69B45L;
                        int32_t l_649 = 3L;
                        int32_t l_650[3];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_624[i] = 0x90D5C1CBL;
                        for (i = 0; i < 3; i++)
                            l_650[i] = 0x0C70FDDCL;
                        --l_652;
                    }
                    l_619 = ((*p_57) = 2L);
                    l_655--;
                }
                for (l_622 = 0; (l_622 != 29); l_622++)
                { /* block id: 276 */
                    uint32_t *l_660 = (void*)0;
                    uint32_t **l_661[1];
                    int i;
                    for (i = 0; i < 1; i++)
                        l_661[i] = &g_566;
                    (*g_427) = ((l_662 = l_660) != (void*)0);
                    if (l_502)
                        goto lbl_664;
                }
            }
            return l_663;
        }
        if ((*p_60))
        { /* block id: 284 */
            int32_t ** const **l_685[8][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}};
            int i, j;
            for (l_620 = 4; (l_620 >= 0); l_620 -= 1)
            { /* block id: 287 */
                int32_t l_665 = 0x7DD98502L;
                int32_t l_666[5][10] = {{0L,0xB66B8345L,0L,0xB66B8345L,0L,0L,(-1L),(-1L),0L,0L},{0L,(-6L),(-6L),0L,0x6980AB8BL,1L,0L,1L,0x6980AB8BL,0L},{1L,0L,1L,0x6980AB8BL,0L,(-6L),(-6L),0L,0x6980AB8BL,1L},{(-1L),(-1L),0L,0L,0xB66B8345L,0L,0xB66B8345L,0L,0L,(-1L)},{0xB66B8345L,(-6L),1L,0xB66B8345L,0x6980AB8BL,0x6980AB8BL,0xB66B8345L,1L,(-6L),0xB66B8345L}};
                int8_t *l_667 = &g_30[6][0];
                uint32_t l_672 = 0UL;
                int32_t l_686[6][4] = {{0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL},{0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL},{0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL},{0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL},{0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL},{0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL,0x9E85AFFFL}};
                uint8_t **l_690 = &l_286;
                int i, j;
                l_686[5][1] &= ((((((-10L) < (((*l_667) = (l_666[3][8] = l_665)) <= ((((safe_add_func_int64_t_s_s((((*g_427) = (*g_427)) , ((safe_add_func_int32_t_s_s((l_672 ^= (*p_57)), (((safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_div_func_float_f_f((0x5.7D6FBBp+40 != p_59), ((p_59 == (((((safe_rshift_func_uint8_t_u_s(((safe_rshift_func_int16_t_s_s(0xF5B0L, (((safe_sub_func_int16_t_s_s((&g_132 == l_685[2][1]), l_507[2][1][6])) <= 2L) >= p_59))) ^ 1L), 0)) | 0x9731L) != 6UL) <= (-1L)) , 0xA.E8EEB2p+85)) != l_665))), (-0x1.8p+1))), 0x0.0p-1)) , 0x42L) & g_13))) | 0L)), p_59)) <= p_59) == (*g_566)) <= 0x9063L))) & g_6) < p_59) , p_59) != l_665);
                for (g_266 = 0; (g_266 <= 6); g_266 += 1)
                { /* block id: 295 */
                    int32_t l_723 = 0x069C75B0L;
                    uint64_t *l_728 = &l_163;
                    uint16_t *l_743 = &g_87;
                    float **l_749 = &l_298[6];
                    int i, j;
                    if (g_30[l_620][(l_620 + 1)])
                        break;
                    if ((*p_60))
                    { /* block id: 297 */
                        int8_t l_687[9][10] = {{0x3BL,0x52L,0xFBL,1L,1L,0xFBL,0x52L,0x3BL,0x52L,0xFBL},{0x08L,(-1L),1L,(-1L),0x08L,0xFBL,0xFBL,0x08L,(-1L),1L},{0x3BL,0x3BL,1L,0x08L,0xF6L,0x08L,1L,0x3BL,0x3BL,1L},{(-1L),0x08L,0xFBL,0xFBL,0x08L,(-1L),1L,(-1L),0x08L,0xFBL},{0x52L,0x3BL,0x52L,0xFBL,1L,1L,0xFBL,0x52L,0x3BL,0x52L},{0x52L,(-1L),0x3BL,0x08L,0x3BL,(-1L),0x52L,0x52L,(-1L),0x3BL},{(-1L),0x52L,0x52L,(-1L),0x3BL,0x08L,0x3BL,(-1L),0x52L,0x52L},{0x3BL,0x52L,0xFBL,1L,0x3BL,1L,0x08L,0xF6L,0x08L,1L},{0xFBL,0x52L,0x3BL,0x52L,0xFBL,1L,1L,0xFBL,0x52L,0x3BL}};
                        int i, j;
                        l_687[5][3] = 0x7.4p-1;
                    }
                    else
                    { /* block id: 299 */
                        int32_t *l_693 = &l_686[5][0];
                        uint64_t *l_718 = &g_106[1][6][5];
                        uint64_t **l_717 = &l_718;
                        uint64_t *l_721 = &l_163;
                        uint64_t *l_722[1][8][2] = {{{&g_106[3][0][3],(void*)0},{(void*)0,&g_106[3][0][3]},{(void*)0,(void*)0},{&g_106[3][0][3],(void*)0},{(void*)0,&g_106[3][0][3]},{(void*)0,(void*)0},{&g_106[3][0][3],(void*)0},{(void*)0,&g_106[3][0][3]}}};
                        int16_t l_724 = 0L;
                        uint16_t *l_725 = &l_336;
                        int i, j, k;
                        l_693 = (((*g_566) && ((safe_sub_func_uint32_t_u_u(((void*)0 != l_690), ((g_30[l_620][(l_620 + 1)] >= (safe_mul_func_int16_t_s_s((p_59 < 3L), (*g_578)))) == p_59))) , ((**p_58) , (*g_84)))) , (void*)0);
                        (*p_58) = g_694;
                        (*p_57) |= ((safe_div_func_uint16_t_u_u((((p_59 , ((safe_mul_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(((*l_725) ^= (p_59 , (safe_sub_func_uint8_t_u_u((safe_add_func_int64_t_s_s(((safe_div_func_int32_t_s_s((safe_lshift_func_int16_t_s_u((safe_add_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((((((l_686[5][1] && (((*g_578) = (((((safe_add_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_u((((((*l_717) = &g_106[2][6][4]) != &g_192) < ((*g_303) | (safe_rshift_func_uint16_t_u_s((((((*l_721) = (1L ^ g_106[2][2][6])) == (l_723 = (g_192 ^= p_59))) , 1UL) || (*p_60)), l_724)))) >= (*g_303)), 0)) < p_59), p_59)) < l_686[4][3]) , 0x60318504DFEC6DD1LL) , (**l_129)) != (void*)0)) <= 6UL)) , 0x3BL) == g_30[l_620][(l_620 + 1)]) ^ l_665) ^ 5L), l_507[2][1][5])), g_9)), 9)), (-1L))) & g_30[l_620][(l_620 + 1)]), (*g_303))), p_59)))), p_59)), 7UL)) == 0x51L)) & 5L) >= p_59), p_59)) , (*g_84));
                    }
                    if ((((*l_728) = ((void*)0 == l_726)) == ((((*l_743) = ((((safe_div_func_int8_t_s_s((-1L), 0xA8L)) > (safe_add_func_int16_t_s_s((p_59 || (p_59 < ((safe_add_func_uint8_t_u_u(p_59, l_666[3][8])) < (safe_lshift_func_uint16_t_u_u((++(*g_578)), (safe_mul_func_int16_t_s_s(p_59, 0x5B3AL))))))), p_59))) < (*g_694)) , 1UL)) >= l_723) != 4294967294UL)))
                    { /* block id: 313 */
                        (*p_57) &= (-9L);
                        (*p_58) = (*p_58);
                    }
                    else
                    { /* block id: 316 */
                        return (*g_303);
                    }
                    (*p_58) = (*p_58);
                    for (l_383 = 2; (l_383 >= 0); l_383 -= 1)
                    { /* block id: 322 */
                        int64_t **l_750 = &g_303;
                        int i, j;
                        g_751 = (safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u((((!((g_304[l_383][l_383] && (p_59 , (l_749 != &g_427))) >= p_59)) , &l_620) != ((*l_750) = &g_304[2][4])), 9)), p_59));
                        return p_59;
                    }
                }
            }
            (*p_58) = (void*)0;
            (*p_58) = (*p_58);
        }
        else
        { /* block id: 331 */
            uint8_t l_762[9][10] = {{253UL,0UL,0xBCL,0x7DL,9UL,0UL,0xE7L,249UL,0x37L,0x19L},{0x89L,0xF4L,0xBCL,9UL,0x52L,249UL,1UL,0xCBL,0xD8L,0xE7L},{0xDAL,255UL,0x52L,0x46L,255UL,0x46L,0x52L,255UL,0xDAL,0xD2L},{0xE7L,0x37L,253UL,0xD2L,0xA0L,0xCBL,0UL,0xD8L,0xBCL,0xDAL},{0xD8L,0x7BL,2UL,0xD2L,0x37L,0xA0L,249UL,0x4BL,0xDAL,0xCBL},{1UL,0xD8L,0x7DL,0x46L,1UL,1UL,0x46L,0x7DL,0xD8L,1UL},{0xA0L,0x7DL,255UL,9UL,0x90L,0x19L,0xCBL,0xBCL,0x37L,255UL},{0xD2L,0UL,0x4BL,0x7DL,0x90L,0xF4L,0xA0L,0UL,0UL,1UL},{0x90L,0xCBL,0xC6L,0x19L,1UL,0x89L,1UL,0x19L,0xC6L,0xCBL}};
            int i, j;
            if ((+(~(safe_add_func_int64_t_s_s(((((void*)0 == &l_620) <= (*g_566)) <= ((((0x57L | (g_106[3][0][3] == (safe_sub_func_int32_t_s_s(0xB59BA8F7L, (safe_lshift_func_int16_t_s_u((safe_sub_func_int16_t_s_s((-3L), p_59)), (*g_578))))))) < p_59) , g_46) < l_762[5][9])), g_104)))))
            { /* block id: 332 */
                return (*g_303);
            }
            else
            { /* block id: 334 */
                return p_59;
            }
        }
        for (l_409 = 0; (l_409 == 6); l_409 = safe_add_func_uint8_t_u_u(l_409, 5))
        { /* block id: 340 */
            uint32_t l_767 = 0xED1330C1L;
            int32_t *l_770 = &l_651;
            int32_t l_789 = 0x6B6A9A9BL;
            int32_t l_790 = 0x9F03A6C4L;
            int32_t l_791[4][4][7] = {{{0x18258214L,1L,(-1L),5L,1L,0x3212D41BL,1L},{8L,(-1L),0x916D6466L,(-5L),(-3L),0xDB66FB81L,8L},{0L,1L,(-4L),0L,1L,0L,(-4L)},{(-5L),(-5L),0L,2L,0x568E02A6L,0x349A7D28L,(-4L)}},{{1L,(-4L),(-8L),1L,1L,0xE39D1F32L,0x18258214L},{0x3151B55AL,0x568E02A6L,0x670C2601L,(-3L),0x568E02A6L,1L,0xB1C9018AL},{5L,0L,(-1L),1L,1L,(-1L),0L},{(-4L),0xB1C9018AL,1L,0x568E02A6L,(-3L),0x670C2601L,0x568E02A6L}},{{0x0472B457L,0x18258214L,0xE39D1F32L,1L,1L,(-8L),(-4L)},{0x54DDEC11L,(-4L),0x349A7D28L,0x568E02A6L,2L,0x053C78B4L,0xF90F315EL},{0xE39D1F32L,0x4F9BA0D4L,(-2L),0L,(-2L),0x4F9BA0D4L,0xE39D1F32L},{1L,0x3151B55AL,0L,0x96D64A07L,0xF90F315EL,0x21D377D8L,(-6L)}},{{0x0472B457L,0L,1L,(-8L),0L,0x97C2C999L,1L},{(-1L),(-6L),0L,(-1L),0x4249845CL,0x4B916792L,0xDB66FB81L},{(-3L),8L,(-2L),(-2L),8L,(-3L),(-1L)},{0x670C2601L,0xB1C9018AL,8L,0xF90F315EL,0x3151B55AL,8L,(-1L)}}};
            int i, j, k;
            for (l_336 = 0; (l_336 <= 6); l_336 += 1)
            { /* block id: 343 */
                float l_765 = 0x1.8p-1;
                int32_t l_766[8] = {0xB76037D7L,0x8272BFCCL,0xB76037D7L,0x8272BFCCL,0xB76037D7L,0x8272BFCCL,0xB76037D7L,0x8272BFCCL};
                int i;
                --l_767;
                return l_767;
            }
            l_641 = (l_767 == (*g_303));
            (*p_58) = (void*)0;
            for (g_142 = 1; (g_142 >= 0); g_142 -= 1)
            { /* block id: 351 */
                float l_792 = 0x6.Dp-1;
                int32_t l_793[10] = {0x9D77DF64L,0x9D77DF64L,0x9D77DF64L,0x9D77DF64L,0x9D77DF64L,0x9D77DF64L,0x9D77DF64L,0x9D77DF64L,0x9D77DF64L,0x9D77DF64L};
                int i;
                for (g_105 = 0; (g_105 <= 1); g_105 += 1)
                { /* block id: 354 */
                    int i;
                    g_771 = l_770;
                    (*l_770) = ((*p_57) &= (((safe_sub_func_uint16_t_u_u(l_281[g_142], ((0xF45A349DCE7C372BLL & (&g_111[2] != &p_56)) != (safe_mul_func_uint16_t_u_u((((safe_div_func_uint8_t_u_u(((+((safe_mul_func_int16_t_s_s(((safe_mul_func_int8_t_s_s(p_59, (((p_59 <= ((safe_lshift_func_uint8_t_u_u((0x76L < (+0xE3372929L)), (&g_105 == &l_458))) ^ g_105)) || 3UL) , 8L))) ^ 0UL), p_59)) != p_59)) ^ 1L), 1L)) >= l_281[g_142]) && 0xF307L), l_788))))) , &g_105) != (void*)0));
                }
                l_794++;
            }
        }
    }
    return (*g_303);
}


/* ------------------------------------------ */
/* 
 * reads : g_44 g_87 g_84 g_9
 * writes: g_84 g_87
 */
static int32_t  func_76(int32_t ** p_77, int8_t * p_78)
{ /* block id: 19 */
    int32_t l_81 = 9L;
    int32_t *l_82[1];
    int32_t **l_83[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint8_t l_85 = 0UL;
    int32_t *l_86 = &g_44;
    int i;
    for (i = 0; i < 1; i++)
        l_82[i] = &g_9;
    g_84 = ((safe_unary_minus_func_int16_t_s(l_81)) , l_82[0]);
    l_86 = ((((**p_77) == 4294967295UL) && (l_85 & 1L)) , (void*)0);
    ++g_87;
    return (*g_84);
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_9, "g_9", print_hash_value);
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_30[i][j], "g_30[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_44, "g_44", print_hash_value);
    transparent_crc(g_46, "g_46", print_hash_value);
    transparent_crc(g_87, "g_87", print_hash_value);
    transparent_crc(g_92, "g_92", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_106[i][j][k], "g_106[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_142, "g_142", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_160[i], "g_160[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_162, "g_162", print_hash_value);
    transparent_crc(g_190, "g_190", print_hash_value);
    transparent_crc(g_192, "g_192", print_hash_value);
    transparent_crc_bytes (&g_203, sizeof(g_203), "g_203", print_hash_value);
    transparent_crc_bytes (&g_213, sizeof(g_213), "g_213", print_hash_value);
    transparent_crc(g_266, "g_266", print_hash_value);
    transparent_crc_bytes (&g_269, sizeof(g_269), "g_269", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_304[i][j], "g_304[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_459, "g_459", print_hash_value);
    transparent_crc(g_467, "g_467", print_hash_value);
    transparent_crc(g_468, "g_468", print_hash_value);
    transparent_crc(g_617, "g_617", print_hash_value);
    transparent_crc(g_751, "g_751", print_hash_value);
    transparent_crc(g_855, "g_855", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_898[i], "g_898[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1109, "g_1109", print_hash_value);
    transparent_crc(g_1120, "g_1120", print_hash_value);
    transparent_crc_bytes (&g_1185, sizeof(g_1185), "g_1185", print_hash_value);
    transparent_crc(g_1365, "g_1365", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_1396[i][j][k], "g_1396[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1499, "g_1499", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc_bytes(&g_1704[i][j][k], sizeof(g_1704[i][j][k]), "g_1704[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1832[i], "g_1832[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1896, "g_1896", print_hash_value);
    transparent_crc(g_1968, "g_1968", print_hash_value);
    transparent_crc(g_2144, "g_2144", print_hash_value);
    transparent_crc(g_2290, "g_2290", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_2380[i][j][k], "g_2380[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_2696[i], "g_2696[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2705, "g_2705", print_hash_value);
    transparent_crc(g_2711, "g_2711", print_hash_value);
    transparent_crc(g_2791, "g_2791", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_2817[i][j], "g_2817[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2848[i][j], "g_2848[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2849, "g_2849", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2927[i], "g_2927[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3168, "g_3168", print_hash_value);
    transparent_crc(g_3199, "g_3199", print_hash_value);
    transparent_crc(g_3252, "g_3252", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 886
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 316
   depth: 2, occurrence: 94
   depth: 3, occurrence: 4
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
   depth: 6, occurrence: 3
   depth: 7, occurrence: 1
   depth: 9, occurrence: 2
   depth: 10, occurrence: 3
   depth: 11, occurrence: 1
   depth: 13, occurrence: 4
   depth: 14, occurrence: 3
   depth: 15, occurrence: 5
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 8
   depth: 19, occurrence: 6
   depth: 20, occurrence: 4
   depth: 21, occurrence: 5
   depth: 22, occurrence: 3
   depth: 23, occurrence: 2
   depth: 24, occurrence: 6
   depth: 25, occurrence: 3
   depth: 26, occurrence: 3
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 3
   depth: 32, occurrence: 1
   depth: 34, occurrence: 2
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 3
   depth: 38, occurrence: 1
   depth: 42, occurrence: 1
   depth: 44, occurrence: 2
   depth: 45, occurrence: 1
   depth: 47, occurrence: 1
   depth: 48, occurrence: 1
   depth: 52, occurrence: 1

XXX total number of pointers: 678

XXX times a variable address is taken: 1516
XXX times a pointer is dereferenced on RHS: 555
breakdown:
   depth: 1, occurrence: 456
   depth: 2, occurrence: 82
   depth: 3, occurrence: 17
XXX times a pointer is dereferenced on LHS: 510
breakdown:
   depth: 1, occurrence: 460
   depth: 2, occurrence: 29
   depth: 3, occurrence: 15
   depth: 4, occurrence: 6
XXX times a pointer is compared with null: 64
XXX times a pointer is compared with address of another variable: 21
XXX times a pointer is compared with another pointer: 23
XXX times a pointer is qualified to be dereferenced: 11146

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2036
   level: 2, occurrence: 473
   level: 3, occurrence: 331
   level: 4, occurrence: 50
   level: 5, occurrence: 3
XXX number of pointers point to pointers: 323
XXX number of pointers point to scalars: 355
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 33.2
XXX average alias set size: 1.51

XXX times a non-volatile is read: 3101
XXX times a non-volatile is write: 1498
XXX times a volatile is read: 51
XXX    times read thru a pointer: 31
XXX times a volatile is write: 23
XXX    times written thru a pointer: 14
XXX times a volatile is available for access: 364
XXX percentage of non-volatile access: 98.4

XXX forward jumps: 4
XXX backward jumps: 10

XXX stmts: 335
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 38
   depth: 1, occurrence: 50
   depth: 2, occurrence: 62
   depth: 3, occurrence: 47
   depth: 4, occurrence: 55
   depth: 5, occurrence: 83

XXX percentage a fresh-made variable is used: 16.5
XXX percentage an existing variable is used: 83.5
********************* end of statistics **********************/


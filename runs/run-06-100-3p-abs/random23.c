/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      1034905638
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   const int32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 0xAC789480L;
static union U0 g_17 = {0xB8AE505BL};
static uint64_t g_19 = 18446744073709551615UL;
static volatile int32_t g_33 = 1L;/* VOLATILE GLOBAL g_33 */
static int16_t g_34 = 1L;
static int16_t g_35[2][1][4] = {{{0x0F4FL,0x0F4FL,0x0F4FL,0x0F4FL}},{{0x0F4FL,0x0F4FL,0x0F4FL,0x0F4FL}}};
static int32_t g_43 = 1L;
static volatile int32_t g_44 = 0x6CE7EF2AL;/* VOLATILE GLOBAL g_44 */
static uint8_t g_47 = 0x92L;
static int32_t *g_79 = &g_2;
static int32_t **g_78[9] = {&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79};
static int8_t g_85 = 5L;
static uint16_t g_110 = 0x721EL;
static uint32_t g_113 = 4294967290UL;
static uint32_t g_124 = 0x19CD64B8L;
static uint32_t g_125 = 0x22312402L;
static uint32_t g_128 = 1UL;
static const int64_t g_192[4][6][10] = {{{0xFD849581409FF84DLL,9L,5L,9L,0xFD849581409FF84DLL,(-1L),7L,0xB5AA6BA308FB7BDALL,0xA332A19252623D08LL,0x32A91C8B06D998ABLL},{7L,0x01185A11B485FB8FLL,0x32A91C8B06D998ABLL,(-1L),(-3L),(-1L),(-1L),(-3L),(-1L),0x32A91C8B06D998ABLL},{(-1L),(-1L),2L,0x32A91C8B06D998ABLL,0xFD849581409FF84DLL,0xB28C7E9381029D39LL,(-3L),5L,0x0FDE11B9BB303BE4LL,0xFD849581409FF84DLL},{0xB5AA6BA308FB7BDALL,9L,0xA9CDB1E1FFC80131LL,0xCC088A21F489C560LL,(-1L),0xCC088A21F489C560LL,0xA9CDB1E1FFC80131LL,9L,0xB5AA6BA308FB7BDALL,5L},{0x0FDE11B9BB303BE4LL,5L,(-3L),0xB28C7E9381029D39LL,0xFD849581409FF84DLL,0x32A91C8B06D998ABLL,2L,(-1L),(-1L),2L},{9L,0x0FDE11B9BB303BE4LL,0xB28C7E9381029D39LL,0xB28C7E9381029D39LL,0x0FDE11B9BB303BE4LL,9L,0x287281CC889B63E0LL,0x32A91C8B06D998ABLL,0xB5AA6BA308FB7BDALL,(-1L)}},{{(-1L),(-1L),5L,0xCC088A21F489C560LL,(-3L),0xA332A19252623D08LL,9L,0xA332A19252623D08LL,(-3L),0xCC088A21F489C560LL},{(-1L),0xA332A19252623D08LL,(-1L),0x01185A11B485FB8FLL,2L,9L,0xCC088A21F489C560LL,0xFD849581409FF84DLL,5L,0x32A91C8B06D998ABLL},{9L,0xCC088A21F489C560LL,0xFD849581409FF84DLL,5L,0x32A91C8B06D998ABLL,0x32A91C8B06D998ABLL,5L,0xFD849581409FF84DLL,0xCC088A21F489C560LL,9L},{0x0FDE11B9BB303BE4LL,7L,(-1L),(-1L),5L,0xCC088A21F489C560LL,(-3L),0xA332A19252623D08LL,9L,0xA332A19252623D08LL},{0xB5AA6BA308FB7BDALL,(-1L),5L,7L,5L,(-1L),0xB5AA6BA308FB7BDALL,0x32A91C8B06D998ABLL,0x287281CC889B63E0LL,9L},{5L,(-3L),0xB28C7E9381029D39LL,0xFD849581409FF84DLL,0x32A91C8B06D998ABLL,2L,(-1L),(-1L),2L,0x32A91C8B06D998ABLL}},{{0x287281CC889B63E0LL,(-3L),(-3L),0x287281CC889B63E0LL,2L,5L,0xB5AA6BA308FB7BDALL,9L,0xA9CDB1E1FFC80131LL,0xCC088A21F489C560LL},{0xB28C7E9381029D39LL,(-1L),0xA9CDB1E1FFC80131LL,0xB5AA6BA308FB7BDALL,(-3L),0xFD849581409FF84DLL,(-3L),0xB5AA6BA308FB7BDALL,0xA9CDB1E1FFC80131LL,(-1L)},{0x32A91C8B06D998ABLL,7L,0xA332A19252623D08LL,0x287281CC889B63E0LL,0x0FDE11B9BB303BE4LL,0x01185A11B485FB8FLL,5L,0xA9CDB1E1FFC80131LL,2L,2L},{7L,0xCC088A21F489C560LL,0x01185A11B485FB8FLL,0xFD849581409FF84DLL,0xFD849581409FF84DLL,0x01185A11B485FB8FLL,0xCC088A21F489C560LL,7L,0x287281CC889B63E0LL,5L},{0x32A91C8B06D998ABLL,0xA332A19252623D08LL,0xB5AA6BA308FB7BDALL,7L,(-1L),0xFD849581409FF84DLL,9L,5L,9L,0xFD849581409FF84DLL},{0xB28C7E9381029D39LL,(-1L),0xB5AA6BA308FB7BDALL,(-1L),0xB28C7E9381029D39LL,5L,0x287281CC889B63E0LL,7L,0xCC088A21F489C560LL,0x01185A11B485FB8FLL}},{{0x287281CC889B63E0LL,0x0FDE11B9BB303BE4LL,0x01185A11B485FB8FLL,5L,0xA9CDB1E1FFC80131LL,2L,2L,0xA9CDB1E1FFC80131LL,5L,0x01185A11B485FB8FLL},{5L,5L,0xA332A19252623D08LL,0x01185A11B485FB8FLL,0xB28C7E9381029D39LL,(-1L),0xA9CDB1E1FFC80131LL,0xB5AA6BA308FB7BDALL,(-3L),0xFD849581409FF84DLL},{0xB5AA6BA308FB7BDALL,9L,0xA9CDB1E1FFC80131LL,0xCC088A21F489C560LL,(-1L),0xCC088A21F489C560LL,0xA9CDB1E1FFC80131LL,9L,0xB5AA6BA308FB7BDALL,5L},{0x0FDE11B9BB303BE4LL,5L,(-3L),0xB28C7E9381029D39LL,0xFD849581409FF84DLL,0x32A91C8B06D998ABLL,2L,(-1L),(-1L),2L},{(-1L),(-3L),(-1L),(-1L),(-3L),(-1L),0x32A91C8B06D998ABLL,0x01185A11B485FB8FLL,7L,2L},{2L,5L,0xB5AA6BA308FB7BDALL,9L,0xA9CDB1E1FFC80131LL,0xCC088A21F489C560LL,(-1L),0xCC088A21F489C560LL,0xA9CDB1E1FFC80131LL,9L}}};
static const int64_t *g_191 = &g_192[3][4][2];
static int32_t ***g_255[3][8] = {{&g_78[2],&g_78[2],&g_78[2],&g_78[8],&g_78[2],&g_78[2],&g_78[2],&g_78[2]},{&g_78[2],&g_78[8],(void*)0,&g_78[8],&g_78[2],&g_78[2],&g_78[2],&g_78[8]},{&g_78[2],&g_78[8],&g_78[2],&g_78[2],&g_78[2],&g_78[2],&g_78[2],&g_78[8]}};
static int32_t *** const *g_254 = &g_255[2][3];
static uint8_t g_271[4][4] = {{5UL,5UL,5UL,5UL},{5UL,5UL,5UL,5UL},{5UL,5UL,5UL,5UL},{5UL,5UL,5UL,5UL}};
static uint64_t g_315[10] = {0UL,0UL,0UL,0UL,6UL,0UL,0UL,0UL,0UL,6UL};
static union U0 *g_334 = &g_17;
static union U0 **g_333 = &g_334;
static int16_t g_364 = (-1L);
static int64_t g_386 = 0x7BCA6CF48EFCF066LL;
static volatile uint32_t *g_527 = (void*)0;
static volatile uint32_t **g_526 = &g_527;
static const int32_t ***g_528[2] = {(void*)0,(void*)0};
static const int32_t *g_533 = &g_17.f0;
static const int32_t **g_532 = &g_533;
static const int32_t ***g_531 = &g_532;
static int32_t g_599 = 3L;
static uint16_t g_601 = 0x240FL;
static union U0 **g_856 = (void*)0;
static int8_t g_892[10] = {0L,0x17L,0L,0x17L,0L,0x17L,0L,0x17L,0L,0x17L};
static int16_t g_920[5][1][5] = {{{0xB26EL,0xB26EL,0xB26EL,0xB26EL,0xB26EL}},{{4L,4L,4L,4L,4L}},{{0xB26EL,0xB26EL,0xB26EL,0xB26EL,0xB26EL}},{{4L,4L,4L,4L,4L}},{{0xB26EL,0xB26EL,0xB26EL,0xB26EL,0xB26EL}}};
static uint16_t g_927 = 0x7FE9L;
static uint64_t g_931 = 0xAC581B730EDA8783LL;
static int32_t g_1008[5] = {0x68825ACFL,0x68825ACFL,0x68825ACFL,0x68825ACFL,0x68825ACFL};
static union U0 ****g_1016 = (void*)0;
static union U0 *****g_1015 = &g_1016;
static union U0 g_1043 = {0xB9363ACDL};
static int32_t g_1108 = 0L;
static int32_t g_1283 = 0L;
static int32_t * volatile g_1292 = &g_1108;/* VOLATILE GLOBAL g_1292 */
static uint16_t *g_1300 = (void*)0;
static uint16_t ** volatile g_1299 = &g_1300;/* VOLATILE GLOBAL g_1299 */
static uint64_t *g_1326 = &g_315[6];
static uint64_t **g_1325 = &g_1326;
static uint64_t *** volatile g_1324 = &g_1325;/* VOLATILE GLOBAL g_1324 */
static uint32_t *g_1332 = &g_125;
static uint32_t **g_1331[9] = {&g_1332,(void*)0,&g_1332,(void*)0,&g_1332,(void*)0,&g_1332,(void*)0,&g_1332};
static int32_t **** volatile g_1462 = &g_255[2][3];/* VOLATILE GLOBAL g_1462 */
static volatile uint32_t g_1577 = 7UL;/* VOLATILE GLOBAL g_1577 */
static const uint64_t *g_1583 = &g_931;
static const uint64_t **g_1582 = &g_1583;
static int16_t g_1609 = 0L;
static volatile uint32_t g_1615[5] = {0x13EE3F3BL,0x13EE3F3BL,0x13EE3F3BL,0x13EE3F3BL,0x13EE3F3BL};
static const uint64_t g_1633[9] = {0x56C0050EACF76614LL,0x56C0050EACF76614LL,0x56C0050EACF76614LL,0x56C0050EACF76614LL,0x56C0050EACF76614LL,0x56C0050EACF76614LL,0x56C0050EACF76614LL,0x56C0050EACF76614LL,0x56C0050EACF76614LL};
static const uint64_t *g_1634 = (void*)0;
static int32_t g_1647 = 0x76D05AFDL;
static union U0 * const *g_1696 = &g_334;
static union U0 * const **g_1695 = &g_1696;
static union U0 * const ** volatile * volatile g_1694 = &g_1695;/* VOLATILE GLOBAL g_1694 */
static union U0 * const ** volatile * volatile *g_1693 = &g_1694;
static union U0 g_1714[7] = {{0L},{0L},{0L},{0L},{0L},{0L},{0L}};
static union U0 ***g_1790 = &g_333;
static union U0 **** const g_1789 = &g_1790;
static union U0 **** const *g_1788 = &g_1789;
static uint8_t *g_1834 = &g_47;
static uint8_t * volatile * volatile g_1833 = &g_1834;/* VOLATILE GLOBAL g_1833 */
static uint8_t g_1863 = 0x94L;
static union U0 g_1881 = {0xDBFB0F65L};
static uint32_t ***g_1976[3][8] = {{&g_1331[7],&g_1331[7],&g_1331[8],(void*)0,&g_1331[4],&g_1331[8],&g_1331[4],(void*)0},{&g_1331[6],(void*)0,&g_1331[6],(void*)0,(void*)0,&g_1331[0],&g_1331[0],(void*)0},{(void*)0,&g_1331[0],&g_1331[0],(void*)0,(void*)0,&g_1331[6],(void*)0,&g_1331[6]}};
static volatile uint16_t g_2108 = 0UL;/* VOLATILE GLOBAL g_2108 */
static volatile int8_t g_2138 = (-1L);/* VOLATILE GLOBAL g_2138 */
static int32_t * const ***g_2165 = (void*)0;
static int32_t * const ****g_2164 = &g_2165;
static int8_t *g_2228 = (void*)0;
static int8_t **g_2227 = &g_2228;
static int8_t **g_2229 = &g_2228;
static uint8_t **g_2293 = &g_1834;
static uint8_t ***g_2292 = &g_2293;
static volatile int32_t g_2303[8][3] = {{3L,0xAD8E04ECL,3L},{0L,0xC3951CD9L,0x8B7EA2CEL},{0L,0L,0xC3951CD9L},{3L,0xC3951CD9L,0xC3951CD9L},{0xC3951CD9L,0xAD8E04ECL,0x8B7EA2CEL},{3L,0xAD8E04ECL,3L},{0L,0xC3951CD9L,0x8B7EA2CEL},{0L,0L,0xC3951CD9L}};
static uint64_t * const *g_2321 = (void*)0;
static uint64_t * const **g_2320 = &g_2321;
static int8_t g_2456 = 0x7FL;
static int32_t g_2593[4][5] = {{0x4A705B59L,0x4A705B59L,0xCAF54CDDL,0x4A705B59L,0x4A705B59L},{(-1L),0x4A705B59L,(-1L),(-1L),0x4A705B59L},{0x4A705B59L,(-1L),(-1L),0x4A705B59L,(-1L)},{0x4A705B59L,0x4A705B59L,0xCAF54CDDL,0x4A705B59L,0x4A705B59L}};


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static union U0  func_11(int16_t  p_12, union U0  p_13, uint32_t  p_14, uint16_t  p_15, uint8_t  p_16);
static int64_t  func_24(union U0  p_25);
static int16_t  func_40(uint32_t  p_41);
static int32_t ** func_50(int32_t * p_51, const int32_t * p_52, int32_t * p_53, int32_t ** p_54);
static union U0  func_56(int8_t  p_57, const union U0  p_58, int16_t  p_59);
static union U0  func_60(union U0  p_61, uint32_t  p_62, uint8_t  p_63, int64_t  p_64, int8_t  p_65);
static union U0  func_66(int16_t  p_67, int32_t * p_68, int32_t  p_69);
static int32_t * func_70(int32_t ** p_71);
static int32_t ** func_72(int32_t * p_73, int32_t ** p_74, const int32_t ** p_75);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_17 g_17.f0 g_19 g_35 g_47 g_532 g_43 g_1332 g_125 g_1834 g_892 g_191 g_192 g_1108 g_1833 g_1788 g_1789 g_1283 g_1324 g_1325 g_1326 g_315 g_1583 g_931 g_2292 g_2293 g_34 g_2227 g_1292 g_79 g_110 g_271 g_124 g_531 g_1790 g_533 g_1633 g_85 g_1582 g_1015 g_1016 g_2108 g_333 g_334 g_2593 g_1647 g_1577
 * writes: g_2 g_19 g_47 g_110 g_533 g_892 g_2227 g_2229 g_35 g_43 g_1283 g_364 g_2320 g_1834 g_315 g_271 g_34 g_124 g_1108 g_920 g_601 g_125 g_1331 g_386 g_2164 g_1790 g_2593
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int16_t l_5 = 0x5B7CL;
    int32_t l_2663 = 0xE1779A18L;
    int8_t **l_2672[1];
    uint16_t *l_2675 = (void*)0;
    uint16_t *l_2676 = (void*)0;
    uint16_t *l_2677[9] = {&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110};
    int32_t l_2678[3];
    int32_t **l_2679 = &g_79;
    int i;
    for (i = 0; i < 1; i++)
        l_2672[i] = (void*)0;
    for (i = 0; i < 3; i++)
        l_2678[i] = 0xDBE1B50AL;
    for (g_2 = 0; (g_2 != 26); g_2++)
    { /* block id: 3 */
        uint16_t l_6 = 65532UL;
        int32_t *l_2662 = &g_2593[0][0];
        (*l_2662) &= (l_5 <= (l_6 , ((safe_lshift_func_int8_t_s_u(((l_5 ^ (safe_mod_func_uint16_t_u_u((func_11(g_2, g_17, l_6, g_17.f0, g_17.f0) , 0xDC65L), 1L))) && 1UL), l_6)) < l_6)));
        l_2663 ^= (*g_533);
        return (*l_2662);
    }
    (*g_79) ^= (safe_unary_minus_func_uint8_t_u((4294967288UL > (((safe_div_func_uint32_t_u_u(((safe_sub_func_uint16_t_u_u(l_2663, 0xE3FEL)) && g_110), (safe_sub_func_uint16_t_u_u((((!(l_2672[0] != &g_2228)) != (safe_lshift_func_uint32_t_u_u((&l_2672[0] != &l_2672[0]), 31))) == (l_2678[2] = l_2663)), 1L)))) == (*g_533)) , l_2678[2]))));
    (*g_532) = func_70(l_2679);
    return g_1577;
}


/* ------------------------------------------ */
/* 
 * reads : g_19 g_17 g_35 g_17.f0 g_47 g_532 g_43 g_1332 g_125 g_1834 g_892 g_191 g_192 g_1108 g_1833 g_1788 g_1789 g_1283 g_1324 g_1325 g_1326 g_315 g_1583 g_931 g_2292 g_2293 g_34 g_2227 g_1292 g_79 g_2 g_110 g_271 g_124 g_531 g_1790 g_533 g_1633 g_85 g_1582 g_1015 g_1016 g_2108 g_333 g_334
 * writes: g_19 g_47 g_110 g_533 g_892 g_2227 g_2229 g_35 g_43 g_1283 g_364 g_2320 g_1834 g_315 g_271 g_34 g_124 g_1108 g_920 g_601 g_125 g_1331 g_386 g_2164 g_1790
 */
static union U0  func_11(int16_t  p_12, union U0  p_13, uint32_t  p_14, uint16_t  p_15, uint8_t  p_16)
{ /* block id: 4 */
    uint32_t l_18[10][5] = {{0xE5AB06C0L,4294967295UL,0xE5AB06C0L,0x456574B6L,0xCA1A1F05L},{1UL,4294967295UL,0x0D3ECF04L,0x2632C5B6L,2UL},{1UL,0x2632C5B6L,1UL,1UL,4294967295UL},{0UL,4294967295UL,0x0D3ECF04L,2UL,8UL},{0x2632C5B6L,0xE5AB06C0L,0xE5AB06C0L,0x2632C5B6L,0UL},{0x2632C5B6L,1UL,1UL,0x064BAE0FL,1UL},{0UL,1UL,0x456574B6L,0x0D3ECF04L,0xCA1A1F05L},{1UL,0UL,0xE5AB06C0L,0x064BAE0FL,0x0D3ECF04L},{1UL,0x2632C5B6L,1UL,0x2632C5B6L,1UL},{0xE5AB06C0L,0x2632C5B6L,0UL,2UL,4294967295UL}};
    union U0 l_2658 = {0x89D24937L};
    int32_t *l_2661 = &g_1647;
    int i, j;
    g_19 &= l_18[8][0];
    for (p_14 = 0; (p_14 >= 26); p_14 = safe_add_func_uint64_t_u_u(p_14, 4))
    { /* block id: 8 */
        uint8_t l_39 = 1UL;
        int32_t l_2659 = (-1L);
        int32_t l_2660 = 0xD7DA9A32L;
        l_2659 = (((safe_lshift_func_int64_t_s_s(func_24(g_17), 16)) | (p_13.f0 <= 5UL)) && (((l_39 != (g_35[1][0][3] , func_24((func_40(g_17.f0) , l_2658)))) , l_2658.f0) && l_2658.f0));
        l_2660 ^= (*g_1292);
    }
    (*g_532) = l_2661;
    return (*****g_1788);
}


/* ------------------------------------------ */
/* 
 * reads : g_19
 * writes:
 */
static int64_t  func_24(union U0  p_25)
{ /* block id: 9 */
    int32_t *l_26 = (void*)0;
    int32_t l_27 = 0xD5EC7E01L;
    int32_t *l_28 = &l_27;
    int32_t *l_29 = &l_27;
    int32_t l_30 = 9L;
    int32_t *l_31 = &l_27;
    int32_t *l_32[3][8][9] = {{{&g_2,&l_30,&g_2,(void*)0,&l_30,&l_30,&l_30,&l_30,&l_30},{&l_30,&l_30,&l_30,&l_30,&l_30,&l_30,&g_2,&g_2,&g_2},{(void*)0,&g_2,&l_30,&g_2,&g_2,(void*)0,&l_30,(void*)0,&g_2},{(void*)0,&l_27,&g_2,&g_2,&l_30,&g_2,&g_2,&l_27,(void*)0},{&l_30,(void*)0,&g_2,&g_2,&l_30,(void*)0,&g_2,&l_27,&g_2},{&g_2,&l_27,(void*)0,&l_30,&l_30,&g_2,&l_30,&l_30,&g_2},{&l_30,&l_30,&g_2,&l_30,&l_30,&l_30,&g_2,&l_30,&g_2},{(void*)0,&l_27,&g_2,&l_30,&l_30,&g_2,&g_2,&l_30,&l_27}},{{(void*)0,&l_30,(void*)0,(void*)0,&l_30,&l_30,(void*)0,(void*)0,&l_30},{&l_30,&g_2,&l_30,&l_27,&g_2,&g_2,&l_27,&l_27,&g_2},{&g_2,&l_30,&l_30,&g_2,&g_2,(void*)0,(void*)0,&g_2,(void*)0},{&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&l_30},{&g_2,&l_30,(void*)0,&l_27,&l_30,&l_30,&l_30,&g_2,&l_27},{&l_30,(void*)0,&l_27,&g_2,&g_2,&l_30,&g_2,&g_2,&l_27},{&g_2,&g_2,&l_30,&l_30,&g_2,(void*)0,&l_30,&l_27,(void*)0},{(void*)0,&g_2,&l_30,&g_2,&g_2,&g_2,&l_27,&g_2,&g_2}},{{(void*)0,&g_2,&l_30,&l_30,&l_30,&g_2,(void*)0,&l_30,&l_30},{&l_27,&l_27,&l_27,&l_30,&g_2,(void*)0,&g_2,&l_30,&g_2},{(void*)0,&g_2,&l_30,&g_2,&l_30,&g_2,&g_2,&l_30,&g_2},{&g_2,(void*)0,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2},{(void*)0,&g_2,&g_2,(void*)0,(void*)0,&l_30,(void*)0,&l_27,&g_2},{&g_2,&g_2,&l_30,&g_2,&g_2,&l_30,&g_2,&g_2,&g_2},{&l_30,&l_30,&l_30,&g_2,&l_30,&g_2,&g_2,&g_2,&l_30},{&g_2,&g_2,&g_2,&l_30,&g_2,&l_27,&l_30,&l_27,&g_2}}};
    uint32_t l_36[10][8][3] = {{{4294967293UL,4294967295UL,0x2038BE40L},{0x257DE97EL,0x5AB2A44BL,0x816884ADL},{4294967293UL,1UL,5UL},{0x206C165AL,0x206C165AL,0x257DE97EL},{1UL,0x8E7029BBL,0xB742D63DL},{0UL,0x96340C09L,1UL},{4294967288UL,4294967295UL,1UL},{0xD1334807L,0UL,1UL}},{{0x8D11A6C9L,0xA5D0C9ADL,0xB742D63DL},{1UL,0xFCF42757L,0x257DE97EL},{0x2FAF7223L,0x70E9879AL,5UL},{0xC30F141BL,1UL,0x816884ADL},{0UL,1UL,0x2038BE40L},{1UL,0x85AA7F79L,0UL},{6UL,0UL,0xA5D0C9ADL},{1UL,4294967288UL,0UL}},{{0xA5D0C9ADL,0x8F643D9DL,0x8F643D9DL},{4UL,0x9F6C8506L,0UL},{4294967288UL,0x8D11A6C9L,9UL},{4294967295UL,0x33F116E8L,0UL},{0x8F643D9DL,0UL,4294967288UL},{4294967294UL,0x33F116E8L,0UL},{4294967290UL,0x8D11A6C9L,4294967295UL},{0x816884ADL,0x9F6C8506L,0x197A07E0L}},{{0xB742D63DL,0x8F643D9DL,4294967288UL},{0x45AB60A9L,4294967288UL,0x086CC6B0L},{4294967295UL,0UL,4294967293UL},{0x5AB2A44BL,0x85AA7F79L,0xC1552C25L},{0x40B387E8L,1UL,4294967295UL},{0x9B5F277FL,4294967295UL,1UL},{4294967290UL,1UL,4294967290UL},{0xFCF42757L,0UL,0xDA6603FAL}},{{0x2038BE40L,0x55CCBD95L,1UL},{0x086CC6B0L,0x37E8F884L,1UL},{4294967288UL,1UL,4294967295UL},{0x086CC6B0L,4294967288UL,0x45AB60A9L},{0x2038BE40L,6UL,4294967295UL},{0xFCF42757L,0x257DE97EL,0xC30F141BL},{4294967290UL,4294967295UL,0xB742D63DL},{0x9B5F277FL,0UL,4294967294UL}},{{0xA23986B2L,0UL,0UL},{0UL,0x35DF961DL,0xFCF42757L},{4294967289UL,0xFC7BC07EL,0UL},{0xC1552C25L,0x33F116E8L,0UL},{4294967295UL,4294967295UL,4294967289UL},{0x822C3E50L,1UL,0x257DE97EL},{0x8E7029BBL,0xA23986B2L,0x8D11A6C9L},{1UL,0UL,0UL}},{{0xA5D0C9ADL,0x8E7029BBL,0x8D11A6C9L},{0x9F6C8506L,4294967294UL,0x257DE97EL},{0x5BDF607DL,0UL,4294967289UL},{0xC82F3C07L,0x816884ADL,0UL},{0x55CCBD95L,0x8D11A6C9L,0UL},{0UL,0x822C3E50L,0xFCF42757L},{0x40B387E8L,8UL,0UL},{0x96340C09L,0x197A07E0L,4294967294UL}},{{8UL,4294967288UL,0xB742D63DL},{4294967294UL,0xC30F141BL,0xC30F141BL},{4294967295UL,5UL,4294967295UL},{1UL,0x9F6C8506L,0x45AB60A9L},{1UL,0x447FD326L,4294967295UL},{0xC30F141BL,0UL,1UL},{4294967288UL,0x447FD326L,1UL},{0x37E8F884L,0x9F6C8506L,0xDA6603FAL}},{{0UL,5UL,4294967290UL},{0x257DE97EL,0xC30F141BL,1UL},{4294967293UL,4294967288UL,0UL},{0x7692AFB5L,0x197A07E0L,4294967290UL},{0xFC7BC07EL,8UL,0x2038BE40L},{4294967288UL,0x822C3E50L,0x35DF961DL},{5UL,0x8D11A6C9L,0x5BDF607DL},{0UL,0x816884ADL,0UL}},{{1UL,0UL,8UL},{4294967290UL,4294967294UL,0x816884ADL},{4294967295UL,0x8E7029BBL,4294967288UL},{4294967294UL,0UL,0x85AA7F79L},{4294967295UL,0xA23986B2L,4294967293UL},{4294967290UL,1UL,0x33F116E8L},{1UL,4294967295UL,0xA5D0C9ADL},{0UL,0x33F116E8L,0x206C165AL}}};
    int i, j, k;
    l_36[3][5][1]++;
    return g_19;
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_110 g_532 g_43 g_1332 g_125 g_1834 g_892 g_35 g_191 g_192 g_1108 g_1833 g_1788 g_1789 g_1283 g_1324 g_1325 g_1326 g_315 g_1583 g_931 g_2292 g_2293 g_34 g_2227 g_1292 g_79 g_2 g_271 g_124 g_531 g_1790 g_533 g_1633 g_85 g_1582 g_1015 g_1016 g_2108
 * writes: g_47 g_110 g_533 g_892 g_2227 g_2229 g_35 g_43 g_1283 g_364 g_2320 g_1834 g_315 g_271 g_34 g_124 g_1108 g_920 g_601 g_125 g_1331 g_386 g_2164 g_1790
 */
static int16_t  func_40(uint32_t  p_41)
{ /* block id: 12 */
    int16_t l_45 = 0x2777L;
    int32_t l_46 = 0xF0A6C5FAL;
    int32_t *l_55[1][1];
    const int32_t *l_81[6] = {&l_46,(void*)0,(void*)0,&l_46,(void*)0,(void*)0};
    const int32_t **l_80 = &l_81[5];
    int32_t **l_1335[3];
    union U0 ** const *l_1396 = &g_856;
    union U0 ** const **l_1395 = &l_1396;
    union U0 ** const ***l_1394 = &l_1395;
    int32_t l_1444 = 0xB7A91462L;
    int32_t ***l_1461 = &l_1335[1];
    const uint64_t *l_1632 = &g_1633[3];
    uint8_t **l_1645 = (void*)0;
    uint64_t l_1646 = 18446744073709551611UL;
    int64_t l_1666[6][3] = {{0xC034DA69C384C3FALL,0xF316F18A0010FABFLL,(-9L)},{(-1L),0xF316F18A0010FABFLL,0xF316F18A0010FABFLL},{0xE68D0E8D6A57454ALL,0xF316F18A0010FABFLL,0xD3E574BCA24DDA14LL},{0xC034DA69C384C3FALL,0xF316F18A0010FABFLL,(-9L)},{(-1L),0xF316F18A0010FABFLL,0xF316F18A0010FABFLL},{0xE68D0E8D6A57454ALL,0xF316F18A0010FABFLL,0xD3E574BCA24DDA14LL}};
    uint16_t l_1673 = 0x4BF7L;
    const uint8_t *l_1700 = &g_271[3][3];
    uint16_t l_1810 = 3UL;
    const union U0 *l_1895 = &g_17;
    const union U0 **l_1894 = &l_1895;
    uint32_t ***l_1975 = &g_1331[4];
    int32_t l_1993 = 0x306520BDL;
    int8_t l_1996 = (-1L);
    int32_t l_1998 = (-4L);
    int16_t l_2053 = 0x3023L;
    uint16_t **l_2075 = &g_1300;
    int32_t *l_2121 = &g_43;
    int32_t * const *l_2163[2][8];
    int32_t * const **l_2162 = &l_2163[0][6];
    int32_t * const ***l_2161 = &l_2162;
    int32_t * const ****l_2160 = &l_2161;
    int32_t l_2203[2];
    int8_t *l_2221 = (void*)0;
    int8_t *l_2222 = &g_892[6];
    int8_t **l_2226[4][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_2222,&l_2222,(void*)0,&l_2222,&l_2222},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_2222,&l_2222,(void*)0,&l_2222,&l_2222,(void*)0,&l_2222,&l_2222,(void*)0}};
    int8_t ***l_2225[9][2][2] = {{{&l_2226[2][0],&l_2226[2][0]},{&l_2226[2][0],&l_2226[2][0]}},{{&l_2226[2][0],&l_2226[2][0]},{&l_2226[2][0],&l_2226[2][0]}},{{&l_2226[2][0],&l_2226[2][0]},{(void*)0,(void*)0}},{{&l_2226[2][0],(void*)0},{(void*)0,&l_2226[2][0]}},{{&l_2226[2][0],&l_2226[2][0]},{&l_2226[2][0],&l_2226[2][0]}},{{&l_2226[2][0],&l_2226[2][0]},{&l_2226[2][0],&l_2226[2][0]}},{{&l_2226[2][0],&l_2226[2][0]},{&l_2226[2][0],&l_2226[2][0]}},{{(void*)0,(void*)0},{&l_2226[2][0],(void*)0}},{{(void*)0,&l_2226[2][0]},{&l_2226[2][0],&l_2226[2][0]}}};
    int8_t l_2230 = 0x0EL;
    int64_t l_2231 = 0x2663ACA4A73BDA03LL;
    int8_t l_2232 = 0x28L;
    int64_t *l_2233[8][10][3] = {{{&l_1666[0][1],(void*)0,&g_386},{(void*)0,&l_1666[0][1],&l_2231},{&l_1666[5][0],&l_1666[0][2],&l_2231},{&l_1666[0][1],&l_2231,&l_2231},{&l_1666[5][1],(void*)0,&l_1666[0][1]},{(void*)0,(void*)0,&g_386},{(void*)0,&l_1666[0][1],&l_1666[2][1]},{&l_1666[0][1],&g_386,(void*)0},{&l_2231,&l_1666[0][1],&g_386},{&g_386,&l_1666[0][1],(void*)0}},{{&g_386,&l_2231,&l_1666[2][1]},{&l_2231,&l_2231,&g_386},{(void*)0,&l_2231,&l_1666[0][1]},{&l_2231,(void*)0,&l_2231},{&l_1666[0][1],&l_1666[0][1],&l_2231},{&g_386,&g_386,&l_2231},{&l_2231,(void*)0,&g_386},{&l_1666[2][1],(void*)0,&l_1666[0][1]},{&l_2231,&l_2231,&l_2231},{(void*)0,(void*)0,(void*)0}},{{&g_386,(void*)0,&l_1666[3][0]},{&l_1666[0][1],&l_2231,(void*)0},{(void*)0,&l_2231,(void*)0},{&l_1666[2][1],&l_2231,&l_1666[2][2]},{&g_386,&l_2231,&l_2231},{&l_2231,&l_2231,&l_2231},{&g_386,&l_1666[2][1],(void*)0},{&l_1666[0][1],&l_1666[2][1],&l_1666[5][1]},{&g_386,&l_2231,(void*)0},{&l_1666[0][1],&l_2231,(void*)0}},{{&l_1666[5][1],&l_2231,&l_2231},{&l_2231,&l_2231,&l_1666[0][1]},{&l_2231,&l_2231,&l_1666[2][1]},{&l_2231,(void*)0,&g_386},{(void*)0,(void*)0,&l_1666[1][0]},{&l_1666[1][0],&l_2231,&g_386},{&g_386,(void*)0,&l_1666[0][1]},{&l_1666[1][1],(void*)0,&l_1666[0][1]},{(void*)0,&g_386,&l_1666[0][0]},{&l_1666[4][0],&l_1666[0][1],&l_1666[0][1]}},{{&l_1666[0][1],(void*)0,&l_2231},{&l_1666[0][1],&l_2231,&l_1666[0][1]},{(void*)0,&l_2231,&l_1666[0][1]},{&l_1666[0][1],&l_1666[2][1],&g_386},{(void*)0,&l_2231,&l_1666[0][1]},{&g_386,&l_1666[0][1],&l_1666[0][1]},{(void*)0,&l_1666[0][1],&l_1666[0][1]},{&l_1666[0][1],&l_2231,&l_2231},{&l_1666[4][0],&l_2231,(void*)0},{&l_1666[0][1],&l_1666[4][0],&l_1666[0][1]}},{{&l_2231,(void*)0,&l_2231},{&l_1666[1][1],(void*)0,&g_386},{(void*)0,&l_1666[0][1],&g_386},{&g_386,&l_2231,(void*)0},{(void*)0,&g_386,(void*)0},{&g_386,&l_1666[0][1],(void*)0},{&l_2231,(void*)0,&l_2231},{&l_1666[0][1],&l_1666[0][1],&l_1666[2][1]},{&l_2231,&l_2231,&l_1666[0][1]},{&g_386,&l_2231,(void*)0}},{{&l_2231,(void*)0,&l_1666[2][2]},{&l_2231,&l_2231,&l_1666[0][1]},{&g_386,&g_386,(void*)0},{&l_1666[0][1],&l_1666[1][0],(void*)0},{&g_386,&l_2231,&l_1666[0][1]},{&g_386,&l_1666[0][1],&l_1666[2][2]},{&l_1666[0][1],&l_1666[0][1],(void*)0},{(void*)0,&g_386,&l_1666[0][1]},{(void*)0,&l_2231,&l_1666[2][1]},{&l_1666[1][0],(void*)0,&l_2231}},{{(void*)0,&g_386,(void*)0},{&l_2231,&l_1666[2][1],(void*)0},{&l_1666[0][2],&l_2231,(void*)0},{(void*)0,&l_1666[0][1],&g_386},{&l_1666[0][1],&l_2231,&g_386},{&l_1666[4][0],(void*)0,&l_2231},{&l_2231,&g_386,&l_1666[0][1]},{&g_386,(void*)0,(void*)0},{&l_1666[0][1],&l_1666[4][0],&l_2231},{&l_1666[2][1],&l_1666[0][1],&l_1666[0][1]}}};
    int32_t l_2275 = (-1L);
    const int32_t l_2294 = 0x08208A4DL;
    int32_t l_2297 = 0L;
    uint64_t *l_2436[7][6][6] = {{{&g_315[6],&g_315[6],&g_931,&g_315[6],&g_19,&g_315[6]},{&g_315[6],&g_19,&g_315[6],&g_931,&g_315[6],&g_315[6]},{&g_315[6],&g_315[6],&g_931,&g_931,&g_315[6],&g_315[6]},{&g_931,&g_19,&g_931,&g_931,&g_19,&g_931},{&g_315[6],&g_315[6],&g_931,&g_931,&g_315[6],&g_315[6]},{&g_315[6],&g_315[6],&g_931,&g_315[6],&g_19,&g_315[6]}},{{&g_315[6],&g_19,&g_315[6],&g_931,&g_315[6],&g_315[6]},{&g_315[6],&g_315[6],&g_931,&g_931,&g_315[6],&g_315[6]},{&g_931,&g_19,&g_931,&g_931,&g_19,&g_931},{&g_315[6],&g_315[6],&g_931,&g_931,&g_315[6],&g_315[6]},{&g_315[6],&g_315[6],&g_931,&g_315[6],&g_19,&g_315[6]},{&g_315[6],&g_19,&g_315[6],&g_931,&g_315[6],&g_315[6]}},{{&g_315[6],&g_315[6],&g_931,&g_931,&g_315[6],&g_315[6]},{&g_931,&g_315[6],(void*)0,(void*)0,&g_315[6],&g_315[5]},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_931,(void*)0},{(void*)0,&g_315[6],&g_315[5],(void*)0,&g_315[6],(void*)0},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_315[6],(void*)0},{(void*)0,&g_931,&g_315[5],(void*)0,&g_315[6],(void*)0}},{{&g_315[5],&g_315[6],(void*)0,(void*)0,&g_315[6],&g_315[5]},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_931,(void*)0},{(void*)0,&g_315[6],&g_315[5],(void*)0,&g_315[6],(void*)0},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_315[6],(void*)0},{(void*)0,&g_931,&g_315[5],(void*)0,&g_315[6],(void*)0},{&g_315[5],&g_315[6],(void*)0,(void*)0,&g_315[6],&g_315[5]}},{{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_931,(void*)0},{(void*)0,&g_315[6],&g_315[5],(void*)0,&g_315[6],(void*)0},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_315[6],(void*)0},{(void*)0,&g_931,&g_315[5],(void*)0,&g_315[6],(void*)0},{&g_315[5],&g_315[6],(void*)0,(void*)0,&g_315[6],&g_315[5]},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_931,(void*)0}},{{(void*)0,&g_315[6],&g_315[5],(void*)0,&g_315[6],(void*)0},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_315[6],(void*)0},{(void*)0,&g_931,&g_315[5],(void*)0,&g_315[6],(void*)0},{&g_315[5],&g_315[6],(void*)0,(void*)0,&g_315[6],&g_315[5]},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_931,(void*)0},{(void*)0,&g_315[6],&g_315[5],(void*)0,&g_315[6],(void*)0}},{{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_315[6],(void*)0},{(void*)0,&g_931,&g_315[5],(void*)0,&g_315[6],(void*)0},{&g_315[5],&g_315[6],(void*)0,(void*)0,&g_315[6],&g_315[5]},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_931,(void*)0},{(void*)0,&g_315[6],&g_315[5],(void*)0,&g_315[6],(void*)0},{(void*)0,&g_315[6],(void*)0,&g_315[5],&g_315[6],(void*)0}}};
    const int64_t l_2444[9][4][2] = {{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}},{{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL},{0xB8060E26A3C6BE61LL,0xB8060E26A3C6BE61LL}}};
    uint8_t l_2580 = 0xD8L;
    int16_t l_2592 = 0xC84EL;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
            l_55[i][j] = &g_43;
    }
    for (i = 0; i < 3; i++)
        l_1335[i] = &l_55[0][0];
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
            l_2163[i][j] = (void*)0;
    }
    for (i = 0; i < 2; i++)
        l_2203[i] = (-6L);
    for (p_41 = 0; (p_41 <= 0); p_41 += 1)
    { /* block id: 15 */
        int32_t *l_42[6] = {&g_2,&g_43,&g_2,&g_2,&g_43,&g_2};
        int32_t **l_77 = &l_55[0][0];
        int32_t ***l_76[1][7] = {{&l_77,&l_77,&l_77,&l_77,&l_77,&l_77,&l_77}};
        int32_t l_1288 = 6L;
        uint64_t l_1370 = 0UL;
        uint16_t *l_1415 = &g_927;
        int8_t *l_1451[2][7] = {{&g_85,&g_85,&g_892[3],&g_85,&g_85,&g_892[3],&g_85},{&g_85,&g_85,&g_85,&g_85,&g_85,&g_85,&g_85}};
        uint32_t **l_1460 = &g_1332;
        union U0 l_1542 = {-1L};
        uint64_t ***l_1558 = &g_1325;
        int32_t l_1580 = 1L;
        union U0 **l_1680 = &g_334;
        int i, j;
        ++g_47;
    }
    for (l_46 = 24; (l_46 > (-20)); l_46--)
    { /* block id: 800 */
        int32_t **l_1723 = &g_79;
        uint8_t l_1754 = 0xFFL;
        uint32_t **l_1761 = &g_1332;
        union U0 **** const *l_1787 = &g_1016;
        union U0 *l_1879[6] = {&g_1043,&g_1714[3],&g_1043,&g_1043,&g_1714[3],&g_1043};
        int32_t l_1886 = 0x6BDD295FL;
        int32_t **l_1925[1][4] = {{&l_55[0][0],&l_55[0][0],&l_55[0][0],&l_55[0][0]}};
        const uint64_t l_1927[8] = {0x5EF0D25AE584FFCELL,4UL,0x5EF0D25AE584FFCELL,4UL,0x5EF0D25AE584FFCELL,4UL,0x5EF0D25AE584FFCELL,4UL};
        uint32_t l_2027[4][1][1];
        uint64_t ***l_2029 = (void*)0;
        uint64_t **** const l_2028 = &l_2029;
        uint8_t l_2159 = 5UL;
        int32_t **l_2199 = &g_79;
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 1; k++)
                    l_2027[i][j][k] = 0xE1EFB40AL;
            }
        }
        for (g_110 = 0; (g_110 <= 54); g_110 = safe_add_func_uint32_t_u_u(g_110, 2))
        { /* block id: 803 */
            int32_t **l_1721 = &g_79;
            int32_t l_1751 = 0x1A68CEAFL;
            uint64_t ** const ** const l_1795 = (void*)0;
            uint32_t *l_1811 = &g_113;
            uint64_t l_1816 = 18446744073709551615UL;
            int64_t l_1835[3][10] = {{0x28A72687655ADAD0LL,1L,0x680014A8DAF94D3DLL,0x680014A8DAF94D3DLL,1L,0x28A72687655ADAD0LL,(-6L),0x28A72687655ADAD0LL,1L,0x680014A8DAF94D3DLL},{(-3L),0x186251303196D041LL,(-3L),0x680014A8DAF94D3DLL,(-6L),(-6L),0x680014A8DAF94D3DLL,(-3L),0x186251303196D041LL,(-3L)},{(-3L),0x28A72687655ADAD0LL,0x186251303196D041LL,1L,0x186251303196D041LL,0x28A72687655ADAD0LL,(-3L),(-3L),0x28A72687655ADAD0LL,0x186251303196D041LL}};
            union U0 l_1906 = {0xC4D27C33L};
            int32_t *l_1926 = (void*)0;
            int8_t *l_1928 = &g_85;
            uint16_t *l_1929[4][8] = {{&g_927,&l_1673,&l_1673,&g_927,&l_1810,&g_110,&l_1810,&g_927},{&l_1673,&l_1810,&l_1673,(void*)0,&l_1673,&l_1673,(void*)0,&l_1673},{&l_1810,&l_1810,&l_1673,&g_110,&l_1673,&g_110,&l_1673,&l_1810},{&l_1810,&l_1673,(void*)0,&l_1673,&l_1673,(void*)0,&l_1673,&l_1810}};
            int32_t l_1995 = 0L;
            int32_t l_2001 = 0x65DEA311L;
            int32_t l_2003 = 1L;
            int32_t l_2005 = 0xC1CC4929L;
            uint8_t l_2056 = 0xBEL;
            int i, j;
        }
    }
    (*g_532) = (**l_1461);
    if (((+((safe_rshift_func_uint8_t_u_u((((+(safe_sub_func_int64_t_s_s(((*l_2121) = ((((safe_sub_func_uint16_t_u_u(0x2C81L, g_43)) == (safe_sub_func_uint8_t_u_u((((*g_1332) == (safe_sub_func_int16_t_s_s((((((safe_div_func_uint64_t_u_u(((-1L) <= (((g_35[0][0][1] |= ((safe_sub_func_uint32_t_u_u((((*l_2222) = 7L) && ((*g_1834) = ((void*)0 == &l_45))), (safe_div_func_uint32_t_u_u(((((g_2227 = &l_2222) == (g_2229 = (void*)0)) & 0x81L) | l_2230), p_41)))) , g_892[3])) >= l_2231) >= l_2232)), (*g_191))) , p_41) , p_41) | 0xDDDD6309L) && p_41), 0xC007L))) | g_1108), p_41))) == p_41) == p_41)), (-1L)))) , p_41) , (**g_1833)), 6)) & p_41)) ^ 0x23E90BE3177D20EFLL))
    { /* block id: 1025 */
        int64_t l_2241[10] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
        int32_t *l_2248 = &g_1283;
        int32_t l_2274[2];
        int8_t l_2307 = (-3L);
        int16_t l_2308 = (-1L);
        uint64_t ***l_2316 = &g_1325;
        uint8_t *l_2330 = &g_271[2][0];
        union U0 *l_2359 = (void*)0;
        uint16_t l_2367 = 0x3980L;
        uint64_t l_2397 = 0x247B44BBBFFE47CCLL;
        int16_t l_2414[5][5] = {{0x886BL,(-1L),(-1L),0x886BL,(-1L)},{0x886BL,1L,(-3L),(-3L),1L},{(-1L),(-1L),(-3L),8L,8L},{(-1L),(-1L),(-1L),(-3L),(-1L)},{(-3L),(-1L),(-1L),(-1L),(-3L)}};
        uint8_t ** const *l_2422 = (void*)0;
        uint32_t l_2432 = 0xEDAF9AFDL;
        int32_t l_2445[6][4][8] = {{{0x4EC2916EL,0xB5EC303BL,1L,0L,1L,0x4C93270DL,0x50F08702L,(-8L)},{(-8L),0xB5EC303BL,0xE8DE898BL,0xB5B01A2EL,0L,0x4C93270DL,0xE019F589L,0xA275CB68L},{0xFDE03299L,0xB5EC303BL,(-1L),0L,0xE6D0F294L,0x4C93270DL,0L,(-8L)},{0xA275CB68L,0xB5EC303BL,0x4C93270DL,0xB5B01A2EL,0xB5B01A2EL,0x4C93270DL,0xB5EC303BL,0xA275CB68L}},{{0x4EC2916EL,0xB5EC303BL,1L,0L,1L,0x4C93270DL,0x50F08702L,(-8L)},{(-8L),0xB5EC303BL,0xE8DE898BL,0xB5B01A2EL,0L,0x4C93270DL,0xE019F589L,0xA275CB68L},{0xFDE03299L,0xB5EC303BL,(-1L),0L,0xE6D0F294L,0x4C93270DL,0L,(-8L)},{0xA275CB68L,0xB5EC303BL,0x4C93270DL,0xB5B01A2EL,0xB5B01A2EL,0x4C93270DL,0xB5EC303BL,0xA275CB68L}},{{0x4EC2916EL,0xB5EC303BL,1L,0L,1L,0x4C93270DL,0x50F08702L,(-8L)},{(-8L),0xB5EC303BL,0xE8DE898BL,0xB5B01A2EL,0L,0x4C93270DL,0xE019F589L,0xA275CB68L},{0xFDE03299L,0xB5EC303BL,(-1L),0L,0xE6D0F294L,0x4C93270DL,0L,(-8L)},{0xA275CB68L,0xB5EC303BL,0x4C93270DL,0xB5B01A2EL,0xB5B01A2EL,0x4C93270DL,0xB5EC303BL,0xA275CB68L}},{{0x4EC2916EL,0xB5EC303BL,1L,0L,1L,0x4C93270DL,0x50F08702L,(-8L)},{(-8L),0xB5EC303BL,0xE8DE898BL,0xB5B01A2EL,0L,0x4C93270DL,0xE019F589L,0xA275CB68L},{0xFDE03299L,0xB5EC303BL,(-1L),0L,0xE6D0F294L,0x4C93270DL,0L,(-8L)},{0xA275CB68L,0xB5EC303BL,0x4C93270DL,0xB5B01A2EL,0xB5B01A2EL,0x4C93270DL,0xB5EC303BL,0xA275CB68L}},{{0x4EC2916EL,0xB5EC303BL,1L,0L,1L,0x4C93270DL,0x50F08702L,(-8L)},{(-8L),0xB5EC303BL,0xE8DE898BL,0xB5B01A2EL,0L,0x4C93270DL,0xE019F589L,0xA275CB68L},{0xFDE03299L,0xB5EC303BL,(-1L),0L,0xE6D0F294L,0x4C93270DL,0L,(-8L)},{0xA275CB68L,0xB5EC303BL,0x4C93270DL,0xB5B01A2EL,0xB5B01A2EL,0x4C93270DL,0xB5EC303BL,0xA275CB68L}},{{0x4EC2916EL,0xB5EC303BL,1L,0L,1L,0x4C93270DL,0x50F08702L,(-8L)},{(-8L),0xB5EC303BL,0xE8DE898BL,0xB5B01A2EL,0L,0x4C93270DL,0xE019F589L,0xA275CB68L},{0xFDE03299L,0xB5EC303BL,(-1L),0L,0xE6D0F294L,0x4C93270DL,0L,(-8L)},{0xA275CB68L,0xB5EC303BL,0x4C93270DL,0xB5B01A2EL,0xB5B01A2EL,0x4C93270DL,0xB5EC303BL,0xA275CB68L}}};
        int32_t l_2531 = 0L;
        int32_t l_2535 = 0x5B715398L;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2274[i] = 0x633B1798L;
        (*l_2248) = ((safe_mul_func_uint64_t_u_u(((((safe_mul_func_int32_t_s_s((safe_mul_func_uint64_t_u_u(((+((l_2241[1] | (!l_2241[3])) >= (((~(safe_add_func_uint16_t_u_u((safe_mul_func_uint64_t_u_u(((**l_2161) == (***l_2160)), ((*g_191) | p_41))), (((l_2241[1] > (l_2248 == ((safe_add_func_uint32_t_u_u(((((p_41 , (void*)0) != (*g_1788)) >= p_41) >= (*l_2248)), 0UL)) , (void*)0))) && 3L) , g_892[3])))) && (***g_1324)) == (*l_2121)))) , 0UL), (*l_2248))), (*l_2248))) > (*l_2248)) < 0x70D16942DBF3DB5FLL) >= (*l_2248)), (*g_1583))) <= p_41);
        for (l_2053 = (-2); (l_2053 >= 19); ++l_2053)
        { /* block id: 1029 */
            int32_t * const *l_2255 = &l_55[0][0];
            int32_t **l_2257 = &l_55[0][0];
            uint8_t l_2273 = 0xABL;
            int32_t l_2277 = 0xEFF896B5L;
            int32_t l_2300[2][9] = {{0xAABB85E9L,0xAABB85E9L,0xAABB85E9L,0xAABB85E9L,0xAABB85E9L,0xAABB85E9L,0xAABB85E9L,0xAABB85E9L,0xAABB85E9L},{0xECEA742DL,7L,0xECEA742DL,7L,0xECEA742DL,7L,0xECEA742DL,7L,0xECEA742DL}};
            int8_t l_2310[1];
            int16_t l_2373[1];
            int i, j;
            for (i = 0; i < 1; i++)
                l_2310[i] = 1L;
            for (i = 0; i < 1; i++)
                l_2373[i] = 0xAFE2L;
            if (p_41)
            { /* block id: 1030 */
                int32_t **l_2256[3];
                union U0 **l_2290 = &g_334;
                int64_t l_2309 = (-1L);
                uint16_t l_2311[5] = {65531UL,65531UL,65531UL,65531UL,65531UL};
                int i;
                for (i = 0; i < 3; i++)
                    l_2256[i] = &l_2248;
                (*l_2121) &= ((safe_rshift_func_int8_t_s_u(((void*)0 != &l_1395), (((65535UL > p_41) , l_2255) != (l_2257 = l_2256[1])))) == 0x5E2D799BL);
                for (g_364 = 8; (g_364 >= 0); g_364 -= 1)
                { /* block id: 1035 */
                    uint16_t l_2260[7] = {0x5BABL,0x5BABL,0x5BABL,0x5BABL,0x5BABL,0x5BABL,0x5BABL};
                    int32_t l_2261 = 1L;
                    int16_t *l_2264 = &g_35[1][0][3];
                    int32_t **l_2272 = &l_2248;
                    uint8_t l_2278 = 0xDFL;
                    int32_t l_2299 = 8L;
                    int32_t l_2301[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2301[i] = 0x4B042B1CL;
                    l_2261 = (safe_div_func_int32_t_s_s(p_41, l_2260[1]));
                }
            }
            else
            { /* block id: 1063 */
                uint64_t * const *l_2318 = &g_1326;
                uint64_t * const **l_2317 = &l_2318;
                int32_t l_2344 = 0x156BB3DCL;
                union U0 ****l_2361 = &g_1790;
                for (p_41 = 0; (p_41 >= 56); ++p_41)
                { /* block id: 1066 */
                    uint64_t * const ***l_2319[6][4] = {{&l_2317,&l_2317,&l_2317,&l_2317},{&l_2317,&l_2317,&l_2317,&l_2317},{&l_2317,&l_2317,&l_2317,&l_2317},{&l_2317,&l_2317,&l_2317,&l_2317},{&l_2317,&l_2317,&l_2317,&l_2317},{&l_2317,&l_2317,&l_2317,&l_2317}};
                    int32_t l_2337 = 0L;
                    uint32_t ***l_2340[7];
                    uint32_t l_2342[9] = {4294967286UL,4294967286UL,4294967286UL,4294967286UL,4294967286UL,4294967286UL,4294967286UL,4294967286UL,4294967286UL};
                    union U0 *l_2358 = &g_1714[3];
                    int i, j;
                    for (i = 0; i < 7; i++)
                        l_2340[i] = &g_1331[7];
                    (***l_1461) = (l_2316 != (g_2320 = l_2317));
                    for (l_1444 = 0; (l_1444 > 2); l_1444++)
                    { /* block id: 1071 */
                        const uint32_t l_2335[3][9][4] = {{{0xBC790364L,18446744073709551612UL,3UL,18446744073709551610UL},{0xA3A9D0F1L,0xFEEF1FF5L,0xFEEF1FF5L,0xA3A9D0F1L},{18446744073709551615UL,5UL,0x9C0B0D1BL,18446744073709551610UL},{0xBC790364L,0xA3A9D0F1L,0x91570E07L,0x83930063L},{18446744073709551610UL,18446744073709551615UL,0xE949CF7DL,0x83930063L},{0xA0E835C0L,0xA3A9D0F1L,18446744073709551615UL,18446744073709551610UL},{1UL,5UL,0UL,0xA3A9D0F1L},{5UL,0xFEEF1FF5L,18446744073709551615UL,18446744073709551610UL},{0x9C0B0D1BL,18446744073709551612UL,18446744073709551615UL,0xCB772727L}},{{18446744073709551615UL,0xBC790364L,0x5ED48294L,0xBC790364L},{18446744073709551610UL,18446744073709551615UL,0xA0E835C0L,5UL},{18446744073709551615UL,18446744073709551612UL,0x9C0B0D1BL,0xE949CF7DL},{0xA3A9D0F1L,5UL,0UL,0xA3A9D0F1L},{0xA3A9D0F1L,0xCB772727L,0x9C0B0D1BL,0x871F9197L},{18446744073709551615UL,0xA3A9D0F1L,0xA0E835C0L,0xB76E4067L},{18446744073709551610UL,18446744073709551615UL,0x5ED48294L,0x83930063L},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0x871F9197L},{0x9C0B0D1BL,5UL,18446744073709551615UL,18446744073709551615UL}},{{5UL,5UL,0UL,18446744073709551610UL},{1UL,2UL,18446744073709551615UL,5UL},{0xA0E835C0L,0xBC790364L,0xE949CF7DL,18446744073709551615UL},{18446744073709551610UL,0xBC790364L,0x91570E07L,5UL},{0xBC790364L,2UL,0x9C0B0D1BL,18446744073709551610UL},{18446744073709551615UL,5UL,0xFEEF1FF5L,18446744073709551615UL},{0xA3A9D0F1L,5UL,3UL,0x871F9197L},{0xBC790364L,18446744073709551615UL,0xA0E835C0L,0x83930063L},{0xE949CF7DL,18446744073709551615UL,0xE949CF7DL,0xB76E4067L}}};
                        uint32_t ***l_2338[1][1];
                        uint32_t ****l_2339 = &l_2338[0][0];
                        int16_t *l_2343 = &g_34;
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_2338[i][j] = &g_1331[7];
                        }
                        (*l_80) = (void*)0;
                        (**l_2257) = ((safe_add_func_int8_t_s_s((((((*l_2343) ^= ((safe_div_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((((*g_2293) = (**g_2292)) != l_2330), (((safe_lshift_func_int16_t_s_u(((--(***l_2317)) > l_2335[0][0][1]), (!p_41))) | (p_41 >= (l_2337 &= 0x9E1EC82DL))) , (((*l_2339) = l_2338[0][0]) == l_2340[0])))), (~(((*l_2330) = l_2342[5]) , 0xB4L)))) < 1L)) <= l_2342[5]) != l_2344) > 0L), p_41)) , (*l_2248));
                        if (p_41)
                            break;
                        (*l_80) = (*l_2257);
                    }
                    for (g_124 = 0; (g_124 > 19); g_124 = safe_add_func_int32_t_s_s(g_124, 9))
                    { /* block id: 1085 */
                        int16_t *l_2351 = &g_35[1][0][2];
                        int32_t l_2357 = (-1L);
                        union U0 ****l_2360 = &g_1790;
                        l_2357 ^= (((*g_1292) ^= (((((safe_mod_func_int8_t_s_s((((*l_2351) = (safe_sub_func_uint16_t_u_u(65526UL, p_41))) | p_41), (*l_2248))) || (0x289776448941D916LL >= ((((**g_2227) = 0x5AL) && ((((safe_mod_func_uint32_t_u_u((((***l_1461) && (((safe_add_func_uint32_t_u_u((p_41 || ((+1UL) , 1L)), p_41)) , (*g_1834)) <= 0xFDL)) < 0x0EL), 1UL)) && p_41) == (*g_191)) || 0xB3FEL)) <= 65533UL))) , p_41) <= p_41) == p_41)) > 5L);
                        l_2359 = l_2358;
                        l_2361 = l_2360;
                        (***l_1461) |= (*g_79);
                    }
                }
            }
            for (g_124 = 0; (g_124 > 4); g_124++)
            { /* block id: 1098 */
                int32_t *l_2375 = &l_1998;
                int32_t **l_2376 = &l_2375;
                for (l_1646 = 0; (l_1646 <= 3); l_1646 += 1)
                { /* block id: 1101 */
                    for (g_110 = 0; (g_110 <= 1); g_110 += 1)
                    { /* block id: 1104 */
                        int i, j;
                        if (l_2300[g_110][(g_110 + 6)])
                            break;
                    }
                }
                for (g_364 = 0; (g_364 >= 23); g_364 = safe_add_func_uint32_t_u_u(g_364, 8))
                { /* block id: 1110 */
                    int16_t *l_2368 = (void*)0;
                    int16_t *l_2369 = &g_920[0][0][3];
                    int32_t *l_2374 = &l_2203[0];
                    (*l_80) = func_70(((+(((*l_2369) = l_2367) | (!(safe_div_func_int8_t_s_s(l_2373[0], p_41))))) , func_50(func_70(func_50(l_2374, l_2374, ((*l_2257) = l_2375), &l_2375)), &l_2274[0], &l_2274[0], l_2376)));
                }
            }
            (**l_2255) = 0xF989D597L;
        }
        for (l_2231 = 0; (l_2231 != (-29)); l_2231 = safe_sub_func_int64_t_s_s(l_2231, 1))
        { /* block id: 1133 */
            uint8_t ***l_2421 = &l_1645;
            uint8_t ** const **l_2423 = &l_2422;
            uint64_t * const ***l_2433 = (void*)0;
            int32_t l_2434 = (-1L);
            int32_t l_2459 = 0x9AA21408L;
            uint64_t *l_2493 = &g_315[6];
            int32_t l_2532 = 1L;
            int32_t l_2533 = 0x93F76CE5L;
            int32_t l_2534[2];
            uint16_t l_2536 = 1UL;
            int i;
            for (i = 0; i < 2; i++)
                l_2534[i] = 0xDD982D49L;
        }
        (**g_531) = func_70(&l_2248);
    }
    else
    { /* block id: 1195 */
        union U0 l_2543 = {1L};
        int32_t l_2560 = 0L;
        int32_t l_2561[3][4][3] = {{{0x96C6EAB2L,0x96C6EAB2L,(-1L)},{0x96C6EAB2L,0x96C6EAB2L,(-1L)},{0x96C6EAB2L,0x96C6EAB2L,(-1L)},{0x96C6EAB2L,0x96C6EAB2L,(-1L)}},{{0x96C6EAB2L,3L,0x96C6EAB2L},{3L,3L,0x96C6EAB2L},{3L,3L,0x96C6EAB2L},{3L,3L,0x96C6EAB2L}},{{3L,3L,0x96C6EAB2L},{3L,3L,0x96C6EAB2L},{3L,3L,0x96C6EAB2L},{3L,3L,0x96C6EAB2L}}};
        union U0 ***l_2591 = &g_856;
        int32_t *l_2607[4][4];
        int8_t **l_2618 = &l_2221;
        int16_t l_2641 = 3L;
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 4; j++)
                l_2607[i][j] = &l_46;
        }
        if ((((*l_2121) , l_2075) != (l_2543 , (void*)0)))
        { /* block id: 1196 */
            uint64_t l_2552 = 0xC27B6C1D321AF847LL;
            int32_t l_2553 = (-3L);
            uint16_t *l_2558[6];
            uint32_t l_2559[2][3][1] = {{{1UL},{0xF66A691AL},{1UL}},{{0xF66A691AL},{1UL},{0xF66A691AL}}};
            int32_t l_2564 = 0xE1EFEAF9L;
            int32_t l_2566 = (-6L);
            int32_t l_2568[1][10][5] = {{{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L},{0L,0L,0L,0xFA0161C6L,0xFA0161C6L}}};
            int32_t * const ****l_2589 = &l_2161;
            uint64_t l_2594 = 18446744073709551615UL;
            int32_t *l_2610[10][9] = {{&l_2568[0][7][0],&g_2593[2][0],&l_2553,&l_2561[2][0][0],&g_1647,&g_1108,&l_2561[1][2][1],&g_2593[2][2],(void*)0},{(void*)0,&g_43,&l_2561[0][3][0],&l_2560,&l_2560,&l_2561[0][3][0],&g_43,(void*)0,&g_43},{(void*)0,&l_2561[2][0][0],&l_2560,&l_1993,(void*)0,&l_2564,&l_2561[1][2][1],&l_2568[0][1][0],&l_2553},{&l_2560,(void*)0,&l_2561[1][2][2],&g_43,&g_1108,(void*)0,(void*)0,&g_1108,&g_43},{&g_2593[0][4],&l_2553,&g_2593[0][4],&l_2203[1],&l_2561[1][2][1],&l_1993,&l_2568[0][7][0],&g_1108,(void*)0},{&l_2561[1][2][2],(void*)0,&l_2560,(void*)0,&l_2561[1][2][2],&g_43,&g_1108,(void*)0,(void*)0},{&l_2560,&l_2561[2][0][0],(void*)0,&l_2203[1],(void*)0,&l_2561[2][0][0],&l_2560,&l_1993,(void*)0},{&l_2561[0][3][0],&g_43,(void*)0,&g_43,&l_1444,&g_43,(void*)0,&g_43,&l_2561[0][3][0]},{&l_2553,&g_2593[2][0],&l_2568[0][7][0],&l_1993,&l_2561[1][2][1],&l_1993,&l_2561[1][2][1],&l_1993,&l_2568[0][7][0]},{&g_1108,&g_1108,(void*)0,&l_2560,&g_43,(void*)0,&l_2561[0][3][0],(void*)0,&g_43}};
            uint8_t *l_2637 = &g_271[3][2];
            uint32_t **l_2640[1];
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_2558[i] = &g_927;
            for (i = 0; i < 1; i++)
                l_2640[i] = (void*)0;
            if ((safe_add_func_int8_t_s_s(((((*l_2222) = 0x05L) > (l_2561[1][2][1] = (p_41 | (safe_rshift_func_uint8_t_u_u((l_2560 ^= ((safe_div_func_uint32_t_u_u(p_41, ((safe_mul_func_uint32_t_u_u((((l_2553 = ((*g_1332) = (((**g_1325) = (0UL && (*g_1292))) == l_2552))) , ((***g_2292) = (safe_sub_func_uint64_t_u_u((safe_div_func_int16_t_s_s(0L, (l_2553 = 0x8DF2L))), ((void*)0 != (*g_1789)))))) , l_2559[0][2][0]), p_41)) , (***g_531)))) , 0x50L)), 0))))) > 0xB613L), l_2543.f0)))
            { /* block id: 1205 */
                int64_t l_2562 = 0x39F1E4E674668FDBLL;
                int32_t l_2565[6][6][7] = {{{0x6F9A8E5EL,(-5L),0L,0x9D3EE545L,(-2L),0x14919F16L,0x76B20BE2L},{8L,0xE6124CD8L,0x058FE49DL,0x809166A3L,9L,0x864D992FL,9L},{0L,0L,0L,0L,6L,1L,0xD4DF5E73L},{0x058FE49DL,1L,(-7L),0x6D471F4EL,0x34617C57L,0x0D737BE8L,0L},{0L,0x3DB31B19L,(-1L),0x1AC4E25DL,(-1L),0xC117E17DL,1L},{0x329C9AE3L,0x12660412L,0xFFD1F01BL,0x14919F16L,0xE63F4FEAL,5L,0x76B20BE2L}},{{0x98BD21CDL,0x14919F16L,8L,0xA6AD817DL,(-1L),0xCE8AFA59L,0x821260DFL},{0x6F9A8E5EL,(-7L),0L,0x1AC4E25DL,0L,0xBC04EAC2L,1L},{(-3L),0x98BD21CDL,8L,0x6F9A8E5EL,(-1L),5L,0L},{0x6D471F4EL,0x1AC4E25DL,0x024C57E8L,0xBB949EDCL,(-1L),0xE63F4FEAL,0x7F331FB9L},{(-1L),(-1L),8L,0x8944C5A2L,(-1L),0x8B31FBF4L,(-1L)},{0x329C9AE3L,0x5F112C24L,0x98BD21CDL,0L,(-1L),0x864D992FL,0xA01180D7L}},{{0xB15F5D11L,7L,0xA6AD817DL,0x12660412L,0L,(-1L),0x058FE49DL},{0L,1L,0xE6124CD8L,(-1L),(-1L),0x36674332L,0x8944C5A2L},{(-8L),8L,0xE63F4FEAL,0x329C9AE3L,0xE63F4FEAL,8L,(-8L)},{0x1A688C77L,0x1AC4E25DL,0x7F331FB9L,0x864D992FL,(-1L),(-1L),0x6D471F4EL},{0x76B20BE2L,(-1L),6L,1L,0x34617C57L,0xD4DF5E73L,0x1AC4E25DL},{0x3DB31B19L,0xFFD1F01BL,0x7F331FB9L,1L,0xCE8AFA59L,0x98BD21CDL,0x14919F16L}},{{0xFFD1F01BL,0x14919F16L,0xE63F4FEAL,5L,0x76B20BE2L,0xE63F4FEAL,0xA6AD817DL},{0x34617C57L,0xA01180D7L,0xE6124CD8L,1L,0x14919F16L,9L,0x1AC4E25DL},{1L,(-1L),0xA6AD817DL,0xFDBAD260L,0x0D737BE8L,0xE4D6500FL,0x6F9A8E5EL},{0xA6AD817DL,1L,0x98BD21CDL,(-7L),0xA01180D7L,0xCE8AFA59L,0xBB949EDCL},{(-1L),0x6F9A8E5EL,8L,0x76B20BE2L,1L,(-1L),0x8944C5A2L},{(-1L),5L,0x024C57E8L,1L,2L,0xA6AD817DL,0L}},{{0xA6AD817DL,0L,8L,0x5F112C24L,1L,0x0D737BE8L,0x12660412L},{1L,0x0C08F3C4L,0L,0L,(-3L),6L,(-1L)},{0x34617C57L,0x30793B1EL,8L,0x329C9AE3L,(-7L),(-7L),0x329C9AE3L},{0xFFD1F01BL,0L,0xFFD1F01BL,5L,0xA01180D7L,2L,0x864D992FL},{0x3DB31B19L,0x0C08F3C4L,(-1L),0xA01180D7L,0x8944C5A2L,3L,1L},{0x76B20BE2L,0xB15F5D11L,(-7L),0L,0xA6AD817DL,2L,1L}},{{0x1A688C77L,(-1L),(-1L),0L,0x821260DFL,6L,(-5L)},{0xE63F4FEAL,5L,0x76B20BE2L,0xE63F4FEAL,0xA6AD817DL,(-7L),0x12660412L},{0x6D471F4EL,0xC41C2154L,6L,5L,0x809166A3L,0L,0xBB949EDCL},{0x36674332L,0x024C57E8L,0xF2E3899AL,0L,0x864D992FL,0xAE4454C6L,6L},{8L,0xFFD1F01BL,1L,2L,0x3CAB9A1DL,1L,0x821260DFL},{0x7F331FB9L,0xAE4454C6L,0x436652EDL,2L,9L,0xC117E17DL,0xB15F5D11L}}};
                int64_t l_2581 = 0x26B40AD365308E67LL;
                const union U0 ***l_2632 = (void*)0;
                union U0 ***l_2633 = &g_333;
                int i, j, k;
                for (p_41 = 0; (p_41 <= 8); p_41 += 1)
                { /* block id: 1208 */
                    int32_t l_2563 = (-1L);
                    int32_t l_2567[2][5] = {{(-1L),(-6L),(-1L),(-6L),(-1L)},{0xA19DB626L,0xA19DB626L,0xA19DB626L,0xA19DB626L,0xA19DB626L}};
                    uint16_t l_2569[4] = {0xE43AL,0xE43AL,0xE43AL,0xE43AL};
                    int16_t *l_2590[9][1] = {{&g_920[3][0][3]},{&l_45},{&g_920[3][0][3]},{&l_45},{&g_920[3][0][3]},{&l_45},{&g_920[3][0][3]},{&l_45},{&g_920[3][0][3]}};
                    int i, j;
                    ++l_2569[1];
                    for (l_2553 = 0; l_2553 < 9; l_2553 += 1)
                    {
                        g_1331[l_2553] = &g_1332;
                    }
                    (**g_531) = &l_2560;
                    l_2565[2][5][1] ^= ((safe_lshift_func_uint64_t_u_u(((safe_rshift_func_uint32_t_u_s(((safe_sub_func_uint64_t_u_u(((g_386 = (safe_rshift_func_int32_t_s_u(l_2580, (l_2581 > ((safe_sub_func_int32_t_s_s(((*l_2121) = 1L), 0x119A9D3BL)) < (-1L)))))) == ((+(((safe_mod_func_int16_t_s_s(((g_110 , (safe_mod_func_int32_t_s_s((((((*g_1332) = 8UL) ^ ((g_364 = ((g_2164 = l_2589) != (void*)0)) & (-1L))) <= (**g_532)) < p_41), p_41))) >= l_2561[2][2][2]), p_41)) , l_2591) != (**l_1394))) | 1L)), l_2543.f0)) && p_41), p_41)) || (*g_79)), (**g_1325))) ^ 1UL);
                }
                ++l_2594;
                if (((safe_mod_func_uint8_t_u_u((safe_sub_func_int32_t_s_s((*g_79), l_2560)), (l_2565[2][5][1] = (safe_mod_func_uint8_t_u_u(((*g_1834) = (0xC4L >= (**g_1833))), (safe_rshift_func_int8_t_s_u(((**g_2227) |= ((g_1633[3] , (((safe_lshift_func_uint16_t_u_u(((void*)0 == l_2607[0][1]), 7)) , (((*g_1332) = 0x44768551L) >= ((safe_sub_func_uint64_t_u_u(((void*)0 == l_2610[2][5]), p_41)) , p_41))) != 1L)) , 9L)), 6))))))) >= p_41))
                { /* block id: 1224 */
                    uint32_t l_2617 = 18446744073709551615UL;
                    int32_t l_2631 = 0xAB919C71L;
                    int32_t l_2634 = 1L;
                    const uint32_t l_2635 = 0x15FF57ECL;
                    int32_t l_2636 = (-2L);
                    l_2636 = (safe_div_func_uint64_t_u_u((safe_sub_func_int32_t_s_s(((((l_2617 < ((void*)0 == l_2618)) & p_41) ^ (safe_rshift_func_int64_t_s_s(((*g_531) != (((safe_mul_func_uint16_t_u_u((((l_2634 ^= ((safe_mod_func_int32_t_s_s(((((safe_unary_minus_func_uint16_t_u((((safe_mul_func_int16_t_s_s((safe_sub_func_int32_t_s_s((((~(*g_191)) != (l_2631 = p_41)) , (l_2632 != ((**g_1788) = l_2633))), 2L)), p_41)) && g_85) | 0xF631L))) | (**g_2227)) > (**g_1582)) , (-7L)), (*g_1292))) & 0UL)) >= l_2617) , p_41), 1UL)) & 0x496F9B1AL) , (***l_2589))), 36))) >= l_2635), l_2565[2][5][1])), 0xEDA30CD24E8BF397LL));
                }
                else
                { /* block id: 1229 */
                    (***l_1461) = ((void*)0 != l_2637);
                }
                l_2565[2][5][1] &= (safe_rshift_func_uint32_t_u_u((((*g_191) & ((p_41 , &l_2560) == (void*)0)) >= p_41), (*l_2121)));
            }
            else
            { /* block id: 1233 */
                (*l_2121) = p_41;
                (*l_1975) = l_2640[0];
            }
        }
        else
        { /* block id: 1237 */
            uint16_t l_2642 = 0x801BL;
            union U0 ***** const l_2657 = &g_1016;
            l_2642++;
            (*l_2121) = (safe_rshift_func_int64_t_s_u((((void*)0 != (*g_2292)) == (safe_lshift_func_int64_t_s_s((safe_add_func_uint64_t_u_u((((((p_41 , (**g_1325)) > ((((**g_2227) = (**g_2227)) || ((((**g_2227) = ((((safe_div_func_uint16_t_u_u(((safe_div_func_int8_t_s_s((safe_add_func_uint64_t_u_u(0x8B9F2DD5AA5ED2CCLL, ((*g_1015) != &l_2591))), (((&g_1694 == l_2657) , p_41) || (*g_1326)))) < 4294967286UL), g_2108)) < p_41) >= 0x1A1937B6L) ^ 0x296EL)) >= p_41) && l_2642)) , p_41)) > 0x24636CF44A599B4DLL) , 7UL) && (*g_191)), (*g_191))), p_41))), l_2642));
        }
    }
    return p_41;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t ** func_50(int32_t * p_51, const int32_t * p_52, int32_t * p_53, int32_t ** p_54)
{ /* block id: 624 */
    int32_t **l_1334 = &g_79;
    return l_1334;
}


/* ------------------------------------------ */
/* 
 * reads : g_1324 g_532
 * writes: g_1325 g_533 g_1331 g_1283 g_43 g_85 g_35
 */
static union U0  func_56(int8_t  p_57, const union U0  p_58, int16_t  p_59)
{ /* block id: 618 */
    uint64_t *l_1322[7] = {&g_931,&g_931,&g_931,&g_931,&g_931,&g_931,&g_931};
    uint64_t **l_1321 = &l_1322[2];
    uint64_t ***l_1323 = (void*)0;
    const int32_t *l_1327 = (void*)0;
    uint32_t *l_1329 = &g_125;
    uint32_t **l_1328[2][3][10] = {{{&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329},{(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329},{&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329}},{{(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329},{&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329,&l_1329},{(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329,(void*)0,&l_1329}}};
    uint32_t ***l_1330[2];
    union U0 l_1333 = {0x367737FCL};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1330[i] = &l_1328[0][0][6];
    (*g_1324) = l_1321;
    (*g_532) = l_1327;
    g_1331[7] = l_1328[1][1][6];
    for (g_1283 = 0; g_1283 < 2; g_1283 += 1)
    {
        for (g_43 = 0; g_43 < 1; g_43 += 1)
        {
            for (g_85 = 0; g_85 < 4; g_85 += 1)
            {
                g_35[g_1283][g_43][g_85] = (-4L);
            }
        }
    }
    return l_1333;
}


/* ------------------------------------------ */
/* 
 * reads : g_1292 g_1108 g_1299 g_271 g_47 g_1008 g_364 g_532 g_35 g_43 g_191 g_192 g_125 g_124 g_110
 * writes: g_1108 g_315 g_927 g_533 g_43 g_271 g_601 g_125
 */
static union U0  func_60(union U0  p_61, uint32_t  p_62, uint8_t  p_63, int64_t  p_64, int8_t  p_65)
{ /* block id: 611 */
    uint16_t l_1289 = 65528UL;
    int32_t ****l_1291 = &g_255[2][3];
    int32_t *****l_1290 = &l_1291;
    const uint16_t *l_1302 = &g_927;
    const uint16_t **l_1301[8] = {&l_1302,&l_1302,&l_1302,&l_1302,&l_1302,&l_1302,&l_1302,&l_1302};
    union U0 l_1303[7][2][10] = {{{{-7L},{8L},{8L},{-7L},{0xFF8107D0L},{-1L},{-7L},{-1L},{0xFF8107D0L},{-7L}},{{-1L},{-7L},{-1L},{0xFF8107D0L},{-7L},{8L},{8L},{-7L},{0xFF8107D0L},{-1L}}},{{{0xDF730AB3L},{0xDF730AB3L},{0x75362A10L},{-7L},{1L},{0x75362A10L},{1L},{-7L},{0x75362A10L},{0xDF730AB3L}},{{1L},{8L},{-1L},{1L},{0xFF8107D0L},{0xFF8107D0L},{1L},{-1L},{8L},{1L}}},{{{-1L},{0xDF730AB3L},{8L},{0xFF8107D0L},{0xDF730AB3L},{0xFF8107D0L},{8L},{0xDF730AB3L},{-1L},{-1L}},{{1L},{-7L},{0x75362A10L},{-1L},{-1L},{0xDF730AB3L},{8L},{0xFF8107D0L},{0xDF730AB3L},{0xFF8107D0L}}},{{{-1L},{0x75362A10L},{0xC4DBD36BL},{-1L},{0xC4DBD36BL},{0x75362A10L},{-1L},{0x8602DCD0L},{0x8602DCD0L},{-1L}},{{0x8602DCD0L},{0xFF8107D0L},{0xC4DBD36BL},{0xC4DBD36BL},{0xFF8107D0L},{0x8602DCD0L},{0x75362A10L},{0xFF8107D0L},{0x75362A10L},{0x8602DCD0L}}},{{{8L},{0xFF8107D0L},{0xDF730AB3L},{0xFF8107D0L},{8L},{0xDF730AB3L},{-1L},{-1L},{0xDF730AB3L},{8L}},{{8L},{0x75362A10L},{0x75362A10L},{8L},{0xC4DBD36BL},{0x8602DCD0L},{8L},{0x8602DCD0L},{0xC4DBD36BL},{8L}}},{{{0x8602DCD0L},{8L},{0x8602DCD0L},{0xC4DBD36BL},{8L},{0x75362A10L},{0x75362A10L},{8L},{0xC4DBD36BL},{0x8602DCD0L}},{{-1L},{-1L},{0xDF730AB3L},{8L},{0xFF8107D0L},{0xDF730AB3L},{0xFF8107D0L},{8L},{0xDF730AB3L},{-1L}}},{{{0xFF8107D0L},{0x75362A10L},{0x8602DCD0L},{0xFF8107D0L},{0xC4DBD36BL},{0xC4DBD36BL},{0xFF8107D0L},{0x8602DCD0L},{0x75362A10L},{0xFF8107D0L}},{{0x8602DCD0L},{-1L},{0x75362A10L},{0xC4DBD36BL},{-1L},{0xC4DBD36BL},{0x75362A10L},{-1L},{0x8602DCD0L},{0x8602DCD0L}}}};
    uint64_t *l_1312 = &g_315[6];
    uint8_t l_1317 = 1UL;
    uint16_t *l_1318 = &g_927;
    int8_t l_1319 = 1L;
    int32_t *l_1320 = &g_1283;
    int i, j, k;
    (*g_1292) &= (l_1289 == (&g_254 != l_1290));
    (*g_532) = ((safe_sub_func_uint64_t_u_u((((void*)0 == &g_527) < ((safe_sub_func_uint64_t_u_u((((g_1299 == l_1301[2]) | (((*l_1318) = (l_1303[0][0][6] , (((safe_div_func_int8_t_s_s(((safe_lshift_func_int32_t_s_s(((safe_add_func_int32_t_s_s(((safe_lshift_func_int64_t_s_s(p_62, 5)) | ((*l_1312) = 18446744073709551614UL)), (safe_lshift_func_uint16_t_u_u((g_271[0][2] <= (safe_mod_func_int16_t_s_s((((p_61.f0 <= p_63) , l_1303[0][0][6]) , p_65), p_65))), p_63)))) ^ 1L), 6)) && g_47), g_1008[0])) & p_63) <= l_1317))) != 0L)) , 18446744073709551609UL), l_1319)) == g_364)), p_65)) , l_1320);
    (*g_532) = func_70(&l_1320);
    return p_61;
}


/* ------------------------------------------ */
/* 
 * reads : g_892 g_43 g_191 g_192 g_364
 * writes: g_43 g_386
 */
static union U0  func_66(int16_t  p_67, int32_t * p_68, int32_t  p_69)
{ /* block id: 605 */
    int32_t *l_1265 = &g_43;
    uint8_t *l_1272 = (void*)0;
    uint8_t **l_1271 = &l_1272;
    union U0 **l_1275 = (void*)0;
    int32_t l_1278 = 0x039C871DL;
    int32_t *l_1279 = &g_1108;
    int32_t *l_1280 = &l_1278;
    int32_t *l_1281[1][4];
    int32_t l_1282[7][2] = {{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L},{0L,0L}};
    uint32_t l_1284[5] = {0x5C66218CL,0x5C66218CL,0x5C66218CL,0x5C66218CL,0x5C66218CL};
    union U0 l_1287[6] = {{-9L},{-9L},{-9L},{-9L},{-9L},{-9L}};
    int i, j;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
            l_1281[i][j] = &l_1278;
    }
    (*l_1265) = 0L;
    p_69 = (safe_sub_func_int8_t_s_s((~(safe_sub_func_uint8_t_u_u(((void*)0 != l_1271), (safe_add_func_int16_t_s_s(g_892[2], (l_1275 != ((*l_1265) , l_1275))))))), ((0x74E1E4F7692551E0LL >= (g_386 = (safe_lshift_func_int64_t_s_u((*g_191), 26)))) , ((*l_1265) , g_364))));
    l_1284[0]--;
    return l_1287[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_35 g_43 g_191 g_192 g_271 g_125 g_124 g_110
 * writes: g_43 g_271 g_601 g_125
 */
static int32_t * func_70(int32_t ** p_71)
{ /* block id: 596 */
    uint32_t l_1236[3][1][3] = {{{0xF8E88E2FL,0xF8E88E2FL,0xF8E88E2FL}},{{9UL,4294967295UL,9UL}},{{0xF8E88E2FL,0xF8E88E2FL,0xF8E88E2FL}}};
    int32_t l_1237 = 0x99196EDDL;
    int32_t *l_1238 = &g_43;
    int64_t *l_1243[9] = {&g_386,&g_386,&g_386,&g_386,&g_386,&g_386,&g_386,&g_386,&g_386};
    uint8_t *l_1246 = &g_271[0][2];
    uint16_t *l_1254[7][6][6] = {{{&g_601,(void*)0,&g_601,&g_927,&g_927,&g_927},{&g_110,&g_110,&g_110,&g_110,&g_927,&g_601},{&g_601,(void*)0,&g_110,&g_601,&g_927,(void*)0},{&g_601,&g_110,&g_601,&g_601,&g_927,&g_110},{&g_110,(void*)0,&g_110,(void*)0,&g_927,&g_927},{&g_601,&g_110,&g_110,&g_601,&g_927,&g_110}},{{&g_601,(void*)0,&g_601,&g_927,&g_927,&g_927},{&g_110,&g_110,&g_110,&g_110,&g_927,&g_601},{&g_601,(void*)0,&g_110,&g_601,&g_927,(void*)0},{&g_601,&g_110,&g_601,&g_601,&g_927,&g_110},{&g_110,&g_927,&g_601,&g_110,(void*)0,&g_110},{&g_927,&g_601,&g_927,&g_927,(void*)0,&g_601}},{{&g_927,&g_927,&g_110,&g_110,(void*)0,(void*)0},{(void*)0,&g_601,&g_601,(void*)0,(void*)0,&g_110},{&g_927,&g_927,&g_927,&g_601,(void*)0,&g_927},{&g_927,&g_601,&g_110,&g_927,(void*)0,&g_927},{(void*)0,&g_927,&g_601,&g_110,(void*)0,&g_110},{&g_927,&g_601,&g_927,&g_927,(void*)0,&g_601}},{{&g_927,&g_927,&g_110,&g_110,(void*)0,(void*)0},{(void*)0,&g_601,&g_601,(void*)0,(void*)0,&g_110},{&g_927,&g_927,&g_927,&g_601,(void*)0,&g_927},{&g_927,&g_601,&g_110,&g_927,(void*)0,&g_927},{(void*)0,&g_927,&g_601,&g_110,(void*)0,&g_110},{&g_927,&g_601,&g_927,&g_927,(void*)0,&g_601}},{{&g_927,&g_927,&g_110,&g_110,(void*)0,(void*)0},{(void*)0,&g_601,&g_601,(void*)0,(void*)0,&g_110},{&g_927,&g_927,&g_927,&g_601,(void*)0,&g_927},{&g_927,&g_601,&g_110,&g_927,(void*)0,&g_927},{(void*)0,&g_927,&g_601,&g_110,(void*)0,&g_110},{&g_927,&g_601,&g_927,&g_927,(void*)0,&g_601}},{{&g_927,&g_927,&g_110,&g_110,(void*)0,(void*)0},{(void*)0,&g_601,&g_601,(void*)0,(void*)0,&g_110},{&g_927,&g_927,&g_927,&g_601,(void*)0,&g_927},{&g_927,&g_601,&g_110,&g_927,(void*)0,&g_927},{(void*)0,&g_927,&g_601,&g_110,(void*)0,&g_110},{&g_927,&g_601,&g_927,&g_927,(void*)0,&g_601}},{{&g_927,&g_927,&g_110,&g_110,(void*)0,(void*)0},{(void*)0,&g_601,&g_601,(void*)0,(void*)0,&g_110},{&g_927,&g_927,&g_927,&g_601,(void*)0,&g_927},{&g_927,&g_601,&g_110,&g_927,(void*)0,&g_927},{(void*)0,&g_927,&g_601,&g_110,(void*)0,&g_110},{&g_927,&g_601,&g_927,&g_927,(void*)0,&g_601}}};
    uint32_t *l_1255 = &g_125;
    uint64_t l_1258 = 18446744073709551610UL;
    int32_t ****l_1260 = (void*)0;
    int32_t *****l_1259 = &l_1260;
    int32_t *l_1261[9][8] = {{&g_43,&g_1108,&g_43,(void*)0,&g_2,&g_43,&g_1108,&g_1108},{(void*)0,&g_2,&g_43,&g_43,&g_43,&g_43,&g_2,(void*)0},{(void*)0,(void*)0,(void*)0,&g_43,&g_2,&g_2,&g_43,&g_2},{&g_43,(void*)0,&g_1108,(void*)0,&g_43,&g_2,&g_1108,&g_2},{&g_1108,(void*)0,(void*)0,&g_2,&g_43,&g_43,&g_2,(void*)0},{&g_2,&g_2,(void*)0,(void*)0,(void*)0,&g_43,&g_1108,&g_1108},{&g_43,&g_1108,&g_1108,&g_2,&g_1108,&g_1108,&g_43,&g_1108},{&g_1108,&g_43,(void*)0,(void*)0,(void*)0,&g_2,&g_2,(void*)0},{&g_2,&g_43,&g_43,&g_2,(void*)0,(void*)0,&g_1108,&g_2}};
    uint8_t l_1262 = 0UL;
    int32_t *l_1263 = (void*)0;
    int i, j, k;
    l_1237 |= (g_35[1][0][0] > (l_1236[2][0][0] != 1UL));
    (*l_1238) = l_1236[2][0][0];
    l_1262 |= (safe_div_func_int64_t_s_s((*l_1238), ((safe_rshift_func_uint64_t_u_s((((0xD456CD8F899DB1B5LL != (*g_191)) , ((l_1237 = (*g_191)) ^ (safe_lshift_func_uint8_t_u_u((++(*l_1246)), 7)))) && (safe_rshift_func_int32_t_s_s(((safe_lshift_func_uint8_t_u_u((!(g_601 = (*l_1238))), ((-1L) != (((l_1243[8] == ((((*l_1255)++) < ((((g_124 <= 0UL) || l_1258) , l_1259) == (void*)0)) , (void*)0)) ^ (*l_1238)) <= (-4L))))) , 0xF4531070L), (*l_1238)))), (*g_191))) , g_110)));
    return l_1263;
}


/* ------------------------------------------ */
/* 
 * reads : g_79 g_2 g_110 g_34 g_17.f0 g_78 g_19 g_85 g_128 g_113 g_47 g_124 g_35 g_43 g_17 g_191 g_125 g_271 g_315 g_192 g_526 g_528 g_532 g_533 g_386 g_334 g_599 g_601 g_364 g_531 g_856 g_927 g_931
 * writes: g_85 g_110 g_113 g_125 g_128 g_47 g_19 g_43 g_35 g_333 g_364 g_271 g_124 g_34 g_528 g_531 g_78 g_386 g_601 g_599 g_334 g_315 g_533 g_892 g_927 g_931
 */
static int32_t ** func_72(int32_t * p_73, int32_t ** p_74, const int32_t ** p_75)
{ /* block id: 18 */
    int8_t *l_84 = &g_85;
    int32_t l_94 = 1L;
    uint8_t l_99 = 0xACL;
    int32_t ***l_106[4];
    uint32_t *l_123 = &g_124;
    uint8_t l_132 = 248UL;
    int32_t l_244 = 0xA619F6E6L;
    uint16_t *l_263 = &g_110;
    union U0 l_273 = {0xEF78CEAEL};
    int32_t l_277[9][4] = {{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0xF7A1DF52L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0xF7A1DF52L,0L},{0L,0L,0L,0L},{0L,0L,0L,0L},{0L,0L,0xF7A1DF52L,0L}};
    int32_t l_366 = 9L;
    int32_t **l_478 = &g_79;
    int16_t l_484 = 0xA202L;
    uint32_t *l_488 = &g_113;
    uint32_t **l_487 = &l_488;
    int64_t l_494[8][7] = {{3L,(-10L),0x41EEEAB23089BA3CLL,(-10L),3L,3L,(-10L)},{0xE4923B36E46F5E9CLL,0x43BA87689B4FBA39LL,0xE4923B36E46F5E9CLL,(-7L),0x514DE6A923FF8772LL,6L,0x00004F21FEF71C7BLL},{(-10L),0L,0x41EEEAB23089BA3CLL,0x41EEEAB23089BA3CLL,0L,(-10L),0L},{0xE4923B36E46F5E9CLL,(-7L),0x514DE6A923FF8772LL,6L,0x00004F21FEF71C7BLL,6L,0x514DE6A923FF8772LL},{3L,3L,(-10L),0x41EEEAB23089BA3CLL,(-10L),3L,3L},{4L,(-7L),9L,(-7L),4L,0xFE2E1FA355EA49A4LL,0x514DE6A923FF8772LL},{0xB7B04CA549E4FBF5LL,0L,0xB7B04CA549E4FBF5LL,(-10L),(-10L),0xB7B04CA549E4FBF5LL,0L},{0x514DE6A923FF8772LL,0x43BA87689B4FBA39LL,9L,0xA8C1E3A1BD62B846LL,0x00004F21FEF71C7BLL,(-7L),0x00004F21FEF71C7BLL}};
    union U0 ***l_497 = &g_333;
    const int32_t l_498 = 0L;
    uint32_t l_504[2];
    int64_t l_505 = 1L;
    const uint32_t l_525[3][3] = {{1UL,1UL,1UL},{1UL,1UL,1UL},{1UL,1UL,1UL}};
    const uint16_t l_540 = 0UL;
    int32_t ***l_541 = &g_78[1];
    uint8_t l_581 = 0x1CL;
    uint32_t l_726 = 8UL;
    union U0 ****l_736 = &l_497;
    uint32_t l_815 = 4294967294UL;
    int16_t l_874[6] = {5L,5L,(-4L),5L,5L,(-4L)};
    uint64_t l_939 = 0x5914720FFE7883CALL;
    uint32_t l_982[7][10] = {{0xDE25B3E1L,18446744073709551615UL,0UL,0UL,0xAA7D429EL,0UL,0UL,18446744073709551615UL,0xDE25B3E1L,0xD2E0C01AL},{18446744073709551615UL,0xDE25B3E1L,0UL,1UL,0xCBF7E497L,18446744073709551615UL,0x199C3D65L,0xD2E0C01AL,0xD2E0C01AL,0x199C3D65L},{0xAA7D429EL,18446744073709551606UL,1UL,1UL,18446744073709551606UL,0xAA7D429EL,1UL,18446744073709551615UL,0x71F9E471L,18446744073709551615UL},{18446744073709551615UL,0xD2E0C01AL,0xDE25B3E1L,18446744073709551615UL,0UL,0UL,0xAA7D429EL,0UL,0UL,18446744073709551615UL},{18446744073709551615UL,0UL,18446744073709551615UL,18446744073709551615UL,0x199C3D65L,0xAA7D429EL,18446744073709551615UL,0xCBF7E497L,0xDE25B3E1L,18446744073709551615UL},{0xAA7D429EL,18446744073709551615UL,0xCBF7E497L,0xDE25B3E1L,18446744073709551615UL,18446744073709551615UL,0xDE25B3E1L,0xCBF7E497L,18446744073709551615UL,0xAA7D429EL},{18446744073709551606UL,0xF9C297BFL,18446744073709551615UL,0xD2E0C01AL,0xDE25B3E1L,18446744073709551615UL,0UL,0UL,0xAA7D429EL,0UL}};
    union U0 *l_1042 = &g_1043;
    const int8_t *l_1067 = &g_85;
    uint32_t l_1167 = 1UL;
    int32_t **l_1170 = &g_79;
    int16_t l_1223 = 0L;
    int32_t **l_1235 = &g_79;
    int i, j;
    for (i = 0; i < 4; i++)
        l_106[i] = (void*)0;
    for (i = 0; i < 2; i++)
        l_504[i] = 1UL;
    if (((*p_73) = (safe_div_func_int8_t_s_s(((*l_84) = (1L & (0xDF6EF7D1L & (*g_79)))), (safe_sub_func_uint32_t_u_u(0xF7F8006AL, ((safe_lshift_func_int16_t_s_u((safe_div_func_uint32_t_u_u((safe_add_func_uint8_t_u_u(l_94, ((((safe_rshift_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(5L, (l_94 , l_94))), l_94)) && 0x48E3606EL) ^ l_94) ^ l_94))), l_99)), 2)) >= l_94)))))))
    { /* block id: 21 */
        return &g_79;
    }
    else
    { /* block id: 23 */
        int8_t l_100 = 1L;
        int32_t *l_105 = (void*)0;
        const int32_t *l_107 = &g_17.f0;
        uint16_t *l_108 = (void*)0;
        uint16_t *l_109[6][7] = {{&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110},{&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110},{&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110},{&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,(void*)0},{&g_110,(void*)0,&g_110,&g_110,(void*)0,&g_110,&g_110},{&g_110,&g_110,&g_110,&g_110,(void*)0,&g_110,&g_110}};
        int64_t l_135[8] = {0x7205C45CDDFB24A3LL,0x7205C45CDDFB24A3LL,0x7205C45CDDFB24A3LL,0x7205C45CDDFB24A3LL,0x7205C45CDDFB24A3LL,0x7205C45CDDFB24A3LL,0x7205C45CDDFB24A3LL,0x7205C45CDDFB24A3LL};
        int32_t l_203 = 0xBC2652C7L;
        int32_t l_204 = 0x1699B072L;
        int32_t l_209 = (-1L);
        int32_t l_212 = 0x77730140L;
        int32_t l_218 = 0xF1D45198L;
        int32_t l_224 = (-1L);
        int32_t l_227[3];
        int32_t **l_258[8] = {&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79,&g_79};
        int32_t *** const l_257 = &l_258[0];
        int32_t *** const *l_256 = &l_257;
        union U0 *l_331 = &l_273;
        union U0 **l_330 = &l_331;
        uint32_t l_376[5];
        uint32_t l_387 = 0x889BB694L;
        int32_t *l_404 = &l_94;
        uint64_t l_408[4];
        int8_t l_430[7][4][7] = {{{0x17L,(-7L),0xF0L,0x29L,0x29L,0xF0L,(-7L)},{7L,0x22L,0L,1L,0x54L,0xA5L,0xB2L},{0xF0L,0x95L,0x38L,(-7L),0x38L,0x95L,0xF0L},{0xB2L,0xA5L,0x54L,1L,0L,0x22L,7L}},{{(-7L),0xF0L,0x29L,0x29L,0xF0L,(-7L),0x17L},{0x1EL,0x78L,0x54L,0xB2L,(-8L),0L,0L},{(-9L),0x03L,0x38L,0x03L,(-9L),1L,0x95L},{0x73L,0x78L,0L,0xA5L,1L,0x1EL,1L}},{{0xB0L,0xF0L,0xF0L,0xB0L,0x03L,1L,0x2CL},{0x73L,0xA5L,(-8L),0L,7L,7L,0L},{(-9L),0x95L,(-9L),1L,0x03L,0xF0L,(-9L)},{0xB2L,0x54L,0x78L,0x1EL,0x73L,0x1EL,0x78L}},{{0x03L,0x03L,0x95L,1L,0x38L,0xF0L,0x17L},{0xA5L,(-8L),0L,7L,7L,0L,(-8L)},{0xB0L,0x97L,0x03L,0x29L,0x38L,1L,1L},{0L,0xB2L,0x73L,(-8L),0x73L,0xB2L,0L}},{{1L,1L,0x38L,0x29L,0x03L,0x97L,0xB0L},{(-8L),0L,7L,7L,0L,(-8L),0xA5L},{0x17L,0xF0L,0x38L,1L,0x95L,0x03L,0x03L},{0x78L,0x1EL,0x73L,0x1EL,0x78L,0x54L,0xB2L}},{{(-9L),0xF0L,0x03L,1L,0x29L,0x17L,0x29L},{0L,0L,0L,0L,0x1EL,0x22L,1L},{(-9L),1L,0x95L,(-7L),0xB0L,0xB0L,(-7L)},{0x78L,0xB2L,0x78L,0x22L,(-8L),7L,1L}},{{0x17L,0x97L,0x29L,0x95L,0x2CL,0x95L,0x29L},{(-8L),(-8L),0x1EL,0xA5L,(-8L),7L,0xB2L},{1L,0x03L,0xB0L,0xF0L,0xF0L,0xB0L,0x03L},{0L,0x54L,(-8L),0x78L,(-8L),0x22L,0xA5L}}};
        int64_t l_435 = 0xA19FBDA79B5EFEC0LL;
        int8_t l_437 = 0x80L;
        uint64_t l_471 = 0x3FAEF98773F745D2LL;
        uint16_t l_479 = 0x04E3L;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_227[i] = 0x5062452FL;
        for (i = 0; i < 5; i++)
            l_376[i] = 0x1B4D3F15L;
        for (i = 0; i < 4; i++)
            l_408[i] = 18446744073709551615UL;
        if ((l_100 > (safe_add_func_uint16_t_u_u((g_110 |= (0UL < (((*p_73) = (safe_sub_func_int32_t_s_s((g_2 | l_100), (&g_85 == (void*)0)))) != ((l_105 = &l_94) != ((l_106[2] == (void*)0) , l_107))))), 0L))))
        { /* block id: 27 */
            uint16_t l_111 = 0x51F0L;
            int32_t l_126 = 0x7268510EL;
            int8_t l_129 = 0L;
            int32_t l_134 = 0xAFB31ACBL;
            for (l_94 = 0; (l_94 <= 0); l_94 += 1)
            { /* block id: 30 */
                uint32_t *l_112 = &g_113;
                int8_t *l_127[2];
                uint8_t *l_138 = &g_47;
                uint64_t *l_139 = (void*)0;
                uint64_t *l_140 = &g_19;
                int32_t l_141 = 1L;
                int32_t **l_145 = &g_79;
                int i;
                for (i = 0; i < 2; i++)
                    l_127[i] = &l_100;
                l_129 &= (((*l_112) = l_111) & (+((!(safe_rshift_func_int8_t_s_s((g_128 &= (safe_div_func_uint16_t_u_u((!((g_110 = (g_34 != g_17.f0)) <= (l_126 = (g_125 = (((((*p_73) |= ((void*)0 != g_78[(l_94 + 5)])) >= (l_123 != l_107)) != 0x829BAEFF72AEA97FLL) != g_19))))), g_85))), 4))) , (*l_107))));
                (*p_75) = (*p_75);
                (*p_73) &= ((((safe_rshift_func_int32_t_s_s((l_132 , ((~(g_113 <= (((g_17.f0 <= ((*l_140) |= ((((*l_84) = (((*l_107) != (l_134 ^ ((g_110 = (l_135[1] , ((safe_mod_func_uint8_t_u_u(((((*l_138) = g_47) , (*p_75)) != &l_126), 0x3BL)) , 0xC806L))) >= g_124))) || (*l_107))) ^ (*l_105)) & 1L))) , 0L) >= l_111))) >= l_141)), l_129)) | g_128) & g_34) , 0xBEB2AB5FL);
                for (l_111 = 0; (l_111 <= 0); l_111 += 1)
                { /* block id: 46 */
                    uint64_t l_142 = 18446744073709551615UL;
                    int32_t l_148 = 7L;
                    for (l_141 = 0; (l_141 <= 0); l_141 += 1)
                    { /* block id: 49 */
                        int i, j, k;
                        --l_142;
                        if (g_35[l_141][l_141][(l_94 + 3)])
                            break;
                        return l_145;
                    }
                    for (l_142 = 0; (l_142 <= 0); l_142 += 1)
                    { /* block id: 56 */
                        int32_t l_149 = 0x7407BC3BL;
                        l_148 ^= (safe_rshift_func_int32_t_s_u(l_134, 14));
                        if (l_149)
                            continue;
                    }
                }
            }
            (*p_75) = (*p_75);
        }
        else
        { /* block id: 63 */
            const int32_t *l_151 = &l_94;
            int64_t l_165 = 0x87A65D3118DE828DLL;
            int32_t l_196 = 0x3AD31088L;
            int32_t l_197 = 4L;
            int32_t l_207 = 0x3E813428L;
            int32_t l_211 = 0x4C4DBC68L;
            int32_t l_215 = 0xAF54D6FEL;
            int32_t l_216 = 0xB3499E89L;
            int32_t l_221 = (-10L);
            int32_t l_226[3];
            int32_t l_249[4] = {(-1L),(-1L),(-1L),(-1L)};
            int i;
            for (i = 0; i < 3; i++)
                l_226[i] = 0x56E5C899L;
            for (g_43 = 1; (g_43 <= 5); g_43 += 1)
            { /* block id: 66 */
                uint8_t l_164 = 0x9EL;
                int32_t l_189 = 0xE7582C1FL;
                uint64_t l_193 = 0x09790A851268E6D0LL;
                int32_t l_201 = 0x264B272FL;
                int32_t l_217 = 0L;
                int32_t l_219[5][1][7] = {{{(-5L),(-1L),(-5L),(-1L),(-5L),(-1L),(-5L)}},{{0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L}},{{(-5L),(-1L),(-5L),(-1L),(-5L),(-1L),(-5L)}},{{0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L,0x778F2E37L}},{{(-5L),(-1L),(-5L),(-1L),(-5L),(-1L),(-5L)}}};
                uint64_t l_234 = 18446744073709551609UL;
                int32_t **l_281 = &g_79;
                uint8_t l_298 = 0xEFL;
                int i, j, k;
                for (g_125 = 0; (g_125 <= 5); g_125 += 1)
                { /* block id: 69 */
                    int8_t l_163 = 1L;
                    int32_t l_194 = 6L;
                    int8_t l_195 = 0x1AL;
                    int32_t l_198 = 0L;
                    int32_t l_200 = 0x6DC195B6L;
                    int32_t l_205 = (-1L);
                    int32_t l_210[2][2][6];
                    int16_t l_228[10] = {0L,0L,(-1L),0L,0L,(-1L),0L,0L,(-1L),0L};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 2; j++)
                        {
                            for (k = 0; k < 6; k++)
                                l_210[i][j][k] = 0xD7BDDD33L;
                        }
                    }
                    for (l_99 = 0; (l_99 <= 5); l_99 += 1)
                    { /* block id: 72 */
                        int32_t **l_150[7] = {&l_105,&l_105,&l_105,&l_105,&l_105,&l_105,&l_105};
                        int i;
                        return &g_79;
                    }
                    for (g_110 = 1; (g_110 <= 5); g_110 += 1)
                    { /* block id: 77 */
                        l_151 = (*p_75);
                    }
                    for (g_47 = 0; (g_47 <= 0); g_47 += 1)
                    { /* block id: 82 */
                        uint16_t l_152 = 1UL;
                        uint8_t *l_158 = &l_99;
                        uint64_t *l_160 = &g_19;
                        int i, j, k;
                        l_152 |= 0L;
                        if (l_152)
                            continue;
                        (*p_73) = ((l_152 , ((safe_rshift_func_int8_t_s_s(((g_17 , (g_35[g_47][g_47][g_47] = (((safe_unary_minus_func_int64_t_s((((((safe_sub_func_uint8_t_u_u(((*l_158) = 0UL), ((*l_84) = (+((*l_160) = 18446744073709551615UL))))) , ((0xB73EL == (safe_mod_func_int16_t_s_s((g_17.f0 | 0UL), (g_85 || (*l_107))))) < 0xCAL)) != l_163) <= (*p_73)) >= 0x99L))) ^ l_163) <= l_164))) , g_124), l_163)) != l_165)) & l_163);
                    }
                    for (l_132 = 0; (l_132 <= 5); l_132 += 1)
                    { /* block id: 93 */
                        int8_t l_168 = 0L;
                        int64_t *l_181 = &l_135[7];
                        int64_t *l_190 = &l_165;
                        int32_t l_199 = 0x902AB11CL;
                        int32_t l_202 = 0x9709B9CAL;
                        int32_t l_206 = 5L;
                        int32_t l_208 = 0x979D5404L;
                        int32_t l_213 = 0x49CAE6DAL;
                        int32_t l_214 = 0x0C0894DAL;
                        int32_t l_220 = 0L;
                        int8_t l_222 = 0xA2L;
                        int32_t l_223 = 0x6C0336B4L;
                        int32_t l_225 = (-7L);
                        int32_t l_229 = 0x5DE3B2CCL;
                        int32_t l_230 = 1L;
                        int32_t l_231 = 0xA4876073L;
                        int32_t l_232 = 0x8006A75EL;
                        int32_t l_233 = (-2L);
                        int i, j;
                        (*p_73) |= ((safe_add_func_uint64_t_u_u(l_168, (safe_rshift_func_uint32_t_u_s(((!(~0x675BL)) ^ ((l_164 > (safe_lshift_func_int16_t_s_u(((safe_sub_func_uint8_t_u_u(((l_164 < (safe_sub_func_int8_t_s_s((safe_lshift_func_int64_t_s_u((((((((*l_181) = 0L) > (l_164 , (safe_sub_func_uint64_t_u_u(((((*l_190) |= (((+(((*l_107) < ((*l_105) = (((safe_mul_func_int32_t_s_s((l_189 = (l_163 || ((safe_mod_func_uint16_t_u_u((&p_75 == &p_74), g_35[0][0][1])) < (*l_105)))), l_168)) || 0x4E480967B42B6AA6LL) || 4L))) , g_34)) != g_35[0][0][0]) > g_2)) , (void*)0) != g_191), g_35[0][0][2])))) , 4294967286UL) ^ l_163) < (*g_79)) > l_168), 46)), 0x80L))) < g_47), l_193)) | l_163), 9))) && l_168)), 17)))) != 1L);
                        ++l_234;
                    }
                }
                for (l_99 = 1; (l_99 <= 5); l_99 += 1)
                { /* block id: 104 */
                    int32_t ****l_242 = (void*)0;
                    int32_t ****l_243 = &l_106[2];
                    int64_t *l_245 = (void*)0;
                    int64_t *l_246[4];
                    uint32_t l_265 = 0x4AD25D55L;
                    uint16_t l_270 = 0x97DFL;
                    int16_t *l_280 = &g_35[1][0][3];
                    int32_t l_289 = 1L;
                    int16_t l_290 = 0xAD1BL;
                    int16_t l_291 = 0x9AC7L;
                    int8_t l_292 = 0xBBL;
                    int32_t l_293 = 0x79847286L;
                    int32_t l_294 = 0x3283B7F0L;
                    int32_t l_295 = 0x8AD08AB2L;
                    int32_t l_296 = 0x78AED668L;
                    int32_t l_297[8] = {0xD8817E3DL,9L,0xD8817E3DL,9L,0xD8817E3DL,9L,0xD8817E3DL,9L};
                    int i;
                    for (i = 0; i < 4; i++)
                        l_246[i] = &l_135[7];
                }
            }
        }
        for (l_218 = 0; (l_218 < 13); l_218 = safe_add_func_uint32_t_u_u(l_218, 1))
        { /* block id: 139 */
            uint8_t l_303 = 0UL;
            uint64_t *l_308 = &g_19;
            uint64_t *l_313 = (void*)0;
            uint64_t *l_314[9] = {(void*)0,&g_315[6],&g_315[6],(void*)0,(void*)0,(void*)0,&g_315[6],(void*)0,(void*)0};
            int32_t l_316 = 0L;
            int32_t l_329 = 0L;
            int i;
            if (l_303)
                break;
            l_329 = (safe_lshift_func_int16_t_s_u(0x0992L, (((*l_308)--) <= (g_125 , ((safe_mul_func_int8_t_s_s(((((l_316 = g_271[3][2]) , (void*)0) != (((~(safe_rshift_func_uint64_t_u_u((safe_rshift_func_uint32_t_u_u((safe_div_func_uint64_t_u_u(0UL, (safe_lshift_func_uint8_t_u_s((safe_mod_func_uint16_t_u_u(((((~(((l_303 >= (l_303 < g_2)) , &g_255[2][3]) == &l_257)) != 0xF4L) < l_303) & (*g_79)), 65533UL)), 7)))), 23)), 12))) == (*l_105)) , &p_73)) >= 255UL), g_271[1][3])) > g_271[3][1])))));
        }
        for (g_43 = 0; (g_43 <= 9); g_43 += 1)
        { /* block id: 147 */
            uint64_t *l_347 = (void*)0;
            int32_t l_361 = (-1L);
            int64_t *l_362 = &l_135[2];
            uint64_t *l_363[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int8_t *l_365 = &l_100;
            int64_t *l_385 = &g_386;
            uint64_t l_401 = 0x1CDC19C9E09A5F6FLL;
            int32_t **l_415 = &g_79;
            int32_t l_434[3];
            int i;
            for (i = 0; i < 3; i++)
                l_434[i] = 0x60A4EF2FL;
            for (l_94 = 3; (l_94 <= 9); l_94 += 1)
            { /* block id: 150 */
                union U0 ***l_332[10][4][6] = {{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}},{{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330},{&l_330,&l_330,&l_330,&l_330,&l_330,&l_330}}};
                int i, j, k;
                g_333 = l_330;
            }
            l_366 |= ((7UL == ((((((*p_73) = (((((*l_84) = g_35[0][0][3]) > ((254UL <= (((safe_mul_func_uint32_t_u_u((g_19 == g_315[3]), (safe_mod_func_uint8_t_u_u((((*l_365) = (safe_lshift_func_uint32_t_u_s((safe_sub_func_uint32_t_u_u((safe_div_func_uint32_t_u_u((g_128 == ((void*)0 == l_347)), (((safe_sub_func_int64_t_s_s((~((!(g_364 = (safe_rshift_func_uint8_t_u_u((safe_mul_func_int64_t_s_s(((*l_362) |= (((+((((0x53L > l_361) && 65531UL) && 0UL) | 0L)) || 0L) >= l_361)), (*g_191))), l_361)))) & (****l_256))), (*l_105))) == (***l_257)) | g_315[g_43]))), g_315[g_43])), (*p_73)))) == g_47), g_315[g_43])))) , (*l_105)) == 0x2BL)) , 8L)) || 0x356EL) <= 6UL)) < g_271[2][0]) , g_113) == 5L) >= 65535UL)) ^ l_361);
        }
        l_479++;
    }
    if (((safe_sub_func_uint32_t_u_u(((l_484 , l_123) == ((*l_487) = ((safe_lshift_func_int16_t_s_s(0xABECL, 5)) , l_123))), 0x08E3A866L)) , (*p_73)))
    { /* block id: 214 */
        uint64_t l_493 = 18446744073709551611UL;
        uint8_t *l_499 = &g_271[3][1];
        const union U0 *l_513[5][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_273,&l_273,&l_273,&l_273,&l_273,&l_273,&l_273,&l_273,&l_273},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_273,&l_273,&l_273,&l_273,&l_273,&l_273,&l_273,&l_273,&l_273},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
        const union U0 **l_512[4][5][4] = {{{&l_513[2][1],(void*)0,&l_513[1][1],&l_513[2][1]},{(void*)0,&l_513[1][6],&l_513[2][1],&l_513[2][8]},{&l_513[3][8],(void*)0,&l_513[2][1],&l_513[2][1]},{&l_513[3][3],&l_513[2][1],(void*)0,&l_513[2][1]},{&l_513[0][2],&l_513[2][1],&l_513[2][1],&l_513[3][4]}},{{&l_513[3][4],&l_513[2][4],&l_513[3][4],(void*)0},{&l_513[2][1],&l_513[3][3],&l_513[2][1],&l_513[2][1]},{&l_513[2][1],&l_513[1][1],&l_513[3][4],&l_513[3][4]},{&l_513[3][4],&l_513[2][1],&l_513[2][1],&l_513[3][8]},{&l_513[0][2],&l_513[2][1],(void*)0,&l_513[1][3]}},{{&l_513[3][3],(void*)0,&l_513[2][1],&l_513[2][1]},{&l_513[3][8],&l_513[3][8],&l_513[2][1],&l_513[2][4]},{(void*)0,&l_513[1][5],&l_513[1][1],&l_513[1][6]},{&l_513[2][1],&l_513[1][1],&l_513[2][1],&l_513[1][1]},{&l_513[1][1],&l_513[1][1],&l_513[2][8],&l_513[1][6]}},{{&l_513[1][1],&l_513[1][5],&l_513[1][3],&l_513[2][4]},{&l_513[0][0],&l_513[3][8],(void*)0,&l_513[2][1]},{&l_513[2][8],(void*)0,(void*)0,&l_513[1][3]},{&l_513[1][5],&l_513[2][1],&l_513[1][5],&l_513[3][8]},{&l_513[1][3],&l_513[2][1],&l_513[3][3],&l_513[3][4]}}};
        const union U0 ***l_511 = &l_512[3][4][1];
        int32_t l_515 = 0x5D1936D1L;
        int32_t **l_538[10][2][4] = {{{&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,(void*)0,&g_79}},{{&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79}},{{&g_79,&g_79,(void*)0,&g_79},{&g_79,&g_79,&g_79,&g_79}},{{&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,(void*)0,&g_79}},{{&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79}},{{&g_79,&g_79,(void*)0,&g_79},{&g_79,&g_79,&g_79,&g_79}},{{&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,(void*)0,&g_79}},{{&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79}},{{&g_79,&g_79,(void*)0,&g_79},{&g_79,&g_79,&g_79,&g_79}},{{&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,(void*)0,&g_79}}};
        int32_t l_604 = (-1L);
        int i, j, k;
        l_505 &= ((*p_73) = ((safe_mul_func_int16_t_s_s((safe_mul_func_uint32_t_u_u(((*l_488) |= ((l_493 ^ l_494[3][6]) || ((1UL < (((g_17.f0 & ((*l_499) = (safe_add_func_uint16_t_u_u(((*l_263) = (&g_333 != l_497)), l_498)))) > (safe_add_func_uint32_t_u_u(g_125, (safe_sub_func_int8_t_s_s(0L, l_504[1]))))) , l_493)) > 246UL))), (*p_73))), g_192[1][0][4])) != g_192[0][5][6]));
        for (g_34 = 3; (g_34 >= 0); g_34 -= 1)
        { /* block id: 222 */
            int32_t l_506[7];
            union U0 ***l_514 = &g_333;
            int32_t *l_518 = &l_366;
            int i;
            for (i = 0; i < 7; i++)
                l_506[i] = 0xB4F9C83AL;
            l_506[1] &= (**l_478);
            (*p_75) = (*p_75);
            for (g_47 = 2; (g_47 <= 6); g_47 += 1)
            { /* block id: 227 */
                int32_t **l_517 = &g_79;
                int i, j;
                if (((*p_73) = (safe_div_func_int64_t_s_s(l_494[(g_34 + 4)][g_47], (safe_mul_func_int32_t_s_s(((((1L > 4294967295UL) , l_511) != l_514) , (*p_73)), 0xDB119065L))))))
                { /* block id: 229 */
                    for (g_113 = 0; (g_113 <= 3); g_113 += 1)
                    { /* block id: 232 */
                        int32_t **l_516 = &g_79;
                        l_515 = (*p_73);
                        return l_516;
                    }
                    return l_517;
                }
                else
                { /* block id: 237 */
                    const int32_t ****l_529 = (void*)0;
                    const int32_t ****l_530 = &g_528[1];
                    uint32_t l_539 = 0x38E3FEAAL;
                    int i;
                    (*p_75) = l_518;
                    if ((*p_73))
                        continue;
                    if (((*l_518) & ((safe_add_func_int32_t_s_s((*g_79), ((safe_lshift_func_uint64_t_u_u((safe_add_func_int32_t_s_s((g_192[2][0][6] <= l_525[0][0]), (g_526 != (void*)0))), (g_192[3][4][2] > ((g_531 = ((*l_530) = g_528[0])) == ((safe_mod_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(((((((g_78[(g_47 + 1)] = l_517) == l_538[2][1][2]) ^ l_539) != (*l_518)) , (*l_518)) | 18446744073709551612UL), l_540)), (**g_532))) , l_541))))) ^ (*l_518)))) != (*l_518))))
                    { /* block id: 243 */
                        uint8_t l_544 = 0UL;
                        const union U0 l_545 = {-1L};
                        int64_t *l_555 = &l_494[3][6];
                        uint64_t l_558[10];
                        int i;
                        for (i = 0; i < 10; i++)
                            l_558[i] = 0xC139B30BE091F822LL;
                        (*l_518) |= (((safe_lshift_func_uint32_t_u_s(l_544, 3)) , ((g_192[3][5][7] != g_386) == (l_545 , (*g_79)))) ^ ((safe_mod_func_int16_t_s_s(((~0x98L) & ((**l_517) ^ (safe_rshift_func_uint8_t_u_u((**l_517), (safe_rshift_func_int64_t_s_s(((*l_555) ^= (safe_rshift_func_uint8_t_u_u((**l_517), g_386))), (*g_191))))))), (-2L))) && (**l_478)));
                        if ((**l_517))
                            continue;
                        (*p_73) ^= (safe_add_func_int64_t_s_s(l_558[7], ((-1L) == 0xA0L)));
                    }
                    else
                    { /* block id: 248 */
                        return g_78[(g_47 + 1)];
                    }
                }
                return l_538[2][1][2];
            }
            if (g_386)
                goto lbl_615;
        }
lbl_615:
        for (l_515 = 0; (l_515 <= 3); l_515 += 1)
        { /* block id: 257 */
            const union U0 **l_559[7][9][4] = {{{&l_513[4][8],&l_513[1][0],&l_513[2][1],&l_513[2][1]},{&l_513[2][1],&l_513[2][1],&l_513[2][3],&l_513[2][1]},{&l_513[2][1],&l_513[2][1],&l_513[4][2],&l_513[3][7]},{&l_513[2][1],&l_513[2][1],&l_513[2][1],&l_513[2][1]},{(void*)0,(void*)0,&l_513[2][1],&l_513[2][3]},{&l_513[2][1],&l_513[2][1],&l_513[2][1],&l_513[3][2]},{&l_513[3][7],&l_513[2][1],&l_513[2][1],&l_513[3][2]},{&l_513[2][3],&l_513[2][1],&l_513[1][0],&l_513[2][3]},{&l_513[2][1],(void*)0,(void*)0,&l_513[2][1]}},{{&l_513[3][7],&l_513[2][1],(void*)0,&l_513[3][7]},{&l_513[2][1],&l_513[2][1],&l_513[1][0],&l_513[2][1]},{&l_513[4][8],&l_513[2][1],(void*)0,&l_513[2][1]},{&l_513[3][7],&l_513[1][0],&l_513[2][3],(void*)0},{&l_513[1][0],&l_513[2][1],&l_513[2][1],&l_513[2][1]},{&l_513[2][1],&l_513[3][7],(void*)0,&l_513[2][1]},{&l_513[2][1],&l_513[2][1],&l_513[2][1],&l_513[2][1]},{&l_513[1][0],&l_513[2][1],&l_513[2][3],&l_513[3][0]},{&l_513[3][7],(void*)0,(void*)0,&l_513[3][2]}},{{&l_513[4][8],(void*)0,&l_513[1][0],&l_513[2][1]},{&l_513[2][1],(void*)0,(void*)0,(void*)0},{&l_513[3][7],&l_513[3][7],(void*)0,&l_513[3][7]},{&l_513[2][1],&l_513[2][1],&l_513[1][0],(void*)0},{&l_513[2][3],&l_513[2][1],&l_513[2][1],&l_513[1][0]},{&l_513[3][7],&l_513[2][1],&l_513[2][1],(void*)0},{&l_513[2][1],&l_513[2][1],&l_513[2][1],&l_513[3][7]},{(void*)0,&l_513[3][7],&l_513[2][1],(void*)0},{&l_513[2][1],(void*)0,&l_513[4][2],&l_513[2][1]}},{{&l_513[2][1],(void*)0,&l_513[2][3],&l_513[3][2]},{&l_513[2][1],(void*)0,&l_513[2][1],(void*)0},{&l_513[2][1],&l_513[2][1],(void*)0,&l_513[2][1]},{&l_513[2][1],(void*)0,(void*)0,&l_513[2][1]},{&l_513[2][5],&l_513[3][0],(void*)0,&l_513[2][1]},{&l_513[2][1],&l_513[2][3],(void*)0,(void*)0},{&l_513[2][1],&l_513[2][1],&l_513[2][1],&l_513[2][1]},{&l_513[2][1],&l_513[2][1],&l_513[4][2],(void*)0},{&l_513[2][1],&l_513[2][3],&l_513[3][7],&l_513[2][1]}},{{&l_513[2][1],&l_513[2][5],&l_513[2][5],&l_513[2][1]},{(void*)0,(void*)0,(void*)0,&l_513[2][1]},{&l_513[2][1],&l_513[2][1],&l_513[2][1],&l_513[1][0]},{&l_513[2][1],&l_513[2][1],&l_513[2][1],&l_513[1][0]},{&l_513[4][2],&l_513[2][1],&l_513[2][1],&l_513[2][1]},{&l_513[2][1],(void*)0,&l_513[3][7],&l_513[2][1]},{&l_513[3][0],&l_513[2][5],(void*)0,&l_513[2][1]},{(void*)0,&l_513[2][3],&l_513[2][1],(void*)0},{&l_513[2][1],&l_513[2][1],(void*)0,&l_513[2][1]}},{{&l_513[2][1],&l_513[2][1],&l_513[4][2],(void*)0},{&l_513[2][1],&l_513[2][3],(void*)0,&l_513[2][1]},{&l_513[2][1],&l_513[3][0],&l_513[3][2],&l_513[2][1]},{&l_513[2][1],(void*)0,(void*)0,&l_513[2][1]},{&l_513[2][1],&l_513[2][1],&l_513[4][2],(void*)0},{&l_513[2][1],&l_513[2][3],(void*)0,&l_513[1][0]},{&l_513[2][1],(void*)0,&l_513[2][1],&l_513[2][1]},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_513[3][0],&l_513[3][0],&l_513[3][7],&l_513[2][1]}},{{&l_513[2][1],&l_513[2][1],&l_513[2][1],(void*)0},{&l_513[4][2],&l_513[2][1],&l_513[2][1],&l_513[2][1]},{&l_513[2][1],&l_513[2][1],&l_513[2][1],(void*)0},{&l_513[2][1],&l_513[2][1],(void*)0,&l_513[2][1]},{(void*)0,&l_513[3][0],&l_513[2][5],(void*)0},{&l_513[2][1],(void*)0,&l_513[3][7],&l_513[2][1]},{&l_513[2][1],(void*)0,&l_513[4][2],&l_513[1][0]},{&l_513[2][1],&l_513[2][3],&l_513[2][1],(void*)0},{&l_513[2][1],&l_513[2][1],(void*)0,&l_513[2][1]}}};
            uint64_t l_566 = 0x5BC856CF5147CA6FLL;
            union U0 **l_594 = &g_334;
            int32_t l_607 = 0L;
            int32_t l_609 = 0L;
            int32_t l_610 = 0x830EB65CL;
            int32_t l_611 = 0x8DE159C3L;
            uint16_t l_612[6][5][1];
            int i, j, k;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 5; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_612[i][j][k] = 0x15CFL;
                }
            }
            l_559[5][8][3] = (*l_511);
            for (g_34 = 0; (g_34 <= 3); g_34 += 1)
            { /* block id: 261 */
                const int32_t l_567 = (-1L);
                int16_t *l_580 = &g_35[1][0][3];
                int64_t *l_582 = &g_386;
                union U0 **l_593[1];
                int32_t l_602 = 0xF90CC803L;
                int32_t l_606 = 0xA933F2E6L;
                int32_t l_608[6] = {1L,0x1E0CE11FL,0x1E0CE11FL,1L,0x1E0CE11FL,0x1E0CE11FL};
                int i, j;
                for (i = 0; i < 1; i++)
                    l_593[i] = &g_334;
                (*p_73) &= ((++g_271[l_515][l_515]) | ((*g_334) , (safe_add_func_int32_t_s_s(((safe_mul_func_uint8_t_u_u(l_566, (l_567 && (((safe_mod_func_int32_t_s_s(((((((*l_582) = (0x69A90F6AL | ((safe_div_func_int16_t_s_s((safe_div_func_uint32_t_u_u(((safe_mod_func_uint64_t_u_u((g_315[7] < 0x97E50DF6L), ((safe_rshift_func_int8_t_s_u(((**l_478) == ((*l_580) = (safe_lshift_func_uint8_t_u_s(g_35[1][0][2], 0)))), 3)) && ((l_567 , &g_334) != (void*)0)))) && l_566), l_567)), l_567)) <= l_581))) , g_113) && g_128) > 18446744073709551615UL) ^ (**l_478)), (-10L))) >= 0x67L) ^ l_567)))) != (**l_478)), l_566))));
                for (l_244 = 3; (l_244 >= 0); l_244 -= 1)
                { /* block id: 268 */
                    uint64_t l_588 = 0x6BA0238C63894B36LL;
                    int64_t *l_600 = &l_505;
                    int32_t l_603 = 1L;
                    int32_t l_605[3][10] = {{2L,0L,8L,0x7506F4C6L,8L,0L,2L,0L,8L,0x7506F4C6L},{8L,0x7506F4C6L,8L,0L,2L,0L,8L,0x7506F4C6L,8L,0L},{2L,0x7506F4C6L,(-1L),0x7506F4C6L,2L,3L,2L,0x7506F4C6L,(-1L),0x7506F4C6L}};
                    int i, j;
                    (*p_73) ^= (safe_mul_func_uint64_t_u_u((((((**l_478) != (safe_add_func_int64_t_s_s((!((((*l_84) = g_125) , l_588) >= (safe_mod_func_int8_t_s_s(g_315[6], (safe_lshift_func_int8_t_s_s((l_593[0] != (l_594 = (void*)0)), 2)))))), (g_601 = ((*l_600) |= (safe_sub_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(l_588, ((*l_582) = l_567))), (((*g_334) , 5UL) && g_599)))))))) >= g_599) != g_599) > g_35[1][0][3]), (*g_191)));
                    if (l_566)
                        continue;
                    l_612[1][0][0]--;
                }
            }
        }
        for (g_85 = 0; (g_85 == 17); g_85++)
        { /* block id: 283 */
            (*p_73) = 8L;
        }
    }
    else
    { /* block id: 286 */
        uint32_t l_618 = 1UL;
        int32_t **l_623 = &g_79;
        int64_t *l_632 = &l_494[0][2];
        uint32_t **l_681 = (void*)0;
        int64_t l_682 = 1L;
        int32_t *l_697 = &g_599;
        const int32_t l_748 = 1L;
        int32_t l_808 = 9L;
        int32_t l_809 = 9L;
        int32_t l_812[10];
        union U0 l_869 = {4L};
        int16_t l_930[3];
        int i;
        for (i = 0; i < 10; i++)
            l_812[i] = 0xC79ED240L;
        for (i = 0; i < 3; i++)
            l_930[i] = 0x002DL;
        if (((**l_478) | (l_618 > (safe_mul_func_int32_t_s_s((((((l_618 , (((safe_sub_func_uint32_t_u_u(l_618, 0x936F9060L)) , l_623) != (((**l_623) , (safe_mod_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s(((((*l_632) = ((((0xC889L | ((safe_add_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((g_47 , (**l_478)), 0xCCL)), g_110)) & 0xBEFCL)) | 0x92CEL) | (-1L)) > 0xDBL)) >= (**l_623)) == (**l_623)), g_47)), 0xBCECL))) , (void*)0))) & (**l_478)) != (**l_623)) != (**l_623)) & g_192[3][4][2]), g_386)))))
        { /* block id: 288 */
            int64_t l_638 = 1L;
            int32_t l_687 = 0L;
            union U0 *l_727 = &l_273;
            uint16_t l_728 = 0xF9A1L;
            int32_t **l_737 = &g_79;
            union U0 ****l_753 = &l_497;
            for (g_34 = (-2); (g_34 != 26); g_34 = safe_add_func_uint64_t_u_u(g_34, 7))
            { /* block id: 291 */
                uint16_t l_641[5][3] = {{0x2AA4L,0x99CBL,0x2AA4L},{1UL,1UL,1UL},{0x2AA4L,0x99CBL,0x2AA4L},{1UL,1UL,1UL},{0x2AA4L,0x99CBL,0x2AA4L}};
                union U0 l_646 = {0xED7D0881L};
                uint8_t l_663 = 0x01L;
                int32_t l_664 = 0x991EE1F9L;
                int32_t l_686 = (-1L);
                union U0 **l_724 = &g_334;
                uint32_t l_741 = 0xE1040A08L;
                uint64_t *l_758 = (void*)0;
                uint64_t *l_759 = &g_19;
                int i, j;
                for (l_366 = 0; (l_366 <= 15); ++l_366)
                { /* block id: 294 */
                    int32_t *l_637 = &l_277[5][3];
                    const int64_t l_644[10][8][1] = {{{0L},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)}},{{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL}},{{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL}},{{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)}},{{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL}},{{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL}},{{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)}},{{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL}},{{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL}},{{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)},{0xC741F068CB4128F1LL},{0xC741F068CB4128F1LL},{(-10L)}}};
                    int16_t *l_661[2];
                    int32_t l_662 = 0xF9ECFC87L;
                    const uint8_t *l_723 = &g_271[3][1];
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_661[i] = (void*)0;
                    (*p_73) ^= ((l_638 = ((*l_637) = g_192[3][4][2])) , (safe_div_func_uint32_t_u_u(((*l_123) = (l_641[3][1] ^ ((*g_191) , (((safe_mul_func_int8_t_s_s(l_644[2][7][0], l_638)) , (+(((*l_84) |= l_644[9][3][0]) && (l_646 , l_646.f0)))) ^ (l_641[3][1] <= g_601))))), g_315[0])));
                    if (((((((*l_84) = ((0xBFL || (safe_lshift_func_uint16_t_u_s(0x4BCDL, 10))) != ((-9L) || l_638))) != (**l_623)) || ((l_664 = (safe_mul_func_int16_t_s_s((safe_div_func_uint32_t_u_u((((((((~(**l_623)) >= ((*p_73) = (l_663 |= (safe_mul_func_uint8_t_u_u(((safe_div_func_int16_t_s_s((l_662 = (safe_rshift_func_int16_t_s_s(((~((**l_623) & g_17.f0)) <= ((((((**l_623) || g_125) , 0x9E007195EB1FE6BALL) >= g_128) == g_35[0][0][2]) == g_364)), 12))), (**l_623))) != 0x4347D6CCL), 0x64L))))) == l_638) & (-10L)) & (**l_478)) > g_315[6]) , l_644[2][7][0]), 1UL)), (**l_478)))) & (*g_191))) ^ (-1L)) , l_638))
                    { /* block id: 305 */
                        uint32_t l_683 = 0x33CE1135L;
                        (*p_73) = ((**l_623) == ((safe_mul_func_uint16_t_u_u(((safe_unary_minus_func_uint32_t_u((safe_mod_func_uint32_t_u_u(l_663, ((safe_div_func_int16_t_s_s((+((**l_623) && (**l_478))), ((((g_17.f0 , (safe_add_func_uint32_t_u_u(((safe_add_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u((((((*l_84) = ((safe_sub_func_uint16_t_u_u(((void*)0 == l_681), (g_35[1][0][3] ^= (((((((*l_637) = l_646.f0) , 0x51E9F86DL) , l_682) < (*g_191)) , &g_85) == (void*)0)))) && (**l_623))) && (**l_478)) > g_128) != 0xCDB34E83L), (-3L))), l_638)) ^ (*g_191)), (**l_623)))) , l_683) < 0x2924L) ^ l_683))) ^ (*g_191)))))) & (**l_623)), 0x6086L)) != g_2));
                        l_686 &= (((safe_add_func_int32_t_s_s(((*p_73) &= (**l_478)), 0xE253826EL)) != g_2) , (p_73 != p_73));
                    }
                    else
                    { /* block id: 312 */
                        uint32_t l_688 = 1UL;
                        int32_t l_725 = 0xEE5E3AB1L;
                        union U0 **l_729 = (void*)0;
                        union U0 **l_730 = &l_727;
                        l_688--;
                        (*p_73) = (((safe_mod_func_uint64_t_u_u((((((*l_541) = &p_73) == &p_73) ^ ((**g_532) | g_271[2][0])) & (((((safe_lshift_func_uint8_t_u_u((l_663 & (g_601 , ((g_315[6] > (safe_rshift_func_int16_t_s_u(((void*)0 != l_697), 1))) ^ 0x78ED1B4A64098D41LL))), 2)) > g_2) , l_688) && 7L) ^ l_644[2][7][0])), 0x2469FCE6EF1B5420LL)) , g_17.f0) > 0xD4D4C971L);
                        l_728 |= ((safe_div_func_uint64_t_u_u((safe_mul_func_int64_t_s_s((safe_sub_func_uint64_t_u_u((((safe_mod_func_int32_t_s_s(((**l_623) | ((*l_84) = ((void*)0 == &l_581))), (((((safe_div_func_int32_t_s_s((safe_mul_func_int32_t_s_s((safe_add_func_uint64_t_u_u((safe_mod_func_uint32_t_u_u((safe_mod_func_int8_t_s_s(((+(safe_div_func_uint64_t_u_u((l_725 = ((l_641[3][1] >= ((safe_sub_func_uint8_t_u_u(l_688, (((safe_add_func_int64_t_s_s(((*l_632) = l_688), ((((((*l_697) = (((l_723 != &g_271[2][0]) == (((((*p_73) = (((*l_541) = &p_73) != &p_73)) <= l_686) , (void*)0) == l_724)) & g_124)) , 18446744073709551615UL) >= (*g_191)) , l_644[8][4][0]) , l_663))) || 6L) > 1UL))) ^ 8UL)) != l_687)), g_315[6]))) == 4294967289UL), 6L)), 0x55C4CBB6L)), 0xD080BBCE3C472CFFLL)), (**l_478))), l_726)) , (void*)0) == l_727) , l_663) ^ l_688))) ^ g_113) , g_47), l_664)), g_386)), (*g_191))) >= 0x896FB88FL);
                        (*l_730) = ((*l_724) = l_727);
                    }
                    for (l_682 = 0; (l_682 > 26); l_682 = safe_add_func_int16_t_s_s(l_682, 7))
                    { /* block id: 328 */
                        union U0 ****l_734 = &l_497;
                        union U0 *****l_733 = &l_734;
                        union U0 *****l_735[4][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}};
                        int i, j;
                        l_736 = ((*l_733) = (void*)0);
                        return l_737;
                    }
                    for (g_386 = 0; (g_386 <= 8); g_386 += 1)
                    { /* block id: 335 */
                        union U0 *l_738 = &l_646;
                        int i;
                        l_738 = ((*l_724) = (g_43 , l_738));
                        if ((*p_73))
                            break;
                    }
                }
                if (l_641[3][0])
                    continue;
                l_687 = ((((**l_487) = (g_192[0][5][9] , (safe_div_func_int32_t_s_s(((*p_73) = l_741), (safe_mod_func_uint32_t_u_u(4294967293UL, (safe_mul_func_uint8_t_u_u(((((*l_759) = (((((safe_sub_func_uint64_t_u_u((l_748 , ((safe_mod_func_int32_t_s_s(((safe_mul_func_int64_t_s_s(((*l_632) = (((g_315[6] = (l_753 != (void*)0)) , (safe_rshift_func_int64_t_s_u(((*g_191) || g_128), 25))) || (safe_add_func_int32_t_s_s(l_664, 0x2E6372BCL)))), (*g_191))) , (**l_623)), 0x0B73B7E4L)) ^ 0x3DL)), 18446744073709551608UL)) | (-2L)) <= 0x42C7DBEEL) | l_641[0][2]) ^ (**l_737))) , 0xB676L) != (**l_623)), (**l_737))))))))) >= (**l_737)) == 0UL);
            }
        }
        else
        { /* block id: 349 */
            uint64_t l_776[10][5][5] = {{{7UL,4UL,1UL,0xD86AD1B08B9E4FC7LL,0x102320FF821715CCLL},{4UL,0x2DAE4D58451BFBA4LL,0x500A03960CD945D7LL,0xA8C136DD21BF300DLL,0UL},{0x37C425557FEC03F8LL,0xA8C136DD21BF300DLL,18446744073709551614UL,18446744073709551614UL,0xA8C136DD21BF300DLL},{0x2DAE4D58451BFBA4LL,18446744073709551615UL,7UL,4UL,0x33A60B5DF8D1D72ALL},{18446744073709551614UL,0x37C425557FEC03F8LL,0x2DAE4D58451BFBA4LL,1UL,0xF63F5869917F1A26LL}},{{0UL,0x64F5B71A3ED85A61LL,0xFA89F8C43EE6B8ACLL,0x638F0163427FE0C4LL,0x8D152FBC3C5C9BD6LL},{18446744073709551614UL,6UL,0x1A2B71F6391DD8CCLL,0x500A03960CD945D7LL,0xD86AD1B08B9E4FC7LL},{0x2DAE4D58451BFBA4LL,0xFA89F8C43EE6B8ACLL,0UL,0xF63F5869917F1A26LL,6UL},{0x37C425557FEC03F8LL,0x5D74093426D2453CLL,0UL,9UL,0xCE37C192204A890ELL},{4UL,0x500A03960CD945D7LL,9UL,0xCE37C192204A890ELL,7UL}},{{7UL,0x500A03960CD945D7LL,0x69D9BCE55E60DCD6LL,0xDB9E7606FBE607D0LL,0x9E3464F74D99E46FLL},{0xDB9E7606FBE607D0LL,0x5D74093426D2453CLL,0xCE37C192204A890ELL,4UL,4UL},{0xF63F5869917F1A26LL,0xFA89F8C43EE6B8ACLL,0xF63F5869917F1A26LL,1UL,7UL},{0xECF70293F15C79DELL,6UL,0x64F5B71A3ED85A61LL,0x33A60B5DF8D1D72ALL,9UL},{0UL,0x64F5B71A3ED85A61LL,0x9E3464F74D99E46FLL,0UL,0x2DAE4D58451BFBA4LL}},{{1UL,0x37C425557FEC03F8LL,0x64F5B71A3ED85A61LL,9UL,0UL},{9UL,18446744073709551615UL,0xF63F5869917F1A26LL,0xECF70293F15C79DELL,0x2693EBBCFF2B95B0LL},{18446744073709551615UL,0xA8C136DD21BF300DLL,0xCE37C192204A890ELL,0x41A88C36D466781ELL,1UL},{0x5D74093426D2453CLL,0x2DAE4D58451BFBA4LL,0x69D9BCE55E60DCD6LL,0UL,3UL},{4UL,4UL,3UL,1UL,0x638F0163427FE0C4LL}},{{0x37C425557FEC03F8LL,0x69D9BCE55E60DCD6LL,1UL,0xFA89F8C43EE6B8ACLL,0x9E3464F74D99E46FLL},{1UL,0xDB9E7606FBE607D0LL,0x2693EBBCFF2B95B0LL,0xDB9E7606FBE607D0LL,1UL},{0x500A03960CD945D7LL,0x41A88C36D466781ELL,0UL,3UL,2UL},{0x69D9BCE55E60DCD6LL,1UL,0x2DAE4D58451BFBA4LL,18446744073709551615UL,0xDB9E7606FBE607D0LL},{0x2DAE4D58451BFBA4LL,4UL,9UL,0x41A88C36D466781ELL,2UL}},{{18446744073709551615UL,18446744073709551615UL,7UL,7UL,1UL},{2UL,0xA8C136DD21BF300DLL,4UL,1UL,0x9E3464F74D99E46FLL},{1UL,4UL,0x9E3464F74D99E46FLL,4UL,0x638F0163427FE0C4LL},{0UL,0UL,7UL,0x500A03960CD945D7LL,0x69D9BCE55E60DCD6LL},{0UL,0xF63F5869917F1A26LL,0xCE37C192204A890ELL,0x64F5B71A3ED85A61LL,7UL}},{{1UL,3UL,6UL,18446744073709551615UL,6UL},{2UL,2UL,0xD86AD1B08B9E4FC7LL,0x9E3464F74D99E46FLL,1UL},{18446744073709551615UL,0UL,0x8D152FBC3C5C9BD6LL,0xD86AD1B08B9E4FC7LL,9UL},{0x2DAE4D58451BFBA4LL,1UL,0xF63F5869917F1A26LL,0x638F0163427FE0C4LL,3UL},{0x69D9BCE55E60DCD6LL,0UL,0x33A60B5DF8D1D72ALL,0xA8C136DD21BF300DLL,4UL}},{{0x500A03960CD945D7LL,2UL,0xA8C136DD21BF300DLL,4UL,1UL},{1UL,3UL,0UL,0xCE37C192204A890ELL,0xECF70293F15C79DELL},{0x37C425557FEC03F8LL,0xF63F5869917F1A26LL,0x102320FF821715CCLL,18446744073709551615UL,7UL},{9UL,0UL,0x102320FF821715CCLL,0UL,0x500A03960CD945D7LL},{0UL,4UL,0UL,9UL,1UL}},{{4UL,0xA8C136DD21BF300DLL,0xA8C136DD21BF300DLL,4UL,18446744073709551615UL},{0x64F5B71A3ED85A61LL,18446744073709551615UL,0x33A60B5DF8D1D72ALL,0x2693EBBCFF2B95B0LL,0UL},{7UL,4UL,0xF63F5869917F1A26LL,0x86F321416D072919LL,18446744073709551615UL},{1UL,1UL,0x8D152FBC3C5C9BD6LL,0x2693EBBCFF2B95B0LL,0x41A88C36D466781ELL},{0xDB9E7606FBE607D0LL,0x41A88C36D466781ELL,0xD86AD1B08B9E4FC7LL,4UL,0xCE37C192204A890ELL}},{{18446744073709551615UL,0xDB9E7606FBE607D0LL,6UL,9UL,18446744073709551615UL},{4UL,0x69D9BCE55E60DCD6LL,0xCE37C192204A890ELL,0UL,18446744073709551614UL},{7UL,0x33A60B5DF8D1D72ALL,7UL,18446744073709551615UL,18446744073709551614UL},{0xA8C136DD21BF300DLL,9UL,0x9E3464F74D99E46FLL,0xCE37C192204A890ELL,18446744073709551615UL},{0x5D74093426D2453CLL,0xCE37C192204A890ELL,4UL,4UL,0xCE37C192204A890ELL}}};
            int32_t l_810 = 4L;
            int32_t l_814 = (-1L);
            int32_t l_821 = (-1L);
            int32_t l_822 = (-4L);
            int32_t l_823 = 0xA5462884L;
            int32_t l_824 = 1L;
            int32_t l_825 = 0xD0E76A88L;
            int32_t l_826 = 0x23C91F3CL;
            int32_t l_827 = 0x15AA9827L;
            int32_t l_828 = 0xF377F6A5L;
            int32_t l_830 = (-8L);
            int32_t l_831 = 0x511D12C4L;
            int32_t l_832 = 1L;
            int32_t l_833 = 0x6239D26EL;
            int32_t l_834 = 0xAFAA060CL;
            int32_t l_835 = 0xCF74FC17L;
            int32_t l_836 = (-8L);
            int32_t l_838 = 0xED97B0D6L;
            int32_t l_839 = 0L;
            int32_t l_840[4];
            const int32_t ***l_855 = &g_532;
            uint64_t l_865 = 0x9A61B260583DA515LL;
            uint8_t l_893 = 0x99L;
            int32_t l_911 = (-3L);
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_840[i] = 0x4F995287L;
            for (g_601 = 1; (g_601 <= 8); g_601 += 1)
            { /* block id: 352 */
                int32_t l_811 = 0x1F7B33DEL;
                int32_t l_819[10][4][6] = {{{1L,(-1L),0x0B42305BL,(-1L),1L,(-1L)},{7L,0xCE56D51EL,(-1L),0x2D752CF9L,0x1D4D91A1L,0x309B8A6EL},{0x23E39490L,0x950151E2L,1L,0xCE56D51EL,0x36569E6BL,0x309B8A6EL},{0L,(-1L),(-1L),(-10L),(-4L),(-1L)}},{{0x36569E6BL,(-1L),0x0B42305BL,(-6L),0x281CAF26L,0L},{(-1L),9L,(-1L),0x36569E6BL,0x02E4BB20L,0L},{1L,0x4EC06B34L,1L,(-10L),1L,(-1L)},{(-6L),0x580C7F3DL,(-1L),0L,1L,0x93C1D0CAL}},{{0xAC57EB48L,0L,1L,0x6D38B405L,0x4EC06B34L,(-1L)},{6L,0xD8C15738L,0x950151E2L,0x950151E2L,0xD8C15738L,6L},{0xEA78045BL,0x23E39490L,(-1L),0x309B8A6EL,(-3L),1L},{(-1L),0xED55B1DBL,(-4L),(-6L),0L,0x653D6B9AL}},{{(-1L),0x580C7F3DL,(-6L),0x309B8A6EL,7L,0x1F2C36DDL},{0xEA78045BL,(-3L),0x4EC06B34L,0x950151E2L,(-4L),0L},{6L,1L,(-1L),0x6D38B405L,(-1L),0x23E39490L},{0xAC57EB48L,0x2D752CF9L,0x59C769DAL,0L,9L,1L}},{{(-6L),7L,6L,(-10L),(-1L),0x0B42305BL},{(-6L),(-5L),0x93C1D0CAL,(-4L),7L,(-1L)},{0L,0xEA78045BL,1L,(-10L),1L,0xEA78045BL},{(-4L),(-1L),3L,0x6D38B405L,(-5L),0L}},{{0x36569E6BL,0x23E39490L,0x950151E2L,1L,0xCE56D51EL,0x36569E6BL},{0x93C1D0CAL,0x23E39490L,6L,8L,(-5L),(-1L)},{(-1L),(-1L),(-10L),0xED55B1DBL,1L,0x653D6B9AL},{1L,0xEA78045BL,0xED55B1DBL,0x1D4D91A1L,7L,0L}},{{1L,(-5L),0x1335ED01L,0x950151E2L,(-1L),7L},{0x36569E6BL,7L,(-1L),0x23E39490L,9L,0xCE56D51EL},{(-10L),0x2D752CF9L,(-4L),(-1L),(-1L),(-1L)},{(-6L),1L,0x0B42305BL,(-4L),(-4L),0x0B42305BL}},{{(-3L),(-3L),1L,0x59C769DAL,7L,(-1L)},{0xED55B1DBL,0x580C7F3DL,3L,(-10L),0L,1L},{0xAC57EB48L,0xED55B1DBL,3L,0x23E39490L,(-3L),(-1L)},{0x0B42305BL,0x23E39490L,1L,0x93C1D0CAL,0xD8C15738L,0x0B42305BL}},{{0x93C1D0CAL,0xD8C15738L,0x0B42305BL,0x309B8A6EL,0x4EC06B34L,(-1L)},{1L,0L,(-4L),(-1L),1L,0xCE56D51EL},{1L,0x580C7F3DL,(-1L),0x1D4D91A1L,1L,7L},{0xEA78045BL,0x4EC06B34L,0x1335ED01L,1L,0xAC57EB48L,0L}},{{0x0B42305BL,7L,0xED55B1DBL,0x653D6B9AL,(-1L),0x653D6B9AL},{(-10L),7L,(-10L),0L,0x309B8A6EL,(-1L)},{0x2D752CF9L,1L,(-10L),0xD8C15738L,0x653D6B9AL,(-4L)},{(-1L),(-1L),(-5L),0xD8C15738L,1L,(-6L)}}};
                int64_t l_820 = 0xE86960312B8C656ALL;
                int32_t l_837 = (-1L);
                uint16_t l_841 = 65533UL;
                int16_t l_921 = 6L;
                int i, j, k;
                (**g_531) = (*p_75);
                (*p_73) |= (safe_rshift_func_int64_t_s_u(6L, 45));
                if ((*p_73))
                { /* block id: 355 */
                    int32_t l_789 = 1L;
                    int32_t **l_793 = &g_79;
                    int32_t l_813[5][7][7] = {{{0x59E2AC1CL,1L,0x4A30182EL,0xB48A7AE0L,1L,0xA29CCD9CL,0x4A30182EL},{(-1L),(-1L),1L,(-9L),4L,0x4F896AAAL,(-1L)},{0xEE5C9C1EL,(-1L),0x5C8E9E9BL,0x264DFACAL,0x36D3BA88L,0xBBF90371L,3L},{0L,3L,0x1BBE64BBL,(-10L),4L,1L,0xEF8F84E0L},{0xBBF90371L,1L,(-1L),(-1L),1L,1L,0x5C8E9E9BL},{5L,1L,4L,(-10L),(-1L),3L,(-1L)},{(-1L),0xBBF90371L,0x264DFACAL,0x264DFACAL,0xBBF90371L,(-1L),(-1L)}},{{0x6F1998E4L,0x4F896AAAL,4L,(-9L),(-1L),(-1L),0x1BBE64BBL},{0x0E45A78AL,0xA29CCD9CL,(-1L),0xB48A7AE0L,0x0E45A78AL,1L,1L},{0x27AFF483L,0x4F896AAAL,(-1L),1L,0x90E2AA30L,(-1L),0x90E2AA30L},{0x529679ACL,0xBBF90371L,0x5C8E9E9BL,(-3L),0xB48A7AE0L,0x581FFA7CL,2L},{0x5B583E63L,1L,(-1L),(-3L),0L,(-5L),0x65F09E67L},{0x0E45A78AL,1L,0x4A30182EL,0x581FFA7CL,(-1L),0x264DFACAL,2L},{(-1L),3L,0x90E2AA30L,0x25731F30L,0L,0x25731F30L,0x90E2AA30L}},{{(-1L),(-1L),0xA29CCD9CL,(-1L),(-5L),0x36D3BA88L,1L},{0xEF8F84E0L,(-1L),0L,0x4F896AAAL,(-10L),(-5L),0x1BBE64BBL},{0xBBF90371L,1L,3L,0x0E45A78AL,(-5L),(-8L),(-1L)},{(-1L),(-1L),0L,(-1L),0L,(-1L),(-1L)},{0xEE5C9C1EL,0x581FFA7CL,0xC971DA04L,0x4A30182EL,(-1L),(-6L),0x5C8E9E9BL},{0x1BBE64BBL,(-5L),(-10L),0x4F896AAAL,0L,(-1L),0xEF8F84E0L},{0x59E2AC1CL,0x264DFACAL,0xC971DA04L,0xEE5C9C1EL,0xB48A7AE0L,3L,3L}},{{0x90E2AA30L,0x25731F30L,0L,0x25731F30L,0x90E2AA30L,3L,(-1L)},{0xD9122800L,0x36D3BA88L,3L,(-8L),0x0E45A78AL,0x59E2AC1CL,0x4A30182EL},{0x65F09E67L,(-5L),0L,(-3L),(-1L),1L,0x5B583E63L},{0xD9122800L,(-8L),0xA29CCD9CL,0x529679ACL,0xBBF90371L,0x5C8E9E9BL,(-3L)},{0x90E2AA30L,(-1L),0x90E2AA30L,3L,1L,(-5L),0x90E2AA30L},{0xEE5C9C1EL,(-5L),0x264DFACAL,1L,0xD9122800L,0x59E2AC1CL,0x264DFACAL},{(-1L),0x25731F30L,0x6F1998E4L,1L,1L,(-5L),0xEF8F84E0L}},{{0x581FFA7CL,0xA29CCD9CL,(-3L),0xBBF90371L,0xBBF90371L,(-3L),0xA29CCD9CL},{0x08C2D8F2L,0xCE3C6DDEL,1L,0x4F896AAAL,0L,3L,0xB7B2F823L},{0x529679ACL,0xEE5C9C1EL,2L,0xA29CCD9CL,0xD9122800L,0xEE5C9C1EL,(-3L)},{0x4BFA36D4L,3L,1L,0x4F896AAAL,(-1L),0xCE3C6DDEL,0x65F09E67L},{0xB48A7AE0L,(-3L),0x5C8E9E9BL,0xBBF90371L,0x529679ACL,0xA29CCD9CL,(-8L)},{5L,(-5L),0L,1L,(-1L),0x25731F30L,1L},{0x36D3BA88L,0x59E2AC1CL,2L,1L,0x36D3BA88L,(-5L),(-1L)}}};
                    int16_t l_829 = 0x11DCL;
                    int i, j, k;
                    if (((*p_73) &= 0x3C6C84BDL))
                    { /* block id: 357 */
                        int64_t l_771 = 1L;
                        int16_t *l_787 = &g_35[1][0][3];
                        int32_t l_788 = 8L;
                        l_789 |= (l_788 &= (safe_lshift_func_int16_t_s_u(((*l_787) = (((safe_add_func_uint32_t_u_u(0x423EF83BL, (((+((l_771 != ((((((safe_lshift_func_uint8_t_u_s(g_2, (safe_rshift_func_int8_t_s_s(l_776[9][1][3], 7)))) >= l_776[9][1][3]) <= (((safe_sub_func_int8_t_s_s(((*l_84) = (**l_623)), 0x1AL)) > (safe_rshift_func_int8_t_s_u((safe_div_func_int16_t_s_s(1L, (safe_rshift_func_int32_t_s_u((safe_rshift_func_int8_t_s_u((**l_478), l_771)), 17)))), g_386))) , g_19)) || (**l_623)) || (**l_623)) <= g_110)) && (*p_73))) > 1UL) > 1UL))) < 0L) , l_776[9][1][3])), (**l_478))));
                        l_788 = (((safe_mul_func_int64_t_s_s(((+(l_793 != &p_73)) > (safe_lshift_func_uint16_t_u_s(((safe_mod_func_int32_t_s_s(1L, 0xC4FA64EFL)) , 0x7D55L), (4294967295UL | (safe_sub_func_int8_t_s_s((((safe_add_func_int32_t_s_s(((*p_73) ^= ((((**l_623) <= (-2L)) ^ (safe_lshift_func_uint64_t_u_s(l_776[9][1][3], 51))) > 0x1CC0L)), (**l_793))) != l_776[9][1][3]) != l_788), g_113)))))), l_776[9][1][3])) , 0x77FFL) >= g_271[2][0]);
                        (*p_73) ^= (safe_rshift_func_int16_t_s_u(((*l_787) = (safe_div_func_uint64_t_u_u(0xCD139C8C9B2D9503LL, (**l_623)))), 8));
                        l_815++;
                    }
                    else
                    { /* block id: 367 */
                        int32_t **l_818 = (void*)0;
                        return l_818;
                    }
                    l_841++;
                    if (((((0x55L >= ((((safe_mul_func_uint16_t_u_u(g_599, (((void*)0 != &l_820) && ((~(&g_334 == &g_334)) != (((0UL != (safe_div_func_uint32_t_u_u(((safe_lshift_func_uint64_t_u_u(((safe_mul_func_uint16_t_u_u(((safe_mul_func_uint64_t_u_u(((void*)0 == l_855), (-1L))) || g_364), (**l_478))) ^ 0xDAL), g_192[0][3][0])) >= 18446744073709551606UL), g_128))) , &l_793) != &p_74))))) != (**l_793)) > g_43) < (**l_623))) | (**l_793)) < 0UL) < 1L))
                    { /* block id: 371 */
                        uint64_t *l_866 = &l_776[9][1][3];
                        int32_t l_875 = (-9L);
                        uint8_t *l_877 = &l_99;
                        (*l_497) = g_856;
                        l_810 = (safe_mul_func_int8_t_s_s(g_34, (((0x74L & 0UL) ^ (safe_mod_func_int16_t_s_s((&l_484 != ((safe_mul_func_int16_t_s_s((safe_add_func_int16_t_s_s(((l_865 | ((*l_866)++)) > ((g_85 = ((**l_793) , (((**l_793) == (l_869 , (safe_mod_func_int64_t_s_s((safe_div_func_uint64_t_u_u(((void*)0 == &l_99), l_837)), (**l_623))))) | g_192[1][2][7]))) > g_35[1][0][3])), l_874[4])), g_125)) , &g_34)), g_315[8]))) , 1UL)));
                        (*p_73) ^= l_875;
                        (*p_73) = (~((**l_623) <= ((*l_877)++)));
                    }
                    else
                    { /* block id: 379 */
                        int8_t *l_891 = &g_892[3];
                        int32_t l_894 = 0L;
                        l_828 |= (safe_mod_func_int64_t_s_s((**l_623), (0xF2BD7D9DL | (safe_sub_func_int8_t_s_s(((g_2 ^ g_386) , ((((((safe_mod_func_int64_t_s_s((*g_191), ((*l_632) &= (*g_191)))) != ((((safe_mul_func_uint16_t_u_u(0x6F87L, ((safe_mod_func_uint8_t_u_u((~((*l_891) = ((((*l_84) ^= ((3L || (((((0x7EB5L & (**l_478)) <= 18446744073709551608UL) == g_125) > (**l_793)) , g_364)) > 0x9EL)) | 4UL) < (**l_478)))), (**l_793))) || 0xFDL))) || (**l_623)) ^ 4294967295UL) < g_601)) | l_893) , (*g_191)) && g_601) && l_894)), g_271[2][1])))));
                    }
                    if ((*p_73))
                        continue;
                }
                else
                { /* block id: 386 */
                    (*p_73) = 1L;
                }
                for (g_364 = 0; (g_364 <= 3); g_364 += 1)
                { /* block id: 391 */
                    int8_t l_897 = 9L;
                    uint64_t *l_898[2][3][4];
                    int32_t l_905 = 0xCD193752L;
                    int32_t l_909[8][6][5] = {{{6L,0xB08E6584L,0xC475889BL,0xDEF025E9L,0x24EAF564L},{0x02BBC3D7L,0xBDD8828DL,(-7L),0L,1L},{6L,0xDBCFDAB3L,0x8309BFAFL,0L,0L},{4L,0xD548F62AL,0xD548F62AL,4L,1L},{(-1L),0L,(-4L),0x31C6DE94L,0L},{0L,(-7L),0xBDD8828DL,0x02BBC3D7L,0x3CD4DD7FL}},{{1L,0L,1L,0x31C6DE94L,0x2B477211L},{9L,1L,(-1L),4L,(-1L)},{0L,0L,0x0E26E3DBL,0L,0xC475889BL},{0xE3FBF7CDL,0x02BBC3D7L,0L,0L,4L},{0x8309BFAFL,0xC6A90A67L,0xD11111CFL,0xDEF025E9L,8L},{1L,0x02BBC3D7L,1L,9L,0xBDD8828DL}},{{(-6L),0L,1L,0L,(-6L)},{(-3L),1L,0xFDC745C2L,0xE3FBF7CDL,0xE0F82C88L},{0L,0L,6L,0L,1L},{0xFDC745C2L,(-7L),0L,1L,0xE0F82C88L},{8L,0L,0L,0L,(-6L)},{0xE0F82C88L,0xD548F62AL,0xE0F82C88L,(-3L),0xBDD8828DL}},{{0xDBB38A52L,0xDBCFDAB3L,(-6L),9L,8L},{0xD548F62AL,0xBDD8828DL,1L,0xFDC745C2L,4L},{4L,0xB08E6584L,(-6L),(-8L),0xC475889BL},{0x837E6395L,(-1L),0xE0F82C88L,0xE0F82C88L,(-1L)},{(-9L),0x9AD62542L,0L,6L,0x2B477211L},{(-7L),0L,0L,0xD548F62AL,0x3CD4DD7FL}},{{(-6L),0x0D8E9E35L,6L,0L,(-9L)},{0xE3FBF7CDL,0xBDD8828DL,(-3L),0xE0F82C88L,0xD548F62AL},{1L,0L,0xC9F83EFBL,0x2D94C069L,1L},{0xE0F82C88L,(-3L),0xBDD8828DL,0xE3FBF7CDL,4L},{0L,(-8L),0x0E26E3DBL,0xDBCFDAB3L,0L},{0xCDC3FD3EL,0x837E6395L,(-7L),0xE3FBF7CDL,0x3CD4DD7FL}},{{1L,0xDEF025E9L,1L,0x2D94C069L,1L},{0x247D659FL,0x247D659FL,1L,0xE0F82C88L,0x837E6395L},{0x8309BFAFL,0xDBCFDAB3L,6L,0x24F79013L,1L},{(-3L),0L,(-1L),0xCDC3FD3EL,0xFDC745C2L},{1L,0xDBCFDAB3L,0xD11111CFL,0xB08E6584L,0x0E26E3DBL},{0x02BBC3D7L,0x247D659FL,0xCDC3FD3EL,0x247D659FL,0x02BBC3D7L}},{{(-6L),0xDEF025E9L,(-6L),0L,0x2B477211L},{0xD548F62AL,0x837E6395L,0xE3FBF7CDL,(-3L),0xCDC3FD3EL},{(-6L),(-8L),(-1L),0xDEF025E9L,0x2B477211L},{0xFDC745C2L,(-3L),0xD548F62AL,0x02BBC3D7L,0x02BBC3D7L},{0x2B477211L,0L,0x2B477211L,0x487C0B72L,0x0E26E3DBL},{0L,0xBDD8828DL,1L,0xD548F62AL,0xFDC745C2L}},{{0xC9F83EFBL,0x9AD62542L,4L,0L,1L},{1L,(-7L),1L,0xFDC745C2L,0x837E6395L},{0L,0xC6A90A67L,0x2B477211L,0x45110704L,1L},{1L,1L,0xD548F62AL,0L,0x3CD4DD7FL},{8L,0x31C6DE94L,(-1L),0L,0L},{1L,(-1L),0xE3FBF7CDL,1L,4L}}};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 3; j++)
                        {
                            for (k = 0; k < 4; k++)
                                l_898[i][j][k] = &l_776[9][1][3];
                        }
                    }
                    (*p_73) &= (((((*l_84) |= ((safe_div_func_uint16_t_u_u(((l_897 != (g_315[6] |= 7UL)) <= ((safe_add_func_int16_t_s_s(((*g_191) < (*g_191)), (((g_386 = (safe_add_func_int16_t_s_s(((l_905 &= (safe_sub_func_uint64_t_u_u((((((0xDF600717L || ((((void*)0 == &g_856) || g_17.f0) , g_35[1][0][3])) , l_897) & 0xA93BDB09507636DCLL) != 0xEEL) || g_43), (-1L)))) && 0xBACA55CFL), g_47))) >= l_897) == (*g_191)))) >= (**l_623))), (**l_623))) , g_601)) || (**l_623)) >= l_897) ^ g_110);
                    for (l_826 = 0; (l_826 <= 0); l_826 += 1)
                    { /* block id: 399 */
                        int32_t l_906 = 0x34AEFD3BL;
                        int32_t l_907 = 1L;
                        int32_t l_908 = (-4L);
                        int32_t l_910 = 0x6D547A8DL;
                        int32_t l_912 = 3L;
                        int32_t l_913 = 0x34DD0DDDL;
                        int32_t l_914 = 0L;
                        int32_t l_915 = (-1L);
                        int32_t l_916 = 0xFDBE7D41L;
                        int32_t l_917 = (-4L);
                        int32_t l_918 = 0L;
                        int32_t l_919 = 0x762F85BCL;
                        int32_t l_922 = 0L;
                        int32_t l_923 = 0x36C94F08L;
                        int32_t l_924 = (-1L);
                        int32_t l_925 = 0xEBD68583L;
                        int32_t l_926[8] = {0x4D0BA542L,0xD2996405L,0xD2996405L,0x4D0BA542L,0xD2996405L,0xD2996405L,0x4D0BA542L,0xD2996405L};
                        int i;
                        (*g_532) = (*g_532);
                        --g_927;
                    }
                    g_931++;
                    for (g_125 = 0; (g_125 <= 0); g_125 += 1)
                    { /* block id: 406 */
                        uint16_t l_934 = 0x7C51L;
                        (*p_75) = (*p_75);
                        ++l_934;
                    }
                    for (l_897 = 0; (l_897 <= 0); l_897 += 1)
                    { /* block id: 412 */
                        uint8_t *l_948 = &g_47;
                        int16_t *l_949 = &l_921;
                        int i, j, k;
                        (*p_73) = (((*l_949) = (((g_35[(l_897 + 1)][l_897][g_364] < l_820) , (safe_lshift_func_int32_t_s_s(l_909[5][2][1], l_939))) , (safe_rshift_func_int16_t_s_u(l_909[5][2][1], (((*l_84) |= (safe_div_func_uint8_t_u_u(((*l_948) |= (safe_mul_func_uint8_t_u_u((g_315[0] && 0x3C467644979EC637LL), ((0xD30BA562190A0E58LL < (safe_sub_func_uint16_t_u_u(g_601, g_19))) | g_124)))), g_601))) ^ 0x8FL))))) ^ g_125);
                    }
                }
            }
            (*p_73) ^= (**l_478);
        }
    }
    for (g_34 = 0; (g_34 <= 1); g_34 += 1)
    { /* block id: 425 */
        uint32_t l_950 = 0xA25586DDL;
        int64_t *l_969 = &l_494[3][0];
        int32_t l_974[1][10][9] = {{{(-1L),2L,0L,(-1L),9L,(-8L),0x92ACC974L,1L,(-1L)},{(-1L),(-1L),0x5109F4A2L,0xC740CC83L,0x2EB7527FL,(-1L),0xF22E338BL,0x8989C847L,0xF22E338BL},{0x8989C847L,2L,0x3DE46548L,0x3DE46548L,2L,0x8989C847L,0xBCABAE59L,0x6680906EL,0xF22E338BL},{0x2EB7527FL,0x6680906EL,1L,0L,(-1L),0x92ACC974L,0x8989C847L,0x3DE46548L,(-1L)},{0L,1L,0x6680906EL,0x2EB7527FL,0xBCABAE59L,(-4L),0xBCABAE59L,0x2EB7527FL,0x6680906EL},{0x3DE46548L,0x3DE46548L,2L,0x8989C847L,0xBCABAE59L,0x6680906EL,0xF22E338BL,(-1L),(-1L)},{0xC740CC83L,0x5109F4A2L,(-1L),(-1L),(-1L),0xA5A2535FL,0x92ACC974L,0xBCABAE59L,0xBCABAE59L},{(-1L),0L,2L,(-1L),(-1L),0xC740CC83L,1L,(-4L),0x6680906EL},{1L,0x5109F4A2L,0x92ACC974L,0xC740CC83L,(-1L),0L,0xBCABAE59L,0L,0x3DE46548L},{0xB77A47ACL,0x8989C847L,0L,0x2EB7527FL,(-4L),(-1L),(-1L),(-4L),0x2EB7527FL}}};
        int32_t l_976 = 0x91A22BBCL;
        int32_t ****l_987 = &g_255[2][3];
        union U0 l_1005 = {0x8A1A40E3L};
        int32_t l_1006 = 3L;
        uint32_t l_1018 = 0UL;
        uint64_t l_1037 = 0x35E4E7A94B4FA5EBLL;
        uint64_t l_1068 = 0x5624AA1E7B0DE9A1LL;
        int32_t **l_1093 = (void*)0;
        union U0 ****l_1125 = &l_497;
        uint32_t l_1168 = 0x99809ACFL;
        int i, j, k;
        if (l_950)
            break;
    }
    return l_1235;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_17.f0, "g_17.f0", print_hash_value);
    transparent_crc(g_19, "g_19", print_hash_value);
    transparent_crc(g_33, "g_33", print_hash_value);
    transparent_crc(g_34, "g_34", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_35[i][j][k], "g_35[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_43, "g_43", print_hash_value);
    transparent_crc(g_44, "g_44", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_85, "g_85", print_hash_value);
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_113, "g_113", print_hash_value);
    transparent_crc(g_124, "g_124", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_128, "g_128", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_192[i][j][k], "g_192[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_271[i][j], "g_271[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_315[i], "g_315[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_364, "g_364", print_hash_value);
    transparent_crc(g_386, "g_386", print_hash_value);
    transparent_crc(g_599, "g_599", print_hash_value);
    transparent_crc(g_601, "g_601", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_892[i], "g_892[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_920[i][j][k], "g_920[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_927, "g_927", print_hash_value);
    transparent_crc(g_931, "g_931", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1008[i], "g_1008[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1043.f0, "g_1043.f0", print_hash_value);
    transparent_crc(g_1108, "g_1108", print_hash_value);
    transparent_crc(g_1283, "g_1283", print_hash_value);
    transparent_crc(g_1577, "g_1577", print_hash_value);
    transparent_crc(g_1609, "g_1609", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1615[i], "g_1615[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1633[i], "g_1633[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1647, "g_1647", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1714[i].f0, "g_1714[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1863, "g_1863", print_hash_value);
    transparent_crc(g_1881.f0, "g_1881.f0", print_hash_value);
    transparent_crc(g_2108, "g_2108", print_hash_value);
    transparent_crc(g_2138, "g_2138", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_2303[i][j], "g_2303[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2456, "g_2456", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_2593[i][j], "g_2593[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 781
XXX total union variables: 20

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 45
breakdown:
   depth: 1, occurrence: 197
   depth: 2, occurrence: 57
   depth: 3, occurrence: 6
   depth: 4, occurrence: 2
   depth: 6, occurrence: 2
   depth: 8, occurrence: 3
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 3
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 1
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 4
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 4
   depth: 31, occurrence: 1
   depth: 34, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 1
   depth: 40, occurrence: 1
   depth: 45, occurrence: 1

XXX total number of pointers: 478

XXX times a variable address is taken: 1066
XXX times a pointer is dereferenced on RHS: 443
breakdown:
   depth: 1, occurrence: 272
   depth: 2, occurrence: 150
   depth: 3, occurrence: 15
   depth: 4, occurrence: 4
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 378
breakdown:
   depth: 1, occurrence: 324
   depth: 2, occurrence: 39
   depth: 3, occurrence: 15
XXX times a pointer is compared with null: 57
XXX times a pointer is compared with address of another variable: 19
XXX times a pointer is compared with another pointer: 14
XXX times a pointer is qualified to be dereferenced: 11857

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1707
   level: 2, occurrence: 650
   level: 3, occurrence: 248
   level: 4, occurrence: 169
   level: 5, occurrence: 57
XXX number of pointers point to pointers: 251
XXX number of pointers point to scalars: 208
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.1
XXX average alias set size: 1.48

XXX times a non-volatile is read: 2482
XXX times a non-volatile is write: 1158
XXX times a volatile is read: 35
XXX    times read thru a pointer: 5
XXX times a volatile is write: 16
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 806
XXX percentage of non-volatile access: 98.6

XXX forward jumps: 1
XXX backward jumps: 4

XXX stmts: 204
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 37
   depth: 1, occurrence: 23
   depth: 2, occurrence: 23
   depth: 3, occurrence: 29
   depth: 4, occurrence: 40
   depth: 5, occurrence: 52

XXX percentage a fresh-made variable is used: 15.4
XXX percentage an existing variable is used: 84.6
XXX total OOB instances added: 0
********************* end of statistics **********************/


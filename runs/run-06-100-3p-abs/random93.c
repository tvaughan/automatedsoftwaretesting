/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      3024293611
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   const signed f0 : 20;
   volatile unsigned f1 : 27;
   const unsigned f2 : 18;
   unsigned f3 : 21;
   volatile signed f4 : 18;
   const unsigned f5 : 3;
   volatile unsigned f6 : 22;
};

union U1 {
   volatile uint32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2 = 0xBE187950L;/* VOLATILE GLOBAL g_2 */
static int32_t g_3 = 0x7A73D2D6L;
static volatile int32_t g_12 = (-1L);/* VOLATILE GLOBAL g_12 */
static int32_t g_13 = (-7L);
static volatile int32_t g_14 = (-3L);/* VOLATILE GLOBAL g_14 */
static int32_t g_15 = 0xAC842F88L;
static volatile int32_t g_16 = 7L;/* VOLATILE GLOBAL g_16 */
static volatile int32_t g_17 = 0L;/* VOLATILE GLOBAL g_17 */
static volatile int32_t g_18 = 0L;/* VOLATILE GLOBAL g_18 */
static int32_t g_19 = 0L;
static int32_t g_33[10][10] = {{1L,9L,9L,1L,1L,9L,9L,1L,1L,9L},{1L,1L,9L,9L,1L,1L,9L,9L,1L,1L},{1L,9L,9L,1L,1L,9L,9L,1L,1L,9L},{1L,1L,9L,9L,1L,1L,9L,9L,1L,1L},{1L,9L,9L,1L,1L,9L,9L,1L,1L,9L},{1L,1L,9L,9L,1L,1L,9L,9L,1L,1L},{1L,9L,9L,1L,1L,9L,9L,1L,1L,9L},{1L,1L,9L,9L,1L,1L,9L,9L,1L,1L},{1L,9L,9L,1L,1L,9L,9L,1L,1L,9L},{1L,1L,9L,9L,1L,1L,9L,9L,1L,1L}};
static int32_t g_36 = (-1L);
static float g_48 = 0x1.6p-1;
static uint16_t g_50[5] = {0UL,0UL,0UL,0UL,0UL};
static uint16_t g_60[9] = {8UL,65535UL,8UL,65535UL,8UL,65535UL,8UL,65535UL,8UL};
static int32_t g_73 = 0x78833E6FL;
static int16_t g_106[3] = {(-3L),(-3L),(-3L)};
static int8_t g_108 = 0x7AL;
static const uint16_t g_111 = 0xFD8CL;
static const int8_t g_116 = 1L;
static const int8_t *g_115[2] = {&g_116,&g_116};
static uint16_t g_134 = 65535UL;
static volatile union U1 g_136 = {0x313D8634L};/* VOLATILE GLOBAL g_136 */
static uint16_t *g_138 = &g_134;
static uint16_t ** const g_137[5] = {&g_138,&g_138,&g_138,&g_138,&g_138};
static uint16_t **g_140 = &g_138;
static uint16_t *** volatile g_139 = &g_140;/* VOLATILE GLOBAL g_139 */
static volatile struct S0 g_141 = {-40,4322,99,703,37,0,1736};/* VOLATILE GLOBAL g_141 */
static uint8_t g_149 = 2UL;
static int32_t g_163 = 0x4AC2E131L;
static int32_t * volatile g_162 = &g_163;/* VOLATILE GLOBAL g_162 */
static float g_171 = 0x7.277C3Fp-71;
static int32_t g_172[10][2] = {{0x41094869L,0x41094869L},{0x41094869L,0x16D73FB0L},{7L,0L},{0x16D73FB0L,0L},{7L,0x16D73FB0L},{0x41094869L,0x41094869L},{0x41094869L,0x16D73FB0L},{7L,0L},{0x16D73FB0L,0L},{7L,0x16D73FB0L}};
static uint32_t g_177 = 0x824672FDL;
static volatile struct S0 g_268[8][9] = {{{246,8994,367,412,324,1,820},{246,8994,367,412,324,1,820},{-31,8843,186,603,-319,0,1424},{356,6807,334,1216,459,0,693},{-31,8843,186,603,-319,0,1424},{246,8994,367,412,324,1,820},{246,8994,367,412,324,1,820},{-31,8843,186,603,-319,0,1424},{356,6807,334,1216,459,0,693}},{{-318,992,250,999,472,0,12},{382,4972,351,801,146,1,216},{-318,992,250,999,472,0,12},{965,7147,445,1163,-135,1,948},{116,10847,393,1338,-260,0,1357},{-9,516,52,603,193,1,1168},{-649,11439,408,863,468,0,713},{-9,516,52,603,193,1,1168},{116,10847,393,1338,-260,0,1357}},{{424,7687,241,694,-385,0,1238},{-31,8843,186,603,-319,0,1424},{-31,8843,186,603,-319,0,1424},{424,7687,241,694,-385,0,1238},{-550,2093,185,1004,216,1,325},{424,7687,241,694,-385,0,1238},{-31,8843,186,603,-319,0,1424},{-31,8843,186,603,-319,0,1424},{424,7687,241,694,-385,0,1238}},{{-985,9393,472,1189,170,0,1561},{965,7147,445,1163,-135,1,948},{32,11385,231,225,24,1,1894},{965,7147,445,1163,-135,1,948},{-985,9393,472,1189,170,0,1561},{823,7945,89,667,434,1,475},{116,10847,393,1338,-260,0,1357},{-180,9485,188,318,219,1,1859},{116,10847,393,1338,-260,0,1357}},{{-31,8843,186,603,-319,0,1424},{-550,2093,185,1004,216,1,325},{356,6807,334,1216,459,0,693},{356,6807,334,1216,459,0,693},{-550,2093,185,1004,216,1,325},{-31,8843,186,603,-319,0,1424},{-550,2093,185,1004,216,1,325},{356,6807,334,1216,459,0,693},{356,6807,334,1216,459,0,693}},{{-985,9393,472,1189,170,0,1561},{823,7945,89,667,434,1,475},{116,10847,393,1338,-260,0,1357},{-180,9485,188,318,219,1,1859},{116,10847,393,1338,-260,0,1357},{823,7945,89,667,434,1,475},{-985,9393,472,1189,170,0,1561},{965,7147,445,1163,-135,1,948},{32,11385,231,225,24,1,1894}},{{424,7687,241,694,-385,0,1238},{-550,2093,185,1004,216,1,325},{424,7687,241,694,-385,0,1238},{-31,8843,186,603,-319,0,1424},{-31,8843,186,603,-319,0,1424},{424,7687,241,694,-385,0,1238},{-550,2093,185,1004,216,1,325},{424,7687,241,694,-385,0,1238},{-31,8843,186,603,-319,0,1424}},{{-318,992,250,999,472,0,12},{965,7147,445,1163,-135,1,948},{116,10847,393,1338,-260,0,1357},{-9,516,52,603,193,1,1168},{-649,11439,408,863,468,0,713},{-9,516,52,603,193,1,1168},{116,10847,393,1338,-260,0,1357},{965,7147,445,1163,-135,1,948},{-318,992,250,999,472,0,12}}};
static uint64_t g_306 = 1UL;
static uint64_t g_309 = 0x3E32F4612B06F095LL;
static uint16_t ** const *g_321 = &g_137[1];
static uint16_t ** const ** volatile g_320[8] = {&g_321,&g_321,&g_321,&g_321,&g_321,&g_321,&g_321,&g_321};
static uint64_t **g_332 = (void*)0;
static uint64_t * volatile *g_333 = (void*)0;
static int16_t g_370[4][1] = {{1L},{0xD03BL},{1L},{0xD03BL}};
static int32_t *g_391[5][4][8] = {{{(void*)0,&g_172[2][0],(void*)0,&g_13,&g_172[4][1],&g_13,(void*)0,&g_172[2][0]},{(void*)0,&g_15,&g_33[8][2],&g_36,&g_172[4][1],&g_36,&g_36,&g_172[4][1]},{&g_33[8][2],(void*)0,&g_172[4][1],&g_172[5][1],&g_13,&g_19,&g_36,(void*)0},{&g_172[4][1],&g_172[5][1],&g_33[8][2],&g_36,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,&g_36,&g_33[8][2],&g_172[5][1],&g_172[4][1]},{(void*)0,&g_36,&g_19,&g_13,&g_172[5][1],&g_172[4][1],(void*)0,&g_33[8][2]},{&g_172[4][1],&g_36,&g_36,&g_172[4][1],&g_36,&g_33[8][2],&g_15,&g_13},{&g_3,(void*)0,&g_172[4][1],(void*)0,&g_172[4][1],(void*)0,&g_3,&g_33[8][2]}},{{&g_36,&g_172[5][1],&g_19,&g_3,&g_33[8][2],&g_19,(void*)0,&g_33[8][2]},{&g_33[8][2],(void*)0,&g_15,&g_172[4][1],&g_33[8][2],&g_36,(void*)0,(void*)0},{&g_36,&g_15,&g_172[4][1],&g_33[8][2],&g_172[4][1],&g_172[4][1],&g_33[8][2],&g_172[4][1]},{&g_3,&g_3,&g_172[4][1],(void*)0,&g_36,&g_19,&g_13,&g_172[5][1]}},{{&g_172[4][1],(void*)0,&g_3,&g_36,&g_172[5][1],&g_15,(void*)0,&g_172[5][1]},{(void*)0,(void*)0,&g_33[8][2],(void*)0,&g_36,&g_172[4][1],(void*)0,&g_172[4][1]},{(void*)0,&g_33[8][2],&g_19,&g_33[8][2],(void*)0,&g_172[4][1],&g_33[8][2],(void*)0},{&g_172[4][1],&g_13,(void*)0,&g_172[4][1],&g_13,&g_3,&g_15,&g_33[8][2]}},{{&g_33[8][2],(void*)0,(void*)0,&g_3,&g_172[4][1],&g_33[8][2],&g_33[8][2],&g_33[8][2]},{&g_13,(void*)0,&g_19,(void*)0,(void*)0,&g_19,(void*)0,&g_13},{&g_33[8][2],&g_33[8][2],&g_33[8][2],&g_172[4][1],&g_3,(void*)0,(void*)0,&g_33[8][2]},{&g_33[8][2],&g_15,&g_3,&g_13,&g_172[4][1],(void*)0,&g_13,&g_172[4][1]}}};
static volatile struct S0 g_418[5] = {{31,10082,184,731,107,0,1975},{31,10082,184,731,107,0,1975},{31,10082,184,731,107,0,1975},{31,10082,184,731,107,0,1975},{31,10082,184,731,107,0,1975}};
static volatile union U1 g_421[4] = {{0xB84948ACL},{0xB84948ACL},{0xB84948ACL},{0xB84948ACL}};
static uint32_t g_487 = 0xBA4709FAL;
static int32_t g_501 = 0xC54CE3A0L;
static int32_t g_502 = (-1L);
static struct S0 g_511 = {-685,9989,510,1424,-259,1,587};/* VOLATILE GLOBAL g_511 */
static struct S0 g_542 = {4,9981,159,528,434,1,1489};/* VOLATILE GLOBAL g_542 */
static const uint16_t *g_556 = &g_134;
static const uint16_t * const *g_555 = &g_556;
static const uint16_t * const ** volatile g_554 = &g_555;/* VOLATILE GLOBAL g_554 */
static uint16_t g_577 = 0x2E1EL;
static float * volatile g_580 = &g_171;/* VOLATILE GLOBAL g_580 */
static volatile struct S0 g_589[2] = {{947,5930,504,603,-208,1,1160},{947,5930,504,603,-208,1,1160}};
static volatile int16_t g_650[8][1] = {{0xE890L},{(-2L)},{(-2L)},{0xE890L},{(-2L)},{(-2L)},{0xE890L},{(-2L)}};
static uint32_t g_656[8] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static const struct S0 g_695 = {264,9382,474,1229,10,1,849};/* VOLATILE GLOBAL g_695 */
static int64_t g_699[3][4][2] = {{{0xC61DE65D5190E96ELL,(-4L)},{(-1L),0xC61DE65D5190E96ELL},{0x1D6039D178CF9438LL,0x1D6039D178CF9438LL},{0x1D6039D178CF9438LL,0xC61DE65D5190E96ELL}},{{(-1L),(-4L)},{0xC61DE65D5190E96ELL,(-4L)},{9L,(-1L)},{0xC61DE65D5190E96ELL,0xC61DE65D5190E96ELL}},{{0xC61DE65D5190E96ELL,(-1L)},{9L,0x1D6039D178CF9438LL},{(-1L),0x1D6039D178CF9438LL},{9L,(-1L)}}};
static const uint16_t * const **g_745[8] = {&g_555,&g_555,&g_555,&g_555,&g_555,&g_555,&g_555,&g_555};
static const uint16_t * const ** const *g_744 = &g_745[7];
static const uint16_t * const ** const **g_743[8] = {&g_744,&g_744,&g_744,&g_744,&g_744,&g_744,&g_744,&g_744};
static struct S0 g_769 = {56,8275,482,453,-217,1,1442};/* VOLATILE GLOBAL g_769 */
static struct S0 g_779 = {-230,9085,340,831,268,1,491};/* VOLATILE GLOBAL g_779 */
static struct S0 g_784 = {-710,853,484,1437,-506,0,1747};/* VOLATILE GLOBAL g_784 */
static float * volatile g_791 = &g_171;/* VOLATILE GLOBAL g_791 */
static union U1 g_890 = {0xA8DAEAA9L};/* VOLATILE GLOBAL g_890 */
static union U1 *g_889 = &g_890;
static volatile struct S0 g_918 = {751,4517,378,936,264,0,1596};/* VOLATILE GLOBAL g_918 */
static int32_t *g_949 = &g_501;
static int32_t **g_948 = &g_949;
static const uint8_t g_996 = 252UL;
static const int8_t g_1006 = (-1L);
static uint8_t *g_1102 = &g_149;
static uint8_t **g_1101 = &g_1102;
static uint8_t ***g_1100 = &g_1101;
static union U1 g_1122 = {0x66CC1529L};/* VOLATILE GLOBAL g_1122 */
static volatile struct S0 g_1160 = {680,3482,119,1089,-486,0,1235};/* VOLATILE GLOBAL g_1160 */
static volatile union U1 g_1162 = {18446744073709551613UL};/* VOLATILE GLOBAL g_1162 */
static int32_t * const *g_1173 = &g_949;
static int32_t * const **g_1172 = &g_1173;
static struct S0 g_1201 = {-977,11218,101,745,317,1,742};/* VOLATILE GLOBAL g_1201 */
static union U1 g_1272 = {18446744073709551615UL};/* VOLATILE GLOBAL g_1272 */
static volatile int64_t g_1293[8][3] = {{0L,(-1L),5L},{5L,0x77E584D66B456BF8LL,5L},{(-3L),0L,5L},{0xB436143120EDA8D0LL,0xB436143120EDA8D0LL,0L},{6L,0L,0L},{0L,0x77E584D66B456BF8LL,0xB0DF0D51A0D24475LL},{6L,(-1L),6L},{0xB436143120EDA8D0LL,0L,0xB0DF0D51A0D24475LL}};
static volatile uint16_t g_1308[10] = {65535UL,8UL,65535UL,8UL,65535UL,8UL,65535UL,8UL,65535UL,8UL};
static union U1 g_1315 = {0UL};/* VOLATILE GLOBAL g_1315 */
static int32_t ** volatile g_1357 = &g_391[4][2][2];/* VOLATILE GLOBAL g_1357 */
static int8_t g_1379 = 0L;
static volatile union U1 *g_1398 = (void*)0;
static volatile union U1 ** volatile g_1397 = &g_1398;/* VOLATILE GLOBAL g_1397 */
static volatile struct S0 g_1418 = {11,1572,48,186,-93,1,1485};/* VOLATILE GLOBAL g_1418 */
static const uint32_t g_1471 = 0x3DD84ABDL;
static const uint32_t *g_1470 = &g_1471;
static struct S0 g_1496 = {-140,805,502,14,-355,1,1752};/* VOLATILE GLOBAL g_1496 */
static int32_t * volatile g_1559[8][10] = {{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73},{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73},{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73},{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73},{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73},{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73},{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73},{&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73,&g_73}};
static int32_t * volatile g_1560 = &g_172[4][1];/* VOLATILE GLOBAL g_1560 */
static uint8_t g_1580[1] = {255UL};
static int32_t * const  volatile g_1583 = &g_172[3][1];/* VOLATILE GLOBAL g_1583 */
static volatile union U1 g_1615 = {3UL};/* VOLATILE GLOBAL g_1615 */
static float g_1646[4] = {0x6.Fp+1,0x6.Fp+1,0x6.Fp+1,0x6.Fp+1};
static union U1 ** const  volatile g_1651 = &g_889;/* VOLATILE GLOBAL g_1651 */
static int32_t * volatile g_1670[10] = {&g_163,&g_163,&g_3,&g_163,&g_163,&g_3,&g_163,&g_163,&g_3,&g_163};
static volatile int64_t g_1699 = 0x486561FFF0D1F480LL;/* VOLATILE GLOBAL g_1699 */
static volatile int64_t g_1700 = (-1L);/* VOLATILE GLOBAL g_1700 */
static volatile int64_t g_1701 = 0xCA3D53613F18D870LL;/* VOLATILE GLOBAL g_1701 */
static volatile int64_t *g_1698[4][7][1] = {{{&g_1699},{&g_1701},{&g_1699},{&g_1699},{&g_1701},{&g_1699},{&g_1699}},{{&g_1701},{&g_1699},{&g_1699},{&g_1701},{&g_1699},{&g_1699},{&g_1701}},{{&g_1699},{&g_1699},{&g_1701},{&g_1699},{&g_1699},{&g_1701},{&g_1699}},{{&g_1699},{&g_1701},{&g_1699},{&g_1699},{&g_1701},{&g_1699},{&g_1699}}};
static volatile int64_t **g_1697 = &g_1698[0][6][0];
static uint16_t g_1702 = 0xC272L;
static const volatile union U1 g_1709 = {1UL};/* VOLATILE GLOBAL g_1709 */
static volatile uint8_t ** volatile * volatile * const g_1737 = (void*)0;
static volatile uint8_t ** volatile * volatile * const  volatile * volatile g_1736 = &g_1737;/* VOLATILE GLOBAL g_1736 */
static union U1 * volatile **g_1774 = (void*)0;
static int32_t g_1796 = 1L;
static int32_t ** volatile ** volatile g_1813 = (void*)0;/* VOLATILE GLOBAL g_1813 */
static int32_t ** volatile *g_1815 = &g_1357;
static int32_t ** volatile ** volatile g_1814[5] = {&g_1815,&g_1815,&g_1815,&g_1815,&g_1815};
static const union U1 *g_1819 = &g_1272;
static const union U1 ** volatile g_1818 = &g_1819;/* VOLATILE GLOBAL g_1818 */
static volatile struct S0 g_1822 = {700,136,349,69,348,0,1578};/* VOLATILE GLOBAL g_1822 */
static int32_t * volatile g_1837 = &g_172[4][1];/* VOLATILE GLOBAL g_1837 */
static const int32_t *g_1840 = &g_172[4][1];
static const int32_t ** volatile g_1839 = &g_1840;/* VOLATILE GLOBAL g_1839 */
static volatile union U1 g_1850 = {2UL};/* VOLATILE GLOBAL g_1850 */
static uint16_t ****g_1865[1] = {(void*)0};
static union U1 ** volatile g_1873 = &g_889;/* VOLATILE GLOBAL g_1873 */
static volatile int16_t g_1948 = 0x956AL;/* VOLATILE GLOBAL g_1948 */
static volatile int16_t *g_1947 = &g_1948;
static volatile int16_t **g_1946 = &g_1947;
static float g_1971 = 0x1.Ap+1;
static union U1 g_2014 = {0x8B0B52D2L};/* VOLATILE GLOBAL g_2014 */
static volatile struct S0 g_2020 = {-729,8399,59,573,-265,0,501};/* VOLATILE GLOBAL g_2020 */
static union U1 **g_2040[7][8] = {{(void*)0,(void*)0,&g_889,&g_889,(void*)0,(void*)0,&g_889,&g_889},{(void*)0,(void*)0,&g_889,&g_889,(void*)0,(void*)0,&g_889,&g_889},{(void*)0,(void*)0,&g_889,&g_889,(void*)0,(void*)0,&g_889,&g_889},{(void*)0,(void*)0,&g_889,&g_889,(void*)0,(void*)0,&g_889,&g_889},{(void*)0,(void*)0,&g_889,&g_889,(void*)0,(void*)0,&g_889,&g_889},{(void*)0,(void*)0,&g_889,&g_889,(void*)0,(void*)0,&g_889,&g_889},{(void*)0,(void*)0,&g_889,&g_889,(void*)0,(void*)0,&g_889,&g_889}};
static union U1 ***g_2039[10][7] = {{&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0]},{&g_2040[2][6],&g_2040[4][0],(void*)0,&g_2040[4][0],&g_2040[2][6],&g_2040[2][6],&g_2040[4][0]},{&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0]},{&g_2040[4][0],&g_2040[4][0],(void*)0,(void*)0,&g_2040[4][0],&g_2040[4][0],&g_2040[4][0]},{&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0]},{&g_2040[2][6],&g_2040[2][6],&g_2040[4][0],(void*)0,&g_2040[4][0],&g_2040[2][6],&g_2040[2][6]},{&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0]},{&g_2040[6][6],&g_2040[4][0],&g_2040[6][6],&g_2040[4][0],&g_2040[4][0],&g_2040[6][6],&g_2040[4][0]},{&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0],&g_2040[4][0]},{&g_2040[6][6],&g_2040[4][0],&g_2040[4][0],&g_2040[6][6],&g_2040[4][0],&g_2040[6][6],&g_2040[4][0]}};
static int32_t ***g_2075 = (void*)0;
static uint16_t *** const **g_2084[3] = {(void*)0,(void*)0,(void*)0};
static struct S0 *g_2098 = &g_784;
static struct S0 ** volatile g_2097 = &g_2098;/* VOLATILE GLOBAL g_2097 */
static volatile union U1 ** volatile g_2122[5] = {&g_1398,&g_1398,&g_1398,&g_1398,&g_1398};
static volatile union U1 ** volatile g_2123 = &g_1398;/* VOLATILE GLOBAL g_2123 */
static volatile uint16_t g_2132 = 4UL;/* VOLATILE GLOBAL g_2132 */
static int32_t * volatile g_2154 = &g_1796;/* VOLATILE GLOBAL g_2154 */
static volatile float g_2168 = 0x4.4p-1;/* VOLATILE GLOBAL g_2168 */
static uint64_t g_2192 = 0x149AB2D0FCB1ABA9LL;
static uint8_t g_2202 = 253UL;
static float * volatile g_2203 = &g_1971;/* VOLATILE GLOBAL g_2203 */
static volatile uint8_t g_2228 = 0x76L;/* VOLATILE GLOBAL g_2228 */
static int32_t * volatile g_2271 = &g_1796;/* VOLATILE GLOBAL g_2271 */
static const int8_t **g_2276 = &g_115[0];
static const int8_t ***g_2275 = &g_2276;
static const int8_t **** volatile g_2274[10][3][1] = {{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}},{{&g_2275},{&g_2275},{&g_2275}}};
static float g_2281 = 0x0.146C59p+40;
static int32_t * volatile * const  volatile g_2291 = (void*)0;/* VOLATILE GLOBAL g_2291 */
static int32_t * volatile * volatile g_2292 = &g_1559[5][1];/* VOLATILE GLOBAL g_2292 */
static volatile uint8_t g_2330 = 0x6FL;/* VOLATILE GLOBAL g_2330 */
static volatile union U1 ** volatile g_2379 = &g_1398;/* VOLATILE GLOBAL g_2379 */
static volatile int16_t g_2389[5] = {(-4L),(-4L),(-4L),(-4L),(-4L)};
static volatile uint32_t g_2392 = 18446744073709551611UL;/* VOLATILE GLOBAL g_2392 */
static volatile uint16_t g_2432[2][6][7] = {{{65535UL,65535UL,0x3E9CL,0xA4C8L,0xDD5FL,1UL,0UL},{0xB89BL,65527UL,0x50E3L,0UL,0xA4C8L,65532UL,0xCF0FL},{1UL,6UL,0xE61FL,0xBB2EL,0xDD5FL,1UL,65532UL},{0x93F1L,0x38F2L,0UL,1UL,0x93F1L,65535UL,65535UL},{0x225CL,0x8B6AL,65531UL,65535UL,0xCF0FL,65535UL,0xCF0FL},{0x3E9CL,65535UL,65535UL,0x3E9CL,0xA4C8L,0xDD5FL,1UL}},{{3UL,65535UL,0xB89BL,0xDD5FL,0x6E13L,0xC32DL,0x1215L},{0xBB2EL,65535UL,1UL,1UL,1UL,0x62E4L,1UL},{0UL,65535UL,0x93F1L,65535UL,0x50E3L,65531UL,0xCF0FL},{0UL,0x50E3L,65527UL,0xB89BL,0x62E4L,6UL,65535UL},{0UL,1UL,65535UL,65535UL,1UL,0UL,0xC32DL},{0UL,0x93F1L,0x0004L,1UL,0xBB2EL,0xB89BL,0x8B6AL}}};
static int32_t ***g_2491 = &g_948;
static int32_t ***g_2492 = (void*)0;
static float * volatile g_2522 = &g_1646[3];/* VOLATILE GLOBAL g_2522 */
static volatile float g_2525 = 0x8.E45A0Dp+67;/* VOLATILE GLOBAL g_2525 */
static volatile struct S0 g_2527 = {-354,5408,1,763,239,1,1678};/* VOLATILE GLOBAL g_2527 */
static volatile union U1 g_2559 = {1UL};/* VOLATILE GLOBAL g_2559 */
static int32_t *g_2563[10][5] = {{(void*)0,&g_33[1][8],&g_19,(void*)0,(void*)0},{&g_33[1][8],(void*)0,&g_33[1][8],&g_19,(void*)0},{&g_172[1][1],&g_163,(void*)0,&g_163,&g_172[1][1]},{&g_33[1][8],&g_163,(void*)0,&g_172[1][1],(void*)0},{(void*)0,(void*)0,(void*)0,&g_172[1][1],&g_163},{&g_163,&g_33[1][8],&g_33[1][8],&g_163,(void*)0},{&g_163,&g_172[1][1],&g_19,&g_19,&g_172[1][1]},{(void*)0,&g_33[1][8],&g_19,(void*)0,(void*)0},{&g_33[1][8],(void*)0,&g_33[1][8],&g_19,(void*)0},{&g_172[1][1],&g_163,(void*)0,&g_163,&g_172[1][1]}};
static int32_t ** volatile g_2562[9][3][5] = {{{&g_2563[4][3],(void*)0,(void*)0,&g_2563[9][1],&g_2563[9][4]},{(void*)0,&g_2563[6][2],&g_2563[4][3],&g_2563[4][3],(void*)0},{&g_2563[2][4],&g_2563[8][2],&g_2563[4][0],&g_2563[4][3],&g_2563[4][3]}},{{&g_2563[4][3],&g_2563[8][3],&g_2563[6][2],&g_2563[4][3],&g_2563[4][3]},{(void*)0,&g_2563[4][3],&g_2563[6][2],&g_2563[9][4],(void*)0},{&g_2563[9][4],&g_2563[4][3],&g_2563[4][0],&g_2563[2][4],&g_2563[8][3]}},{{&g_2563[4][3],(void*)0,&g_2563[4][3],&g_2563[4][3],(void*)0},{&g_2563[4][3],&g_2563[4][3],(void*)0,&g_2563[9][0],&g_2563[0][1]},{&g_2563[4][3],&g_2563[4][3],&g_2563[4][3],&g_2563[4][3],&g_2563[8][3]}},{{&g_2563[4][3],&g_2563[4][3],&g_2563[9][1],(void*)0,&g_2563[4][3]},{&g_2563[4][3],(void*)0,&g_2563[3][2],&g_2563[4][3],&g_2563[4][3]},{&g_2563[4][3],&g_2563[4][2],&g_2563[4][3],&g_2563[4][3],&g_2563[4][3]}},{{&g_2563[4][3],&g_2563[4][3],(void*)0,&g_2563[4][3],&g_2563[4][3]},{&g_2563[9][4],&g_2563[4][3],(void*)0,&g_2563[4][3],&g_2563[4][3]},{(void*)0,&g_2563[4][3],&g_2563[3][3],&g_2563[4][3],(void*)0}},{{&g_2563[4][3],(void*)0,&g_2563[9][1],&g_2563[4][3],(void*)0},{&g_2563[4][3],&g_2563[4][3],&g_2563[4][3],&g_2563[4][3],&g_2563[4][3]},{(void*)0,(void*)0,&g_2563[4][3],&g_2563[0][1],&g_2563[3][2]}},{{&g_2563[4][3],&g_2563[4][3],&g_2563[3][3],&g_2563[4][3],&g_2563[9][1]},{&g_2563[4][3],&g_2563[7][2],&g_2563[2][1],&g_2563[4][3],&g_2563[4][3]},{(void*)0,&g_2563[4][3],&g_2563[1][0],(void*)0,(void*)0}},{{&g_2563[8][2],(void*)0,&g_2563[4][0],&g_2563[3][3],&g_2563[4][3]},{&g_2563[3][2],&g_2563[4][3],&g_2563[2][1],&g_2563[4][3],&g_2563[4][0]},{&g_2563[4][3],&g_2563[6][2],&g_2563[4][3],&g_2563[3][2],&g_2563[6][2]}},{{&g_2563[4][3],&g_2563[4][3],&g_2563[4][3],&g_2563[4][0],&g_2563[6][2]},{&g_2563[4][3],&g_2563[4][3],&g_2563[4][3],&g_2563[4][3],&g_2563[4][0]},{&g_2563[4][3],&g_2563[4][3],(void*)0,&g_2563[4][3],&g_2563[4][3]}}};
static volatile uint16_t g_2590 = 65535UL;/* VOLATILE GLOBAL g_2590 */
static uint32_t g_2638 = 0UL;
static uint64_t g_2665 = 0x30F57F58CE94D726LL;
static int64_t g_2677 = (-1L);
static float * volatile g_2726 = &g_171;/* VOLATILE GLOBAL g_2726 */
static volatile union U1 g_2731 = {3UL};/* VOLATILE GLOBAL g_2731 */
static int32_t ****g_2742[5][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
static int32_t *****g_2741 = &g_2742[3][0];
static int64_t g_2757 = 0x81667252574A7358LL;
static uint32_t *g_2770 = (void*)0;
static uint32_t * const * volatile g_2769[6] = {(void*)0,&g_2770,(void*)0,(void*)0,&g_2770,(void*)0};
static uint32_t * const * volatile *g_2768[7] = {&g_2769[0],&g_2769[0],&g_2769[0],&g_2769[0],&g_2769[0],&g_2769[0],&g_2769[0]};
static uint32_t **g_2774 = (void*)0;
static uint32_t ***g_2773[7][10][3] = {{{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0},{(void*)0,&g_2774,(void*)0},{&g_2774,(void*)0,(void*)0},{&g_2774,&g_2774,&g_2774}},{{&g_2774,(void*)0,&g_2774},{&g_2774,(void*)0,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{(void*)0,&g_2774,&g_2774},{(void*)0,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0},{&g_2774,&g_2774,(void*)0},{&g_2774,(void*)0,(void*)0},{&g_2774,&g_2774,&g_2774}},{{(void*)0,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,(void*)0,(void*)0},{(void*)0,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0},{&g_2774,(void*)0,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774}},{{(void*)0,&g_2774,(void*)0},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,(void*)0,&g_2774},{(void*)0,(void*)0,(void*)0},{&g_2774,&g_2774,(void*)0},{(void*)0,&g_2774,(void*)0},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774}},{{&g_2774,(void*)0,&g_2774},{&g_2774,&g_2774,(void*)0},{(void*)0,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_2774,&g_2774,&g_2774},{(void*)0,&g_2774,&g_2774}},{{&g_2774,(void*)0,(void*)0},{&g_2774,&g_2774,&g_2774},{&g_2774,(void*)0,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0},{&g_2774,(void*)0,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{(void*)0,&g_2774,&g_2774},{(void*)0,&g_2774,&g_2774}},{{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,(void*)0,&g_2774},{(void*)0,&g_2774,&g_2774},{(void*)0,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,&g_2774},{&g_2774,&g_2774,(void*)0}}};
static int8_t g_2836 = 0x4EL;
static int16_t g_2839 = (-8L);
static int64_t *g_2861[5] = {&g_2677,&g_2677,&g_2677,&g_2677,&g_2677};
static float * const  volatile g_2877 = &g_171;/* VOLATILE GLOBAL g_2877 */
static volatile union U1 * volatile ** volatile **g_3001 = (void*)0;
static struct S0 g_3016 = {-918,10326,416,1286,360,1,1438};/* VOLATILE GLOBAL g_3016 */
static const volatile union U1 g_3028 = {0UL};/* VOLATILE GLOBAL g_3028 */
static const int32_t g_3097 = 1L;
static struct S0 ***g_3105 = (void*)0;
static struct S0 ***g_3106 = (void*)0;
static union U1 g_3127 = {0x667251FCL};/* VOLATILE GLOBAL g_3127 */
static uint8_t g_3152 = 0x0AL;
static float * volatile g_3162 = (void*)0;/* VOLATILE GLOBAL g_3162 */
static uint32_t ****g_3190 = (void*)0;
static uint32_t ***** volatile g_3189[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint32_t ***** volatile g_3191 = &g_3190;/* VOLATILE GLOBAL g_3191 */
static int32_t * const  volatile g_3197 = (void*)0;/* VOLATILE GLOBAL g_3197 */
static int32_t * volatile g_3198[2] = {(void*)0,(void*)0};
static struct S0 g_3215 = {223,1453,399,756,-5,1,1617};/* VOLATILE GLOBAL g_3215 */
static struct S0 **g_3258 = (void*)0;
static struct S0 ***g_3257 = &g_3258;
static union U1 g_3261 = {0xCE7E623FL};/* VOLATILE GLOBAL g_3261 */
static union U1 g_3266 = {18446744073709551608UL};/* VOLATILE GLOBAL g_3266 */
static float * volatile g_3268 = &g_1646[3];/* VOLATILE GLOBAL g_3268 */
static struct S0 g_3292 = {0,8357,441,1129,509,1,843};/* VOLATILE GLOBAL g_3292 */
static float g_3305 = 0x8.F51967p+44;
static int8_t *g_3378 = &g_108;
static int8_t **g_3377 = &g_3378;
static int8_t ** const *g_3376 = &g_3377;
static int8_t ** const **g_3375[1] = {&g_3376};
static int8_t ** const ***g_3374 = &g_3375[0];
static int8_t g_3446[4][6] = {{0xACL,0x50L,1L,1L,0x50L,0xACL},{1L,0x50L,0xACL,1L,0x01L,(-1L)},{(-1L),(-1L),0x48L,(-1L),(-1L),0x50L},{(-1L),1L,(-1L),1L,0xA8L,0xA8L}};
static const int8_t ****g_3489 = &g_2275;
static const int8_t *****g_3488 = &g_3489;
static float * volatile g_3500 = (void*)0;/* VOLATILE GLOBAL g_3500 */
static float * volatile g_3501 = &g_171;/* VOLATILE GLOBAL g_3501 */
static union U1 g_3502 = {18446744073709551611UL};/* VOLATILE GLOBAL g_3502 */
static uint8_t g_3524[4] = {0x35L,0x35L,0x35L,0x35L};
static volatile union U1 g_3549 = {0xCE978764L};/* VOLATILE GLOBAL g_3549 */
static volatile union U1 g_3554 = {0UL};/* VOLATILE GLOBAL g_3554 */
static union U1 g_3596 = {18446744073709551615UL};/* VOLATILE GLOBAL g_3596 */
static volatile union U1 g_3640 = {0x6C62560EL};/* VOLATILE GLOBAL g_3640 */
static volatile int16_t ***g_3694 = (void*)0;
static volatile int16_t **** volatile g_3693 = &g_3694;/* VOLATILE GLOBAL g_3693 */
static union U1 g_3695 = {1UL};/* VOLATILE GLOBAL g_3695 */
static union U1 g_3711[3][1][7] = {{{{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L}}},{{{0x79F7E9DBL},{0UL},{0x79F7E9DBL},{0UL},{0x79F7E9DBL},{0UL},{0x79F7E9DBL}}},{{{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L},{0xFA93EE26L}}}};
static uint16_t ***** volatile g_3715 = &g_1865[0];/* VOLATILE GLOBAL g_3715 */


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static union U1  func_6(int32_t  p_7);
static uint8_t  func_20(int32_t  p_21, float  p_22, uint64_t  p_23, int32_t  p_24, const uint32_t  p_25);
static uint8_t  func_30(int8_t  p_31);
static uint8_t  func_43(uint16_t  p_44, const int32_t * p_45, int32_t * p_46, int64_t  p_47);
static int32_t * func_53(const int32_t  p_54, int32_t  p_55);
static int8_t  func_63(uint16_t * p_64, float  p_65, uint16_t * p_66, uint16_t * p_67);
static uint16_t * func_68(int32_t * p_69, uint32_t  p_70);
static int16_t  func_84(int32_t  p_85, uint32_t  p_86);
static int32_t  func_87(uint16_t  p_88, float  p_89, int32_t * p_90, const uint16_t * p_91);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_1819 g_1272 g_3488 g_3489 g_2275 g_2276 g_115 g_116
 * writes: g_3
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_8 = 0x1A7ECC24L;
    int16_t l_3718 = 0xEB50L;
    int32_t l_3719 = 0xAB7D7282L;
lbl_3722:
    for (g_3 = (-20); (g_3 > 16); ++g_3)
    { /* block id: 3 */
        uint64_t l_3716[8][8] = {{0x0CEB2695C3ED888CLL,18446744073709551615UL,2UL,0UL,0xD57795837D40A87DLL,1UL,1UL,0xD57795837D40A87DLL},{0UL,0xD57795837D40A87DLL,0xD57795837D40A87DLL,0UL,0x0CEB2695C3ED888CLL,0x027785295DE08131LL,1UL,0x1FC860FE65E57170LL},{18446744073709551615UL,0UL,2UL,1UL,2UL,0UL,18446744073709551612UL,0x027785295DE08131LL},{0x1FC860FE65E57170LL,0xD57795837D40A87DLL,18446744073709551612UL,0x027785295DE08131LL,2UL,2UL,0x027785295DE08131LL,18446744073709551612UL},{1UL,1UL,0xD57795837D40A87DLL,0UL,2UL,18446744073709551615UL,0x0CEB2695C3ED888CLL,18446744073709551615UL},{0x1FC860FE65E57170LL,18446744073709551612UL,0UL,18446744073709551612UL,0x1FC860FE65E57170LL,0UL,1UL,18446744073709551615UL},{18446744073709551612UL,2UL,0x0CEB2695C3ED888CLL,0UL,0UL,0x0CEB2695C3ED888CLL,2UL,18446744073709551612UL},{0xD57795837D40A87DLL,0UL,0x0CEB2695C3ED888CLL,0x027785295DE08131LL,1UL,0x1FC860FE65E57170LL,1UL,0x027785295DE08131LL}};
        int32_t *l_3717[3][8][2] = {{{&g_73,&g_163},{&g_73,&g_163},{&g_73,&g_73},{(void*)0,(void*)0},{(void*)0,&g_73},{&g_73,&g_163},{&g_73,&g_163},{&g_73,&g_73}},{{(void*)0,(void*)0},{(void*)0,&g_73},{&g_73,&g_163},{&g_73,&g_163},{&g_73,&g_73},{(void*)0,(void*)0},{(void*)0,&g_73},{&g_73,&g_163}},{{&g_73,&g_163},{&g_73,&g_73},{(void*)0,(void*)0},{(void*)0,&g_73},{&g_73,&g_163},{&g_73,&g_163},{&g_73,&g_73},{(void*)0,(void*)0}}};
        int i, j, k;
        l_3718 ^= (func_6(l_8) , l_3716[6][4]);
        l_3719 |= l_3718;
    }
    l_3719 = (safe_div_func_int8_t_s_s(0x57L, ((255UL != (*****g_3488)) , l_3719)));
    if (l_3719)
        goto lbl_3722;
    return l_8;
}


/* ------------------------------------------ */
/* 
 * reads : g_1819 g_1272
 * writes:
 */
static union U1  func_6(int32_t  p_7)
{ /* block id: 4 */
    uint8_t l_2837[10][5] = {{0x36L,0x61L,0xDBL,0xE0L,0xC5L},{0x30L,0xE0L,0x49L,0x44L,0x44L},{0x36L,254UL,0x36L,0x49L,0UL},{0x7AL,254UL,0UL,0x30L,0x61L},{8UL,0xE0L,0x44L,0xDBL,0x7AL},{0xA5L,0x61L,0UL,0x61L,0xA5L},{0x8BL,0x30L,0x36L,0x61L,0xDBL},{247UL,0x8BL,0x49L,0xDBL,0UL},{0UL,0x36L,0xDBL,0x30L,0xDBL},{0xDBL,0xDBL,254UL,0x49L,0xA5L}};
    union U1 *l_3239 = &g_2014;
    struct S0 **l_3256 = &g_2098;
    struct S0 ***l_3255 = &l_3256;
    const uint32_t ****l_3267 = (void*)0;
    int32_t l_3273 = 0L;
    int32_t l_3275 = 0L;
    int32_t l_3276 = 0x478D874DL;
    int32_t l_3278[3][2][8] = {{{(-2L),(-1L),0x09D1A56AL,(-1L),(-2L),(-2L),(-1L),0x09D1A56AL},{(-2L),(-2L),(-1L),0x09D1A56AL,(-1L),(-2L),(-2L),(-1L)}},{{0L,(-1L),(-1L),0L,1L,0L,(-1L),(-1L)},{(-1L),1L,0x09D1A56AL,0x09D1A56AL,1L,(-1L),1L,0x09D1A56AL}},{{0L,1L,0L,(-1L),(-1L),0L,1L,0L},{(-2L),(-1L),0x09D1A56AL,(-1L),(-2L),(-2L),(-1L),0x09D1A56AL}}};
    float l_3323 = 0x0.0p-1;
    const int16_t *l_3362 = &g_370[0][0];
    const int16_t **l_3361[4];
    int64_t **l_3367 = &g_2861[0];
    int8_t ** const ***l_3379 = &g_3375[0];
    float l_3440 = 0x1.Ep+1;
    int32_t *l_3578 = &g_19;
    uint32_t l_3708 = 0x2E87D565L;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_3361[i] = &l_3362;
    for (p_7 = 0; (p_7 > (-14)); --p_7)
    { /* block id: 7 */
        int64_t l_11[3][6][2] = {{{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL}},{{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL}},{{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL},{0x609B2B534DE6F885LL,0x609B2B534DE6F885LL}}};
        int16_t l_3288 = 0xCF5CL;
        uint32_t **l_3296 = (void*)0;
        uint32_t ***l_3295 = &l_3296;
        int32_t l_3297 = 0xD74373A4L;
        int32_t l_3303 = 0L;
        int32_t l_3304 = 0xE8F77CDAL;
        int32_t l_3310 = 0x40F2CAC1L;
        int32_t l_3312 = 0xD004C21BL;
        int32_t l_3313 = 0xF3E9B3C2L;
        int32_t l_3314 = 0x5C44AF50L;
        int32_t l_3315 = (-1L);
        int32_t l_3318 = 0xF9BADA5AL;
        int32_t l_3319 = 0xFD5487A0L;
        int32_t l_3320 = 1L;
        int32_t l_3321 = 1L;
        int32_t l_3322 = 0L;
        int32_t l_3324 = 0L;
        int32_t l_3325 = 0x377B0C24L;
        int32_t **l_3364 = &g_2563[4][3];
        int32_t * const *l_3366 = &g_391[2][3][3];
        int8_t ** const ***l_3380 = &g_3375[0];
        const uint16_t *** const * const *l_3408 = (void*)0;
        union U1 **l_3517 = (void*)0;
        float l_3521 = (-0x5.Fp+1);
        uint8_t **l_3547 = &g_1102;
        uint8_t l_3658 = 0x7BL;
        uint16_t ***l_3713 = &g_140;
        uint16_t ****l_3712 = &l_3713;
        uint16_t *****l_3714 = &l_3712;
        int i, j, k;
    }
    return (*g_1819);
}


/* ------------------------------------------ */
/* 
 * reads : g_487 g_2276 g_115 g_116 g_162 g_163 g_2098 g_784 g_15 g_140 g_138 g_73 g_1839 g_1840 g_172 g_1946 g_1947 g_1948 g_580 g_171 g_2877 g_1470 g_1471 g_2275 g_1102 g_1100 g_1101 g_321 g_137 g_2097 g_149 g_1815 g_1357 g_949 g_2726 g_3001 g_699 g_33 g_554 g_555 g_556 g_134 g_3028 g_1173 g_501 g_1697 g_1698 g_1796 g_1560 g_2491 g_948 g_1701 g_1700 g_1699 g_108 g_3016.f5 g_1379 g_3127 g_2522 g_1646 g_106 g_2677 g_2836 g_542.f0 g_370 g_3152 g_791 g_511.f3 g_3097 g_3016.f0 g_1496.f5 g_769.f0 g_1971 g_2203 g_542.f3 g_502 g_3191 g_309 g_2839 g_1496.f0
 * writes: g_487 g_2861 g_2757 g_106 g_134 g_171 g_149 g_163 g_784.f3 g_391 g_501 g_502 g_1646 g_33 g_309 g_108 g_370 g_2839 g_73 g_1796 g_1702 g_2677 g_2098 g_949 g_3105 g_3106 g_1971 g_48 g_699 g_3190
 */
static uint8_t  func_20(int32_t  p_21, float  p_22, uint64_t  p_23, int32_t  p_24, const uint32_t  p_25)
{ /* block id: 1204 */
    uint16_t l_2842[10] = {0xC7F4L,0xBC18L,0x9372L,0x9372L,0xBC18L,0xC7F4L,0xBC18L,0x9372L,0x9372L,0xBC18L};
    int32_t l_2878[5];
    float l_2883 = 0x1.7p-1;
    int8_t *l_2984 = (void*)0;
    int8_t **l_2983 = &l_2984;
    int8_t ***l_2982 = &l_2983;
    int8_t ****l_2981 = &l_2982;
    int8_t *****l_2980[10] = {&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981};
    uint64_t *l_2995 = (void*)0;
    struct S0 *l_3015 = &g_3016;
    uint8_t l_3078 = 1UL;
    int32_t ****l_3124 = (void*)0;
    uint32_t l_3131 = 18446744073709551611UL;
    int32_t ****l_3211 = (void*)0;
    int32_t *l_3218 = &g_502;
    int i;
    for (i = 0; i < 5; i++)
        l_2878[i] = 0x7D95BD16L;
    for (g_487 = (-6); (g_487 != 51); g_487 = safe_add_func_uint8_t_u_u(g_487, 3))
    { /* block id: 1207 */
        uint64_t l_2843 = 1UL;
        int64_t *l_2859 = &g_699[1][1][0];
        int32_t l_2881 = 0xC8FB3084L;
        int32_t l_2882[10] = {0xBECE1A06L,0xBECE1A06L,0xBECE1A06L,0xBECE1A06L,0xBECE1A06L,0xBECE1A06L,0xBECE1A06L,0xBECE1A06L,0xBECE1A06L,0xBECE1A06L};
        uint8_t l_2888[7][5] = {{4UL,0x7AL,4UL,0UL,9UL},{4UL,0x7AL,4UL,4UL,0x7AL},{1UL,255UL,0x70L,0x7AL,9UL},{255UL,4UL,0x70L,0UL,0x70L},{9UL,9UL,4UL,1UL,255UL},{255UL,255UL,4UL,1UL,1UL},{1UL,0xDEL,1UL,0UL,0UL}};
        const uint32_t *l_2938 = &g_2638;
        const uint32_t **l_2937 = &l_2938;
        const uint32_t ***l_2936 = &l_2937;
        const uint32_t ****l_2935[2];
        uint64_t l_2948 = 0xDD222846D06BB92DLL;
        uint8_t ** const *l_2959 = &g_1101;
        uint8_t ** const **l_2958 = &l_2959;
        uint8_t ** const ***l_2957 = &l_2958;
        int32_t l_2965 = 8L;
        uint16_t l_2966 = 9UL;
        int8_t *****l_2985[8] = {&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981,&l_2981};
        union U1 **l_3021 = &g_889;
        int64_t **l_3051 = &l_2859;
        int64_t ***l_3050[7] = {&l_3051,&l_3051,&l_3051,&l_3051,&l_3051,&l_3051,&l_3051};
        int i, j;
        for (i = 0; i < 2; i++)
            l_2935[i] = &l_2936;
        l_2842[5] = 0x7FD6896CL;
        if (l_2843)
        { /* block id: 1209 */
            int64_t **l_2860[7] = {&l_2859,&l_2859,&l_2859,&l_2859,&l_2859,&l_2859,&l_2859};
            uint16_t *** const l_2868 = (void*)0;
            uint16_t ***l_2869 = &g_140;
            uint16_t ****l_2870 = (void*)0;
            uint16_t ****l_2871 = &l_2869;
            int16_t *l_2874 = &g_106[0];
            int32_t l_2884 = 0x2BA43773L;
            int32_t l_2885[4][7][9] = {{{1L,0x63E2073EL,8L,0x784F828AL,(-1L),9L,9L,(-10L),1L},{0x996815BBL,0x087D7C6BL,1L,0x06A86FCCL,(-1L),0x06A86FCCL,1L,0x087D7C6BL,0x996815BBL},{1L,0x98C6C651L,0xC10E96BCL,1L,0x436571BDL,6L,0L,0x9E6B39C1L,0x63E2073EL},{0x6DB0C127L,0xE2166E6FL,0x9819BB82L,0x04A75D80L,0x6188EEABL,(-1L),0x087D7C6BL,0xDD8CB413L,0xB03C8F09L},{1L,0x436571BDL,0L,0x1A06F55DL,0x05901C2DL,0x98C6C651L,(-7L),0L,9L},{0x996815BBL,0x64FD26B8L,0x20A6EDD0L,0x588A0266L,0x31D1CE11L,1L,6L,0x63AC1735L,0x087D7C6BL},{1L,1L,0xE727781CL,4L,1L,(-1L),0x93ED212EL,0L,0L}},{{0x1DDEFC97L,(-3L),0L,(-1L),0xFFE3B7CFL,(-1L),(-1L),(-9L),0xFFE3B7CFL},{(-7L),1L,9L,1L,(-7L),0x6908F95DL,(-8L),1L,0L},{(-3L),(-1L),0xDD8CB413L,0xE2166E6FL,0x87BFC06CL,0x087D7C6BL,0x63AC1735L,6L,1L},{0x15136AB8L,1L,0x05901C2DL,0L,0xCE32A651L,0x6908F95DL,(-1L),6L,1L},{0x6DB0C127L,0x9819BB82L,(-10L),0x588A0266L,1L,0xFFE3B7CFL,0x6188EEABL,0L,6L},{8L,(-7L),1L,0x950938F2L,0x027E6B40L,0x1A06F55DL,5L,(-1L),0x68D786CBL},{0x64F95456L,0L,0x3825F889L,0x6188EEABL,0x20A6EDD0L,0L,0x06A86FCCL,0x9819BB82L,0x55A258CDL}},{{0xE89C65E4L,(-10L),1L,1L,0xC10E96BCL,0xC10E96BCL,1L,1L,(-10L)},{0x31D1CE11L,(-4L),0x1DDEFC97L,0xAF66FAEEL,0xE2166E6FL,(-9L),0xE5F623F2L,0x996815BBL,0x6DB0C127L},{0L,0x05901C2DL,0x950938F2L,1L,1L,0x05741289L,0xB0D49180L,0xDD89E2EAL,(-3L)},{0L,(-4L),0x175B527AL,0L,1L,0L,4L,(-8L),1L},{0L,(-10L),0xE727781CL,1L,6L,(-7L),0x6A19AD05L,0x93ED212EL,0x05741289L},{0L,0L,(-3L),(-3L),0x0052FD0EL,(-1L),0xAF66FAEEL,0x63AC1735L,0x20A6EDD0L},{0x950938F2L,(-7L),1L,1L,0L,1L,0x9E6B39C1L,6L,0x9E6B39C1L}},{{(-1L),0x9819BB82L,0xFFE3B7CFL,0xFFE3B7CFL,0x9819BB82L,(-1L),(-8L),0x64F95456L,(-1L)},{0x6A19AD05L,1L,0L,(-7L),8L,0x9E6B39C1L,1L,1L,0xE727781CL},{0x2DC59FE1L,(-1L),0xFCE58D60L,(-1L),0L,0x3825F889L,(-8L),0xDD8CB413L,0x06A86FCCL},{0xC10E96BCL,1L,0xB0D49180L,9L,0x93ED212EL,0L,0x9E6B39C1L,(-7L),0L},{(-1L),(-10L),0L,3L,0L,0x55A258CDL,0xAF66FAEEL,1L,(-1L)},{6L,1L,6L,(-3L),1L,0x2B337075L,0x6A19AD05L,0L,0x15136AB8L},{(-1L),(-1L),0xE2166E6FL,1L,4L,3L,4L,1L,0xE2166E6FL}}};
            int32_t l_2886 = (-3L);
            int8_t l_2887[9][10][2] = {{{0xF5L,(-10L)},{0xF4L,0L},{0x98L,0x98L},{0L,0xF4L},{(-10L),0L},{0xADL,1L},{7L,0xADL},{0L,(-10L)},{0L,0xADL},{7L,1L}},{{0xADL,0L},{(-1L),0L},{0x66L,1L},{1L,0x66L},{0L,(-1L)},{0L,0xADL},{1L,7L},{0xADL,0L},{(-10L),0L},{0xADL,7L}},{{1L,0xADL},{0L,(-1L)},{0L,0x66L},{1L,1L},{0x66L,0L},{(-1L),0L},{0xADL,1L},{7L,0xADL},{0L,(-10L)},{0L,0xADL}},{{7L,1L},{0xADL,0L},{(-1L),0L},{0x66L,1L},{1L,0x66L},{0L,(-1L)},{0L,0xADL},{1L,7L},{0xADL,0L},{(-10L),0L}},{{0xADL,7L},{1L,0xADL},{0L,(-1L)},{0L,0x66L},{1L,1L},{0x66L,0L},{(-1L),0L},{0xADL,1L},{7L,0xADL},{0L,(-10L)}},{{0L,0xADL},{7L,1L},{0xADL,0L},{(-1L),0L},{0x66L,1L},{1L,0x66L},{0L,(-1L)},{0L,0xADL},{1L,7L},{0xADL,0L}},{{(-10L),0L},{0xADL,7L},{1L,0xADL},{0L,(-1L)},{0L,0x66L},{1L,1L},{0x66L,0L},{(-1L),0L},{0xADL,1L},{7L,0xADL}},{{0L,(-10L)},{0L,0xADL},{7L,1L},{0xADL,0L},{(-1L),0L},{0x66L,1L},{1L,0x66L},{0L,(-1L)},{0L,0xADL},{1L,7L}},{{0xADL,0L},{(-10L),0L},{0xADL,7L},{1L,0xADL},{0L,(-1L)},{0L,0x66L},{1L,1L},{0x66L,0L},{(-1L),0L},{0xADL,1L}}};
            uint32_t *l_2905 = (void*)0;
            uint32_t **l_2904 = &l_2905;
            uint32_t ****l_2939 = &g_2773[4][2][0];
            int8_t l_2989[6];
            uint8_t l_3003 = 0x4CL;
            uint32_t l_3005 = 18446744073709551615UL;
            struct S0 **l_3008 = &g_2098;
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_2989[i] = (-1L);
            if ((safe_lshift_func_int8_t_s_u((((!((safe_add_func_int32_t_s_s((l_2843 | p_24), (((safe_rshift_func_int16_t_s_s(((safe_mod_func_uint8_t_u_u(((((**g_140) = (safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((1L && ((*l_2874) = ((g_2757 = (((safe_add_func_int64_t_s_s(0L, ((g_2861[2] = l_2859) != ((safe_add_func_int32_t_s_s((((((safe_sub_func_uint16_t_u_u((safe_div_func_int16_t_s_s((l_2842[1] , ((l_2868 == ((*l_2871) = l_2869)) >= ((safe_add_func_uint64_t_u_u((p_23 = ((**g_2276) & 0x03L)), p_21)) | (*g_162)))), 0xF009L)), 0x1D7AL)) , (*g_2098)) , 9UL) | l_2843) >= g_15), p_25)) , (void*)0)))) ^ 0x2E43CE176B3DD126LL) , l_2842[5])) & 0x3F5649298EB77F97LL))), 1)), 0xE90CL))) != g_73) > 1UL), p_21)) < l_2843), 2)) <= p_24) , (**g_1839)))) <= p_25)) | 1L) , l_2842[5]), l_2843)))
            { /* block id: 1216 */
                int32_t *l_2879 = &g_163;
                int32_t *l_2880[1][5][4] = {{{&g_36,&g_3,&g_36,&g_172[4][1]},{&l_2878[1],&g_3,&g_33[0][8],(void*)0},{&g_3,(void*)0,(void*)0,&g_3},{&g_36,(void*)0,(void*)0,&g_172[4][1]},{&g_3,&l_2878[1],&g_33[0][8],&l_2878[1]}}};
                uint32_t ***l_2906 = &l_2904;
                uint32_t **l_2964 = (void*)0;
                float l_3002 = (-0x8.Dp-1);
                int i, j, k;
                (*g_2877) = ((safe_lshift_func_int16_t_s_s((**g_1946), 6)) , (*g_580));
                --l_2888[0][1];
                if (((p_21 ^ ((safe_div_func_int16_t_s_s(((*g_1470) <= ((safe_rshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u((safe_unary_minus_func_int16_t_s(l_2886)), (safe_add_func_uint32_t_u_u((safe_mod_func_int8_t_s_s((&g_1470 == ((*l_2906) = (p_21 , l_2904))), l_2887[1][9][0])), l_2842[0])))), (!(safe_lshift_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s((l_2882[3] & (***g_2275)), l_2842[5])), 2))))) != p_21)), 0xA304L)) < l_2878[0])) & 0x72427EA4L))
                { /* block id: 1220 */
                    float l_2912 = 0xB.4AE4E0p+87;
                    int32_t l_2913 = (-1L);
                    if (p_25)
                        break;
                    l_2913 ^= (-1L);
                    if (p_23)
                    { /* block id: 1223 */
                        uint16_t l_2916 = 65535UL;
                        l_2916 ^= (safe_lshift_func_uint8_t_u_s(((*g_1102) = p_23), 1));
                    }
                    else
                    { /* block id: 1226 */
                        uint32_t l_2962[8];
                        uint32_t *l_2963[2][1];
                        int32_t l_2967[8] = {0x977C9B43L,0x977C9B43L,0x977C9B43L,0x977C9B43L,0x977C9B43L,0x977C9B43L,0x977C9B43L,0x977C9B43L};
                        int i, j;
                        for (i = 0; i < 8; i++)
                            l_2962[i] = 6UL;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_2963[i][j] = (void*)0;
                        }
                        l_2967[6] = ((((safe_sub_func_uint32_t_u_u((safe_lshift_func_int8_t_s_s((((safe_add_func_int32_t_s_s(((*l_2879) = 1L), ((safe_sub_func_int32_t_s_s(((!((((((p_21 & (l_2965 = ((((g_784.f3 = (safe_add_func_uint8_t_u_u((!((safe_rshift_func_int16_t_s_u((safe_div_func_uint64_t_u_u(p_24, (((safe_mul_func_uint8_t_u_u(((***g_1100) = (l_2935[0] != l_2939)), (((safe_lshift_func_uint8_t_u_u((((safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_u((safe_sub_func_int16_t_s_s(l_2948, (safe_mod_func_int64_t_s_s((((safe_sub_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_u((p_24 , (l_2957 == (((safe_rshift_func_uint16_t_u_s(((***g_321) = l_2842[5]), p_21)) >= 0xEE46C749L) , (void*)0))), 6)), p_21)), p_24)) || (-1L)) >= 18446744073709551609UL), p_24)))), 3)), 6)) == 9L) < 0x08L), 3)) , l_2913) || p_25))) | p_25) | p_23))), p_24)) , 253UL)), l_2962[3]))) , (void*)0) != l_2964) != l_2887[1][9][0]))) || l_2913) != l_2913) & l_2962[3]) , p_21) , (*g_1947))) , p_25), 0x3A6CB537L)) & g_784.f2))) < p_23) , p_24), (***g_2275))), l_2887[1][9][0])) > l_2843) | l_2887[1][9][0]) , l_2966);
                        return p_24;
                    }
                }
                else
                { /* block id: 1235 */
                    int64_t l_2978[5][2][10] = {{{0x71BF7D0A03BE042DLL,0x0A51EEE18590FC7ALL,(-1L),0x25A1662171E7296BLL,0x6085D86353B60F8FLL,1L,0x6085D86353B60F8FLL,0x25A1662171E7296BLL,(-1L),0x0A51EEE18590FC7ALL},{0x764A33688D7ACD5CLL,0L,1L,0x0A51EEE18590FC7ALL,0x6085D86353B60F8FLL,0x25C6C5A724D8DF6ALL,0x25C6C5A724D8DF6ALL,0x6085D86353B60F8FLL,0x0A51EEE18590FC7ALL,1L}},{{0x6085D86353B60F8FLL,0x6085D86353B60F8FLL,0x71BF7D0A03BE042DLL,0x764A33688D7ACD5CLL,0x80DF472566D84860LL,0x25C6C5A724D8DF6ALL,(-1L),0x25C6C5A724D8DF6ALL,0x80DF472566D84860LL,0x764A33688D7ACD5CLL},{0x764A33688D7ACD5CLL,1L,0x764A33688D7ACD5CLL,0x25C6C5A724D8DF6ALL,0x25A1662171E7296BLL,1L,(-1L),(-1L),1L,0x25A1662171E7296BLL}},{{0x71BF7D0A03BE042DLL,0x6085D86353B60F8FLL,0x6085D86353B60F8FLL,0x71BF7D0A03BE042DLL,0x764A33688D7ACD5CLL,0x80DF472566D84860LL,0x25C6C5A724D8DF6ALL,(-1L),0x25C6C5A724D8DF6ALL,0x80DF472566D84860LL},{1L,0L,0x764A33688D7ACD5CLL,0L,1L,0x0A51EEE18590FC7ALL,0x6085D86353B60F8FLL,0x25C6C5A724D8DF6ALL,0x25C6C5A724D8DF6ALL,0x6085D86353B60F8FLL}},{{(-1L),0x0A51EEE18590FC7ALL,0x71BF7D0A03BE042DLL,0x71BF7D0A03BE042DLL,0x0A51EEE18590FC7ALL,(-1L),0x25A1662171E7296BLL,0x6085D86353B60F8FLL,1L,0x6085D86353B60F8FLL},{0L,0x71BF7D0A03BE042DLL,1L,0x25C6C5A724D8DF6ALL,1L,0x71BF7D0A03BE042DLL,0L,0x25A1662171E7296BLL,0x80DF472566D84860LL,0x80DF472566D84860LL}},{{0L,0x80DF472566D84860LL,(-1L),0x764A33688D7ACD5CLL,0x764A33688D7ACD5CLL,(-1L),0x80DF472566D84860LL,0L,0x0A51EEE18590FC7ALL,0x25A1662171E7296BLL},{(-1L),0x80DF472566D84860LL,0L,0x0A51EEE18590FC7ALL,0x25A1662171E7296BLL,0x0A51EEE18590FC7ALL,0L,0x80DF472566D84860LL,(-1L),0x764A33688D7ACD5CLL}}};
                    int32_t *l_2979 = &g_163;
                    union U1 ****l_3000 = &g_2039[8][0];
                    union U1 *****l_2999 = &l_3000;
                    float *l_3009[2];
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_3009[i] = (void*)0;
                    if (((((**g_2097) , ((((safe_div_func_int64_t_s_s(0L, (safe_rshift_func_int8_t_s_s(((252UL == (***g_2275)) == (safe_mul_func_int8_t_s_s(((safe_lshift_func_int8_t_s_u((safe_rshift_func_uint16_t_u_u(l_2978[4][1][8], 6)), (1UL ^ p_23))) < (l_2842[5] , ((((((p_23 <= p_21) ^ p_23) && l_2978[1][1][2]) , 0UL) >= p_23) | 0x64C1BCE0BD1878B2LL))), l_2882[3]))), p_24)))) < p_21) >= (*g_1947)) || (*g_1470))) >= (*g_1102)) != l_2885[0][5][4]))
                    { /* block id: 1236 */
                        (**g_1815) = l_2979;
                        l_2985[7] = l_2980[8];
                    }
                    else
                    { /* block id: 1239 */
                        int32_t *l_2988 = &g_502;
                        float *l_2994 = &g_1646[3];
                        int32_t l_3004 = 1L;
                        l_3004 = (p_22 = (l_3003 = ((safe_add_func_float_f_f(((((*l_2988) = ((*g_949) = 0xA5284E26L)) , l_2989[0]) , (((0x3.4A2DFFp-93 > ((safe_div_func_float_f_f(((*g_2726) >= (safe_div_func_float_f_f((((*l_2994) = (*l_2879)) > (l_2878[0] = (((&p_23 != l_2995) != (safe_div_func_float_f_f(((!((l_2999 == g_3001) <= l_3002)) < (*l_2979)), 0xC.F5A64Ap+86))) == p_25))), 0x7.3p-1))), 0x2.08BB04p+15)) < g_699[2][2][0])) , p_24) >= p_22)), (*l_2979))) != l_2888[0][1])));
                        ++l_3005;
                        if (p_23)
                            continue;
                        return (**g_1101);
                    }
                    l_2882[3] ^= ((0x31A77267L & 0L) > p_21);
                    p_22 = (l_3008 != &g_2098);
                }
            }
            else
            { /* block id: 1254 */
                float *l_3010[8];
                int i;
                for (i = 0; i < 8; i++)
                    l_3010[i] = &l_2883;
                p_22 = (p_22 >= (*g_580));
                if (p_21)
                    break;
                for (l_2884 = 0; (l_2884 < 11); l_2884 = safe_add_func_int64_t_s_s(l_2884, 7))
                { /* block id: 1259 */
                    for (p_24 = 9; (p_24 >= 0); p_24 -= 1)
                    { /* block id: 1262 */
                        int i, j;
                        g_33[p_24][p_24] &= 0xE92D2D95L;
                    }
                }
                if (l_3003)
                    break;
            }
        }
        else
        { /* block id: 1268 */
            struct S0 *l_3013 = &g_542;
            struct S0 **l_3014[1][4];
            int32_t l_3031 = (-2L);
            int32_t l_3037 = 0x2F6F129EL;
            uint16_t ***l_3071 = &g_140;
            uint16_t ****l_3070 = &l_3071;
            float *l_3110 = &g_48;
            float *l_3130[1][5] = {{&g_171,&g_171,&g_171,&g_171,&g_171}};
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 4; j++)
                    l_3014[i][j] = &l_3013;
            }
            l_3015 = l_3013;
            for (g_309 = 0; (g_309 > 16); g_309++)
            { /* block id: 1272 */
                float l_3035[10][8][3] = {{{0xC.496931p-12,(-0x4.Cp+1),0x1.Ap+1},{0x4.2C9449p+47,0xE.45EB86p-26,0xE.E27B44p+20},{0x9.6p+1,(-0x4.Cp+1),(-0x10.1p+1)},{0x4.2C9449p+47,0x4.2C9449p+47,0xE.E27B44p+20},{0xC.496931p-12,(-0x4.Cp+1),0x1.Ap+1},{0x4.2C9449p+47,0xE.45EB86p-26,0xE.E27B44p+20},{0x9.6p+1,(-0x4.Cp+1),(-0x10.1p+1)},{0x4.2C9449p+47,0x4.2C9449p+47,0xE.E27B44p+20}},{{0xC.496931p-12,(-0x4.Cp+1),0x1.Ap+1},{0x4.2C9449p+47,0xE.45EB86p-26,0xE.E27B44p+20},{0x9.6p+1,(-0x4.Cp+1),(-0x10.1p+1)},{0x4.2C9449p+47,0x4.2C9449p+47,0xE.E27B44p+20},{0xC.496931p-12,(-0x4.Cp+1),0x1.Ap+1},{0x4.2C9449p+47,0xE.45EB86p-26,0xE.E27B44p+20},{0x9.6p+1,(-0x4.Cp+1),(-0x10.1p+1)},{0x4.2C9449p+47,0x4.2C9449p+47,0xE.E27B44p+20}},{{0xC.496931p-12,(-0x4.Cp+1),0x1.Ap+1},{0x4.2C9449p+47,0xE.45EB86p-26,0xE.E27B44p+20},{0x9.6p+1,(-0x4.Cp+1),(-0x10.1p+1)},{0x4.2C9449p+47,0x4.2C9449p+47,0xE.E27B44p+20},{0xC.496931p-12,(-0x4.Cp+1),0x1.Ap+1},{0x4.2C9449p+47,0xE.45EB86p-26,0xE.E27B44p+20},{0x9.6p+1,(-0x4.Cp+1),(-0x10.1p+1)},{0x4.2C9449p+47,0x4.2C9449p+47,0xE.E27B44p+20}},{{0xC.496931p-12,(-0x4.Cp+1),0x1.Ap+1},{0x4.2C9449p+47,0xE.45EB86p-26,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26},{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26}},{{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26},{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26}},{{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26},{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26}},{{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26},{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26}},{{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26},{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26}},{{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26},{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26}},{{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26},{0x3.17B286p+96,0x1.5B31ADp+61,0xC.496931p-12},{(-0x1.3p+1),0x5.CA7A6Cp-1,0xE.45EB86p-26},{(-0x1.8p+1),0x1.5B31ADp+61,0x9.6p+1},{(-0x1.3p+1),(-0x1.3p+1),0xE.45EB86p-26}}};
                int32_t l_3036 = 1L;
                struct S0 **l_3044 = &g_2098;
                uint16_t ***l_3069 = &g_140;
                uint16_t ****l_3068 = &l_3069;
                float l_3085 = 0x2.86A31Ap-51;
                const int32_t *l_3096 = &g_3097;
                const int32_t **l_3095 = &l_3096;
                struct S0 ***l_3104 = &l_3014[0][2];
                struct S0 ****l_3103[10] = {(void*)0,&l_3104,&l_3104,(void*)0,(void*)0,(void*)0,&l_3104,&l_3104,(void*)0,(void*)0};
                float *l_3107 = &g_1971;
                int i, j, k;
                for (g_108 = (-13); (g_108 >= (-10)); ++g_108)
                { /* block id: 1275 */
                    uint32_t l_3034 = 4294967288UL;
                    struct S0 **l_3045 = &l_3013;
                    int64_t **l_3049 = &l_2859;
                    int64_t ***l_3048[5][8][3] = {{{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,(void*)0},{(void*)0,(void*)0,&l_3049},{(void*)0,(void*)0,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,(void*)0,&l_3049}},{{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,(void*)0},{&l_3049,&l_3049,&l_3049},{(void*)0,&l_3049,(void*)0},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049}},{{&l_3049,(void*)0,(void*)0},{&l_3049,&l_3049,&l_3049},{(void*)0,&l_3049,(void*)0},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,(void*)0},{&l_3049,&l_3049,&l_3049},{&l_3049,(void*)0,&l_3049}},{{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,(void*)0},{&l_3049,&l_3049,&l_3049},{&l_3049,(void*)0,&l_3049},{(void*)0,&l_3049,&l_3049}},{{(void*)0,&l_3049,&l_3049},{(void*)0,&l_3049,(void*)0},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{(void*)0,&l_3049,&l_3049},{&l_3049,&l_3049,&l_3049},{(void*)0,(void*)0,&l_3049}}};
                    int32_t *l_3077[8][3];
                    int i, j, k;
                    for (i = 0; i < 8; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_3077[i][j] = &l_3036;
                    }
                    if ((((l_3021 != l_3021) , (safe_lshift_func_int16_t_s_s(((p_25 & ((*g_162) = ((safe_mod_func_uint32_t_u_u((((***g_554) && (((((safe_lshift_func_uint8_t_u_s((1UL >= (((g_3028 , ((!((!((l_2878[0] = (l_3031 , ((((**g_1946) , (safe_rshift_func_uint8_t_u_s(((l_3034 != l_3031) || 0xE08685C50A38239ALL), 3))) && l_3031) == (-9L)))) < (*g_1470))) & l_3034)) & p_24)) && l_3036) | 0x68A707459B70D916LL)), p_25)) != 0x73L) == l_2842[5]) < 1L) , p_24)) , l_3037), 7UL)) , 0x21E48553L))) , (**g_1946)), p_23))) ^ p_23))
                    { /* block id: 1278 */
                        struct S0 ***l_3046 = &l_3014[0][3];
                        int16_t *l_3047 = &g_106[1];
                        int16_t *l_3052 = &g_370[3][0];
                        int16_t *l_3053 = &g_2839;
                        int32_t *l_3054 = &g_73;
                        const int64_t *l_3058 = (void*)0;
                        const int64_t **l_3057 = &l_3058;
                        uint32_t *l_3072[10];
                        int32_t l_3073 = 0L;
                        int32_t *l_3074 = &g_1796;
                        int i;
                        for (i = 0; i < 10; i++)
                            l_3072[i] = &g_2638;
                        (*l_3054) = ((safe_lshift_func_uint16_t_u_s((65535UL >= l_2842[5]), 13)) ^ (safe_lshift_func_uint16_t_u_u(((**g_1173) , ((***g_321) = (safe_rshift_func_int16_t_s_s(((*l_3047) = ((l_3044 != ((*l_3046) = l_3045)) != p_25)), ((l_3048[3][3][0] != l_3050[2]) | (l_2878[0] = (((*l_3053) = ((*l_3052) = (0x35L ^ (**g_1101)))) > p_21))))))), l_3037)));
                        (*l_3074) |= (safe_lshift_func_int16_t_s_s(((*g_1697) == ((*l_3057) = (void*)0)), (safe_lshift_func_uint8_t_u_u(((l_3073 ^= (((safe_rshift_func_uint16_t_u_s((~((*l_3054) >= ((*g_1102) = ((safe_lshift_func_uint16_t_u_u((1UL >= (safe_div_func_uint32_t_u_u((*g_1470), p_24))), 12)) && 3UL)))), ((l_3068 == l_3070) == (-1L)))) < l_2878[4]) , 0UL)) , (*l_3054)), 6))));
                    }
                    else
                    { /* block id: 1290 */
                        return (***g_1100);
                    }
                    for (g_1702 = 25; (g_1702 <= 34); g_1702 = safe_add_func_int64_t_s_s(g_1702, 3))
                    { /* block id: 1295 */
                        if ((*g_1560))
                            break;
                    }
                    l_3078++;
                }
                for (g_2677 = (-22); (g_2677 != (-21)); g_2677++)
                { /* block id: 1302 */
                    for (g_1796 = 0; (g_1796 > 4); g_1796 = safe_add_func_int32_t_s_s(g_1796, 9))
                    { /* block id: 1305 */
                        if (l_3036)
                            break;
                        (*l_3044) = (*g_2097);
                    }
                }
                p_22 = (((*l_3107) = (safe_mul_func_float_f_f(0x4.3D2316p+51, (((!(safe_sub_func_float_f_f((safe_div_func_float_f_f(((-0x1.Fp-1) != ((safe_sub_func_float_f_f(((l_2842[5] <= ((((*g_948) = (**g_2491)) != ((*l_3095) = l_2938)) >= (((p_24 , ((p_22 == ((+((safe_mul_func_float_f_f(((safe_mul_func_float_f_f(((((g_3106 = (g_3105 = (void*)0)) != (void*)0) == (**g_1697)) , p_21), p_23)) <= 0xA.A26593p+47), p_23)) <= l_3037)) == g_108)) >= l_3036)) > 0x6.3p+1) < p_25))) == g_3016.f5), p_22)) == l_3036)), l_2842[9])), g_1379))) < p_24) > g_784.f5)))) > l_3031);
            }
            l_3031 = (safe_add_func_float_f_f(((*g_2726) = ((((*l_3110) = p_23) > ((safe_sub_func_float_f_f((safe_mul_func_float_f_f(l_2878[0], 0x6.9p-1)), ((l_2882[3] = ((safe_div_func_float_f_f(((safe_div_func_float_f_f(((!((safe_div_func_float_f_f((safe_sub_func_float_f_f((l_3124 != (void*)0), (safe_sub_func_float_f_f((p_22 = (-0x1.Bp+1)), ((l_3131 = (((((g_3127 , ((*g_2522) == (safe_add_func_float_f_f(l_2948, p_24)))) > l_3031) < g_106[2]) == g_2677) < g_2836)) >= l_2882[3]))))), p_23)) <= (-0x10.Bp+1))) < (-0x1.Ap-1)), 0xE.0F0C8Bp+19)) == 0xE.834C92p+30), g_542.f0)) == p_24)) <= 0x3.4p-1))) < 0xD.FDACF3p-59)) == g_370[3][0])), l_2888[0][1]));
        }
    }
    for (l_3131 = 1; (l_3131 <= 6); l_3131 += 1)
    { /* block id: 1327 */
        uint8_t **l_3141 = &g_1102;
        int32_t l_3158 = 0xA3E0CE39L;
        uint32_t l_3161 = 3UL;
        int32_t * const ****l_3194 = (void*)0;
        uint32_t ****l_3219[3];
        int i;
        for (i = 0; i < 3; i++)
            l_3219[i] = &g_2773[5][2][0];
        for (g_73 = 4; (g_73 >= 0); g_73 -= 1)
        { /* block id: 1330 */
            uint8_t ****l_3151 = &g_1100;
            uint8_t *****l_3150 = &l_3151;
            float *l_3159 = &g_171;
            int32_t l_3160 = 0x47F38A27L;
            int64_t *l_3184 = &g_699[0][2][0];
            int32_t l_3185 = 0x1F0CBAB6L;
            (*g_2203) = (+(safe_div_func_float_f_f(((safe_rshift_func_int8_t_s_s(0L, 7)) , (((safe_sub_func_float_f_f(((((safe_sub_func_float_f_f((l_3141 != (void*)0), 0x2.EB0E9Ep-18)) > ((((safe_add_func_float_f_f(((safe_div_func_float_f_f((safe_add_func_float_f_f(((safe_add_func_float_f_f(((void*)0 != l_3150), 0x9.FAE313p+62)) == ((((g_3152 > ((*l_3159) = ((safe_add_func_float_f_f(((((safe_add_func_float_f_f((+(-0x1.5p-1)), (*g_791))) <= g_33[3][0]) > l_2842[0]) < 0x0.6p-1), l_3158)) <= g_511.f3))) < g_3097) < g_3016.f0) < g_1496.f5)), g_149)), p_24)) != g_769.f0), l_3158)) >= l_3160) == g_172[4][1]) > p_24)) <= p_22) != p_23), 0x3.018E8Bp+17)) < p_25) >= g_1971)), l_3161)));
            (*l_3159) = (safe_mul_func_float_f_f(p_21, (!(safe_sub_func_float_f_f((safe_mul_func_float_f_f((0x1.6p+1 <= (((*g_2203) != ((l_2878[1] , (((((safe_lshift_func_uint16_t_u_u(((***g_321) ^= ((safe_mod_func_int8_t_s_s((l_3160 | ((safe_mod_func_int64_t_s_s(((*l_3184) = (0x672FDFCC5255AE4CLL && (safe_sub_func_int32_t_s_s((safe_lshift_func_int8_t_s_s((((**l_3141) = (*g_1102)) <= (safe_mod_func_uint16_t_u_u(((safe_add_func_int64_t_s_s((p_25 > 18446744073709551611UL), l_3161)) < l_2842[2]), g_3097))), p_21)), (*g_1470))))), 1L)) | 0x37L)), (-1L))) >= (*g_1470))), 3)) & l_3185) , p_23) <= l_3185) <= 0x8.6p+1)) != g_542.f3)) >= g_502)), p_25)), 0x1.3p-1)))));
            for (g_309 = 0; (g_309 <= 1); g_309 += 1)
            { /* block id: 1339 */
                uint32_t ****l_3186 = &g_2773[0][3][1];
                uint32_t *****l_3187 = (void*)0;
                uint32_t *****l_3188 = (void*)0;
                int32_t *l_3199 = (void*)0;
                int32_t *l_3200 = &l_2878[1];
                int i, j, k;
                (*g_3191) = l_3186;
                (*l_3200) &= (((((g_699[(g_309 + 1)][(g_309 + 2)][g_309] || ((safe_lshift_func_uint8_t_u_u(((***g_1100) = ((void*)0 != l_3194)), 6)) == ((safe_rshift_func_uint16_t_u_u(((0x14DEB2C4E7D89DB2LL > g_172[(l_3131 + 1)][g_309]) > 0x48L), 9)) && (g_172[(l_3131 + 3)][g_309] | g_172[(l_3131 + 3)][g_309])))) , (((*l_3184) = g_172[(l_3131 + 3)][g_309]) && 0L)) | p_23) > p_25) > 0x5BB2A744L);
            }
        }
        for (g_502 = 0; (g_502 <= 6); g_502 += 1)
        { /* block id: 1348 */
            float l_3203 = 0x4.A50CA4p-49;
            struct S0 *l_3214 = &g_3215;
            int32_t l_3221 = 0x7FBDDA5FL;
            for (g_134 = 0; (g_134 <= 6); g_134 += 1)
            { /* block id: 1351 */
                uint32_t ****l_3220 = &g_2773[0][7][0];
                const int32_t l_3222 = 0xE556A0C0L;
                int32_t l_3223 = 0x0BC723A7L;
                l_2878[0] |= ((l_3221 = (0xEB6EE1DD4A49AB7DLL >= ((safe_mul_func_uint16_t_u_u(((p_21 & (safe_sub_func_uint16_t_u_u(((*g_1102) | (safe_lshift_func_int8_t_s_s(((((+((safe_add_func_float_f_f((((void*)0 == l_3211) == (*g_791)), ((((safe_add_func_uint64_t_u_u(((l_3214 != l_3214) >= (safe_lshift_func_int16_t_s_s((4UL > (*g_1470)), g_2839))), g_1496.f0)) >= 1UL) , &p_24) != l_3218))) >= p_21)) , l_3219[1]) == l_3220) == p_25), p_24))), p_21))) <= l_3221), p_24)) <= l_3222))) , l_3223);
            }
            l_2878[0] &= (*g_162);
        }
    }
    for (g_487 = 1; (g_487 <= 4); g_487 += 1)
    { /* block id: 1360 */
        int32_t l_3224 = 0x11023FD0L;
        int32_t *l_3225[5] = {&g_163,&g_163,&g_163,&g_163,&g_163};
        uint16_t l_3226 = 2UL;
        int i;
        l_3226++;
    }
    return p_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_33 g_36 g_177 g_2392 g_172 g_1100 g_1101 g_1102 g_1947 g_1948 g_1736 g_1737 g_321 g_137 g_138 g_134 g_140 g_554 g_555 g_556 g_106 g_13 g_784.f0 g_2097 g_2098 g_784 g_2389 g_1583 g_149 g_1470 g_1471 g_2276 g_115 g_116 g_580 g_171 g_699 g_501 g_2432 g_139 g_542 g_1796 g_1697 g_1698 g_1701 g_1700 g_1699 g_2522 g_2527 g_370 g_1172 g_1173 g_949 g_487 g_1839 g_1840 g_2559 g_1837 g_1815 g_1357 g_2292 g_2202 g_791 g_2203 g_695.f5 g_695.f0 g_577 g_2590 g_769.f0 g_60 g_333 g_2638 g_306 g_1201.f2 g_163 g_2154 g_162 g_1971 g_2677 g_1379 g_1946 g_769.f3 g_769.f5 g_2726 g_2731 g_2757 g_108 g_1646 g_2768 g_2020 g_2084
 * writes: g_33 g_36 g_177 g_2392 g_306 g_2192 g_149 g_134 g_172 g_2491 g_2492 g_108 g_699 g_1646 g_370 g_487 g_391 g_1559 g_171 g_1971 g_577 g_332 g_48 g_163 g_2665 g_2677 g_1379 g_769.f3 g_2741 g_2757 g_2773 g_2084
 */
static uint8_t  func_30(int8_t  p_31)
{ /* block id: 17 */
    int32_t *l_32 = &g_33[8][2];
    int32_t l_2167[5][8][1] = {{{0x582FEA4CL},{0xA4A9D096L},{4L},{0xAC09D84DL},{(-3L)},{0x39D1511DL},{(-1L)},{0x39D1511DL}},{{(-3L)},{0xAC09D84DL},{4L},{0xA4A9D096L},{0x582FEA4CL},{(-4L)},{0x582FEA4CL},{0xA4A9D096L}},{{4L},{0xAC09D84DL},{(-3L)},{0x39D1511DL},{(-1L)},{0x39D1511DL},{(-3L)},{0xAC09D84DL}},{{4L},{0xA4A9D096L},{0x582FEA4CL},{(-4L)},{0x582FEA4CL},{0xA4A9D096L},{4L},{0xAC09D84DL}},{{(-3L)},{0x39D1511DL},{(-1L)},{0x39D1511DL},{(-3L)},{0xAC09D84DL},{4L},{0xA4A9D096L}}};
    int32_t *l_2246 = (void*)0;
    int32_t * const *l_2247 = &g_949;
    float l_2390 = 0x1.Cp+1;
    int32_t l_2391[7][2] = {{3L,0xC0756D4BL},{3L,0xD20A8B0FL},{0xA6AD457BL,0xA6AD457BL},{0xD20A8B0FL,3L},{0xC0756D4BL,3L},{0xD20A8B0FL,0xA6AD457BL},{0xA6AD457BL,0xD20A8B0FL}};
    uint8_t *** const *l_2408[1][10][9] = {{{(void*)0,&g_1100,&g_1100,(void*)0,(void*)0,(void*)0,(void*)0,&g_1100,&g_1100},{&g_1100,&g_1100,&g_1100,(void*)0,&g_1100,&g_1100,&g_1100,(void*)0,&g_1100},{(void*)0,(void*)0,(void*)0,(void*)0,&g_1100,(void*)0,(void*)0,&g_1100,(void*)0},{&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,(void*)0,&g_1100,(void*)0,&g_1100},{(void*)0,&g_1100,&g_1100,&g_1100,(void*)0,(void*)0,&g_1100,&g_1100,(void*)0},{&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100},{&g_1100,(void*)0,(void*)0,&g_1100,(void*)0,&g_1100,(void*)0,&g_1100,&g_1100},{&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100,&g_1100},{(void*)0,(void*)0,&g_1100,&g_1100,&g_1100,(void*)0,(void*)0,&g_1100,&g_1100},{&g_1100,&g_1100,&g_1100,(void*)0,(void*)0,(void*)0,&g_1100,&g_1100,&g_1100}}};
    int32_t ****l_2442 = (void*)0;
    union U1 ****l_2528 = &g_2039[8][0];
    uint8_t ****l_2706 = (void*)0;
    uint8_t *****l_2705 = &l_2706;
    int16_t *l_2762 = &g_370[0][0];
    int16_t **l_2761 = &l_2762;
    uint64_t l_2787 = 0xB96582651C464B67LL;
    uint32_t l_2816 = 0xBD0F88D7L;
    int i, j, k;
    (*l_32) |= 0L;
    for (p_31 = (-30); (p_31 > (-19)); p_31++)
    { /* block id: 21 */
        uint32_t l_58 = 5UL;
        uint16_t *l_1082 = &g_577;
        int32_t *l_2096 = &g_33[3][8];
        int32_t l_2126[4][9][7] = {{{0xCFB92198L,0xF919F839L,0x87E7C7A0L,0L,3L,0x8F32AAC6L,0x168E69EDL},{2L,5L,0xC320DDDCL,(-1L),(-1L),1L,0L},{0xD6F0C207L,0xF919F839L,(-8L),(-8L),(-4L),1L,(-1L)},{0x0F34FB4BL,0x93925AC4L,(-10L),2L,0L,0L,3L},{1L,(-1L),0xC2F9702CL,3L,(-1L),5L,(-1L)},{0x948FE102L,1L,0L,1L,0xA10A256FL,0x07AE5B95L,(-1L)},{(-1L),3L,0L,0L,2L,(-10L),0x93925AC4L},{0L,1L,(-1L),0L,0x07AE5B95L,0x55F79E31L,0x2AABAB0EL},{0xD6F0C207L,(-1L),(-1L),1L,0L,1L,0L}},{{0xC2B202C6L,0x168E69EDL,0x8F32AAC6L,3L,0L,0x87E7C7A0L,0xF919F839L},{(-6L),5L,8L,2L,1L,1L,2L},{0xC5B89417L,0L,0xC5B89417L,(-8L),0x168E69EDL,(-1L),(-1L)},{(-1L),(-6L),0x8F32AAC6L,(-1L),0x2AABAB0EL,0L,1L},{0xA10A256FL,(-1L),1L,0L,0x12C94409L,(-1L),0x2264A0CCL},{0x948FE102L,(-1L),1L,0x12C94409L,(-1L),1L,(-1L)},{0L,(-4L),0L,(-1L),0x6700FD58L,0x87E7C7A0L,(-1L)},{1L,(-1L),5L,0x2AABAB0EL,0x07AE5B95L,1L,(-1L)},{8L,0x6700FD58L,0x07AE5B95L,(-1L),(-6L),0x55F79E31L,0L}},{{(-1L),0x0F34FB4BL,(-10L),0x92470996L,1L,0x2264A0CCL,(-1L)},{(-1L),0x8F32AAC6L,7L,0xC2F9702CL,0x6F632612L,0x1C70795CL,0x07AE5B95L},{0xF196FEE6L,1L,0xD4518B8DL,(-5L),0L,0x8F32AAC6L,1L},{0L,0x12C94409L,3L,0x07AE5B95L,1L,1L,0xBD70D152L},{0xC320DDDCL,1L,0xF35119CDL,(-8L),0xF35119CDL,1L,0xC320DDDCL},{0x1E5A53BCL,0xD6F0C207L,0L,0x770D46AEL,0xBD70D152L,6L,1L},{0xD6F0C207L,0x55F79E31L,1L,5L,0x12C94409L,0xA10A256FL,0xC2F9702CL},{(-1L),1L,0L,1L,0x1C70795CL,(-1L),(-9L)},{(-1L),1L,0xF35119CDL,0x6F632612L,1L,0xF196FEE6L,(-10L)}},{{(-8L),0x5CED3934L,3L,1L,8L,0xA10A256FL,1L},{1L,0x8F32AAC6L,0xD4518B8DL,0x12C94409L,0x770D46AEL,0L,1L},{(-5L),0L,7L,(-5L),8L,(-4L),1L},{(-1L),(-1L),0x2264A0CCL,0x407C4B02L,8L,1L,0xD6F0C207L},{0x55F79E31L,1L,0x1C70795CL,8L,0x770D46AEL,(-10L),0x948FE102L},{0x1E5A53BCL,0x948FE102L,0x8F32AAC6L,0xF35119CDL,8L,(-4L),(-4L)},{(-8L),8L,1L,(-9L),1L,0x2264A0CCL,1L},{8L,1L,1L,8L,0x1C70795CL,1L,0L},{(-5L),1L,6L,0xF35119CDL,0x12C94409L,1L,(-10L)}}};
        struct S0 **l_2143[10][10] = {{(void*)0,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,(void*)0,&g_2098,(void*)0,&g_2098},{&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,(void*)0,&g_2098,&g_2098,&g_2098},{&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,(void*)0,(void*)0},{&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,(void*)0,&g_2098,&g_2098},{&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,(void*)0,&g_2098,&g_2098,&g_2098,&g_2098},{&g_2098,&g_2098,&g_2098,(void*)0,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098},{&g_2098,(void*)0,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098},{&g_2098,&g_2098,(void*)0,(void*)0,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098},{(void*)0,&g_2098,&g_2098,&g_2098,&g_2098,(void*)0,&g_2098,&g_2098,&g_2098,&g_2098},{&g_2098,(void*)0,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098,&g_2098}};
        union U1 **l_2235 = &g_889;
        int i, j, k;
        (*l_32) &= p_31;
        for (g_36 = (-26); (g_36 <= (-22)); g_36 = safe_add_func_uint32_t_u_u(g_36, 2))
        { /* block id: 25 */
            uint16_t *l_49 = &g_50[1];
            uint16_t *l_59 = &g_60[4];
            int32_t l_71 = 0xB2A12551L;
            int64_t l_2110 = 0x9B05DC10B920D11FLL;
            int32_t *l_2121 = &g_33[6][3];
            int32_t l_2170[6][9] = {{(-10L),(-9L),0xFA086D47L,(-9L),(-10L),0x02F636D1L,0x02F636D1L,(-10L),(-9L)},{1L,0xC529C84BL,1L,0xB66BC7D9L,0x13E6C281L,0x13E6C281L,0xB66BC7D9L,1L,0xC529C84BL},{(-1L),0x27758F6BL,0x02F636D1L,0xFA086D47L,0xFA086D47L,0x02F636D1L,0x27758F6BL,(-1L),0x27758F6BL},{0x95940481L,1L,0xB66BC7D9L,0xB66BC7D9L,1L,0x95940481L,0x13E6C281L,0x95940481L,1L},{(-9L),0x27758F6BL,0x27758F6BL,(-9L),(-1L),(-10L),(-1L),(-9L),0x27758F6BL},{0xC529C84BL,0xC529C84BL,0x13E6C281L,1L,0xD0BD2C62L,1L,0x13E6C281L,0xC529C84BL,0xC529C84BL}};
            uint16_t l_2171 = 0xB8BDL;
            uint64_t * const l_2191 = &g_2192;
            uint64_t * const *l_2190 = &l_2191;
            uint64_t * const **l_2189 = &l_2190;
            uint8_t *l_2201 = &g_2202;
            int32_t **l_2211 = &g_949;
            int i, j;
        }
    }
    if (((void*)0 != l_2247))
    { /* block id: 939 */
        uint32_t l_2268 = 0UL;
        int8_t **l_2269[9][7];
        int64_t *l_2286[10][5] = {{&g_699[0][2][0],(void*)0,(void*)0,&g_699[0][2][0],&g_699[0][2][0]},{&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[2][0][0]},{&g_699[0][2][0],(void*)0,(void*)0,&g_699[0][2][0],&g_699[0][2][0]},{&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[2][0][0]},{&g_699[0][2][0],(void*)0,(void*)0,&g_699[0][2][0],&g_699[0][2][0]},{&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[2][0][0]},{&g_699[0][2][0],(void*)0,(void*)0,&g_699[0][2][0],&g_699[0][2][0]},{&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[2][0][0]},{&g_699[0][2][0],(void*)0,(void*)0,&g_699[0][2][0],&g_699[0][2][0]},{&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[2][0][0]}};
        int64_t **l_2285[8] = {(void*)0,(void*)0,&l_2286[0][2],(void*)0,(void*)0,&l_2286[0][2],(void*)0,(void*)0};
        uint8_t ****l_2318 = &g_1100;
        uint8_t *****l_2317[2][6][6] = {{{&l_2318,&l_2318,&l_2318,&l_2318,&l_2318,&l_2318},{&l_2318,&l_2318,(void*)0,&l_2318,(void*)0,&l_2318},{&l_2318,&l_2318,(void*)0,&l_2318,&l_2318,&l_2318},{&l_2318,&l_2318,&l_2318,&l_2318,&l_2318,&l_2318},{(void*)0,(void*)0,&l_2318,&l_2318,&l_2318,&l_2318},{&l_2318,(void*)0,&l_2318,(void*)0,&l_2318,&l_2318}},{{&l_2318,&l_2318,&l_2318,(void*)0,(void*)0,&l_2318},{&l_2318,(void*)0,&l_2318,&l_2318,&l_2318,&l_2318},{(void*)0,(void*)0,(void*)0,&l_2318,&l_2318,&l_2318},{&l_2318,&l_2318,&l_2318,&l_2318,&l_2318,&l_2318},{&l_2318,&l_2318,&l_2318,&l_2318,&l_2318,&l_2318},{&l_2318,&l_2318,(void*)0,&l_2318,&l_2318,&l_2318}}};
        const int64_t l_2347[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        int32_t l_2356 = 0L;
        uint64_t l_2357 = 0x91402B1F5DF2FD5ELL;
        int32_t *l_2380 = &l_2356;
        int32_t *l_2381 = &g_172[4][1];
        int32_t *l_2382 = &g_172[4][1];
        int32_t *l_2383 = &l_2167[4][0][0];
        int32_t *l_2384 = &g_172[4][1];
        int32_t *l_2385 = &l_2167[2][4][0];
        int32_t *l_2386 = (void*)0;
        int32_t *l_2387[3][4][8] = {{{&g_3,&g_33[6][7],&g_15,&g_19,&g_19,&g_15,&g_33[6][7],&g_3},{&g_15,&g_1796,&g_15,(void*)0,&l_2167[4][0][0],&g_19,&l_2167[4][0][0],(void*)0},{&g_19,&g_172[4][1],&g_19,&g_13,(void*)0,&g_19,&g_15,&g_15},{&g_15,&g_1796,&g_15,&g_15,&g_1796,&g_15,(void*)0,&l_2167[4][0][0]}},{{&g_15,&g_33[6][7],&g_3,&g_1796,(void*)0,&g_1796,&g_3,&g_33[6][7]},{&g_19,&g_3,&g_19,&g_1796,&l_2167[4][0][0],&g_13,&g_13,&l_2167[4][0][0]},{&g_15,&l_2167[4][0][0],&l_2167[4][0][0],&g_15,&g_19,&g_33[6][7],&g_13,&g_15},{&g_3,&g_15,&g_19,&g_13,&g_19,&g_15,&g_3,(void*)0}},{{&g_19,&g_15,&g_3,(void*)0,&g_33[6][7],&g_33[6][7],(void*)0,&g_3},{&l_2167[4][0][0],&l_2167[4][0][0],&g_15,&g_19,&g_33[6][7],&g_13,&g_15,&g_13},{&g_19,&g_3,&g_19,&g_3,&g_19,&g_1796,&l_2167[4][0][0],&g_13},{&g_3,&g_33[6][7],&g_15,&g_19,&g_19,&g_15,&g_33[6][7],&g_3}}};
        int16_t l_2388 = 0xD3D4L;
        uint64_t *l_2403 = &g_306;
        uint64_t *l_2404 = &g_2192;
        struct S0 *l_2412 = &g_542;
        int32_t * const *l_2456 = &g_949;
        uint32_t l_2520 = 4294967295UL;
        uint64_t ***l_2551 = &g_332;
        uint32_t l_2577 = 0x1A1E77F1L;
        uint16_t l_2640[10] = {65535UL,0x783FL,65535UL,0x783FL,65535UL,0x783FL,65535UL,0x783FL,65535UL,0x783FL};
        uint32_t *l_2651 = (void*)0;
        uint32_t **l_2650 = &l_2651;
        uint64_t l_2657 = 0x354803B48D6DF37CLL;
        int i, j, k;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 7; j++)
                l_2269[i][j] = (void*)0;
        }
        for (g_177 = 0; (g_177 != 59); ++g_177)
        { /* block id: 942 */
            uint64_t *l_2256 = &g_309;
            int32_t l_2270 = 0x32D4F8B5L;
            int64_t **l_2283 = (void*)0;
            uint16_t ***l_2310 = &g_140;
        }
        g_2392++;
        if ((safe_mod_func_int32_t_s_s((((*l_2404) = ((*l_2403) = (((((*l_2381) & (safe_rshift_func_uint16_t_u_u(0UL, ((((*l_32) != 1L) & ((((*l_2385) = ((void*)0 != l_2387[0][1][2])) <= ((safe_lshift_func_int16_t_s_u((p_31 ^ (safe_add_func_int64_t_s_s(0x406C32FCCF2BECADLL, p_31))), 5)) , p_31)) , 0x8544L)) < p_31)))) ^ 0xB2B33EC9DAD14CF9LL) , 0x43L) ^ p_31))) , 4L), p_31)))
        { /* block id: 1036 */
            (*l_2381) |= ((((safe_sub_func_int8_t_s_s(((((**g_140) = (((p_31 < ((****l_2318) = 0xD0L)) && (*g_1947)) , ((*g_1736) == ((~((***g_321) || (0x6ABDL == (-5L)))) , l_2408[0][0][3])))) && (safe_mul_func_int16_t_s_s((((((***g_554) ^ g_106[1]) > g_13) != p_31) , 0xE7ABL), 0x0A6CL))) || g_33[3][0]), p_31)) && 0x3CCEA948L) , 0x2100L) != g_784.f0);
        }
        else
        { /* block id: 1040 */
            int32_t ****l_2441 = &g_2075;
            int32_t l_2453 = 0x24F84B81L;
            int32_t l_2457 = 0L;
            int32_t ***l_2464 = (void*)0;
            int32_t l_2481[2];
            struct S0 *l_2504 = (void*)0;
            struct S0 *l_2596 = &g_511;
            int32_t l_2637 = 0x8C6A7EAEL;
            int64_t l_2639[4][6][6] = {{{1L,8L,5L,8L,1L,5L},{8L,1L,5L,0xD53B164AEADB586ELL,0xD53B164AEADB586ELL,5L},{0xD53B164AEADB586ELL,0xD53B164AEADB586ELL,5L,1L,8L,5L},{1L,8L,5L,8L,1L,5L},{8L,1L,5L,0xD53B164AEADB586ELL,0xD53B164AEADB586ELL,5L},{0xD53B164AEADB586ELL,0xD53B164AEADB586ELL,5L,1L,8L,5L}},{{1L,8L,5L,8L,1L,5L},{8L,1L,5L,0xD53B164AEADB586ELL,0xD53B164AEADB586ELL,5L},{0xD53B164AEADB586ELL,0xD53B164AEADB586ELL,5L,1L,0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL},{(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL,0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL},{0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL,0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL},{0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL,(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL}},{{(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL,0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL},{0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL,0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL},{0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL,(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL},{(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL,0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL},{0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL,0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL},{0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL,(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL}},{{(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL,0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL},{0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL,0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL},{0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL,(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL},{(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL,0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL},{0xFE9B06FFBB3F4301LL,(-3L),0xD53B164AEADB586ELL,0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL},{0xB0778B0BB3C35D94LL,0xB0778B0BB3C35D94LL,0xD53B164AEADB586ELL,(-3L),0xFE9B06FFBB3F4301LL,0xD53B164AEADB586ELL}}};
            uint32_t **l_2656 = &l_2651;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_2481[i] = 0xECA2A428L;
            for (g_36 = 0; (g_36 <= 2); g_36 += 1)
            { /* block id: 1043 */
                int32_t *l_2411 = (void*)0;
                struct S0 **l_2413 = &l_2412;
                int i;
                l_2386 = ((**g_2097) , l_2411);
                (*l_2383) &= g_2389[(g_36 + 1)];
                if ((*g_1583))
                    continue;
                (*l_2413) = l_2412;
                for (g_149 = 0; (g_149 <= 2); g_149 += 1)
                { /* block id: 1050 */
                    int8_t l_2431 = (-1L);
                    l_2431 = ((*g_1470) == (((safe_lshift_func_int8_t_s_s(p_31, 6)) ^ p_31) && (safe_mod_func_uint16_t_u_u(65527UL, ((safe_lshift_func_int8_t_s_u((**g_2276), 0)) | (!(safe_add_func_int64_t_s_s(((safe_sub_func_int64_t_s_s(0L, ((safe_unary_minus_func_uint64_t_u((p_31 || (safe_sub_func_int16_t_s_s((((!((safe_add_func_uint32_t_u_u(((((*g_580) <= g_699[0][2][0]) , p_31) && p_31), p_31)) && (-6L))) , 0x2F95423AL) ^ (*l_2383)), g_501))))) , 0UL))) | (*g_1470)), (*l_2384)))))))));
                }
            }
            if (g_2432[1][4][0])
            { /* block id: 1054 */
                uint32_t l_2445[9] = {0x5D307CACL,18446744073709551611UL,0x5D307CACL,18446744073709551611UL,0x5D307CACL,18446744073709551611UL,0x5D307CACL,18446744073709551611UL,0x5D307CACL};
                int64_t * const * const l_2446 = &l_2286[0][2];
                uint64_t l_2475 = 0x09AB83EB61943EC3LL;
                int32_t l_2476 = 1L;
                int32_t l_2479 = 0xBB5C02ACL;
                int32_t l_2483 = 0x7A53CC61L;
                int32_t ***l_2489 = &g_948;
                int64_t l_2521 = 0L;
                union U1 ****l_2526 = (void*)0;
                int8_t **l_2556 = (void*)0;
                int i;
                if (((safe_sub_func_uint16_t_u_u((safe_add_func_int64_t_s_s((safe_sub_func_int8_t_s_s(((((l_2445[5] ^= ((safe_mod_func_uint64_t_u_u((l_2441 == l_2442), (safe_rshift_func_int16_t_s_u(0xC4E3L, 11)))) == p_31)) , l_2446) != &g_1698[0][3][0]) != (safe_lshift_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((l_2453 ^= 3L), (safe_rshift_func_int16_t_s_u((l_2456 == (p_31 , l_2456)), 13)))), (-7L))) && p_31), (***g_139)))), l_2457)), p_31)), p_31)) <= p_31))
                { /* block id: 1057 */
                    int8_t l_2477 = 0L;
                    int32_t l_2478 = 1L;
                    int32_t l_2480 = 1L;
                    int32_t l_2482 = 0L;
                    int32_t ****l_2490[9][3] = {{&l_2489,&l_2464,&l_2489},{(void*)0,(void*)0,&l_2489},{&l_2464,&l_2489,&l_2489},{&l_2489,&l_2464,&l_2489},{(void*)0,(void*)0,&l_2489},{&l_2464,&l_2489,&l_2489},{&l_2489,&l_2464,&l_2489},{(void*)0,(void*)0,&l_2489},{&l_2464,&l_2489,&l_2489}};
                    struct S0 **l_2505[2][3] = {{&l_2504,&l_2504,&l_2504},{&l_2504,&l_2504,&l_2504}};
                    int i, j;
                    for (g_149 = 8; (g_149 <= 58); g_149 = safe_add_func_uint16_t_u_u(g_149, 6))
                    { /* block id: 1060 */
                        int32_t ****l_2465 = &l_2464;
                        uint32_t *l_2466 = &l_2268;
                        uint32_t l_2484 = 0x650F8394L;
                        l_2476 = ((safe_lshift_func_int8_t_s_u(((p_31 < (safe_lshift_func_int16_t_s_u(p_31, (((0x1B35L || (((*l_2466) = (((*l_2465) = ((*l_2412) , l_2464)) == &l_2456)) > (safe_rshift_func_int16_t_s_u((safe_div_func_uint16_t_u_u(p_31, 0x5641L)), (safe_mod_func_int32_t_s_s((((safe_sub_func_uint32_t_u_u((*l_2382), p_31)) != g_1796) < g_784.f5), l_2445[5])))))) > 0UL) ^ l_2475)))) | p_31), p_31)) > 0x87B8B0D5L);
                        l_2484++;
                    }
                    l_2504 = ((safe_add_func_int32_t_s_s(p_31, ((g_2492 = (g_2491 = (((*g_2098) , (*g_2098)) , l_2489))) == ((((*l_2404) = ((safe_mul_func_uint8_t_u_u((~(safe_mod_func_uint64_t_u_u((safe_mod_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s(((p_31 < p_31) > (((*g_1102) ^= 0x74L) != (((((safe_mod_func_int8_t_s_s((g_108 = p_31), p_31)) & p_31) , 1UL) && p_31) || (-2L)))), p_31)), (**g_2276))), p_31))), p_31)) == 6L)) , l_2478) , &g_1173)))) , l_2504);
                    (*g_2522) = (((*l_32) = (safe_add_func_float_f_f(((safe_mul_func_int8_t_s_s(0x52L, ((*l_2385) &= (safe_rshift_func_uint16_t_u_s((((l_2441 != (((-8L) > ((safe_mod_func_int64_t_s_s((safe_mul_func_uint8_t_u_u(((safe_lshift_func_uint8_t_u_u((1UL == ((*g_1102) <= p_31)), 7)) & ((*g_138) |= 0x759DL)), p_31)), (g_699[0][2][0] |= (((safe_lshift_func_int16_t_s_u(((((***g_1100) != 0x50L) <= (**g_2276)) >= p_31), l_2520)) != l_2521) != p_31)))) & (**g_1697))) , &g_1815)) ^ p_31) < (-1L)), 9))))) , p_31), p_31))) == 0x0.EF9F67p-37);
                }
                else
                { /* block id: 1077 */
                    union U1 *****l_2529 = &l_2528;
                    int32_t l_2532 = 0x91174088L;
                    int16_t *l_2533 = &g_370[0][0];
                    (*l_2384) = ((*l_32) , (safe_sub_func_int32_t_s_s(((p_31 && ((((*l_2382) , p_31) & ((((l_2526 != ((*l_2529) = (g_2527 , l_2528))) ^ 0x84A4L) < (safe_sub_func_int16_t_s_s(((((*l_2533) |= (((l_2532 && p_31) ^ 0x4BB34BBFL) == 0x0DL)) || (**g_555)) || l_2532), 0x5516L))) <= p_31)) || p_31)) && (*l_2384)), 0x1281BCF5L)));
                    (*l_2385) = (p_31 && l_2532);
                }
                if (((void*)0 == (**g_1172)))
                { /* block id: 1083 */
                    uint64_t l_2561 = 1UL;
                    int32_t l_2576 = 0x7D2546FFL;
                    for (g_487 = 0; (g_487 <= 1); g_487 += 1)
                    { /* block id: 1086 */
                        int8_t *l_2555 = &g_108;
                        int8_t **l_2554 = &l_2555;
                        int32_t l_2560 = 0xD071B3E1L;
                        float *l_2575[1][8] = {{&l_2390,(void*)0,&l_2390,(void*)0,&l_2390,(void*)0,&l_2390,(void*)0}};
                        int i, j;
                        (*g_1837) = ((((**g_1101) = (7L <= (safe_lshift_func_int16_t_s_u((((safe_sub_func_int16_t_s_s((-1L), ((safe_unary_minus_func_int16_t_s((((p_31 ^ ((-4L) >= (safe_div_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u(((safe_div_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_s(((safe_div_func_int64_t_s_s((((safe_mod_func_uint32_t_u_u(((void*)0 == l_2551), ((safe_div_func_uint8_t_u_u((l_2554 != l_2556), ((*l_2555) = (((((l_2481[g_487] = p_31) > (**g_1839)) < 0UL) , g_2559) , 2L)))) & (**g_2276)))) > l_2560) != p_31), 0xABBA40B5A963199CLL)) | 1L), p_31)) != 0x0DL), 0x0EL)) != (*g_556)), p_31)), p_31)))) | p_31) & 0L))) >= 0xFD56L))) == 0x3B66AF959E53F41FLL) <= p_31), p_31)))) < 9L) > l_2561);
                        (*g_2292) = ((**g_1815) = (void*)0);
                        (*l_2385) = ((safe_add_func_uint8_t_u_u(((p_31 | (safe_div_func_int32_t_s_s(l_2561, (((*l_32) = (((0x5.D1B8DAp-88 <= 0xC.D23E3Ap-5) > (((((p_31 != (l_2576 = ((*g_2203) = (l_2481[g_487] = (p_31 == (safe_sub_func_float_f_f((l_2483 = (((*g_791) = (safe_div_func_float_f_f(l_2475, (((safe_sub_func_float_f_f((*g_580), (+(*l_2385)))) > g_2202) == (*l_2383))))) != l_2560)), p_31))))))) != g_695.f5) < g_699[2][2][1]) > 0x1.7C6A24p-55) <= g_370[2][0])) == g_695.f0)) , 0x0E6F0F13L)))) , l_2577), (*g_1102))) == p_31);
                    }
                    return (**g_1101);
                }
                else
                { /* block id: 1102 */
                    return p_31;
                }
            }
            else
            { /* block id: 1105 */
                const uint16_t l_2591 = 0x045DL;
                struct S0 *l_2594 = &g_1496;
                int32_t l_2601 = 0x79ADF89CL;
                int32_t l_2607[5][4][8] = {{{1L,(-9L),0x12172C68L,0xCD55A8FBL,2L,(-1L),(-1L),0xF4611E25L},{(-1L),1L,0x5C0E06A6L,8L,0x12172C68L,0x3FAAA289L,(-6L),1L},{0x36E93BA5L,0xB1C4E8F1L,0x2AC5C648L,0xC5042F78L,0x3FDEEFCCL,1L,(-1L),1L},{0x3FAAA289L,0x3C71B25CL,0xF3B7B1D0L,0x3CE67C05L,0xAF277E67L,1L,1L,0xAF277E67L}},{{0xF3B7B1D0L,(-4L),(-4L),0xF3B7B1D0L,0x243D1E10L,(-1L),0x3FAAA289L,0L},{(-1L),0xC5042F78L,0L,(-1L),0xB1C4E8F1L,2L,0x3CE67C05L,(-9L)},{0x0FF6F944L,8L,0L,1L,(-1L),(-6L),0L,0L},{5L,(-6L),0x19DD4833L,0xC5042F78L,0x59846D02L,(-1L),0xF4611E25L,0xD075B9A5L}},{{0x36E93BA5L,(-9L),0L,0x5C0E06A6L,(-1L),0x239E7E5CL,6L,(-4L)},{0xD5E73020L,0xEEC59925L,0x3C71B25CL,0xB38FC1FCL,0xFB89E635L,(-1L),(-1L),(-6L)},{0L,1L,0x12172C68L,0xEEC59925L,1L,6L,(-9L),(-1L)},{0x3FAAA289L,0L,(-1L),(-9L),(-1L),0L,0x3FAAA289L,1L}},{{0x3FDEEFCCL,1L,0xD5E73020L,(-1L),0L,1L,0x74BB8263L,0L},{0x3C71B25CL,0x5C0E06A6L,9L,1L,0L,0xF4611E25L,(-10L),(-9L)},{0x3FDEEFCCL,0xF3B7B1D0L,0x59846D02L,0L,(-1L),2L,0x2AC5C648L,0L},{0x3FAAA289L,(-10L),6L,(-1L),1L,(-1L),0xB166D845L,0L}},{{0L,0x12172C68L,(-6L),0x3CE67C05L,0xFB89E635L,0x5C0E06A6L,0L,0xC5042F78L},{0xD5E73020L,9L,0x3FAAA289L,0xCD55A8FBL,(-1L),1L,0x36E93BA5L,2L},{0x36E93BA5L,0xB166D845L,0xEEC59925L,0xB1C4E8F1L,0x59846D02L,0x59846D02L,0xB1C4E8F1L,0xEEC59925L},{5L,5L,0x36E93BA5L,(-10L),(-1L),0xF3B7B1D0L,(-1L),(-1L)}}};
                uint16_t **l_2621[8];
                uint32_t *l_2635 = &g_177;
                uint32_t **l_2634[4][5] = {{(void*)0,&l_2635,&l_2635,&l_2635,(void*)0},{(void*)0,&l_2635,&l_2635,&l_2635,(void*)0},{(void*)0,&l_2635,&l_2635,&l_2635,(void*)0},{(void*)0,&l_2635,&l_2635,&l_2635,(void*)0}};
                float *l_2649 = &g_1971;
                uint32_t ***l_2652 = &l_2650;
                uint8_t **l_2655 = &g_1102;
                int i, j, k;
                for (i = 0; i < 8; i++)
                    l_2621[i] = &g_138;
                for (g_577 = (-21); (g_577 >= 6); g_577 = safe_add_func_int64_t_s_s(g_577, 3))
                { /* block id: 1108 */
                    uint64_t **l_2580 = (void*)0;
                    uint8_t l_2604 = 0x05L;
                    (*l_2551) = l_2580;
                    for (g_487 = 0; (g_487 == 30); ++g_487)
                    { /* block id: 1112 */
                        struct S0 **l_2595 = &l_2594;
                        int8_t l_2606 = 0x2BL;
                        int32_t l_2612 = 0x12D646E1L;
                        uint32_t *l_2615 = &l_2520;
                        uint16_t **l_2620[9][6] = {{(void*)0,&g_138,&g_138,&g_138,&g_138,(void*)0},{&g_138,&g_138,&g_138,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_138,&g_138,&g_138},{(void*)0,&g_138,&g_138,&g_138,&g_138,(void*)0},{&g_138,&g_138,&g_138,&g_138,&g_138,&g_138},{(void*)0,(void*)0,&g_138,&g_138,&g_138,&g_138},{(void*)0,&g_138,&g_138,(void*)0,&g_138,&g_138},{&g_138,&g_138,(void*)0,&g_138,&g_138,(void*)0},{(void*)0,&g_138,(void*)0,&g_138,&g_138,&g_138}};
                        float *l_2630 = &g_48;
                        uint32_t ***l_2636 = &l_2634[2][1];
                        int i, j;
                        (*l_2380) = ((p_31 & ((p_31 & ((*g_138) = (p_31 && ((~0x67L) < (safe_rshift_func_int16_t_s_s(1L, 0)))))) , (0x7CBC0A3B53B9236ELL & (safe_mul_func_uint8_t_u_u(((((safe_mod_func_uint64_t_u_u(g_2590, (((g_769.f0 < l_2591) , 0xD9A96C7A1140AC0BLL) , (-7L)))) > (-5L)) | (-2L)) != 0xD5F9AD4D96096972LL), (*g_1102)))))) > p_31);
                        l_2612 &= (safe_mul_func_uint16_t_u_u((((*l_2595) = l_2594) != l_2596), (safe_rshift_func_uint16_t_u_s((safe_div_func_uint32_t_u_u(((l_2601 ^= ((*l_32) = (-6L))) <= (((safe_rshift_func_int8_t_s_u((g_108 = (l_2604 ^ (safe_unary_minus_func_int32_t_s((0x1DA3A345L == (0xD79A8471376D4884LL < (l_2607[3][1][1] = l_2606))))))), 1)) & 2UL) == (safe_add_func_uint64_t_u_u(((*l_2404) = (safe_div_func_int32_t_s_s(l_2481[0], l_2591))), p_31)))), (*l_2381))), 10))));
                        (*l_2383) ^= (safe_mod_func_uint16_t_u_u((((((((((l_2637 = ((((*l_2615) = ((void*)0 != (*g_1736))) , (((--(**g_140)) && (safe_mul_func_uint16_t_u_u(((l_2621[0] = l_2620[3][0]) == (void*)0), (((((l_2607[3][1][1] &= (safe_div_func_uint32_t_u_u((((safe_div_func_uint32_t_u_u(l_2604, (~(safe_lshift_func_uint16_t_u_s(((+(((*l_2630) = 0x6.AF44C7p-23) , ((*l_32) &= (p_31 & (safe_mul_func_uint8_t_u_u(((+(((*l_2636) = l_2634[1][3]) != (void*)0)) < (-9L)), p_31)))))) > 0x79CB047E52BEC052LL), p_31))))) != 0x535721BE7830CF46LL) , 0UL), (*g_1470)))) | p_31) || p_31) >= (-1L)) && 1UL)))) || (**g_1697))) , p_31)) > g_60[0]) , (void*)0) == g_333) && 0xFBL) || g_2638) , p_31) | l_2612) , l_2639[2][3][4]), l_2612));
                        return l_2601;
                    }
                }
                (*l_2381) = l_2640[6];
                (*l_2649) = (((+(((*g_580) != (((safe_mul_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u(((*l_2635) = (((((*l_2403) &= (((!((((*g_138) != (*l_2380)) ^ (l_2601 &= 0x95AAL)) > (((&g_1100 == l_2408[0][2][0]) < (((void*)0 != (**l_2318)) != (safe_div_func_float_f_f(p_31, p_31)))) , p_31))) , p_31) && p_31)) , p_31) == p_31) | l_2457)), (*g_1470))), 1UL)) , g_106[1]) > g_1201.f2)) >= g_769.f0)) == p_31) >= (-0x1.5p+1));
                (*l_2384) = (((*l_2652) = l_2650) != (((safe_sub_func_uint8_t_u_u((l_2655 == l_2655), ((*g_1470) <= l_2607[3][1][1]))) != p_31) , l_2656));
            }
            (*l_2383) = 0x6D5E7484L;
            (*l_2384) &= (*l_2383);
        }
        --l_2657;
    }
    else
    { /* block id: 1146 */
        uint32_t l_2671 = 4UL;
        int32_t l_2678[6];
        int32_t l_2697 = 0x35E92B72L;
        uint8_t ****l_2704 = &g_1100;
        uint8_t *****l_2703 = &l_2704;
        int8_t *l_2754 = &g_108;
        uint32_t ***l_2771[1];
        int8_t **l_2785 = &l_2754;
        int8_t ***l_2784 = &l_2785;
        int64_t *l_2786[9][8][3] = {{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}},{{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757},{&g_2677,(void*)0,&g_2677},{&g_2677,&g_2677,&g_2757},{&g_699[1][3][0],(void*)0,&g_699[1][3][0]},{&g_2677,&g_2757,&g_2757}}};
        int32_t *l_2796 = &g_1796;
        int32_t *l_2797 = (void*)0;
        int32_t *l_2798 = (void*)0;
        int32_t *l_2799 = &g_172[9][0];
        int32_t *l_2800 = &l_2678[5];
        int32_t *l_2801 = (void*)0;
        int32_t *l_2802 = (void*)0;
        int32_t *l_2803 = &g_163;
        int32_t *l_2804 = &g_73;
        int32_t *l_2805 = (void*)0;
        int32_t *l_2806 = &l_2391[4][0];
        int32_t *l_2807 = &g_33[0][4];
        int32_t *l_2808 = &l_2678[5];
        int32_t *l_2809 = &g_172[7][0];
        int32_t *l_2810 = &l_2167[4][0][0];
        int32_t *l_2811 = &l_2678[5];
        int32_t *l_2812 = &g_172[5][0];
        int32_t *l_2813 = &l_2391[5][0];
        int32_t *l_2814 = (void*)0;
        int32_t *l_2815 = &l_2678[0];
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_2678[i] = (-1L);
        for (i = 0; i < 1; i++)
            l_2771[i] = (void*)0;
        for (g_163 = 0; (g_163 < 22); ++g_163)
        { /* block id: 1149 */
            int64_t *l_2664 = (void*)0;
            const int32_t **l_2670 = &g_1840;
            const int32_t ***l_2669 = &l_2670;
            const int32_t ****l_2668 = &l_2669;
            int64_t *l_2676 = &g_2677;
            int8_t *l_2683 = &g_108;
            int8_t **l_2682 = &l_2683;
            int8_t ***l_2681 = &l_2682;
            int8_t ****l_2680 = &l_2681;
            int32_t * const **l_2755 = &g_1173;
            if ((*g_2154))
                break;
            if (p_31)
                continue;
            if ((((((*l_2676) &= (safe_mod_func_uint32_t_u_u(((((g_2665 = (g_699[1][2][1] = p_31)) && p_31) < ((safe_lshift_func_int8_t_s_u((l_2671 ^= ((*g_1470) , ((l_2668 == ((*g_949) , &l_2669)) == p_31))), ((safe_mod_func_uint16_t_u_u((((*g_162) , (((((safe_mul_func_float_f_f(((p_31 , p_31) <= (***l_2669)), p_31)) > 0xD.F44F75p-94) <= p_31) == (*g_2203)) > (****l_2668))) , 0xA432L), (*l_32))) && (*g_1947)))) < p_31)) < 1UL), (****l_2668)))) == p_31) <= l_2678[5]) <= (**l_2670)))
            { /* block id: 1156 */
                uint16_t l_2696 = 0x5FB9L;
                l_2697 ^= (~(p_31 <= (((((**g_1815) = &l_2678[5]) != (void*)0) ^ (((void*)0 != l_2680) & 1L)) >= (safe_sub_func_uint64_t_u_u((((safe_mul_func_float_f_f((**l_2670), (safe_mul_func_float_f_f(((*l_32) = (safe_mul_func_float_f_f(((safe_mul_func_float_f_f((l_2678[4] , (safe_mul_func_float_f_f(0x1.90295Bp+72, (-0x2.7p+1)))), p_31)) > g_784.f5), g_784.f0))), 0x3.9E5BC2p+81)))) , l_2696) , 0UL), 0x1840488C37341C26LL)))));
            }
            else
            { /* block id: 1160 */
                uint32_t l_2702 = 0UL;
                uint32_t l_2725[8] = {4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL};
                uint32_t l_2743[6][7][2] = {{{0x9EF1719AL,0x836D4355L},{0x9EF1719AL,0UL},{1UL,18446744073709551615UL},{0UL,0x800FB456L},{18446744073709551606UL,1UL},{0UL,0xF5786877L},{0xF5786877L,0x800FB456L}},{{18446744073709551615UL,0x9EF1719AL},{1UL,18446744073709551615UL},{0xBE7EA915L,0x836D4355L},{18446744073709551615UL,0x10F4204DL},{1UL,0xBE7EA915L},{0x10F4204DL,0x800FB456L},{1UL,18446744073709551606UL}},{{0UL,18446744073709551606UL},{1UL,0x800FB456L},{0x10F4204DL,0xBE7EA915L},{1UL,0x10F4204DL},{18446744073709551615UL,0x836D4355L},{0xBE7EA915L,18446744073709551615UL},{1UL,0x9EF1719AL}},{{18446744073709551615UL,0x800FB456L},{0xF5786877L,0xF5786877L},{0UL,1UL},{18446744073709551606UL,0x800FB456L},{0UL,18446744073709551615UL},{1UL,0UL},{0x9EF1719AL,0x836D4355L}},{{0x9EF1719AL,0UL},{1UL,18446744073709551615UL},{0UL,0x800FB456L},{18446744073709551606UL,1UL},{0UL,0xF5786877L},{0xF5786877L,0x800FB456L},{18446744073709551615UL,0x9EF1719AL}},{{1UL,18446744073709551615UL},{0xBE7EA915L,0x836D4355L},{18446744073709551615UL,0x10F4204DL},{1UL,0xBE7EA915L},{0x10F4204DL,0x800FB456L},{1UL,18446744073709551606UL},{0UL,18446744073709551606UL}}};
                float *l_2775 = (void*)0;
                int i, j, k;
                for (g_2677 = 2; (g_2677 >= 0); g_2677 -= 1)
                { /* block id: 1163 */
                    int32_t *****l_2740 = (void*)0;
                    int64_t *l_2756 = &g_2757;
                    float *l_2758 = &g_171;
                    uint32_t ****l_2772[8][3][4] = {{{&l_2771[0],(void*)0,(void*)0,&l_2771[0]},{&l_2771[0],&l_2771[0],&l_2771[0],(void*)0},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]}},{{&l_2771[0],(void*)0,(void*)0,&l_2771[0]},{&l_2771[0],&l_2771[0],(void*)0,(void*)0},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]}},{{&l_2771[0],(void*)0,&l_2771[0],&l_2771[0]},{&l_2771[0],&l_2771[0],(void*)0,(void*)0},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]}},{{&l_2771[0],(void*)0,(void*)0,&l_2771[0]},{&l_2771[0],&l_2771[0],&l_2771[0],(void*)0},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]}},{{&l_2771[0],(void*)0,(void*)0,&l_2771[0]},{&l_2771[0],&l_2771[0],(void*)0,(void*)0},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]}},{{&l_2771[0],(void*)0,&l_2771[0],&l_2771[0]},{&l_2771[0],&l_2771[0],(void*)0,(void*)0},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]}},{{&l_2771[0],(void*)0,(void*)0,&l_2771[0]},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]},{(void*)0,(void*)0,&l_2771[0],&l_2771[0]}},{{(void*)0,&l_2771[0],&l_2771[0],(void*)0},{&l_2771[0],&l_2771[0],&l_2771[0],&l_2771[0]},{(void*)0,(void*)0,&l_2771[0],(void*)0}}};
                    int i, j, k;
                    if (l_2678[(g_2677 + 1)])
                    { /* block id: 1164 */
                        int i;
                        l_2678[g_2677] &= ((safe_rshift_func_uint8_t_u_s((**g_1101), (safe_add_func_int8_t_s_s(((l_2702 && (-6L)) , ((g_1379 &= ((****l_2680) = (((*g_1470) , &l_2408[0][5][0]) == (l_2705 = l_2703)))) ^ (***g_1100))), (!((void*)0 != &l_2528)))))) == p_31);
                        if (p_31)
                            break;
                    }
                    else
                    { /* block id: 1170 */
                        int16_t *l_2739 = &g_370[0][0];
                        int i;
                        (*g_2726) = ((+(((safe_add_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_s((((g_769.f3 &= (l_2697 = (safe_sub_func_int8_t_s_s(4L, (p_31 > (((**g_1946) == (((safe_mod_func_uint32_t_u_u((((l_2678[(g_2677 + 1)] = (safe_sub_func_int32_t_s_s((safe_sub_func_int16_t_s_s(l_2697, ((p_31 , ((**g_140) == (0UL == (p_31 , (safe_mod_func_uint64_t_u_u(((*g_1102) != (**l_2670)), (**l_2670))))))) , 1UL))), l_2702))) , l_2678[(g_2677 + 1)]) ^ p_31), p_31)) , (void*)0) != (void*)0)) >= l_2725[2])))))) | 0UL) <= p_31), (**g_2276))) <= (***l_2669)), (**g_2276))), p_31)) , p_31) <= 0x3.45CE31p+56)) == g_769.f5);
                        (*l_32) ^= ((safe_lshift_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(((g_2731 , l_2725[2]) >= ((safe_mul_func_uint16_t_u_u(((safe_div_func_uint16_t_u_u(p_31, (l_2678[(g_2677 + 1)] = p_31))) , (safe_add_func_int32_t_s_s((!(((*l_2739) = ((-1L) && l_2678[(g_2677 + 1)])) , (((p_31 | ((g_2741 = l_2740) == &g_2742[1][1])) | l_2678[5]) & p_31))), 0x2793B3B7L))), p_31)) != p_31)), (**l_2670))), 2)) | 8L);
                        if ((*g_2154))
                            continue;
                        l_2743[0][1][0] |= (**l_2670);
                    }
                    (*l_2758) = ((((((**g_2097) , (((p_31 <= (g_699[0][2][0] = 0x044F1879924834A0LL)) && 0x2AL) > ((safe_div_func_int8_t_s_s((-1L), ((****l_2680) |= (safe_div_func_uint64_t_u_u(l_2678[1], ((*l_2756) &= ((((+(0x7B4F66C1L ^ (safe_rshift_func_int8_t_s_u((safe_lshift_func_uint8_t_u_s((~((((p_31 , &p_31) == l_2754) , &g_1173) == l_2755)), p_31)), p_31)))) ^ p_31) != 0xB37D4CBDL) | l_2743[3][0][1]))))))) || l_2671))) , l_2671) >= (****l_2668)) , (*g_2522)) , (*g_2203));
                    (*l_32) ^= ((((safe_rshift_func_int16_t_s_u((****l_2668), ((void*)0 == l_2761))) > (p_31 >= ((*****l_2703) = (safe_lshift_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s((safe_unary_minus_func_int64_t_s((**g_1697))), (g_2768[5] != (g_2773[0][3][1] = l_2771[0])))) <= (l_2775 == l_2775)), p_31))))) < (**l_2670)) , 0x37B45C48L);
                    return p_31;
                }
            }
        }
        (**g_1815) = func_53((safe_div_func_uint16_t_u_u((((((safe_mod_func_uint32_t_u_u(((g_2192 = (safe_rshift_func_uint8_t_u_u((((*l_32) = ((void*)0 == l_2784)) | ((65532UL || l_2787) > ((*l_2754) &= p_31))), p_31))) == ((++(**g_1101)) ^ ((safe_add_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((l_2671 & 1UL), 14)), (((*g_162) <= p_31) >= l_2678[1]))) > l_2697))), p_31)) != 0xA152614948C1EE8ALL) , (void*)0) == (void*)0) <= 0x28DFE8A6L), 0x97B1L)), p_31);
        --l_2816;
        (*l_32) ^= (*l_2799);
    }
    return (*l_32);
}


/* ------------------------------------------ */
/* 
 * reads : g_2097 g_1815 g_1357
 * writes: g_2098 g_391
 */
static uint8_t  func_43(uint16_t  p_44, const int32_t * p_45, int32_t * p_46, int64_t  p_47)
{ /* block id: 863 */
    int32_t *l_2099 = (void*)0;
    (*g_2097) = &g_1496;
    (**g_1815) = l_2099;
    return p_47;
}


/* ------------------------------------------ */
/* 
 * reads : g_177 g_2020 g_2084 g_1100 g_1101 g_1102
 * writes: g_177 g_391 g_306 g_2084 g_149 g_699 g_1580
 */
static int32_t * func_53(const int32_t  p_54, int32_t  p_55)
{ /* block id: 658 */
    uint16_t ***l_1611[7][9] = {{(void*)0,&g_140,(void*)0,(void*)0,&g_140,(void*)0,&g_140,&g_140,&g_140},{(void*)0,&g_140,(void*)0,(void*)0,&g_140,(void*)0,&g_140,&g_140,&g_140},{&g_140,(void*)0,(void*)0,&g_140,&g_140,(void*)0,&g_140,&g_140,(void*)0},{&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140},{&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140},{(void*)0,&g_140,(void*)0,&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140},{&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140,(void*)0,&g_140,&g_140}};
    uint16_t ****l_1610 = &l_1611[5][8];
    int32_t l_1613[5][9][3] = {{{0xF8BB3279L,0xE9ED8466L,0x5061EDB6L},{0x23F09553L,(-7L),(-4L)},{0x7AAED3E0L,0x7AAED3E0L,1L},{0x23F09553L,0x50B333D8L,(-7L)},{0xF8BB3279L,1L,(-1L)},{0x50B333D8L,(-2L),(-2L)},{6L,0xF8BB3279L,(-1L)},{0xDFF5778AL,0xE8D01CACL,(-7L)},{0x5061EDB6L,0x67D6E1A6L,1L}},{{(-4L),0x0FB9C23DL,(-4L)},{1L,0x67D6E1A6L,0x5061EDB6L},{(-7L),0xE8D01CACL,0xDFF5778AL},{(-1L),0xF8BB3279L,6L},{(-2L),(-2L),0x50B333D8L},{(-1L),1L,0xF8BB3279L},{(-7L),0x50B333D8L,0x23F09553L},{1L,0x7AAED3E0L,0x7AAED3E0L},{(-4L),(-7L),0x23F09553L}},{{0x5061EDB6L,0xE9ED8466L,0xF8BB3279L},{0xDFF5778AL,1L,0x50B333D8L},{6L,(-5L),6L},{0x50B333D8L,1L,0xDFF5778AL},{0xF8BB3279L,0xE9ED8466L,0x5061EDB6L},{0x23F09553L,(-7L),(-4L)},{0x7AAED3E0L,0x7AAED3E0L,1L},{0x23F09553L,0x50B333D8L,(-7L)},{0xF8BB3279L,1L,(-1L)}},{{0x50B333D8L,(-2L),(-2L)},{6L,0xF8BB3279L,(-1L)},{0xDFF5778AL,0xE8D01CACL,(-7L)},{0x5061EDB6L,0x67D6E1A6L,1L},{(-4L),0x0FB9C23DL,(-4L)},{1L,0x67D6E1A6L,0x5061EDB6L},{(-7L),0xE8D01CACL,0xDFF5778AL},{(-1L),0xF8BB3279L,6L},{(-2L),(-2L),0x50B333D8L}},{{(-1L),1L,(-5L)},{0x0FB9C23DL,(-2L),0xDFF5778AL},{0x7AAED3E0L,0L,0L},{1L,0x0FB9C23DL,0xDFF5778AL},{6L,(-1L),(-5L)},{(-4L),0x50B333D8L,(-2L)},{0x67D6E1A6L,2L,0x67D6E1A6L},{(-2L),0x50B333D8L,(-4L)},{(-5L),(-1L),6L}}};
    int16_t *l_1642 = &g_370[0][0];
    uint32_t *l_1674 = (void*)0;
    union U1 **l_1704 = (void*)0;
    union U1 ***l_1703 = &l_1704;
    uint8_t l_1706 = 246UL;
    uint32_t l_1788[1][6];
    int32_t *l_1824 = (void*)0;
    int32_t l_1876 = 0x5DDCDEB5L;
    uint64_t ** const *l_1895 = &g_332;
    int32_t *l_1902[7][1][5] = {{{&l_1613[3][1][1],&g_33[8][9],&g_33[6][1],&g_33[6][1],&g_33[8][9]}},{{(void*)0,(void*)0,&g_33[6][1],(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_33[6][1],(void*)0}},{{&g_33[8][9],&l_1613[3][1][1],(void*)0,&l_1613[3][1][1],&g_33[8][9]}},{{(void*)0,&l_1613[3][1][1],(void*)0,&g_33[8][9],(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_33[8][9],&g_33[8][2]}},{{&l_1613[3][1][1],(void*)0,(void*)0,&l_1613[3][1][1],(void*)0}}};
    volatile struct S0 *l_2019 = &g_2020;
    uint8_t l_2082 = 0xD4L;
    uint64_t *l_2083 = &g_306;
    int64_t *l_2091 = (void*)0;
    int64_t *l_2092 = &g_699[0][2][0];
    int32_t l_2093 = (-8L);
    int32_t l_2094 = 1L;
    int32_t *l_2095 = &g_1796;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
            l_1788[i][j] = 0UL;
    }
    for (g_177 = 0; (g_177 < 42); g_177++)
    { /* block id: 661 */
        int32_t **l_1586 = &g_391[3][3][5];
        struct S0 *l_1655 = &g_769;
        int32_t *l_1744 = (void*)0;
        volatile int32_t *l_1780 = (void*)0;
        const int32_t l_1869 = 0xCD4FE8D5L;
        int32_t l_1883 = 0L;
        union U1 *** const *l_2065 = &l_1703;
        (*l_1586) = &p_55;
    }
    l_2094 &= (((*l_2019) , (safe_mod_func_uint64_t_u_u(((*l_2083) = l_2082), ((p_54 ^ ((g_2084[1] = g_2084[1]) != &g_320[1])) & ((safe_add_func_int16_t_s_s(p_54, ((p_54 < ((safe_mod_func_uint32_t_u_u(((~((*l_2092) = ((safe_unary_minus_func_uint8_t_u(((***g_1100) = (p_54 , 0xA5L)))) , p_54))) ^ l_2093), (-1L))) > p_55)) <= p_55))) >= 4294967295UL))))) <= p_54);
    return l_2095;
}


/* ------------------------------------------ */
/* 
 * reads : g_1100 g_577 g_996 g_542.f1 g_501 g_650 g_656 g_1122 g_163 g_769.f2 g_108 g_890.f0 g_502 g_177 g_487 g_1102 g_889 g_890 g_1101 g_1160 g_511.f3 g_149 g_695.f2 g_1162 g_918.f3 g_699 g_1172 g_172 g_784.f0 g_309 g_73 g_370 g_784.f3 g_106 g_1201 g_769.f0 g_321 g_137 g_138 g_134 g_162 g_779.f6 g_580 g_791 g_1272 g_1308 g_333 g_1357 g_14 g_1379 g_589.f3 g_695.f5 g_1397 g_418.f3 g_15 g_13 g_140 g_769.f3 g_141.f2 g_33 g_171 g_555 g_556 g_17 g_306 g_1560 g_1470 g_1471 g_1580 g_1583
 * writes: g_391 g_1100 g_172 g_163 g_502 g_73 g_177 g_501 g_699 g_487 g_149 g_108 g_309 g_889 g_171 g_656 g_370 g_134 g_48 g_1398 g_577 g_1470 g_106 g_138 g_769.f3
 */
static int8_t  func_63(uint16_t * p_64, float  p_65, uint16_t * p_66, uint16_t * p_67)
{ /* block id: 457 */
    uint32_t l_1089 = 0UL;
    uint64_t l_1092 = 0UL;
    int32_t **l_1095 = &g_391[4][2][2];
    uint8_t ****l_1103 = &g_1100;
    const int16_t l_1104[9][8][2] = {{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}},{{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L},{1L,(-10L)},{1L,1L}}};
    int32_t *l_1105 = &g_172[4][1];
    int32_t **l_1106 = &l_1105;
    int32_t *l_1107 = &g_163;
    const uint8_t ** const * const *l_1110 = (void*)0;
    const uint8_t ** const * const **l_1109 = &l_1110;
    int32_t l_1129 = 0x06CF87F2L;
    int8_t *l_1184 = &g_108;
    int32_t l_1212 = 0L;
    int32_t l_1213 = (-3L);
    int64_t l_1214 = 4L;
    int32_t l_1215 = (-6L);
    int16_t l_1216 = 0xE220L;
    int32_t l_1222 = 0x2ADE5526L;
    int32_t l_1225 = 0x0EB99064L;
    int32_t l_1226 = (-1L);
    int32_t l_1232[5][2][10] = {{{0xB338F3F6L,0xB338F3F6L,1L,0L,0x3D7AA957L,0x6EA12A3DL,1L,0x6EA12A3DL,0x3D7AA957L,0L},{0L,0xF2CC44EEL,0L,0x6EA12A3DL,6L,(-1L),1L,1L,(-1L),6L}},{{1L,0xB338F3F6L,0xB338F3F6L,1L,0L,0x3D7AA957L,0x6EA12A3DL,1L,0x6EA12A3DL,0x3D7AA957L},{(-1L),0xFBE92538L,0L,0xFBE92538L,(-1L),0x052E5363L,0xB338F3F6L,0x6EA12A3DL,0x6EA12A3DL,0xB338F3F6L}},{{1L,0x052E5363L,1L,1L,0x052E5363L,1L,6L,0xB338F3F6L,(-1L),0xB338F3F6L},{0xFBE92538L,1L,(-1L),0x6EA12A3DL,(-1L),1L,0xFBE92538L,6L,0x3D7AA957L,0x3D7AA957L}},{{0xFBE92538L,0x3D7AA957L,1L,0L,0L,1L,0x3D7AA957L,0xFBE92538L,0x052E5363L,6L},{1L,0x3D7AA957L,0xFBE92538L,0x052E5363L,6L,0x052E5363L,0xFBE92538L,0x3D7AA957L,1L,0L}},{{(-1L),1L,0xFBE92538L,6L,0x3D7AA957L,0x3D7AA957L,6L,0xFBE92538L,1L,(-1L)},{1L,0x052E5363L,1L,6L,0xB338F3F6L,(-1L),0xB338F3F6L,6L,1L,0x052E5363L}}};
    union U1 *l_1250 = &g_1122;
    int16_t l_1264 = 0xBB32L;
    int16_t l_1292[5];
    int32_t * const l_1356 = &g_33[8][4];
    int32_t *l_1406 = &g_163;
    volatile struct S0 *l_1417 = &g_1418;
    int32_t l_1446 = 5L;
    int16_t l_1573[9][7][2] = {{{1L,0x674EL},{0L,0x88E6L},{0x674EL,0x88E6L},{0L,0x674EL},{1L,1L},{1L,0x674EL},{0L,0x88E6L}},{{0x674EL,0x88E6L},{0L,0x674EL},{1L,1L},{1L,0x674EL},{0L,0x88E6L},{0x674EL,0x88E6L},{0L,0x674EL}},{{1L,1L},{1L,0x674EL},{0L,0x88E6L},{0x674EL,0x88E6L},{0L,0x674EL},{1L,1L},{1L,0x674EL}},{{0L,0x88E6L},{0x674EL,0x88E6L},{0L,0x674EL},{1L,1L},{1L,0x674EL},{0L,0x88E6L},{0x674EL,0x88E6L}},{{0L,0x674EL},{1L,1L},{1L,0x674EL},{0L,0x88E6L},{0x674EL,0x88E6L},{0L,0x674EL},{1L,1L}},{{1L,0x674EL},{0L,0x88E6L},{0x674EL,0x88E6L},{0L,0L},{0x674EL,0x674EL},{0x674EL,0L},{0L,1L}},{{0L,1L},{0L,0L},{0x674EL,0x674EL},{0x674EL,0L},{0L,1L},{0L,1L},{0L,0L}},{{0x674EL,0x674EL},{0x674EL,0L},{0L,1L},{0L,1L},{0L,0L},{0x674EL,0x674EL},{0x674EL,0L}},{{0L,1L},{0L,1L},{0L,0L},{0x674EL,0x674EL},{0x674EL,0L},{0L,1L},{0L,1L}}};
    int64_t *l_1581 = &l_1214;
    uint32_t *l_1582[1][10][1] = {{{&g_177},{&g_656[0]},{&g_177},{&g_656[0]},{&g_177},{&g_656[0]},{&g_177},{&g_656[0]},{&g_177},{&g_656[0]}}};
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_1292[i] = 0x715FL;
    if (((*l_1107) = (1L > (safe_div_func_int32_t_s_s((safe_add_func_uint64_t_u_u(0x9B69B4F6529F9C1CLL, (0x31ACL & (safe_mod_func_int64_t_s_s(l_1089, (safe_mod_func_int16_t_s_s((((l_1092 > l_1089) >= (safe_mod_func_uint64_t_u_u(((((((*l_1095) = &g_33[5][6]) == ((*l_1106) = (((*l_1105) = (0xF5A62AD12D6B2F94LL ^ ((safe_rshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_s(((((*l_1103) = g_1100) != &g_1101) > l_1092), l_1092)) > l_1104[3][3][1]), (*p_66))) >= l_1104[6][1][0]))) , (void*)0))) , l_1104[3][3][1]) != (-1L)) <= 1L), g_996))) && (-1L)), g_542.f1))))))), 0x6D5295ACL)))))
    { /* block id: 463 */
        const uint8_t ** const * const **l_1108 = (void*)0;
        int32_t l_1123 = 0x0785DFF1L;
        int32_t l_1217 = 2L;
        int32_t l_1219 = 0xE93AD22EL;
        int32_t l_1220 = 1L;
        int32_t l_1223 = 0xAF0CE402L;
        int32_t l_1224 = 0x898391CDL;
        int32_t l_1233[1];
        uint16_t **l_1253 = &g_138;
        int32_t **l_1268 = (void*)0;
        int16_t l_1344 = 0x9C07L;
        float * const l_1345 = (void*)0;
        uint8_t l_1346 = 7UL;
        int64_t l_1445[5];
        int32_t l_1452 = 0L;
        const uint32_t *l_1468 = (void*)0;
        struct S0 *l_1495 = &g_1496;
        int i;
        for (i = 0; i < 1; i++)
            l_1233[i] = 7L;
        for (i = 0; i < 5; i++)
            l_1445[i] = 0xA3A5EFFDFF63C802LL;
lbl_1351:
        for (g_502 = 1; (g_502 >= 0); g_502 -= 1)
        { /* block id: 466 */
            int32_t l_1118 = 0xACC4FB5AL;
            int32_t l_1128 = (-4L);
            const uint8_t *l_1156 = (void*)0;
            int32_t l_1164 = 0xA4941585L;
            const union U1 *l_1176 = &g_1122;
            const union U1 * const *l_1175 = &l_1176;
            int8_t l_1202 = 0xF3L;
            int32_t l_1218 = 0L;
            int32_t l_1229 = 1L;
            int16_t l_1234[3];
            int32_t l_1238 = 0xEB03B602L;
            uint8_t ***l_1254 = &g_1101;
            int i;
            for (i = 0; i < 3; i++)
                l_1234[i] = 0xCF25L;
            l_1109 = l_1108;
            (*l_1107) = 8L;
            for (g_73 = 0; (g_73 >= 0); g_73 -= 1)
            { /* block id: 471 */
                uint32_t l_1121 = 1UL;
                uint8_t ***l_1136[9] = {&g_1101,&g_1101,&g_1101,&g_1101,&g_1101,&g_1101,&g_1101,&g_1101,&g_1101};
                int32_t *l_1165[8] = {&g_33[4][1],(void*)0,&g_33[4][1],&g_33[4][1],(void*)0,&g_33[4][1],&g_33[4][1],(void*)0};
                int32_t * const **l_1174 = &g_1173;
                uint16_t *l_1205 = &g_134;
                int i;
                for (g_177 = 0; (g_177 <= 3); g_177 += 1)
                { /* block id: 474 */
                    uint64_t l_1130[7][1][6] = {{{18446744073709551615UL,18446744073709551615UL,0xA38C65DEB2B6ABEBLL,0x593C13FA8A9F6D91LL,0xA38C65DEB2B6ABEBLL,18446744073709551615UL}},{{0xA38C65DEB2B6ABEBLL,0xE56D0F2528175601LL,0x593C13FA8A9F6D91LL,0x593C13FA8A9F6D91LL,0xE56D0F2528175601LL,0xA38C65DEB2B6ABEBLL}},{{18446744073709551615UL,0xA38C65DEB2B6ABEBLL,0x593C13FA8A9F6D91LL,0xA38C65DEB2B6ABEBLL,18446744073709551615UL,18446744073709551615UL}},{{0UL,0xA38C65DEB2B6ABEBLL,0xA38C65DEB2B6ABEBLL,0UL,0xE56D0F2528175601LL,0UL}},{{0UL,0xE56D0F2528175601LL,0UL,0xA38C65DEB2B6ABEBLL,0xA38C65DEB2B6ABEBLL,0UL}},{{18446744073709551615UL,18446744073709551615UL,0xA38C65DEB2B6ABEBLL,0x593C13FA8A9F6D91LL,0xA38C65DEB2B6ABEBLL,18446744073709551615UL}},{{0xA38C65DEB2B6ABEBLL,0xE56D0F2528175601LL,0x593C13FA8A9F6D91LL,0x593C13FA8A9F6D91LL,0xE56D0F2528175601LL,0xA38C65DEB2B6ABEBLL}}};
                    uint32_t l_1146 = 0x985C9E4CL;
                    uint64_t ***l_1148 = &g_332;
                    int32_t * const *l_1171 = &g_949;
                    int32_t * const **l_1170 = &l_1171;
                    int i, j, k;
                    for (g_501 = 0; (g_501 >= 0); g_501 -= 1)
                    { /* block id: 477 */
                        int i, j;
                        return g_650[(g_501 + 3)][g_501];
                    }
                    if ((safe_lshift_func_uint16_t_u_u((safe_div_func_int32_t_s_s((safe_unary_minus_func_int32_t_s((safe_add_func_uint32_t_u_u((0xC088L || g_656[2]), (((l_1118 > (safe_lshift_func_int16_t_s_s((0xE5L | l_1121), 12))) | (g_1122 , (l_1118 , ((((((8L ^ ((*l_1107) || 0x0EL)) , 0xA209CB9BL) >= 0xDE2660CAL) >= l_1123) < 0UL) <= 0x223716B50545B85FLL)))) < g_769.f2))))), g_108)), 10)))
                    { /* block id: 480 */
                        int32_t *l_1124 = (void*)0;
                        int32_t *l_1125 = &l_1123;
                        int32_t *l_1126 = &l_1123;
                        int32_t *l_1127[6][8] = {{&l_1123,&g_33[3][8],&g_33[3][8],&l_1123,&g_163,&g_33[8][2],(void*)0,&g_3},{&g_3,&g_172[0][0],(void*)0,&g_33[3][8],&g_33[8][2],&g_13,(void*)0,&g_13},{&g_13,&g_172[0][0],&g_172[4][1],&g_172[4][1],(void*)0,&g_33[8][2],(void*)0,&g_172[4][1]},{&g_163,&g_33[3][8],&g_163,&g_172[1][1],&g_36,&g_172[4][1],&l_1123,(void*)0},{&g_163,(void*)0,&g_3,&g_15,&g_33[3][8],(void*)0,&g_36,&g_172[0][0]},{&g_163,&g_172[4][1],(void*)0,&g_172[3][1],&g_36,&g_36,&g_172[3][1],(void*)0}};
                        uint8_t ***l_1135[3];
                        int32_t **l_1143 = &g_949;
                        uint32_t *l_1147 = &g_487;
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                            l_1135[i] = (void*)0;
                        ++l_1130[0][0][5];
                        l_1128 = (0x6B0CL > (safe_mod_func_int16_t_s_s((((((*g_1102) = ((l_1135[0] == l_1136[5]) && (((*l_1147) &= (safe_lshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(((safe_mod_func_uint16_t_u_u((l_1143 != (void*)0), (g_890.f0 || ((safe_sub_func_int64_t_s_s((g_699[g_502][g_177][g_502] = (((*l_1126) || ((((((&g_699[2][3][0] == (void*)0) , (*p_66)) >= (*p_66)) > 0xD8L) >= l_1130[1][0][2]) > 0xE7E8L)) >= l_1130[5][0][4])), (*l_1107))) ^ l_1128)))) && l_1146), l_1146)), l_1118))) , l_1128))) , (*g_889)) , l_1148) == &g_333), l_1118)));
                    }
                    else
                    { /* block id: 486 */
                        return l_1121;
                    }
                    for (g_163 = 1; (g_163 <= 7); g_163 += 1)
                    { /* block id: 491 */
                        const uint8_t **l_1157 = &l_1156;
                        int32_t l_1161[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
                        int32_t *l_1163[6];
                        const union U1 * const **l_1177 = &l_1175;
                        uint32_t *l_1178 = &l_1121;
                        int i;
                        for (i = 0; i < 6; i++)
                            l_1163[i] = &l_1129;
                        l_1164 = ((safe_rshift_func_int16_t_s_s(((&g_177 == ((((safe_unary_minus_func_int32_t_s(((safe_add_func_int32_t_s_s(((safe_mul_func_uint16_t_u_u((((l_1121 && (((*l_1157) = l_1156) != (*g_1101))) >= 0xF2L) > ((void*)0 != &g_320[1])), ((((((safe_mul_func_int8_t_s_s((((l_1130[0][0][5] || (l_1128 = (((*g_1102) = (g_1160 , l_1161[3])) , 247UL))) <= (-10L)) != l_1130[1][0][1]), l_1146)) , l_1146) >= g_511.f3) , (*g_1102)) , 0x8A26L) && 0x7A26L))) >= g_695.f2), 0xB904DFA5L)) || l_1118))) , g_1162) , g_918.f3) , &l_1121)) < l_1146), g_699[0][2][0])) < 0x9EL);
                        g_391[0][3][7] = l_1165[4];
                        l_1123 = ((g_108 = (safe_div_func_uint64_t_u_u(0xBC3B12E835DCDC01LL, (safe_mul_func_uint16_t_u_u(((*p_66) | (l_1170 == (l_1174 = g_1172))), ((l_1128 = ((g_172[4][1] ^ (((*l_1178) = (((((*l_1177) = l_1175) == &g_889) , 0x81E0696EL) > g_784.f0)) <= l_1123)) && 1L)) , 1L)))))) < 252UL);
                    }
                }
                for (g_309 = 0; (g_309 <= 7); g_309 += 1)
                { /* block id: 507 */
                    int8_t *l_1183 = &g_108;
                    const int64_t * const l_1192 = &g_699[0][0][0];
                    int32_t l_1198 = 0x5A102312L;
                    int32_t l_1228[1][7] = {{0L,0L,0L,0L,0L,0L,0L}};
                    int64_t l_1236[7][9][4] = {{{0x23DAA7C94C0CF7BALL,6L,0x86DF5281BE8B41A8LL,(-1L)},{1L,0x62D2218F41F208C4LL,0x3C7F844F84C54B05LL,0x1EFE883BE0E85692LL},{0xB271AA4F5B028926LL,(-9L),0x630912D67762E017LL,(-1L)},{0xEE1C809F9CE966F0LL,(-1L),3L,1L},{0x9147AF1A9E864B82LL,0x202F9FF821B38402LL,0xEE1C809F9CE966F0LL,0x587CAE9D5BB8FBF8LL},{0x682D516D1E420482LL,0xCE18C0AE0805A855LL,7L,1L},{1L,(-7L),(-9L),(-10L)},{0x785A7C967DA8C777LL,(-1L),0xCE18C0AE0805A855LL,0xCE18C0AE0805A855LL},{0xBFDA070F7E1EA20ELL,0xBFDA070F7E1EA20ELL,0x6D5FA910D681EDCCLL,(-4L)}},{{4L,0L,1L,0xEE1C809F9CE966F0LL},{3L,1L,(-1L),1L},{0xEA6E10559BBF0A55LL,1L,(-7L),0xEE1C809F9CE966F0LL},{1L,0L,1L,(-4L)},{1L,0xBFDA070F7E1EA20ELL,1L,0xCE18C0AE0805A855LL},{(-4L),(-1L),1L,(-10L)},{0x630912D67762E017LL,(-7L),0x202F9FF821B38402LL,1L},{(-5L),0xCE18C0AE0805A855LL,0L,0x587CAE9D5BB8FBF8LL},{(-9L),0x202F9FF821B38402LL,1L,1L}},{{0xD38EFE34317E5AD3LL,(-1L),0x23DAA7C94C0CF7BALL,(-1L)},{0xCE18C0AE0805A855LL,3L,1L,0L},{0xB229153992516B1FLL,0x86DF5281BE8B41A8LL,0xFA10A1E6BBC28567LL,1L},{0x43943823A3121446LL,(-10L),4L,0x39CE9D83D53E2113LL},{0x59DD74E4036F6A88LL,1L,1L,0x62D2218F41F208C4LL},{6L,1L,0xCCB9167A8A2D834BLL,0x9147AF1A9E864B82LL},{0L,0xCCB9167A8A2D834BLL,(-5L),0xDCA8A9082FDF76FDLL},{0x62D2218F41F208C4LL,0xFC5666559BE6A670LL,(-1L),1L},{(-1L),1L,(-1L),0xF23C6468B4E5B4F2LL}},{{0xC6AEFE1BA19D8C1CLL,0x630912D67762E017LL,0L,(-1L)},{0xB271AA4F5B028926LL,1L,0xBFDA070F7E1EA20ELL,0x630912D67762E017LL},{0x6E5855736E71B3D4LL,(-1L),0xBFDA070F7E1EA20ELL,0x86DF5281BE8B41A8LL},{0xB271AA4F5B028926LL,0xF4DE1E802CF4CB04LL,0L,(-1L)},{0xC6AEFE1BA19D8C1CLL,(-10L),(-1L),0x97102EB79E729E06LL},{(-1L),0x97102EB79E729E06LL,(-1L),0x43943823A3121446LL},{0x62D2218F41F208C4LL,0x682D516D1E420482LL,(-5L),6L},{0L,(-1L),0xCCB9167A8A2D834BLL,5L},{6L,0L,1L,4L}},{{0x59DD74E4036F6A88LL,0x365EEB3B9C22F5D1LL,4L,1L},{0x43943823A3121446LL,(-8L),0xFA10A1E6BBC28567LL,(-7L)},{0xB229153992516B1FLL,(-5L),1L,0L},{0xCE18C0AE0805A855LL,(-4L),0x23DAA7C94C0CF7BALL,1L},{0xD38EFE34317E5AD3LL,0x43943823A3121446LL,1L,(-8L)},{(-9L),8L,0L,0x5ACB01A8789D6E57LL},{(-5L),0x23DAA7C94C0CF7BALL,0x202F9FF821B38402LL,1L},{0x630912D67762E017LL,0x3C7F844F84C54B05LL,1L,0xF4DE1E802CF4CB04LL},{(-4L),1L,1L,(-4L)}},{{1L,0xEE1C809F9CE966F0LL,1L,0xFA10A1E6BBC28567LL},{1L,0x62D2218F41F208C4LL,(-7L),0x6E5855736E71B3D4LL},{0xEA6E10559BBF0A55LL,(-1L),(-1L),0x6E5855736E71B3D4LL},{3L,0x62D2218F41F208C4LL,1L,0xFA10A1E6BBC28567LL},{4L,0xEE1C809F9CE966F0LL,0x6D5FA910D681EDCCLL,(-4L)},{0xBFDA070F7E1EA20ELL,(-4L),0L,5L},{7L,0xEE4C99BFD6CB2841LL,3L,1L},{0x62D2218F41F208C4LL,(-1L),(-8L),0x1EFE883BE0E85692LL},{0x365EEB3B9C22F5D1LL,0x59DD74E4036F6A88LL,(-7L),0x23DAA7C94C0CF7BALL}},{{0x202F9FF821B38402LL,1L,1L,0x587CAE9D5BB8FBF8LL},{(-7L),0xC6AEFE1BA19D8C1CLL,0x785A7C967DA8C777LL,1L},{1L,0x97102EB79E729E06LL,0xCE18C0AE0805A855LL,(-1L)},{(-10L),0x23DAA7C94C0CF7BALL,5L,4L},{6L,0xFC5666559BE6A670LL,0xDCA8A9082FDF76FDLL,1L},{(-4L),1L,0xF23C6468B4E5B4F2LL,0xB229153992516B1FLL},{5L,6L,7L,(-10L)},{0x6E5855736E71B3D4LL,0x365EEB3B9C22F5D1LL,4L,1L},{0x682D516D1E420482LL,0xEE1C809F9CE966F0LL,0x1EFE883BE0E85692LL,0xEE1C809F9CE966F0LL}}};
                    int16_t l_1239 = 0xC51FL;
                    uint32_t l_1241[3];
                    uint64_t l_1255 = 0xD8BAD68D84BC1D2FLL;
                    float *l_1261[4][4] = {{&g_171,&g_171,&g_48,&g_171},{&g_171,&g_171,&g_171,&g_171},{&g_171,&g_171,&g_171,&g_171},{&g_171,&g_171,&g_48,&g_171}};
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_1241[i] = 4294967295UL;
                    if ((safe_add_func_int64_t_s_s((safe_mod_func_int64_t_s_s((l_1183 != l_1184), ((g_650[g_309][g_73] , (g_650[(g_73 + 2)][g_73] == (((safe_rshift_func_int16_t_s_u(0L, 1)) <= ((safe_rshift_func_uint16_t_u_s((+(safe_lshift_func_int8_t_s_s(((((void*)0 == l_1192) == 0x026E8DE0L) , (safe_div_func_uint64_t_u_u((((!(safe_rshift_func_int16_t_s_s(((((*l_1107) = ((-7L) != (*p_66))) && 0L) != g_370[0][0]), g_784.f3))) | g_309) , 0x245047AF915D50B2LL), g_699[0][2][1]))), 7))), g_106[1])) && 2UL)) , l_1164))) & l_1198))), 0L)))
                    { /* block id: 509 */
                        int8_t ** const l_1210 = &l_1183;
                        int32_t l_1211 = 0L;
                        int32_t l_1221 = 0x1B02D944L;
                        int32_t l_1227 = 0xA7CDF571L;
                        int32_t l_1230 = (-10L);
                        int16_t l_1231 = 1L;
                        int32_t l_1235 = (-6L);
                        int32_t l_1237 = 1L;
                        int32_t l_1240 = 0x71E6A119L;
                        union U1 **l_1251 = &g_889;
                        int16_t *l_1252 = &l_1234[2];
                        (*g_162) = (safe_rshift_func_int16_t_s_s(1L, (g_1201 , (l_1202 <= (safe_mod_func_int8_t_s_s((((void*)0 == l_1205) , (safe_mul_func_uint16_t_u_u(((((safe_sub_func_int32_t_s_s(g_650[g_309][g_73], (((0x5966CEAEAF345305LL != ((((l_1210 == (void*)0) & 1L) >= l_1198) != 0x607FB43DL)) != l_1123) == l_1123))) , g_769.f0) | l_1211) | 4UL), (***g_321)))), 0xE9L))))));
                        l_1241[0]--;
                        (*l_1107) = ((((safe_mul_func_uint8_t_u_u((***g_1100), (safe_mod_func_uint32_t_u_u(((1UL < (safe_add_func_uint16_t_u_u(((((*l_1251) = l_1250) == (((*l_1252) |= g_779.f6) , (void*)0)) || (((l_1231 != 0x6FL) , ((void*)0 == l_1253)) ^ l_1220)), (-6L)))) & 0x003AL), l_1221)))) , 4L) , &g_1101) != l_1254);
                        l_1255--;
                    }
                    else
                    { /* block id: 516 */
                        uint16_t l_1258[7][7] = {{0x6A4DL,0x1283L,0xF408L,0x6A4DL,0xF408L,0x1283L,0x6A4DL},{0x1AADL,0x6A4DL,0x1283L,0xF408L,0x6A4DL,0xF408L,0x1283L},{0x6A4DL,0x6A4DL,0xFA6EL,0x9DE7L,0UL,0xFA6EL,0UL},{0x9DE7L,0x1283L,0x1283L,0x9DE7L,0xF408L,0x1AADL,0x9DE7L},{0x1AADL,0UL,0xF408L,0xF408L,0UL,0x1AADL,0x1283L},{0UL,0x9DE7L,0xFA6EL,0x6A4DL,0x6A4DL,0xFA6EL,0x9DE7L},{0UL,0x1283L,0x1AADL,0UL,0xF408L,0xF408L,0UL}};
                        int i, j;
                        (*l_1095) = (void*)0;
                        --l_1258[5][0];
                    }
                    (*g_580) = p_65;
                }
            }
        }
        for (l_1092 = 0; (l_1092 >= 11); ++l_1092)
        { /* block id: 526 */
            return l_1264;
        }
        if ((~l_1123))
        { /* block id: 529 */
            float *l_1269[9] = {&g_171,&g_171,&g_171,&g_171,&g_171,&g_171,&g_171,&g_171,&g_171};
            int32_t l_1274 = 0x6369009FL;
            int32_t l_1286 = 0L;
            int32_t l_1287 = 0xD6581181L;
            int32_t l_1288 = 1L;
            int32_t l_1289 = 1L;
            int32_t l_1290 = (-1L);
            int32_t l_1291 = 0xA0615DFBL;
            int32_t l_1294 = 0L;
            int32_t l_1295 = 0L;
            int32_t l_1296 = 0xF2586483L;
            int32_t l_1297 = 1L;
            int32_t l_1298[8][7][3] = {{{0x26B2F794L,0x262D8154L,0xCB77B636L},{0xB8B0F26FL,0x5E074AE2L,9L},{1L,0x77675E84L,(-1L)},{(-1L),0L,(-1L)},{(-1L),0x77675E84L,1L},{9L,0x5E074AE2L,0xB8B0F26FL},{0xCB77B636L,0x262D8154L,0x26B2F794L}},{{0x708BD124L,0x708BD124L,0L},{0xCB77B636L,0x766853FBL,(-5L)},{9L,0L,0xDC4D3593L},{(-1L),0xBE4D1F76L,0x0841D946L},{(-1L),9L,0xDC4D3593L},{1L,3L,(-5L)},{0xB8B0F26FL,1L,0L}},{{0x26B2F794L,0x2B161A5EL,0x26B2F794L},{0L,1L,0xB8B0F26FL},{(-5L),3L,1L},{0xDC4D3593L,9L,(-1L)},{0x0841D946L,0xBE4D1F76L,(-1L)},{0xDC4D3593L,0L,9L},{(-5L),0x766853FBL,0xCB77B636L}},{{0L,0x708BD124L,0x708BD124L},{(-1L),0x2B161A5EL,1L},{(-1L),0xDC4D3593L,0L},{0x26B2F794L,0x766853FBL,0x0841D946L},{1L,0x2BCB96A8L,1L},{0x0841D946L,0x766853FBL,0x26B2F794L},{0L,0xDC4D3593L,(-1L)}},{{1L,0x2B161A5EL,(-1L)},{9L,9L,0x708BD124L},{1L,0xBE4D1F76L,0xB5F55654L},{0L,0x708BD124L,0xB8B0F26FL},{0x0841D946L,(-1L),(-1L)},{1L,0L,0xB8B0F26FL},{0x26B2F794L,0x315D0303L,0xB5F55654L}},{{(-1L),0L,0x708BD124L},{(-1L),0xE7A12207L,(-1L)},{0x708BD124L,0L,(-1L)},{0xB5F55654L,0x315D0303L,0x26B2F794L},{0xB8B0F26FL,0L,1L},{(-1L),(-1L),0x0841D946L},{0xB8B0F26FL,0x708BD124L,0L}},{{0xB5F55654L,0xBE4D1F76L,1L},{0x708BD124L,9L,9L},{(-1L),0x2B161A5EL,1L},{(-1L),0xDC4D3593L,0L},{0x26B2F794L,0x766853FBL,0x0841D946L},{1L,0x2BCB96A8L,1L},{0x0841D946L,0x766853FBL,0x26B2F794L}},{{0L,0xDC4D3593L,(-1L)},{1L,0x2B161A5EL,(-1L)},{9L,9L,0x708BD124L},{1L,0xBE4D1F76L,0xB5F55654L},{0L,0x708BD124L,0xB8B0F26FL},{0x0841D946L,(-1L),(-1L)},{1L,0L,0xB8B0F26FL}}};
            int i, j, k;
            p_65 = ((*g_791) = (safe_mul_func_float_f_f(((void*)0 != l_1268), (p_65 >= 0x1.685DB0p+18))));
            for (g_163 = 0; (g_163 > 26); ++g_163)
            { /* block id: 534 */
                uint64_t *l_1273 = (void*)0;
                int32_t l_1282 = 1L;
                int32_t l_1283 = 0x5C577E82L;
                int32_t l_1284 = 1L;
                int32_t l_1285[2];
                uint16_t l_1299 = 1UL;
                union U1 *l_1314 = &g_1315;
                float l_1316 = 0xA.B63D76p+59;
                uint32_t *l_1332 = (void*)0;
                uint32_t *l_1333 = &g_656[5];
                int32_t *l_1347 = (void*)0;
                int32_t *l_1348 = &g_172[5][0];
                int i;
                for (i = 0; i < 2; i++)
                    l_1285[i] = 0xCB530BCDL;
                if (((g_1272 , l_1273) == (void*)0))
                { /* block id: 535 */
                    uint32_t *l_1275 = &g_656[1];
                    int32_t *l_1280 = &l_1232[0][0][1];
                    int32_t *l_1281[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1281[i] = &l_1220;
                    (*l_1280) = ((++(*l_1275)) | (+(safe_unary_minus_func_int64_t_s(g_1201.f3))));
                    ++l_1299;
                }
                else
                { /* block id: 539 */
                    int16_t *l_1319 = (void*)0;
                    int16_t *l_1320 = (void*)0;
                    int16_t *l_1321 = &l_1292[3];
                    int16_t *l_1322[3][9][9] = {{{&l_1216,&l_1216,(void*)0,&g_370[1][0],(void*)0,&l_1216,&g_106[0],&l_1264,(void*)0},{&l_1264,&g_106[1],&l_1216,&l_1216,&g_106[1],&l_1216,(void*)0,&g_106[1],&l_1264},{&g_106[1],&g_106[0],(void*)0,(void*)0,&l_1264,(void*)0,&g_106[2],&g_106[1],&g_106[2]},{&l_1216,&l_1216,&g_106[1],&g_106[1],&l_1216,&l_1216,&g_106[0],(void*)0,&g_370[0][0]},{&g_106[2],&g_106[2],&g_370[1][0],(void*)0,&l_1216,&g_106[1],(void*)0,(void*)0,&g_370[3][0]},{&g_370[2][0],&g_106[1],&g_106[1],&l_1264,&g_370[0][0],(void*)0,&g_106[0],&l_1216,&l_1264},{&g_370[3][0],(void*)0,&g_106[0],&g_106[2],&g_370[3][0],&g_370[3][0],&g_106[2],&g_106[0],(void*)0},{&g_106[1],&g_370[2][0],&l_1264,&g_370[0][0],&l_1216,&l_1216,(void*)0,&l_1264,&l_1216},{(void*)0,(void*)0,(void*)0,&g_370[3][0],&g_106[1],&g_106[1],&g_106[0],&g_106[2],&g_106[1]}},{{(void*)0,&g_370[2][0],&g_106[0],&l_1264,&g_370[2][0],&l_1264,&l_1216,&l_1216,&l_1216},{&l_1216,&l_1264,&g_370[1][0],&l_1264,&l_1216,(void*)0,&l_1216,&g_106[1],&g_106[2]},{&g_370[0][0],(void*)0,(void*)0,&g_106[1],&l_1216,&g_106[1],&g_106[2],(void*)0,&l_1264},{&g_106[1],&g_370[3][0],(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_1264,&g_106[0]},{&l_1216,&g_106[1],(void*)0,&l_1216,&g_370[2][0],&l_1216,(void*)0,&g_106[1],&l_1216},{&g_106[2],&l_1264,&l_1264,&g_106[2],&g_370[0][0],&g_106[1],&l_1216,(void*)0,(void*)0},{&g_106[1],(void*)0,&l_1216,&l_1264,&l_1264,&g_106[1],(void*)0,(void*)0,(void*)0},{&g_106[2],&g_370[0][0],&l_1216,&g_106[0],&g_106[1],&l_1216,&g_106[2],&l_1216,&g_106[1]},{&l_1216,&g_106[1],&g_106[1],&l_1216,(void*)0,&g_370[0][0],&l_1264,&l_1264,&g_106[1]}},{{&g_106[1],&g_106[0],&g_106[1],(void*)0,&l_1216,&g_370[1][0],(void*)0,&g_106[2],&l_1264},{&g_370[0][0],&g_106[1],&g_106[1],(void*)0,(void*)0,&l_1264,&g_370[0][0],&g_106[0],(void*)0},{&l_1216,&g_106[1],&g_106[1],&g_106[1],&g_106[1],&g_106[1],&g_106[1],&l_1216,&l_1216},{&g_106[2],&l_1264,(void*)0,&g_106[1],&l_1264,&g_106[1],&g_370[1][0],&l_1216,(void*)0},{&l_1216,(void*)0,&g_106[1],&l_1264,&g_370[0][0],&g_106[1],&g_106[2],(void*)0,&l_1216},{(void*)0,(void*)0,&g_106[2],(void*)0,&g_370[2][0],(void*)0,(void*)0,&g_370[2][0],(void*)0},{(void*)0,(void*)0,(void*)0,&l_1216,(void*)0,&g_106[2],&g_106[1],&g_370[0][0],&l_1264},{(void*)0,&g_370[0][0],&l_1216,(void*)0,&l_1216,&g_370[1][0],&g_106[1],&l_1264,&g_106[1]},{(void*)0,(void*)0,&g_370[3][0],&l_1216,&l_1216,&g_106[1],&g_106[1],&g_106[1],&g_106[1]}}};
                    int32_t l_1323 = 0x196FA2B6L;
                    int32_t *l_1324 = (void*)0;
                    int32_t *l_1325 = (void*)0;
                    int32_t *l_1326 = &g_172[2][1];
                    int i, j, k;
                    (*l_1326) &= (l_1323 = (safe_lshift_func_int16_t_s_u((safe_rshift_func_uint8_t_u_u(((l_1285[1] , ((safe_sub_func_uint8_t_u_u(g_1308[6], (+((((((g_370[3][0] = ((safe_add_func_uint16_t_u_u(((0UL <= (safe_mul_func_uint16_t_u_u((l_1283 > (((void*)0 != l_1314) , (255UL ^ (*l_1107)))), ((*l_1321) = (safe_lshift_func_int16_t_s_s((l_1299 ^ 3L), 14)))))) >= (*l_1107)), l_1298[6][2][2])) , 1L)) > l_1287) , (void*)0) != &g_1101) && 0x94D750D8D8EA8361LL) != l_1299)))) , l_1323)) > (-1L)), 2)), 4)));
                }
                (*l_1348) = (safe_mul_func_uint16_t_u_u(((safe_unary_minus_func_int16_t_s((safe_rshift_func_int8_t_s_u((-1L), 0)))) == ((*l_1333) = g_1201.f1)), ((safe_div_func_uint8_t_u_u(((l_1274 = (l_1346 = (l_1291 &= (((g_177 > (p_65 = ((safe_div_func_float_f_f((((*p_66) | ((((((((l_1286 , (safe_div_func_uint16_t_u_u(((*g_138) |= ((safe_rshift_func_uint16_t_u_s((l_1282 <= l_1290), ((safe_div_func_int8_t_s_s(0x98L, l_1287)) , l_1295))) , l_1344)), l_1286))) , (void*)0) == l_1345) && 5L) , g_333) != g_333) == (*l_1107)) , 0x13DCL)) , g_784.f0), g_511.f3)) < (-0x2.9p+1)))) , (void*)0) != (void*)0)))) ^ l_1284), (***g_1100))) ^ 255UL)));
            }
            for (l_1092 = (-24); (l_1092 >= 42); l_1092 = safe_add_func_int8_t_s_s(l_1092, 2))
            { /* block id: 555 */
                int32_t *l_1352[9] = {(void*)0,&l_1286,(void*)0,(void*)0,&l_1286,(void*)0,(void*)0,&l_1286,(void*)0};
                uint8_t l_1353 = 251UL;
                int i;
                if (g_1201.f2)
                    goto lbl_1351;
                ++l_1353;
                if (l_1217)
                    continue;
            }
        }
        else
        { /* block id: 560 */
            int32_t *l_1358 = &l_1123;
            int32_t l_1359[8][6] = {{(-1L),0x3FCE40B9L,0x32A490F1L,0x32A490F1L,0x3FCE40B9L,(-1L)},{(-1L),2L,0x40169972L,0x32A490F1L,2L,0x32A490F1L},{(-1L),0x8E536292L,(-1L),0x32A490F1L,0x8E536292L,0x40169972L},{(-1L),0x3FCE40B9L,0x32A490F1L,0x32A490F1L,0x3FCE40B9L,(-1L)},{(-1L),2L,0x40169972L,0x409B1D57L,(-1L),0x409B1D57L},{0xDCC23273L,0x40169972L,0xDCC23273L,0x409B1D57L,0x40169972L,0L},{0xDCC23273L,0x32A490F1L,0x409B1D57L,0x409B1D57L,0x32A490F1L,0xDCC23273L},{0xDCC23273L,(-1L),0L,0x409B1D57L,(-1L),0x409B1D57L}};
            int32_t l_1412 = 1L;
            uint32_t l_1441 = 1UL;
            int32_t l_1453 = (-6L);
            int32_t l_1454 = 1L;
            int32_t l_1455 = 6L;
            int32_t l_1456[10];
            int32_t l_1457 = 0x1860FBA5L;
            int16_t l_1458 = 0xBAC9L;
            union U1 **l_1503 = (void*)0;
            int i, j;
            for (i = 0; i < 10; i++)
                l_1456[i] = (-1L);
            if ((*l_1107))
            { /* block id: 561 */
                union U1 **l_1372 = &g_889;
                union U1 ***l_1371[6][7][4] = {{{(void*)0,&l_1372,(void*)0,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,(void*)0,(void*)0,(void*)0},{&l_1372,&l_1372,&l_1372,&l_1372},{(void*)0,&l_1372,&l_1372,&l_1372},{(void*)0,(void*)0,&l_1372,(void*)0}},{{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{(void*)0,&l_1372,(void*)0,&l_1372},{&l_1372,&l_1372,&l_1372,(void*)0},{(void*)0,(void*)0,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372}},{{&l_1372,(void*)0,&l_1372,&l_1372},{(void*)0,(void*)0,(void*)0,&l_1372},{&l_1372,&l_1372,&l_1372,(void*)0},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,(void*)0,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372}},{{&l_1372,&l_1372,(void*)0,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,(void*)0,&l_1372},{(void*)0,&l_1372,&l_1372,(void*)0},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372}},{{(void*)0,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,(void*)0,&l_1372},{(void*)0,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,(void*)0},{&l_1372,(void*)0,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{(void*)0,&l_1372,&l_1372,&l_1372}},{{(void*)0,(void*)0,&l_1372,&l_1372},{&l_1372,(void*)0,(void*)0,(void*)0},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,(void*)0,&l_1372},{(void*)0,&l_1372,&l_1372,&l_1372},{&l_1372,&l_1372,&l_1372,&l_1372}}};
                int16_t *l_1373 = (void*)0;
                int16_t *l_1374[9];
                int32_t l_1378 = 0L;
                uint32_t *l_1380 = (void*)0;
                uint32_t *l_1381 = &l_1089;
                int32_t l_1396[1][6] = {{(-3L),0x6351C817L,0x7F5E6E86L,0x7F5E6E86L,(-3L),0x7F5E6E86L}};
                const uint8_t *l_1404 = &g_149;
                const uint8_t * const *l_1403 = &l_1404;
                const uint8_t * const **l_1402 = &l_1403;
                const uint8_t * const ***l_1401[3];
                uint8_t l_1413 = 0UL;
                uint32_t l_1443 = 3UL;
                uint8_t *****l_1462[3];
                struct S0 * const l_1494 = &g_784;
                int i, j, k;
                for (i = 0; i < 9; i++)
                    l_1374[i] = &l_1344;
                for (i = 0; i < 3; i++)
                    l_1401[i] = &l_1402;
                for (i = 0; i < 3; i++)
                    l_1462[i] = (void*)0;
                (*g_1357) = l_1356;
                (*l_1106) = l_1358;
                if ((((l_1359[6][4] > (*l_1358)) | ((*l_1381) = (((safe_rshift_func_int16_t_s_s(((safe_add_func_uint64_t_u_u((safe_unary_minus_func_uint16_t_u((*p_66))), (*l_1358))) != 0UL), (safe_div_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u((g_370[0][0] = ((*l_1107) <= (l_1371[0][1][2] == (void*)0))), ((~((((safe_lshift_func_uint16_t_u_s((**l_1106), g_14)) < l_1378) != 0x21L) , (*l_1358))) , 65535UL))), 0x6E634D7CL)), g_784.f0)))) | 5L) > g_1379))) & 0UL))
                { /* block id: 566 */
                    float l_1384 = 0xA.A9CCCBp-97;
                    int32_t l_1385 = 0L;
                    union U1 ** const l_1392 = &l_1250;
                    float *l_1394 = (void*)0;
                    float *l_1395 = &g_48;
                    if ((safe_add_func_int8_t_s_s(l_1385, (((((safe_mod_func_uint16_t_u_u(((0x7827C85EE904FB46LL != (~(*l_1358))) , ((safe_unary_minus_func_uint32_t_u((safe_sub_func_uint8_t_u_u(((l_1392 == ((!(*l_1358)) , (((l_1378 = ((((*l_1395) = (g_1201 , p_65)) == ((&p_66 != l_1253) > 0x0.0p-1)) < p_65)) > (**l_1106)) , &g_889))) >= l_1385), l_1396[0][4])))) || g_589[0].f3)), g_695.f5)) | (*l_1105)) , (*l_1358)) , 0xEDC8L) > l_1396[0][4]))))
                    { /* block id: 569 */
                        const uint8_t * const ***l_1399 = (void*)0;
                        const uint8_t * const ****l_1400[8][10][3] = {{{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{(void*)0,&l_1399,(void*)0},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399}},{{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{(void*)0,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399}},{{&l_1399,&l_1399,&l_1399},{(void*)0,&l_1399,(void*)0},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,(void*)0,&l_1399},{(void*)0,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399}},{{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{(void*)0,&l_1399,(void*)0},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399}},{{&l_1399,(void*)0,&l_1399},{(void*)0,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{(void*)0,&l_1399,(void*)0}},{{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,(void*)0,&l_1399},{(void*)0,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399}},{{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,(void*)0},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399}},{{&l_1399,&l_1399,&l_1399},{(void*)0,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,&l_1399,(void*)0},{&l_1399,&l_1399,&l_1399},{(void*)0,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,&l_1399,&l_1399},{&l_1399,(void*)0,&l_1399},{&l_1399,&l_1399,&l_1399}}};
                        int32_t l_1405 = 0x478E3EF1L;
                        int i, j, k;
                        (*g_1397) = &g_421[2];
                        l_1401[2] = l_1399;
                        (*l_1106) = &l_1385;
                        return l_1405;
                    }
                    else
                    { /* block id: 574 */
lbl_1416:
                        (*l_1358) &= 0x76B778F3L;
                    }
                    (*l_1106) = l_1406;
                    for (l_1123 = 0; (l_1123 >= 18); ++l_1123)
                    { /* block id: 580 */
                        int32_t *l_1409 = (void*)0;
                        int32_t *l_1410 = (void*)0;
                        int32_t *l_1411[5][7][6] = {{{&g_36,&g_15,&g_15,&g_172[4][1],&g_15,&g_15},{&g_36,&g_36,&g_172[4][1],&l_1220,&l_1215,&l_1233[0]},{(void*)0,&g_172[4][1],&g_36,&g_163,&g_163,&g_36},{&g_172[4][1],&g_172[4][1],(void*)0,(void*)0,&l_1215,&g_163},{&g_15,&g_36,&l_1220,(void*)0,&g_15,(void*)0},{&l_1220,&g_15,&l_1220,&l_1233[0],&g_172[4][1],&g_163},{&l_1213,&l_1233[0],(void*)0,&l_1385,&g_36,&g_36}},{{&l_1385,&g_36,&g_36,&l_1385,(void*)0,&l_1233[0]},{&l_1213,&g_163,&g_172[4][1],&l_1233[0],&l_1220,&g_15},{&l_1220,(void*)0,&g_15,(void*)0,&l_1220,&g_36},{&g_15,&g_163,&l_1215,(void*)0,(void*)0,&g_172[4][1]},{&g_172[4][1],&g_36,&g_163,&g_163,&g_36,&g_172[4][1]},{(void*)0,&l_1233[0],&l_1215,&l_1220,&g_172[4][1],&g_36},{&g_36,&g_15,&g_15,&g_172[4][1],&g_15,&g_15}},{{&g_36,&g_36,&g_172[4][1],&l_1220,&l_1215,&l_1233[0]},{(void*)0,&g_172[4][1],&g_36,&g_163,&g_163,&g_36},{&g_172[4][1],&g_172[4][1],(void*)0,(void*)0,&l_1215,&g_163},{&g_15,&g_36,&l_1220,(void*)0,&g_15,(void*)0},{&l_1220,&g_15,&l_1220,&l_1233[0],&g_172[4][1],&g_163},{&l_1213,&l_1233[0],(void*)0,&l_1385,&g_36,&g_36},{&l_1385,&g_36,&g_36,&l_1385,(void*)0,&l_1233[0]}},{{&l_1213,&g_163,&g_172[4][1],&l_1233[0],&l_1220,&g_15},{&l_1220,(void*)0,&g_15,(void*)0,&l_1220,&l_1215},{(void*)0,&g_36,&g_15,&l_1233[0],&g_15,(void*)0},{(void*)0,&l_1385,&g_36,&g_36,&l_1385,(void*)0},{&l_1233[0],&g_36,&g_15,&g_163,(void*)0,&l_1215},{&l_1385,(void*)0,&l_1213,(void*)0,&l_1213,(void*)0},{&l_1385,&l_1215,(void*)0,&g_163,&g_15,&g_36}},{{&l_1233[0],(void*)0,&l_1385,&g_36,&g_36,&l_1385},{(void*)0,(void*)0,&g_15,&l_1233[0],&g_15,&g_36},{(void*)0,&l_1215,&g_163,&g_15,&l_1213,&g_15},{&g_163,(void*)0,&g_163,&g_36,(void*)0,&g_36},{&l_1220,&g_36,&g_15,&g_172[4][1],&l_1385,&l_1385},{&g_172[4][1],&l_1385,&l_1385,&g_172[4][1],&g_15,&g_36},{&l_1220,&g_36,(void*)0,&g_36,&g_163,(void*)0}}};
                        int i, j, k;
                        ++l_1413;
                        if (g_695.f2)
                            goto lbl_1416;
                    }
                }
                else
                { /* block id: 584 */
                    uint16_t ***l_1421[9][2][10] = {{{&l_1253,&g_140,(void*)0,(void*)0,&g_140,(void*)0,(void*)0,&g_140,&l_1253,&g_140},{&l_1253,&l_1253,(void*)0,&g_140,&g_140,&l_1253,&g_140,&l_1253,&g_140,&l_1253}},{{&g_140,&l_1253,&l_1253,&g_140,&g_140,&g_140,&l_1253,&l_1253,&l_1253,(void*)0},{&g_140,&l_1253,&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140,(void*)0,&g_140}},{{&g_140,&g_140,&l_1253,&l_1253,&g_140,&l_1253,&l_1253,&g_140,(void*)0,&g_140},{&l_1253,&g_140,&g_140,&g_140,&g_140,(void*)0,(void*)0,&g_140,&l_1253,&g_140}},{{(void*)0,&g_140,&g_140,&g_140,&g_140,&g_140,(void*)0,&l_1253,(void*)0,&g_140},{&g_140,&l_1253,&g_140,&g_140,&g_140,(void*)0,&l_1253,&l_1253,&g_140,&l_1253}},{{&l_1253,(void*)0,(void*)0,&g_140,(void*)0,&l_1253,&g_140,&g_140,&g_140,&g_140},{(void*)0,&g_140,(void*)0,(void*)0,&g_140,(void*)0,&l_1253,&l_1253,(void*)0,(void*)0}},{{&g_140,&g_140,&g_140,&g_140,&g_140,&l_1253,&l_1253,&g_140,&l_1253,&l_1253},{&g_140,&g_140,&g_140,&l_1253,&l_1253,(void*)0,&l_1253,&g_140,&l_1253,&g_140}},{{&g_140,&g_140,&g_140,&l_1253,&g_140,&l_1253,&l_1253,&g_140,&l_1253,&l_1253},{&g_140,&g_140,&g_140,&l_1253,(void*)0,&g_140,&g_140,&g_140,&l_1253,&l_1253}},{{&g_140,&g_140,&g_140,&l_1253,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140},{&l_1253,(void*)0,(void*)0,&l_1253,&g_140,&l_1253,&l_1253,&g_140,&l_1253,&l_1253}},{{(void*)0,&l_1253,&g_140,(void*)0,&g_140,&l_1253,(void*)0,(void*)0,&l_1253,&g_140},{(void*)0,&l_1253,(void*)0,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140}}};
                    int32_t l_1444[7];
                    int32_t l_1447 = 0x9179CE40L;
                    uint64_t l_1459 = 0xE4D4E6B45F08E589LL;
                    float l_1466 = 0x3.48ADE2p-11;
                    uint32_t l_1467 = 0xB8587A82L;
                    const uint32_t **l_1469[7][7] = {{&l_1468,&l_1468,&l_1468,(void*)0,&l_1468,(void*)0,&l_1468},{&l_1468,&l_1468,(void*)0,&l_1468,&l_1468,(void*)0,&l_1468},{(void*)0,&l_1468,&l_1468,(void*)0,(void*)0,&l_1468,(void*)0},{&l_1468,&l_1468,&l_1468,&l_1468,&l_1468,&l_1468,&l_1468},{&l_1468,(void*)0,(void*)0,&l_1468,&l_1468,(void*)0,(void*)0},{&l_1468,&l_1468,&l_1468,&l_1468,&l_1468,&l_1468,&l_1468},{&l_1468,(void*)0,&l_1468,(void*)0,(void*)0,&l_1468,&l_1468}};
                    int32_t **l_1481 = &g_391[4][2][2];
                    uint64_t l_1484 = 0x9B57C95E60F649FCLL;
                    uint16_t l_1485 = 1UL;
                    int16_t l_1486 = 0x5151L;
                    int32_t *l_1487 = &g_172[5][1];
                    int32_t *l_1488 = &l_1378;
                    int32_t *l_1489[1][1];
                    float l_1490 = (-0x4.4p-1);
                    uint64_t l_1491 = 18446744073709551613UL;
                    int i, j, k;
                    for (i = 0; i < 7; i++)
                        l_1444[i] = (-10L);
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_1489[i][j] = &l_1455;
                    }
                    l_1417 = &g_918;
                    l_1396[0][5] &= (*l_1358);
                    if (g_149)
                        goto lbl_1463;
                    for (l_1413 = (-12); (l_1413 == 10); l_1413++)
                    { /* block id: 589 */
                        const uint32_t * const l_1428 = &g_487;
                        int32_t l_1442 = (-1L);
                        int32_t *l_1448 = &l_1220;
                        int32_t *l_1449 = &l_1213;
                        int32_t *l_1450 = &l_1233[0];
                        int32_t *l_1451[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1451[i] = (void*)0;
                        l_1447 |= (((((((*l_1107) = ((*l_1358) = (&l_1253 != l_1421[3][0][8]))) & (((((****l_1103)++) , (((((-1L) < (safe_sub_func_uint16_t_u_u((--(*p_66)), (l_1396[0][4] != g_418[4].f3)))) != ((void*)0 == l_1428)) == ((safe_add_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((safe_sub_func_uint16_t_u_u((((safe_sub_func_uint64_t_u_u(((l_1378 = ((safe_div_func_uint8_t_u_u((((*l_1184) = ((((((g_1201.f3 <= (safe_mul_func_int16_t_s_s(1L, 0xAAD0L))) | (***g_1100)) && 0x4306FCC7L) != l_1413) , g_15) == 0x526951242AAA8307LL)) < l_1441), 0x84L)) != l_1378)) == l_1442), 0x612BA09BAECC966FLL)) <= 6UL) != l_1413), 0xF667L)), l_1443)), 0xA442L)) || 1UL)) , l_1444[3])) , l_1445[1]) > l_1446)) | l_1442) > g_13) >= l_1442) <= 4L);
                        if (l_1444[3])
                            continue;
                        l_1459++;
                    }
                    if (((l_1462[2] = l_1462[2]) != &l_1401[2]))
                    { /* block id: 601 */
lbl_1463:
                        (*l_1095) = &l_1444[1];
                        l_1486 ^= ((l_1219 = ((*l_1358) = (safe_add_func_uint32_t_u_u(((*l_1381) = (((((*l_1184) |= (*l_1358)) == l_1467) < ((&l_1441 != (g_1470 = l_1468)) != (~(safe_mod_func_int16_t_s_s(((((l_1220 &= (((safe_div_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(((safe_rshift_func_int8_t_s_u(((l_1444[5] && l_1459) > ((void*)0 == l_1481)), 1)) ^ ((safe_mod_func_int16_t_s_s((g_106[0] = (l_1484 < (*l_1358))), g_134)) != 0x32C8L)), (*g_138))), (**g_140))) == l_1223) || g_769.f3)) == (*l_1107)) != l_1346) , 0L), 0x202BL))))) || g_141.f2)), 0x3B62541AL)))) & l_1485);
                        ++l_1491;
                    }
                    else
                    { /* block id: 613 */
                        return l_1413;
                    }
                }
                l_1495 = l_1494;
            }
            else
            { /* block id: 618 */
                float *l_1504 = &g_171;
                int32_t l_1511 = 0x8F891470L;
                float *l_1512 = &g_48;
                (*l_1512) = (safe_div_func_float_f_f((safe_add_func_float_f_f((safe_sub_func_float_f_f(((*l_1504) = (((*l_1356) && (&l_1250 == l_1503)) , (*g_791))), (safe_sub_func_float_f_f((safe_mul_func_float_f_f(((void*)0 == &g_391[1][2][2]), (safe_div_func_float_f_f(((0x1.0p+1 >= ((*g_555) == ((*g_140) = p_66))) != g_502), 0xF.084071p-66)))), 0xE.C83AB8p-34)))), p_65)), l_1511));
            }
            return l_1344;
        }
    }
    else
    { /* block id: 625 */
        uint32_t l_1517 = 0UL;
        uint16_t l_1531 = 0xE99BL;
        int32_t l_1532 = 0xE9E64CE2L;
        int64_t *l_1552 = &g_699[1][2][1];
        int64_t **l_1551 = &l_1552;
        l_1417 = l_1417;
        for (g_108 = (-25); (g_108 == (-30)); g_108--)
        { /* block id: 629 */
            uint32_t l_1521 = 6UL;
            int32_t l_1539[8] = {0xE9A94B46L,0xE9A94B46L,0xE9A94B46L,0xE9A94B46L,0xE9A94B46L,0xE9A94B46L,0xE9A94B46L,0xE9A94B46L};
            int i;
            for (l_1216 = (-18); (l_1216 >= 0); l_1216++)
            { /* block id: 632 */
                int32_t *l_1520[7][4] = {{(void*)0,(void*)0,&g_163,&l_1215},{&l_1226,&l_1222,(void*)0,&l_1226},{(void*)0,(void*)0,&g_163,(void*)0},{&g_13,(void*)0,&g_13,&l_1226},{(void*)0,&l_1222,&l_1232[3][1][5],&l_1215},{&l_1215,(void*)0,(void*)0,&l_1222},{&l_1232[0][0][1],(void*)0,(void*)0,&l_1232[0][0][1]}};
                int64_t *l_1536 = (void*)0;
                int64_t *l_1537 = &g_699[0][2][0];
                int i, j;
                --l_1517;
                (*l_1095) = l_1520[5][1];
                if ((((l_1521 && (safe_mod_func_uint8_t_u_u((~l_1517), (safe_mod_func_uint8_t_u_u((((g_17 & (18446744073709551615UL | l_1521)) != (safe_add_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((0xD6F3L == (((l_1532 &= l_1531) && (safe_mod_func_int8_t_s_s((((((((!((*l_1537) &= g_501)) , 18446744073709551615UL) && g_306) || 0xE562L) , 1UL) , l_1521) & l_1531), l_1521))) ^ l_1517)), 1UL)), (-1L)))) , 0x06L), l_1531))))) >= l_1521) ^ 253UL))
                { /* block id: 637 */
                    l_1222 ^= (*g_162);
                }
                else
                { /* block id: 639 */
                    uint16_t l_1538 = 0UL;
                    l_1532 ^= (*g_162);
                    (*g_162) = l_1521;
                    l_1538 = (*l_1406);
                    l_1539[7] ^= (*g_162);
                }
                (*l_1095) = &l_1539[0];
            }
        }
        (*g_1560) = (l_1531 == (safe_lshift_func_uint16_t_u_s((safe_mod_func_int16_t_s_s((l_1517 & ((((0UL == (safe_unary_minus_func_uint32_t_u(((safe_mod_func_uint8_t_u_u((*g_1102), l_1531)) == (safe_add_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u((((*l_1107) = (((*l_1551) = (void*)0) == &l_1214)) && (!(*l_1356))), 14)), ((safe_lshift_func_int16_t_s_s((+(safe_rshift_func_int16_t_s_s((&g_1100 == (void*)0), 5))), 1)) && l_1517))))))) != l_1517) >= 0x8032AE3A92F8DA2BLL) , l_1517)), l_1531)), l_1531)));
        (*l_1406) ^= (-10L);
    }
    (*g_1583) = ((*g_1470) | (g_769.f3 = ((*l_1406) && ((safe_add_func_uint16_t_u_u((((*l_1107) = ((safe_rshift_func_uint16_t_u_s((((safe_lshift_func_int16_t_s_u((*l_1107), 8)) == ((*l_1406) >= ((safe_rshift_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_u((((safe_div_func_int32_t_s_s(l_1573[4][4][0], 0xD72F141EL)) | (((((*l_1581) = ((*l_1107) <= ((safe_add_func_uint8_t_u_u((safe_add_func_int64_t_s_s((safe_lshift_func_int16_t_s_s((0x19B46895L == (*l_1406)), 5)), g_779.f6)), g_1580[0])) && 0L))) , 0xEED62F0BL) >= (*g_1470)) , (*g_1470))) <= (*l_1107)), 4)) , (*l_1356)), 0)) , (*l_1107)))) && (*p_66)), g_699[0][2][0])) && (*l_1356))) ^ (*g_1470)), 0x4550L)) > 2UL))));
    return (*l_1107);
}


/* ------------------------------------------ */
/* 
 * reads : g_73
 * writes: g_73 g_391
 */
static uint16_t * func_68(int32_t * p_69, uint32_t  p_70)
{ /* block id: 28 */
    int32_t *l_72 = &g_73;
    const uint16_t *l_110 = &g_111;
    int32_t **l_1078 = &g_391[0][0][5];
    uint16_t *l_1079 = (void*)0;
    (*l_72) = 0xE7C4BD4AL;
    for (g_73 = 0; (g_73 <= (-26)); g_73 = safe_sub_func_uint64_t_u_u(g_73, 6))
    { /* block id: 32 */
        int32_t *l_103 = &g_33[8][2];
        int16_t *l_104 = (void*)0;
        int16_t *l_105 = &g_106[1];
        int8_t *l_107 = &g_108;
        float *l_109 = &g_48;
        int16_t *l_1010 = (void*)0;
        int16_t *l_1011 = &g_370[0][0];
        const uint16_t * const ****l_1014 = (void*)0;
        uint32_t *l_1025 = &g_656[2];
        int8_t l_1059 = 0x84L;
        int32_t ***l_1072 = &g_948;
    }
    (*l_1078) = l_72;
    return l_1079;
}


/* ------------------------------------------ */
/* 
 * reads : g_14 g_15 g_306 g_2 g_149 g_108 g_172 g_333 g_134 g_36 g_141.f5 g_138 g_177 g_309 g_321 g_137 g_140 g_141.f4 g_33 g_13 g_268.f2 g_370 g_136 g_116 g_12 g_139 g_163 g_418 g_421 g_136.f0 g_111 g_141.f6 g_106 g_511 g_73 g_542 g_487 g_554 g_577 g_580 g_589 g_18 g_502 g_656 g_695 g_699 g_556 g_171 g_743 g_3 g_784 g_791 g_268.f6 g_769.f3 g_141.f2 g_779.f5 g_769.f1 g_650 g_744 g_745 g_555 g_889 g_918 g_19 g_996 g_1006
 * writes: g_177 g_306 g_309 g_149 g_108 g_332 g_172 g_134 g_106 g_370 g_391 g_163 g_138 g_487 g_501 g_502 g_555 g_577 g_171 g_656 g_699 g_48 g_948
 */
static int16_t  func_84(int32_t  p_85, uint32_t  p_86)
{ /* block id: 100 */
    uint32_t *l_281[6];
    int32_t l_282 = 1L;
    int32_t l_285 = 0xA32A2159L;
    uint64_t *l_304 = (void*)0;
    uint64_t *l_305 = &g_306;
    int32_t *l_308 = (void*)0;
    int32_t ** const l_307 = &l_308;
    uint8_t *l_310 = &g_149;
    int8_t *l_311[3][6] = {{&g_108,&g_108,&g_108,&g_108,&g_108,&g_108},{&g_108,&g_108,&g_108,&g_108,&g_108,&g_108},{&g_108,&g_108,&g_108,&g_108,&g_108,&g_108}};
    const int32_t *l_326 = (void*)0;
    int16_t l_458 = 0x580EL;
    int8_t l_533 = 0x39L;
    uint16_t *** const l_622 = (void*)0;
    int32_t l_647 = 0L;
    int32_t l_648 = (-1L);
    int32_t l_649 = 0xCB3C9EB3L;
    int32_t l_654 = 0xF240E94BL;
    int32_t l_655 = (-1L);
    int16_t l_661 = 1L;
    const uint32_t l_761 = 0x18BA88CFL;
    uint16_t l_828 = 0xBDE0L;
    union U1 *l_893 = &g_890;
    int64_t l_930 = 0x0A86E23D829BF2B6LL;
    int64_t l_969 = 8L;
    uint64_t l_997 = 18446744073709551613UL;
    int i, j;
    for (i = 0; i < 6; i++)
        l_281[i] = (void*)0;
    if (((l_282 = 0x76370667L) ^ (safe_sub_func_int8_t_s_s((g_108 &= (((*l_310) ^= (l_285 && (p_86 ^ ((g_309 = (safe_div_func_uint64_t_u_u(((((safe_lshift_func_uint8_t_u_s(((g_14 != (((safe_lshift_func_int16_t_s_u((safe_sub_func_uint32_t_u_u((safe_add_func_uint32_t_u_u((&g_138 != &g_138), (g_177 = (safe_rshift_func_uint8_t_u_s(p_85, 1))))), (safe_add_func_uint32_t_u_u(1UL, ((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u((((*l_305) &= ((l_285 , g_15) ^ 0L)) & l_285), 0x82A3L)), 3)) == 0xAC75BEA6F736BA97LL))))), 8)) , l_285) && 0xC1571923A1A75236LL)) || g_2), l_285)) , l_307) != &l_308) <= l_285), p_86))) && l_285)))) && 0x1CL)), p_86))))
    { /* block id: 107 */
        uint64_t l_314 = 0xAA3F7372CE9E399FLL;
        const int32_t *l_324 = &g_36;
        uint64_t **l_331[9][4] = {{&l_305,&l_305,(void*)0,&l_305},{&l_305,&l_304,(void*)0,&l_304},{&l_305,&l_304,&l_304,(void*)0},{&l_304,&l_304,&l_304,&l_304},{&l_304,&l_304,&l_305,&l_305},{&l_304,&l_305,&l_304,&l_304},{&l_304,&l_304,(void*)0,(void*)0},{&l_305,&l_305,&l_305,&l_304},{&l_304,&l_304,&l_305,&l_304}};
        int32_t l_375 = 1L;
        uint32_t l_392 = 4294967295UL;
        uint8_t l_506 = 249UL;
        int16_t *l_532[8][4] = {{&g_370[0][0],&l_458,&g_106[1],(void*)0},{&g_370[3][0],&l_458,&l_458,&g_370[3][0]},{&l_458,&g_370[3][0],&g_370[0][0],&g_370[3][0]},{&l_458,&g_370[0][0],&l_458,&g_106[1]},{&g_370[3][0],&g_370[3][0],&g_106[1],&g_106[1]},{&g_370[0][0],&g_370[0][0],(void*)0,&g_370[3][0]},{&g_370[3][0],&g_370[3][0],(void*)0,&g_370[3][0]},{&g_370[0][0],&l_458,&g_106[1],(void*)0}};
        uint32_t l_563 = 0x5EFC87DFL;
        int16_t l_564 = (-1L);
        int32_t *l_604 = &g_502;
        int16_t l_607 = (-5L);
        int8_t * const l_615[10] = {&g_108,&l_533,&l_533,&g_108,&g_108,&g_108,&l_533,&l_533,&g_108,&g_108};
        int32_t *l_616 = &l_285;
        int i, j;
lbl_459:
        for (l_285 = 0; (l_285 > 6); l_285++)
        { /* block id: 110 */
            const int32_t l_317 = 0L;
            uint16_t **l_319 = (void*)0;
            uint16_t ** const *l_323 = &g_140;
            int32_t l_339 = 0x41EC3F80L;
            float l_376 = 0x7.Ap-1;
            float l_442 = (-0x1.Dp-1);
            int32_t l_443 = (-1L);
            if (l_314)
                break;
            for (p_85 = 0; (p_85 <= 1); p_85 += 1)
            { /* block id: 114 */
                uint64_t **l_318 = &l_304;
                int i, j;
                if (((0L <= g_172[(p_85 + 1)][p_85]) || ((((safe_lshift_func_uint16_t_u_u(l_317, ((l_317 , (p_85 , &g_309)) != ((*l_318) = &g_306)))) ^ ((void*)0 != l_319)) || p_86) == 7L)))
                { /* block id: 116 */
                    const int32_t **l_325[8];
                    int i;
                    for (i = 0; i < 8; i++)
                        l_325[i] = &l_324;
                    for (g_309 = 0; (g_309 <= 1); g_309 += 1)
                    { /* block id: 119 */
                        uint16_t ** const **l_322 = (void*)0;
                        l_323 = &g_137[1];
                        if (p_86)
                            continue;
                    }
                    for (g_108 = 1; (g_108 >= 0); g_108 -= 1)
                    { /* block id: 125 */
                        return l_317;
                    }
                    l_326 = l_324;
                }
                else
                { /* block id: 129 */
                    uint64_t *l_334 = &g_309;
                    int32_t l_354[7];
                    int16_t *l_368 = &g_106[1];
                    int16_t *l_369 = &g_370[0][0];
                    int16_t l_393 = 0x8EB9L;
                    int32_t *l_394 = &g_163;
                    int32_t *l_395 = &l_339;
                    int32_t *l_396 = (void*)0;
                    int32_t *l_397 = &g_172[2][1];
                    int32_t *l_398 = &g_172[4][1];
                    int32_t *l_399 = &l_282;
                    int32_t *l_400 = &l_282;
                    int32_t *l_401[1][9] = {{&l_354[3],&l_354[3],&l_354[3],&l_354[3],&l_354[3],&l_354[3],&l_354[3],&l_354[3],&l_354[3]}};
                    uint32_t l_402 = 0x9C8A9460L;
                    int i, j;
                    for (i = 0; i < 7; i++)
                        l_354[i] = 0xD9411513L;
                    if (((((safe_lshift_func_uint16_t_u_u((((safe_add_func_int64_t_s_s(1L, ((g_332 = l_331[3][1]) == g_333))) != (((g_134 , l_334) != (void*)0) | (g_172[(p_85 + 1)][p_85] = p_86))) && (l_339 = ((*g_138) = ((safe_mod_func_int16_t_s_s((-1L), (safe_add_func_int64_t_s_s((((18446744073709551606UL && g_36) == 4294967295UL) , g_141.f5), p_85)))) || (-8L))))), p_86)) >= p_86) ^ (*l_324)) , 1L))
                    { /* block id: 134 */
                        const int8_t l_349 = 0x66L;
                        float *l_355[2];
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_355[i] = &g_48;
                        l_339 = ((+(g_108 = ((safe_div_func_int16_t_s_s((((safe_rshift_func_uint8_t_u_s(((*l_324) != (p_86 , (l_317 || (safe_lshift_func_uint8_t_u_s((safe_rshift_func_uint16_t_u_u(((***g_321) = (((l_349 , (&l_349 != &g_108)) , g_177) != (safe_mul_func_int32_t_s_s(l_349, (((((safe_mul_func_int8_t_s_s(p_85, g_309)) || 0x297BL) , l_354[3]) , 0x56L) < g_134))))), 8)), l_339))))), 0)) >= p_86) | 1L), 5UL)) == l_349))) , g_149);
                        g_172[(p_85 + 7)][p_85] &= (safe_add_func_int16_t_s_s(p_86, l_317));
                    }
                    else
                    { /* block id: 139 */
                        return g_172[(p_85 + 1)][p_85];
                    }
                    if ((safe_sub_func_uint16_t_u_u(((**g_140) = l_317), (safe_add_func_uint8_t_u_u((p_86 != (0x9DL == (l_339 = 0L))), (g_108 = (safe_add_func_int16_t_s_s((((((*l_334) = (((safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_u(((*l_369) = ((*l_368) = (-1L))), (safe_lshift_func_uint16_t_u_u((l_375 = (g_141.f4 == ((((safe_sub_func_uint16_t_u_u(l_317, (l_317 | (g_33[6][1] != p_86)))) | p_85) || p_85) ^ p_85))), p_86)))), 1)) & g_172[(p_85 + 1)][p_85]) <= p_86)) ^ 0xB079A7C3D983DDCALL) <= g_172[(p_85 + 1)][p_85]) , g_13), p_86))))))))
                    { /* block id: 149 */
                        uint16_t *l_390 = (void*)0;
                        (*l_307) = (void*)0;
                        g_172[5][0] |= (((((safe_div_func_int32_t_s_s(l_354[3], (((((!(g_268[4][1].f2 < ((safe_sub_func_float_f_f((((safe_mul_func_float_f_f((safe_sub_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f(g_370[2][0], ((l_390 == (**g_321)) != ((((g_36 > g_108) < (((*l_307) = (void*)0) != (g_391[4][2][2] = &g_3))) != p_86) != p_86)))), p_85)), p_85)), g_306)) > g_134) > l_392), 0x5.85E572p-16)) <= p_86))) , g_136) , g_116) , g_136) , 0xC450B530L))) != 0x5A2EL) ^ 0x0FD4L) , (void*)0) != l_334);
                    }
                    else
                    { /* block id: 154 */
                        return g_12;
                    }
                    --l_402;
                    if (p_86)
                        break;
                }
                return (*l_324);
            }
            if (g_134)
                goto lbl_459;
            if (l_339)
                break;
            for (g_108 = 13; (g_108 >= 19); g_108++)
            { /* block id: 165 */
                int64_t l_411 = 0L;
                uint64_t * const l_415 = &g_309;
                int32_t *l_416 = &g_172[6][1];
                int32_t *l_417 = &g_163;
                uint64_t *l_422 = &l_314;
                int8_t *l_434 = &g_108;
                const int64_t l_441 = 0x34960F4A7E1435C6LL;
                int64_t *l_444 = &l_411;
                (*l_417) ^= ((*l_416) = ((safe_unary_minus_func_int64_t_s(1L)) == ((((l_339 , ((+((safe_sub_func_uint64_t_u_u(((*l_305) = l_411), (~(***g_139)))) <= (safe_rshift_func_int8_t_s_u((p_85 , ((l_305 == l_415) , (l_411 <= 65535UL))), 4)))) ^ 0x5DL)) >= 0x5BCD7E6F2683E85DLL) == (*l_324)) >= l_411)));
                (*l_417) |= (((p_86 & (g_418[4] , (safe_div_func_int32_t_s_s((g_421[2] , ((l_422 == (void*)0) == (~((*l_444) = (safe_add_func_uint64_t_u_u((((safe_add_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(((safe_mul_func_int8_t_s_s(((safe_rshift_func_uint16_t_u_u((((void*)0 != l_434) >= ((0xDE09L != (safe_add_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_u(l_441, p_85)), 11)) , p_86), g_141.f5))) < (*l_416))), 10)) & 3UL), 255UL)) , p_86), l_317)), l_443)) , p_86) | (-8L)), p_86)))))), p_85)))) || p_85) == g_116);
                (*l_307) = ((0xF.227866p-25 != ((((p_86 ^ g_136.f0) > ((safe_div_func_uint16_t_u_u((**g_140), (*g_138))) == ((safe_unary_minus_func_int64_t_s(((safe_mul_func_int8_t_s_s((safe_rshift_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((safe_sub_func_int64_t_s_s(l_339, (&l_305 != &l_422))), ((((*l_305) = p_85) | p_85) || (*l_324)))), p_86)), 0xD8L)) ^ 0x8FFE5B91L))) <= l_458))) , p_85) > g_370[0][0])) , &l_443);
            }
        }
        for (g_177 = 0; (g_177 >= 23); g_177 = safe_add_func_uint32_t_u_u(g_177, 3))
        { /* block id: 178 */
            int64_t l_472 = 1L;
            int32_t *l_479 = &l_375;
            (*l_479) ^= (safe_rshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_u(g_111, 5)), (safe_mod_func_uint16_t_u_u(((0x1414223D1CBF5D3BLL & ((safe_mul_func_float_f_f((((0xA3FC121777A6A502LL == (-1L)) == ((g_309 = ((*l_324) <= ((l_472 >= (p_86 <= p_86)) < ((safe_add_func_uint64_t_u_u((++(*l_305)), ((safe_add_func_int8_t_s_s((((*g_140) = (void*)0) == (void*)0), 1L)) != l_472))) < p_85)))) != 0L)) , l_472), p_85)) , 1L)) && p_86), (*l_324)))));
        }
        for (g_108 = (-9); (g_108 == 8); ++g_108)
        { /* block id: 186 */
            int64_t l_493 = 1L;
            int32_t l_505 = 0x91593D3EL;
            const uint16_t * const *l_541 = (void*)0;
            float l_553 = 0xF.C44F93p-1;
            int32_t **l_605 = &l_604;
            int32_t *l_606 = &g_172[1][0];
            int32_t *l_608 = &l_282;
            int8_t **l_611 = &l_311[2][4];
            const int64_t l_614 = 0x845F0961D629665ALL;
            for (g_149 = 0; (g_149 <= 28); g_149++)
            { /* block id: 189 */
                int32_t **l_490 = &g_391[0][2][4];
                int32_t *l_500 = &g_501;
                int32_t l_565[9] = {0x656C4C73L,0x656C4C73L,(-1L),0x656C4C73L,0x656C4C73L,(-1L),0x656C4C73L,0x656C4C73L,(-1L)};
                int i;
                for (p_86 = 1; (p_86 <= 5); p_86 += 1)
                { /* block id: 192 */
                    int32_t *l_484 = &g_172[4][1];
                    int i;
                    l_484 = l_281[p_86];
                    return p_85;
                }
                if (p_86)
                    break;
                if ((((g_487 = p_86) != ((safe_add_func_uint8_t_u_u(((*l_324) < (((g_12 , (((((*l_490) = &p_85) != &g_33[5][6]) | (safe_div_func_uint16_t_u_u((l_493 | (g_502 = (((((safe_rshift_func_int16_t_s_u(((safe_lshift_func_uint8_t_u_s(g_141.f6, 6)) > ((((*l_500) = (safe_mod_func_uint32_t_u_u(g_33[8][2], ((((65526UL != g_33[8][2]) & 0xA9L) & g_106[1]) && p_85)))) , 65535UL) && g_370[0][0])), p_86)) > (-3L)) || 2UL) , 0UL) || p_86))), 1L))) != p_85)) > l_493) || 0UL)), p_85)) <= l_493)) >= p_86))
                { /* block id: 201 */
                    int32_t *l_503 = &l_285;
                    int32_t *l_504[9][10][2] = {{{&g_15,&g_36},{&g_172[4][1],&g_15},{(void*)0,&g_163},{&g_15,&l_285},{&g_172[4][1],&g_19},{&l_285,&g_33[8][8]},{&g_33[8][2],&g_15},{&g_73,(void*)0},{&g_163,&g_73},{(void*)0,&g_36}},{{&g_3,&g_33[4][4]},{&g_33[8][2],&g_73},{&g_15,&g_15},{&g_73,&l_285},{&g_73,&g_33[8][8]},{&g_163,&g_33[4][4]},{&g_172[4][1],&g_163},{&g_163,&g_163},{&g_163,&g_163},{&g_172[4][1],&g_33[4][4]}},{{&g_163,&g_33[8][8]},{&g_73,&l_285},{&g_73,&g_15},{&g_15,&g_73},{&g_33[8][2],&g_33[4][4]},{&g_3,&g_36},{(void*)0,&g_73},{&g_163,(void*)0},{&g_73,&g_15},{&g_33[8][2],&g_33[8][8]}},{{&l_285,&g_19},{&g_172[4][1],&l_285},{&g_15,&g_163},{(void*)0,&g_15},{&g_172[4][1],&g_36},{&g_15,&g_33[8][8]},{(void*)0,&g_163},{&g_73,&g_163},{(void*)0,&g_73},{&g_73,&g_19}},{{&g_3,&g_19},{&g_73,&g_73},{(void*)0,&g_163},{&g_73,&g_163},{(void*)0,&g_33[8][8]},{&g_15,&g_36},{&g_172[4][1],&g_15},{(void*)0,&g_163},{&g_15,&l_285},{&g_172[4][1],&g_19}},{{&l_285,&g_33[8][8]},{&g_33[8][2],&g_15},{&g_73,(void*)0},{&g_163,&g_73},{(void*)0,&g_36},{&g_3,&g_33[4][4]},{&g_33[8][2],&g_73},{&g_15,&g_15},{&g_73,&l_285},{&g_73,&g_33[8][8]}},{{&g_19,&l_285},{&g_172[4][1],&g_19},{(void*)0,&g_33[8][2]},{(void*)0,&g_19},{&g_172[4][1],&l_285},{&g_19,&g_163},{(void*)0,&g_172[4][1]},{&g_36,&g_3},{&g_3,&g_33[0][0]},{&g_15,&l_285}},{{&g_15,&g_163},{&g_163,&g_33[0][0]},{(void*)0,&g_73},{&g_36,&g_73},{&g_15,&g_163},{&g_172[4][1],&g_73},{&g_172[4][1],&g_172[4][1]},{&g_3,&g_33[8][2]},{&g_73,&g_73},{&g_172[4][1],&g_163}},{{&g_73,&g_163},{&g_163,&g_19},{&g_36,(void*)0},{&g_73,&g_33[0][0]},{(void*)0,&g_73},{&g_15,&g_73},{(void*)0,&g_33[0][0]},{&g_73,(void*)0},{&g_36,&g_19},{&g_163,&g_163}}};
                    int i, j, k;
                    (*l_307) = &g_73;
                    l_506--;
                    for (l_285 = 0; (l_285 < 28); ++l_285)
                    { /* block id: 206 */
                        (*l_490) = &l_505;
                        if (p_86)
                            break;
                        (*l_307) = (*l_307);
                    }
                    (*l_503) &= (g_511 , ((safe_div_func_int32_t_s_s((!((safe_mod_func_uint8_t_u_u(g_418[4].f4, (g_309 & ((((safe_lshift_func_int8_t_s_u(p_85, g_33[9][6])) != (safe_add_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u(((safe_div_func_uint64_t_u_u(((~((safe_sub_func_int16_t_s_s((safe_div_func_int64_t_s_s((safe_rshift_func_uint16_t_u_s((g_73 || ((void*)0 == l_532[1][0])), (**l_307))), p_86)), p_85)) & p_86)) , p_86), l_493)) , 6UL), (-4L))), 0x6D4718C7L))) & p_86) <= 6UL)))) , l_533)), g_511.f3)) < p_86));
                }
                else
                { /* block id: 212 */
                    int64_t l_543 = 0xD55BE729135A29DELL;
                    int32_t l_549 = 0xFC2E89DFL;
                    uint32_t *l_550 = &g_487;
                    int16_t *l_570[1][5][8] = {{{&g_106[2],&g_370[0][0],(void*)0,(void*)0,&g_370[0][0],&g_106[2],(void*)0,&g_106[2]},{&g_370[0][0],&g_106[2],(void*)0,&g_106[2],&g_370[0][0],(void*)0,(void*)0,&g_370[0][0]},{&g_106[2],&l_564,&l_564,&g_106[2],&l_458,&g_370[0][0],&l_458,&g_106[2]},{&l_564,&l_458,&l_564,(void*)0,(void*)0,(void*)0,(void*)0,&l_564},{&l_458,&l_458,(void*)0,&g_370[0][0],&g_370[0][0],&g_370[0][0],(void*)0,&l_458}}};
                    int i, j, k;
                    (*g_554) = ((safe_lshift_func_uint16_t_u_s((safe_mod_func_uint32_t_u_u(((safe_lshift_func_uint16_t_u_u(p_86, (safe_unary_minus_func_int64_t_s(((p_85 == (p_85 >= (l_543 = (l_541 == (g_542 , (*g_139)))))) >= ((safe_mul_func_uint16_t_u_u(p_86, (((safe_rshift_func_uint8_t_u_u(((!p_85) && ((((*l_550)++) , l_493) , p_86)), 3)) == p_86) , 0x7CECL))) < 1UL)))))) && p_85), 0xDDCABFB5L)), 3)) , l_541);
                    for (g_306 = 0; (g_306 >= 37); ++g_306)
                    { /* block id: 218 */
                        uint16_t *l_566 = &g_134;
                        int32_t l_573 = (-8L);
                        uint16_t *l_576 = &g_577;
                        l_565[5] ^= (safe_mul_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s((g_542.f3 == (l_563 && p_86)), (l_564 , ((((++(*l_566)) <= (((+((((l_570[0][3][3] == &g_370[0][0]) | 0x81651E58L) <= ((*l_576) &= (safe_sub_func_int8_t_s_s(((l_573 >= (safe_div_func_uint64_t_u_u(p_85, p_85))) != 5L), l_493)))) > l_543)) > p_86) == p_86)) != 7L) , g_511.f2)))), p_86));
                    }
                    for (g_163 = 0; (g_163 != 0); g_163 = safe_add_func_int64_t_s_s(g_163, 2))
                    { /* block id: 225 */
                        (*g_580) = p_85;
                        if (p_85)
                            break;
                    }
                }
            }
            (*l_608) ^= (safe_div_func_uint8_t_u_u((safe_unary_minus_func_int64_t_s((safe_lshift_func_int16_t_s_s((+((((((safe_rshift_func_int16_t_s_s((g_589[0] , g_18), ((safe_mod_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u(((-9L) > (safe_add_func_uint64_t_u_u((safe_sub_func_int32_t_s_s((p_85 | (safe_lshift_func_uint16_t_u_u(0x134BL, p_85))), (l_607 &= (safe_mod_func_int32_t_s_s(((*l_606) ^= (safe_sub_func_int16_t_s_s(0x6391L, (((*l_605) = l_604) != l_324)))), (*l_324)))))), p_85))), (*l_324))) <= g_163), p_85)) == (*l_324)))) <= p_86) < p_85) | p_85) | p_86) >= (*l_324))), 0)))), g_106[1]));
            (*g_580) = (((((g_589[0].f3 & (safe_mod_func_int32_t_s_s(0xEB99085DL, ((l_611 == &g_115[1]) | p_85)))) >= ((safe_add_func_int16_t_s_s(0xBC54L, (*l_324))) == l_614)) , (*g_321)) == (void*)0) , (-0x5.Bp-1));
        }
        (*l_616) |= ((void*)0 == l_615[8]);
    }
    else
    { /* block id: 238 */
        int32_t **l_621[9];
        int32_t l_636 = 0xBDA8C91DL;
        uint16_t l_700 = 0xFB41L;
        uint16_t ** const *l_748[2];
        uint8_t l_790[2][10][2] = {{{255UL,255UL},{0x1CL,255UL},{0x45L,0x45L},{0x45L,255UL},{0x1CL,255UL},{255UL,255UL},{0x1CL,255UL},{0x45L,0x45L},{0x45L,255UL},{0x1CL,255UL}},{{255UL,255UL},{0x1CL,255UL},{0x45L,0x45L},{0x45L,255UL},{0x1CL,255UL},{255UL,255UL},{0x1CL,255UL},{0x45L,0x45L},{0x45L,255UL},{0x1CL,255UL}}};
        int32_t l_803 = (-1L);
        int8_t *l_868 = &l_533;
        uint32_t l_900 = 0x0E7E3CFFL;
        volatile int32_t *l_1003 = &g_2;
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_621[i] = (void*)0;
        for (i = 0; i < 2; i++)
            l_748[i] = &g_137[4];
lbl_999:
        if (((safe_div_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((*l_310) |= ((&l_326 == l_621[5]) | (l_622 != (((***g_139) <= p_85) , l_622)))), 6)), ((((safe_unary_minus_func_uint64_t_u(0x321581F993387B31LL)) > ((safe_add_func_int32_t_s_s(p_86, (((*l_305) = ((g_542.f3 && (*g_138)) <= 0x1DL)) >= g_502))) | g_13)) == (***g_321)) , g_106[1]))) <= p_86))
        { /* block id: 241 */
            int32_t *l_626 = &g_33[5][5];
            int32_t l_643 = 0x346788B6L;
            int32_t l_646 = 0x74AD7A4BL;
            int32_t l_651 = 0x246566BAL;
            int32_t l_666 = 7L;
            int32_t l_667 = 0x87FF1570L;
            uint32_t l_668 = 1UL;
            uint16_t ***l_742[6][3][9] = {{{&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,(void*)0},{&g_140,(void*)0,(void*)0,(void*)0,&g_140,(void*)0,&g_140,&g_140,(void*)0},{&g_140,&g_140,&g_140,(void*)0,(void*)0,&g_140,(void*)0,(void*)0,&g_140}},{{(void*)0,(void*)0,&g_140,&g_140,&g_140,(void*)0,(void*)0,&g_140,&g_140},{(void*)0,&g_140,(void*)0,&g_140,&g_140,(void*)0,&g_140,(void*)0,&g_140},{&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,(void*)0,&g_140,(void*)0}},{{&g_140,&g_140,&g_140,(void*)0,(void*)0,(void*)0,(void*)0,&g_140,&g_140},{(void*)0,&g_140,&g_140,&g_140,(void*)0,(void*)0,(void*)0,&g_140,&g_140},{(void*)0,&g_140,&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140,&g_140}},{{&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,(void*)0},{(void*)0,(void*)0,&g_140,&g_140,&g_140,(void*)0,&g_140,&g_140,&g_140},{&g_140,(void*)0,&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140,(void*)0}},{{&g_140,&g_140,(void*)0,(void*)0,&g_140,&g_140,&g_140,&g_140,(void*)0},{&g_140,(void*)0,&g_140,(void*)0,(void*)0,&g_140,&g_140,&g_140,&g_140},{&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140,(void*)0,&g_140,&g_140}},{{&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140},{&g_140,(void*)0,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140},{&g_140,&g_140,&g_140,&g_140,&g_140,(void*)0,&g_140,&g_140,&g_140}}};
            uint16_t ****l_741[9][3] = {{&l_742[5][1][6],&l_742[0][0][1],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[3][1][8],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[0][0][1],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[3][1][8],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[0][0][1],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[3][1][8],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[0][0][1],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[3][1][8],&l_742[5][1][6]},{&l_742[5][1][6],&l_742[0][0][1],&l_742[5][1][6]}};
            uint16_t *****l_740 = &l_741[1][0];
            int i, j, k;
            p_85 &= ((void*)0 == &g_116);
            g_391[3][3][6] = l_626;
            for (g_163 = 0; (g_163 <= 2); g_163 += 1)
            { /* block id: 246 */
                uint8_t *l_631 = &g_149;
                uint32_t * const l_633 = &g_487;
                int32_t l_644 = 0x206229DFL;
                int32_t l_645[8] = {0x84FD29E6L,0x4E69548AL,0x84FD29E6L,0x84FD29E6L,0x4E69548AL,0x84FD29E6L,0x84FD29E6L,0x4E69548AL};
                int64_t *l_719 = &g_699[2][0][0];
                int16_t *l_723 = (void*)0;
                uint16_t ***l_747 = &g_140;
                int32_t *l_768 = &l_645[2];
                uint32_t l_774[10][6] = {{9UL,1UL,1UL,9UL,1UL,18446744073709551615UL},{9UL,1UL,18446744073709551615UL,0xFA92DE98L,1UL,18446744073709551615UL},{18446744073709551607UL,8UL,1UL,18446744073709551615UL,1UL,8UL},{0xFA92DE98L,1UL,4UL,18446744073709551615UL,1UL,1UL},{18446744073709551607UL,1UL,4UL,0xFA92DE98L,8UL,8UL},{9UL,1UL,1UL,9UL,1UL,18446744073709551615UL},{9UL,1UL,18446744073709551615UL,0xFA92DE98L,1UL,18446744073709551615UL},{18446744073709551607UL,8UL,0UL,1UL,0UL,18446744073709551609UL},{18446744073709551615UL,0x0FF31369L,0x8238EECDL,1UL,0x0FF31369L,0UL},{1UL,0UL,0x8238EECDL,18446744073709551615UL,18446744073709551609UL,18446744073709551609UL}};
                int i, j;
                for (l_533 = 2; (l_533 >= 0); l_533 -= 1)
                { /* block id: 249 */
                    int i;
                    p_85 ^= g_106[l_533];
                }
                for (l_285 = 2; (l_285 >= 0); l_285 -= 1)
                { /* block id: 254 */
                    uint8_t **l_632[4] = {&l_310,&l_310,&l_310,&l_310};
                    int32_t l_642[5][5][7] = {{{0x38B039C4L,1L,1L,1L,0L,0x457E5FE2L,(-6L)},{1L,0L,0xFF04AB9EL,1L,0x843755C6L,0xA14CBB54L,0x74C575B2L},{1L,0L,1L,0x843755C6L,(-1L),1L,0x6708DFF3L},{(-1L),1L,0x457E5FE2L,0xAE9CC528L,0L,0L,0L},{(-1L),1L,0xEF5E38AEL,1L,(-1L),1L,0L}},{{0x55E468F2L,0L,0L,1L,1L,0xAE9CC528L,1L},{(-6L),0L,0x38B039C4L,(-1L),0x6708DFF3L,0L,1L},{0x55E468F2L,1L,0x9A6CC948L,1L,1L,(-10L),0xD3784B47L},{(-1L),0x38B039C4L,0L,0xD3784B47L,0L,0L,0xD3784B47L},{(-1L),6L,(-1L),0L,0xD3784B47L,0x457E5FE2L,1L}},{{1L,1L,3L,0x74C575B2L,(-1L),0x9A6CC948L,1L},{1L,0xAE9CC528L,1L,0xD3784B47L,(-6L),0x457E5FE2L,0L},{0x38B039C4L,0L,(-6L),0x38B039C4L,0x843755C6L,0L,0L},{1L,0x6708DFF3L,(-10L),0x843755C6L,0x843755C6L,(-10L),0x6708DFF3L},{0x843755C6L,0L,0x457E5FE2L,1L,(-6L),0L,0x74C575B2L}},{{0x457E5FE2L,1L,(-1L),0L,(-1L),0xAE9CC528L,(-6L)},{0x55E468F2L,0x6708DFF3L,1L,1L,0xD3784B47L,1L,1L},{0L,(-6L),0x38B039C4L,0x843755C6L,0L,0L,0x55E468F2L},{1L,1L,0x38B039C4L,0x38B039C4L,1L,1L,1L},{(-1L),1L,1L,0xD3784B47L,0x6708DFF3L,0xA14CBB54L,0xD3784B47L}},{{0x843755C6L,0L,(-1L),0x74C575B2L,1L,0x457E5FE2L,0x55E468F2L},{0L,1L,0x457E5FE2L,0L,(-1L),0x38B039C4L,0L},{1L,1L,(-10L),0xD3784B47L,0L,3L,0L},{1L,(-6L),(-6L),1L,(-1L),0L,0x74C575B2L},{0L,0x6708DFF3L,1L,1L,0xAE9CC528L,0xEF5E38AEL,3L}}};
                    uint16_t * const **l_716 = (void*)0;
                    uint16_t * const ***l_715[9][4][3] = {{{&l_716,&l_716,(void*)0},{(void*)0,&l_716,&l_716},{&l_716,&l_716,&l_716},{&l_716,(void*)0,&l_716}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_716},{&l_716,&l_716,(void*)0},{(void*)0,&l_716,&l_716}},{{(void*)0,&l_716,&l_716},{(void*)0,&l_716,&l_716},{&l_716,&l_716,&l_716},{&l_716,(void*)0,(void*)0}},{{&l_716,(void*)0,&l_716},{&l_716,(void*)0,&l_716},{(void*)0,&l_716,&l_716},{&l_716,&l_716,&l_716}},{{&l_716,&l_716,&l_716},{&l_716,&l_716,&l_716},{&l_716,&l_716,&l_716},{(void*)0,&l_716,&l_716}},{{(void*)0,&l_716,&l_716},{(void*)0,&l_716,&l_716},{&l_716,(void*)0,&l_716},{(void*)0,&l_716,&l_716}},{{(void*)0,&l_716,&l_716},{&l_716,(void*)0,&l_716},{&l_716,&l_716,&l_716},{(void*)0,&l_716,(void*)0}},{{&l_716,(void*)0,&l_716},{&l_716,&l_716,&l_716},{&l_716,&l_716,&l_716},{(void*)0,&l_716,&l_716}},{{&l_716,&l_716,(void*)0},{&l_716,&l_716,&l_716},{&l_716,&l_716,(void*)0},{(void*)0,&l_716,&l_716}}};
                    int32_t l_720[8];
                    int i, j, k;
                    for (i = 0; i < 8; i++)
                        l_720[i] = (-9L);
                    if ((safe_div_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u(((((l_310 = l_631) == &g_149) > (l_633 == &g_487)) != (g_106[g_163] >= p_86)), (safe_rshift_func_int8_t_s_s((l_636 <= (safe_unary_minus_func_uint16_t_u((+(!((l_642[1][1][0] = (((safe_sub_func_uint32_t_u_u(g_370[0][0], (g_421[2] , 4294967295UL))) , 9L) || 0x769C28889A2521D7LL)) | g_106[g_163])))))), 0)))), 9L)))
                    { /* block id: 257 */
                        float l_652 = 0x9.2A97D9p-45;
                        int32_t l_653[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_653[i] = 7L;
                        g_656[2]++;
                    }
                    else
                    { /* block id: 259 */
                        int16_t l_659 = (-7L);
                        int32_t l_660 = 0x9A57191BL;
                        int32_t l_662 = (-1L);
                        int32_t l_663 = 0L;
                        int32_t l_664 = (-1L);
                        int32_t l_665[4] = {1L,1L,1L,1L};
                        int64_t *l_698[4] = {&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0],&g_699[0][2][0]};
                        uint16_t * const ****l_717 = (void*)0;
                        uint16_t * const ****l_718 = &l_715[6][2][1];
                        int i;
                        --l_668;
                        l_645[0] |= (safe_mod_func_uint64_t_u_u(l_663, (safe_rshift_func_int16_t_s_s((safe_add_func_uint8_t_u_u(((*l_310) ^= p_86), (((g_699[0][2][0] ^= (safe_add_func_int16_t_s_s((safe_lshift_func_int16_t_s_u((safe_mul_func_int16_t_s_s((safe_add_func_int64_t_s_s((((!(((safe_mod_func_int32_t_s_s((((safe_sub_func_int32_t_s_s(((safe_sub_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u((((1UL && ((((!((g_695 , 65526UL) > ((l_622 != (((safe_rshift_func_uint16_t_u_s(0xBC41L, ((l_642[1][1][0] | ((0xD2L >= 249UL) ^ 0L)) && g_106[g_163]))) && 0xFAL) , &g_137[2])) >= g_108))) >= p_85) == 0UL) ^ p_86)) > p_86) && 0UL), p_86)), p_85)) < 0xC367L), p_85)) & g_134) , l_660), p_86)) ^ p_86) , l_642[4][2][1])) || g_370[2][0]) ^ p_86), g_36)), (**g_140))), g_106[g_163])), g_487))) , p_85) != l_700))), g_695.f5))));
                        l_645[1] &= (safe_add_func_uint16_t_u_u((safe_div_func_uint16_t_u_u((&g_139 == ((safe_div_func_int64_t_s_s(((*l_719) |= (safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s((((*l_631) = ((safe_div_func_uint32_t_u_u((safe_sub_func_int32_t_s_s(l_642[1][1][0], (((*l_718) = l_715[6][1][1]) == &l_716))), (((-5L) && ((g_656[2] |= ((l_720[2] |= (l_719 != l_719)) != 0x1FL)) >= ((g_589[0].f1 && 0x46A3L) == 0L))) && 0x2632B20BL))) >= 0xCFL)) , 0xA591L), 1UL)), 0x8DF4L))), 0x11797956FA709DD2LL)) , (void*)0)), (*g_556))), p_86));
                    }
                }
                l_651 = ((0xA9DC0E9536412412LL == 1L) || (l_645[7] = p_86));
                for (l_655 = 0; (l_655 <= 1); l_655 += 1)
                { /* block id: 276 */
                    uint64_t l_725[6][9][4] = {{{0xE3EADB7DA57EF4D9LL,0xC2C9F82D43B185D2LL,0x5CF3225147A1AA12LL,6UL},{0xF017C58A9FE0D230LL,0x5CF3225147A1AA12LL,0xD271ECDF1AE85582LL,0UL},{1UL,18446744073709551612UL,0x36D8AA41A1D421C5LL,0xD271ECDF1AE85582LL},{18446744073709551609UL,18446744073709551611UL,0xC2C9F82D43B185D2LL,0xB124A009A6B002E5LL},{5UL,0xEDC2071E8C1814D2LL,5UL,0xD6FDAC189BF9A613LL},{0UL,0xC2C9F82D43B185D2LL,18446744073709551610UL,18446744073709551609UL},{0xB227F45578ED2DDALL,0xF33CB52B5F1A789DLL,1UL,0xC2C9F82D43B185D2LL},{0UL,18446744073709551611UL,1UL,18446744073709551607UL},{0xB227F45578ED2DDALL,18446744073709551612UL,18446744073709551610UL,0xBB972B8ABEB62E28LL}},{{0UL,18446744073709551608UL,5UL,0x7DC82A3BD14C5C39LL},{5UL,0x7DC82A3BD14C5C39LL,0xC2C9F82D43B185D2LL,0x2D3A9AA3AF1600EFLL},{18446744073709551609UL,7UL,0x36D8AA41A1D421C5LL,0x1FEA394D4C2BEB74LL},{1UL,0xCA61124D0434D796LL,0xD271ECDF1AE85582LL,18446744073709551608UL},{0xF017C58A9FE0D230LL,9UL,0x5CF3225147A1AA12LL,0xBB972B8ABEB62E28LL},{0xE3EADB7DA57EF4D9LL,0x4FCF1777BF7AD285LL,18446744073709551609UL,18446744073709551607UL},{18446744073709551608UL,0x1FEA394D4C2BEB74LL,18446744073709551607UL,0xE3EADB7DA57EF4D9LL},{0x3A35BFECE152BD25LL,0xF33CB52B5F1A789DLL,18446744073709551615UL,1UL},{0xC2C9F82D43B185D2LL,0xA8A0C845685003A2LL,1UL,0x87BAE6766A29370DLL}},{{0xD6FDAC189BF9A613LL,0x1FEA394D4C2BEB74LL,0x1F861826F55ADFD5LL,1UL},{18446744073709551608UL,1UL,18446744073709551607UL,0x1F861826F55ADFD5LL},{0xB124A009A6B002E5LL,9UL,18446744073709551615UL,0xBB972B8ABEB62E28LL},{18446744073709551614UL,0x2D3A9AA3AF1600EFLL,18446744073709551611UL,0UL},{0x516E1A9D5B4CC840LL,18446744073709551609UL,0x58F80848FAAC3001LL,18446744073709551612UL},{0xE95A86D3AFC42E7FLL,18446744073709551610UL,0UL,18446744073709551610UL},{0xBB972B8ABEB62E28LL,0x3A35BFECE152BD25LL,18446744073709551615UL,18446744073709551609UL},{18446744073709551606UL,1UL,1UL,18446744073709551606UL},{18446744073709551610UL,0x7F1A87F5EE6D3E29LL,18446744073709551611UL,0xE3EADB7DA57EF4D9LL}},{{9UL,0x846F45FC5F523904LL,18446744073709551610UL,0UL},{0UL,0x7DC82A3BD14C5C39LL,18446744073709551610UL,0UL},{18446744073709551608UL,0x846F45FC5F523904LL,0xB124A009A6B002E5LL,0xE3EADB7DA57EF4D9LL},{1UL,0x7F1A87F5EE6D3E29LL,0x2D3A9AA3AF1600EFLL,18446744073709551606UL},{0x152616A7CB516D88LL,1UL,18446744073709551612UL,18446744073709551609UL},{0x294365B540A41758LL,0x3A35BFECE152BD25LL,0x6B35339A546DE4E4LL,18446744073709551610UL},{0x7DC82A3BD14C5C39LL,18446744073709551610UL,5UL,18446744073709551612UL},{0xD271ECDF1AE85582LL,18446744073709551609UL,0x285669637D5BA378LL,0UL},{1UL,0x2D3A9AA3AF1600EFLL,0x1F861826F55ADFD5LL,0xBB972B8ABEB62E28LL}},{{0x5C9AF5BEE3D30D1ELL,9UL,0x9A52E49EFDF49E19LL,0x1F861826F55ADFD5LL},{0x1F861826F55ADFD5LL,1UL,18446744073709551610UL,1UL},{18446744073709551614UL,0x1FEA394D4C2BEB74LL,9UL,0x87BAE6766A29370DLL},{18446744073709551610UL,18446744073709551609UL,0x36D8AA41A1D421C5LL,5UL},{18446744073709551606UL,0x516E1A9D5B4CC840LL,0UL,0xD271ECDF1AE85582LL},{0x4D843E5330631D7ELL,18446744073709551608UL,0x87BAE6766A29370DLL,18446744073709551609UL},{0xB81E7DC84D171033LL,0xE1AB64B1ADF65B43LL,0x58F80848FAAC3001LL,0x73832D81B39D3FEBLL},{18446744073709551610UL,0x4D843E5330631D7ELL,9UL,0xA8A0C845685003A2LL},{0x2019ED6717460BB0LL,0x846F45FC5F523904LL,0xD271ECDF1AE85582LL,18446744073709551608UL}},{{0xB124A009A6B002E5LL,0x294365B540A41758LL,18446744073709551610UL,0x96176BD81AEC4FB4LL},{0x5C9AF5BEE3D30D1ELL,0xE3EADB7DA57EF4D9LL,18446744073709551608UL,0xE3EADB7DA57EF4D9LL},{6UL,0xB227F45578ED2DDALL,18446744073709551611UL,0x73832D81B39D3FEBLL},{0x152616A7CB516D88LL,0x050973B9F4689D75LL,5UL,0x6B35339A546DE4E4LL},{18446744073709551608UL,0x3A35BFECE152BD25LL,18446744073709551615UL,18446744073709551615UL},{18446744073709551608UL,0x516E1A9D5B4CC840LL,5UL,18446744073709551608UL},{0x152616A7CB516D88LL,18446744073709551615UL,18446744073709551611UL,0UL},{6UL,18446744073709551613UL,18446744073709551608UL,1UL},{0x5C9AF5BEE3D30D1ELL,18446744073709551614UL,18446744073709551610UL,18446744073709551608UL}}};
                    float *l_726 = &g_48;
                    const uint16_t * const ** const **l_746 = (void*)0;
                    int32_t l_763 = 0xEDB839DAL;
                    int32_t l_764 = 3L;
                    uint8_t l_765[1];
                    const int16_t * const l_776 = &g_370[2][0];
                    uint32_t l_777 = 4294967295UL;
                    int32_t l_778[10] = {1L,1L,1L,1L,1L,1L,1L,1L,1L,1L};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_765[i] = 0xE3L;
                    if ((!(0x58F4L <= l_725[3][3][1])))
                    { /* block id: 277 */
                        int16_t *l_735 = &l_458;
                        (*l_307) = ((((l_726 == l_726) <= 0L) && (safe_sub_func_int64_t_s_s(0x3A064582C5829C5ALL, ((safe_add_func_int16_t_s_s(p_85, (((*g_580) , g_589[0].f5) < (((safe_mul_func_int16_t_s_s(((*l_735) ^= (safe_sub_func_int64_t_s_s(((l_645[7] = (*l_626)) , p_85), p_86))), 0xAD47L)) == 4294967294UL) | p_85)))) < 7L)))) , (void*)0);
                    }
                    else
                    { /* block id: 281 */
                        l_647 |= 0L;
                        (*l_307) = &p_85;
                    }
                    p_85 = ((((g_108 = p_85) ^ ((safe_mul_func_uint8_t_u_u((0x22L >= ((l_740 != (l_746 = g_743[3])) == (l_747 == l_748[0]))), (safe_div_func_uint16_t_u_u(((***l_747) = (safe_sub_func_uint32_t_u_u(g_134, ((((safe_add_func_int64_t_s_s((safe_add_func_uint64_t_u_u(p_86, ((safe_add_func_int64_t_s_s(((safe_sub_func_int8_t_s_s(l_761, l_725[3][3][1])) == l_644), p_86)) != p_85))), p_85)) != g_3) && 247UL) < 0UL)))), (*l_626))))) != 2L)) , p_86) ^ p_86);
                }
            }
        }
        else
        { /* block id: 308 */
lbl_874:
            g_391[4][2][2] = &p_85;
        }
        for (g_502 = 12; (g_502 != (-2)); g_502 = safe_sub_func_uint64_t_u_u(g_502, 2))
        { /* block id: 313 */
            const uint32_t l_785 = 0x8149A3C4L;
            int64_t *l_815 = &g_699[0][2][0];
            uint8_t *l_854 = (void*)0;
            int32_t l_862 = (-6L);
            const uint64_t l_873 = 18446744073709551606UL;
            volatile union U1 *l_877 = &g_421[1];
            int64_t l_927[6];
            int32_t *l_971[8][10] = {{&l_636,&g_33[8][2],&l_647,&l_285,&l_285,&l_647,&g_33[8][2],&l_636,&g_73,&l_285},{&l_636,(void*)0,&l_285,&l_285,&l_649,&l_285,&g_33[8][2],&l_285,&l_649,&l_285},{&l_285,&g_33[8][2],&l_285,&l_649,&l_285,&l_285,(void*)0,&l_636,&l_649,&l_649},{&l_636,&g_33[8][2],&l_647,&l_285,&l_285,&l_803,&g_172[4][0],&g_15,&l_636,&l_285},{&g_15,&l_636,&l_655,&l_285,&l_647,&l_655,&g_172[4][0],&l_655,&l_647,&l_285},{&l_655,&g_172[4][0],&l_655,&l_647,&l_285,&l_655,&l_636,&g_15,&l_647,&l_647},{&g_15,&g_172[4][0],&l_803,&l_285,&l_285,&l_803,&g_172[4][0],&g_15,&l_636,&l_285},{&g_15,&l_636,&l_655,&l_285,&l_647,&l_655,&g_172[4][0],&l_655,&l_647,&l_285}};
            uint64_t **l_973 = &l_305;
            int i, j;
            for (i = 0; i < 6; i++)
                l_927[i] = 4L;
            for (g_501 = 0; (g_501 == (-23)); g_501 = safe_sub_func_uint32_t_u_u(g_501, 5))
            { /* block id: 316 */
                uint16_t *l_802 = &g_134;
                int64_t *l_814 = (void*)0;
                int32_t l_827 = 0L;
                int32_t l_851 = (-1L);
                int32_t l_869 = 1L;
                union U1 *l_892[7];
                int i;
                for (i = 0; i < 7; i++)
                    l_892[i] = &g_890;
                p_85 &= 1L;
                if ((g_784 , l_785))
                { /* block id: 318 */
                    l_285 &= p_85;
                }
                else
                { /* block id: 320 */
                    for (g_108 = 0; (g_108 == 26); ++g_108)
                    { /* block id: 323 */
                        (*l_307) = &g_15;
                        (*g_791) = (safe_mul_func_float_f_f((l_790[1][1][0] < p_86), 0xA.86AAF5p+44));
                    }
                }
                if (((0xA011L == ((safe_mul_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u(g_542.f3, (((safe_mod_func_uint32_t_u_u((p_86 == ((void*)0 != &l_790[1][1][0])), g_418[4].f5)) || (safe_mod_func_int16_t_s_s(0x1AF3L, (safe_rshift_func_uint8_t_u_u(((((l_802 == (void*)0) == p_85) < g_268[4][1].f6) & g_769.f3), l_785))))) != 0L))), l_803)) , l_785)) != 18446744073709551615UL))
                { /* block id: 328 */
                    const int32_t l_826 = 0L;
                    int32_t l_829 = 0x083AAAF6L;
                    int8_t *l_867 = &l_533;
                    uint8_t l_870 = 0UL;
                    if ((safe_sub_func_int32_t_s_s(0x9E1AEA20L, g_172[8][1])))
                    { /* block id: 329 */
                        uint16_t l_850 = 0x2343L;
                        l_654 = (safe_mul_func_int16_t_s_s((g_542.f1 , (safe_lshift_func_int8_t_s_u(((safe_lshift_func_int16_t_s_u(((safe_lshift_func_uint16_t_u_u(0UL, 1)) <= ((*l_802) = ((l_814 == l_815) < (l_829 = ((safe_sub_func_uint16_t_u_u(((safe_div_func_int64_t_s_s(((!(safe_unary_minus_func_uint64_t_u(((safe_div_func_uint8_t_u_u(((*l_310) = ((l_828 = (((((*l_815) = (safe_add_func_int64_t_s_s(9L, 18446744073709551614UL))) | (g_141.f2 >= (&g_106[1] != ((((0xD0E83240L != 0x09D0EEF8L) || l_826) >= 0UL) , &g_370[0][0])))) == l_827) && 4294967293UL)) || 0x6EL)), p_85)) ^ l_785)))) >= 4L), l_827)) == 0x6EL), 0L)) && (-3L)))))), p_85)) == g_779.f5), 3))), l_826));
                        l_851 |= ((safe_mod_func_uint32_t_u_u(((safe_div_func_int16_t_s_s(l_785, g_769.f1)) >= (l_826 ^ (((((*l_815) = (((safe_mul_func_uint8_t_u_u(p_86, (safe_mod_func_uint8_t_u_u((safe_add_func_uint16_t_u_u(((*g_138) = p_85), (safe_div_func_uint8_t_u_u((p_85 > (safe_add_func_int64_t_s_s((l_827 , (safe_mod_func_uint64_t_u_u((p_85 != (safe_lshift_func_uint8_t_u_u(((*l_310)--), p_86))), l_826))), l_785))), l_785)))), l_850)))) != g_695.f3) ^ p_85)) != l_850) > p_86) > 0xE75DL))), 0x8ED8C8D3L)) , l_829);
                    }
                    else
                    { /* block id: 340 */
                        uint16_t l_861 = 9UL;
                        l_869 |= (p_85 && ((~(((~((l_854 == (void*)0) <= ((safe_add_func_int64_t_s_s(((safe_sub_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u(((l_862 = (l_861 , l_827)) | (p_86 , 4294967289UL)), (safe_mod_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((l_867 == l_868), l_826)), g_784.f3)))) | l_861), g_650[4][0])) > p_85), 0xA5A9355386013C20LL)) >= 3UL))) != (****g_744)) > l_851)) && 0x64AD80FD6031EF99LL));
                        if (p_86)
                            continue;
                        l_870++;
                        p_85 ^= l_873;
                    }
                    if (g_784.f0)
                        goto lbl_874;
                    for (l_862 = 14; (l_862 < (-22)); l_862 = safe_sub_func_uint64_t_u_u(l_862, 4))
                    { /* block id: 350 */
                        (*l_307) = &p_85;
                        l_877 = &g_421[3];
                        if (l_870)
                            continue;
                    }
                    l_829 = l_873;
                }
                else
                { /* block id: 356 */
                    uint64_t l_880 = 18446744073709551615UL;
                    union U1 **l_891[10][9] = {{(void*)0,&g_889,(void*)0,&g_889,&g_889,(void*)0,&g_889,(void*)0,&g_889},{&g_889,&g_889,(void*)0,&g_889,&g_889,&g_889,(void*)0,&g_889,&g_889},{&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889},{(void*)0,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889},{&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889},{&g_889,&g_889,&g_889,&g_889,(void*)0,&g_889,&g_889,&g_889,(void*)0},{(void*)0,&g_889,&g_889,(void*)0,&g_889,(void*)0,&g_889,&g_889,(void*)0},{&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,(void*)0,&g_889,(void*)0},{&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889,&g_889},{&g_889,&g_889,(void*)0,&g_889,(void*)0,&g_889,&g_889,&g_889,&g_889}};
                    int16_t *l_899 = &l_458;
                    float *l_901 = &g_171;
                    uint64_t l_902 = 1UL;
                    float *l_903 = &g_48;
                    int i, j;
                    p_85 = p_86;
                    (*l_903) = (safe_mul_func_float_f_f(l_869, ((l_880 == (safe_div_func_float_f_f((safe_mul_func_float_f_f((((safe_div_func_float_f_f((0x0.5419FBp+56 < (&l_622 != &l_622)), ((*l_901) = (safe_add_func_float_f_f(((((p_85 = ((l_892[4] = g_889) != (l_893 = &g_890))) | (safe_lshift_func_int16_t_s_s(((*l_899) = (~((*l_305)++))), 4))) , &g_115[1]) == (void*)0), l_900))))) == l_785) != l_902), 0x9.Bp-1)), p_86))) > p_86)));
                }
            }
            if (((*l_307) == (void*)0))
            { /* block id: 367 */
                return g_418[4].f4;
            }
            else
            { /* block id: 369 */
                float l_913[10][5][5] = {{{0x7.29A12Ep-91,0x0.Fp-1,(-0x10.2p+1),0x5.Ep-1,0x5.A7C505p+54},{(-0x1.Ap-1),0x4.6p-1,0x3.4977EEp-64,0x5.A7C505p+54,0x1.Cp-1},{(-0x1.Cp+1),0x9.4234F2p-15,0x3.4977EEp-64,0x1.0ADD06p+0,(-0x1.Dp-1)},{0x0.8p-1,0x4.43620Bp+19,(-0x10.2p+1),0x9.069F3Fp+28,(-0x1.6p+1)},{(-0x1.Cp+1),0x6.0042F9p+55,0x7.29A12Ep-91,0x9.069F3Fp+28,0x9.069F3Fp+28}},{{(-0x1.Ap-1),0x0.Dp+1,(-0x1.Ap-1),0x1.0ADD06p+0,0x1.691E95p+56},{0x7.29A12Ep-91,0x6.0042F9p+55,(-0x1.Cp+1),0x5.A7C505p+54,0x1.691E95p+56},{(-0x10.2p+1),0x4.43620Bp+19,0x0.8p-1,0x5.Ep-1,0x9.069F3Fp+28},{0x3.4977EEp-64,0x9.4234F2p-15,(-0x1.Cp+1),0x1.691E95p+56,(-0x1.6p+1)},{0x3.4977EEp-64,0x4.6p-1,(-0x1.Ap-1),(-0x10.9p+1),(-0x1.Dp-1)}},{{(-0x10.2p+1),0x0.Fp-1,0x7.29A12Ep-91,0x1.691E95p+56,0x1.Cp-1},{0x7.29A12Ep-91,(-0x1.6p-1),0x1.38A5C7p+58,0x0.8p-1,(-0x10.2p+1)},{0x5.7p+1,(-0x5.2p-1),0x6.551A52p+21,(-0x10.2p+1),0x6.EC54C1p-2},{(-0x2.5p+1),0xF.2EF906p+26,0x6.551A52p+21,(-0x1.Cp+1),0x7.29A12Ep-91},{0x1.Dp-1,(-0x6.8p+1),0x1.38A5C7p+58,(-0x1.2p+1),0x3.4977EEp-64}},{{(-0x2.5p+1),0x4.2p-1,0x9.3DF24Dp+66,(-0x1.2p+1),(-0x1.2p+1)},{0x5.7p+1,0x8.9AB7C4p+11,0x5.7p+1,(-0x1.Cp+1),0x0.9B67C4p+2},{0x9.3DF24Dp+66,0x4.2p-1,(-0x2.5p+1),(-0x10.2p+1),0x0.9B67C4p+2},{0x1.38A5C7p+58,(-0x6.8p+1),0x1.Dp-1,0x0.8p-1,(-0x1.2p+1)},{0x6.551A52p+21,0xF.2EF906p+26,(-0x2.5p+1),0x0.9B67C4p+2,0x3.4977EEp-64}},{{0x6.551A52p+21,(-0x5.2p-1),0x5.7p+1,0x6.F49C49p-58,0x7.29A12Ep-91},{0x1.38A5C7p+58,(-0x1.6p-1),0x9.3DF24Dp+66,0x0.9B67C4p+2,0x6.EC54C1p-2},{0x9.3DF24Dp+66,(-0x1.6p-1),0x1.38A5C7p+58,0x0.8p-1,(-0x10.2p+1)},{0x5.7p+1,(-0x5.2p-1),0x6.551A52p+21,(-0x10.2p+1),0x6.EC54C1p-2},{(-0x2.5p+1),0xF.2EF906p+26,0x6.551A52p+21,(-0x1.Cp+1),0x7.29A12Ep-91}},{{0x1.Dp-1,(-0x6.8p+1),0x1.38A5C7p+58,(-0x1.2p+1),0x3.4977EEp-64},{(-0x2.5p+1),0x4.2p-1,0x9.3DF24Dp+66,(-0x1.2p+1),(-0x1.2p+1)},{0x5.7p+1,0x8.9AB7C4p+11,0x5.7p+1,(-0x1.Cp+1),0x0.9B67C4p+2},{0x9.3DF24Dp+66,0x4.2p-1,(-0x2.5p+1),(-0x10.2p+1),0x0.9B67C4p+2},{0x1.38A5C7p+58,(-0x6.8p+1),0x1.Dp-1,0x0.8p-1,(-0x1.2p+1)}},{{0x6.551A52p+21,0xF.2EF906p+26,(-0x2.5p+1),0x0.9B67C4p+2,0x3.4977EEp-64},{0x6.551A52p+21,(-0x5.2p-1),0x5.7p+1,0x6.F49C49p-58,0x7.29A12Ep-91},{0x1.38A5C7p+58,(-0x1.6p-1),0x9.3DF24Dp+66,0x0.9B67C4p+2,0x6.EC54C1p-2},{0x9.3DF24Dp+66,(-0x1.6p-1),0x1.38A5C7p+58,0x0.8p-1,(-0x10.2p+1)},{0x5.7p+1,(-0x5.2p-1),0x6.551A52p+21,(-0x10.2p+1),0x6.EC54C1p-2}},{{(-0x2.5p+1),0xF.2EF906p+26,0x6.551A52p+21,(-0x1.Cp+1),0x7.29A12Ep-91},{0x1.Dp-1,(-0x6.8p+1),0x1.38A5C7p+58,(-0x1.2p+1),0x3.4977EEp-64},{(-0x2.5p+1),0x4.2p-1,0x9.3DF24Dp+66,(-0x1.2p+1),(-0x1.2p+1)},{0x5.7p+1,0x8.9AB7C4p+11,0x5.7p+1,(-0x1.Cp+1),0x0.9B67C4p+2},{0x9.3DF24Dp+66,0x4.2p-1,(-0x2.5p+1),(-0x10.2p+1),0x0.9B67C4p+2}},{{0x1.38A5C7p+58,(-0x6.8p+1),0x1.Dp-1,0x0.8p-1,(-0x1.2p+1)},{0x6.551A52p+21,0xF.2EF906p+26,(-0x2.5p+1),0x0.9B67C4p+2,0x3.4977EEp-64},{0x6.551A52p+21,(-0x5.2p-1),0x5.7p+1,0x6.F49C49p-58,0x7.29A12Ep-91},{0x1.38A5C7p+58,(-0x1.6p-1),0x9.3DF24Dp+66,0x0.9B67C4p+2,0x6.EC54C1p-2},{0x9.3DF24Dp+66,(-0x1.6p-1),0x1.38A5C7p+58,0x0.8p-1,(-0x10.2p+1)}},{{0x5.7p+1,(-0x5.2p-1),0x6.551A52p+21,(-0x10.2p+1),0x6.EC54C1p-2},{(-0x2.5p+1),0xF.2EF906p+26,0x6.551A52p+21,(-0x1.Cp+1),0x7.29A12Ep-91},{0x1.Dp-1,(-0x6.8p+1),0x1.38A5C7p+58,(-0x1.2p+1),0x3.4977EEp-64},{(-0x2.5p+1),0x4.2p-1,0x9.3DF24Dp+66,(-0x1.2p+1),(-0x1.2p+1)},{0x5.7p+1,(-0x1.6p+1),0x9.4234F2p-15,(-0x2.5p+1),0x5.C39592p+35}}};
                int32_t l_914 = 0L;
                uint32_t *l_915 = &g_656[2];
                int32_t *l_946 = &g_502;
                int32_t **l_945 = &l_946;
                int32_t **l_950[8] = {&l_946,&l_946,&l_946,&l_946,&l_946,&l_946,&l_946,&l_946};
                int32_t *l_972 = &l_649;
                int i, j, k;
                if (p_85)
                    break;
                if ((l_648 |= (~(p_86 && (safe_lshift_func_uint8_t_u_s((p_85 , 6UL), (safe_mod_func_uint8_t_u_u((safe_div_func_int64_t_s_s(((*l_815) |= (safe_lshift_func_int8_t_s_u((((*l_868) = (l_785 , (p_86 != l_914))) , 0x66L), ((l_915 == &l_900) == p_86)))), 1UL)), 0x53L))))))))
                { /* block id: 374 */
                    uint32_t l_931 = 18446744073709551610UL;
                    if (g_695.f0)
                        goto lbl_874;
                    l_862 &= ((safe_div_func_uint16_t_u_u(p_85, (g_918 , (safe_mul_func_int16_t_s_s(((((l_748[0] == (void*)0) == ((~(p_86 > (!(((((safe_add_func_float_f_f((safe_mul_func_float_f_f(l_785, (l_927[1] >= (((safe_lshift_func_int8_t_s_s(p_85, 5)) , &l_310) != (void*)0)))), l_930)) , 0x4968536BL) , l_914) | 18446744073709551615UL) ^ 4L)))) , p_86)) , l_914) , 0x2CBBL), g_134))))) ^ 0xFD62DA1DL);
                    if (l_931)
                        continue;
                }
                else
                { /* block id: 378 */
                    int32_t ***l_947[5][1][10] = {{{&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945}},{{&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945}},{{&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945}},{{&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945}},{{&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945,&l_945}}};
                    int32_t l_970 = 0x8A51F16AL;
                    uint32_t l_998 = 0xD366D086L;
                    int i, j, k;
                    for (g_577 = 0; (g_577 == 38); g_577 = safe_add_func_int64_t_s_s(g_577, 8))
                    { /* block id: 381 */
                        float *l_934 = &g_48;
                        (*l_934) = (-0x1.Dp+1);
                    }
                    if (((safe_lshift_func_uint16_t_u_s((0UL || (safe_div_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s((g_511.f1 | (safe_mod_func_int16_t_s_s((0x96619711L != (safe_add_func_uint16_t_u_u(((g_948 = l_945) == (l_950[3] = &l_946)), ((safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s((safe_mul_func_int16_t_s_s((((*l_310) |= p_85) < (safe_div_func_int8_t_s_s(((*l_868) = 0L), (safe_mul_func_int16_t_s_s((0x113EBD1046B418D0LL <= (safe_rshift_func_uint8_t_u_s((((safe_add_func_int8_t_s_s((l_969 ^= (safe_mul_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_u(((*g_138) = 0x370DL), l_914)) , p_86), 0x0469L))), 0x9CL)) & l_970) || l_970), 4))), l_970))))), 0x0BC9L)), l_927[1])), l_927[2])) <= g_695.f3)))), (-1L)))), 4)), 0x66L))), 7)) <= 1UL))
                    { /* block id: 390 */
                        l_971[5][1] = (*l_307);
                        (*l_307) = &g_33[6][5];
                        (*l_307) = (l_972 = (*l_307));
                    }
                    else
                    { /* block id: 395 */
                        uint64_t ***l_974 = (void*)0;
                        uint64_t ***l_975 = &l_973;
                        (*l_975) = l_973;
                        return p_85;
                    }
                    l_655 &= (p_85 = ((safe_mul_func_uint16_t_u_u(5UL, (safe_add_func_int32_t_s_s((p_85 , (safe_div_func_uint64_t_u_u(l_970, (p_86 & (safe_rshift_func_int8_t_s_u((**l_307), ((*l_310) = (*l_972)))))))), (safe_mod_func_uint64_t_u_u((((safe_sub_func_int16_t_s_s(((safe_lshift_func_int16_t_s_s(p_85, (safe_div_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_u(g_172[4][1], ((*g_138) = (safe_add_func_uint8_t_u_u((*l_972), 0x30L))))) , (*l_972)), 0xE8L)))) & g_19), (**l_307))) | g_996) == g_511.f4), l_997)))))) >= l_998));
                }
                if (g_3)
                    goto lbl_874;
                if (p_86)
                { /* block id: 405 */
                    if (g_177)
                        goto lbl_999;
                }
                else
                { /* block id: 407 */
                    int8_t l_1000 = 0x65L;
                    l_1000 &= 0xE60DBFC3L;
                    if (p_86)
                    { /* block id: 409 */
                        volatile int32_t *l_1002 = &g_14;
                        volatile int32_t **l_1001[9][2] = {{&l_1002,&l_1002},{&l_1002,&l_1002},{&l_1002,&l_1002},{&l_1002,&l_1002},{&l_1002,&l_1002},{&l_1002,&l_1002},{&l_1002,&l_1002},{&l_1002,&l_1002},{&l_1002,&l_1002}};
                        uint8_t * const *l_1005[7];
                        uint8_t * const **l_1004 = &l_1005[3];
                        int i, j;
                        for (i = 0; i < 7; i++)
                            l_1005[i] = &l_854;
                        l_1003 = &g_18;
                        (*l_1004) = (void*)0;
                        if (g_1006)
                            continue;
                    }
                    else
                    { /* block id: 413 */
                        return g_136.f0;
                    }
                    p_85 |= 1L;
                    for (l_700 = 0; (l_700 <= 1); l_700 += 1)
                    { /* block id: 419 */
                        uint64_t l_1007 = 0xBDF74539646B3BD5LL;
                        ++l_1007;
                    }
                }
            }
        }
    }
    (*l_307) = &l_647;
    return p_86;
}


/* ------------------------------------------ */
/* 
 * reads : g_12 g_111 g_108 g_17 g_136 g_137 g_139 g_141 g_149 g_162 g_163 g_177 g_15 g_138 g_106 g_73 g_48 g_140 g_134 g_33 g_268 g_116
 * writes: g_115 g_134 g_108 g_140 g_2 g_149 g_48 g_163 g_172 g_177 g_106 g_171 g_162
 */
static int32_t  func_87(uint16_t  p_88, float  p_89, int32_t * p_90, const uint16_t * p_91)
{ /* block id: 36 */
    uint32_t l_112[3];
    int32_t *l_113 = (void*)0;
    const int8_t *l_114 = &g_108;
    int16_t *l_117 = &g_106[1];
    uint32_t l_130[6];
    uint16_t l_132 = 65527UL;
    uint16_t ***l_143 = &g_140;
    uint16_t ****l_142 = &l_143;
    int32_t l_170 = (-4L);
    int32_t l_175 = 1L;
    uint32_t l_199 = 4294967293UL;
    uint8_t l_231 = 247UL;
    int32_t l_239 = 0x945F2B69L;
    int32_t l_242 = 7L;
    int32_t l_243 = 0L;
    int32_t l_247 = 0x9EB32EB6L;
    int32_t l_248 = (-1L);
    int32_t l_250 = 1L;
    int32_t l_251 = 0L;
    int32_t l_253 = 0x63735463L;
    int32_t l_255 = (-9L);
    int32_t l_257 = (-1L);
    int32_t l_258 = 1L;
    int16_t l_280[1][2];
    int i, j;
    for (i = 0; i < 3; i++)
        l_112[i] = 18446744073709551615UL;
    for (i = 0; i < 6; i++)
        l_130[i] = 18446744073709551615UL;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_280[i][j] = 3L;
    }
    if ((((l_112[2] < (p_88 >= 0x62L)) & g_12) > ((void*)0 == l_113)))
    { /* block id: 37 */
        int32_t l_131 = 0L;
        uint16_t *l_133 = &g_134;
        int64_t l_188[3];
        uint16_t ****l_230 = &l_143;
        int8_t l_232 = 0x49L;
        int32_t l_233 = (-5L);
        int32_t l_240 = 0xE4834083L;
        int32_t l_241 = 0x4A37C750L;
        int32_t l_244 = 0x8CA7A099L;
        int16_t l_245 = 0L;
        int32_t l_246 = 0xA0433F7CL;
        int32_t l_249 = 0L;
        int32_t l_252 = 0xAEB59524L;
        int32_t l_254 = 1L;
        int32_t l_256 = 0x1EC57159L;
        uint16_t l_259[5] = {9UL,9UL,9UL,9UL,9UL};
        int i;
        for (i = 0; i < 3; i++)
            l_188[i] = 0x086F9374BA5FEA88LL;
        if ((((((*l_133) = ((((g_115[1] = l_114) == (void*)0) > (l_117 == (void*)0)) || ((safe_lshift_func_uint16_t_u_u((*p_91), 14)) <= (l_132 &= (safe_sub_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(0xF60EL, (((safe_add_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(((g_108 | 0xA4DEL) & (0x81B9L >= l_130[4])), p_88)), l_131)) && 0xC2L) == 4L))), g_111)))))) , p_88) <= 0xACL) && g_17))
        { /* block id: 41 */
            uint8_t l_154 = 0x96L;
            for (g_108 = 0; (g_108 <= 5); g_108 += 1)
            { /* block id: 44 */
                uint32_t l_150 = 0xDD06FED4L;
                for (l_132 = 0; (l_132 <= 2); l_132 += 1)
                { /* block id: 47 */
                    float *l_135 = &g_48;
                    int32_t *l_153 = &l_131;
                    int i;
                    if ((((l_131 = (l_112[l_132] | l_130[g_108])) , (l_135 == (l_130[(l_132 + 3)] , (g_136 , l_113)))) == (-8L)))
                    { /* block id: 49 */
                        uint16_t ****l_144 = &l_143;
                        uint8_t *l_148 = &g_149;
                        (*g_139) = g_137[1];
                        g_2 = (g_141 , (-3L));
                        l_144 = l_142;
                        (*l_135) = (!(&g_137[4] == ((((g_141.f3 , (l_131 = (safe_mul_func_uint8_t_u_u(0xD3L, ((*l_148) &= l_130[g_108]))))) , (void*)0) == &g_108) , (*l_142))));
                    }
                    else
                    { /* block id: 56 */
                        ++l_150;
                    }
                    l_154++;
                }
                for (l_154 = 0; (l_154 <= 2); l_154 += 1)
                { /* block id: 63 */
                    uint16_t l_157 = 4UL;
                    if (l_157)
                        break;
                    g_48 = l_157;
                }
                return l_131;
            }
        }
        else
        { /* block id: 69 */
            int8_t *l_164 = (void*)0;
            int8_t *l_165 = (void*)0;
            int8_t *l_166 = &g_108;
            int32_t l_169 = 0L;
            uint32_t *l_176 = &g_177;
            int32_t **l_178[2];
            int8_t **l_181 = &l_166;
            float *l_200 = (void*)0;
            int i;
            for (i = 0; i < 2; i++)
                l_178[i] = &l_113;
            (*g_162) ^= (safe_rshift_func_uint16_t_u_s((safe_lshift_func_uint8_t_u_s(1UL, 1)), 14));
            p_90 = ((((*g_138) = (((*l_176) ^= (((*l_166) = p_88) ^ (safe_add_func_int32_t_s_s(((*g_162) = (-5L)), ((((void*)0 == &g_140) >= ((l_169 | (g_172[4][1] = (l_170 & l_131))) != 0x1112FB1C122574F0LL)) & (safe_sub_func_int32_t_s_s(l_175, p_88))))))) || (*p_90))) && 0x45D3L) , (void*)0);
            if (p_88)
                goto lbl_201;
lbl_201:
            g_48 = (safe_div_func_float_f_f((((((&g_108 == ((*l_181) = l_166)) > (safe_add_func_float_f_f(((g_171 = ((safe_mul_func_float_f_f(p_88, (safe_div_func_float_f_f(((((g_111 , &g_115[1]) == (void*)0) != ((l_188[0] == (((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_add_func_float_f_f((((*g_162) , ((((*l_117) |= ((safe_div_func_uint8_t_u_u(((g_108 = (((safe_lshift_func_uint16_t_u_u(((0x25F0BFBCL || g_177) && l_199), l_188[0])) , &g_149) != (void*)0)) != p_88), p_88)) , 0L)) | (*p_91)) | l_188[1])) , p_88), g_149)), (-0x2.5p+1))), l_188[0])) == 0x0.3p-1) <= p_88)) >= g_73)) == l_188[1]), g_48)))) > l_188[0])) == l_188[0]), 0x1.19AB3Fp+25))) > p_89) < l_131) < g_149), (-0x2.2p-1)));
            return (*g_162);
        }
        l_233 |= ((safe_sub_func_uint64_t_u_u(((*p_90) <= ((!((safe_add_func_int16_t_s_s((safe_sub_func_int8_t_s_s(p_88, (safe_sub_func_int64_t_s_s((safe_div_func_uint8_t_u_u(l_132, l_131)), (safe_add_func_int8_t_s_s((safe_mul_func_int16_t_s_s((safe_add_func_int8_t_s_s(0L, (p_88 <= ((safe_sub_func_int16_t_s_s((~(safe_mod_func_uint32_t_u_u((safe_div_func_int8_t_s_s(((safe_rshift_func_int8_t_s_s((((p_88 || ((safe_add_func_uint16_t_u_u(((void*)0 != l_230), l_231)) , p_88)) & l_232) == (***g_139)), l_188[0])) || 0UL), p_88)), 0xC30D8970L))), 0xBE69L)) < p_88)))), 65535UL)), (-1L))))))), l_188[0])) & 0x83EFAFE7L)) && 0x98D20E63L)), g_33[7][1])) , (*g_162));
        for (l_170 = 25; (l_170 > 1); --l_170)
        { /* block id: 88 */
            int32_t *l_236 = &l_233;
            int32_t *l_237 = &l_175;
            int32_t *l_238[7] = {&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3};
            int i;
            --l_259[0];
        }
    }
    else
    { /* block id: 91 */
        int32_t **l_262[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
        uint16_t *l_267[6] = {&l_132,&l_132,&l_132,&l_132,&l_132,&l_132};
        float *l_277 = &g_171;
        const int32_t l_278[10] = {8L,0L,8L,8L,0L,8L,8L,0L,8L,8L};
        uint16_t l_279[9][1][7] = {{{65535UL,65535UL,4UL,65535UL,65535UL,4UL,65535UL}},{{0x41D8L,65526UL,65526UL,0x41D8L,65526UL,65526UL,0x41D8L}},{{65532UL,65535UL,65532UL,65532UL,65535UL,65532UL,65532UL}},{{0x41D8L,0x41D8L,65530UL,0x41D8L,0x41D8L,65530UL,0x41D8L}},{{65535UL,65532UL,65532UL,65535UL,65532UL,65532UL,65535UL}},{{65526UL,0x41D8L,65526UL,65526UL,0x41D8L,65526UL,65526UL}},{{65535UL,65535UL,4UL,65535UL,65535UL,4UL,65535UL}},{{0x41D8L,65526UL,65526UL,0x41D8L,65526UL,65526UL,0x41D8L}},{{65532UL,65535UL,65532UL,65532UL,65535UL,65532UL,65532UL}}};
        int i, j, k;
        g_162 = (void*)0;
        l_279[3][0][6] ^= ((safe_add_func_int16_t_s_s(0x624DL, ((safe_add_func_uint8_t_u_u((((l_251 = ((****l_142) = (*p_91))) , g_268[4][1]) , ((0L <= ((safe_mul_func_int16_t_s_s(p_88, (*g_138))) , (*p_90))) && ((**g_140) = ((safe_div_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(0xB8L, ((((*l_277) = (safe_mul_func_float_f_f(0x7.Fp-1, g_268[4][1].f6))) <= l_278[3]) , g_116))), p_88)) >= 0x78L)))), 9L)) > 5UL))) && p_88);
    }
    return l_280[0][1];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_12, "g_12", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_17, "g_17", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    transparent_crc(g_19, "g_19", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_33[i][j], "g_33[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc_bytes (&g_48, sizeof(g_48), "g_48", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_50[i], "g_50[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_60[i], "g_60[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_73, "g_73", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_106[i], "g_106[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_108, "g_108", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_134, "g_134", print_hash_value);
    transparent_crc(g_136.f0, "g_136.f0", print_hash_value);
    transparent_crc(g_141.f0, "g_141.f0", print_hash_value);
    transparent_crc(g_141.f1, "g_141.f1", print_hash_value);
    transparent_crc(g_141.f2, "g_141.f2", print_hash_value);
    transparent_crc(g_141.f3, "g_141.f3", print_hash_value);
    transparent_crc(g_141.f4, "g_141.f4", print_hash_value);
    transparent_crc(g_141.f5, "g_141.f5", print_hash_value);
    transparent_crc(g_141.f6, "g_141.f6", print_hash_value);
    transparent_crc(g_149, "g_149", print_hash_value);
    transparent_crc(g_163, "g_163", print_hash_value);
    transparent_crc_bytes (&g_171, sizeof(g_171), "g_171", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_172[i][j], "g_172[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_177, "g_177", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_268[i][j].f0, "g_268[i][j].f0", print_hash_value);
            transparent_crc(g_268[i][j].f1, "g_268[i][j].f1", print_hash_value);
            transparent_crc(g_268[i][j].f2, "g_268[i][j].f2", print_hash_value);
            transparent_crc(g_268[i][j].f3, "g_268[i][j].f3", print_hash_value);
            transparent_crc(g_268[i][j].f4, "g_268[i][j].f4", print_hash_value);
            transparent_crc(g_268[i][j].f5, "g_268[i][j].f5", print_hash_value);
            transparent_crc(g_268[i][j].f6, "g_268[i][j].f6", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_306, "g_306", print_hash_value);
    transparent_crc(g_309, "g_309", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_370[i][j], "g_370[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_418[i].f0, "g_418[i].f0", print_hash_value);
        transparent_crc(g_418[i].f1, "g_418[i].f1", print_hash_value);
        transparent_crc(g_418[i].f2, "g_418[i].f2", print_hash_value);
        transparent_crc(g_418[i].f3, "g_418[i].f3", print_hash_value);
        transparent_crc(g_418[i].f4, "g_418[i].f4", print_hash_value);
        transparent_crc(g_418[i].f5, "g_418[i].f5", print_hash_value);
        transparent_crc(g_418[i].f6, "g_418[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_421[i].f0, "g_421[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_487, "g_487", print_hash_value);
    transparent_crc(g_501, "g_501", print_hash_value);
    transparent_crc(g_502, "g_502", print_hash_value);
    transparent_crc(g_511.f0, "g_511.f0", print_hash_value);
    transparent_crc(g_511.f1, "g_511.f1", print_hash_value);
    transparent_crc(g_511.f2, "g_511.f2", print_hash_value);
    transparent_crc(g_511.f3, "g_511.f3", print_hash_value);
    transparent_crc(g_511.f4, "g_511.f4", print_hash_value);
    transparent_crc(g_511.f5, "g_511.f5", print_hash_value);
    transparent_crc(g_511.f6, "g_511.f6", print_hash_value);
    transparent_crc(g_542.f0, "g_542.f0", print_hash_value);
    transparent_crc(g_542.f1, "g_542.f1", print_hash_value);
    transparent_crc(g_542.f2, "g_542.f2", print_hash_value);
    transparent_crc(g_542.f3, "g_542.f3", print_hash_value);
    transparent_crc(g_542.f4, "g_542.f4", print_hash_value);
    transparent_crc(g_542.f5, "g_542.f5", print_hash_value);
    transparent_crc(g_542.f6, "g_542.f6", print_hash_value);
    transparent_crc(g_577, "g_577", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_589[i].f0, "g_589[i].f0", print_hash_value);
        transparent_crc(g_589[i].f1, "g_589[i].f1", print_hash_value);
        transparent_crc(g_589[i].f2, "g_589[i].f2", print_hash_value);
        transparent_crc(g_589[i].f3, "g_589[i].f3", print_hash_value);
        transparent_crc(g_589[i].f4, "g_589[i].f4", print_hash_value);
        transparent_crc(g_589[i].f5, "g_589[i].f5", print_hash_value);
        transparent_crc(g_589[i].f6, "g_589[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_650[i][j], "g_650[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_656[i], "g_656[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_695.f0, "g_695.f0", print_hash_value);
    transparent_crc(g_695.f1, "g_695.f1", print_hash_value);
    transparent_crc(g_695.f2, "g_695.f2", print_hash_value);
    transparent_crc(g_695.f3, "g_695.f3", print_hash_value);
    transparent_crc(g_695.f4, "g_695.f4", print_hash_value);
    transparent_crc(g_695.f5, "g_695.f5", print_hash_value);
    transparent_crc(g_695.f6, "g_695.f6", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_699[i][j][k], "g_699[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_769.f0, "g_769.f0", print_hash_value);
    transparent_crc(g_769.f1, "g_769.f1", print_hash_value);
    transparent_crc(g_769.f2, "g_769.f2", print_hash_value);
    transparent_crc(g_769.f3, "g_769.f3", print_hash_value);
    transparent_crc(g_769.f4, "g_769.f4", print_hash_value);
    transparent_crc(g_769.f5, "g_769.f5", print_hash_value);
    transparent_crc(g_769.f6, "g_769.f6", print_hash_value);
    transparent_crc(g_779.f0, "g_779.f0", print_hash_value);
    transparent_crc(g_779.f1, "g_779.f1", print_hash_value);
    transparent_crc(g_779.f2, "g_779.f2", print_hash_value);
    transparent_crc(g_779.f3, "g_779.f3", print_hash_value);
    transparent_crc(g_779.f4, "g_779.f4", print_hash_value);
    transparent_crc(g_779.f5, "g_779.f5", print_hash_value);
    transparent_crc(g_779.f6, "g_779.f6", print_hash_value);
    transparent_crc(g_784.f0, "g_784.f0", print_hash_value);
    transparent_crc(g_784.f1, "g_784.f1", print_hash_value);
    transparent_crc(g_784.f2, "g_784.f2", print_hash_value);
    transparent_crc(g_784.f3, "g_784.f3", print_hash_value);
    transparent_crc(g_784.f4, "g_784.f4", print_hash_value);
    transparent_crc(g_784.f5, "g_784.f5", print_hash_value);
    transparent_crc(g_784.f6, "g_784.f6", print_hash_value);
    transparent_crc(g_890.f0, "g_890.f0", print_hash_value);
    transparent_crc(g_918.f0, "g_918.f0", print_hash_value);
    transparent_crc(g_918.f1, "g_918.f1", print_hash_value);
    transparent_crc(g_918.f2, "g_918.f2", print_hash_value);
    transparent_crc(g_918.f3, "g_918.f3", print_hash_value);
    transparent_crc(g_918.f4, "g_918.f4", print_hash_value);
    transparent_crc(g_918.f5, "g_918.f5", print_hash_value);
    transparent_crc(g_918.f6, "g_918.f6", print_hash_value);
    transparent_crc(g_996, "g_996", print_hash_value);
    transparent_crc(g_1006, "g_1006", print_hash_value);
    transparent_crc(g_1122.f0, "g_1122.f0", print_hash_value);
    transparent_crc(g_1160.f0, "g_1160.f0", print_hash_value);
    transparent_crc(g_1160.f1, "g_1160.f1", print_hash_value);
    transparent_crc(g_1160.f2, "g_1160.f2", print_hash_value);
    transparent_crc(g_1160.f3, "g_1160.f3", print_hash_value);
    transparent_crc(g_1160.f4, "g_1160.f4", print_hash_value);
    transparent_crc(g_1160.f5, "g_1160.f5", print_hash_value);
    transparent_crc(g_1160.f6, "g_1160.f6", print_hash_value);
    transparent_crc(g_1162.f0, "g_1162.f0", print_hash_value);
    transparent_crc(g_1201.f0, "g_1201.f0", print_hash_value);
    transparent_crc(g_1201.f1, "g_1201.f1", print_hash_value);
    transparent_crc(g_1201.f2, "g_1201.f2", print_hash_value);
    transparent_crc(g_1201.f3, "g_1201.f3", print_hash_value);
    transparent_crc(g_1201.f4, "g_1201.f4", print_hash_value);
    transparent_crc(g_1201.f5, "g_1201.f5", print_hash_value);
    transparent_crc(g_1201.f6, "g_1201.f6", print_hash_value);
    transparent_crc(g_1272.f0, "g_1272.f0", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_1293[i][j], "g_1293[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1308[i], "g_1308[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1315.f0, "g_1315.f0", print_hash_value);
    transparent_crc(g_1379, "g_1379", print_hash_value);
    transparent_crc(g_1418.f0, "g_1418.f0", print_hash_value);
    transparent_crc(g_1418.f1, "g_1418.f1", print_hash_value);
    transparent_crc(g_1418.f2, "g_1418.f2", print_hash_value);
    transparent_crc(g_1418.f3, "g_1418.f3", print_hash_value);
    transparent_crc(g_1418.f4, "g_1418.f4", print_hash_value);
    transparent_crc(g_1418.f5, "g_1418.f5", print_hash_value);
    transparent_crc(g_1418.f6, "g_1418.f6", print_hash_value);
    transparent_crc(g_1471, "g_1471", print_hash_value);
    transparent_crc(g_1496.f0, "g_1496.f0", print_hash_value);
    transparent_crc(g_1496.f1, "g_1496.f1", print_hash_value);
    transparent_crc(g_1496.f2, "g_1496.f2", print_hash_value);
    transparent_crc(g_1496.f3, "g_1496.f3", print_hash_value);
    transparent_crc(g_1496.f4, "g_1496.f4", print_hash_value);
    transparent_crc(g_1496.f5, "g_1496.f5", print_hash_value);
    transparent_crc(g_1496.f6, "g_1496.f6", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1580[i], "g_1580[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1615.f0, "g_1615.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc_bytes(&g_1646[i], sizeof(g_1646[i]), "g_1646[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1699, "g_1699", print_hash_value);
    transparent_crc(g_1700, "g_1700", print_hash_value);
    transparent_crc(g_1701, "g_1701", print_hash_value);
    transparent_crc(g_1702, "g_1702", print_hash_value);
    transparent_crc(g_1709.f0, "g_1709.f0", print_hash_value);
    transparent_crc(g_1796, "g_1796", print_hash_value);
    transparent_crc(g_1822.f0, "g_1822.f0", print_hash_value);
    transparent_crc(g_1822.f1, "g_1822.f1", print_hash_value);
    transparent_crc(g_1822.f2, "g_1822.f2", print_hash_value);
    transparent_crc(g_1822.f3, "g_1822.f3", print_hash_value);
    transparent_crc(g_1822.f4, "g_1822.f4", print_hash_value);
    transparent_crc(g_1822.f5, "g_1822.f5", print_hash_value);
    transparent_crc(g_1822.f6, "g_1822.f6", print_hash_value);
    transparent_crc(g_1850.f0, "g_1850.f0", print_hash_value);
    transparent_crc(g_1948, "g_1948", print_hash_value);
    transparent_crc_bytes (&g_1971, sizeof(g_1971), "g_1971", print_hash_value);
    transparent_crc(g_2014.f0, "g_2014.f0", print_hash_value);
    transparent_crc(g_2020.f0, "g_2020.f0", print_hash_value);
    transparent_crc(g_2020.f1, "g_2020.f1", print_hash_value);
    transparent_crc(g_2020.f2, "g_2020.f2", print_hash_value);
    transparent_crc(g_2020.f3, "g_2020.f3", print_hash_value);
    transparent_crc(g_2020.f4, "g_2020.f4", print_hash_value);
    transparent_crc(g_2020.f5, "g_2020.f5", print_hash_value);
    transparent_crc(g_2020.f6, "g_2020.f6", print_hash_value);
    transparent_crc(g_2132, "g_2132", print_hash_value);
    transparent_crc_bytes (&g_2168, sizeof(g_2168), "g_2168", print_hash_value);
    transparent_crc(g_2192, "g_2192", print_hash_value);
    transparent_crc(g_2202, "g_2202", print_hash_value);
    transparent_crc(g_2228, "g_2228", print_hash_value);
    transparent_crc_bytes (&g_2281, sizeof(g_2281), "g_2281", print_hash_value);
    transparent_crc(g_2330, "g_2330", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2389[i], "g_2389[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2392, "g_2392", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_2432[i][j][k], "g_2432[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_2525, sizeof(g_2525), "g_2525", print_hash_value);
    transparent_crc(g_2527.f0, "g_2527.f0", print_hash_value);
    transparent_crc(g_2527.f1, "g_2527.f1", print_hash_value);
    transparent_crc(g_2527.f2, "g_2527.f2", print_hash_value);
    transparent_crc(g_2527.f3, "g_2527.f3", print_hash_value);
    transparent_crc(g_2527.f4, "g_2527.f4", print_hash_value);
    transparent_crc(g_2527.f5, "g_2527.f5", print_hash_value);
    transparent_crc(g_2527.f6, "g_2527.f6", print_hash_value);
    transparent_crc(g_2559.f0, "g_2559.f0", print_hash_value);
    transparent_crc(g_2590, "g_2590", print_hash_value);
    transparent_crc(g_2638, "g_2638", print_hash_value);
    transparent_crc(g_2665, "g_2665", print_hash_value);
    transparent_crc(g_2677, "g_2677", print_hash_value);
    transparent_crc(g_2731.f0, "g_2731.f0", print_hash_value);
    transparent_crc(g_2757, "g_2757", print_hash_value);
    transparent_crc(g_2836, "g_2836", print_hash_value);
    transparent_crc(g_2839, "g_2839", print_hash_value);
    transparent_crc(g_3016.f0, "g_3016.f0", print_hash_value);
    transparent_crc(g_3016.f1, "g_3016.f1", print_hash_value);
    transparent_crc(g_3016.f2, "g_3016.f2", print_hash_value);
    transparent_crc(g_3016.f3, "g_3016.f3", print_hash_value);
    transparent_crc(g_3016.f4, "g_3016.f4", print_hash_value);
    transparent_crc(g_3016.f5, "g_3016.f5", print_hash_value);
    transparent_crc(g_3016.f6, "g_3016.f6", print_hash_value);
    transparent_crc(g_3028.f0, "g_3028.f0", print_hash_value);
    transparent_crc(g_3097, "g_3097", print_hash_value);
    transparent_crc(g_3127.f0, "g_3127.f0", print_hash_value);
    transparent_crc(g_3152, "g_3152", print_hash_value);
    transparent_crc(g_3215.f0, "g_3215.f0", print_hash_value);
    transparent_crc(g_3215.f1, "g_3215.f1", print_hash_value);
    transparent_crc(g_3215.f2, "g_3215.f2", print_hash_value);
    transparent_crc(g_3215.f3, "g_3215.f3", print_hash_value);
    transparent_crc(g_3215.f4, "g_3215.f4", print_hash_value);
    transparent_crc(g_3215.f5, "g_3215.f5", print_hash_value);
    transparent_crc(g_3215.f6, "g_3215.f6", print_hash_value);
    transparent_crc(g_3261.f0, "g_3261.f0", print_hash_value);
    transparent_crc(g_3266.f0, "g_3266.f0", print_hash_value);
    transparent_crc(g_3292.f0, "g_3292.f0", print_hash_value);
    transparent_crc(g_3292.f1, "g_3292.f1", print_hash_value);
    transparent_crc(g_3292.f2, "g_3292.f2", print_hash_value);
    transparent_crc(g_3292.f3, "g_3292.f3", print_hash_value);
    transparent_crc(g_3292.f4, "g_3292.f4", print_hash_value);
    transparent_crc(g_3292.f5, "g_3292.f5", print_hash_value);
    transparent_crc(g_3292.f6, "g_3292.f6", print_hash_value);
    transparent_crc_bytes (&g_3305, sizeof(g_3305), "g_3305", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_3446[i][j], "g_3446[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3502.f0, "g_3502.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_3524[i], "g_3524[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_3549.f0, "g_3549.f0", print_hash_value);
    transparent_crc(g_3554.f0, "g_3554.f0", print_hash_value);
    transparent_crc(g_3596.f0, "g_3596.f0", print_hash_value);
    transparent_crc(g_3640.f0, "g_3640.f0", print_hash_value);
    transparent_crc(g_3695.f0, "g_3695.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_3711[i][j][k].f0, "g_3711[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 865
   depth: 1, occurrence: 16
XXX total union variables: 21

XXX non-zero bitfields defined in structs: 7
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 3
XXX volatile bitfields defined in structs: 3
XXX structs with bitfields in the program: 36
breakdown:
   indirect level: 0, occurrence: 16
   indirect level: 1, occurrence: 12
   indirect level: 2, occurrence: 4
   indirect level: 3, occurrence: 4
XXX full-bitfields structs in the program: 16
breakdown:
   indirect level: 0, occurrence: 16
XXX times a bitfields struct's address is taken: 18
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 22
XXX times a single bitfield on LHS: 8
XXX times a single bitfield on RHS: 104

XXX max expression depth: 53
breakdown:
   depth: 1, occurrence: 388
   depth: 2, occurrence: 84
   depth: 3, occurrence: 9
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 8, occurrence: 2
   depth: 9, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 5
   depth: 18, occurrence: 5
   depth: 19, occurrence: 6
   depth: 20, occurrence: 5
   depth: 21, occurrence: 12
   depth: 22, occurrence: 3
   depth: 23, occurrence: 3
   depth: 24, occurrence: 6
   depth: 25, occurrence: 3
   depth: 26, occurrence: 7
   depth: 27, occurrence: 1
   depth: 28, occurrence: 6
   depth: 29, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 5
   depth: 32, occurrence: 4
   depth: 33, occurrence: 2
   depth: 34, occurrence: 2
   depth: 35, occurrence: 1
   depth: 39, occurrence: 1
   depth: 40, occurrence: 2
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1
   depth: 53, occurrence: 1

XXX total number of pointers: 852

XXX times a variable address is taken: 1994
XXX times a pointer is dereferenced on RHS: 437
breakdown:
   depth: 1, occurrence: 309
   depth: 2, occurrence: 83
   depth: 3, occurrence: 36
   depth: 4, occurrence: 7
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 447
breakdown:
   depth: 1, occurrence: 393
   depth: 2, occurrence: 29
   depth: 3, occurrence: 18
   depth: 4, occurrence: 5
   depth: 5, occurrence: 2
XXX times a pointer is compared with null: 68
XXX times a pointer is compared with address of another variable: 19
XXX times a pointer is compared with another pointer: 29
XXX times a pointer is qualified to be dereferenced: 10823

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2426
   level: 2, occurrence: 645
   level: 3, occurrence: 313
   level: 4, occurrence: 88
   level: 5, occurrence: 25
XXX number of pointers point to pointers: 365
XXX number of pointers point to scalars: 456
XXX number of pointers point to structs: 14
XXX percent of pointers has null in alias set: 31.1
XXX average alias set size: 1.37

XXX times a non-volatile is read: 2946
XXX times a non-volatile is write: 1338
XXX times a volatile is read: 188
XXX    times read thru a pointer: 34
XXX times a volatile is write: 81
XXX    times written thru a pointer: 20
XXX times a volatile is available for access: 7.59e+03
XXX percentage of non-volatile access: 94.1

XXX forward jumps: 3
XXX backward jumps: 10

XXX stmts: 379
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 36
   depth: 2, occurrence: 49
   depth: 3, occurrence: 65
   depth: 4, occurrence: 90
   depth: 5, occurrence: 107

XXX percentage a fresh-made variable is used: 16.4
XXX percentage an existing variable is used: 83.6
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


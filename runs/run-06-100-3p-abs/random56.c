/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      503435991
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   signed f0 : 10;
   unsigned f1 : 25;
   volatile unsigned f2 : 24;
   const signed f3 : 3;
   unsigned f4 : 8;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S1 {
   const signed f0 : 5;
   signed f1 : 11;
   volatile signed f2 : 14;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S2 {
   uint8_t  f0;
   volatile uint32_t  f1;
   int8_t  f2;
   volatile int16_t  f3;
   const uint16_t  f4;
   volatile int8_t  f5;
   int16_t  f6;
   const uint32_t  f7;
   const struct S1  f8;
   const volatile int32_t  f9;
};
#pragma pack(pop)

union U3 {
   int8_t  f0;
   signed f1 : 17;
};

union U4 {
   int32_t  f0;
   volatile int8_t  f1;
   unsigned f2 : 18;
   volatile uint8_t  f3;
};

union U5 {
   uint32_t  f0;
   volatile int8_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static uint64_t g_3 = 18446744073709551615UL;
static float g_16[3] = {0x1.9820B2p+50,0x1.9820B2p+50,0x1.9820B2p+50};
static int32_t * volatile g_19 = (void*)0;/* VOLATILE GLOBAL g_19 */
static int32_t g_21 = 0xA0CEC519L;
static int32_t * volatile g_20 = &g_21;/* VOLATILE GLOBAL g_20 */
static int32_t g_51 = 0x75DAAFE4L;
static uint16_t g_74[3] = {65535UL,65535UL,65535UL};
static uint16_t g_75[3][9] = {{4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL},{4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL},{4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL,4UL}};
static uint16_t g_81 = 0UL;
static uint8_t g_85 = 0x5EL;
static uint32_t g_91[8][10][3] = {{{4294967286UL,0xA13E652FL,4294967286UL},{4294967295UL,0xBBFFD320L,0UL},{0UL,0UL,0x0BDBE799L},{0UL,0xBBFFD320L,0xBBFFD320L},{0x0BDBE799L,0UL,0xA13E652FL},{0UL,4294967295UL,0UL},{0x0BDBE799L,4294967286UL,0xA13E652FL},{0xBBFFD320L,0xBBFFD320L,0UL},{0xB67A03D6L,4294967286UL,4294967286UL},{0UL,4294967295UL,1UL}},{{0xB67A03D6L,0UL,0xB67A03D6L},{0xBBFFD320L,0UL,1UL},{0x0BDBE799L,0x0BDBE799L,4294967286UL},{0UL,0UL,0UL},{4294967286UL,0UL,0xA13E652FL},{0UL,4294967295UL,0UL},{0x0BDBE799L,4294967286UL,0xA13E652FL},{0xBBFFD320L,0xBBFFD320L,0UL},{0xB67A03D6L,4294967286UL,4294967286UL},{0UL,4294967295UL,1UL}},{{0xB67A03D6L,0UL,0xB67A03D6L},{0xBBFFD320L,0UL,1UL},{0x0BDBE799L,0x0BDBE799L,4294967286UL},{0UL,0UL,0UL},{4294967286UL,0UL,0xA13E652FL},{0UL,4294967295UL,0UL},{0x0BDBE799L,4294967286UL,0xA13E652FL},{0xBBFFD320L,0xBBFFD320L,0UL},{0xB67A03D6L,4294967286UL,4294967286UL},{0UL,4294967295UL,1UL}},{{0xB67A03D6L,0UL,0xB67A03D6L},{0xBBFFD320L,0UL,1UL},{0x0BDBE799L,0x0BDBE799L,4294967286UL},{0UL,0UL,0UL},{4294967286UL,0UL,0xA13E652FL},{0UL,4294967295UL,0UL},{0x0BDBE799L,4294967286UL,0xA13E652FL},{0xBBFFD320L,0xBBFFD320L,0UL},{0xB67A03D6L,4294967286UL,4294967286UL},{0UL,4294967295UL,1UL}},{{0xB67A03D6L,0UL,0xB67A03D6L},{0xBBFFD320L,0UL,1UL},{0x0BDBE799L,0x0BDBE799L,4294967286UL},{0UL,0UL,0UL},{4294967286UL,0UL,0xA13E652FL},{0UL,4294967295UL,0UL},{0x0BDBE799L,4294967286UL,0xA13E652FL},{0xBBFFD320L,0xBBFFD320L,0UL},{0xB67A03D6L,4294967286UL,4294967286UL},{0UL,4294967295UL,1UL}},{{0xB67A03D6L,0UL,0xB67A03D6L},{0xBBFFD320L,0UL,1UL},{0x0BDBE799L,0x0BDBE799L,4294967286UL},{0UL,0UL,0UL},{4294967286UL,0UL,0xA13E652FL},{0UL,4294967295UL,0UL},{0x0BDBE799L,4294967286UL,0xA13E652FL},{0xBBFFD320L,0xBBFFD320L,0UL},{0xB67A03D6L,4294967286UL,4294967286UL},{0UL,4294967295UL,1UL}},{{0xB67A03D6L,0UL,0xB67A03D6L},{0xBBFFD320L,0UL,1UL},{0x0BDBE799L,0x0BDBE799L,4294967286UL},{0UL,0UL,0UL},{4294967286UL,0UL,0xA13E652FL},{0UL,4294967295UL,0UL},{0x0BDBE799L,4294967286UL,0xA13E652FL},{0xBBFFD320L,0xBBFFD320L,0UL},{0xA13E652FL,0xB67A03D6L,0xB67A03D6L},{0UL,0xBBFFD320L,4294967295UL}},{{0xA13E652FL,0x0BDBE799L,0xA13E652FL},{0UL,0UL,4294967295UL},{4294967286UL,4294967286UL,0xB67A03D6L},{1UL,0UL,0UL},{0xB67A03D6L,0x0BDBE799L,0UL},{1UL,0xBBFFD320L,1UL},{4294967286UL,0xB67A03D6L,0UL},{0UL,0UL,0UL},{0xA13E652FL,0xB67A03D6L,0xB67A03D6L},{0UL,0xBBFFD320L,4294967295UL}}};
static int16_t g_95 = (-2L);
static uint8_t *g_112 = (void*)0;
static const uint8_t g_115 = 0x6AL;
static const uint8_t g_117 = 0x0AL;
static const uint8_t g_119 = 0xC2L;
static float g_121 = (-0x10.6p+1);
static uint32_t g_143[5][3] = {{1UL,0x07892551L,1UL},{0xEBC02147L,0x57C9A115L,4294967291UL},{0xEBC02147L,0xEBC02147L,0x57C9A115L},{1UL,0x57C9A115L,0x57C9A115L},{0x57C9A115L,0x07892551L,4294967291UL}};
static uint64_t g_147 = 18446744073709551615UL;
static int32_t g_153 = 0L;
static uint64_t * volatile g_182 = &g_3;/* VOLATILE GLOBAL g_182 */
static uint64_t * volatile *g_181 = &g_182;
static uint32_t g_212[1][5][4] = {{{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL}}};
static uint32_t g_214[1] = {0UL};
static int32_t *g_232 = &g_153;
static int32_t * volatile *g_231[9][4][5] = {{{&g_232,&g_232,&g_232,(void*)0,(void*)0},{&g_232,&g_232,&g_232,(void*)0,(void*)0},{&g_232,&g_232,(void*)0,&g_232,(void*)0},{&g_232,&g_232,&g_232,&g_232,&g_232}},{{&g_232,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,(void*)0,&g_232},{&g_232,&g_232,&g_232,(void*)0,&g_232},{&g_232,&g_232,&g_232,&g_232,(void*)0}},{{&g_232,&g_232,(void*)0,&g_232,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,(void*)0,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_232}},{{&g_232,&g_232,&g_232,(void*)0,&g_232},{&g_232,&g_232,(void*)0,&g_232,(void*)0},{&g_232,&g_232,(void*)0,(void*)0,&g_232},{&g_232,&g_232,(void*)0,&g_232,&g_232}},{{&g_232,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,(void*)0,(void*)0},{&g_232,&g_232,&g_232,(void*)0,(void*)0}},{{&g_232,&g_232,(void*)0,&g_232,(void*)0},{&g_232,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,(void*)0,&g_232}},{{&g_232,&g_232,&g_232,&g_232,&g_232},{(void*)0,&g_232,(void*)0,(void*)0,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_232},{(void*)0,&g_232,&g_232,&g_232,&g_232}},{{&g_232,(void*)0,(void*)0,(void*)0,&g_232},{(void*)0,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_232},{&g_232,&g_232,&g_232,&g_232,&g_232}},{{&g_232,&g_232,&g_232,(void*)0,&g_232},{(void*)0,(void*)0,&g_232,&g_232,&g_232},{&g_232,(void*)0,&g_232,&g_232,&g_232},{&g_232,(void*)0,&g_232,(void*)0,&g_232}}};
static int16_t g_234[7][4] = {{0xC598L,(-1L),6L,0xAA89L},{0x73B7L,0x25BEL,0x18ECL,0x25BEL},{0x25BEL,6L,0xC598L,0x25BEL},{0xC598L,0x25BEL,0xAA89L,0xAA89L},{(-1L),(-1L),0x18ECL,0x73B7L},{(-1L),6L,0xAA89L,(-1L)},{0xC598L,0x73B7L,0xC598L,0xAA89L}};
static uint8_t **g_276[3] = {&g_112,&g_112,&g_112};
static struct S0 g_334 = {-6,4375,2306,-0,13};/* VOLATILE GLOBAL g_334 */
static union U4 g_366 = {0x7C95A084L};/* VOLATILE GLOBAL g_366 */
static float *g_400 = (void*)0;
static uint64_t *g_427 = &g_3;
static uint64_t ** volatile g_426[1] = {&g_427};
static uint64_t ** volatile *g_425[5] = {&g_426[0],&g_426[0],&g_426[0],&g_426[0],&g_426[0]};
static uint64_t ** volatile **g_424 = &g_425[2];
static int64_t g_463 = 2L;
static volatile struct S0 * volatile g_466 = (void*)0;/* VOLATILE GLOBAL g_466 */
static volatile struct S0 * volatile *g_465 = &g_466;
static int8_t g_502 = 0x95L;
static uint8_t g_510[8] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static int16_t g_512[7][7][5] = {{{0xCDB9L,0L,1L,0x4133L,0xFF07L},{0L,1L,(-7L),1L,1L},{7L,0L,0x5B89L,0x3B45L,0xE722L},{(-9L),0L,1L,0x0C2AL,(-7L)},{(-1L),1L,2L,1L,(-10L)},{0x42D0L,0L,0x59C0L,1L,0xFF07L},{1L,0L,(-7L),(-1L),5L}},{{7L,1L,(-10L),0x3B45L,(-7L)},{0xD717L,0L,(-1L),0x97A0L,(-7L)},{1L,0L,2L,0L,1L},{0x42D0L,1L,1L,1L,1L},{0L,0L,0xB2CBL,1L,5L},{9L,0L,(-10L),(-1L),0xE722L},{0xD717L,1L,1L,0x97A0L,2L}},{{(-1L),0L,0x0494L,1L,1L},{0xCDB9L,0L,1L,0x4133L,0xFF07L},{0L,1L,(-7L),1L,(-1L)},{1L,(-7L),(-8L),(-1L),(-1L)},{0x0494L,(-7L),(-1L),1L,0x7C0FL},{1L,0xB2CBL,0x1667L,0xC837L,(-3L)},{(-10L),(-7L),(-1L),0x8E8EL,0L}},{{0xB2CBL,(-7L),0xFD26L,0x2B55L,0xF802L},{1L,0xB2CBL,(-3L),(-1L),0xFD26L},{2L,(-7L),0x01E2L,0L,0x7C0FL},{(-1L),(-7L),0x1667L,0xCF5AL,0x4364L},{(-10L),0xB2CBL,(-5L),0x8E8EL,(-5L)},{(-7L),(-7L),0L,0x8FC9L,0xF802L},{0x59C0L,(-7L),(-3L),0x384BL,(-1L)}},{{2L,0xB2CBL,(-1L),0L,0x1667L},{1L,(-7L),(-1L),0xC837L,0x4364L},{0x5B89L,(-7L),(-5L),0L,0L},{(-7L),0xB2CBL,0xFD26L,0x8FC9L,(-1L)},{1L,(-7L),(-8L),(-1L),(-1L)},{0x0494L,(-7L),(-1L),1L,0x7C0FL},{1L,0xB2CBL,0x1667L,0xC837L,(-3L)}},{{(-10L),(-7L),(-1L),0x8E8EL,0L},{0xB2CBL,(-7L),0xFD26L,0x2B55L,0xF802L},{1L,0xB2CBL,(-3L),(-1L),0xFD26L},{2L,(-7L),0x01E2L,0L,0x7C0FL},{(-1L),(-7L),0x1667L,0xCF5AL,0x4364L},{(-10L),0xB2CBL,(-5L),0x8E8EL,(-5L)},{(-7L),(-7L),0L,0x8FC9L,0xF802L}},{{0x59C0L,(-7L),(-3L),0x384BL,(-1L)},{2L,0xB2CBL,(-1L),0L,0x1667L},{1L,(-7L),(-1L),0xC837L,0x4364L},{0x5B89L,(-7L),(-5L),0L,0L},{(-7L),0xB2CBL,0xFD26L,0x8FC9L,(-1L)},{1L,(-7L),(-8L),(-1L),(-1L)},{0x0494L,(-7L),(-1L),1L,0x7C0FL}}};
static int64_t g_513[3][5][10] = {{{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)}},{{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)}},{{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)},{0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L),0xB3628DB0124F788FLL,(-4L)}}};
static struct S0 g_523 = {-3,1841,3131,0,9};/* VOLATILE GLOBAL g_523 */
static const union U4 g_535 = {1L};/* VOLATILE GLOBAL g_535 */
static const union U4 *g_534 = &g_535;
static const union U4 g_548 = {0xDCFD291AL};/* VOLATILE GLOBAL g_548 */
static int8_t g_565[5][2][6] = {{{(-1L),(-1L),0xB7L,1L,0xB7L,(-1L)},{0xB7L,8L,1L,1L,8L,0xB7L}},{{(-1L),0xB7L,1L,0xB7L,(-1L),(-1L)},{0x1CL,0xB7L,0xB7L,0x1CL,8L,0x1CL}},{{0x1CL,8L,0x1CL,0xB7L,0xB7L,0x1CL},{(-1L),(-1L),0xB7L,1L,0xB7L,(-1L)}},{{0xB7L,8L,1L,1L,8L,0xB7L},{(-1L),0xB7L,1L,0xB7L,(-1L),(-1L)}},{{0x1CL,0xB7L,0xB7L,0x1CL,8L,0x1CL},{0x1CL,8L,0x1CL,0xB7L,0xB7L,0x1CL}}};
static struct S1 g_576 = {-4,-0,61};/* VOLATILE GLOBAL g_576 */
static struct S1 *g_575 = &g_576;
static struct S0 g_598 = {26,2782,3873,-1,9};/* VOLATILE GLOBAL g_598 */
static struct S0 *g_597[1] = {&g_598};
static struct S0 * const *g_596[8] = {&g_597[0],&g_597[0],&g_597[0],&g_597[0],&g_597[0],&g_597[0],&g_597[0],&g_597[0]};
static struct S0 * const **g_595 = &g_596[7];
static union U3 g_607 = {0x2EL};
static float g_620[8][2] = {{0x4.5C51F5p+4,0x4.5C51F5p+4},{0x4.5C51F5p+4,0x4.5C51F5p+4},{0x4.5C51F5p+4,0x4.5C51F5p+4},{0x4.5C51F5p+4,0x4.5C51F5p+4},{0x4.5C51F5p+4,0x4.5C51F5p+4},{0x4.5C51F5p+4,0x4.5C51F5p+4},{0x4.5C51F5p+4,0x4.5C51F5p+4},{0x4.5C51F5p+4,0x4.5C51F5p+4}};
static int32_t **g_702 = &g_232;
static struct S0 **g_740[1][9] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static struct S0 ***g_739[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const int32_t *g_750 = &g_366.f0;
static struct S2 g_791[5][5] = {{{0x87L,0UL,1L,0x8DE1L,1UL,9L,-1L,0xBF46CE3DL,{3,-30,21},0x30812535L},{0xE3L,9UL,0x1CL,0x35E6L,0x8D18L,0xDFL,0xD374L,3UL,{-1,-41,-4},1L},{0x7EL,0xC50449DBL,0x32L,0xC74EL,0xCD45L,1L,1L,4294967287UL,{3,-34,36},1L},{0xE3L,9UL,0x1CL,0x35E6L,0x8D18L,0xDFL,0xD374L,3UL,{-1,-41,-4},1L},{0x87L,0UL,1L,0x8DE1L,1UL,9L,-1L,0xBF46CE3DL,{3,-30,21},0x30812535L}},{{0xBEL,0xDDE6DF0BL,0L,0x2DBCL,0x7B6CL,0xD3L,0x1AF8L,0x7646EF53L,{-3,20,27},0xBAE2D77DL},{0xE3L,9UL,0x1CL,0x35E6L,0x8D18L,0xDFL,0xD374L,3UL,{-1,-41,-4},1L},{1UL,0xB4AC6260L,0x2DL,0x5C84L,0x8D6AL,-2L,-3L,3UL,{1,16,24},0xD9A06AE4L},{0x87L,0UL,1L,0x8DE1L,1UL,9L,-1L,0xBF46CE3DL,{3,-30,21},0x30812535L},{1UL,0xB4AC6260L,0x2DL,0x5C84L,0x8D6AL,-2L,-3L,3UL,{1,16,24},0xD9A06AE4L}},{{1UL,0xB4AC6260L,0x2DL,0x5C84L,0x8D6AL,-2L,-3L,3UL,{1,16,24},0xD9A06AE4L},{1UL,0xB4AC6260L,0x2DL,0x5C84L,0x8D6AL,-2L,-3L,3UL,{1,16,24},0xD9A06AE4L},{0x7EL,0xC50449DBL,0x32L,0xC74EL,0xCD45L,1L,1L,4294967287UL,{3,-34,36},1L},{0x87L,0UL,1L,0x8DE1L,1UL,9L,-1L,0xBF46CE3DL,{3,-30,21},0x30812535L},{0x3EL,18446744073709551608UL,0x4DL,-3L,1UL,0x46L,1L,0x44D2B7A4L,{-1,3,90},-1L}},{{0xE3L,9UL,0x1CL,0x35E6L,0x8D18L,0xDFL,0xD374L,3UL,{-1,-41,-4},1L},{0xBEL,0xDDE6DF0BL,0L,0x2DBCL,0x7B6CL,0xD3L,0x1AF8L,0x7646EF53L,{-3,20,27},0xBAE2D77DL},{0xBEL,0xDDE6DF0BL,0L,0x2DBCL,0x7B6CL,0xD3L,0x1AF8L,0x7646EF53L,{-3,20,27},0xBAE2D77DL},{0xE3L,9UL,0x1CL,0x35E6L,0x8D18L,0xDFL,0xD374L,3UL,{-1,-41,-4},1L},{1UL,0xB4AC6260L,0x2DL,0x5C84L,0x8D6AL,-2L,-3L,3UL,{1,16,24},0xD9A06AE4L}},{{0xE3L,9UL,0x1CL,0x35E6L,0x8D18L,0xDFL,0xD374L,3UL,{-1,-41,-4},1L},{0x87L,0UL,1L,0x8DE1L,1UL,9L,-1L,0xBF46CE3DL,{3,-30,21},0x30812535L},{0xC3L,0x82C17991L,2L,0x19BBL,65531UL,0x9FL,0x529EL,1UL,{4,19,-89},0x53DB8C84L},{0xC3L,0x82C17991L,2L,0x19BBL,65531UL,0x9FL,0x529EL,1UL,{4,19,-89},0x53DB8C84L},{0x87L,0UL,1L,0x8DE1L,1UL,9L,-1L,0xBF46CE3DL,{3,-30,21},0x30812535L}}};
static struct S2 *g_790 = &g_791[0][4];
static struct S2 g_793[1][1] = {{{1UL,0x546B1C3DL,0L,5L,1UL,0L,0xF4E4L,0x5037C3EDL,{-0,-30,14},0L}}};
static struct S2 g_794 = {1UL,18446744073709551606UL,6L,0L,0xE9C7L,0x63L,-1L,0x33C1DCF9L,{3,23,-124},1L};/* VOLATILE GLOBAL g_794 */
static volatile uint32_t g_817[4][7] = {{0UL,0x2AF29EF9L,1UL,0xBB06526FL,1UL,0x2AF29EF9L,0UL},{0x3512A898L,0x9DAADF07L,4294967295UL,0x78FD0F05L,4294967295UL,0x9DAADF07L,0x3512A898L},{0UL,0x2AF29EF9L,1UL,0xBB06526FL,1UL,0x2AF29EF9L,0UL},{0x3512A898L,0x9DAADF07L,4294967295UL,0x78FD0F05L,4294967295UL,0x9DAADF07L,0x3512A898L}};
static volatile uint32_t *g_816 = &g_817[2][5];
static volatile uint32_t * volatile *g_815 = &g_816;
static const int16_t g_835 = 0xCC1CL;
static uint64_t **g_870 = &g_427;
static int32_t g_923 = 5L;
static volatile union U5 g_930 = {0x2CBDDF83L};/* VOLATILE GLOBAL g_930 */
static volatile union U5 *g_929 = &g_930;
static union U4 g_1018[9][8] = {{{0x4DA6119CL},{1L},{1L},{0x4DA6119CL},{0xAC314C1FL},{0x4DA6119CL},{1L},{1L}},{{1L},{0xAC314C1FL},{-1L},{-1L},{0xAC314C1FL},{1L},{0xAC314C1FL},{-1L}},{{0x4DA6119CL},{0xAC314C1FL},{0x4DA6119CL},{1L},{1L},{0x4DA6119CL},{0xAC314C1FL},{0x4DA6119CL}},{{0x4871251BL},{1L},{-1L},{1L},{0x4871251BL},{0x4871251BL},{1L},{-1L}},{{0x4871251BL},{0x4871251BL},{1L},{-1L},{1L},{0x4871251BL},{0x4871251BL},{1L}},{{0x4DA6119CL},{1L},{1L},{0x4DA6119CL},{0xAC314C1FL},{0x4DA6119CL},{1L},{1L}},{{1L},{0xAC314C1FL},{-1L},{-1L},{0xAC314C1FL},{1L},{0xAC314C1FL},{-1L}},{{0x4DA6119CL},{0xAC314C1FL},{0x4DA6119CL},{1L},{1L},{0x4DA6119CL},{0xAC314C1FL},{0x4DA6119CL}},{{0x4871251BL},{1L},{-1L},{1L},{0x4871251BL},{0x4871251BL},{1L},{-1L}}};
static uint64_t g_1034 = 0x26CD44A0D893AA34LL;
static union U3 g_1049 = {-8L};
static struct S0 g_1092 = {3,1540,1941,1,0};/* VOLATILE GLOBAL g_1092 */
static struct S0 g_1093 = {31,2073,3102,1,10};/* VOLATILE GLOBAL g_1093 */
static uint64_t g_1141 = 1UL;
static float g_1176 = 0x6.280D61p+82;
static struct S0 g_1222 = {-17,5649,2668,0,11};/* VOLATILE GLOBAL g_1222 */
static volatile struct S1 g_1230 = {0,-14,89};/* VOLATILE GLOBAL g_1230 */
static volatile union U4 g_1247 = {0xE5A34D66L};/* VOLATILE GLOBAL g_1247 */
static float * volatile g_1266[5][4][2] = {{{&g_16[2],&g_121},{&g_16[2],&g_1176},{&g_620[2][1],&g_121},{&g_1176,&g_620[2][0]}},{{&g_620[6][0],&g_1176},{&g_121,&g_16[2]},{&g_16[2],&g_620[6][1]},{&g_620[6][0],&g_121}},{{&g_620[6][1],&g_121},{&g_620[6][0],&g_620[6][1]},{&g_16[2],&g_16[2]},{&g_121,&g_1176}},{{&g_620[6][0],&g_620[2][0]},{&g_1176,&g_121},{&g_620[2][1],&g_1176},{&g_16[2],&g_121}},{{&g_16[2],&g_1176},{&g_620[2][1],&g_121},{&g_620[2][1],&g_121},{&g_16[0],&g_620[2][1]}}};
static float * const  volatile g_1267 = &g_620[3][0];/* VOLATILE GLOBAL g_1267 */
static volatile uint32_t *g_1285[3][2][7] = {{{(void*)0,&g_793[0][0].f1,&g_794.f1,&g_794.f1,&g_793[0][0].f1,(void*)0,&g_794.f1},{&g_793[0][0].f1,&g_794.f1,&g_793[0][0].f1,&g_791[0][4].f1,&g_791[0][4].f1,&g_793[0][0].f1,&g_794.f1}},{{&g_793[0][0].f1,&g_794.f1,(void*)0,&g_793[0][0].f1,&g_794.f1,&g_794.f1,&g_793[0][0].f1},{(void*)0,&g_794.f1,(void*)0,&g_794.f1,&g_794.f1,&g_794.f1,&g_794.f1}},{{&g_794.f1,&g_793[0][0].f1,&g_793[0][0].f1,&g_793[0][0].f1,&g_794.f1,&g_793[0][0].f1,&g_791[0][4].f1},{&g_791[0][4].f1,&g_794.f1,&g_794.f1,&g_791[0][4].f1,&g_794.f1,&g_794.f1,&g_791[0][4].f1}}};
static volatile uint32_t **g_1284 = &g_1285[0][0][3];
static union U5 g_1286 = {4294967295UL};/* VOLATILE GLOBAL g_1286 */
static union U5 *g_1302 = (void*)0;
static union U5 ** volatile g_1301[7][5] = {{&g_1302,&g_1302,(void*)0,&g_1302,(void*)0},{(void*)0,(void*)0,&g_1302,&g_1302,(void*)0},{&g_1302,&g_1302,&g_1302,&g_1302,(void*)0},{&g_1302,&g_1302,&g_1302,&g_1302,&g_1302},{(void*)0,&g_1302,&g_1302,&g_1302,&g_1302},{&g_1302,(void*)0,&g_1302,&g_1302,&g_1302},{&g_1302,&g_1302,&g_1302,&g_1302,&g_1302}};
static int32_t ** const  volatile g_1305 = &g_232;/* VOLATILE GLOBAL g_1305 */
static int32_t g_1328 = 5L;
static const volatile struct S0 g_1426 = {-16,440,3929,1,4};/* VOLATILE GLOBAL g_1426 */
static uint32_t g_1464 = 0x2C54C7AEL;
static volatile int32_t g_1473[2][10] = {{0x9857B4B8L,0x05AA2663L,0x05AA2663L,0x9857B4B8L,0x05AA2663L,0x05AA2663L,0x9857B4B8L,0x05AA2663L,0x05AA2663L,0x9857B4B8L},{0x05AA2663L,0x9857B4B8L,0x05AA2663L,0x05AA2663L,0x9857B4B8L,0x05AA2663L,0x05AA2663L,0x9857B4B8L,0x05AA2663L,0x05AA2663L}};
static struct S2 g_1532 = {0x76L,0UL,1L,-6L,0UL,0x7EL,-1L,0x315EDF17L,{1,14,-17},0xCC848BE6L};/* VOLATILE GLOBAL g_1532 */
static union U4 g_1534[1][9][7] = {{{{3L},{0L},{7L},{0L},{3L},{0L},{7L}},{{0L},{0x5AF284CEL},{0x22299625L},{0x22299625L},{0x5AF284CEL},{0L},{-1L}},{{0x037B1BE1L},{-1L},{0x037B1BE1L},{0L},{0x037B1BE1L},{-1L},{0x037B1BE1L}},{{0L},{0x22299625L},{-1L},{0x5AF284CEL},{0x5AF284CEL},{-1L},{0x22299625L}},{{3L},{-1L},{7L},{-1L},{3L},{-1L},{7L}},{{0x5AF284CEL},{0x5AF284CEL},{-1L},{0x22299625L},{0L},{0L},{0x22299625L}},{{0x037B1BE1L},{0L},{0x037B1BE1L},{-1L},{0x037B1BE1L},{0L},{0x037B1BE1L}},{{0x5AF284CEL},{0x22299625L},{0x22299625L},{0x5AF284CEL},{0L},{-1L},{-1L}},{{3L},{0L},{7L},{0L},{3L},{0L},{7L}}}};
static int32_t g_1538[1] = {1L};
static uint8_t g_1549 = 0x04L;
static struct S1 g_1551 = {4,-3,-57};/* VOLATILE GLOBAL g_1551 */
static float g_1594 = 0x8.5p+1;
static uint32_t * const g_1598 = &g_1464;
static uint32_t * const *g_1597 = &g_1598;
static uint32_t g_1602 = 18446744073709551610UL;
static volatile union U5 g_1633 = {1UL};/* VOLATILE GLOBAL g_1633 */
static int32_t *g_1659 = &g_21;
static int32_t ** volatile g_1658 = &g_1659;/* VOLATILE GLOBAL g_1658 */
static const union U4 **g_1696[10] = {&g_534,&g_534,&g_534,&g_534,&g_534,&g_534,&g_534,&g_534,&g_534,&g_534};
static uint32_t *g_1703 = &g_212[0][0][2];
static volatile struct S1 g_1706 = {-1,-21,74};/* VOLATILE GLOBAL g_1706 */
static int8_t g_1725 = 0x45L;
static struct S2 g_1747 = {1UL,0x7794EC09L,0xB4L,5L,0x14F8L,0x72L,0x7C40L,0xABB44851L,{1,-5,-64},4L};/* VOLATILE GLOBAL g_1747 */
static volatile float g_1760 = 0xE.08D9F7p+38;/* VOLATILE GLOBAL g_1760 */
static float * volatile g_1782 = &g_620[3][1];/* VOLATILE GLOBAL g_1782 */
static volatile struct S0 g_1819 = {5,3121,166,0,15};/* VOLATILE GLOBAL g_1819 */
static volatile union U4 g_1829[3] = {{1L},{1L},{1L}};
static int8_t g_1832 = 0x1BL;
static volatile uint16_t g_1889 = 3UL;/* VOLATILE GLOBAL g_1889 */
static const volatile uint16_t * const g_1888 = &g_1889;
static const volatile uint16_t * const  volatile * volatile g_1887 = &g_1888;/* VOLATILE GLOBAL g_1887 */
static struct S0 * const **g_1930 = &g_596[7];
static float g_1932 = 0x1.3p-1;
static volatile union U5 g_1956 = {4294967289UL};/* VOLATILE GLOBAL g_1956 */
static uint32_t **g_2055 = &g_1703;
static uint32_t ** volatile *g_2054 = &g_2055;
static uint32_t ** volatile **g_2053 = &g_2054;
static volatile struct S0 g_2059 = {-21,1371,2575,0,1};/* VOLATILE GLOBAL g_2059 */
static float g_2085 = 0xF.0B8220p-73;
static struct S0 * const ***g_2089 = &g_595;
static struct S0 * const **** volatile g_2088[1] = {&g_2089};
static struct S0 * const **** volatile g_2090 = &g_2089;/* VOLATILE GLOBAL g_2090 */
static struct S0 g_2113 = {19,4541,1548,-1,7};/* VOLATILE GLOBAL g_2113 */
static uint32_t ** const * const * const g_2121 = (void*)0;
static uint32_t ** const * const * const *g_2120[1] = {&g_2121};
static uint32_t g_2173 = 2UL;
static struct S0 g_2269 = {27,1690,686,0,13};/* VOLATILE GLOBAL g_2269 */
static float * volatile g_2295 = &g_121;/* VOLATILE GLOBAL g_2295 */
static union U4 g_2308 = {-1L};/* VOLATILE GLOBAL g_2308 */
static int32_t ***g_2319[4][3] = {{&g_702,&g_702,&g_702},{&g_702,&g_702,&g_702},{&g_702,&g_702,&g_702},{&g_702,&g_702,&g_702}};
static float * volatile g_2320 = &g_2085;/* VOLATILE GLOBAL g_2320 */
static const volatile union U5 g_2322 = {0x2ED3CBA9L};/* VOLATILE GLOBAL g_2322 */
static uint64_t ***g_2341[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
static uint64_t ****g_2340 = &g_2341[1];
static const uint64_t *g_2347 = &g_1034;
static const uint64_t **g_2346 = &g_2347;
static const uint64_t ***g_2345 = &g_2346;
static const uint64_t ****g_2344 = &g_2345;
static volatile uint32_t **g_2371 = &g_816;
static volatile uint32_t *** volatile g_2370 = &g_2371;/* VOLATILE GLOBAL g_2370 */
static volatile uint32_t *** volatile *g_2369[6] = {&g_2370,&g_2370,&g_2370,&g_2370,&g_2370,&g_2370};
static volatile uint32_t *** volatile **g_2368 = &g_2369[1];
static const volatile struct S1 g_2417 = {1,-11,112};/* VOLATILE GLOBAL g_2417 */
static struct S1 g_2432 = {1,-33,76};/* VOLATILE GLOBAL g_2432 */
static union U4 g_2446 = {0x3C5A4A10L};/* VOLATILE GLOBAL g_2446 */
static volatile struct S2 g_2447 = {246UL,0UL,1L,0xF2A6L,0xDD33L,0x73L,1L,7UL,{-2,22,-48},0x711F64DBL};/* VOLATILE GLOBAL g_2447 */
static float * volatile g_2452 = &g_620[6][1];/* VOLATILE GLOBAL g_2452 */
static struct S2 g_2484 = {0xE5L,0x9FE0D49BL,4L,-8L,0UL,0x93L,0x6AE2L,2UL,{-1,26,-51},-7L};/* VOLATILE GLOBAL g_2484 */
static struct S0 **g_2523 = &g_597[0];
static struct S0 *** const g_2522[3] = {&g_2523,&g_2523,&g_2523};
static struct S0 *** const *g_2521 = &g_2522[1];
static struct S0 *** const **g_2520[1] = {&g_2521};
static union U4 g_2525 = {0x2275AA4AL};/* VOLATILE GLOBAL g_2525 */
static struct S2 g_2544 = {0x72L,0xA13F4F6DL,0x50L,0x9E51L,0x5207L,0x75L,0xAC69L,0x4AB7F0B0L,{4,44,32},1L};/* VOLATILE GLOBAL g_2544 */
static struct S0 g_2568 = {-26,1133,2136,-1,3};/* VOLATILE GLOBAL g_2568 */
static struct S2 g_2572 = {0x2EL,4UL,0xAFL,0xE9ABL,0x93D0L,1L,-1L,4294967287UL,{1,1,61},0L};/* VOLATILE GLOBAL g_2572 */
static union U4 g_2573 = {-5L};/* VOLATILE GLOBAL g_2573 */
static struct S0 ****g_2590 = &g_739[4];
static struct S0 *****g_2589 = &g_2590;
static volatile union U4 g_2624[10][8] = {{{0x7519545CL},{1L},{0x7519545CL},{0L},{0xF50FCE14L},{0xB46ECC85L},{0xCB5FD59EL},{1L}},{{0x296F131FL},{4L},{0L},{1L},{9L},{0xF50FCE14L},{0xF50FCE14L},{9L}},{{0x296F131FL},{0x39C4DF7FL},{0x39C4DF7FL},{0x296F131FL},{0xF50FCE14L},{1L},{0x4F0498DFL},{0xB46ECC85L}},{{0x7519545CL},{0xD4BDB758L},{1L},{0xCB5FD59EL},{0x39C4DF7FL},{1L},{0xB46ECC85L},{1L}},{{-1L},{0xD4BDB758L},{0x4F0498DFL},{0xD4BDB758L},{-1L},{1L},{0x296F131FL},{1L}},{{1L},{0x39C4DF7FL},{-1L},{1L},{0xDA25B2F4L},{0xF50FCE14L},{0xD4BDB758L},{0xD4BDB758L}},{{0xB46ECC85L},{4L},{-1L},{-1L},{4L},{0xB46ECC85L},{0x296F131FL},{0xDA25B2F4L}},{{0xDA25B2F4L},{1L},{0x4F0498DFL},{1L},{0xD4BDB758L},{1L},{0xB46ECC85L},{1L}},{{0x4F0498DFL},{0x7519545CL},{1L},{1L},{1L},{0x7519545CL},{0x4F0498DFL},{0xDA25B2F4L}},{{4L},{1L},{0x39C4DF7FL},{-1L},{1L},{0xDA25B2F4L},{0xF50FCE14L},{0xD4BDB758L}}};
static volatile union U4 g_2638 = {0L};/* VOLATILE GLOBAL g_2638 */
static struct S2 g_2663 = {0x73L,0UL,-1L,-6L,0xDCDBL,-7L,0xD0FBL,0x7C8B32E1L,{-2,14,48},0xFF1E0223L};/* VOLATILE GLOBAL g_2663 */
static union U5 ** volatile g_2682 = &g_1302;/* VOLATILE GLOBAL g_2682 */
static struct S2 g_2735[5][8] = {{{0x53L,0UL,-2L,0x5F51L,0x6DE6L,0x5FL,0L,4294967295UL,{1,-12,35},0L},{0x27L,0xAF65EB92L,-6L,-4L,2UL,0x35L,-9L,4294967289UL,{4,1,-27},-1L},{0x53L,0UL,-2L,0x5F51L,0x6DE6L,0x5FL,0L,4294967295UL,{1,-12,35},0L},{0xFEL,0xAA0D40FFL,0xE7L,8L,0xB54AL,0L,1L,4294967287UL,{1,18,52},0x3A7C934FL},{255UL,2UL,1L,0xB6FDL,1UL,-1L,0x8441L,0xADAD0AC4L,{2,24,11},0L},{250UL,0xAF52DA61L,-7L,0x690EL,0UL,0xC0L,0xB8D5L,0x67B2A463L,{3,0,112},0xE2631EB6L},{0UL,0x03703908L,0x17L,0xD083L,0xF4D4L,0x97L,0xF315L,4UL,{3,-14,-124},0x499AFB89L},{0x59L,18446744073709551611UL,1L,2L,0UL,9L,0x57CBL,0xBA795881L,{0,32,73},1L}},{{255UL,0x8E9F50F9L,1L,-1L,0x4A4CL,0xBBL,-9L,0x8DB97C4DL,{-2,6,-30},3L},{0x53L,0UL,-2L,0x5F51L,0x6DE6L,0x5FL,0L,4294967295UL,{1,-12,35},0L},{249UL,0x8C037808L,1L,0xE088L,0UL,0xBAL,0x99C5L,0x81C0D582L,{2,-3,39},-1L},{1UL,0x484886A2L,8L,0xFC17L,65532UL,0x0FL,-1L,0xED69E62CL,{2,30,124},0x2CDC7761L},{7UL,0x0034FCF9L,1L,1L,0xC4ADL,-1L,-2L,0x970B7FCAL,{3,-1,82},-3L},{247UL,0xC7FBDF4BL,1L,0xB96DL,0x1CFAL,0x5AL,1L,0UL,{-1,12,-4},0x84EC2A00L},{255UL,2UL,1L,0xB6FDL,1UL,-1L,0x8441L,0xADAD0AC4L,{2,24,11},0L},{255UL,2UL,1L,0xB6FDL,1UL,-1L,0x8441L,0xADAD0AC4L,{2,24,11},0L}},{{255UL,0x8E9F50F9L,1L,-1L,0x4A4CL,0xBBL,-9L,0x8DB97C4DL,{-2,6,-30},3L},{255UL,2UL,1L,0xB6FDL,1UL,-1L,0x8441L,0xADAD0AC4L,{2,24,11},0L},{0xEAL,18446744073709551611UL,-4L,0x60A4L,65535UL,-1L,0x281CL,4294967295UL,{-0,-5,64},5L},{0xEAL,18446744073709551611UL,-4L,0x60A4L,65535UL,-1L,0x281CL,4294967295UL,{-0,-5,64},5L},{255UL,2UL,1L,0xB6FDL,1UL,-1L,0x8441L,0xADAD0AC4L,{2,24,11},0L},{255UL,0x8E9F50F9L,1L,-1L,0x4A4CL,0xBBL,-9L,0x8DB97C4DL,{-2,6,-30},3L},{0x27L,0xAF65EB92L,-6L,-4L,2UL,0x35L,-9L,4294967289UL,{4,1,-27},-1L},{1UL,0xE2595FBEL,0x7CL,0xF118L,1UL,-6L,-1L,4294967295UL,{0,-0,-1},-9L}},{{0x53L,0UL,-2L,0x5F51L,0x6DE6L,0x5FL,0L,4294967295UL,{1,-12,35},0L},{3UL,18446744073709551615UL,0x87L,0xCA5FL,2UL,0x1EL,0xE06FL,4294967291UL,{-2,-32,10},-1L},{247UL,0xC7FBDF4BL,1L,0xB96DL,0x1CFAL,0x5AL,1L,0UL,{-1,12,-4},0x84EC2A00L},{0x27L,0xAF65EB92L,-6L,-4L,2UL,0x35L,-9L,4294967289UL,{4,1,-27},-1L},{249UL,0x8C037808L,1L,0xE088L,0UL,0xBAL,0x99C5L,0x81C0D582L,{2,-3,39},-1L},{0xB6L,0x45A12C6DL,8L,-9L,0x0ADBL,0L,0L,0x4E45F982L,{-1,40,46},1L},{0x59L,18446744073709551611UL,1L,2L,0UL,9L,0x57CBL,0xBA795881L,{0,32,73},1L},{4UL,0xFDA28336L,0x56L,0x05E9L,5UL,0x2CL,1L,0xF62A6155L,{-3,-30,75},0x03182638L}},{{7UL,0x0034FCF9L,1L,1L,0xC4ADL,-1L,-2L,0x970B7FCAL,{3,-1,82},-3L},{1UL,0xE2595FBEL,0x7CL,0xF118L,1UL,-6L,-1L,4294967295UL,{0,-0,-1},-9L},{0xB6L,0x45A12C6DL,8L,-9L,0x0ADBL,0L,0L,0x4E45F982L,{-1,40,46},1L},{0x27L,0xAF65EB92L,-6L,-4L,2UL,0x35L,-9L,4294967289UL,{4,1,-27},-1L},{4UL,18446744073709551609UL,0x55L,-10L,0x0263L,0x33L,-1L,1UL,{0,-32,-124},0x7CF21852L},{0x27L,0xAF65EB92L,-6L,-4L,2UL,0x35L,-9L,4294967289UL,{4,1,-27},-1L},{0xB6L,0x45A12C6DL,8L,-9L,0x0ADBL,0L,0L,0x4E45F982L,{-1,40,46},1L},{1UL,0xE2595FBEL,0x7CL,0xF118L,1UL,-6L,-1L,4294967295UL,{0,-0,-1},-9L}}};
static int64_t g_2777 = 0x4ABB65AF1EE8026FLL;
static struct S2 g_2816 = {3UL,1UL,0x72L,0x0D2AL,0UL,-1L,0xD5D8L,1UL,{1,-36,123},0xDD4E567DL};/* VOLATILE GLOBAL g_2816 */
static volatile union U5 g_2821[1] = {{4294967295UL}};
static struct S0 g_2822 = {-14,4361,1226,-1,3};/* VOLATILE GLOBAL g_2822 */
static struct S2 g_2825 = {0x93L,0xB3843AF7L,-1L,-8L,0x1C7CL,0x92L,0x5122L,4294967295UL,{4,44,-74},0x79CE1E44L};/* VOLATILE GLOBAL g_2825 */
static uint16_t g_2836 = 0UL;
static volatile struct S1 g_2837 = {-1,-41,103};/* VOLATILE GLOBAL g_2837 */
static float g_2840[4][3] = {{(-0x1.Cp+1),0xA.CEA57Bp-73,0xA.CEA57Bp-73},{(-0x1.Cp+1),0xA.CEA57Bp-73,0xA.CEA57Bp-73},{(-0x1.Cp+1),0xA.CEA57Bp-73,0xA.CEA57Bp-73},{(-0x1.Cp+1),0xA.CEA57Bp-73,0xA.CEA57Bp-73}};
static struct S0 g_2857 = {-29,3976,1490,1,15};/* VOLATILE GLOBAL g_2857 */
static uint32_t g_2891[8][7] = {{1UL,1UL,1UL,0UL,0x81D87B7DL,0xBFD9E2CFL,18446744073709551606UL},{5UL,0x81D87B7DL,0x12653692L,0x9A117503L,0x53393C8EL,18446744073709551615UL,0x97BE0394L},{0x0EE3C251L,1UL,0xBFD9E2CFL,0x58104058L,0x81D87B7DL,18446744073709551610UL,0x81D87B7DL},{1UL,0x53393C8EL,0x53393C8EL,1UL,5UL,18446744073709551610UL,0x8514BFBBL},{0x58104058L,0xBFD9E2CFL,1UL,0x0EE3C251L,1UL,18446744073709551615UL,0UL},{0x9A117503L,0x12653692L,0x81D87B7DL,5UL,1UL,0xBFD9E2CFL,0x8514BFBBL},{0UL,0x8514BFBBL,0xA4E1B76DL,0xA4E1B76DL,0x8514BFBBL,1UL,0UL},{1UL,0xC408FF48L,0x97BE0394L,0x58104058L,1UL,0x81D87B7DL,1UL}};
static union U3 *g_2901 = &g_1049;
static union U3 * volatile * volatile g_2900 = &g_2901;/* VOLATILE GLOBAL g_2900 */
static volatile struct S1 g_2935[7] = {{2,-23,56},{2,-23,56},{2,-23,56},{2,-23,56},{2,-23,56},{2,-23,56},{2,-23,56}};
static int64_t *g_2976 = (void*)0;
static int64_t **g_2975 = &g_2976;
static int64_t ***g_2974[4][2][9] = {{{&g_2975,&g_2975,&g_2975,&g_2975,(void*)0,(void*)0,&g_2975,&g_2975,&g_2975},{(void*)0,(void*)0,&g_2975,&g_2975,&g_2975,&g_2975,&g_2975,&g_2975,&g_2975}},{{(void*)0,&g_2975,&g_2975,&g_2975,&g_2975,(void*)0,(void*)0,&g_2975,(void*)0},{&g_2975,&g_2975,&g_2975,&g_2975,(void*)0,(void*)0,(void*)0,&g_2975,&g_2975}},{{(void*)0,(void*)0,&g_2975,&g_2975,&g_2975,&g_2975,(void*)0,&g_2975,&g_2975},{&g_2975,&g_2975,&g_2975,&g_2975,(void*)0,&g_2975,&g_2975,(void*)0,&g_2975}},{{&g_2975,&g_2975,(void*)0,&g_2975,&g_2975,(void*)0,&g_2975,(void*)0,&g_2975},{(void*)0,&g_2975,&g_2975,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_2975}}};
static int64_t **** volatile g_2973[6] = {&g_2974[0][1][1],&g_2974[0][1][1],&g_2974[0][1][1],&g_2974[0][1][1],&g_2974[0][1][1],&g_2974[0][1][1]};
static int64_t **** volatile g_2977 = (void*)0;/* VOLATILE GLOBAL g_2977 */
static int64_t **** volatile g_2978 = (void*)0;/* VOLATILE GLOBAL g_2978 */
static volatile uint32_t g_2992 = 0x516586ECL;/* VOLATILE GLOBAL g_2992 */
static const struct S0 *g_2995 = &g_2113;
static const struct S0 ** volatile g_2994 = &g_2995;/* VOLATILE GLOBAL g_2994 */
static struct S2 g_2996 = {0x45L,0xE3423710L,9L,0x6E31L,0xED64L,0x50L,0xC83BL,0x1A68CC9CL,{-0,37,-0},0x525BFD62L};/* VOLATILE GLOBAL g_2996 */


/* --- FORWARD DECLARATIONS --- */
static struct S2  func_1(void);
static uint8_t  func_4(uint8_t  p_5, int32_t  p_6);
static uint32_t  func_8(uint8_t  p_9, union U3  p_10);
static int64_t  func_12(int8_t  p_13, uint16_t  p_14);
static union U3  func_22(int32_t * p_23, int32_t * p_24, int64_t  p_25);
static int32_t * func_26(int32_t  p_27, int16_t  p_28, int32_t * p_29);
static int32_t * func_30(const int16_t  p_31, const int32_t * p_32, uint16_t  p_33, int32_t * p_34);
static const int32_t * func_35(float  p_36, const int32_t * p_37, uint8_t  p_38);
static int32_t  func_44(int32_t * p_45, int16_t  p_46, int32_t  p_47, int32_t  p_48, float  p_49);
static uint16_t  func_56(uint32_t  p_57, uint32_t  p_58, int32_t * p_59, float  p_60, int32_t * p_61);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_790 g_791
 * writes:
 */
static struct S2  func_1(void)
{ /* block id: 0 */
    float l_2[5];
    int32_t l_15 = (-6L);
    int64_t l_2354 = 0x521CC269081EA468LL;
    int32_t *l_2363 = (void*)0;
    union U3 *l_2411 = (void*)0;
    uint32_t *l_2436[4][4][10] = {{{&g_91[4][9][2],(void*)0,&g_91[5][4][2],&g_91[5][4][2],(void*)0,&g_91[4][9][2],&g_143[3][1],&g_91[5][3][0],&g_1286.f0,&g_91[5][3][0]},{&g_1286.f0,&g_143[3][1],&g_143[3][1],(void*)0,&g_1286.f0,&g_143[0][0],(void*)0,&g_143[3][1],&g_143[3][1],&g_91[4][3][0]},{&g_1286.f0,&g_143[2][1],(void*)0,&g_143[3][1],&g_143[3][1],&g_91[4][9][2],&g_91[4][3][0],&g_1286.f0,(void*)0,&g_1286.f0},{&g_91[4][9][2],&g_91[4][3][0],&g_1286.f0,(void*)0,&g_1286.f0,&g_91[4][3][0],&g_91[4][9][2],&g_143[3][1],&g_143[3][1],(void*)0}},{{&g_91[5][3][0],&g_1286.f0,&g_91[4][3][0],&g_143[3][1],&g_143[3][1],(void*)0,&g_143[0][0],&g_1286.f0,(void*)0,&g_143[3][1]},{&g_143[3][1],&g_1286.f0,&g_91[5][3][0],&g_1286.f0,&g_91[5][3][0],&g_143[3][1],&g_91[4][9][2],(void*)0,&g_91[5][4][2],&g_91[5][4][2]},{&g_143[3][1],&g_91[4][3][0],(void*)0,&g_1286.f0,&g_1286.f0,(void*)0,&g_91[4][3][0],&g_143[3][1],&g_143[2][1],&g_1286.f0},{&g_91[5][3][0],&g_143[2][1],&g_143[3][1],&g_1286.f0,&g_91[5][3][0],&g_91[5][3][0],(void*)0,(void*)0,&g_91[4][9][2],&g_1286.f0}},{{&g_143[3][1],&g_143[3][1],&g_143[3][1],&g_91[5][3][0],(void*)0,(void*)0,&g_143[3][1],&g_143[3][1],&g_1286.f0,(void*)0},{&g_91[4][1][0],(void*)0,&g_1286.f0,(void*)0,&g_143[3][1],(void*)0,&g_1286.f0,&g_91[5][4][2],&g_143[3][1],(void*)0},{&g_91[4][3][0],&g_1286.f0,&g_91[4][1][0],(void*)0,&g_1286.f0,&g_143[3][1],&g_143[0][0],(void*)0,(void*)0,&g_143[3][1]},{(void*)0,&g_143[2][1],&g_1286.f0,(void*)0,&g_1286.f0,&g_1286.f0,&g_91[5][3][0],&g_91[5][3][0],&g_143[3][1],&g_143[3][1]}},{{&g_143[3][1],(void*)0,&g_1286.f0,(void*)0,&g_143[3][1],&g_143[3][1],(void*)0,&g_1286.f0,(void*)0,&g_143[3][1]},{&g_1286.f0,&g_1286.f0,&g_91[5][4][2],&g_91[4][9][2],(void*)0,&g_143[3][1],&g_143[3][1],(void*)0,&g_1286.f0,&g_143[3][1]},{(void*)0,(void*)0,&g_91[5][3][0],&g_1286.f0,(void*)0,(void*)0,&g_1286.f0,&g_143[0][0],&g_143[3][1],&g_143[3][1]},{(void*)0,&g_143[3][1],&g_1286.f0,&g_143[3][1],&g_143[3][1],&g_91[5][3][0],&g_143[3][1],&g_143[3][1],&g_1286.f0,&g_143[3][1]}}};
    uint32_t **l_2435 = &l_2436[1][3][4];
    uint32_t ***l_2434[1];
    uint32_t ****l_2433[8][1];
    const int32_t *l_2437 = &g_2308.f0;
    uint8_t l_2483 = 0UL;
    float l_2505 = 0x0.9p-1;
    int32_t l_2508 = 0L;
    uint8_t l_2559 = 0UL;
    int64_t l_2571 = (-1L);
    uint32_t **l_2574 = &g_1703;
    union U5 * const l_2580 = &g_1286;
    union U4 *l_2585[5] = {&g_2525,&g_2525,&g_2525,&g_2525,&g_2525};
    union U4 **l_2584[9] = {&l_2585[4],&l_2585[1],&l_2585[4],&l_2585[1],&l_2585[4],&l_2585[1],&l_2585[4],&l_2585[1],&l_2585[4]};
    uint64_t ***l_2596 = &g_870;
    uint8_t ***l_2600 = (void*)0;
    uint8_t *** const *l_2599 = &l_2600;
    uint64_t l_2647 = 0UL;
    int32_t l_2677 = 0x274D7577L;
    union U5 *l_2684[7] = {&g_1286,(void*)0,(void*)0,&g_1286,(void*)0,(void*)0,&g_1286};
    uint32_t l_2732 = 3UL;
    const uint64_t l_2856 = 3UL;
    uint16_t l_2982[5];
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_2[i] = 0x6.37C5B1p+62;
    for (i = 0; i < 1; i++)
        l_2434[i] = &l_2435;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
            l_2433[i][j] = &l_2434[0];
    }
    for (i = 0; i < 5; i++)
        l_2982[i] = 65535UL;
    return (*g_790);
}


/* ------------------------------------------ */
/* 
 * reads : g_1305 g_794.f0 g_21 g_1659 g_790 g_791 g_1247.f1 g_143 g_576.f0 g_334.f3 g_2322 g_702 g_815 g_816 g_817 g_793.f4 g_510 g_1538 g_1532.f2 g_51
 * writes: g_232 g_794.f0 g_21 g_794.f6 g_1538 g_1092.f4 g_510 g_75 g_51
 */
static uint8_t  func_4(uint8_t  p_5, int32_t  p_6)
{ /* block id: 693 */
    int32_t *l_1514 = &g_21;
    struct S0 ****l_1530 = &g_739[1];
    union U4 *l_1533 = &g_1534[0][0][5];
    int32_t * const l_1537 = &g_1538[0];
    int32_t * const *l_1536 = &l_1537;
    struct S2 **l_1543 = &g_790;
    uint32_t *l_1560 = &g_214[0];
    uint32_t **l_1559[10][5] = {{(void*)0,&l_1560,(void*)0,(void*)0,&l_1560},{&l_1560,&l_1560,&l_1560,&l_1560,&l_1560},{&l_1560,&l_1560,&l_1560,&l_1560,&l_1560},{&l_1560,&l_1560,&l_1560,&l_1560,&l_1560},{&l_1560,(void*)0,(void*)0,&l_1560,(void*)0},{&l_1560,&l_1560,&l_1560,&l_1560,&l_1560},{(void*)0,&l_1560,(void*)0,(void*)0,&l_1560},{&l_1560,&l_1560,&l_1560,&l_1560,&l_1560},{&l_1560,&l_1560,&l_1560,&l_1560,&l_1560},{&l_1560,&l_1560,&l_1560,&l_1560,&l_1560}};
    int32_t l_1576 = 0x06E7398BL;
    int16_t l_1595 = 1L;
    uint32_t * const l_1601 = &g_1602;
    uint32_t * const *l_1600 = &l_1601;
    float l_1614[7];
    int32_t l_1641 = 0x8D9DB730L;
    float l_1684 = 0x4.F13AB4p-93;
    int32_t l_1687[7][4][3] = {{{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L},{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L}},{{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L},{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L}},{{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L},{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L}},{{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L},{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0L,0L}},{{0xBF0874EEL,0x30CF3AA5L,0x30CF3AA5L},{0x912680F3L,0x59E9FB46L,0x59E9FB46L},{0x30CF3AA5L,1L,1L},{0L,0x59E9FB46L,0x59E9FB46L}},{{0x30CF3AA5L,1L,1L},{0L,0x59E9FB46L,0x59E9FB46L},{0x30CF3AA5L,1L,1L},{0L,0x59E9FB46L,0x59E9FB46L}},{{0x30CF3AA5L,1L,1L},{0L,0x59E9FB46L,0x59E9FB46L},{0x30CF3AA5L,1L,1L},{0L,0x59E9FB46L,0x59E9FB46L}}};
    int32_t l_1726 = 0L;
    union U3 *l_1822[8][8][2] = {{{(void*)0,&g_607},{&g_607,&g_607},{&g_1049,(void*)0},{(void*)0,&g_1049},{&g_1049,&g_1049},{&g_1049,&g_1049},{(void*)0,&g_1049},{(void*)0,&g_1049}},{{&g_607,&g_1049},{(void*)0,&g_1049},{(void*)0,&g_1049},{&g_1049,&g_1049},{&g_1049,&g_1049},{(void*)0,(void*)0},{&g_1049,&g_607},{&g_607,&g_607}},{{(void*)0,&g_607},{&g_1049,&g_1049},{&g_1049,&g_607},{(void*)0,&g_607},{&g_607,&g_607},{&g_1049,(void*)0},{(void*)0,&g_1049},{&g_1049,&g_1049}},{{&g_1049,&g_1049},{(void*)0,&g_1049},{(void*)0,&g_1049},{&g_607,&g_1049},{(void*)0,&g_1049},{(void*)0,&g_1049},{&g_1049,&g_1049},{&g_1049,&g_1049}},{{(void*)0,(void*)0},{&g_1049,&g_607},{&g_607,&g_607},{(void*)0,&g_607},{&g_1049,&g_1049},{&g_1049,&g_607},{(void*)0,&g_607},{&g_607,&g_607}},{{&g_1049,(void*)0},{(void*)0,&g_1049},{&g_1049,&g_1049},{&g_1049,&g_1049},{(void*)0,&g_1049},{(void*)0,&g_1049},{&g_607,&g_1049},{(void*)0,&g_1049}},{{(void*)0,&g_1049},{&g_1049,&g_1049},{&g_1049,&g_1049},{(void*)0,(void*)0},{&g_1049,&g_607},{&g_607,&g_607},{(void*)0,&g_607},{&g_1049,&g_1049}},{{&g_1049,&g_607},{(void*)0,&g_607},{&g_607,&g_607},{&g_1049,(void*)0},{(void*)0,&g_1049},{&g_1049,&g_1049},{&g_1049,&g_1049},{(void*)0,&g_1049}}};
    const uint16_t **l_1854 = (void*)0;
    const uint16_t *l_1856[3];
    const uint16_t **l_1855 = &l_1856[1];
    int16_t **l_1859 = (void*)0;
    int16_t *l_1860[7][9] = {{&g_793[0][0].f6,(void*)0,&g_793[0][0].f6,(void*)0,&g_793[0][0].f6,&g_1747.f6,&g_1747.f6,&g_793[0][0].f6,(void*)0},{&g_234[0][1],&g_234[5][2],&g_234[0][1],&g_512[2][1][4],(void*)0,(void*)0,&g_512[2][1][4],&g_234[0][1],&g_234[5][2]},{&g_1747.f6,&g_794.f6,&g_1747.f6,&g_793[0][0].f6,&g_793[0][0].f6,&g_1747.f6,&g_794.f6,&g_1747.f6,&g_794.f6},{(void*)0,&g_1532.f6,&g_512[2][1][4],&g_512[2][1][4],&g_512[2][1][4],&g_234[0][1],&g_234[5][2],&g_234[0][1],&g_512[2][1][4]},{&g_794.f6,&g_793[0][0].f6,&g_793[0][0].f6,&g_794.f6,&g_791[0][4].f6,&g_1747.f6,&g_791[0][4].f6,&g_794.f6,&g_793[0][0].f6},{(void*)0,(void*)0,&g_234[5][2],&g_512[2][1][4],(void*)0,&g_512[2][1][4],&g_234[5][2],(void*)0,(void*)0},{&g_793[0][0].f6,&g_794.f6,&g_791[0][4].f6,&g_1747.f6,&g_791[0][4].f6,&g_794.f6,&g_793[0][0].f6,&g_793[0][0].f6,&g_794.f6}};
    struct S0 ***l_1861 = (void*)0;
    union U4 **l_1873[2][7] = {{&l_1533,(void*)0,&l_1533,(void*)0,&l_1533,&l_1533,(void*)0},{&l_1533,&l_1533,&l_1533,(void*)0,(void*)0,&l_1533,&l_1533}};
    int32_t *l_1876 = &g_51;
    struct S1 *l_1960 = &g_1551;
    uint8_t l_2056 = 0UL;
    uint16_t *l_2190 = &g_75[1][2];
    uint16_t **l_2189[2];
    uint64_t ***l_2211 = &g_870;
    int64_t l_2254[8];
    int8_t l_2294[4] = {0L,0L,0L,0L};
    uint32_t l_2309 = 0x1BB77464L;
    int32_t *l_2323 = &l_1687[5][1][0];
    uint8_t *l_2333 = (void*)0;
    uint8_t *l_2334 = &g_510[6];
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_1614[i] = 0x5.2p+1;
    for (i = 0; i < 3; i++)
        l_1856[i] = &g_793[0][0].f4;
    for (i = 0; i < 2; i++)
        l_2189[i] = &l_2190;
    for (i = 0; i < 8; i++)
        l_2254[i] = 0xCC469B754EF5927BLL;
    (*g_1305) = l_1514;
    for (g_794.f0 = 22; (g_794.f0 >= 19); g_794.f0--)
    { /* block id: 697 */
        uint64_t ***l_1519[9][9][3] = {{{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{(void*)0,&g_870,&g_870},{&g_870,&g_870,&g_870}},{{(void*)0,(void*)0,&g_870},{&g_870,&g_870,(void*)0},{&g_870,(void*)0,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870}},{{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{(void*)0,&g_870,&g_870},{&g_870,&g_870,&g_870},{(void*)0,(void*)0,&g_870},{&g_870,&g_870,(void*)0},{&g_870,(void*)0,&g_870},{&g_870,&g_870,&g_870}},{{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{(void*)0,&g_870,&g_870}},{{&g_870,&g_870,&g_870},{(void*)0,(void*)0,&g_870},{&g_870,&g_870,(void*)0},{&g_870,(void*)0,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870}},{{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{(void*)0,&g_870,&g_870},{(void*)0,&g_870,&g_870},{(void*)0,(void*)0,(void*)0},{&g_870,(void*)0,&g_870},{&g_870,(void*)0,&g_870}},{{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870}},{{(void*)0,&g_870,&g_870},{(void*)0,&g_870,&g_870},{(void*)0,(void*)0,(void*)0},{&g_870,(void*)0,&g_870},{&g_870,(void*)0,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0}},{{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,&g_870},{&g_870,&g_870,(void*)0},{&g_870,&g_870,&g_870},{(void*)0,&g_870,&g_870},{(void*)0,&g_870,&g_870},{(void*)0,(void*)0,(void*)0},{&g_870,(void*)0,&g_870}}};
        uint64_t ****l_1518 = &l_1519[7][6][2];
        int16_t *l_1525[3];
        uint16_t l_1526 = 0x96A5L;
        struct S0 ****l_1529 = &g_739[4];
        int64_t *l_1531[3][6][4] = {{{(void*)0,&g_513[1][0][8],(void*)0,&g_513[1][4][9]},{&g_513[1][0][8],&g_513[1][4][9],&g_513[1][3][6],(void*)0},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9]},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][3][6],(void*)0},{&g_513[1][0][8],&g_513[1][4][9],(void*)0,&g_513[1][4][9]},{(void*)0,&g_513[1][4][9],(void*)0,&g_513[1][4][9]}},{{(void*)0,&g_513[1][4][9],&g_513[1][0][8],(void*)0},{&g_513[1][3][6],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9]},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],(void*)0},{&g_513[1][3][6],&g_513[1][4][9],&g_513[1][0][8],&g_513[1][4][9]},{(void*)0,&g_513[1][0][8],(void*)0,&g_513[1][0][8]},{(void*)0,&g_513[1][0][8],(void*)0,&g_513[1][4][9]}},{{&g_513[1][0][8],&g_513[1][4][9],&g_513[1][3][6],(void*)0},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9]},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][3][6],(void*)0},{&g_513[1][0][8],&g_513[1][4][9],(void*)0,&g_513[1][4][9]},{(void*)0,&g_513[1][4][9],(void*)0,&g_513[1][4][9]},{(void*)0,&g_513[1][4][9],&g_513[1][0][8],(void*)0}}};
        int32_t * const *l_1535 = (void*)0;
        struct S1 *l_1550 = &g_1551;
        int8_t l_1603[1];
        uint64_t * const ***l_1632 = (void*)0;
        int32_t *l_1636[1];
        union U3 l_1638 = {1L};
        int32_t l_1728[3];
        struct S2 **l_1738 = &g_790;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1525[i] = &g_95;
        for (i = 0; i < 1; i++)
            l_1603[i] = 0x03L;
        for (i = 0; i < 1; i++)
            l_1636[i] = &g_51;
        for (i = 0; i < 3; i++)
            l_1728[i] = 0x0371A514L;
        (*l_1514) &= (p_6 == (~(l_1526 = ((l_1518 == &g_425[0]) >= (safe_mul_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s(p_5, (safe_unary_minus_func_uint16_t_u(0xE32DL)))), p_5))))));
    }
    if (((g_1092.f4 = (p_6 >= ((((((((*g_1659) = (4UL || 8UL)) | (((((*l_1855) = &g_74[0]) == (void*)0) == (safe_div_func_int16_t_s_s(((*l_1537) = (((void*)0 == l_1859) && (g_794.f6 = ((*g_790) , g_1247.f1)))), p_6))) <= g_143[3][2])) != g_576.f0) , l_1861) != l_1861) , p_5) , p_6))) >= g_334.f3))
    { /* block id: 834 */
        int32_t l_1868[6] = {0xDAF72166L,0xDAF72166L,0x1D14A27BL,0xDAF72166L,0xDAF72166L,0x1D14A27BL};
        int32_t l_1869 = 9L;
        union U4 **l_1874 = &l_1533;
        int8_t *l_1875 = &g_502;
        uint32_t l_1895 = 1UL;
        uint64_t l_1966 = 0xB362C66F8B1C6264LL;
        float *l_1987 = (void*)0;
        uint32_t l_1997 = 18446744073709551612UL;
        int64_t l_2010 = 0xFC1A04FDDD8106BELL;
        int16_t *l_2050[8][5] = {{&g_95,&g_793[0][0].f6,&g_794.f6,&g_512[2][0][0],&g_793[0][0].f6},{&g_234[0][3],&g_793[0][0].f6,&g_794.f6,&g_794.f6,&g_793[0][0].f6},{&l_1595,&g_95,(void*)0,(void*)0,&g_95},{&l_1595,&g_512[2][1][4],&g_793[0][0].f6,(void*)0,&g_512[2][1][4]},{&g_1532.f6,&g_512[2][1][4],(void*)0,(void*)0,&g_512[2][1][4]},{&l_1595,&g_95,(void*)0,(void*)0,&g_95},{&l_1595,&g_512[2][1][4],&g_793[0][0].f6,(void*)0,&g_512[2][1][4]},{&g_1532.f6,&g_512[2][1][4],(void*)0,(void*)0,&g_512[2][1][4]}};
        int32_t l_2094 = (-1L);
        int32_t l_2096[10] = {(-7L),(-4L),(-7L),(-4L),(-7L),(-4L),(-7L),(-4L),(-7L),(-4L)};
        uint64_t l_2102 = 18446744073709551609UL;
        const uint16_t **l_2194 = &l_1856[0];
        struct S0 **l_2266 = &g_597[0];
        const int32_t **l_2316[4] = {&g_750,&g_750,&g_750,&g_750};
        const int32_t ***l_2315 = &l_2316[0];
        int32_t ***l_2318 = (void*)0;
        int32_t ****l_2317[8][2] = {{&l_2318,(void*)0},{(void*)0,(void*)0},{&l_2318,(void*)0},{(void*)0,(void*)0},{&l_2318,(void*)0},{(void*)0,(void*)0},{&l_2318,(void*)0},{(void*)0,(void*)0}};
        int i, j;
    }
    else
    { /* block id: 1038 */
        (*g_702) = (g_2322 , l_2323);
    }
    (*l_1876) ^= ((safe_sub_func_uint8_t_u_u((*l_2323), ((safe_mod_func_uint16_t_u_u((safe_sub_func_int8_t_s_s(((*l_1514) & p_5), (!(-2L)))), (safe_div_func_uint32_t_u_u((**g_815), ((*l_2323) | ((*l_1514) && (*l_1514))))))) ^ ((((*l_2190) = ((((*l_2334) ^= ((((g_793[0][0].f4 < 0x9D8ED350EEE0E7F3LL) , &p_6) == (void*)0) != p_6)) < (*l_1537)) , g_1532.f2)) < 0UL) < (*l_1514))))) && (*l_2323));
    return (**l_1536);
}


/* ------------------------------------------ */
/* 
 * reads : g_153 g_1222 g_1230 g_21 g_1247 g_427 g_3 g_794.f7 g_598.f4 g_791.f6 g_794.f6 g_1267 g_576.f1 g_513 g_816 g_817 g_75 g_793.f8.f1 g_1284 g_565 g_1286 g_51 g_794.f2 g_1305 g_1285 g_923 g_1247.f2 g_334.f0 g_1328 g_620 g_20 g_212 g_1247.f1 g_424 g_425 g_426 g_523.f4 g_214 g_791.f7 g_598.f0 g_794.f0 g_535.f0 g_815 g_143 g_1049 g_835 g_1426 g_575 g_576 g_1093.f0 g_790 g_791 g_523.f3 g_119 g_793.f7 g_598.f3 g_1018.f0 g_1034 g_793.f2 g_794.f4 g_147
 * writes: g_153 g_21 g_793.f2 g_512 g_620 g_51 g_513 g_794.f2 g_1302 g_232 g_923 g_16 g_75 g_3 g_510 g_793.f6 g_143 g_1464 g_791.f6 g_147 g_750 g_81 g_121 g_1176 g_463 g_1034
 */
static uint32_t  func_8(uint8_t  p_9, union U3  p_10)
{ /* block id: 576 */
    uint64_t l_1208[8][3][10] = {{{18446744073709551610UL,0x283F5F58779EC9D6LL,1UL,0x9C0A3A8F2E3581A8LL,0x033E528C142DB276LL,0UL,18446744073709551613UL,1UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551610UL,0xC969E8BFB637E3B7LL,4UL,0xB559B79189F95E26LL,4UL,7UL,18446744073709551615UL,0x9C0A3A8F2E3581A8LL,0xB559B79189F95E26LL,0x7EDE6B188C29B169LL},{7UL,18446744073709551615UL,0x9C0A3A8F2E3581A8LL,0xB559B79189F95E26LL,0x7EDE6B188C29B169LL,18446744073709551613UL,18446744073709551609UL,18446744073709551615UL,18446744073709551615UL,0xC969E8BFB637E3B7LL}},{{0xBA6D0D3C4C309DC0LL,4UL,1UL,0x9C0A3A8F2E3581A8LL,18446744073709551613UL,8UL,18446744073709551609UL,0UL,0xF058C26BABD708CELL,0x7FA7730CBE1847DFLL},{0UL,18446744073709551615UL,0x9209E49DC3DA0B2ALL,0x4389083B4599ED63LL,0xC969E8BFB637E3B7LL,3UL,18446744073709551615UL,0xB559B79189F95E26LL,1UL,0x7FA7730CBE1847DFLL},{0x25DFCF5F9875EA0ELL,0xC969E8BFB637E3B7LL,0UL,0x83748FDF973714B6LL,18446744073709551613UL,18446744073709551610UL,18446744073709551613UL,0x83748FDF973714B6LL,0UL,0xC969E8BFB637E3B7LL}},{{3UL,0x283F5F58779EC9D6LL,0UL,1UL,0x7EDE6B188C29B169LL,18446744073709551615UL,0UL,0xB559B79189F95E26LL,0x83748FDF973714B6LL,0x7EDE6B188C29B169LL},{8UL,0UL,0x9209E49DC3DA0B2ALL,0xF058C26BABD708CELL,4UL,18446744073709551615UL,1UL,0UL,4UL,18446744073709551615UL},{3UL,18446744073709551609UL,1UL,0UL,0x033E528C142DB276LL,18446744073709551610UL,0UL,18446744073709551615UL,4UL,0x283F5F58779EC9D6LL}},{{0x25DFCF5F9875EA0ELL,2UL,0x9C0A3A8F2E3581A8LL,0xF058C26BABD708CELL,0UL,3UL,0x7FA7730CBE1847DFLL,0x9C0A3A8F2E3581A8LL,0x83748FDF973714B6LL,1UL},{0UL,2UL,4UL,1UL,18446744073709551615UL,8UL,0UL,1UL,0UL,18446744073709551613UL},{0xBA6D0D3C4C309DC0LL,18446744073709551609UL,1UL,0x83748FDF973714B6LL,18446744073709551615UL,18446744073709551613UL,1UL,1UL,1UL,1UL}},{{7UL,0UL,0x4389083B4599ED63LL,0x4389083B4599ED63LL,0UL,7UL,0UL,1UL,0xF058C26BABD708CELL,0x283F5F58779EC9D6LL},{18446744073709551610UL,0x110678B0635268C7LL,0xC969E8BFB637E3B7LL,18446744073709551609UL,18446744073709551611UL,2UL,0UL,0x7EDE6B188C29B169LL,0UL,0x920FF2B9860CBF3BLL},{18446744073709551615UL,0xBAB5DCE0E6E3574ALL,0x033E528C142DB276LL,2UL,0xB0B510FCA8634B67LL,0xE14D1A7EB055BDEELL,0x920FF2B9860CBF3BLL,18446744073709551609UL,2UL,18446744073709551610UL}},{{0xE14D1A7EB055BDEELL,0x920FF2B9860CBF3BLL,18446744073709551609UL,2UL,18446744073709551610UL,0UL,0xB0283389BFA79AEALL,18446744073709551613UL,0UL,0xBAB5DCE0E6E3574ALL},{7UL,0xB0B510FCA8634B67LL,1UL,18446744073709551609UL,0UL,1UL,0xB0283389BFA79AEALL,4UL,18446744073709551615UL,0xCD466B92C48EACCCLL},{0UL,0x920FF2B9860CBF3BLL,0x7FA7730CBE1847DFLL,0UL,0xBAB5DCE0E6E3574ALL,0x304606309EB8236ALL,0x920FF2B9860CBF3BLL,2UL,1UL,0xCD466B92C48EACCCLL}},{{0xEA7314B30D928759LL,0xBAB5DCE0E6E3574ALL,4UL,0x283F5F58779EC9D6LL,0UL,18446744073709551615UL,0UL,0x283F5F58779EC9D6LL,4UL,0xBAB5DCE0E6E3574ALL},{0x304606309EB8236ALL,0x110678B0635268C7LL,4UL,0xC969E8BFB637E3B7LL,18446744073709551610UL,0xC450FE6CA6F8FC1CLL,0UL,2UL,0x283F5F58779EC9D6LL,18446744073709551610UL},{0UL,0x143C4246871827A3LL,0x7FA7730CBE1847DFLL,18446744073709551615UL,0xB0B510FCA8634B67LL,0xC450FE6CA6F8FC1CLL,18446744073709551614UL,4UL,0x033E528C142DB276LL,0x920FF2B9860CBF3BLL}},{{0x304606309EB8236ALL,0xB0283389BFA79AEALL,1UL,4UL,18446744073709551611UL,18446744073709551615UL,0x143C4246871827A3LL,18446744073709551613UL,0x033E528C142DB276LL,0x110678B0635268C7LL},{0xEA7314B30D928759LL,0x4A287133DD36D26DLL,18446744073709551609UL,18446744073709551615UL,0x143C4246871827A3LL,0x304606309EB8236ALL,0xCD466B92C48EACCCLL,18446744073709551609UL,0x283F5F58779EC9D6LL,18446744073709551614UL},{0UL,0x4A287133DD36D26DLL,0x033E528C142DB276LL,0xC969E8BFB637E3B7LL,0x920FF2B9860CBF3BLL,1UL,0x143C4246871827A3LL,0x7EDE6B188C29B169LL,4UL,0UL}}};
    uint32_t l_1219 = 1UL;
    union U3 *l_1221 = &g_1049;
    union U3 ** const l_1220 = &l_1221;
    int64_t * const l_1231 = (void*)0;
    int8_t l_1265 = 7L;
    int16_t l_1270[1];
    int32_t * const l_1304 = (void*)0;
    union U5 *l_1309 = (void*)0;
    int32_t l_1313 = 1L;
    int32_t l_1338 = 3L;
    int32_t l_1342 = 0xACD641C5L;
    int32_t l_1343 = 0x1E1CADD2L;
    int32_t l_1346 = 0x02DEAF17L;
    int32_t l_1351 = 0xCDDFC1EFL;
    int32_t l_1354 = 0xCCDCF113L;
    int32_t l_1357 = 0x82B6954DL;
    uint8_t l_1362 = 9UL;
    int32_t ***l_1382 = &g_702;
    int32_t *l_1417[1];
    uint32_t *l_1419 = &g_214[0];
    uint32_t **l_1418 = &l_1419;
    uint64_t **l_1428 = &g_427;
    uint64_t ***l_1429[9];
    uint64_t **l_1430 = &g_427;
    uint32_t l_1431 = 1UL;
    int8_t l_1445 = 0x12L;
    uint16_t l_1465 = 0xEE20L;
    uint8_t l_1466 = 0xCCL;
    uint8_t *l_1467[5];
    int32_t l_1468 = (-5L);
    int16_t *l_1469 = &g_791[0][4].f6;
    uint64_t l_1470 = 1UL;
    uint16_t l_1489 = 0x9632L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1270[i] = 0x391AL;
    for (i = 0; i < 1; i++)
        l_1417[i] = (void*)0;
    for (i = 0; i < 9; i++)
        l_1429[i] = &g_870;
    for (i = 0; i < 5; i++)
        l_1467[i] = &g_85;
    if (l_1208[0][1][9])
    { /* block id: 577 */
        int32_t *l_1209 = &g_21;
        int32_t *l_1210 = &g_51;
        int32_t *l_1211 = &g_153;
        int32_t l_1212 = 0x5B313D3EL;
        int32_t *l_1213 = &g_21;
        uint32_t l_1214 = 1UL;
        int8_t *l_1232 = (void*)0;
        ++l_1214;
        (*l_1213) = (l_1219 > ((-1L) > ((*l_1211) |= (l_1220 == &l_1221))));
        if ((g_1222 , (((1UL & 255UL) == (p_10.f0 |= (~((4UL <= ((safe_lshift_func_int8_t_s_u((-1L), 5)) || ((g_1230 , (*l_1211)) > ((void*)0 != l_1231)))) ^ 0x82A5B990L)))) , 0x1706C38EL)))
        { /* block id: 582 */
            uint16_t *l_1242[10] = {&g_75[0][5],&g_74[0],&g_81,&g_81,&g_74[0],&g_75[0][5],&g_74[0],&g_81,&g_81,&g_74[0]};
            const uint16_t *l_1244 = &g_794.f4;
            const uint16_t **l_1243 = &l_1244;
            int32_t l_1260 = 0L;
            int8_t *l_1261 = &g_793[0][0].f2;
            int16_t l_1262 = (-1L);
            int32_t l_1263 = 0xF806578DL;
            int16_t *l_1264 = &g_512[6][6][3];
            int i;
            (*l_1213) &= 0x86D151D5L;
            (*g_1267) = (safe_div_func_float_f_f((safe_add_func_float_f_f(((-((((safe_lshift_func_int8_t_s_s(((safe_lshift_func_int8_t_s_u(((l_1242[4] == ((*l_1243) = (void*)0)) , (((safe_sub_func_uint32_t_u_u((g_1247 , p_9), (((*l_1213) = (((*g_427) != ((safe_sub_func_int64_t_s_s((1UL && ((safe_add_func_int16_t_s_s(((*l_1264) = (safe_add_func_int64_t_s_s(g_794.f7, (((safe_add_func_int64_t_s_s(((l_1263 ^= ((safe_add_func_uint64_t_u_u((((((*l_1261) = (safe_mul_func_uint8_t_u_u(l_1260, (g_598.f4 >= l_1260)))) , &g_596[2]) == (void*)0) , l_1262), (-1L))) || p_10.f0)) , p_9), p_10.f0)) ^ (*l_1213)) ^ l_1208[0][1][9])))), l_1208[0][2][0])) <= 3UL)), p_9)) ^ 0UL)) ^ p_9)) != g_791[0][4].f6))) | l_1219) <= 0xC3L)), (*l_1211))) >= l_1265), g_794.f6)) , (void*)0) == (void*)0) < l_1208[0][1][9])) != 0x2.6p+1), 0x1.Ap+1)), 0x1.B34393p-12));
            l_1210 = &l_1260;
            (*l_1213) |= p_9;
        }
        else
        { /* block id: 592 */
            int16_t *l_1271[1][5][9] = {{{&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6},{&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6},{&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6},{&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6},{&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6,&g_794.f6,&g_95,&g_794.f6}}};
            int32_t l_1272 = 0L;
            int32_t l_1277 = 0x691926C6L;
            int64_t *l_1280 = (void*)0;
            int64_t *l_1281 = &g_513[1][4][9];
            int i, j, k;
            (*l_1210) = 0x92CE7AB5L;
            (*l_1213) ^= ((safe_lshift_func_uint8_t_u_u(((l_1270[0] , (l_1272 = p_9)) && g_576.f1), (safe_add_func_int16_t_s_s((((&g_596[0] != &g_740[0][2]) == (((safe_mod_func_int64_t_s_s((l_1277 || (((safe_add_func_uint16_t_u_u((((((*l_1281) |= p_9) && (p_9 , (~(((safe_unary_minus_func_uint16_t_u(0x6BDCL)) , (*g_816)) | 0x7C9A6C99L)))) & g_75[1][4]) != g_793[0][0].f8.f1), 1UL)) , g_1284) != &g_1285[2][1][1])), g_565[0][0][3])) , g_1286) , p_9)) && l_1270[0]), g_1222.f4)))) | (*l_1210));
            (*l_1211) = ((*l_1210) = ((*l_1211) || p_10.f0));
        }
    }
    else
    { /* block id: 600 */
        uint64_t l_1298 = 0x6AE61B98319AB4B0LL;
        const uint32_t *l_1318 = &g_212[0][3][0];
        int16_t l_1333 = (-4L);
        int32_t l_1344 = 0xA65C1828L;
        int32_t l_1347 = 0xF1396BA2L;
        int32_t l_1348 = 0x1ED09034L;
        int32_t l_1349 = 9L;
        int32_t l_1350[1][7] = {{(-2L),1L,(-2L),(-2L),1L,(-2L),(-2L)}};
        int32_t ***l_1380 = &g_702;
        int32_t *l_1416 = &l_1350[0][5];
        int i, j;
        for (g_794.f2 = 13; (g_794.f2 > (-2)); g_794.f2 = safe_sub_func_uint16_t_u_u(g_794.f2, 8))
        { /* block id: 603 */
            uint32_t l_1306 = 0x772CE095L;
            int32_t l_1336 = (-2L);
            int32_t l_1341 = 0xB73C2490L;
            int32_t l_1352 = 1L;
            int32_t l_1353 = 0x35ED0668L;
            int32_t l_1355 = 0L;
            int32_t l_1356 = 0x6DAB3076L;
            int32_t l_1358 = (-3L);
            int32_t l_1359 = (-9L);
            int32_t l_1360 = 7L;
            int32_t l_1361 = (-7L);
            int32_t ***l_1383 = &g_702;
            struct S0 **l_1399[6] = {&g_597[0],&g_597[0],&g_597[0],&g_597[0],&g_597[0],&g_597[0]};
            const uint8_t l_1403 = 0xC7L;
            int i;
            for (g_51 = 16; (g_51 >= (-8)); g_51 = safe_sub_func_uint16_t_u_u(g_51, 1))
            { /* block id: 606 */
                int32_t *l_1291 = (void*)0;
                int32_t *l_1292 = (void*)0;
                int32_t *l_1293 = (void*)0;
                int32_t *l_1294 = &g_153;
                int32_t *l_1295 = &g_21;
                int32_t *l_1296 = &g_366.f0;
                int32_t *l_1297 = &g_21;
                l_1298--;
                return p_10.f0;
            }
            for (g_21 = 0; (g_21 <= 0); g_21 += 1)
            { /* block id: 612 */
                union U5 **l_1303 = &g_1302;
                (*l_1303) = &g_1286;
                (*g_1305) = l_1304;
                if (l_1306)
                    continue;
                for (g_153 = 0; (g_153 <= 1); g_153 += 1)
                { /* block id: 618 */
                    volatile int32_t *l_1308[3];
                    volatile int32_t **l_1307 = &l_1308[2];
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_1308[i] = (void*)0;
                    (*l_1307) = g_1285[g_21][(g_21 + 1)][(g_153 + 5)];
                }
            }
            for (g_923 = 4; (g_923 >= 1); g_923 -= 1)
            { /* block id: 624 */
                int8_t *l_1310 = &l_1265;
                int32_t *l_1311 = (void*)0;
                int32_t *l_1312[1][5];
                const uint32_t **l_1319 = &l_1318;
                float *l_1334 = &g_16[2];
                float *l_1335 = &g_620[3][1];
                int16_t l_1384 = 9L;
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 5; j++)
                        l_1312[i][j] = &g_1018[1][0].f0;
                }
                l_1313 ^= (((g_1247.f2 && p_10.f0) == (g_1222.f3 && (((((*l_1310) = ((l_1309 != (void*)0) && 0xB4L)) , p_9) , 4294967293UL) ^ (g_334.f0 && 0x784AC3D50D44FB49LL)))) != p_9);
                (*l_1335) = (l_1306 <= ((safe_mul_func_float_f_f(((l_1298 , (safe_mul_func_float_f_f(((((((((*l_1319) = l_1318) != (*g_1284)) == (safe_add_func_float_f_f(((safe_sub_func_float_f_f((safe_mul_func_float_f_f(((void*)0 == &g_534), (safe_add_func_float_f_f((g_1328 <= (((*l_1334) = ((l_1270[0] , (l_1333 = ((safe_mul_func_float_f_f((safe_add_func_float_f_f((*g_1267), p_10.f0)), 0xD.831375p+89)) < 0x0.2p-1))) >= l_1298)) < 0x8.467786p-29)), p_9)))), p_9)) != g_1222.f3), 0xC.4F9BDCp-25))) != l_1298) != 0x8.3p+1) >= l_1306) == p_10.f0), l_1306))) != p_9), l_1298)) == l_1298));
                (*g_20) |= (l_1336 &= p_9);
                for (l_1298 = 0; (l_1298 <= 0); l_1298 += 1)
                { /* block id: 635 */
                    int8_t l_1337 = (-3L);
                    int32_t l_1339 = 0x467543AEL;
                    int32_t l_1340 = 0L;
                    int32_t l_1345[1][9][10] = {{{(-1L),0xDCDA5D1AL,0x745CA49AL,7L,1L,0xC8DA433CL,1L,0xC8DA433CL,1L,7L},{7L,5L,7L,0xDCDA5D1AL,0xA432CA5DL,1L,7L,0xB75B9D78L,0xB75B9D78L,7L},{0xA432CA5DL,1L,1L,1L,1L,0xA432CA5DL,0xDCDA5D1AL,0xB75B9D78L,1L,0x745CA49AL},{(-1L),0x745CA49AL,(-2L),5L,0xB75B9D78L,5L,(-2L),0x745CA49AL,(-1L),0xA432CA5DL},{(-1L),1L,0xB7A8D407L,0xDCDA5D1AL,5L,0xA432CA5DL,0xA432CA5DL,5L,0xDCDA5D1AL,0xB7A8D407L},{0xA432CA5DL,0xA432CA5DL,5L,0xDCDA5D1AL,0xB7A8D407L,1L,(-1L),7L,(-1L),1L},{(-2L),5L,0xB75B9D78L,5L,(-2L),0x745CA49AL,(-1L),0xA432CA5DL,1L,1L},{0xDCDA5D1AL,0xA432CA5DL,1L,1L,1L,1L,0xA432CA5DL,0xDCDA5D1AL,0xB75B9D78L,1L},{7L,1L,0xA432CA5DL,0xC8DA433CL,(-2L),(-1L),(-2L),0xC8DA433CL,0xA432CA5DL,1L}}};
                    uint16_t *l_1369 = &g_75[0][2];
                    int32_t ****l_1381[10] = {&l_1380,&l_1380,&l_1380,&l_1380,&l_1380,&l_1380,&l_1380,&l_1380,&l_1380,&l_1380};
                    int i, j, k;
                    --l_1362;
                    l_1353 |= ((safe_div_func_int8_t_s_s((g_212[l_1298][(l_1298 + 2)][(l_1298 + 2)] != (l_1355 = ((safe_sub_func_uint8_t_u_u(((((****g_424) = (((*l_1369) = (g_1247.f1 & l_1337)) , p_10.f0)) <= (p_10.f0 >= ((safe_add_func_int8_t_s_s((safe_add_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s(((safe_mod_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((p_10.f0 , l_1350[0][2]), ((&g_702 == (l_1383 = (l_1382 = l_1380))) != 1UL))), g_523.f4)) > p_10.f0), 2)) ^ g_576.f1), g_214[0])), p_10.f0)) < l_1384))) <= p_10.f0), p_9)) & p_9))), 255UL)) > g_791[0][4].f7);
                }
                for (l_1353 = 0; (l_1353 <= 4); l_1353 += 1)
                { /* block id: 646 */
                    uint64_t l_1396[9][8] = {{18446744073709551610UL,8UL,0UL,0xC7BA5A0EA9244DCFLL,0UL,8UL,18446744073709551610UL,8UL},{18446744073709551610UL,0xC7BA5A0EA9244DCFLL,1UL,0xC7BA5A0EA9244DCFLL,18446744073709551610UL,0UL,18446744073709551610UL,0xC7BA5A0EA9244DCFLL},{0UL,0xC7BA5A0EA9244DCFLL,0UL,8UL,18446744073709551610UL,8UL,0UL,0xC7BA5A0EA9244DCFLL},{18446744073709551610UL,8UL,0UL,0xC7BA5A0EA9244DCFLL,0UL,8UL,18446744073709551610UL,8UL},{18446744073709551610UL,0xC7BA5A0EA9244DCFLL,1UL,0xC7BA5A0EA9244DCFLL,18446744073709551610UL,0UL,18446744073709551610UL,0xC7BA5A0EA9244DCFLL},{0UL,0xC7BA5A0EA9244DCFLL,0UL,8UL,18446744073709551610UL,8UL,0UL,0xC7BA5A0EA9244DCFLL},{18446744073709551610UL,8UL,0UL,0xC7BA5A0EA9244DCFLL,0UL,8UL,18446744073709551610UL,8UL},{18446744073709551610UL,0xC7BA5A0EA9244DCFLL,1UL,0xC7BA5A0EA9244DCFLL,18446744073709551610UL,0UL,18446744073709551610UL,0xC7BA5A0EA9244DCFLL},{0UL,0xC7BA5A0EA9244DCFLL,0UL,8UL,18446744073709551610UL,8UL,0UL,0xC7BA5A0EA9244DCFLL}};
                    uint8_t *l_1397 = (void*)0;
                    uint8_t *l_1398 = &g_510[0];
                    int64_t *l_1400 = &g_513[1][4][9];
                    int i, j;
                    (*g_20) = ((p_10.f0 != (safe_mul_func_int16_t_s_s(((0x2DL | (safe_rshift_func_int16_t_s_u(2L, (safe_unary_minus_func_int32_t_s(((safe_mod_func_int64_t_s_s((safe_mod_func_uint32_t_u_u((((g_793[0][0].f6 = ((safe_rshift_func_int16_t_s_u(p_10.f0, (((((*l_1398) = (p_10.f0 & l_1396[6][4])) < ((void*)0 == l_1399[4])) == ((*l_1400) &= (p_9 | p_10.f0))) >= p_9))) < p_9)) <= 0x7789L) && g_598.f0), p_9)), 6UL)) > 1L)))))) || 0UL), g_794.f0))) < g_535.f0);
                }
            }
            if (p_10.f0)
            { /* block id: 653 */
                uint32_t *l_1406 = (void*)0;
                uint32_t *l_1407 = &g_143[3][1];
                (*l_1416) |= (safe_mod_func_uint64_t_u_u(l_1403, ((safe_mul_func_uint16_t_u_u(g_794.f7, (0xCC027427AC9345EDLL >= p_10.f0))) || ((((**g_815) <= ((*l_1407)--)) , (((safe_div_func_int16_t_s_s((safe_mod_func_uint32_t_u_u((safe_div_func_int32_t_s_s(((g_1049 , g_923) <= 0xE8L), 0xAE35764CL)), 0x84CE7F67L)), g_835)) , &l_1318) == l_1418)) & 0xDD8AFBF1L))));
            }
            else
            { /* block id: 656 */
                uint64_t l_1423 = 0UL;
                if ((*l_1416))
                { /* block id: 657 */
                    int8_t l_1420 = 0L;
                    return l_1420;
                }
                else
                { /* block id: 659 */
                    float l_1421 = 0xF.7F540Dp+44;
                    int32_t l_1422 = 0xB67F9017L;
                    ++l_1423;
                }
            }
        }
    }
    l_1431 &= ((g_1426 , (((((*g_575) , &g_739[4]) != &g_739[2]) ^ ((~(1L && (((l_1428 != (l_1430 = l_1428)) == p_10.f0) != (p_9 >= 1L)))) > g_1093.f0)) >= 0xAEA81599A54C5A4FLL)) != p_10.f0);
    if ((p_10.f0 == (safe_mod_func_uint16_t_u_u((safe_unary_minus_func_uint8_t_u((((++(**l_1428)) , func_22(&g_21, &l_1343, (((safe_rshift_func_int16_t_s_s((safe_add_func_uint8_t_u_u(((((*l_1469) = (safe_lshift_func_uint8_t_u_u((((l_1468 ^= (safe_mul_func_int8_t_s_s((((((*g_790) , l_1445) < (safe_rshift_func_int8_t_s_u((safe_sub_func_uint64_t_u_u((l_1465 = ((**l_1428) = (safe_lshift_func_uint16_t_u_u(g_1222.f4, (safe_mod_func_uint64_t_u_u(((safe_div_func_int8_t_s_s((p_10.f0 >= (g_1464 = (((safe_add_func_int32_t_s_s((safe_lshift_func_int8_t_s_u(((g_794.f0 < (safe_rshift_func_uint16_t_u_s((((safe_add_func_uint32_t_u_u(1UL, g_598.f0)) , 0xEF83L) != 6L), 8))) ^ 0L), 1)), g_523.f3)) | g_119) < g_793[0][0].f7))), p_10.f0)) | 0xE2170CA48970B884LL), 3L)))))), 0x3F8643F323264EB8LL)), l_1466))) | 0UL) | 0x0E352D88E59A9AA3LL), p_9))) >= 0L) == p_10.f0), p_9))) > g_598.f3) , p_10.f0), p_9)), p_9)) , l_1470) > 0UL))) , p_10.f0))), 0xD825L))))
    { /* block id: 673 */
        (*l_1220) = &p_10;
        return p_10.f0;
    }
    else
    { /* block id: 676 */
        int32_t l_1474[7];
        int32_t l_1476 = 0x826FF3BCL;
        int32_t l_1482 = (-1L);
        int32_t l_1483 = 0x3B661F1BL;
        int32_t l_1484 = 0xBCB19A7EL;
        int32_t l_1485 = 0L;
        int32_t l_1486 = 0x19DF4D54L;
        int32_t l_1488[2][1];
        union U5 **l_1513 = (void*)0;
        int i, j;
        for (i = 0; i < 7; i++)
            l_1474[i] = 0xE5CEB701L;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 1; j++)
                l_1488[i][j] = 6L;
        }
        for (l_1313 = 20; (l_1313 == (-30)); l_1313 = safe_sub_func_int32_t_s_s(l_1313, 6))
        { /* block id: 679 */
            int32_t l_1475[7][6] = {{0x3B5A7AF3L,0L,3L,0x3B5A7AF3L,3L,0L},{(-1L),0L,0x6424154BL,(-1L),3L,3L},{0x36A81B14L,0L,0L,0x36A81B14L,3L,0x6424154BL},{0x3B5A7AF3L,0L,3L,0x3B5A7AF3L,3L,0L},{(-1L),0L,0x6424154BL,(-1L),3L,3L},{0x36A81B14L,0L,0x9710DF56L,0L,0x941CDBAEL,0xB6D8E0A6L},{0x6424154BL,0x9710DF56L,0x941CDBAEL,0x6424154BL,0x941CDBAEL,0x9710DF56L}};
            int32_t l_1477 = 0L;
            int32_t l_1478 = 0x41BD9C05L;
            int32_t l_1479 = 0x84876E44L;
            int32_t l_1480 = (-6L);
            int32_t l_1481[4] = {0x7DCCFCE2L,0x7DCCFCE2L,0x7DCCFCE2L,0x7DCCFCE2L};
            int64_t l_1487 = 0L;
            const uint32_t l_1494 = 0xBC895F97L;
            int i, j;
            l_1489--;
            for (g_153 = (-22); (g_153 >= 4); g_153++)
            { /* block id: 683 */
                if (l_1494)
                    break;
            }
        }
        l_1513 = l_1513;
    }
    return p_10.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_20 g_3
 * writes: g_16 g_21
 */
static int64_t  func_12(int8_t  p_13, uint16_t  p_14)
{ /* block id: 2 */
    for (p_13 = 0; p_13 < 3; p_13 += 1)
    {
        g_16[p_13] = 0x2.Bp-1;
    }
    for (p_14 = (-29); (p_14 >= 47); p_14++)
    { /* block id: 6 */
        (*g_20) = p_13;
    }
    return g_3;
}


/* ------------------------------------------ */
/* 
 * reads : g_1018.f0 g_424 g_425 g_1034 g_793.f2 g_794.f4 g_463 g_119 g_51 g_791.f4 g_1049 g_147
 * writes: g_147 g_750 g_81 g_620 g_121 g_1176 g_463 g_1018.f0 g_1034 g_153
 */
static union U3  func_22(int32_t * p_23, int32_t * p_24, int64_t  p_25)
{ /* block id: 554 */
    struct S1 *l_1149 = (void*)0;
    const int32_t *l_1150 = &g_1018[1][0].f0;
    union U3 l_1163 = {-1L};
    uint64_t *l_1172 = &g_3;
    union U3 *l_1191[1];
    union U3 **l_1190 = &l_1191[0];
    uint32_t l_1203 = 0xB37EFB01L;
    int i;
    for (i = 0; i < 1; i++)
        l_1191[i] = &g_1049;
lbl_1180:
    for (g_147 = 0; (g_147 != 45); g_147 = safe_add_func_uint32_t_u_u(g_147, 9))
    { /* block id: 557 */
        const int32_t **l_1151 = (void*)0;
        const int32_t **l_1152[1];
        uint16_t *l_1173[10][2] = {{&g_74[1],&g_75[1][2]},{&g_75[1][2],&g_74[0]},{&g_75[1][2],&g_75[1][2]},{&g_74[1],&g_74[1]},{&g_75[1][2],&g_75[1][2]},{&g_74[0],&g_75[1][2]},{&g_75[1][2],&g_74[1]},{&g_74[1],&g_75[1][2]},{&g_75[1][2],&g_74[0]},{&g_75[1][2],&g_75[1][2]}};
        float *l_1174 = &g_121;
        float *l_1175 = &g_1176;
        uint8_t l_1177 = 251UL;
        int i, j;
        for (i = 0; i < 1; i++)
            l_1152[i] = &g_750;
        g_750 = ((l_1149 != (void*)0) , l_1150);
        (*l_1175) = (safe_sub_func_float_f_f((*l_1150), ((*l_1174) = (g_620[3][1] = (0xD.2C3BFBp-93 <= (safe_div_func_float_f_f((safe_add_func_float_f_f(((safe_mod_func_int8_t_s_s(((safe_add_func_int32_t_s_s((l_1163 , (safe_rshift_func_int8_t_s_u(((((*g_424) == (*g_424)) < (safe_div_func_uint8_t_u_u((g_1034 < (l_1163.f1 = (g_81 = ((((safe_mul_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u(0x29L, (*l_1150))), ((l_1172 != l_1172) != p_25))) & g_793[0][0].f2) | g_794.f4) >= p_25)))), p_25))) < p_25), 7))), g_1018[1][0].f0)) == 0x49FDL), (-6L))) , 0x8.BA1979p-4), 0x4.31170Cp+0)), p_25)))))));
        if (l_1177)
            continue;
    }
    for (g_463 = (-14); (g_463 != (-13)); g_463++)
    { /* block id: 568 */
        uint8_t l_1181 = 0x1CL;
        int32_t l_1182[1];
        uint64_t *l_1183 = &g_1034;
        int32_t l_1206 = 0x9D889154L;
        int32_t l_1207 = 0xD0558CEFL;
        int i;
        for (i = 0; i < 1; i++)
            l_1182[i] = 0xBBE9984DL;
        if (g_147)
            goto lbl_1180;
        (*p_24) = l_1181;
        l_1207 &= (((((*l_1183)++) & (safe_add_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(((void*)0 != l_1190), (safe_lshift_func_uint8_t_u_u((safe_sub_func_int32_t_s_s((l_1206 |= ((safe_add_func_int16_t_s_s((*l_1150), ((((safe_mod_func_uint16_t_u_u(l_1182[0], (safe_mul_func_int8_t_s_s((safe_unary_minus_func_int8_t_s((-6L))), ((l_1203 != (((*l_1150) || (~(~0x09L))) == (*l_1150))) , (*l_1150)))))) > 0x50L) || g_119) > 0L))) && g_51)), (*l_1150))), p_25)))), g_791[0][4].f4))) == (*l_1150)) < l_1182[0]);
    }
    return g_1049;
}


/* ------------------------------------------ */
/* 
 * reads : g_702 g_232 g_463 g_95 g_366.f0 g_153 g_51 g_620 g_143 g_147 g_929 g_750 g_115 g_534 g_607.f0 g_794.f6 g_1034 g_791.f2 g_3 g_1049 g_598.f1 g_214 g_513 g_1049.f0 g_793.f6 g_74 g_427 g_366.f2 g_523.f4 g_512 g_565 g_75 g_117 g_870 g_1092.f3 g_1141
 * writes: g_463 g_51 g_153 g_232 g_95 g_366.f0 g_512 g_794.f0 g_923 g_929 g_607.f0 g_794.f6 g_147 g_1034 g_793.f2 g_1049.f0 g_74 g_565 g_793.f6 g_121
 */
static int32_t * func_26(int32_t  p_27, int16_t  p_28, int32_t * p_29)
{ /* block id: 426 */
    const int64_t l_941 = (-1L);
    int32_t l_952 = 4L;
    int32_t l_973 = (-2L);
    uint32_t l_1001 = 0x3A269991L;
    uint8_t l_1028 = 0xB0L;
    int32_t **l_1056 = &g_232;
    uint8_t * const *l_1068 = (void*)0;
    uint8_t * const **l_1067[8][4] = {{&l_1068,&l_1068,&l_1068,&l_1068},{&l_1068,(void*)0,&l_1068,&l_1068},{&l_1068,&l_1068,&l_1068,&l_1068},{&l_1068,(void*)0,&l_1068,&l_1068},{&l_1068,&l_1068,&l_1068,&l_1068},{&l_1068,&l_1068,&l_1068,&l_1068},{&l_1068,(void*)0,&l_1068,&l_1068},{&l_1068,&l_1068,&l_1068,&l_1068}};
    int32_t *l_1105 = &l_952;
    int32_t l_1116 = (-9L);
    int32_t l_1117[3][4];
    int64_t l_1129 = 0x8E5E73443D1FE498LL;
    uint8_t l_1131 = 1UL;
    int32_t *l_1144[7] = {&g_153,&g_153,&l_952,&g_153,&g_153,&l_952,&g_153};
    int32_t *l_1145 = (void*)0;
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
            l_1117[i][j] = (-4L);
    }
    if (l_941)
    { /* block id: 427 */
        return (*g_702);
    }
    else
    { /* block id: 429 */
        uint16_t l_964 = 0xAF0CL;
        uint32_t l_996 = 4294967292UL;
        int32_t l_1005[4] = {0x13430529L,0x13430529L,0x13430529L,0x13430529L};
        volatile union U5 **l_1007 = &g_929;
        int i;
        for (g_463 = 1; (g_463 >= 0); g_463 -= 1)
        { /* block id: 432 */
            int32_t l_948[5];
            int i;
            for (i = 0; i < 5; i++)
                l_948[i] = 0L;
            for (g_51 = 0; (g_51 <= 1); g_51 += 1)
            { /* block id: 435 */
                int32_t *l_942 = &g_366.f0;
                int32_t l_962 = 0xBE8C1B01L;
                int32_t l_963[2];
                union U3 *l_968 = (void*)0;
                union U3 **l_967 = &l_968;
                uint32_t *l_1000 = (void*)0;
                uint32_t **l_999 = &l_1000;
                int i;
                for (i = 0; i < 2; i++)
                    l_963[i] = 0x6A2D6A71L;
                for (g_153 = 0; (g_153 <= 0); g_153 += 1)
                { /* block id: 438 */
                    int16_t l_961 = 0xF205L;
                    (*g_702) = l_942;
                    (*g_702) = &p_27;
                    for (g_95 = 0; (g_95 <= 1); g_95 += 1)
                    { /* block id: 443 */
                        int32_t *l_943 = &g_366.f0;
                        int32_t *l_944 = &g_366.f0;
                        int32_t *l_945 = &g_366.f0;
                        int32_t *l_946 = &g_366.f0;
                        int32_t *l_947 = &g_366.f0;
                        int32_t *l_949 = &l_948[4];
                        int32_t *l_950 = (void*)0;
                        int32_t *l_951 = &l_948[2];
                        int32_t *l_953 = &l_952;
                        int32_t *l_954 = &l_948[1];
                        int32_t *l_955 = &l_948[1];
                        int32_t *l_956 = &g_366.f0;
                        int32_t *l_957 = &g_366.f0;
                        int32_t *l_958 = (void*)0;
                        int32_t *l_959 = &l_948[3];
                        int32_t *l_960[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_960[i] = &l_948[1];
                        l_964--;
                    }
                }
                (*l_942) |= l_948[0];
                (*l_967) = (void*)0;
                if (l_948[1])
                    continue;
                for (l_952 = 0; (l_952 <= 1); l_952 += 1)
                { /* block id: 452 */
                    int32_t l_971 = 0xB4A598DBL;
                    uint32_t *l_995[4][6] = {{&g_91[5][3][0],&g_143[1][1],&g_91[2][4][1],&g_143[1][1],&g_143[1][1],&g_91[2][4][1]},{&g_91[5][3][0],&g_91[5][3][0],&g_143[1][1],&g_143[4][0],(void*)0,&g_143[4][0]},{&g_143[1][1],&g_91[5][3][0],&g_143[1][1],&g_91[2][4][1],&g_143[1][1],&g_143[1][1]},{&g_91[5][3][0],&g_143[1][1],&g_143[1][1],&g_91[5][3][0],&g_91[5][3][0],&g_143[4][0]}};
                    int16_t *l_1002 = &g_512[0][2][3];
                    int16_t *l_1003[1];
                    uint8_t l_1004 = 0x1BL;
                    int i, j;
                    for (i = 0; i < 1; i++)
                        l_1003[i] = &g_793[0][0].f6;
                    for (g_153 = 0; (g_153 <= 1); g_153 += 1)
                    { /* block id: 455 */
                        int16_t l_972 = 0x1057L;
                        p_27 = (((l_948[1] >= (safe_rshift_func_int8_t_s_s(l_971, 4))) , l_972) >= 0x59L);
                        if (l_972)
                            break;
                        l_973 |= 0x3B867809L;
                        if (l_952)
                            break;
                    }
                    if ((**g_702))
                        continue;
                    (*l_942) ^= (((void*)0 == &g_231[6][3][1]) , ((safe_lshift_func_int16_t_s_s(1L, 7)) , (safe_mod_func_uint8_t_u_u((l_1005[3] = ((safe_add_func_int16_t_s_s((safe_sub_func_int32_t_s_s((+(p_28 = ((*l_1002) = (safe_div_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_u(((((++l_996) & 0L) , (((0L || (l_999 != &g_816)) >= 0x07L) , g_620[(g_463 + 3)][l_952])) , l_971), 4)) >= 7UL), p_28)), 0)), p_27)) , l_1001), p_28))))), g_143[3][2])), 0x85ABL)) >= l_1004)), l_941))));
                }
            }
            for (g_794.f0 = 0; (g_794.f0 <= 1); g_794.f0 += 1)
            { /* block id: 471 */
                for (g_923 = 0; (g_923 <= 0); g_923 += 1)
                { /* block id: 474 */
                    int32_t *l_1006 = &g_366.f0;
                    (*g_702) = l_1006;
                    (*l_1006) = (g_147 , 0L);
                }
            }
        }
        (*l_1007) = g_929;
    }
    if ((*g_750))
    { /* block id: 482 */
        uint64_t ***l_1010 = (void*)0;
        uint64_t ***l_1012 = &g_870;
        uint64_t ****l_1011 = &l_1012;
        uint16_t *l_1015[5][4] = {{&g_74[1],&g_74[0],&g_74[1],&g_74[1]},{&g_74[0],&g_74[0],&g_74[2],&g_74[0]},{&g_74[0],&g_74[1],&g_74[1],&g_74[0]},{&g_74[1],&g_74[0],&g_74[1],&g_74[1]},{&g_74[0],&g_74[0],&g_74[2],&g_74[0]}};
        int32_t l_1016[9][5];
        union U4 *l_1017 = &g_1018[1][0];
        int32_t l_1021[4] = {0x80CFF565L,0x80CFF565L,0x80CFF565L,0x80CFF565L};
        uint8_t l_1022 = 4UL;
        int32_t *l_1023 = &g_51;
        uint16_t l_1096 = 0xE660L;
        uint8_t ***l_1102 = &g_276[2];
        int32_t *l_1104 = &l_952;
        int8_t l_1130 = (-1L);
        int16_t *l_1140 = &g_794.f6;
        int i, j;
        for (i = 0; i < 9; i++)
        {
            for (j = 0; j < 5; j++)
                l_1016[i][j] = (-1L);
        }
lbl_1031:
        (*l_1023) ^= (l_1022 = (safe_lshift_func_int8_t_s_u((l_1010 != ((*l_1011) = &g_870)), (safe_add_func_int8_t_s_s(((((l_1016[4][3] = g_115) != (g_534 == l_1017)) >= (safe_sub_func_uint8_t_u_u(l_1021[3], p_27))) && l_952), p_27)))));
        for (g_607.f0 = 0; (g_607.f0 <= 2); g_607.f0 += 1)
        { /* block id: 489 */
            int32_t *l_1024 = &l_1021[3];
            int32_t *l_1025 = (void*)0;
            int32_t *l_1026 = &g_51;
            int32_t *l_1027[7] = {&g_51,&g_51,&g_51,&g_51,&g_51,&g_51,&g_51};
            int32_t l_1052 = 3L;
            int i;
            l_1028++;
            for (l_1001 = 0; (l_1001 <= 2); l_1001 += 1)
            { /* block id: 493 */
                int32_t l_1033 = 0xBBD1EC84L;
                if (g_153)
                    goto lbl_1031;
                for (g_794.f6 = 3; (g_794.f6 >= 0); g_794.f6 -= 1)
                { /* block id: 497 */
                    uint8_t l_1050 = 0xE8L;
                    for (g_147 = 0; (g_147 <= 3); g_147 += 1)
                    { /* block id: 500 */
                        int32_t l_1032 = (-3L);
                        uint32_t *l_1051[2];
                        int8_t *l_1053 = (void*)0;
                        int8_t *l_1054 = (void*)0;
                        int8_t *l_1055 = &g_793[0][0].f2;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_1051[i] = &g_91[5][3][0];
                        --g_1034;
                        if (g_143[(l_1001 + 1)][g_607.f0])
                            continue;
                        (*l_1024) ^= (g_143[(g_147 + 1)][l_1001] == ((safe_sub_func_int32_t_s_s((((((&g_702 == (g_791[0][4].f2 , &g_231[g_147][g_607.f0][(g_147 + 1)])) , (safe_sub_func_uint16_t_u_u(0UL, 6L))) != ((*l_1055) = (safe_rshift_func_uint16_t_u_s(((((safe_mod_func_int16_t_s_s((g_3 | ((safe_mod_func_uint32_t_u_u((l_973 = ((safe_mod_func_uint8_t_u_u(((g_1049 , p_27) <= 0L), 250UL)) < l_1050)), (**g_702))) & 246UL)), g_598.f1)) != 0xE2EE6B277D324E3DLL) | (-1L)) >= l_1052), g_214[0])))) , g_513[2][4][6]) , (-7L)), l_1001)) , (*l_1023)));
                        (*l_1026) ^= ((void*)0 != l_1056);
                    }
                }
            }
            if ((*g_750))
                continue;
            if ((**g_702))
                break;
        }
        for (l_1001 = 6; (l_1001 == 15); l_1001 = safe_add_func_uint64_t_u_u(l_1001, 8))
        { /* block id: 515 */
            int64_t *l_1059[4][10] = {{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9]},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9]},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9]},{&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9],&g_513[1][4][9]}};
            uint32_t *l_1060 = &g_91[3][0][2];
            int32_t l_1073 = (-10L);
            const uint16_t l_1099 = 65535UL;
            int32_t l_1113 = 0xC90239F5L;
            int32_t l_1114[10][4] = {{(-5L),0x5F4C8A89L,0x5588DCEDL,0x342CE938L},{3L,0x5F7F0643L,(-7L),0x342CE938L},{0xF43A23A9L,0x5F4C8A89L,0x279B0077L,0x1FF20A53L},{0L,0xF43A23A9L,0L,0x55729AE5L},{(-7L),(-1L),0L,0x279B0077L},{0x1FF20A53L,0x3DCF7E35L,0L,(-7L)},{(-1L),0x342CE938L,0L,0x5588DCEDL},{0x1FF20A53L,0x5F7F0643L,0x55729AE5L,0x55729AE5L},{3L,3L,0x3DCF7E35L,(-1L)},{0x3DCF7E35L,(-1L),(-5L),(-3L)}};
            float l_1118 = (-0x3.7p-1);
            uint32_t l_1119 = 0x0B290C82L;
            int i, j;
            (*l_1056) = &p_27;
            for (l_952 = 0; (l_952 != 2); l_952++)
            { /* block id: 521 */
                float l_1094 = (-0x7.9p-1);
                int32_t l_1098 = 1L;
                int32_t *l_1107 = &l_1021[2];
                int32_t *l_1108 = &l_1073;
                int32_t *l_1109 = &l_1016[2][4];
                int32_t *l_1110 = &g_1018[1][0].f0;
                int32_t *l_1111 = &l_973;
                int32_t *l_1112[4];
                int16_t l_1115 = 0xB381L;
                int i;
                for (i = 0; i < 4; i++)
                    l_1112[i] = &g_51;
                if (g_115)
                    goto lbl_1031;
                if ((*l_1023))
                    continue;
                for (g_1049.f0 = 0; (g_1049.f0 < 6); g_1049.f0 = safe_add_func_uint8_t_u_u(g_1049.f0, 1))
                { /* block id: 526 */
                    struct S0 *l_1091[10] = {&g_1093,(void*)0,&g_1093,&g_1093,(void*)0,&g_1093,&g_1093,(void*)0,&g_1093,&g_1093};
                    int32_t l_1095 = 0xA1BE5371L;
                    int8_t *l_1097[5][3] = {{&g_791[0][4].f2,&g_791[0][4].f2,&g_791[0][4].f2},{&g_607.f0,&g_607.f0,&g_607.f0},{&g_791[0][4].f2,&g_791[0][4].f2,&g_791[0][4].f2},{&g_607.f0,&g_607.f0,&g_607.f0},{&g_791[0][4].f2,&g_791[0][4].f2,&g_791[0][4].f2}};
                    int16_t *l_1100 = (void*)0;
                    int16_t *l_1101 = &g_793[0][0].f6;
                    int i, j;
                    if (p_28)
                        break;
                    (*l_1023) = ((safe_mul_func_int8_t_s_s(g_793[0][0].f6, (&l_1060 == &g_816))) != (g_1049 , g_143[3][1]));
                    if ((l_1067[4][0] == ((((*l_1101) = ((((**l_1056) = (safe_lshift_func_uint8_t_u_s(0xDDL, 2))) & (((*l_1023) = l_1073) == ((g_565[4][1][1] &= ((((((g_513[1][4][9] , ((-4L) >= (safe_rshift_func_uint16_t_u_u(g_463, ((safe_div_func_uint64_t_u_u(((safe_div_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((l_1098 = (p_28 , (safe_unary_minus_func_uint16_t_u((safe_rshift_func_int16_t_s_u(((((((((safe_add_func_float_f_f((safe_add_func_float_f_f((((((g_74[0]++) | ((void*)0 == l_1091[5])) ^ (*g_427)) , 0xE.63925Fp-28) > p_28), l_1094)), l_1095)) >= p_28) == p_28) >= l_1095) , l_1096) >= p_28) , g_366.f2) != p_28), g_794.f6)))))), p_28)), l_1099)) != g_523.f4), g_607.f0)) != l_1095))))) ^ 0x53A0C1F5395F6693LL) & l_1099) & 0L) == 0xA8L) && g_512[2][1][4])) , 4294967295UL))) <= 65531UL)) && (*l_1023)) , l_1102)))
                    { /* block id: 535 */
                        int32_t *l_1103 = (void*)0;
                        return l_1103;
                    }
                    else
                    { /* block id: 537 */
                        int32_t *l_1106 = &g_51;
                        return l_1106;
                    }
                }
                l_1119++;
            }
            (*g_232) |= (-1L);
        }
        (*l_1104) ^= (safe_mod_func_int32_t_s_s((safe_add_func_int64_t_s_s(((~(((*l_1140) = (safe_mul_func_uint8_t_u_u(p_27, ((g_75[1][2] ^ (((g_117 , (((*g_870) == (void*)0) < ((++l_1131) | 9L))) > ((((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s((safe_add_func_uint8_t_u_u(0x25L, (p_27 , p_28))), p_28)), (**l_1056))) != p_27) <= 0x6BL) < 0x1588237DL)) , p_27)) , 1L)))) <= g_1092.f3)) & g_1141), (**g_870))), (**l_1056)));
    }
    else
    { /* block id: 548 */
        uint32_t l_1142 = 4294967288UL;
        int32_t *l_1143 = &g_153;
        (*g_232) = l_1142;
        return l_1143;
    }
    g_121 = (*l_1105);
    return l_1145;
}


/* ------------------------------------------ */
/* 
 * reads : g_366.f0 g_143 g_212 g_607 g_85 g_702 g_232 g_119 g_463 g_791.f0 g_794.f2 g_794.f4 g_510 g_153 g_51 g_75 g_793.f0 g_334.f0 g_929 g_523.f4 g_2308.f0
 * writes: g_607 g_153 g_51 g_510 g_463 g_794.f2 g_620 g_121 g_929
 */
static int32_t * func_30(const int16_t  p_31, const int32_t * p_32, uint16_t  p_33, int32_t * p_34)
{ /* block id: 331 */
    union U3 *l_763 = (void*)0;
    union U3 *l_764 = (void*)0;
    union U3 *l_765 = &g_607;
    int8_t l_782 = 3L;
    uint8_t l_785 = 0x83L;
    uint8_t *l_786 = &g_510[0];
    int32_t l_787 = 4L;
    uint64_t l_788 = 0xE83C21C5AC1E0481LL;
    struct S2 *l_792[2][2][2] = {{{&g_793[0][0],&g_793[0][0]},{(void*)0,&g_793[0][0]}},{{&g_793[0][0],(void*)0},{&g_793[0][0],&g_793[0][0]}}};
    const uint64_t **l_832 = (void*)0;
    const uint64_t ***l_831 = &l_832;
    int32_t l_874[9][8][3] = {{{0x05B59D90L,0x232E0F2FL,0x7FA4C889L},{2L,(-7L),3L},{1L,0x64C838D1L,1L},{3L,(-7L),2L},{0x7FA4C889L,0x232E0F2FL,0x05B59D90L},{1L,0L,(-1L)},{0x74430883L,0x74430883L,1L},{1L,1L,0xB849DD89L}},{{0x7FA4C889L,1L,1L},{3L,0x074A6DE7L,0x9345424AL},{1L,0x7FA4C889L,1L},{2L,0xEF00A8D3L,0xB849DD89L},{0x05B59D90L,0xE39C9E57L,1L},{(-1L),(-1L),(-1L)},{1L,0xE39C9E57L,0x05B59D90L},{0xB849DD89L,0xEF00A8D3L,2L}},{{1L,0x7FA4C889L,1L},{0x9345424AL,0x074A6DE7L,3L},{1L,1L,0x7FA4C889L},{0xB849DD89L,1L,1L},{1L,0x74430883L,0x74430883L},{(-1L),0L,1L},{0x05B59D90L,0x232E0F2FL,0x7FA4C889L},{2L,(-7L),3L}},{{1L,0x64C838D1L,1L},{3L,(-7L),2L},{0x7FA4C889L,0x232E0F2FL,0x05B59D90L},{1L,0L,(-1L)},{0x74430883L,0x74430883L,1L},{1L,1L,0xB849DD89L},{0x7FA4C889L,1L,1L},{3L,0x074A6DE7L,0x9345424AL}},{{1L,0x7FA4C889L,1L},{2L,0xEF00A8D3L,0xB849DD89L},{0x05B59D90L,0xE39C9E57L,1L},{(-1L),(-1L),(-1L)},{1L,0xE39C9E57L,0x05B59D90L},{0xB849DD89L,0xEF00A8D3L,2L},{1L,0x7FA4C889L,1L},{0x9345424AL,0x074A6DE7L,3L}},{{1L,1L,0x7FA4C889L},{0xB849DD89L,1L,1L},{1L,0x74430883L,0x74430883L},{(-1L),0L,1L},{0x05B59D90L,0x232E0F2FL,0x7FA4C889L},{2L,(-7L),0x9345424AL},{0xE39C9E57L,1L,0xE39C9E57L},{0x9345424AL,1L,(-1L)}},{{0x64C838D1L,1L,1L},{2L,(-1L),0x3EA4F30EL},{(-1L),(-1L),0x74430883L},{2L,0x074A6DE7L,0x6329DDFCL},{0x64C838D1L,0x74430883L,0x05B59D90L},{0x9345424AL,0x478362BAL,0xEFF9CFB6L},{0xE39C9E57L,0x64C838D1L,0x05B59D90L},{(-1L),0xD6D45E44L,0x6329DDFCL}},{{1L,1L,0x74430883L},{0x3EA4F30EL,(-4L),0x3EA4F30EL},{0x74430883L,1L,1L},{0x6329DDFCL,0xD6D45E44L,(-1L)},{0x05B59D90L,0x64C838D1L,0xE39C9E57L},{0xEFF9CFB6L,0x478362BAL,0x9345424AL},{0x05B59D90L,0x74430883L,0x64C838D1L},{0x6329DDFCL,0x074A6DE7L,2L}},{{0x74430883L,(-1L),(-1L)},{0x3EA4F30EL,(-1L),2L},{1L,1L,0x64C838D1L},{(-1L),1L,0x9345424AL},{0xE39C9E57L,1L,0xE39C9E57L},{0x9345424AL,1L,(-1L)},{0x64C838D1L,1L,1L},{2L,(-1L),0x3EA4F30EL}}};
    uint8_t l_915 = 0x60L;
    int i, j, k;
    if ((safe_div_func_int32_t_s_s((*p_32), (safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s(g_143[3][0], (safe_add_func_int64_t_s_s(3L, ((l_787 = (((safe_sub_func_uint64_t_u_u(((g_212[0][0][2] , (safe_add_func_uint8_t_u_u(((*l_786) = (((*l_765) = g_607) , (!(!(0x54L <= ((safe_rshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s(((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((((safe_mod_func_int16_t_s_s((((*p_32) < ((**g_702) = ((safe_div_func_uint32_t_u_u(((safe_mul_func_int8_t_s_s((l_782 ^ (safe_rshift_func_uint16_t_u_u(2UL, g_85))), l_782)) ^ l_782), l_785)) == p_33))) > p_33), 1UL)) , 0x03606F94FD004693LL) , 0UL), l_785)), l_782)) && 0xA111L), 0xDD38569DF7D274C0LL)), 14)) ^ p_33)))))), p_31))) ^ 0xE7L), 0xAD6C9055A70F7BB4LL)) & 0xF1D8C3C65C335E02LL) != l_782)) | l_788))))), g_119)))))
    { /* block id: 336 */
        int32_t *l_795[9][8][3] = {{{(void*)0,&g_366.f0,&l_787},{&g_366.f0,&g_366.f0,&g_153},{(void*)0,(void*)0,&l_787},{(void*)0,(void*)0,&g_51},{&g_153,&g_366.f0,&g_51},{(void*)0,(void*)0,&g_51},{&l_787,&g_153,&l_787},{&g_153,(void*)0,&g_153}},{{&g_153,&g_51,&l_787},{&g_153,&l_787,&l_787},{&g_51,&l_787,(void*)0},{&g_366.f0,&g_366.f0,&l_787},{&g_51,(void*)0,&g_366.f0},{&g_153,&g_51,(void*)0},{&g_153,&l_787,&g_51},{(void*)0,(void*)0,(void*)0}},{{&l_787,&l_787,&g_51},{&g_51,&l_787,&g_366.f0},{&l_787,&l_787,&l_787},{&g_153,(void*)0,&l_787},{&l_787,&l_787,&l_787},{&g_51,&l_787,(void*)0},{&l_787,&g_366.f0,&g_366.f0},{(void*)0,&g_51,(void*)0}},{{&g_153,(void*)0,&g_366.f0},{&g_153,(void*)0,&g_51},{&g_51,&g_366.f0,&g_153},{&g_366.f0,(void*)0,&g_153},{&g_51,&g_153,&l_787},{&g_153,&g_51,&g_51},{&g_153,&l_787,&g_366.f0},{&g_153,&g_51,&g_51}},{{&l_787,&g_153,&g_366.f0},{(void*)0,&g_366.f0,&l_787},{&g_153,&g_153,(void*)0},{(void*)0,&g_51,&g_51},{(void*)0,&l_787,(void*)0},{&g_51,(void*)0,(void*)0},{&g_366.f0,(void*)0,&l_787},{(void*)0,(void*)0,&l_787}},{{&l_787,&l_787,&g_153},{&g_153,(void*)0,&l_787},{(void*)0,&g_153,&l_787},{&l_787,&l_787,&g_153},{(void*)0,(void*)0,&g_366.f0},{&l_787,&l_787,&g_366.f0},{&l_787,(void*)0,&l_787},{&g_153,&g_153,(void*)0}},{{&g_51,&l_787,&l_787},{&g_153,&g_51,&g_366.f0},{&l_787,&l_787,&g_366.f0},{&g_51,&g_153,&g_153},{(void*)0,(void*)0,&l_787},{(void*)0,&g_51,&l_787},{&g_51,&g_366.f0,&g_153},{(void*)0,&g_51,&l_787}},{{&g_51,&g_366.f0,&l_787},{(void*)0,&g_366.f0,(void*)0},{&l_787,&l_787,&l_787},{(void*)0,&g_51,&g_366.f0},{&l_787,&g_153,&g_51},{&g_51,&g_51,&g_366.f0},{(void*)0,(void*)0,&g_366.f0},{&g_51,&g_366.f0,&g_51}},{{&l_787,&g_51,(void*)0},{(void*)0,&g_51,(void*)0},{&l_787,&l_787,&l_787},{(void*)0,&l_787,&l_787},{&g_51,&g_366.f0,&g_51},{(void*)0,&l_787,&g_51},{&g_51,&g_51,&l_787},{(void*)0,(void*)0,&g_51}}};
        int32_t l_797 = 0xC57E8E3AL;
        uint64_t **l_872 = &g_427;
        int i, j, k;
        for (g_463 = 0; (g_463 <= 0); g_463 += 1)
        { /* block id: 339 */
            int8_t l_789 = (-7L);
            int16_t l_811[4] = {0x65A1L,0x65A1L,0x65A1L,0x65A1L};
            int32_t * const l_813 = &g_153;
            uint32_t *l_847 = &g_143[1][1];
            uint32_t **l_846 = &l_847;
            int32_t l_876 = 8L;
            int32_t l_878 = (-1L);
            uint8_t l_879 = 255UL;
            int i;
            if (l_789)
                break;
        }
        return p_34;
    }
    else
    { /* block id: 397 */
        int32_t l_886 = 0x7F1584ACL;
        uint64_t *l_887[2][2][4] = {{{&l_788,&l_788,&l_788,&l_788},{&l_788,&l_788,&l_788,&l_788}},{{&l_788,&l_788,&l_788,&l_788},{&l_788,&l_788,&l_788,&l_788}}};
        int8_t l_891 = 1L;
        int64_t l_892 = 7L;
        int8_t *l_916 = (void*)0;
        int8_t *l_917 = (void*)0;
        int8_t *l_918 = &g_794.f2;
        float *l_919 = &g_620[3][1];
        float *l_920 = &g_121;
        uint32_t l_926[10] = {0xF455D7CBL,0xF455D7CBL,0xF455D7CBL,0xF455D7CBL,0xF455D7CBL,0xF455D7CBL,0xF455D7CBL,0xF455D7CBL,0xF455D7CBL,0xF455D7CBL};
        uint64_t *****l_939[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int i, j, k;
        (*g_232) &= (safe_add_func_int16_t_s_s((g_791[0][4].f0 || (safe_add_func_uint16_t_u_u((((g_794.f2 > (l_886 >= (l_887[0][1][1] == (void*)0))) > (~p_33)) ^ g_794.f4), (((*l_786) ^= (l_874[8][4][1] < (safe_lshift_func_int16_t_s_s((((*p_32) | l_886) || 0x2973L), 4)))) != l_891)))), l_892));
        (*l_920) = ((*l_919) = (0x1.Fp-1 > ((safe_sub_func_float_f_f(((safe_sub_func_float_f_f((0x6.5p-1 <= 0x7.8p+1), (safe_sub_func_float_f_f(p_33, (safe_div_func_float_f_f((safe_mul_func_float_f_f((safe_sub_func_float_f_f(p_31, (safe_div_func_float_f_f((safe_add_func_float_f_f((((*l_918) = (0x93L & (l_915 = (safe_lshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s(g_75[1][2], ((safe_mul_func_int16_t_s_s(0L, ((g_607 , l_785) , p_31))) || l_891))), l_891))))) , 0x0.8p+1), p_33)), g_793[0][0].f0)))), p_33)), l_892)))))) != l_788), g_334.f0)) < p_31)));
        for (l_892 = 3; (l_892 <= 1); l_892 = safe_sub_func_uint8_t_u_u(l_892, 3))
        { /* block id: 406 */
            int32_t *l_924 = &l_886;
            int32_t *l_925[4];
            int i;
            for (i = 0; i < 4; i++)
                l_925[i] = (void*)0;
            l_926[6]++;
            for (p_33 = 0; (p_33 <= 0); p_33 += 1)
            { /* block id: 410 */
                int8_t l_937 = 0x65L;
                int32_t l_940 = 3L;
                if ((**g_702))
                { /* block id: 411 */
                    g_929 = g_929;
                }
                else
                { /* block id: 413 */
                    const int8_t l_938 = 0x31L;
                    l_940 |= (safe_lshift_func_int8_t_s_s(1L, (safe_add_func_int16_t_s_s((safe_rshift_func_int16_t_s_u((((*g_232) |= ((*l_924) = ((g_119 < l_937) ^ (l_938 == ((*l_786) = 1UL))))) == g_523.f4), (((l_939[1] != (void*)0) , (-1L)) ^ 0x1950477DL))), p_31))));
                    if ((**g_702))
                        continue;
                    return p_34;
                }
            }
        }
    }
    p_34 = (*g_702);
    return (*g_702);
}


/* ------------------------------------------ */
/* 
 * reads : g_51 g_3 g_81 g_85 g_91 g_95 g_119 g_117 g_121 g_147 g_74 g_75 g_153 g_181 g_143 g_112 g_231 g_232 g_234 g_465 g_502 g_512 g_513 g_366.f0 g_510 g_534 g_548.f0 g_523.f1 g_565 g_575 g_595 g_607 g_334.f3 g_366.f2 g_523.f3 g_334.f1 g_576.f1 g_334.f0 g_702 g_598.f0 g_334.f4 g_607.f0 g_750
 * writes: g_74 g_75 g_81 g_85 g_91 g_95 g_112 g_121 g_143 g_147 g_153 g_181 g_212 g_214 g_234 g_232 g_502 g_510 g_512 g_513 g_51 g_231 g_565 g_575 g_595 g_620 g_702 g_463 g_607.f0 g_739
 */
static const int32_t * func_35(float  p_36, const int32_t * p_37, uint8_t  p_38)
{ /* block id: 10 */
    const uint32_t l_39 = 4294967289UL;
    int32_t *l_50 = &g_51;
    uint16_t *l_73 = &g_74[0];
    int8_t l_515 = 0x02L;
    uint32_t l_741 = 0xD672F89AL;
    int16_t l_742 = 0x6E4EL;
    uint8_t *l_747 = &g_510[0];
    int32_t *l_748 = (void*)0;
    int32_t l_749 = 0x4E8831A9L;
    l_742 = (l_39 == ((*l_50) = (safe_mod_func_uint16_t_u_u((safe_add_func_int8_t_s_s((((func_44(l_50, p_38, ((((p_38 , ((*l_50) = (safe_mul_func_uint8_t_u_u((((safe_mul_func_int16_t_s_s((*l_50), func_56(((((g_75[1][2] = (((((*l_50) >= ((safe_unary_minus_func_int32_t_s((safe_add_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_s(((safe_div_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_s(((*l_73) = (((safe_mod_func_uint64_t_u_u((*l_50), (((*l_50) != ((((l_50 == l_50) | (*p_37)) < g_3) <= (*l_50))) ^ 0x916DL))) , (*l_50)) , 65529UL)), g_51)), g_51)) == (*l_50)), 3)) >= g_3), p_38)))) <= g_3)) >= 0x74L) <= 1L) , (*p_37))) < p_38) , g_51) ^ 4L), (*l_50), &g_51, (*l_50), &g_51))) > (*l_50)) <= p_38), l_515)))) == (*p_37)) < p_38) <= 0xF4L), g_117, p_38) >= (-1L)) & l_515) > l_741), 0UL)), g_366.f2))));
    l_749 &= ((*l_50) | ((*l_747) = ((((249UL >= (*l_50)) <= (safe_rshift_func_int16_t_s_s(((void*)0 == &l_50), 8))) == 0xB9L) , (--p_38))));
    return g_750;
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_510 g_534 g_95 g_51 g_232 g_153 g_143 g_548.f0 g_513 g_74 g_523.f1 g_75 g_565 g_575 g_595 g_607 g_334.f3 g_366.f2 g_366.f0 g_523.f3 g_81 g_85 g_117 g_334.f1 g_576.f1 g_334.f0 g_702 g_598.f0 g_334.f4 g_607.f0
 * writes: g_510 g_95 g_51 g_231 g_502 g_153 g_81 g_513 g_74 g_565 g_575 g_595 g_121 g_620 g_702 g_463 g_607.f0 g_232 g_739
 */
static int32_t  func_44(int32_t * p_45, int16_t  p_46, int32_t  p_47, int32_t  p_48, float  p_49)
{ /* block id: 220 */
    uint8_t *l_518 = &g_510[0];
    struct S0 *l_522 = &g_523;
    struct S0 **l_521 = &l_522;
    int32_t l_542 = 0xB74D7A7CL;
    const union U4 *l_547 = &g_548;
    int32_t l_549[8][5][6] = {{{0x471D3CF5L,0x0EB4EEB7L,0x29B50F9AL,(-4L),0x288E46E9L,0xDB6B2C28L},{0x288E46E9L,0xF7A04CECL,(-4L),0x5009F199L,0x1827B0D8L,5L},{0x652B31FEL,0xCBC252F9L,1L,0x723C9A92L,6L,0x48941D91L},{0L,0xF3CB8A0EL,0L,0x723C9A92L,(-7L),(-5L)},{0x652B31FEL,0x288E46E9L,(-1L),0x5009F199L,0x6DB85AE8L,2L}},{{0x288E46E9L,0x0762A8CEL,0L,(-4L),0L,0x5C398C03L},{0x471D3CF5L,0x1827B0D8L,1L,0x3CCBCB50L,1L,(-3L)},{0xD0F72359L,(-1L),2L,0xD0F72359L,(-2L),0xADB81A0BL},{1L,0x48A583ADL,1L,0L,0xD5FB5604L,0x6DB85AE8L},{0L,4L,(-7L),1L,0xD5FB5604L,0L}},{{0x8653980FL,0x48A583ADL,0x1827B0D8L,0xA5ECDE77L,(-2L),0x288E46E9L},{0xC28DA5A7L,0x4CBF84ABL,0xD0F72359L,(-1L),0x8653980FL,0x723C9A92L},{0x8A9AB692L,1L,0x471D3CF5L,0x8653980FL,(-10L),(-1L)},{0L,0L,0x288E46E9L,0L,0L,0x0EB4EEB7L},{8L,(-1L),0x652B31FEL,0x48A583ADL,0L,0x0826E457L}},{{0xC835080BL,0xA5ECDE77L,0L,(-1L),(-3L),0x0826E457L},{7L,0x6B3D9169L,0x652B31FEL,0x4DC5743FL,1L,0x0EB4EEB7L},{(-3L),0xD5FB5604L,0x288E46E9L,7L,(-1L),(-1L)},{5L,0x31126C65L,0x471D3CF5L,4L,0x22A4986DL,0x723C9A92L},{0x398337A3L,0x753899EDL,0xD0F72359L,(-8L),0x753899EDL,0x288E46E9L}},{{0x4DC5743FL,0L,0x1827B0D8L,0xD5FB5604L,0xC28DA5A7L,0L},{0x6B3D9169L,0x8653980FL,(-7L),0x753899EDL,4L,0x6DB85AE8L},{0x6B3D9169L,0xCCC6E8BAL,1L,0xD5FB5604L,5L,0xADB81A0BL},{0x4DC5743FL,0xC28DA5A7L,(-1L),(-8L),(-1L),0xE4A1B07BL},{0x398337A3L,3L,1L,4L,0L,0L}},{{5L,0x338F6B5BL,0x6DB85AE8L,7L,3L,(-1L)},{(-3L),(-8L),(-10L),0x4DC5743FL,4L,(-1L)},{7L,0x8A9AB692L,0x723C9A92L,(-1L),0xA5ECDE77L,0x73A21AEFL},{0xC835080BL,0x8A9AB692L,0x0826E457L,0x48A583ADL,4L,0L},{8L,(-8L),6L,0L,3L,0x471D3CF5L}},{{0L,0x338F6B5BL,0xADB81A0BL,0x8653980FL,0L,(-4L)},{0x8A9AB692L,3L,(-5L),(-1L),(-1L),(-5L)},{0xC28DA5A7L,0xC28DA5A7L,4L,0xA5ECDE77L,5L,(-8L)},{0x8653980FL,0xCCC6E8BAL,0xCBC252F9L,1L,4L,4L},{0L,0x8653980FL,0xCBC252F9L,0L,0xC28DA5A7L,(-8L)}},{{1L,0L,4L,4L,0x753899EDL,(-5L)},{4L,0x753899EDL,(-5L),(-2L),0x22A4986DL,(-4L)},{(-8L),0x31126C65L,0xADB81A0BL,(-4L),(-1L),0x471D3CF5L},{(-1L),0xD5FB5604L,6L,0x3C6D9DC3L,1L,0L},{1L,0x6B3D9169L,0x0826E457L,5L,(-6L),0xC290B173L}}};
    uint64_t * const * const l_568[6] = {&g_427,&g_427,&g_427,&g_427,&g_427,&g_427};
    int32_t l_648 = 0xF0B54B9AL;
    int32_t l_672[3];
    int32_t l_675 = 0x50A32F23L;
    int32_t l_676 = 0x279B7471L;
    int32_t l_677 = 0xDB29A496L;
    int32_t l_679[7][3][1] = {{{1L},{0x967D1C70L},{1L}},{{0x644DBB93L},{1L},{0x967D1C70L}},{{1L},{0x644DBB93L},{1L}},{{0x967D1C70L},{1L},{0x644DBB93L}},{{1L},{0x967D1C70L},{1L}},{{0x644DBB93L},{1L},{0x967D1C70L}},{{1L},{0x644DBB93L},{1L}}};
    uint8_t ***l_696 = &g_276[1];
    uint8_t ****l_695 = &l_696;
    int16_t l_707 = 0x5172L;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_672[i] = 0x2B68AB68L;
    if ((g_3 == (safe_lshift_func_uint8_t_u_u(0x80L, ((*l_518)++)))))
    { /* block id: 222 */
        struct S0 ***l_524[4][1][1];
        int i, j, k;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 1; k++)
                    l_524[i][j][k] = &l_521;
            }
        }
        for (p_48 = 0; p_48 < 9; p_48 += 1)
        {
            for (g_95 = 0; g_95 < 4; g_95 += 1)
            {
                for (g_51 = 0; g_51 < 5; g_51 += 1)
                {
                    g_231[p_48][g_95][g_51] = &g_232;
                }
            }
        }
        l_521 = l_521;
    }
    else
    { /* block id: 225 */
        const int8_t *l_525 = &g_502;
        int32_t l_532 = 8L;
        int8_t *l_533 = &g_502;
        uint8_t ** const l_545 = &l_518;
        int16_t *l_546 = &g_95;
        uint64_t *l_550[9] = {&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3,&g_3};
        uint64_t **l_558 = &g_427;
        struct S0 * const **l_600 = &g_596[7];
        const union U3 l_627 = {1L};
        uint8_t ***l_638 = &g_276[1];
        int32_t l_682 = (-8L);
        int8_t l_684 = 0xF0L;
        uint64_t ***l_694 = &l_558;
        uint64_t ****l_693 = &l_694;
        uint64_t *****l_692 = &l_693;
        uint8_t *****l_697 = &l_695;
        int32_t **l_700 = &g_232;
        int32_t ***l_701 = &l_700;
        uint16_t *l_703 = &g_74[1];
        int64_t *l_704 = &g_463;
        int64_t *l_705 = &g_513[1][4][9];
        int32_t *l_706[5] = {&l_676,&l_676,&l_676,&l_676,&l_676};
        int i;
lbl_574:
        (*p_45) = ((*g_232) ^= ((((((l_525 == (((l_532 = (safe_lshift_func_uint16_t_u_u((((safe_mul_func_uint16_t_u_u((safe_add_func_int8_t_s_s(((*l_533) = l_532), ((g_534 != ((safe_mul_func_int8_t_s_s((safe_add_func_int16_t_s_s(((*l_546) |= (0L >= (safe_sub_func_uint16_t_u_u(l_542, (safe_mul_func_uint16_t_u_u(((void*)0 == l_545), p_47)))))), 0UL)), 6L)) , l_547)) <= (-1L)))), 65535UL)) && l_549[6][2][3]) != p_48), 4))) == 0x0FA7B70260D5B529LL) , (void*)0)) < p_46) ^ p_47) , l_532) && l_532) , (*p_45)));
        for (g_95 = 26; (g_95 <= (-25)); g_95 = safe_sub_func_int8_t_s_s(g_95, 2))
        { /* block id: 233 */
            uint16_t *l_555 = &g_81;
            int64_t *l_561 = &g_513[1][1][1];
            uint16_t *l_564[10][5] = {{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]},{&g_74[0],&g_74[1],&g_75[2][4],&g_75[2][4],&g_74[1]}};
            const int32_t l_615 = 6L;
            uint8_t ***l_637 = &g_276[1];
            int32_t l_671 = 0L;
            int8_t l_673 = 0xB9L;
            int32_t l_680[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
            int32_t l_683 = 0xE18D10A7L;
            uint64_t l_685 = 0UL;
            int i, j;
            (*g_232) ^= (safe_lshift_func_int16_t_s_u((l_532 < ((*l_555) = (g_143[2][1] | 0L))), (g_565[4][1][1] &= ((safe_mod_func_int32_t_s_s((((l_558 != (((*l_561) &= (safe_add_func_int16_t_s_s(g_548.f0, p_48))) , (p_48 , &l_550[2]))) < ((g_74[0] ^= (safe_div_func_int32_t_s_s((*p_45), 0x4A58D5EAL))) != g_523.f1)) | p_47), 0xB0921E0AL)) <= g_75[2][8]))));
            for (l_532 = 0; (l_532 != (-24)); --l_532)
            { /* block id: 241 */
                uint64_t * const *l_570 = &g_427;
                uint64_t * const **l_569[2][6][7] = {{{&l_570,&l_570,&l_570,&l_570,(void*)0,&l_570,&l_570},{(void*)0,&l_570,&l_570,&l_570,&l_570,(void*)0,&l_570},{(void*)0,&l_570,&l_570,&l_570,&l_570,(void*)0,(void*)0},{&l_570,&l_570,&l_570,&l_570,&l_570,&l_570,(void*)0},{&l_570,&l_570,(void*)0,&l_570,&l_570,(void*)0,&l_570},{(void*)0,&l_570,&l_570,(void*)0,&l_570,&l_570,&l_570}},{{&l_570,&l_570,&l_570,(void*)0,&l_570,&l_570,(void*)0},{&l_570,(void*)0,&l_570,&l_570,(void*)0,&l_570,&l_570},{(void*)0,&l_570,&l_570,&l_570,&l_570,&l_570,&l_570},{(void*)0,(void*)0,&l_570,&l_570,&l_570,&l_570,(void*)0},{&l_570,(void*)0,&l_570,&l_570,&l_570,&l_570,(void*)0},{&l_570,&l_570,(void*)0,&l_570,&l_570,&l_570,&l_570}}};
                uint64_t * const **l_571 = &l_570;
                struct S0 * const **l_602[4] = {&g_596[7],&g_596[7],&g_596[7],&g_596[7]};
                int32_t l_618 = 0L;
                uint8_t ***l_641[5];
                uint32_t *l_651 = &g_212[0][3][0];
                uint32_t **l_650 = &l_651;
                int32_t l_674 = 0x6A6E33CEL;
                int32_t l_678 = (-3L);
                int32_t l_681[9][9] = {{0x35024448L,0x92A65D73L,(-9L),(-6L),1L,0x68DD8D20L,(-10L),0x92A65D73L,0xBDF748E9L},{0x35024448L,0xF0E620C4L,(-10L),(-6L),0x92A65D73L,(-6L),(-10L),0xF0E620C4L,0x35024448L},{0x35024448L,1L,0x1FB0C371L,(-6L),0xF0E620C4L,0x99998796L,(-10L),1L,0xCB76F13FL},{0x35024448L,0x92A65D73L,(-9L),(-6L),1L,0x68DD8D20L,(-10L),0x92A65D73L,0xBDF748E9L},{0x35024448L,0xF0E620C4L,(-10L),(-6L),0x92A65D73L,(-6L),(-10L),0xF0E620C4L,0x35024448L},{0x35024448L,1L,0x1FB0C371L,(-6L),0xF0E620C4L,0x99998796L,(-10L),1L,0xCB76F13FL},{0x35024448L,0x92A65D73L,(-9L),(-6L),1L,0x68DD8D20L,(-10L),0x92A65D73L,0xBDF748E9L},{0x35024448L,0xF0E620C4L,(-10L),(-6L),0x92A65D73L,(-6L),(-10L),0xF0E620C4L,0x35024448L},{0x35024448L,1L,0x1FB0C371L,(-6L),0xF0E620C4L,0x99998796L,(-10L),1L,0xCB76F13FL}};
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_641[i] = &g_276[1];
                (*l_571) = l_568[5];
                (*p_45) = (*p_45);
                for (g_51 = 14; (g_51 >= 21); g_51 = safe_add_func_int64_t_s_s(g_51, 7))
                { /* block id: 246 */
                    struct S1 **l_577 = (void*)0;
                    struct S1 **l_578 = &g_575;
                    struct S1 *l_580 = (void*)0;
                    struct S1 **l_579 = &l_580;
                    uint16_t *l_614 = &g_75[1][2];
                    int32_t l_616 = 0x74166F3FL;
                    if (g_548.f0)
                        goto lbl_574;
                    (*l_579) = ((*l_578) = g_575);
                    for (p_48 = 16; (p_48 > 23); p_48 = safe_add_func_int8_t_s_s(p_48, 7))
                    { /* block id: 252 */
                        struct S0 * const ***l_599 = &g_595;
                        struct S0 * const ***l_601[2];
                        float *l_617 = &g_121;
                        float *l_619 = &g_620[3][1];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_601[i] = &l_600;
                        (*l_619) = (safe_add_func_float_f_f((p_47 == (safe_mul_func_float_f_f((0x9.CA8117p-21 > (safe_sub_func_float_f_f(((safe_add_func_float_f_f((p_48 < (((*l_545) = l_518) == (void*)0)), ((*l_617) = (safe_div_func_float_f_f(((((safe_div_func_int64_t_s_s(((((*l_599) = g_595) == (l_602[3] = l_600)) != (safe_lshift_func_uint8_t_u_u(((((safe_div_func_int16_t_s_s((g_607 , (safe_add_func_uint16_t_u_u(((g_502 = (safe_div_func_int8_t_s_s(((safe_add_func_uint8_t_u_u(((l_614 = (void*)0) != l_546), 0x06L)) != g_334.f3), p_46))) , l_615), p_47))), p_48)) && g_366.f2) , (-8L)) > g_366.f0), l_616))), l_616)) , g_366.f0) , (-0x1.1p+1)) >= 0xC.92CCFFp+12), 0xF.57AF3Ep+43))))) > 0xC.5F78FBp-89), l_618))), g_523.f3))), p_47));
                    }
                    for (l_542 = (-10); (l_542 < (-30)); l_542 = safe_sub_func_uint8_t_u_u(l_542, 1))
                    { /* block id: 263 */
                        int8_t l_632 = (-1L);
                        uint8_t ****l_639 = (void*)0;
                        uint8_t ****l_640[2][7][6] = {{{&l_638,&l_638,(void*)0,&l_638,(void*)0,&l_638},{&l_638,&l_638,(void*)0,&l_638,&l_637,&l_638},{&l_638,&l_638,&l_638,&l_638,&l_638,&l_638},{&l_638,&l_638,&l_638,&l_638,&l_637,(void*)0},{(void*)0,&l_638,&l_638,&l_638,(void*)0,&l_638},{(void*)0,&l_638,&l_638,&l_638,&l_638,&l_638},{&l_638,&l_637,&l_637,&l_638,&l_638,&l_638}},{{&l_638,&l_638,&l_638,&l_638,&l_638,&l_638},{&l_638,&l_638,&l_638,&l_638,&l_638,(void*)0},{&l_638,&l_638,&l_638,&l_638,&l_638,&l_638},{&l_637,&l_637,&l_638,&l_638,&l_638,&l_638},{&l_638,&l_638,(void*)0,&l_638,(void*)0,&l_638},{&l_638,&l_638,(void*)0,&l_638,&l_637,&l_638},{&l_638,&l_638,&l_638,&l_638,&l_638,&l_638}}};
                        int32_t l_649 = 0xE735167CL;
                        int i, j, k;
                        if ((*p_45))
                            break;
                        (*g_232) |= (safe_mul_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s(0x9AFDL, (((2L && ((l_627 , (safe_sub_func_int8_t_s_s(0x11L, ((l_632 = (--(*l_555))) <= ((safe_lshift_func_int8_t_s_s((g_565[4][1][1] = ((*l_533) = ((safe_div_func_int64_t_s_s((((l_637 != (l_641[0] = l_638)) , (1UL < (safe_mod_func_int8_t_s_s((safe_sub_func_int64_t_s_s((-10L), (((l_648 = (safe_rshift_func_int8_t_s_s(p_48, l_627.f0))) != l_616) , l_649))), 1UL)))) != p_46), p_46)) || (*p_45)))), l_615)) , (-1L)))))) != g_85)) == p_47) > p_48))), g_117));
                        l_650 = l_650;
                        l_616 = ((safe_add_func_float_f_f((safe_div_func_float_f_f((((-0x1.3p-1) > g_334.f1) == 0x1.7p+1), p_48)), l_649)) > (safe_div_func_float_f_f((safe_mul_func_float_f_f((-p_47), 0x4.A9FA8Ap+21)), ((((safe_add_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u((g_576.f1 < (((void*)0 != &p_46) != 0x32201C15L)), l_615)) ^ 0xC4B6L), g_334.f0)) , l_615) , l_618) == 0x1.2p+1))));
                    }
                }
                for (l_542 = 0; (l_542 > (-25)); l_542 = safe_sub_func_int32_t_s_s(l_542, 4))
                { /* block id: 278 */
                    int32_t *l_667 = &l_618;
                    int32_t *l_668 = &g_366.f0;
                    int32_t *l_669 = &l_648;
                    int32_t *l_670[4][9][1] = {{{(void*)0},{(void*)0},{&g_366.f0},{&l_648},{&g_366.f0},{(void*)0},{(void*)0},{&g_366.f0},{&l_648}},{{&g_366.f0},{(void*)0},{(void*)0},{&g_366.f0},{&l_648},{&g_366.f0},{(void*)0},{(void*)0},{&g_366.f0}},{{&l_648},{&g_366.f0},{(void*)0},{(void*)0},{&g_366.f0},{&l_648},{&g_366.f0},{&g_153},{&g_153}},{{&l_648},{(void*)0},{&l_648},{&g_153},{&g_153},{&l_648},{(void*)0},{&l_648},{&g_153}}};
                    int i, j, k;
                    l_685--;
                }
            }
            return l_673;
        }
        l_648 &= ((*p_45) ^= (((*l_705) ^= ((*l_704) = (((safe_div_func_int32_t_s_s((((safe_mul_func_int8_t_s_s(((void*)0 != l_692), ((&l_638 == ((*l_697) = l_695)) >= p_47))) , ((*l_546) = (safe_sub_func_uint64_t_u_u((0xEACFL <= ((*l_703) = ((((*l_701) = l_700) != (g_702 = g_702)) < ((g_334.f1 | p_47) , 0x07L)))), l_672[1])))) && p_46), l_679[4][1][0])) && l_677) && (***l_701)))) && (-6L)));
    }
    if (l_707)
    { /* block id: 294 */
        uint8_t l_733 = 0UL;
lbl_736:
        for (l_676 = 0; (l_676 > (-20)); --l_676)
        { /* block id: 297 */
            (*g_232) = (*g_232);
        }
        for (g_81 = 26; (g_81 < 45); ++g_81)
        { /* block id: 302 */
            int8_t *l_721 = &g_607.f0;
            int32_t l_722 = 0L;
            for (p_48 = 0; (p_48 <= 1); p_48 += 1)
            { /* block id: 305 */
                return (**g_702);
            }
            if ((safe_add_func_uint64_t_u_u((safe_sub_func_int8_t_s_s(p_47, ((0xB1L != (safe_mul_func_uint8_t_u_u(1UL, (~(g_598.f0 , ((*l_721) |= ((((safe_add_func_int16_t_s_s(g_334.f4, p_46)) || p_47) , (-1L)) ^ (*p_45)))))))) || (**g_702)))), l_722)))
            { /* block id: 309 */
                int64_t l_731 = (-1L);
                (*g_702) = p_45;
                for (l_675 = 0; (l_675 == 3); ++l_675)
                { /* block id: 313 */
                    int32_t *l_725 = &l_677;
                    int32_t *l_726 = &l_679[3][0][0];
                    int32_t *l_727 = (void*)0;
                    int32_t *l_728 = &l_677;
                    int32_t *l_729 = &l_677;
                    int32_t *l_730[3][7][4] = {{{&g_366.f0,&g_366.f0,&l_722,&l_677},{&l_542,&l_676,&l_648,&l_679[0][0][0]},{&l_542,&g_51,&g_366.f0,&l_722},{&g_366.f0,&g_51,&g_366.f0,&l_679[0][0][0]},{&l_679[0][0][0],&l_722,&g_51,&g_366.f0},{(void*)0,&l_542,(void*)0,&l_679[1][1][0]},{&g_153,&l_679[0][0][0],&l_542,&g_366.f0}},{{&l_676,&g_366.f0,&l_679[0][0][0],&l_679[0][0][0]},{&l_648,&l_679[6][2][0],&l_679[0][0][0],&l_676},{&l_676,(void*)0,&l_542,(void*)0},{&g_153,&l_542,(void*)0,&l_542},{(void*)0,&l_542,&g_51,(void*)0},{&l_679[0][0][0],&l_648,&g_366.f0,&g_366.f0},{&g_366.f0,&g_366.f0,&g_366.f0,&l_542}},{{&l_542,&l_679[1][1][0],&l_648,&g_51},{&l_542,(void*)0,&l_722,&l_648},{&g_366.f0,(void*)0,&l_679[6][2][0],&g_51},{(void*)0,&l_679[1][1][0],&l_648,&l_542},{&l_679[3][0][0],&g_366.f0,&g_51,&g_366.f0},{&l_722,&l_648,&g_366.f0,(void*)0},{&g_51,&l_542,&g_153,&l_542}}};
                    int32_t l_732 = 0x4C94EB04L;
                    int i, j, k;
                    --l_733;
                }
            }
            else
            { /* block id: 316 */
                (*g_702) = (*g_702);
            }
            if (l_677)
                goto lbl_736;
        }
    }
    else
    { /* block id: 321 */
        struct S0 ***l_738 = &l_521;
        struct S0 ****l_737[7][10] = {{(void*)0,&l_738,&l_738,&l_738,(void*)0,&l_738,(void*)0,&l_738,&l_738,&l_738},{&l_738,&l_738,&l_738,&l_738,&l_738,&l_738,(void*)0,&l_738,&l_738,&l_738},{&l_738,&l_738,(void*)0,&l_738,&l_738,&l_738,&l_738,(void*)0,&l_738,&l_738},{&l_738,&l_738,&l_738,(void*)0,&l_738,&l_738,&l_738,&l_738,&l_738,&l_738},{(void*)0,&l_738,&l_738,&l_738,&l_738,&l_738,&l_738,&l_738,(void*)0,&l_738},{&l_738,&l_738,&l_738,&l_738,&l_738,(void*)0,&l_738,&l_738,&l_738,&l_738},{&l_738,(void*)0,&l_738,&l_738,&l_738,&l_738,(void*)0,&l_738,(void*)0,&l_738}};
        int i, j;
        g_739[4] = &l_521;
    }
    return l_549[1][2][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_81 g_85 g_91 g_95 g_119 g_117 g_121 g_3 g_147 g_51 g_74 g_75 g_153 g_181 g_143 g_112 g_231 g_232 g_234 g_465 g_502 g_512 g_513 g_366.f0
 * writes: g_81 g_85 g_91 g_95 g_112 g_121 g_143 g_147 g_74 g_153 g_181 g_212 g_214 g_234 g_75 g_232 g_502 g_510 g_512 g_513
 */
static uint16_t  func_56(uint32_t  p_57, uint32_t  p_58, int32_t * p_59, float  p_60, int32_t * p_61)
{ /* block id: 13 */
    int32_t l_76 = 0x8DAE6E92L;
    uint8_t *l_89[3];
    const uint8_t *l_118 = &g_119;
    uint8_t l_122[4] = {0xF9L,0xF9L,0xF9L,0xF9L};
    int32_t l_123[3][9] = {{0xC869DFF3L,8L,0xC869DFF3L,8L,0xC869DFF3L,8L,0xC869DFF3L,8L,0xC869DFF3L},{0xFBD3DF35L,0xFBD3DF35L,0x2EAD1D30L,0x2EAD1D30L,0xFBD3DF35L,0xFBD3DF35L,0x2EAD1D30L,0x2EAD1D30L,0xFBD3DF35L},{(-10L),8L,(-10L),8L,(-10L),8L,(-10L),8L,(-10L)}};
    uint16_t l_244 = 65533UL;
    uint16_t l_266 = 65529UL;
    float l_356 = 0x0.Fp+1;
    union U4 *l_365 = &g_366;
    int32_t *l_382 = &l_123[2][4];
    uint64_t *l_387[5] = {&g_3,&g_3,&g_3,&g_3,&g_3};
    uint64_t **l_386[5] = {&l_387[4],&l_387[4],&l_387[4],&l_387[4],&l_387[4]};
    uint64_t ***l_385 = &l_386[2];
    uint64_t ****l_384 = &l_385;
    struct S0 **l_464 = (void*)0;
    int8_t *l_501[3];
    int16_t *l_507 = &g_234[0][1];
    float *l_508 = &g_121;
    float *l_509[8] = {&l_356,&l_356,&l_356,&l_356,&l_356,&l_356,&l_356,&l_356};
    int16_t *l_511 = &g_512[2][1][4];
    int i, j;
    for (i = 0; i < 3; i++)
        l_89[i] = &g_85;
    for (i = 0; i < 3; i++)
        l_501[i] = &g_502;
lbl_514:
    if (l_76)
    { /* block id: 14 */
        uint64_t l_79 = 0x299D621DD8CACF64LL;
        int32_t l_80 = 0xAA0C7D0BL;
        uint8_t *l_84[2];
        uint8_t **l_90 = &l_84[1];
        int16_t *l_94 = &g_95;
        const uint8_t *l_114 = &g_115;
        const uint8_t **l_113[7] = {&l_114,&l_114,&l_114,&l_114,&l_114,&l_114,&l_114};
        const uint8_t *l_116[4];
        float *l_120[9][2][1] = {{{&g_121},{&g_121}},{{&g_121},{&g_121}},{{&g_121},{&g_121}},{{&g_121},{&g_121}},{{&g_121},{&g_121}},{{&g_121},{&g_121}},{{&g_121},{&g_121}},{{&g_121},{&g_121}},{{&g_121},{&g_121}}};
        int32_t l_216[6] = {0x85193FB3L,0x85193FB3L,0x85193FB3L,0x85193FB3L,0x85193FB3L,0x85193FB3L};
        int32_t *l_222 = &l_216[0];
        int8_t l_288 = 0x4FL;
        int64_t l_303 = 0x41743E5C0779B58FLL;
        uint64_t l_361[10][7][3] = {{{0xA2691D0D9984C3AELL,18446744073709551615UL,0UL},{0xA7735C133B732911LL,0UL,0UL},{0xE3EECA6E10ED28E3LL,0UL,18446744073709551608UL},{0x665C5118341D7230LL,0UL,0UL},{0x84CD22CDBB26A68FLL,1UL,18446744073709551608UL},{18446744073709551608UL,0xFA9CBDC005F595BDLL,0xAA3B2545CEC79590LL},{8UL,1UL,18446744073709551615UL}},{{9UL,1UL,0x7B81363D7FB8D961LL},{18446744073709551615UL,0xFA9CBDC005F595BDLL,0x790A94A64FC53933LL},{9UL,1UL,18446744073709551614UL},{0x66AB07F6DA724A44LL,0UL,8UL},{0x56F5464C12E7F529LL,0UL,0xD0427F134EDAD87BLL},{0x01D432DFEADECD47LL,0UL,1UL},{0UL,18446744073709551615UL,0xCF0CCA076E5CE79FLL}},{{0xB79B9CB4D59CA45ELL,0x1C3951CD99B518B7LL,18446744073709551608UL},{0x66AB07F6DA724A44LL,18446744073709551614UL,18446744073709551612UL},{18446744073709551614UL,0x18D794A1633E6D08LL,0x25512561FEC4C33ALL},{0xA2691D0D9984C3AELL,0x9C322D2E1EDE2D17LL,0xA2691D0D9984C3AELL},{0x1C3951CD99B518B7LL,18446744073709551607UL,0UL},{1UL,0xD62464B9D06EDF1CLL,0x08851F163692AF2CLL},{18446744073709551608UL,0UL,18446744073709551612UL}},{{18446744073709551607UL,7UL,0xC3C40BEF4245CC11LL},{18446744073709551608UL,0x57478550B225A566LL,0x66AB07F6DA724A44LL},{1UL,8UL,18446744073709551615UL},{0x1C3951CD99B518B7LL,1UL,0x88F025AB22C986EBLL},{0xA2691D0D9984C3AELL,0xFA9CBDC005F595BDLL,8UL},{18446744073709551614UL,7UL,0xEA742DC938AFD058LL},{0x66AB07F6DA724A44LL,0x2636098FC4DE4B1ALL,0xE3EECA6E10ED28E3LL}},{{0xB79B9CB4D59CA45ELL,0x88F025AB22C986EBLL,0xD0427F134EDAD87BLL},{0UL,0x84CD22CDBB26A68FLL,18446744073709551615UL},{0x01D432DFEADECD47LL,18446744073709551615UL,0xAA3B2545CEC79590LL},{0x56F5464C12E7F529LL,0x18D794A1633E6D08LL,0xC3C40BEF4245CC11LL},{0x66AB07F6DA724A44LL,0x4627E3ACA77ED90ELL,1UL},{9UL,0xA7735C133B732911LL,0x25512561FEC4C33ALL},{18446744073709551615UL,18446744073709551606UL,0UL}},{{9UL,18446744073709551607UL,0UL},{8UL,0UL,0x25512561FEC4C33ALL},{18446744073709551608UL,0x2636098FC4DE4B1ALL,1UL},{0x84CD22CDBB26A68FLL,0x374393EFF77A4FC9LL,0xC3C40BEF4245CC11LL},{0x665C5118341D7230LL,0x529D328F16AD2B3ELL,0xAA3B2545CEC79590LL},{0xE3EECA6E10ED28E3LL,8UL,18446744073709551615UL},{0xA7735C133B732911LL,1UL,0xD0427F134EDAD87BLL}},{{0xA2691D0D9984C3AELL,0x57478550B225A566LL,0xE3EECA6E10ED28E3LL},{9UL,0x374393EFF77A4FC9LL,0xEA742DC938AFD058LL},{0UL,18446744073709551615UL,8UL},{18446744073709551615UL,1UL,1UL},{18446744073709551615UL,0xB79B9CB4D59CA45ELL,7UL},{0xC3C40BEF4245CC11LL,1UL,0xE2EB16BC2C94C3D7LL},{0xA2CE0FBE80FF8076LL,0x7B81363D7FB8D961LL,0x18D794A1633E6D08LL}},{{18446744073709551607UL,0xFA9CBDC005F595BDLL,0x18AE3D7A3D239EF2LL},{18446744073709551610UL,0x7B81363D7FB8D961LL,0xA2691D0D9984C3AELL},{1UL,1UL,0xD29E822CC9B3C0C0LL},{0xD0427F134EDAD87BLL,0xB79B9CB4D59CA45ELL,0x7FDE7EC967DBF7A0LL},{0xAA3B2545CEC79590LL,1UL,0x2636098FC4DE4B1ALL},{0UL,0x279522852FF65BF8LL,0x18AE3D7A3D239EF2LL},{0x0C7B6CD0E46D5464LL,18446744073709551613UL,9UL}},{{0xA2691D0D9984C3AELL,1UL,0UL},{0xCF0CCA076E5CE79FLL,0UL,18446744073709551615UL},{0x88F025AB22C986EBLL,0x66AB07F6DA724A44LL,0xD2AD8E04EC45E47BLL},{0x279522852FF65BF8LL,8UL,0x66AB07F6DA724A44LL},{18446744073709551610UL,18446744073709551613UL,18446744073709551608UL},{0xCAE7648232200663LL,18446744073709551615UL,0UL},{5UL,0xA565E3BB80DC45C5LL,0UL}},{{18446744073709551608UL,18446744073709551615UL,7UL},{18446744073709551608UL,0x374393EFF77A4FC9LL,0xCAE7648232200663LL},{5UL,0x7B81363D7FB8D961LL,9UL},{0xCAE7648232200663LL,0x529D328F16AD2B3ELL,0x4311A3B16C7A9694LL},{18446744073709551610UL,0xD62464B9D06EDF1CLL,0UL},{0x279522852FF65BF8LL,18446744073709551610UL,0xD29E822CC9B3C0C0LL},{0x88F025AB22C986EBLL,0x56F5464C12E7F529LL,1UL}}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_84[i] = &g_85;
        for (i = 0; i < 4; i++)
            l_116[i] = &g_117;
lbl_178:
        g_91[5][3][0] &= (p_59 == ((safe_lshift_func_uint8_t_u_s((g_85 ^= ((l_79 != (((g_81++) , 255UL) < p_57)) ^ (p_57 , p_58))), l_76)) , ((safe_add_func_uint32_t_u_u(((~(p_58 ^ (((*l_90) = l_89[0]) == &g_85))) && l_80), (-2L))) , p_59)));
        if (((safe_add_func_int16_t_s_s(((((*l_94) ^= l_76) | ((!(((g_91[1][3][1] != ((safe_lshift_func_int8_t_s_u(((safe_mul_func_float_f_f((l_123[2][4] = (safe_mul_func_float_f_f(((l_94 != l_94) <= l_76), (+(p_58 , (g_121 = (safe_div_func_float_f_f((safe_sub_func_float_f_f(((safe_sub_func_float_f_f(((p_60 = ((g_112 = l_89[0]) == (l_118 = (l_116[2] = &g_85)))) == l_122[3]), g_119)) > g_117), g_121)), 0xE.3ABF99p+48)))))))), g_117)) , 0L), 1)) > p_58)) || 0x2E6F118E40900504LL) && g_91[1][9][1])) && g_81)) < g_3), l_79)) | l_76))
        { /* block id: 26 */
            uint64_t *l_146 = &g_147;
            float *l_150 = (void*)0;
            uint16_t *l_151 = (void*)0;
            uint16_t *l_152 = &g_74[0];
            int32_t l_154 = 0xBD1596F4L;
            int32_t l_176 = 0x31D30752L;
            int32_t *l_241 = &l_80;
            int32_t l_258 = (-7L);
            int32_t l_290 = 0x8C25C54FL;
            int32_t l_291 = 0x2FCB7D22L;
            int32_t l_296 = (-1L);
            int32_t l_300[7] = {1L,8L,1L,1L,8L,1L,1L};
            uint8_t l_330 = 0xCBL;
            int i;
            g_121 = (safe_mul_func_float_f_f((safe_add_func_float_f_f(p_60, (g_121 >= (safe_mul_func_float_f_f((safe_div_func_float_f_f(((safe_unary_minus_func_int64_t_s((((g_153 = (safe_div_func_uint8_t_u_u((safe_add_func_int16_t_s_s((0L < (safe_mul_func_int16_t_s_s(1L, l_79))), ((*l_152) &= ((safe_mod_func_int32_t_s_s((((((g_143[3][1] = (safe_mul_func_int8_t_s_s(p_58, g_81))) < (safe_sub_func_uint64_t_u_u(((*l_146)++), (l_122[1] > ((void*)0 != l_150))))) ^ l_123[0][1]) > l_79) , g_81), (*p_59))) <= g_51)))), l_79))) == g_81) , p_58))) , l_154), g_75[1][2])), g_121))))), p_57));
            if ((g_153 &= ((safe_sub_func_int64_t_s_s(((((safe_mod_func_uint64_t_u_u(l_122[3], p_57)) & (p_58 , (safe_add_func_uint32_t_u_u((safe_add_func_int16_t_s_s(l_154, (safe_rshift_func_int16_t_s_s((safe_unary_minus_func_int16_t_s(((!((safe_rshift_func_int16_t_s_u(0x54EFL, ((*l_152) ^= ((safe_unary_minus_func_uint32_t_u((l_80 ^= (safe_sub_func_int32_t_s_s(((g_91[5][3][0] < (g_91[5][3][0] != l_154)) , (safe_sub_func_uint32_t_u_u(p_57, l_154))), g_95))))) < 65526UL)))) != l_123[2][4])) , 0xBFFBL))), l_79)))), 0xF527E66CL)))) >= l_154) >= l_123[2][4]), 0x6EB578CF17765441LL)) , l_176)))
            { /* block id: 35 */
                int32_t *l_177 = &g_153;
                (*l_177) |= g_75[0][4];
            }
            else
            { /* block id: 37 */
                uint64_t **l_184[6];
                uint8_t l_197 = 246UL;
                int i;
                for (i = 0; i < 6; i++)
                    l_184[i] = &l_146;
                if ((*p_59))
                { /* block id: 38 */
                    if (g_147)
                        goto lbl_178;
                    return g_119;
                }
                else
                { /* block id: 41 */
                    uint32_t *l_211 = &g_212[0][3][0];
                    uint32_t *l_213 = &g_214[0];
                    int32_t l_215 = 1L;
                    int32_t *l_219 = &l_216[5];
                    int32_t **l_218 = &l_219;
                    for (g_85 = 0; (g_85 != 45); g_85 = safe_add_func_uint8_t_u_u(g_85, 9))
                    { /* block id: 44 */
                        uint64_t * volatile **l_183 = &g_181;
                        (*l_183) = g_181;
                        if (g_51)
                            goto lbl_217;
                        if (l_123[2][4])
                            break;
                        g_181 = l_184[3];
                    }
lbl_217:
                    l_216[0] = (safe_add_func_float_f_f(0x0.2p-1, ((safe_div_func_float_f_f(((safe_div_func_float_f_f((((((*l_213) = ((*l_211) = (l_80 = (safe_sub_func_uint32_t_u_u((safe_mul_func_int8_t_s_s((l_154 , (safe_rshift_func_int16_t_s_u(g_119, 9))), l_197)), ((safe_sub_func_int32_t_s_s(((safe_rshift_func_uint16_t_u_s(g_153, 8)) < 9L), (safe_lshift_func_int16_t_s_u((((safe_div_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((!0x9AL), (((*l_94) = ((safe_mod_func_int32_t_s_s((((((void*)0 == &g_95) , l_154) , p_58) > g_75[1][2]), (*p_59))) & (-8L))) | p_58))), l_154)) <= 18446744073709551615UL) & l_79), 0)))) && l_123[2][4])))))) , l_152) != l_151) > g_143[3][1]), p_60)) < l_215), 0x8.2p-1)) == 0x0.6p-1)));
                    (*l_218) = (((*g_112) = 249UL) , &g_51);
                    return p_58;
                }
            }
            for (g_95 = 24; (g_95 < 1); g_95--)
            { /* block id: 62 */
                uint8_t *l_227 = &l_122[3];
                int32_t *l_240 = &l_123[2][4];
                int32_t l_287 = 5L;
                int32_t l_289 = 0x2CF89645L;
                int32_t l_293 = (-1L);
                int32_t l_294 = 0L;
                int32_t l_295 = 7L;
                int32_t l_298 = 0L;
                int32_t l_301[7][6] = {{(-9L),(-9L),0xCCE189E2L,(-9L),(-9L),0xCCE189E2L},{(-9L),(-9L),0xCCE189E2L,(-9L),(-9L),0xCCE189E2L},{(-9L),(-9L),0xCCE189E2L,(-9L),(-9L),0xCCE189E2L},{(-9L),(-9L),0xCCE189E2L,(-9L),(-9L),0xCCE189E2L},{(-9L),(-9L),0xCCE189E2L,(-9L),(-9L),0xCCE189E2L},{(-9L),(-9L),0xCCE189E2L,(-9L),(-9L),0xCCE189E2L},{(-9L),(-9L),0xCCE189E2L,(-9L),(-9L),0xCCE189E2L}};
                struct S0 *l_333 = &g_334;
                int i, j;
                l_222 = p_59;
                if (l_154)
                    break;
                for (g_147 = 0; (g_147 != 32); g_147 = safe_add_func_uint16_t_u_u(g_147, 4))
                { /* block id: 67 */
                    uint8_t *l_228 = (void*)0;
                    int32_t **l_235 = &l_222;
                    int32_t *l_242 = &l_216[2];
                    int32_t *l_243 = (void*)0;
                    for (l_79 = 0; (l_79 <= 5); l_79 += 1)
                    { /* block id: 70 */
                        int16_t *l_233[10][3][5] = {{{&g_234[3][3],&g_95,&g_95,&g_234[3][1],&g_234[6][1]},{&g_234[0][1],(void*)0,&g_234[1][3],&g_234[0][1],(void*)0},{(void*)0,&g_234[0][1],&g_95,&g_95,&g_234[6][1]}},{{&g_95,&g_234[0][1],&g_234[5][1],&g_234[0][1],&g_234[5][3]},{&g_234[6][1],&g_234[0][1],&g_95,&g_234[0][1],(void*)0},{&g_234[2][0],&g_234[6][1],&g_234[1][3],&g_234[2][0],&g_234[1][3]}},{{&g_234[6][1],&g_234[6][1],&g_234[0][1],&g_234[4][0],&g_234[3][3]},{&g_95,&g_234[2][0],&g_234[0][1],&g_95,&g_95},{(void*)0,&g_95,&g_234[0][1],&g_234[0][1],&g_234[3][1]}},{{&g_234[0][1],&g_234[2][0],&g_234[0][1],&g_234[6][1],(void*)0},{&g_234[3][3],&g_234[6][1],&g_234[0][1],&g_95,&g_234[0][1]},{&g_234[5][3],&g_234[6][1],&g_234[5][1],&g_95,&g_95}},{{&g_234[3][1],&g_234[0][1],&g_234[0][1],&g_234[3][1],&g_234[6][2]},{&g_234[2][0],&g_234[0][1],&g_234[0][1],(void*)0,&g_234[1][3]},{&g_234[0][1],&g_234[0][1],&g_234[0][1],&g_234[4][0],&g_234[0][1]}},{{&g_234[5][3],(void*)0,&g_234[0][1],(void*)0,&g_234[5][3]},{&g_234[6][2],&g_95,&g_234[0][1],&g_234[3][1],&g_234[0][1]},{&g_234[0][1],&g_95,&g_234[1][3],&g_95,(void*)0}},{{&g_234[0][1],&g_234[0][1],&g_95,&g_95,&g_234[0][1]},{&g_95,&g_95,&g_234[5][1],&g_234[6][1],&g_234[5][3]},{&g_234[0][1],&g_234[0][1],&g_95,&g_234[0][1],&g_234[0][1]}},{{&g_234[2][0],&g_234[0][1],&g_234[1][3],&g_95,&g_234[1][3]},{&g_234[0][1],&g_234[6][1],&g_95,&g_234[4][0],&g_234[6][2]},{&g_95,&g_95,&g_234[0][1],&g_234[2][0],&g_95}},{{&g_234[0][1],&g_95,&g_234[1][3],&g_234[0][1],&g_234[0][1]},{&g_234[0][1],&g_95,&g_234[0][1],&g_234[0][1],(void*)0},{&g_234[6][2],&g_234[6][1],&g_234[4][0],&g_95,&g_234[3][1]}},{{&g_234[5][3],&g_234[0][1],&g_234[5][1],&g_234[0][1],&g_95},{&g_234[0][1],&g_234[0][1],&g_234[4][0],&g_234[3][1],&g_234[3][3]},{&g_234[2][0],&g_95,&g_234[0][1],&g_95,&g_234[1][3]}}};
                        int i, j, k;
                        (*g_232) = (safe_mod_func_int32_t_s_s((((l_228 = (l_227 = &g_85)) == (void*)0) & (g_234[0][1] = (safe_lshift_func_uint16_t_u_u(((void*)0 == g_231[6][1][0]), 12)))), l_216[l_79]));
                        if (l_216[l_79])
                            break;
                        if ((*p_61))
                            break;
                        if (p_58)
                            goto lbl_178;
                    }
                    (*l_235) = &g_153;
                    l_241 = ((((safe_div_func_int32_t_s_s(0xDDDCD2A6L, (safe_mod_func_uint32_t_u_u(g_91[5][3][0], (*p_59))))) , p_58) , p_60) , l_240);
                    --l_244;
                }
                for (l_80 = 4; (l_80 >= (-10)); l_80--)
                { /* block id: 85 */
                    int16_t l_257 = (-4L);
                    uint32_t l_259 = 0x64D273E0L;
                    uint16_t *l_275 = (void*)0;
                    int32_t l_282 = 4L;
                    int32_t l_292 = 0x1FE63895L;
                    int32_t l_297 = 9L;
                    int32_t l_299 = (-2L);
                    int32_t l_302 = 0x73050510L;
                    int32_t l_304 = 0x5414E303L;
                    int32_t l_305 = 0xE533C787L;
                    int32_t l_306 = 0x57894A56L;
                    int32_t l_307 = 5L;
                    int32_t l_308 = 7L;
                    int32_t l_309 = 0L;
                    int32_t l_310 = 0x1AA369B1L;
                    int32_t l_311 = (-1L);
                    struct S0 **l_335 = &l_333;
                    int32_t **l_338[2][8] = {{&l_240,&l_240,&l_240,&l_240,&l_240,&l_240,&l_240,&l_240},{&l_240,&l_240,&l_240,&l_240,&l_240,&l_240,&l_240,&l_240}};
                    int i, j;
                }
            }
            (*l_241) ^= (*g_232);
        }
        else
        { /* block id: 115 */
            uint16_t *l_362[9][9][3] = {{{(void*)0,&g_74[2],&g_75[1][2]},{&l_266,&g_81,&g_75[0][0]},{(void*)0,&g_81,&g_75[1][2]},{&g_74[2],&l_266,&g_74[0]},{&g_74[2],(void*)0,&g_75[1][2]},{&g_74[0],&l_266,&l_266},{(void*)0,&g_81,&l_244},{&g_74[0],&g_81,&l_244},{&g_74[2],&g_74[2],&l_244}},{{&g_75[1][2],&g_74[2],&l_266},{(void*)0,&g_75[1][2],&g_75[1][2]},{&g_75[1][2],&g_81,&g_74[0]},{(void*)0,&g_75[2][0],&g_75[1][2]},{&g_75[1][2],&l_266,&g_75[0][0]},{&g_74[2],(void*)0,&g_75[1][2]},{&g_74[0],&l_266,(void*)0},{(void*)0,&g_75[2][0],&l_244},{&g_74[0],&g_81,&g_74[0]}},{{&g_74[2],&g_75[1][2],&l_244},{&g_74[2],&g_74[2],(void*)0},{(void*)0,&g_74[2],&g_75[1][2]},{&l_266,&g_81,&g_75[0][0]},{(void*)0,&g_81,&g_75[1][2]},{&g_74[2],&l_266,&g_74[0]},{&g_74[2],(void*)0,&g_75[1][2]},{&g_74[0],&l_266,&l_266},{(void*)0,&g_81,&l_244}},{{&g_74[0],&g_81,&l_244},{&g_74[2],&g_74[2],&l_244},{&g_75[1][2],&g_74[2],&l_266},{(void*)0,&g_75[1][2],(void*)0},{&l_244,&g_74[0],&l_244},{&g_75[1][2],&g_75[1][2],(void*)0},{&l_266,&g_74[0],&g_81},{&g_74[2],&g_74[0],(void*)0},{&g_74[0],&g_74[0],&g_74[0]}},{{&g_74[0],&g_75[1][2],(void*)0},{&g_75[0][0],&g_74[0],&l_266},{&g_74[2],(void*)0,(void*)0},{(void*)0,&g_74[0],&g_74[0]},{&g_75[1][2],&g_74[0],(void*)0},{&g_74[0],&g_74[0],&g_81},{&g_75[1][2],&l_244,(void*)0},{(void*)0,&g_74[0],&l_244},{&g_74[2],(void*)0,(void*)0}},{{&g_75[0][0],&g_74[0],&g_75[1][2]},{&g_74[0],&l_244,(void*)0},{&g_74[0],&g_74[0],&g_81},{&g_74[2],&g_74[0],(void*)0},{&l_266,&g_74[0],&g_75[1][2]},{&g_75[1][2],(void*)0,(void*)0},{&l_244,&g_74[0],&l_244},{&g_75[1][2],&g_75[1][2],(void*)0},{&l_266,&g_74[0],&g_81}},{{&g_74[2],&g_74[0],(void*)0},{&g_74[0],&g_74[0],&g_74[0]},{&g_74[0],&g_75[1][2],(void*)0},{&g_75[0][0],&g_74[0],&l_266},{&g_74[2],(void*)0,(void*)0},{(void*)0,&g_74[0],&g_74[0]},{&g_75[1][2],&g_74[0],(void*)0},{&g_74[0],&g_74[0],&g_81},{&g_75[1][2],&l_244,(void*)0}},{{(void*)0,&g_74[0],&l_244},{&g_74[2],(void*)0,(void*)0},{&g_75[0][0],&g_74[0],&g_75[1][2]},{&g_74[0],&l_244,(void*)0},{&g_74[0],&g_74[0],&g_81},{&g_74[2],&g_74[0],(void*)0},{&l_266,&g_74[0],&g_75[1][2]},{&g_75[1][2],(void*)0,(void*)0},{&l_244,&g_74[0],&l_244}},{{&g_75[1][2],&g_75[1][2],(void*)0},{&l_266,&g_74[0],&g_81},{&g_74[2],&g_74[0],(void*)0},{&g_74[0],&g_74[0],&g_74[0]},{&g_74[0],&g_75[1][2],(void*)0},{&g_75[0][0],&g_74[0],&l_266},{&g_74[2],(void*)0,(void*)0},{(void*)0,&g_74[0],&g_74[0]},{&g_75[1][2],&g_74[0],(void*)0}}};
            int i, j, k;
            (*g_232) = (safe_lshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_u(l_361[6][1][1], (g_75[1][2]--))), 12));
            return g_234[4][2];
        }
        l_365 = l_365;
    }
    else
    { /* block id: 121 */
        int8_t l_374 = 0x5BL;
        float *l_398 = (void*)0;
        uint16_t l_416[4][5] = {{0x76ABL,0UL,0xF634L,1UL,0xF634L},{0xF634L,0xF634L,65535UL,1UL,0xF19FL},{0UL,0x76ABL,0x76ABL,0UL,0xF634L},{0UL,1UL,0xD873L,0xD873L,1UL}};
        int32_t l_458 = (-1L);
        int32_t l_459 = 0xD3186862L;
        uint64_t l_460[1][6] = {{18446744073709551611UL,18446744073709551611UL,18446744073709551610UL,18446744073709551611UL,18446744073709551611UL,18446744073709551610UL}};
        int32_t l_461 = 7L;
        uint32_t *l_469 = &g_143[3][1];
        int i, j;
        for (g_85 = 0; (g_85 > 1); g_85 = safe_add_func_int16_t_s_s(g_85, 4))
        { /* block id: 124 */
            struct S2 *l_370 = (void*)0;
            struct S2 **l_369 = &l_370;
            int32_t l_379 = (-8L);
            (*l_369) = (void*)0;
            if ((*p_59))
            { /* block id: 126 */
                return l_122[3];
            }
            else
            { /* block id: 128 */
                int32_t **l_371 = &g_232;
                (*l_371) = &g_153;
                for (g_147 = (-12); (g_147 < 16); g_147++)
                { /* block id: 132 */
                    uint32_t l_375 = 4294967286UL;
                    union U4 **l_376 = &l_365;
                    (**l_371) = (l_374 <= (1L ^ l_375));
                    (*l_376) = l_365;
                    for (p_58 = 0; (p_58 == 24); p_58 = safe_add_func_uint64_t_u_u(p_58, 5))
                    { /* block id: 137 */
                        (*g_232) ^= (l_379 & (p_58 || 0xAAL));
                    }
                }
                return p_57;
            }
        }
        for (g_85 = 0; (g_85 == 13); g_85 = safe_add_func_int32_t_s_s(g_85, 9))
        { /* block id: 146 */
            float *l_397 = &l_356;
            int32_t l_401 = 0xCE05073EL;
            uint16_t l_417 = 0x96F3L;
        }
        (*l_382) = (((l_464 != ((*l_382) , g_465)) | ((safe_div_func_uint32_t_u_u((++(*l_469)), l_459)) == ((((l_416[2][4] <= (*l_382)) > (-1L)) == ((0xE0L | (((safe_add_func_int8_t_s_s((l_458 = p_58), p_57)) < 0xCBB7319AL) <= p_58)) && 7L)) || 0xA9L))) && (*l_382));
    }
    if ((((safe_add_func_uint64_t_u_u(((g_513[1][4][9] |= ((safe_lshift_func_int16_t_s_s(((*l_511) ^= (safe_rshift_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s(((safe_rshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_u(((((~g_75[1][2]) , 5UL) && ((safe_add_func_int64_t_s_s(((safe_add_func_uint32_t_u_u((*l_382), (safe_div_func_int32_t_s_s((safe_mul_func_int16_t_s_s((((g_510[0] = ((*l_508) = (((safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u(((*l_507) = ((0x70L == ((g_502 |= (-5L)) <= (1L == (safe_mul_func_int16_t_s_s((((void*)0 != &l_89[2]) >= (safe_mod_func_uint32_t_u_u(((-1L) != (*l_382)), 0x2F4A0A3BL))), p_57))))) != g_81)), 2)), 5L)) | (*l_382)) , (*l_382)))) , (*p_59)) <= 0x5A397BB0L), p_57)), (*p_59))))) > (*g_232)), 18446744073709551611UL)) && 0x1C40D58F68FE203ELL)) || (-2L)), 13)) || p_57), 14)) , (*l_382)), g_95)) , p_58), 2))), (*l_382))) || p_57)) <= p_57), (*l_382))) <= (*l_382)) && 0x8AL))
    { /* block id: 213 */
        if (g_117)
            goto lbl_514;
    }
    else
    { /* block id: 215 */
        return p_58;
    }
    return g_366.f0;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc_bytes(&g_16[i], sizeof(g_16[i]), "g_16[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_21, "g_21", print_hash_value);
    transparent_crc(g_51, "g_51", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_74[i], "g_74[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_75[i][j], "g_75[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_81, "g_81", print_hash_value);
    transparent_crc(g_85, "g_85", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_91[i][j][k], "g_91[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_115, "g_115", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc_bytes (&g_121, sizeof(g_121), "g_121", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_143[i][j], "g_143[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_147, "g_147", print_hash_value);
    transparent_crc(g_153, "g_153", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_212[i][j][k], "g_212[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_214[i], "g_214[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_234[i][j], "g_234[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_334.f0, "g_334.f0", print_hash_value);
    transparent_crc(g_334.f1, "g_334.f1", print_hash_value);
    transparent_crc(g_334.f2, "g_334.f2", print_hash_value);
    transparent_crc(g_334.f3, "g_334.f3", print_hash_value);
    transparent_crc(g_334.f4, "g_334.f4", print_hash_value);
    transparent_crc(g_366.f0, "g_366.f0", print_hash_value);
    transparent_crc(g_366.f1, "g_366.f1", print_hash_value);
    transparent_crc(g_366.f2, "g_366.f2", print_hash_value);
    transparent_crc(g_366.f3, "g_366.f3", print_hash_value);
    transparent_crc(g_463, "g_463", print_hash_value);
    transparent_crc(g_502, "g_502", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_510[i], "g_510[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_512[i][j][k], "g_512[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_513[i][j][k], "g_513[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_523.f0, "g_523.f0", print_hash_value);
    transparent_crc(g_523.f1, "g_523.f1", print_hash_value);
    transparent_crc(g_523.f2, "g_523.f2", print_hash_value);
    transparent_crc(g_523.f3, "g_523.f3", print_hash_value);
    transparent_crc(g_523.f4, "g_523.f4", print_hash_value);
    transparent_crc(g_535.f0, "g_535.f0", print_hash_value);
    transparent_crc(g_535.f1, "g_535.f1", print_hash_value);
    transparent_crc(g_535.f2, "g_535.f2", print_hash_value);
    transparent_crc(g_535.f3, "g_535.f3", print_hash_value);
    transparent_crc(g_548.f0, "g_548.f0", print_hash_value);
    transparent_crc(g_548.f1, "g_548.f1", print_hash_value);
    transparent_crc(g_548.f2, "g_548.f2", print_hash_value);
    transparent_crc(g_548.f3, "g_548.f3", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_565[i][j][k], "g_565[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_576.f0, "g_576.f0", print_hash_value);
    transparent_crc(g_576.f1, "g_576.f1", print_hash_value);
    transparent_crc(g_576.f2, "g_576.f2", print_hash_value);
    transparent_crc(g_598.f0, "g_598.f0", print_hash_value);
    transparent_crc(g_598.f1, "g_598.f1", print_hash_value);
    transparent_crc(g_598.f2, "g_598.f2", print_hash_value);
    transparent_crc(g_598.f3, "g_598.f3", print_hash_value);
    transparent_crc(g_598.f4, "g_598.f4", print_hash_value);
    transparent_crc(g_607.f0, "g_607.f0", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc_bytes(&g_620[i][j], sizeof(g_620[i][j]), "g_620[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_791[i][j].f0, "g_791[i][j].f0", print_hash_value);
            transparent_crc(g_791[i][j].f1, "g_791[i][j].f1", print_hash_value);
            transparent_crc(g_791[i][j].f2, "g_791[i][j].f2", print_hash_value);
            transparent_crc(g_791[i][j].f3, "g_791[i][j].f3", print_hash_value);
            transparent_crc(g_791[i][j].f4, "g_791[i][j].f4", print_hash_value);
            transparent_crc(g_791[i][j].f5, "g_791[i][j].f5", print_hash_value);
            transparent_crc(g_791[i][j].f6, "g_791[i][j].f6", print_hash_value);
            transparent_crc(g_791[i][j].f7, "g_791[i][j].f7", print_hash_value);
            transparent_crc(g_791[i][j].f8.f0, "g_791[i][j].f8.f0", print_hash_value);
            transparent_crc(g_791[i][j].f8.f1, "g_791[i][j].f8.f1", print_hash_value);
            transparent_crc(g_791[i][j].f8.f2, "g_791[i][j].f8.f2", print_hash_value);
            transparent_crc(g_791[i][j].f9, "g_791[i][j].f9", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_793[i][j].f0, "g_793[i][j].f0", print_hash_value);
            transparent_crc(g_793[i][j].f1, "g_793[i][j].f1", print_hash_value);
            transparent_crc(g_793[i][j].f2, "g_793[i][j].f2", print_hash_value);
            transparent_crc(g_793[i][j].f3, "g_793[i][j].f3", print_hash_value);
            transparent_crc(g_793[i][j].f4, "g_793[i][j].f4", print_hash_value);
            transparent_crc(g_793[i][j].f5, "g_793[i][j].f5", print_hash_value);
            transparent_crc(g_793[i][j].f6, "g_793[i][j].f6", print_hash_value);
            transparent_crc(g_793[i][j].f7, "g_793[i][j].f7", print_hash_value);
            transparent_crc(g_793[i][j].f8.f0, "g_793[i][j].f8.f0", print_hash_value);
            transparent_crc(g_793[i][j].f8.f1, "g_793[i][j].f8.f1", print_hash_value);
            transparent_crc(g_793[i][j].f8.f2, "g_793[i][j].f8.f2", print_hash_value);
            transparent_crc(g_793[i][j].f9, "g_793[i][j].f9", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_794.f0, "g_794.f0", print_hash_value);
    transparent_crc(g_794.f1, "g_794.f1", print_hash_value);
    transparent_crc(g_794.f2, "g_794.f2", print_hash_value);
    transparent_crc(g_794.f3, "g_794.f3", print_hash_value);
    transparent_crc(g_794.f4, "g_794.f4", print_hash_value);
    transparent_crc(g_794.f5, "g_794.f5", print_hash_value);
    transparent_crc(g_794.f6, "g_794.f6", print_hash_value);
    transparent_crc(g_794.f7, "g_794.f7", print_hash_value);
    transparent_crc(g_794.f8.f0, "g_794.f8.f0", print_hash_value);
    transparent_crc(g_794.f8.f1, "g_794.f8.f1", print_hash_value);
    transparent_crc(g_794.f8.f2, "g_794.f8.f2", print_hash_value);
    transparent_crc(g_794.f9, "g_794.f9", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_817[i][j], "g_817[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_835, "g_835", print_hash_value);
    transparent_crc(g_923, "g_923", print_hash_value);
    transparent_crc(g_930.f0, "g_930.f0", print_hash_value);
    transparent_crc(g_930.f1, "g_930.f1", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1018[i][j].f0, "g_1018[i][j].f0", print_hash_value);
            transparent_crc(g_1018[i][j].f1, "g_1018[i][j].f1", print_hash_value);
            transparent_crc(g_1018[i][j].f2, "g_1018[i][j].f2", print_hash_value);
            transparent_crc(g_1018[i][j].f3, "g_1018[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1034, "g_1034", print_hash_value);
    transparent_crc(g_1049.f0, "g_1049.f0", print_hash_value);
    transparent_crc(g_1092.f0, "g_1092.f0", print_hash_value);
    transparent_crc(g_1092.f1, "g_1092.f1", print_hash_value);
    transparent_crc(g_1092.f2, "g_1092.f2", print_hash_value);
    transparent_crc(g_1092.f3, "g_1092.f3", print_hash_value);
    transparent_crc(g_1092.f4, "g_1092.f4", print_hash_value);
    transparent_crc(g_1093.f0, "g_1093.f0", print_hash_value);
    transparent_crc(g_1093.f1, "g_1093.f1", print_hash_value);
    transparent_crc(g_1093.f2, "g_1093.f2", print_hash_value);
    transparent_crc(g_1093.f3, "g_1093.f3", print_hash_value);
    transparent_crc(g_1093.f4, "g_1093.f4", print_hash_value);
    transparent_crc(g_1141, "g_1141", print_hash_value);
    transparent_crc_bytes (&g_1176, sizeof(g_1176), "g_1176", print_hash_value);
    transparent_crc(g_1222.f0, "g_1222.f0", print_hash_value);
    transparent_crc(g_1222.f1, "g_1222.f1", print_hash_value);
    transparent_crc(g_1222.f2, "g_1222.f2", print_hash_value);
    transparent_crc(g_1222.f3, "g_1222.f3", print_hash_value);
    transparent_crc(g_1222.f4, "g_1222.f4", print_hash_value);
    transparent_crc(g_1230.f0, "g_1230.f0", print_hash_value);
    transparent_crc(g_1230.f1, "g_1230.f1", print_hash_value);
    transparent_crc(g_1230.f2, "g_1230.f2", print_hash_value);
    transparent_crc(g_1247.f0, "g_1247.f0", print_hash_value);
    transparent_crc(g_1247.f1, "g_1247.f1", print_hash_value);
    transparent_crc(g_1247.f2, "g_1247.f2", print_hash_value);
    transparent_crc(g_1247.f3, "g_1247.f3", print_hash_value);
    transparent_crc(g_1286.f0, "g_1286.f0", print_hash_value);
    transparent_crc(g_1286.f1, "g_1286.f1", print_hash_value);
    transparent_crc(g_1328, "g_1328", print_hash_value);
    transparent_crc(g_1426.f0, "g_1426.f0", print_hash_value);
    transparent_crc(g_1426.f1, "g_1426.f1", print_hash_value);
    transparent_crc(g_1426.f2, "g_1426.f2", print_hash_value);
    transparent_crc(g_1426.f3, "g_1426.f3", print_hash_value);
    transparent_crc(g_1426.f4, "g_1426.f4", print_hash_value);
    transparent_crc(g_1464, "g_1464", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1473[i][j], "g_1473[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1532.f0, "g_1532.f0", print_hash_value);
    transparent_crc(g_1532.f1, "g_1532.f1", print_hash_value);
    transparent_crc(g_1532.f2, "g_1532.f2", print_hash_value);
    transparent_crc(g_1532.f3, "g_1532.f3", print_hash_value);
    transparent_crc(g_1532.f4, "g_1532.f4", print_hash_value);
    transparent_crc(g_1532.f5, "g_1532.f5", print_hash_value);
    transparent_crc(g_1532.f6, "g_1532.f6", print_hash_value);
    transparent_crc(g_1532.f7, "g_1532.f7", print_hash_value);
    transparent_crc(g_1532.f8.f0, "g_1532.f8.f0", print_hash_value);
    transparent_crc(g_1532.f8.f1, "g_1532.f8.f1", print_hash_value);
    transparent_crc(g_1532.f8.f2, "g_1532.f8.f2", print_hash_value);
    transparent_crc(g_1532.f9, "g_1532.f9", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_1534[i][j][k].f0, "g_1534[i][j][k].f0", print_hash_value);
                transparent_crc(g_1534[i][j][k].f1, "g_1534[i][j][k].f1", print_hash_value);
                transparent_crc(g_1534[i][j][k].f2, "g_1534[i][j][k].f2", print_hash_value);
                transparent_crc(g_1534[i][j][k].f3, "g_1534[i][j][k].f3", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1538[i], "g_1538[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1549, "g_1549", print_hash_value);
    transparent_crc(g_1551.f0, "g_1551.f0", print_hash_value);
    transparent_crc(g_1551.f1, "g_1551.f1", print_hash_value);
    transparent_crc(g_1551.f2, "g_1551.f2", print_hash_value);
    transparent_crc_bytes (&g_1594, sizeof(g_1594), "g_1594", print_hash_value);
    transparent_crc(g_1602, "g_1602", print_hash_value);
    transparent_crc(g_1633.f0, "g_1633.f0", print_hash_value);
    transparent_crc(g_1633.f1, "g_1633.f1", print_hash_value);
    transparent_crc(g_1706.f0, "g_1706.f0", print_hash_value);
    transparent_crc(g_1706.f1, "g_1706.f1", print_hash_value);
    transparent_crc(g_1706.f2, "g_1706.f2", print_hash_value);
    transparent_crc(g_1725, "g_1725", print_hash_value);
    transparent_crc(g_1747.f0, "g_1747.f0", print_hash_value);
    transparent_crc(g_1747.f1, "g_1747.f1", print_hash_value);
    transparent_crc(g_1747.f2, "g_1747.f2", print_hash_value);
    transparent_crc(g_1747.f3, "g_1747.f3", print_hash_value);
    transparent_crc(g_1747.f4, "g_1747.f4", print_hash_value);
    transparent_crc(g_1747.f5, "g_1747.f5", print_hash_value);
    transparent_crc(g_1747.f6, "g_1747.f6", print_hash_value);
    transparent_crc(g_1747.f7, "g_1747.f7", print_hash_value);
    transparent_crc(g_1747.f8.f0, "g_1747.f8.f0", print_hash_value);
    transparent_crc(g_1747.f8.f1, "g_1747.f8.f1", print_hash_value);
    transparent_crc(g_1747.f8.f2, "g_1747.f8.f2", print_hash_value);
    transparent_crc(g_1747.f9, "g_1747.f9", print_hash_value);
    transparent_crc_bytes (&g_1760, sizeof(g_1760), "g_1760", print_hash_value);
    transparent_crc(g_1819.f0, "g_1819.f0", print_hash_value);
    transparent_crc(g_1819.f1, "g_1819.f1", print_hash_value);
    transparent_crc(g_1819.f2, "g_1819.f2", print_hash_value);
    transparent_crc(g_1819.f3, "g_1819.f3", print_hash_value);
    transparent_crc(g_1819.f4, "g_1819.f4", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1829[i].f0, "g_1829[i].f0", print_hash_value);
        transparent_crc(g_1829[i].f1, "g_1829[i].f1", print_hash_value);
        transparent_crc(g_1829[i].f2, "g_1829[i].f2", print_hash_value);
        transparent_crc(g_1829[i].f3, "g_1829[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1832, "g_1832", print_hash_value);
    transparent_crc(g_1889, "g_1889", print_hash_value);
    transparent_crc_bytes (&g_1932, sizeof(g_1932), "g_1932", print_hash_value);
    transparent_crc(g_1956.f0, "g_1956.f0", print_hash_value);
    transparent_crc(g_1956.f1, "g_1956.f1", print_hash_value);
    transparent_crc(g_2059.f0, "g_2059.f0", print_hash_value);
    transparent_crc(g_2059.f1, "g_2059.f1", print_hash_value);
    transparent_crc(g_2059.f2, "g_2059.f2", print_hash_value);
    transparent_crc(g_2059.f3, "g_2059.f3", print_hash_value);
    transparent_crc(g_2059.f4, "g_2059.f4", print_hash_value);
    transparent_crc_bytes (&g_2085, sizeof(g_2085), "g_2085", print_hash_value);
    transparent_crc(g_2113.f0, "g_2113.f0", print_hash_value);
    transparent_crc(g_2113.f1, "g_2113.f1", print_hash_value);
    transparent_crc(g_2113.f2, "g_2113.f2", print_hash_value);
    transparent_crc(g_2113.f3, "g_2113.f3", print_hash_value);
    transparent_crc(g_2113.f4, "g_2113.f4", print_hash_value);
    transparent_crc(g_2173, "g_2173", print_hash_value);
    transparent_crc(g_2269.f0, "g_2269.f0", print_hash_value);
    transparent_crc(g_2269.f1, "g_2269.f1", print_hash_value);
    transparent_crc(g_2269.f2, "g_2269.f2", print_hash_value);
    transparent_crc(g_2269.f3, "g_2269.f3", print_hash_value);
    transparent_crc(g_2269.f4, "g_2269.f4", print_hash_value);
    transparent_crc(g_2308.f0, "g_2308.f0", print_hash_value);
    transparent_crc(g_2308.f1, "g_2308.f1", print_hash_value);
    transparent_crc(g_2308.f2, "g_2308.f2", print_hash_value);
    transparent_crc(g_2308.f3, "g_2308.f3", print_hash_value);
    transparent_crc(g_2322.f0, "g_2322.f0", print_hash_value);
    transparent_crc(g_2322.f1, "g_2322.f1", print_hash_value);
    transparent_crc(g_2417.f0, "g_2417.f0", print_hash_value);
    transparent_crc(g_2417.f1, "g_2417.f1", print_hash_value);
    transparent_crc(g_2417.f2, "g_2417.f2", print_hash_value);
    transparent_crc(g_2432.f0, "g_2432.f0", print_hash_value);
    transparent_crc(g_2432.f1, "g_2432.f1", print_hash_value);
    transparent_crc(g_2432.f2, "g_2432.f2", print_hash_value);
    transparent_crc(g_2446.f0, "g_2446.f0", print_hash_value);
    transparent_crc(g_2446.f1, "g_2446.f1", print_hash_value);
    transparent_crc(g_2446.f2, "g_2446.f2", print_hash_value);
    transparent_crc(g_2446.f3, "g_2446.f3", print_hash_value);
    transparent_crc(g_2447.f0, "g_2447.f0", print_hash_value);
    transparent_crc(g_2447.f1, "g_2447.f1", print_hash_value);
    transparent_crc(g_2447.f2, "g_2447.f2", print_hash_value);
    transparent_crc(g_2447.f3, "g_2447.f3", print_hash_value);
    transparent_crc(g_2447.f4, "g_2447.f4", print_hash_value);
    transparent_crc(g_2447.f5, "g_2447.f5", print_hash_value);
    transparent_crc(g_2447.f6, "g_2447.f6", print_hash_value);
    transparent_crc(g_2447.f7, "g_2447.f7", print_hash_value);
    transparent_crc(g_2447.f8.f0, "g_2447.f8.f0", print_hash_value);
    transparent_crc(g_2447.f8.f1, "g_2447.f8.f1", print_hash_value);
    transparent_crc(g_2447.f8.f2, "g_2447.f8.f2", print_hash_value);
    transparent_crc(g_2447.f9, "g_2447.f9", print_hash_value);
    transparent_crc(g_2484.f0, "g_2484.f0", print_hash_value);
    transparent_crc(g_2484.f1, "g_2484.f1", print_hash_value);
    transparent_crc(g_2484.f2, "g_2484.f2", print_hash_value);
    transparent_crc(g_2484.f3, "g_2484.f3", print_hash_value);
    transparent_crc(g_2484.f4, "g_2484.f4", print_hash_value);
    transparent_crc(g_2484.f5, "g_2484.f5", print_hash_value);
    transparent_crc(g_2484.f6, "g_2484.f6", print_hash_value);
    transparent_crc(g_2484.f7, "g_2484.f7", print_hash_value);
    transparent_crc(g_2484.f8.f0, "g_2484.f8.f0", print_hash_value);
    transparent_crc(g_2484.f8.f1, "g_2484.f8.f1", print_hash_value);
    transparent_crc(g_2484.f8.f2, "g_2484.f8.f2", print_hash_value);
    transparent_crc(g_2484.f9, "g_2484.f9", print_hash_value);
    transparent_crc(g_2525.f0, "g_2525.f0", print_hash_value);
    transparent_crc(g_2525.f1, "g_2525.f1", print_hash_value);
    transparent_crc(g_2525.f2, "g_2525.f2", print_hash_value);
    transparent_crc(g_2525.f3, "g_2525.f3", print_hash_value);
    transparent_crc(g_2544.f0, "g_2544.f0", print_hash_value);
    transparent_crc(g_2544.f1, "g_2544.f1", print_hash_value);
    transparent_crc(g_2544.f2, "g_2544.f2", print_hash_value);
    transparent_crc(g_2544.f3, "g_2544.f3", print_hash_value);
    transparent_crc(g_2544.f4, "g_2544.f4", print_hash_value);
    transparent_crc(g_2544.f5, "g_2544.f5", print_hash_value);
    transparent_crc(g_2544.f6, "g_2544.f6", print_hash_value);
    transparent_crc(g_2544.f7, "g_2544.f7", print_hash_value);
    transparent_crc(g_2544.f8.f0, "g_2544.f8.f0", print_hash_value);
    transparent_crc(g_2544.f8.f1, "g_2544.f8.f1", print_hash_value);
    transparent_crc(g_2544.f8.f2, "g_2544.f8.f2", print_hash_value);
    transparent_crc(g_2544.f9, "g_2544.f9", print_hash_value);
    transparent_crc(g_2568.f0, "g_2568.f0", print_hash_value);
    transparent_crc(g_2568.f1, "g_2568.f1", print_hash_value);
    transparent_crc(g_2568.f2, "g_2568.f2", print_hash_value);
    transparent_crc(g_2568.f3, "g_2568.f3", print_hash_value);
    transparent_crc(g_2568.f4, "g_2568.f4", print_hash_value);
    transparent_crc(g_2572.f0, "g_2572.f0", print_hash_value);
    transparent_crc(g_2572.f1, "g_2572.f1", print_hash_value);
    transparent_crc(g_2572.f2, "g_2572.f2", print_hash_value);
    transparent_crc(g_2572.f3, "g_2572.f3", print_hash_value);
    transparent_crc(g_2572.f4, "g_2572.f4", print_hash_value);
    transparent_crc(g_2572.f5, "g_2572.f5", print_hash_value);
    transparent_crc(g_2572.f6, "g_2572.f6", print_hash_value);
    transparent_crc(g_2572.f7, "g_2572.f7", print_hash_value);
    transparent_crc(g_2572.f8.f0, "g_2572.f8.f0", print_hash_value);
    transparent_crc(g_2572.f8.f1, "g_2572.f8.f1", print_hash_value);
    transparent_crc(g_2572.f8.f2, "g_2572.f8.f2", print_hash_value);
    transparent_crc(g_2572.f9, "g_2572.f9", print_hash_value);
    transparent_crc(g_2573.f0, "g_2573.f0", print_hash_value);
    transparent_crc(g_2573.f1, "g_2573.f1", print_hash_value);
    transparent_crc(g_2573.f2, "g_2573.f2", print_hash_value);
    transparent_crc(g_2573.f3, "g_2573.f3", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_2624[i][j].f0, "g_2624[i][j].f0", print_hash_value);
            transparent_crc(g_2624[i][j].f1, "g_2624[i][j].f1", print_hash_value);
            transparent_crc(g_2624[i][j].f2, "g_2624[i][j].f2", print_hash_value);
            transparent_crc(g_2624[i][j].f3, "g_2624[i][j].f3", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2638.f0, "g_2638.f0", print_hash_value);
    transparent_crc(g_2638.f1, "g_2638.f1", print_hash_value);
    transparent_crc(g_2638.f2, "g_2638.f2", print_hash_value);
    transparent_crc(g_2638.f3, "g_2638.f3", print_hash_value);
    transparent_crc(g_2663.f0, "g_2663.f0", print_hash_value);
    transparent_crc(g_2663.f1, "g_2663.f1", print_hash_value);
    transparent_crc(g_2663.f2, "g_2663.f2", print_hash_value);
    transparent_crc(g_2663.f3, "g_2663.f3", print_hash_value);
    transparent_crc(g_2663.f4, "g_2663.f4", print_hash_value);
    transparent_crc(g_2663.f5, "g_2663.f5", print_hash_value);
    transparent_crc(g_2663.f6, "g_2663.f6", print_hash_value);
    transparent_crc(g_2663.f7, "g_2663.f7", print_hash_value);
    transparent_crc(g_2663.f8.f0, "g_2663.f8.f0", print_hash_value);
    transparent_crc(g_2663.f8.f1, "g_2663.f8.f1", print_hash_value);
    transparent_crc(g_2663.f8.f2, "g_2663.f8.f2", print_hash_value);
    transparent_crc(g_2663.f9, "g_2663.f9", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_2735[i][j].f0, "g_2735[i][j].f0", print_hash_value);
            transparent_crc(g_2735[i][j].f1, "g_2735[i][j].f1", print_hash_value);
            transparent_crc(g_2735[i][j].f2, "g_2735[i][j].f2", print_hash_value);
            transparent_crc(g_2735[i][j].f3, "g_2735[i][j].f3", print_hash_value);
            transparent_crc(g_2735[i][j].f4, "g_2735[i][j].f4", print_hash_value);
            transparent_crc(g_2735[i][j].f5, "g_2735[i][j].f5", print_hash_value);
            transparent_crc(g_2735[i][j].f6, "g_2735[i][j].f6", print_hash_value);
            transparent_crc(g_2735[i][j].f7, "g_2735[i][j].f7", print_hash_value);
            transparent_crc(g_2735[i][j].f8.f0, "g_2735[i][j].f8.f0", print_hash_value);
            transparent_crc(g_2735[i][j].f8.f1, "g_2735[i][j].f8.f1", print_hash_value);
            transparent_crc(g_2735[i][j].f8.f2, "g_2735[i][j].f8.f2", print_hash_value);
            transparent_crc(g_2735[i][j].f9, "g_2735[i][j].f9", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2777, "g_2777", print_hash_value);
    transparent_crc(g_2816.f0, "g_2816.f0", print_hash_value);
    transparent_crc(g_2816.f1, "g_2816.f1", print_hash_value);
    transparent_crc(g_2816.f2, "g_2816.f2", print_hash_value);
    transparent_crc(g_2816.f3, "g_2816.f3", print_hash_value);
    transparent_crc(g_2816.f4, "g_2816.f4", print_hash_value);
    transparent_crc(g_2816.f5, "g_2816.f5", print_hash_value);
    transparent_crc(g_2816.f6, "g_2816.f6", print_hash_value);
    transparent_crc(g_2816.f7, "g_2816.f7", print_hash_value);
    transparent_crc(g_2816.f8.f0, "g_2816.f8.f0", print_hash_value);
    transparent_crc(g_2816.f8.f1, "g_2816.f8.f1", print_hash_value);
    transparent_crc(g_2816.f8.f2, "g_2816.f8.f2", print_hash_value);
    transparent_crc(g_2816.f9, "g_2816.f9", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2821[i].f0, "g_2821[i].f0", print_hash_value);
        transparent_crc(g_2821[i].f1, "g_2821[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2822.f0, "g_2822.f0", print_hash_value);
    transparent_crc(g_2822.f1, "g_2822.f1", print_hash_value);
    transparent_crc(g_2822.f2, "g_2822.f2", print_hash_value);
    transparent_crc(g_2822.f3, "g_2822.f3", print_hash_value);
    transparent_crc(g_2822.f4, "g_2822.f4", print_hash_value);
    transparent_crc(g_2825.f0, "g_2825.f0", print_hash_value);
    transparent_crc(g_2825.f1, "g_2825.f1", print_hash_value);
    transparent_crc(g_2825.f2, "g_2825.f2", print_hash_value);
    transparent_crc(g_2825.f3, "g_2825.f3", print_hash_value);
    transparent_crc(g_2825.f4, "g_2825.f4", print_hash_value);
    transparent_crc(g_2825.f5, "g_2825.f5", print_hash_value);
    transparent_crc(g_2825.f6, "g_2825.f6", print_hash_value);
    transparent_crc(g_2825.f7, "g_2825.f7", print_hash_value);
    transparent_crc(g_2825.f8.f0, "g_2825.f8.f0", print_hash_value);
    transparent_crc(g_2825.f8.f1, "g_2825.f8.f1", print_hash_value);
    transparent_crc(g_2825.f8.f2, "g_2825.f8.f2", print_hash_value);
    transparent_crc(g_2825.f9, "g_2825.f9", print_hash_value);
    transparent_crc(g_2836, "g_2836", print_hash_value);
    transparent_crc(g_2837.f0, "g_2837.f0", print_hash_value);
    transparent_crc(g_2837.f1, "g_2837.f1", print_hash_value);
    transparent_crc(g_2837.f2, "g_2837.f2", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc_bytes(&g_2840[i][j], sizeof(g_2840[i][j]), "g_2840[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2857.f0, "g_2857.f0", print_hash_value);
    transparent_crc(g_2857.f1, "g_2857.f1", print_hash_value);
    transparent_crc(g_2857.f2, "g_2857.f2", print_hash_value);
    transparent_crc(g_2857.f3, "g_2857.f3", print_hash_value);
    transparent_crc(g_2857.f4, "g_2857.f4", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_2891[i][j], "g_2891[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2935[i].f0, "g_2935[i].f0", print_hash_value);
        transparent_crc(g_2935[i].f1, "g_2935[i].f1", print_hash_value);
        transparent_crc(g_2935[i].f2, "g_2935[i].f2", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2992, "g_2992", print_hash_value);
    transparent_crc(g_2996.f0, "g_2996.f0", print_hash_value);
    transparent_crc(g_2996.f1, "g_2996.f1", print_hash_value);
    transparent_crc(g_2996.f2, "g_2996.f2", print_hash_value);
    transparent_crc(g_2996.f3, "g_2996.f3", print_hash_value);
    transparent_crc(g_2996.f4, "g_2996.f4", print_hash_value);
    transparent_crc(g_2996.f5, "g_2996.f5", print_hash_value);
    transparent_crc(g_2996.f6, "g_2996.f6", print_hash_value);
    transparent_crc(g_2996.f7, "g_2996.f7", print_hash_value);
    transparent_crc(g_2996.f8.f0, "g_2996.f8.f0", print_hash_value);
    transparent_crc(g_2996.f8.f1, "g_2996.f8.f1", print_hash_value);
    transparent_crc(g_2996.f8.f2, "g_2996.f8.f2", print_hash_value);
    transparent_crc(g_2996.f9, "g_2996.f9", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 2
breakdown:
   depth: 0, occurrence: 719
   depth: 1, occurrence: 14
   depth: 2, occurrence: 11
XXX total union variables: 22

XXX non-zero bitfields defined in structs: 10
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 2
XXX volatile bitfields defined in structs: 2
XXX structs with bitfields in the program: 103
breakdown:
   indirect level: 0, occurrence: 42
   indirect level: 1, occurrence: 25
   indirect level: 2, occurrence: 20
   indirect level: 3, occurrence: 10
   indirect level: 4, occurrence: 2
   indirect level: 5, occurrence: 4
XXX full-bitfields structs in the program: 14
breakdown:
   indirect level: 0, occurrence: 14
XXX times a bitfields struct's address is taken: 55
XXX times a bitfields struct on LHS: 1
XXX times a bitfields struct on RHS: 58
XXX times a single bitfield on LHS: 6
XXX times a single bitfield on RHS: 104

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 226
   depth: 2, occurrence: 55
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
   depth: 5, occurrence: 3
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 10, occurrence: 1
   depth: 12, occurrence: 3
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 20, occurrence: 1
   depth: 22, occurrence: 2
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 27, occurrence: 5
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 33, occurrence: 1
   depth: 35, occurrence: 2
   depth: 41, occurrence: 1
   depth: 44, occurrence: 2
   depth: 45, occurrence: 1
   depth: 52, occurrence: 1

XXX total number of pointers: 694

XXX times a variable address is taken: 1849
XXX times a pointer is dereferenced on RHS: 361
breakdown:
   depth: 1, occurrence: 284
   depth: 2, occurrence: 55
   depth: 3, occurrence: 4
   depth: 4, occurrence: 16
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 384
breakdown:
   depth: 1, occurrence: 358
   depth: 2, occurrence: 20
   depth: 3, occurrence: 1
   depth: 4, occurrence: 4
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 45
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 20
XXX times a pointer is qualified to be dereferenced: 8801

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1518
   level: 2, occurrence: 216
   level: 3, occurrence: 30
   level: 4, occurrence: 52
   level: 5, occurrence: 9
XXX number of pointers point to pointers: 267
XXX number of pointers point to scalars: 378
XXX number of pointers point to structs: 19
XXX percent of pointers has null in alias set: 27.8
XXX average alias set size: 1.52

XXX times a non-volatile is read: 2324
XXX times a non-volatile is write: 1136
XXX times a volatile is read: 132
XXX    times read thru a pointer: 44
XXX times a volatile is write: 25
XXX    times written thru a pointer: 5
XXX times a volatile is available for access: 4.05e+03
XXX percentage of non-volatile access: 95.7

XXX forward jumps: 3
XXX backward jumps: 15

XXX stmts: 230
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 47
   depth: 2, occurrence: 40
   depth: 3, occurrence: 45
   depth: 4, occurrence: 41
   depth: 5, occurrence: 25

XXX percentage a fresh-made variable is used: 18.6
XXX percentage an existing variable is used: 81.4
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/


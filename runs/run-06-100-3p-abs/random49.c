/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2147711739
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint8_t g_8 = 255UL;
static int32_t g_22 = 0x52AA0A5DL;
static int32_t g_38[4][4] = {{0x6BC22736L,0x3659EA4DL,0x3659EA4DL,0x6BC22736L},{0x6BC22736L,0x3659EA4DL,0x3659EA4DL,0x6BC22736L},{0x6BC22736L,0x3659EA4DL,0x3659EA4DL,0x6BC22736L},{0x6BC22736L,0x3659EA4DL,0x3659EA4DL,0x6BC22736L}};
static int32_t *g_37 = &g_38[1][0];
static int32_t ** volatile g_60 = &g_37;/* VOLATILE GLOBAL g_60 */
static uint64_t g_88 = 0x925CFB43B78C37F2LL;
static int8_t g_90[7][9] = {{0x49L,0xBBL,0xBBL,0x49L,0x3FL,0L,1L,0x02L,5L},{0xD6L,0x02L,0xBBL,5L,5L,0xBBL,0x02L,0xD6L,0xDCL},{0L,5L,5L,0xD6L,0x3FL,0x02L,0x02L,0x3FL,0xD6L},{0x71L,0L,0x71L,0x78L,0x3FL,0xBBL,0x71L,1L,1L},{0x49L,0xD6L,1L,0x3FL,1L,0xD6L,0x49L,0x71L,0xDCL},{0x71L,0xBBL,0x3FL,0x78L,5L,0xD6L,5L,0x78L,0x3FL},{0x3FL,0x3FL,0L,0L,0xDCL,0xBBL,0xD6L,0x71L,0xD6L}};
static uint8_t g_94 = 1UL;
static int32_t ** volatile g_96 = &g_37;/* VOLATILE GLOBAL g_96 */
static int16_t g_110 = 0xC55BL;
static int16_t g_112 = 0x3F42L;
static int16_t g_114 = 1L;
static int32_t ** volatile g_115 = &g_37;/* VOLATILE GLOBAL g_115 */
static int8_t g_133 = (-8L);
static int32_t g_143[1] = {0L};
static uint32_t g_157[5][5][10] = {{{3UL,0UL,1UL,0xAFFF8CD5L,0x8A13AAE1L,3UL,0xD7F04335L,3UL,0x504B5D79L,0xA6D5B5E9L},{1UL,0xBC80B3D8L,8UL,1UL,0xEBFDBDBAL,3UL,4UL,2UL,0x502811D3L,8UL},{3UL,0x49A0EE20L,1UL,2UL,0x91EDA5D4L,6UL,0xAFFF8CD5L,4294967294UL,1UL,0x8A13AAE1L},{0x5C3DF7CFL,0x139CCBA0L,0UL,1UL,4294967295UL,0xCD540B5EL,8UL,0x7DA9072AL,1UL,4294967295UL},{0x7BBD1554L,3UL,0x5DF7E9F5L,0xDCF7CF00L,1UL,1UL,0xDCF7CF00L,0x5DF7E9F5L,3UL,0x7BBD1554L}},{{0x504B5D79L,0x7DA9072AL,0x49A0EE20L,6UL,4294967295UL,0x502811D3L,0x7BBD1554L,1UL,1UL,9UL},{0x34B8501FL,0xD7F04335L,0UL,0x7DA9072AL,4294967295UL,0xEBFDBDBAL,1UL,0x5C3DF7CFL,0x26374416L,0x7BBD1554L},{4294967295UL,0x7BBD1554L,0xA6D5B5E9L,0xAFFF8CD5L,1UL,0UL,0UL,4294967295UL,1UL,4294967295UL},{1UL,0xCD540B5EL,0x8A13AAE1L,0x5C3DF7CFL,4294967295UL,0xA6D5B5E9L,3UL,2UL,0x7DA9072AL,0x8A13AAE1L},{0xA8EAFA31L,9UL,0xE1DEC708L,1UL,0x91EDA5D4L,1UL,0xE1DEC708L,9UL,0xA8EAFA31L,8UL}},{{0xCD540B5EL,0x139CCBA0L,0xDCF7CF00L,4UL,0xEBFDBDBAL,0x7DA9072AL,1UL,0x502811D3L,1UL,0xA6D5B5E9L},{0UL,0xA8EAFA31L,0x26374416L,4UL,0x8A13AAE1L,1UL,4294967295UL,0xD7F04335L,0xA8EAFA31L,0x5DF7E9F5L},{1UL,0x502811D3L,0x49A0EE20L,1UL,1UL,0xBC80B3D8L,0x5DF7E9F5L,0x26374416L,0x7DA9072AL,9UL},{0x502811D3L,0x26374416L,0x504B5D79L,0x5C3DF7CFL,4294967294UL,0x139CCBA0L,1UL,0x34B8501FL,1UL,1UL},{0x49A0EE20L,0x504B5D79L,4294967295UL,0xAFFF8CD5L,0xAFFF8CD5L,4294967295UL,0x504B5D79L,0x49A0EE20L,0x26374416L,0xDCF7CF00L}},{{1UL,0x5C3DF7CFL,0xAFFF8CD5L,0x7DA9072AL,6UL,0xDCF7CF00L,1UL,3UL,0x5DF7E9F5L,0xA8EAFA31L},{0x1AC7F9B7L,0x91EDA5D4L,0xA8EAFA31L,0xA6D5B5E9L,0x5C3DF7CFL,3UL,3UL,0x18EFB25BL,0x0B276921L,4294967295UL},{0x504B5D79L,0xDCF7CF00L,0x8A13AAE1L,1UL,3UL,0x7BBD1554L,4294967294UL,0x26374416L,0UL,0xAFFF8CD5L},{1UL,0x1AC7F9B7L,2UL,0xAFFF8CD5L,3UL,4294967294UL,1UL,0x139CCBA0L,0x1AC7F9B7L,0x139CCBA0L},{0x139CCBA0L,0x26374416L,0x18EFB25BL,3UL,0x18EFB25BL,0x26374416L,0x139CCBA0L,4294967295UL,0UL,0xE132C18EL}},{{1UL,6UL,0x139CCBA0L,0x5DF7E9F5L,0x0B276921L,0UL,0x1AC7F9B7L,0UL,2UL,4294967295UL},{0xE132C18EL,6UL,1UL,0xA8EAFA31L,4294967295UL,0xAFFF8CD5L,0x139CCBA0L,0xE132C18EL,4294967295UL,8UL},{1UL,0x26374416L,9UL,0xD7F04335L,0UL,0xE1DEC708L,1UL,3UL,0x504B5D79L,9UL},{0x91EDA5D4L,0x1AC7F9B7L,0x49A0EE20L,4294967295UL,0x5C3DF7CFL,4UL,4294967294UL,0xF1C9105AL,0xF1C9105AL,4294967294UL},{0x5DF7E9F5L,0xDCF7CF00L,1UL,1UL,0xDCF7CF00L,0x5DF7E9F5L,3UL,0x7BBD1554L,0UL,1UL}}};
static int8_t g_158 = (-6L);
static int32_t ** volatile g_183 = (void*)0;/* VOLATILE GLOBAL g_183 */
static float g_224 = 0xC.B0287Ep-1;
static int64_t g_295 = 0x696D96D8BCE761BELL;
static uint64_t g_304 = 18446744073709551615UL;
static float g_324 = 0x9.C0721Cp-64;
static volatile uint64_t g_329 = 18446744073709551615UL;/* VOLATILE GLOBAL g_329 */
static int64_t g_348 = 0xDFDDE1CF89A00F02LL;
static uint32_t g_386[6] = {4294967287UL,4294967287UL,4294967287UL,4294967287UL,4294967287UL,4294967287UL};
static uint32_t g_392 = 6UL;
static int64_t *g_406 = &g_295;
static const int32_t *g_449 = &g_38[3][2];
static const int32_t ** volatile g_448 = &g_449;/* VOLATILE GLOBAL g_448 */
static uint16_t g_455 = 1UL;
static uint16_t *g_488[6] = {&g_455,&g_455,&g_455,&g_455,&g_455,&g_455};
static uint16_t g_492 = 8UL;
static float *** volatile g_527 = (void*)0;/* VOLATILE GLOBAL g_527 */
static float *g_530 = &g_224;
static float **g_529 = &g_530;
static float *** volatile g_528 = &g_529;/* VOLATILE GLOBAL g_528 */
static const int32_t ** volatile g_574 = &g_449;/* VOLATILE GLOBAL g_574 */
static int32_t g_583 = 0x8457FD53L;
static int32_t **g_629 = &g_37;
static int32_t ***g_628 = &g_629;
static const volatile uint16_t g_659 = 0x690BL;/* VOLATILE GLOBAL g_659 */
static const volatile uint16_t *g_658 = &g_659;
static const volatile uint16_t * volatile *g_657 = &g_658;
static const volatile uint16_t * volatile * volatile * const g_656[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static volatile int16_t g_722 = 1L;/* VOLATILE GLOBAL g_722 */
static volatile int16_t *g_721 = &g_722;
static volatile int16_t **g_720 = &g_721;
static float * volatile g_764[10][10] = {{(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324},{(void*)0,&g_324,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0},{(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324},{(void*)0,&g_324,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0},{(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324},{(void*)0,&g_324,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0},{(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0,&g_324},{(void*)0,&g_324,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,(void*)0,(void*)0},{&g_324,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,&g_324,(void*)0,&g_324},{&g_324,&g_324,&g_324,&g_324,(void*)0,&g_324,&g_324,&g_324,(void*)0,(void*)0}};
static const uint16_t g_784 = 0x1FC4L;
static int32_t ****g_803 = &g_628;
static float g_804[6] = {0xC.95539Ep-79,0xC.95539Ep-79,0x1.D6468Ep+92,0xC.95539Ep-79,0xC.95539Ep-79,0x1.D6468Ep+92};
static const int16_t g_928 = (-5L);
static uint64_t g_944[8][9][3] = {{{0UL,0UL,0UL},{0x22F865693306F22DLL,0xA8E772512E49AFC9LL,18446744073709551606UL},{0xDC2C7E3DCC1FA3BFLL,0xF80FB80DEEBFC554LL,18446744073709551606UL},{0UL,0UL,0UL},{0xECBB31AA13BA49CFLL,0xAE6723873F225039LL,18446744073709551613UL},{0x2BCCF4201DD1221CLL,1UL,0xECBB31AA13BA49CFLL},{0x7554CE808BA77726LL,0xB4030CD349FD47C6LL,0xFA6C342AEC042DBDLL},{0xB2B8AF85D7F1C410LL,0xBF30E765D4440780LL,0UL},{0xA8E772512E49AFC9LL,0xB4030CD349FD47C6LL,0xF80FB80DEEBFC554LL}},{{0x904C0CE81563650DLL,1UL,1UL},{0x37A3E43789C1E0DBLL,0xAE6723873F225039LL,0x22F865693306F22DLL},{0UL,0UL,3UL},{0UL,0xF80FB80DEEBFC554LL,0xBF30E765D4440780LL},{0UL,0xA8E772512E49AFC9LL,0x37A3E43789C1E0DBLL},{0UL,0UL,0xB2B8AF85D7F1C410LL},{0x37A3E43789C1E0DBLL,0xDC2C7E3DCC1FA3BFLL,0x2BCCF4201DD1221CLL},{0x904C0CE81563650DLL,0UL,0x45FB7F8D9D5946B3LL},{0xA8E772512E49AFC9LL,0x45FB7F8D9D5946B3LL,0xBD3292FC775C90FELL}},{{0xB2B8AF85D7F1C410LL,0x904C0CE81563650DLL,0x45FB7F8D9D5946B3LL},{0x7554CE808BA77726LL,18446744073709551613UL,0x2BCCF4201DD1221CLL},{0x2BCCF4201DD1221CLL,18446744073709551606UL,0xB2B8AF85D7F1C410LL},{0xECBB31AA13BA49CFLL,0x2BCCF4201DD1221CLL,0x37A3E43789C1E0DBLL},{0UL,1UL,0xBF30E765D4440780LL},{0xDC2C7E3DCC1FA3BFLL,1UL,3UL},{0x22F865693306F22DLL,0x2BCCF4201DD1221CLL,0x22F865693306F22DLL},{0UL,18446744073709551606UL,1UL},{0xADA583820E6AB50ALL,18446744073709551613UL,0xF80FB80DEEBFC554LL}},{{0xAE6723873F225039LL,0x904C0CE81563650DLL,0UL},{1UL,0x45FB7F8D9D5946B3LL,0xFA6C342AEC042DBDLL},{0xAE6723873F225039LL,0UL,0xECBB31AA13BA49CFLL},{0xADA583820E6AB50ALL,0xDC2C7E3DCC1FA3BFLL,18446744073709551613UL},{0UL,0UL,0UL},{0x22F865693306F22DLL,0xA8E772512E49AFC9LL,18446744073709551606UL},{0xDC2C7E3DCC1FA3BFLL,0xF80FB80DEEBFC554LL,18446744073709551606UL},{0UL,0UL,0UL},{0xECBB31AA13BA49CFLL,0xAE6723873F225039LL,18446744073709551613UL}},{{0x2BCCF4201DD1221CLL,1UL,0xECBB31AA13BA49CFLL},{0x7554CE808BA77726LL,0xB4030CD349FD47C6LL,0xFA6C342AEC042DBDLL},{0xB2B8AF85D7F1C410LL,0xBF30E765D4440780LL,0UL},{0xA8E772512E49AFC9LL,0xB4030CD349FD47C6LL,0xF80FB80DEEBFC554LL},{0x904C0CE81563650DLL,1UL,1UL},{0x37A3E43789C1E0DBLL,0xAE6723873F225039LL,0x22F865693306F22DLL},{0UL,0UL,3UL},{0UL,0xF80FB80DEEBFC554LL,0xBF30E765D4440780LL},{0UL,0xA8E772512E49AFC9LL,0x37A3E43789C1E0DBLL}},{{0UL,0UL,0xB2B8AF85D7F1C410LL},{0x37A3E43789C1E0DBLL,0xDC2C7E3DCC1FA3BFLL,0x2BCCF4201DD1221CLL},{0x904C0CE81563650DLL,0UL,0x45FB7F8D9D5946B3LL},{0xA8E772512E49AFC9LL,0x45FB7F8D9D5946B3LL,0xBD3292FC775C90FELL},{0xB2B8AF85D7F1C410LL,0x904C0CE81563650DLL,0x45FB7F8D9D5946B3LL},{0x7554CE808BA77726LL,18446744073709551613UL,0x2BCCF4201DD1221CLL},{0x2BCCF4201DD1221CLL,18446744073709551606UL,0xB2B8AF85D7F1C410LL},{0xECBB31AA13BA49CFLL,0x2BCCF4201DD1221CLL,0x37A3E43789C1E0DBLL},{0UL,1UL,0xBF30E765D4440780LL}},{{0xDC2C7E3DCC1FA3BFLL,1UL,18446744073709551606UL},{0x7554CE808BA77726LL,0UL,0x7554CE808BA77726LL},{18446744073709551613UL,0UL,3UL},{0xB2B8AF85D7F1C410LL,18446744073709551615UL,0x904C0CE81563650DLL},{0xADA583820E6AB50ALL,0xBF30E765D4440780LL,0UL},{3UL,0x2BCCF4201DD1221CLL,0xBD3292FC775C90FELL},{0xADA583820E6AB50ALL,0x37A3E43789C1E0DBLL,0x51B9FD79C856AC75LL},{0xB2B8AF85D7F1C410LL,0xA8E772512E49AFC9LL,18446744073709551615UL},{18446744073709551613UL,1UL,0UL}},{{0x7554CE808BA77726LL,1UL,0UL},{0xA8E772512E49AFC9LL,0x904C0CE81563650DLL,0UL},{0UL,0UL,0UL},{0x51B9FD79C856AC75LL,0xADA583820E6AB50ALL,18446744073709551615UL},{0UL,0x45FB7F8D9D5946B3LL,0x51B9FD79C856AC75LL},{0x2729D6DC62E37AC0LL,1UL,0xBD3292FC775C90FELL},{0UL,0xFA6C342AEC042DBDLL,0UL},{1UL,1UL,0x904C0CE81563650DLL},{0xBF30E765D4440780LL,0x45FB7F8D9D5946B3LL,3UL}}};
static int16_t * const * volatile *g_1044[2] = {(void*)0,(void*)0};
static int16_t * const * volatile **g_1043 = &g_1044[0];
static int32_t g_1078 = 1L;
static int32_t * volatile * volatile g_1093 = &g_37;/* VOLATILE GLOBAL g_1093 */
static int32_t **g_1119[5] = {&g_37,&g_37,&g_37,&g_37,&g_37};
static uint16_t g_1122 = 0x0186L;
static const int32_t ** volatile g_1138 = &g_449;/* VOLATILE GLOBAL g_1138 */
static int32_t * volatile g_1160 = &g_38[0][1];/* VOLATILE GLOBAL g_1160 */
static float g_1161 = 0xE.068502p-8;
static uint16_t *g_1214 = (void*)0;
static const int16_t g_1221 = 0L;
static int16_t *g_1242[7] = {&g_110,&g_110,&g_110,&g_110,&g_110,&g_110,&g_110};
static int16_t **g_1241 = &g_1242[5];
static int16_t ***g_1240 = &g_1241;
static int16_t ****g_1239 = &g_1240;
static const int64_t **g_1255 = (void*)0;
static volatile uint16_t g_1342 = 0x3D66L;/* VOLATILE GLOBAL g_1342 */
static int8_t *g_1373 = &g_158;
static int8_t **g_1372 = &g_1373;
static int8_t *** volatile g_1371 = &g_1372;/* VOLATILE GLOBAL g_1371 */
static uint16_t **g_1384 = &g_488[3];
static uint16_t ***g_1383[9] = {&g_1384,&g_1384,&g_1384,&g_1384,&g_1384,&g_1384,&g_1384,&g_1384,&g_1384};
static int32_t g_1409 = 0xAC418A2FL;
static uint16_t ****g_1468[8] = {&g_1383[4],&g_1383[4],&g_1383[4],&g_1383[4],&g_1383[4],&g_1383[4],&g_1383[4],&g_1383[4]};
static volatile int32_t g_1491 = 0L;/* VOLATILE GLOBAL g_1491 */
static float g_1585 = 0xF.DB5B6Bp-18;
static int64_t g_1604 = (-5L);
static uint16_t g_1605 = 65533UL;
static int8_t g_1625 = 0x20L;
static uint32_t g_1658 = 3UL;
static uint32_t g_1660 = 0xD0E965B9L;
static uint8_t g_1717 = 0UL;
static uint8_t g_1730 = 0x5CL;
static uint32_t g_1742 = 0x53594240L;
static volatile uint16_t g_1753 = 0x6751L;/* VOLATILE GLOBAL g_1753 */
static const int64_t ***g_1758 = &g_1255;
static const int64_t **** volatile g_1757 = &g_1758;/* VOLATILE GLOBAL g_1757 */
static volatile int8_t * volatile ** const  volatile *g_1777 = (void*)0;
static volatile int8_t * volatile ** const  volatile ** volatile g_1776 = &g_1777;/* VOLATILE GLOBAL g_1776 */
static int64_t **g_1783 = &g_406;
static int64_t ***g_1782 = &g_1783;
static uint8_t *g_1870 = &g_8;
static uint8_t **g_1869 = &g_1870;
static uint8_t ***g_1868 = &g_1869;
static uint8_t **** volatile g_1867 = &g_1868;/* VOLATILE GLOBAL g_1867 */
static volatile int32_t g_1877 = 0xFBF77D75L;/* VOLATILE GLOBAL g_1877 */
static uint64_t * const g_1981 = &g_944[6][4][2];
static uint64_t * const  volatile *g_1980 = &g_1981;
static uint16_t *** volatile g_2024 = &g_1384;/* VOLATILE GLOBAL g_2024 */
static uint64_t *g_2082 = &g_304;
static volatile uint32_t g_2096 = 0xB123ABEDL;/* VOLATILE GLOBAL g_2096 */
static uint32_t *g_2154[10][10][2] = {{{&g_157[1][1][9],&g_157[1][1][9]},{(void*)0,(void*)0},{&g_157[1][1][9],&g_392},{&g_392,&g_157[3][2][5]},{&g_157[3][2][4],&g_157[3][4][6]},{&g_157[3][4][6],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][3][1]},{&g_157[1][1][9],(void*)0},{(void*)0,&g_392},{&g_157[2][0][6],&g_157[3][4][6]}},{{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[3][4][6]},{&g_157[2][0][6],&g_392},{(void*)0,(void*)0},{&g_157[1][1][9],&g_157[1][3][1]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[3][4][6],&g_157[3][4][6]}},{{&g_157[3][2][4],&g_157[3][2][5]},{&g_392,&g_392},{&g_157[1][1][9],(void*)0},{(void*)0,&g_157[1][1][9]},{&g_157[1][1][9],&g_157[3][4][6]},{&g_157[1][1][9],&g_157[1][1][9]},{(void*)0,(void*)0},{&g_157[1][1][9],&g_392},{&g_392,&g_157[3][2][5]},{&g_157[3][2][4],&g_157[3][4][6]}},{{&g_157[3][4][6],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][3][1]},{&g_157[1][1][9],(void*)0},{(void*)0,&g_392},{&g_157[2][0][6],&g_157[3][4][6]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[3][4][6]}},{{&g_157[2][0][6],&g_392},{(void*)0,(void*)0},{&g_157[1][1][9],&g_157[1][3][1]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[3][4][6],&g_157[3][4][6]},{&g_157[3][2][4],&g_157[3][2][5]},{&g_392,&g_392},{&g_157[1][1][9],(void*)0},{(void*)0,&g_157[1][1][9]},{&g_157[1][1][9],&g_157[3][4][6]}},{{&g_157[1][1][9],&g_157[1][1][9]},{(void*)0,(void*)0},{&g_157[1][1][9],&g_392},{&g_392,&g_157[3][2][5]},{&g_157[3][2][5],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[3][4][6]},{&g_157[1][1][9],&g_157[3][2][4]},{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_157[1][1][9]}},{{&g_392,(void*)0},{&g_157[0][3][3],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[0][3][3],(void*)0},{&g_392,&g_157[1][1][9]},{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_157[3][2][4]},{&g_157[1][1][9],&g_157[3][4][6]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]}},{{&g_157[3][2][5],(void*)0},{&g_157[1][1][9],&g_157[2][0][6]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[2][0][6]},{&g_157[1][1][9],(void*)0},{&g_157[3][2][5],&g_157[1][1][9]}},{{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[3][4][6]},{&g_157[1][1][9],&g_157[3][2][4]},{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_157[1][1][9]},{&g_392,(void*)0},{&g_157[0][3][3],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[0][3][3],(void*)0},{&g_392,&g_157[1][1][9]}},{{&g_157[1][1][9],&g_392},{&g_157[1][1][9],&g_157[3][2][4]},{&g_157[1][1][9],&g_157[3][4][6]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[3][2][5],(void*)0},{&g_157[1][1][9],&g_157[2][0][6]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]},{&g_157[1][1][9],&g_157[1][1][9]}}};
static uint8_t *g_2165 = &g_1717;
static uint32_t *g_2271 = &g_1660;
static uint32_t **g_2270[10] = {&g_2271,&g_2271,&g_2271,&g_2271,&g_2271,&g_2271,&g_2271,&g_2271,&g_2271,&g_2271};
static int32_t *g_2277 = (void*)0;
static int32_t ** volatile g_2276 = &g_2277;/* VOLATILE GLOBAL g_2276 */
static uint32_t ***g_2365 = (void*)0;
static uint32_t ****g_2364 = &g_2365;
static uint32_t *****g_2363 = &g_2364;
static int32_t *g_2450[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const int64_t ****g_2473 = &g_1758;
static const int64_t ***** volatile g_2472[8][7] = {{&g_2473,(void*)0,&g_2473,&g_2473,&g_2473,&g_2473,(void*)0},{(void*)0,&g_2473,&g_2473,&g_2473,&g_2473,(void*)0,&g_2473},{&g_2473,&g_2473,&g_2473,&g_2473,&g_2473,(void*)0,&g_2473},{&g_2473,&g_2473,&g_2473,&g_2473,&g_2473,&g_2473,&g_2473},{&g_2473,&g_2473,&g_2473,&g_2473,(void*)0,&g_2473,&g_2473},{&g_2473,&g_2473,&g_2473,&g_2473,&g_2473,&g_2473,&g_2473},{(void*)0,&g_2473,&g_2473,(void*)0,&g_2473,&g_2473,(void*)0},{(void*)0,&g_2473,&g_2473,&g_2473,(void*)0,&g_2473,&g_2473}};
static volatile float g_2541 = 0xD.940D45p-14;/* VOLATILE GLOBAL g_2541 */
static float g_2553[4] = {(-0x9.9p+1),(-0x9.9p+1),(-0x9.9p+1),(-0x9.9p+1)};
static int32_t * volatile g_2578 = &g_22;/* VOLATILE GLOBAL g_2578 */
static float g_2605 = (-0x4.4p-1);
static int32_t ** volatile g_2621 = &g_2277;/* VOLATILE GLOBAL g_2621 */
static const int16_t g_2691 = (-2L);
static volatile uint64_t g_2716 = 0xD9B6EDE9FFD4C033LL;/* VOLATILE GLOBAL g_2716 */


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static const int32_t  func_2(uint8_t  p_3, uint16_t  p_4, int64_t  p_5, int32_t  p_6, uint16_t  p_7);
static int32_t  func_10(uint32_t  p_11);
static int32_t  func_15(float  p_16, uint8_t  p_17, int32_t  p_18);
static int8_t  func_19(uint16_t  p_20);
static float  func_25(int32_t * p_26, const int8_t  p_27, const int64_t  p_28, int32_t * p_29, int32_t * p_30);
static int32_t * func_31(int32_t * p_32, int64_t  p_33);
static int32_t * func_34(int8_t  p_35, const int32_t * p_36);
static int32_t * const  func_41(int32_t * p_42, int32_t  p_43, uint32_t  p_44, int32_t * p_45);
static uint32_t  func_46(int32_t * p_47, int16_t  p_48, int32_t  p_49, uint32_t  p_50);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_8 g_37 g_38 g_60 g_96 g_90 g_157 g_94 g_112 g_110 g_448 g_143 g_158 g_304 g_88 g_449 g_492 g_329 g_406 g_295 g_528 g_386 g_529 g_114 g_574 g_583 g_628 g_629 g_530 g_656 g_455 g_224 g_657 g_658 g_659 g_1161 g_348 g_133 g_1138 g_1373 g_1240 g_1241 g_1242 g_1221 g_944 g_1371 g_1372 g_928 g_1604 g_1239 g_721 g_722 g_1658 g_1078 g_803 g_1409 g_1717 g_1742 g_1160 g_1753 g_1757 g_22 g_1776 g_784 g_1782 g_1783 g_1043 g_1044 g_1122 g_1867 g_1870 g_1758 g_1255 g_1214 g_1869 g_1660 g_1625 g_1605 g_1980 g_1981 g_1777 g_1119 g_2024 g_115 g_1868 g_2096 g_2270 g_2276 g_2082 g_1730 g_2271 g_720 g_2165 g_2716 g_2621
 * writes: g_22 g_37 g_94 g_110 g_449 g_455 g_143 g_488 g_38 g_158 g_114 g_112 g_529 g_88 g_304 g_90 g_628 g_224 g_583 g_295 g_492 g_804 g_348 g_1605 g_1658 g_1660 g_133 g_1239 g_1717 g_386 g_1730 g_1753 g_1758 g_1122 g_1409 g_1868 g_1625 g_1242 g_1384 g_944 g_2082 g_1468 g_2154 g_2165 g_2277 g_2363 g_2716 g_8
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int16_t l_9 = 1L;
    int32_t *l_39 = (void*)0;
    uint32_t l_1013 = 1UL;
    int32_t *l_1077[1][1][6] = {{{&g_1078,(void*)0,(void*)0,&g_1078,(void*)0,(void*)0}}};
    float *l_1561 = &g_804[4];
    uint8_t l_1562 = 255UL;
    float *l_1563[10] = {(void*)0,&g_324,&g_324,&g_324,(void*)0,(void*)0,&g_324,&g_324,&g_324,(void*)0};
    int8_t l_1564 = (-7L);
    int32_t l_1565[7] = {(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)};
    int64_t l_2719 = 0L;
    int8_t l_2724 = 0x9DL;
    int16_t l_2734 = 5L;
    uint32_t l_2736 = 0x4A6100C7L;
    uint64_t l_2750 = 0UL;
    uint8_t l_2763[9][5][5] = {{{251UL,0x76L,255UL,0x23L,0UL},{0x37L,0xA2L,255UL,0xBDL,0x9CL},{0x4FL,255UL,0x9DL,0UL,4UL},{0UL,6UL,251UL,0x37L,248UL},{0x3CL,0x4FL,247UL,1UL,255UL}},{{0xBDL,248UL,1UL,0xF9L,252UL},{0x87L,248UL,255UL,250UL,0x37L},{0UL,0x4FL,255UL,0UL,0xF6L},{255UL,6UL,4UL,4UL,6UL},{0UL,255UL,0x4DL,0x16L,0xADL}},{{255UL,0xA2L,254UL,247UL,255UL},{254UL,0x76L,246UL,255UL,0x3CL},{255UL,251UL,0UL,1UL,0xEDL},{0UL,2UL,0UL,0xADL,1UL},{255UL,0x16L,5UL,0x76L,1UL}},{{0UL,0x9DL,255UL,5UL,2UL},{0x87L,0x6AL,0UL,6UL,2UL},{0xBDL,4UL,0x6AL,1UL,1UL},{0x3CL,1UL,0x40L,0UL,1UL},{0UL,0x3CL,0x3CL,0UL,0xEDL}},{{0x4FL,0x23L,1UL,0xFCL,0x3CL},{0x37L,255UL,1UL,255UL,255UL},{251UL,252UL,0x37L,0xFCL,0xADL},{255UL,0x6CL,0UL,0UL,6UL},{0x9DL,0UL,4UL,0UL,0xF6L}},{{1UL,1UL,0xF6L,1UL,0x37L},{0x37L,247UL,0xA2L,6UL,252UL},{0UL,0UL,0xA2L,5UL,255UL},{0x23L,255UL,0xF6L,0x76L,248UL},{255UL,0xF6L,4UL,0xADL,4UL}},{{5UL,5UL,0UL,1UL,0x9CL},{2UL,0xEDL,0x37L,255UL,0UL},{4UL,251UL,1UL,247UL,0xBDL},{255UL,0xEDL,1UL,0x16L,4UL},{4UL,5UL,0x3CL,4UL,254UL}},{{0xA2L,0xF6L,0x40L,0UL,255UL},{255UL,255UL,0x6AL,250UL,0x87L},{0xEDL,0UL,0x87L,248UL,255UL},{251UL,0UL,0x37L,255UL,252UL},{4UL,0x9DL,0xADL,0xEDL,248UL}},{{246UL,0x87L,0UL,0x87L,246UL},{0x9CL,255UL,255UL,0UL,0xADL},{1UL,0xFCL,0x3CL,4UL,4UL},{0x16L,2UL,0x4FL,255UL,0xADL},{0UL,4UL,255UL,0x4DL,246UL}}};
    uint16_t l_2784 = 65534UL;
    uint64_t **l_2789[2];
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_2789[i] = (void*)0;
    if (func_2(g_8, l_9, g_8, func_10((safe_unary_minus_func_int8_t_s(((**g_1372) = (safe_sub_func_int32_t_s_s(func_15((l_1564 = ((((func_19(l_9) , (-0x8.Ep+1)) != ((*l_1561) = (safe_add_func_float_f_f((g_8 <= (((*g_530) = func_25(func_31(func_34(((g_37 == l_39) , 0x44L), &g_38[0][2]), l_1013), g_386[1], l_1013, l_1077[0][0][3], l_1077[0][0][3])) != g_1161)), g_348)))) >= g_133) <= l_1562)), g_133, l_1565[1]), g_784)))))), l_2719))
    { /* block id: 1321 */
        uint64_t l_2720 = 0xD70777A299FE74D2LL;
        (*g_2621) = ((***g_803) = l_39);
        return l_2720;
    }
    else
    { /* block id: 1325 */
        float l_2723 = 0x0.EA836Dp+23;
        int32_t *l_2726[9][3][9] = {{{&g_1409,&l_1565[2],&g_1409,&l_1565[4],&l_1565[0],&l_1565[1],(void*)0,&l_1565[1],&l_1565[4]},{(void*)0,&l_1565[0],&l_1565[1],(void*)0,&l_1565[1],&g_1409,&l_1565[3],&l_1565[1],&l_1565[1]},{&l_1565[1],(void*)0,(void*)0,(void*)0,&l_1565[1],&l_1565[1],&g_1409,&l_1565[4],&l_1565[5]}},{{&l_1565[1],&g_1409,&l_1565[1],&g_1409,&l_1565[1],&g_1409,&g_1409,(void*)0,&l_1565[4]},{&l_1565[4],&g_1409,(void*)0,&l_1565[1],&l_1565[1],&g_1409,&l_1565[0],(void*)0,&g_1409},{&l_1565[1],&l_1565[1],&g_1409,&g_1409,&l_1565[0],&g_1409,&l_1565[3],&g_1409,&l_1565[1]}},{{&l_1565[3],&l_1565[4],&l_1565[4],&l_1565[1],&l_1565[1],&l_1565[1],&g_1409,&l_1565[1],&g_1409},{(void*)0,&l_1565[4],&g_1409,&g_1409,&l_1565[4],(void*)0,&l_1565[1],(void*)0,&l_1565[3]},{&l_1565[3],&g_1409,&l_1565[1],&g_1409,&l_1565[1],(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,&l_1565[1],&l_1565[1],&l_1565[1],&g_1409,&g_1409,&l_1565[1],&l_1565[1],&l_1565[3]},{&g_1409,(void*)0,&l_1565[3],&l_1565[3],&l_1565[1],&l_1565[1],&g_1409,&l_1565[4],&l_1565[1]},{&l_1565[4],&g_1409,&l_1565[1],&l_1565[3],(void*)0,&l_1565[1],&l_1565[3],&g_1409,&l_1565[4]}},{{&l_1565[1],&l_1565[1],(void*)0,&l_1565[1],&l_1565[1],&l_1565[4],&l_1565[0],&l_1565[4],(void*)0},{&l_1565[2],&l_1565[1],&l_1565[4],&g_1409,(void*)0,&l_1565[1],&g_1409,&l_1565[1],&l_1565[1]},{(void*)0,(void*)0,&g_1409,&l_1565[1],(void*)0,&g_1409,&g_1409,&g_1409,&l_1565[1]}},{{&l_1565[1],(void*)0,(void*)0,&l_1565[1],&l_1565[1],&l_1565[1],&l_1565[3],(void*)0,(void*)0},{(void*)0,&l_1565[1],(void*)0,&g_1409,&g_1409,&l_1565[1],(void*)0,&l_1565[1],&l_1565[4]},{(void*)0,&l_1565[1],&l_1565[1],&l_1565[4],&g_1409,&l_1565[1],(void*)0,&g_1409,&g_1409}},{{&g_1409,&l_1565[1],&l_1565[1],&l_1565[1],&g_1409,&g_1409,&l_1565[1],&l_1565[1],&l_1565[1]},{&l_1565[1],&l_1565[1],&l_1565[1],&l_1565[1],&l_1565[1],(void*)0,&l_1565[1],&l_1565[1],(void*)0},{&g_1409,&l_1565[3],&g_1409,&l_1565[0],&g_1409,&g_1409,&l_1565[1],&l_1565[1],&g_1409}},{{&l_1565[1],&l_1565[1],&l_1565[1],&l_1565[1],&l_1565[1],&l_1565[1],&l_1565[1],&g_1409,&l_1565[3]},{(void*)0,&l_1565[1],(void*)0,(void*)0,(void*)0,&l_1565[1],&g_1409,&l_1565[1],(void*)0},{&l_1565[1],&l_1565[1],&l_1565[5],(void*)0,&l_1565[1],&l_1565[1],(void*)0,&g_1409,&l_1565[1]}},{{&l_1565[4],&g_1409,&l_1565[4],&g_1409,(void*)0,(void*)0,(void*)0,&l_1565[3],(void*)0},{(void*)0,&l_1565[1],&l_1565[1],&g_1409,&l_1565[1],&g_1409,&g_1409,&g_1409,&l_1565[1]},{&g_1409,&g_1409,(void*)0,&g_1409,(void*)0,&l_1565[3],&g_1409,&g_1409,&l_1565[1]}}};
        int32_t **l_2725 = &l_2726[2][2][1];
        int32_t l_2731 = 0x94359860L;
        uint32_t *l_2732 = &g_386[1];
        int64_t l_2733[6];
        int8_t *l_2735[2][4];
        int32_t l_2737 = 0x6F746214L;
        float **l_2759 = &l_1563[2];
        const uint8_t *l_2766 = &g_1717;
        const uint8_t **l_2765 = &l_2766;
        const uint8_t ***l_2764[8][9][3] = {{{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,(void*)0,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,(void*)0,(void*)0},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{(void*)0,&l_2765,&l_2765}},{{&l_2765,&l_2765,(void*)0},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,(void*)0,&l_2765},{&l_2765,&l_2765,&l_2765},{(void*)0,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,(void*)0,(void*)0}},{{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,(void*)0},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,(void*)0,&l_2765},{&l_2765,&l_2765,&l_2765}},{{&l_2765,&l_2765,&l_2765},{&l_2765,(void*)0,(void*)0},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{(void*)0,&l_2765,&l_2765},{&l_2765,&l_2765,(void*)0},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765}},{{&l_2765,&l_2765,(void*)0},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{(void*)0,(void*)0,&l_2765}},{{&l_2765,&l_2765,&l_2765},{(void*)0,&l_2765,&l_2765},{&l_2765,&l_2765,(void*)0},{(void*)0,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765}},{{&l_2765,&l_2765,&l_2765},{&l_2765,(void*)0,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,(void*)0},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765}},{{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{&l_2765,&l_2765,&l_2765},{(void*)0,(void*)0,&l_2765},{&l_2765,&l_2765,&l_2765},{(void*)0,&l_2765,&l_2765},{&l_2765,&l_2765,(void*)0},{(void*)0,&l_2765,&l_2765}}};
        uint16_t *l_2782[1][4][3] = {{{&g_1122,&g_1605,&g_1122},{&g_455,&g_455,&g_455},{&g_1122,&g_1605,&g_1122},{&g_455,&g_455,&g_455}}};
        int32_t l_2783[3];
        uint8_t l_2792[7] = {0xF8L,0xF8L,0xF8L,0xF8L,0xF8L,0xF8L,0xF8L};
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_2733[i] = 0x935D3C5136358DDALL;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 4; j++)
                l_2735[i][j] = &g_1625;
        }
        for (i = 0; i < 3; i++)
            l_2783[i] = 0x45D4762FL;
        l_2737 ^= ((**g_1241) < (l_2736 = (safe_rshift_func_int8_t_s_s(l_2724, (g_133 ^= ((((((*l_2725) = &g_1409) != ((l_2734 = ((((*l_2732) = (safe_mul_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(l_2731, (0x5DL || (*g_1373)))), ((**g_1138) | (((0x92E74B07B7CA47AFLL < (l_2731 != (***g_1782))) , &g_1383[2]) == (void*)0))))) , 1UL) && l_2733[5])) , &g_1491)) || (*g_1981)) || (*g_2165)) != l_2731))))));
lbl_2767:
        (*l_1561) = (safe_mul_func_float_f_f((safe_add_func_float_f_f((safe_sub_func_float_f_f((safe_mul_func_float_f_f((((safe_mul_func_float_f_f((safe_sub_func_float_f_f(0x4.25CAEBp-55, 0x0.466806p-60)), (l_2750 , (safe_mul_func_float_f_f((safe_add_func_float_f_f(l_2733[0], ((((safe_sub_func_float_f_f((((safe_mul_func_int16_t_s_s(((l_2759 = (void*)0) == (void*)0), (((safe_unary_minus_func_uint8_t_u(((safe_add_func_int8_t_s_s((l_2733[1] , (l_2737 <= 0x7155AAA0CE58E331LL)), 1L)) != 1UL))) ^ l_2763[8][3][0]) | (**g_1241)))) , l_2764[6][1][1]) != (void*)0), (*g_530))) != l_2731) <= 0x1.Ep-1) > l_2733[5]))), 0x4.A87EFBp+74))))) != l_2733[5]) != 0x1.2p-1), (-0x1.6p+1))), 0x5.F696B6p+19)), l_2733[0])), 0x1.1p-1));
        for (g_8 = 1; (g_8 <= 4); g_8 += 1)
        { /* block id: 1336 */
            if (g_22)
                goto lbl_2767;
            (*l_1561) = 0x8.Ep-1;
        }
        (*l_1561) = (((*g_530) = ((safe_div_func_float_f_f(((((l_2733[4] , (safe_div_func_float_f_f(((l_2737 >= (-(safe_add_func_float_f_f((+(((((((****g_1239) = (safe_mod_func_uint32_t_u_u(0x9C5BF444L, ((18446744073709551615UL != 0x5CAE7325AF527705LL) | ((safe_rshift_func_int8_t_s_u(0xE3L, 7)) , (++l_2784)))))) , ((safe_sub_func_uint64_t_u_u((&g_2082 != l_2789[0]), ((safe_rshift_func_uint8_t_u_u((**g_1869), 0)) >= l_2783[0]))) && l_2731)) ^ 0L) , &g_157[3][4][9]) != &l_2736) , 0x0.4p+1)), l_2783[0])))) > l_2792[5]), (*g_530)))) != (*g_530)) > l_2731) < 0x3.2p+1), 0xA.0183CEp+20)) == (*g_530))) != l_2792[5]);
    }
    return (**g_657);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int32_t  func_2(uint8_t  p_3, uint16_t  p_4, int64_t  p_5, int32_t  p_6, uint16_t  p_7)
{ /* block id: 1319 */
    return p_6;
}


/* ------------------------------------------ */
/* 
 * reads : g_629 g_784 g_583 g_530 g_1867 g_1783 g_406 g_295 g_143 g_88 g_1373 g_158 g_1870 g_8 g_1757 g_1758 g_1255 g_803 g_628 g_1214 g_1869 g_37 g_386 g_944 g_1371 g_1372 g_304 g_1660 g_1239 g_1240 g_1241 g_1242 g_133 g_1605 g_1980 g_1981 g_60 g_224 g_574 g_449 g_38 g_1776 g_1777 g_110 g_492 g_1119 g_2024 g_1782 g_1160 g_721 g_722 g_115 g_1868 g_2096 g_455 g_96 g_348 g_2270 g_2276 g_657 g_658 g_659 g_2082 g_1730 g_2271 g_720 g_1138 g_2165 g_1717 g_94 g_22 g_2716 g_1409 g_1625 g_1658 g_112
 * writes: g_1730 g_386 g_37 g_455 g_110 g_224 g_1409 g_1868 g_88 g_158 g_295 g_133 g_1625 g_1605 g_492 g_1242 g_1384 g_1658 g_583 g_112 g_944 g_143 g_2082 g_1468 g_449 g_1122 g_2154 g_348 g_2165 g_1660 g_2277 g_304 g_94 g_2363 g_22 g_2716
 */
static int32_t  func_10(uint32_t  p_11)
{ /* block id: 855 */
    int32_t l_1854 = 8L;
    uint8_t *l_1856 = &g_1730;
    uint32_t *l_1857 = &g_386[1];
    uint16_t **l_1863 = &g_1214;
    uint8_t ***l_1866 = (void*)0;
    int32_t l_1871 = (-6L);
    int32_t l_1874 = (-1L);
    int32_t l_1875 = 0xCF3C53BEL;
    int32_t l_1876[1][8];
    uint32_t l_1878 = 0xE4BE5B2AL;
    int16_t * const *l_1899 = &g_1242[5];
    int16_t * const * const *l_1898[6][5] = {{&l_1899,&l_1899,&l_1899,&l_1899,&l_1899},{&l_1899,(void*)0,&l_1899,&l_1899,&l_1899},{(void*)0,&l_1899,(void*)0,&l_1899,&l_1899},{&l_1899,&l_1899,&l_1899,&l_1899,&l_1899},{(void*)0,&l_1899,&l_1899,&l_1899,&l_1899},{&l_1899,&l_1899,&l_1899,&l_1899,&l_1899}};
    int16_t * const * const **l_1897 = &l_1898[2][1];
    const int32_t * const *l_1916 = &g_449;
    const int32_t * const **l_1915[3];
    int32_t *l_1917 = &g_143[0];
    uint16_t l_1944 = 65535UL;
    int32_t ***l_1977 = &g_1119[2];
    float ***l_1982[1];
    int8_t ** const *l_2046[2][6] = {{&g_1372,&g_1372,&g_1372,&g_1372,&g_1372,&g_1372},{&g_1372,&g_1372,&g_1372,&g_1372,&g_1372,&g_1372}};
    int64_t l_2151 = (-2L);
    uint32_t l_2171 = 4294967295UL;
    int64_t ***l_2207 = &g_1783;
    uint32_t l_2231 = 0x5F67E899L;
    uint64_t l_2249 = 7UL;
    uint32_t ***l_2362 = &g_2270[2];
    uint32_t ****l_2361 = &l_2362;
    uint32_t *****l_2360 = &l_2361;
    int16_t l_2379 = 1L;
    const int8_t ** const **l_2404 = (void*)0;
    uint32_t l_2407[9][6][4] = {{{1UL,0UL,18446744073709551615UL,0xF03E2F31L},{0xA454F560L,1UL,0x18EB490BL,0x8411B79DL},{0xDC9B001AL,18446744073709551615UL,0xF03E2F31L,0x8181FC2EL},{18446744073709551615UL,1UL,0xB3B69FA8L,1UL},{0xA454F560L,0xECAD2D32L,0UL,0xECAD2D32L},{18446744073709551615UL,1UL,0x4E8D8ACEL,0x8411B79DL}},{{18446744073709551615UL,18446744073709551615UL,0x430876C4L,0xA454F560L},{0xDC9B001AL,0x18EB490BL,7UL,0UL},{0xDC9B001AL,0x71B30696L,0x430876C4L,8UL},{18446744073709551615UL,0UL,0x4E8D8ACEL,0xC705967CL},{18446744073709551615UL,0x9E7A82F8L,0UL,0xDB9A9D8BL},{0xA454F560L,18446744073709551615UL,0xB3B69FA8L,0xDC9B001AL}},{{0xDE62DC53L,0UL,18446744073709551608UL,0xBB614C2CL},{0UL,0xA454F560L,0x71B30696L,0UL},{18446744073709551615UL,0xD272A241L,0xDB9A9D8BL,0xD59F84B0L},{0x6E69B3FBL,0xBB614C2CL,18446744073709551615UL,0x5F268B54L},{8UL,1UL,0xD272A241L,0x18EB490BL},{0UL,0x248BDBA1L,0xCB07AC6EL,0UL}},{{18446744073709551615UL,18446744073709551615UL,0x70252D86L,0xDE62DC53L},{0xCB07AC6EL,0UL,0UL,0xCB07AC6EL},{0x18EB490BL,0x71A813B9L,0x9CD50A71L,18446744073709551615UL},{0xD59F84B0L,0x5F268B54L,18446744073709551615UL,0xA83F2798L},{0x430876C4L,18446744073709551615UL,0UL,0xA83F2798L},{0x8181FC2EL,0x5F268B54L,18446744073709551615UL,18446744073709551615UL}},{{0x4E8D8ACEL,0x71A813B9L,1UL,0xCB07AC6EL},{3UL,0UL,18446744073709551615UL,0xDE62DC53L},{1UL,18446744073709551615UL,0xBB614C2CL,0UL},{1UL,0x248BDBA1L,0xECAD2D32L,0x18EB490BL},{0x515AE152L,1UL,8UL,0x5F268B54L},{18446744073709551615UL,0xBB614C2CL,0x8411B79DL,0xD59F84B0L}},{{0UL,0xD272A241L,0UL,0UL},{0UL,0xA454F560L,0xF6D13EA6L,0xBB614C2CL},{18446744073709551615UL,0UL,0xA83F2798L,0xDC9B001AL},{8UL,18446744073709551615UL,0xDC9B001AL,0xDB9A9D8BL},{0x71A813B9L,0x9E7A82F8L,0x71A813B9L,0xC705967CL},{0xB3B69FA8L,0UL,18446744073709551615UL,8UL}},{{0xCAB2B712L,0x71B30696L,1UL,0UL},{0x8411B79DL,0x18EB490BL,1UL,0xA454F560L},{0xCAB2B712L,18446744073709551615UL,18446744073709551615UL,0x8411B79DL},{0xB3B69FA8L,1UL,0x71A813B9L,0xECAD2D32L},{0x71A813B9L,0xECAD2D32L,0xDC9B001AL,1UL},{8UL,0x6E69B3FBL,0xA83F2798L,0UL}},{{18446744073709551615UL,0x4E8D8ACEL,0xF6D13EA6L,0x248BDBA1L},{0UL,0xF6D13EA6L,0UL,18446744073709551608UL},{0UL,18446744073709551615UL,0x8411B79DL,18446744073709551615UL},{18446744073709551615UL,1UL,8UL,2UL},{0x515AE152L,0x70252D86L,0xECAD2D32L,0xF6D13EA6L},{1UL,0xB4BAEB79L,0xBB614C2CL,0x71B30696L}},{{1UL,0x8693FCB7L,18446744073709551615UL,18446744073709551615UL},{3UL,3UL,1UL,0x6E69B3FBL},{0x4E8D8ACEL,0x515AE152L,18446744073709551615UL,0x71A813B9L},{0x8181FC2EL,0x70252D86L,18446744073709551615UL,0xA6C925BBL},{0x8181FC2EL,0x70252D86L,0xDB9A9D8BL,0x9CD50A71L},{0x70252D86L,18446744073709551615UL,0xD272A241L,0xF03E2F31L}}};
    uint16_t *****l_2415 = &g_1468[0];
    int16_t l_2423 = (-1L);
    float l_2509[6];
    int16_t l_2521[6] = {0xE0D5L,0xE0D5L,0xE0D5L,0xE0D5L,0xE0D5L,0xE0D5L};
    uint64_t l_2535 = 0x8A45CE233F4ACB36LL;
    float * const *l_2550 = &g_530;
    uint32_t l_2597[8] = {4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL};
    uint32_t l_2606 = 0x8F852F59L;
    int32_t l_2622 = 0x9C9ED3A6L;
    int64_t l_2669[10][7][3] = {{{0xDB7DEC1A1063FF71LL,0x17F0D242C8DAD5CALL,0x961FFB1C195E6E49LL},{0x51AAA443E822A531LL,0x0B69557BF5D3054DLL,0xE95BB5521034903ELL},{1L,0xDB7DEC1A1063FF71LL,3L},{(-6L),0x51AAA443E822A531LL,1L},{1L,1L,0L},{0x51AAA443E822A531LL,(-6L),1L},{0xDB7DEC1A1063FF71LL,1L,5L}},{{0x0B69557BF5D3054DLL,0x51AAA443E822A531LL,3L},{0x17F0D242C8DAD5CALL,0xDB7DEC1A1063FF71LL,5L},{1L,0x0B69557BF5D3054DLL,1L},{0xDD450A13648CB26ELL,0x17F0D242C8DAD5CALL,0L},{1L,1L,1L},{0x17F0D242C8DAD5CALL,0xDD450A13648CB26ELL,3L},{0x0B69557BF5D3054DLL,1L,0xE95BB5521034903ELL}},{{0xDB7DEC1A1063FF71LL,0x17F0D242C8DAD5CALL,0x961FFB1C195E6E49LL},{0x51AAA443E822A531LL,0x0B69557BF5D3054DLL,0xE95BB5521034903ELL},{1L,0xDB7DEC1A1063FF71LL,3L},{(-6L),0x51AAA443E822A531LL,1L},{1L,1L,0L},{0x51AAA443E822A531LL,(-6L),1L},{0xDB7DEC1A1063FF71LL,1L,5L}},{{0x0B69557BF5D3054DLL,0x51AAA443E822A531LL,3L},{0x17F0D242C8DAD5CALL,0xDB7DEC1A1063FF71LL,5L},{1L,0x0B69557BF5D3054DLL,1L},{0xDD450A13648CB26ELL,0x17F0D242C8DAD5CALL,0L},{1L,1L,1L},{0x17F0D242C8DAD5CALL,0xDD450A13648CB26ELL,3L},{0x0B69557BF5D3054DLL,1L,0xE95BB5521034903ELL}},{{0xDB7DEC1A1063FF71LL,0x17F0D242C8DAD5CALL,0x961FFB1C195E6E49LL},{0x51AAA443E822A531LL,0x0B69557BF5D3054DLL,0xE95BB5521034903ELL},{1L,0xDB7DEC1A1063FF71LL,3L},{(-6L),0x51AAA443E822A531LL,1L},{1L,1L,0L},{0x51AAA443E822A531LL,(-6L),1L},{0xDB7DEC1A1063FF71LL,1L,5L}},{{0x0B69557BF5D3054DLL,0x51AAA443E822A531LL,3L},{0x17F0D242C8DAD5CALL,0xDB7DEC1A1063FF71LL,5L},{1L,0x0B69557BF5D3054DLL,1L},{0xDD450A13648CB26ELL,0x17F0D242C8DAD5CALL,0L},{1L,1L,1L},{0x17F0D242C8DAD5CALL,0xDD450A13648CB26ELL,3L},{0x0B69557BF5D3054DLL,1L,0xE95BB5521034903ELL}},{{0xDB7DEC1A1063FF71LL,0x17F0D242C8DAD5CALL,0x961FFB1C195E6E49LL},{0x51AAA443E822A531LL,0x0B69557BF5D3054DLL,0xE95BB5521034903ELL},{1L,0xDB7DEC1A1063FF71LL,3L},{(-6L),0x51AAA443E822A531LL,1L},{1L,1L,0L},{0x51AAA443E822A531LL,(-6L),1L},{0xDB7DEC1A1063FF71LL,1L,5L}},{{0x0B69557BF5D3054DLL,0x51AAA443E822A531LL,3L},{0x17F0D242C8DAD5CALL,0xDB7DEC1A1063FF71LL,5L},{1L,0x0B69557BF5D3054DLL,1L},{0xDD450A13648CB26ELL,0x17F0D242C8DAD5CALL,0L},{1L,1L,1L},{0x17F0D242C8DAD5CALL,0xDD450A13648CB26ELL,3L},{0x0B69557BF5D3054DLL,(-1L),(-6L)}},{{0xB5182B9280CF39F8LL,(-1L),0x675422D324052EEALL},{0x2398405B0582FCE7LL,0xE3B559AE18B7BDB3LL,(-6L)},{0xC28A442086D0986ALL,0xB5182B9280CF39F8LL,0x14739A51ED6FAAFDLL},{1L,0x2398405B0582FCE7LL,1L},{0xC28A442086D0986ALL,0xC28A442086D0986ALL,1L},{0x2398405B0582FCE7LL,1L,(-3L)},{0xB5182B9280CF39F8LL,0xC28A442086D0986ALL,0xDD450A13648CB26ELL}},{{0xE3B559AE18B7BDB3LL,0x2398405B0582FCE7LL,9L},{(-1L),0xB5182B9280CF39F8LL,0xDD450A13648CB26ELL},{(-1L),0xE3B559AE18B7BDB3LL,(-3L)},{1L,(-1L),1L},{(-1L),(-1L),1L},{(-1L),1L,0x14739A51ED6FAAFDLL},{0xE3B559AE18B7BDB3LL,(-1L),(-6L)}}};
    int16_t l_2684[5] = {(-9L),(-9L),(-9L),(-9L),(-9L)};
    uint32_t l_2685 = 0x7335DAB8L;
    const int16_t *l_2690 = &g_2691;
    int16_t *l_2693 = (void*)0;
    int16_t **l_2692 = &l_2693;
    int32_t *l_2706 = &g_22;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
            l_1876[i][j] = (-7L);
    }
    for (i = 0; i < 3; i++)
        l_1915[i] = &l_1916;
    for (i = 0; i < 1; i++)
        l_1982[i] = &g_529;
    for (i = 0; i < 6; i++)
        l_2509[i] = 0xB.736387p+6;
    if (((!l_1854) > ((*l_1857) = (+(((*l_1856) = ((-1L) || (p_11 , 0xA9L))) == (-1L))))))
    { /* block id: 858 */
        int32_t *l_1858[5][1];
        uint16_t **l_1865 = &g_488[2];
        uint64_t *l_1892 = &g_88;
        float ***l_1983 = &g_529;
        uint32_t l_1985 = 4294967289UL;
        int16_t l_1992 = 0xEE7EL;
        float l_2029 = (-0x1.0p+1);
        uint16_t l_2060 = 0UL;
        uint64_t l_2065 = 2UL;
        int8_t l_2133 = 0xD3L;
        int i, j;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 1; j++)
                l_1858[i][j] = &g_583;
        }
lbl_2109:
        (*g_629) = l_1858[0][0];
        (*g_629) = func_31(l_1857, (safe_rshift_func_uint8_t_u_s(g_784, 4)));
        for (g_1409 = 0; (g_1409 > 24); g_1409++)
        { /* block id: 863 */
            uint16_t ***l_1864[5][9][5] = {{{(void*)0,&l_1863,(void*)0,&g_1384,&g_1384},{&l_1863,(void*)0,(void*)0,(void*)0,&g_1384},{&l_1863,&l_1863,(void*)0,&g_1384,(void*)0},{&l_1863,&l_1863,(void*)0,&g_1384,&l_1863},{(void*)0,&g_1384,&g_1384,&l_1863,&g_1384},{(void*)0,(void*)0,&g_1384,&l_1863,&l_1863},{&g_1384,&l_1863,(void*)0,&l_1863,(void*)0},{(void*)0,&l_1863,&g_1384,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863}},{{&l_1863,&l_1863,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1863,&l_1863,&g_1384},{(void*)0,&g_1384,&g_1384,&g_1384,(void*)0},{&l_1863,&l_1863,&g_1384,&g_1384,(void*)0},{&g_1384,&l_1863,&l_1863,&l_1863,(void*)0},{&l_1863,(void*)0,(void*)0,&l_1863,(void*)0},{&g_1384,&l_1863,&g_1384,&l_1863,(void*)0},{(void*)0,&l_1863,&l_1863,&g_1384,&g_1384},{&l_1863,&g_1384,&l_1863,(void*)0,(void*)0}},{{(void*)0,&l_1863,&l_1863,(void*)0,&l_1863},{&g_1384,&l_1863,(void*)0,(void*)0,&l_1863},{&g_1384,(void*)0,(void*)0,&g_1384,(void*)0},{&g_1384,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&g_1384,&l_1863,(void*)0,&g_1384},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&g_1384,&l_1863,&g_1384,&g_1384,(void*)0},{(void*)0,(void*)0,(void*)0,&l_1863,&g_1384},{(void*)0,(void*)0,&l_1863,&l_1863,&g_1384}},{{(void*)0,&g_1384,&g_1384,&g_1384,&l_1863},{&g_1384,&l_1863,&l_1863,&g_1384,&l_1863},{&g_1384,(void*)0,&g_1384,&g_1384,&l_1863},{(void*)0,&g_1384,&g_1384,(void*)0,&g_1384},{(void*)0,&l_1863,(void*)0,(void*)0,(void*)0},{&l_1863,(void*)0,(void*)0,(void*)0,&g_1384},{&l_1863,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1863,&g_1384,&l_1863,&g_1384,&l_1863},{&g_1384,&l_1863,(void*)0,&g_1384,(void*)0}},{{(void*)0,(void*)0,&g_1384,&g_1384,&l_1863},{(void*)0,&l_1863,(void*)0,&g_1384,&l_1863},{&g_1384,(void*)0,&l_1863,&g_1384,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{&l_1863,(void*)0,&g_1384,&l_1863,(void*)0},{&l_1863,&l_1863,(void*)0,&g_1384,(void*)0},{(void*)0,&g_1384,&l_1863,(void*)0,(void*)0},{&g_1384,(void*)0,(void*)0,&l_1863,&l_1863},{&l_1863,(void*)0,&g_1384,(void*)0,&l_1863}}};
            int32_t l_1872 = 1L;
            int32_t l_1873[6][8] = {{1L,0L,0L,0xE57FEDECL,(-1L),0xE57FEDECL,0L,0L},{1L,0L,(-1L),0xE57FEDECL,0x7935EAEFL,8L,8L,0x7935EAEFL},{9L,0x7935EAEFL,0x7935EAEFL,9L,1L,0L,8L,1L},{0L,9L,(-1L),8L,(-1L),9L,0L,(-1L)},{(-1L),9L,0L,(-1L),0L,0L,(-1L),0L},{0x7935EAEFL,0x7935EAEFL,9L,1L,0L,8L,1L,8L}};
            int i, j, k;
            l_1865 = l_1863;
            (*g_1867) = l_1866;
            l_1878--;
        }
        if (((**g_1783) ^ 0x30BDFBFD435989B4LL))
        { /* block id: 868 */
            int32_t l_1893 = (-1L);
            int32_t l_1894 = (-1L);
            uint64_t l_1896 = 0xD6C6D74E97B5ABB9LL;
            uint64_t l_1927 = 2UL;
            uint64_t l_1928 = 0x127179C9B68FFF05LL;
            int16_t l_2006 = 0L;
            if ((safe_mod_func_uint16_t_u_u((safe_add_func_int8_t_s_s((((safe_lshift_func_int16_t_s_s(((safe_unary_minus_func_uint32_t_u((((safe_mul_func_int8_t_s_s((p_11 > (safe_mul_func_uint8_t_u_u((&g_944[0][1][0] != l_1892), ((*l_1856) = ((0x1A937A5DL > ((l_1893 = 5UL) != g_143[0])) ^ (l_1894 , ((4294967286UL > (((*l_1892) ^= ((+6UL) && p_11)) >= l_1896)) & (*g_1373)))))))), l_1896)) , 0L) | (*g_1870)))) , 0xDF9AL), 0)) , l_1897) == &l_1898[2][1]), 0xE8L)), p_11)))
            { /* block id: 872 */
                return p_11;
            }
            else
            { /* block id: 874 */
                for (g_455 = 19; (g_455 == 26); g_455 = safe_add_func_uint64_t_u_u(g_455, 5))
                { /* block id: 877 */
                    l_1917 = ((safe_sub_func_int8_t_s_s(((safe_rshift_func_uint8_t_u_u((l_1876[0][2] | 0xE1L), ((safe_add_func_int64_t_s_s((p_11 <= (0xCD32L == (0xF4EE7C7AL == (safe_mul_func_int8_t_s_s((((*g_1373) &= ((void*)0 != (**g_1757))) ^ (!(safe_add_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_s(((*g_803) != l_1915[0]), l_1893)) , p_11), p_11)))), p_11))))), l_1894)) ^ p_11))) < 18446744073709551609UL), p_11)) , (void*)0);
                }
            }
            if (((*g_37) ^= ((((void*)0 == (*l_1863)) > (((*l_1856) = (*g_1870)) <= (safe_rshift_func_uint8_t_u_s((**g_1869), l_1894)))) || ((safe_div_func_uint64_t_u_u(l_1894, ((**g_1783) &= ((safe_mul_func_uint8_t_u_u((((safe_add_func_uint8_t_u_u((l_1893 = (((((((**g_1869) <= (~l_1927)) || p_11) > l_1894) , p_11) >= p_11) , l_1928)), l_1927)) && p_11) | 0x8B77L), p_11)) , 8L)))) , p_11))))
            { /* block id: 886 */
                int8_t *l_1941 = &g_133;
                int8_t l_1942 = (-1L);
                int32_t l_1943[9][1];
                int i, j;
                for (i = 0; i < 9; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1943[i][j] = 0xD3DFF81DL;
                }
                (***g_628) &= (((((safe_mul_func_uint16_t_u_u(p_11, ((safe_lshift_func_int8_t_s_u((l_1942 &= ((*l_1941) |= (((****g_1239) = ((safe_unary_minus_func_int64_t_s((((((((g_944[7][4][1] <= p_11) == ((((***g_1371) & l_1893) != (safe_sub_func_uint64_t_u_u(g_304, p_11))) & (safe_add_func_uint32_t_u_u((safe_add_func_int8_t_s_s(((*g_1373) & ((*l_1856) = ((+(0x0E6CL >= p_11)) != (*g_1870)))), p_11)), 5UL)))) <= p_11) ^ 0L) < g_1660) >= (**g_1372)) & l_1893))) , 0xB8B0L)) && 0x15C2L))), l_1896)) && 1UL))) ^ l_1943[2][0]) & p_11) , l_1894) , 0x1B0961A2L);
                l_1944++;
            }
            else
            { /* block id: 893 */
                uint32_t l_1984 = 1UL;
                int32_t *** const *l_1986 = &g_628;
                int32_t l_1999[9][10] = {{0xF28A7207L,(-9L),0L,0x556A340FL,(-1L),(-4L),0xA1C4076EL,1L,(-9L),0x81884852L},{1L,0x556A340FL,0xF3F6A2F9L,3L,(-4L),1L,0xA1C4076EL,0L,0x556A340FL,0xCEBEDBA5L},{0x60A2554EL,(-9L),1L,3L,0x81884852L,0x81884852L,3L,1L,(-9L),0x60A2554EL},{0x60A2554EL,0x6D5D61D3L,1L,0x556A340FL,0xF28A7207L,1L,5L,1L,0x6D5D61D3L,0xF28A7207L},{1L,5L,1L,0x6D5D61D3L,0xF28A7207L,(-4L),(-9L),0L,5L,0x60A2554EL},{0xF28A7207L,5L,0xF3F6A2F9L,0xA1C4076EL,0x81884852L,0xCEBEDBA5L,5L,1L,5L,0xA1C4076EL},{3L,0x2104454DL,0x584633F3L,0x2104454DL,3L,0xA1C4076EL,0x5B908B7EL,0x4D968221L,0x2104454DL,(-9L)},{6L,7L,0x584633F3L,(-1L),0x6D5D61D3L,3L,0x0EA5CA75L,0x56699D66L,7L,(-9L)},{5L,(-1L),0L,0x5B908B7EL,3L,5L,0x0EA5CA75L,0xD8CE089AL,(-1L),0xA1C4076EL}};
                uint16_t *l_2000 = &g_492;
                int i, j;
                for (g_1625 = 0; (g_1625 > 17); g_1625 = safe_add_func_int8_t_s_s(g_1625, 3))
                { /* block id: 896 */
                    uint32_t l_1949 = 18446744073709551615UL;
                    (****g_803) |= l_1949;
                    for (l_1893 = 2; (l_1893 >= 0); l_1893 -= 1)
                    { /* block id: 900 */
                        uint64_t l_1952 = 0x2E81F7631854AC29LL;
                        uint16_t *l_1968 = &g_1605;
                        int i, j;
                        (***g_628) = (*g_37);
                        (***g_628) = (((safe_add_func_float_f_f(p_11, (l_1952 <= ((safe_mul_func_float_f_f(0x9.Cp+1, ((safe_sub_func_float_f_f((((l_1985 = ((safe_unary_minus_func_int8_t_s((safe_div_func_int8_t_s_s((safe_mul_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((safe_div_func_int32_t_s_s((((**l_1899) = (((safe_sub_func_uint16_t_u_u((l_1894 = ((*l_1968)++)), l_1952)) , (safe_sub_func_uint32_t_u_u(((l_1894 ^ (((0xB71D11A8L > ((safe_div_func_uint64_t_u_u(((***g_1371) == ((*l_1856) = ((safe_add_func_uint32_t_u_u(((l_1977 = (*g_803)) != (((((safe_add_func_float_f_f(((((void*)0 == g_1980) , p_11) >= p_11), 0x6.Fp+1)) , l_1982[0]) != l_1983) < p_11) , (*g_803))), 1L)) , 0xB6L))), p_11)) && (**g_1980))) || 0x0D1EL) && 0x5EL)) && l_1984), l_1952))) == 0x66L)) <= p_11), p_11)), p_11)), 9L)), (*g_1373))))) < l_1952)) , (**g_60)) , 0xF.5CCFD5p-4), p_11)) <= (*g_530)))) == p_11)))) <= p_11) , l_1928);
                        if ((**g_574))
                            break;
                        l_1986 = (void*)0;
                    }
                }
                if (p_11)
                { /* block id: 913 */
                    return p_11;
                }
                else
                { /* block id: 915 */
                    int32_t *l_1987 = &l_1876[0][0];
                    (**g_628) = l_1987;
                    l_1893 &= (safe_mod_func_int16_t_s_s((safe_sub_func_uint64_t_u_u(p_11, ((18446744073709551615UL == (((*g_1870) <= 0xF5L) < ((l_1999[2][1] = ((**g_629) = ((l_1894 = l_1992) || (safe_add_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(0L, ((void*)0 == (*g_1776)))), ((((safe_mod_func_int32_t_s_s(p_11, 0xC75827DEL)) , (**g_1241)) ^ (*l_1987)) > p_11)))))) != (*g_449)))) , 0xD4997F2C373D9A86LL))), 9UL));
                }
                l_1871 = (((++(*l_2000)) ^ (-7L)) <= ((safe_sub_func_int32_t_s_s(((***l_1977) = ((**g_1241) != ((l_1927 & (~l_2006)) , (((**g_1372) = 0x3EL) != (safe_lshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_s((((safe_mod_func_uint8_t_u_u(((((0x6ACDF063FEF74AD8LL < (((safe_div_func_int64_t_s_s((0xE316ECCC4ED892A9LL && (!(safe_lshift_func_int16_t_s_u((0x3EC9F18DL <= ((safe_unary_minus_func_uint8_t_u(((void*)0 != &l_1894))) >= p_11)), 13)))), p_11)) <= (**g_1980)) && 0x7AL)) || (**g_1783)) | p_11) >= p_11), (-6L))) > 0xD3C2FDBCL) < 0L), p_11)), p_11)))))), p_11)) && (*g_37)));
                if ((p_11 == (!(**g_60))))
                { /* block id: 926 */
                    int16_t *l_2022 = &g_114;
                    int32_t l_2023 = 2L;
                    l_2023 = ((safe_add_func_float_f_f(((((*g_1241) = (***g_1239)) != l_2022) > (-0x1.Bp+1)), p_11)) , p_11);
                }
                else
                { /* block id: 929 */
                    (*g_2024) = &l_2000;
                    (****g_803) = l_1896;
                    (**l_1977) = (***g_803);
                }
            }
        }
        else
        { /* block id: 935 */
            int8_t l_2044[1][8][4] = {{{0x59L,(-6L),0x59L,0x59L},{(-6L),(-6L),6L,(-6L)},{(-6L),0x59L,0x59L,(-6L)},{0x59L,(-6L),0x59L,0x59L},{(-6L),(-6L),6L,(-6L)},{(-6L),0x59L,0x59L,(-6L)},{0x59L,(-6L),0x59L,0x59L},{(-6L),(-6L),6L,(-6L)}}};
            int32_t l_2048 = 4L;
            int32_t l_2050 = 0x218663B3L;
            int32_t l_2054[7] = {0xD5FFC887L,0xD5FFC887L,0xD5FFC887L,0xD5FFC887L,0xD5FFC887L,0xD5FFC887L,0xD5FFC887L};
            int8_t l_2058 = 5L;
            float *l_2083[3];
            uint8_t ***l_2139 = &g_1869;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2083[i] = (void*)0;
            for (g_1658 = (-7); (g_1658 != 55); ++g_1658)
            { /* block id: 938 */
                uint8_t l_2045[4][1] = {{0UL},{249UL},{0UL},{249UL}};
                int16_t l_2047[3];
                int32_t l_2055[8][9] = {{0xC138D549L,0xCA5307A6L,0x9D6B5E73L,(-1L),0xD28C025FL,0x03F0FE0CL,0x31A85997L,0x9644CC4DL,0xFDCEBFB5L},{0x3C299AC9L,0x31A85997L,0xD6947B7EL,6L,0x5216E5D2L,0xF006A8CFL,0x7DA4DFA0L,0xF006A8CFL,0x5216E5D2L},{6L,0x3C299AC9L,0x3C299AC9L,6L,(-2L),0x9D6B5E73L,0L,0xCA5307A6L,(-1L)},{0xC630B9D8L,(-1L),0x9506A179L,(-1L),0xD6947B7EL,(-1L),0x9D6B5E73L,6L,0xF006A8CFL},{0x7AA1D430L,0xFDCEBFB5L,0x7DA4DFA0L,(-1L),(-2L),0xCA5307A6L,0xC630B9D8L,0x5216E5D2L,0xC630B9D8L},{0xCA5307A6L,0x5216E5D2L,(-1L),(-1L),0x5216E5D2L,0xCA5307A6L,0L,0xE2E60AA7L,(-1L)},{1L,(-1L),0L,0x3C299AC9L,0xD28C025FL,(-1L),0x9644CC4DL,0xC630B9D8L,0x7AA1D430L},{0xD6947B7EL,0xF006A8CFL,0x9644CC4DL,0x03F0FE0CL,(-1L),0x9D6B5E73L,0L,0xFDCEBFB5L,0x70D6A228L}};
                int64_t l_2063 = 0xE80CFDD6DD7F16B9LL;
                uint64_t **l_2078 = &l_1892;
                const float l_2092 = 0x1.3DDF5Ap-23;
                uint8_t ** const *l_2094 = &g_1869;
                int i, j;
                for (i = 0; i < 3; i++)
                    l_2047[i] = 0L;
                for (g_583 = 0; (g_583 < 5); ++g_583)
                { /* block id: 941 */
                    uint32_t l_2031 = 4294967295UL;
                    int32_t l_2043 = 0x9332DB4FL;
                    int32_t l_2051 = (-5L);
                    int32_t l_2053 = 0L;
                    int32_t l_2056 = 0xEB164515L;
                    int32_t l_2057[4] = {0x064E208DL,0x064E208DL,0x064E208DL,0x064E208DL};
                    int32_t l_2064 = 1L;
                    int i;
                    for (l_1992 = 1; (l_1992 <= 7); l_1992 += 1)
                    { /* block id: 944 */
                        int16_t l_2030[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_2030[i] = 0xE312L;
                        ++l_2031;
                    }
                    for (g_112 = 0; (g_112 == 11); g_112 = safe_add_func_uint16_t_u_u(g_112, 9))
                    { /* block id: 949 */
                        const int64_t *l_2039 = (void*)0;
                        int32_t l_2049 = 0xEE68694EL;
                        int32_t l_2052[8][2][5] = {{{1L,2L,(-8L),7L,(-8L)},{0x85892221L,0x85892221L,0L,0x85892221L,0x85892221L}},{{(-8L),7L,(-8L),2L,1L},{0x85892221L,0xC95B0FC8L,0xC95B0FC8L,0x85892221L,0xC95B0FC8L}},{{1L,7L,0xE137EC27L,7L,1L},{0xC95B0FC8L,0x85892221L,0xC95B0FC8L,0xC95B0FC8L,0x85892221L}},{{1L,2L,(-8L),7L,(-8L)},{0x85892221L,0x85892221L,0L,0x85892221L,0x85892221L}},{{(-8L),2L,0xE137EC27L,6L,(-8L)},{0xC95B0FC8L,0L,0L,0xC95B0FC8L,0L}},{{(-8L),2L,1L,2L,(-8L)},{0L,0xC95B0FC8L,0L,0L,0xC95B0FC8L}},{{(-8L),6L,0xE137EC27L,2L,0xE137EC27L},{0xC95B0FC8L,0xC95B0FC8L,0x85892221L,0xC95B0FC8L,0xC95B0FC8L}},{{0xE137EC27L,2L,0xE137EC27L,6L,(-8L)},{0xC95B0FC8L,0L,0L,0xC95B0FC8L,0L}}};
                        float l_2059[3];
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                            l_2059[i] = 0xA.51A681p+85;
                        l_2047[0] ^= (((**g_1372) = (!((((0xC2L ^ ((-2L) == (safe_add_func_uint64_t_u_u((((l_2039 == (**g_1782)) || (p_11 != (((((***l_1977) = ((***g_1240) = (l_2031 >= 7UL))) , ((((***l_1977) , (safe_div_func_int8_t_s_s((((+(l_2043 |= ((**l_1899) = p_11))) != l_2031) < 3L), l_2044[0][4][1]))) < (-5L)) <= 0x81L)) | 0x8258F949L) >= (*l_1917)))) & l_2044[0][4][1]), l_2045[0][0])))) , (void*)0) != l_2046[0][4]) & 0x68L))) ^ 0x2DL);
                        if ((*g_1160))
                            continue;
                        l_2060++;
                        return l_2031;
                    }
                    l_2065--;
                    (*l_1917) = ((safe_sub_func_int16_t_s_s(p_11, (safe_lshift_func_uint8_t_u_s(9UL, 6)))) < (l_2055[2][3] <= (safe_mul_func_int8_t_s_s((((*l_1857) = ((-1L) && 252UL)) || 0UL), (((0x43L > (safe_sub_func_uint64_t_u_u(((*g_1981)++), p_11))) , l_2054[1]) != l_2055[3][3])))));
                }
                if ((((*l_2078) = &g_88) == (void*)0))
                { /* block id: 966 */
                    int32_t l_2091 = 0L;
                    uint8_t ****l_2095 = &l_1866;
                    for (g_1409 = 2; (g_1409 >= 0); g_1409 -= 1)
                    { /* block id: 969 */
                        uint64_t *l_2081 = (void*)0;
                        uint16_t *****l_2084 = &g_1468[7];
                        int32_t *l_2093 = &l_1854;
                        l_2091 = ((safe_sub_func_int16_t_s_s((((*l_2093) = ((p_11 || (((g_2082 = l_2081) != (*g_1980)) , (g_38[1][0] != ((void*)0 == l_2083[0])))) > ((((*l_1917) = ((((****g_803) = ((((&g_656[5] == ((*l_2084) = &g_1383[2])) || (safe_div_func_int16_t_s_s(((((safe_add_func_int64_t_s_s(((***g_1782) = (safe_sub_func_int32_t_s_s(((void*)0 == l_2083[1]), p_11))), (**g_1980))) >= l_2091) , (**g_1241)) && (***g_1240)), p_11))) >= 4294967295UL) <= (*g_721))) | 0x6EAE6DC6L) > p_11)) > p_11) && 0x00L))) , (-5L)), (***g_1240))) | 0x3BL);
                        if ((**g_115))
                            break;
                        (*g_530) = l_2091;
                    }
                    l_2055[2][3] = (l_2094 == ((*l_2095) = (*g_1867)));
                }
                else
                { /* block id: 982 */
                    float l_2130 = (-0x1.0p+1);
                    int32_t l_2131[6];
                    uint8_t ***l_2138 = &g_1869;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_2131[i] = 0x0E6B37D5L;
                    if ((*l_1917))
                        break;
                    if ((((*l_1857) = (p_11 >= ((0x8EL <= p_11) == (((g_2096 , (safe_mod_func_int32_t_s_s((***g_628), (((safe_unary_minus_func_int8_t_s(((void*)0 == &l_2055[3][4]))) , (p_11 <= (safe_lshift_func_int16_t_s_u((-1L), p_11)))) ^ (**g_1372))))) >= (-6L)) , 0x2F0AAD05087DF6DALL)))) , 0x69F23FBEL))
                    { /* block id: 985 */
                        uint16_t *l_2106 = &g_455;
                        int32_t **l_2132[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        (***l_1977) = (safe_rshift_func_int16_t_s_u((safe_mul_func_uint8_t_u_u((*g_1870), (***l_1977))), ((*l_2106)--)));
                        if (p_11)
                            goto lbl_2109;
                        (*g_574) = func_41(l_1858[0][0], ((((*l_2106) |= ((safe_sub_func_uint8_t_u_u(((p_11 > ((((*g_1981) = ((0x86L == (-3L)) , ((safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s((p_11 ^ (safe_mul_func_int32_t_s_s(0x8C740088L, ((safe_mul_func_int8_t_s_s(((((safe_rshift_func_int16_t_s_s(l_2044[0][3][2], 9)) & ((((((((safe_lshift_func_int8_t_s_u(((*g_1981) && (safe_sub_func_int16_t_s_s((*g_721), 65535UL))), 3)) == (-1L)) == 0UL) || 0x67L) > 0x3EL) > (*g_1373)) , 18446744073709551615UL) >= 0UL)) >= p_11) && 0L), (**g_1869))) && p_11)))), (-2L))), (****g_1239))) , 0xEDD08AA0B8EFACEFLL))) == p_11) != p_11)) < (*g_1373)), l_2054[2])) > 18446744073709551606UL)) , p_11) != p_11), l_2131[3], (***g_803));
                        if (l_2133)
                            break;
                    }
                    else
                    { /* block id: 993 */
                        uint16_t l_2140 = 0UL;
                        int16_t l_2145 = 6L;
                        int32_t *l_2148[2];
                        uint16_t *l_2153 = &g_1122;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_2148[i] = (void*)0;
                        (*l_1917) ^= ((0x58743923L >= 0xE1E7C019L) && (*g_1981));
                        l_2145 = (safe_sub_func_float_f_f((-0x1.4p+1), ((safe_div_func_float_f_f(((l_2138 = l_2138) != l_2139), (p_11 == (((0x2.Fp-1 != l_2131[3]) < (((-0x3.1p+1) < l_2140) != (((safe_div_func_uint8_t_u_u(((((*g_1373) = (safe_mul_func_int8_t_s_s(((p_11 >= (*g_530)) , (***g_1371)), l_2131[3]))) ^ l_2055[2][3]) >= 7L), p_11)) , (*g_530)) > 0x8.555819p+72))) , p_11)))) <= l_2047[0])));
                        (*l_1917) = ((l_2050 > ((***l_1977) = (****g_803))) < ((((safe_rshift_func_int8_t_s_s(((g_2154[9][3][1] = func_31(l_2148[1], (&l_1858[1][0] != ((((*l_2153) = ((((l_2048 ^ (0x21L < (g_143[0] | ((safe_mod_func_int16_t_s_s((l_2151 , (((~0x34D7L) , 0xEBL) & 0UL)), 1UL)) != 2L)))) <= l_2050) >= l_2131[3]) | (*g_406))) != 0UL) , (void*)0)))) == &g_157[1][1][9]), 7)) >= (**g_1372)) || p_11) == 0x6D35ECC0CCD8F5F8LL));
                    }
                }
                for (g_492 = 0; (g_492 <= 1); g_492 += 1)
                { /* block id: 1006 */
                    uint32_t l_2155 = 0xA941D87EL;
                    int i;
                    (*g_37) ^= (l_2155 , 0x660CEFE8L);
                    if (g_158)
                        goto lbl_2109;
                    if (p_11)
                        break;
                }
                return l_2058;
            }
        }
    }
    else
    { /* block id: 1014 */
        uint8_t *l_2163 = &g_94;
        int32_t *l_2172[6][10][4] = {{{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]}},{{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]}},{{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]}},{{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],&g_38[3][2]},{&g_143[0],&g_143[0],(void*)0,&g_143[0]},{&g_143[0],&g_38[3][2],&g_38[3][2],&g_143[0]},{&g_38[3][2],&g_143[0],&g_38[3][2],(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]},{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]},{(void*)0,&g_38[3][2],(void*)0,(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]}},{{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]},{(void*)0,&g_38[3][2],(void*)0,(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]},{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]},{(void*)0,&g_38[3][2],(void*)0,(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]},{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]},{(void*)0,&g_38[3][2],(void*)0,(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]},{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]}},{{(void*)0,&g_38[3][2],(void*)0,(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]},{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]},{(void*)0,&g_38[3][2],(void*)0,(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]},{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]},{(void*)0,&g_38[3][2],(void*)0,(void*)0},{&g_38[3][2],&g_38[3][2],&g_143[0],&g_38[3][2]},{&g_38[3][2],(void*)0,(void*)0,&g_38[3][2]},{(void*)0,&g_38[3][2],(void*)0,(void*)0}}};
        int64_t * const **l_2206 = (void*)0;
        uint32_t l_2228[7] = {0UL,0UL,8UL,0UL,0UL,8UL,0UL};
        float l_2315 = 0x1.Cp+1;
        int32_t ****l_2342 = &l_1977;
        uint16_t *****l_2347 = (void*)0;
        int8_t ***l_2406 = &g_1372;
        int8_t *** const *l_2405 = &l_2406;
        int i, j, k;
        for (g_348 = (-24); (g_348 < (-7)); g_348++)
        { /* block id: 1017 */
            int16_t l_2158 = (-1L);
            uint16_t ** const l_2161 = &g_1214;
            int8_t ***l_2182[4][5] = {{&g_1372,&g_1372,&g_1372,&g_1372,&g_1372},{&g_1372,&g_1372,&g_1372,&g_1372,&g_1372},{&g_1372,&g_1372,&g_1372,&g_1372,&g_1372},{&g_1372,&g_1372,&g_1372,&g_1372,&g_1372}};
            int8_t ****l_2181 = &l_2182[1][3];
            uint16_t l_2184 = 1UL;
            int32_t l_2186 = 0x748D039CL;
            int32_t l_2189 = 0x11E6C3F6L;
            int32_t l_2192 = 0x4CECEF35L;
            int64_t ***l_2209 = &g_1783;
            int32_t l_2224 = (-4L);
            uint32_t **l_2273 = &g_2271;
            uint32_t l_2278 = 0UL;
            uint16_t *****l_2348 = (void*)0;
            float l_2355[6][8][5] = {{{(-0x7.Cp+1),0x3.B50536p+93,0xC.EC048Cp-28,0x3.B50536p+93,(-0x7.Cp+1)},{0x7.2CEB90p-86,(-0x2.3p+1),(-0x1.5p-1),0x1.Cp-1,(-0x2.3p+1)},{(-0x7.Cp+1),0x5.Bp-1,0x5.Bp-1,(-0x7.Cp+1),0x1.Bp-1},{0x5.622BD6p-24,0x1.154948p-34,(-0x1.Dp-1),(-0x2.3p+1),(-0x2.3p+1)},{(-0x7.4p-1),(-0x7.Cp+1),(-0x7.4p-1),0x1.Bp-1,(-0x7.Cp+1)},{(-0x2.3p+1),(-0x1.5p-1),0x1.Cp-1,(-0x2.3p+1),0x1.Cp-1},{0x0.439017p-25,0x0.439017p-25,0xC.EC048Cp-28,(-0x7.Cp+1),0x3.B50536p+93},{0x7.2CEB90p-86,0x5.622BD6p-24,0x1.Cp-1,0x1.Cp-1,0x5.622BD6p-24}},{{0x3.B50536p+93,0x5.Bp-1,(-0x7.4p-1),0x3.B50536p+93,0x1.Bp-1},{0x1.154948p-34,0x5.622BD6p-24,(-0x1.Dp-1),0x5.622BD6p-24,0x1.154948p-34},{(-0x7.4p-1),0x0.439017p-25,0x5.Bp-1,0x1.Bp-1,0x0.439017p-25},{0x1.154948p-34,(-0x1.5p-1),(-0x1.5p-1),0x1.154948p-34,0x1.Cp-1},{0x3.B50536p+93,(-0x7.Cp+1),0xC.EC048Cp-28,0x0.439017p-25,0x0.439017p-25},{0x7.2CEB90p-86,(-0x1.5p-1),0xA.EB4BAEp+0,0x4.89D118p+96,(-0x1.5p-1)},{(-0x7.4p-1),0xC.EC048Cp-28,0x3.Ep-1,(-0x7.4p-1),0x3.Ep-1},{0x7.2CEB90p-86,0x7.2CEB90p-86,(-0x2.3p+1),(-0x1.5p-1),0x1.Cp-1}},{{0x5.2p+1,0x1.Bp-1,0x3.Ep-1,0x3.Ep-1,0x1.Bp-1},{0x1.Cp-1,(-0x1.Dp-1),0xA.EB4BAEp+0,0x1.Cp-1,0x4.89D118p+96},{0x5.Bp-1,0x1.Bp-1,0x0.439017p-25,0x1.Bp-1,0x5.Bp-1},{0xA.EB4BAEp+0,0x7.2CEB90p-86,(-0x1.Dp-1),0x4.89D118p+96,0x7.2CEB90p-86},{0x5.Bp-1,0xC.EC048Cp-28,0xC.EC048Cp-28,0x5.Bp-1,0x3.Ep-1},{0x1.Cp-1,(-0x1.5p-1),(-0x2.3p+1),0x7.2CEB90p-86,0x7.2CEB90p-86},{0x5.2p+1,0x5.Bp-1,0x5.2p+1,0x3.Ep-1,0x5.Bp-1},{0x7.2CEB90p-86,(-0x1.Dp-1),0x4.89D118p+96,0x7.2CEB90p-86,0x4.89D118p+96}},{{(-0x7.4p-1),(-0x7.4p-1),0x0.439017p-25,0x5.Bp-1,0x1.Bp-1},{0xA.EB4BAEp+0,0x1.Cp-1,0x4.89D118p+96,0x4.89D118p+96,0x1.Cp-1},{0x1.Bp-1,0xC.EC048Cp-28,0x5.2p+1,0x1.Bp-1,0x3.Ep-1},{(-0x1.5p-1),0x1.Cp-1,(-0x2.3p+1),0x1.Cp-1,(-0x1.5p-1)},{0x5.2p+1,(-0x7.4p-1),0xC.EC048Cp-28,0x3.Ep-1,(-0x7.4p-1)},{(-0x1.5p-1),(-0x1.Dp-1),(-0x1.Dp-1),(-0x1.5p-1),0x4.89D118p+96},{0x1.Bp-1,0x5.Bp-1,0x0.439017p-25,(-0x7.4p-1),(-0x7.4p-1)},{0xA.EB4BAEp+0,(-0x1.5p-1),0xA.EB4BAEp+0,0x4.89D118p+96,(-0x1.5p-1)}},{{(-0x7.4p-1),0xC.EC048Cp-28,0x3.Ep-1,(-0x7.4p-1),0x3.Ep-1},{0x7.2CEB90p-86,0x7.2CEB90p-86,(-0x2.3p+1),(-0x1.5p-1),0x1.Cp-1},{0x5.2p+1,0x1.Bp-1,0x3.Ep-1,0x3.Ep-1,0x1.Bp-1},{0x1.Cp-1,(-0x1.Dp-1),0xA.EB4BAEp+0,0x1.Cp-1,0x4.89D118p+96},{0x5.Bp-1,0x1.Bp-1,0x0.439017p-25,0x1.Bp-1,0x5.Bp-1},{0xA.EB4BAEp+0,0x7.2CEB90p-86,(-0x1.Dp-1),0x4.89D118p+96,0x7.2CEB90p-86},{0x5.Bp-1,0xC.EC048Cp-28,0xC.EC048Cp-28,0x5.Bp-1,0x3.Ep-1},{0x1.Cp-1,(-0x1.5p-1),(-0x2.3p+1),0x7.2CEB90p-86,0x7.2CEB90p-86}},{{0x5.2p+1,0x5.Bp-1,0x5.2p+1,0x3.Ep-1,0x5.Bp-1},{0x7.2CEB90p-86,(-0x1.Dp-1),0x4.89D118p+96,0x7.2CEB90p-86,0x4.89D118p+96},{(-0x7.4p-1),(-0x7.4p-1),0x0.439017p-25,0x5.Bp-1,0x1.Bp-1},{0xA.EB4BAEp+0,0x1.Cp-1,0x4.89D118p+96,0x4.89D118p+96,0x1.Cp-1},{0x1.Bp-1,0xC.EC048Cp-28,0x5.2p+1,0x1.Bp-1,0x3.Ep-1},{(-0x1.5p-1),0x1.Cp-1,(-0x2.3p+1),0x1.Cp-1,(-0x1.5p-1)},{0x5.2p+1,(-0x7.4p-1),0xC.EC048Cp-28,0x3.Ep-1,(-0x7.4p-1)},{(-0x1.5p-1),(-0x1.Dp-1),(-0x1.Dp-1),(-0x1.5p-1),0x4.89D118p+96}}};
            int64_t l_2356 = 0x5A46CED2CB00A159LL;
            int16_t l_2395 = 1L;
            int i, j, k;
            if (l_2158)
                break;
            for (g_455 = (-10); (g_455 < 25); g_455 = safe_add_func_uint16_t_u_u(g_455, 1))
            { /* block id: 1021 */
                int32_t l_2162 = 0x3AEB7CFCL;
                uint32_t l_2166 = 0xB88C0AE1L;
                int32_t l_2191 = (-2L);
                uint8_t l_2218 = 249UL;
                int32_t l_2225[8][9][3] = {{{(-1L),1L,0x093DA3BBL},{0x28710BD5L,1L,1L},{0xCA53CAB4L,0xCA53CAB4L,0x093DA3BBL},{1L,0x28710BD5L,1L},{1L,(-1L),0x093DA3BBL},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L}},{{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L},{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)}},{{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L},{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L}},{{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L},{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)}},{{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L},{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L}},{{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L},{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)}},{{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L},{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L}},{{0x319BBD4AL,1L,(-1L)},{0x83C47773L,1L,0xCA53CAB4L},{(-1L),(-1L),(-1L)},{1L,0x83C47773L,0xCA53CAB4L},{1L,0x319BBD4AL,(-1L)},{0xE88B842BL,0xE88B842BL,0xCA53CAB4L},{0x319BBD4AL,1L,(-1L)},{0x093DA3BBL,1L,0xE88B842BL},{0xE50C80C8L,0xE50C80C8L,(-1L)}}};
                float l_2227 = (-0x1.5p+1);
                uint16_t l_2234[1];
                uint32_t *l_2269 = &l_2166;
                uint32_t ** const l_2268 = &l_2269;
                int32_t *l_2275 = &l_2224;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_2234[i] = 1UL;
                if ((l_2161 != l_2161))
                { /* block id: 1022 */
                    uint8_t **l_2164[6] = {&l_2163,&l_2163,&l_2163,&l_2163,&l_2163,&l_2163};
                    int8_t ****l_2183 = &l_2182[1][3];
                    int32_t l_2185 = 0L;
                    uint32_t l_2193 = 0xDB038D69L;
                    int i;
                    l_2162 ^= 0x93DF3E5DL;
                    if (((***g_1867) == (g_2165 = l_2163)))
                    { /* block id: 1025 */
                        l_2166--;
                        l_2184 = ((((**g_1980) = (safe_lshift_func_uint16_t_u_u((l_2171 & ((void*)0 == l_2172[4][6][0])), 13))) , ((safe_sub_func_int64_t_s_s(((**g_1783) = (safe_div_func_int8_t_s_s((safe_lshift_func_int16_t_s_u(0xC189L, ((safe_add_func_uint64_t_u_u(p_11, (l_2181 != l_2183))) & 0UL))), 255UL))), p_11)) , l_2172[1][8][2])) != (void*)0);
                    }
                    else
                    { /* block id: 1030 */
                        int32_t l_2187 = 0x808A8A18L;
                        int32_t l_2188 = 7L;
                        int32_t l_2190[3][8] = {{0x49D1C5C5L,(-3L),0x49D1C5C5L,9L,9L,0x49D1C5C5L,(-3L),0x49D1C5C5L},{3L,9L,8L,9L,3L,3L,9L,8L},{3L,3L,9L,8L,9L,3L,3L,9L}};
                        int i, j;
                        if (l_2185)
                            break;
                        l_2186 &= p_11;
                        --l_2193;
                    }
                }
                else
                { /* block id: 1035 */
                    uint8_t *l_2212 = &g_1717;
                    int32_t l_2215 = (-4L);
                    int32_t l_2216 = 0x1F2D664DL;
                    int16_t l_2219 = 0x2F79L;
                    int32_t l_2222 = 1L;
                    int32_t l_2223 = 0xAB28B61DL;
                    int32_t l_2226 = 0L;
                    for (g_1660 = 0; (g_1660 <= 24); ++g_1660)
                    { /* block id: 1038 */
                        int64_t ****l_2208[6][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,&g_1782,&l_2207},{(void*)0,&g_1782,(void*)0},{&l_2207,(void*)0,&l_2207},{&l_2207,&l_2207,(void*)0},{(void*)0,(void*)0,(void*)0}};
                        int32_t l_2217 = 0x8DD4FFFFL;
                        int32_t l_2220 = 4L;
                        int32_t l_2221 = 2L;
                        int i, j;
                        (*g_530) = 0x1.1p-1;
                        (*l_1917) ^= (safe_mul_func_uint16_t_u_u((safe_add_func_uint8_t_u_u(p_11, ((p_11 ^ (safe_mod_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s(((l_2162 &= ((**g_1372) &= ((((((((*l_1857) = 4294967295UL) , l_2206) == (l_2209 = l_2207)) && (p_11 , (safe_mod_func_uint16_t_u_u((((***g_1867) == l_2212) ^ (((l_2215 &= ((safe_rshift_func_uint16_t_u_u(0UL, 2)) , l_2189)) >= (-1L)) | l_2216)), l_2217)))) , (**g_1980)) < (*g_406)) & p_11))) , p_11), l_2218)) <= 0x589E795BB4D04592LL), p_11))) != l_2217))), l_2219));
                        if (p_11)
                            continue;
                        l_2228[0]--;
                    }
                    --l_2231;
                }
                (**l_1977) = &l_2192;
                ++l_2234[0];
                if ((((safe_mul_func_int16_t_s_s(((l_2225[5][1][1] = ((****g_1239) ^= p_11)) ^ (safe_rshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_u(((((safe_sub_func_int16_t_s_s((safe_rshift_func_uint8_t_u_s(0xE0L, (((safe_add_func_uint64_t_u_u(((((l_2249--) , 0xE878208EC6E4B56ALL) <= p_11) < (+0x55C0D174CFA075D9LL)), (*g_406))) < 0x1334AAE0L) == (p_11 == (((-6L) & 6L) == 0UL))))), p_11)) | (****g_1867)) >= p_11) , (*g_1373)), l_2166)), 12))), l_2192)) & p_11) < 0L))
                { /* block id: 1056 */
                    uint32_t l_2253 = 0xC148C7F0L;
                    uint32_t *l_2260 = &g_1742;
                    int32_t l_2265 = 0x4D457338L;
                    uint32_t **l_2274 = (void*)0;
                    l_2253--;
                    for (l_2151 = 0; (l_2151 == 2); l_2151 = safe_add_func_uint32_t_u_u(l_2151, 5))
                    { /* block id: 1060 */
                        return p_11;
                    }
                    for (g_1122 = 0; (g_1122 <= 39); g_1122 = safe_add_func_uint32_t_u_u(g_1122, 5))
                    { /* block id: 1065 */
                        uint32_t *l_2262 = (void*)0;
                        uint32_t **l_2261 = &l_2262;
                        if (p_11)
                            break;
                        if ((**g_574))
                            break;
                        (*l_1917) = (l_2260 == ((*l_2261) = &g_1742));
                    }
                    for (l_2192 = 0; (l_2192 > (-29)); l_2192--)
                    { /* block id: 1073 */
                        uint32_t ***l_2272[10] = {&g_2270[2],&g_2270[2],&g_2270[3],&g_2270[2],&g_2270[2],&g_2270[3],&g_2270[2],&g_2270[2],&g_2270[3],&g_2270[2]};
                        int i;
                        l_2265 = (***l_1977);
                        (*g_2276) = (l_2275 = ((**g_628) = ((p_11 ^ (safe_lshift_func_uint16_t_u_u((l_2268 == (l_2274 = (l_2273 = g_2270[2]))), 11))) , &l_2189)));
                    }
                }
                else
                { /* block id: 1081 */
                    int32_t l_2290 = 0x440E6351L;
                    float **l_2337 = &g_530;
                    l_2278 &= 0xFFC0A01FL;
                    if ((**g_60))
                        break;
                    for (l_1854 = 19; (l_1854 > (-10)); --l_1854)
                    { /* block id: 1086 */
                        const uint8_t l_2283 = 1UL;
                        uint16_t *l_2291 = &l_1944;
                        int32_t *l_2314 = &g_1409;
                        int32_t **l_2313 = &l_2314;
                        int64_t *l_2338 = &l_2151;
                        (*g_530) = (((((0x73B281E7L ^ (0x78A9L != ((safe_sub_func_int16_t_s_s((l_2283 , 5L), ((((*l_2275) > 0L) , (*l_2275)) < (((safe_rshift_func_int16_t_s_u(1L, (safe_div_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u(((*l_2291)--), (safe_mul_func_uint8_t_u_u(p_11, (**g_1372))))), l_2184)))) < p_11) ^ 0xDF16720BD8FB0DC1LL)))) , p_11))) , p_11) == 0x8.9p-1) < p_11) != l_2186);
                        (*g_530) = ((l_2189 |= ((***g_1240) && ((0x22L != ((*l_2275) && (((((safe_add_func_int32_t_s_s((safe_lshift_func_uint8_t_u_u((safe_unary_minus_func_int16_t_s((safe_rshift_func_int16_t_s_u(l_2290, ((0xC3L != (p_11 != (safe_mul_func_uint8_t_u_u((((((*g_2082) &= ((safe_mul_func_uint8_t_u_u((l_2192 ^= (((safe_mod_func_uint16_t_u_u(((safe_div_func_uint16_t_u_u((**g_657), p_11)) , (((-9L) ^ p_11) , p_11)), (-2L))) <= (*l_2275)) ^ (**g_1869))), 0xABL)) ^ 0x24E4L)) , 18446744073709551607UL) | p_11) | p_11), p_11)))) , p_11))))), 3)), (*l_2275))) && (*g_1981)) , l_2283) & 0UL) , 9L))) && (*g_1981)))) , (*l_2275));
                        (*g_37) ^= (((*l_2313) = (void*)0) != (void*)0);
                        (***g_803) = ((((***g_628) || (l_2189 > (-1L))) > (safe_add_func_uint16_t_u_u(((safe_add_func_uint8_t_u_u((l_2224 = ((*l_2163) = (!((((p_11 , (safe_mul_func_int8_t_s_s(0xDEL, (safe_sub_func_int8_t_s_s(0x38L, ((*l_1856) |= (((*l_2338) = (safe_add_func_uint64_t_u_u(((*g_2082) = (*g_2082)), (safe_add_func_int32_t_s_s(((*l_1917) = ((safe_mul_func_int8_t_s_s((((safe_lshift_func_int8_t_s_u((safe_add_func_int8_t_s_s((((((***g_1782) = 0x2A06E0520964DCE6LL) < ((((safe_sub_func_uint16_t_u_u(((void*)0 == l_2337), 65535UL)) == (*g_1981)) >= p_11) <= (*l_2275))) <= 5UL) || p_11), p_11)), p_11)) , p_11) != p_11), (*l_2275))) != l_2283)), 4UL))))) < l_2290))))))) ^ p_11) == (-1L)) < 0x5BE81512L)))), p_11)) ^ 255UL), 65535UL))) , (*g_629));
                    }
                    return l_2224;
                }
            }
            if (((*l_1917) , (*g_1160)))
            { /* block id: 1107 */
                uint16_t * const ***l_2346 = (void*)0;
                uint16_t * const ****l_2345 = &l_2346;
                int32_t l_2354 = 0L;
                uint32_t * const *l_2359 = &g_2271;
                uint32_t * const **l_2358 = &l_2359;
                uint32_t * const ***l_2357 = &l_2358;
                l_2189 = (safe_sub_func_float_f_f((p_11 , p_11), ((+p_11) >= ((((*l_1917) = (((l_2342 == (void*)0) <= (safe_mul_func_float_f_f(((l_2224 = l_2278) > ((*g_530) = ((l_2345 == (l_2348 = l_2347)) < ((!(((safe_add_func_float_f_f((((*g_2271) &= (safe_add_func_int8_t_s_s((l_2354 == l_2189), 8L))) , 0x1.A3EEE0p+43), (-0x1.7p-1))) , p_11) && 0xB87B16C9L)) , l_2356)))), p_11))) < l_2354)) <= l_2186) < p_11))));
                (*l_2357) = (void*)0;
                g_2363 = l_2360;
            }
            else
            { /* block id: 1116 */
                uint32_t * const *l_2375 = (void*)0;
                uint32_t * const **l_2374[8];
                uint32_t * const ***l_2373 = &l_2374[5];
                int32_t l_2376 = 0x56199233L;
                const int8_t *l_2403 = &g_133;
                const int8_t **l_2402 = &l_2403;
                const int8_t ** const *l_2401[9] = {&l_2402,&l_2402,&l_2402,&l_2402,&l_2402,&l_2402,&l_2402,&l_2402,&l_2402};
                const int8_t ** const **l_2400 = &l_2401[5];
                int i;
                for (i = 0; i < 8; i++)
                    l_2374[i] = &l_2375;
                for (l_2278 = 0; (l_2278 == 20); l_2278 = safe_add_func_int32_t_s_s(l_2278, 5))
                { /* block id: 1119 */
                    const uint32_t *** const l_2372 = (void*)0;
                    const uint32_t *** const *l_2371 = &l_2372;
                    int32_t l_2384 = 0L;
                    for (g_22 = 1; (g_22 >= 0); g_22 -= 1)
                    { /* block id: 1122 */
                        if (p_11)
                            break;
                        (*l_1917) = (+(((safe_add_func_int8_t_s_s(((p_11 , (l_2371 != l_2373)) <= l_2376), (0xDCL ^ (safe_mul_func_uint16_t_u_u(l_2379, (safe_sub_func_int8_t_s_s(((*g_1373) && (65532UL == ((safe_mul_func_uint16_t_u_u(0x374EL, p_11)) < l_2376))), p_11))))))) , l_2384) , l_2376));
                        (***l_2342) = &l_2384;
                    }
                }
                l_1874 ^= (~(((((*l_1917) = ((safe_add_func_uint32_t_u_u((((*g_1373) |= (l_2189 >= (safe_sub_func_uint32_t_u_u(l_2184, ((+(p_11 >= (((safe_lshift_func_int8_t_s_s(((safe_sub_func_int8_t_s_s((l_2395 && (safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s((((l_2186 |= ((l_2404 = l_2400) != l_2405)) <= (p_11 && l_2376)) > 0xDA9AL), 255UL)), 0x106BL))), p_11)) > p_11), p_11)) || p_11) , (*g_406)))) >= p_11))))) , l_2376), l_2407[1][5][2])) & l_2376)) , (*g_2082)) < p_11) || 0xAB96L));
            }
        }
    }
    for (g_88 = (-30); (g_88 != 11); g_88 = safe_add_func_int16_t_s_s(g_88, 1))
    { /* block id: 1138 */
        uint64_t l_2414 = 0xE4D5B8F424D464BELL;
        uint16_t *l_2416 = &g_455;
        int32_t *l_2424 = &g_143[0];
        int32_t l_2425 = 0xBD850270L;
        int32_t l_2430 = 1L;
        uint8_t l_2448 = 0x82L;
        int32_t l_2449 = 9L;
        const int64_t ****l_2475[1];
        uint16_t ***l_2489[2];
        uint32_t ****l_2496 = &g_2365;
        int32_t l_2533[3];
        uint8_t **l_2567 = &g_2165;
        uint8_t **l_2568 = &g_2165;
        int32_t *l_2609 = &g_38[1][3];
        uint16_t *****l_2665 = &g_1468[1];
        int i;
        for (i = 0; i < 1; i++)
            l_2475[i] = &g_1758;
        for (i = 0; i < 2; i++)
            l_2489[i] = &l_1863;
        for (i = 0; i < 3; i++)
            l_2533[i] = 0xDF2D24EAL;
        l_2425 |= (((*l_2424) = (((*l_2416) &= (safe_mod_func_uint32_t_u_u(((*l_1857) &= ((((l_2414 > p_11) >= (&g_1468[2] != (l_2415 = &g_1468[6]))) <= ((*l_1856) = p_11)) > p_11)), p_11))) == ((((safe_add_func_uint16_t_u_u((((**l_1899) = (safe_add_func_int64_t_s_s(l_2414, ((p_11 , l_2414) != 0L)))) | l_2423), p_11)) , l_2414) < 18446744073709551611UL) == l_2414))) <= 3UL);
        for (g_455 = 0; (g_455 <= 3); g_455 += 1)
        { /* block id: 1148 */
            int16_t l_2435[10][2][6] = {{{(-1L),0x0EA6L,(-1L),(-6L),1L,9L},{0x6352L,0xFD57L,(-10L),(-10L),0xFD57L,0x6352L}},{{9L,1L,(-6L),(-1L),0x0EA6L,(-1L)},{(-2L),0x6352L,3L,(-6L),0xB0C7L,0xCE01L}},{{(-2L),0xA9B2L,(-6L),(-1L),3L,(-8L)},{9L,0x0EA6L,1L,(-10L),1L,0x0EA6L}},{{0x6352L,0xF953L,0x3B10L,(-6L),(-4L),0x6352L},{(-1L),(-1L),0xBFE6L,3L,0x0EA6L,0xC769L}},{{0xCE01L,(-1L),1L,(-6L),(-4L),8L},{(-8L),0xF953L,(-6L),1L,1L,(-6L)}},{{0x0EA6L,0x0EA6L,0x6352L,0x3B10L,3L,0xBBC1L},{0x6352L,0xA9B2L,5L,0xBFE6L,0xB0C7L,0x6352L}},{{0xC769L,0x6352L,5L,1L,0x0EA6L,0xBBC1L},{8L,1L,0x6352L,(-6L),0xFD57L,(-6L)}},{{(-6L),0xFD57L,(-6L),0x6352L,1L,8L},{0xBBC1L,0x0EA6L,1L,5L,0x6352L,0xC769L}},{{0x6352L,0xB0C7L,0xBFE6L,5L,0xA9B2L,0x6352L},{0xBBC1L,3L,0x3B10L,0x6352L,0x0EA6L,0x0EA6L}},{{(-6L),1L,1L,(-6L),0xF953L,(-8L)},{8L,0xBBC1L,0xBFE6L,(-8L),(-2L),0x3B10L}}};
            int32_t l_2437 = 0x102D1F73L;
            int32_t l_2438 = 0x68169B2BL;
            int32_t l_2440 = (-1L);
            uint16_t * const l_2451 = &g_1605;
            uint32_t l_2467 = 18446744073709551612UL;
            int32_t *l_2468 = &l_2449;
            uint32_t l_2490[1][1];
            float l_2516[6][3][8] = {{{(-0x4.Bp-1),0x2.Cp+1,0x4.Dp+1,0x2.7731F3p+34,0x2.Fp+1,0x0.3p+1,0x4.DEAE50p-37,0x3.3A2A3Ap-36},{0x1.Fp+1,0x3.1p-1,0x2.7731F3p+34,(-0x1.7p-1),(-0x4.Bp-1),0x5.094A51p-66,0x4.DEAE50p-37,0x0.Cp-1},{0xC.872F47p-33,(-0x1.7p-1),0x4.Dp+1,0x0.3p+1,0x0.9p+1,0x4.DEAE50p-37,0x1.Fp+1,0xA.882A54p-5}},{{0x0.9p+1,0x4.DEAE50p-37,0x1.Fp+1,0xA.882A54p-5,0x3.3A2A3Ap-36,0x0.2p+1,0x3.49C081p-60,0x3.49C081p-60},{0xB.E40E7Bp+69,0x1.Cp+1,0x2.Cp+1,0x2.Cp+1,0x1.Cp+1,0xB.E40E7Bp+69,(-0x1.Fp-1),0x5.32663Ep-40},{0xB.F56614p+14,0x0.4p-1,0x1.Cp+1,0x0.9p+1,0x5.32663Ep-40,0x7.C7AF32p+84,0x1.Cp+1,0x1.Fp+1}},{{0x3.49C081p-60,0xA.882A54p-5,0x0.7p+1,0x0.9p+1,0x0.11D501p-2,0x3.3A2A3Ap-36,0x0.4p-1,0x5.32663Ep-40},{0x2.7731F3p+34,0x0.11D501p-2,0xA.4B0395p-78,0x2.Cp+1,(-0x9.Ap-1),0xA.882A54p-5,0x0.9p+1,0x3.49C081p-60},{(-0x1.6p-1),0xB.F56614p+14,0x9.3052EAp+45,0xA.882A54p-5,(-0x1.3p-1),0xA.4B0395p-78,(-0x1.3p-1),0xA.882A54p-5}},{{(-0x1.7p-1),0x5.32663Ep-40,(-0x1.7p-1),0x0.3p+1,0xB.F56614p+14,(-0x4.Bp-1),0x2.7731F3p+34,0x0.Cp-1},{0x0.7p+1,(-0x1.6p-1),0x0.9p+1,(-0x1.7p-1),0x3.1p-1,(-0x1.Fp-1),0xB.F56614p+14,0x3.3A2A3Ap-36},{0x0.7p+1,0x1.Cp+1,(-0x9.Ap-1),0x2.7731F3p+34,0xB.F56614p+14,0x2.Fp+1,0x4.Dp+1,0x2.BBD4C0p-35}},{{(-0x1.7p-1),0x0.2p+1,0x7.C7AF32p+84,0x9.3052EAp+45,(-0x1.3p-1),0x2.7731F3p+34,0xC.872F47p-33,0x1.Cp+1},{(-0x1.6p-1),0x7.C7AF32p+84,0x0.11D501p-2,(-0x9.Ap-1),(-0x9.Ap-1),0x0.11D501p-2,0x7.C7AF32p+84,(-0x1.6p-1)},{0x2.7731F3p+34,0x0.9p+1,(-0x1.3p-1),0x5.32663Ep-40,0x0.11D501p-2,(-0x1.6p-1),0xA.4B0395p-78,(-0x1.0p-1)}},{{0x3.49C081p-60,0x2.Fp+1,(-0x1.0p-1),0xC.872F47p-33,0x5.32663Ep-40,(-0x1.6p-1),(-0x1.7p-1),0xB.E40E7Bp+69},{0xB.F56614p+14,0x0.9p+1,0x2.BBD4C0p-35,0x2.Fp+1,0x1.Cp+1,0x0.11D501p-2,0x0.Cp-1,(-0x1.Fp-1)},{0xB.E40E7Bp+69,0x7.C7AF32p+84,0xA.DCE77Dp-65,0x5.094A51p-66,0x3.3A2A3Ap-36,0x2.7731F3p+34,0x0.2p+1,0x2.7731F3p+34}}};
            int16_t l_2520 = 2L;
            int8_t l_2542 = 0L;
            int32_t l_2544[8] = {9L,0x06466B91L,0x06466B91L,9L,0x06466B91L,0x06466B91L,9L,0x06466B91L};
            uint8_t l_2545 = 5UL;
            uint32_t l_2562 = 0x5DB39EC2L;
            uint8_t **l_2566 = &g_1870;
            int64_t l_2604 = 0x71E23042A752D06ELL;
            uint8_t l_2647 = 248UL;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                    l_2490[i][j] = 0x96FBAD4EL;
            }
        }
        for (g_133 = (-21); (g_133 <= (-24)); g_133--)
        { /* block id: 1288 */
            uint16_t *****l_2666 = &g_1468[7];
            uint32_t *l_2672 = &l_2597[3];
            int32_t **l_2679 = (void*)0;
            uint64_t *l_2680 = &g_304;
            int16_t **l_2681 = &g_1242[5];
            for (g_1605 = 0; (g_1605 >= 41); g_1605 = safe_add_func_uint16_t_u_u(g_1605, 7))
            { /* block id: 1291 */
                return p_11;
            }
            (*l_2424) ^= (((safe_mod_func_int32_t_s_s((safe_mul_func_int16_t_s_s((***g_1240), ((**g_1980) >= (!(l_2669[9][4][1] ^= ((**g_720) && (safe_unary_minus_func_int64_t_s((((safe_lshift_func_int8_t_s_s((((!((safe_sub_func_int16_t_s_s(((l_2415 = l_2665) == l_2666), (safe_rshift_func_uint16_t_u_s(p_11, 7)))) , 0x19489864L)) , &l_2361) != (void*)0), 7)) , 0xA1CBL) , 0xC1E35E1867BA512DLL))))))))), p_11)) , l_2665) != l_2666);
            if ((**g_1138))
                continue;
            for (l_1875 = 0; (l_1875 <= 2); l_1875 += 1)
            { /* block id: 1300 */
                int i, j;
                if (g_38[l_1875][(l_1875 + 1)])
                    break;
                (*l_2424) |= (((((((safe_lshift_func_uint8_t_u_s((*g_2165), (**l_1916))) == p_11) , (&g_157[0][4][3] != l_2672)) >= (safe_sub_func_float_f_f((0x2.5p+1 > ((safe_div_func_float_f_f((safe_div_func_float_f_f(((void*)0 != l_2679), (((l_2680 != (void*)0) == (*g_1373)) , p_11))), g_38[l_1875][(l_1875 + 1)])) == 0x3.0C2B7Ap+25)), p_11))) , (void*)0) != l_2681) == p_11);
            }
        }
    }
    (*l_2706) |= (((**g_1241) |= l_2684[1]) >= (l_2685 , (((((((safe_div_func_int8_t_s_s((((safe_add_func_uint64_t_u_u(((l_2690 = (p_11 , (*g_1241))) != ((*l_2692) = &l_2379)), (safe_mod_func_int8_t_s_s((((l_1874 = (p_11 & (l_1871 &= (((safe_div_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_s((***g_1371), 6)) , (safe_lshift_func_int8_t_s_u(((*g_1373) = (safe_rshift_func_uint16_t_u_s(0x8C2FL, 5))), 2))), 3)), 0x8FL)) > p_11) & (**l_1916))))) >= 6L) < p_11), 7L)))) & 1L) | (*g_1981)), 1UL)) <= 4294967287UL) | (**g_1980)) , 0x4B3AL) , 9L) ^ (**l_1916)) != (**l_1916))));
    for (g_295 = 15; (g_295 != 12); g_295 = safe_sub_func_int16_t_s_s(g_295, 1))
    { /* block id: 1315 */
        int32_t *l_2709 = &g_38[1][0];
        int32_t *l_2710 = &g_38[0][2];
        int32_t *l_2711 = &g_22;
        int32_t *l_2712 = &l_1876[0][7];
        int32_t *l_2713[1];
        int8_t l_2714 = 0x2DL;
        int32_t l_2715 = 0xBDE2CD43L;
        int i;
        for (i = 0; i < 1; i++)
            l_2713[i] = &g_38[1][0];
        g_2716++;
    }
    return p_11;
}


/* ------------------------------------------ */
/* 
 * reads : g_1138 g_449 g_38 g_1373 g_1240 g_1241 g_1242 g_110 g_1221 g_94 g_944 g_295 g_1371 g_1372 g_158 g_530 g_224 g_928 g_1604 g_1239 g_721 g_722 g_406 g_583 g_448 g_143 g_657 g_658 g_659 g_1658 g_90 g_1078 g_803 g_628 g_629 g_1409 g_1717 g_386 g_1742 g_1160 g_1753 g_1757 g_22 g_1776 g_455 g_784 g_1782 g_1783 g_37 g_1043 g_1044 g_112 g_88 g_574 g_1122 g_133
 * writes: g_158 g_110 g_348 g_1605 g_94 g_38 g_304 g_1658 g_1660 g_224 g_455 g_295 g_133 g_88 g_1239 g_37 g_1717 g_386 g_1730 g_1753 g_1758 g_1122
 */
static int32_t  func_15(float  p_16, uint8_t  p_17, int32_t  p_18)
{ /* block id: 722 */
    uint64_t l_1566 = 0UL;
    int32_t l_1571[9][2][6] = {{{(-10L),0x33EDF3BAL,0x2721261CL,0x5AD54D6FL,0L,0L},{0x69988A20L,0x82A58D2BL,0xA8553089L,0xCE9B8BBDL,6L,1L}},{{0x82A58D2BL,(-1L),(-10L),(-1L),0xCE9B8BBDL,4L},{(-4L),7L,0xCE9B8BBDL,7L,(-4L),0x2721261CL}},{{1L,0xB5E7B5EEL,0x33EDF3BAL,1L,(-1L),(-3L)},{2L,4L,0x2AE23B05L,0xB5E7B5EEL,4L,0xCFD3D2BEL}},{{0x69988A20L,(-1L),1L,(-10L),1L,0xA8553089L},{4L,1L,0xA5101DCCL,(-9L),(-1L),7L}},{{0L,(-1L),0x2AE23B05L,0xCE9B8BBDL,0x5AD54D6FL,(-9L)},{(-9L),0x42F129ADL,(-1L),0x2721261CL,0xD12C1553L,0xD12C1553L}},{{(-1L),0xA8553089L,0xA8553089L,(-1L),0xE9A17ACEL,0xB5E7B5EEL},{1L,0x69988A20L,(-4L),0xA379B175L,(-1L),0L}},{{0xA8553089L,0x1FADB299L,0x5AD54D6FL,0L,(-1L),4L},{0xB5E7B5EEL,0x69988A20L,0xCFD3D2BEL,0L,0xE9A17ACEL,0xCE9B8BBDL}},{{1L,0xA8553089L,1L,1L,0xD12C1553L,1L},{6L,0x42F129ADL,(-10L),4L,0x5AD54D6FL,0x33EDF3BAL}},{{1L,(-1L),0xE9A17ACEL,(-3L),(-1L),(-3L)},{0x1FADB299L,1L,0x1FADB299L,0x82A58D2BL,1L,0L}}};
    const uint16_t *l_1582[9][2] = {{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492},{&g_492,&g_492}};
    const uint16_t **l_1581 = &l_1582[3][0];
    const uint16_t ***l_1580 = &l_1581;
    uint64_t *l_1583 = (void*)0;
    uint64_t **l_1584 = &l_1583;
    int32_t l_1586 = 0xFC044110L;
    float **l_1601 = &g_530;
    int16_t *****l_1607 = &g_1239;
    int32_t l_1627 = 0x85A30BF7L;
    int32_t l_1630 = 0xAAF22208L;
    int32_t l_1633 = 0x1D07BAF4L;
    int32_t l_1634 = 0xBEB8243DL;
    int32_t l_1635 = (-1L);
    int32_t l_1637[5][5] = {{(-6L),(-6L),(-8L),(-6L),(-6L)},{0x02D5EB3AL,0L,0x02D5EB3AL,0L,(-6L)},{(-6L),0xFF0295D2L,0xFF0295D2L,(-6L),0xFF0295D2L},{(-6L),0L,(-1L),0L,(-6L)},{0xFF0295D2L,(-6L),0xFF0295D2L,0xFF0295D2L,(-6L)}};
    int32_t *l_1669[1][8][6] = {{{&g_583,&l_1634,&g_583,&l_1634,&g_583,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&g_583,&l_1634,&g_583,&l_1634,&g_583,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&g_583,&l_1634,&g_583,&l_1634,&g_583,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634},{&g_583,&l_1634,&g_583,&l_1634,&g_583,&l_1634},{&l_1634,&l_1634,&l_1634,&l_1634,&l_1634,&l_1634}}};
    uint32_t l_1686[7] = {0x7D5A6269L,0x7D5A6269L,0x7D5A6269L,0x7D5A6269L,0x7D5A6269L,0x7D5A6269L,0x7D5A6269L};
    uint16_t l_1700[6][1];
    int8_t l_1743[3][10] = {{0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL},{0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL},{0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL,0x1DL}};
    uint8_t *l_1846[1];
    uint8_t **l_1845 = &l_1846[0];
    int i, j, k;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
            l_1700[i][j] = 0UL;
    }
    for (i = 0; i < 1; i++)
        l_1846[i] = (void*)0;
    if (((l_1566 | (**g_1138)) , (((safe_mul_func_int16_t_s_s((((((safe_sub_func_int8_t_s_s(((*g_1373) = l_1571[5][1][0]), 0x50L)) && 3UL) > (safe_lshift_func_int16_t_s_s(p_17, 6))) != (l_1586 = ((((safe_mod_func_uint8_t_u_u((safe_div_func_uint16_t_u_u(((safe_sub_func_uint32_t_u_u((((l_1580 == (((l_1571[0][0][1] && ((***g_1240) ^= (((*l_1584) = l_1583) != &g_329))) && l_1571[5][1][0]) , &l_1581)) >= l_1571[4][0][2]) & g_1221), l_1571[4][1][2])) != l_1571[1][0][0]), p_18)), l_1571[5][1][0])) , g_94) != 247UL) , l_1571[5][1][0]))) | p_17), 65529UL)) | p_18) >= g_944[3][6][2])))
    { /* block id: 727 */
        float ***l_1599 = &g_529;
        float ***l_1600 = &g_529;
        const int32_t l_1602 = 0L;
        int64_t *l_1603[9] = {&g_1604,&g_1604,&g_1604,&g_1604,&g_1604,&g_1604,&g_1604,&g_1604,&g_1604};
        int i;
        g_1605 = (safe_sub_func_int64_t_s_s((p_18 | ((safe_add_func_int8_t_s_s(((65535UL & (p_18 == 0x7344L)) < g_295), (l_1571[5][1][0] && ((l_1586 = (safe_div_func_int32_t_s_s((safe_add_func_int64_t_s_s((g_348 = (((safe_mul_func_float_f_f((safe_add_func_float_f_f(((l_1601 = (void*)0) == (((p_18 ^ (((***g_1371) <= l_1571[7][1][2]) || l_1602)) && l_1602) , (void*)0)), p_16)), (*g_530))) <= 0xA.9C4FC7p+25) , l_1602)), 0x01AE22FC0D898EB3LL)), (*g_449)))) , p_18)))) && p_17)), p_17));
    }
    else
    { /* block id: 732 */
        int64_t l_1606 = 0xBCBCF103E4C7A221LL;
        return l_1606;
    }
    l_1607 = &g_1239;
    for (g_94 = 0; (g_94 != 25); g_94 = safe_add_func_int8_t_s_s(g_94, 5))
    { /* block id: 738 */
        int32_t *l_1610 = &g_38[3][1];
        int32_t l_1629 = 0L;
        int32_t l_1631[9];
        int64_t l_1661 = 0xE4E7E90763ADEAC1LL;
        const int32_t l_1718 = 0L;
        int16_t l_1752 = 1L;
        int64_t ***l_1763[1];
        uint32_t l_1832 = 0xA95FB6F3L;
        uint8_t ***l_1836 = (void*)0;
        uint8_t *l_1839 = &g_8;
        uint8_t **l_1838[7] = {&l_1839,&l_1839,&l_1839,&l_1839,&l_1839,&l_1839,&l_1839};
        uint8_t ***l_1837 = &l_1838[3];
        int32_t l_1842 = (-2L);
        uint64_t l_1847 = 18446744073709551606UL;
        uint16_t *l_1848 = &g_1122;
        int i;
        for (i = 0; i < 9; i++)
            l_1631[i] = 9L;
        for (i = 0; i < 1; i++)
            l_1763[i] = (void*)0;
        if (((*l_1610) = p_18))
        { /* block id: 740 */
            int32_t *l_1617 = &l_1586;
            int32_t l_1636[7][10] = {{1L,1L,0x38A84109L,1L,1L,0x38A84109L,1L,1L,0x38A84109L,1L},{1L,0L,0L,1L,0L,0L,1L,0L,0L,1L},{0L,1L,0L,0L,1L,0L,0L,1L,0L,0L},{1L,1L,0x38A84109L,1L,1L,0x38A84109L,1L,1L,0x38A84109L,1L},{1L,0L,0L,1L,0L,0L,1L,0L,0L,1L},{0L,1L,0L,0L,1L,0L,0L,1L,0L,0L},{1L,1L,0x38A84109L,1L,1L,0x38A84109L,1L,1L,0x38A84109L,1L}};
            int8_t l_1638[8] = {0x14L,(-7L),(-7L),0x14L,(-7L),(-7L),0x14L,(-7L)};
            uint16_t l_1641[4] = {0UL,0UL,0UL,0UL};
            uint16_t * const *l_1663 = &g_488[3];
            uint16_t * const **l_1662 = &l_1663;
            uint8_t l_1701 = 0x5BL;
            int16_t **** const l_1710 = &g_1240;
            uint8_t *l_1729 = &g_1717;
            uint8_t * const *l_1728[4][2] = {{&l_1729,&l_1729},{&l_1729,&l_1729},{&l_1729,&l_1729},{&l_1729,&l_1729}};
            int i, j;
            if (p_18)
            { /* block id: 741 */
                uint32_t l_1616[3];
                int32_t l_1626 = 0x3BF07609L;
                int32_t l_1628[2][5][8] = {{{1L,0xE293E772L,0xF68F3A4AL,0x470B540EL,0x40493660L,0x4D354BEBL,1L,0L},{0xDF855EB2L,0L,0x4C1F090EL,0x4CAA06B2L,0xE63B8580L,1L,0xE63B8580L,0x4CAA06B2L},{1L,(-10L),1L,0x20913CE0L,9L,(-10L),0xE293E772L,0xDF855EB2L},{0L,(-7L),0x4CAA06B2L,0x685665EAL,0x4D354BEBL,(-1L),9L,0x470B540EL},{0L,(-3L),(-10L),1L,9L,1L,0x685665EAL,(-9L)}},{{1L,0xBB6A9A49L,0xDDF8F1D2L,(-3L),0xE63B8580L,0xE63B8580L,(-3L),0xDDF8F1D2L},{0xDF855EB2L,0xDF855EB2L,1L,1L,0x40493660L,0xE293E772L,0x20913CE0L,(-10L)},{1L,1L,0xB18EFD7AL,0xF68F3A4AL,0xBB6A9A49L,9L,0L,(-10L)},{1L,0x20913CE0L,(-9L),1L,1L,0x685665EAL,(-1L),0xDDF8F1D2L},{0xB18EFD7AL,0x4C1F090EL,0x4CAA06B2L,0xE63B8580L,1L,0xE63B8580L,0x4CAA06B2L,0x4C1F090EL}}};
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_1616[i] = 18446744073709551613UL;
                (*l_1610) &= (safe_rshift_func_uint8_t_u_s(g_928, 0));
                for (g_304 = 0; (g_304 >= 53); ++g_304)
                { /* block id: 745 */
                    int32_t l_1615 = (-10L);
                    int32_t l_1622 = 0xF2AE2754L;
                    uint16_t *l_1623[9][7] = {{&g_1605,&g_1605,&g_455,&g_1605,&g_492,&g_1605,&g_1605},{&g_492,&g_1605,&g_455,&g_1605,&g_492,&g_1122,(void*)0},{&g_492,&g_1605,&g_455,&g_1605,&g_1605,&g_1605,(void*)0},{&g_1605,&g_1605,&g_455,&g_1605,&g_492,&g_1605,&g_1605},{&g_492,&g_1605,&g_455,&g_1605,&g_492,&g_1122,(void*)0},{&g_492,&g_1605,&g_455,&g_1605,&g_1605,&g_1605,(void*)0},{&g_1605,&g_1605,&g_455,&g_1605,&g_492,&g_1605,&g_1605},{&g_492,&g_1605,&g_455,&g_1605,&g_492,&g_1122,(void*)0},{&g_492,&g_1605,&g_455,&g_1605,&g_1605,&g_1605,(void*)0}};
                    int32_t l_1624 = 0L;
                    int32_t l_1632 = 0x8A403A04L;
                    int32_t l_1639 = 0xA85A51B3L;
                    int32_t l_1640[4];
                    uint32_t *l_1646[1][10];
                    uint32_t *l_1657 = &g_1658;
                    uint32_t *l_1659 = &g_1660;
                    const uint16_t ** const *l_1664 = &l_1581;
                    int32_t **l_1665 = &g_37;
                    int32_t **l_1666 = (void*)0;
                    int32_t **l_1667 = (void*)0;
                    int32_t **l_1668[8] = {&l_1617,(void*)0,&l_1617,(void*)0,&l_1617,(void*)0,&l_1617,(void*)0};
                    int i, j;
                    for (i = 0; i < 4; i++)
                        l_1640[i] = (-2L);
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 10; j++)
                            l_1646[i][j] = &g_157[3][1][1];
                    }
                    p_18 &= 0L;
                    (*g_530) = (l_1615 <= ((l_1616[1] >= ((l_1669[0][2][0] = func_31(l_1617, ((safe_mul_func_int8_t_s_s((((*g_1373) |= ((((safe_mul_func_uint16_t_u_u((++l_1641[0]), ((safe_mul_func_int8_t_s_s(((((g_1604 < ((*l_1610) = 0x2A386695L)) , (safe_sub_func_float_f_f(((*g_530) = (p_17 == (safe_sub_func_float_f_f((((safe_add_func_int64_t_s_s((65535UL >= p_18), (safe_rshift_func_int16_t_s_u(((***g_1240) = ((((((safe_rshift_func_int8_t_s_u(((l_1632 = (((*l_1659) = ((*l_1657) = (l_1639 | (*l_1617)))) , l_1622)) <= p_18), g_94)) , (void*)0) != (****l_1607)) == p_17) || l_1661) & (*g_721))), l_1615)))) , (-0x1.Ap-1)) == p_16), 0x0.6p-1)))), p_18))) , l_1662) != l_1664), 0x5CL)) , 0x5AF4L))) , (*l_1617)) > p_17) && (*g_406))) , p_17), p_18)) != 250UL))) != &l_1626)) > p_18));
                }
            }
            else
            { /* block id: 758 */
                uint64_t l_1699 = 18446744073709551611UL;
                uint16_t ***l_1741 = (void*)0;
                int32_t l_1744 = 0L;
                int32_t l_1745 = 0L;
                int32_t l_1747 = 3L;
                int32_t l_1749 = 0xDA0EF3D1L;
                for (g_304 = 0; (g_304 <= 4); g_304 += 1)
                { /* block id: 761 */
                    int32_t * const l_1670 = &g_143[0];
                    for (g_295 = 3; (g_295 >= 1); g_295 -= 1)
                    { /* block id: 764 */
                        if ((**g_448))
                            break;
                        return p_17;
                    }
                    for (g_133 = 4; (g_133 >= 1); g_133 -= 1)
                    { /* block id: 770 */
                        int32_t **l_1671 = (void*)0;
                        int32_t **l_1672 = (void*)0;
                        int32_t **l_1673 = &l_1669[0][0][2];
                        uint64_t *l_1678 = &g_88;
                        (*l_1673) = l_1670;
                        if ((*l_1670))
                            continue;
                        (*l_1617) ^= ((&g_406 == (void*)0) == (p_18 , (((**g_657) , (((***l_1607) != (void*)0) == ((*l_1678) = (safe_mul_func_int16_t_s_s((*l_1670), p_18))))) == ((+(safe_rshift_func_int16_t_s_u(((((**g_1372) = (p_18 , 0x88L)) <= (*l_1670)) == 6UL), 12))) , g_1658))));
                    }
                }
                if ((safe_lshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_u(((4UL < (l_1686[2] , p_17)) < (*l_1617)), g_295)), p_18)))
                { /* block id: 778 */
                    uint16_t l_1715 = 9UL;
                    for (l_1635 = 0; (l_1635 <= 19); ++l_1635)
                    { /* block id: 781 */
                        int8_t *** volatile *l_1690 = &g_1371;
                        int8_t *** volatile **l_1689 = &l_1690;
                        int16_t ****l_1695 = &g_1240;
                        uint16_t *l_1696 = &l_1641[3];
                        uint8_t *l_1716 = &g_1717;
                        int32_t l_1719 = 0x4C63214FL;
                        (*l_1689) = &g_1371;
                        (***g_803) = ((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((*l_1617) ^ (((*l_1607) = l_1695) != &g_1044[0])) < (((*l_1696) ^= (*l_1610)) & p_17)), 0x2288L)), (((p_18 , (((g_90[0][4] == ((((safe_div_func_uint64_t_u_u((0L == g_1078), 7UL)) < 0x9050F889L) , 0xFDDC2C9597725BFELL) != 6L)) , 1L) == l_1699)) | l_1700[2][0]) < 0x8E58L))) , (void*)0);
                        (*l_1610) ^= ((l_1701 | (((safe_rshift_func_uint16_t_u_s(l_1699, 4)) < (safe_rshift_func_int16_t_s_u(((safe_rshift_func_int8_t_s_u((((*l_1716) |= (0xBD19B765L || ((((*l_1617) < (safe_mod_func_int16_t_s_s(((l_1710 == (void*)0) , (*l_1617)), (p_18 , (safe_div_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(((((p_17 , g_1409) >= p_18) , p_17) > p_18), p_17)), p_18)))))) <= p_18) ^ l_1715))) != l_1718), p_17)) > l_1719), p_17))) == 0x34C40D0FCA461FB8LL)) != p_18);
                    }
                    l_1610 = &l_1633;
                    if ((+g_386[1]))
                    { /* block id: 790 */
                        uint32_t *l_1723 = &g_386[3];
                        uint8_t *l_1727 = &g_1717;
                        uint8_t **l_1726[7][6] = {{&l_1727,&l_1727,&l_1727,&l_1727,(void*)0,&l_1727},{&l_1727,&l_1727,(void*)0,&l_1727,(void*)0,&l_1727},{&l_1727,&l_1727,&l_1727,&l_1727,(void*)0,(void*)0},{&l_1727,&l_1727,&l_1727,&l_1727,(void*)0,&l_1727},{&l_1727,&l_1727,(void*)0,&l_1727,(void*)0,&l_1727},{&l_1727,&l_1727,&l_1727,&l_1727,(void*)0,(void*)0},{&l_1727,&l_1727,&l_1727,&l_1727,(void*)0,&l_1727}};
                        int i, j;
                        (*l_1617) ^= ((safe_mod_func_int8_t_s_s(p_17, ((0x1.5p+1 >= l_1699) , (((--(*l_1723)) | ((0x92L > (g_1730 = (g_1717 = (l_1726[2][1] != l_1728[1][0])))) & (safe_add_func_int32_t_s_s((safe_sub_func_int8_t_s_s((((((!(g_722 || (safe_rshift_func_int8_t_s_u(((*g_1373) = (safe_rshift_func_int16_t_s_u((safe_unary_minus_func_uint32_t_u(p_18)), ((void*)0 == l_1741)))), 0)))) , l_1699) , (**g_657)) ^ g_1742) , p_17), l_1743[1][1])), (*l_1610))))) && 8UL)))) > 4294967286UL);
                        if ((**g_1138))
                            break;
                        if (l_1699)
                            break;
                    }
                    else
                    { /* block id: 798 */
                        return (*g_1160);
                    }
                }
                else
                { /* block id: 801 */
                    int8_t l_1746 = 1L;
                    int32_t l_1748 = 0xA20EF1C0L;
                    int32_t l_1750 = (-2L);
                    int32_t l_1751 = 0x8DF506AAL;
                    float l_1778 = 0x1.9p-1;
                    int64_t ***l_1787 = &g_1783;
                    ++g_1753;
                    p_18 ^= (+p_17);
                    (*g_1757) = &g_1255;
                    if (((*l_1610) = (255UL & (safe_div_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(p_17, g_22)), ((0L ^ ((*g_406) = (l_1763[0] != l_1763[0]))) || (0x686AB169L != (((safe_lshift_func_int16_t_s_s((safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u(((((safe_mul_func_uint8_t_u_u((p_18 , (safe_rshift_func_uint8_t_u_u(((g_1776 == &g_1777) & 0x30L), 6))), p_18)) < p_18) == p_18) >= 0L), l_1699)), g_455)), p_17)) , 0xC44C07E73810325CLL) <= g_784))))))))
                    { /* block id: 807 */
                        int64_t ****l_1784 = &l_1763[0];
                        int32_t l_1786 = 0x71A6223DL;
                        (*g_629) = &p_18;
                        l_1629 &= (safe_sub_func_int16_t_s_s(((****l_1710) = (~(((*l_1784) = g_1782) == ((((l_1744 ^= (**g_1783)) == (*l_1617)) <= ((0x586CL & 65535UL) != ((safe_unary_minus_func_int16_t_s(((l_1786 && (p_18 == 0xE532L)) >= (l_1751 |= (g_38[2][0] || 0xCDL))))) | l_1747))) , l_1787)))), (*l_1610)));
                        (****g_803) ^= 0x8AA2CAC3L;
                    }
                    else
                    { /* block id: 815 */
                        (*g_530) = 0x1.3p-1;
                    }
                }
            }
        }
        else
        { /* block id: 820 */
            uint32_t l_1803 = 0x073AC6C1L;
            int64_t l_1804[3];
            const int32_t **l_1828[9] = {&g_449,&g_449,&g_449,&g_449,&g_449,&g_449,&g_449,&g_449,&g_449};
            int i;
            for (i = 0; i < 3; i++)
                l_1804[i] = (-8L);
            for (g_88 = 0; (g_88 > 34); g_88++)
            { /* block id: 823 */
                int64_t l_1790 = (-6L);
                int16_t **** const *l_1807 = &g_1239;
                int32_t **l_1830 = &l_1669[0][2][0];
                if ((l_1790 <= p_18))
                { /* block id: 824 */
                    p_16 = (-0x9.3AC15Bp+24);
                }
                else
                { /* block id: 826 */
                    uint32_t l_1796 = 0x3FDCDC7DL;
                    l_1804[2] |= ((((**g_1372) = (safe_lshift_func_int8_t_s_s((l_1790 >= (((((***g_1782) ^= ((l_1796 , (p_18 = (p_18 < 0xECL))) == (0xFC7F64D8L > (safe_mul_func_uint16_t_u_u(((((((p_17 <= ((((((((safe_mul_func_float_f_f(p_16, 0x3.Bp+1)) != ((safe_mul_func_uint16_t_u_u(0x71FAL, p_17)) , 0x3.8p-1)) , l_1790) , (void*)0) != (*g_1241)) , (void*)0) != (*g_1043)) >= 4294967287UL)) || l_1803) < g_944[6][4][2]) , p_17) || p_17) && 0x4AE2F1CAL), l_1803))))) , (*l_1610)) >= 0x14L) >= p_17)), 4))) , 0xBBBEL) , 0x18DDDE3EL);
                    if (l_1804[2])
                    { /* block id: 831 */
                        uint32_t l_1819 = 0x952DFBE4L;
                        int32_t l_1820 = 0L;
                        (*l_1610) |= ((((g_112 | p_17) , (((safe_lshift_func_uint16_t_u_u(((((void*)0 != l_1807) || ((((((-0x3.Ap+1) > (!((safe_mul_func_float_f_f(((*g_530) = (p_16 >= (((safe_sub_func_float_f_f((((safe_sub_func_float_f_f((-(((safe_mul_func_float_f_f((!p_18), l_1819)) <= l_1790) == l_1796)), p_17)) , &p_18) != &l_1718), p_16)) != p_17) == l_1790))), p_18)) != p_18))) , p_17) > g_1078) == 0x5E118714L) == p_17)) , 0xB42CL), l_1796)) , 0x51L) != p_18)) && p_18) | g_88);
                        if (l_1820)
                            continue;
                    }
                    else
                    { /* block id: 835 */
                        const int32_t ***l_1829 = &l_1828[7];
                        int32_t l_1831[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1831[i] = 2L;
                        (*g_530) = 0x1.185864p-81;
                        if ((**g_574))
                            break;
                        l_1831[2] &= ((safe_sub_func_int64_t_s_s((((safe_lshift_func_int8_t_s_s((**g_1372), 4)) != ((l_1790 ^ ((safe_mod_func_int64_t_s_s((!((((*l_1829) = l_1828[7]) == l_1830) <= ((g_1122 > (&g_1717 != (void*)0)) == (p_18 && (p_17 & g_133))))), p_17)) == 0x77CCL)) > p_17)) , p_18), g_158)) <= (*l_1610));
                        (***g_803) = &p_18;
                    }
                    if (p_17)
                        continue;
                }
            }
        }
        --l_1832;
        (*l_1610) = (((safe_unary_minus_func_int8_t_s((*l_1610))) , (((((*g_406) = (((*l_1837) = (void*)0) != ((safe_lshift_func_int16_t_s_s((l_1842 ^ 0x3E03L), (safe_rshift_func_int8_t_s_u(0xCDL, 0)))) , l_1845))) & (((*l_1848) ^= l_1847) , (((((safe_div_func_uint8_t_u_u((((p_18 | ((((*l_1848)--) | (p_17 , p_17)) | 1UL)) < (-1L)) && 0x3BD44DACL), p_18)) == (*l_1610)) , 0xC3L) || (*l_1610)) , 1L))) >= p_17) & 0L)) <= 0xEDL);
    }
    return p_18;
}


/* ------------------------------------------ */
/* 
 * reads : g_8
 * writes: g_22
 */
static int8_t  func_19(uint16_t  p_20)
{ /* block id: 1 */
    int32_t *l_21[5];
    int i;
    for (i = 0; i < 5; i++)
        l_21[i] = &g_22;
    g_22 = g_8;
    return p_20;
}


/* ------------------------------------------ */
/* 
 * reads : g_492
 * writes: g_492
 */
static float  func_25(int32_t * p_26, const int8_t  p_27, const int64_t  p_28, int32_t * p_29, int32_t * p_30)
{ /* block id: 487 */
    int16_t l_1079 = (-3L);
    uint32_t l_1091 = 4294967290UL;
    int32_t **l_1116 = &g_37;
    int32_t l_1135 = 0xD926D8ECL;
    uint32_t l_1147 = 0x6D7D7A6BL;
    int32_t l_1152 = 0x6D25B6DEL;
    float l_1159 = 0x7.2p-1;
    uint8_t *l_1204[7][8] = {{(void*)0,&g_8,&g_8,(void*)0,&g_94,&g_94,(void*)0,(void*)0},{&g_8,(void*)0,&g_8,(void*)0,&g_8,(void*)0,&g_8,&g_8},{&g_8,(void*)0,&g_8,&g_8,&g_94,&g_94,&g_8,&g_8},{&g_8,&g_8,(void*)0,&g_94,&g_94,(void*)0,(void*)0,(void*)0},{&g_8,&g_8,&g_94,&g_8,&g_8,&g_8,&g_8,(void*)0},{&g_8,&g_94,(void*)0,&g_94,&g_94,(void*)0,&g_94,&g_8},{(void*)0,&g_8,(void*)0,&g_8,&g_8,&g_8,&g_8,&g_8}};
    const int16_t *l_1220 = &g_1221;
    uint32_t l_1236 = 0xBEBB445DL;
    int64_t **l_1325 = &g_406;
    int64_t ***l_1324 = &l_1325;
    int32_t ****l_1357 = &g_628;
    int32_t l_1436[10] = {0x921A6255L,(-1L),9L,(-1L),0x921A6255L,0x921A6255L,0x842E070DL,0x5C6F158DL,0x842E070DL,(-1L)};
    uint64_t l_1492 = 0xCD08BD9A9B43B210LL;
    int i, j;
    for (g_492 = 0; (g_492 <= 5); g_492 += 1)
    { /* block id: 490 */
        const int64_t l_1090 = 0x9CB40FFF111A8E53LL;
        int32_t l_1105 = 1L;
        const int32_t *l_1106 = &g_38[0][0];
        int32_t ***l_1127 = &l_1116;
        uint64_t l_1153[9];
        uint8_t * const *l_1205 = (void*)0;
        uint16_t l_1229[6];
        int32_t l_1234 = (-1L);
        int32_t l_1235 = (-7L);
        int32_t l_1261 = 7L;
        int8_t *l_1370 = &g_90[0][4];
        int8_t ** const l_1369 = &l_1370;
        uint64_t l_1412[1][10] = {{18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL}};
        uint64_t l_1485[8] = {9UL,9UL,9UL,9UL,9UL,9UL,9UL,9UL};
        int64_t *l_1522 = &g_295;
        int i, j;
        for (i = 0; i < 9; i++)
            l_1153[i] = 0x1EE78919CF279216LL;
        for (i = 0; i < 6; i++)
            l_1229[i] = 0xCAD1L;
    }
    return p_27;
}


/* ------------------------------------------ */
/* 
 * reads : g_455 g_110 g_583 g_530
 * writes: g_455 g_110 g_224
 */
static int32_t * func_31(int32_t * p_32, int64_t  p_33)
{ /* block id: 456 */
    uint16_t l_1014[6][2][4] = {{{65532UL,5UL,0x3813L,65535UL},{0x90DDL,0x0B0DL,0x3813L,1UL}},{{65532UL,3UL,0x72EBL,0xD3F0L},{0x0B0DL,6UL,6UL,0x0B0DL}},{{6UL,0x0B0DL,5UL,0x90DDL},{3UL,65532UL,0x6916L,65532UL}},{{0x0B0DL,0x90DDL,65529UL,65532UL},{5UL,65532UL,0x3813L,0x90DDL}},{{65535UL,0x0B0DL,0xD410L,0x0B0DL},{65532UL,6UL,65529UL,0xD3F0L}},{{1UL,3UL,6UL,1UL},{3UL,0x0B0DL,65535UL,65535UL}}};
    const uint16_t **l_1015 = (void*)0;
    const int32_t *l_1016 = &g_143[0];
    const int32_t **l_1017 = &l_1016;
    int32_t l_1024 = 0xB70C000DL;
    int32_t l_1050[8][2][4] = {{{(-1L),0x1F780D37L,0x1F780D37L,(-1L)},{(-1L),0xE10E536FL,0x2BCF0D9DL,0x6E6FA030L}},{{(-7L),(-1L),0x15440B3BL,0x546E7C72L},{(-5L),0x35C390D9L,(-5L),0x546E7C72L}},{{0x15440B3BL,(-1L),(-7L),0x6E6FA030L},{0x2BCF0D9DL,0xE10E536FL,(-1L),(-1L)}},{{0x1F780D37L,0x1F780D37L,(-1L),(-5L)},{0x2BCF0D9DL,0xD0D79337L,(-7L),0xE10E536FL}},{{0x15440B3BL,(-7L),(-5L),(-7L)},{(-5L),(-7L),0x15440B3BL,0xE10E536FL}},{{(-7L),0xD0D79337L,0x2BCF0D9DL,(-5L)},{(-1L),0x1F780D37L,0x1F780D37L,(-1L)}},{{(-1L),0xE10E536FL,0x2BCF0D9DL,0x6E6FA030L},{(-7L),(-1L),0x15440B3BL,0x546E7C72L}},{{(-5L),0x35C390D9L,(-5L),0x546E7C72L},{0x15440B3BL,(-1L),(-7L),0x6E6FA030L}}};
    int i, j, k;
lbl_1066:
    (*l_1017) = l_1016;
    for (g_455 = (-29); (g_455 > 18); g_455 = safe_add_func_int8_t_s_s(g_455, 2))
    { /* block id: 461 */
        uint16_t l_1025 = 0x48A9L;
        int32_t *l_1028 = &g_583;
        int64_t *l_1041 = (void*)0;
        int64_t *l_1042 = &g_295;
        uint16_t l_1045 = 65535UL;
        int32_t l_1048 = 0xDBA04C06L;
        int32_t l_1049 = (-3L);
        int32_t l_1052 = (-1L);
        int32_t l_1055 = 0x4F0B2921L;
        int32_t l_1060 = (-6L);
        uint64_t l_1074 = 0xBB16F5E05ACAEB58LL;
        for (g_110 = 0; (g_110 > 23); ++g_110)
        { /* block id: 464 */
            int64_t l_1022 = 0L;
            int32_t *l_1023[6][1][5] = {{{&g_143[0],(void*)0,&g_38[1][0],(void*)0,&g_143[0]}},{{&g_143[0],&g_38[1][0],&g_583,&g_38[1][0],&g_143[0]}},{{&g_143[0],(void*)0,&g_38[1][0],(void*)0,&g_143[0]}},{{&g_143[0],&g_38[1][0],&g_583,&g_38[1][0],&g_143[0]}},{{&g_143[0],(void*)0,&g_38[1][0],(void*)0,&g_143[0]}},{{&g_143[0],&g_38[1][0],&g_583,&g_38[1][0],&g_143[0]}}};
            int16_t *l_1036 = &g_114;
            int16_t **l_1035 = &l_1036;
            uint32_t l_1063[8][1][8] = {{{0UL,18446744073709551608UL,0xD7431616L,0xBB716A59L,7UL,1UL,0x6089FA30L,1UL}},{{0xBB716A59L,18446744073709551610UL,18446744073709551610UL,18446744073709551610UL,0xBB716A59L,1UL,18446744073709551611UL,7UL}},{{1UL,18446744073709551608UL,18446744073709551610UL,2UL,0x6089FA30L,0x6089FA30L,2UL,18446744073709551610UL}},{{7UL,7UL,18446744073709551610UL,18446744073709551608UL,18446744073709551610UL,0x6089FA30L,0UL,0xBB716A59L}},{{2UL,0UL,1UL,18446744073709551610UL,1UL,0UL,2UL,0xBB716A59L}},{{0UL,0x6089FA30L,18446744073709551610UL,18446744073709551608UL,18446744073709551611UL,18446744073709551610UL,18446744073709551610UL,18446744073709551611UL}},{{7UL,2UL,2UL,7UL,18446744073709551611UL,1UL,0xBB716A59L,18446744073709551610UL}},{{0UL,0xD7431616L,18446744073709551615UL,18446744073709551611UL,1UL,18446744073709551611UL,18446744073709551615UL,0xD7431616L}}};
            int i, j, k;
            ++l_1025;
            (*l_1017) = (l_1025 , (*l_1017));
            for (l_1045 = 0; (l_1045 <= 0); l_1045 += 1)
            { /* block id: 470 */
                int32_t l_1053 = 0L;
                int32_t l_1054 = 1L;
                int32_t l_1058 = 1L;
                int32_t l_1059 = 9L;
                int32_t l_1062 = 0x6C01C4D4L;
                uint64_t l_1067[3];
                int32_t l_1071[4] = {0x83082E8DL,0x83082E8DL,0x83082E8DL,0x83082E8DL};
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_1067[i] = 1UL;
                if ((*l_1028))
                { /* block id: 471 */
                    float l_1056 = (-0x1.Dp+1);
                    int32_t l_1057 = 1L;
                    for (l_1025 = 0; (l_1025 < 45); ++l_1025)
                    { /* block id: 474 */
                        int32_t l_1051 = 6L;
                        int32_t l_1061[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1061[i] = (-10L);
                        --l_1063[0][0][3];
                        (*g_530) = 0xC.91FD4Ep+49;
                        if (l_1025)
                            goto lbl_1066;
                    }
                }
                else
                { /* block id: 479 */
                    int32_t l_1070 = 0xA6F60AF1L;
                    int32_t l_1072 = (-5L);
                    int32_t l_1073[6][3][9] = {{{0L,0x8BDF04CDL,0xFCBF350FL,(-1L),0xB06C237DL,1L,(-1L),0x1E564856L,0x1E564856L},{0x8013BD20L,0xCF18AE60L,0xA19FB72FL,0x5A990257L,0xA19FB72FL,0xCF18AE60L,0x8013BD20L,2L,(-3L)},{0L,0x7D9BCC26L,(-1L),0x8BDF04CDL,(-1L),0x1E564856L,0x7AE4377BL,0xFCBF350FL,(-10L)}},{{0x0796A4F8L,(-1L),0x32ADF984L,(-1L),0x0796A4F8L,0x5A990257L,0xE8C3D036L,2L,0x0796A4F8L},{0x7D9BCC26L,0x92D403F0L,0x8BDF04CDL,7L,0x1E564856L,0xFCBF350FL,0xFCBF350FL,0x1E564856L,7L},{0xD288026AL,2L,0xD288026AL,(-1L),(-3L),0x5A990257L,0x8013BD20L,(-1L),0xA19FB72FL}},{{7L,0xB06C237DL,4L,(-1L),(-10L),0x1E564856L,0x92D403F0L,1L,0x7D9BCC26L},{0xCAE41A58L,0x7271E0EBL,0xD83AAF88L,(-1L),0x0796A4F8L,0xCF18AE60L,0xD83AAF88L,1L,0xCAE41A58L},{(-10L),0x8BDF04CDL,1L,7L,7L,1L,0x8BDF04CDL,(-10L),0L}},{{0x8013BD20L,0x7271E0EBL,0xD288026AL,(-1L),0xA19FB72FL,0x7271E0EBL,(-3L),0x38B3B0A4L,(-3L)},{0x1E564856L,0xB06C237DL,0xECA5BC82L,0x8BDF04CDL,0x7D9BCC26L,(-10L),1L,0xFCBF350FL,0L},{0xCAE41A58L,2L,0x32ADF984L,0x5A990257L,0xCAE41A58L,(-1L),0xE8C3D036L,(-1L),0xCAE41A58L}},{{(-1L),0x92D403F0L,0x92D403F0L,(-1L),0L,0xFCBF350FL,1L,(-10L),0x7D9BCC26L},{0xD288026AL,(-1L),0xA19FB72FL,0x7271E0EBL,(-3L),0x38B3B0A4L,(-3L),0x7271E0EBL,0xA19FB72FL},{0xB06C237DL,0x7D9BCC26L,0x7AE4377BL,(-1L),0L,(-10L),0x8BDF04CDL,1L,7L}},{{0x0796A4F8L,0xCF18AE60L,0xD83AAF88L,1L,0xCAE41A58L,1L,0xD83AAF88L,0xCF18AE60L,0x0796A4F8L},{0x5426397DL,0x8BDF04CDL,0x7AE4377BL,(-1L),0x7D9BCC26L,1L,0x92D403F0L,0x1E564856L,(-10L)},{0x8013BD20L,(-1L),0xA19FB72FL,2L,0xA19FB72FL,(-1L),0x8013BD20L,0x5A990257L,(-3L)}}};
                    int i, j, k;
                    l_1067[1]++;
                    --l_1074;
                }
            }
        }
    }
    return p_32;
}


/* ------------------------------------------ */
/* 
 * reads : g_38 g_8 g_60 g_96 g_37 g_90 g_157 g_94 g_112 g_110 g_448 g_143 g_158 g_304 g_88 g_449 g_492 g_329 g_406 g_295 g_528 g_386 g_529 g_114 g_574 g_583 g_628 g_629 g_530 g_656 g_455 g_224 g_657 g_658 g_659
 * writes: g_37 g_94 g_110 g_449 g_455 g_143 g_488 g_38 g_158 g_114 g_112 g_529 g_88 g_304 g_90 g_628 g_224 g_583 g_295
 */
static int32_t * func_34(int8_t  p_35, const int32_t * p_36)
{ /* block id: 4 */
    int32_t * const l_40[2] = {&g_38[1][0],&g_38[1][0]};
    int32_t *l_51 = &g_38[1][0];
    int32_t l_65 = 0L;
    int16_t *l_352 = &g_110;
    float l_456 = 0x0.BE778Cp-99;
    int64_t *l_470 = (void*)0;
    uint8_t l_499[3][1];
    int64_t *l_514 = &g_295;
    int32_t l_517 = 0x92D88767L;
    float * const *l_563 = (void*)0;
    uint8_t l_572 = 0xB0L;
    int32_t l_610 = 0x1F51A380L;
    uint8_t l_662 = 0x16L;
    int64_t l_664 = 0x85234B0ACC1BC967LL;
    uint8_t *l_689[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t l_697 = 1UL;
    int16_t **l_715 = (void*)0;
    float l_770 = 0x7.0DE301p-17;
    int32_t ****l_773 = &g_628;
    int32_t l_910 = 0xF3CD90CBL;
    uint32_t l_915[9][9] = {{0xAAD7D39DL,1UL,1UL,0xAAD7D39DL,0UL,4294967295UL,1UL,0x6AE51A9FL,0xAAD7D39DL},{0x8EA1CE71L,0x05125ADEL,9UL,0x05125ADEL,0x8EA1CE71L,0x9C53E9A0L,4294967292UL,4294967295UL,4294967292UL},{0x6AE51A9FL,0UL,0xAC86067CL,0xAC86067CL,0UL,0x6AE51A9FL,0x0368B84DL,0xAC86067CL,4294967295UL},{0UL,0x9C53E9A0L,2UL,4294967295UL,2UL,0x9C53E9A0L,0UL,0x05125ADEL,0x25A5C9CAL},{0xAAD7D39DL,0x0368B84DL,4294967295UL,0x6AE51A9FL,0x6AE51A9FL,4294967295UL,0x0368B84DL,0xAAD7D39DL,0x6AE51A9FL},{7UL,0x05125ADEL,4294967292UL,0x8EF2A2A8L,9UL,0x8EF2A2A8L,4294967292UL,0x05125ADEL,7UL},{0xD73A8525L,0x6AE51A9FL,0xAC86067CL,1UL,0xD73A8525L,0xD73A8525L,1UL,0xAC86067CL,0x6AE51A9FL},{2UL,1UL,0x25A5C9CAL,4294967295UL,0x1A94523FL,0x05125ADEL,0x1A94523FL,4294967295UL,0x25A5C9CAL},{0xD73A8525L,0UL,1UL,4294967295UL,0x6AE51A9FL,0UL,0UL,0x6AE51A9FL,4294967295UL}};
    int64_t l_940 = 0x3019E9AC0652C8ADLL;
    int8_t l_1002 = 1L;
    int32_t *l_1011 = &l_610;
    int32_t *l_1012 = &g_38[3][2];
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_499[i][j] = 1UL;
    }
    if (((l_40[1] == ((&g_38[2][0] == l_40[1]) , func_41(&g_38[1][0], (((void*)0 == &g_38[1][0]) > (func_46(l_51, g_38[2][1], (safe_rshift_func_int8_t_s_u((((safe_div_func_int32_t_s_s((safe_unary_minus_func_int32_t_s((((safe_mod_func_int64_t_s_s((!((0xE06DFE53L & g_8) || (*p_36))), (-5L))) != 254UL) && (*l_51)))), (*p_36))) , (-8L)) <= g_38[1][0]), p_35)), (*l_51)) , l_65)), p_35, &l_65))) & (-4L)))
    { /* block id: 117 */
        uint8_t l_258 = 253UL;
        uint8_t *l_271 = (void*)0;
        uint8_t *l_272 = &g_94;
        uint8_t *l_275 = (void*)0;
        int32_t l_276 = 0xFD715AADL;
        int32_t l_277 = 0xF1D413EEL;
        int32_t l_278 = 4L;
        int32_t l_279 = 0xB1FC8BD9L;
        uint32_t l_280[3];
        int32_t **l_364 = &g_37;
        uint32_t l_391 = 0x74188EB7L;
        uint32_t l_445[7][9] = {{4294967295UL,4294967288UL,4294967288UL,4294967295UL,4294967295UL,8UL,4294967295UL,8UL,4294967295UL},{6UL,0UL,0UL,6UL,4294967293UL,0x16670EDBL,6UL,0x16670EDBL,4294967293UL},{4294967295UL,4294967288UL,4294967288UL,4294967295UL,4294967295UL,8UL,4294967295UL,8UL,4294967295UL},{6UL,0UL,0UL,6UL,4294967293UL,0x16670EDBL,6UL,0x16670EDBL,4294967293UL},{4294967295UL,4294967288UL,4294967288UL,4294967295UL,4294967295UL,8UL,4294967295UL,8UL,4294967295UL},{6UL,0UL,0UL,6UL,4294967293UL,0x16670EDBL,6UL,0x16670EDBL,4294967293UL},{4294967295UL,4294967288UL,4294967288UL,4294967295UL,4294967295UL,8UL,4294967295UL,8UL,4294967295UL}};
        float *l_446 = &g_324;
        int64_t l_520 = 0xEED2AFF8B1D5CB25LL;
        uint32_t l_577 = 0x09042108L;
        uint16_t **l_606 = &g_488[1];
        int i, j;
        for (i = 0; i < 3; i++)
            l_280[i] = 1UL;
        l_277 ^= (safe_mul_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(0x3DF8L, g_90[1][2])), (g_157[1][1][9] && (l_258 ^ (((safe_mul_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((safe_div_func_int8_t_s_s((safe_add_func_int16_t_s_s((((p_35 , (*l_51)) == (safe_sub_func_uint32_t_u_u((l_258 , ((((safe_mod_func_int32_t_s_s((((l_276 = (((*l_272)--) || ((void*)0 == &g_133))) || 0x80L) == p_35), p_35)) , &g_110) != &g_110) < 1L)), 0xF3639B91L))) , l_276), 65535UL)), l_258)), p_35)), (*l_51))) <= p_35) ^ (*p_36)))))), g_112));
        l_280[2]++;
        for (g_110 = 4; (g_110 < 25); g_110++)
        { /* block id: 124 */
            const int32_t l_305 = 0x9EFD839DL;
            uint64_t *l_313 = (void*)0;
            int32_t l_328 = 0x631CFC7AL;
            int16_t *l_351 = &g_110;
            float *l_403[3][10][2] = {{{&g_224,&g_324},{&g_324,&g_224},{&g_224,&g_324},{&g_324,&g_224},{&g_324,&g_324},{&g_224,&g_224},{&g_324,&g_324},{&g_224,&g_324},{&g_224,&g_224},{&g_224,&g_224}},{{&g_324,&g_224},{&g_324,&g_324},{&g_224,&g_224},{&g_324,&g_324},{&g_324,&g_224},{&g_224,(void*)0},{&g_224,&g_224},{(void*)0,&g_224},{&g_224,&g_224},{&g_224,&g_224}},{{&g_224,&g_224},{&g_224,(void*)0},{&g_224,&g_224},{(void*)0,&g_224},{&g_224,&g_324},{&g_224,&g_224},{(void*)0,&g_224},{&g_224,(void*)0},{&g_224,&g_224},{&g_224,&g_224}}};
            uint8_t *l_409 = &l_258;
            int i, j, k;
        }
        if ((&l_258 != &l_258))
        { /* block id: 208 */
            uint32_t l_453[8] = {0x3BAD6BF1L,0x29D05F50L,0x29D05F50L,0x3BAD6BF1L,0x29D05F50L,0x29D05F50L,0x3BAD6BF1L,0x29D05F50L};
            uint16_t *l_454 = &g_455;
            uint32_t l_457 = 18446744073709551608UL;
            int32_t l_468 = 0xDA2AB3D2L;
            int32_t l_469 = 6L;
            int32_t l_497 = 0x0A7A9D0EL;
            int64_t *l_522[3][8][8] = {{{&l_520,&l_520,&g_348,&g_348,&g_348,&g_348,&g_348,&g_348},{&g_348,&l_520,&g_348,(void*)0,(void*)0,&l_520,&g_348,&l_520},{&g_348,&l_520,&l_520,&g_348,&g_348,&g_295,(void*)0,&l_520},{&g_348,(void*)0,&l_520,&g_348,(void*)0,(void*)0,&g_348,(void*)0},{&g_348,&g_348,(void*)0,&g_295,&g_348,(void*)0,&g_295,&g_295},{&l_520,&g_348,&l_520,&g_295,(void*)0,&g_348,(void*)0,&l_520},{&g_295,&g_348,&g_295,&g_295,&g_348,&g_348,&l_520,(void*)0},{&g_295,(void*)0,&g_295,&g_295,&g_295,&g_295,&g_295,&g_295}},{{&g_348,&g_295,(void*)0,(void*)0,&l_520,&g_295,&g_348,&g_295},{&g_295,&g_295,&g_348,&g_348,&g_348,&g_295,&g_348,(void*)0},{(void*)0,&g_295,&l_520,&g_295,&g_295,(void*)0,&l_520,&l_520},{(void*)0,&g_295,&g_295,(void*)0,(void*)0,&g_295,&g_348,&l_520},{&l_520,&g_348,&g_348,&g_348,&g_348,&g_348,&g_348,&l_520},{&l_520,&g_295,&g_295,&l_520,&l_520,&l_520,(void*)0,&g_295},{&g_348,(void*)0,(void*)0,&g_348,&l_520,&l_520,(void*)0,&g_295},{&l_520,&g_295,&g_348,&g_348,(void*)0,&g_348,(void*)0,&g_348}},{{&g_295,&g_348,(void*)0,(void*)0,&g_295,&g_295,&g_295,&g_295},{&g_295,&g_295,&l_520,&g_348,&l_520,(void*)0,(void*)0,(void*)0},{(void*)0,&g_295,&g_295,&g_348,(void*)0,&g_295,&g_295,&l_520},{&g_348,&g_295,&g_295,&g_295,&l_520,&g_348,(void*)0,&g_295},{(void*)0,&g_348,(void*)0,(void*)0,&g_348,&g_348,&g_348,&g_348},{(void*)0,&g_348,&l_520,&l_520,&g_295,&g_348,&g_295,&l_520},{&l_520,(void*)0,&l_520,&g_295,&g_348,&g_348,&g_348,&l_520},{&g_295,&g_348,&g_295,(void*)0,&g_295,(void*)0,&g_295,(void*)0}}};
            float ***l_551 = &g_529;
            int i, j, k;
            (*g_448) = p_36;
            l_457 = ((!(((*l_272) = (safe_rshift_func_uint16_t_u_u(((*l_454) = l_453[7]), g_143[0]))) , p_35)) | 0x2007L);
            if ((safe_mod_func_int32_t_s_s((*p_36), ((safe_add_func_int8_t_s_s(g_8, 0xC6L)) & (((safe_div_func_uint8_t_u_u(((l_469 |= (l_468 ^= (p_35 < (safe_mul_func_int8_t_s_s((((*l_352) ^= ((l_457 | g_157[1][1][9]) || p_35)) , (((safe_add_func_uint32_t_u_u((g_158 , ((((g_304 , 0x4E76L) , p_35) , 0xFAL) < 252UL)), l_453[1])) && p_35) && g_88)), 0xA2L))))) , 248UL), g_38[1][0])) , &g_295) == l_470)))))
            { /* block id: 216 */
                uint16_t l_486 = 1UL;
                int32_t l_494 = 1L;
                int32_t l_498 = 0x00B03BCFL;
                int64_t *l_515 = &g_295;
                uint32_t l_521 = 0x7B15FAC7L;
                float **l_526 = &l_446;
                g_143[0] ^= (**g_448);
                for (l_277 = 17; (l_277 == (-30)); l_277--)
                { /* block id: 220 */
                    int32_t l_487[1];
                    float *l_491 = (void*)0;
                    int32_t l_493 = 9L;
                    int8_t *l_516[1];
                    int16_t *l_518 = &g_114;
                    int16_t *l_519 = &g_112;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_487[i] = (-5L);
                    for (i = 0; i < 1; i++)
                        l_516[i] = (void*)0;
                    l_493 &= (((((safe_div_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u((safe_mul_func_uint16_t_u_u((((~(safe_unary_minus_func_int32_t_s((*p_36)))) <= (safe_mul_func_int16_t_s_s((((*l_352) = ((((safe_rshift_func_int8_t_s_s((~l_486), 1)) & ((4294967295UL & (((&g_455 != (g_488[3] = (l_487[0] , &g_455))) > (p_35 , (safe_mul_func_int16_t_s_s((((void*)0 != l_491) == g_110), p_35)))) & (*p_36))) && p_35)) , 0x74A6L) ^ p_35)) == p_35), 8UL))) > p_35), g_492)), 3)) <= g_329), 0xB9L)) , g_157[3][3][0]) != (-7L)) , p_35) <= g_38[1][2]);
                    if (((*l_51) = (4UL <= (18446744073709551610UL | 0x5A0B3D02AC45A2CFLL))))
                    { /* block id: 225 */
                        (*l_51) &= (-8L);
                        (*l_364) = (*g_60);
                    }
                    else
                    { /* block id: 228 */
                        float l_495 = (-0x5.5p-1);
                        int32_t l_496 = 0x1512072AL;
                        ++l_499[0][0];
                        l_494 = (p_36 == (void*)0);
                    }
                    if ((safe_div_func_int32_t_s_s(((l_469 &= (safe_mul_func_int8_t_s_s(((safe_mod_func_int32_t_s_s((-1L), (safe_mod_func_int32_t_s_s(((((*l_519) = ((*l_518) = (((((safe_unary_minus_func_int8_t_s(p_35)) , l_486) != (((*l_454) = ((((+(((safe_sub_func_int16_t_s_s(((*l_352) = g_329), ((((p_35 && l_453[7]) > ((l_515 = l_514) == (void*)0)) & (g_158 = ((((0x5F64B87A51C239A2LL != p_35) == p_35) && (*g_406)) > g_157[2][1][8]))) | 0xD0D0FC8EL))) , l_497) && p_35)) || 4294967290UL) || g_8) && l_517)) ^ 3UL)) < l_494) == 0x32L))) && 0xAFF2L) >= (*p_36)), (*g_449))))) || g_90[0][5]), 251UL))) != l_520), p_35)))
                    { /* block id: 239 */
                        int64_t **l_523 = &l_470;
                        if ((*g_449))
                            break;
                        (*l_51) |= (l_521 , (((*l_523) = l_522[0][3][5]) == (void*)0));
                    }
                    else
                    { /* block id: 243 */
                        uint16_t **l_525 = (void*)0;
                        uint16_t ***l_524 = &l_525;
                        uint64_t *l_552 = &g_88;
                        (*l_524) = &l_454;
                        (*g_528) = l_526;
                        (*l_51) = ((safe_mul_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_u(0UL, 11)) , (safe_add_func_uint8_t_u_u(((safe_add_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(p_35, ((*l_519) = ((*l_518) = (safe_div_func_uint16_t_u_u(((g_304 = (p_35 , (safe_div_func_uint8_t_u_u((((void*)0 == &l_518) , ((p_35 , 0xE633B7DFEC0DF3DELL) ^ ((*l_552) = (l_497 >= ((safe_lshift_func_int8_t_s_u((safe_lshift_func_uint16_t_u_u(((l_551 = &g_529) != &l_526), p_35)), p_35)) >= p_35))))), (*l_51))))) != l_487[0]), p_35)))))), p_35)), g_386[3])) && 18446744073709551615UL), p_35))), g_143[0])) >= g_157[1][3][8]);
                    }
                    for (l_468 = 23; (l_468 < (-17)); l_468--)
                    { /* block id: 255 */
                        uint64_t *l_555 = &g_304;
                        int32_t l_573 = 7L;
                        if (l_487[0])
                            break;
                        (*l_51) |= (((*l_518) = (((*l_272) = ((l_493 , ((*l_555) = 18446744073709551614UL)) && (((l_573 = (((safe_add_func_uint16_t_u_u((((safe_lshift_func_uint16_t_u_s(p_35, 5)) | (safe_add_func_int64_t_s_s((!(((l_563 == ((g_90[1][1] |= l_468) , (*l_551))) , (safe_mul_func_uint16_t_u_u(p_35, (safe_add_func_int16_t_s_s(0x6CE0L, ((safe_mul_func_float_f_f((((safe_mul_func_float_f_f(p_35, p_35)) != p_35) < l_572), (-0x1.Bp+1))) , 0x160BL)))))) & 0x24L)), (*g_406)))) < 0x441C06CDL), (-10L))) <= l_573) | 0xA867D6B0L)) == p_35) && p_35))) >= g_114)) & p_35);
                        (*g_574) = (*g_448);
                    }
                }
            }
            else
            { /* block id: 266 */
                int32_t *l_582[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_582[i] = &l_276;
                for (g_304 = 29; (g_304 < 46); g_304 = safe_add_func_uint32_t_u_u(g_304, 4))
                { /* block id: 269 */
                    --l_577;
                    (*l_51) = (safe_rshift_func_int8_t_s_u((g_157[1][1][9] != 1L), 4));
                    for (g_112 = 3; (g_112 >= 0); g_112 -= 1)
                    { /* block id: 274 */
                        int i, j;
                        if (g_38[g_112][g_112])
                            break;
                    }
                }
                (*l_364) = &l_65;
                l_582[1] = (*l_364);
                (*l_364) = &l_65;
            }
            l_469 = ((*l_51) = g_583);
        }
        else
        { /* block id: 284 */
            int16_t l_604 = 0L;
            const int32_t l_605 = 0x44ACA6DDL;
            int32_t *l_612 = &l_65;
            int32_t l_633 = 0x4749A87DL;
            int32_t l_634 = (-2L);
            if ((&g_529 != (g_583 , &g_529)))
            { /* block id: 285 */
                int16_t *l_593 = (void*)0;
                int32_t l_601[7] = {0x4235446FL,0x4235446FL,1L,0x4235446FL,0x4235446FL,1L,0x4235446FL};
                uint16_t *l_602 = &g_455;
                int8_t *l_603 = (void*)0;
                uint16_t ***l_607[9][4] = {{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606},{&l_606,&l_606,&l_606,&l_606}};
                int i, j;
                l_278 |= (safe_add_func_uint32_t_u_u((safe_div_func_int32_t_s_s((!(*p_36)), ((safe_lshift_func_uint8_t_u_s(0xDEL, ((safe_sub_func_int64_t_s_s(((void*)0 != l_593), (p_35 || ((safe_sub_func_int64_t_s_s(((l_604 &= (safe_sub_func_uint8_t_u_u(p_35, (+(safe_lshift_func_int16_t_s_s(((((*l_602) = l_601[2]) , &g_386[1]) != (void*)0), 1)))))) <= p_35), g_114)) , p_35)))) != l_605))) , g_38[3][1]))), (*p_36)));
                (*l_364) = &l_601[1];
                l_606 = l_606;
                for (l_278 = 7; (l_278 == 15); l_278 = safe_add_func_uint32_t_u_u(l_278, 4))
                { /* block id: 293 */
                    int32_t *l_611 = &l_601[5];
                    int32_t l_614 = 0L;
                    int32_t l_615[9] = {(-1L),0x9D5C9149L,(-1L),0x9D5C9149L,(-1L),0x9D5C9149L,(-1L),0x9D5C9149L,(-1L)};
                    int i;
                    if (l_610)
                    { /* block id: 294 */
                        int32_t *l_613 = &g_583;
                        return l_613;
                    }
                    else
                    { /* block id: 296 */
                        uint64_t l_616[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_616[i] = 0x4E5235E1BA5065ECLL;
                        l_616[1]++;
                        (*l_612) &= ((*l_51) = (((((void*)0 == &g_492) || ((p_35 , (~p_35)) && ((safe_div_func_int16_t_s_s((l_616[0] & ((l_616[1] && (((*l_272) = (*l_51)) & 0x8BL)) >= (safe_lshift_func_int16_t_s_u((safe_lshift_func_int16_t_s_u(((safe_rshift_func_uint16_t_u_u(((0x38ABL & 65531UL) , l_616[0]), (*l_51))) && l_601[2]), p_35)), 11)))), p_35)) == l_601[6]))) == 1L) != (**g_574)));
                    }
                    l_51 = ((*l_364) = (*l_364));
                }
            }
            else
            { /* block id: 305 */
                int32_t ***l_632 = &l_364;
                int32_t l_635 = 0x64217492L;
                for (l_65 = 0; (l_65 >= 0); l_65 -= 1)
                { /* block id: 308 */
                    int32_t ****l_630 = &g_628;
                    int32_t ****l_631[6];
                    uint32_t l_636 = 0x30B1FD9EL;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_631[i] = (void*)0;
                    l_632 = ((*l_630) = g_628);
                    ++l_636;
                    for (l_276 = 0; (l_276 <= 0); l_276 += 1)
                    { /* block id: 314 */
                        return (**l_632);
                    }
                }
            }
            (***g_528) = (p_35 , p_35);
        }
    }
    else
    { /* block id: 321 */
        int64_t l_642[9] = {6L,6L,6L,6L,6L,6L,6L,6L,6L};
        uint8_t *l_645 = &l_499[0][0];
        uint16_t ***l_660 = (void*)0;
        uint16_t ****l_661 = &l_660;
        int32_t l_663 = 0xBABE103CL;
        uint8_t l_665 = 3UL;
        int32_t l_727 = 1L;
        uint32_t l_775 = 0xFFF1AA90L;
        int32_t ****l_802 = &g_628;
        int32_t l_845 = 0xD9683D92L;
        uint16_t l_867 = 65527UL;
        const int16_t *l_871 = &g_114;
        const int16_t **l_870 = &l_871;
        const int16_t ***l_869 = &l_870;
        uint32_t l_912 = 3UL;
        const int16_t *l_927 = &g_928;
        uint8_t l_965[4][2] = {{0xBDL,0x92L},{0xBDL,0x92L},{0xBDL,0x92L},{0xBDL,0x92L}};
        uint32_t *l_985 = &l_912;
        uint16_t *l_986 = &l_697;
        int8_t *l_987 = &g_90[4][4];
        const int32_t **l_988 = &g_449;
        int i, j;
        if (((*l_51) = (((safe_sub_func_uint16_t_u_u((~((*l_645) &= (l_642[5] , (safe_lshift_func_uint8_t_u_u(250UL, 2))))), (((g_583 ^= (+(**g_448))) < ((!(((safe_mul_func_int16_t_s_s(((l_663 = (safe_div_func_int16_t_s_s((safe_mod_func_int64_t_s_s((((((*g_406) = p_35) >= ((p_35 || (0xC7C0082D24F9C9BBLL ^ (safe_rshift_func_int16_t_s_u(((((((g_88 ^= (g_656[5] != ((*l_661) = l_660))) , g_455) == (-1L)) , g_143[0]) >= g_158) <= l_662), 12)))) != p_35)) > l_642[5]) ^ (-10L)), (-10L))), 0x602AL))) <= l_664), p_35)) == 4L) , 0xD50591CA6162FA01LL)) && 4294967295UL)) ^ p_35))) , p_35) < l_665)))
        { /* block id: 329 */
            uint8_t l_682 = 0x39L;
            uint16_t *l_690 = (void*)0;
            uint16_t *l_691[7][10] = {{&g_455,&g_455,&g_455,&g_492,&g_492,&g_455,&g_492,&g_492,&g_492,&g_492},{&g_455,&g_492,&g_455,&g_492,&g_492,&g_455,&g_492,&g_455,&g_455,&g_492},{&g_492,&g_492,&g_492,&g_492,&g_492,&g_492,&g_492,&g_492,&g_492,&g_492},{&g_455,&g_492,&g_492,&g_492,&g_492,&g_455,&g_492,&g_492,&g_455,&g_492},{&g_455,&g_455,&g_455,&g_492,&g_492,&g_492,&g_492,&g_492,&g_492,&g_492},{&g_492,&g_492,&g_492,&g_492,&g_492,&g_455,&g_492,&g_492,&g_492,&g_492},{&g_455,&g_492,&g_492,&g_455,&g_492,&g_455,&g_492,&g_455,&g_492,&g_492}};
            int32_t l_692 = 0xE6536799L;
            int32_t **l_704 = &g_37;
            int64_t **l_730 = &l_470;
            int16_t l_916 = 0xB963L;
            int16_t **l_941 = &l_352;
            int32_t l_964 = (-1L);
            int i, j;
            (*g_530) = ((safe_div_func_float_f_f((safe_add_func_float_f_f((0x9.69F644p-92 == (0x1.Cp-1 == p_35)), ((((l_352 != ((0x5E8BL || ((safe_rshift_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u((((g_455 && (p_35 == (safe_sub_func_int64_t_s_s(0xC1A574A75B3B6C23LL, ((safe_add_func_int16_t_s_s(((((safe_mul_func_uint16_t_u_u(0xA7DFL, g_157[1][1][9])) == l_642[5]) ^ p_35) , 0x7F4FL), g_386[1])) && g_8))))) < (*l_51)) || 0L), 0)), 7)) > (*g_406))) , l_352)) >= 0xD.3AE7B5p-75) == 0xB.C02B33p-74) == l_665))), l_642[5])) == (***g_528));
        }
        else
        { /* block id: 437 */
            return (*g_629);
        }
        (*l_988) = (((*l_987) = (g_8 , (((((*l_986) &= (~(((p_35 || (safe_lshift_func_int8_t_s_s((p_35 || (((*l_985) |= (safe_div_func_uint8_t_u_u((safe_rshift_func_int8_t_s_u((g_8 , (((**g_657) <= (safe_sub_func_uint64_t_u_u((p_35 != (3UL < 65533UL)), (safe_mul_func_uint16_t_u_u((safe_sub_func_uint16_t_u_u((safe_mod_func_int64_t_s_s(((void*)0 == &g_348), 0x5C6349476C4CA774LL)), l_642[6])), g_455))))) != g_143[0])), p_35)), 255UL))) , (*g_406))), p_35))) || p_35) , p_35))) <= 0L) , p_35) & p_35))) , (*g_448));
    }
    for (g_94 = (-5); (g_94 > 12); g_94 = safe_add_func_uint32_t_u_u(g_94, 2))
    { /* block id: 447 */
        uint16_t l_991[6];
        int32_t l_997 = 8L;
        int32_t l_998 = 0L;
        int32_t l_1000 = 0x9A45C245L;
        int32_t l_1003 = 2L;
        int32_t l_1004 = 0L;
        int32_t l_1005[10][8][3] = {{{0x336608EEL,0xFEA0E2DFL,0x31C821AFL},{0xDD13D998L,0x7395C5BBL,(-10L)},{0xA35BD3F0L,0x8ECA9908L,(-1L)},{(-7L),0L,0xDD13D998L},{0xDD13D998L,0xD00F67F8L,0x2F08BB94L},{(-2L),(-2L),1L},{0x4EA94046L,0xB1ECE3B3L,(-2L)},{(-10L),0x3725D039L,0x9E7C53DBL}},{{(-1L),0L,0xD00F67F8L},{0L,(-10L),0x9E7C53DBL},{0L,1L,(-2L)},{0x8ECA9908L,(-1L),1L},{0x197106B5L,0xB1AF86CBL,0x2F08BB94L},{0x3DBD3F35L,(-4L),0xDD13D998L},{0x3725D039L,1L,0xD79F51CBL},{0xB1AF86CBL,0xD6FE1012L,0xFAC3E3E4L}},{{1L,0x8656270DL,2L},{0xA7DE1A56L,0L,0x9993BC4DL},{1L,9L,0x31C821AFL},{(-1L),0x3DBD3F35L,0x0098253FL},{0xE7029ED5L,(-1L),0x7395C5BBL},{(-3L),(-1L),0x1154443CL},{0xA35BD3F0L,0x3DBD3F35L,0xD6FE1012L},{0x56BC93DCL,9L,(-10L)}},{{8L,0L,(-1L)},{0xF0608DC9L,0x8656270DL,(-1L)},{0xD79F51CBL,0xD6FE1012L,0L},{0x1154443CL,1L,(-1L)},{(-10L),(-4L),0xA35BD3F0L},{(-4L),0xB1AF86CBL,(-4L)},{(-1L),(-1L),0x690F4F09L},{2L,1L,1L}},{{0x6FC68A51L,(-10L),0xE6495920L},{0x5CAFE7C7L,0L,(-2L)},{0x6FC68A51L,0x3725D039L,0xB1AF86CBL},{2L,0xB1ECE3B3L,(-1L)},{(-1L),(-2L),0x422789D5L},{(-4L),0xD00F67F8L,0xFEA0E2DFL},{(-10L),0L,0xA7DE1A56L},{0x1154443CL,(-1L),(-10L)}},{{0xD79F51CBL,0xA35BD3F0L,0x23906738L},{0xF0608DC9L,0x23906738L,(-2L)},{8L,(-1L),0x6FC68A51L},{0x56BC93DCL,0x880665A0L,1L},{0xA35BD3F0L,0x6FC68A51L,0x5C3CFE35L},{(-3L),0xD79F51CBL,0x5C3CFE35L},{0xE7029ED5L,(-1L),1L},{(-1L),0xE6495920L,0x6FC68A51L}},{{1L,0x690F4F09L,(-2L)},{0xA7DE1A56L,(-5L),0x23906738L},{1L,0x56BC93DCL,(-10L)},{0xB1AF86CBL,0x9E7C53DBL,0xA7DE1A56L},{0x3725D039L,0x197106B5L,0xFEA0E2DFL},{0x3DBD3F35L,0x422789D5L,0x422789D5L},{0x197106B5L,(-7L),(-1L)},{0x8ECA9908L,0xD63C1FCAL,0xB1AF86CBL}},{{0L,4L,(-2L)},{0L,0xFEA0E2DFL,0xE6495920L},{(-1L),4L,1L},{(-10L),0xD63C1FCAL,0x690F4F09L},{0x4EA94046L,(-7L),(-4L)},{(-2L),0x422789D5L,0xA35BD3F0L},{0xDD13D998L,0x197106B5L,(-1L)},{0xB1ECE3B3L,0x9E7C53DBL,0L}},{{(-1L),0x56BC93DCL,(-1L)},{1L,(-5L),(-1L)},{0x422789D5L,0x690F4F09L,(-10L)},{(-10L),(-5L),0x56BC93DCL},{0x9993BC4DL,0xD6FE1012L,0x5CAFE7C7L},{(-10L),0xD00F67F8L,0xA35BD3F0L},{(-10L),0xDD13D998L,(-10L)},{0x9993BC4DL,0L,2L}},{{(-10L),0x9993BC4DL,5L},{1L,4L,(-1L)},{9L,(-1L),(-1L)},{0xD79F51CBL,0L,0xD00F67F8L},{0x0098253FL,0x7395C5BBL,1L},{1L,(-1L),1L},{0L,0L,0x690F4F09L},{0x1154443CL,0x0098253FL,(-2L)}}};
        uint32_t l_1008 = 1UL;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_991[i] = 0xB185L;
        ++l_991[5];
        for (g_110 = 0; (g_110 < (-28)); g_110 = safe_sub_func_int16_t_s_s(g_110, 5))
        { /* block id: 451 */
            float l_996[1];
            int32_t l_999 = 0xE7CCB0D0L;
            int32_t l_1001 = 1L;
            int32_t l_1006[5][1];
            int32_t l_1007 = 0x9B53E797L;
            int i, j;
            for (i = 0; i < 1; i++)
                l_996[i] = 0x1.Fp+1;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1006[i][j] = 0xD7ECBE1CL;
            }
            l_1008--;
        }
    }
    return l_1012;
}


/* ------------------------------------------ */
/* 
 * reads : g_96 g_37
 * writes:
 */
static int32_t * const  func_41(int32_t * p_42, int32_t  p_43, uint32_t  p_44, int32_t * p_45)
{ /* block id: 10 */
    uint16_t l_74 = 0xC0CBL;
    int32_t l_134[5][8][6] = {{{(-7L),0L,0x6DE8FF6DL,0x0A1BDD19L,0x5124C238L,0x99D7E73FL},{(-1L),0xAFF4B716L,0L,0x99D7E73FL,(-1L),(-1L)},{0xCF73297CL,0xAFF4B716L,0xE1C9646AL,0x3949AADFL,0x5124C238L,0x3949AADFL},{(-3L),0L,(-3L),0xAFF4B716L,1L,0L},{0x052CD35FL,(-1L),0xE1C9646AL,0x0A1BDD19L,(-7L),(-1L)},{1L,0x221D915BL,0L,0x0A1BDD19L,0xCFD8255CL,0xAFF4B716L},{0x052CD35FL,0xAFF4B716L,0x6DE8FF6DL,0xAFF4B716L,0x052CD35FL,(-1L)},{(-3L),0x99D7E73FL,1L,0x3949AADFL,0xCFD8255CL,0L}},{{0xCF73297CL,0L,(-1L),0x99D7E73FL,(-7L),0L},{(-1L),(-1L),1L,0x0A1BDD19L,1L,(-1L)},{(-7L),0x221D915BL,0x6DE8FF6DL,0x49983F17L,0x5124C238L,0xAFF4B716L},{(-1L),0x99D7E73FL,0L,0xAFF4B716L,(-1L),(-1L)},{0xCF73297CL,0x99D7E73FL,0xE1C9646AL,0L,0x5124C238L,0L},{(-3L),0x221D915BL,(-3L),0x99D7E73FL,1L,0x3949AADFL},{0x052CD35FL,(-1L),0xE1C9646AL,0x49983F17L,(-7L),(-1L)},{1L,0L,0L,0x49983F17L,0xCFD8255CL,0x99D7E73FL}},{{0x052CD35FL,0x99D7E73FL,0x6DE8FF6DL,0x99D7E73FL,0x052CD35FL,(-1L)},{(-3L),0xAFF4B716L,1L,0L,0xCFD8255CL,0x3949AADFL},{0xCF73297CL,0x221D915BL,(-1L),0xAFF4B716L,(-7L),0x3949AADFL},{(-1L),(-1L),1L,0x49983F17L,1L,(-1L)},{(-7L),0L,0x6DE8FF6DL,0x0A1BDD19L,0x5124C238L,0x99D7E73FL},{(-1L),0xAFF4B716L,0L,0x99D7E73FL,(-1L),(-1L)},{0xCF73297CL,0xAFF4B716L,0xE1C9646AL,0x3949AADFL,0x5124C238L,0x3949AADFL},{(-3L),0L,(-3L),0xAFF4B716L,1L,0L}},{{0x052CD35FL,(-1L),0xE1C9646AL,0x0A1BDD19L,(-7L),(-1L)},{1L,(-1L),0xCFD8255CL,0L,(-1L),0x3949AADFL},{(-7L),0x3949AADFL,(-4L),0x3949AADFL,(-7L),0xAFF4B716L},{0L,0L,(-3L),0x49983F17L,(-1L),0x0A1BDD19L},{(-1L),(-1L),0x6DE8FF6DL,0L,0xCF73297CL,0x0A1BDD19L},{1L,0xAFF4B716L,(-3L),0L,(-3L),0xAFF4B716L},{0xCF73297CL,(-1L),(-4L),0x221D915BL,0x052CD35FL,0x3949AADFL},{1L,0L,0xCFD8255CL,0x3949AADFL,1L,0x99D7E73FL}},{{(-1L),0L,(-1L),0x0A1BDD19L,0x052CD35FL,0x0A1BDD19L},{0L,(-1L),0L,0L,(-3L),0x49983F17L},{(-7L),0xAFF4B716L,(-1L),0x221D915BL,0xCF73297CL,0xAFF4B716L},{(-3L),(-1L),0xCFD8255CL,0x221D915BL,(-1L),0L},{(-7L),0L,(-4L),0L,(-7L),0x99D7E73FL},{0L,0x3949AADFL,(-3L),0x0A1BDD19L,(-1L),0x49983F17L},{(-1L),(-1L),0x6DE8FF6DL,0x3949AADFL,0xCF73297CL,0x49983F17L},{1L,0x99D7E73FL,(-3L),0x221D915BL,(-3L),0x99D7E73FL}}};
    uint32_t l_150 = 0xB88E3999L;
    int16_t l_189 = 0x1FE2L;
    int i, j, k;
    for (p_44 = 0; (p_44 >= 43); ++p_44)
    { /* block id: 13 */
        int32_t l_68 = (-2L);
        int32_t l_91 = 0xF0491844L;
        const uint64_t *l_99 = &g_88;
        float l_108 = 0x1.Ap-1;
        int32_t * const l_116[6][2] = {{(void*)0,&g_38[0][3]},{(void*)0,(void*)0},{&g_38[0][3],(void*)0},{(void*)0,&g_38[0][3]},{(void*)0,(void*)0},{&g_38[0][3],(void*)0}};
        int16_t *l_135 = &g_112;
        uint32_t l_159[6][2] = {{18446744073709551613UL,18446744073709551613UL},{18446744073709551613UL,18446744073709551613UL},{18446744073709551613UL,18446744073709551613UL},{18446744073709551613UL,18446744073709551613UL},{18446744073709551613UL,18446744073709551613UL},{18446744073709551613UL,18446744073709551613UL}};
        uint64_t l_181 = 0x4EEC173A89DF1001LL;
        int32_t l_249 = 0x13DB608AL;
        int i, j;
        if (l_68)
            break;
    }
    return (*g_96);
}


/* ------------------------------------------ */
/* 
 * reads : g_60
 * writes: g_37
 */
static uint32_t  func_46(int32_t * p_47, int16_t  p_48, int32_t  p_49, uint32_t  p_50)
{ /* block id: 5 */
    int32_t **l_61 = &g_37;
    int32_t *l_63 = (void*)0;
    int32_t **l_62 = &l_63;
    uint64_t l_64[5];
    int i;
    for (i = 0; i < 5; i++)
        l_64[i] = 0x7BE627823D12EF61LL;
    (*g_60) = &g_38[1][0];
    (*l_62) = ((((*l_61) = (void*)0) == &g_38[1][0]) , p_47);
    return l_64[0];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_22, "g_22", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_38[i][j], "g_38[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_88, "g_88", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_90[i][j], "g_90[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_94, "g_94", print_hash_value);
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    transparent_crc(g_133, "g_133", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_143[i], "g_143[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_157[i][j][k], "g_157[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_158, "g_158", print_hash_value);
    transparent_crc_bytes (&g_224, sizeof(g_224), "g_224", print_hash_value);
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc(g_304, "g_304", print_hash_value);
    transparent_crc_bytes (&g_324, sizeof(g_324), "g_324", print_hash_value);
    transparent_crc(g_329, "g_329", print_hash_value);
    transparent_crc(g_348, "g_348", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_386[i], "g_386[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_392, "g_392", print_hash_value);
    transparent_crc(g_455, "g_455", print_hash_value);
    transparent_crc(g_492, "g_492", print_hash_value);
    transparent_crc(g_583, "g_583", print_hash_value);
    transparent_crc(g_659, "g_659", print_hash_value);
    transparent_crc(g_722, "g_722", print_hash_value);
    transparent_crc(g_784, "g_784", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc_bytes(&g_804[i], sizeof(g_804[i]), "g_804[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_928, "g_928", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_944[i][j][k], "g_944[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1078, "g_1078", print_hash_value);
    transparent_crc(g_1122, "g_1122", print_hash_value);
    transparent_crc_bytes (&g_1161, sizeof(g_1161), "g_1161", print_hash_value);
    transparent_crc(g_1221, "g_1221", print_hash_value);
    transparent_crc(g_1342, "g_1342", print_hash_value);
    transparent_crc(g_1409, "g_1409", print_hash_value);
    transparent_crc(g_1491, "g_1491", print_hash_value);
    transparent_crc_bytes (&g_1585, sizeof(g_1585), "g_1585", print_hash_value);
    transparent_crc(g_1604, "g_1604", print_hash_value);
    transparent_crc(g_1605, "g_1605", print_hash_value);
    transparent_crc(g_1625, "g_1625", print_hash_value);
    transparent_crc(g_1658, "g_1658", print_hash_value);
    transparent_crc(g_1660, "g_1660", print_hash_value);
    transparent_crc(g_1717, "g_1717", print_hash_value);
    transparent_crc(g_1730, "g_1730", print_hash_value);
    transparent_crc(g_1742, "g_1742", print_hash_value);
    transparent_crc(g_1753, "g_1753", print_hash_value);
    transparent_crc(g_1877, "g_1877", print_hash_value);
    transparent_crc(g_2096, "g_2096", print_hash_value);
    transparent_crc_bytes (&g_2541, sizeof(g_2541), "g_2541", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc_bytes(&g_2553[i], sizeof(g_2553[i]), "g_2553[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc_bytes (&g_2605, sizeof(g_2605), "g_2605", print_hash_value);
    transparent_crc(g_2691, "g_2691", print_hash_value);
    transparent_crc(g_2716, "g_2716", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 704
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 46
breakdown:
   depth: 1, occurrence: 276
   depth: 2, occurrence: 65
   depth: 3, occurrence: 8
   depth: 4, occurrence: 5
   depth: 6, occurrence: 3
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 4
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 23, occurrence: 4
   depth: 24, occurrence: 4
   depth: 25, occurrence: 4
   depth: 26, occurrence: 1
   depth: 27, occurrence: 7
   depth: 28, occurrence: 2
   depth: 29, occurrence: 3
   depth: 30, occurrence: 4
   depth: 31, occurrence: 1
   depth: 32, occurrence: 2
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 38, occurrence: 2
   depth: 46, occurrence: 2

XXX total number of pointers: 557

XXX times a variable address is taken: 1369
XXX times a pointer is dereferenced on RHS: 385
breakdown:
   depth: 1, occurrence: 244
   depth: 2, occurrence: 106
   depth: 3, occurrence: 30
   depth: 4, occurrence: 5
XXX times a pointer is dereferenced on LHS: 382
breakdown:
   depth: 1, occurrence: 314
   depth: 2, occurrence: 35
   depth: 3, occurrence: 24
   depth: 4, occurrence: 9
XXX times a pointer is compared with null: 59
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 12
XXX times a pointer is qualified to be dereferenced: 8757

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1363
   level: 2, occurrence: 497
   level: 3, occurrence: 301
   level: 4, occurrence: 201
   level: 5, occurrence: 7
XXX number of pointers point to pointers: 260
XXX number of pointers point to scalars: 297
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.4
XXX average alias set size: 1.44

XXX times a non-volatile is read: 2446
XXX times a non-volatile is write: 1231
XXX times a volatile is read: 129
XXX    times read thru a pointer: 55
XXX times a volatile is write: 34
XXX    times written thru a pointer: 7
XXX times a volatile is available for access: 1.5e+03
XXX percentage of non-volatile access: 95.8

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 274
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 27
   depth: 1, occurrence: 30
   depth: 2, occurrence: 29
   depth: 3, occurrence: 41
   depth: 4, occurrence: 64
   depth: 5, occurrence: 83

XXX percentage a fresh-made variable is used: 16.1
XXX percentage an existing variable is used: 83.9
********************* end of statistics **********************/


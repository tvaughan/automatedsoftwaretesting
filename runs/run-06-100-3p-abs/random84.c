/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      2182592419
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   const int32_t  f0;
   const float  f1;
   volatile uint32_t  f2;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static const volatile int32_t g_3 = 0xF9E923E6L;/* VOLATILE GLOBAL g_3 */
static int32_t g_10 = 0x9B389519L;
static uint8_t g_35 = 0xD5L;
static uint32_t g_37[1][10] = {{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}};
static uint16_t g_46 = 0UL;
static int32_t *g_88 = (void*)0;
static int32_t **g_87[2][5] = {{&g_88,&g_88,&g_88,&g_88,&g_88},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static int32_t g_90 = 0x5D6903F8L;
static int8_t g_114[4] = {(-1L),(-1L),(-1L),(-1L)};
static uint16_t *g_150 = &g_46;
static uint16_t **g_149 = &g_150;
static uint32_t g_166[3][7][1] = {{{0x0EC93635L},{0x59FB421CL},{0x59FB421CL},{0x0EC93635L},{0xE38D5F57L},{0x184ED857L},{0x0EC93635L}},{{0x184ED857L},{0xE38D5F57L},{0x0EC93635L},{0x59FB421CL},{0x59FB421CL},{0x0EC93635L},{0xE38D5F57L}},{{0x184ED857L},{0x0EC93635L},{0x184ED857L},{0xE38D5F57L},{0x0EC93635L},{0x59FB421CL},{0x59FB421CL}}};
static int64_t g_171 = 0xBA2CA0061E2E1E1BLL;
static uint8_t g_173 = 3UL;
static uint64_t g_183 = 0UL;
static uint32_t g_235 = 0x5E8D5C61L;
static const uint16_t g_253 = 0xA813L;
static int16_t g_255[3][4] = {{2L,2L,0x904BL,0x904BL},{2L,2L,2L,2L},{2L,0x904BL,0x904BL,2L}};
static uint16_t **g_288 = &g_150;
static int32_t g_296 = 0L;
static int32_t g_298 = 4L;
static int16_t g_299 = 1L;
static uint64_t g_302[3] = {0x88ED9E33FC4FF34FLL,0x88ED9E33FC4FF34FLL,0x88ED9E33FC4FF34FLL};
static volatile int16_t g_324 = 0x475BL;/* VOLATILE GLOBAL g_324 */
static volatile int16_t * volatile g_323 = &g_324;/* VOLATILE GLOBAL g_323 */
static volatile int16_t * volatile *g_322 = &g_323;
static int32_t *g_412 = &g_296;
static float g_482 = 0xB.1C5467p+82;
static float * volatile g_481 = &g_482;/* VOLATILE GLOBAL g_481 */
static float * const g_483[8][2] = {{&g_482,&g_482},{&g_482,&g_482},{&g_482,&g_482},{&g_482,&g_482},{&g_482,&g_482},{&g_482,&g_482},{&g_482,&g_482},{&g_482,&g_482}};
static float * const  volatile *g_480[3][4][10] = {{{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481}},{{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481}},{{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481},{&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481,&g_481}}};
static int32_t g_500 = 0x9BE09076L;
static struct S0 g_544 = {0xB44ADA35L,0xE.FB98FEp-48,0xECCADC91L};/* VOLATILE GLOBAL g_544 */
static struct S0 *g_543 = &g_544;
static struct S0 g_546 = {-1L,0x9.85ACABp-6,0x52988262L};/* VOLATILE GLOBAL g_546 */
static int16_t *g_569[3][2][9] = {{{&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299},{&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299}},{{&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299},{&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299}},{{&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299},{&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299,&g_299}}};
static int16_t **g_568 = &g_569[1][0][3];
static int16_t ** const *g_567[6][3][1] = {{{&g_568},{(void*)0},{&g_568}},{{(void*)0},{&g_568},{(void*)0}},{{&g_568},{(void*)0},{&g_568}},{{(void*)0},{&g_568},{(void*)0}},{{&g_568},{(void*)0},{&g_568}},{{(void*)0},{&g_568},{(void*)0}}};
static int32_t ** volatile g_634 = (void*)0;/* VOLATILE GLOBAL g_634 */
static int32_t ** volatile g_635[7] = {&g_88,&g_88,&g_412,&g_88,&g_88,&g_412,&g_88};
static int32_t ** volatile g_636 = &g_88;/* VOLATILE GLOBAL g_636 */
static uint8_t * volatile g_645 = &g_35;/* VOLATILE GLOBAL g_645 */
static uint8_t * volatile * volatile g_644 = &g_645;/* VOLATILE GLOBAL g_644 */
static const volatile struct S0 g_658 = {0x1DAE8BB5L,0x2.10A451p+64,0x188C7A71L};/* VOLATILE GLOBAL g_658 */
static int32_t * volatile g_670 = &g_500;/* VOLATILE GLOBAL g_670 */
static volatile uint64_t g_674 = 0xEED4B0DBEC48095ALL;/* VOLATILE GLOBAL g_674 */
static const uint32_t g_693 = 0UL;
static float ** const **g_759 = (void*)0;
static volatile struct S0 g_760 = {0xD969BF6DL,0x8.4p-1,18446744073709551615UL};/* VOLATILE GLOBAL g_760 */
static volatile float g_765 = (-0x5.5p+1);/* VOLATILE GLOBAL g_765 */
static volatile float *g_764 = &g_765;
static volatile float **g_763 = &g_764;
static volatile float *** volatile g_762 = &g_763;/* VOLATILE GLOBAL g_762 */
static volatile float *** volatile * volatile g_761 = &g_762;/* VOLATILE GLOBAL g_761 */
static uint64_t *g_768[9][7][2] = {{{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183}},{{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183}},{{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]}},{{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]}},{{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]}},{{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183}},{{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183}},{{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]}},{{(void*)0,&g_183},{&g_302[0],&g_302[0]},{&g_302[0],&g_183},{(void*)0,&g_302[1]},{&g_183,&g_302[1]},{(void*)0,&g_183},{&g_302[0],&g_302[0]}}};
static const volatile struct S0 g_800 = {0xBD8ED0BFL,0x4.CE4061p-98,18446744073709551610UL};/* VOLATILE GLOBAL g_800 */
static volatile struct S0 g_817 = {0xDB32D182L,0x1.1p-1,0UL};/* VOLATILE GLOBAL g_817 */
static int16_t g_856 = 0xCA16L;
static volatile uint32_t g_877[9][7][4] = {{{0xAEA3E1CFL,1UL,7UL,1UL},{1UL,0xC72C9B6BL,7UL,7UL},{0xAEA3E1CFL,0xAEA3E1CFL,1UL,7UL},{0x5A01B381L,0xC72C9B6BL,0x5A01B381L,1UL},{0x5A01B381L,1UL,1UL,0x5A01B381L},{0xAEA3E1CFL,1UL,7UL,1UL},{1UL,0xC72C9B6BL,0xC72C9B6BL,0xC72C9B6BL}},{{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L}},{{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L}},{{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL}},{{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,0x5A01B381L,0x5A01B381L,7UL}},{{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL}},{{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L}},{{7UL,0x5A01B381L,0x5A01B381L,7UL},{1UL,0x5A01B381L,0xC72C9B6BL,0x5A01B381L},{0x5A01B381L,0xAEA3E1CFL,0xC72C9B6BL,0xC72C9B6BL},{1UL,1UL,0x5A01B381L,0xC72C9B6BL},{7UL,0xAEA3E1CFL,7UL,0x5A01B381L},{7UL,7UL,7UL,0xC72C9B6BL},{0x5A01B381L,7UL,0xAEA3E1CFL,7UL}},{{7UL,1UL,0xAEA3E1CFL,0xAEA3E1CFL},{0x5A01B381L,0x5A01B381L,7UL,0xAEA3E1CFL},{0xC72C9B6BL,1UL,0xC72C9B6BL,7UL},{0xC72C9B6BL,7UL,7UL,0xC72C9B6BL},{0x5A01B381L,7UL,0xAEA3E1CFL,7UL},{7UL,1UL,0xAEA3E1CFL,0xAEA3E1CFL},{0x5A01B381L,0x5A01B381L,7UL,0xAEA3E1CFL}}};
static uint32_t g_886 = 0xB99F174BL;
static int32_t g_951 = 0x81E251DEL;
static uint32_t *g_966 = (void*)0;
static volatile struct S0 g_980 = {0x91E8371FL,0x5.6p+1,0UL};/* VOLATILE GLOBAL g_980 */
static struct S0 g_981 = {0xB410EEC1L,0x0.DE33CDp-60,6UL};/* VOLATILE GLOBAL g_981 */
static const struct S0 g_1021[1][5][8] = {{{{9L,0x6.2p+1,0xC5E173A9L},{0x3F92A926L,0x1.B8881Ep+68,0xF79BC865L},{-4L,0xB.B55CA9p+99,18446744073709551615UL},{9L,0x6.2p+1,0xC5E173A9L},{-4L,0xB.B55CA9p+99,18446744073709551615UL},{0x3F92A926L,0x1.B8881Ep+68,0xF79BC865L},{9L,0x6.2p+1,0xC5E173A9L},{0xABA03768L,0xE.6A5D0Ep+77,8UL}},{{0x883B8B8CL,0xD.4EF55Fp+47,1UL},{-10L,-0x10.5p-1,0xAB4B9372L},{0x5D07FB45L,0x1.3p+1,0x56F706E6L},{9L,0x6.2p+1,0xC5E173A9L},{9L,0x6.2p+1,0xC5E173A9L},{0x5D07FB45L,0x1.3p+1,0x56F706E6L},{-10L,-0x10.5p-1,0xAB4B9372L},{0x883B8B8CL,0xD.4EF55Fp+47,1UL}},{{0xABA03768L,0xE.6A5D0Ep+77,8UL},{9L,0x6.2p+1,0xC5E173A9L},{0x3F92A926L,0x1.B8881Ep+68,0xF79BC865L},{-4L,0xB.B55CA9p+99,18446744073709551615UL},{9L,0x6.2p+1,0xC5E173A9L},{-4L,0xB.B55CA9p+99,18446744073709551615UL},{0x3F92A926L,0x1.B8881Ep+68,0xF79BC865L},{9L,0x6.2p+1,0xC5E173A9L}},{{0x883B8B8CL,0xD.4EF55Fp+47,1UL},{0x3F92A926L,0x1.B8881Ep+68,0xF79BC865L},{0xABA03768L,0xE.6A5D0Ep+77,8UL},{0x883B8B8CL,0xD.4EF55Fp+47,1UL},{-4L,0xB.B55CA9p+99,18446744073709551615UL},{-4L,0xB.B55CA9p+99,18446744073709551615UL},{0x883B8B8CL,0xD.4EF55Fp+47,1UL},{0xABA03768L,0xE.6A5D0Ep+77,8UL}},{{9L,0x6.2p+1,0xC5E173A9L},{9L,0x6.2p+1,0xC5E173A9L},{0x5D07FB45L,0x1.3p+1,0x56F706E6L},{-10L,-0x10.5p-1,0xAB4B9372L},{0x883B8B8CL,0xD.4EF55Fp+47,1UL},{0x5D07FB45L,0x1.3p+1,0x56F706E6L},{0x883B8B8CL,0xD.4EF55Fp+47,1UL},{-10L,-0x10.5p-1,0xAB4B9372L}}}};
static const struct S0 g_1023 = {0x8DF9AD19L,0x0.3p-1,18446744073709551615UL};/* VOLATILE GLOBAL g_1023 */
static int32_t g_1025 = 0L;
static uint8_t * volatile * volatile *g_1027[3] = {&g_644,&g_644,&g_644};
static uint8_t * volatile * volatile ** volatile g_1026 = &g_1027[0];/* VOLATILE GLOBAL g_1026 */
static const int32_t *g_1030 = &g_500;
static const int32_t ** volatile g_1029 = &g_1030;/* VOLATILE GLOBAL g_1029 */
static volatile uint8_t g_1057[8][8][4] = {{{1UL,1UL,0x98L,7UL},{255UL,0UL,253UL,0xB7L},{253UL,0x3BL,1UL,255UL},{255UL,9UL,255UL,0xC1L},{0x77L,4UL,0UL,0UL},{0x4BL,3UL,255UL,7UL},{0xCEL,9UL,0x3BL,255UL},{0x0BL,0UL,0x05L,0xF4L}},{{0x0EL,0x05L,255UL,1UL},{255UL,0x11L,0x95L,0x25L},{251UL,247UL,7UL,0x3EL},{246UL,3UL,0x98L,1UL},{7UL,1UL,0x4BL,7UL},{0x11L,1UL,0x27L,4UL},{0x37L,9UL,0x37L,255UL},{0x28L,0x1AL,0xF5L,0xF6L}},{{253UL,255UL,1UL,0x1AL},{0xCEL,255UL,1UL,0x49L},{253UL,2UL,0xF5L,0UL},{0x28L,0x69L,0x37L,1UL},{0x37L,1UL,0x27L,3UL},{0x11L,0x7AL,0x4BL,0UL},{7UL,0UL,0x98L,0x3BL},{246UL,1UL,7UL,0UL}},{{251UL,0x7FL,0x95L,0xAEL},{255UL,9UL,255UL,253UL},{0x0EL,7UL,0x05L,0xAEL},{0x0BL,0x16L,0x3BL,255UL},{0xCEL,255UL,0x98L,1UL},{0UL,0x28L,0UL,0x1AL},{255UL,255UL,0UL,0UL},{3UL,0UL,1UL,255UL}},{{0x53L,255UL,0x49L,0xF4L},{0x98L,1UL,8UL,255UL},{0xD1L,1UL,0xBEL,0UL},{6UL,0xD1L,253UL,1UL},{255UL,0x83L,1UL,0xF5L},{1UL,0x56L,7UL,1UL},{0xF0L,1UL,0xCEL,0x4AL},{0xAEL,0x72L,0UL,0x25L}},{{255UL,1UL,255UL,4UL},{1UL,7UL,255UL,0UL},{1UL,6UL,255UL,246UL},{255UL,0x95L,0x3EL,4UL},{0x56L,8UL,8UL,0x56L},{255UL,1UL,1UL,2UL},{0x0BL,0UL,7UL,0x11L},{0x25L,0x83L,0xF4L,0x11L}},{{255UL,0UL,0UL,2UL},{255UL,1UL,3UL,0x56L},{0xAEL,8UL,1UL,4UL},{0xBEL,0x95L,0x53L,246UL},{255UL,6UL,0x25L,0UL},{0xF4L,7UL,0x4BL,4UL},{0UL,1UL,0xF0L,0x25L},{9UL,0x72L,8UL,0x4AL}},{{0x4AL,1UL,0x37L,1UL},{0UL,0x56L,254UL,0xF5L},{0x80L,0x83L,1UL,1UL},{254UL,0xD1L,0x0BL,0UL},{1UL,1UL,255UL,255UL},{0xAEL,1UL,0xD1L,0xF4L},{1UL,255UL,253UL,255UL},{1UL,0UL,0x80L,0UL}}};
static int8_t g_1064 = (-1L);
static volatile int8_t g_1127 = 0L;/* VOLATILE GLOBAL g_1127 */
static volatile int8_t *g_1126 = &g_1127;
static volatile int8_t **g_1125 = &g_1126;
static int32_t g_1157 = (-1L);
static uint16_t g_1207 = 0x90A3L;
static volatile int64_t g_1225[8][7] = {{(-4L),1L,0xEA19AC27BD835AADLL,(-1L),0L,0x8538CEDC30B3A81CLL,0x164D8B74774731CELL},{0x2FC8CEE437BB4207LL,(-1L),0x8538CEDC30B3A81CLL,0xEA19AC27BD835AADLL,0x8538CEDC30B3A81CLL,(-1L),0x2FC8CEE437BB4207LL},{(-1L),0x2FC8CEE437BB4207LL,0x42EB52C4591A552DLL,0xEA19AC27BD835AADLL,0L,0x164D8B74774731CELL,(-1L)},{1L,0x621F355E090B7132LL,0xC36508DF0D853437LL,(-1L),(-1L),0L,0L},{0xC36508DF0D853437LL,1L,0x42EB52C4591A552DLL,1L,0xC36508DF0D853437LL,(-4L),1L},{1L,1L,0x8538CEDC30B3A81CLL,0x2FC8CEE437BB4207LL,0xEA19AC27BD835AADLL,(-1L),(-1L)},{1L,0x621F355E090B7132LL,0xEA19AC27BD835AADLL,0L,0x42EB52C4591A552DLL,0x42EB52C4591A552DLL,(-1L)},{(-1L),0x621F355E090B7132LL,(-1L),1L,0L,(-4L),(-1L)}};
static volatile int64_t *g_1224 = &g_1225[5][6];
static volatile int64_t **g_1223 = &g_1224;
static int16_t g_1242[9][5][5] = {{{(-10L),0x3584L,(-1L),(-1L),0x0A2CL},{0x70DDL,6L,0xBCE9L,1L,0xC03AL},{(-10L),(-1L),1L,(-1L),(-10L)},{0xD583L,0xF404L,0xC03AL,0xBCE9L,9L},{(-1L),0x78B0L,(-1L),0x9221L,0L}},{{(-1L),0x1695L,0x83FBL,0xF404L,9L},{1L,0x9221L,0x9221L,1L,(-10L)},{9L,0xEBD2L,(-1L),0L,0xC03AL},{8L,(-10L),0x5B46L,0xF6D6L,0x0A2CL},{(-1L),0L,6L,0L,0L}},{{0x78B0L,8L,0x78B0L,1L,1L},{0L,0x70DDL,0xA957L,0xF404L,0xBCE9L},{0x9955L,0x61DFL,(-10L),0x9221L,5L},{0xF404L,0L,0xA957L,0xBCE9L,0xA957L},{0x5B46L,0x5B46L,0x78B0L,(-1L),0xF6D6L}},{{0xC03AL,0x9CA8L,6L,1L,0xD583L},{0L,0x0A2CL,0x5B46L,(-1L),(-1L)},{0x1B78L,0x9CA8L,(-1L),(-1L),0x9CA8L},{5L,0x5B46L,0x9221L,0x9955L,0x3584L},{0x1695L,0L,0x83FBL,0x1B78L,1L}},{{(-1L),0x61DFL,(-1L),0x0A2CL,1L},{0x1695L,0x70DDL,0xC03AL,0x70DDL,0x1695L},{5L,8L,1L,(-1L),0x5B46L},{0x1B78L,0L,0xBCE9L,0xEBD2L,(-1L)},{0L,(-10L),(-1L),8L,0x5B46L}},{{0xC03AL,0xEBD2L,0xEBD2L,0xC03AL,0x1695L},{0x5B46L,0x9221L,0x9955L,0x3584L,1L},{0xF404L,0x1695L,9L,0xA957L,1L},{0x9955L,0x78B0L,0x61DFL,0x3584L,0x3584L},{0L,0xF404L,0L,0xC03AL,0x9CA8L}},{{0x78B0L,(-1L),0xF6D6L,8L,(-1L)},{(-1L),6L,0x1695L,0xEBD2L,0xD583L},{8L,0x3584L,0xF6D6L,(-1L),0xF6D6L},{9L,9L,0L,0x70DDL,0xA957L},{1L,1L,0x61DFL,0x0A2CL,5L}},{{(-1L),1L,9L,0x1B78L,0xBCE9L},{(-1L),1L,1L,1L,0x0A2CL},{0L,(-1L),9L,0xC03AL,0xA957L},{(-1L),0xF6D6L,8L,(-1L),(-1L)},{0xEBD2L,0x1B78L,(-1L),0x83FBL,0L}},{{(-1L),0x9221L,0x78B0L,0x9221L,(-1L)},{0L,6L,0L,(-1L),(-1L)},{(-1L),5L,0x9955L,0x5B46L,0L},{0xB05FL,0x70DDL,0xF404L,6L,(-1L)},{0x78B0L,0x5B46L,0x5B46L,0x78B0L,(-1L)}}};
static uint8_t g_1248[8] = {0xD7L,251UL,0xD7L,251UL,0xD7L,251UL,0xD7L,251UL};
static int64_t g_1295 = 0x1CF16EC6BF5F6944LL;
static volatile int16_t * volatile * volatile * const g_1301 = &g_322;
static volatile int16_t * volatile * volatile * const * volatile g_1300 = &g_1301;/* VOLATILE GLOBAL g_1300 */
static volatile int16_t * volatile * volatile * const * volatile *g_1299 = &g_1300;
static const int64_t g_1327[1][9][1] = {{{0xE3AB1869788F3C1FLL},{0x2120CBA050F1C811LL},{0xE3AB1869788F3C1FLL},{0x2120CBA050F1C811LL},{0xE3AB1869788F3C1FLL},{0x2120CBA050F1C811LL},{0xE3AB1869788F3C1FLL},{0x2120CBA050F1C811LL},{0xE3AB1869788F3C1FLL}}};
static const int64_t *g_1326 = &g_1327[0][0][0];
static float g_1379 = 0x1.3190D7p-64;
static volatile int64_t g_1383 = 1L;/* VOLATILE GLOBAL g_1383 */
static int32_t g_1384 = 1L;
static volatile uint32_t g_1409 = 18446744073709551615UL;/* VOLATILE GLOBAL g_1409 */
static int32_t ***g_1417 = &g_87[1][2];
static int32_t ****g_1416 = &g_1417;
static int32_t ***** volatile g_1415[8] = {&g_1416,(void*)0,(void*)0,&g_1416,(void*)0,(void*)0,&g_1416,(void*)0};
static volatile uint16_t g_1423 = 0xF580L;/* VOLATILE GLOBAL g_1423 */
static volatile uint8_t g_1436 = 1UL;/* VOLATILE GLOBAL g_1436 */
static int32_t ** volatile g_1440 = (void*)0;/* VOLATILE GLOBAL g_1440 */
static uint8_t *g_1484 = &g_173;
static uint8_t **g_1483 = &g_1484;
static uint8_t *** volatile g_1482 = &g_1483;/* VOLATILE GLOBAL g_1482 */
static volatile uint16_t * volatile * volatile *g_1487 = (void*)0;
static volatile uint16_t * volatile * volatile **g_1486[4][4][6] = {{{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,&g_1487},{&g_1487,&g_1487,&g_1487,&g_1487,&g_1487,&g_1487},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,(void*)0},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,&g_1487}},{{&g_1487,&g_1487,&g_1487,&g_1487,&g_1487,(void*)0},{&g_1487,&g_1487,&g_1487,&g_1487,&g_1487,&g_1487},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,(void*)0},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,&g_1487}},{{&g_1487,&g_1487,&g_1487,&g_1487,&g_1487,&g_1487},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,(void*)0},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,&g_1487},{&g_1487,&g_1487,&g_1487,&g_1487,&g_1487,(void*)0}},{{&g_1487,&g_1487,&g_1487,&g_1487,&g_1487,&g_1487},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,(void*)0},{&g_1487,&g_1487,(void*)0,&g_1487,&g_1487,&g_1487},{&g_1487,&g_1487,&g_1487,&g_1487,&g_1487,&g_1487}}};
static uint8_t ***g_1508 = &g_1483;
static uint8_t ****g_1507 = &g_1508;
static uint8_t *****g_1506 = &g_1507;
static volatile uint8_t g_1517[10] = {0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL};
static const uint8_t ****g_1566[2] = {(void*)0,(void*)0};
static const uint8_t ***** volatile g_1565[9][6][4] = {{{&g_1566[0],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[1],(void*)0,(void*)0},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],(void*)0,&g_1566[1],&g_1566[1]}},{{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[0],&g_1566[1],&g_1566[0]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[0]}},{{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],(void*)0},{&g_1566[0],&g_1566[1],&g_1566[1],&g_1566[0]},{&g_1566[1],(void*)0,&g_1566[1],&g_1566[1]},{&g_1566[0],&g_1566[1],&g_1566[1],&g_1566[1]}},{{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],(void*)0,&g_1566[1],&g_1566[0]},{&g_1566[1],&g_1566[1],&g_1566[1],(void*)0},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[1],(void*)0,&g_1566[1]}},{{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[0]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[0]},{&g_1566[0],&g_1566[0],(void*)0,&g_1566[1]},{&g_1566[1],&g_1566[1],(void*)0,&g_1566[1]},{&g_1566[1],&g_1566[1],(void*)0,&g_1566[1]}},{{&g_1566[0],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],(void*)0,&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],(void*)0,&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[0]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]}},{{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],(void*)0},{&g_1566[1],&g_1566[0],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[0],&g_1566[1],(void*)0},{&g_1566[0],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]}},{{&g_1566[0],&g_1566[1],&g_1566[1],&g_1566[0]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],(void*)0,&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[1],&g_1566[1]}},{{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[1],&g_1566[0],&g_1566[1],&g_1566[0]},{&g_1566[1],&g_1566[1],&g_1566[0],&g_1566[1]},{&g_1566[0],&g_1566[1],&g_1566[1],&g_1566[1]},{&g_1566[1],&g_1566[1],(void*)0,&g_1566[1]}}};
static uint16_t g_1644[5] = {65532UL,65532UL,65532UL,65532UL,65532UL};
static uint32_t g_1687 = 0xEF1BC82AL;
static int32_t ** volatile g_1762 = &g_412;/* VOLATILE GLOBAL g_1762 */
static int8_t *g_1791[9][7][4] = {{{&g_114[0],&g_114[0],&g_114[1],&g_114[0]},{&g_114[0],&g_114[0],&g_1064,&g_114[0]},{&g_114[0],&g_114[2],&g_1064,&g_114[1]},{&g_114[1],&g_1064,&g_114[0],&g_1064},{&g_114[0],&g_1064,(void*)0,(void*)0},{&g_114[0],&g_114[1],&g_114[0],&g_114[0]},{&g_114[1],(void*)0,&g_1064,&g_114[0]}},{{&g_114[0],&g_114[0],&g_1064,&g_114[2]},{&g_114[0],&g_114[0],&g_114[1],&g_114[0]},{&g_114[0],&g_1064,&g_1064,&g_114[0]},{(void*)0,&g_114[0],&g_1064,&g_114[0]},{&g_1064,&g_114[1],&g_1064,&g_114[0]},{&g_114[3],(void*)0,&g_1064,&g_1064},{&g_114[0],&g_114[0],&g_1064,&g_114[0]}},{{&g_114[0],&g_114[2],&g_114[0],&g_114[0]},{&g_114[0],(void*)0,&g_114[3],&g_114[0]},{&g_1064,(void*)0,&g_1064,&g_114[0]},{(void*)0,&g_114[2],(void*)0,&g_114[0]},{&g_114[1],&g_114[0],&g_114[1],&g_1064},{(void*)0,(void*)0,(void*)0,&g_114[0]},{&g_114[0],&g_114[1],&g_114[3],&g_114[0]}},{{&g_114[1],&g_114[0],&g_1064,&g_114[0]},{&g_114[0],&g_1064,&g_1064,&g_114[0]},{&g_114[0],&g_114[0],&g_114[0],&g_114[2]},{&g_114[0],&g_114[0],&g_114[0],&g_114[0]},{&g_114[0],(void*)0,&g_1064,&g_114[0]},{&g_114[0],&g_114[1],(void*)0,(void*)0},{&g_114[3],&g_1064,(void*)0,&g_1064}},{{&g_114[0],&g_1064,&g_1064,&g_114[1]},{&g_114[0],&g_114[2],&g_114[0],&g_114[0]},{&g_114[0],&g_114[0],&g_114[0],&g_114[0]},{&g_114[0],&g_114[0],&g_1064,(void*)0},{&g_114[0],&g_114[2],&g_1064,&g_114[2]},{&g_114[1],&g_114[0],&g_114[3],&g_1064},{&g_114[0],&g_114[0],(void*)0,&g_114[0]}},{{(void*)0,&g_114[1],&g_114[1],(void*)0},{&g_114[1],&g_114[0],(void*)0,&g_114[0]},{(void*)0,(void*)0,&g_1064,&g_114[1]},{&g_1064,&g_114[0],&g_114[3],&g_114[1]},{&g_114[0],(void*)0,&g_114[0],&g_114[0]},{&g_114[0],&g_114[0],&g_1064,(void*)0},{&g_114[0],&g_114[1],&g_1064,&g_114[0]}},{{&g_114[3],&g_114[0],&g_1064,&g_1064},{&g_1064,&g_114[0],&g_1064,(void*)0},{(void*)0,&g_114[0],&g_1064,&g_1064},{(void*)0,&g_114[0],(void*)0,&g_114[0]},{&g_114[1],&g_114[0],&g_1064,&g_114[0]},{&g_114[0],&g_114[0],&g_114[2],&g_114[0]},{&g_114[0],&g_114[0],&g_1064,&g_114[0]}},{{&g_1064,&g_114[1],&g_114[2],&g_1064},{&g_1064,(void*)0,&g_1064,&g_1064},{&g_114[0],&g_1064,&g_114[2],&g_1064},{&g_114[0],&g_114[0],&g_1064,(void*)0},{&g_114[1],&g_1064,(void*)0,&g_114[2]},{(void*)0,&g_114[1],&g_1064,&g_1064},{(void*)0,&g_114[0],&g_114[0],&g_1064}},{{&g_114[0],(void*)0,&g_114[2],&g_114[0]},{&g_1064,(void*)0,&g_114[0],&g_114[0]},{&g_114[3],&g_114[3],&g_114[0],&g_114[2]},{&g_1064,&g_114[0],&g_114[0],&g_114[0]},{(void*)0,&g_1064,&g_1064,&g_114[0]},{&g_114[0],&g_1064,&g_1064,&g_114[0]},{&g_1064,&g_114[0],&g_1064,&g_114[2]}}};
static int8_t **g_1790[10] = {&g_1791[3][1][1],&g_1791[3][1][1],&g_1791[3][2][0],&g_1791[3][2][0],&g_1791[3][2][0],&g_1791[3][1][1],&g_1791[3][1][1],&g_1791[3][2][0],&g_1791[3][2][0],&g_1791[3][2][0]};
static const uint64_t g_1854 = 1UL;
static int32_t ** volatile g_1891 = (void*)0;/* VOLATILE GLOBAL g_1891 */
static int32_t ** volatile g_1892 = (void*)0;/* VOLATILE GLOBAL g_1892 */
static struct S0 *g_1953 = &g_981;
static struct S0 ** volatile g_1952[7][7][5] = {{{(void*)0,&g_1953,&g_1953,&g_1953,(void*)0},{&g_1953,&g_1953,&g_1953,&g_1953,(void*)0},{&g_1953,(void*)0,&g_1953,&g_1953,&g_1953},{&g_1953,(void*)0,&g_1953,(void*)0,&g_1953},{(void*)0,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953}},{{&g_1953,(void*)0,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,(void*)0,&g_1953,&g_1953},{(void*)0,&g_1953,&g_1953,(void*)0,&g_1953},{&g_1953,&g_1953,(void*)0,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953}},{{(void*)0,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{(void*)0,&g_1953,&g_1953,(void*)0,&g_1953},{&g_1953,&g_1953,&g_1953,(void*)0,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953}},{{&g_1953,&g_1953,(void*)0,(void*)0,&g_1953},{&g_1953,(void*)0,&g_1953,&g_1953,&g_1953},{(void*)0,&g_1953,(void*)0,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{(void*)0,(void*)0,(void*)0,&g_1953,&g_1953},{&g_1953,&g_1953,(void*)0,(void*)0,&g_1953},{&g_1953,&g_1953,&g_1953,(void*)0,&g_1953}},{{&g_1953,(void*)0,(void*)0,(void*)0,&g_1953},{&g_1953,&g_1953,(void*)0,&g_1953,(void*)0},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,(void*)0,(void*)0,&g_1953,(void*)0},{(void*)0,&g_1953,&g_1953,&g_1953,&g_1953},{(void*)0,&g_1953,(void*)0,&g_1953,&g_1953},{(void*)0,&g_1953,&g_1953,&g_1953,&g_1953}},{{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{(void*)0,(void*)0,&g_1953,&g_1953,&g_1953},{(void*)0,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,&g_1953,(void*)0,&g_1953,&g_1953},{&g_1953,(void*)0,(void*)0,&g_1953,&g_1953},{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953}},{{&g_1953,&g_1953,&g_1953,&g_1953,&g_1953},{&g_1953,(void*)0,&g_1953,&g_1953,&g_1953},{(void*)0,&g_1953,&g_1953,&g_1953,&g_1953},{(void*)0,&g_1953,&g_1953,&g_1953,(void*)0},{&g_1953,(void*)0,&g_1953,(void*)0,(void*)0},{&g_1953,&g_1953,&g_1953,(void*)0,&g_1953},{(void*)0,&g_1953,(void*)0,(void*)0,(void*)0}}};
static struct S0 g_1955 = {-10L,0x1.3p-1,0x444DC082L};/* VOLATILE GLOBAL g_1955 */
static uint32_t g_2029 = 1UL;
static struct S0 ** volatile g_2101 = (void*)0;/* VOLATILE GLOBAL g_2101 */
static volatile struct S0 g_2115 = {0xD5641405L,0xA.D954E0p-42,18446744073709551615UL};/* VOLATILE GLOBAL g_2115 */
static uint32_t g_2169 = 0x8A53D64BL;
static int16_t g_2184 = 8L;
static float *g_2187 = &g_1379;
static int32_t g_2342 = 0L;
static uint64_t g_2350 = 1UL;
static const uint32_t g_2351 = 0xDE902B51L;
static int32_t g_2495 = (-2L);
static struct S0 ** volatile g_2502 = &g_1953;/* VOLATILE GLOBAL g_2502 */
static int64_t ****g_2541 = (void*)0;
static int32_t * volatile g_2561 = &g_1157;/* VOLATILE GLOBAL g_2561 */
static struct S0 g_2588 = {0x6857E8B9L,-0x9.3p+1,0xC2F3E85DL};/* VOLATILE GLOBAL g_2588 */
static struct S0 g_2599 = {-1L,0xE.79CCCBp+63,18446744073709551615UL};/* VOLATILE GLOBAL g_2599 */
static int32_t ** volatile g_2619 = &g_412;/* VOLATILE GLOBAL g_2619 */
static volatile struct S0 g_2624[2][5] = {{{8L,0xB.12EA8Ap+11,0x5B8FCAAAL},{8L,0xB.12EA8Ap+11,0x5B8FCAAAL},{8L,0x0.F8DD26p-25,0x7AB8116DL},{0x6992916FL,0x9.6759CBp+72,18446744073709551607UL},{8L,0x0.F8DD26p-25,0x7AB8116DL}},{{8L,0xB.12EA8Ap+11,0x5B8FCAAAL},{8L,0xB.12EA8Ap+11,0x5B8FCAAAL},{8L,0x0.F8DD26p-25,0x7AB8116DL},{0x6992916FL,0x9.6759CBp+72,18446744073709551607UL},{8L,0x0.F8DD26p-25,0x7AB8116DL}}};
static volatile uint8_t g_2675 = 247UL;/* VOLATILE GLOBAL g_2675 */
static int8_t ***g_2710 = &g_1790[0];
static int32_t ** volatile g_2728 = &g_88;/* VOLATILE GLOBAL g_2728 */


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static int32_t * func_4(uint32_t  p_5);
static int32_t * func_12(uint32_t  p_13);
static int16_t  func_26(int32_t * p_27, int32_t * p_28);
static uint8_t  func_39(int32_t * p_40, uint16_t  p_41, int32_t * p_42, uint32_t  p_43, uint64_t  p_44);
static uint8_t  func_56(uint16_t * p_57, int32_t  p_58, int16_t  p_59, int32_t ** p_60);
static uint16_t * func_61(uint8_t  p_62, uint32_t  p_63, int32_t * p_64, int32_t * p_65);
static int8_t  func_70(uint16_t  p_71, const int64_t  p_72, float  p_73, int32_t  p_74);
static uint16_t  func_75(uint32_t  p_76, int64_t  p_77, int8_t  p_78, const int16_t  p_79, int8_t  p_80);
static uint16_t  func_83(const int64_t  p_84, int32_t ** p_85, int8_t  p_86);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_10 g_951 g_1482 g_1483 g_1484 g_173 g_1326 g_1327 g_1224 g_1225 g_568 g_569 g_412 g_1762 g_1417 g_296 g_322 g_323 g_324 g_500 g_1299 g_1300 g_1301 g_543 g_546 g_544 g_1955 g_1507 g_1508 g_645 g_35 g_800.f2 g_1384 g_762 g_763 g_2029 g_288 g_150 g_46 g_299 g_1026 g_1027 g_644 g_2115 g_1506 g_166 g_1953 g_981 g_149 g_2169 g_1126 g_1127 g_114 g_2184 g_1223 g_235 g_171 g_37 g_886 g_764 g_1125 g_658.f2 g_1295 g_1030 g_1409 g_636 g_88 g_1423 g_255 g_298 g_1064 g_1486 g_670 g_765 g_1644 g_761 g_481 g_482 g_1687 g_856 g_1157 g_90 g_1416 g_2187 g_1379 g_2342 g_2350 g_2351 g_1207 g_1023.f2 g_1436 g_2495 g_183 g_2502 g_2541 g_658.f0 g_2561 g_2728
 * writes: g_10 g_299 g_296 g_951 g_1295 g_1687 g_87 g_90 g_166 g_543 g_1506 g_500 g_886 g_1379 g_46 g_173 g_1248 g_114 g_302 g_1326 g_171 g_235 g_482 g_1409 g_35 g_1423 g_1064 g_298 g_1483 g_1207 g_765 g_1644 g_1384 g_856 g_88 g_150 g_2187 g_1157 g_255 g_1030 g_1507 g_183 g_1953 g_768 g_2541 g_2710
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_2 = 0x6E3245F1DBECCCE1LL;
    uint16_t l_2149[7] = {0x35E0L,0x35E0L,0xB47EL,0x35E0L,0x35E0L,0xB47EL,0x35E0L};
    int32_t l_2151 = 0xE9283211L;
    int16_t l_2195 = 0xB398L;
    uint32_t **l_2225 = (void*)0;
    uint16_t l_2266[5] = {65530UL,65530UL,65530UL,65530UL,65530UL};
    float ** const l_2268 = &g_2187;
    int64_t *l_2303[1][7][6] = {{{&g_1295,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295},{&g_171,&g_1295,&g_1295,&g_171,&g_171,&g_171},{&g_171,&g_171,&g_171,&g_1295,&g_1295,&g_171},{&g_1295,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295},{&g_1295,&g_171,&g_1295,&g_1295,&g_1295,&g_171},{&g_1295,&g_171,&g_171,&g_171,&g_1295,&g_1295},{&g_1295,&g_171,&g_171,&g_1295,&g_1295,&g_1295}}};
    int64_t **l_2302 = &l_2303[0][1][5];
    int64_t ***l_2301 = &l_2302;
    const int32_t l_2368 = 0x11ACFD2EL;
    int32_t l_2369[1][1][1];
    int32_t l_2402 = 0x600CC320L;
    float l_2407 = 0x0.A050E5p+34;
    uint32_t l_2465 = 0xBD7FB289L;
    uint64_t l_2469 = 0x7718A911CE6433A1LL;
    int64_t l_2472 = (-2L);
    int32_t *l_2498 = &g_500;
    int8_t **l_2505 = (void*)0;
    uint32_t l_2549 = 0x245B8006L;
    uint8_t ***l_2552 = (void*)0;
    int32_t l_2559 = 0L;
    uint8_t l_2560 = 252UL;
    int32_t *l_2565[7];
    uint16_t l_2571[3];
    int16_t l_2591 = 0xF63FL;
    uint64_t l_2592 = 0UL;
    uint64_t *l_2644 = &g_2350;
    int16_t l_2667 = 0xE289L;
    uint64_t l_2702 = 0UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
                l_2369[i][j][k] = 6L;
        }
    }
    for (i = 0; i < 7; i++)
        l_2565[i] = (void*)0;
    for (i = 0; i < 3; i++)
        l_2571[i] = 65535UL;
    if ((l_2 , 0xC0D37AD4L))
    { /* block id: 1 */
        uint32_t l_2131 = 0x34645ECCL;
        uint32_t *l_2138[4][3] = {{(void*)0,(void*)0,(void*)0},{&g_37[0][1],&l_2131,&g_37[0][1]},{(void*)0,(void*)0,(void*)0},{&g_37[0][1],&l_2131,&g_37[0][1]}};
        float l_2139 = 0xA.B5D04Fp+33;
        int32_t l_2144 = 0xCA4A6A34L;
        uint64_t *l_2150[1];
        int8_t *l_2152 = &g_114[1];
        uint8_t *** const *l_2154[10][9][2] = {{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508}},{{&g_1508,&g_1508},{(void*)0,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{&g_1508,&g_1508},{(void*)0,&g_1508},{&g_1508,&g_1508}}};
        uint8_t *** const ** const l_2153 = &l_2154[3][3][1];
        int32_t l_2155 = (-3L);
        uint16_t l_2164 = 6UL;
        uint32_t l_2172 = 4294967292UL;
        float l_2196 = 0xB.CFB050p-45;
        int32_t **l_2289[4] = {&g_412,&g_412,&g_412,&g_412};
        uint16_t l_2310 = 1UL;
        uint32_t l_2319 = 0x0077916CL;
        uint32_t l_2352 = 0x69DE5D4DL;
        int32_t l_2379 = 0x6E7B8F7DL;
        int32_t l_2471 = (-1L);
        struct S0 *l_2501[2][10][2] = {{{&g_1955,&g_1955},{&g_1955,&g_546},{(void*)0,&g_544},{&g_546,&g_544},{(void*)0,&g_546},{&g_1955,&g_1955},{&g_1955,&g_546},{(void*)0,&g_544},{&g_546,&g_544},{(void*)0,&g_546}},{{&g_1955,&g_1955},{&g_1955,&g_546},{(void*)0,&g_544},{&g_546,&g_544},{(void*)0,&g_546},{&g_1955,&g_1955},{&g_1955,&g_546},{(void*)0,&g_544},{&g_546,&g_544},{(void*)0,&g_546}}};
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2150[i] = (void*)0;
lbl_2173:
        if (g_3)
        { /* block id: 2 */
            int16_t l_6 = 1L;
            int32_t *l_2126 = &g_296;
            l_2126 = func_4(l_6);
        }
        else
        { /* block id: 1011 */
            for (g_90 = 0; g_90 < 8; g_90 += 1)
            {
                g_1248[g_90] = 0x59L;
            }
        }
        (*g_412) ^= (safe_rshift_func_uint16_t_u_s((((*****g_1506) &= (safe_mul_func_uint16_t_u_u(l_2131, l_2131))) ^ (safe_sub_func_int16_t_s_s((*****g_1299), (safe_add_func_uint64_t_u_u((((safe_add_func_uint32_t_u_u(l_2, (g_166[0][6][0] ^= l_2))) , (l_2 , (safe_sub_func_uint32_t_u_u(((((((*l_2152) = ((safe_mul_func_int8_t_s_s(((l_2144 &= l_2) , (safe_mod_func_int64_t_s_s(((safe_div_func_uint64_t_u_u(0xF305981EB3E768A9LL, (l_2151 = l_2149[5]))) == 0UL), (*g_1326)))), l_2149[5])) || 0x07AEL)) , l_2153) != &g_1566[0]) & l_2131) >= l_2155), l_2131)))) , l_2144), l_2155))))), 2));
        if ((((safe_div_func_uint64_t_u_u((((safe_unary_minus_func_uint32_t_u(((safe_mul_func_uint8_t_u_u((+((void*)0 != &g_1126)), ((****g_1507)++))) , l_2155))) ^ ((l_2151 = ((*l_2152) &= (l_2164 <= (((*g_1953) , (safe_div_func_int16_t_s_s(l_2, (**g_149)))) , (safe_lshift_func_int8_t_s_s((g_2169 | (((*g_412) = (((safe_add_func_int16_t_s_s((l_2144 |= (l_2149[5] <= l_2172)), 0xD67AL)) == 8L) , l_2155)) , (*g_1126))), 3)))))) >= l_2)) | l_2155), l_2172)) | 0x35EFB44BE0464C74LL) >= 2L))
        { /* block id: 1025 */
            if (g_546.f0)
                goto lbl_2173;
        }
        else
        { /* block id: 1027 */
            int8_t l_2183 = 0x3EL;
            int32_t l_2185 = (-4L);
            int32_t l_2186[9][7] = {{0L,0x1527E83FL,0L,0L,(-5L),0xDE4F1FE8L,3L},{0x15F267CBL,0x1527E83FL,(-8L),0x15F267CBL,(-5L),0x5C50F2E3L,(-5L)},{0x15F267CBL,0L,0L,0x15F267CBL,3L,0xDE4F1FE8L,(-5L)},{0L,0x1527E83FL,0L,0L,(-5L),0xDE4F1FE8L,3L},{0x15F267CBL,0x1527E83FL,(-8L),0x15F267CBL,(-5L),0x5C50F2E3L,(-5L)},{0x15F267CBL,0L,0L,0x15F267CBL,3L,0xDE4F1FE8L,(-5L)},{0L,0x1527E83FL,0L,0L,(-5L),0xDE4F1FE8L,3L},{0x15F267CBL,0x1527E83FL,(-8L),0x15F267CBL,(-5L),0x5C50F2E3L,(-5L)},{0x15F267CBL,0L,0L,0x15F267CBL,3L,0xDE4F1FE8L,(-5L)}};
            int8_t **l_2211 = &l_2152;
            uint16_t *l_2212[5][3][9] = {{{&g_1644[3],&g_1644[3],(void*)0,&l_2149[5],&g_1207,&l_2149[0],&g_1644[0],(void*)0,(void*)0},{&l_2149[4],&l_2164,(void*)0,&l_2149[0],&l_2149[6],&g_1207,(void*)0,&l_2164,&l_2164},{&l_2164,&g_1644[0],&g_46,&l_2164,&g_46,&g_1644[0],&l_2164,&g_46,&g_1644[3]}},{{&g_1644[0],&l_2149[0],&g_1207,&l_2149[5],&g_1644[0],&l_2149[4],&g_1644[4],(void*)0,&l_2164},{&g_1207,(void*)0,&g_1644[0],(void*)0,&l_2164,(void*)0,&g_1644[0],&g_46,(void*)0},{&g_1644[1],&l_2149[6],&g_1644[2],&l_2164,&g_1644[0],&g_1644[0],&g_1644[0],&l_2164,&g_1644[2]}},{{&g_1207,&g_1207,(void*)0,&g_1644[3],&l_2164,&l_2149[5],(void*)0,(void*)0,&g_46},{(void*)0,(void*)0,&g_1207,&l_2164,&g_1207,(void*)0,&g_1207,(void*)0,&l_2164},{(void*)0,(void*)0,&g_1207,&g_1644[0],&g_1207,&l_2149[1],&g_1644[1],&g_1644[3],&g_1644[1]}},{{(void*)0,&g_1644[2],&l_2149[5],&l_2149[5],&g_1644[2],(void*)0,&g_46,(void*)0,(void*)0},{(void*)0,&g_1644[1],&g_1644[3],(void*)0,&g_1644[0],&g_1644[0],&g_1207,&l_2149[5],&l_2149[5]},{&l_2149[4],&g_1644[4],(void*)0,&l_2164,&l_2164,(void*)0,&g_46,(void*)0,&g_1644[2]}},{{&g_46,&l_2149[5],(void*)0,&g_1644[1],&l_2149[5],&l_2149[5],&g_1644[1],(void*)0,&l_2149[5]},{(void*)0,&l_2149[4],&g_1644[0],(void*)0,(void*)0,(void*)0,&g_1207,&l_2164,&g_1207},{&g_1207,&l_2149[1],&g_1644[0],&l_2149[5],&l_2164,&g_1207,&g_46,&g_1207,&l_2164}}};
            int16_t l_2213[9] = {0x1657L,0x1657L,0x1657L,0x1657L,0x1657L,0x1657L,0x1657L,0x1657L,0x1657L};
            float l_2229 = (-0x1.Fp+1);
            int32_t l_2239 = 0x5D2AF3BEL;
            const int32_t **l_2248 = &g_1030;
            const int32_t ***l_2247 = &l_2248;
            uint8_t l_2299 = 0xC9L;
            struct S0 **l_2309 = &g_1953;
            uint16_t ***l_2334 = &g_288;
            uint8_t l_2391 = 255UL;
            const uint8_t l_2466[6] = {0UL,0UL,0UL,0UL,0UL,0UL};
            int32_t l_2467[3];
            uint8_t *l_2470 = &l_2299;
            uint64_t l_2496 = 1UL;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_2467[i] = 0x139A9F9CL;
            for (g_951 = (-3); (g_951 != (-3)); g_951 = safe_add_func_int32_t_s_s(g_951, 7))
            { /* block id: 1030 */
                uint16_t l_2180 = 0UL;
                int32_t *l_2188 = &g_1157;
                (*l_2188) ^= ((g_2187 = func_12((((*g_1484) ^= (18446744073709551615UL < ((void*)0 != &g_966))) != (((((**g_568) |= (safe_rshift_func_uint16_t_u_u(l_2155, 1))) & ((void*)0 == (*g_1299))) != (l_2186[8][5] = (l_2180 | (l_2185 = (safe_mul_func_uint16_t_u_u(((((0UL > l_2183) & g_2184) > (*g_1326)) | l_2131), l_2183)))))) || l_2186[1][5])))) != (*g_763));
                return l_2183;
            }
            if ((safe_mod_func_int16_t_s_s((safe_sub_func_uint64_t_u_u((l_2183 >= (((safe_lshift_func_int8_t_s_s(((l_2195 | ((*g_150) = ((((**g_149) ^ (safe_rshift_func_uint16_t_u_u((l_2144 = (safe_add_func_int32_t_s_s((safe_div_func_int32_t_s_s(((*g_412) |= (safe_add_func_int64_t_s_s(((((-1L) ^ 0x74L) <= 1L) <= (safe_mul_func_uint8_t_u_u((**g_1483), 0xFFL))), (safe_lshift_func_uint8_t_u_s(((g_1295 = (safe_mul_func_uint16_t_u_u((l_2151 |= (((*l_2211) = &l_2183) == (void*)0)), 0x9BDAL))) >= l_2195), l_2))))), l_2)), l_2186[6][1]))), l_2213[0]))) >= 0L) | (***g_1301)))) >= l_2186[8][5]), 6)) , l_2155) || 0x79L)), l_2195)), l_2131)))
            { /* block id: 1045 */
                uint64_t l_2214 = 0x80E73AC5FEFE1889LL;
                uint32_t ***l_2226 = &l_2225;
                int16_t *l_2238 = &g_255[1][3];
                int32_t l_2240 = (-3L);
                uint32_t l_2267 = 0xD81225F7L;
                uint32_t l_2274 = 0xF33D55DBL;
                int32_t **l_2285 = &g_88;
                uint32_t l_2321 = 4UL;
                uint16_t ** const l_2392[8][6] = {{&l_2212[0][1][5],(void*)0,&l_2212[0][1][5],&l_2212[0][1][5],(void*)0,&l_2212[0][1][5]},{&l_2212[0][1][5],&g_150,&g_150,&l_2212[0][1][5],&g_150,&l_2212[0][1][5]},{&l_2212[0][1][5],&l_2212[2][2][0],&l_2212[0][1][5],&l_2212[0][1][5],&l_2212[2][2][0],&g_150},{&l_2212[0][1][5],(void*)0,&l_2212[0][1][5],&l_2212[0][1][5],(void*)0,&l_2212[0][1][5]},{&l_2212[0][1][5],&g_150,&g_150,&l_2212[0][1][5],&g_150,&l_2212[0][1][5]},{&l_2212[0][1][5],&l_2212[2][2][0],&l_2212[0][1][5],&l_2212[0][1][5],&l_2212[2][2][0],&g_150},{&l_2212[0][1][5],(void*)0,&l_2212[0][1][5],&l_2212[0][1][5],(void*)0,&l_2212[0][1][5]},{&l_2212[0][1][5],&g_150,&g_150,&l_2212[0][1][5],&g_150,&l_2212[0][1][5]}};
                int32_t l_2403[1][2];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_2403[i][j] = 0xC64F37FDL;
                }
                for (g_90 = 0; (g_90 <= 1); g_90 += 1)
                { /* block id: 1048 */
                    return l_2214;
                }
                for (g_296 = 0; (g_296 >= (-20)); --g_296)
                { /* block id: 1053 */
                    l_2151 |= l_2195;
                }
                if ((l_2240 &= ((**g_1762) = (safe_rshift_func_uint8_t_u_s((safe_mod_func_int64_t_s_s((*g_1224), (safe_mod_func_int8_t_s_s(((0x7F57L && (safe_add_func_uint64_t_u_u((((*l_2226) = l_2225) == &g_966), (((safe_mod_func_int16_t_s_s((**g_568), ((((*l_2238) = (safe_mul_func_uint8_t_u_u(l_2214, (safe_mul_func_int8_t_s_s((safe_div_func_uint32_t_u_u((((*g_1416) != (((safe_sub_func_int64_t_s_s(((*g_1326) > (l_2214 < 1L)), (*g_1326))) != (***g_1508)) , (void*)0)) ^ l_2144), l_2149[5])), 0x4FL))))) <= 0xCC9AL) , 6UL))) , l_2213[0]) != l_2239)))) || l_2214), (***g_1508))))), l_2213[0])))))
                { /* block id: 1060 */
                    uint64_t l_2249 = 0x616A1C60F3BC4E11LL;
                    float *l_2277 = &g_482;
                    int32_t l_2278 = 0L;
                    if ((safe_add_func_uint16_t_u_u(1UL, 0x2B2FL)))
                    { /* block id: 1061 */
                        const int32_t l_2264 = 0L;
                        int64_t *l_2265 = &g_171;
                        int32_t ***l_2286 = &g_87[1][4];
                        int32_t ***l_2287 = (void*)0;
                        int32_t ***l_2288[5][2];
                        float *l_2300 = &l_2139;
                        int i, j;
                        for (i = 0; i < 5; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_2288[i][j] = (void*)0;
                        }
                        (*g_2187) = ((safe_sub_func_int8_t_s_s(((((*g_1484) &= ((safe_add_func_int16_t_s_s((l_2247 == (*g_1416)), 0x74E3L)) == ((**g_568) &= 0x6EDEL))) && ((****g_1507) = l_2155)) & ((*l_2265) = (l_2249 != ((0x5D0F9506L || (safe_rshift_func_int8_t_s_s((safe_add_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_s(((safe_add_func_int64_t_s_s((l_2249 | (safe_rshift_func_int8_t_s_s((safe_add_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u(l_2264, l_2240)) < l_2214), l_2214)), 6))), (-8L))) || l_2264), 7)) || (*g_412)), (*g_1224))), l_2144))) == l_2131)))), l_2266[1])) , l_2267);
                        l_2278 ^= ((l_2268 != (void*)0) != (safe_div_func_int8_t_s_s((safe_add_func_int16_t_s_s(0x0CF7L, (~(l_2274 , l_2249)))), (safe_sub_func_int32_t_s_s(((*g_412) |= (l_2277 != (*l_2268))), l_2164)))));
                        l_2278 = ((((*l_2277) = 0x6.F7BA7Cp+38) > ((*g_2187) = (***g_762))) > (safe_div_func_float_f_f((safe_add_func_float_f_f(l_2266[2], ((*l_2300) = ((l_2214 < ((safe_sub_func_float_f_f(((l_2289[1] = l_2285) == ((safe_mul_func_uint8_t_u_u((****g_1507), (+(((((safe_lshift_func_uint16_t_u_s((*g_150), (((safe_rshift_func_uint16_t_u_u((((l_2186[8][5] = (safe_lshift_func_uint16_t_u_u(((l_2151 = 0xEB39A19FL) != ((l_2278 , (void*)0) == (void*)0)), 1))) < (****g_1507)) != (**g_149)), 6)) <= l_2131) , 0x61CBL))) < g_114[0]) && 0x95L) ^ (**l_2248)) == (*g_1326))))) , (void*)0)), l_2299)) >= 0x1.3p-1)) > l_2249)))), 0x7.56ECCFp+44)));
                        (*l_2285) = (*g_1762);
                    }
                    else
                    { /* block id: 1077 */
                        int64_t ***l_2304 = (void*)0;
                        int64_t ****l_2305 = &l_2304;
                        (*g_412) = (l_2301 == ((*l_2305) = l_2304));
                        return (**g_1125);
                    }
                    if (l_2240)
                        goto lbl_2562;
                    for (g_46 = 0; (g_46 <= 21); ++g_46)
                    { /* block id: 1084 */
                        struct S0 **l_2308 = (void*)0;
                        l_2309 = l_2308;
                        (*l_2248) = (**l_2247);
                    }
                    l_2310++;
                }
                else
                { /* block id: 1089 */
                    int32_t l_2316 = 0x066FAB94L;
                    uint32_t l_2326 = 1UL;
                    int32_t l_2370 = 0xABE8DDC9L;
                    float ** const * const l_2376 = &l_2268;
                    int8_t l_2380[10][8][3] = {{{0xF7L,(-6L),7L},{0x66L,(-1L),0xFBL},{(-1L),0xFBL,0L},{(-3L),0L,1L},{0x1BL,0x0CL,1L},{1L,0x4DL,(-6L)},{0xF5L,0x7FL,0xC0L},{0xCBL,(-1L),0x33L}},{{1L,0L,0xF7L},{1L,0xDCL,0xF5L},{0xCBL,7L,0xA9L},{0xF5L,1L,(-1L)},{1L,1L,1L},{0x1BL,0x2AL,(-4L)},{(-3L),4L,2L},{(-1L),0x07L,0x66L}},{{0x66L,(-3L),(-1L)},{0xF7L,0x3AL,1L},{0xB6L,2L,0x07L},{2L,0x8FL,0xBBL},{(-1L),0x66L,3L},{0x97L,0L,0L},{0x02L,0L,0x07L},{1L,0x3FL,0x71L}},{{0xC0L,(-1L),(-9L)},{0L,(-1L),1L},{1L,(-1L),(-2L)},{2L,0x3FL,0x33L},{0L,0L,(-1L)},{2L,0L,1L},{1L,0x66L,0xBBL},{0xF7L,0x61L,0L}},{{0xD3L,(-6L),0x1BL},{(-4L),1L,0xCBL},{0xDCL,(-1L),2L},{7L,(-3L),0xA6L},{0xD0L,0x02L,0x54L},{8L,1L,0x02L},{(-1L),0xBBL,0xA9L},{0x88L,0x97L,(-1L)}},{{1L,1L,0xD2L},{0x47L,0xFBL,(-1L)},{(-1L),0xD3L,(-1L)},{0x99L,0x16L,0xD2L},{0x7FL,3L,(-1L)},{0xB6L,0x8EL,0xA9L},{0x3DL,0x8FL,0x02L},{(-1L),0xD8L,0x54L}},{{(-9L),1L,0xA6L},{1L,0x3EL,2L},{0xC7L,2L,0xCBL},{0x9EL,1L,0x1BL},{0xDDL,1L,0L},{(-1L),0x07L,0xBBL},{0x1BL,(-10L),1L},{(-1L),1L,(-1L)}},{{(-1L),0x74L,0x33L},{7L,(-1L),(-2L)},{0x8FL,2L,1L},{0xDFL,0xC7L,(-9L)},{0x8FL,0xDDL,0x71L},{7L,0xDCL,0x07L},{(-1L),2L,0L},{(-1L),0x54L,3L}},{{0x1BL,(-1L),(-1L)},{(-1L),(-9L),8L},{0xDDL,0xDFL,0x97L},{0x9EL,8L,8L},{0xC7L,0xBBL,1L},{1L,(-7L),(-6L)},{(-9L),0x0AL,0x3FL},{(-1L),1L,0xC6L}},{{0x3DL,0x7FL,0x16L},{0xB6L,1L,0x5FL},{0x7FL,0xC0L,0x5CL},{0x99L,0xF5L,(-1L)},{(-1L),0xF5L,2L},{0x47L,0xC0L,1L},{1L,1L,2L},{0x88L,0x7FL,(-1L)}}};
                    float l_2401[7][8] = {{0x1.8p+1,(-0x1.4p-1),(-0x1.Ap+1),0x1.1p+1,(-0x3.3p+1),0x1.8p+1,0x2.EEB876p+59,0xB.F3F1FBp+6},{0x0.0p+1,(-0x1.4p-1),0x1.D4F6BCp-31,0xA.9DC0C2p+2,0x1.4p+1,0xB.F3F1FBp+6,0x3.4p+1,0xB.F3F1FBp+6},{0x1.8p+1,0x1.D4F6BCp-31,0x1.Ep-1,0x1.D4F6BCp-31,0x1.8p+1,0xC.A6956Ep-62,0x1.4p+1,0x1.Ep-1},{0xD.A2BECDp+31,0x1.8p+1,0x2.0p+1,0x2.EEB876p+59,0x6.Ep-1,0xA.9DC0C2p+2,0xC.A6956Ep-62,0x1.D4F6BCp-31},{0xF.955C2Bp+10,0x0.0p+1,0x2.0p+1,0x1.1p+1,0x0.2p-1,(-0x1.Ap+1),0x1.4p+1,0x1.4p+1},{0x6.Ep-1,0xD.9B19D5p-61,0x1.Ep-1,0x1.Ep-1,0xD.9B19D5p-61,0x6.Ep-1,0x3.4p+1,0x2.EEB876p+59},{(-0x1.Ap+1),0x0.2p-1,0x1.1p+1,0x2.0p+1,0x0.0p+1,0xF.955C2Bp+10,0x0.2p-1,0xB.F3F1FBp+6}};
                    int i, j, k;
                    (*l_2226) = l_2225;
                    (***g_762) = ((l_2185 = 0x0.BBE31Bp+58) > (((+(***g_762)) <= (safe_add_func_float_f_f(l_2316, l_2316))) >= (((safe_div_func_float_f_f(l_2319, (-l_2321))) >= ((((safe_rshift_func_uint8_t_u_u(((*g_2187) , (safe_add_func_uint16_t_u_u((*g_150), ((0xA43CL || 0x0118L) != (*g_1484))))), 7)) , l_2326) , (-0x10.4p-1)) != (*g_2187))) >= l_2266[1])));
                    if ((((*g_412) = ((safe_lshift_func_uint8_t_u_u((+((((((safe_mod_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u((**g_322), ((void*)0 == l_2334))), l_2195)) & ((!(safe_mod_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((safe_div_func_int8_t_s_s((((l_2266[1] && ((l_2151 = g_2342) ^ 0x6DL)) || (safe_lshift_func_uint8_t_u_u((**g_1483), (safe_mul_func_uint16_t_u_u(((((!(safe_mod_func_int32_t_s_s(l_2326, g_2350))) | (*g_1030)) && l_2326) > (*****g_1506)), (-5L)))))) , 0x46L), l_2149[5])) , 0UL), (***l_2247))), g_2351))) , l_2352)) ^ 0L) >= (*g_1326)) | (*g_150)) | 18446744073709551608UL)), (****g_1507))) != (*g_1326))) , (***l_2247)))
                    { /* block id: 1095 */
                        int32_t l_2361 = 0x217D301FL;
                        int32_t l_2381 = 0L;
                        int32_t l_2382 = 1L;
                        l_2370 = ((safe_mod_func_int8_t_s_s(((((safe_add_func_uint8_t_u_u((0UL < ((g_114[3] , (safe_rshift_func_uint16_t_u_s(((***l_2334) ^= 0xF157L), 10))) ^ (safe_rshift_func_int8_t_s_s(((g_166[0][6][0]++) < (safe_mul_func_int16_t_s_s(2L, (*****g_1299)))), 1)))), (*****g_1506))) , ((4294967295UL || l_2368) == (0x9CL == 255UL))) < (**l_2248)) | l_2361), (**l_2248))) == l_2369[0][0][0]);
                        (*g_412) = ((***g_1508) || 1L);
                        (*g_412) = (safe_mod_func_int16_t_s_s((***g_1301), (safe_lshift_func_uint8_t_u_u((((((!0x2.491C2Bp-87) , (l_2376 != &g_763)) , l_2266[1]) >= l_2361) && (*g_150)), ((((safe_mod_func_uint16_t_u_u((++g_1644[0]), (safe_sub_func_uint64_t_u_u(((((safe_add_func_float_f_f((**l_2248), (safe_mul_func_float_f_f((((l_2151 = l_2381) , l_2381) >= l_2391), l_2)))) >= l_2370) , (*g_1326)) & 0x3C0F94EB5A87C926LL), l_2381)))) <= l_2382) < 18446744073709551607UL) && l_2)))));
                    }
                    else
                    { /* block id: 1103 */
                        float l_2397 = 0x6.9p+1;
                        int32_t l_2398 = 6L;
                        (*g_412) = (l_2392[5][2] == l_2392[2][2]);
                        (*l_2248) = ((*l_2285) = func_12((safe_sub_func_int8_t_s_s((+(~0UL)), l_2398))));
                    }
                    for (g_10 = 0; (g_10 == 17); ++g_10)
                    { /* block id: 1110 */
                        uint8_t l_2404 = 0x03L;
                        l_2404--;
                        (*l_2285) = &l_2402;
                    }
                }
            }
            else
            { /* block id: 1115 */
                uint8_t l_2408 = 0xB3L;
                uint8_t l_2431 = 0xCDL;
                int32_t l_2440[5][6] = {{0xA1E0A5C8L,3L,(-1L),0x008C4769L,(-1L),3L},{0xA1E0A5C8L,3L,(-1L),0x008C4769L,(-1L),3L},{0xA1E0A5C8L,3L,(-1L),0x008C4769L,(-1L),3L},{0xA1E0A5C8L,3L,(-1L),0x008C4769L,(-1L),3L},{0xA1E0A5C8L,3L,(-1L),0x008C4769L,(-1L),3L}};
                int i, j;
                ++l_2408;
                (*l_2248) = (*g_1762);
                (*g_412) = (safe_div_func_int64_t_s_s((safe_unary_minus_func_int32_t_s(((l_2402 = (((safe_rshift_func_uint16_t_u_s((((**g_568) = (safe_sub_func_int8_t_s_s((safe_mul_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((0xC0CEL ^ (l_2440[2][5] &= (!(safe_rshift_func_int16_t_s_u(0L, (g_1207 |= ((safe_mod_func_int64_t_s_s((safe_add_func_int32_t_s_s((***l_2247), l_2431)), (*g_1224))) , (safe_mod_func_int8_t_s_s((safe_add_func_int64_t_s_s(((l_2185 = (safe_mul_func_uint16_t_u_u((**g_149), (safe_lshift_func_int16_t_s_s(l_2408, 6))))) != ((void*)0 == &l_2408)), 0x318F9FBBDD78444DLL)), (**g_1125)))))))))) & (**g_149)), (*g_1030))), (**l_2248))), l_2408)), (*g_1484)))) <= (-8L)), 7)) , 0x5.BF852Bp-10) , 0L)) , l_2151))), l_2369[0][0][0]));
            }
            for (l_2 = 0; (l_2 < 34); l_2 = safe_add_func_int16_t_s_s(l_2, 7))
            { /* block id: 1127 */
                float l_2463 = 0x2.B8CB4Cp-80;
                uint32_t *l_2464[5][3] = {{&g_1687,&g_235,&g_235},{&g_235,(void*)0,(void*)0},{&g_1687,&g_235,&g_235},{&g_235,(void*)0,(void*)0},{&g_1687,&g_235,&g_235}};
                uint64_t l_2468 = 18446744073709551606UL;
                int32_t l_2473 = (-9L);
                uint8_t **l_2488[9][10][2] = {{{&g_1484,(void*)0},{&g_1484,&g_1484},{&g_1484,&g_1484},{&g_1484,(void*)0},{&g_1484,(void*)0},{(void*)0,&l_2470},{&l_2470,&g_1484},{&g_1484,&g_1484},{&g_1484,&l_2470},{(void*)0,(void*)0}},{{&g_1484,&l_2470},{&g_1484,&l_2470},{&g_1484,&l_2470},{&l_2470,&g_1484},{&g_1484,(void*)0},{&g_1484,&g_1484},{&l_2470,&l_2470},{&g_1484,&l_2470},{&g_1484,&l_2470},{&g_1484,(void*)0}},{{(void*)0,&l_2470},{&g_1484,&g_1484},{&g_1484,&g_1484},{&l_2470,&l_2470},{(void*)0,(void*)0},{&g_1484,(void*)0},{&g_1484,&g_1484},{&g_1484,&g_1484},{&g_1484,(void*)0},{&g_1484,(void*)0}},{{(void*)0,&l_2470},{&l_2470,&g_1484},{&g_1484,&g_1484},{&g_1484,&l_2470},{(void*)0,(void*)0},{&g_1484,&l_2470},{&g_1484,&l_2470},{&g_1484,&l_2470},{&l_2470,&g_1484},{&g_1484,(void*)0}},{{&g_1484,&g_1484},{&l_2470,&l_2470},{&g_1484,&l_2470},{&g_1484,&l_2470},{&g_1484,(void*)0},{(void*)0,&l_2470},{&g_1484,&g_1484},{&g_1484,&g_1484},{&l_2470,&l_2470},{(void*)0,(void*)0}},{{&g_1484,&l_2470},{&g_1484,&g_1484},{&g_1484,&g_1484},{&g_1484,&l_2470},{&l_2470,&g_1484},{&l_2470,&g_1484},{&g_1484,&g_1484},{&g_1484,(void*)0},{&g_1484,&g_1484},{&g_1484,&g_1484}},{{&l_2470,&l_2470},{&g_1484,(void*)0},{&g_1484,&g_1484},{&l_2470,&g_1484},{(void*)0,&g_1484},{(void*)0,&g_1484},{&l_2470,&g_1484},{&g_1484,(void*)0},{&g_1484,&l_2470},{&l_2470,&g_1484}},{{&g_1484,&g_1484},{&g_1484,(void*)0},{&g_1484,&g_1484},{&g_1484,&g_1484},{&l_2470,&g_1484},{&l_2470,&l_2470},{&g_1484,&g_1484},{&g_1484,&g_1484},{&g_1484,&l_2470},{&l_2470,&g_1484}},{{&l_2470,&g_1484},{&g_1484,&g_1484},{&g_1484,(void*)0},{&g_1484,&g_1484},{&g_1484,&g_1484},{&l_2470,&l_2470},{&g_1484,(void*)0},{&g_1484,&g_1484},{&l_2470,&g_1484},{(void*)0,&g_1484}}};
                int32_t l_2497[4][2][2] = {{{0xB74D3BB2L,(-1L)},{0xB74D3BB2L,0xB74D3BB2L}},{{0xB74D3BB2L,(-1L)},{0xB74D3BB2L,0xB74D3BB2L}},{{0xB74D3BB2L,(-1L)},{0xB74D3BB2L,0xB74D3BB2L}},{{0xB74D3BB2L,(-1L)},{0xB74D3BB2L,0xB74D3BB2L}}};
                int i, j, k;
                l_2473 &= (safe_mul_func_int16_t_s_s(((safe_div_func_int16_t_s_s((safe_sub_func_int32_t_s_s((((safe_rshift_func_uint8_t_u_u(((*l_2470) = (safe_add_func_int64_t_s_s(0x224A2C0ACE51F4F9LL, (((safe_add_func_uint32_t_u_u((((!((((((safe_rshift_func_uint8_t_u_s(((safe_mod_func_int32_t_s_s((l_2186[8][5] = 0xEB06B7BAL), ((*g_412) ^= 0x47FCBF26L))) == (safe_div_func_int8_t_s_s(((5L || (l_2149[5] , (l_2467[0] = ((**l_2211) &= (((((*g_1506) = (*g_1506)) == (void*)0) & ((safe_unary_minus_func_int32_t_s(((l_2266[4] < ((((l_2185 = g_2342) , (void*)0) == (void*)0) , l_2465)) , (-1L)))) && l_2465)) ^ l_2466[2]))))) >= 4294967291UL), l_2468))), 6)) < l_2469) != g_1023.f2) ^ l_2468) , g_1436) , l_2149[2])) , &g_1487) != (void*)0), l_2468)) , l_2470) != &l_2466[4])))), l_2471)) > l_2468) & l_2469), l_2472)), l_2468)) == l_2402), (**g_288)));
                l_2497[2][0][1] &= (l_2402 = (((0x4837L < (((safe_lshift_func_int8_t_s_s(1L, 0)) == (*g_1224)) == (safe_div_func_int16_t_s_s(((safe_mul_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s((safe_div_func_uint8_t_u_u((*g_1484), ((l_2266[1] <= (l_2488[3][7][0] == (***g_1506))) & 251UL))), (((safe_add_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((((**g_149) = (safe_sub_func_int8_t_s_s((l_2368 | l_2402), 0x6DL))) < g_2495), (**g_568))), 65531UL)) , (*g_412)) , (**g_288)))), 7)), l_2473)) > (*g_412)), (**g_568))))) <= g_1384) < l_2496));
                (*l_2248) = (l_2498 = &l_2497[2][0][1]);
            }
            l_2151 ^= (-3L);
        }
        for (g_183 = 0; (g_183 > 20); g_183 = safe_add_func_int64_t_s_s(g_183, 4))
        { /* block id: 1146 */
            (*g_2502) = l_2501[0][3][0];
        }
    }
    else
    { /* block id: 1149 */
        uint32_t l_2513[2][3];
        uint64_t **l_2540 = &g_768[2][5][0];
        int64_t *** const *l_2542 = &l_2301;
        float l_2543 = 0xE.D42F6Dp+36;
        uint8_t l_2544[4][8][8] = {{{0x59L,0xCBL,0xCBL,0x59L,0xF8L,255UL,0xCBL,251UL},{251UL,2UL,1UL,0x27L,0xF8L,0xCBL,2UL,0x27L},{0x59L,2UL,255UL,251UL,251UL,255UL,2UL,0x59L},{255UL,0xCBL,1UL,251UL,255UL,0x4AL,0xCBL,0x27L},{255UL,0x4AL,0xCBL,0x27L,251UL,0x4AL,0x4AL,251UL},{0x59L,0xCBL,0xCBL,0x59L,0xF8L,255UL,0xCBL,251UL},{251UL,2UL,1UL,0x27L,0xF8L,0xCBL,2UL,0x27L},{0x59L,2UL,255UL,251UL,251UL,255UL,2UL,0x59L}},{{255UL,0xCBL,1UL,251UL,255UL,0x4AL,0xCBL,0x27L},{255UL,0x4AL,0xCBL,0x27L,251UL,0x4AL,0x4AL,251UL},{0x59L,0xCBL,0xCBL,0x59L,0xF8L,255UL,0xCBL,251UL},{251UL,2UL,1UL,0x27L,0xF8L,0xCBL,2UL,0x27L},{0x27L,0x4AL,1UL,0x59L,0x59L,1UL,0x4AL,0x27L},{251UL,255UL,2UL,0x59L,251UL,0xCBL,255UL,0xF8L},{251UL,0xCBL,255UL,0xF8L,0x59L,0xCBL,0xCBL,0x59L},{0x27L,255UL,255UL,0x27L,255UL,1UL,255UL,0x59L}},{{0x59L,0x4AL,2UL,0xF8L,255UL,255UL,0x4AL,0xF8L},{0x27L,0x4AL,1UL,0x59L,0x59L,1UL,0x4AL,0x27L},{251UL,255UL,2UL,0x59L,251UL,0xCBL,255UL,0xF8L},{251UL,0xCBL,255UL,0xF8L,0x59L,0xCBL,0xCBL,0x59L},{0x27L,255UL,255UL,0x27L,255UL,1UL,255UL,0x59L},{0x59L,0x4AL,2UL,0xF8L,255UL,255UL,0x4AL,0xF8L},{0x27L,0x4AL,1UL,0x59L,0x59L,1UL,0x4AL,0x27L},{251UL,255UL,2UL,0x59L,251UL,0xCBL,255UL,0xF8L}},{{251UL,0xCBL,255UL,0xF8L,0x59L,0xCBL,0xCBL,0x59L},{0x27L,255UL,255UL,0x27L,255UL,1UL,255UL,0x59L},{0x59L,0x4AL,2UL,0xF8L,255UL,255UL,0x4AL,0xF8L},{0x27L,0x4AL,1UL,0x59L,0x59L,1UL,0x4AL,0x27L},{251UL,255UL,2UL,0x59L,251UL,0xCBL,255UL,0xF8L},{251UL,0xCBL,255UL,0xF8L,0x59L,0xCBL,0xCBL,0x59L},{0x27L,255UL,255UL,0x27L,255UL,1UL,255UL,0x59L},{0x59L,0x4AL,2UL,0xF8L,255UL,255UL,0x4AL,0xF8L}}};
        int32_t l_2545 = (-1L);
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 3; j++)
                l_2513[i][j] = 0UL;
        }
        (*l_2498) = ((*g_412) = ((((1L != ((void*)0 != l_2505)) , &g_1487) == (void*)0) || (safe_mod_func_int8_t_s_s((((((!(safe_rshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s(l_2513[0][2], (*l_2498))), 5))) == ((((**g_1223) < (safe_mod_func_int8_t_s_s(((-10L) || (*l_2498)), l_2513[0][2]))) & l_2513[0][2]) , (*l_2498))) < l_2513[0][2]) >= 0x05L) != 0x54L), (***g_1508)))));
        l_2545 &= (((((((safe_mul_func_int16_t_s_s((l_2513[0][0] , ((safe_lshift_func_uint16_t_u_u(((!(safe_sub_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_s(((safe_mod_func_uint64_t_u_u((((safe_sub_func_uint16_t_u_u(((void*)0 != (***g_1299)), 1UL)) , ((safe_div_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((safe_mod_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((***g_1301), (safe_rshift_func_int16_t_s_u((2L || (((((+(&g_1854 == ((*l_2540) = (void*)0))) < ((g_2541 = g_2541) != l_2542)) > (*l_2498)) | 0xB446BC24EE8C89BFLL) , 247UL)), 2)))), l_2513[0][2])) != 0x83L), (*****g_1506))), l_2513[1][0])) , (**g_1223))) || l_2513[0][2]), 0x13C6F1A316A082EELL)) >= g_35), 6)) != (-1L)), (**g_149)))) && (*l_2498)), 14)) | (**g_1483))), l_2513[0][2])) || 0x063F45D9L) , l_2544[2][7][2]) , (*l_2542)) == &g_1223) || l_2513[0][2]) ^ 0x97DDL);
        return l_2513[0][2];
    }
    (*g_412) |= ((safe_lshift_func_uint16_t_u_s((+(l_2549 , l_2266[3])), 3)) , (((**g_288) ^= (((((safe_sub_func_uint32_t_u_u(0UL, g_658.f0)) , (l_2552 == (((safe_sub_func_uint16_t_u_u((safe_mul_func_uint32_t_u_u(0UL, ((safe_lshift_func_int16_t_s_u((l_2559 || (**g_1125)), 9)) <= 8L))), l_2149[4])) | 0x51628BCBF17AA391LL) , (void*)0))) | 65535UL) == l_2560) <= (**g_568))) & 0xC304L));
lbl_2562:
    (*g_2561) ^= (*g_412);
    for (l_2549 = 0; (l_2549 <= 38); l_2549++)
    { /* block id: 1163 */
        uint32_t l_2566[10] = {0x98C434F5L,0x98C434F5L,0x9A016368L,0x98C434F5L,0x98C434F5L,0x9A016368L,0x98C434F5L,0x98C434F5L,0x9A016368L,0x98C434F5L};
        int32_t l_2572 = 0x550F6F41L;
        struct S0 *l_2587 = &g_2588;
        struct S0 **l_2589 = &g_1953;
        struct S0 **l_2590 = &g_543;
        uint16_t l_2593[4][4][8] = {{{0xA8B5L,0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L},{0xA8B5L,0xA8B5L,0xDC49L,0xA8B5L,0xA8B5L,0xDC49L,0xA8B5L,0xA8B5L},{0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L,0xA8B5L,0xDC49L,0xDC49L,0xEBF6L},{0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L}},{{0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L},{0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L},{0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L},{0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L}},{{0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L},{0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L},{0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L},{0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L}},{{0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L},{0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L,0xA8B5L,0xEBF6L,0xEBF6L},{0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L},{0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L,0xDC49L,0xEBF6L,0xDC49L}}};
        int16_t l_2617 = 1L;
        int32_t *l_2618 = (void*)0;
        int64_t l_2625 = 0x5579CB3B1B7D4836LL;
        uint64_t *l_2643 = &g_2350;
        const int32_t l_2701 = 0x04DC5837L;
        int8_t ***l_2708 = (void*)0;
        uint16_t *l_2720[10] = {&l_2266[2],&g_1644[0],&l_2266[1],&g_1644[0],&l_2266[2],&l_2266[2],&g_1644[0],&l_2266[1],&g_1644[0],&l_2266[2]};
        int16_t ***l_2726[9] = {&g_568,&g_568,&g_568,&g_568,&g_568,&g_568,&g_568,&g_568,&g_568};
        float l_2727 = 0x1.3DE512p-14;
        int i, j, k;
        l_2565[1] = &l_2151;
        ++l_2566[1];
        if ((safe_div_func_int64_t_s_s(((((**g_1762) , 0x4015DAF2L) & (l_2572 = (l_2571[0] = l_2566[8]))) == (((((((*g_150) || (safe_rshift_func_int8_t_s_s((safe_div_func_uint64_t_u_u(((safe_mod_func_uint32_t_u_u((safe_mod_func_int8_t_s_s((((safe_lshift_func_int16_t_s_u((-2L), 15)) >= (safe_mod_func_uint32_t_u_u(4294967294UL, (safe_add_func_int64_t_s_s((((9L <= ((((*l_2589) = l_2587) != ((*l_2590) = g_543)) ^ l_2566[7])) , (**g_288)) != (**g_568)), 4UL))))) && 0x1DL), 0xDFL)), 0xDD677936L)) & l_2566[1]), l_2566[1])), l_2566[1]))) , l_2591) , l_2566[1]) & l_2592) > l_2566[1]) < l_2593[1][1][6])), l_2566[5])))
        { /* block id: 1170 */
            int8_t l_2596[2][1][1];
            int64_t *l_2600 = &g_1295;
            int16_t l_2640 = 2L;
            int32_t l_2656[9];
            uint64_t **l_2684 = (void*)0;
            uint64_t ***l_2683[7] = {&l_2684,&l_2684,&l_2684,&l_2684,&l_2684,&l_2684,&l_2684};
            int32_t l_2703[2][5][3] = {{{0L,0L,0L},{4L,4L,4L},{0L,0L,0L},{4L,4L,4L},{0L,0L,0L}},{{4L,4L,4L},{0L,0L,0L},{4L,4L,4L},{0L,0L,0L},{4L,4L,4L}}};
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_2596[i][j][k] = 0x56L;
                }
            }
            for (i = 0; i < 9; i++)
                l_2656[i] = 0xBF0078EFL;
            for (g_886 = 9; (g_886 > 3); g_886 = safe_sub_func_uint16_t_u_u(g_886, 4))
            { /* block id: 1173 */
                uint32_t l_2615 = 0x4D9230C9L;
                int32_t l_2621 = 0xEBC9D988L;
                int16_t l_2634 = (-3L);
                int16_t l_2655 = 0x847FL;
            }
            (*l_2589) = (*g_2502);
        }
        else
        { /* block id: 1217 */
            uint32_t l_2704 = 1UL;
            for (l_2702 = 0; (l_2702 <= 3); l_2702 += 1)
            { /* block id: 1220 */
                uint64_t l_2705 = 0UL;
                int8_t ***l_2711 = &g_1790[0];
                (*g_2187) = (*g_2187);
                l_2704 &= (*g_670);
                for (l_2667 = 0; (l_2667 <= 3); l_2667 += 1)
                { /* block id: 1225 */
                    int8_t ****l_2709[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_2709[i] = &l_2708;
                    ++l_2705;
                    l_2711 = (g_2710 = l_2708);
                    (*g_412) = (-6L);
                }
            }
            if (l_2704)
                continue;
        }
        (*g_2728) = &l_2572;
    }
    return (*g_1126);
}


/* ------------------------------------------ */
/* 
 * reads : g_10 g_951 g_1482 g_1483 g_1484 g_173 g_1326 g_1327 g_1224 g_1225 g_568 g_569 g_412 g_1762 g_1417 g_296 g_322 g_323 g_324 g_500 g_1299 g_1300 g_1301 g_543 g_546 g_544 g_1507 g_1508 g_645 g_35 g_800.f2 g_1384 g_762 g_763 g_886 g_2029 g_288 g_150 g_46 g_299 g_1026 g_1027 g_644 g_2115 g_1506 g_1295 g_1687 g_1955
 * writes: g_10 g_299 g_296 g_951 g_1295 g_1687 g_87 g_90 g_166 g_543 g_1506 g_500 g_886 g_1379 g_46 g_173
 */
static int32_t * func_4(uint32_t  p_5)
{ /* block id: 3 */
    uint8_t l_9[5] = {0x22L,0x22L,0x22L,0x22L,0x22L};
    int32_t *l_22[2][7] = {{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10}};
    int32_t l_1760 = (-1L);
    int32_t *l_1761 = &g_951;
    int32_t l_1870 = 0x055C1D94L;
    uint64_t l_1890[2][5][4] = {{{18446744073709551615UL,0UL,2UL,0xFE76D85C82A684A2LL},{1UL,18446744073709551615UL,1UL,0UL},{1UL,18446744073709551615UL,18446744073709551615UL,0xFE76D85C82A684A2LL},{0x3AAAD10DB9160E73LL,0UL,0UL,0x3AAAD10DB9160E73LL},{2UL,1UL,8UL,0UL}},{{8UL,0UL,0x0F451CD1CF68ACA7LL,18446744073709551615UL},{0x90E2F1F0448AAC4ALL,0x1D6845E0455C1759LL,0x8430B5D0A279ABB0LL,18446744073709551615UL},{0x7EC7626E882C62C3LL,0UL,0x90E2F1F0448AAC4ALL,0UL},{1UL,1UL,0x4C273C0FD1F793DCLL,0x3AAAD10DB9160E73LL},{0x0F451CD1CF68ACA7LL,0UL,0x7EC7626E882C62C3LL,0xFE76D85C82A684A2LL}}};
    float *l_1912 = &g_1379;
    float **l_1911 = &l_1912;
    int16_t l_1992 = 3L;
    uint32_t l_2013[10][3][6] = {{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,0xE19DC145L,0x6AFDF78CL,4UL}},{{0x6592CC65L,4UL,4294967291UL,0x6592CC65L,0x6AFDF78CL,0x6AFDF78CL},{1UL,4UL,4UL,1UL,0x6AFDF78CL,4294967291UL},{0xE19DC145L,4UL,0x6AFDF78CL,4294967291UL,0xA3E357B4L,0x8EDED18DL}},{{0x6AFDF78CL,0x8EDED18DL,0UL,0x6AFDF78CL,0xA3E357B4L,0xA3E357B4L},{4UL,0x8EDED18DL,0x8EDED18DL,4UL,0xA3E357B4L,0UL},{4294967291UL,0x8EDED18DL,0xA3E357B4L,4294967291UL,0xA3E357B4L,0x8EDED18DL}}};
    uint32_t l_2018 = 0x7904A710L;
    int32_t l_2030 = 4L;
    float l_2031 = (-0x6.2p-1);
    struct S0 * const l_2069 = &g_1955;
    int32_t l_2085 = 1L;
    int32_t *l_2125 = (void*)0;
    int i, j, k;
    for (p_5 = 6; (p_5 < 30); p_5 = safe_add_func_uint32_t_u_u(p_5, 4))
    { /* block id: 6 */
        int32_t *l_25 = &g_10;
        int32_t *l_29[1][10][9] = {{{&g_10,(void*)0,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,(void*)0},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,(void*)0,&g_10,(void*)0},{&g_10,&g_10,(void*)0,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_10,&g_10,&g_10,(void*)0,(void*)0,&g_10,&g_10,&g_10,&g_10},{&g_10,(void*)0,&g_10,&g_10,&g_10,&g_10,&g_10,(void*)0,&g_10},{&g_10,&g_10,(void*)0,&g_10,&g_10,&g_10,&g_10,(void*)0,&g_10},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,(void*)0,&g_10},{&g_10,&g_10,&g_10,(void*)0,&g_10,&g_10,&g_10,(void*)0,(void*)0},{&g_10,&g_10,&g_10,&g_10,(void*)0,&g_10,&g_10,&g_10,(void*)0},{&g_10,(void*)0,(void*)0,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10}}};
        int64_t l_1834 = 0x39174981BC0D3886LL;
        uint8_t *l_1848 = &g_1248[5];
        float **l_1863 = (void*)0;
        float ***l_1862[7][7][5] = {{{&l_1863,(void*)0,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,(void*)0,&l_1863,(void*)0,&l_1863},{(void*)0,(void*)0,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863}},{{&l_1863,(void*)0,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{(void*)0,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{&l_1863,&l_1863,(void*)0,(void*)0,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863}},{{(void*)0,(void*)0,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,(void*)0,&l_1863,(void*)0},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,(void*)0}},{{&l_1863,&l_1863,(void*)0,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,(void*)0},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{(void*)0,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863}},{{&l_1863,&l_1863,(void*)0,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{(void*)0,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863}},{{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{(void*)0,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,(void*)0},{&l_1863,&l_1863,(void*)0,&l_1863,(void*)0},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{(void*)0,(void*)0,&l_1863,&l_1863,&l_1863},{&l_1863,(void*)0,&l_1863,&l_1863,&l_1863}},{{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,(void*)0,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,(void*)0,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863},{&l_1863,&l_1863,&l_1863,&l_1863,&l_1863}}};
        int32_t l_1874 = 5L;
        int32_t *l_1875[2];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1875[i] = &g_90;
        for (g_10 = 0; (g_10 <= 4); g_10 += 1)
        { /* block id: 9 */
            int32_t *l_14 = &g_10;
            int64_t *l_1292 = &g_171;
            uint8_t *l_1293 = &g_1248[3];
            int64_t *l_1294[8][8] = {{(void*)0,(void*)0,(void*)0,(void*)0,&g_1295,&g_1295,&g_1295,&g_1295},{&g_1295,(void*)0,&g_1295,&g_1295,&g_1295,(void*)0,&g_1295,&g_1295},{&g_1295,(void*)0,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295},{(void*)0,&g_1295,(void*)0,(void*)0,&g_1295,&g_1295,&g_1295,&g_1295},{&g_1295,&g_1295,(void*)0,&g_1295,&g_1295,(void*)0,&g_1295,&g_1295},{&g_1295,&g_1295,&g_1295,(void*)0,(void*)0,&g_1295,&g_1295,&g_1295},{(void*)0,(void*)0,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295},{(void*)0,&g_1295,(void*)0,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295}};
            int32_t l_1296[10] = {0x773D7DEDL,0x96D0ED4DL,0x9521D782L,0x9521D782L,0x96D0ED4DL,0x773D7DEDL,0x96D0ED4DL,0x9521D782L,0x9521D782L,0x96D0ED4DL};
            int16_t l_1832 = 0L;
            uint64_t l_1836[2];
            int64_t l_1889 = 0x5515DEA639C6697DLL;
            int i, j;
            for (i = 0; i < 2; i++)
                l_1836[i] = 1UL;
        }
    }
lbl_1994:
    if (l_1890[0][0][1])
    { /* block id: 892 */
        l_22[1][2] = &l_1870;
        if (g_10)
            goto lbl_1994;
    }
    else
    { /* block id: 894 */
        int16_t l_1897 = 0x1460L;
        int32_t l_1900 = 0x1CD0CCB7L;
        const uint8_t l_1905 = 0x40L;
        const float *l_1910 = &g_1023.f1;
        const float ** const l_1909[7][8][4] = {{{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,(void*)0,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,(void*)0},{&l_1910,&l_1910,&l_1910,(void*)0},{&l_1910,&l_1910,&l_1910,&l_1910}},{{(void*)0,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,(void*)0,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{(void*)0,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910}},{{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,(void*)0,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,(void*)0},{&l_1910,&l_1910,&l_1910,(void*)0}},{{&l_1910,&l_1910,&l_1910,&l_1910},{(void*)0,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,(void*)0,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{(void*)0,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910}},{{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{(void*)0,&l_1910,&l_1910,(void*)0},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910}},{{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,(void*)0,(void*)0},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,(void*)0,&l_1910},{&l_1910,(void*)0,&l_1910,&l_1910}},{{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{&l_1910,(void*)0,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{(void*)0,&l_1910,&l_1910,&l_1910},{&l_1910,&l_1910,&l_1910,&l_1910},{(void*)0,&l_1910,&l_1910,(void*)0},{&l_1910,&l_1910,&l_1910,&l_1910}}};
        float ***l_1913 = (void*)0;
        float ***l_1914 = &l_1911;
        int8_t * const *l_1915 = &g_1791[2][6][0];
        int32_t **l_1921 = &g_412;
        int32_t l_1925 = 0x868ACCB8L;
        int32_t l_1932 = (-1L);
        int32_t l_1933[9][8] = {{2L,0xE1819B48L,2L,(-1L),(-5L),0x9100222AL,(-10L),(-10L)},{(-10L),0x43B0F5CAL,9L,9L,0x43B0F5CAL,(-10L),(-5L),0x5F78A96DL},{(-10L),8L,(-1L),0x43B0F5CAL,(-5L),0x43B0F5CAL,(-1L),8L},{2L,(-1L),0x9100222AL,0x43B0F5CAL,0x5F78A96DL,(-1L),(-1L),0x5F78A96DL},{9L,0x5F78A96DL,0x5F78A96DL,9L,2L,8L,(-1L),(-10L)},{(-1L),9L,0x9100222AL,(-1L),0x9100222AL,9L,(-1L),(-5L)},{0x9100222AL,9L,(-1L),(-5L),8L,8L,(-5L),(-1L)},{0x5F78A96DL,0x5F78A96DL,9L,2L,8L,(-1L),(-10L),(-1L)},{0x9100222AL,(-1L),2L,(-1L),0x9100222AL,0x43B0F5CAL,0x5F78A96DL,(-1L)}};
        uint64_t l_1957 = 0UL;
        uint8_t *****l_1966 = &g_1507;
        int i, j, k;
        (*l_1761) = ((safe_mul_func_int8_t_s_s((*l_1761), (safe_lshift_func_uint8_t_u_s((***g_1482), ((*g_1326) | l_1897))))) , ((*g_412) = (((l_1900 = ((**g_568) = (0x4214L & (safe_div_func_int32_t_s_s(((*g_1224) , 1L), 1L))))) | (((((safe_rshift_func_uint16_t_u_s((safe_mul_func_uint16_t_u_u((p_5 && p_5), 1UL)), p_5)) > p_5) != 0x595AAC9BE36FA566LL) >= 0x9E34D9F11B3A7910LL) <= 0x4A8EBB5BL)) >= l_1905)));
        if (((*l_1761) < (((7L & (*l_1761)) & (-3L)) != ((~(safe_mod_func_uint16_t_u_u((((g_1327[0][8][0] , l_1909[2][1][2]) != ((*l_1914) = l_1911)) || ((l_1915 == &g_1791[3][2][0]) && (*l_1761))), 65535UL))) > 0xD09F3888L))))
        { /* block id: 900 */
            return (*g_1762);
        }
        else
        { /* block id: 902 */
            int32_t *l_1916 = &g_500;
            float l_1924[5] = {0xE.182816p-24,0xE.182816p-24,0xE.182816p-24,0xE.182816p-24,0xE.182816p-24};
            int32_t l_1926[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1926[i] = (-1L);
            for (g_1295 = 5; (g_1295 >= 2); g_1295 -= 1)
            { /* block id: 905 */
                return l_1916;
            }
            for (g_1687 = 5; (g_1687 >= 12); g_1687 = safe_add_func_int8_t_s_s(g_1687, 5))
            { /* block id: 910 */
                int32_t *l_1919 = &g_298;
                return l_1919;
            }
            for (g_951 = 0; (g_951 <= 9); g_951 += 1)
            { /* block id: 915 */
                int32_t **l_1920 = &l_22[0][1];
                int32_t l_1922 = (-1L);
                int32_t l_1923 = 7L;
                int32_t l_1930 = 0x9A8371A9L;
                int32_t l_1931[4][9][7] = {{{0x18A972BFL,0x79D60494L,0xFE517E33L,0x0436DDABL,7L,(-3L),0x91E29988L},{8L,0x7C0D85AEL,(-6L),0x0436DDABL,(-6L),0x7C0D85AEL,8L},{0x7C0D85AEL,8L,0xA4B35B72L,(-7L),0xACBE4A64L,(-6L),0x206822AFL},{0L,0xACBE4A64L,0xD14D5660L,0L,0x18A972BFL,0x83327D55L,0x0436DDABL},{8L,0L,0xA4B35B72L,0x18A972BFL,0x954E45C7L,0xFE517E33L,(-6L)},{0x954E45C7L,0xD5812F4AL,0xAAF882D5L,1L,0xFBB73320L,(-1L),(-1L)},{0xA4B35B72L,0xD5812F4AL,8L,0xD5812F4AL,0xA4B35B72L,0L,0xFBB73320L},{(-1L),(-3L),0xF850BEC4L,0x7C0D85AEL,0x83327D55L,1L,0xD5812F4AL},{9L,0xFBB73320L,0xD14D5660L,0x954E45C7L,(-6L),(-1L),0xFE517E33L}},{{(-1L),0x7C0D85AEL,0xACBE4A64L,0x206822AFL,0x7C0D85AEL,0x18A972BFL,0L},{0xA4B35B72L,0xD14D5660L,0xF850BEC4L,0L,(-3L),(-3L),0L},{0x954E45C7L,(-7L),0x954E45C7L,(-1L),0L,0xAAF882D5L,0xFE517E33L},{0xD5812F4AL,0x206822AFL,0x91E29988L,0x1E3D3D51L,0xA4B35B72L,0xACBE4A64L,0xD5812F4AL},{0x206822AFL,1L,0xFE517E33L,0L,9L,0xAAF882D5L,0xFBB73320L},{0xD14D5660L,0xFBB73320L,9L,0xD14D5660L,0x19F4748CL,(-3L),(-1L)},{0xD5812F4AL,(-6L),0x18A972BFL,0x19F4748CL,0x19F4748CL,0x18A972BFL,(-6L)},{0x19F4748CL,0xF850BEC4L,0xAAF882D5L,0x7C0D85AEL,9L,(-1L),0x1E3D3D51L},{0xAAF882D5L,0xD5812F4AL,0x954E45C7L,0xF850BEC4L,0xA4B35B72L,1L,9L}},{{(-1L),(-6L),0x79D60494L,0x7C0D85AEL,0L,0L,0xD5812F4AL},{0xFBB73320L,9L,0xD14D5660L,0x19F4748CL,(-3L),(-1L),(-1L)},{0xFE517E33L,0x7C0D85AEL,0xD14D5660L,0xD14D5660L,0x7C0D85AEL,0xFE517E33L,0x83327D55L},{0xA4B35B72L,0x206822AFL,0x79D60494L,0L,(-6L),0x7EE452DAL,0L},{0x19F4748CL,0xC53EE8B1L,0x954E45C7L,0x1E3D3D51L,0x83327D55L,0xAAF882D5L,(-1L)},{0xF850BEC4L,0x206822AFL,0xAAF882D5L,(-1L),0xA4B35B72L,0xD14D5660L,0xF850BEC4L},{0x206822AFL,0x7C0D85AEL,0x18A972BFL,0L,0xFBB73320L,0x91E29988L,0xFBB73320L},{0x206822AFL,9L,9L,0x206822AFL,0x954E45C7L,(-3L),0x1E3D3D51L},{0xF850BEC4L,(-6L),0xFE517E33L,0x954E45C7L,0x19F4748CL,0xFE517E33L,(-3L)}},{{0x19F4748CL,0xD5812F4AL,0x91E29988L,0x7C0D85AEL,0xFBB73320L,8L,0x1E3D3D51L},{0xA4B35B72L,0xF850BEC4L,0x954E45C7L,0xD5812F4AL,0xAAF882D5L,1L,0xFBB73320L},{0xFE517E33L,(-6L),0xF850BEC4L,1L,0L,1L,0xF850BEC4L},{0xFBB73320L,0xFBB73320L,0xACBE4A64L,0x19F4748CL,(-6L),8L,(-1L)},{(-1L),1L,0xD14D5660L,0x206822AFL,1L,8L,(-3L)},{0x18A972BFL,9L,7L,0x7EE452DAL,0xAAF882D5L,0x91E29988L,0x7EE452DAL},{1L,0x83327D55L,0x7C0D85AEL,0xF850BEC4L,(-3L),(-1L),0x954E45C7L},{0xC53EE8B1L,0x0436DDABL,0x18A972BFL,0xF850BEC4L,0x18A972BFL,0x0436DDABL,0xC53EE8B1L},{0x0436DDABL,0xD14D5660L,8L,0x7EE452DAL,(-1L),0x18A972BFL,8L}}};
                uint64_t l_1934 = 0xCEA4E7BFC2C20525LL;
                float *l_1990[2];
                uint32_t *l_1991[5][4][7] = {{{&g_1687,&g_886,(void*)0,&g_235,&g_1687,&g_235,(void*)0},{&g_1687,&g_886,&g_1687,&g_886,&g_235,&g_886,&g_886},{&g_886,&g_886,&g_235,&g_886,&g_886,&g_1687,&g_1687},{&g_1687,&g_886,&g_235,&g_235,&g_235,&g_235,&g_235}},{{&g_1687,&g_886,&g_1687,&g_886,&g_1687,(void*)0,&g_1687},{&g_886,&g_1687,(void*)0,&g_235,&g_235,&g_235,&g_235},{&g_1687,&g_235,&g_886,(void*)0,(void*)0,(void*)0,&g_235},{&g_235,&g_1687,&g_1687,&g_235,&g_886,&g_235,(void*)0}},{{(void*)0,&g_1687,&g_235,&g_886,&g_1687,&g_1687,&g_886},{&g_1687,&g_1687,&g_886,&g_886,&g_235,&g_886,&g_886},{&g_1687,&g_1687,&g_235,(void*)0,&g_235,&g_235,&g_886},{&g_886,&g_235,&g_886,(void*)0,(void*)0,&g_235,&g_1687}},{{(void*)0,&g_1687,&g_1687,&g_1687,&g_235,&g_886,&g_235},{(void*)0,&g_886,&g_886,&g_1687,&g_235,&g_235,&g_235},{&g_235,&g_886,&g_1687,(void*)0,&g_1687,&g_235,(void*)0},{&g_235,&g_886,&g_1687,&g_886,&g_886,&g_1687,&g_886}},{{(void*)0,&g_886,&g_886,&g_1687,(void*)0,&g_886,&g_235},{(void*)0,&g_886,&g_1687,&g_1687,&g_235,&g_886,&g_1687},{&g_886,&g_235,&g_1687,&g_1687,&g_1687,&g_1687,(void*)0},{&g_1687,&g_1687,&g_235,&g_886,&g_235,(void*)0,&g_235}}};
                uint32_t *l_1993 = &g_166[0][6][0];
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_1990[i] = &g_482;
                if ((l_1920 != ((*g_1417) = l_1921)))
                { /* block id: 917 */
                    (**g_1762) &= p_5;
                }
                else
                { /* block id: 919 */
                    int8_t l_1927 = 0x31L;
                    int32_t l_1928 = 0xF9DC9414L;
                    int32_t l_1929[2][9] = {{0x8D78AC79L,0xCA1DE5EFL,0L,0L,0xCA1DE5EFL,0x8D78AC79L,0x79AECACCL,0xCA1DE5EFL,0x79AECACCL},{0x8D78AC79L,0xCA1DE5EFL,0L,0L,0xCA1DE5EFL,0x8D78AC79L,0x79AECACCL,0xCA1DE5EFL,0x79AECACCL}};
                    int i, j;
                    --l_1934;
                }
                for (g_90 = 0; (g_90 <= 9); g_90 += 1)
                { /* block id: 924 */
                    int32_t l_1949[9] = {4L,4L,4L,4L,4L,4L,4L,4L,4L};
                    uint32_t *l_1950 = &g_166[0][6][0];
                    struct S0 *l_1954[2][2][5] = {{{&g_544,&g_1955,&g_544,&g_544,&g_1955},{&g_544,(void*)0,(void*)0,&g_544,(void*)0}},{{&g_1955,&g_1955,&g_546,&g_1955,&g_1955},{(void*)0,&g_544,(void*)0,(void*)0,&g_544}}};
                    int32_t l_1956 = 0x47F9DD7FL;
                    int i, j, k;
                    (*g_412) |= ((p_5 != 0x73L) & (((0x4E23F61EL == ((*l_1950) = (safe_sub_func_uint16_t_u_u((((((**g_322) , (safe_mul_func_int16_t_s_s((safe_lshift_func_int8_t_s_s((*l_1916), ((65535UL > (safe_rshift_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u(((*****g_1299) != (safe_rshift_func_int16_t_s_u((l_1949[0] |= 0x4A76L), (((**l_1920) < p_5) != p_5)))), p_5)), 15))) , (-3L)))), 0xAE57L))) == p_5) ^ 0x5308706EL) && (-6L)), p_5)))) , p_5) == p_5));
                    for (g_299 = 8; (g_299 >= 2); g_299 -= 1)
                    { /* block id: 930 */
                        struct S0 **l_1951 = &g_543;
                        l_1954[1][0][4] = ((*l_1951) = &g_546);
                    }
                    --l_1957;
                }
                l_1925 = (((*l_1993) = (safe_mul_func_int16_t_s_s(((**l_1920) ^= ((**g_568) = (p_5 > (((((safe_mul_func_int8_t_s_s((safe_div_func_int32_t_s_s(((p_5 , 0L) & ((*g_543) , (((g_1506 = l_1966) != &g_1507) | (safe_mod_func_int32_t_s_s((safe_div_func_uint32_t_u_u((((safe_sub_func_int32_t_s_s(((((safe_sub_func_uint8_t_u_u((((0xE3DEF98FE76E5863LL & ((((*l_1916) = (safe_div_func_int16_t_s_s((+(safe_rshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(((**l_1921) = (((safe_mul_func_uint16_t_u_u((safe_unary_minus_func_uint8_t_u(((((((safe_add_func_uint64_t_u_u((1L <= (safe_unary_minus_func_int64_t_s((safe_lshift_func_uint8_t_u_s((((void*)0 == (***l_1966)) <= p_5), 6))))), 0x3F99CCE772B2773BLL)) , (void*)0) != l_1990[1]) ^ p_5) < (-1L)) , (*g_645)))), 65528UL)) , g_800.f2) & 0x7B4B13B1L)), p_5)), p_5))), 0x8694L))) , (void*)0) != l_1920)) && l_1992) >= 0x0B6CL), 0x40L)) == 0UL) , 0xC9L) != p_5), (-1L))) ^ p_5) < (*l_1761)), g_1384)), 0xB7805BE1L))))), 5L)), 0x8CL)) & p_5) , (*g_762)) == &g_483[5][1]) >= 0x957EL)))), 0xDAE1L))) != 0x5C669638L);
                if (p_5)
                    continue;
            }
        }
    }
    for (g_886 = 20; (g_886 < 22); g_886++)
    { /* block id: 950 */
        int8_t l_1997 = 0xAFL;
        uint64_t l_1999[5] = {18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL,18446744073709551608UL};
        int16_t * const *l_2003[1][8] = {{(void*)0,(void*)0,&g_569[2][1][1],(void*)0,(void*)0,&g_569[2][1][1],(void*)0,(void*)0}};
        int16_t * const **l_2002 = &l_2003[0][2];
        int32_t *l_2004 = &l_1760;
        uint64_t l_2019 = 0x6D2AF94C7072A908LL;
        int64_t l_2028 = 0x03BF46E384BFB23DLL;
        int16_t l_2032[9][6] = {{0x9932L,0x5E66L,1L,0xFE46L,1L,0x5E66L},{0xFE46L,1L,0x5E66L,0x9932L,1L,1L},{0x5E66L,0x809BL,0x70DAL,3L,0x4BB7L,1L},{0x4BB7L,0x809BL,5L,0x70DAL,1L,1L},{0xDCCBL,1L,6L,6L,1L,0xDCCBL},{1L,0x5E66L,3L,1L,6L,0x9932L},{0xA087L,0x70DAL,9L,(-1L),0x92B6L,5L},{0xA087L,5L,(-1L),1L,0x809BL,0x92B6L},{1L,6L,0x4BB7L,6L,1L,9L}};
        uint64_t *l_2033 = &l_1999[1];
        uint64_t *l_2034[8][1][2] = {{{&g_302[1],&l_2019}},{{&g_302[0],&g_302[0]}},{{&g_302[0],&l_2019}},{{&g_302[1],&g_302[1]}},{{&l_2019,&g_302[1]}},{{&g_302[1],&l_2019}},{{&g_302[0],&g_302[0]}},{{&g_302[0],&l_2019}}};
        int32_t l_2035 = 0x12553DBCL;
        int32_t l_2036 = 0x879278D9L;
        int32_t l_2037[4] = {0x3202752DL,0x3202752DL,0x3202752DL,0x3202752DL};
        uint16_t **l_2065[1];
        uint8_t l_2098 = 0UL;
        const uint32_t l_2120 = 4294967295UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_2065[i] = (void*)0;
        (*g_412) = ((0x6D30CEB9L & ((l_1997 = ((**g_568) = 0x1DB8L)) , ((+((l_1999[3] = ((((*g_1417) = &l_22[0][1]) != (void*)0) == 4294967286UL)) , ((*g_1326) == (safe_lshift_func_int16_t_s_s(((*g_1300) == (p_5 , l_2002)), p_5))))) || 0xB968BF005C69F8C2LL))) < p_5);
        l_22[0][1] = l_2004;
        if ((4294967295UL >= ((((l_2035 ^= (safe_div_func_uint32_t_u_u((safe_sub_func_int8_t_s_s(((safe_div_func_uint64_t_u_u(((*l_2033) &= (((*l_2004) = (safe_div_func_int8_t_s_s(((l_2013[3][2][3] != (((((**g_288) |= (safe_add_func_uint16_t_u_u((((safe_lshift_func_int16_t_s_u((****g_1300), ((((l_2018 , l_2019) < (safe_mul_func_int16_t_s_s((!(*l_2004)), ((&g_1487 != ((((**l_1911) = (safe_div_func_float_f_f(((safe_mul_func_float_f_f((-(*l_1761)), l_2028)) < g_2029), 0x7.1E8DFCp-75))) >= p_5) , (void*)0)) , l_2030)))) , 0xD91F824CL) && 0x2480F4ACL))) && 1UL) , p_5), 0xA61EL))) < (*l_1761)) && p_5) ^ p_5)) | (*l_1761)), p_5))) , l_2032[0][0])), p_5)) != p_5), 0xDBL)), p_5))) , p_5) , (*g_1299)) == &l_2002)))
        { /* block id: 962 */
            uint32_t l_2038[8][8][2] = {{{9UL,1UL},{0xCC455BB8L,1UL},{0UL,0xCC455BB8L},{1UL,1UL},{1UL,0xCC455BB8L},{0UL,1UL},{0xCC455BB8L,1UL},{9UL,9UL}},{{0UL,9UL},{9UL,1UL},{0xCC455BB8L,1UL},{0UL,0xCC455BB8L},{1UL,1UL},{1UL,0xCC455BB8L},{0UL,1UL},{0xCC455BB8L,1UL}},{{9UL,9UL},{0UL,9UL},{9UL,1UL},{0xCC455BB8L,1UL},{0UL,0xCC455BB8L},{1UL,1UL},{1UL,0xCC455BB8L},{0UL,1UL}},{{0xCC455BB8L,1UL},{9UL,9UL},{0UL,9UL},{9UL,1UL},{0xCC455BB8L,1UL},{0UL,0xCC455BB8L},{1UL,1UL},{1UL,0xCC455BB8L}},{{0UL,1UL},{0xCC455BB8L,1UL},{9UL,9UL},{0UL,9UL},{9UL,1UL},{0xCC455BB8L,1UL},{0UL,1UL},{0xA335AD2EL,0x8491F02EL}},{{0xA335AD2EL,1UL},{1UL,0xA335AD2EL},{1UL,0x8491F02EL},{0UL,0UL},{1UL,0UL},{0UL,0x8491F02EL},{1UL,0xA335AD2EL},{1UL,1UL}},{{0xA335AD2EL,0x8491F02EL},{0xA335AD2EL,1UL},{1UL,0xA335AD2EL},{1UL,0x8491F02EL},{0UL,0UL},{1UL,0UL},{0UL,0x8491F02EL},{1UL,0xA335AD2EL}},{{1UL,1UL},{0xA335AD2EL,0x8491F02EL},{0xA335AD2EL,1UL},{1UL,0xA335AD2EL},{1UL,0x8491F02EL},{0UL,0UL},{1UL,0UL},{0UL,0x8491F02EL}}};
            int32_t l_2071 = 0x79A9C783L;
            int32_t l_2072 = (-7L);
            int32_t l_2079 = 1L;
            int32_t l_2081[6] = {0L,0L,0L,0L,0L,0L};
            uint32_t l_2082 = 0UL;
            struct S0 *l_2099 = &g_1955;
            int i, j, k;
            l_2038[0][4][1]--;
            for (g_90 = (-29); (g_90 > 6); g_90 = safe_add_func_int16_t_s_s(g_90, 1))
            { /* block id: 966 */
                uint32_t l_2045 = 0x33B13C4EL;
                int32_t l_2070 = 0x56512E63L;
                int32_t l_2077 = 0xE81BDB50L;
                int32_t l_2078 = 1L;
                int32_t l_2080 = 7L;
                int32_t l_2089[5][10] = {{1L,0xEA211369L,9L,0xDFE1F2A7L,0xDFE1F2A7L,9L,0xEA211369L,1L,9L,1L},{0xDFE1F2A7L,0x6D8214CAL,1L,0xDFE1F2A7L,1L,0x6D8214CAL,0xDFE1F2A7L,(-1L),(-1L),0xDFE1F2A7L},{(-1L),1L,1L,1L,1L,(-1L),0x6D8214CAL,1L,0x6D8214CAL,(-1L)},{0xEA211369L,1L,9L,1L,0xEA211369L,9L,0xDFE1F2A7L,0xDFE1F2A7L,9L,0xEA211369L},{0xEA211369L,0x6D8214CAL,0x6D8214CAL,0xEA211369L,1L,(-1L),0xEA211369L,(-1L),1L,0xEA211369L}};
                uint8_t l_2095 = 8UL;
                int i, j;
                for (l_2028 = 0; (l_2028 < 22); l_2028++)
                { /* block id: 969 */
                    int32_t *l_2048 = &g_296;
                    int32_t l_2058 = 0x87C6A151L;
                    float l_2074 = (-0x6.4p+1);
                    int32_t l_2075 = 0L;
                    int32_t l_2086 = 1L;
                    int32_t l_2090 = 0xACDC0D79L;
                    int32_t l_2093 = 0x12338958L;
                    int32_t l_2094 = 0x043EEA61L;
                    if (p_5)
                    { /* block id: 970 */
                        int32_t **l_2049 = &l_1761;
                        struct S0 *l_2052 = &g_981;
                        int32_t l_2068 = 0xD82110ABL;
                        ++l_2045;
                        (*l_2049) = l_2048;
                        l_2070 ^= ((safe_rshift_func_uint8_t_u_u((l_2052 == (((safe_div_func_int16_t_s_s(((**g_568) &= (safe_mul_func_uint64_t_u_u((p_5 != (*l_2004)), ((((safe_unary_minus_func_uint64_t_u(((*l_2033)++))) | (safe_sub_func_int8_t_s_s((safe_mod_func_int64_t_s_s((l_2065[0] != (void*)0), l_2038[0][4][1])), (((**l_2049) = ((safe_div_func_float_f_f(((*l_1761) <= ((**l_1911) = l_2038[5][2][1])), l_2045)) >= (-0x1.Ep+1))) , (*l_2004))))) > l_2038[0][4][1]) < l_2068)))), 0x3F40L)) | (****g_1026)) , l_2069)), 6)) < l_2038[0][4][1]);
                    }
                    else
                    { /* block id: 978 */
                        int32_t l_2073 = (-1L);
                        int32_t l_2076 = (-1L);
                        int32_t l_2087 = (-1L);
                        int32_t l_2088 = (-1L);
                        int32_t l_2091 = 0x991AB3A7L;
                        int32_t l_2092 = 0x8459020DL;
                        ++l_2082;
                        --l_2095;
                    }
                    if (l_2038[6][0][0])
                        continue;
                }
            }
            if (l_2098)
            { /* block id: 985 */
                struct S0 **l_2100 = (void*)0;
                struct S0 **l_2102 = &g_543;
                (*l_2102) = l_2099;
            }
            else
            { /* block id: 987 */
                int32_t *l_2103[8] = {&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10,&g_10};
                int i;
                l_2103[6] = l_2103[6];
                (*l_1761) = l_2081[2];
                (*l_1761) ^= p_5;
            }
        }
        else
        { /* block id: 992 */
            uint32_t l_2110 = 1UL;
            int32_t l_2122[9] = {0L,1L,0L,1L,0L,1L,0L,1L,0L};
            int i;
            for (l_2085 = 0; (l_2085 == 6); l_2085 = safe_add_func_int64_t_s_s(l_2085, 7))
            { /* block id: 995 */
                int8_t l_2121 = 0x3FL;
                (*g_412) ^= (safe_lshift_func_int8_t_s_s(((-6L) != (l_2122[6] = (p_5 , (p_5 <= (((safe_sub_func_uint64_t_u_u(l_2110, ((safe_mul_func_uint16_t_u_u((safe_unary_minus_func_int32_t_s(l_2110)), (+(g_2115 , ((((*****g_1506) = (safe_rshift_func_int8_t_s_s((safe_div_func_int32_t_s_s(l_2120, l_2110)), (p_5 == 1UL)))) != (*l_2004)) < 1UL))))) ^ 1L))) == l_2121) <= l_2110))))), p_5));
                l_22[0][2] = (void*)0;
            }
            for (l_2019 = 19; (l_2019 > 13); --l_2019)
            { /* block id: 1003 */
                l_2004 = &l_2035;
            }
        }
        if (p_5)
            continue;
    }
    return l_2125;
}


/* ------------------------------------------ */
/* 
 * reads : g_149 g_150 g_1299 g_299 g_1223 g_1224 g_543 g_544 g_171 g_235 g_37 g_886 g_114 g_762 g_763 g_764 g_46 g_1125 g_1126 g_1127 g_412 g_296 g_658.f2 g_1295 g_288 g_3 g_1030 g_500 g_1409 g_636 g_88 g_1423 g_1064 g_255 g_568 g_569 g_298 g_1482 g_1486 g_670 g_1207 g_765 g_173 g_1507 g_1508 g_1483 g_1484 g_644 g_645 g_35 g_1644 g_761 g_481 g_482 g_1026 g_1027 g_1384 g_166 g_1687 g_856 g_1301 g_322 g_323 g_546 g_1955
 * writes: g_46 g_299 g_302 g_1326 g_171 g_886 g_235 g_482 g_1379 g_296 g_114 g_1409 g_35 g_1423 g_1064 g_298 g_1483 g_500 g_1207 g_765 g_173 g_1644 g_1384 g_1687 g_856 g_88 g_150
 */
static int32_t * func_12(uint32_t  p_13)
{ /* block id: 498 */
    const int32_t l_1298 = 0x4719E7F1L;
    int32_t l_1330 = (-6L);
    struct S0 **l_1339[10];
    const float *l_1348 = &g_544.f1;
    float l_1382[4][10] = {{0x1.2p-1,(-0x1.9p-1),0x2.525D8Ap+59,0x2.525D8Ap+59,(-0x1.9p-1),0x1.2p-1,0x8.91F76Cp+18,0x1.2p-1,(-0x1.9p-1),0x2.525D8Ap+59},{0xB.65987Bp+73,0x1.3p-1,0xB.65987Bp+73,0x2.525D8Ap+59,0x8.91F76Cp+18,0x8.91F76Cp+18,0x2.525D8Ap+59,0xB.65987Bp+73,0x1.3p-1,0xB.65987Bp+73},{0xB.65987Bp+73,0x1.2p-1,0x1.3p-1,(-0x1.9p-1),0x1.3p-1,0x1.2p-1,0xB.65987Bp+73,0xB.65987Bp+73,0x1.2p-1,0x1.3p-1},{0x1.2p-1,0xB.65987Bp+73,0xB.65987Bp+73,0x1.2p-1,0x1.3p-1,(-0x1.9p-1),0x1.3p-1,0x1.2p-1,0xB.65987Bp+73,0xB.65987Bp+73}};
    int32_t l_1385 = (-7L);
    int16_t l_1406[5][10][5] = {{{0L,0x612EL,0x0F34L,0x0C46L,8L},{0xE20EL,0x83B0L,0x46E2L,0x67FAL,0x99C5L},{0xACA1L,(-9L),0xD240L,0x9838L,0x9838L},{0L,7L,0L,0x0C06L,(-9L)},{(-2L),0x5C71L,0x99C5L,0x0E5AL,1L},{0L,0x6C5CL,0x6C5CL,0x041CL,1L},{3L,0xC7EDL,3L,7L,(-3L)},{0xAF0BL,1L,(-1L),5L,7L},{0x3AB8L,1L,(-3L),0x99C5L,0x78E2L},{1L,(-1L),0x13F8L,0xFAD0L,5L}},{{0x73C5L,(-4L),0L,0L,7L},{0x0C46L,0L,0x2D93L,7L,0x6885L},{8L,(-5L),0xC023L,0x474AL,0x73C5L},{9L,0x5CD4L,7L,5L,7L},{7L,0L,0x9F49L,0x5C71L,0x0C46L},{0xBDADL,0x0C46L,0xA628L,0x6959L,7L},{0x68F3L,(-4L),(-1L),0xA76EL,3L},{(-4L),7L,0x612EL,0xA76EL,0x5C71L},{1L,(-10L),0xD240L,0x6959L,(-10L)},{0x73C5L,1L,0x2C53L,0x5C71L,0xE06DL}},{{0xAF0BL,0x46E2L,0x9838L,5L,(-9L)},{0x2FCFL,0x73C5L,3L,0x474AL,0x78E2L},{0x05ADL,0xD240L,0xE06DL,7L,1L},{(-10L),0x05ADL,(-2L),0L,0xC1A5L},{1L,0x0C46L,1L,0xFAD0L,(-3L)},{1L,1L,0xC023L,0x99C5L,0xA628L},{0x5C71L,5L,(-2L),5L,0x5C71L},{0xBDADL,0L,0x13F8L,7L,0xECF9L},{0x83B0L,0x2C53L,5L,0x041CL,0x6959L},{0x68F3L,0L,0x9838L,0L,0xECF9L}},{{8L,0x041CL,1L,0x7B2FL,0x5C71L},{0xECF9L,5L,0x6959L,(-1L),0xA628L},{(-10L),3L,0x612EL,0x5C71L,(-3L)},{1L,0xC7EDL,7L,1L,0xC1A5L},{0x99C5L,5L,3L,0x99C5L,1L},{0L,7L,0x9F49L,0x0C06L,0x78E2L},{3L,0x05ADL,0L,3L,(-9L)},{0L,0x2C53L,0x5CD4L,0x83B0L,0xE06DL},{1L,(-5L),0x2D93L,0x4957L,(-10L)},{(-1L),0x78E2L,0L,1L,0x5C71L}},{{7L,1L,0xE06DL,9L,3L},{7L,0xECF9L,(-3L),0x041CL,7L},{(-1L),0xAF0BL,7L,0x7B2FL,0x0C46L},{1L,0xD240L,1L,0xDA21L,(-4L)},{0x5CD4L,0x474AL,(-10L),0xA628L,0xA6F9L},{(-1L),(-1L),0x2D93L,(-4L),0L},{6L,0x9F49L,0x6C5CL,(-4L),0x67FAL},{3L,0xA6F9L,0x4957L,1L,1L},{3L,5L,(-10L),(-1L),(-4L)},{0x2FCFL,0x0E5AL,0x67FAL,0xC023L,0x67FAL}}};
    int32_t *l_1407[7][6][6] = {{{&g_951,&g_500,&g_951,&g_1384,&g_1384,&g_951},{&g_1157,&g_1157,&g_1384,&g_296,&g_1384,&g_1157},{&g_1384,&g_500,&g_296,&g_296,&g_500,&g_1384},{&g_1157,&g_1384,&g_296,&g_1384,&g_1157,&g_1157},{&g_951,&g_1384,&g_1384,&g_951,&g_500,&g_951},{&g_951,&g_500,&g_951,&g_1384,&g_1384,&g_951}},{{&g_1157,&g_1157,&g_1384,&g_296,&g_1384,&g_1157},{&g_1384,&g_500,&g_296,&g_296,&g_500,&g_1384},{&g_1157,&g_1384,&g_296,&g_1384,&g_1157,&g_1157},{&g_951,&g_1384,&g_1384,&g_951,&g_500,&g_951},{&g_951,&g_500,&g_951,&g_1384,&g_1384,&g_951},{&g_1157,&g_1157,&g_1384,&g_296,&g_1384,&g_1157}},{{&g_1384,&g_500,&g_296,&g_296,&g_500,&g_1384},{&g_1157,&g_1384,&g_296,&g_1384,&g_1157,&g_1157},{&g_951,&g_1384,&g_1384,&g_951,&g_500,&g_951},{&g_951,&g_500,&g_951,&g_1384,&g_1384,&g_951},{&g_1157,&g_1157,&g_1384,&g_296,&g_1384,&g_1157},{&g_1384,&g_500,&g_296,&g_296,&g_500,&g_1384}},{{&g_1157,&g_1384,&g_296,&g_1384,&g_1157,&g_1157},{&g_951,&g_1384,&g_1384,&g_951,&g_500,&g_951},{&g_951,&g_500,&g_951,&g_1384,&g_1384,&g_951},{&g_1157,&g_1157,&g_1384,&g_296,&g_1384,&g_1157},{&g_1384,&g_500,&g_296,&g_296,&g_500,&g_1384},{&g_1157,&g_1384,&g_296,&g_1384,&g_1157,&g_1157}},{{&g_951,&g_1384,&g_1384,&g_951,&g_500,&g_951},{&g_951,&g_500,&g_951,&g_1384,&g_1384,&g_951},{&g_1157,&g_1157,&g_951,&g_500,&g_951,&g_1384},{&g_951,&g_1157,&g_500,&g_500,&g_1157,&g_951},{&g_1384,&g_951,&g_500,&g_951,&g_1384,&g_1384},{&g_296,&g_951,&g_951,&g_296,&g_1157,&g_296}},{{&g_296,&g_1157,&g_296,&g_951,&g_951,&g_296},{&g_1384,&g_1384,&g_951,&g_500,&g_951,&g_1384},{&g_951,&g_1157,&g_500,&g_500,&g_1157,&g_951},{&g_1384,&g_951,&g_500,&g_951,&g_1384,&g_1384},{&g_296,&g_951,&g_951,&g_296,&g_1157,&g_296},{&g_296,&g_1157,&g_296,&g_951,&g_951,&g_296}},{{&g_1384,&g_1384,&g_951,&g_500,&g_951,&g_1384},{&g_951,&g_1157,&g_500,&g_500,&g_1157,&g_951},{&g_1384,&g_951,&g_500,&g_951,&g_1384,&g_1384},{&g_296,&g_951,&g_951,&g_296,&g_1157,&g_296},{&g_296,&g_1157,&g_296,&g_951,&g_951,&g_296},{&g_1384,&g_1384,&g_951,&g_500,&g_951,&g_1384}}};
    int32_t ***l_1413 = &g_87[1][2];
    int32_t ****l_1412 = &l_1413;
    int32_t ****l_1418 = &l_1413;
    const int64_t **l_1429 = &g_1326;
    const int64_t ***l_1428 = &l_1429;
    int64_t *l_1432 = (void*)0;
    uint8_t **l_1481[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t l_1522 = 0x5317AF22L;
    uint16_t l_1735 = 0x7B2FL;
    int32_t *l_1757[1];
    int32_t *l_1758[7] = {&g_1384,&g_951,&g_951,&g_1384,&g_951,&g_951,&g_1384};
    int16_t l_1759 = 0x799CL;
    int i, j, k;
    for (i = 0; i < 10; i++)
        l_1339[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_1757[i] = &l_1330;
    if ((+(l_1298 , (((**g_149) = 0xDEC7L) || ((void*)0 != g_1299)))))
    { /* block id: 500 */
        int16_t ***l_1311 = &g_568;
        int16_t ****l_1310 = &l_1311;
        int16_t *****l_1309 = &l_1310;
        int32_t l_1321 = (-1L);
        const int64_t *l_1324 = &g_171;
        int32_t l_1351 = (-9L);
        int32_t l_1386[8];
        int32_t l_1443 = 0xCAA7F7AAL;
        uint16_t l_1456 = 0x7DD7L;
        int i;
        for (i = 0; i < 8; i++)
            l_1386[i] = (-1L);
        for (g_299 = 12; (g_299 < 20); g_299 = safe_add_func_uint8_t_u_u(g_299, 4))
        { /* block id: 503 */
            int16_t *****l_1308 = (void*)0;
            uint64_t *l_1312 = &g_302[1];
            const int64_t **l_1325[4][7] = {{(void*)0,&l_1324,(void*)0,&l_1324,&l_1324,&l_1324,(void*)0},{&l_1324,&l_1324,&l_1324,&l_1324,&l_1324,&l_1324,&l_1324},{&l_1324,&l_1324,(void*)0,&l_1324,(void*)0,&l_1324,&l_1324},{&l_1324,&l_1324,&l_1324,&l_1324,&l_1324,&l_1324,&l_1324}};
            uint64_t l_1328 = 0xE458B0AD263DCD46LL;
            int8_t l_1329 = 0xD3L;
            int i, j;
            l_1330 |= (safe_mul_func_int16_t_s_s(((((*l_1312) = ((safe_sub_func_float_f_f((-0x9.5p-1), (l_1308 != l_1309))) , 7UL)) && ((safe_sub_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((safe_div_func_int8_t_s_s((((*l_1312) = (safe_lshift_func_uint8_t_u_u((((((l_1321 <= (safe_div_func_float_f_f(p_13, ((g_1326 = l_1324) == (*g_1223))))) > ((*g_543) , l_1298)) , (void*)0) != &g_88) != 0x7412L), p_13))) , p_13), l_1328)), 0x6AL)), l_1329)) < 65535UL)) || (-5L)), 0x6E2FL));
        }
        for (g_171 = 0; (g_171 >= 0); g_171 -= 1)
        { /* block id: 511 */
            uint16_t l_1341 = 0xC7B4L;
            int32_t ***l_1342 = &g_87[1][2];
            int32_t l_1352[2][7][9] = {{{8L,0L,0x2D652313L,0x6D12C06BL,0L,0L,0x8B4C95A5L,0L,0L},{0L,0x36F26EAFL,0x36F26EAFL,0L,0xA2550B7EL,0x6D30A962L,0L,0L,(-1L)},{5L,0xA2550B7EL,0x84A4A5EDL,0L,0x35323B71L,0L,7L,0L,0x8E9ECCD5L},{(-8L),0x47A735E9L,0x8E9ECCD5L,0xDEBF7E87L,0xA2550B7EL,0L,(-10L),0x71C12C43L,(-1L)},{0xA53DC18DL,(-1L),(-9L),0x24BA7C65L,0L,0x53169094L,0x7139A991L,0x6D30A962L,0L},{0x71C12C43L,0L,(-10L),0x03E460E1L,0L,0xD3C1E0F5L,0x7139A991L,1L,0L},{0x8E9ECCD5L,(-10L),(-10L),0x8B4C95A5L,0xA2550B7EL,0x9DD87215L,0x6D30A962L,(-1L),0L}},{{(-8L),0xDD445DD2L,0L,(-9L),(-10L),0x1CE9B723L,(-6L),(-1L),0x6D12C06BL},{1L,0xDEBF7E87L,1L,0x1CE9B723L,0x4A96CD37L,(-10L),0L,0x36F26EAFL,0L},{(-1L),(-1L),0L,0x2D652313L,0x47A735E9L,0x7139A991L,0xA2550B7EL,0x35323B71L,0L},{0L,0x71C12C43L,(-1L),(-10L),0x35323B71L,(-8L),0xBCB425B1L,0x6D30A962L,(-10L)},{0x03E460E1L,0x71C12C43L,(-6L),1L,0x2D652313L,0x84A4A5EDL,0x84A4A5EDL,0x2D652313L,1L},{0xBCB425B1L,(-1L),0xBCB425B1L,7L,(-6L),0xDDC46E14L,(-9L),0L,(-1L)},{0L,0xDEBF7E87L,(-9L),0xA2550B7EL,0L,0L,0L,0L,1L}}};
            int32_t ***l_1356 = (void*)0;
            uint16_t ** const l_1390 = &g_150;
            int64_t *l_1430 = &g_171;
            int64_t **l_1431 = &l_1430;
            int64_t **l_1433 = &l_1432;
            int64_t l_1435 = 0x8E833475C5CD731DLL;
            float l_1442 = 0x4.AC02C4p-1;
            uint16_t l_1445[7][8] = {{1UL,3UL,0xEC9DL,0xEC9DL,3UL,1UL,0x8BFBL,1UL},{3UL,1UL,0x8BFBL,1UL,3UL,0xEC9DL,0xEC9DL,3UL},{1UL,0xA4EEL,0xA4EEL,1UL,65526UL,3UL,65526UL,1UL},{0xA4EEL,65526UL,0xA4EEL,0xEC9DL,0x8BFBL,0x8BFBL,0xEC9DL,0xA4EEL},{65526UL,65526UL,0x8BFBL,3UL,0x92B7L,3UL,0x8BFBL,65526UL},{65526UL,0xA4EEL,0xEC9DL,0x8BFBL,0x8BFBL,0xEC9DL,0xA4EEL,65526UL},{0xA4EEL,1UL,65526UL,3UL,65526UL,1UL,0xA4EEL,0xA4EEL}};
            int i, j, k;
            for (g_886 = 0; (g_886 <= 0); g_886 += 1)
            { /* block id: 514 */
                int32_t l_1376 = 0x4DBD830AL;
                int32_t l_1380 = 0x67AC2040L;
                uint32_t l_1387 = 0x6B5BD0DFL;
                int32_t *l_1419 = &l_1352[1][1][6];
                int32_t l_1421 = 5L;
                int32_t l_1422 = 0L;
                int i, j;
                for (g_235 = 0; (g_235 <= 0); g_235 += 1)
                { /* block id: 517 */
                    int32_t * const **l_1333 = (void*)0;
                    struct S0 **l_1338 = &g_543;
                    int64_t *l_1340[6][4][9] = {{{&g_1295,&g_171,(void*)0,&g_171,&g_171,&g_171,&g_171,(void*)0,&g_171},{&g_171,&g_171,&g_1295,&g_171,(void*)0,&g_171,&g_1295,&g_171,&g_171},{&g_171,&g_171,&g_171,(void*)0,&g_1295,&g_171,&g_1295,&g_171,(void*)0},{&g_1295,&g_171,&g_171,&g_1295,&g_1295,&g_171,&g_171,&g_1295,&g_1295}},{{&g_171,&g_171,&g_1295,&g_1295,&g_1295,&g_1295,&g_171,&g_1295,(void*)0},{(void*)0,&g_171,&g_171,&g_1295,(void*)0,&g_171,&g_1295,&g_171,&g_1295},{&g_171,(void*)0,(void*)0,(void*)0,(void*)0,&g_171,&g_1295,&g_171,(void*)0},{&g_171,(void*)0,&g_1295,&g_171,&g_1295,&g_1295,&g_1295,&g_171,(void*)0}},{{(void*)0,&g_171,(void*)0,&g_1295,(void*)0,&g_171,&g_171,&g_171,(void*)0},{&g_171,&g_171,&g_171,&g_171,&g_1295,&g_171,&g_1295,&g_1295,&g_171},{&g_171,&g_1295,&g_171,(void*)0,&g_171,&g_1295,&g_171,&g_171,&g_1295},{&g_1295,&g_171,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295,&g_1295}},{{&g_171,&g_171,(void*)0,&g_1295,(void*)0,&g_1295,&g_171,&g_1295,&g_1295},{&g_1295,(void*)0,&g_171,&g_171,&g_1295,(void*)0,(void*)0,&g_171,&g_1295},{&g_171,&g_171,(void*)0,(void*)0,&g_171,(void*)0,(void*)0,&g_171,&g_171},{&g_1295,&g_1295,&g_1295,&g_1295,&g_171,&g_171,&g_171,&g_1295,&g_1295}},{{(void*)0,&g_171,&g_171,(void*)0,&g_171,(void*)0,&g_171,(void*)0,(void*)0},{&g_1295,&g_171,&g_171,&g_1295,&g_171,&g_171,&g_1295,&g_171,&g_1295},{&g_171,&g_1295,(void*)0,&g_171,&g_1295,(void*)0,&g_1295,&g_171,(void*)0},{&g_1295,&g_1295,&g_1295,&g_171,&g_171,&g_171,&g_171,&g_171,&g_171}},{{&g_171,&g_1295,(void*)0,&g_1295,&g_171,&g_171,(void*)0,(void*)0,&g_171},{&g_1295,&g_1295,(void*)0,&g_171,(void*)0,&g_1295,&g_171,&g_1295,&g_1295},{&g_171,&g_171,&g_1295,&g_171,&g_171,&g_171,&g_171,&g_171,&g_171},{&g_171,(void*)0,&g_171,&g_1295,&g_1295,&g_171,(void*)0,&g_171,&g_171}}};
                    int32_t ****l_1343 = &l_1342;
                    const float **l_1349 = &l_1348;
                    int32_t ***l_1350[8];
                    int32_t l_1408[3];
                    int16_t ***l_1420 = &g_568;
                    int i, j, k;
                    for (i = 0; i < 8; i++)
                        l_1350[i] = (void*)0;
                    for (i = 0; i < 3; i++)
                        l_1408[i] = 0xB788A3D4L;
                    if ((((safe_rshift_func_uint8_t_u_s(g_37[g_235][(g_171 + 7)], (l_1333 != ((safe_div_func_int64_t_s_s((l_1341 = (safe_div_func_uint8_t_u_u((l_1338 != l_1339[0]), g_37[g_886][(g_886 + 7)]))), g_114[g_886])) , ((*l_1343) = l_1342))))) ^ (((safe_lshift_func_uint8_t_u_u(((l_1352[1][4][0] = (((safe_sub_func_float_f_f((((((l_1321 = p_13) >= (((((((*l_1349) = l_1348) != (void*)0) > p_13) , l_1350[6]) != (void*)0) == 0x1.3p-1)) == p_13) <= l_1351) != 0xC.81B647p+92), l_1352[0][6][7])) < (-0x1.1p-1)) , 0xA512L)) <= 65527UL), 6)) > 251UL) != 0x75D6E0BCF88D28F2LL)) && p_13))
                    { /* block id: 523 */
                        int32_t *l_1353 = &l_1352[0][6][7];
                        float *l_1377 = &g_482;
                        float *l_1378 = &g_1379;
                        int32_t l_1381 = 0xF10DCB3EL;
                        (*l_1353) = ((void*)0 == l_1353);
                        l_1380 = (safe_rshift_func_uint8_t_u_s(((l_1356 == (((l_1330 && ((*l_1353) = (+(safe_mod_func_int16_t_s_s(((**g_762) == (void*)0), (safe_sub_func_uint64_t_u_u((~(safe_rshift_func_uint16_t_u_s(((*g_150) = (**g_149)), 1))), 18446744073709551615UL))))))) ^ (safe_sub_func_int8_t_s_s((l_1376 = (((&g_1223 != ((safe_mul_func_float_f_f((+((*l_1378) = (safe_add_func_float_f_f((safe_div_func_float_f_f((((*l_1377) = l_1376) <= 0x0.0p-1), p_13)), p_13)))), 0x1.Dp+1)) , (void*)0)) < 0L) ^ p_13)), p_13))) , (*l_1343))) || (**g_1125)), 2));
                        l_1387--;
                    }
                    else
                    { /* block id: 532 */
                        uint32_t l_1397[7];
                        int8_t *l_1404 = &g_114[0];
                        uint32_t *l_1405 = &l_1387;
                        int i;
                        for (i = 0; i < 7; i++)
                            l_1397[i] = 5UL;
                        (*g_412) ^= ((void*)0 == l_1390);
                        l_1380 ^= ((g_658.f2 && (safe_rshift_func_int16_t_s_s((safe_sub_func_int32_t_s_s((safe_mod_func_uint64_t_u_u((((g_1295 , p_13) ^ (l_1397[2] ^ 0xD2L)) < (safe_lshift_func_int16_t_s_s(((p_13 > ((safe_add_func_int32_t_s_s((0x58F68D87EAD11E9BLL | ((((*l_1405) = (((**g_288) , ((safe_rshift_func_uint8_t_u_u((((((*l_1404) = (-1L)) < 0xFAL) & g_3) < p_13), 2)) , (void*)0)) == (void*)0)) > l_1397[3]) >= 9L)), p_13)) || p_13)) & l_1330), 0))), 0x2EE1E52CF51B5E42LL)), (*g_1030))), 4))) || 1L);
                        if (l_1406[4][4][4])
                            break;
                        l_1407[6][1][4] = (void*)0;
                    }
                    g_1409++;
                    for (g_35 = 0; (g_35 <= 2); g_35 += 1)
                    { /* block id: 543 */
                        int32_t *****l_1414 = &l_1412;
                        l_1418 = ((*l_1414) = l_1412);
                        return (*g_636);
                    }
                    (**l_1309) = l_1420;
                }
                (*g_412) = g_37[g_171][(g_886 + 3)];
                g_1423--;
            }
        }
        for (l_1330 = 0; (l_1330 <= 2); l_1330 += 1)
        { /* block id: 589 */
            int8_t l_1474 = 0xEAL;
            int32_t l_1476 = 0xAE006377L;
            int32_t l_1478 = 0L;
            for (g_1064 = 5; (g_1064 >= 1); g_1064 -= 1)
            { /* block id: 592 */
                int16_t *l_1475 = &l_1406[4][1][1];
                uint64_t *l_1477 = &g_302[1];
                int32_t *l_1480 = (void*)0;
                int i, j;
                if ((l_1478 = (safe_rshift_func_uint16_t_u_u((((*l_1477) = ((safe_div_func_int16_t_s_s((g_255[l_1330][(l_1330 + 1)] < (safe_add_func_int16_t_s_s(p_13, p_13))), (l_1476 = ((*l_1475) ^= ((safe_sub_func_int16_t_s_s(((***l_1311) = p_13), p_13)) > (((**g_288) = 1UL) == (p_13 < ((safe_sub_func_uint8_t_u_u(l_1474, 0xBEL)) <= (*g_1126))))))))) , p_13)) , 0x5611L), p_13))))
                { /* block id: 599 */
                    int32_t *l_1479[1][5] = {{&l_1321,&l_1321,&l_1321,&l_1321,&l_1321}};
                    int i, j;
                    return l_1480;
                }
                else
                { /* block id: 601 */
                    for (g_298 = 5; (g_298 >= 1); g_298 -= 1)
                    { /* block id: 604 */
                        int i, j, k;
                        (*g_1482) = l_1481[3];
                        return l_1407[(g_298 + 1)][g_1064][(l_1330 + 3)];
                    }
                    return l_1480;
                }
            }
            for (g_235 = 0; (g_235 <= 1); g_235 += 1)
            { /* block id: 613 */
                int32_t *****l_1485 = &l_1412;
                int32_t l_1488 = 1L;
                l_1478 ^= (l_1485 != (void*)0);
                for (l_1385 = 0; (l_1385 <= 5); l_1385 += 1)
                { /* block id: 617 */
                    for (g_296 = 0; (g_296 <= 1); g_296 += 1)
                    { /* block id: 620 */
                        int i, j, k;
                        l_1407[(g_296 + 3)][(g_296 + 3)][(g_235 + 1)] = (void*)0;
                        (*g_670) |= (g_1486[3][1][4] != &g_1487);
                        if (l_1476)
                            continue;
                        l_1488 = p_13;
                    }
                }
            }
        }
    }
    else
    { /* block id: 629 */
        uint8_t l_1505 = 0xF2L;
        uint8_t **** const *l_1509[2];
        int32_t l_1516 = 0x7CBBD2A5L;
        int32_t *l_1525 = (void*)0;
        int32_t *l_1569 = (void*)0;
        int32_t l_1570 = (-1L);
        int32_t l_1576 = 1L;
        int32_t l_1579 = 0xDE5A5AAFL;
        int32_t l_1580 = 0xF85E0DCFL;
        int32_t l_1583 = 0x637D44F1L;
        int32_t l_1585[7][2] = {{0xB628D517L,0xAB16C554L},{0xB628D517L,0xB628D517L},{0xAB16C554L,0xB628D517L},{0xB628D517L,0xAB16C554L},{0xB628D517L,0xB628D517L},{0xAB16C554L,0xB628D517L},{0xB628D517L,0xAB16C554L}};
        uint64_t l_1590[1][8][8] = {{{0xE197E7C14AB653B1LL,0x494DFD99643747C9LL,18446744073709551610UL,0x494DFD99643747C9LL,0xE197E7C14AB653B1LL,0x93BA6AED1D25BB08LL,0x39150040B319BD12LL,0x3FE6966EF3B0639ELL},{0x494DFD99643747C9LL,0x5EF13550E352601CLL,1UL,18446744073709551610UL,18446744073709551610UL,1UL,0x5EF13550E352601CLL,0x494DFD99643747C9LL},{18446744073709551615UL,0x93BA6AED1D25BB08LL,1UL,0x37D6C5051DD2E91CLL,0x39150040B319BD12LL,0xE197E7C14AB653B1LL,0x3FE6966EF3B0639ELL,0x5EF13550E352601CLL},{0x93BA6AED1D25BB08LL,0x37D6C5051DD2E91CLL,0x93BA6AED1D25BB08LL,0x494DFD99643747C9LL,0x5EF13550E352601CLL,1UL,18446744073709551610UL,18446744073709551610UL},{18446744073709551610UL,18446744073709551615UL,0x39150040B319BD12LL,0x39150040B319BD12LL,18446744073709551615UL,18446744073709551610UL,0x5EF13550E352601CLL,0x3FE6966EF3B0639ELL},{18446744073709551610UL,0xE197E7C14AB653B1LL,0x7967268F57F54AB1LL,18446744073709551615UL,0x5EF13550E352601CLL,18446744073709551615UL,0x7967268F57F54AB1LL,0xE197E7C14AB653B1LL},{0x93BA6AED1D25BB08LL,0x7967268F57F54AB1LL,1UL,18446744073709551615UL,0x3FE6966EF3B0639ELL,0x494DFD99643747C9LL,0x494DFD99643747C9LL,0x3FE6966EF3B0639ELL},{0x39150040B319BD12LL,0x3FE6966EF3B0639ELL,0x3FE6966EF3B0639ELL,0x39150040B319BD12LL,0x93BA6AED1D25BB08LL,0xE197E7C14AB653B1LL,0x494DFD99643747C9LL,18446744073709551610UL}}};
        int32_t l_1593 = (-10L);
        uint32_t l_1642 = 18446744073709551615UL;
        uint32_t l_1649 = 0UL;
        int32_t *l_1662 = &g_951;
        int32_t ** const **l_1684 = (void*)0;
        int32_t ** const ***l_1683 = &l_1684;
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1509[i] = &g_1507;
        for (g_1207 = 0; (g_1207 <= 2); g_1207 += 1)
        { /* block id: 632 */
            uint64_t *l_1496 = (void*)0;
            int32_t l_1510 = (-1L);
            const uint8_t *l_1564 = &g_173;
            const uint8_t **l_1563 = &l_1564;
            const uint8_t ***l_1562 = &l_1563;
            const uint8_t ****l_1561 = &l_1562;
            int32_t l_1573 = 0x36080C8EL;
            int32_t l_1574 = (-6L);
            int32_t l_1575 = 0x1489D624L;
            int32_t l_1577 = (-5L);
            int32_t l_1578 = 0xB9DA98FAL;
            int32_t l_1584 = 0x4186935BL;
            int32_t l_1586[4];
            int64_t l_1587 = (-5L);
            int64_t l_1588 = 1L;
            int32_t *l_1653[3][2][3] = {{{(void*)0,(void*)0,(void*)0},{&g_500,(void*)0,&l_1573}},{{&g_1157,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{&g_500,(void*)0,&l_1573},{&g_1157,(void*)0,(void*)0}}};
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_1586[i] = (-9L);
            for (g_35 = 1; (g_35 <= 9); g_35 += 1)
            { /* block id: 635 */
                int64_t l_1493 = 1L;
                uint64_t *l_1498[5][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
                int32_t l_1515[4] = {0x199E0788L,0x199E0788L,0x199E0788L,0x199E0788L};
                int64_t l_1556[8][3] = {{1L,1L,1L},{0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL},{1L,1L,1L},{0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL},{1L,1L,1L},{0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL},{1L,1L,1L},{0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL,0xA3CED0372F9921AFLL}};
                uint16_t l_1558[7][3][8] = {{{0xCCBAL,0xE5AEL,3UL,0xD6ACL,1UL,0xCCBAL,0xB295L,65526UL},{0x66F9L,0x5C3EL,0xB492L,1UL,0xEF0CL,65535UL,0x5007L,1UL},{9UL,0xC359L,0xEF0CL,0xFAF9L,1UL,1UL,65533UL,0xD5B2L}},{{0xB295L,1UL,0x636CL,3UL,0x61CFL,0xE5AEL,1UL,0x4C52L},{0UL,0UL,7UL,0xEF0CL,0xB492L,0x320FL,0xA63AL,0x320FL},{0x5007L,0x1B77L,65526UL,0x1B77L,0x5007L,65531UL,0xC359L,0xFBACL}},{{0x66F9L,0x552EL,0x1B77L,0x9078L,0xE5AEL,1UL,65535UL,0x1B77L},{0x90F3L,0xB492L,0x1B77L,0UL,65529UL,1UL,0xC359L,65533UL},{0xE5AEL,0x3F8EL,65526UL,0xFBACL,6UL,0xC359L,0xA63AL,65531UL}},{{0x3BD7L,0x61CFL,7UL,0xB295L,0x1B77L,0x90F3L,1UL,65526UL},{65535UL,0xE5AEL,0x636CL,0xCBA3L,0x9E24L,0x552EL,65533UL,0xFBACL},{65535UL,65531UL,0xEF0CL,0xF689L,0xD5B2L,0xC359L,0xD6ACL,7UL}},{{0x9078L,7UL,65526UL,3UL,0x0068L,1UL,0xFAF9L,0xFAF9L},{65526UL,65535UL,0xA63AL,0xA63AL,65535UL,65526UL,0x552EL,0UL},{0x99F8L,1UL,0xF689L,0x320FL,1UL,0x4C52L,0x5C3EL,1UL}},{{0xD6ACL,0UL,0x61CFL,0x320FL,0x1B77L,0xE5AEL,9UL,0UL},{1UL,0x1B77L,0x8061L,0xA63AL,65535UL,0xCBA3L,0x1B77L,0xFAF9L},{1UL,0x320FL,0x5040L,3UL,0x9E24L,65535UL,0x8061L,7UL}},{{1UL,65535UL,0UL,0x3F8EL,0x5C3EL,7UL,0xA69EL,65526UL},{0x99F8L,65531UL,6UL,0x90F3L,0x8061L,0x9078L,0xCCBAL,6UL},{0x1B77L,65526UL,0x3F8EL,0xFAF9L,0xD5B2L,0xE5AEL,0UL,0xACE6L}}};
                const uint8_t ****l_1567 = &l_1562;
                int8_t l_1582 = 0x59L;
                int8_t l_1589 = 1L;
                int32_t *l_1597 = &l_1515[2];
                int i, j, k;
            }
            for (g_500 = 2; (g_500 >= 0); g_500 -= 1)
            { /* block id: 707 */
                uint32_t l_1614 = 0xDC6606A8L;
                uint16_t l_1615 = 0x4ED1L;
                float *l_1616 = &l_1382[0][5];
                int32_t l_1617 = 0x22BEB855L;
                int32_t l_1646 = 0xD5AEA95DL;
                int32_t l_1647[5][5] = {{0xB2358A98L,0xB2358A98L,0xB2358A98L,0xB2358A98L,0xB2358A98L},{0x339D48ECL,0xC1DF0E7DL,0x339D48ECL,0xC1DF0E7DL,0x339D48ECL},{0xB2358A98L,0xB2358A98L,0xB2358A98L,0xB2358A98L,0xB2358A98L},{0x339D48ECL,0xC1DF0E7DL,0x339D48ECL,0xC1DF0E7DL,0x339D48ECL},{0xB2358A98L,0xB2358A98L,0xB2358A98L,0xB2358A98L,0xB2358A98L}};
                uint8_t ***l_1656 = &g_1483;
                int i, j;
                (*g_764) = (l_1617 = ((*l_1616) = ((safe_add_func_float_f_f((***g_762), l_1578)) <= (p_13 , (safe_mul_func_float_f_f(((+(l_1575 = (safe_div_func_float_f_f((((safe_div_func_uint16_t_u_u((p_13 || (safe_div_func_int64_t_s_s(((p_13 = (safe_rshift_func_int16_t_s_s(l_1586[0], 0))) || ((~((((safe_mod_func_int64_t_s_s(l_1614, (p_13 || 0x853DL))) , g_46) , 0x5F075F97L) && l_1614)) != l_1587)), l_1614))), l_1615)) != g_46) , l_1586[3]), l_1575)))) != l_1574), l_1573))))));
                for (g_173 = 0; (g_173 <= 2); g_173 += 1)
                { /* block id: 715 */
                    uint8_t l_1618[9] = {0x8FL,9UL,0x8FL,9UL,0x8FL,9UL,0x8FL,9UL,0x8FL};
                    int32_t l_1645[9][3] = {{0xC49EEE8CL,0xC49EEE8CL,0L},{1L,0L,0L},{0L,0L,1L},{0L,0xC49EEE8CL,0xC49EEE8CL},{0xF077B290L,0L,1L},{1L,7L,0L},{1L,9L,0L},{0xF077B290L,0x7CD66DA8L,0xF077B290L},{0L,9L,1L}};
                    int i, j;
                    for (l_1588 = 3; (l_1588 >= 0); l_1588 -= 1)
                    { /* block id: 718 */
                        int32_t l_1619 = (-1L);
                        uint16_t *l_1643 = &g_1644[0];
                        int32_t l_1648[3][2];
                        int32_t **l_1652 = &l_1407[3][3][5];
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_1648[i][j] = 3L;
                        }
                        l_1619 = (l_1618[7] = ((*g_412) = g_255[g_500][(g_173 + 1)]));
                        (**g_763) = ((safe_mul_func_float_f_f((((((safe_sub_func_uint16_t_u_u(((**g_149)++), g_255[g_500][g_173])) < (safe_mod_func_int32_t_s_s((((((p_13 < ((*g_412) &= l_1615)) >= (1L < (l_1615 | (safe_lshift_func_uint16_t_u_u((safe_div_func_int64_t_s_s((g_171 = 1L), (safe_mod_func_int16_t_s_s(0xD467L, ((*l_1643) ^= (p_13 & (((safe_add_func_uint64_t_u_u(((((safe_rshift_func_uint8_t_u_s(((((safe_mul_func_uint8_t_u_u((p_13 > 4294967286UL), p_13)) >= (****g_1507)) && 1L) | p_13), l_1617)) >= l_1642) < l_1618[7]) , 0x49FED5CAB5B2332FLL), p_13)) ^ 246UL) >= (**g_644)))))))), p_13))))) == l_1618[3]) <= 1UL) || (*g_1126)), p_13))) , (****g_761)) != p_13) == 0x8.82180Dp+19), p_13)) < p_13);
                        --l_1649;
                        l_1653[0][1][1] = ((*l_1652) = &l_1580);
                    }
                    for (g_296 = 0; (g_296 <= 2); g_296 += 1)
                    { /* block id: 733 */
                        int i, j, k;
                        l_1646 = (safe_div_func_float_f_f((*g_481), p_13));
                        l_1645[1][1] = ((*g_1026) != l_1656);
                    }
                }
            }
        }
        for (g_299 = 1; (g_299 >= 0); g_299 -= 1)
        { /* block id: 742 */
            uint64_t l_1658 = 0x82D3B2C2567F932FLL;
            int32_t l_1661 = (-1L);
            int32_t l_1671[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
            int i;
            for (l_1385 = 4; (l_1385 >= 0); l_1385 -= 1)
            { /* block id: 745 */
                int32_t l_1657 = 3L;
                int i, j;
                l_1658--;
                (*g_412) ^= (l_1661 = (l_1585[(g_299 + 5)][g_299] >= p_13));
                return l_1662;
            }
            for (l_1583 = 3; (l_1583 >= 0); l_1583 -= 1)
            { /* block id: 753 */
                int32_t l_1667[8][5] = {{0L,(-4L),0L,0L,(-4L)},{(-4L),0L,0L,(-4L),0L},{(-4L),(-4L),0x6173B267L,(-4L),(-4L)},{0L,(-4L),0L,0L,(-4L)},{(-4L),0L,0L,(-4L),0L},{(-4L),(-4L),0x6173B267L,(-4L),(-4L)},{0L,(-4L),0L,0L,(-4L)},{(-4L),0L,0L,(-4L),0L}};
                uint64_t *l_1668 = &g_302[1];
                int16_t l_1672 = 1L;
                int i, j;
                (*g_412) &= (safe_sub_func_int16_t_s_s(((((*l_1668) = ((l_1585[(g_299 + 2)][g_299] , 0xAAL) && (safe_mod_func_uint8_t_u_u(((***g_1508) = (6UL > (l_1585[(l_1583 + 3)][g_299] <= (p_13 || p_13)))), (l_1667[0][2] |= 1UL))))) , l_1658) >= p_13), p_13));
                for (g_1384 = 0; (g_1384 >= 0); g_1384 -= 1)
                { /* block id: 760 */
                    int32_t l_1669 = 0x78807F30L;
                    int32_t l_1670 = 0xDD84AA60L;
                    int32_t l_1673[6][4] = {{0x17561C6BL,0x17561C6BL,4L,0xAD851558L},{0xB9AAB883L,0x0F9DADCBL,0x17561C6BL,0x0F9DADCBL},{0x8EDED3E8L,0xAD851558L,4L,0x17561C6BL},{0x0F9DADCBL,0xAD851558L,0xAD851558L,0x0F9DADCBL},{0xAD851558L,0x0F9DADCBL,0x8EDED3E8L,0xB9AAB883L},{0xAD851558L,0x8EDED3E8L,0xAD851558L,4L}};
                    uint32_t l_1674 = 1UL;
                    int i, j, k;
                    if (g_166[(g_1384 + 1)][(l_1583 + 3)][g_1384])
                        break;
                    l_1674--;
                    (*g_412) &= l_1670;
                }
            }
        }
        for (l_1505 = 11; (l_1505 <= 46); l_1505++)
        { /* block id: 769 */
            const float **l_1680 = &l_1348;
            const float ***l_1679 = &l_1680;
            int32_t *l_1686[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1686[i] = &g_1384;
            (*l_1679) = &l_1348;
            for (l_1522 = 0; (l_1522 <= 6); l_1522++)
            { /* block id: 773 */
                int32_t *l_1685[7][4][9] = {{{&g_90,&g_298,&l_1330,&g_500,(void*)0,&l_1576,&l_1580,&g_90,&l_1576},{&l_1570,&g_10,(void*)0,&g_1157,&g_10,&l_1576,&l_1583,&g_10,(void*)0},{&g_90,&g_500,(void*)0,&g_951,&l_1385,&l_1580,&l_1330,&l_1585[0][1],&l_1576},{&l_1585[0][1],(void*)0,&l_1583,&l_1330,&l_1580,&l_1579,&g_298,&l_1583,(void*)0}},{{&l_1330,&l_1330,&g_1384,&g_951,(void*)0,&g_298,&g_500,(void*)0,&l_1580},{&g_500,&l_1570,&l_1585[5][1],&g_1157,&g_951,&g_10,&l_1385,&l_1330,&l_1580},{&l_1579,&l_1570,&l_1583,&g_500,&g_500,&l_1583,&l_1570,&l_1579,&g_90},{&l_1385,&l_1330,(void*)0,&l_1583,(void*)0,&l_1576,&l_1385,&g_500,&g_1157}},{{&g_298,(void*)0,&l_1576,&g_296,&g_90,&g_90,&l_1583,(void*)0,&g_90},{&g_298,&g_500,(void*)0,&l_1330,&g_500,&g_1384,&l_1579,&g_951,&l_1580},{&l_1330,&g_10,&g_10,&g_500,&l_1576,&l_1583,&l_1579,&g_90,&l_1580},{&g_500,&g_298,&g_951,&g_500,&g_500,&l_1585[5][1],&l_1583,&g_500,(void*)0}},{{&g_10,(void*)0,&l_1580,&l_1330,&g_1157,(void*)0,&l_1385,(void*)0,&l_1576},{(void*)0,&g_10,&l_1576,&l_1576,&g_500,(void*)0,&l_1570,&g_500,(void*)0},{(void*)0,&l_1583,(void*)0,(void*)0,&l_1576,&l_1576,&l_1385,&l_1580,&l_1576},{&l_1385,&l_1385,(void*)0,(void*)0,&g_500,&l_1580,&g_500,&l_1385,(void*)0}},{{&g_296,&g_296,&l_1576,&l_1385,&g_90,&g_90,&g_298,&l_1330,(void*)0},{&l_1583,&l_1585[5][1],&l_1580,&l_1385,(void*)0,&g_1384,&l_1330,&l_1330,&g_951},{&g_296,&g_90,&g_951,&g_500,&g_500,&g_10,&l_1583,&l_1580,(void*)0},{(void*)0,&l_1570,&g_296,&l_1576,&g_298,&l_1579,&g_951,&g_951,&l_1579}},{{(void*)0,&g_298,&l_1330,&g_298,(void*)0,&g_500,&g_298,&l_1585[5][1],&g_298},{&l_1570,(void*)0,&g_10,(void*)0,&g_298,&g_951,&l_1579,&l_1570,(void*)0},{&g_90,&l_1580,&l_1579,&l_1580,(void*)0,&g_500,&g_298,&l_1583,&g_296},{&l_1580,(void*)0,&g_951,&l_1583,&g_90,&l_1579,&l_1570,&g_298,&g_500}},{{&l_1585[5][1],(void*)0,&l_1585[0][1],&l_1583,&l_1579,&g_296,&l_1583,&l_1580,&g_90},{&l_1576,&l_1579,(void*)0,&l_1580,&g_90,&l_1570,&l_1580,&l_1579,&g_951},{&g_296,&l_1580,&g_500,(void*)0,&l_1583,&l_1585[5][1],(void*)0,&l_1579,&l_1570},{&l_1583,&l_1579,(void*)0,&g_298,&g_951,&g_10,(void*)0,&l_1579,&l_1330}}};
                int i, j, k;
                (*g_412) ^= (p_13 >= (l_1683 == &l_1418));
                (*g_412) |= p_13;
                l_1686[1] = l_1685[6][0][3];
            }
        }
        ++g_1687;
    }
    for (g_173 = (-26); (g_173 < 7); g_173 = safe_add_func_int32_t_s_s(g_173, 1))
    { /* block id: 783 */
        float l_1696 = 0xD.324B57p+56;
        int32_t l_1699[1];
        float *l_1714 = (void*)0;
        float **l_1713 = &l_1714;
        float ***l_1712 = &l_1713;
        float ****l_1711 = &l_1712;
        int32_t *l_1742[4][2];
        uint32_t **l_1754 = (void*)0;
        int i, j;
        for (i = 0; i < 1; i++)
            l_1699[i] = (-3L);
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 2; j++)
                l_1742[i][j] = &g_500;
        }
        for (g_1687 = 0; (g_1687 < 19); ++g_1687)
        { /* block id: 786 */
            uint32_t l_1700 = 18446744073709551614UL;
            int32_t l_1701 = (-1L);
            uint8_t l_1717 = 0xA9L;
            const float **l_1740 = &l_1348;
            uint32_t *l_1749[7][6] = {{&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]},{&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]},{&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]},{&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]},{&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]},{&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]},{&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]}};
            uint16_t *l_1755 = &g_1207;
            int i, j;
            if ((safe_add_func_int64_t_s_s(p_13, 0x7FC2B63E0678FFEALL)))
            { /* block id: 787 */
                uint32_t l_1704[7][8] = {{0xD6E2AB5AL,0xC3FEE604L,0xCB74F963L,3UL,0xA28AB6ECL,0xA28AB6ECL,3UL,0xCB74F963L},{3UL,3UL,9UL,4294967295UL,0x9C95290AL,0x88C21DECL,2UL,0UL},{0x9C95290AL,0x88C21DECL,2UL,0UL,0x7A8C65C0L,0xCB74F963L,0x7A8C65C0L,0UL},{0x88C21DECL,9UL,0x88C21DECL,4294967295UL,0x84355FB1L,0xD6E2AB5AL,0xC3FEE604L,0xCB74F963L},{4294967295UL,1UL,4294967295UL,3UL,2UL,0x84355FB1L,0x84355FB1L,2UL},{4294967295UL,0x7A8C65C0L,0x7A8C65C0L,4294967295UL,0x84355FB1L,3UL,0UL,0xD6E2AB5AL},{0x88C21DECL,4294967295UL,3UL,0xC3FEE604L,0x7A8C65C0L,9UL,0xD6E2AB5AL,9UL}};
                int32_t l_1734 = (-1L);
                int32_t *****l_1737 = &l_1418;
                const float **l_1738 = &l_1348;
                int32_t l_1756[1][2];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_1756[i][j] = 0xFE72F9E8L;
                }
                for (g_856 = 0; (g_856 <= 2); g_856 += 1)
                { /* block id: 790 */
                    int32_t l_1702 = 0L;
                    int32_t l_1703 = 0x0B315BCCL;
                    uint32_t *l_1724 = &l_1704[0][0];
                    (*g_412) = (l_1701 = (p_13 | (((*g_150) ^= ((****g_1507) > (safe_div_func_int16_t_s_s(l_1699[0], 0x30F0L)))) || l_1700)));
                    --l_1704[4][2];
                    for (g_1207 = 0; (g_1207 <= 1); g_1207 += 1)
                    { /* block id: 797 */
                        int32_t l_1710 = 0x0F30D5CEL;
                        uint32_t **l_1725 = &l_1724;
                        int64_t l_1736 = (-7L);
                        int i, j, k;
                        l_1710 = ((safe_lshift_func_uint8_t_u_s(0x1EL, 4)) ^ (l_1704[(g_856 + 2)][(g_856 + 1)] = ((~0xE7F2L) ^ 0x773B88732A64952BLL)));
                        (*g_412) = (p_13 & (l_1711 != &g_762));
                        if (l_1710)
                            break;
                        l_1703 |= (safe_sub_func_int32_t_s_s(l_1717, ((1L || l_1699[0]) <= (((l_1735 = ((((((safe_add_func_int32_t_s_s((!(~((((safe_add_func_int32_t_s_s(p_13, (((l_1717 , &g_37[0][2]) == ((*l_1725) = l_1724)) != (((safe_add_func_int64_t_s_s(((safe_div_func_int16_t_s_s((-7L), (safe_add_func_int64_t_s_s(((safe_mod_func_int32_t_s_s(((*g_412) = (((**g_288) = ((l_1734 |= ((0x00C429CBB987F491LL != 0L) >= l_1699[0])) , 0x1703L)) && p_13)), l_1710)) >= 1L), p_13)))) , l_1699[0]), l_1704[(g_856 + 2)][(g_856 + 1)])) & p_13) & (-9L))))) == 1UL) || 0x76L) == 0x54L))), p_13)) , 0x16B46595L) == p_13) , 65532UL) >= p_13) , (****g_1026))) || l_1736) && g_544.f0))));
                    }
                    l_1737 = &g_1416;
                    for (l_1735 = 0; (l_1735 <= 2); l_1735 += 1)
                    { /* block id: 812 */
                        const float ***l_1739 = &l_1738;
                        l_1740 = ((*l_1739) = l_1738);
                    }
                }
                if (p_13)
                    continue;
                for (l_1734 = 4; (l_1734 >= 0); l_1734 -= 1)
                { /* block id: 820 */
                    for (g_856 = 4; (g_856 >= 0); g_856 -= 1)
                    { /* block id: 823 */
                        int32_t **l_1741 = &g_88;
                        l_1742[2][1] = ((*l_1741) = (void*)0);
                    }
                }
                (*g_670) = ((*g_412) ^= (safe_sub_func_uint32_t_u_u((safe_sub_func_int16_t_s_s(((safe_add_func_uint64_t_u_u(((void*)0 == (**g_1301)), (((l_1749[3][4] == &g_37[0][7]) == 65535UL) && ((safe_mul_func_int16_t_s_s(p_13, (((l_1701 |= 255UL) != (((*g_149) = func_61((safe_rshift_func_int8_t_s_s(1L, 6)), (((p_13 == 3UL) , l_1754) != &g_966), &l_1699[0], &l_1699[0])) != l_1755)) || l_1756[0][0]))) || 0x13L)))) != p_13), 0x5725L)), p_13)));
            }
            else
            { /* block id: 832 */
                (*g_412) |= p_13;
            }
        }
        return l_1758[6];
    }
    (*g_412) = ((l_1759 ^ p_13) <= p_13);
    return (*g_636);
}


/* ------------------------------------------ */
/* 
 * reads : g_296
 * writes: g_35
 */
static int16_t  func_26(int32_t * p_27, int32_t * p_28)
{ /* block id: 10 */
    uint32_t l_33[7] = {0xD4C01E07L,0xD46D2320L,0xD4C01E07L,0xD4C01E07L,0xD46D2320L,0xD4C01E07L,0xD4C01E07L};
    int32_t l_34 = 0xF283019CL;
    int32_t l_36[1][7][5] = {{{1L,0x18302CCAL,0x18302CCAL,1L,1L},{0x023024A1L,0xF5B872A6L,0x023024A1L,0xF5B872A6L,0x023024A1L},{1L,1L,0x18302CCAL,0x18302CCAL,1L},{0x43993911L,0xF5B872A6L,0x43993911L,0xF5B872A6L,0x43993911L},{1L,0x18302CCAL,0x18302CCAL,1L,1L},{0x023024A1L,0xF5B872A6L,0x023024A1L,0xF5B872A6L,0x023024A1L},{1L,1L,0x18302CCAL,0x18302CCAL,1L}}};
    uint16_t *l_45 = &g_46;
    int32_t *l_50 = &l_36[0][3][3];
    int32_t **l_49 = &l_50;
    uint8_t *l_1247 = &g_1248[4];
    uint8_t *l_1267[9][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_173,(void*)0,&g_173},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_173,(void*)0,&g_173},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
    int32_t l_1268 = (-4L);
    int32_t l_1269 = 0L;
    int32_t l_1270 = 0x3CC7F40AL;
    int8_t l_1271 = 0x37L;
    uint32_t l_1272 = 0x532101ABL;
    uint8_t l_1277 = 0x3DL;
    int16_t ****l_1280[1];
    int16_t *****l_1281 = &l_1280[0];
    int16_t ***l_1283 = &g_568;
    int16_t ****l_1282 = &l_1283;
    uint64_t *l_1284[8];
    int32_t l_1285 = 0xCAA617D5L;
    int32_t *l_1286 = &g_296;
    int32_t *l_1287[9];
    int64_t l_1288 = 0L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1280[i] = (void*)0;
    for (i = 0; i < 8; i++)
        l_1284[i] = &g_302[1];
    for (i = 0; i < 9; i++)
        l_1287[i] = &g_951;
    l_36[0][3][3] = (~(((void*)0 == p_27) >= (g_35 = (safe_lshift_func_int16_t_s_u((l_34 = l_33[2]), 6)))));
    return (*l_1286);
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_46 g_87 g_35 g_88 g_90 g_10 g_114 g_166 g_171 g_183 g_173 g_150 g_235 g_255 g_302 g_299 g_288 g_322 g_412 g_253 g_296 g_298 g_480 g_568 g_569 g_544.f0 g_500 g_546.f0 g_636 g_644 g_670 g_674 g_693 g_323 g_324 g_645 g_481 g_482 g_760 g_761 g_764 g_765 g_762 g_763 g_658.f0 g_800 g_817 g_856 g_877 g_886 g_980 g_981 g_1026 g_1029 g_1027 g_1057 g_1064 g_951 g_1030 g_1021.f2 g_1125 g_1126 g_1127 g_1207 g_1157 g_1223 g_1224 g_1225
 * writes: g_90 g_88 g_114 g_149 g_35 g_173 g_183 g_235 g_288 g_302 g_322 g_171 g_296 g_46 g_166 g_298 g_546.f2 g_569 g_644 g_500 g_674 g_759 g_768 g_765 g_482 g_255 g_299 g_877 g_966 g_412 g_1025 g_1027 g_1030 g_1057 g_951 g_886 g_1207 g_1242
 */
static uint8_t  func_39(int32_t * p_40, uint16_t  p_41, int32_t * p_42, uint32_t  p_43, uint64_t  p_44)
{ /* block id: 17 */
    int32_t l_53 = 0xA3807906L;
    uint32_t *l_628[7] = {&g_166[1][4][0],&g_166[0][6][0],&g_166[1][4][0],&g_166[1][4][0],&g_166[0][6][0],&g_166[1][4][0],&g_166[1][4][0]};
    int32_t **l_782 = &g_412;
    uint8_t l_799 = 0x74L;
    uint8_t *l_822 = &g_35;
    uint8_t **l_821 = &l_822;
    uint64_t **l_835[3][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
    uint16_t l_851 = 6UL;
    uint16_t l_854 = 3UL;
    int32_t l_869[7][8][3] = {{{1L,(-6L),0xFE6BF096L},{0x6C10EE9DL,0x6C10EE9DL,0x4A8D90FFL},{0x3B9896B9L,0L,0x4A8D90FFL},{0L,0L,0xFE6BF096L},{(-8L),0xC9B848B6L,(-1L)},{0x0F50DF98L,0L,0xFE6BF096L},{1L,0x0F50DF98L,0x4A8D90FFL},{(-8L),0xB1BF647AL,0x4A8D90FFL}},{{(-6L),8L,0xFE6BF096L},{(-1L),0L,(-1L)},{0x6C10EE9DL,0xB1BF647AL,0xFE6BF096L},{0x0F50DF98L,1L,0x4A8D90FFL},{(-1L),(-6L),0x4A8D90FFL},{0xB1BF647AL,0xC9B848B6L,0xFE6BF096L},{0x3B9896B9L,8L,(-1L)},{1L,(-6L),0xFE6BF096L}},{{0x6C10EE9DL,0x6C10EE9DL,0x4A8D90FFL},{0x3B9896B9L,0L,0x4A8D90FFL},{0L,0L,0xFE6BF096L},{(-8L),0xC9B848B6L,(-1L)},{0x0F50DF98L,0L,0xFE6BF096L},{1L,0x0F50DF98L,0x4A8D90FFL},{(-8L),0xB1BF647AL,0x4A8D90FFL},{(-6L),8L,0xFE6BF096L}},{{(-1L),0L,(-1L)},{0x6C10EE9DL,0xB1BF647AL,0xFE6BF096L},{0x0F50DF98L,1L,0x4A8D90FFL},{(-1L),(-6L),0x4A8D90FFL},{0xB1BF647AL,0xC9B848B6L,0xFE6BF096L},{0x3B9896B9L,8L,(-1L)},{1L,(-6L),0xFE6BF096L},{0x6C10EE9DL,0x6C10EE9DL,0x4A8D90FFL}},{{0x3B9896B9L,0L,0x4A8D90FFL},{0L,0L,0xFE6BF096L},{(-8L),0xC9B848B6L,(-1L)},{0x0F50DF98L,0L,0xFE6BF096L},{1L,0x0F50DF98L,0x4A8D90FFL},{(-8L),0xB1BF647AL,0x4A8D90FFL},{(-6L),8L,0xFE6BF096L},{(-1L),0L,(-1L)}},{{0xADF93A34L,0xF9619C86L,0xC9B848B6L},{0xC30B6C01L,(-1L),(-8L)},{(-5L),6L,(-8L)},{0xF9619C86L,0x23991079L,0xC9B848B6L},{(-7L),0xE2C5D6C3L,9L},{(-1L),6L,0xC9B848B6L},{0xADF93A34L,0xADF93A34L,(-8L)},{(-7L),0xBE657355L,(-8L)}},{{0xBE657355L,0x2D4127E9L,0xC9B848B6L},{5L,0x23991079L,9L},{0xC30B6C01L,0xBE657355L,0xC9B848B6L},{(-1L),0xC30B6C01L,(-8L)},{5L,0xF9619C86L,(-8L)},{6L,0xE2C5D6C3L,0xC9B848B6L},{(-5L),0x2D4127E9L,9L},{0xADF93A34L,0xF9619C86L,0xC9B848B6L}}};
    const struct S0 *l_1022[2];
    uint16_t **l_1082 = (void*)0;
    uint8_t ***l_1093 = &l_821;
    uint8_t ****l_1092 = &l_1093;
    int8_t l_1149 = (-8L);
    uint16_t l_1194 = 1UL;
    int32_t l_1204[6][8] = {{1L,1L,0x45FDA87EL,0L,0x45FDA87EL,1L,1L,0x45FDA87EL},{0xBFD77801L,0x45FDA87EL,0x45FDA87EL,0xBFD77801L,(-8L),0xBFD77801L,0x45FDA87EL,0x45FDA87EL},{0x45FDA87EL,(-8L),0L,0L,(-8L),0x45FDA87EL,(-8L),0L},{0xBFD77801L,(-8L),0xBFD77801L,0x45FDA87EL,0x45FDA87EL,0xBFD77801L,(-8L),0xBFD77801L},{1L,0x45FDA87EL,0L,0x45FDA87EL,1L,1L,0x45FDA87EL,0L},{1L,1L,0x45FDA87EL,0L,0x45FDA87EL,1L,1L,0x45FDA87EL}};
    int32_t *l_1205[2];
    int8_t l_1206 = 0x70L;
    uint64_t l_1208 = 0x930A9F64E16A3FD9LL;
    int16_t **l_1209 = (void*)0;
    int16_t ***l_1211[2][9] = {{&l_1209,&l_1209,&l_1209,&l_1209,&l_1209,&l_1209,&l_1209,&l_1209,&l_1209},{&g_568,&g_568,&g_568,&g_568,&g_568,&g_568,&g_568,&g_568,&g_568}};
    int16_t ****l_1210 = &l_1211[0][4];
    float *l_1212 = &g_482;
    int64_t *l_1229 = &g_171;
    int64_t **l_1228[8] = {&l_1229,&l_1229,&l_1229,&l_1229,&l_1229,&l_1229,&l_1229,&l_1229};
    int32_t l_1230 = 0x629531CDL;
    uint16_t l_1245[3][5][6] = {{{0UL,65527UL,0UL,0xEAF0L,0xEAF0L,0UL},{7UL,7UL,0xEAF0L,4UL,0xEAF0L,7UL},{0xEAF0L,65527UL,4UL,4UL,65527UL,0xEAF0L},{7UL,0xEAF0L,4UL,0xEAF0L,7UL,7UL},{0UL,0xEAF0L,0xEAF0L,0UL,65527UL,0UL}},{{0UL,65527UL,0UL,0xEAF0L,0xEAF0L,0UL},{7UL,7UL,0xEAF0L,4UL,0xEAF0L,7UL},{0xEAF0L,65527UL,4UL,4UL,65527UL,0xEAF0L},{7UL,0xEAF0L,4UL,0xEAF0L,7UL,7UL},{0UL,0xEAF0L,0xEAF0L,0UL,65527UL,0UL}},{{0UL,65527UL,0UL,0xEAF0L,0xEAF0L,0UL},{7UL,7UL,0xEAF0L,65527UL,0UL,0xEAF0L},{0UL,7UL,65527UL,65527UL,7UL,0UL},{0xEAF0L,0UL,65527UL,0UL,0xEAF0L,0xEAF0L},{4UL,0UL,0UL,4UL,7UL,4UL}}};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1022[i] = &g_1023;
    for (i = 0; i < 2; i++)
        l_1205[i] = &g_90;
    if ((safe_mod_func_uint8_t_u_u((((l_53 , ((void*)0 != &g_10)) || l_53) && (safe_sub_func_uint8_t_u_u(func_56(func_61(((safe_sub_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_s((func_70(func_75((g_166[2][1][0] = (p_41 || (safe_div_func_int16_t_s_s(((l_53 != 0xB415AEE2L) < g_3), func_83((((l_53 | 4L) ^ g_46) != g_46), g_87[1][2], g_46))))), l_53, g_544.f0, l_53, g_10), g_500, p_43, g_546.f0) || l_53), 2)) == g_255[2][1]), 0xFA237314C1927976LL)) && p_43), g_255[0][1], l_628[3], p_40), p_41, p_41, l_782), g_255[0][1]))), l_53)))
    { /* block id: 281 */
        int32_t l_814[9][3] = {{(-3L),(-2L),0x654578ADL},{(-1L),0x72F2F9B8L,(-1L)},{0x654578ADL,(-2L),(-3L)},{1L,(-1L),(-3L)},{0L,1L,(-1L)},{6L,6L,0x654578ADL},{0L,0x654578ADL,1L},{1L,0x654578ADL,0L},{0x654578ADL,6L,6L}};
        float *l_815 = &g_482;
        int32_t l_816[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
        int32_t l_818[5] = {2L,2L,2L,2L,2L};
        uint16_t l_819 = 65533UL;
        int i, j;
        (*g_412) = (((safe_mod_func_int64_t_s_s((safe_mul_func_int16_t_s_s(((safe_sub_func_int16_t_s_s((safe_mod_func_int64_t_s_s(((safe_sub_func_int64_t_s_s((((((l_799 = (**g_288)) , ((g_800 , ((((-2L) >= (((**g_568) = (safe_lshift_func_int8_t_s_s(((**l_782) || (safe_lshift_func_uint8_t_u_s((((!(safe_div_func_float_f_f((**g_763), (safe_add_func_float_f_f(((l_816[5] = (l_814[5][1] = ((safe_add_func_float_f_f(((*l_815) = (safe_div_func_float_f_f(l_814[5][1], 0x1.4p-1))), p_41)) >= l_814[5][1]))) < 0x9.4E27EBp+46), 0x1.5p-1))))) , g_817) , (**l_782)), (**l_782)))), 3))) ^ p_41)) < 0x7CL) && p_41)) , 2L)) > l_818[3]) & (**l_782)) & p_41), p_43)) > (**l_782)), p_44)), 0x0B4AL)) & 255UL), 0xA8F6L)), p_44)) <= p_44) | 0x785FD44B40AA07AFLL);
        (*g_412) |= l_819;
    }
    else
    { /* block id: 289 */
        int8_t l_830 = 0x5BL;
        int16_t ***l_832 = (void*)0;
        int16_t ***l_834 = &g_568;
        int16_t ****l_833 = &l_834;
        int32_t l_863 = 0xF549D2A7L;
        int32_t l_871 = 0xA0A95F32L;
        int32_t l_882[4][4] = {{0x741DAB63L,0x741DAB63L,0x741DAB63L,0x741DAB63L},{0x741DAB63L,0x741DAB63L,0x741DAB63L,0x741DAB63L},{0x741DAB63L,0x741DAB63L,0x741DAB63L,0x741DAB63L},{0x741DAB63L,0x741DAB63L,0x741DAB63L,0x741DAB63L}};
        int8_t l_892[7] = {8L,2L,2L,8L,2L,2L,8L};
        int32_t l_893 = 0x57019B4BL;
        float l_894[3][9] = {{(-0x1.8p+1),(-0x2.8p-1),0x9.D62322p-18,0x9.D62322p-18,(-0x2.8p-1),(-0x1.8p+1),(-0x9.4p-1),(-0x2.8p-1),(-0x9.4p-1)},{(-0x1.8p+1),(-0x2.8p-1),0x9.D62322p-18,0x9.D62322p-18,(-0x2.8p-1),(-0x1.8p+1),(-0x9.4p-1),(-0x2.8p-1),(-0x9.4p-1)},{(-0x1.8p+1),(-0x2.8p-1),0x9.D62322p-18,0x9.D62322p-18,(-0x2.8p-1),(-0x1.8p+1),(-0x9.4p-1),(-0x2.8p-1),(-0x9.4p-1)}};
        uint32_t l_895 = 0x0F48196DL;
        int64_t *l_906[6];
        int64_t **l_935 = &l_906[1];
        uint16_t ** const *l_1067 = &g_149;
        float **l_1098 = (void*)0;
        float ***l_1097[3][4][9] = {{{&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098,&l_1098,&l_1098,&l_1098},{&l_1098,(void*)0,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098},{&l_1098,(void*)0,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098},{&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,(void*)0}},{{&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098,&l_1098,(void*)0,(void*)0},{&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098,&l_1098,&l_1098,&l_1098},{&l_1098,(void*)0,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098},{&l_1098,(void*)0,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098}},{{&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098},{&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098},{&l_1098,(void*)0,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,&l_1098,(void*)0},{&l_1098,&l_1098,&l_1098,&l_1098,(void*)0,&l_1098,(void*)0,&l_1098,(void*)0}}};
        uint64_t l_1170 = 0x61D9629CACBA4EA3LL;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_906[i] = &g_171;
lbl_940:
        for (l_53 = 0; (l_53 <= 3); l_53 += 1)
        { /* block id: 292 */
            uint8_t **l_823 = &l_822;
            int8_t *l_831 = &l_830;
            int i;
            (*g_412) = ((!((l_821 = l_821) == l_823)) && (safe_add_func_int64_t_s_s(((g_114[l_53] ^ (safe_add_func_int16_t_s_s((((((p_43 > (((void*)0 == &g_568) || (safe_rshift_func_int16_t_s_u(((((*l_831) = ((l_830 , (**l_782)) || (4294967288UL | 0x95270427L))) <= (**g_644)) >= p_43), p_43)))) , (void*)0) != (void*)0) > p_43) > (*p_40)), (*g_150)))) & p_44), p_41)));
        }
        if (((l_832 != ((*l_833) = (void*)0)) > (l_835[0][1] == (g_114[0] , l_835[2][1]))))
        { /* block id: 298 */
            uint32_t l_857 = 0x01845D9EL;
            uint16_t l_872 = 4UL;
            int32_t l_875 = 0xCB17A33CL;
            int32_t l_876 = (-10L);
            uint32_t l_883 = 0x338C2693L;
            int32_t *l_887 = &g_500;
            int32_t *l_888 = &g_296;
            int32_t *l_889 = &l_882[2][3];
            int32_t *l_890[10] = {&l_869[1][7][0],(void*)0,&l_876,(void*)0,&l_869[1][7][0],&l_869[1][7][0],(void*)0,&l_876,(void*)0,&l_869[1][7][0]};
            int8_t l_891 = (-1L);
            int i;
            for (g_235 = 0; (g_235 <= 41); g_235 = safe_add_func_int16_t_s_s(g_235, 4))
            { /* block id: 301 */
                int32_t *l_838 = &g_500;
                int16_t *l_855[7][6][6] = {{{&g_856,&g_856,(void*)0,&g_856,&g_856,&g_856},{(void*)0,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,(void*)0,&g_856,&g_856},{&g_856,(void*)0,&g_856,&g_856,&g_856,&g_856}},{{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{(void*)0,&g_856,(void*)0,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,(void*)0,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856}},{{&g_856,(void*)0,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,(void*)0},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856}},{{&g_856,&g_856,&g_856,&g_856,(void*)0,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{(void*)0,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,(void*)0,&g_856,(void*)0,&g_856,(void*)0},{&g_856,(void*)0,&g_856,&g_856,&g_856,&g_856}},{{&g_856,(void*)0,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,(void*)0,(void*)0},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{(void*)0,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,(void*)0}},{{&g_856,&g_856,(void*)0,&g_856,&g_856,(void*)0},{&g_856,&g_856,&g_856,&g_856,(void*)0,&g_856},{&g_856,(void*)0,&g_856,(void*)0,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,(void*)0,(void*)0},{&g_856,(void*)0,(void*)0,&g_856,(void*)0,(void*)0}},{{&g_856,(void*)0,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,(void*)0},{&g_856,(void*)0,&g_856,&g_856,(void*)0,&g_856},{&g_856,(void*)0,&g_856,&g_856,(void*)0,&g_856},{&g_856,&g_856,&g_856,&g_856,&g_856,&g_856}}};
                int8_t *l_858 = (void*)0;
                int8_t *l_859 = &g_114[0];
                int8_t l_860 = 0L;
                float l_862 = 0xC.AAA1A9p-98;
                int32_t l_870[8] = {0xEF28E25FL,(-10L),0xEF28E25FL,(-10L),0xEF28E25FL,(-10L),0xEF28E25FL,(-10L)};
                int i, j, k;
                l_838 = p_40;
                l_860 ^= ((((*l_859) = (safe_mod_func_int8_t_s_s((((**g_288) > (((**g_644) , (safe_sub_func_int32_t_s_s(4L, (safe_rshift_func_uint8_t_u_u((((**l_782) >= (((safe_rshift_func_int16_t_s_s((safe_sub_func_int32_t_s_s(l_851, ((l_830 > ((l_857 = (safe_add_func_int16_t_s_s(((**g_568) |= (l_854 != (254UL <= (((*g_670) < l_830) , l_830)))), (**g_288)))) || p_44)) && (**l_782)))), 12)) != 0xABL) ^ (*l_838))) < 1L), (**l_782)))))) & 0xEE97L)) | g_856), 0x98L))) | 0UL) <= 0xDB77L);
                for (l_830 = 0; (l_830 >= 0); l_830 -= 1)
                { /* block id: 309 */
                    int32_t *l_861 = (void*)0;
                    int32_t *l_864 = &g_90;
                    int32_t *l_865 = &g_90;
                    int32_t *l_866 = (void*)0;
                    int32_t *l_867 = &g_500;
                    int32_t *l_868[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_868[i] = &g_298;
                    ++l_872;
                    for (g_299 = 0; (g_299 <= 2); g_299 += 1)
                    { /* block id: 313 */
                        int32_t l_880 = (-1L);
                        int32_t l_881 = 0x38A19A5FL;
                        int i;
                        g_877[4][3][1]--;
                        l_883++;
                        if (g_302[l_830])
                            continue;
                    }
                }
                return g_886;
            }
            l_895--;
            (*l_887) &= l_830;
        }
        else
        { /* block id: 323 */
            int64_t *l_907[1];
            const int32_t l_929 = 0xDD02D426L;
            int16_t l_944 = 7L;
            int32_t l_946 = 0xF7D1B0C9L;
            int32_t l_948 = 0L;
            int32_t l_952 = (-1L);
            int32_t l_953 = 1L;
            int32_t l_954 = 0xDE76C576L;
            int32_t l_955 = 0xB7EFC088L;
            int32_t l_958 = 1L;
            int32_t l_959 = 0xC4B00689L;
            uint16_t l_960 = 65535UL;
            struct S0 *l_1024 = &g_546;
            float l_1045 = (-0x1.3p-1);
            float *l_1071 = &l_1045;
            float **l_1070 = &l_1071;
            uint8_t ***l_1077 = &l_821;
            uint8_t ****l_1076[4][7][6] = {{{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{(void*)0,&l_1077,&l_1077,&l_1077,(void*)0,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,(void*)0},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{(void*)0,(void*)0,&l_1077,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077}},{{&l_1077,(void*)0,&l_1077,(void*)0,&l_1077,&l_1077},{&l_1077,&l_1077,(void*)0,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,(void*)0,&l_1077,(void*)0,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{(void*)0,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077}},{{&l_1077,&l_1077,(void*)0,(void*)0,&l_1077,&l_1077},{&l_1077,(void*)0,(void*)0,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,(void*)0,&l_1077},{(void*)0,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,(void*)0,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,&l_1077,&l_1077,&l_1077},{&l_1077,&l_1077,&l_1077,(void*)0,&l_1077,&l_1077}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&l_1077},{(void*)0,&l_1077,(void*)0,(void*)0,&l_1077,&l_1077},{&l_1077,&l_1077,(void*)0,&l_1077,&l_1077,&l_1077},{&l_1077,(void*)0,(void*)0,(void*)0,&l_1077,(void*)0},{&l_1077,(void*)0,(void*)0,&l_1077,&l_1077,(void*)0},{(void*)0,&l_1077,&l_1077,&l_1077,&l_1077,(void*)0},{&l_1077,&l_1077,&l_1077,&l_1077,(void*)0,&l_1077}}};
            int i, j, k;
            for (i = 0; i < 1; i++)
                l_907[i] = &g_171;
            for (l_895 = (-21); (l_895 > 35); l_895 = safe_add_func_uint8_t_u_u(l_895, 5))
            { /* block id: 326 */
                uint32_t l_905 = 0x374E27DEL;
                int64_t **l_908[9] = {&l_907[0],&l_907[0],&l_907[0],&l_907[0],&l_907[0],&l_907[0],&l_907[0],&l_907[0],&l_907[0]};
                int16_t **l_924 = &g_569[2][0][3];
                int32_t l_930 = 0x0AD59B6BL;
                int32_t l_947 = 0x33057CD0L;
                int32_t l_949 = 8L;
                uint16_t l_963[5][3][2] = {{{65531UL,65534UL},{65534UL,65531UL},{65534UL,65534UL}},{{65531UL,65534UL},{65534UL,65531UL},{65534UL,65534UL}},{{65531UL,65534UL},{65534UL,65531UL},{65534UL,65534UL}},{{65531UL,65534UL},{65534UL,65531UL},{65534UL,65534UL}},{{65531UL,65534UL},{65534UL,65531UL},{65534UL,65534UL}}};
                int i, j, k;
                if (((((safe_div_func_uint16_t_u_u((**l_782), (((+0xFEB811D9L) , l_905) || (&p_43 != &g_166[1][3][0])))) , l_906[1]) != (l_907[0] = l_907[0])) , (safe_mod_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_s(((*l_822) = p_41), ((safe_rshift_func_uint8_t_u_s((safe_mul_func_int16_t_s_s(0xF1A9L, p_43)), 2)) && (**l_782)))), (-6L)))))
                { /* block id: 329 */
                    for (p_44 = (-13); (p_44 > 40); ++p_44)
                    { /* block id: 332 */
                        int16_t **l_925 = &g_569[1][0][7];
                        int32_t ***l_926 = &l_782;
                        l_930 = ((safe_lshift_func_uint8_t_u_s((+(p_43 | ((**l_821) = (safe_mul_func_int16_t_s_s(((l_905 , l_924) != l_925), ((*g_670) ^ (&g_412 != ((*l_926) = l_782)))))))), 3)) & (safe_add_func_uint16_t_u_u((**g_288), (p_43 != l_929))));
                    }
                    for (g_171 = (-20); (g_171 <= (-5)); g_171 = safe_add_func_uint16_t_u_u(g_171, 9))
                    { /* block id: 339 */
                        return p_41;
                    }
                    for (g_173 = 0; (g_173 <= 1); g_173 += 1)
                    { /* block id: 344 */
                        int32_t *l_933 = &l_882[2][3];
                        int64_t ***l_934[9][5] = {{(void*)0,(void*)0,&l_908[7],&l_908[7],&l_908[7]},{&l_908[7],&l_908[7],&l_908[7],&l_908[7],&l_908[7]},{&l_908[0],&l_908[7],&l_908[1],&l_908[1],&l_908[7]},{&l_908[7],&l_908[7],&l_908[7],&l_908[4],&l_908[4]},{&l_908[1],(void*)0,&l_908[1],&l_908[1],&l_908[7]},{&l_908[7],&l_908[7],&l_908[4],&l_908[7],&l_908[7]},{&l_908[1],&l_908[0],(void*)0,&l_908[7],(void*)0},{&l_908[7],&l_908[7],&l_908[4],&l_908[7],&l_908[3]},{&l_908[0],&l_908[1],&l_908[1],&l_908[0],(void*)0}};
                        int i, j;
                        (*l_933) |= (*g_412);
                        if ((*p_40))
                            break;
                        if ((*g_670))
                            continue;
                        l_935 = &l_906[1];
                    }
                }
                else
                { /* block id: 350 */
                    const int32_t *l_937 = (void*)0;
                    const int32_t **l_936 = &l_937;
                    int32_t l_943 = (-8L);
                    int32_t l_945 = (-1L);
                    int32_t l_950 = 0xF151B2FFL;
                    int32_t l_956 = 0xFE3440BAL;
                    int32_t l_957[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_957[i] = (-2L);
                    (*l_936) = &l_929;
                    for (g_90 = (-15); (g_90 == (-28)); g_90 = safe_sub_func_uint64_t_u_u(g_90, 1))
                    { /* block id: 354 */
                        int8_t l_941 = (-1L);
                        int32_t *l_942[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_942[i] = &g_500;
                        if (l_851)
                            goto lbl_940;
                        if (l_929)
                            goto lbl_940;
                        --l_960;
                        return p_44;
                    }
                }
                if (l_963[4][0][0])
                    continue;
            }
            for (p_41 = 0; (p_41 <= 2); p_41 += 1)
            { /* block id: 365 */
                uint32_t **l_964 = (void*)0;
                uint32_t **l_965[1];
                int16_t l_977 = 0xE387L;
                float *l_989 = &l_894[1][1];
                float **l_988 = &l_989;
                float ** const *l_987[3][9][1] = {{{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988}},{{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988}},{{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988},{&l_988}}};
                int32_t l_1042[8];
                uint8_t ***l_1074 = &l_821;
                uint8_t ****l_1073 = &l_1074;
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_965[i] = &l_628[4];
                for (i = 0; i < 8; i++)
                    l_1042[i] = 0x7807C4DAL;
                if (((g_966 = &p_43) == &p_43))
                { /* block id: 367 */
                    int32_t l_986[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_986[i] = 1L;
                    (*g_412) = (safe_sub_func_uint8_t_u_u(255UL, g_302[p_41]));
                    for (l_893 = 0; (l_893 <= 2); l_893 += 1)
                    { /* block id: 371 */
                        int i;
                        if (g_302[l_893])
                            break;
                        (*l_782) = p_40;
                    }
                    l_986[2] ^= (safe_mod_func_uint64_t_u_u((p_44 = (safe_mul_func_uint8_t_u_u((((g_171 <= (g_302[p_41] , (safe_div_func_uint32_t_u_u((((((safe_div_func_uint16_t_u_u(p_44, l_977)) | 0x7059D2E25028AC37LL) >= ((safe_div_func_float_f_f((g_980 , (g_981 , 0x9.89BA51p-73)), (safe_mul_func_float_f_f(((safe_add_func_float_f_f(l_977, (**l_782))) >= 0x6.953519p+81), l_977)))) , p_43)) && (*g_645)) <= l_954), 0xA0650E8EL)))) & 1UL) >= p_44), p_43))), g_302[p_41]));
                }
                else
                { /* block id: 377 */
                    const float l_1002 = 0x9.7p-1;
                    p_42 = (*l_782);
                    if ((((**l_782) = ((void*)0 == l_987[0][1][0])) < 0x56EFL))
                    { /* block id: 380 */
                        const struct S0 *l_1020 = &g_1021[0][0][0];
                        const struct S0 **l_1019[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_1019[i] = &l_1020;
                        l_871 = (((safe_add_func_int64_t_s_s(((g_1025 = (safe_lshift_func_int8_t_s_s((safe_sub_func_int16_t_s_s(((safe_mul_func_int16_t_s_s(0xD91CL, ((safe_rshift_func_int16_t_s_u((safe_sub_func_int8_t_s_s(0x46L, l_929)), 15)) & (0L > 247UL)))) >= (**l_782)), (safe_mod_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(((safe_sub_func_uint32_t_u_u(4294967295UL, (safe_sub_func_uint64_t_u_u(((safe_mod_func_int32_t_s_s((((safe_mul_func_int8_t_s_s(((safe_add_func_int32_t_s_s((safe_div_func_uint16_t_u_u((((l_1022[0] = &g_546) == l_1024) ^ 0xCA9F9536835C5EECLL), 0xD32BL)), (*p_40))) <= 0x01BC4A14L), (-7L))) , 0UL) ^ 0x1A9AC5B7CA0A4185LL), (*p_40))) > 0x1C82272EDD31C3EBLL), (**l_782))))) != p_41), p_41)), p_41)))), 0))) , p_43), g_235)) > p_43) | p_44);
                        if (l_892[3])
                            continue;
                        (*g_1026) = &g_644;
                        if ((*g_670))
                            continue;
                    }
                    else
                    { /* block id: 387 */
                        const int32_t *l_1028 = (void*)0;
                        (*g_1029) = l_1028;
                    }
                }
                (*l_782) = l_989;
                if ((safe_rshift_func_uint16_t_u_s(0x5384L, 8)))
                { /* block id: 392 */
                    int8_t *l_1043 = &l_892[3];
                    int32_t l_1044[3][4][4] = {{{(-1L),0x625198A9L,0x106CB8B2L,0L},{(-1L),0xE2A52E47L,0x625198A9L,(-8L)},{9L,0L,0L,9L},{0x10D645EBL,3L,9L,1L}},{{0x106CB8B2L,(-8L),1L,0x63E3E8E8L},{8L,0x85654B4EL,(-6L),0x63E3E8E8L},{3L,(-8L),0x1AEA08E3L,1L},{0xE2A52E47L,3L,0xE2A52E47L,9L}},{{(-6L),0L,(-1L),(-8L)},{0x63E3E8E8L,0xE2A52E47L,1L,0L},{0x384AB39DL,0x625198A9L,1L,0x85654B4EL},{0x63E3E8E8L,0xBA9ECE78L,(-1L),(-1L)}}};
                    int i, j, k;
                    (*g_412) = (((safe_mod_func_int64_t_s_s((safe_mod_func_int64_t_s_s(p_41, ((p_43 , (safe_mul_func_float_f_f(((+((safe_add_func_float_f_f((((*l_1043) = (l_871 && ((0x5A19L | (l_977 >= 0x271D8CE2L)) <= ((****g_1026) != (((l_1042[0] = l_882[0][1]) > (p_44 = l_959)) && 0x08L))))) , l_1042[0]), 0x6.3D32A1p+15)) < 0x0.6p+1)) == l_1044[1][1][1]), 0x8.Ap-1))) , 0xEB4B1C40E3134FEELL))), p_41)) || p_43) , l_1042[0]);
                }
                else
                { /* block id: 397 */
                    int32_t *l_1046 = &l_882[2][1];
                    int32_t *l_1047 = &l_893;
                    int32_t *l_1048 = &g_951;
                    int32_t *l_1049 = (void*)0;
                    int32_t l_1050 = 1L;
                    int32_t *l_1051 = &l_948;
                    int32_t *l_1052 = &l_869[1][7][0];
                    int32_t *l_1053 = &l_955;
                    int32_t *l_1054 = &l_893;
                    int32_t *l_1055 = (void*)0;
                    int32_t *l_1056[1][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                    float **l_1072 = &l_989;
                    uint8_t *****l_1075[5][2][5] = {{{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073},{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073}},{{&l_1073,&l_1073,&l_1073,&l_1073,(void*)0},{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073}},{{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073},{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073}},{{(void*)0,&l_1073,&l_1073,&l_1073,&l_1073},{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073}},{{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073},{&l_1073,&l_1073,&l_1073,&l_1073,&l_1073}}};
                    int i, j, k;
                    ++g_1057[7][1][2];
                    (*l_1048) ^= (safe_add_func_int32_t_s_s((*p_40), ((*l_1047) = (safe_add_func_int64_t_s_s(g_1064, ((((safe_lshift_func_int8_t_s_s((((l_1067 == (p_41 , &g_288)) & (((((((l_1070 != l_1072) & ((l_1076[3][3][0] = l_1073) != (void*)0)) , l_952) & g_1057[7][1][2]) != p_44) ^ g_546.f0) >= 0x45CB1B2DL)) || g_817.f0), g_302[1])) > p_43) != p_44) & p_41))))));
                }
                if (l_830)
                    continue;
            }
        }
        for (g_951 = 28; (g_951 <= 27); g_951 = safe_sub_func_uint32_t_u_u(g_951, 5))
        { /* block id: 408 */
            const int32_t *l_1081 = (void*)0;
            int32_t l_1133 = 0x2AAB0376L;
            int32_t l_1148 = 0x36E8AB35L;
            int32_t l_1152 = 0xA116FA23L;
            int32_t l_1154 = (-7L);
            int32_t l_1156 = 1L;
            int32_t l_1158 = 0x7DDE8B0CL;
            int32_t l_1159 = 1L;
            int32_t l_1164 = 0x930B620FL;
            int32_t l_1169 = 7L;
            if ((**l_782))
            { /* block id: 409 */
                const int32_t **l_1080[3][6][5] = {{{&g_1030,&g_1030,&g_1030,(void*)0,(void*)0},{(void*)0,&g_1030,&g_1030,&g_1030,&g_1030},{&g_1030,&g_1030,(void*)0,(void*)0,&g_1030},{&g_1030,(void*)0,&g_1030,&g_1030,&g_1030},{&g_1030,&g_1030,&g_1030,(void*)0,&g_1030},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{&g_1030,&g_1030,&g_1030,(void*)0,&g_1030},{&g_1030,&g_1030,&g_1030,(void*)0,&g_1030},{&g_1030,(void*)0,(void*)0,&g_1030,&g_1030},{(void*)0,&g_1030,&g_1030,&g_1030,(void*)0},{&g_1030,(void*)0,&g_1030,&g_1030,&g_1030},{&g_1030,&g_1030,(void*)0,&g_1030,&g_1030}},{{&g_1030,&g_1030,&g_1030,(void*)0,&g_1030},{&g_1030,&g_1030,&g_1030,(void*)0,(void*)0},{&g_1030,&g_1030,(void*)0,(void*)0,&g_1030},{&g_1030,&g_1030,&g_1030,(void*)0,&g_1030},{&g_1030,&g_1030,&g_1030,&g_1030,&g_1030},{&g_1030,&g_1030,&g_1030,&g_1030,(void*)0}}};
                int i, j, k;
                l_1081 = (*g_1029);
                return (*g_645);
            }
            else
            { /* block id: 412 */
                uint8_t l_1085 = 0x61L;
                int32_t l_1094 = (-1L);
                int32_t l_1145 = 6L;
                int16_t l_1150[8][7][4] = {{{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)},{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)}},{{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)},{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL}},{{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)},{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x961CL,8L}},{{0x961CL,8L,0x781BL,(-10L)},{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)},{8L,8L,0x8E42L,8L}},{{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)},{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL}},{{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)},{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x961CL,8L},{0x961CL,8L,0x781BL,(-10L)}},{{8L,8L,0x8E42L,8L},{0x781BL,0L,0x8E42L,0xA92FL},{8L,(-3L),0x781BL,0xA92FL},{0x961CL,0L,0x8E42L,(-10L)},{0x8E42L,(-10L),0x961CL,0xA92FL},{0x781BL,(-10L),0x4BA7L,(-10L)},{0x961CL,(-3L),0x4BA7L,0L}},{{0x781BL,8L,0x961CL,0L},{0x8E42L,(-3L),0x8E42L,(-10L)},{0x8E42L,(-10L),0x961CL,0xA92FL},{0x781BL,(-10L),0x4BA7L,(-10L)},{0x961CL,(-3L),0x4BA7L,0L},{0x781BL,8L,0x961CL,0L},{0x8E42L,(-3L),0x8E42L,(-10L)}}};
                int32_t l_1151[6];
                int i, j, k;
                for (i = 0; i < 6; i++)
                    l_1151[i] = (-10L);
                if (((&g_150 != l_1082) < l_892[0]))
                { /* block id: 413 */
                    float *l_1083[7] = {&l_894[1][1],&g_482,&l_894[1][1],&l_894[1][1],&g_482,&l_894[1][1],&l_894[1][1]};
                    int16_t l_1084 = 0x58CAL;
                    uint8_t ****l_1090 = (void*)0;
                    int64_t l_1104[5] = {0x3BD92820B26A5D27LL,0x3BD92820B26A5D27LL,0x3BD92820B26A5D27LL,0x3BD92820B26A5D27LL,0x3BD92820B26A5D27LL};
                    int32_t l_1110[8][10] = {{0L,0x231BE62AL,0xD7AA4E98L,0x231BE62AL,0L,0L,0L,0L,0x231BE62AL,0xD7AA4E98L},{0xAA92EBC7L,0xAA92EBC7L,0xD7AA4E98L,0L,0x33EAF989L,0L,0xD7AA4E98L,0xAA92EBC7L,0xAA92EBC7L,0xD7AA4E98L},{0x231BE62AL,0L,0L,0L,0L,0x231BE62AL,0xD7AA4E98L,0x231BE62AL,0L,0L},{(-1L),0xAA92EBC7L,(-1L),0L,0xD7AA4E98L,0xD7AA4E98L,0L,(-1L),0xAA92EBC7L,(-1L)},{(-1L),0x231BE62AL,0xAA92EBC7L,0L,0xAA92EBC7L,0x231BE62AL,(-1L),(-1L),0x231BE62AL,0xAA92EBC7L},{0x231BE62AL,(-1L),(-1L),0x231BE62AL,0xAA92EBC7L,0L,0xAA92EBC7L,0x231BE62AL,(-1L),(-1L)},{0xAA92EBC7L,(-1L),0L,0xD7AA4E98L,0xD7AA4E98L,0L,(-1L),0xAA92EBC7L,(-1L),0L},{0L,0x231BE62AL,0xD7AA4E98L,0x231BE62AL,0L,0L,0L,0L,0x231BE62AL,0xD7AA4E98L}};
                    int i, j;
                    l_1085 = (l_1084 = (***g_762));
                    for (g_886 = 0; (g_886 < 40); ++g_886)
                    { /* block id: 418 */
                        uint8_t ***l_1089 = (void*)0;
                        uint8_t ****l_1088 = &l_1089;
                        uint8_t *****l_1091 = &l_1090;
                        int32_t l_1109 = 0xFE7E64D5L;
                        int32_t *l_1111 = &l_893;
                        (*g_412) = 5L;
                        (*l_782) = (*l_782);
                        l_1094 ^= (l_1088 != (l_1092 = ((*l_1091) = l_1090)));
                        (*l_1111) ^= (safe_add_func_uint32_t_u_u((l_1097[2][0][6] != (void*)0), (((safe_mul_func_int8_t_s_s(((p_44 |= (safe_unary_minus_func_uint32_t_u(p_43))) | (((((((l_1094 = (safe_rshift_func_uint8_t_u_u(l_1104[4], 1))) , (p_43 < (l_1110[2][3] = ((safe_rshift_func_uint8_t_u_u(((**l_782) = (((*g_670) = (safe_mod_func_int64_t_s_s(0x869A9F5E18279553LL, g_1021[0][0][0].f2))) <= 0xAC37E8E9L)), 5)) & l_1109)))) != 0x26L) != 252UL) && 65535UL) == (*g_150)) >= 0x1BL)), p_41)) , g_800.f2) , (*g_670))));
                    }
                    for (l_863 = 0; (l_863 != (-4)); --l_863)
                    { /* block id: 433 */
                        uint32_t l_1124 = 0xD30F5CC5L;
                        (**l_782) |= (((safe_mul_func_int16_t_s_s(((((0x6664CBA9L > ((!(((safe_unary_minus_func_uint64_t_u(((l_1094 | (safe_add_func_uint16_t_u_u((l_1110[1][9] &= 0x7D04L), l_863))) , (p_44++)))) , &p_43) == (void*)0)) || l_1124)) , ((**g_288) = p_43)) ^ (g_1125 == &g_1126)) && 0xD4L), l_1104[4])) & 3UL) < l_1124);
                        (*g_412) = 0x6D0597BDL;
                    }
                    for (l_851 = 0; (l_851 <= 1); l_851 += 1)
                    { /* block id: 442 */
                        (*g_636) = (void*)0;
                    }
                }
                else
                { /* block id: 445 */
                    int32_t l_1132 = 0x8A8B6BF0L;
                    int32_t l_1144 = 0L;
                    int32_t l_1146 = 8L;
                    int32_t l_1147 = 0L;
                    int32_t l_1153 = 0x08409D43L;
                    int32_t l_1155 = 1L;
                    int32_t l_1160 = 0L;
                    int32_t l_1161 = 0xD75A80A7L;
                    int32_t l_1162 = 0L;
                    int32_t l_1163 = 0L;
                    int32_t l_1165 = 0xD693C50CL;
                    int32_t l_1166 = 1L;
                    int32_t l_1167 = (-7L);
                    int32_t l_1168 = 8L;
                    if ((**l_782))
                    { /* block id: 446 */
                        int32_t *l_1134 = &l_869[5][2][0];
                        int32_t *l_1135 = &l_1094;
                        int32_t *l_1136 = &l_863;
                        int32_t *l_1137 = &l_882[2][3];
                        int32_t *l_1138 = &l_882[2][3];
                        int32_t *l_1139 = &l_1132;
                        int32_t *l_1140 = &g_500;
                        int32_t *l_1141 = &l_863;
                        int32_t *l_1142 = &l_882[2][1];
                        int32_t *l_1143[9][8][3] = {{{&l_882[2][3],(void*)0,(void*)0},{&l_871,&l_882[2][3],&l_1132},{(void*)0,&l_893,&l_893},{&l_882[2][3],&l_871,&l_893},{&g_90,&l_893,&g_90},{&g_951,&l_882[2][3],&l_871},{&l_1133,(void*)0,&l_1132},{&g_296,&l_882[2][3],&g_500}},{{&l_863,&l_893,&l_1094},{&g_298,&l_871,&l_1132},{&l_1133,&l_893,&g_296},{&l_869[1][7][0],&l_882[2][3],&l_882[2][3]},{&l_863,(void*)0,&g_90},{&l_863,&l_882[2][3],&l_1133},{(void*)0,&l_893,(void*)0},{(void*)0,&l_871,&l_1133}},{{&l_882[0][3],&l_893,&l_871},{&l_1133,&l_882[2][3],&l_863},{&l_882[2][3],(void*)0,(void*)0},{&l_1133,&l_869[1][6][0],(void*)0},{&l_1094,&l_869[1][7][0],&l_869[1][7][0]},{&l_1132,(void*)0,&l_863},{&g_90,&l_869[1][7][0],&l_869[1][7][0]},{&l_871,&l_869[1][6][0],(void*)0}},{{&l_871,&l_863,&l_882[2][3]},{&l_1132,&l_869[1][6][0],&g_296},{&l_893,&l_869[1][7][0],&g_951},{&l_1133,(void*)0,&l_869[1][7][0]},{&l_1132,&l_869[1][7][0],&l_863},{&l_863,&l_869[1][6][0],&l_869[1][6][0]},{&g_296,&l_863,(void*)0},{&l_893,&l_869[1][6][0],(void*)0}},{{(void*)0,&l_869[1][7][0],&l_863},{&g_500,(void*)0,&l_882[2][3]},{(void*)0,&l_869[1][7][0],&l_871},{&l_882[2][3],&l_869[1][6][0],&g_90},{&g_90,&l_863,(void*)0},{&l_1133,&l_869[1][6][0],(void*)0},{&l_1094,&l_869[1][7][0],&l_869[1][7][0]},{&l_1132,(void*)0,&l_863}},{{&g_90,&l_869[1][7][0],&l_869[1][7][0]},{&l_871,&l_869[1][6][0],(void*)0},{&l_871,&l_863,&l_882[2][3]},{&l_1132,&l_869[1][6][0],&g_296},{&l_893,&l_869[1][7][0],&g_951},{&l_1133,(void*)0,&l_869[1][7][0]},{&l_1132,&l_869[1][7][0],&l_863},{&l_863,&l_869[1][6][0],&l_869[1][6][0]}},{{&g_296,&l_863,(void*)0},{&l_893,&l_869[1][6][0],(void*)0},{(void*)0,&l_869[1][7][0],&l_863},{&g_500,(void*)0,&l_882[2][3]},{(void*)0,&l_869[1][7][0],&l_871},{&l_882[2][3],&l_869[1][6][0],&g_90},{&g_90,&l_863,(void*)0},{&l_1133,&l_869[1][6][0],(void*)0}},{{&l_1094,&l_869[1][7][0],&l_869[1][7][0]},{&l_1132,(void*)0,&l_863},{&g_90,&l_869[1][7][0],&l_869[1][7][0]},{&l_871,&l_869[1][6][0],(void*)0},{&l_871,&l_863,&l_882[2][3]},{&l_1132,&l_869[1][6][0],&g_296},{&l_893,&l_869[1][7][0],&g_951},{&l_1133,(void*)0,&l_869[1][7][0]}},{{&l_1132,&l_869[1][7][0],&l_863},{&l_863,&l_869[1][6][0],&l_869[1][6][0]},{&g_296,&l_863,(void*)0},{&l_893,&l_869[1][6][0],(void*)0},{(void*)0,&l_869[1][7][0],&l_863},{&g_500,(void*)0,&l_882[2][3]},{(void*)0,&l_869[1][7][0],&l_871},{&l_882[2][3],&l_869[1][6][0],&g_90}}};
                        int i, j, k;
                        (*l_1134) |= (l_1133 = ((**l_782) = ((+0x3DA39A9086107B30LL) >= (((safe_lshift_func_uint8_t_u_u((+0x1DL), p_41)) & (**l_782)) == (l_1132 && p_41)))));
                        ++l_1170;
                        (*l_782) = &l_863;
                        p_40 = l_1141;
                    }
                    else
                    { /* block id: 453 */
                        if (l_895)
                            break;
                        (*l_782) = (*l_782);
                    }
                }
                (*l_782) = &l_871;
            }
        }
    }
    (*l_782) = ((((**g_1125) | p_44) < (((safe_unary_minus_func_int16_t_s(((safe_rshift_func_int16_t_s_u((safe_add_func_uint16_t_u_u(((~((safe_mod_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((((safe_add_func_int64_t_s_s(g_114[0], ((g_1207 ^= ((l_1206 = (((safe_add_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s(g_302[1], (((safe_div_func_uint32_t_u_u((!0x1B5C7F35L), ((l_869[1][7][0] = (((l_1194 || ((p_44 != (safe_mod_func_uint8_t_u_u((((!((safe_mod_func_uint32_t_u_u(((((safe_add_func_uint8_t_u_u((p_41 >= (safe_mul_func_int8_t_s_s(p_41, 255UL))), l_1204[0][1])) & (*g_150)) && 0x56L) ^ 0xAB95EE7FL), p_43)) | p_43)) >= l_869[1][7][0]) ^ l_1194), g_171))) != 0xA450L)) , l_854) , p_44)) , 0x32ED29A9L))) | p_44) ^ p_43))), p_43)) >= 65535UL) && p_43)) || 0xDFB8A274L)) && l_1208))) >= g_1157) > (**g_568)), 0L)), p_43)), 0x85F9A571L)) >= p_43)) || g_546.f0), (*g_150))), 14)) , p_43))) , l_1209) == l_1209)) , (void*)0);
    (**g_763) = (((void*)0 != l_1210) != (((p_44 | 0x7A9F96E3L) , ((*l_1212) = (p_44 <= 0x3.6p-1))) != ((p_41 < (((void*)0 == &g_674) , (*g_764))) > p_44)));
    if ((safe_mul_func_uint16_t_u_u(((safe_add_func_uint64_t_u_u((p_43 , 1UL), (((void*)0 != &l_1082) | (**g_568)))) < p_41), (safe_rshift_func_int16_t_s_u(((((safe_mul_func_int16_t_s_s(((((safe_lshift_func_int8_t_s_u((g_1223 == ((safe_lshift_func_uint8_t_u_u((p_44 || (((p_44 & p_43) && 0x115D18E26350EEC9LL) , p_43)), p_44)) , l_1228[1])), l_1230)) | (**g_644)) >= 0xC0046425L) != 4294967288UL), p_43)) < 0x31L) ^ p_44) <= p_44), 2)))))
    { /* block id: 468 */
        int8_t l_1231[7][9][4] = {{{0x3EL,0L,0x0DL,3L},{0x0DL,3L,9L,9L},{0L,0L,0xC0L,(-1L)},{0xD9L,(-4L),0xF7L,0x90L},{0x0DL,(-1L),(-1L),0xF7L},{3L,(-1L),0x4CL,0x90L},{(-1L),(-4L),(-6L),(-1L)},{0xE8L,0L,(-4L),9L},{(-6L),0xCAL,0x90L,0xCAL}},{{9L,0x4CL,0x8CL,0xF7L},{1L,9L,1L,0x22L},{0x0DL,0xE8L,0xD9L,0L},{0x0DL,0x4BL,1L,(-1L)},{1L,0L,0x8CL,(-7L)},{9L,(-6L),0x90L,0xE8L},{(-6L),0x4BL,0x4BL,(-6L)},{0x8CL,0x0DL,0xC0L,0x22L},{(-4L),0xF7L,0x90L,9L}},{{0xCAL,0x4CL,(-1L),9L},{1L,0xF7L,(-7L),0x22L},{0xE8L,0x0DL,0xD9L,(-6L)},{(-1L),0x4BL,0x22L,0xE8L},{1L,(-6L),1L,(-7L)},{0xF7L,0L,0x90L,(-1L)},{0L,0x4BL,0x4CL,0L},{0x8CL,0xE8L,0x4CL,0x22L},{0L,9L,0x90L,0xF7L}},{{0xF7L,0x4CL,1L,0xCAL},{1L,0xCAL,0x22L,0x22L},{(-1L),(-1L),0xD9L,(-4L)},{0xE8L,0x4BL,(-7L),0x0DL},{1L,(-4L),(-1L),(-7L)},{0xCAL,(-4L),0x90L,0x0DL},{(-4L),0x4BL,0xC0L,(-4L)},{0x8CL,(-1L),0x4BL,0x22L},{(-6L),0xCAL,0x90L,0xCAL}},{{9L,0x4CL,0x8CL,0xF7L},{1L,9L,1L,0x22L},{0x0DL,0xE8L,0xD9L,0L},{0x0DL,0x4BL,1L,(-1L)},{1L,0L,0x8CL,(-7L)},{9L,(-6L),0x90L,0xE8L},{(-6L),0x4BL,0x4BL,(-6L)},{0x8CL,0x0DL,0xC0L,0x22L},{(-4L),0xF7L,0x90L,9L}},{{0xCAL,0x4CL,(-1L),9L},{1L,0xF7L,(-7L),0x22L},{0xE8L,0x0DL,0xD9L,(-6L)},{(-1L),0x4BL,0x22L,0xE8L},{1L,(-6L),1L,(-7L)},{0xF7L,0L,0x90L,(-1L)},{0L,0x4BL,0x4CL,0L},{0x8CL,0xE8L,0x4CL,0x22L},{0L,9L,0x90L,0xF7L}},{{0xF7L,0x4CL,1L,0xCAL},{1L,0xCAL,0x35L,0x35L},{(-1L),(-1L),0xE8L,0x4BL},{0x8CL,0L,(-1L),1L},{0xB4L,0x4BL,3L,(-1L)},{1L,0x4BL,0x0DL,1L},{0x4BL,0L,0xD9L,0x4BL},{0x3EL,(-1L),0L,0x35L},{0xC0L,1L,0x0DL,1L}}};
        int i, j, k;
        l_1231[3][5][1] = p_44;
        (*l_782) = (void*)0;
    }
    else
    { /* block id: 471 */
        int8_t l_1241 = 0x92L;
        int32_t l_1243[9][7] = {{(-1L),(-8L),1L,0x9260AFADL,0xA3E6C4D6L,0xA3E6C4D6L,0x9260AFADL},{0x07036F11L,0x9AD263C5L,0x07036F11L,0xA9578964L,0x7BFE0C83L,0x0F03CB31L,0xB67B3D7EL},{(-7L),(-10L),0x47E72D8DL,1L,(-2L),1L,0x47E72D8DL},{0x7BFE0C83L,0x7BFE0C83L,(-1L),0xFCD3A1CEL,(-1L),0x0F03CB31L,0x9AD263C5L},{0x890BA491L,5L,0xA3E6C4D6L,8L,8L,0xA3E6C4D6L,5L},{0xB4957C09L,0x94542C9FL,0x7BFE0C83L,0x07036F11L,(-1L),0xA9578964L,0xFCD3A1CEL},{0xA3E6C4D6L,(-7L),(-2L),5L,(-2L),(-7L),0xA3E6C4D6L},{0xFCD3A1CEL,0xA9578964L,(-1L),0x07036F11L,0x7BFE0C83L,0x94542C9FL,0xB4957C09L},{5L,0xA3E6C4D6L,8L,8L,0xA3E6C4D6L,5L,0x890BA491L}};
        int16_t l_1244 = (-7L);
        int32_t l_1246 = 0x61BCFF3BL;
        int i, j;
        l_1246 = (l_1245[1][4][0] |= (+(((*l_1229) ^= (*g_1224)) == (((**g_568) && (p_43 , (**g_568))) | (safe_sub_func_int8_t_s_s((l_1243[6][5] = (((0x26B3L <= (safe_mod_func_uint16_t_u_u(0UL, p_44))) & (g_1242[1][2][1] = (safe_mod_func_uint32_t_u_u((safe_div_func_uint64_t_u_u((0x3F48L ^ l_1241), p_43)), p_43)))) && (**g_288))), l_1244))))));
        return p_44;
    }
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_412 g_296 g_500 g_644 g_645 g_35
 * writes: g_296 g_500
 */
static uint8_t  func_56(uint16_t * p_57, int32_t  p_58, int16_t  p_59, int32_t ** p_60)
{ /* block id: 276 */
    int32_t *l_783 = &g_500;
    int32_t *l_784 = (void*)0;
    int32_t *l_785[6];
    uint8_t l_786 = 0x64L;
    int i;
    for (i = 0; i < 6; i++)
        l_785[i] = &g_90;
    (*l_783) &= ((**p_60) = ((**p_60) , 1L));
    --l_786;
    return (**g_644);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint16_t * func_61(uint8_t  p_62, uint32_t  p_63, int32_t * p_64, int32_t * p_65)
{ /* block id: 274 */
    uint16_t *l_781 = &g_46;
    return l_781;
}


/* ------------------------------------------ */
/* 
 * reads : g_298 g_636 g_644 g_90 g_412 g_670 g_674 g_693 g_296 g_323 g_324 g_35 g_302 g_150 g_645 g_481 g_482 g_171 g_760 g_761 g_764 g_765 g_166 g_762 g_763 g_658.f0
 * writes: g_298 g_88 g_235 g_546.f2 g_46 g_569 g_644 g_90 g_296 g_500 g_674 g_302 g_114 g_759 g_768 g_765
 */
static int8_t  func_70(uint16_t  p_71, const int64_t  p_72, float  p_73, int32_t  p_74)
{ /* block id: 203 */
    uint64_t l_641 = 0UL;
    uint8_t *l_652[5] = {&g_35,&g_35,&g_35,&g_35,&g_35};
    int32_t l_659 = 0x411352F8L;
    int32_t l_662 = 0L;
    int32_t l_663[7][10][3] = {{{1L,0xCE2F1851L,0xA1BF1D84L},{1L,0x985F8C34L,0x985F8C34L},{1L,0x4B16A419L,1L},{0xE92D491AL,3L,1L},{(-7L),1L,1L},{0x4F990BBFL,1L,0xCFCF10C0L},{1L,1L,0xDFBC1BC4L},{0xE13DC8BEL,3L,0L},{0xCE2F1851L,0x4B16A419L,0x1C4240A8L},{1L,0x985F8C34L,3L}},{{0x1C4240A8L,0xCE2F1851L,0x1C4240A8L},{0xB7D61847L,0xC2B4C562L,0L},{0x3C457F16L,0x519DCFF1L,0xDFBC1BC4L},{0x985F8C34L,0xE13DC8BEL,0xCFCF10C0L},{(-1L),1L,1L},{0x985F8C34L,(-2L),1L},{0x3C457F16L,0x3268661AL,1L},{0xB7D61847L,1L,0x985F8C34L},{0x1C4240A8L,0xDFBC1BC4L,0xA1BF1D84L},{1L,1L,1L}},{{0xCE2F1851L,0x3268661AL,1L},{0xE13DC8BEL,(-2L),0x4F990BBFL},{1L,1L,0x3268661AL},{0x4F990BBFL,0xE13DC8BEL,0x4F990BBFL},{(-7L),0x519DCFF1L,1L},{0xE92D491AL,0xC2B4C562L,1L},{1L,0xCE2F1851L,0xA1BF1D84L},{1L,0x985F8C34L,0x985F8C34L},{1L,0x4B16A419L,1L},{0xE92D491AL,3L,1L}},{{(-7L),1L,1L},{3L,0xE13DC8BEL,0xE92D491AL},{0xDFBC1BC4L,0xDFBC1BC4L,0xCE2F1851L},{0xB7D61847L,0x985F8C34L,0xCFCF10C0L},{(-7L),1L,0x3268661AL},{1L,1L,0x985F8C34L},{0x3268661AL,(-7L),0x3268661AL},{0xC2B4C562L,(-2L),0xCFCF10C0L},{0x1C4240A8L,0x4B16A419L,0xCE2F1851L},{1L,0xB7D61847L,0xE92D491AL}},{{1L,(-1L),(-1L)},{1L,0L,1L},{0x1C4240A8L,1L,0xDFBC1BC4L},{0xC2B4C562L,1L,1L},{0x3268661AL,0xCE2F1851L,0x3C457F16L},{1L,1L,0xE13DC8BEL},{(-7L),1L,0xA1BF1D84L},{0xB7D61847L,0L,3L},{0xDFBC1BC4L,(-1L),1L},{3L,0xB7D61847L,3L}},{{0x519DCFF1L,0x4B16A419L,0xA1BF1D84L},{0x4F990BBFL,(-2L),0xE13DC8BEL},{(-1L),(-7L),0x3C457F16L},{1L,1L,1L},{(-1L),1L,0xDFBC1BC4L},{0x4F990BBFL,0x985F8C34L,1L},{0x519DCFF1L,0xDFBC1BC4L,(-1L)},{3L,0xE13DC8BEL,0xE92D491AL},{0xDFBC1BC4L,0xDFBC1BC4L,0xCE2F1851L},{0xB7D61847L,0x985F8C34L,0xCFCF10C0L}},{{(-7L),1L,0x3268661AL},{1L,1L,0x985F8C34L},{0x3268661AL,(-7L),0x3268661AL},{0xC2B4C562L,(-2L),0xCFCF10C0L},{0x1C4240A8L,0x4B16A419L,0xCE2F1851L},{1L,0xB7D61847L,0xE92D491AL},{1L,(-1L),(-1L)},{1L,0L,1L},{0x1C4240A8L,1L,0xDFBC1BC4L},{0xC2B4C562L,1L,1L}}};
    uint64_t l_666[7] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL};
    int32_t *l_703 = (void*)0;
    const uint16_t **l_780 = (void*)0;
    int i, j, k;
    for (g_298 = 0; (g_298 <= 2); g_298 += 1)
    { /* block id: 206 */
        int32_t l_632 = 0x4D312CD5L;
        int32_t *l_633 = &g_90;
        int32_t l_640[9] = {0x877B59B9L,0x801AC14DL,0x801AC14DL,0x877B59B9L,0x801AC14DL,0x801AC14DL,0x877B59B9L,0x801AC14DL,0x801AC14DL};
        uint8_t *l_651 = (void*)0;
        int32_t l_664 = 0x7726A56AL;
        int32_t l_665 = 0x79085C69L;
        float l_698 = 0x8.4C5B58p+37;
        uint16_t ***l_699 = &g_288;
        uint64_t *l_766 = &l_641;
        int i;
        if (l_632)
            break;
        (*g_636) = l_633;
        for (g_235 = 0; (g_235 <= 1); g_235 += 1)
        { /* block id: 211 */
            int32_t *l_637 = (void*)0;
            int32_t *l_638 = (void*)0;
            int32_t *l_639[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            uint8_t * volatile * volatile *l_646[2];
            int i, j;
            for (i = 0; i < 2; i++)
                l_646[i] = &g_644;
            for (p_71 = 0; p_71 < 3; p_71 += 1)
            {
                for (g_546.f2 = 0; g_546.f2 < 2; g_546.f2 += 1)
                {
                    for (g_46 = 0; g_46 < 9; g_46 += 1)
                    {
                        g_569[p_71][g_546.f2][g_46] = &g_255[0][1];
                    }
                }
            }
            --l_641;
            g_644 = g_644;
            for (g_46 = 0; (g_46 <= 2); g_46 += 1)
            { /* block id: 217 */
                float *l_653[4][8][3] = {{{&g_482,&g_482,&g_482},{&g_482,&g_482,(void*)0},{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{&g_482,&g_482,(void*)0},{&g_482,&g_482,&g_482},{&g_482,&g_482,(void*)0}},{{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{(void*)0,&g_482,&g_482},{&g_482,&g_482,(void*)0},{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{&g_482,&g_482,(void*)0},{&g_482,&g_482,(void*)0}},{{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{(void*)0,&g_482,&g_482},{&g_482,&g_482,(void*)0},{&g_482,&g_482,&g_482},{&g_482,&g_482,(void*)0},{(void*)0,&g_482,&g_482},{&g_482,&g_482,(void*)0}},{{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{&g_482,&g_482,&g_482},{&g_482,(void*)0,(void*)0},{&g_482,&g_482,&g_482},{&g_482,&g_482,(void*)0}}};
                int32_t l_660 = 1L;
                int32_t l_661[1][4][3] = {{{(-5L),0L,(-5L)},{0xBFE2C26DL,(-5L),0xBFE2C26DL},{(-5L),0L,(-5L)},{0xBFE2C26DL,(-5L),0xBFE2C26DL}}};
                int32_t **l_669[1];
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_669[i] = &l_637;
            }
        }
        for (g_90 = 0; (g_90 >= 0); g_90 -= 1)
        { /* block id: 231 */
            int32_t l_671 = 0xDB71687CL;
            uint32_t *l_697 = &g_166[0][6][0];
            uint16_t * const *l_701 = &g_150;
            uint16_t * const ** const l_700 = &l_701;
            int32_t l_709 = (-7L);
            int32_t l_712 = 1L;
            int32_t l_713 = (-8L);
            uint8_t * const l_744 = (void*)0;
            float ** const l_758 = (void*)0;
            float ** const *l_757[10][4];
            float ** const **l_756 = &l_757[8][1];
            int i, j;
            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 4; j++)
                    l_757[i][j] = &l_758;
            }
            (*g_670) = ((*g_412) = (*l_633));
            if (l_671)
                continue;
            for (l_641 = 0; (l_641 <= 0); l_641 += 1)
            { /* block id: 237 */
                int32_t *l_672 = &l_640[1];
                int32_t *l_673 = (void*)0;
                uint8_t **l_690 = &l_651;
                uint8_t *l_692 = &g_173;
                uint8_t **l_691 = &l_692;
                uint64_t *l_702[5][2] = {{&g_302[1],&g_302[0]},{&g_302[1],&g_302[0]},{&g_302[1],&g_302[0]},{&g_302[1],&g_302[0]},{&g_302[1],&g_302[0]}};
                int32_t l_705[4][10][6] = {{{0x29DA0EC0L,(-10L),0xF90D12D6L,(-1L),(-7L),0x0927354FL},{(-1L),0x0927354FL,(-3L),0x5E8683DCL,(-10L),3L},{(-3L),0xFDC3291BL,0L,0xF3E7BDBCL,0xC87D943BL,1L},{(-1L),1L,0xA073E681L,8L,0L,4L},{(-10L),0xA073E681L,0x94F91FAEL,(-1L),0x379E42ECL,0x74C288BAL},{0xF3E7BDBCL,0xCE9A9D53L,1L,0x763132EEL,0x763132EEL,1L},{0x924DA988L,0x924DA988L,(-10L),(-1L),(-1L),6L},{0L,(-1L),0L,(-1L),(-1L),(-10L)},{1L,0L,0L,0x20186EAEL,0x924DA988L,6L},{0xC2BA5B24L,0x20186EAEL,(-10L),0xE40D956AL,0x94F91FAEL,1L}},{{0xE40D956AL,0x94F91FAEL,1L,(-9L),0xF72CE0ACL,0x74C288BAL},{0x4E84A773L,(-3L),0x94F91FAEL,0x79D3A82AL,0x1E4D7A47L,4L},{0x1E4D7A47L,6L,0xA073E681L,0xC2BA5B24L,(-1L),1L},{1L,(-7L),0L,(-3L),0x34B5ADBFL,3L},{(-1L),(-10L),(-3L),0L,0xE40D956AL,0x0927354FL},{(-7L),3L,0xF90D12D6L,0L,6L,0x79D3A82AL},{0x0D92996BL,0x0D49EAA7L,0L,0x4E84A773L,(-3L),0x4E84A773L},{0xA547860CL,8L,0xA547860CL,0x94F91FAEL,0x4E84A773L,(-7L)},{0xA073E681L,1L,(-10L),(-1L),8L,0xF90D12D6L},{(-1L),0x1E4D7A47L,0x34B5ADBFL,(-1L),(-4L),0x94F91FAEL}},{{0xA073E681L,(-4L),0L,0x94F91FAEL,(-1L),(-4L)},{1L,1L,(-1L),(-10L),0L,1L},{1L,3L,0xCE9A9D53L,8L,(-1L),0x20186EAEL},{0xF08F66C5L,0xA547860CL,0x79D3A82AL,1L,0xA14113F2L,(-1L)},{0xD58F25C7L,(-1L),(-1L),0L,0x0927354FL,1L},{(-9L),0x79D3A82AL,0x34B5ADBFL,0x94F91FAEL,0x379E42ECL,4L},{0x0D92996BL,1L,(-1L),0L,0xE40D956AL,1L},{(-10L),1L,0L,(-3L),1L,1L},{(-7L),0x29DA0EC0L,0x29DA0EC0L,(-7L),0xA547860CL,0x1E4D7A47L},{0x94F91FAEL,(-3L),0xA073E681L,0xC87D943BL,0L,(-1L)}},{{0xE40D956AL,0x379E42ECL,0x95483129L,(-7L),0L,0L},{0xF90D12D6L,(-3L),(-10L),8L,0xA547860CL,(-10L)},{0xF3E7BDBCL,0x29DA0EC0L,1L,4L,1L,0x0927354FL},{0x379E42ECL,1L,(-1L),0L,0xE40D956AL,(-7L)},{6L,1L,8L,0x0D49EAA7L,0x379E42ECL,0L},{(-1L),0x79D3A82AL,0L,0x379E42ECL,0x0927354FL,0x0D49EAA7L},{0L,(-1L),(-7L),(-1L),0xA14113F2L,0x79D3A82AL},{(-7L),0xA547860CL,0L,(-1L),(-1L),0x763132EEL},{0L,3L,(-1L),(-9L),0L,0xC2BA5B24L},{(-1L),1L,0xF3E7BDBCL,1L,(-1L),0x5E8683DCL}}};
                int i, j, k;
                --g_674;
                if (((safe_div_func_uint64_t_u_u(((+(g_302[1] = (safe_mul_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((l_671 ^ 0UL), 8)), (((((safe_lshift_func_int8_t_s_s(((safe_lshift_func_uint8_t_u_s(((safe_sub_func_int32_t_s_s(((l_652[3] != ((*l_691) = ((*l_690) = l_651))) | g_693), g_296)) == (safe_sub_func_uint8_t_u_u((*l_633), ((~(*g_323)) < (((((*l_672) = 1L) != l_641) , (void*)0) != l_697))))), g_35)) | g_302[0]), 4)) > p_74) , l_699) != l_700) | p_72))))) & 0x159930A0CF2B2644LL), (*l_633))) && 7UL))
                { /* block id: 243 */
                    int16_t l_706 = 0x7D18L;
                    int32_t l_707 = 0xA9F9FA8DL;
                    int32_t l_710 = 0xCF69BC34L;
                    int32_t l_711 = 0x9509DB63L;
                    int32_t l_714 = 1L;
                    uint64_t l_715 = 8UL;
                    int32_t l_769[5] = {0x58D70C03L,0x58D70C03L,0x58D70C03L,0x58D70C03L,0x58D70C03L};
                    int i;
                    for (l_632 = 2; (l_632 >= 0); l_632 -= 1)
                    { /* block id: 246 */
                        int32_t *l_704[3];
                        float l_708 = 0x0.9p-1;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_704[i] = &l_640[1];
                        l_703 = (void*)0;
                        --l_715;
                    }
                    if (((safe_mul_func_uint16_t_u_u(((*g_150) = 0xEB0CL), ((safe_rshift_func_uint8_t_u_u((*g_645), 3)) <= (((*g_412) = ((safe_mod_func_int8_t_s_s((l_662 >= ((safe_div_func_uint64_t_u_u((p_74 & 255UL), (g_302[1] ^= (p_71 || p_71)))) || (0xC2L < p_74))), 7L)) < l_715)) >= p_72)))) & l_711))
                    { /* block id: 253 */
                        return p_71;
                    }
                    else
                    { /* block id: 255 */
                        int8_t **l_740 = (void*)0;
                        int8_t *l_742 = &g_114[0];
                        int8_t **l_741 = &l_742;
                        int8_t *l_743 = &g_114[3];
                        int32_t l_745 = 0xCA04565AL;
                        uint64_t **l_767 = &l_702[4][0];
                        int i, j, k;
                        p_73 = (((safe_mul_func_float_f_f(((*g_481) <= ((safe_mul_func_float_f_f((p_71 >= 0x9.B68E0Bp+85), (safe_mul_func_float_f_f(((*l_672) = l_712), (((safe_mul_func_uint8_t_u_u((((safe_sub_func_uint16_t_u_u((safe_div_func_int16_t_s_s(((p_72 | (0x8A3F0EA46510C7B8LL | (l_707 | ((((*l_743) = (&g_114[3] == ((*l_741) = (void*)0))) > 0x1CL) != 3L)))) | 0L), (-1L))), l_671)) , (void*)0) != l_744), g_171)) & g_35) , p_74))))) < l_713)), 0x2.2p+1)) > p_74) <= l_745);
                        (*g_412) = ((safe_sub_func_uint16_t_u_u(((((safe_mod_func_uint32_t_u_u(((safe_sub_func_uint16_t_u_u(((0xC789B3353162CEC1LL <= ((((safe_add_func_int32_t_s_s((((*l_633) , (safe_sub_func_uint64_t_u_u(((g_759 = l_756) != (g_760 , g_761)), (((*l_767) = l_766) == (g_768[2][5][0] = &g_302[0]))))) && 0x7E5BC0E4L), l_745)) < p_74) & p_72) , l_769[0])) < 0xDAL), l_671)) || 0xF022L), l_710)) , p_71) ^ 0x45C7C2B5L) && p_72), p_74)) , (-7L));
                    }
                }
                else
                { /* block id: 265 */
                    int32_t **l_770 = &l_703;
                    (*l_770) = &l_659;
                    if (p_72)
                        break;
                    (****g_761) = (((0x1.3p-1 < (*g_764)) != (g_302[1] , (safe_add_func_float_f_f(((safe_sub_func_float_f_f((safe_add_func_float_f_f((p_71 <= (-0x4.7p+1)), p_72)), (safe_add_func_float_f_f((-((&g_150 != (g_166[0][5][0] , l_780)) != l_709)), 0x1.1F5285p-78)))) > l_712), (*l_633))))) >= 0x7.9p+1);
                }
            }
        }
    }
    return g_658.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_412
 * writes: g_296
 */
static uint16_t  func_75(uint32_t  p_76, int64_t  p_77, int8_t  p_78, const int16_t  p_79, int8_t  p_80)
{ /* block id: 200 */
    uint64_t l_631 = 18446744073709551609UL;
    (*g_412) = (safe_mul_func_uint16_t_u_u(p_77, 1L));
    return l_631;
}


/* ------------------------------------------ */
/* 
 * reads : g_35 g_88 g_90 g_10 g_46 g_114 g_166 g_171 g_183 g_173 g_150 g_235 g_255 g_302 g_299 g_288 g_322 g_412 g_253 g_296 g_298 g_480 g_568 g_569
 * writes: g_90 g_88 g_114 g_149 g_35 g_173 g_183 g_235 g_288 g_302 g_322 g_171 g_296 g_46
 */
static uint16_t  func_83(const int64_t  p_84, int32_t ** p_85, int8_t  p_86)
{ /* block id: 18 */
    int32_t *l_89 = &g_90;
    int32_t **l_91 = &g_88;
    const int8_t l_106[8] = {0x2BL,0x2BL,0x2BL,0x2BL,0x2BL,0x2BL,0x2BL,0x2BL};
    int8_t *l_113 = &g_114[0];
    float l_142 = 0x0.Bp+1;
    uint64_t *l_239 = &g_183;
    float *l_263 = (void*)0;
    float **l_262 = &l_263;
    float ***l_319 = &l_262;
    int32_t l_341 = 0x4D433172L;
    int32_t l_342 = 0x49318141L;
    int32_t l_351 = 0x4AD2A175L;
    int32_t l_353[7];
    uint32_t l_401 = 1UL;
    uint8_t l_404 = 0xA4L;
    int64_t l_440 = 0x8ECECE4411029F25LL;
    int16_t *l_450 = &g_255[0][1];
    int16_t **l_449 = &l_450;
    int16_t ***l_448 = &l_449;
    int8_t l_473 = (-10L);
    int32_t l_499 = 0xB066E8FDL;
    int8_t l_596 = 0x56L;
    uint64_t l_609 = 1UL;
    uint16_t ***l_622 = &g_149;
    uint16_t l_627[5] = {0x692CL,0x692CL,0x692CL,0x692CL,0x692CL};
    int i;
    for (i = 0; i < 7; i++)
        l_353[i] = 1L;
lbl_626:
    (*l_89) = 9L;
lbl_363:
    (*l_91) = l_89;
    if (((safe_lshift_func_int8_t_s_s(((safe_sub_func_int32_t_s_s((safe_sub_func_int64_t_s_s(((g_35 & (1UL || (safe_mul_func_uint8_t_u_u((((((**l_91) <= ((safe_mul_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s(l_106[0], ((!(safe_unary_minus_func_int16_t_s(((safe_lshift_func_uint8_t_u_s((*l_89), 0)) <= ((void*)0 == (*l_91)))))) , (safe_lshift_func_int8_t_s_u((((*l_113) = g_35) != (((safe_div_func_int64_t_s_s(((0x52D1L | p_86) == 4294967291UL), 0xC98CB2CAB94E7F8FLL)) & 0x1824DA47L) , g_10)), 3))))), 1UL)) != g_46)) ^ 3L) || g_46) ^ 255UL), g_10)))) | g_35), (*l_89))), (*g_88))) , g_114[1]), 7)) && 0x513FL))
    { /* block id: 22 */
        uint16_t l_120 = 1UL;
        const uint64_t l_136 = 18446744073709551609UL;
        if (((p_84 == (*l_89)) >= ((p_85 != p_85) <= 0x2EL)))
        { /* block id: 23 */
            for (p_86 = 0; (p_86 >= 29); p_86++)
            { /* block id: 26 */
                return p_86;
            }
        }
        else
        { /* block id: 29 */
            int16_t l_119 = 0x876FL;
            int32_t l_133 = 1L;
            uint32_t l_139 = 18446744073709551612UL;
            float *l_145 = &l_142;
            uint16_t *l_147 = &g_46;
            uint16_t **l_146[9] = {&l_147,&l_147,&l_147,&l_147,&l_147,&l_147,&l_147,&l_147,&l_147};
            uint16_t ***l_148[6] = {&l_146[0],&l_146[0],(void*)0,&l_146[0],&l_146[0],(void*)0};
            int i;
            (*l_89) = (l_120 = l_119);
            (*g_88) = ((((((safe_mul_func_uint16_t_u_u(((safe_div_func_int16_t_s_s((((((safe_add_func_float_f_f((safe_mul_func_float_f_f((safe_div_func_float_f_f(p_86, (g_90 , ((safe_div_func_float_f_f(l_133, ((((*l_89) < (((void*)0 == &g_88) > ((*l_113) = ((safe_mul_func_uint8_t_u_u(p_86, (*l_89))) >= (l_133 || 3L))))) | 0x6B221C00E8DF19FBLL) , g_46))) != 0x6.6p+1)))), l_119)), p_84)) , 0xCE90L) , g_114[0]) != (*g_88)) <= 0UL), g_10)) <= p_84), g_35)) || l_136) && (-4L)) || 0UL) != (*l_89)) || g_114[0]);
            g_149 = (((safe_div_func_float_f_f((l_139 = p_84), ((&p_86 != &p_86) <= (((safe_sub_func_int64_t_s_s(l_119, ((*l_91) == (*l_91)))) , p_84) > 0x9.35BBF9p+22)))) == ((safe_sub_func_float_f_f(((((*l_145) = g_46) > p_84) >= g_114[1]), g_35)) < g_46)) , l_146[7]);
        }
    }
    else
    { /* block id: 38 */
        int8_t l_157 = 9L;
        uint16_t ***l_164 = (void*)0;
        uint32_t *l_165[6] = {&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0],&g_166[0][6][0]};
        int32_t l_167 = 5L;
        int32_t l_168[5] = {0xA27A7C49L,0xA27A7C49L,0xA27A7C49L,0xA27A7C49L,0xA27A7C49L};
        uint8_t *l_169 = (void*)0;
        uint8_t *l_170 = &g_35;
        uint8_t *l_172 = &g_173;
        int32_t l_195 = 0xC0E31BEAL;
        uint8_t *l_208 = &g_173;
        uint32_t l_257 = 18446744073709551615UL;
        float *l_261 = &l_142;
        float **l_260 = &l_261;
        uint16_t l_501[2];
        int8_t *l_616 = &l_596;
        int16_t *l_621 = (void*)0;
        uint64_t l_623[3][10] = {{4UL,4UL,0UL,0x042BA897BDA923E1LL,0x54D72DA54A1A2587LL,18446744073709551608UL,0x7EF3AA63CAE46EC5LL,18446744073709551608UL,0x54D72DA54A1A2587LL,0x042BA897BDA923E1LL},{0x042BA897BDA923E1LL,0x75715A9AE22C86A2LL,0x042BA897BDA923E1LL,18446744073709551608UL,0x3AB8C11DC6F7A2F7LL,1UL,0x7EF3AA63CAE46EC5LL,0x7EF3AA63CAE46EC5LL,1UL,0x3AB8C11DC6F7A2F7LL},{0UL,4UL,4UL,0UL,0x042BA897BDA923E1LL,0x54D72DA54A1A2587LL,18446744073709551608UL,0x7EF3AA63CAE46EC5LL,18446744073709551608UL,0x54D72DA54A1A2587LL}};
        uint16_t ****l_624 = &l_164;
        int8_t l_625[7][2][8] = {{{0x72L,0x93L,4L,0L,0x96L,(-10L),0xA7L,0x41L},{0x04L,(-1L),3L,(-1L),(-9L),1L,0xA7L,0x96L}},{{0L,(-1L),4L,(-10L),0L,0x46L,9L,0xE2L},{0L,0x46L,9L,0xE2L,0L,0x30L,0x03L,0x94L}},{{(-1L),0x12L,1L,(-10L),0x41L,0x41L,(-10L),1L},{(-1L),(-1L),0xC2L,0L,(-1L),(-10L),0x12L,0x46L}},{{0x96L,0x9FL,0x04L,0xFFL,0x94L,0xA9L,1L,0x46L},{0x9FL,0x93L,1L,0L,9L,4L,0x3FL,1L}},{{1L,0x4FL,0x03L,(-10L),0x93L,(-1L),0x9FL,0x94L},{0x3FL,9L,0x0DL,0xE2L,(-1L),(-1L),(-1L),0xE2L}},{{0L,0L,0L,(-10L),6L,0L,0xFFL,0x96L},{0x93L,0xE2L,(-1L),(-1L),0x72L,3L,6L,0x41L}},{{0x93L,1L,(-1L),0L,6L,1L,0L,0x03L},{0L,4L,0x30L,(-1L),(-1L),0x04L,(-1L),0x12L}}};
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_501[i] = 65535UL;
        if ((((*l_172) = ((((((safe_lshift_func_int8_t_s_s((safe_div_func_uint64_t_u_u((safe_add_func_int16_t_s_s(((l_157 | ((*l_170) = ((0x23L == (g_35 , (g_114[0] | 8L))) != ((l_168[1] ^= (l_167 = (safe_div_func_uint64_t_u_u(((*g_88) || ((**l_91) = (safe_rshift_func_uint16_t_u_s((p_86 | ((safe_mul_func_int8_t_s_s((**l_91), ((l_164 = l_164) != &g_149))) , l_157)), 1)))), p_86)))) == 4294967291UL)))) > p_84), p_86)), g_166[0][6][0])), 7)) && (**l_91)) , g_171) >= g_166[0][6][0]) && 0UL) == p_86)) || (*l_89)))
        { /* block id: 45 */
            uint8_t *l_174 = &g_35;
            uint64_t *l_182[6][3] = {{&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183},{&g_183,&g_183,&g_183}};
            int32_t l_206 = 0x8A53124DL;
            int32_t l_207 = 0L;
            int16_t l_234 = 0x806FL;
            int32_t l_248 = (-9L);
            int32_t l_300 = 0x86165CE2L;
            float ***l_317 = (void*)0;
            int32_t l_336 = 3L;
            int32_t l_338 = 0L;
            int32_t l_339 = (-1L);
            int32_t l_340 = 0xEA3D4966L;
            int32_t l_343 = 0x60EF523AL;
            int32_t l_346 = 0L;
            int32_t l_348[2][7] = {{(-9L),(-9L),(-3L),(-9L),(-9L),(-3L),(-9L)},{(-9L),(-1L),(-1L),(-9L),(-1L),(-1L),(-9L)}};
            uint32_t l_354 = 0UL;
            int16_t l_398 = 0x56BAL;
            int16_t l_399[5][4][9] = {{{(-10L),3L,(-10L),3L,(-10L),3L,(-10L),3L,(-10L)},{0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L},{0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L},{0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L}},{{(-10L),3L,(-10L),3L,(-10L),3L,(-10L),3L,(-10L)},{0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L},{0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L},{0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L}},{{(-10L),3L,(-10L),3L,(-10L),3L,(-10L),3L,(-10L)},{0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L},{0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L},{0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L}},{{(-10L),3L,(-10L),3L,(-10L),3L,(-10L),3L,(-10L)},{0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L},{0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L},{0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L}},{{(-10L),3L,(-10L),3L,(-10L),3L,(-10L),3L,(-10L)},{0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L},{0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L,3L,0x6E33L},{0x1BC5L,0L,0L,0x1BC5L,0x1BC5L,0L,0L,0x1BC5L,0x1BC5L}}};
            int32_t *l_413 = &l_342;
            int16_t ***l_447 = (void*)0;
            int16_t l_469 = 0xCD6DL;
            int16_t l_472 = 0x3F6FL;
            int32_t *l_492 = &l_300;
            int32_t *l_493 = &l_351;
            int32_t *l_494 = &l_207;
            int32_t *l_495 = &l_342;
            int32_t *l_496 = &l_348[0][6];
            int32_t *l_497 = &l_353[1];
            int32_t *l_498[4][5] = {{(void*)0,&l_168[4],&l_348[1][2],&l_348[1][2],&l_168[4]},{(void*)0,&l_300,&l_348[1][2],&l_339,&l_339},{&l_300,(void*)0,&l_300,&l_348[1][2],&l_339},{&l_168[4],(void*)0,&l_339,(void*)0,&l_168[4]}};
            int16_t ** const *l_571 = (void*)0;
            int32_t l_597 = 0x7D5399C4L;
            uint16_t **l_606 = &g_150;
            int i, j, k;
            if ((l_174 != ((((l_207 |= ((*l_174) = (safe_mod_func_uint32_t_u_u((+((((safe_add_func_uint16_t_u_u(((safe_mul_func_int8_t_s_s(g_171, 0L)) && (g_183--)), (((((void*)0 != &g_149) && (safe_mul_func_int8_t_s_s((!(safe_mul_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(1UL, (l_195 >= (safe_mul_func_int16_t_s_s((safe_add_func_int8_t_s_s((safe_mod_func_int64_t_s_s((((((safe_lshift_func_int8_t_s_s(l_157, 6)) || ((safe_rshift_func_uint8_t_u_u(0x6AL, 3)) | 0L)) | g_35) >= g_173) != p_86), p_84)), g_166[0][6][0])), l_206))))), (**l_91))) > l_206), g_35))), 7L))) > p_86) == (*g_150)))) | g_35) , p_84) || (*g_88))), (-6L))))) && 247UL) < l_168[1]) , l_208)))
            { /* block id: 49 */
                int8_t l_233 = 0xA0L;
                uint16_t *l_236 = &g_46;
                uint64_t *l_249[6];
                uint32_t l_285 = 0UL;
                int32_t l_301 = 0x12AE4F81L;
                int32_t l_344 = 0xAFACCB90L;
                int32_t l_347 = 0x9DC0D136L;
                int32_t l_349 = 0x1B0A819CL;
                int32_t l_350 = 1L;
                int32_t l_352[6];
                float l_397 = (-0x5.3p+1);
                int i;
                for (i = 0; i < 6; i++)
                    l_249[i] = &g_183;
                for (i = 0; i < 6; i++)
                    l_352[i] = 8L;
                if ((((l_206 = (safe_lshift_func_int8_t_s_s(((g_235 = (safe_div_func_int16_t_s_s(0x0907L, (safe_mod_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u(((l_195 || ((0x2F4E6B69ECCD4FD1LL <= ((**l_91) < ((safe_add_func_uint32_t_u_u(4294967291UL, (safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((safe_rshift_func_int16_t_s_s(0x7251L, g_35)), (safe_add_func_uint64_t_u_u(l_233, p_84)))), (*g_150))), l_233)), 4)), l_233)))) != l_167))) & l_234)) && l_233), p_84)), g_166[1][2][0]))))) ^ l_167), g_90))) >= p_86) || l_206))
                { /* block id: 52 */
                    uint64_t **l_240 = &l_239;
                    int32_t l_245 = 1L;
                    int16_t *l_250 = &l_234;
                    const uint16_t *l_252[8][10][3] = {{{&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,(void*)0},{(void*)0,&g_253,&g_253},{&g_253,&g_253,(void*)0},{(void*)0,(void*)0,&g_253}},{{&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0},{&g_253,&g_253,(void*)0},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0},{&g_253,&g_253,(void*)0},{&g_253,(void*)0,&g_253},{(void*)0,&g_253,&g_253}},{{(void*)0,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,(void*)0},{&g_253,&g_253,(void*)0},{(void*)0,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{(void*)0,&g_253,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253}},{{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,&g_253},{(void*)0,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253},{(void*)0,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253}},{{&g_253,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{(void*)0,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,&g_253,(void*)0},{&g_253,&g_253,(void*)0},{&g_253,(void*)0,&g_253},{(void*)0,&g_253,&g_253}},{{(void*)0,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,(void*)0},{&g_253,&g_253,(void*)0},{(void*)0,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{(void*)0,&g_253,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253}},{{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,&g_253},{(void*)0,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{&g_253,&g_253,&g_253},{(void*)0,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253}},{{&g_253,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{(void*)0,(void*)0,&g_253},{&g_253,&g_253,(void*)0},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253},{&g_253,&g_253,&g_253},{&g_253,(void*)0,&g_253},{&g_253,&g_253,&g_253},{(void*)0,&g_253,&g_253}}};
                    const uint16_t **l_251 = &l_252[2][7][1];
                    int16_t *l_254[10][8] = {{(void*)0,&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[2][2],&g_255[0][1]},{&g_255[0][1],&g_255[2][1],(void*)0,&g_255[2][2],&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[0][1]},{(void*)0,&g_255[0][1],(void*)0,(void*)0,&g_255[0][1],(void*)0,&g_255[0][1],&g_255[0][1]},{&g_255[0][1],(void*)0,(void*)0,&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[2][2],&g_255[1][1]},{&g_255[2][2],(void*)0,&g_255[1][0],&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[0][1]},{&g_255[0][1],&g_255[0][1],(void*)0,(void*)0,&g_255[0][1],(void*)0,(void*)0,&g_255[0][1]},{&g_255[0][1],&g_255[1][1],(void*)0,&g_255[2][2],(void*)0,&g_255[0][1],&g_255[0][1],&g_255[0][1]},{&g_255[2][2],&g_255[0][1],&g_255[0][2],&g_255[0][1],&g_255[2][2],&g_255[0][1],(void*)0,&g_255[0][1]},{&g_255[0][1],&g_255[0][1],&g_255[0][1],&g_255[1][2],&g_255[0][1],(void*)0,&g_255[1][2],&g_255[0][1]},{&g_255[0][1],(void*)0,&g_255[0][1],&g_255[0][1],(void*)0,(void*)0,(void*)0,&g_255[0][1]}};
                    uint16_t ***l_264 = &g_149;
                    const int32_t *l_284 = &l_206;
                    const int32_t **l_283 = &l_284;
                    uint16_t **l_287[9] = {(void*)0,&l_236,(void*)0,&l_236,(void*)0,&l_236,(void*)0,&l_236,(void*)0};
                    uint16_t ***l_286[7][6][6] = {{{&l_287[0],&l_287[0],&l_287[0],&l_287[0],&l_287[0],&l_287[0]},{&l_287[7],&l_287[5],&l_287[4],&l_287[4],&l_287[0],&l_287[6]},{(void*)0,&l_287[1],&l_287[0],&l_287[4],&l_287[7],&l_287[0]},{&l_287[4],&l_287[1],&l_287[0],&l_287[0],&l_287[0],&l_287[0]},{&l_287[5],&l_287[5],&l_287[1],(void*)0,&l_287[0],&l_287[0]},{&l_287[0],&l_287[0],&l_287[7],(void*)0,&l_287[0],&l_287[1]}},{{&l_287[7],&l_287[0],&l_287[7],&l_287[0],&l_287[5],&l_287[0]},{&l_287[1],&l_287[0],&l_287[1],&l_287[6],&l_287[8],&l_287[0]},{&l_287[6],&l_287[8],&l_287[0],&l_287[0],&l_287[7],&l_287[0]},{(void*)0,&l_287[0],&l_287[0],&l_287[0],&l_287[6],&l_287[6]},{&l_287[6],&l_287[4],&l_287[4],&l_287[6],&l_287[0],&l_287[0]},{&l_287[1],(void*)0,&l_287[0],&l_287[0],&l_287[0],&l_287[7]}},{{&l_287[7],&l_287[0],&l_287[5],(void*)0,&l_287[0],&l_287[8]},{&l_287[0],(void*)0,&l_287[6],(void*)0,&l_287[0],(void*)0},{&l_287[5],&l_287[4],&l_287[4],&l_287[0],&l_287[6],&l_287[7]},{&l_287[4],&l_287[0],(void*)0,&l_287[5],&l_287[0],(void*)0},{&l_287[0],&l_287[0],&l_287[6],&l_287[6],&l_287[0],&l_287[0]},{&l_287[0],&l_287[0],&l_287[1],&l_287[4],&l_287[7],&l_287[0]}},{{&l_287[0],(void*)0,&l_287[7],&l_287[0],&l_287[7],&l_287[0]},{&l_287[0],&l_287[0],&l_287[0],&l_287[4],&l_287[0],&l_287[0]},{&l_287[0],&l_287[7],&l_287[5],&l_287[6],&l_287[0],&l_287[1]},{&l_287[0],&l_287[7],&l_287[4],&l_287[5],(void*)0,&l_287[0]},{&l_287[6],&l_287[7],(void*)0,&l_287[0],&l_287[0],(void*)0},{&l_287[7],&l_287[7],&l_287[7],&l_287[4],&l_287[0],&l_287[0]}},{{(void*)0,&l_287[0],&l_287[0],&l_287[0],&l_287[7],&l_287[7]},{(void*)0,(void*)0,&l_287[0],&l_287[0],&l_287[7],&l_287[0]},{&l_287[7],&l_287[0],&l_287[7],&l_287[1],&l_287[0],(void*)0},{&l_287[1],&l_287[0],(void*)0,&l_287[7],&l_287[0],&l_287[0]},{&l_287[4],(void*)0,&l_287[4],&l_287[7],&l_287[1],&l_287[1]},{&l_287[1],&l_287[5],&l_287[5],&l_287[1],(void*)0,&l_287[0]}},{{&l_287[7],&l_287[4],&l_287[0],&l_287[0],&l_287[4],&l_287[0]},{(void*)0,&l_287[7],&l_287[7],&l_287[0],&l_287[4],&l_287[0]},{(void*)0,&l_287[4],&l_287[1],&l_287[4],(void*)0,&l_287[0]},{&l_287[7],&l_287[5],&l_287[6],&l_287[0],&l_287[1],(void*)0},{&l_287[6],(void*)0,(void*)0,&l_287[5],&l_287[0],(void*)0},{&l_287[0],&l_287[0],&l_287[6],&l_287[6],&l_287[0],&l_287[0]}},{{&l_287[0],&l_287[0],&l_287[1],&l_287[4],&l_287[7],&l_287[0]},{&l_287[0],(void*)0,&l_287[7],&l_287[0],&l_287[7],&l_287[0]},{&l_287[0],&l_287[0],&l_287[0],&l_287[4],&l_287[0],&l_287[0]},{&l_287[0],&l_287[7],&l_287[5],&l_287[6],&l_287[0],&l_287[1]},{&l_287[0],&l_287[7],&l_287[4],&l_287[5],(void*)0,&l_287[0]},{&l_287[6],&l_287[7],(void*)0,&l_287[0],&l_287[0],(void*)0}}};
                    int32_t l_335 = 0x8E90C7F6L;
                    int32_t l_337[8][6][5] = {{{7L,1L,7L,0xD86FEFD7L,0xFAC9E9EDL},{0xCD7A1B21L,1L,0xD29CBA65L,1L,0x713EDC58L},{0x50DF61B3L,(-1L),0xCF70450DL,(-1L),0xCD7A1B21L},{7L,0x713EDC58L,0xD29CBA65L,0x713EDC58L,7L},{0x44D72B92L,1L,7L,0x713EDC58L,(-1L)},{0L,0x44D72B92L,0xD86FEFD7L,(-1L),(-1L)}},{{1L,0xFAC9E9EDL,0xD29CBA65L,0L,0xD29CBA65L},{0xD29CBA65L,0xD29CBA65L,(-1L),0x44D72B92L,7L},{0xD29CBA65L,(-1L),7L,7L,0xD86FEFD7L},{1L,0x713EDC58L,0xCF70450DL,0x50DF61B3L,(-1L)},{0xCD7A1B21L,(-1L),(-1L),0xCD7A1B21L,1L},{2L,0xD29CBA65L,(-1L),7L,7L}},{{7L,0xFAC9E9EDL,0xCF70450DL,1L,(-1L)},{0x713EDC58L,2L,7L,7L,2L},{0xD86FEFD7L,0L,(-1L),0xCD7A1B21L,2L},{0xFAC9E9EDL,(-1L),0xD29CBA65L,0x50DF61B3L,(-1L)},{0L,0x50DF61B3L,0x44D72B92L,7L,7L},{0xFAC9E9EDL,(-1L),0xFAC9E9EDL,0x44D72B92L,1L}},{{0xD86FEFD7L,(-1L),1L,0L,(-1L)},{0x713EDC58L,0x50DF61B3L,7L,0xD29CBA65L,0xD86FEFD7L},{7L,(-1L),1L,(-1L),7L},{2L,0L,0xFAC9E9EDL,(-1L),0xD29CBA65L},{0xCD7A1B21L,2L,0x44D72B92L,0xD29CBA65L,0xCF70450DL},{1L,0xFAC9E9EDL,0xD29CBA65L,0L,0xD29CBA65L}},{{0xD29CBA65L,0xD29CBA65L,(-1L),0x44D72B92L,7L},{0xD29CBA65L,(-1L),7L,7L,0xD86FEFD7L},{1L,0x713EDC58L,0xCF70450DL,0x50DF61B3L,(-1L)},{0xCD7A1B21L,(-1L),(-1L),0xCD7A1B21L,1L},{2L,0xD29CBA65L,(-1L),7L,7L},{7L,0xFAC9E9EDL,0xCF70450DL,1L,(-1L)}},{{0x713EDC58L,2L,7L,7L,2L},{0xD86FEFD7L,0L,(-1L),0xCD7A1B21L,2L},{0xFAC9E9EDL,(-1L),0xD29CBA65L,0x50DF61B3L,(-1L)},{0L,0x50DF61B3L,0x44D72B92L,7L,7L},{0xFAC9E9EDL,(-1L),0xFAC9E9EDL,0x44D72B92L,1L},{0xD86FEFD7L,(-1L),1L,0L,(-1L)}},{{0x713EDC58L,0x50DF61B3L,7L,0xD29CBA65L,0xD86FEFD7L},{7L,(-1L),1L,(-1L),7L},{2L,0L,0xFAC9E9EDL,(-1L),0xD29CBA65L},{0xCD7A1B21L,2L,0x44D72B92L,0xD29CBA65L,0xCF70450DL},{1L,0xFAC9E9EDL,0xD29CBA65L,0L,0xD29CBA65L},{0xD29CBA65L,0xD29CBA65L,(-1L),0x44D72B92L,7L}},{{0xD29CBA65L,(-1L),7L,7L,0xD86FEFD7L},{1L,0x713EDC58L,0xCF70450DL,0x50DF61B3L,(-1L)},{0xCD7A1B21L,0xCF70450DL,0xCF70450DL,0xD86FEFD7L,(-1L)},{(-1L),1L,0xCF70450DL,0xFAC9E9EDL,7L},{0xFAC9E9EDL,1L,7L,0L,0xCF70450DL},{(-1L),(-1L),0xFAC9E9EDL,0xFAC9E9EDL,(-1L)}}};
                    int8_t l_345 = 0x87L;
                    int i, j, k;
                    if ((((l_168[3] = ((l_236 == ((*l_251) = ((safe_sub_func_uint8_t_u_u((((*l_240) = l_239) != &g_183), (safe_rshift_func_int16_t_s_s(((*l_250) = (p_86 >= (((l_245 , (safe_div_func_float_f_f((((((p_84 , l_207) && ((*g_88) > ((((void*)0 != l_174) , 0xFEEEC83478F368B3LL) == g_183))) ^ l_248) , p_84) <= 0xB.A9BF42p-79), l_206))) , &g_183) == l_249[5]))), 9)))) , (void*)0))) > 0xDC40L)) > (*g_150)) > 0x7735L))
                    { /* block id: 57 */
                        (*g_88) &= (+0x306029FF4D63770DLL);
                        (*l_89) |= (-2L);
                        l_257--;
                    }
                    else
                    { /* block id: 61 */
                        l_262 = l_260;
                    }
                    if (((**l_91) = (((*l_264) = (void*)0) == (g_288 = (((((*l_113) &= ((safe_lshift_func_uint8_t_u_s(((safe_rshift_func_uint8_t_u_s(g_235, 5)) , p_86), p_84)) <= (safe_mod_func_int64_t_s_s((((((safe_mod_func_int16_t_s_s((((void*)0 != &g_88) >= ((safe_rshift_func_int16_t_s_u((0x4F8DL != ((safe_div_func_uint64_t_u_u(p_84, (safe_sub_func_int8_t_s_s((((safe_div_func_uint64_t_u_u((l_206 == g_10), (**l_91))) ^ (-10L)) , (**l_91)), l_207)))) || 0x2F218E09BA9FC394LL)), (*g_150))) >= p_86)), p_86)) < 0x763B51E3D7954E52LL) && g_255[0][1]) , (void*)0) == l_283), p_86)))) , p_86) & l_285) , &g_150)))))
                    { /* block id: 68 */
                        int16_t l_295 = 0xDC82L;
                        int32_t l_297[2][8] = {{0x2BC51397L,0x2BC51397L,0x4E5E22F4L,0x4E5E22F4L,0x2BC51397L,0x2BC51397L,0x4E5E22F4L,0x4E5E22F4L},{0x2BC51397L,0x2BC51397L,0x4E5E22F4L,0x4E5E22F4L,0x2BC51397L,0x2BC51397L,0x4E5E22F4L,0x4E5E22F4L}};
                        int8_t **l_305 = &l_113;
                        int8_t *l_315 = &l_233;
                        int32_t *l_316 = &l_168[1];
                        int i, j;
                        (*l_316) = (safe_div_func_uint16_t_u_u((*g_150), ((safe_mod_func_int8_t_s_s(((*l_315) = (((((safe_rshift_func_uint16_t_u_u((--g_302[1]), (((((((((*l_305) = &g_114[0]) != (((safe_div_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u((+1UL), 1L)), ((*g_88) = (safe_rshift_func_uint8_t_u_s((l_300 &= ((*l_170) = (safe_add_func_int16_t_s_s(0xF471L, (0xE6B9L & 0x2C6DL))))), 6))))) , ((*l_261) = p_84)) , (void*)0)) | (-2L)) ^ g_46) ^ g_299) && l_297[1][0]) & (*l_284)) >= l_234))) , (*g_150)) != g_255[0][1]) > g_173) & 0xDCDAA2D2L)), g_299)) & g_10)));
                        return (**g_288);
                    }
                    else
                    { /* block id: 78 */
                        float ****l_318 = (void*)0;
                        l_319 = l_317;
                    }
                    for (g_183 = 0; (g_183 >= 9); g_183 = safe_add_func_int64_t_s_s(g_183, 6))
                    { /* block id: 83 */
                        volatile int16_t * volatile **l_325 = &g_322;
                        int32_t *l_326 = &l_207;
                        int32_t *l_327 = (void*)0;
                        int32_t *l_328 = (void*)0;
                        int32_t *l_329 = &g_90;
                        int32_t *l_330 = &l_245;
                        int32_t *l_331 = &g_296;
                        int32_t *l_332 = &g_90;
                        int32_t *l_333 = &l_168[1];
                        int32_t *l_334[8][3][8] = {{{&l_167,&l_167,&g_90,&g_296,&g_90,&l_207,(void*)0,&l_168[2]},{&l_168[1],&g_296,&l_207,&g_10,&l_207,&l_167,&l_300,(void*)0},{&l_207,&l_167,&l_300,(void*)0,(void*)0,&l_300,&l_167,&l_207}},{{&g_296,&l_245,&l_207,&g_10,&l_168[2],(void*)0,&l_207,&g_90},{&g_296,&l_300,&l_167,&g_10,&g_10,(void*)0,&g_10,&g_296},{&l_245,&l_245,&g_296,&l_167,&l_207,&l_300,&l_207,&g_90}},{{&g_296,&l_167,&l_245,&l_300,&l_245,&l_167,&g_296,&l_167},{&l_300,&g_296,&l_245,&g_296,&g_296,&l_207,&l_168[1],&l_167},{&l_207,&l_167,(void*)0,&g_298,&g_296,&g_10,&l_206,&l_300}},{{&l_300,(void*)0,(void*)0,&l_167,&l_245,&l_207,&g_10,&g_10},{&g_296,&l_207,&l_168[4],&l_168[4],&l_207,&g_296,(void*)0,&l_168[1]},{&l_245,&g_10,&g_90,&l_245,&g_10,&l_168[1],&l_300,&l_168[3]}},{{&g_296,&l_207,&l_167,&l_245,&l_168[2],&l_206,&g_296,&l_168[1]},{&g_296,&l_168[2],&l_207,&l_168[4],(void*)0,&g_10,(void*)0,&g_10},{&l_207,&l_167,&l_300,&l_167,&l_207,(void*)0,&l_168[3],&l_300}},{{&l_168[1],&l_207,&l_167,&g_298,&g_90,&l_300,&g_296,&l_167},{&l_167,&l_207,&l_167,&g_296,&g_296,&g_296,&l_168[3],&l_167},{&g_90,&l_245,&l_300,&l_300,&g_90,(void*)0,(void*)0,&g_90}},{{&l_167,&l_207,&l_207,&l_167,&l_167,&l_168[3],&g_296,&g_296},{&l_207,&l_206,&l_167,&g_90,&l_207,&l_168[2],&l_167,&l_168[3]},{&l_168[1],&l_300,&l_168[3],&g_296,(void*)0,&l_207,&g_298,&l_206}},{{&g_90,&l_206,&l_245,&g_90,&g_90,&l_168[1],&g_90,&g_90},{&g_298,&l_167,&g_298,&l_167,&g_296,&l_207,&l_300,&g_90},{&g_10,&g_10,&g_90,&l_167,&l_207,&l_167,&g_296,&g_296}}};
                        int i, j, k;
                        (*l_325) = g_322;
                        l_354++;
                    }
                }
                else
                { /* block id: 87 */
                    uint64_t l_360 = 0xBEE34228D4045217LL;
                    int32_t l_377 = (-5L);
                    int32_t l_384 = 0x327F36DEL;
                    int16_t l_390 = 5L;
                    int32_t l_392 = (-1L);
                    int32_t l_393 = 0x02F33945L;
                    int32_t l_394 = 0L;
                    int32_t l_395 = 2L;
                    int32_t l_396[7][5] = {{0x2E4D3A5AL,0x974D873EL,(-1L),(-6L),(-5L)},{0x2E4D3A5AL,(-5L),1L,0L,0L},{(-6L),5L,(-6L),0x974D873EL,(-5L)},{0x71AC3836L,0L,(-5L),0x974D873EL,(-6L)},{(-5L),1L,0L,0L,1L},{1L,0xD15F9FA1L,(-5L),(-6L),(-1L)},{5L,0xD15F9FA1L,(-6L),6L,0x33FA02BCL}};
                    int i, j;
                    for (l_167 = 0; (l_167 != (-18)); l_167 = safe_sub_func_int32_t_s_s(l_167, 6))
                    { /* block id: 90 */
                        int32_t *l_359 = &l_346;
                        l_340 = (((p_86 && 0x22C5L) ^ (-9L)) >= 1L);
                        l_360++;
                        if (l_360)
                            goto lbl_363;
                        (*l_359) = ((*g_88) = (safe_add_func_uint64_t_u_u(p_86, 0x78ADA1DB88194C3CLL)));
                    }
                    for (l_351 = 2; (l_351 >= 0); l_351 -= 1)
                    { /* block id: 99 */
                        int32_t *l_368 = &l_347;
                        int32_t *l_369 = (void*)0;
                        int32_t l_370 = 3L;
                        int32_t *l_371 = &l_346;
                        int32_t *l_372 = &l_349;
                        int32_t *l_373 = &l_346;
                        int32_t *l_374 = &l_352[4];
                        int32_t *l_375 = &l_348[1][5];
                        int32_t *l_376 = &l_341;
                        int32_t *l_378 = &l_342;
                        int32_t *l_379 = &l_206;
                        int32_t *l_380 = &l_338;
                        int32_t *l_381 = &l_352[3];
                        int32_t *l_382 = &l_353[4];
                        int32_t *l_383 = &l_339;
                        int32_t *l_385 = &l_348[0][2];
                        int32_t *l_386 = &l_343;
                        int32_t *l_387 = (void*)0;
                        int32_t *l_388 = &l_352[4];
                        int32_t l_389 = 2L;
                        int32_t *l_391[4][4] = {{&l_384,&l_301,&l_384,&l_301},{&l_384,&l_301,&l_384,&l_301},{&l_384,&l_301,&l_384,&l_301},{&l_384,&l_301,&l_384,&l_301}};
                        int32_t l_400 = 0xA808C3F8L;
                        int i, j;
                        (*l_89) &= (safe_mod_func_int8_t_s_s(g_173, l_360));
                        l_401--;
                        return l_396[6][4];
                    }
                }
                if ((l_404 , ((**l_91) < 1UL)))
                { /* block id: 105 */
                    int64_t l_405 = 0x31AF0A251046F833LL;
                    return l_405;
                }
                else
                { /* block id: 107 */
                    uint16_t l_409 = 0x9070L;
                    for (l_347 = 0; (l_347 != (-8)); l_347 = safe_sub_func_int8_t_s_s(l_347, 4))
                    { /* block id: 110 */
                        int32_t *l_408[1][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
                        int i, j;
                        ++l_409;
                        (*l_91) = g_412;
                        (*l_91) = &l_336;
                    }
                }
                (*l_91) = l_413;
            }
            else
            { /* block id: 117 */
                uint64_t l_422 = 0xCEA65AFA4DA50156LL;
                const float *l_424[7][5] = {{&l_142,&l_142,&l_142,&l_142,&l_142},{(void*)0,&l_142,&l_142,&l_142,&l_142},{&l_142,&l_142,&l_142,&l_142,&l_142},{(void*)0,&l_142,&l_142,&l_142,&l_142},{&l_142,&l_142,&l_142,&l_142,&l_142},{&l_142,&l_142,&l_142,&l_142,(void*)0},{&l_142,&l_142,&l_142,&l_142,&l_142}};
                const float ** const l_423 = &l_424[5][4];
                int32_t l_456 = 0xBD295F02L;
                int32_t l_465 = 1L;
                int32_t l_466 = 0x6983B3E5L;
                int32_t l_467 = (-8L);
                int32_t l_468 = 0x9CD9A14BL;
                int32_t l_470 = 3L;
                int32_t l_471[9][5][5] = {{{0L,(-1L),0x9A96BF83L,0x804F6EDAL,0x7C272A32L},{0x5AE54645L,3L,(-1L),(-3L),1L},{0x8EB9F7FAL,1L,1L,0x8EB9F7FAL,0xF18BAEBDL},{(-9L),3L,0x0C863A66L,0L,0xAC35500FL},{6L,(-1L),1L,(-8L),0xE6D2370BL}},{{(-9L),0L,0L,(-9L),(-2L)},{0x8EB9F7FAL,0x94427BFCL,0x9A96BF83L,0L,0xE6D2370BL},{0x5AE54645L,0x77477AFAL,0xFAD4F798L,(-3L),0xAC35500FL},{0L,1L,0x59E41D44L,0L,0xF18BAEBDL},{0L,6L,0x0C863A66L,(-9L),1L}},{{6L,0x8F23D9ECL,0x59E41D44L,(-8L),0x7C272A32L},{0L,0L,0xFAD4F798L,0L,(-2L)},{0x804F6EDAL,0x8F23D9ECL,0L,(-1L),(-4L)},{0x67FDE18BL,0x5AE54645L,0x9A2191D5L,0xE70F92A1L,0xC907EA7DL},{(-9L),0xA5AE0778L,0xA5AE0778L,(-9L),(-1L)}},{{0xB03C4CDDL,4L,0L,0xAB70DEA4L,0xC907EA7DL},{0x6BA46E08L,0x1A8DC967L,1L,0x5745DCFFL,(-4L)},{0xAB70DEA4L,0x9A2191D5L,0x5CF54DBEL,0xAB70DEA4L,0x8B63348DL},{(-1L),6L,0L,(-9L),1L},{0x67FDE18BL,(-3L),0x5CF54DBEL,0xE70F92A1L,0x65886C57L}},{{(-1L),0xA5AE0778L,1L,(-1L),(-1L)},{0xE1D371C3L,(-3L),0L,0xB03C4CDDL,0L},{0x6BA46E08L,6L,0xA5AE0778L,0x5745DCFFL,0xAD448859L},{0xE1D371C3L,0x9A2191D5L,0x9A2191D5L,0xE1D371C3L,0x8B63348DL},{(-1L),0x1A8DC967L,0L,(-1L),0xAD448859L}},{{0x67FDE18BL,4L,1L,0xE70F92A1L,0L},{(-1L),0xA5AE0778L,(-3L),(-1L),(-1L)},{0xAB70DEA4L,0x5AE54645L,0L,0xE1D371C3L,0x65886C57L},{0x6BA46E08L,(-8L),(-3L),0x5745DCFFL,1L},{0xB03C4CDDL,0x9A2191D5L,1L,0xB03C4CDDL,0x8B63348DL}},{{(-9L),(-8L),0L,(-1L),(-4L)},{0x67FDE18BL,0x5AE54645L,0x9A2191D5L,0xE70F92A1L,0xC907EA7DL},{(-9L),0xA5AE0778L,0xA5AE0778L,(-9L),(-1L)},{0xB03C4CDDL,4L,0L,0xAB70DEA4L,0xC907EA7DL},{0x6BA46E08L,0x1A8DC967L,1L,0x5745DCFFL,(-4L)}},{{0xAB70DEA4L,0x9A2191D5L,0x5CF54DBEL,0xAB70DEA4L,0x8B63348DL},{(-1L),6L,0L,(-9L),1L},{0x67FDE18BL,(-3L),0x5CF54DBEL,0xE70F92A1L,0x65886C57L},{(-1L),0xA5AE0778L,1L,(-1L),(-1L)},{0xE1D371C3L,(-3L),0L,0xB03C4CDDL,0L}},{{0x6BA46E08L,6L,0xA5AE0778L,0x5745DCFFL,0xAD448859L},{0xE1D371C3L,0x9A2191D5L,0x9A2191D5L,0xE1D371C3L,0x8B63348DL},{(-1L),0x1A8DC967L,0L,(-1L),0xAD448859L},{0x67FDE18BL,4L,1L,0xE70F92A1L,0L},{(-1L),0xA5AE0778L,(-3L),(-1L),(-1L)}}};
                int32_t l_474[2];
                const int32_t l_488 = 0x03698BE8L;
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_474[i] = 0L;
                for (l_346 = (-8); (l_346 > (-15)); --l_346)
                { /* block id: 120 */
                    int16_t *l_445[5];
                    int64_t *l_446 = &g_171;
                    int32_t l_455[6][7] = {{0xB5483618L,0xB5483618L,(-1L),0L,1L,(-1L),1L},{0xE0304CA0L,0x663A4968L,0x663A4968L,0xE0304CA0L,4L,0x69700503L,0xE0304CA0L},{0x6D4EA6BCL,1L,9L,9L,1L,0x6D4EA6BCL,0xEFD074F4L},{0L,0xE0304CA0L,0x295C7CEFL,(-1L),(-1L),0x295C7CEFL,0x663A4968L},{9L,(-1L),0L,9L,0x62EB62D4L,0x62EB62D4L,9L},{0x8E78E5FDL,0x663A4968L,0x8E78E5FDL,0L,0x663A4968L,0x295C7CEFL,0x295C7CEFL}};
                    int32_t *l_457 = &g_296;
                    int32_t *l_458 = &l_340;
                    int32_t *l_459 = &l_341;
                    int32_t *l_460 = &g_298;
                    int32_t *l_461 = (void*)0;
                    int32_t *l_462 = &l_341;
                    int32_t *l_463 = &l_206;
                    int32_t *l_464[7][9][4] = {{{&l_342,&l_168[0],(void*)0,(void*)0},{(void*)0,&l_353[1],&g_90,(void*)0},{&l_341,&l_353[2],&l_353[2],&l_341},{&l_343,(void*)0,&l_340,&l_338},{&l_353[2],&l_167,(void*)0,(void*)0},{&l_168[0],&l_342,(void*)0,(void*)0},{(void*)0,&l_167,&l_455[2][4],&l_338},{&l_168[1],(void*)0,(void*)0,&l_341},{(void*)0,&l_353[2],(void*)0,(void*)0}},{{(void*)0,&l_353[1],(void*)0,(void*)0},{&l_343,&l_168[0],(void*)0,(void*)0},{&l_342,&l_167,&l_342,(void*)0},{&l_341,&l_338,(void*)0,&g_90},{(void*)0,(void*)0,&l_168[1],&l_338},{&l_338,&g_90,&l_168[1],(void*)0},{(void*)0,&l_343,(void*)0,&l_168[1]},{&l_341,&l_353[1],&l_342,(void*)0},{&l_342,(void*)0,(void*)0,&l_168[1]}},{{&l_343,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_168[1],&g_90,&l_455[2][4],(void*)0},{(void*)0,&l_353[2],(void*)0,&l_455[2][4]},{&l_168[0],&l_353[2],(void*)0,(void*)0},{&l_353[2],&g_90,&l_340,(void*)0},{&l_343,(void*)0,&l_353[2],(void*)0},{&l_341,(void*)0,&g_90,(void*)0}},{{(void*)0,(void*)0,(void*)0,&l_168[1]},{&l_342,(void*)0,&l_168[1],(void*)0},{(void*)0,&l_353[1],(void*)0,&l_168[1]},{&l_168[0],&l_343,&l_353[2],(void*)0},{&l_342,&g_90,(void*)0,&l_338},{&l_342,(void*)0,&l_353[2],&g_90},{&l_168[0],&l_338,(void*)0,(void*)0},{(void*)0,&l_167,&l_168[1],(void*)0},{&l_342,&l_168[0],(void*)0,(void*)0}},{{(void*)0,&l_353[1],&g_90,(void*)0},{&l_341,&l_353[2],&l_353[2],&l_341},{&l_343,(void*)0,&l_340,&l_338},{&l_353[2],&l_167,(void*)0,(void*)0},{&l_168[0],&l_342,(void*)0,(void*)0},{(void*)0,&l_167,&l_455[2][4],&l_338},{&l_168[1],(void*)0,(void*)0,&l_341},{(void*)0,&l_353[2],(void*)0,(void*)0},{&l_342,&l_342,(void*)0,&l_353[1]}},{{&l_338,&l_353[2],&g_10,(void*)0},{(void*)0,&l_342,(void*)0,&l_455[2][4]},{(void*)0,&g_90,&l_455[2][4],&l_168[1]},{&l_353[1],&g_10,&l_167,&g_90},{&g_90,&l_343,&l_167,&l_353[1]},{&l_353[1],&l_338,&l_455[2][4],&l_167},{(void*)0,&l_342,(void*)0,&l_342},{(void*)0,&l_342,&g_10,(void*)0},{&l_338,&g_10,(void*)0,&l_455[2][4]}},{{&l_342,&l_339,&l_339,&l_339},{&l_353[1],&l_353[1],(void*)0,(void*)0},{(void*)0,&l_343,&l_340,&l_342},{&g_90,&l_168[1],&l_455[2][4],&l_340},{&l_353[2],&l_168[1],(void*)0,&l_342},{&l_168[1],&l_343,&l_348[0][6],(void*)0},{&l_338,&l_353[1],&l_168[1],&l_339},{(void*)0,&l_339,&l_168[1],&l_455[2][4]},{&g_90,&g_10,(void*)0,(void*)0}}};
                    uint64_t l_475 = 0x84C87B246E178F79LL;
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                        l_445[i] = (void*)0;
                    if (((*l_89) = ((*l_413) = ((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s(((((safe_rshift_func_int16_t_s_u(0xE26AL, l_422)) , p_84) , (((l_423 != (((*l_446) = (safe_add_func_int64_t_s_s(((safe_div_func_int8_t_s_s(((safe_mod_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_s((safe_sub_func_int64_t_s_s((((!((safe_mod_func_int64_t_s_s((safe_add_func_int32_t_s_s((l_440 , (((l_168[2] = (p_84 <= ((**l_260) = 0x3.3173A6p+23))) , ((((safe_div_func_int8_t_s_s((p_84 , (((safe_mul_func_int16_t_s_s((((*l_113) = g_253) <= l_157), p_84)) , (void*)0) != (void*)0)), g_302[0])) , (-0x4.9p+1)) , (void*)0) != l_445[1])) || 0L)), 4294967293UL)), g_166[0][4][0])) && p_84)) || g_253) , g_296), 0UL)), 5)) , p_84), p_84)) , g_166[1][5][0]), g_299)) > l_422), g_183))) , &l_424[6][2])) || 6L) && (*l_413))) ^ 4L), 2)), 0x5A12L)) != p_86))))
                    { /* block id: 127 */
                        int32_t *l_453 = (void*)0;
                        int32_t *l_454 = &l_206;
                        (*l_454) |= (((**l_91) |= ((l_447 != l_448) || ((void*)0 != &l_168[3]))) | ((*g_412) |= (safe_add_func_int64_t_s_s((p_86 < p_84), (p_86 <= ((((*l_413) = 0x561CFE55L) & (&l_424[5][4] != (void*)0)) , p_86))))));
                    }
                    else
                    { /* block id: 132 */
                        return (*g_150);
                    }
                    l_475--;
                    (*g_412) = ((*l_458) ^= (g_298 < (safe_div_func_int64_t_s_s((((((void*)0 == g_480[2][1][4]) < (!(!g_255[0][1]))) , 0x368086B4E086AB15LL) , (((((*l_446) = ((safe_add_func_int16_t_s_s((1UL & (((0xD8L == l_488) , (safe_mul_func_uint16_t_u_u((~g_302[1]), l_471[2][2][1]))) ^ p_86)), 0x825BL)) & 6L)) , &g_173) != l_169) != p_86)), g_296))));
                }
            }
            --l_501[1];
            for (g_296 = 0; (g_296 <= 3); g_296 += 1)
            { /* block id: 144 */
                uint8_t *l_525[8][10];
                int32_t l_528 = 0xD167D4ABL;
                int32_t l_541 = 0x87A744B9L;
                int16_t ****l_601 = &l_448;
                uint16_t **l_607[9];
                int i, j;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 10; j++)
                        l_525[i][j] = &g_35;
                }
                for (i = 0; i < 9; i++)
                    l_607[i] = &g_150;
                (*l_493) &= (safe_sub_func_int64_t_s_s(((((*l_495) , (((safe_sub_func_float_f_f(((safe_sub_func_uint8_t_u_u((safe_div_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((((safe_unary_minus_func_int64_t_s((safe_mul_func_int8_t_s_s(((safe_sub_func_int32_t_s_s((*g_412), (((safe_lshift_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u((p_86 , ((-6L) > (safe_add_func_uint16_t_u_u(((*g_150) |= (l_525[4][4] == (void*)0)), (((safe_mul_func_uint16_t_u_u(((**l_91) & (*l_89)), (((p_86 & (**l_91)) , l_528) >= g_255[0][1]))) == (*g_88)) , 0x9B30L))))), (*l_492))) , g_35), 14)) , p_86) != 7UL))) < p_86), 0x10L)))) & (*g_412)) | 18446744073709551612UL) , l_501[1]), 7)), p_86)), p_86)) , 0x2.B0BD76p-11), 0x8.2F6A32p+2)) > p_84) , &g_88)) == (void*)0) | (*l_496)), l_528));
                (*g_88) = ((void*)0 == &l_168[1]);
                for (g_183 = 0; (g_183 <= 3); g_183 += 1)
                { /* block id: 150 */
                    int16_t l_533 = 0xD2B0L;
                    struct S0 *l_545 = &g_546;
                    int64_t *l_562[6] = {&g_171,&g_171,&g_171,&g_171,&g_171,&g_171};
                    int32_t l_574[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_574[i] = 0x5C055DFDL;
                }
            }
        }
        else
        { /* block id: 183 */
            l_168[1] = 0x7.8p+1;
        }
        (*l_89) = (safe_lshift_func_int16_t_s_s(p_86, (safe_lshift_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u(((((((**g_288) |= (p_84 >= (((g_114[0] < ((((((*l_616) = 1L) ^ (safe_sub_func_uint16_t_u_u(1UL, (p_86 || ((*g_568) != (p_84 , l_621)))))) | 0x7A38141BD77BE98ALL) & 0xB61518AC270976EELL) > (**l_91))) , l_622) == l_622))) ^ 3L) == g_299) > l_501[0]) < 3L), l_623[0][1])), p_86))));
        (*g_412) &= ((g_166[1][1][0] >= (((*l_624) = l_622) == &g_149)) || 0xEFDFL);
        return l_625[5][1][4];
    }
    for (l_401 = 0; (l_401 <= 2); l_401 += 1)
    { /* block id: 195 */
        if (g_90)
            goto lbl_626;
    }
    return l_627[2];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_10, "g_10", print_hash_value);
    transparent_crc(g_35, "g_35", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_37[i][j], "g_37[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_46, "g_46", print_hash_value);
    transparent_crc(g_90, "g_90", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_114[i], "g_114[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_166[i][j][k], "g_166[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_171, "g_171", print_hash_value);
    transparent_crc(g_173, "g_173", print_hash_value);
    transparent_crc(g_183, "g_183", print_hash_value);
    transparent_crc(g_235, "g_235", print_hash_value);
    transparent_crc(g_253, "g_253", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_255[i][j], "g_255[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_296, "g_296", print_hash_value);
    transparent_crc(g_298, "g_298", print_hash_value);
    transparent_crc(g_299, "g_299", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_302[i], "g_302[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_324, "g_324", print_hash_value);
    transparent_crc_bytes (&g_482, sizeof(g_482), "g_482", print_hash_value);
    transparent_crc(g_500, "g_500", print_hash_value);
    transparent_crc(g_544.f0, "g_544.f0", print_hash_value);
    transparent_crc_bytes (&g_544.f1, sizeof(g_544.f1), "g_544.f1", print_hash_value);
    transparent_crc(g_544.f2, "g_544.f2", print_hash_value);
    transparent_crc(g_546.f0, "g_546.f0", print_hash_value);
    transparent_crc_bytes (&g_546.f1, sizeof(g_546.f1), "g_546.f1", print_hash_value);
    transparent_crc(g_546.f2, "g_546.f2", print_hash_value);
    transparent_crc(g_658.f0, "g_658.f0", print_hash_value);
    transparent_crc_bytes (&g_658.f1, sizeof(g_658.f1), "g_658.f1", print_hash_value);
    transparent_crc(g_658.f2, "g_658.f2", print_hash_value);
    transparent_crc(g_674, "g_674", print_hash_value);
    transparent_crc(g_693, "g_693", print_hash_value);
    transparent_crc(g_760.f0, "g_760.f0", print_hash_value);
    transparent_crc_bytes (&g_760.f1, sizeof(g_760.f1), "g_760.f1", print_hash_value);
    transparent_crc(g_760.f2, "g_760.f2", print_hash_value);
    transparent_crc_bytes (&g_765, sizeof(g_765), "g_765", print_hash_value);
    transparent_crc(g_800.f0, "g_800.f0", print_hash_value);
    transparent_crc_bytes (&g_800.f1, sizeof(g_800.f1), "g_800.f1", print_hash_value);
    transparent_crc(g_800.f2, "g_800.f2", print_hash_value);
    transparent_crc(g_817.f0, "g_817.f0", print_hash_value);
    transparent_crc_bytes (&g_817.f1, sizeof(g_817.f1), "g_817.f1", print_hash_value);
    transparent_crc(g_817.f2, "g_817.f2", print_hash_value);
    transparent_crc(g_856, "g_856", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_877[i][j][k], "g_877[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_886, "g_886", print_hash_value);
    transparent_crc(g_951, "g_951", print_hash_value);
    transparent_crc(g_980.f0, "g_980.f0", print_hash_value);
    transparent_crc_bytes (&g_980.f1, sizeof(g_980.f1), "g_980.f1", print_hash_value);
    transparent_crc(g_980.f2, "g_980.f2", print_hash_value);
    transparent_crc(g_981.f0, "g_981.f0", print_hash_value);
    transparent_crc_bytes (&g_981.f1, sizeof(g_981.f1), "g_981.f1", print_hash_value);
    transparent_crc(g_981.f2, "g_981.f2", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_1021[i][j][k].f0, "g_1021[i][j][k].f0", print_hash_value);
                transparent_crc_bytes(&g_1021[i][j][k].f1, sizeof(g_1021[i][j][k].f1), "g_1021[i][j][k].f1", print_hash_value);
                transparent_crc(g_1021[i][j][k].f2, "g_1021[i][j][k].f2", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1023.f0, "g_1023.f0", print_hash_value);
    transparent_crc_bytes (&g_1023.f1, sizeof(g_1023.f1), "g_1023.f1", print_hash_value);
    transparent_crc(g_1023.f2, "g_1023.f2", print_hash_value);
    transparent_crc(g_1025, "g_1025", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1057[i][j][k], "g_1057[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1064, "g_1064", print_hash_value);
    transparent_crc(g_1127, "g_1127", print_hash_value);
    transparent_crc(g_1157, "g_1157", print_hash_value);
    transparent_crc(g_1207, "g_1207", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_1225[i][j], "g_1225[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1242[i][j][k], "g_1242[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1248[i], "g_1248[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1295, "g_1295", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_1327[i][j][k], "g_1327[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc_bytes (&g_1379, sizeof(g_1379), "g_1379", print_hash_value);
    transparent_crc(g_1383, "g_1383", print_hash_value);
    transparent_crc(g_1384, "g_1384", print_hash_value);
    transparent_crc(g_1409, "g_1409", print_hash_value);
    transparent_crc(g_1423, "g_1423", print_hash_value);
    transparent_crc(g_1436, "g_1436", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1517[i], "g_1517[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1644[i], "g_1644[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1687, "g_1687", print_hash_value);
    transparent_crc(g_1854, "g_1854", print_hash_value);
    transparent_crc(g_1955.f0, "g_1955.f0", print_hash_value);
    transparent_crc_bytes (&g_1955.f1, sizeof(g_1955.f1), "g_1955.f1", print_hash_value);
    transparent_crc(g_1955.f2, "g_1955.f2", print_hash_value);
    transparent_crc(g_2029, "g_2029", print_hash_value);
    transparent_crc(g_2115.f0, "g_2115.f0", print_hash_value);
    transparent_crc_bytes (&g_2115.f1, sizeof(g_2115.f1), "g_2115.f1", print_hash_value);
    transparent_crc(g_2115.f2, "g_2115.f2", print_hash_value);
    transparent_crc(g_2169, "g_2169", print_hash_value);
    transparent_crc(g_2184, "g_2184", print_hash_value);
    transparent_crc(g_2342, "g_2342", print_hash_value);
    transparent_crc(g_2350, "g_2350", print_hash_value);
    transparent_crc(g_2351, "g_2351", print_hash_value);
    transparent_crc(g_2495, "g_2495", print_hash_value);
    transparent_crc(g_2588.f0, "g_2588.f0", print_hash_value);
    transparent_crc_bytes (&g_2588.f1, sizeof(g_2588.f1), "g_2588.f1", print_hash_value);
    transparent_crc(g_2588.f2, "g_2588.f2", print_hash_value);
    transparent_crc(g_2599.f0, "g_2599.f0", print_hash_value);
    transparent_crc_bytes (&g_2599.f1, sizeof(g_2599.f1), "g_2599.f1", print_hash_value);
    transparent_crc(g_2599.f2, "g_2599.f2", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_2624[i][j].f0, "g_2624[i][j].f0", print_hash_value);
            transparent_crc_bytes(&g_2624[i][j].f1, sizeof(g_2624[i][j].f1), "g_2624[i][j].f1", print_hash_value);
            transparent_crc(g_2624[i][j].f2, "g_2624[i][j].f2", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2675, "g_2675", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 775
   depth: 1, occurrence: 9
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 51
breakdown:
   depth: 1, occurrence: 385
   depth: 2, occurrence: 110
   depth: 3, occurrence: 12
   depth: 4, occurrence: 4
   depth: 5, occurrence: 4
   depth: 6, occurrence: 2
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 3
   depth: 14, occurrence: 2
   depth: 15, occurrence: 1
   depth: 16, occurrence: 2
   depth: 17, occurrence: 3
   depth: 18, occurrence: 2
   depth: 19, occurrence: 5
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 5
   depth: 23, occurrence: 3
   depth: 24, occurrence: 5
   depth: 25, occurrence: 3
   depth: 26, occurrence: 4
   depth: 27, occurrence: 5
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 6
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 35, occurrence: 2
   depth: 36, occurrence: 3
   depth: 37, occurrence: 2
   depth: 40, occurrence: 1
   depth: 42, occurrence: 1
   depth: 44, occurrence: 1
   depth: 46, occurrence: 1
   depth: 51, occurrence: 1

XXX total number of pointers: 549

XXX times a variable address is taken: 1835
XXX times a pointer is dereferenced on RHS: 344
breakdown:
   depth: 1, occurrence: 200
   depth: 2, occurrence: 106
   depth: 3, occurrence: 20
   depth: 4, occurrence: 11
   depth: 5, occurrence: 7
XXX times a pointer is dereferenced on LHS: 325
breakdown:
   depth: 1, occurrence: 263
   depth: 2, occurrence: 53
   depth: 3, occurrence: 4
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
XXX times a pointer is compared with null: 40
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 17
XXX times a pointer is qualified to be dereferenced: 13732

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1593
   level: 2, occurrence: 574
   level: 3, occurrence: 202
   level: 4, occurrence: 158
   level: 5, occurrence: 38
XXX number of pointers point to pointers: 250
XXX number of pointers point to scalars: 287
XXX number of pointers point to structs: 12
XXX percent of pointers has null in alias set: 33.2
XXX average alias set size: 1.58

XXX times a non-volatile is read: 2078
XXX times a non-volatile is write: 1068
XXX times a volatile is read: 164
XXX    times read thru a pointer: 92
XXX times a volatile is write: 37
XXX    times written thru a pointer: 9
XXX times a volatile is available for access: 7.19e+03
XXX percentage of non-volatile access: 94

XXX forward jumps: 2
XXX backward jumps: 7

XXX stmts: 389
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 48
   depth: 2, occurrence: 54
   depth: 3, occurrence: 73
   depth: 4, occurrence: 74
   depth: 5, occurrence: 107

XXX percentage a fresh-made variable is used: 15.6
XXX percentage an existing variable is used: 84.4
********************* end of statistics **********************/


/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 30dccd7
 * Options:   --float
 * Seed:      612392697
 */

#include <float.h>
#include <math.h>
#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
union U0 {
   const volatile uint32_t  f0;
   uint32_t  f1;
};

union U1 {
   int32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 0x925B0E44L;
static volatile float g_5 = 0x1.Dp-1;/* VOLATILE GLOBAL g_5 */
static volatile int32_t g_6 = 0xE04A276AL;/* VOLATILE GLOBAL g_6 */
static uint32_t g_7 = 0xB659BAAAL;
static union U1 g_25 = {0L};
static int8_t g_39[6] = {(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)};
static int16_t g_71 = 0x0603L;
static int8_t g_73 = (-1L);
static union U1 *g_77 = &g_25;
static union U1 **g_76[6] = {&g_77,(void*)0,&g_77,&g_77,(void*)0,&g_77};
static int64_t g_114 = 0x55447BFCA48CC921LL;
static int32_t g_120[5] = {1L,1L,1L,1L,1L};
static uint64_t g_122 = 0xF170B72F399B0BCFLL;
static float g_123 = 0x0.E459F6p+4;
static int32_t g_127 = 0L;
static int8_t g_159 = 0x61L;
static int8_t g_161 = 5L;
static int64_t g_166 = (-8L);
static int64_t * const g_168[2] = {&g_114,&g_114};
static int64_t * const *g_167 = &g_168[1];
static uint32_t g_177 = 0UL;
static uint32_t g_188 = 0x76EB19FDL;
static int16_t *g_201[1] = {&g_71};
static int16_t g_224 = 8L;
static uint8_t g_248 = 0x94L;
static int16_t g_275 = 0x9771L;
static uint16_t g_364[8] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static float g_366[2][3][2] = {{{0x4.046861p+55,(-0x1.Fp+1)},{0x1.9p-1,(-0x1.Fp+1)},{0x4.046861p+55,0x1.9p-1}},{{0x2.D4B6FAp+53,0x2.D4B6FAp+53},{0x2.D4B6FAp+53,0x1.9p-1},{0x4.046861p+55,(-0x1.Fp+1)}}};
static int8_t g_367 = 0L;
static int16_t g_374 = 0L;
static int32_t *g_389 = &g_120[3];
static uint16_t g_456[8] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static uint8_t g_512 = 0x2DL;
static union U1 ***g_528 = (void*)0;
static const uint64_t g_540 = 18446744073709551615UL;
static union U1 * volatile *g_556 = &g_77;
static int32_t g_569[10][4] = {{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L},{0L,0x05EB6B4BL,0x05EB6B4BL,0L}};
static int16_t * const **g_606 = (void*)0;
static int32_t g_642[3] = {(-3L),(-3L),(-3L)};
static int8_t g_646 = 1L;
static union U1 *g_660 = &g_25;
static uint32_t g_685 = 0x42046FDCL;
static union U0 g_718[5] = {{1UL},{1UL},{1UL},{1UL},{1UL}};
static union U0 *g_717 = &g_718[3];
static union U0 g_723 = {0xEA6C0222L};/* VOLATILE GLOBAL g_723 */
static int32_t *g_746 = (void*)0;
static int32_t g_801 = 0x58FE2482L;
static int32_t *g_800 = &g_801;
static uint64_t **g_806 = (void*)0;
static uint64_t ***g_805 = &g_806;
static uint32_t g_808 = 1UL;
static uint32_t g_817 = 0x73ADE265L;
static union U0 g_929 = {0UL};/* VOLATILE GLOBAL g_929 */
static float * volatile g_979[10] = {&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0],&g_366[0][2][0]};
static float * volatile g_980 = &g_123;/* VOLATILE GLOBAL g_980 */
static uint16_t g_1017[8] = {0x9F15L,0x9F15L,0x9F15L,0x9F15L,0x9F15L,0x9F15L,0x9F15L,0x9F15L};
static union U1 * volatile **g_1044 = &g_556;
static union U1 * volatile ** volatile * volatile g_1043[9] = {&g_1044,&g_1044,&g_1044,&g_1044,&g_1044,&g_1044,&g_1044,&g_1044,&g_1044};
static union U1 * volatile ** volatile * volatile * volatile g_1042 = &g_1043[6];/* VOLATILE GLOBAL g_1042 */
static volatile int8_t g_1061 = 0x7FL;/* VOLATILE GLOBAL g_1061 */
static volatile uint16_t g_1108[8] = {2UL,0xF6D3L,2UL,0xF6D3L,2UL,0xF6D3L,2UL,0xF6D3L};
static volatile uint16_t *g_1107[3][10][5] = {{{&g_1108[5],&g_1108[5],&g_1108[4],&g_1108[3],&g_1108[5]},{&g_1108[4],&g_1108[5],(void*)0,&g_1108[4],(void*)0},{(void*)0,&g_1108[5],&g_1108[1],&g_1108[5],&g_1108[4]},{&g_1108[1],(void*)0,&g_1108[4],(void*)0,&g_1108[1]},{&g_1108[0],(void*)0,&g_1108[2],&g_1108[5],&g_1108[2]},{&g_1108[5],&g_1108[5],&g_1108[5],&g_1108[4],&g_1108[4]},{&g_1108[2],&g_1108[5],&g_1108[2],(void*)0,&g_1108[2]},{(void*)0,&g_1108[4],&g_1108[5],&g_1108[1],&g_1108[1]},{&g_1108[2],&g_1108[5],&g_1108[2],&g_1108[0],&g_1108[4]},{&g_1108[1],(void*)0,&g_1108[5],&g_1108[5],(void*)0}},{{&g_1108[5],&g_1108[4],&g_1108[5],&g_1108[2],&g_1108[5]},{&g_1108[2],&g_1108[5],&g_1108[5],(void*)0,&g_1108[5]},{(void*)0,&g_1108[5],&g_1108[2],&g_1108[2],&g_1108[5]},{(void*)0,&g_1108[5],&g_1108[5],&g_1108[1],&g_1108[5]},{&g_1108[5],&g_1108[4],&g_1108[2],&g_1108[5],&g_1108[5]},{&g_1108[5],(void*)0,&g_1108[5],&g_1108[2],&g_1108[5]},{&g_1108[5],&g_1108[1],&g_1108[2],(void*)0,(void*)0},{(void*)0,&g_1108[4],&g_1108[4],(void*)0,&g_1108[5]},{(void*)0,&g_1108[2],&g_1108[1],&g_1108[5],(void*)0},{&g_1108[2],&g_1108[5],(void*)0,&g_1108[5],&g_1108[5]}},{{&g_1108[5],&g_1108[2],&g_1108[4],&g_1108[5],&g_1108[5]},{&g_1108[1],&g_1108[4],&g_1108[5],&g_1108[5],&g_1108[5]},{&g_1108[5],&g_1108[5],&g_1108[5],&g_1108[2],(void*)0},{&g_1108[4],(void*)0,&g_1108[5],&g_1108[5],&g_1108[5]},{&g_1108[0],&g_1108[3],&g_1108[5],&g_1108[7],&g_1108[2]},{&g_1108[4],(void*)0,(void*)0,&g_1108[5],&g_1108[5]},{&g_1108[5],&g_1108[5],&g_1108[7],&g_1108[5],&g_1108[5]},{&g_1108[5],&g_1108[4],&g_1108[1],&g_1108[4],&g_1108[5]},{&g_1108[5],(void*)0,&g_1108[5],&g_1108[0],&g_1108[7]},{&g_1108[1],(void*)0,(void*)0,&g_1108[4],&g_1108[5]}}};
static volatile uint16_t * volatile * volatile g_1106[2][9][5] = {{{&g_1107[0][1][1],&g_1107[2][1][2],&g_1107[0][1][1],(void*)0,&g_1107[1][1][3]},{&g_1107[2][6][3],(void*)0,&g_1107[2][1][1],(void*)0,&g_1107[2][6][3]},{&g_1107[1][0][2],&g_1107[0][1][1],(void*)0,&g_1107[1][1][3],&g_1107[0][1][1]},{&g_1107[0][1][1],(void*)0,&g_1107[2][1][2],&g_1107[0][1][1],&g_1107[0][7][0]},{(void*)0,(void*)0,&g_1107[2][1][1],&g_1107[0][1][1],&g_1107[0][1][1]},{&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[1][0][2],&g_1107[0][7][0],&g_1107[2][6][3]},{&g_1107[0][1][1],(void*)0,&g_1107[1][1][3],&g_1107[2][6][3],&g_1107[1][1][3]},{&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[0][1][1],(void*)0,&g_1107[0][1][1]},{&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[1][1][3],&g_1107[0][1][1]}},{{(void*)0,&g_1107[2][1][2],(void*)0,&g_1107[0][1][1],&g_1107[1][1][3]},{&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[2][1][1],&g_1107[0][1][1],(void*)0},{&g_1107[1][0][2],&g_1107[0][1][1],(void*)0,&g_1107[1][1][3],&g_1107[2][6][3]},{&g_1107[2][6][3],(void*)0,(void*)0,&g_1107[2][6][3],&g_1107[0][7][0]},{&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[2][1][1],&g_1107[0][1][1],&g_1107[0][1][1]},{&g_1107[0][1][1],(void*)0,(void*)0,&g_1107[0][7][0],&g_1107[0][1][1]},{&g_1107[0][1][1],(void*)0,&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[1][1][3]},{&g_1107[2][6][3],&g_1107[0][1][1],&g_1107[0][1][1],&g_1107[2][6][3],(void*)0},{&g_1107[0][1][1],(void*)0,&g_1107[1][1][3],&g_1107[1][1][3],(void*)0}}};
static const uint16_t *g_1110 = &g_364[0];
static const uint16_t **g_1109 = &g_1110;
static volatile int8_t * volatile * volatile g_1209 = (void*)0;/* VOLATILE GLOBAL g_1209 */
static float * volatile g_1233 = &g_366[1][2][0];/* VOLATILE GLOBAL g_1233 */
static int32_t g_1236 = 0xD359D858L;
static int32_t *g_1235 = &g_1236;
static int32_t ** volatile g_1237 = &g_389;/* VOLATILE GLOBAL g_1237 */
static volatile uint8_t g_1256 = 0xB6L;/* VOLATILE GLOBAL g_1256 */
static volatile int64_t g_1326[4] = {0L,0L,0L,0L};
static int8_t *g_1347 = &g_646;
static int8_t **g_1346[6][7] = {{&g_1347,(void*)0,&g_1347,&g_1347,(void*)0,&g_1347,&g_1347},{(void*)0,&g_1347,&g_1347,&g_1347,&g_1347,&g_1347,&g_1347},{(void*)0,&g_1347,&g_1347,(void*)0,&g_1347,&g_1347,(void*)0},{&g_1347,&g_1347,&g_1347,&g_1347,&g_1347,&g_1347,&g_1347},{&g_1347,(void*)0,&g_1347,(void*)0,&g_1347,&g_1347,&g_1347},{&g_1347,&g_1347,&g_1347,&g_1347,&g_1347,&g_1347,&g_1347}};
static int16_t **g_1393 = (void*)0;
static int16_t ***g_1392[3][3] = {{(void*)0,&g_1393,(void*)0},{(void*)0,&g_1393,(void*)0},{(void*)0,&g_1393,(void*)0}};
static uint16_t g_1472 = 65531UL;
static float g_1545 = 0x1.Ap-1;
static float g_1547 = 0x5.2028CAp-45;
static float * volatile g_1562 = (void*)0;/* VOLATILE GLOBAL g_1562 */
static float * volatile g_1563[10] = {&g_1545,&g_1545,&g_1545,&g_1545,&g_1545,&g_1545,&g_1545,&g_1545,&g_1545,&g_1545};
static float * volatile g_1564 = &g_366[0][2][0];/* VOLATILE GLOBAL g_1564 */
static int32_t ** volatile g_1597 = &g_389;/* VOLATILE GLOBAL g_1597 */


/* --- FORWARD DECLARATIONS --- */
static const union U1  func_1(void);
static int32_t * func_12(union U1  p_13, int32_t * p_14);
static union U1  func_15(int8_t  p_16, int32_t * p_17, int32_t * const  p_18, int32_t * p_19);
static int32_t * func_20(union U1  p_21, int16_t  p_22, int32_t * p_23, uint16_t  p_24);
static int32_t * func_34(int16_t  p_35, const int32_t  p_36, int8_t  p_37);
static uint64_t  func_42(int16_t * p_43, const float  p_44, int16_t * p_45, int16_t * p_46, int32_t * p_47);
static int16_t * func_48(int16_t * p_49);
static int16_t * func_50(int32_t * p_51, int32_t  p_52);
static int64_t  func_53(const uint32_t  p_54);
static float  func_65(uint32_t * p_66, int64_t  p_67, int8_t  p_68);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_556 g_77 g_25
 * writes: g_7
 */
static const union U1  func_1(void)
{ /* block id: 0 */
    int32_t *l_2[6][8][2] = {{{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3}},{{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3}},{{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3}},{{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3}},{{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3}},{{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3},{&g_3,&g_3}}};
    int16_t l_4 = 0xE4FCL;
    int8_t l_802 = 0L;
    uint16_t l_1268 = 9UL;
    const union U1 *l_1270 = (void*)0;
    const union U1 **l_1269[2][2][6] = {{{&l_1270,&l_1270,&l_1270,&l_1270,&l_1270,&l_1270},{&l_1270,&l_1270,&l_1270,&l_1270,&l_1270,&l_1270}},{{&l_1270,&l_1270,&l_1270,&l_1270,&l_1270,&l_1270},{&l_1270,&l_1270,&l_1270,&l_1270,&l_1270,&l_1270}}};
    int32_t l_1296 = 0L;
    uint8_t l_1322[7] = {1UL,1UL,0x30L,1UL,1UL,0x30L,1UL};
    int16_t l_1330 = 1L;
    int32_t l_1409 = 0x0D54BCABL;
    uint64_t *** const l_1410 = &g_806;
    union U0 **l_1423 = &g_717;
    union U0 ** const l_1426[4] = {&g_717,&g_717,&g_717,&g_717};
    int32_t l_1427 = (-1L);
    uint16_t l_1431 = 0x6A1DL;
    int32_t l_1432 = 2L;
    union U1 l_1433 = {-1L};
    int32_t l_1461[3][1][4] = {{{1L,1L,1L,1L}},{{1L,1L,1L,1L}},{{1L,1L,1L,1L}}};
    int32_t l_1490 = 1L;
    uint8_t l_1510[8][4][4] = {{{4UL,0x05L,0xC3L,2UL},{255UL,0xE4L,6UL,0UL},{6UL,0UL,0UL,6UL},{0UL,255UL,2UL,0xC3L}},{{0x69L,254UL,0x03L,8UL},{0x58L,0x05L,1UL,8UL},{0UL,254UL,6UL,0xC3L},{3UL,255UL,0x69L,6UL}},{{0x05L,0UL,247UL,0UL},{0x69L,0xE4L,0UL,0x85L},{0UL,0x69L,255UL,0UL},{0UL,0x58L,8UL,3UL}},{{0UL,0UL,255UL,8UL},{0UL,3UL,254UL,0x30L},{0x30L,0x05L,255UL,255UL},{0x69L,0x69L,0x30L,254UL}},{{0x03L,255UL,8UL,255UL},{247UL,0x30L,3UL,8UL},{0xC3L,0x30L,0UL,255UL},{0x30L,255UL,0x85L,254UL}},{{255UL,0x69L,6UL,255UL},{8UL,0x05L,8UL,0x30L},{2UL,3UL,0UL,8UL},{1UL,0UL,0xE4L,3UL}},{{0x30L,0x58L,0xE4L,0UL},{1UL,0x69L,0UL,0x85L},{2UL,0UL,8UL,6UL},{8UL,6UL,6UL,8UL}},{{255UL,255UL,0x85L,0UL},{0x30L,4UL,0UL,0xE4L},{0xC3L,0x69L,3UL,0xE4L},{247UL,4UL,8UL,0UL}}};
    const float l_1566 = 0x7.759AEDp+33;
    int i, j, k;
    --g_7;
    for (g_7 = 0; (g_7 == 50); g_7 = safe_add_func_int8_t_s_s(g_7, 5))
    { /* block id: 4 */
        uint32_t l_32 = 0xE8AFF65CL;
        int32_t *l_33[6];
        int16_t *l_38[9] = {&l_4,&l_4,&l_4,&l_4,&l_4,&l_4,&l_4,&l_4,&l_4};
        uint32_t *l_55[6] = {&g_7,&g_7,&g_7,&g_7,&g_7,&g_7};
        uint32_t *l_56 = &l_32;
        int32_t **l_798 = &l_2[2][6][1];
        int8_t *l_799 = &g_39[3];
        uint16_t *l_1248 = &g_364[0];
        uint16_t **l_1247 = &l_1248;
        uint16_t ** const *l_1246[10][9][2] = {{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}},{{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247},{&l_1247,&l_1247}}};
        int32_t l_1277 = (-5L);
        uint8_t l_1308[7] = {0x8CL,0UL,0UL,0x8CL,0UL,0UL,0x8CL};
        int16_t l_1319 = 8L;
        int32_t l_1324 = (-1L);
        int32_t l_1329 = 0x8EA441A7L;
        int16_t * const **l_1389 = (void*)0;
        int64_t *l_1411[4][1][7];
        int32_t l_1412[9];
        int32_t l_1413 = 0xADBD8877L;
        int8_t l_1420 = 0L;
        union U0 ***l_1424 = (void*)0;
        union U0 ***l_1425 = &l_1423;
        uint32_t l_1428[5][9][5] = {{{4294967295UL,4294967287UL,0x9FA83B31L,0x109779A7L,4294967295UL},{8UL,0UL,7UL,0x3926645AL,4294967295UL},{4294967295UL,4294967295UL,1UL,4294967293UL,4294967288UL},{0UL,0xAF10088AL,0xAF10088AL,0UL,0x8A778D03L},{4294967295UL,4294967293UL,0UL,0xE120AD94L,4294967295UL},{4294967295UL,0x253386F1L,0x68E767F5L,0UL,1UL},{0x0F899CE3L,0x4EDEC206L,1UL,0xE120AD94L,0xD51011B2L},{0x1111F2D5L,0x4E57331EL,0xCD19F43AL,0UL,0x13618B1AL},{0x1D9309ABL,4294967287UL,0UL,4294967293UL,0UL}},{{0x8A778D03L,4294967295UL,0UL,0x3926645AL,9UL},{4294967295UL,4294967288UL,1UL,0x109779A7L,4294967295UL},{0UL,0x3926645AL,4294967295UL,0UL,0x1111F2D5L},{0UL,4294967288UL,0UL,0x2BD7CA4BL,0x9FA83B31L},{0x253386F1L,4294967295UL,0xE6C4A223L,4294967295UL,1UL},{3UL,4294967287UL,1UL,4294967287UL,3UL},{8UL,0x4E57331EL,0x950E9831L,0x13618B1AL,4294967295UL},{0UL,0x4EDEC206L,0x9FA83B31L,4294967293UL,0xDEBD0625L},{4294967295UL,0x253386F1L,0xAF10088AL,0x4E57331EL,4294967295UL}},{{4294967295UL,0xF8C7BF4FL,1UL,4294967295UL,0x28844D4EL},{9UL,0x3926645AL,0UL,4294967295UL,0x8A778D03L},{4294967295UL,0x2B6D5200L,4294967295UL,0x2BD7CA4BL,0UL},{0UL,0x253386F1L,0x950E9831L,0xAF10088AL,4294967295UL},{3UL,4294967295UL,5UL,0xE120AD94L,4294967288UL},{4294967290UL,4294967295UL,0x950E9831L,0UL,0UL},{0x1D9309ABL,0UL,4294967295UL,4294967288UL,0xDEBD0625L},{8UL,0UL,0UL,0UL,0UL},{4294967288UL,4294967293UL,1UL,4294967295UL,4294967295UL}},{{0x13618B1AL,4294967295UL,0x3926645AL,0x389D4567L,0x8A778D03L},{0UL,4294967288UL,0UL,4294967295UL,0UL},{0x13618B1AL,0x253386F1L,4294967292UL,4294967295UL,4294967290UL},{4294967288UL,2UL,7UL,0xE120AD94L,4294967295UL},{8UL,0x3926645AL,4294967287UL,0x253386F1L,9UL},{0x1D9309ABL,0xE120AD94L,0x9FA83B31L,4294967295UL,0UL},{4294967290UL,0x13618B1AL,0UL,7UL,4294967295UL},{3UL,0x2B6D5200L,0x40F56B14L,4294967295UL,4294967295UL},{0UL,0xAF10088AL,7UL,0x253386F1L,4294967295UL}},{{4294967295UL,4294967288UL,0xDEBD0625L,0xE120AD94L,0xDEBD0625L},{9UL,9UL,0x950E9831L,4294967295UL,4294967295UL},{0x0F899CE3L,0UL,0x40F56B14L,4294967295UL,4294967288UL},{8UL,4294967295UL,0xCD19F43AL,0x389D4567L,0UL},{0xDEBD0625L,0UL,0x9FA83B31L,4294967295UL,0UL},{0x1111F2D5L,9UL,0x3926645AL,0UL,4294967295UL},{0x28844D4EL,4294967288UL,7UL,4294967288UL,0x28844D4EL},{0x13618B1AL,0xAF10088AL,4294967295UL,0UL,4294967290UL},{0xDEBD0625L,0x2B6D5200L,0UL,0xE120AD94L,4294967295UL}}};
        int32_t l_1429 = 0xE4D1B539L;
        int8_t l_1430[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
        uint32_t l_1434 = 1UL;
        uint64_t l_1440 = 18446744073709551612UL;
        uint8_t l_1467 = 5UL;
        int32_t l_1511 = 0x45243D33L;
        union U1 ****l_1582 = &g_528;
        uint32_t l_1589 = 0xE4527CAEL;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_33[i] = &g_3;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 7; k++)
                    l_1411[i][j][k] = &g_114;
            }
        }
        for (i = 0; i < 9; i++)
            l_1412[i] = (-8L);
    }
    return (**g_556);
}


/* ------------------------------------------ */
/* 
 * reads : g_1237 g_389
 * writes: g_389
 */
static int32_t * func_12(union U1  p_13, int32_t * p_14)
{ /* block id: 541 */
    uint8_t l_1238 = 0x16L;
    uint16_t *l_1244[3];
    uint16_t **l_1243 = &l_1244[2];
    uint16_t ***l_1242 = &l_1243;
    uint16_t *** const *l_1241 = &l_1242;
    int32_t **l_1245 = &g_389;
    int i;
    for (i = 0; i < 3; i++)
        l_1244[i] = (void*)0;
    (*g_1237) = p_14;
    l_1238--;
    l_1241 = l_1241;
    (*l_1245) = (*g_1237);
    return (*l_1245);
}


/* ------------------------------------------ */
/* 
 * reads : g_646 g_801 g_159 g_275 g_800 g_177 g_7 g_120 g_456 g_929 g_224 g_114 g_6 g_929.f0 g_540 g_71 g_569 g_364 g_25.f0 g_980 g_660 g_25 g_718.f1 g_188 g_1017 g_389 g_127 g_161 g_122 g_248 g_1042 g_73 g_1106 g_1109 g_1043 g_1209 g_1110 g_718.f0 g_1233
 * writes: g_122 g_646 g_159 g_275 g_801 g_718.f1 g_73 g_456 g_71 g_389 g_800 g_123 g_569 g_161 g_120 g_746 g_808 g_1109 g_817 g_366
 */
static union U1  func_15(int8_t  p_16, int32_t * p_17, int32_t * const  p_18, int32_t * p_19)
{ /* block id: 369 */
    const uint16_t *l_833 = &g_456[6];
    int32_t l_834 = 0x254261B5L;
    uint64_t *l_835 = &g_122;
    int32_t l_863 = (-8L);
    int32_t l_868 = 0x0BEEA81CL;
    int32_t l_870[4];
    uint64_t l_874 = 0x052D6831F51F02ABLL;
    int8_t l_907 = (-1L);
    uint8_t l_923 = 0x68L;
    int32_t l_1083 = 2L;
    union U1 l_1140 = {0x1C87C61BL};
    int32_t l_1168 = 0x7B24EAD0L;
    int32_t *l_1211[4] = {&g_120[0],&g_120[0],&g_120[0],&g_120[0]};
    uint64_t l_1212 = 0x254C580BDF8D431ELL;
    int32_t **l_1215 = &g_389;
    int32_t **l_1216 = &l_1211[1];
    uint16_t *l_1219 = (void*)0;
    uint16_t *l_1220[3];
    int32_t **l_1225 = &g_746;
    union U1 l_1234 = {0x9B96A047L};
    int i;
    for (i = 0; i < 4; i++)
        l_870[i] = (-1L);
    for (i = 0; i < 3; i++)
        l_1220[i] = &g_1017[6];
    if (((safe_rshift_func_uint8_t_u_s(0xFEL, (safe_add_func_uint32_t_u_u((safe_lshift_func_int8_t_s_s((safe_div_func_int16_t_s_s((((safe_rshift_func_uint8_t_u_s(((safe_add_func_int32_t_s_s((p_16 == (l_833 != ((l_834 , l_834) , l_833))), (((*l_835) = p_16) != (l_834 != l_834)))) | l_834), p_16)) ^ p_16) && l_834), p_16)), 2)), 4294967293UL)))) && 0x19L))
    { /* block id: 371 */
        float l_845 = 0x9.Dp-1;
        int32_t l_846 = (-3L);
        uint32_t l_847 = 1UL;
        for (g_646 = 0; (g_646 >= 5); ++g_646)
        { /* block id: 374 */
            union U1 ****l_839 = (void*)0;
            union U1 *****l_838[10] = {&l_839,&l_839,&l_839,&l_839,&l_839,&l_839,&l_839,&l_839,&l_839,&l_839};
            int32_t *l_840 = &g_3;
            int32_t l_841 = 0x45984648L;
            int32_t *l_842 = &g_569[8][2];
            int32_t *l_843 = &l_834;
            int32_t *l_844[2];
            uint64_t ***l_850 = (void*)0;
            int i;
            for (i = 0; i < 2; i++)
                l_844[i] = &g_569[4][0];
            l_838[5] = (void*)0;
            ++l_847;
            l_850 = &g_806;
            if ((*p_19))
                break;
        }
    }
    else
    { /* block id: 380 */
        int64_t l_856 = 0xB4B655BE80261C91LL;
        int32_t l_869[8][9] = {{0xAC172DC3L,0x1E345F12L,0xAC172DC3L,0xCFADC8AEL,(-7L),0x0E228C26L,0xFCDAB0DEL,(-1L),0x8AECE240L},{0xD007C0FDL,0xAC172DC3L,0xA9CE6B19L,0x3CB25FB7L,(-1L),(-1L),(-1L),0x3CB25FB7L,0xA9CE6B19L},{0x8AECE240L,0x8AECE240L,0x1E345F12L,0xCFADC8AEL,0xD007C0FDL,0xAC172DC3L,0xA9CE6B19L,0x3CB25FB7L,(-1L)},{0x0E228C26L,0xFCDAB0DEL,(-1L),0x8AECE240L,0L,0xCFADC8AEL,0x3CB25FB7L,0xD007C0FDL,0x0E228C26L},{0xCFADC8AEL,(-5L),0xFCDAB0DEL,0L,(-1L),0x0E8837E9L,0x3CB25FB7L,(-1L),0x1E345F12L},{0x098ED580L,0xAEFDB89CL,(-1L),0x0E228C26L,0x8AECE240L,0x0E228C26L,(-1L),0xAEFDB89CL,0x098ED580L},{(-1L),(-5L),(-7L),0x8AECE240L,0x3CB25FB7L,0x0E228C26L,0xA9CE6B19L,0xFCDAB0DEL,0xAEFDB89CL},{(-5L),0x0E228C26L,0x0E8837E9L,0xD007C0FDL,0xD007C0FDL,0x0E8837E9L,0x0E228C26L,(-5L),0L}};
        int32_t *l_879 = &g_642[1];
        int32_t l_902 = 0L;
        int32_t *l_904 = &g_120[3];
        int32_t *l_905 = &g_120[0];
        int32_t *l_906[2];
        uint32_t l_908 = 4294967289UL;
        uint8_t l_912 = 7UL;
        uint32_t *l_981[10] = {&g_808,&g_808,&g_808,&g_808,&g_808,&g_808,&g_808,&g_808,&g_808,&g_808};
        union U1 *l_1037[7] = {&g_25,&g_25,&g_25,&g_25,&g_25,&g_25,&g_25};
        uint8_t l_1041 = 251UL;
        int16_t *l_1129 = &g_71;
        uint64_t *l_1172 = (void*)0;
        int i, j;
        for (i = 0; i < 2; i++)
            l_906[i] = &g_120[3];
        for (g_159 = 0; (g_159 == (-28)); g_159--)
        { /* block id: 383 */
            int16_t l_858 = 0x934FL;
            int32_t l_860 = 0x010145EBL;
            int32_t l_861 = (-7L);
            int32_t l_867 = 5L;
            int32_t l_871 = (-8L);
            int32_t l_872[7][5][3] = {{{(-5L),0x8D7F3334L,0x6D1C3F1EL},{0x37DE0D16L,0xA2ADF698L,0x91CA8814L},{5L,0x8D7F3334L,3L},{0x37DE0D16L,0x91CA8814L,0x91CA8814L},{(-5L),0x8D7F3334L,0x6D1C3F1EL}},{{0x37DE0D16L,0xA2ADF698L,0x91CA8814L},{5L,0x8D7F3334L,3L},{0x37DE0D16L,0x91CA8814L,0x91CA8814L},{(-5L),0x8D7F3334L,0x6D1C3F1EL},{0x37DE0D16L,0xA2ADF698L,0x91CA8814L}},{{5L,0x8D7F3334L,3L},{0x37DE0D16L,0x91CA8814L,0x91CA8814L},{(-5L),0x8D7F3334L,0x6D1C3F1EL},{0x37DE0D16L,0xA2ADF698L,0x91CA8814L},{5L,0x8D7F3334L,3L}},{{0x37DE0D16L,0x91CA8814L,0x91CA8814L},{(-5L),0x8D7F3334L,0x6D1C3F1EL},{0x37DE0D16L,0xA2ADF698L,0x91CA8814L},{5L,0x8D7F3334L,3L},{0x37DE0D16L,0x91CA8814L,0x91CA8814L}},{{(-5L),0x8D7F3334L,0x6D1C3F1EL},{0x37DE0D16L,0xA2ADF698L,0x91CA8814L},{5L,0x8D7F3334L,3L},{0x37DE0D16L,0x91CA8814L,0x91CA8814L},{(-5L),0x8D7F3334L,0x6D1C3F1EL}},{{0x37DE0D16L,0xA2ADF698L,0x91CA8814L},{5L,0x8D7F3334L,3L},{0x37DE0D16L,0x91CA8814L,0x91CA8814L},{(-5L),0x8D7F3334L,0x6D1C3F1EL},{0x37DE0D16L,0xA2ADF698L,0x91CA8814L}},{{5L,0x8D7F3334L,3L},{0x37DE0D16L,0x91CA8814L,0x91CA8814L},{(-5L),0x8D7F3334L,0x6D1C3F1EL},{0x37DE0D16L,0xA2ADF698L,0x91CA8814L},{5L,0x8D7F3334L,3L}}};
            int32_t *l_880 = &g_642[2];
            union U1 l_903 = {-7L};
            int i, j, k;
            for (g_275 = 3; (g_275 > 25); g_275++)
            { /* block id: 386 */
                float l_855 = 0x1.5p-1;
                int32_t l_859 = 0x446C5D5BL;
                int32_t l_862 = 7L;
                int32_t l_864 = 1L;
                int32_t l_865 = (-1L);
                int32_t l_866 = 1L;
                int32_t l_873 = 0x7F7DD581L;
                uint32_t *l_881 = &g_718[3].f1;
                if (((0x5FL != p_16) >= (l_834 = p_16)))
                { /* block id: 388 */
                    (*p_19) = 1L;
                }
                else
                { /* block id: 390 */
                    int32_t *l_857[9];
                    int i;
                    for (i = 0; i < 9; i++)
                        l_857[i] = &g_120[3];
                    (*g_800) = l_856;
                    if ((*g_800))
                        continue;
                    ++l_874;
                }
                l_860 = (((p_16 , ((*g_800) ^ ((*g_800) , ((*l_881) = ((l_879 = ((safe_div_func_uint64_t_u_u(((void*)0 != &p_19), l_867)) , &g_642[2])) != l_880))))) , (safe_add_func_float_f_f(((safe_sub_func_float_f_f((safe_sub_func_float_f_f(((0xB.B1E4E4p-45 == p_16) == p_16), l_872[1][2][2])), p_16)) >= p_16), p_16))) >= 0xF.1ECE4Ep+56);
                if (l_834)
                    goto lbl_911;
            }
            if (l_870[3])
                break;
            l_902 |= (safe_rshift_func_uint16_t_u_u(((l_858 <= l_867) || ((safe_sub_func_int64_t_s_s(((((*l_835) = (+(((safe_unary_minus_func_uint32_t_u(4294967287UL)) | ((safe_lshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_s((((l_872[0][1][2] == (l_835 != (void*)0)) > ((*p_18) = ((void*)0 == &g_188))) >= (safe_mod_func_uint16_t_u_u(0UL, 0xBF2FL))), 7)), p_16)), g_177)) ^ 5L)) >= g_7))) && 0xCF920A0CAAD89639LL) >= 0xC06FDBFAL), p_16)) > l_869[1][0])), 1));
            return l_903;
        }
lbl_911:
        l_908++;
        --l_912;
        for (l_912 = (-7); (l_912 <= 7); l_912 = safe_add_func_uint32_t_u_u(l_912, 2))
        { /* block id: 410 */
            int32_t l_944 = 0xA596314CL;
            float l_945 = 0x0.76E691p-20;
            uint32_t l_946 = 18446744073709551615UL;
            int32_t l_952 = 0x85DA4E9AL;
            uint16_t *l_953[7][2][10] = {{{&g_364[0],&g_456[6],&g_456[1],&g_456[1],&g_364[0],&g_456[6],&g_456[1],&g_456[1],&g_456[1],&g_456[1]},{&g_364[0],(void*)0,&g_456[6],&g_364[0],&g_364[4],&g_364[6],&g_456[1],&g_456[1],&g_364[6],&g_364[4]}},{{&g_364[6],&g_456[1],&g_456[1],&g_364[6],&g_364[4],&g_364[0],&g_456[6],(void*)0,&g_364[0],&g_456[1]},{(void*)0,&g_364[0],&g_456[6],&g_364[3],&g_456[4],&g_364[6],&g_364[0],&g_456[0],&g_364[0],&g_456[1]}},{{&g_364[0],&g_364[0],&g_456[1],&g_456[5],&g_456[1],&g_364[6],&g_456[6],&g_456[1],&g_456[5],&g_456[4]},{(void*)0,&g_456[0],&g_456[1],&g_364[0],&g_364[0],&g_364[0],&g_456[1],&g_456[0],(void*)0,&g_456[4]}},{{&g_456[5],&g_456[1],&g_456[6],&g_364[6],&g_456[1],&g_456[5],&g_456[1],&g_364[0],&g_364[0],&g_456[1]},{&g_364[0],&g_456[0],&g_364[0],&g_364[6],&g_456[4],&g_364[3],&g_456[6],&g_364[0],(void*)0,&g_456[1]}},{{&g_364[0],&g_364[0],&g_456[0],&g_364[0],&g_456[7],&g_456[5],&g_364[0],&g_364[0],&g_456[5],&g_456[7]},{&g_456[5],&g_364[0],&g_364[0],&g_456[5],&g_456[7],&g_364[0],&g_456[0],&g_364[0],&g_364[0],&g_456[1]}},{{(void*)0,&g_364[0],&g_456[6],&g_364[3],&g_456[4],&g_364[6],&g_364[0],&g_456[0],&g_364[0],&g_456[1]},{&g_364[0],&g_364[0],&g_456[1],&g_456[5],&g_456[1],&g_364[6],&g_456[6],&g_456[1],&g_456[5],&g_456[4]}},{{(void*)0,&g_456[0],&g_456[1],&g_364[0],&g_364[0],&g_364[0],&g_456[1],&g_456[0],(void*)0,&g_456[4]},{&g_456[5],&g_456[1],&g_456[6],&g_364[6],&g_456[1],&g_456[5],&g_456[1],&g_364[0],&g_364[0],&g_456[1]}}};
            int16_t l_956 = (-1L);
            int16_t *l_959 = &g_71;
            int32_t **l_966 = &g_800;
            int32_t l_984[10][1][7] = {{{0xEE2EDD8BL,0L,0xEE2EDD8BL,0x44347D55L,1L,1L,0x44347D55L}},{{0xD07BCA11L,0x0B9C659AL,0xD07BCA11L,(-9L),1L,1L,(-9L)}},{{0xEE2EDD8BL,0L,0xEE2EDD8BL,0x44347D55L,1L,1L,0x44347D55L}},{{0xD07BCA11L,0x0B9C659AL,0xD07BCA11L,(-9L),1L,1L,(-9L)}},{{0xEE2EDD8BL,0L,0xEE2EDD8BL,0x44347D55L,1L,1L,0x44347D55L}},{{0xD07BCA11L,0x0B9C659AL,0xD07BCA11L,(-9L),1L,1L,(-9L)}},{{0xEE2EDD8BL,0L,0xEE2EDD8BL,0x44347D55L,1L,1L,0x44347D55L}},{{0xD07BCA11L,0x0B9C659AL,0xD07BCA11L,(-9L),1L,1L,(-9L)}},{{0xEE2EDD8BL,0L,0xEE2EDD8BL,0x44347D55L,1L,1L,0x44347D55L}},{{0xD07BCA11L,0x0B9C659AL,0xD07BCA11L,(-9L),1L,1L,(-9L)}}};
            uint32_t *l_1018 = (void*)0;
            int64_t l_1023 = 0xE05C59E939F0FE6FLL;
            uint32_t l_1027[10][7][3] = {{{0x04786489L,0x2021B089L,0xE7B7A98AL},{0UL,1UL,0x7B437128L},{1UL,0x04786489L,0xE7B7A98AL},{0UL,0x2D0256BAL,1UL},{0xBBCCA259L,4294967287UL,4294967289UL},{0xD53DA648L,0xB5B2C717L,0x22B7D02BL},{1UL,0x5AA68CD2L,0xC6B03A12L}},{{0x2F166112L,0x0C20D7DBL,2UL},{7UL,1UL,4294967293UL},{0x27DE3E3AL,0x3F929364L,0xF2FF26A0L},{6UL,0x5D20106AL,0xB10AAED5L},{0x3317B67CL,0xCCF29A8DL,0x2021B089L},{0x99C3167CL,0xBBCCA259L,4294967295UL},{0x31F7FA6BL,0x059768F8L,4294967291UL}},{{4294967295UL,4294967295UL,0x4DC03BDDL},{4294967295UL,4294967289UL,0UL},{4294967295UL,0x99C3167CL,0x31F7FA6BL},{4294967295UL,4294967293UL,0x7CBC7042L},{0x31F7FA6BL,0x2021B089L,4294967293UL},{0x99C3167CL,4294967291UL,0x2469C853L},{0x3317B67CL,1UL,1UL}},{{6UL,1UL,0UL},{0x27DE3E3AL,0xE7B7A98AL,7UL},{7UL,0x29384DD9L,0x059768F8L},{0x2F166112L,1UL,0x76CCA7A6L},{1UL,0xCB334201L,0xCB334201L},{0xD53DA648L,0x04786489L,0xD8015F55L},{0xBBCCA259L,0xA2662DF3L,0x27DE3E3AL}},{{1UL,4294967292UL,0x99C3167CL},{0x059768F8L,4294967295UL,4294967287UL},{4UL,4294967292UL,0xB62C45EAL},{0xD8015F55L,0xA2662DF3L,0UL},{1UL,0x04786489L,0x0C20D7DBL},{0x7B437128L,0xCB334201L,0xE7B7A98AL},{4294967292UL,1UL,0x5AA68CD2L}},{{0x89DEAB06L,0x29384DD9L,0x7D41B029L},{0x2BCCAAF4L,0xE7B7A98AL,4294967292UL},{0x2021B089L,1UL,3UL},{4294967291UL,1UL,0UL},{0xB9ABFBA4L,4294967291UL,0x9DF95242L},{0xF1338B03L,0x2021B089L,0x7B437128L},{7UL,4294967293UL,4294967287UL}},{{0x5D20106AL,0x99C3167CL,0UL},{4294967295UL,4294967289UL,0UL},{0xFECFDA7AL,4294967295UL,4294967287UL},{0x5AA68CD2L,0x059768F8L,0x7B437128L},{0UL,0xBBCCA259L,0x9DF95242L},{4294967293UL,0xCCF29A8DL,0UL},{0UL,0x5D20106AL,3UL}},{{1UL,0x3F929364L,4294967292UL},{0xB10AAED5L,1UL,0x7D41B029L},{0UL,0x0C20D7DBL,0x5AA68CD2L},{0xB5B2C717L,0x5AA68CD2L,0xE7B7A98AL},{0x0C20D7DBL,0xB5B2C717L,0x0C20D7DBL},{0UL,4294967287UL,0UL},{2UL,0x2D0256BAL,0xB62C45EAL}},{{0x9DF95242L,0xD8015F55L,4294967287UL},{1UL,0x22B7D02BL,0x99C3167CL},{0x9DF95242L,0x3317B67CL,0x27DE3E3AL},{2UL,4294967293UL,0xD8015F55L},{0UL,0x7B437128L,0xCB334201L},{0x0C20D7DBL,0x7CBC7042L,0x76CCA7A6L},{0xB5B2C717L,0xF1338B03L,0x059768F8L}},{{0UL,0x166030C2L,7UL},{0xB10AAED5L,0x2F166112L,0UL},{1UL,0UL,1UL},{0UL,0x12DB629DL,0x0C20D7DBL},{0xAD70F9FEL,0UL,1UL},{1UL,4294967295UL,4294967292UL},{0xA2662DF3L,0x2BCCAAF4L,1UL}}};
            union U1 *l_1038 = &g_25;
            int64_t l_1064 = 0L;
            union U1 * const **l_1128 = (void*)0;
            int i, j, k;
            for (g_73 = 0; (g_73 == (-8)); g_73 = safe_sub_func_int32_t_s_s(g_73, 2))
            { /* block id: 413 */
                uint16_t *l_926 = &g_456[1];
                int32_t l_937 = 0xD10ACB2EL;
                int32_t **l_947[3][6] = {{&l_904,&g_389,&g_389,&l_904,&l_906[0],&l_904},{&l_904,&l_906[0],&l_904,&g_389,&g_389,&l_904},{&l_905,&l_905,&g_389,&l_906[0],&g_389,&l_905}};
                int i, j;
                p_17 = (l_904 = ((safe_rshift_func_int16_t_s_s((safe_add_func_uint16_t_u_u(((l_923 != 0UL) != (((safe_lshift_func_uint16_t_u_s(((*l_926) &= (*l_905)), (safe_div_func_int8_t_s_s((g_929 , ((safe_mul_func_int8_t_s_s(((+(safe_div_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((l_937 | ((safe_add_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s(0xFAL, 2)), ((((((((p_16 && l_937) , (safe_div_func_int16_t_s_s(((4294967295UL <= l_937) & 4294967292UL), l_944))) == l_834) >= g_224) >= p_16) & p_16) || p_16) && g_114))) , p_16)), p_16)), 0xCE00L))) , l_874), g_6)) == 0UL)), 0x6EL)))) == l_946) != l_834)), 0x3942L)), 12)) , (void*)0));
                p_19 = &l_863;
            }
            if ((0xC2D3E0EE483976F0LL >= (p_16 != ((((*p_18) = (l_923 , (((safe_lshift_func_uint16_t_u_s((safe_rshift_func_int16_t_s_s(((*l_959) |= ((l_834 ^ g_929.f0) , ((l_952 = (p_16 , (g_456[1]++))) || (l_956 == (((safe_add_func_int8_t_s_s(7L, 0x76L)) <= g_540) != 1L))))), 15)), l_944)) || (*g_800)) || 0x59L))) != (*l_905)) & l_863))))
            { /* block id: 423 */
                int32_t **l_960[8][9][1] = {{{&l_906[0]},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{&l_906[0]},{&g_800},{&g_389},{&l_906[0]}},{{&g_389},{&g_800},{&l_906[0]},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{&l_906[0]},{&g_800}},{{&g_389},{&l_906[0]},{&g_389},{&g_800},{&l_906[0]},{(void*)0},{(void*)0},{(void*)0},{(void*)0}},{{&l_906[0]},{&g_800},{&g_389},{&l_906[0]},{&g_389},{&g_800},{&l_906[0]},{(void*)0},{(void*)0}},{{(void*)0},{(void*)0},{&l_906[0]},{&g_800},{&g_389},{&l_906[0]},{&g_389},{&g_800},{&l_906[0]}},{{(void*)0},{(void*)0},{(void*)0},{(void*)0},{&l_906[0]},{&g_800},{&g_389},{&l_906[0]},{&g_389}},{{&g_800},{&l_906[0]},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{&l_906[0]},{&g_800},{&g_389}},{{&l_906[0]},{&g_389},{&g_800},{&l_906[0]},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{&l_906[0]}}};
                int32_t ***l_965 = (void*)0;
                int32_t l_987 = 0x2CDD150EL;
                int i, j, k;
                g_800 = (g_389 = &g_569[4][1]);
                (*g_980) = (safe_mul_func_float_f_f(g_540, (safe_sub_func_float_f_f(((l_966 = &p_17) != (void*)0), (safe_mul_func_float_f_f((safe_mul_func_float_f_f((safe_sub_func_float_f_f(((safe_mul_func_float_f_f(((safe_sub_func_float_f_f(0x5.9F5F18p-58, p_16)) != g_275), g_120[3])) >= (l_874 , (safe_add_func_float_f_f((g_364[0] < l_870[0]), (-0x6.0p-1))))), 0xC.944481p+90)), g_25.f0)), 0xC.067EABp-36))))));
                if (((*g_800) = (*p_18)))
                { /* block id: 429 */
                    int16_t l_982 = (-6L);
                    int32_t l_986 = 0x1DEFC026L;
                    int32_t l_988 = 0xB9F10FDBL;
                    int32_t l_989[3];
                    uint32_t l_996 = 0x4808B458L;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_989[i] = 0L;
                    if ((((void*)0 == l_981[8]) , ((void*)0 == &g_389)))
                    { /* block id: 430 */
                        int64_t l_983 = 1L;
                        int32_t l_985 = 0x978A060FL;
                        int32_t l_990 = 0x385B4B88L;
                        int32_t l_991 = 2L;
                        int32_t l_992 = 6L;
                        int32_t l_993 = 0x40C7F038L;
                        int32_t l_994 = (-1L);
                        int32_t l_995 = 0x151C3FEAL;
                        l_996--;
                        return (*g_660);
                    }
                    else
                    { /* block id: 433 */
                        uint16_t **l_1016 = &l_953[2][1][3];
                        int32_t l_1019 = 0x8CD63F4AL;
                        int32_t l_1020 = (-1L);
                        l_1020 |= (safe_mul_func_int8_t_s_s(p_16, ((safe_lshift_func_int8_t_s_u((-1L), ((((((g_718[3].f1 <= (((safe_lshift_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u(p_16, ((((*p_18) ^= 0x445EF208L) != ((g_188 && 0x19L) , p_16)) || (safe_unary_minus_func_int16_t_s((((safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((((((*l_1016) = ((safe_sub_func_int32_t_s_s((safe_sub_func_uint64_t_u_u(((*l_835) = 18446744073709551609UL), l_907)), (*p_19))) , (void*)0)) != &g_364[0]) && g_1017[6]) ^ p_16), g_120[3])), 0x22L)) , p_18) != l_1018)))))) < (*g_800)), 7)) <= (*g_389)) < l_982)) ^ l_1019) | p_16) , p_16) && 8UL) && g_127))) >= (*g_389))));
                    }
                    for (g_161 = 0; (g_161 <= 11); g_161 = safe_add_func_uint16_t_u_u(g_161, 1))
                    { /* block id: 441 */
                        uint32_t l_1024 = 0xAAE81E85L;
                        l_1024++;
                        l_1027[3][4][1]--;
                        (*p_19) = (*g_800);
                        p_17 = func_34(l_982, ((safe_sub_func_uint16_t_u_u(((p_17 == (void*)0) | (l_996 , (safe_unary_minus_func_uint32_t_u((safe_sub_func_int64_t_s_s(p_16, (p_16 , ((p_16 && ((*l_835) &= (*l_905))) && (safe_sub_func_int64_t_s_s(((l_1037[0] == l_1038) ^ p_16), p_16)))))))))), g_248)) , 0xE0EB6DFDL), l_834);
                    }
                }
                else
                { /* block id: 448 */
                    int64_t l_1059[1][9] = {{0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL,0x9F6ACA880C6E6170LL}};
                    int32_t l_1062[9] = {0x093E3BA6L,0x093E3BA6L,0x093E3BA6L,0x093E3BA6L,0x093E3BA6L,0x093E3BA6L,0x093E3BA6L,0x093E3BA6L,0x093E3BA6L};
                    int8_t *l_1072 = &g_73;
                    int8_t *l_1073[5];
                    uint16_t **l_1076 = &l_953[5][0][8];
                    union U1 * const l_1084 = &g_25;
                    int i, j;
                    for (i = 0; i < 5; i++)
                        l_1073[i] = &g_39[2];
                    for (l_902 = 0; (l_902 >= (-2)); --l_902)
                    { /* block id: 451 */
                        uint32_t l_1049 = 4294967295UL;
                        float *l_1058[3];
                        int32_t l_1060 = 0x26FA22D3L;
                        int32_t l_1063 = 6L;
                        int16_t l_1065[9][6] = {{(-10L),0L,0x1BB2L,1L,0L,0x437CL},{(-2L),0x437CL,0x1BB2L,(-10L),6L,6L},{0xD37AL,0x437CL,0x437CL,0xD37AL,0L,1L},{0xD37AL,0L,1L,(-10L),0x437CL,1L},{(-2L),6L,0x437CL,1L,0x437CL,6L},{(-10L),0L,0x1BB2L,1L,0L,0x437CL},{(-2L),0x437CL,0x1BB2L,(-10L),6L,6L},{0xD37AL,0x437CL,0x437CL,0xD37AL,0L,1L},{0xD37AL,0L,1L,(-10L),0x437CL,1L}};
                        int32_t l_1066 = 0xCE6BF00DL;
                        uint16_t l_1067[10][5] = {{0xFB61L,0xFB61L,0xFB61L,0xFB61L,0xFB61L},{65535UL,0UL,65535UL,0UL,65535UL},{0xFB61L,0xFB61L,0xFB61L,0xFB61L,0xFB61L},{65535UL,0UL,65535UL,0UL,65535UL},{0xFB61L,0xFB61L,0xFB61L,0xFB61L,0xFB61L},{65535UL,0UL,65535UL,0UL,65535UL},{0xFB61L,0xFB61L,0xFB61L,0xFB61L,0xFB61L},{65535UL,0UL,65535UL,0UL,65535UL},{0xFB61L,0xFB61L,0xFB61L,0xFB61L,0xFB61L},{65535UL,0UL,65535UL,0UL,65535UL}};
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_1058[i] = &g_366[1][0][1];
                        (*l_905) = (l_1041 > ((((void*)0 == g_1042) , (p_16 < (safe_add_func_float_f_f((safe_div_func_float_f_f(l_1049, ((safe_add_func_float_f_f((+(safe_sub_func_float_f_f((-0x1.6p-1), (safe_div_func_float_f_f(((+p_16) <= 0x9.F8B56Ep-48), 0x1.0FB675p+19))))), 0x7.145902p+98)) >= p_16))), p_16)))) == 0x1.Bp-1));
                        l_1067[3][1]--;
                    }
                    if ((((*l_835) = 18446744073709551610UL) >= ((safe_div_func_uint8_t_u_u((0x5FL | (((*l_1072) = 0L) | (g_159 ^= l_1064))), (safe_sub_func_uint16_t_u_u(g_224, ((g_746 = func_34(((void*)0 == l_1076), (safe_mod_func_int64_t_s_s((safe_rshift_func_uint8_t_u_u((((void*)0 == p_18) && 0UL), 1)), p_16)), g_224)) == &l_944))))) <= 8L)))
                    { /* block id: 459 */
                        union U1 **l_1085 = &l_1037[6];
                        if (l_1083)
                            break;
                        (*l_1085) = l_1084;
                    }
                    else
                    { /* block id: 462 */
                        float l_1086 = 0x6.792CD6p-35;
                        const int32_t l_1097 = 0x700B3D5AL;
                        (*l_905) ^= l_834;
                        (*p_18) &= (((void*)0 == &g_685) ^ (~((((*g_800) = (((safe_lshift_func_uint8_t_u_u((((safe_rshift_func_uint16_t_u_s(g_929.f0, ((p_16 <= (safe_lshift_func_uint16_t_u_u((((*l_905) = l_863) <= (p_16 > (((+l_1097) , p_16) && ((((*l_959) = ((0UL & p_16) , p_16)) ^ g_364[0]) || 0x28L)))), 13))) != 0L))) | l_1097) ^ 2UL), g_73)) > l_1097) && l_1059[0][7])) <= l_1097) | 0x6D4CL)));
                    }
                    for (g_73 = 0; (g_73 <= 0); g_73 += 1)
                    { /* block id: 471 */
                        if ((*g_800))
                            break;
                    }
                }
                for (g_801 = (-6); (g_801 == 22); g_801++)
                { /* block id: 477 */
                    return (*l_1038);
                }
            }
            else
            { /* block id: 480 */
                int16_t *l_1113 = &g_374;
                int32_t l_1126 = 0L;
                int32_t l_1127 = 0x08C4B1CEL;
                uint64_t l_1191[1];
                uint64_t l_1208 = 0UL;
                int i;
                for (i = 0; i < 1; i++)
                    l_1191[i] = 0UL;
                for (l_874 = 21; (l_874 != 24); l_874 = safe_add_func_int32_t_s_s(l_874, 1))
                { /* block id: 483 */
                    const uint16_t ***l_1111 = &g_1109;
                    int64_t *l_1124 = &l_1064;
                    int64_t *l_1125[10][2] = {{&l_856,&l_856},{&l_856,&g_114},{&g_114,&g_166},{&g_114,&g_166},{&g_114,&g_114},{&l_856,&l_856},{&l_856,&g_114},{&g_114,&g_166},{&g_114,&g_166},{&g_114,&g_114}};
                    int32_t l_1139[3];
                    int i, j;
                    for (i = 0; i < 3; i++)
                        l_1139[i] = (-4L);
                    for (g_808 = (-20); (g_808 < 9); g_808++)
                    { /* block id: 486 */
                        return (*g_660);
                    }
                    if (((safe_mul_func_int16_t_s_s(((g_1106[0][6][4] != ((*l_1111) = g_1109)) , p_16), (((safe_unary_minus_func_uint8_t_u((l_1113 == ((safe_mul_func_uint32_t_u_u((safe_sub_func_uint32_t_u_u((+(-1L)), (g_817 = g_364[0]))), (&g_76[1] == ((safe_add_func_uint64_t_u_u(((safe_mul_func_uint8_t_u_u(((l_1126 = ((*l_1124) &= ((!(**l_966)) <= p_16))) | l_907), l_1127)) != 1L), 18446744073709551615UL)) , l_1128)))) , l_1129)))) | 0x0E7755C5L) && p_16))) >= p_16))
                    { /* block id: 493 */
                        int32_t **l_1130 = &l_904;
                        (*l_1130) = p_18;
                        (**l_1130) = (((*l_835) = 1UL) , (*p_19));
                        if ((**l_1130))
                            continue;
                    }
                    else
                    { /* block id: 498 */
                        if ((*l_905))
                            break;
                        (*l_905) ^= (((safe_mul_func_int16_t_s_s(((*p_19) ^ 0x9A6863FEL), (safe_add_func_int32_t_s_s((*p_18), (!18446744073709551611UL))))) == (p_16 , ((!(p_16 > (p_16 , (safe_div_func_int64_t_s_s(l_1139[0], p_16))))) < 0x501E15C0L))) && 0x2EFAL);
                        return l_1140;
                    }
                    for (l_944 = 0; (l_944 < (-10)); l_944--)
                    { /* block id: 505 */
                        uint16_t l_1143 = 0x025FL;
                        uint64_t **l_1173 = (void*)0;
                        uint64_t **l_1174 = &l_1172;
                        uint32_t *l_1192 = &l_1027[0][4][2];
                        ++l_1143;
                        l_1126 |= (safe_div_func_int64_t_s_s((safe_lshift_func_int16_t_s_u(p_16, (safe_add_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(p_16, (safe_add_func_int8_t_s_s(0L, ((safe_add_func_uint64_t_u_u((**l_966), 18446744073709551614UL)) | (safe_div_func_int32_t_s_s((((safe_rshift_func_int16_t_s_s(l_1143, (safe_mul_func_int16_t_s_s(l_1139[0], (l_863 = p_16))))) || (safe_mul_func_uint16_t_u_u(((((safe_sub_func_uint16_t_u_u(p_16, p_16)) ^ l_1139[0]) <= 0x7BL) && 0L), l_1168))) <= 0xEBL), l_1168))))))), 0xC330D1A7L)))), l_1127));
                        if ((*g_800))
                            continue;
                        (*l_905) ^= (safe_unary_minus_func_int8_t_s(((safe_add_func_uint64_t_u_u(((l_1124 == ((*l_1174) = l_1172)) && l_1139[1]), (safe_mul_func_uint16_t_u_u((l_1126 |= (safe_rshift_func_uint8_t_u_s((safe_mod_func_uint64_t_u_u(((safe_rshift_func_int16_t_s_u(0xB2D7L, (safe_lshift_func_uint16_t_u_s((safe_mod_func_uint32_t_u_u(((*l_1192) |= (p_16 <= ((safe_mod_func_uint8_t_u_u((0xAFL == (safe_sub_func_uint8_t_u_u(0x4AL, g_127))), (((*l_835) = p_16) , 0x0EL))) | l_1191[0]))), (*g_800))), l_1168)))) > l_1127), l_1127)), p_16))), 0x1206L)))) < 1UL)));
                    }
                    (*l_966) = &l_869[1][3];
                }
                for (g_73 = 5; (g_73 >= 0); g_73 -= 1)
                { /* block id: 520 */
                    uint32_t l_1204 = 0x74ADFC11L;
                    int32_t l_1210 = 0L;
                    for (l_834 = 0; (l_834 <= 6); l_834 += 1)
                    { /* block id: 523 */
                        int8_t *l_1195[7] = {&g_39[2],&g_39[2],&g_39[2],&g_39[2],&g_39[2],&g_39[2],&g_39[2]};
                        int8_t ** const l_1194[9][9][3] = {{{&l_1195[0],&l_1195[3],&l_1195[2]},{&l_1195[0],&l_1195[0],(void*)0},{&l_1195[3],&l_1195[6],&l_1195[0]},{&l_1195[0],(void*)0,&l_1195[0]},{&l_1195[3],&l_1195[6],&l_1195[0]},{&l_1195[0],&l_1195[0],&l_1195[0]},{&l_1195[0],&l_1195[0],&l_1195[2]},{&l_1195[0],&l_1195[0],&l_1195[0]},{&l_1195[4],&l_1195[6],&l_1195[0]}},{{&l_1195[0],&l_1195[4],&l_1195[2]},{&l_1195[1],&l_1195[6],(void*)0},{&l_1195[0],&l_1195[0],&l_1195[0]},{&l_1195[0],&l_1195[0],&l_1195[6]},{&l_1195[2],&l_1195[0],&l_1195[2]},{&l_1195[6],&l_1195[6],(void*)0},{&l_1195[2],(void*)0,&l_1195[0]},{&l_1195[6],&l_1195[6],(void*)0},{(void*)0,&l_1195[0],&l_1195[2]}},{{&l_1195[6],&l_1195[3],&l_1195[6]},{&l_1195[0],(void*)0,&l_1195[0]},{&l_1195[2],&l_1195[1],(void*)0},{(void*)0,&l_1195[0],&l_1195[2]},{&l_1195[2],(void*)0,&l_1195[0]},{(void*)0,&l_1195[0],&l_1195[0]},{&l_1195[2],&l_1195[2],&l_1195[2]},{&l_1195[0],&l_1195[2],&l_1195[0]},{&l_1195[6],&l_1195[2],&l_1195[0]}},{{(void*)0,&l_1195[0],&l_1195[0]},{&l_1195[6],&l_1195[0],&l_1195[0]},{&l_1195[2],&l_1195[0],(void*)0},{&l_1195[6],&l_1195[2],&l_1195[2]},{&l_1195[2],&l_1195[2],&l_1195[0]},{&l_1195[0],&l_1195[2],&l_1195[1]},{&l_1195[0],&l_1195[0],&l_1195[4]},{&l_1195[1],(void*)0,&l_1195[6]},{&l_1195[0],&l_1195[0],&l_1195[4]}},{{&l_1195[4],&l_1195[1],&l_1195[1]},{&l_1195[0],(void*)0,&l_1195[0]},{&l_1195[0],&l_1195[3],&l_1195[2]},{&l_1195[0],&l_1195[0],(void*)0},{&l_1195[3],&l_1195[6],&l_1195[0]},{&l_1195[0],(void*)0,&l_1195[0]},{&l_1195[3],&l_1195[6],&l_1195[0]},{&l_1195[0],&l_1195[0],&l_1195[0]},{&l_1195[0],&l_1195[6],&l_1195[0]}},{{&l_1195[0],&l_1195[0],&l_1195[0]},{&l_1195[0],&l_1195[0],&l_1195[1]},{&l_1195[3],&l_1195[4],&l_1195[0]},{&l_1195[2],&l_1195[0],&l_1195[4]},{&l_1195[0],&l_1195[0],&l_1195[0]},{&l_1195[6],&l_1195[6],&l_1195[0]},{&l_1195[0],&l_1195[2],&l_1195[0]},{&l_1195[2],&l_1195[0],&l_1195[3]},{&l_1195[0],(void*)0,&l_1195[0]}},{{&l_1195[0],&l_1195[2],&l_1195[3]},{&l_1195[0],&l_1195[4],&l_1195[0]},{&l_1195[0],(void*)0,&l_1195[0]},{&l_1195[0],(void*)0,&l_1195[0]},{&l_1195[6],&l_1195[0],&l_1195[4]},{(void*)0,&l_1195[0],&l_1195[0]},{&l_1195[2],&l_1195[4],&l_1195[1]},{(void*)0,&l_1195[0],&l_1195[0]},{&l_1195[6],&l_1195[0],&l_1195[0]}},{{&l_1195[0],&l_1195[0],&l_1195[2]},{&l_1195[0],&l_1195[2],&l_1195[6]},{&l_1195[0],&l_1195[0],&l_1195[2]},{&l_1195[0],(void*)0,&l_1195[6]},{&l_1195[0],&l_1195[0],(void*)0},{&l_1195[2],&l_1195[2],&l_1195[6]},{&l_1195[0],&l_1195[0],&l_1195[0]},{&l_1195[6],&l_1195[0],&l_1195[2]},{&l_1195[0],&l_1195[0],(void*)0}},{{&l_1195[2],&l_1195[4],&l_1195[2]},{&l_1195[3],&l_1195[0],(void*)0},{&l_1195[0],&l_1195[0],&l_1195[2]},{&l_1195[0],(void*)0,&l_1195[0]},{&l_1195[6],(void*)0,&l_1195[6]},{&l_1195[0],&l_1195[4],(void*)0},{(void*)0,&l_1195[2],&l_1195[6]},{&l_1195[0],(void*)0,&l_1195[2]},{(void*)0,&l_1195[0],&l_1195[6]}}};
                        int32_t l_1207 = 0L;
                        int i, j, k;
                        l_1210 &= (safe_unary_minus_func_uint64_t_u((4294967286UL > (((l_1194[3][8][1] == ((safe_add_func_int8_t_s_s((((*g_800) & ((void*)0 == (*g_1042))) < ((safe_add_func_int32_t_s_s((((((safe_lshift_func_int8_t_s_u((safe_sub_func_int8_t_s_s(l_1204, ((p_16 | (safe_mul_func_int16_t_s_s((((*l_905) &= ((void*)0 != &l_959)) , 0x13F7L), 65532UL))) > 0x67B4D115BF66723BLL))), l_1204)) != (**l_966)) & l_1126) <= 7L) | l_1207), l_1208)) , 0UL)), 247UL)) , g_1209)) > 0x4F75L) <= 0xDCL))));
                    }
                }
                return l_1140;
            }
            if ((*g_800))
                continue;
        }
    }
    l_1212++;
    (*l_1216) = ((*l_1215) = (void*)0);
    (*g_1233) = ((((0x6441L < (g_456[6] |= (~((*p_19) = (~(**g_1109)))))) , (safe_lshift_func_uint16_t_u_s(((safe_mod_func_uint32_t_u_u((l_1225 != (void*)0), (+((safe_rshift_func_uint8_t_u_s(g_718[3].f0, 7)) || p_16)))) ^ (4294967295UL != ((safe_rshift_func_int8_t_s_u(p_16, 0)) | (((*l_835) = ((safe_lshift_func_uint16_t_u_u((0x0CAF477D769B70BBLL >= p_16), (*g_1110))) && 0x2132E86BL)) > 0x0C4AB606722E127DLL)))), 0))) , &l_874) != (void*)0);
    return l_1234;
}


/* ------------------------------------------ */
/* 
 * reads : g_805 g_122 g_817 g_367 g_800 g_801
 * writes: g_808 g_248 g_801
 */
static int32_t * func_20(union U1  p_21, int16_t  p_22, int32_t * p_23, uint16_t  p_24)
{ /* block id: 361 */
    int32_t *l_803 = (void*)0;
    int32_t **l_804 = &l_803;
    uint32_t *l_807 = &g_808;
    uint8_t *l_819 = &g_248;
    uint8_t l_820 = 0x8BL;
    (*l_804) = l_803;
    (*g_800) ^= (((*l_807) = (((void*)0 == g_805) >= p_24)) , ((p_22 = p_24) >= (safe_lshift_func_uint16_t_u_s(g_122, (safe_div_func_uint8_t_u_u(((*l_819) = ((((safe_div_func_uint64_t_u_u(((((((safe_mul_func_float_f_f(p_21.f0, g_817)) <= (p_24 , (+(-0x1.1p-1)))) < g_367) , 0x06L) > p_21.f0) == p_24), p_21.f0)) >= p_24) , 9UL) , 1UL)), l_820))))));
    p_23 = (*l_804);
    return p_23;
}


/* ------------------------------------------ */
/* 
 * reads : g_389
 * writes: g_389
 */
static int32_t * func_34(int16_t  p_35, const int32_t  p_36, int8_t  p_37)
{ /* block id: 356 */
    int32_t **l_797 = &g_389;
    (*l_797) = (void*)0;
    return (*l_797);
}


/* ------------------------------------------ */
/* 
 * reads : g_374 g_685 g_120 g_606 g_161 g_389 g_127 g_569 g_3 g_166 g_367 g_188 g_364 g_114 g_248 g_723.f1
 * writes: g_374 g_746 g_569 g_3 g_166 g_367 g_188 g_248
 */
static uint64_t  func_42(int16_t * p_43, const float  p_44, int16_t * p_45, int16_t * p_46, int32_t * p_47)
{ /* block id: 324 */
    float * const l_757 = &g_366[0][2][0];
    const int32_t l_758 = 0L;
    float *l_760 = &g_123;
    int16_t **l_762 = (void*)0;
    int16_t ***l_761[10][7] = {{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762},{&l_762,&l_762,&l_762,&l_762,&l_762,(void*)0,&l_762}};
    union U1 l_763[6] = {{0xB371DA64L},{0xB371DA64L},{0xB371DA64L},{0xB371DA64L},{0xB371DA64L},{0xB371DA64L}};
    uint64_t **l_765 = (void*)0;
    uint64_t ***l_766 = &l_765;
    int32_t l_786 = 8L;
    int32_t l_788 = (-7L);
    int32_t l_789 = 0L;
    int32_t l_790[2][8][7] = {{{0x29F49B9FL,0x205230F9L,0x7E1E191AL,0x7E1E191AL,0x2E560C64L,0x19A92A79L,0x2E560C64L},{0xBBD68B71L,3L,3L,0xBBD68B71L,0x1D38068FL,0xBBD68B71L,3L},{0x29F49B9FL,0x29F49B9FL,0x19A92A79L,0x205230F9L,0x19A92A79L,0x29F49B9FL,0x29F49B9FL},{3L,3L,5L,3L,3L,3L,3L},{0x7E1E191AL,0x2E560C64L,0x7E1E191AL,0x19A92A79L,0x19A92A79L,0x7E1E191AL,0x2E560C64L},{3L,0x1D38068FL,5L,5L,0x1D38068FL,3L,0x1D38068FL},{0x7E1E191AL,0x19A92A79L,0x19A92A79L,0x7E1E191AL,0x2E560C64L,0x7E1E191AL,0x19A92A79L},{3L,3L,3L,5L,3L,3L,3L}},{{0x29F49B9FL,0x19A92A79L,0x205230F9L,0x19A92A79L,0x29F49B9FL,0x29F49B9FL,0x19A92A79L},{0xBBD68B71L,0x1D38068FL,0xBBD68B71L,3L,3L,0xBBD68B71L,0x1D38068FL},{0x19A92A79L,0x2E560C64L,0x205230F9L,0x205230F9L,0x2E560C64L,0x19A92A79L,0x2E560C64L},{0xBBD68B71L,3L,3L,0xBBD68B71L,0x1D38068FL,0xBBD68B71L,3L},{0x29F49B9FL,0x29F49B9FL,0x19A92A79L,0x205230F9L,0x19A92A79L,0x29F49B9FL,0x29F49B9FL},{3L,3L,5L,3L,3L,3L,3L},{0x7E1E191AL,0x2E560C64L,0x7E1E191AL,0x19A92A79L,0x19A92A79L,0x7E1E191AL,0x2E560C64L},{3L,0x1D38068FL,5L,5L,0x1D38068FL,3L,0x1D38068FL}}};
    int64_t l_792[8][10] = {{(-1L),0x51264884E80ADB4BLL,1L,1L,0x51264884E80ADB4BLL,(-1L),0x094A93DCC14BFC3CLL,(-1L),(-3L),0xA2E3C95C43870D0ALL},{(-1L),(-9L),7L,(-1L),0L,6L,0x68BACE5928000F51LL,(-5L),0x68BACE5928000F51LL,6L},{(-1L),1L,(-3L),1L,(-1L),(-1L),6L,3L,(-2L),(-9L)},{(-1L),6L,3L,(-2L),(-9L),0xA2E3C95C43870D0ALL,0x51264884E80ADB4BLL,0x51264884E80ADB4BLL,0xA2E3C95C43870D0ALL,(-9L)},{(-3L),(-2L),(-2L),(-3L),(-1L),(-9L),7L,(-1L),0L,6L},{0x094A93DCC14BFC3CLL,(-1L),(-3L),0xA2E3C95C43870D0ALL,0L,7L,(-2L),7L,0L,0xA2E3C95C43870D0ALL},{6L,0x094A93DCC14BFC3CLL,6L,(-3L),0x51264884E80ADB4BLL,(-5L),0L,0x68BACE5928000F51LL,0xA2E3C95C43870D0ALL,1L},{1L,(-5L),(-1L),(-2L),0x68BACE5928000F51LL,(-9L),(-9L),0x68BACE5928000F51LL,(-2L),(-1L)}};
    int64_t l_793 = 0x6AFE5E99B496EC48LL;
    uint32_t l_794[4];
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_794[i] = 0x2EE3DC9DL;
    for (g_374 = 0; (g_374 == (-13)); g_374 = safe_sub_func_uint16_t_u_u(g_374, 1))
    { /* block id: 327 */
        int32_t *l_745 = &g_25.f0;
        uint8_t *l_759 = (void*)0;
        int32_t *l_764 = &g_3;
        (*l_764) |= ((*p_47) &= (safe_lshift_func_int8_t_s_u(((g_746 = l_745) != p_47), (safe_lshift_func_int16_t_s_u(((safe_rshift_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((safe_rshift_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s(g_685, (p_44 , ((((((l_757 != (g_120[3] , ((&g_512 == ((l_758 <= (l_758 , (-7L))) , l_759)) , l_760))) != 0xB07EL) , g_606) != l_761[2][1]) , l_763[0]) , (*p_45))))), g_161)), (*g_389))), 9)) <= (*g_389)), 10)))));
    }
    (*l_766) = l_765;
    for (g_166 = (-25); (g_166 != (-25)); g_166 = safe_add_func_int64_t_s_s(g_166, 2))
    { /* block id: 335 */
        for (g_367 = 0; (g_367 < (-2)); g_367 = safe_sub_func_int64_t_s_s(g_367, 4))
        { /* block id: 338 */
            int64_t l_771 = 0x892FEEC0234D1127LL;
            (*p_47) |= l_771;
        }
    }
    for (g_166 = (-25); (g_166 == (-1)); g_166++)
    { /* block id: 344 */
        int16_t l_778 = (-1L);
        uint8_t *l_779 = &g_248;
        int32_t *l_782 = &g_127;
        int32_t *l_783 = &g_127;
        int32_t *l_784 = (void*)0;
        int32_t l_785 = 0xB1DD2FACL;
        int32_t *l_787[6] = {(void*)0,(void*)0,(void*)0,(void*)0,&l_785,&l_785};
        int64_t l_791 = 0L;
        int i;
        for (g_188 = 14; (g_188 < 13); --g_188)
        { /* block id: 347 */
            return l_758;
        }
        (*p_47) = (safe_sub_func_int8_t_s_s(g_364[5], (l_778 || (g_114 || ((--(*l_779)) && l_778)))));
        --l_794[3];
        return g_723.f1;
    }
    return l_786;
}


/* ------------------------------------------ */
/* 
 * reads : g_159 g_177 g_114 g_71 g_127 g_224 g_73 g_25.f0 g_3 g_166 g_120 g_188 g_275 g_248 g_122 g_7 g_364 g_374 g_389
 * writes: g_159 g_177 g_188 g_127 g_122 g_73 g_201 g_120 g_161 g_71 g_248 g_123 g_224 g_364 g_366 g_367 g_389 g_77 g_456 g_275
 */
static int16_t * func_48(int16_t * p_49)
{ /* block id: 68 */
    int32_t l_184 = 0x1C190B43L;
    uint32_t *l_236 = &g_177;
    int64_t *l_255[2][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_114,&g_114,&g_114,&g_114,&g_114}};
    int64_t **l_254 = &l_255[0][1];
    int64_t ***l_253 = &l_254;
    uint8_t l_268 = 1UL;
    int32_t l_285[2];
    int32_t l_384 = (-5L);
    union U1 l_426 = {-1L};
    union U1 *l_454 = &l_426;
    union U1 **l_453 = &l_454;
    const uint64_t l_472 = 0UL;
    int32_t l_583[10][10][2] = {{{0xD20D9E4EL,0xD56C7111L},{1L,(-1L)},{(-8L),0xD20D9E4EL},{(-1L),9L},{(-1L),0xD74A63E5L},{0x83545172L,0xC24A00DBL},{1L,(-1L)},{(-1L),0x282B2FB7L},{0x2DD19413L,0x825A18B2L},{0L,0L}},{{1L,0x0EC7890AL},{0xF016B6FDL,0x8813FA40L},{0x3E2735CCL,0xCC7BC3A6L},{(-7L),0x3E2735CCL},{(-1L),0x3CD4F3BFL},{(-1L),0x3E2735CCL},{(-7L),0xCC7BC3A6L},{0x3E2735CCL,0x8813FA40L},{0xF016B6FDL,0x0EC7890AL},{1L,0L}},{{0L,0x825A18B2L},{0x2DD19413L,0x282B2FB7L},{(-1L),(-1L)},{1L,0xC24A00DBL},{0x83545172L,0xD74A63E5L},{(-1L),9L},{(-1L),0xD20D9E4EL},{(-8L),(-1L)},{1L,0xD56C7111L},{0xD20D9E4EL,0xF761D017L}},{{0xCFE1D11BL,0xA7A851C6L},{0x4FA55604L,(-1L)},{5L,5L},{6L,(-1L)},{0x8813FA40L,0xAF70D081L},{0x282B2FB7L,1L},{4L,5L},{0xD56C7111L,(-2L)},{0xFE72E0D1L,0x9ED4FEA0L},{0xB04D5039L,1L}},{{0xADB1D3F3L,(-1L)},{0x4AB5682EL,(-7L)},{5L,0xCFE1D11BL},{1L,0x54E23AE5L},{(-8L),0xB04D5039L},{0x65F49826L,0xB04D5039L},{(-8L),0x54E23AE5L},{1L,0xCFE1D11BL},{5L,(-7L)},{0x4AB5682EL,(-1L)}},{{0xADB1D3F3L,1L},{0xB04D5039L,0x9ED4FEA0L},{0xFE72E0D1L,(-2L)},{0xD56C7111L,5L},{4L,1L},{0x282B2FB7L,0xAF70D081L},{0x8813FA40L,(-1L)},{6L,5L},{5L,(-1L)},{0x4FA55604L,0xA7A851C6L}},{{0xCFE1D11BL,0xF761D017L},{0xD20D9E4EL,0xD56C7111L},{1L,(-1L)},{(-8L),0xD20D9E4EL},{(-1L),9L},{(-1L),0xD74A63E5L},{0x83545172L,0xC24A00DBL},{1L,(-1L)},{(-1L),0x282B2FB7L},{0x2DD19413L,0x825A18B2L}},{{0L,0L},{1L,0x0EC7890AL},{0xF016B6FDL,0x8813FA40L},{0x3E2735CCL,0xCC7BC3A6L},{(-7L),0x3E2735CCL},{(-1L),0x3CD4F3BFL},{(-1L),0x3E2735CCL},{(-7L),0xCC7BC3A6L},{0x3E2735CCL,0x8813FA40L},{0xF016B6FDL,0x0EC7890AL}},{{1L,0L},{0L,0x825A18B2L},{0x2DD19413L,0x282B2FB7L},{(-1L),(-1L)},{1L,0xC24A00DBL},{0x83545172L,0xD74A63E5L},{(-1L),9L},{(-1L),0xD20D9E4EL},{(-8L),(-1L)},{(-1L),1L}},{{0x4FA55604L,5L},{0L,0xD56C7111L},{0x83545172L,0xB04D5039L},{(-7L),0xC24A00DBL},{(-9L),5L},{0x65F49826L,0xADB1D3F3L},{0xB5ED9690L,1L},{0x81D7FDC5L,(-7L)},{1L,0xF761D017L},{9L,0xCD1A0883L}}};
    uint32_t l_624[4][8][3] = {{{0xA86C9AF4L,5UL,0x934B9437L},{0UL,1UL,0xEC2AEBECL},{0xA86C9AF4L,0x934B9437L,1UL},{0x8BBB80A8L,1UL,0UL},{8UL,5UL,1UL},{0UL,0UL,0xEC2AEBECL},{8UL,0x934B9437L,0x934B9437L},{0x8BBB80A8L,0UL,0UL}},{{0xA86C9AF4L,5UL,0x934B9437L},{0UL,1UL,0xEC2AEBECL},{0xA86C9AF4L,0x934B9437L,1UL},{0x8BBB80A8L,1UL,0UL},{8UL,5UL,1UL},{0UL,0UL,0xEC2AEBECL},{8UL,0x934B9437L,0x934B9437L},{0x8BBB80A8L,0UL,0UL}},{{0xA86C9AF4L,5UL,0x934B9437L},{0UL,1UL,0xEC2AEBECL},{0xA86C9AF4L,0x934B9437L,1UL},{0x8BBB80A8L,1UL,0UL},{8UL,5UL,1UL},{0UL,0UL,0xEC2AEBECL},{8UL,0x934B9437L,0x934B9437L},{0x8BBB80A8L,0UL,0UL}},{{0xA86C9AF4L,5UL,0x934B9437L},{0UL,1UL,0xEC2AEBECL},{0xA86C9AF4L,0x934B9437L,1UL},{0x8BBB80A8L,1UL,0UL},{8UL,5UL,1UL},{0UL,0UL,0xEC2AEBECL},{8UL,0x934B9437L,0x934B9437L},{0x8BBB80A8L,0UL,0UL}}};
    uint16_t *l_663 = &g_364[0];
    int16_t l_711 = (-10L);
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_285[i] = 0x2001A86CL;
    for (g_159 = 0; (g_159 == (-17)); g_159 = safe_sub_func_int8_t_s_s(g_159, 5))
    { /* block id: 71 */
        int8_t l_175 = 8L;
        uint8_t *l_176 = (void*)0;
        uint32_t *l_185 = (void*)0;
        uint32_t *l_186 = (void*)0;
        uint32_t *l_187 = &g_188;
        int16_t *l_191 = &g_71;
        int32_t l_290 = (-10L);
        int32_t l_296 = (-7L);
        int32_t l_330 = (-1L);
        uint16_t l_331 = 0xB2E3L;
        uint64_t *l_363 = &g_122;
        int32_t l_375 = (-8L);
        int32_t l_376 = 1L;
        int32_t l_377 = (-1L);
        int32_t l_378 = (-1L);
        int32_t l_379 = (-2L);
        int32_t l_380 = (-2L);
        int32_t l_381[6];
        float l_382[8][6] = {{(-0x9.Fp+1),0x6.0CFFF7p+37,0x1.Dp-1,0x1.Dp-1,0x6.0CFFF7p+37,(-0x9.Fp+1)},{(-0x9.Fp+1),0x1.Dp-1,0x1.1p+1,0x6.0CFFF7p+37,0x6.0CFFF7p+37,0x1.1p+1},{0x6.0CFFF7p+37,0x6.0CFFF7p+37,0x1.1p+1,0x1.Dp-1,(-0x9.Fp+1),(-0x9.Fp+1)},{0x6.0CFFF7p+37,0x1.Dp-1,0x1.Dp-1,0x6.0CFFF7p+37,(-0x9.Fp+1),0x1.1p+1},{(-0x9.Fp+1),0x6.0CFFF7p+37,0x1.Dp-1,0x1.Dp-1,0x6.0CFFF7p+37,(-0x9.Fp+1)},{(-0x9.Fp+1),0x1.Dp-1,0x1.1p+1,0x6.0CFFF7p+37,0x6.0CFFF7p+37,0x1.1p+1},{0x6.0CFFF7p+37,0x6.0CFFF7p+37,0x1.1p+1,0x1.Dp-1,(-0x9.Fp+1),(-0x9.Fp+1)},{0x6.0CFFF7p+37,0x1.Dp-1,0x1.Dp-1,0x6.0CFFF7p+37,(-0x9.Fp+1),0x1.1p+1}};
        union U1 l_396 = {0xC87CBD62L};
        int i, j;
        for (i = 0; i < 6; i++)
            l_381[i] = (-4L);
        if ((((safe_add_func_int64_t_s_s(0x39E70C8CA251F1BFLL, l_175)) < (--g_177)) > (safe_rshift_func_int8_t_s_s((safe_mod_func_uint32_t_u_u((l_184 , ((*l_187) = l_184)), g_114)), 6))))
        { /* block id: 74 */
            uint16_t l_215[8][4][8] = {{{65527UL,65528UL,65535UL,65535UL,0xB0BAL,65526UL,65532UL,65535UL},{0xCE05L,65527UL,0x93CEL,65528UL,65535UL,0x3C57L,0x83A4L,65535UL},{0xD2F8L,65533UL,65527UL,65535UL,65535UL,65527UL,65533UL,0xD2F8L},{0x93CEL,65526UL,0x994BL,0xA565L,0xD2F8L,0x3A44L,0x0DCBL,0x3C57L}},{{0x0DCBL,0x83A4L,65530UL,6UL,0xA565L,0x3A44L,65535UL,65532UL},{1UL,65526UL,0xAD39L,0x5BA6L,1UL,0xA565L,0UL,65528UL},{65532UL,0xB0BAL,0x5BA6L,0x9A31L,0xCE05L,0UL,0xA565L,65535UL},{1UL,0xA565L,65532UL,0xD5C4L,0x5BA6L,0xD5C4L,65532UL,0xA565L}},{{0x3A44L,65532UL,65530UL,0x93CEL,65535UL,65533UL,1UL,8UL},{65530UL,65535UL,65526UL,0x3C57L,0x3A44L,0x994BL,1UL,65535UL},{0x93CEL,0x3C57L,65530UL,65533UL,6UL,65535UL,65532UL,0x8BEAL},{6UL,65535UL,65532UL,0x8BEAL,1UL,0x95C5L,0xA565L,65535UL}},{{0UL,65535UL,0x5BA6L,0UL,8UL,8UL,0UL,0x5BA6L},{0xD2F8L,0xD2F8L,0xAD39L,1UL,65535UL,0UL,65535UL,65535UL},{65535UL,0UL,65530UL,0x3438L,65535UL,0xCE05L,0x0DCBL,65535UL},{0UL,65528UL,0x994BL,1UL,65532UL,65532UL,0xB0BAL,0x5BA6L}},{{0x5BA6L,1UL,0xA565L,0UL,65528UL,0xD2F8L,0UL,65535UL},{0xB0BAL,65532UL,0UL,0x8BEAL,0x9A31L,65535UL,0x9A31L,0x8BEAL},{0x83A4L,65527UL,0x83A4L,65533UL,1UL,1UL,0x3438L,65535UL},{65528UL,0x8BEAL,0xD5C4L,0x3C57L,7UL,65526UL,1UL,8UL}},{{65528UL,0x5BA6L,0xD2F8L,0x93CEL,1UL,0x0DCBL,1UL,0xA565L},{0x83A4L,65530UL,0x95C5L,0xD5C4L,0x9A31L,65530UL,0x3C57L,65535UL},{0xB0BAL,0x994BL,0x93CEL,0x9A31L,65528UL,1UL,1UL,65528UL},{0x5BA6L,0x0DCBL,0x0DCBL,0x5BA6L,65532UL,0UL,65535UL,65532UL}},{{0UL,1UL,1UL,6UL,65535UL,0xAD39L,8UL,0x3C57L},{65535UL,1UL,0x3438L,0xA565L,65535UL,0UL,6UL,0xAD39L},{0xD2F8L,0x0DCBL,1UL,65527UL,8UL,1UL,0x95C5L,0UL},{1UL,65535UL,0UL,0x9A31L,7UL,1UL,0xCE05L,1UL}},{{0x93CEL,0x8BEAL,0x3438L,0x8BEAL,0x93CEL,0x5BA6L,65527UL,1UL},{0UL,65535UL,0xA565L,65535UL,65535UL,0xD5C4L,1UL,0x8BEAL},{1UL,8UL,0xA565L,65528UL,65532UL,7UL,65527UL,0x3438L},{65535UL,0xA565L,0x3438L,1UL,65535UL,65530UL,0xCE05L,0xB0BAL}}};
            int32_t l_246 = 0x09C307FEL;
            int64_t l_289 = 0L;
            uint16_t l_291 = 0x8A84L;
            int32_t l_294 = 0xF5A8E44CL;
            int32_t l_299[3][5][7] = {{{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L},{(-1L),0x633ADEF6L,0x77B13800L,0xEC2F750BL,0x5A282962L,0xEC2F750BL,0x77B13800L},{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L},{(-1L),0x633ADEF6L,0x77B13800L,0xEC2F750BL,0x5A282962L,0xEC2F750BL,0x77B13800L},{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L}},{{(-1L),0x633ADEF6L,0x77B13800L,0xEC2F750BL,0x5A282962L,0xEC2F750BL,0x77B13800L},{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L},{(-1L),0x633ADEF6L,0x77B13800L,0xEC2F750BL,0x5A282962L,0xEC2F750BL,0x77B13800L},{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L},{(-1L),0x633ADEF6L,0x77B13800L,0xEC2F750BL,0x5A282962L,0xEC2F750BL,0x77B13800L}},{{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L},{(-1L),0x633ADEF6L,0x77B13800L,0xEC2F750BL,0x5A282962L,0xEC2F750BL,0x77B13800L},{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L},{(-1L),0x633ADEF6L,0x77B13800L,0xEC2F750BL,0x5A282962L,0xEC2F750BL,0x77B13800L},{0xD732F0C6L,0xD732F0C6L,0x28C6EC92L,0x5881AEEFL,0x68DB9444L,0x5881AEEFL,0x28C6EC92L}}};
            uint16_t l_301[9][10] = {{0x6825L,0x3F8BL,0x6825L,1UL,0x4361L,65533UL,0xA809L,65533UL,0x4361L,1UL},{0x13EEL,0x3F8BL,0x13EEL,1UL,0UL,65533UL,0x3690L,65533UL,0UL,1UL},{0x6825L,0x3F8BL,0x6825L,1UL,0x4361L,65533UL,0xA809L,65533UL,0x4361L,1UL},{0x13EEL,0x3F8BL,0x13EEL,1UL,0UL,65533UL,0x3690L,65533UL,0UL,1UL},{0x6825L,0x3F8BL,0x6825L,1UL,0x4361L,0x5B0AL,0x0351L,0x5B0AL,0x6825L,65533UL},{65535UL,0xF6F6L,65535UL,65533UL,0x13EEL,0x5B0AL,0xE2F2L,0x5B0AL,0x13EEL,65533UL},{1UL,0xF6F6L,1UL,65533UL,0x6825L,0x5B0AL,0x0351L,0x5B0AL,0x6825L,65533UL},{65535UL,0xF6F6L,65535UL,65533UL,0x13EEL,0x5B0AL,0xE2F2L,0x5B0AL,0x13EEL,65533UL},{1UL,0xF6F6L,1UL,65533UL,0x6825L,0x5B0AL,0x0351L,0x5B0AL,0x6825L,65533UL}};
            uint64_t l_386 = 0xAA616FBC5D3EBFB3LL;
            int i, j, k;
            for (g_127 = 17; (g_127 == (-14)); g_127 = safe_sub_func_uint8_t_u_u(g_127, 9))
            { /* block id: 77 */
                return l_191;
            }
            for (l_184 = (-14); (l_184 < 1); l_184 = safe_add_func_uint16_t_u_u(l_184, 9))
            { /* block id: 82 */
                union U1 * const l_209 = &g_25;
                int32_t l_210 = 0x6DD1E676L;
                float *l_279 = &g_123;
                int32_t l_300 = 5L;
                int32_t l_329[9][6][4] = {{{0x44F0B4FEL,1L,2L,(-2L)},{1L,(-1L),0x10879E1DL,(-1L)},{0L,0x44F0B4FEL,0x8CF15F90L,0x78523272L},{0xB53181D6L,0x24D7A882L,1L,2L},{2L,(-1L),0x78523272L,0x3828780DL},{2L,0x21E31B4DL,1L,0L}},{{0xB53181D6L,0x3828780DL,0x8CF15F90L,0x0651DE6FL},{0L,0x4B3ED0F1L,0x10879E1DL,0x8007EA7FL},{1L,0xA848C921L,2L,3L},{0x44F0B4FEL,0xB799F499L,1L,0xE7065C46L},{0x10879E1DL,0xABB87969L,0x44F0B4FEL,0x44F0B4FEL},{(-3L),(-3L),1L,(-1L)}},{{0xB799F499L,0L,0xABB87969L,1L},{0x24D7A882L,3L,0x9B74890EL,0xABB87969L},{1L,3L,0x8007EA7FL,1L},{3L,0L,0x593AB50BL,(-1L)},{0x78523272L,(-3L),0L,0x44F0B4FEL},{1L,0xABB87969L,(-3L),0xE7065C46L}},{{0x21E31B4DL,0xB799F499L,(-2L),3L},{0x8CF15F90L,0xA848C921L,4L,0x8007EA7FL},{0x3828780DL,0x4B3ED0F1L,0x3828780DL,0x0651DE6FL},{0x64A9CFC3L,0x3828780DL,0xED34DC58L,0L},{0x8007EA7FL,0x21E31B4DL,1L,0x3828780DL},{1L,(-1L),1L,2L}},{{0x8007EA7FL,0x24D7A882L,0xED34DC58L,0x78523272L},{0x64A9CFC3L,0x44F0B4FEL,0x3828780DL,(-1L)},{0x3828780DL,(-1L),4L,(-2L)},{0x8CF15F90L,1L,(-2L),1L},{0x21E31B4DL,4L,(-3L),0xED34DC58L},{1L,0L,0L,1L}},{{0x78523272L,1L,0x593AB50BL,0x4B3ED0F1L},{3L,1L,0x8007EA7FL,0xB53181D6L},{1L,1L,0x9B74890EL,0xB53181D6L},{0x24D7A882L,1L,0xABB87969L,0x4B3ED0F1L},{0xB799F499L,1L,1L,1L},{(-3L),0L,0x44F0B4FEL,0xED34DC58L}},{{0x10879E1DL,4L,0x21E31B4DL,0x8007EA7FL},{1L,0x78523272L,0x8CF15F90L,0x44F0B4FEL},{2L,(-2L),0x3828780DL,(-2L)},{0x593AB50BL,1L,0x64A9CFC3L,1L},{0xE7065C46L,0x0651DE6FL,0x8007EA7FL,0x8CF15F90L},{0x8CF15F90L,0x174530BEL,1L,0L}},{{0x8CF15F90L,1L,0x8007EA7FL,0x2C8D41C1L},{0xE7065C46L,0L,0x64A9CFC3L,0xABB87969L},{0x593AB50BL,0xA848C921L,0x3828780DL,0x24D7A882L},{2L,0L,0x8CF15F90L,4L},{1L,(-1L),0x21E31B4DL,(-1L)},{0x3828780DL,3L,1L,1L}},{{0x10879E1DL,0x10879E1DL,0x78523272L,0x174530BEL},{(-1L),0x2C8D41C1L,3L,2L},{0x0651DE6FL,4L,1L,3L},{0x4B3ED0F1L,4L,0x24D7A882L,2L},{4L,0x2C8D41C1L,0xB799F499L,0x174530BEL},{1L,0x10879E1DL,(-3L),1L}}};
                int64_t l_385[7] = {(-1L),0x149473950678CA93LL,(-1L),(-1L),0x149473950678CA93LL,(-1L),(-1L)};
                int i, j, k;
                for (g_122 = 0; (g_122 >= 40); g_122 = safe_add_func_uint64_t_u_u(g_122, 6))
                { /* block id: 85 */
                    union U1 l_198 = {0x8D2EE02EL};
                    const int16_t *l_199 = &g_71;
                    for (g_73 = 0; (g_73 <= 5); g_73 += 1)
                    { /* block id: 88 */
                        int16_t **l_200[4];
                        int32_t l_204 = 5L;
                        int32_t *l_211 = (void*)0;
                        int32_t *l_212 = &g_120[3];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_200[i] = &l_191;
                        (*l_212) = (safe_add_func_uint64_t_u_u((l_198 , ((l_199 != (g_201[0] = &g_71)) < (safe_mul_func_int16_t_s_s(((*p_49) >= l_204), ((safe_div_func_uint32_t_u_u(0x788E8F36L, (safe_mod_func_uint64_t_u_u(((g_71 , (void*)0) == l_209), l_210)))) ^ g_71))))), l_210));
                    }
                }
                for (g_73 = 0; (g_73 <= 5); g_73 += 1)
                { /* block id: 95 */
                    uint64_t l_225 = 0x5583D22D4EF8352ALL;
                    float l_245 = 0xE.3F2653p+32;
                    uint8_t l_274 = 0xAAL;
                    for (g_161 = 0; (g_161 <= 0); g_161 += 1)
                    { /* block id: 98 */
                        int32_t *l_216 = &g_127;
                        int16_t *l_223[10][4] = {{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_224},{(void*)0,(void*)0,(void*)0,&g_224},{(void*)0,&g_224,&g_224,&g_224},{(void*)0,(void*)0,&g_224,&g_224},{&g_224,(void*)0,&g_224,(void*)0},{(void*)0,(void*)0,&g_224,&g_224},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_224},{(void*)0,(void*)0,(void*)0,&g_224}};
                        int16_t **l_244[8][8][4] = {{{(void*)0,&l_223[1][1],&l_223[1][1],(void*)0},{&g_201[0],&l_223[4][1],&l_191,&g_201[0]},{(void*)0,&l_223[4][1],&l_191,(void*)0},{&g_201[0],&l_223[1][1],&l_191,(void*)0},{&g_201[0],&l_223[4][1],&l_223[1][1],&g_201[0]},{&g_201[0],&l_223[4][1],&l_191,(void*)0},{(void*)0,&l_223[1][1],&l_223[1][1],(void*)0},{&g_201[0],&l_223[4][1],&l_191,&g_201[0]}},{{(void*)0,&l_223[4][1],&l_191,(void*)0},{&g_201[0],&l_223[1][1],&l_191,(void*)0},{&g_201[0],&l_223[4][1],&l_223[1][1],&g_201[0]},{&g_201[0],&l_223[4][1],&l_191,(void*)0},{(void*)0,&l_223[1][1],&l_223[1][1],(void*)0},{&g_201[0],&l_223[4][1],&l_191,&g_201[0]},{(void*)0,&l_223[4][1],&l_191,(void*)0},{&g_201[0],&l_223[1][1],&l_191,(void*)0}},{{&g_201[0],&l_223[4][1],&l_223[1][1],&g_201[0]},{&g_201[0],&l_223[4][1],&l_191,(void*)0},{(void*)0,&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&l_223[4][1],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_191,&g_201[0]}},{{&g_201[0],&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&l_223[4][1],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_191,&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]}},{{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&l_223[4][1],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_191,&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]}},{{&l_223[4][1],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_191,&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&l_223[4][1],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_191,&g_201[0]}},{{&g_201[0],&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&l_223[4][1],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_191,&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]}},{{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&l_223[4][1],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_191,&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]},{&g_201[0],&l_223[1][1],&l_191,&l_223[4][1]},{&g_201[0],&l_223[1][1],&l_223[4][1],&g_201[0]},{&g_201[0],&l_191,&l_191,&g_201[0]}}};
                        int16_t ***l_243 = &l_244[2][7][2];
                        uint8_t *l_247 = &g_248;
                        float *l_276 = &g_123;
                        int i, j, k;
                        (*l_216) &= (safe_lshift_func_int8_t_s_u(l_215[7][1][4], l_210));
                        if (l_184)
                            continue;
                        (*l_216) = (safe_div_func_uint8_t_u_u(((*l_247) = (safe_rshift_func_int16_t_s_u((safe_div_func_uint16_t_u_u(((l_225 = ((*l_191) = 0x33CDL)) , (safe_rshift_func_int16_t_s_s((((l_246 = ((safe_mul_func_int16_t_s_s(((*p_49) = (safe_div_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_u((safe_sub_func_int64_t_s_s(l_225, (l_184 , (l_236 != (void*)0)))), l_210)) , ((safe_add_func_int32_t_s_s((safe_mul_func_uint8_t_u_u((~g_224), (!l_184))), ((l_210 = (((((*l_243) = (void*)0) == (void*)0) > g_73) != l_215[5][0][7])) != l_184))) ^ g_25.f0)), 0x16AD06E79B7C538DLL))), g_3)) >= l_175)) , l_184) , l_184), l_184))), g_166)), g_224))), g_120[4]));
                        (*l_276) = (safe_add_func_float_f_f((((((safe_rshift_func_int16_t_s_s(((l_253 != &g_167) < (safe_lshift_func_uint8_t_u_u((((*l_187) |= (safe_rshift_func_int8_t_s_s(0xD7L, 7))) , (((safe_div_func_uint32_t_u_u(((safe_mul_func_int16_t_s_s(((((void*)0 == &g_248) >= (safe_mul_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u(3UL, (l_268 == (safe_lshift_func_int16_t_s_s(((~((safe_mul_func_uint16_t_u_u((((((&g_167 == &g_167) & l_274) < 0xBD3B7CBDB25185A7LL) & 0x68L) | 0xE32586A3L), l_215[7][1][5])) < 0xB0985F9BD9F5654ALL)) > l_210), (*p_49)))))), l_215[3][0][5]))) ^ l_215[0][0][1]), 0UL)) || 18446744073709551615UL), l_274)) && g_73) , l_175)), l_274))), 6)) > 0xC80609C47C9F8D51LL) , (*l_216)) == g_275) != g_248), 0x4.D2B49Cp+25));
                    }
                }
                (*l_279) = (safe_div_func_float_f_f(0x8.687B3Fp+6, 0x1.9p-1));
                for (g_224 = 3; (g_224 >= 0); g_224 -= 1)
                { /* block id: 116 */
                    int64_t l_287 = 0x0539E8B652B0E125LL;
                    int32_t l_288 = 1L;
                    int32_t l_295 = 0x1C9156F2L;
                    int32_t l_297 = 0xD4DD8C49L;
                    int32_t l_298[10][1] = {{0xEF291634L},{0L},{0L},{0xEF291634L},{0L},{0L},{0xEF291634L},{0L},{0xEF291634L},{1L}};
                    union U1 l_306[3] = {{-9L},{-9L},{-9L}};
                    uint8_t l_334 = 1UL;
                    int16_t l_383 = 0x7467L;
                    int i, j;
                    for (g_71 = 0; (g_71 <= 1); g_71 += 1)
                    { /* block id: 119 */
                        int32_t *l_282 = &g_3;
                        int32_t *l_283 = (void*)0;
                        int32_t *l_284 = &g_120[3];
                        int32_t *l_286[9][2] = {{&l_285[0],&l_285[0]},{&l_246,&l_285[0]},{&l_285[0],&l_246},{&l_285[0],&l_285[0]},{&l_246,&l_285[0]},{&l_285[0],&l_246},{&l_285[0],&l_285[0]},{&l_246,&l_285[0]},{&l_285[0],&l_246}};
                        int i, j;
                        g_120[g_224] |= (safe_lshift_func_int16_t_s_s(0x0819L, 2));
                        ++l_291;
                        l_301[5][3]++;
                        l_285[0] = (safe_unary_minus_func_int16_t_s((+(((l_306[2] , &g_120[(g_224 + 1)]) == (void*)0) != (g_120[g_224] && (l_288 &= 1UL))))));
                    }
                    if (g_120[(g_224 + 1)])
                    { /* block id: 126 */
                        int32_t *l_308 = &l_298[8][0];
                        int32_t **l_307 = &l_308;
                        int i;
                        if (g_120[(g_224 + 1)])
                            break;
                        (*l_307) = &l_210;
                    }
                    else
                    { /* block id: 129 */
                        int32_t *l_309 = (void*)0;
                        int32_t *l_310 = &g_120[(g_224 + 1)];
                        int32_t *l_311 = (void*)0;
                        int32_t *l_312 = (void*)0;
                        int32_t *l_313 = &l_290;
                        int32_t *l_314 = &l_299[0][2][5];
                        int32_t *l_315 = &g_127;
                        int32_t *l_316 = &g_120[(g_224 + 1)];
                        int32_t *l_317 = &l_210;
                        int32_t *l_318 = &l_285[0];
                        int32_t *l_319 = &g_120[(g_224 + 1)];
                        int32_t l_320[7];
                        int32_t *l_321 = (void*)0;
                        int32_t *l_322 = &l_297;
                        int32_t *l_323 = &l_298[8][0];
                        int32_t *l_324 = (void*)0;
                        int32_t *l_325 = &l_300;
                        int32_t *l_326 = &l_320[5];
                        int32_t *l_327 = &l_299[0][2][5];
                        int32_t *l_328[2];
                        int i;
                        for (i = 0; i < 7; i++)
                            l_320[i] = 0x069F1A1AL;
                        for (i = 0; i < 2; i++)
                            l_328[i] = &g_120[3];
                        l_331++;
                        if (l_287)
                            break;
                        (*l_322) = 0xD2EC96DFL;
                        --l_334;
                    }
                    for (g_73 = 0; (g_73 <= 0); g_73 += 1)
                    { /* block id: 137 */
                        uint64_t *l_341 = &g_122;
                        float *l_365 = &g_366[0][2][0];
                        uint8_t *l_368 = &g_248;
                        int32_t *l_369 = &g_120[3];
                        int32_t *l_370 = &g_127;
                        int32_t *l_371 = &l_285[0];
                        int32_t *l_372[2][8] = {{&l_285[0],(void*)0,&l_285[0],(void*)0,(void*)0,&l_285[0],(void*)0,&l_285[0]},{&g_120[(g_224 + 1)],(void*)0,(void*)0,(void*)0,&g_120[(g_224 + 1)],&g_120[(g_224 + 1)],(void*)0,(void*)0}};
                        int64_t l_373 = 0x65CC771989D66A2FLL;
                        int i, j, k;
                        (*l_369) |= ((l_298[g_224][g_73] ^ (safe_lshift_func_uint8_t_u_s(((*l_368) = (safe_rshift_func_int16_t_s_u((((++(*l_341)) || (safe_mod_func_int8_t_s_s(l_329[g_224][(g_73 + 5)][(g_73 + 3)], ((g_367 = ((safe_sub_func_float_f_f((l_296 = ((safe_mul_func_float_f_f(((((*l_279) = l_215[2][3][3]) > (safe_mul_func_float_f_f((!(safe_add_func_float_f_f(((g_71 , 0x0.Ap-1) != (safe_sub_func_float_f_f(((l_268 <= ((((*l_365) = (safe_div_func_float_f_f(((((g_364[0] = (safe_mul_func_float_f_f((((safe_div_func_float_f_f(((void*)0 != l_363), (l_294 < l_329[8][4][2]))) > l_329[g_224][(g_73 + 5)][(g_73 + 3)]) > 0xE.952917p-32), (-0x2.1p-1)))) <= 0x4.3BFFD3p-26) < l_210) > l_285[0]), l_288))) > g_224) == 0x8.B81C6Bp+1)) != g_114), l_298[g_224][g_73]))), l_175))), g_188))) <= 0x2.BCE109p-58), g_7)) <= 0x1.A0C521p-56)), l_331)) , 0x1.E25683p-92)) , g_122)))) <= l_329[g_224][(g_73 + 5)][(g_73 + 3)]), 11))), g_275))) > l_330);
                        --l_386;
                    }
                }
            }
            g_389 = &g_127;
        }
        else
        { /* block id: 151 */
            int64_t ***l_397 = (void*)0;
            int64_t ***l_398 = (void*)0;
            int64_t **l_400 = &l_255[0][1];
            int64_t ***l_399 = &l_400;
            int8_t *l_411 = &l_175;
            int32_t l_414[2];
            uint16_t *l_415 = &g_364[3];
            int32_t *l_416 = &l_377;
            int i;
            for (i = 0; i < 2; i++)
                l_414[i] = 1L;
            (*l_416) |= (safe_div_func_int8_t_s_s(g_224, ((((safe_rshift_func_uint16_t_u_u((8L ^ (safe_sub_func_uint16_t_u_u((((((((*l_399) = ((*l_253) = (l_396 , (*l_253)))) == &l_255[0][1]) & (((*g_389) = (((((((safe_add_func_int16_t_s_s(0xB666L, ((safe_rshift_func_uint16_t_u_s(((*l_415) |= (safe_rshift_func_int16_t_s_u((safe_rshift_func_uint16_t_u_u(((g_122 |= (safe_mul_func_int8_t_s_s(((*l_411) = g_71), (safe_sub_func_uint32_t_u_u(4294967290UL, (l_285[0] || g_177)))))) < g_120[3]), 12)), l_414[1]))), l_268)) , l_285[0]))) < l_285[0]) >= l_184) ^ 255UL) == l_378) <= g_374) && 0x1CF6L)) != l_381[3])) > 0x987BL) , l_414[1]) , l_414[0]), l_381[5]))), 0)) != 0xC473912ADB7F4CA5LL) == l_285[0]) || l_268)));
        }
    }
    for (g_248 = (-21); (g_248 < 14); g_248 = safe_add_func_uint64_t_u_u(g_248, 7))
    { /* block id: 163 */
        int16_t l_433[10][7] = {{0x1932L,0x1D80L,7L,(-10L),7L,0x1D80L,0x1932L},{1L,0x3484L,1L,1L,0x3484L,1L,1L},{0x1932L,(-10L),0x0C5CL,(-10L),0x1932L,5L,0x1932L},{0x3484L,1L,1L,0x3484L,1L,1L,0x3484L},{7L,(-10L),7L,0x1D80L,0x1932L,0x1D80L,7L},{0x3484L,0x3484L,(-1L),0x3484L,0x3484L,(-1L),0x3484L},{0x1932L,0x1D80L,7L,(-10L),7L,0x1D80L,0x1932L},{1L,0x3484L,1L,1L,0x3484L,1L,1L},{0x1932L,(-10L),0x0C5CL,(-10L),0x1932L,5L,0x1932L},{0x3484L,1L,1L,0x3484L,1L,1L,0x3484L}};
        union U1 *l_438 = &l_426;
        union U1 *l_451 = &g_25;
        union U1 **l_450 = &l_451;
        union U1 **l_452 = &l_451;
        int32_t l_463 = 0xBF755DEFL;
        int i, j;
        for (g_122 = 8; (g_122 >= 33); g_122++)
        { /* block id: 166 */
            int32_t *l_421 = &g_120[3];
            int32_t **l_422 = &g_389;
            union U1 **l_455 = (void*)0;
            const int64_t *l_461[4][9] = {{&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114},{&g_114,&g_166,&g_114,&g_114,&g_114,&g_114,&g_166,&g_114,&g_114},{&g_166,&g_114,&g_114,&g_166,&g_166,&g_166,&g_114,&g_114,&g_166},{&g_166,&g_114,&g_114,&g_114,&g_166,&g_166,&g_114,&g_114,&g_114}};
            const int64_t **l_460 = &l_461[2][3];
            const int64_t ***l_459 = &l_460;
            int i, j;
            (*l_422) = l_421;
            (*l_422) = ((&l_421 != (((((safe_unary_minus_func_int16_t_s(((((void*)0 == &g_114) && (g_364[6] , ((l_426 , (safe_div_func_uint64_t_u_u(((-1L) & (safe_mul_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u(l_433[6][4], (((safe_div_func_uint32_t_u_u((((**l_253) = (void*)0) == (void*)0), (*g_389))) , (*g_389)) <= (*g_389)))) & l_433[6][4]), 0xAFL))), 0xA8BB5C44C4A7A9D3LL))) > 0x22L))) & 0x3385BD0553D8E2ACLL))) > l_268) , (void*)0) == p_49) , (void*)0)) , (*l_422));
            for (g_224 = 10; (g_224 != 12); ++g_224)
            { /* block id: 172 */
                union U1 **l_439 = &g_77;
                union U1 ***l_446 = &g_76[4];
                union U1 ***l_447 = &g_76[1];
                union U1 ***l_448 = &g_76[1];
                union U1 ***l_449[2][2][3];
                int32_t l_462 = 0xB5960D21L;
                int i, j, k;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 2; j++)
                    {
                        for (k = 0; k < 3; k++)
                            l_449[i][j][k] = &g_76[1];
                    }
                }
                if (l_433[6][2])
                    break;
                l_463 = ((&l_426 == ((*l_439) = l_438)) & (safe_lshift_func_int16_t_s_s(((*p_49) = ((((((safe_mul_func_float_f_f(((((g_456[1] = ((**l_422) && ((l_453 = (l_433[0][3] , (l_452 = (l_450 = l_439)))) != l_455))) , (safe_sub_func_int32_t_s_s(l_433[0][6], (&g_167 == l_459)))) , (**l_422)) == 0x4.57B6CDp-97), g_177)) == 0x3.3C0C11p+28) != l_285[0]) > l_462) , (**l_422)) , 0x1194L)), l_433[6][6])));
                if (l_285[0])
                    continue;
            }
        }
    }
    for (g_275 = 0; (g_275 < 9); ++g_275)
    { /* block id: 187 */
        float l_468 = 0x6.3AB048p-83;
        int32_t l_482 = 1L;
        int32_t l_515 = 0L;
        int8_t *l_521 = (void*)0;
        union U1 ***l_531 = &l_453;
        float l_588 = 0x5.A34AB5p+93;
        uint16_t l_613 = 0x1741L;
        int16_t l_651[5][7][2] = {{{0x7B92L,0L},{0x09FCL,2L},{1L,2L},{0x09FCL,0L},{0x7B92L,(-9L)},{(-1L),(-1L)},{0L,1L}},{{0xA1D7L,0x746BL},{7L,0x0AB6L},{0x32ABL,7L},{0xFA35L,0x09FCL},{0xFA35L,7L},{0x32ABL,0x0AB6L},{7L,0x746BL}},{{0xA1D7L,1L},{0L,(-1L)},{(-1L),(-9L)},{0x7B92L,0L},{0x09FCL,2L},{1L,2L},{0x09FCL,0L}},{{0x7B92L,(-9L)},{(-1L),(-1L)},{(-9L),0x7B92L},{1L,2L},{1L,(-1L)},{7L,1L},{1L,0x0AB6L}},{{1L,1L},{7L,(-1L)},{1L,2L},{1L,0x7B92L},{(-9L),0xFA35L},{0xFA35L,2L},{0xA1D7L,(-9L)}}};
        int16_t *l_736 = (void*)0;
        int i, j, k;
    }
    return p_49;
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_71 g_7 g_114 g_120 g_3 g_127 g_25.f0 g_122
 * writes: g_73 g_114 g_120 g_122 g_3 g_127 g_159 g_161 g_71 g_167
 */
static int16_t * func_50(int32_t * p_51, int32_t  p_52)
{ /* block id: 42 */
    union U1 l_101 = {0x9BBF73BCL};
    int32_t l_118[1];
    int64_t *l_119 = &g_114;
    int32_t *l_170 = (void*)0;
    int32_t **l_169 = &l_170;
    int i;
    for (i = 0; i < 1; i++)
        l_118[i] = (-5L);
    for (g_73 = 15; (g_73 < 10); g_73 = safe_sub_func_int16_t_s_s(g_73, 1))
    { /* block id: 45 */
        const uint16_t l_94[4][1][3] = {{{3UL,0x0E04L,3UL}},{{0UL,0UL,0UL}},{{3UL,0x0E04L,3UL}},{{0UL,0UL,0UL}}};
        int16_t *l_105[7] = {&g_71,&g_71,&g_71,&g_71,&g_71,&g_71,&g_71};
        int16_t **l_104 = &l_105[2];
        int64_t *l_113[3][8] = {{(void*)0,(void*)0,&g_114,(void*)0,&g_114,(void*)0,(void*)0,&g_114},{&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114,&g_114},{&g_114,&g_114,(void*)0,(void*)0,&g_114,&g_114,&g_114,(void*)0}};
        int32_t l_115 = 7L;
        uint64_t *l_121 = &g_122;
        int32_t *l_124 = (void*)0;
        int32_t *l_125 = (void*)0;
        int32_t *l_126 = &g_127;
        int8_t *l_158 = &g_159;
        int8_t *l_160 = &g_161;
        int64_t * const l_165 = &g_166;
        int64_t * const *l_164 = &l_165;
        int64_t * const **l_163 = &l_164;
        int i, j, k;
        if (l_94[3][0][0])
            break;
        (*l_126) ^= (safe_add_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s((safe_sub_func_int8_t_s_s((l_101 , (safe_rshift_func_int8_t_s_s(((void*)0 != l_104), 2))), (+(safe_sub_func_int32_t_s_s(((*p_51) = (safe_mod_func_uint64_t_u_u(((*l_121) = ((safe_sub_func_int64_t_s_s((l_94[3][0][0] <= g_71), (g_120[3] |= ((*l_119) &= (0xE8FFC0A99849AA35LL < ((l_115 &= p_52) | (((p_52 & (l_118[0] = ((((safe_div_func_uint32_t_u_u((l_94[3][0][0] & 0x22L), 0x44B22B5DL)) | g_7) , (-6L)) ^ l_94[3][0][0]))) , l_119) == l_119))))))) | g_3)), g_73))), l_94[2][0][0]))))), 0)), 0xBAEE16EA25DA6338LL));
        if (((safe_lshift_func_uint16_t_u_u(((g_71 = (((safe_unary_minus_func_uint16_t_u(g_7)) >= (safe_sub_func_int8_t_s_s(g_7, (~(((void*)0 != l_119) == (safe_rshift_func_uint8_t_u_u((&g_120[3] != ((safe_mod_func_uint8_t_u_u(g_120[2], (safe_mul_func_int8_t_s_s((safe_add_func_int32_t_s_s((safe_add_func_int16_t_s_s((l_118[0] ^= p_52), (safe_sub_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_u(((((*l_160) = ((safe_div_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_u(((~(((+p_52) != (((*l_158) = (safe_rshift_func_int16_t_s_u((safe_rshift_func_int16_t_s_u(g_25.f0, g_3)), 5))) <= (-1L))) <= g_3)) | (-1L)), g_122)) == p_52), 0xBB384EBA52E2A0ADLL)) || g_127)) >= p_52) >= 1L), l_101.f0)) , 7UL), p_52)))), g_120[4])), p_52)))) , &g_120[3])), p_52))))))) != p_52)) , p_52), 2)) & p_52))
        { /* block id: 58 */
            if (g_120[3])
                break;
        }
        else
        { /* block id: 60 */
            int16_t *l_162 = &g_71;
            return l_162;
        }
        g_167 = ((*l_163) = &l_113[0][0]);
    }
    (*l_169) = &g_3;
    return &g_71;
}


/* ------------------------------------------ */
/* 
 * reads : g_71 g_3
 * writes: g_3 g_73 g_76 g_71
 */
static int64_t  func_53(const uint32_t  p_54)
{ /* block id: 7 */
    union U1 *l_57 = &g_25;
    union U1 **l_58 = &l_57;
    uint32_t *l_69 = &g_7;
    int32_t l_81 = 0x0F7F7A37L;
lbl_84:
    (*l_58) = l_57;
    for (g_3 = 0; (g_3 <= (-1)); g_3 = safe_sub_func_uint32_t_u_u(g_3, 3))
    { /* block id: 11 */
        int16_t *l_70 = &g_71;
        int16_t **l_72 = &l_70;
        union U1 **l_74 = &l_57;
        union U1 ***l_75[8][4][6] = {{{&l_58,&l_58,&l_74,&l_58,&l_58,&l_74},{&l_58,&l_58,&l_58,&l_58,&l_58,&l_58},{(void*)0,&l_58,&l_74,&l_58,&l_58,&l_74},{&l_58,&l_58,&l_74,&l_58,&l_58,&l_74}},{{&l_74,&l_58,&l_74,&l_74,&l_74,&l_58},{&l_58,&l_58,&l_58,&l_58,&l_58,&l_74},{&l_74,(void*)0,&l_74,(void*)0,&l_58,&l_58},{&l_58,&l_58,&l_58,&l_74,(void*)0,&l_58}},{{(void*)0,&l_58,&l_74,&l_74,&l_58,&l_74},{&l_58,&l_74,&l_74,&l_58,&l_74,&l_58},{&l_74,&l_58,(void*)0,&l_58,&l_74,&l_74},{&l_74,&l_58,&l_58,&l_74,&l_74,&l_58}},{{&l_74,&l_58,(void*)0,&l_74,&l_58,&l_58},{&l_58,&l_74,&l_74,&l_74,&l_58,&l_58},{&l_74,&l_58,&l_74,(void*)0,&l_74,&l_74},{(void*)0,&l_58,&l_58,&l_58,&l_74,&l_74}},{{&l_58,&l_58,&l_74,&l_58,&l_74,&l_58},{&l_58,&l_74,&l_74,&l_74,&l_58,&l_58},{(void*)0,&l_58,&l_58,&l_58,(void*)0,&l_74},{&l_74,&l_58,&l_58,(void*)0,&l_58,&l_74}},{{&l_58,(void*)0,(void*)0,&l_74,(void*)0,&l_74},{&l_74,(void*)0,&l_74,(void*)0,&l_58,(void*)0},{&l_74,&l_74,&l_74,&l_74,&l_74,&l_74},{&l_58,&l_58,&l_58,&l_74,&l_58,(void*)0}},{{&l_74,&l_58,&l_58,(void*)0,&l_74,&l_58},{&l_74,&l_74,&l_58,&l_74,&l_74,&l_74},{&l_74,&l_58,(void*)0,&l_58,&l_58,&l_58},{&l_58,(void*)0,&l_74,(void*)0,(void*)0,&l_58}},{{&l_58,&l_58,&l_74,&l_74,&l_74,&l_58},{&l_74,&l_58,&l_74,(void*)0,&l_58,&l_74},{&l_58,&l_74,&l_58,&l_74,&l_74,&l_58},{&l_58,&l_58,(void*)0,&l_58,&l_58,&l_74}}};
        int32_t l_80 = (-5L);
        int32_t *l_90 = &l_80;
        int32_t *l_91 = &l_80;
        int i, j, k;
        l_81 = ((safe_sub_func_float_f_f((safe_sub_func_float_f_f(func_65(l_69, (((*l_72) = l_70) == (void*)0), ((void*)0 == l_69)), ((g_76[1] = l_74) == &g_77))), (safe_add_func_float_f_f(l_80, g_71)))) <= 0xE.CDE22Fp-32);
        for (l_81 = 0; (l_81 < 3); l_81++)
        { /* block id: 20 */
            int32_t **l_87 = (void*)0;
            int32_t *l_89 = (void*)0;
            int32_t **l_88[7][6] = {{&l_89,&l_89,&l_89,&l_89,&l_89,&l_89},{(void*)0,&l_89,&l_89,&l_89,(void*)0,(void*)0},{&l_89,&l_89,&l_89,&l_89,&l_89,&l_89},{&l_89,&l_89,&l_89,&l_89,&l_89,&l_89},{(void*)0,(void*)0,&l_89,&l_89,&l_89,(void*)0},{&l_89,&l_89,&l_89,&l_89,&l_89,&l_89},{(void*)0,&l_89,&l_89,&l_89,(void*)0,(void*)0}};
            int i, j;
            for (g_73 = 0; (g_73 <= 5); g_73 += 1)
            { /* block id: 23 */
                float l_85 = 0x0.3p-1;
                int32_t l_86 = (-6L);
                if (l_80)
                    goto lbl_84;
                for (g_71 = 3; (g_71 >= 0); g_71 -= 1)
                { /* block id: 27 */
                    for (l_80 = 0; (l_80 <= 5); l_80 += 1)
                    { /* block id: 30 */
                        return g_71;
                    }
                }
                return l_86;
            }
            l_91 = (l_90 = &g_3);
        }
        if (g_3)
            continue;
    }
    return l_81;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_73
 */
static float  func_65(uint32_t * p_66, int64_t  p_67, int8_t  p_68)
{ /* block id: 13 */
    g_73 = 0x385FAE11L;
    return p_67;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc_bytes (&g_5, sizeof(g_5), "g_5", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_25.f0, "g_25.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_39[i], "g_39[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_71, "g_71", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_120[i], "g_120[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_122, "g_122", print_hash_value);
    transparent_crc_bytes (&g_123, sizeof(g_123), "g_123", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_159, "g_159", print_hash_value);
    transparent_crc(g_161, "g_161", print_hash_value);
    transparent_crc(g_166, "g_166", print_hash_value);
    transparent_crc(g_177, "g_177", print_hash_value);
    transparent_crc(g_188, "g_188", print_hash_value);
    transparent_crc(g_224, "g_224", print_hash_value);
    transparent_crc(g_248, "g_248", print_hash_value);
    transparent_crc(g_275, "g_275", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_364[i], "g_364[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc_bytes(&g_366[i][j][k], sizeof(g_366[i][j][k]), "g_366[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_367, "g_367", print_hash_value);
    transparent_crc(g_374, "g_374", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_456[i], "g_456[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_512, "g_512", print_hash_value);
    transparent_crc(g_540, "g_540", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_569[i][j], "g_569[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_642[i], "g_642[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_646, "g_646", print_hash_value);
    transparent_crc(g_685, "g_685", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_718[i].f0, "g_718[i].f0", print_hash_value);
        transparent_crc(g_718[i].f1, "g_718[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_723.f0, "g_723.f0", print_hash_value);
    transparent_crc(g_723.f1, "g_723.f1", print_hash_value);
    transparent_crc(g_801, "g_801", print_hash_value);
    transparent_crc(g_808, "g_808", print_hash_value);
    transparent_crc(g_817, "g_817", print_hash_value);
    transparent_crc(g_929.f0, "g_929.f0", print_hash_value);
    transparent_crc(g_929.f1, "g_929.f1", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1017[i], "g_1017[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1061, "g_1061", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1108[i], "g_1108[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1236, "g_1236", print_hash_value);
    transparent_crc(g_1256, "g_1256", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1326[i], "g_1326[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1472, "g_1472", print_hash_value);
    transparent_crc_bytes (&g_1545, sizeof(g_1545), "g_1545", print_hash_value);
    transparent_crc_bytes (&g_1547, sizeof(g_1547), "g_1547", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 409
XXX total union variables: 19

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 162
   depth: 2, occurrence: 48
   depth: 3, occurrence: 1
   depth: 4, occurrence: 2
   depth: 6, occurrence: 2
   depth: 9, occurrence: 1
   depth: 11, occurrence: 1
   depth: 13, occurrence: 3
   depth: 15, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 4
   depth: 23, occurrence: 1
   depth: 24, occurrence: 3
   depth: 25, occurrence: 1
   depth: 28, occurrence: 1
   depth: 30, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 2
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 41, occurrence: 1

XXX total number of pointers: 355

XXX times a variable address is taken: 793
XXX times a pointer is dereferenced on RHS: 129
breakdown:
   depth: 1, occurrence: 113
   depth: 2, occurrence: 16
XXX times a pointer is dereferenced on LHS: 178
breakdown:
   depth: 1, occurrence: 168
   depth: 2, occurrence: 10
XXX times a pointer is compared with null: 37
XXX times a pointer is compared with address of another variable: 3
XXX times a pointer is compared with another pointer: 10
XXX times a pointer is qualified to be dereferenced: 4676

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 705
   level: 2, occurrence: 109
   level: 3, occurrence: 36
   level: 4, occurrence: 1
   level: 5, occurrence: 2
XXX number of pointers point to pointers: 129
XXX number of pointers point to scalars: 208
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29.3
XXX average alias set size: 1.3

XXX times a non-volatile is read: 1065
XXX times a non-volatile is write: 544
XXX times a volatile is read: 25
XXX    times read thru a pointer: 3
XXX times a volatile is write: 8
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 449
XXX percentage of non-volatile access: 98

XXX forward jumps: 1
XXX backward jumps: 5

XXX stmts: 168
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 36
   depth: 1, occurrence: 20
   depth: 2, occurrence: 24
   depth: 3, occurrence: 23
   depth: 4, occurrence: 21
   depth: 5, occurrence: 44

XXX percentage a fresh-made variable is used: 17.8
XXX percentage an existing variable is used: 82.2
********************* end of statistics **********************/


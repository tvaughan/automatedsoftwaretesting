/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      813518359
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   volatile unsigned f0 : 10;
   unsigned f1 : 25;
   int8_t  f2;
   int32_t  f3;
   volatile unsigned f4 : 12;
   unsigned f5 : 10;
   unsigned f6 : 5;
};
#pragma pack(pop)

struct S1 {
   unsigned f0 : 6;
   signed f1 : 16;
};

#pragma pack(push)
#pragma pack(1)
struct S2 {
   const uint64_t  f0;
};
#pragma pack(pop)

union U3 {
   struct S2  f0;
   int32_t  f1;
   const int64_t  f2;
   volatile uint16_t  f3;
};

union U4 {
   int64_t  f0;
   const int64_t  f1;
};

/* --- GLOBAL VARIABLES --- */
static volatile int32_t g_2[7][10] = {{0L,9L,0x68BB4AADL,0L,5L,0x68BB4AADL,0x68BB4AADL,5L,0L,0x68BB4AADL},{5L,5L,0xB0AE28DEL,9L,5L,0xFC4B03E3L,9L,9L,0xFC4B03E3L,5L},{5L,0x68BB4AADL,0x68BB4AADL,5L,0L,0x68BB4AADL,9L,0L,0L,9L},{0L,5L,0x68BB4AADL,0x68BB4AADL,5L,0L,0x68BB4AADL,9L,0L,0L},{5L,9L,0xB0AE28DEL,5L,5L,0xB0AE28DEL,0x68BB4AADL,0L,5L,0x68BB4AADL},{0L,0xFC4B03E3L,0xB0AE28DEL,0x68BB4AADL,0xFC4B03E3L,0xFC4B03E3L,0x68BB4AADL,0xB0AE28DEL,0xFC4B03E3L,0L},{0xFC4B03E3L,0x68BB4AADL,0xB0AE28DEL,0xFC4B03E3L,0L,0xB0AE28DEL,0xB0AE28DEL,0L,0xFC4B03E3L,0xB0AE28DEL}};
static volatile int32_t g_3 = 0x11C3FC02L;/* VOLATILE GLOBAL g_3 */
static volatile int32_t g_4[7][7][5] = {{{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL}},{{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L}},{{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL}},{{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L}},{{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,0x37BD55FDL,0xBFEDAACDL,1L},{3L,0xA3205F5AL,0xA19820B2L,0xA19820B2L,0xA3205F5AL},{0xF9EB11DEL,0x5CF8B874L,1L,0x5CF8B874L,0xF2A6CA0CL},{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L}},{{1L,0xF617A941L,1L,0x5CF8B874L,0xF2A6CA0CL},{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L},{1L,0xF617A941L,1L,0x5CF8B874L,0xF2A6CA0CL},{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L},{1L,0xF617A941L,1L,0x5CF8B874L,0xF2A6CA0CL},{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L},{1L,0xF617A941L,1L,0x5CF8B874L,0xF2A6CA0CL}},{{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L},{1L,0xF617A941L,1L,0x5CF8B874L,0xF2A6CA0CL},{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L},{1L,0xF617A941L,1L,0x5CF8B874L,0xF2A6CA0CL},{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L},{1L,0xF617A941L,1L,0x5CF8B874L,0xF2A6CA0CL},{0xA907865EL,7L,0xA3205F5AL,0xA3205F5AL,7L}}};
static int32_t g_5 = (-9L);
static volatile int32_t g_8 = (-3L);/* VOLATILE GLOBAL g_8 */
static volatile int32_t g_9 = 0xE1665806L;/* VOLATILE GLOBAL g_9 */
static int32_t g_10 = 0xFE905687L;
static int32_t g_13[4] = {0L,0L,0L,0L};
static volatile int32_t g_14 = (-1L);/* VOLATILE GLOBAL g_14 */
static volatile int32_t g_15 = 0xD2CE6C1CL;/* VOLATILE GLOBAL g_15 */
static int32_t g_16 = 0x2975A144L;
static int32_t g_18 = 0x9E7675DAL;
static volatile int64_t g_39 = (-4L);/* VOLATILE GLOBAL g_39 */
static int64_t g_47 = 1L;
static volatile int64_t g_50 = 5L;/* VOLATILE GLOBAL g_50 */
static union U4 g_78 = {-1L};
static struct S1 g_81[5][7][4] = {{{{6,-91},{3,100},{1,-165},{7,-180}},{{2,113},{6,54},{2,113},{4,53}},{{2,113},{4,53},{1,-165},{1,-22}},{{6,-91},{4,53},{6,150},{4,53}},{{1,-165},{6,54},{6,150},{7,-180}},{{6,-91},{3,100},{1,-165},{7,-180}},{{2,113},{6,54},{2,113},{4,53}}},{{{2,113},{4,53},{1,-165},{1,-22}},{{6,-91},{4,53},{6,150},{4,53}},{{1,-165},{6,54},{6,150},{7,-180}},{{6,-91},{3,100},{1,-165},{7,-180}},{{2,113},{6,54},{2,113},{4,53}},{{2,113},{4,53},{1,-165},{1,-22}},{{6,-91},{4,53},{6,150},{4,53}}},{{{1,-165},{6,54},{6,150},{7,-180}},{{6,-91},{3,100},{1,-165},{7,-180}},{{2,113},{6,54},{2,113},{4,53}},{{2,113},{4,53},{1,-165},{1,-22}},{{6,-91},{4,53},{6,150},{4,53}},{{1,-165},{6,54},{6,150},{7,-180}},{{6,-91},{3,100},{1,-165},{7,-180}}},{{{2,113},{6,54},{2,113},{4,53}},{{2,113},{4,53},{1,-165},{1,-22}},{{6,-91},{4,53},{6,150},{4,53}},{{1,-165},{6,54},{6,150},{7,-180}},{{6,-91},{3,100},{1,-165},{7,-180}},{{2,113},{6,54},{2,113},{4,53}},{{6,150},{1,-22},{2,113},{7,-180}}},{{{1,-165},{1,-22},{5,-195},{1,-22}},{{2,113},{3,100},{5,-195},{6,54}},{{1,-165},{4,53},{2,113},{6,54}},{{6,150},{3,100},{6,150},{1,-22}},{{6,150},{1,-22},{2,113},{7,-180}},{{1,-165},{1,-22},{5,-195},{1,-22}},{{2,113},{3,100},{5,-195},{6,54}}}};
static union U3 g_91 = {{1UL}};/* VOLATILE GLOBAL g_91 */
static int32_t *g_100 = &g_13[1];
static int32_t **g_99 = &g_100;
static volatile int32_t g_107[7] = {0xDB061A07L,0xDB061A07L,0xDB061A07L,0xDB061A07L,0xDB061A07L,0xDB061A07L,0xDB061A07L};
static uint8_t g_117 = 255UL;
static uint16_t g_138 = 0x9156L;
static uint64_t g_140 = 9UL;
static uint32_t g_142 = 18446744073709551613UL;
static struct S2 g_143 = {0UL};
static uint16_t *g_154[9] = {&g_138,&g_138,&g_138,&g_138,&g_138,&g_138,&g_138,&g_138,&g_138};
static uint16_t ** volatile g_153 = &g_154[4];/* VOLATILE GLOBAL g_153 */
static struct S1 * const  volatile g_164 = &g_81[3][2][0];/* VOLATILE GLOBAL g_164 */
static volatile struct S0 g_165 = {20,2931,8L,7L,8,26,1};/* VOLATILE GLOBAL g_165 */
static volatile struct S0 * volatile g_167 = (void*)0;/* VOLATILE GLOBAL g_167 */
static uint64_t g_189 = 18446744073709551615UL;
static struct S0 g_191 = {3,4076,1L,2L,56,4,3};/* VOLATILE GLOBAL g_191 */
static volatile int32_t *g_223 = &g_4[2][6][0];
static volatile int32_t ** volatile g_222 = &g_223;/* VOLATILE GLOBAL g_222 */
static int32_t g_248[8] = {8L,8L,0x14F1C4A0L,8L,8L,0x14F1C4A0L,8L,8L};
static int32_t g_249 = 4L;
static volatile int32_t g_251[6] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
static union U3 *g_256 = &g_91;
static union U3 **g_255 = &g_256;
static int16_t g_271 = 1L;
static struct S0 g_323[5][5] = {{{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3}},{{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1}},{{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3}},{{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1},{8,3449,1L,-1L,3,13,1}},{{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3},{1,4051,0xA4L,-5L,17,24,3}}};
static volatile struct S0 g_353 = {8,1279,0x11L,0L,20,25,3};/* VOLATILE GLOBAL g_353 */
static volatile struct S0 * volatile g_354 = &g_165;/* VOLATILE GLOBAL g_354 */
static volatile int8_t *g_413 = &g_165.f2;
static int32_t ** volatile g_420 = &g_100;/* VOLATILE GLOBAL g_420 */
static struct S2 g_444 = {0x66D166B4C1D23B27LL};
static struct S2 *g_443 = &g_444;
static uint64_t g_445 = 0xA7C6B1C9273D314BLL;
static union U3 g_456[9][8][1] = {{{{{0xB866903F71493F62LL}}},{{{0x3D13207F95C52AD1LL}}},{{{1UL}}},{{{0xDFAB369E10197CC1LL}}},{{{18446744073709551615UL}}},{{{0UL}}},{{{0UL}}},{{{0xB866903F71493F62LL}}}},{{{{2UL}}},{{{18446744073709551615UL}}},{{{0x5D71DCEB407FEE63LL}}},{{{0UL}}},{{{0UL}}},{{{0UL}}},{{{0UL}}},{{{0UL}}}},{{{{0UL}}},{{{0x5D71DCEB407FEE63LL}}},{{{18446744073709551615UL}}},{{{2UL}}},{{{0xB866903F71493F62LL}}},{{{0UL}}},{{{0UL}}},{{{18446744073709551615UL}}}},{{{{0xDFAB369E10197CC1LL}}},{{{1UL}}},{{{0x3D13207F95C52AD1LL}}},{{{0xB866903F71493F62LL}}},{{{0UL}}},{{{2UL}}},{{{1UL}}},{{{1UL}}}},{{{{1UL}}},{{{2UL}}},{{{0UL}}},{{{0xB866903F71493F62LL}}},{{{0x3D13207F95C52AD1LL}}},{{{1UL}}},{{{0xDFAB369E10197CC1LL}}},{{{18446744073709551615UL}}}},{{{{0UL}}},{{{0UL}}},{{{0xB866903F71493F62LL}}},{{{2UL}}},{{{18446744073709551615UL}}},{{{0x5D71DCEB407FEE63LL}}},{{{0UL}}},{{{0UL}}}},{{{{0UL}}},{{{0UL}}},{{{0UL}}},{{{0UL}}},{{{0x5D71DCEB407FEE63LL}}},{{{18446744073709551615UL}}},{{{2UL}}},{{{0xB866903F71493F62LL}}}},{{{{0UL}}},{{{0UL}}},{{{18446744073709551615UL}}},{{{0xDFAB369E10197CC1LL}}},{{{1UL}}},{{{0x3D13207F95C52AD1LL}}},{{{0xB866903F71493F62LL}}},{{{0UL}}}},{{{{2UL}}},{{{1UL}}},{{{1UL}}},{{{1UL}}},{{{2UL}}},{{{0UL}}},{{{0xB866903F71493F62LL}}},{{{0x3D13207F95C52AD1LL}}}}};
static uint16_t g_464 = 0x6DE7L;
static int32_t g_470 = 0x5ACEF4BCL;
static struct S0 g_499[5] = {{20,3737,-1L,0L,5,22,4},{20,3737,-1L,0L,5,22,4},{20,3737,-1L,0L,5,22,4},{20,3737,-1L,0L,5,22,4},{20,3737,-1L,0L,5,22,4}};
static struct S0 * volatile g_501 = (void*)0;/* VOLATILE GLOBAL g_501 */
static struct S0 * volatile g_502 = &g_323[0][1];/* VOLATILE GLOBAL g_502 */
static int32_t ** volatile g_525 = &g_100;/* VOLATILE GLOBAL g_525 */
static struct S0 g_527 = {22,4525,-9L,1L,61,0,3};/* VOLATILE GLOBAL g_527 */
static struct S0 g_528 = {25,4487,0x64L,-8L,53,21,3};/* VOLATILE GLOBAL g_528 */
static int16_t *g_612 = (void*)0;
static int16_t **g_611 = &g_612;
static int32_t g_616 = 0x3336980DL;
static uint32_t g_634 = 9UL;
static uint32_t g_644 = 0xACC76F44L;
static volatile int32_t * volatile *g_671[10] = {&g_223,&g_223,&g_223,&g_223,&g_223,&g_223,&g_223,&g_223,&g_223,&g_223};
static volatile int32_t * volatile **g_670 = &g_671[2];
static int16_t * const *g_687 = &g_612;
static struct S0 * const  volatile g_728 = &g_528;/* VOLATILE GLOBAL g_728 */
static const struct S2 g_731 = {1UL};
static const struct S2 *g_733 = &g_456[6][6][0].f0;
static const struct S2 ** volatile g_732[5] = {&g_733,&g_733,&g_733,&g_733,&g_733};
static const struct S2 ** volatile g_734 = (void*)0;/* VOLATILE GLOBAL g_734 */
static const struct S2 g_736 = {8UL};
static const volatile struct S0 * const  volatile g_758 = &g_165;/* VOLATILE GLOBAL g_758 */
static const volatile struct S0 * const  volatile * volatile g_757[10][4][6] = {{{&g_758,&g_758,&g_758,(void*)0,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,(void*)0,&g_758},{&g_758,&g_758,&g_758,(void*)0,&g_758,(void*)0},{&g_758,(void*)0,&g_758,&g_758,&g_758,&g_758}},{{&g_758,&g_758,(void*)0,&g_758,&g_758,&g_758},{(void*)0,&g_758,&g_758,(void*)0,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758}},{{&g_758,(void*)0,&g_758,(void*)0,&g_758,(void*)0},{&g_758,&g_758,(void*)0,(void*)0,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758}},{{&g_758,(void*)0,(void*)0,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,(void*)0,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,(void*)0,&g_758,&g_758,&g_758,&g_758}},{{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,(void*)0,&g_758,&g_758,&g_758,&g_758}},{{&g_758,&g_758,&g_758,(void*)0,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758}},{{&g_758,(void*)0,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,(void*)0,&g_758,&g_758,(void*)0},{&g_758,&g_758,&g_758,(void*)0,(void*)0,&g_758},{&g_758,&g_758,(void*)0,&g_758,&g_758,&g_758}},{{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,(void*)0,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758}},{{&g_758,&g_758,&g_758,(void*)0,&g_758,&g_758},{(void*)0,&g_758,&g_758,&g_758,(void*)0,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758}},{{&g_758,&g_758,&g_758,&g_758,&g_758,&g_758},{&g_758,&g_758,&g_758,&g_758,(void*)0,&g_758},{&g_758,&g_758,&g_758,(void*)0,&g_758,&g_758},{&g_758,&g_758,(void*)0,&g_758,&g_758,&g_758}}};
static const volatile struct S0 * const  volatile * volatile *g_756 = &g_757[9][3][0];
static const volatile struct S0 * const  volatile * volatile * volatile * volatile g_755 = &g_756;/* VOLATILE GLOBAL g_755 */
static uint8_t *g_779[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t * volatile * volatile g_778[7][10][3] = {{{(void*)0,&g_779[4],&g_779[1]},{(void*)0,&g_779[2],&g_779[0]},{&g_779[2],&g_779[0],(void*)0},{(void*)0,(void*)0,&g_779[0]},{(void*)0,&g_779[0],(void*)0},{(void*)0,(void*)0,&g_779[1]},{&g_779[1],&g_779[0],(void*)0},{&g_779[1],&g_779[2],&g_779[1]},{(void*)0,&g_779[0],&g_779[0]},{&g_779[1],&g_779[0],&g_779[0]}},{{&g_779[1],&g_779[0],&g_779[0]},{(void*)0,&g_779[0],&g_779[0]},{(void*)0,&g_779[0],(void*)0},{(void*)0,&g_779[0],&g_779[4]},{&g_779[2],&g_779[1],(void*)0},{(void*)0,&g_779[0],&g_779[3]},{(void*)0,&g_779[3],&g_779[4]},{&g_779[1],&g_779[0],&g_779[1]},{&g_779[0],&g_779[0],&g_779[0]},{&g_779[0],&g_779[1],&g_779[0]}},{{(void*)0,(void*)0,&g_779[0]},{&g_779[0],&g_779[0],&g_779[0]},{(void*)0,&g_779[1],&g_779[1]},{&g_779[1],&g_779[1],&g_779[4]},{&g_779[4],&g_779[2],&g_779[3]},{&g_779[0],&g_779[0],(void*)0},{&g_779[0],&g_779[0],&g_779[4]},{&g_779[0],&g_779[0],(void*)0},{(void*)0,&g_779[0],&g_779[0]},{&g_779[0],&g_779[0],&g_779[0]}},{{&g_779[2],&g_779[0],&g_779[0]},{&g_779[4],&g_779[0],&g_779[0]},{&g_779[0],(void*)0,&g_779[1]},{&g_779[0],&g_779[0],(void*)0},{&g_779[2],&g_779[0],&g_779[1]},{&g_779[4],&g_779[0],(void*)0},{&g_779[0],&g_779[0],&g_779[0]},{&g_779[3],&g_779[0],(void*)0},{&g_779[0],&g_779[0],&g_779[0]},{&g_779[0],&g_779[0],&g_779[1]}},{{&g_779[0],&g_779[2],(void*)0},{&g_779[0],&g_779[1],&g_779[2]},{&g_779[0],&g_779[1],&g_779[2]},{&g_779[0],&g_779[0],&g_779[2]},{&g_779[0],(void*)0,&g_779[2]},{&g_779[0],&g_779[1],&g_779[0]},{&g_779[0],&g_779[0],&g_779[3]},{&g_779[0],&g_779[0],&g_779[0]},{&g_779[0],&g_779[3],&g_779[1]},{&g_779[0],&g_779[0],&g_779[0]}},{{(void*)0,&g_779[0],&g_779[0]},{&g_779[0],&g_779[2],&g_779[0]},{&g_779[0],(void*)0,&g_779[2]},{&g_779[4],&g_779[4],&g_779[1]},{&g_779[2],&g_779[0],&g_779[0]},{&g_779[0],&g_779[0],(void*)0},{&g_779[1],&g_779[4],(void*)0},{&g_779[0],&g_779[0],(void*)0},{&g_779[0],&g_779[3],&g_779[0]},{&g_779[1],&g_779[0],&g_779[1]}},{{&g_779[3],&g_779[0],&g_779[2]},{&g_779[4],&g_779[4],&g_779[0]},{&g_779[0],&g_779[2],&g_779[0]},{&g_779[0],&g_779[0],&g_779[0]},{&g_779[3],&g_779[0],&g_779[0]},{&g_779[1],(void*)0,&g_779[0]},{&g_779[0],&g_779[0],&g_779[1]},{&g_779[0],&g_779[4],(void*)0},{&g_779[0],&g_779[0],&g_779[0]},{(void*)0,&g_779[0],(void*)0}}};
static volatile struct S0 g_804[6] = {{1,187,0L,0xC23F8306L,26,26,3},{29,5722,0x33L,7L,48,12,1},{29,5722,0x33L,7L,48,12,1},{1,187,0L,0xC23F8306L,26,26,3},{29,5722,0x33L,7L,48,12,1},{29,5722,0x33L,7L,48,12,1}};
static volatile uint32_t g_834[3] = {0x7495C8B5L,0x7495C8B5L,0x7495C8B5L};
static volatile uint32_t * volatile g_833 = &g_834[2];/* VOLATILE GLOBAL g_833 */
static volatile uint32_t * const  volatile *g_832[4][8][4] = {{{&g_833,(void*)0,&g_833,&g_833},{(void*)0,&g_833,(void*)0,&g_833},{(void*)0,(void*)0,&g_833,&g_833},{&g_833,&g_833,&g_833,&g_833},{&g_833,&g_833,&g_833,&g_833},{&g_833,&g_833,&g_833,&g_833},{&g_833,&g_833,&g_833,&g_833},{&g_833,&g_833,&g_833,&g_833}},{{&g_833,&g_833,&g_833,&g_833},{(void*)0,&g_833,(void*)0,&g_833},{&g_833,&g_833,&g_833,&g_833},{(void*)0,&g_833,&g_833,&g_833},{&g_833,&g_833,(void*)0,&g_833},{(void*)0,&g_833,&g_833,&g_833},{(void*)0,(void*)0,&g_833,&g_833},{&g_833,&g_833,&g_833,&g_833}},{{(void*)0,(void*)0,&g_833,&g_833},{&g_833,&g_833,&g_833,&g_833},{&g_833,&g_833,(void*)0,&g_833},{&g_833,&g_833,(void*)0,&g_833},{&g_833,&g_833,&g_833,&g_833},{&g_833,(void*)0,&g_833,&g_833},{&g_833,(void*)0,(void*)0,(void*)0},{&g_833,&g_833,&g_833,(void*)0}},{{&g_833,&g_833,(void*)0,&g_833},{&g_833,(void*)0,&g_833,(void*)0},{&g_833,&g_833,&g_833,&g_833},{&g_833,&g_833,(void*)0,&g_833},{&g_833,(void*)0,(void*)0,&g_833},{&g_833,&g_833,&g_833,(void*)0},{(void*)0,&g_833,&g_833,(void*)0},{&g_833,&g_833,&g_833,&g_833}}};
static int8_t g_838 = 1L;
static union U3 ***g_849 = &g_255;
static struct S0 * volatile g_860 = &g_499[2];/* VOLATILE GLOBAL g_860 */
static struct S1 *g_872 = &g_81[3][6][0];
static uint8_t g_915 = 0x93L;
static union U3 g_922 = {{18446744073709551615UL}};/* VOLATILE GLOBAL g_922 */
static union U3 g_929 = {{18446744073709551615UL}};/* VOLATILE GLOBAL g_929 */
static int8_t g_950 = 9L;
static volatile union U3 g_955 = {{18446744073709551615UL}};/* VOLATILE GLOBAL g_955 */
static union U3 g_964 = {{0x6DE30644A61CFB95LL}};/* VOLATILE GLOBAL g_964 */
static struct S0 *g_989 = &g_499[4];
static struct S0 ** volatile g_988 = &g_989;/* VOLATILE GLOBAL g_988 */
static union U3 g_991 = {{0x4AC954BF2BC9B6D3LL}};/* VOLATILE GLOBAL g_991 */
static const uint16_t g_1017 = 65528UL;
static struct S0 g_1049[1][4][3] = {{{{10,2288,0xDCL,0xE859B7B5L,18,25,0},{10,2288,0xDCL,0xE859B7B5L,18,25,0},{5,4523,0xE1L,0L,11,16,1}},{{10,2288,0xDCL,0xE859B7B5L,18,25,0},{10,2288,0xDCL,0xE859B7B5L,18,25,0},{5,4523,0xE1L,0L,11,16,1}},{{10,2288,0xDCL,0xE859B7B5L,18,25,0},{10,2288,0xDCL,0xE859B7B5L,18,25,0},{5,4523,0xE1L,0L,11,16,1}},{{10,2288,0xDCL,0xE859B7B5L,18,25,0},{10,2288,0xDCL,0xE859B7B5L,18,25,0},{5,4523,0xE1L,0L,11,16,1}}}};
static struct S0 * const  volatile g_1050[1] = {&g_528};
static union U3 g_1080 = {{9UL}};/* VOLATILE GLOBAL g_1080 */
static struct S1 * const  volatile g_1086 = &g_81[0][5][2];/* VOLATILE GLOBAL g_1086 */
static const volatile struct S0 * const  volatile g_1092[7][2] = {{&g_804[5],&g_353},{&g_804[5],&g_804[5]},{&g_353,&g_804[5]},{&g_804[5],&g_353},{&g_804[5],&g_804[5]},{&g_353,&g_804[5]},{&g_804[5],&g_353}};
static struct S0 g_1162 = {25,5483,1L,0xEA794CE0L,35,10,4};/* VOLATILE GLOBAL g_1162 */
static struct S0 *g_1161 = &g_1162;
static struct S1 * volatile g_1212 = &g_81[4][6][1];/* VOLATILE GLOBAL g_1212 */
static const volatile struct S0 g_1214 = {21,3024,0xACL,-1L,49,10,3};/* VOLATILE GLOBAL g_1214 */
static int32_t ** volatile g_1259[3][5][4] = {{{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100}},{{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100}},{{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100},{&g_100,&g_100,&g_100,&g_100}}};
static struct S0 **g_1265 = &g_989;
static struct S0 ***g_1264 = &g_1265;
static struct S0 ****g_1263 = &g_1264;
static const uint32_t g_1287[7] = {5UL,5UL,5UL,5UL,5UL,5UL,5UL};
static uint32_t g_1296[6] = {0xE836E487L,0xE836E487L,0xE836E487L,0xE836E487L,0xE836E487L,0xE836E487L};
static volatile int16_t g_1309 = 0x0553L;/* VOLATILE GLOBAL g_1309 */
static volatile uint8_t g_1311 = 0x1BL;/* VOLATILE GLOBAL g_1311 */
static union U4 g_1353 = {1L};
static union U4 *g_1352 = &g_1353;
static uint32_t g_1374[4][2] = {{1UL,1UL},{1UL,1UL},{1UL,1UL},{1UL,1UL}};
static uint8_t g_1395 = 0UL;
static union U3 g_1396 = {{0UL}};/* VOLATILE GLOBAL g_1396 */
static volatile struct S0 g_1416 = {20,2910,-6L,9L,46,14,4};/* VOLATILE GLOBAL g_1416 */
static union U3 g_1437 = {{18446744073709551614UL}};/* VOLATILE GLOBAL g_1437 */
static union U3 g_1439 = {{0xC2A224BE788F2141LL}};/* VOLATILE GLOBAL g_1439 */
static uint8_t g_1447 = 0x82L;
static struct S2 g_1454 = {8UL};
static struct S2 ** volatile g_1481[6][2] = {{&g_443,&g_443},{&g_443,&g_443},{&g_443,&g_443},{&g_443,&g_443},{&g_443,&g_443},{&g_443,&g_443}};
static int64_t *g_1485 = &g_78.f0;
static int64_t **g_1484 = &g_1485;
static int64_t *** volatile g_1486 = &g_1484;/* VOLATILE GLOBAL g_1486 */


/* --- FORWARD DECLARATIONS --- */
static union U3  func_1(void);
static int32_t  func_25(int32_t * p_26, int32_t * const  p_27, int32_t * p_28, int32_t  p_29, int32_t * p_30);
static int32_t * func_32(int32_t * p_33);
static union U3  func_62(int32_t * p_63, struct S1  p_64);
static int32_t * func_65(int32_t * const  p_66);
static struct S1 * func_70(struct S2  p_71, int32_t * p_72, union U4  p_73, uint8_t  p_74);
static int32_t * func_82(struct S1 * p_83);
static struct S0  func_84(struct S1 * p_85, struct S2  p_86);
static int32_t ** func_93(int32_t  p_94);
static struct S1  func_122(uint32_t  p_123, uint8_t  p_124, struct S2  p_125);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_223 g_4 g_10 g_1049.f5 g_464 g_100 g_13 g_222 g_164 g_81 g_872 g_915 g_99 g_1484 g_1486 g_91
 * writes: g_5 g_1049.f5 g_138 g_464 g_13 g_81 g_915 g_443 g_100 g_1484
 */
static union U3  func_1(void)
{ /* block id: 0 */
    union U4 l_31 = {1L};
    int32_t * const l_55[10][6] = {{&g_13[1],(void*)0,&g_13[1],&g_10,&g_10,&g_13[1]},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_10,(void*)0,&g_10,&g_10,(void*)0,&g_10},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_13[1],&g_10,&g_10,&g_13[1],(void*)0,&g_13[1]},{&g_13[1],(void*)0,&g_13[1],&g_10,&g_10,&g_13[1]},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_10,(void*)0,&g_10,&g_10,(void*)0,&g_10},{&g_10,&g_10,&g_10,&g_10,&g_10,&g_10},{&g_13[1],&g_10,&g_10,&g_13[1],(void*)0,&g_13[1]}};
    const int8_t *l_1204 = &g_323[0][1].f2;
    struct S1 **l_1226 = &g_872;
    uint32_t l_1250 = 0x90EFBC48L;
    struct S0 ****l_1269[1];
    int16_t l_1270[1];
    int8_t l_1272 = 0xBAL;
    uint32_t l_1394 = 18446744073709551610UL;
    int64_t *l_1420 = &l_31.f0;
    uint8_t *l_1423 = &g_915;
    int64_t l_1442 = 0xB49D86BC737B852ALL;
    struct S2 *l_1452[5][5][1];
    uint32_t *l_1461 = (void*)0;
    uint32_t *l_1462[5][3][6] = {{{&g_1374[1][0],&g_1374[2][0],&g_1374[2][0],&g_644,&g_1374[1][0],&g_1374[2][0]},{&g_644,&g_1374[1][0],&g_1374[2][0],&g_1374[2][0],&g_1374[1][0],&g_644},{&g_1374[1][0],&g_644,&g_644,&g_1374[1][0],&g_644,&g_644}},{{&g_644,&g_1374[1][0],&g_1374[2][0],&g_634,&g_634,&g_1374[2][0]},{&g_644,&g_644,&g_634,&g_1374[1][0],&g_1374[2][0],&g_1374[1][0]},{&g_1374[1][0],&g_644,&g_1374[1][0],&g_1374[2][0],&g_634,&g_634}},{{&g_644,&g_1374[1][0],&g_1374[1][0],&g_644,&g_644,&g_1374[1][0]},{&g_1374[1][0],&g_644,&g_634,&g_644,&g_1374[1][0],&g_1374[2][0]},{&g_644,&g_1374[1][0],&g_1374[2][0],&g_1374[2][0],&g_1374[1][0],&g_644}},{{&g_1374[1][0],&g_644,&g_644,&g_1374[1][0],&g_644,&g_644},{&g_644,&g_1374[1][0],&g_1374[2][0],&g_634,&g_634,&g_1374[2][0]},{&g_644,&g_644,&g_634,&g_1374[1][0],&g_1374[2][0],&g_1374[1][0]}},{{&g_1374[1][0],&g_644,&g_1374[1][0],&g_1374[2][0],&g_634,&g_634},{&g_644,&g_1374[1][0],&g_1374[1][0],&g_644,&g_644,&g_1374[1][0]},{&g_1374[1][0],&g_644,&g_634,&g_644,&g_1374[1][0],&g_1374[2][0]}}};
    int32_t l_1465 = (-1L);
    int8_t l_1466 = 1L;
    uint32_t l_1467[8];
    uint16_t *l_1468 = &g_138;
    uint16_t *l_1469 = &g_464;
    struct S2 *l_1480 = (void*)0;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1269[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_1270[i] = (-10L);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 1; k++)
                l_1452[i][j][k] = &g_991.f0;
        }
    }
    for (i = 0; i < 8; i++)
        l_1467[i] = 0x03992EF2L;
    for (g_5 = 0; (g_5 <= (-24)); g_5 = safe_sub_func_int64_t_s_s(g_5, 5))
    { /* block id: 3 */
        uint16_t l_1084 = 1UL;
        int32_t l_1174 = 0xFA41E130L;
        int32_t l_1176 = 8L;
        int32_t l_1181[9][1][2] = {{{0x038E8702L,0L}},{{0x038E8702L,0x038E8702L}},{{0x038E8702L,0L}},{{0x038E8702L,0x038E8702L}},{{0x038E8702L,0L}},{{0x038E8702L,0x038E8702L}},{{0x038E8702L,0L}},{{0x038E8702L,0x038E8702L}},{{0x038E8702L,0L}}};
        int32_t l_1197 = 1L;
        int32_t l_1213 = 0x730A2492L;
        uint16_t l_1232 = 0xB3A8L;
        uint16_t l_1233 = 65535UL;
        int16_t l_1237 = 0xD023L;
        int32_t l_1248 = 0xE9CFED2FL;
        int64_t l_1249[6];
        uint32_t l_1256 = 0UL;
        int32_t l_1291 = 0xA6429EE9L;
        uint64_t *l_1326 = &g_189;
        uint16_t *l_1346[3];
        uint32_t l_1347 = 4UL;
        int8_t l_1348 = 0xF0L;
        const int16_t l_1349 = (-1L);
        struct S2 l_1368 = {18446744073709551606UL};
        int8_t l_1375 = 0x37L;
        int8_t l_1390 = 1L;
        uint32_t l_1428 = 4294967295UL;
        struct S2 *l_1453 = &g_1454;
        int8_t *l_1456[7][7] = {{(void*)0,(void*)0,&g_1162.f2,&l_1390,&l_1375,&g_1162.f2,&l_1375},{&g_323[0][1].f2,(void*)0,(void*)0,&g_323[0][1].f2,&g_527.f2,&g_323[0][1].f2,&g_323[0][1].f2},{&l_1272,&l_1375,&g_499[4].f2,&g_499[4].f2,&l_1375,&l_1272,&g_499[4].f2},{(void*)0,&g_323[0][1].f2,&g_527.f2,&g_323[0][1].f2,&g_323[0][1].f2,&g_527.f2,&g_323[0][1].f2},{&l_1375,&g_499[4].f2,&l_1272,&l_1375,&g_499[4].f2,&g_499[4].f2,&l_1375},{&g_323[0][1].f2,&g_323[0][1].f2,&g_323[0][1].f2,&g_527.f2,&g_323[0][1].f2,(void*)0,(void*)0},{&l_1390,&l_1375,&g_1162.f2,&l_1375,&l_1390,&g_1162.f2,(void*)0}};
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_1249[i] = (-9L);
        for (i = 0; i < 3; i++)
            l_1346[i] = &g_464;
    }
    if (((*g_223) != (18446744073709551615UL > (18446744073709551607UL > (safe_div_func_int16_t_s_s((-9L), (safe_lshift_func_uint32_t_u_s((g_1049[0][2][2].f5 ^= g_10), ((*g_100) |= (safe_mul_func_int16_t_s_s(l_1465, (l_1466 <= ((*l_1469) &= ((*l_1468) = l_1467[7]))))))))))))))
    { /* block id: 784 */
        uint16_t l_1470 = 0UL;
        int32_t l_1474 = 0x3FB0FEA8L;
        if ((**g_222))
        { /* block id: 785 */
            uint32_t l_1475 = 0xE5C2BB5AL;
            (**l_1226) = (*g_164);
            if (l_1465)
                goto lbl_1473;
lbl_1473:
            ++l_1470;
            l_1475--;
        }
        else
        { /* block id: 790 */
            for (g_915 = 23; (g_915 != 23); g_915 = safe_add_func_int16_t_s_s(g_915, 6))
            { /* block id: 793 */
                struct S2 **l_1482 = &g_443;
                (*l_1482) = l_1480;
            }
        }
    }
    else
    { /* block id: 797 */
        int32_t *l_1483[5] = {&g_191.f3,&g_191.f3,&g_191.f3,&g_191.f3,&g_191.f3};
        int i;
        (*g_99) = l_1483[1];
        (*g_1486) = g_1484;
        (*g_872) = (**l_1226);
    }
    return g_91;
}


/* ------------------------------------------ */
/* 
 * reads : g_1086 g_528.f3 g_755 g_756 g_757 g_527.f3 g_191.f2 g_255 g_256 g_804.f5 g_153 g_154 g_138 g_10 g_528.f2 g_223 g_5
 * writes: g_81 g_528.f3 g_527.f3 g_191.f2 g_47 g_78.f0 g_528.f2 g_4
 */
static int32_t  func_25(int32_t * p_26, int32_t * const  p_27, int32_t * p_28, int32_t  p_29, int32_t * p_30)
{ /* block id: 616 */
    struct S1 l_1085[3][3] = {{{0,221},{0,221},{0,221}},{{5,53},{0,-155},{5,53}},{{0,221},{0,221},{0,221}}};
    const volatile struct S0 * const  volatile * volatile l_1091 = &g_1092[6][1];/* VOLATILE GLOBAL l_1091 */
    int i, j;
    (*g_1086) = l_1085[0][2];
    for (g_528.f3 = (-26); (g_528.f3 >= (-1)); g_528.f3++)
    { /* block id: 620 */
        l_1085[0][2].f1 &= (safe_lshift_func_int64_t_s_s(0L, 13));
        l_1091 = (**g_755);
    }
    for (g_527.f3 = 0; (g_527.f3 < (-25)); g_527.f3--)
    { /* block id: 626 */
        uint16_t l_1099 = 65526UL;
        for (g_191.f2 = 0; (g_191.f2 <= 2); g_191.f2 += 1)
        { /* block id: 629 */
            struct S2 **l_1121 = (void*)0;
            struct S2 ***l_1120 = &l_1121;
            int64_t *l_1122 = &g_47;
            int32_t l_1123 = 0x4830A4A8L;
            int32_t l_1124 = 0x8E3C0E41L;
            int64_t *l_1125[7][7][5] = {{{(void*)0,&g_78.f0,&g_78.f0,&g_78.f0,(void*)0},{&g_78.f0,&g_78.f0,&g_78.f0,(void*)0,&g_78.f0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,(void*)0},{&g_78.f0,(void*)0,&g_78.f0,(void*)0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,&g_78.f0,&g_78.f0,&g_78.f0,(void*)0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0}},{{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,&g_78.f0,(void*)0,(void*)0,&g_78.f0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,(void*)0,(void*)0,(void*)0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0}},{{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,&g_78.f0,&g_78.f0,(void*)0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,(void*)0,(void*)0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,(void*)0,&g_78.f0,&g_78.f0}},{{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,&g_78.f0,&g_78.f0,&g_78.f0,(void*)0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,&g_78.f0,(void*)0,(void*)0,&g_78.f0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0}},{{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,(void*)0,(void*)0,(void*)0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,&g_78.f0,&g_78.f0,(void*)0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0}},{{&g_78.f0,(void*)0,(void*)0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,(void*)0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{(void*)0,&g_78.f0,&g_78.f0,&g_78.f0,(void*)0},{(void*)0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0}},{{&g_78.f0,(void*)0,&g_78.f0,(void*)0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,(void*)0,&g_78.f0},{&g_78.f0,(void*)0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,(void*)0,&g_78.f0,&g_78.f0,(void*)0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0},{&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0,&g_78.f0}}};
            int i, j, k;
            (*g_223) = (safe_lshift_func_int8_t_s_s((g_528.f2 |= (safe_mod_func_int8_t_s_s((((((g_78.f0 = (0UL ^ (l_1099 && (safe_sub_func_int32_t_s_s((((void*)0 != (*g_255)) > ((safe_mod_func_int16_t_s_s((((safe_rshift_func_uint32_t_u_u((((safe_sub_func_uint32_t_u_u(p_29, 6L)) & (l_1123 = ((safe_div_func_int64_t_s_s(((*l_1122) = (safe_mod_func_uint16_t_u_u(((p_29 , (safe_add_func_int8_t_s_s((-2L), (safe_rshift_func_int32_t_s_u((((safe_lshift_func_uint64_t_u_s((((*l_1120) = (void*)0) != (void*)0), g_804[5].f5)) || (**g_153)) ^ 0xACC55E607463B208LL), 1))))) > l_1085[0][2].f0), 1L))), g_527.f3)) == l_1085[0][2].f1))) ^ l_1085[0][2].f1), 22)) , l_1099) || l_1085[0][2].f0), p_29)) <= l_1124)), g_10))))) , l_1099) != 0x30D1L) , 0xA4F8L) | p_29), l_1124))), 4));
        }
    }
    return (*p_26);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_32(int32_t * p_33)
{ /* block id: 15 */
    int32_t *l_34 = &g_18;
    int32_t l_35 = (-1L);
    int32_t *l_36 = &l_35;
    int32_t *l_37 = &l_35;
    int32_t *l_38[1][2];
    uint16_t l_40 = 65535UL;
    int64_t l_43[9][7][3] = {{{(-1L),0L,0x28CA52837081A23FLL},{(-4L),(-4L),0x1808E25E921D82DDLL},{0x16F2BD89C7B4ADEELL,1L,0x7AF40EE7F369DC22LL},{0L,1L,(-2L)},{1L,0xA11E95E8B2C41968LL,0x926699BEB6E18EC0LL},{0xD35FC118C1422C92LL,0L,(-2L)},{0xDDCFDBBA7677C38BLL,0xC662CD814C8B0953LL,0x7AF40EE7F369DC22LL}},{{(-7L),9L,0x1808E25E921D82DDLL},{(-1L),0L,0x28CA52837081A23FLL},{0x3883621335F78ABDLL,(-2L),0xE1404719F1796B59LL},{(-7L),0x16F2BD89C7B4ADEELL,4L},{0L,0x3C58A0399461F976LL,0x6CD6A9AA89525BE5LL},{0xE1DC5D4B51E4AA30LL,(-2L),0xC662CD814C8B0953LL},{0x537CAC03C0FEA0DCLL,0xEC59831632895B54LL,0xBFDC2BF5E7D46517LL}},{{0xC662CD814C8B0953LL,(-10L),(-10L)},{1L,1L,0x49419EC0FAF10161LL},{0x06CC1A3542F59D8BLL,0xDDCFDBBA7677C38BLL,8L},{0L,0x573FC3EFA699DF98LL,6L},{(-10L),0xE018DE91D9FE2B2FLL,1L},{0x0514DB471E7E6F17LL,0x573FC3EFA699DF98LL,(-1L)},{0xF1BDA687BA435B6ELL,0xDDCFDBBA7677C38BLL,0x490EA19DDA7D469FLL}},{{0xE1404719F1796B59LL,1L,(-1L)},{0x7AF40EE7F369DC22LL,(-10L),0x06CC1A3542F59D8BLL},{0x989F2823213DDA54LL,0xEC59831632895B54LL,7L},{7L,(-2L),0x58306E769FB3CB69LL},{(-1L),0x3C58A0399461F976LL,0xE3CDDDCD2A6C3B22LL},{0L,0x16F2BD89C7B4ADEELL,0x57AD140F5BE3012BLL},{1L,(-2L),(-7L)}},{{0xE018DE91D9FE2B2FLL,0L,0xE018DE91D9FE2B2FLL},{1L,9L,0x3C58A0399461F976LL},{0x58306E769FB3CB69LL,0xC662CD814C8B0953LL,0L},{0x2B9AB5317331A696LL,0L,0x0514DB471E7E6F17LL},{3L,0xA11E95E8B2C41968LL,5L},{0x2B9AB5317331A696LL,1L,0x5B4213D0C7CD9FD2LL},{0x58306E769FB3CB69LL,1L,0L}},{{1L,(-4L),0xD35FC118C1422C92LL},{0xE018DE91D9FE2B2FLL,0L,0x74A8696E9BF4C0ABLL},{1L,(-1L),0x3883621335F78ABDLL},{0L,0x57AD140F5BE3012BLL,0L},{(-1L),7L,1L},{7L,0x7768BC73AE716A21LL,0x16F2BD89C7B4ADEELL},{0x989F2823213DDA54LL,0x49419EC0FAF10161LL,0xE3216281FC51678ALL}},{{0x7AF40EE7F369DC22LL,0x7AF40EE7F369DC22LL,7L},{0xE1404719F1796B59LL,0xF4849B7B0AB9F43FLL,(-4L)},{0xF1BDA687BA435B6ELL,0L,0xAE449593A49E77FELL},{0x0514DB471E7E6F17LL,0x99FB3213122096FELL,9L},{(-10L),0xF1BDA687BA435B6ELL,0xAE449593A49E77FELL},{0L,0x3883621335F78ABDLL,(-4L)},{0x06CC1A3542F59D8BLL,0x926699BEB6E18EC0LL,7L}},{{1L,(-1L),0xE3216281FC51678ALL},{0xC662CD814C8B0953LL,0xAE449593A49E77FELL,0x16F2BD89C7B4ADEELL},{0x537CAC03C0FEA0DCLL,0xE1404719F1796B59LL,1L},{0xE1DC5D4B51E4AA30LL,0x490EA19DDA7D469FLL,0L},{1L,(-1L),0x2B9AB5317331A696LL},{0x021763DC0B0FB132LL,1L,0x7AF40EE7F369DC22LL},{0x2B9AB5317331A696LL,0x3883621335F78ABDLL,0x3883621335F78ABDLL}},{{0xA11E95E8B2C41968LL,0x28CA52837081A23FLL,(-5L)},{(-2L),1L,0x989F2823213DDA54LL},{0x9DD07A39AD043346LL,0L,0x490EA19DDA7D469FLL},{0x3883621335F78ABDLL,0L,(-7L)},{0x06CC1A3542F59D8BLL,0L,0x16F2BD89C7B4ADEELL},{0xF4849B7B0AB9F43FLL,1L,0x4E6ECF98005EDC9CLL},{8L,0x28CA52837081A23FLL,0xF1BDA687BA435B6ELL}}};
    int32_t l_44 = 0xFB46097EL;
    int16_t l_45 = 1L;
    int64_t l_46 = 7L;
    int32_t l_48 = 0xC5EF55CFL;
    int32_t l_49 = 7L;
    int16_t l_51 = 0x09F7L;
    uint32_t l_52 = 18446744073709551615UL;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_38[i][j] = (void*)0;
    }
    --l_40;
    l_52--;
    return p_33;
}


/* ------------------------------------------ */
/* 
 * reads : g_189 g_9 g_18 g_2 g_10 g_99 g_323 g_670 g_671 g_420 g_100 g_13 g_528.f1 g_872 g_223 g_4 g_528.f2 g_922 g_929 g_525 g_634 g_950 g_955 g_413 g_165.f2 g_964 g_988 g_991 g_527.f3 g_755 g_756 g_757 g_249 g_1017 g_445 g_499.f5 g_991.f1 g_222 g_915 g_443 g_444 g_499.f3 g_143.f0 g_191.f2 g_1049 g_922.f1 g_528.f6 g_456 g_1080
 * writes: g_189 g_464 g_117 g_872 g_18 g_78.f0 g_91.f1 g_99 g_100 g_138 g_915 g_271 g_644 g_4 g_634 g_499.f2 g_989 g_757 g_249 g_527.f3 g_445 g_991.f1 g_191 g_922.f1 g_443
 */
static union U3  func_62(int32_t * p_63, struct S1  p_64)
{ /* block id: 505 */
    int32_t l_882 = 0x1D56ADC5L;
    uint32_t l_908 = 18446744073709551606UL;
    int32_t l_918[7][5][7] = {{{(-8L),4L,(-2L),1L,4L,1L,(-2L)},{4L,4L,0xE3AB9893L,(-7L),0xC773D0DBL,1L,1L},{0x7518B7BFL,0xE3AB9893L,1L,0x362CC027L,(-8L),0xC773D0DBL,(-1L)},{1L,0xEB378F04L,0xC773D0DBL,1L,0xC773D0DBL,0xEB378F04L,1L},{6L,1L,0x27AC9345L,1L,4L,(-1L),0x362CC027L}},{{0x2CFE406EL,0x3B3F126AL,4L,0x362CC027L,6L,(-8L),(-8L)},{4L,(-7L),0x27AC9345L,(-7L),4L,0x7518B7BFL,0xE3AB9893L},{0xE3AB9893L,(-7L),0xC773D0DBL,1L,1L,(-2L),6L},{(-7L),0x3B3F126AL,1L,0xD27F45A8L,0x27AC9345L,0x27AC9345L,0xD27F45A8L},{0xE3AB9893L,1L,0xE3AB9893L,0x27AC9345L,(-1L),0x3B3F126AL,0xD27F45A8L}},{{4L,0xEB378F04L,(-2L),0x7518B7BFL,0xD27F45A8L,0xE3AB9893L,6L},{0x2CFE406EL,0xE3AB9893L,0x3B3F126AL,0xEB378F04L,0xEB378F04L,0x3B3F126AL,0xE3AB9893L},{6L,4L,0x362CC027L,0x2CFE406EL,0xEB378F04L,0x27AC9345L,(-8L)},{1L,4L,4L,0xC773D0DBL,0xD27F45A8L,(-2L),0x362CC027L},{0x7518B7BFL,(-1L),0x2CFE406EL,0x2CFE406EL,(-1L),0x7518B7BFL,1L}},{{4L,0x362CC027L,0x2CFE406EL,0xEB378F04L,0x27AC9345L,(-8L),(-1L)},{(-8L),0xD27F45A8L,4L,0x7518B7BFL,1L,(-1L),1L},{0x27AC9345L,0x362CC027L,0x362CC027L,0x27AC9345L,4L,0xEB378F04L,(-2L)},{0x27AC9345L,(-1L),0x3B3F126AL,0xD27F45A8L,6L,0xC773D0DBL,0x2CFE406EL},{(-8L),4L,(-2L),1L,4L,1L,(-2L)}},{{4L,4L,0xE3AB9893L,(-7L),0xC773D0DBL,1L,1L},{0x7518B7BFL,0xE3AB9893L,1L,0x362CC027L,(-8L),0xC773D0DBL,(-1L)},{1L,0xEB378F04L,0xC773D0DBL,1L,0xC773D0DBL,0xEB378F04L,1L},{0xEB378F04L,0x3B3F126AL,0xE3AB9893L,0x2CFE406EL,(-8L),0xD27F45A8L,4L},{0x27AC9345L,0x7518B7BFL,(-7L),4L,0xEB378F04L,9L,9L}},{{(-7L),0x362CC027L,0xE3AB9893L,0x362CC027L,(-7L),1L,6L},{6L,0x362CC027L,4L,0x3B3F126AL,0x2CFE406EL,1L,0xEB378F04L},{0x362CC027L,0x7518B7BFL,0x2CFE406EL,(-2L),0xE3AB9893L,0xE3AB9893L,(-2L)},{6L,0x3B3F126AL,6L,0xE3AB9893L,0xD27F45A8L,0x7518B7BFL,(-2L)},{(-7L),0xC773D0DBL,1L,1L,(-2L),6L,0xEB378F04L}},{{0x27AC9345L,6L,0x7518B7BFL,0xC773D0DBL,0xC773D0DBL,0x7518B7BFL,6L},{0xEB378F04L,(-8L),4L,0x27AC9345L,0xC773D0DBL,0xE3AB9893L,9L},{0x3B3F126AL,(-7L),(-8L),4L,(-2L),1L,4L},{1L,0xD27F45A8L,0x27AC9345L,0x27AC9345L,0xD27F45A8L,1L,0x3B3F126AL},{(-8L),4L,0x27AC9345L,0xC773D0DBL,0xE3AB9893L,9L,0xD27F45A8L}}};
    int32_t **l_934 = &g_100;
    int32_t l_971 = (-6L);
    int32_t l_972[3];
    int8_t l_990[9][9][3] = {{{0L,0x39L,0xEEL},{0x7CL,(-2L),0x77L},{0xA1L,(-6L),(-3L)},{0x1FL,9L,0x35L},{0x88L,(-5L),(-5L)},{0x59L,0x8DL,(-4L)},{(-5L),0x03L,0x76L},{0x9DL,8L,0x1DL},{(-2L),0xB8L,0L}},{{(-1L),8L,0xD7L},{0xAFL,0x03L,7L},{0x81L,0x8DL,0x1EL},{8L,(-5L),0x87L},{0L,9L,0L},{0x03L,(-6L),0x03L},{(-1L),(-2L),0x81L},{(-1L),0x39L,0x0EL},{0x77L,(-4L),0x9DL}},{{2L,0xEEL,0x88L},{0x77L,0x35L,1L},{(-1L),(-4L),0xB8L},{(-1L),7L,0x7CL},{0x03L,0L,0L},{0L,0x9DL,9L},{8L,8L,0x96L},{0x81L,(-4L),0L},{0xAFL,0L,0x88L}},{{(-1L),0L,0x8DL},{(-2L),0xAFL,0x88L},{0x9DL,0x20L,0L},{(-5L),0x12L,0x96L},{0x59L,0x4DL,9L},{0x88L,(-3L),0L},{0x1FL,0x7CL,0x7CL},{0xA1L,9L,0xB8L},{0x7CL,(-4L),0x1FL}},{{2L,(-3L),0xBEL},{9L,7L,1L},{0x39L,(-3L),9L},{0x77L,(-4L),0x1DL},{0x88L,0x03L,(-4L)},{(-6L),(-1L),(-1L)},{7L,0x12L,1L},{(-4L),0L,(-4L)},{(-1L),0x76L,0x88L}},{{0x7CL,8L,0x8DL},{0xEEL,0x96L,2L},{0x59L,0x81L,0xCBL},{0xEEL,0xB8L,0x88L},{0x7CL,(-4L),7L},{(-1L),0L,(-1L)},{(-4L),1L,(-4L)},{7L,2L,0x12L},{(-6L),(-6L),0L}},{{0x88L,0xAFL,7L},{0x77L,(-4L),0x35L},{0x39L,7L,0x03L},{9L,0x77L,0x35L},{2L,(-2L),7L},{(-1L),1L,0L},{(-6L),0x87L,0x12L},{0x35L,(-2L),(-4L)},{0L,(-1L),(-1L)}},{{0x4DL,0x1EL,7L},{(-1L),(-4L),0x88L},{1L,9L,0xCBL},{(-3L),8L,2L},{0x20L,9L,0x8DL},{0x96L,(-4L),0x88L},{0x1DL,0x1EL,(-4L)},{0L,(-1L),1L},{0x81L,(-2L),(-1L)}},{{(-4L),0x87L,(-4L)},{0L,1L,0x1DL},{(-5L),(-2L),9L},{0L,0x77L,1L},{0xA1L,7L,0xBEL},{0L,(-4L),0x1FL},{(-5L),0xAFL,8L},{0L,(-6L),(-1L)},{(-4L),2L,0xB8L}}};
    struct S1 l_1013 = {3,195};
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_972[i] = 0xD37345E5L;
    for (g_189 = 16; (g_189 >= 26); g_189 = safe_add_func_uint16_t_u_u(g_189, 1))
    { /* block id: 508 */
        int16_t l_886[4] = {0x9185L,0x9185L,0x9185L,0x9185L};
        struct S2 l_887 = {0x9B1579B488EFFEC0LL};
        struct S2 **l_913 = &g_443;
        int32_t l_919 = 0xDDDE8E65L;
        int32_t l_981[7] = {0L,(-6L),0L,0L,(-6L),0L,0L};
        struct S0 *l_987 = &g_527;
        int32_t *l_994 = &g_527.f3;
        int16_t l_1015 = 0L;
        union U3 * const l_1022 = &g_922;
        int i;
        for (g_464 = 29; (g_464 != 12); g_464 = safe_sub_func_uint8_t_u_u(g_464, 1))
        { /* block id: 511 */
            uint16_t l_884 = 0x9A27L;
            int32_t l_885 = 0xDB87168BL;
            int32_t l_917 = 0x7580D9CFL;
            int32_t l_975 = (-2L);
            int32_t l_976 = (-1L);
            int32_t l_977 = 0xC11E392DL;
            int32_t l_978 = 0x79C5FA8AL;
            int32_t l_979 = 7L;
            int32_t l_982 = 0x6770EAAAL;
            int32_t l_983 = 0L;
            for (g_117 = (-8); (g_117 > 31); g_117 = safe_add_func_int64_t_s_s(g_117, 7))
            { /* block id: 514 */
                struct S2 l_873 = {0x057DDB719F0801DDLL};
                uint16_t *l_883 = &g_138;
                int32_t l_927[10] = {0xE95E7EF2L,0xC8936EDAL,0xC8936EDAL,0xE95E7EF2L,0L,0xE95E7EF2L,0xC8936EDAL,0xC8936EDAL,0xE95E7EF2L,0L};
                int32_t **l_930 = (void*)0;
                int32_t **l_931 = &g_100;
                int i;
                if ((l_885 &= (safe_mul_func_uint64_t_u_u(p_64.f0, (((((*l_883) = ((((((safe_lshift_func_uint64_t_u_u((func_84((g_872 = &p_64), l_873) , 0x679F6B3E35206ECFLL), 11)) > (safe_mul_func_uint16_t_u_u(0x1C86L, l_873.f0))) , (*g_670)) == ((safe_add_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((safe_lshift_func_int8_t_s_u(l_882, p_64.f0)), 0x65FA89D90B72CF02LL)), 0L)) , (void*)0)) || (**g_420)) || (-9L))) < 65535UL) || l_884) , g_528.f1)))))
                { /* block id: 518 */
                    int8_t l_907 = 0L;
                    uint8_t *l_914 = &g_915;
                    int16_t *l_916 = &g_271;
                    (*g_872) = (*g_872);
                    l_919 = ((~65532UL) | ((~(safe_mod_func_uint64_t_u_u((safe_rshift_func_uint64_t_u_u(((safe_add_func_uint8_t_u_u((l_873.f0 | (safe_sub_func_uint16_t_u_u(((safe_mod_func_int16_t_s_s((l_917 = ((((p_64.f0 == (((*l_916) = (safe_mul_func_int64_t_s_s((0L != ((*l_914) = ((*g_223) >= (0x7EF94A55L != (safe_unary_minus_func_int32_t_s((safe_mod_func_uint8_t_u_u((l_885 = ((safe_mod_func_int32_t_s_s(l_907, (((l_908 , (safe_mul_func_int64_t_s_s((safe_sub_func_int64_t_s_s((l_913 != (void*)0), 1UL)), 5L))) ^ 0x20D4EC899C3CD409LL) , l_908))) <= 0x3AL)), l_873.f0)))))))), p_64.f0))) && p_64.f0)) ^ p_64.f1) , 6UL) , (-10L))), l_918[5][0][6])) && p_64.f1), 0x120BL))), l_884)) ^ 1L), 3)), g_528.f2))) <= 0UL));
                }
                else
                { /* block id: 525 */
                    int64_t l_928 = 0xBD34D550C8967634LL;
                    for (g_18 = (-26); (g_18 < 21); g_18++)
                    { /* block id: 528 */
                        return g_922;
                    }
                    for (g_644 = 11; (g_644 <= 50); g_644 = safe_add_func_uint32_t_u_u(g_644, 5))
                    { /* block id: 533 */
                        l_927[5] &= (safe_rshift_func_int64_t_s_u(l_908, 6));
                        if (l_919)
                            break;
                        (*g_223) &= 0x5764C002L;
                        if (l_928)
                            break;
                    }
                    return g_929;
                }
                (*l_931) = func_32(&l_927[5]);
            }
            for (l_884 = (-16); (l_884 > 47); ++l_884)
            { /* block id: 545 */
                int32_t *l_935 = (void*)0;
                uint16_t l_961[7];
                int32_t l_969 = 3L;
                int32_t l_970 = 0x68016DD4L;
                int32_t l_973 = 1L;
                int32_t l_974 = 0x410C3B7DL;
                int32_t l_980 = (-1L);
                uint32_t l_984 = 0xCFC45531L;
                int i;
                for (i = 0; i < 7; i++)
                    l_961[i] = 0x13C2L;
                p_64.f1 &= ((void*)0 != l_934);
                (*g_525) = l_935;
                for (g_78.f0 = (-13); (g_78.f0 == 10); ++g_78.f0)
                { /* block id: 550 */
                    uint32_t *l_941 = &g_634;
                    int8_t *l_956[3];
                    int32_t l_962 = (-4L);
                    uint32_t *l_963 = &g_644;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_956[i] = &g_838;
                    if ((!((*l_963) = (safe_sub_func_uint32_t_u_u((--(*l_941)), ((0x84L & (p_64.f1 , ((safe_mod_func_int32_t_s_s((l_882 = (safe_rshift_func_int16_t_s_u(l_884, 11))), (g_950 ^ 1L))) > ((safe_sub_func_int16_t_s_s(((((safe_mul_func_uint32_t_u_u(((g_955 , ((g_499[4].f2 = (*g_413)) >= ((safe_div_func_uint16_t_u_u((((p_64.f1 == ((safe_mod_func_uint16_t_u_u(p_64.f1, l_917)) || l_961[6])) , p_64.f1) && l_962), 0x8B47L)) > l_884))) != l_918[5][0][6]), p_64.f1)) < p_64.f0) == 3L) >= p_64.f0), 0UL)) , 0xDC777EA8L)))) != p_64.f1))))))
                    { /* block id: 555 */
                        return g_964;
                    }
                    else
                    { /* block id: 557 */
                        int32_t *l_965 = &g_91.f1;
                        int32_t *l_966 = &l_882;
                        int32_t *l_967 = &g_929.f1;
                        int32_t *l_968[6] = {&g_323[0][1].f3,&l_919,&l_919,&g_323[0][1].f3,&l_919,&l_919};
                        int i;
                        ++l_984;
                        (*l_934) = func_32(l_965);
                        (*g_988) = l_987;
                        if (l_990[2][2][0])
                            continue;
                    }
                    return g_991;
                }
            }
        }
        for (g_634 = 23; (g_634 >= 35); g_634 = safe_add_func_int64_t_s_s(g_634, 4))
        { /* block id: 569 */
            (*l_934) = p_63;
            (*l_934) = l_994;
            if ((**g_420))
                continue;
        }
        (*g_756) = (**g_755);
        for (g_249 = 19; (g_249 == 17); --g_249)
        { /* block id: 577 */
            int32_t l_1010 = 0xD545C880L;
            uint32_t *l_1014 = &l_908;
            struct S1 l_1016 = {3,105};
            uint64_t *l_1031 = &g_445;
            struct S2 *l_1079[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            (*g_223) |= (safe_div_func_uint16_t_u_u(0x8FB9L, (safe_mul_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u(((-4L) & (l_990[2][2][0] , 0x6CL)), (safe_add_func_uint32_t_u_u((p_64.f0 && (((((l_1016 , 0L) != (*l_994)) <= 0L) <= 0L) , g_1017)), 0x7B4B8A60L)))), 0xE3FE5CEBL))));
            if (((safe_rshift_func_uint8_t_u_s(l_1016.f1, 7)) <= ((((p_64.f0 > (safe_rshift_func_uint64_t_u_s(((p_64.f1 >= (l_1022 == (((*l_994) = p_64.f0) , l_1022))) >= (safe_lshift_func_int64_t_s_s((safe_lshift_func_uint64_t_u_u(((safe_div_func_uint64_t_u_u(0x55EE8394D46A4CEBLL, (safe_rshift_func_uint16_t_u_u(p_64.f1, (((++(*l_1031)) != ((p_64.f0 & p_64.f0) >= l_1016.f1)) , 0x6BABL))))) , p_64.f1), g_499[4].f5)), 28))), 25))) <= 18446744073709551615UL) != (-1L)) | 0xCFL)))
            { /* block id: 582 */
                for (g_991.f1 = 0; (g_991.f1 != (-21)); g_991.f1 = safe_sub_func_uint16_t_u_u(g_991.f1, 1))
                { /* block id: 585 */
                    if ((**g_222))
                        break;
                    if ((*l_994))
                        break;
                }
                for (g_915 = 8; (g_915 >= 52); g_915 = safe_add_func_int8_t_s_s(g_915, 2))
                { /* block id: 591 */
                    const int32_t l_1046 = 0xB741F8D4L;
                    (*l_994) ^= (g_4[2][6][0] && ((**l_913) , (((p_64.f1 || (safe_mul_func_int8_t_s_s((*g_413), (safe_rshift_func_int32_t_s_s((p_64.f1 >= (safe_rshift_func_int64_t_s_u(((((safe_add_func_int32_t_s_s(2L, 0xF48619EDL)) != l_1046) != (safe_mul_func_uint64_t_u_u((1UL <= l_1046), g_499[4].f3))) , g_143.f0), 16))), 6))))) < g_191.f2) , l_1016.f1)));
                }
            }
            else
            { /* block id: 594 */
                struct S0 *l_1051[1];
                struct S0 *l_1052 = &g_191;
                int32_t l_1056 = 0x75F3374FL;
                union U3 **l_1062 = &g_256;
                union U4 l_1072 = {0xAD2C2996A6B161D5LL};
                int i;
                for (i = 0; i < 1; i++)
                    l_1051[i] = (void*)0;
                (*l_1052) = g_1049[0][2][2];
                if (((safe_div_func_int16_t_s_s(1L, 0x01A9L)) | l_1016.f1))
                { /* block id: 596 */
                    return g_991;
                }
                else
                { /* block id: 598 */
                    uint16_t l_1055 = 65527UL;
                    const struct S2 l_1059[5][3][2] = {{{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}}},{{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}}},{{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}}},{{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}}},{{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}},{{18446744073709551606UL},{18446744073709551606UL}}}};
                    int i, j, k;
                    l_1056 = l_1055;
                    for (g_922.f1 = (-25); (g_922.f1 <= (-13)); g_922.f1 = safe_add_func_uint8_t_u_u(g_922.f1, 3))
                    { /* block id: 602 */
                        int8_t l_1069 = 0x7DL;
                        uint8_t l_1073 = 249UL;
                        int64_t *l_1074 = &l_1072.f0;
                        uint32_t *l_1077 = &g_644;
                        union U3 **l_1078[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1078[i] = (void*)0;
                        (*g_223) ^= (l_1059[1][2][1] , (safe_div_func_uint8_t_u_u((l_1062 == (((safe_mul_func_int16_t_s_s((safe_mod_func_uint64_t_u_u((safe_rshift_func_int32_t_s_u(l_1069, 27)), (safe_rshift_func_int64_t_s_s(((*l_1074) = (((*l_994) = g_528.f6) >= ((l_1072 , l_1073) > p_64.f1))), 32)))), l_1010)) | (((*l_1077) = (safe_rshift_func_int32_t_s_u((((0x7DBB29CEL & l_1055) > p_64.f1) ^ (-4L)), 10))) && 0xAEAD5072L)) , l_1078[1])), l_1015)));
                    }
                }
                (*l_913) = l_1079[3];
            }
            return g_456[6][6][0];
        }
    }
    return g_1080;
}


/* ------------------------------------------ */
/* 
 * reads : g_47 g_78 g_4 g_9 g_18 g_2 g_10 g_99 g_323 g_153 g_154 g_138 g_100 g_13 g_191.f3 g_164 g_81 g_420 g_191.f2 g_464 g_16 g_470 g_255 g_256 g_222 g_223 g_755 g_117 g_778 g_528.f6 g_731.f0 g_527.f6 g_78.f0 g_271 g_804 g_499.f6 g_832 g_527.f5 g_353.f5 g_525 g_527.f1 g_248 g_527.f3 g_736.f0 g_860
 * writes: g_47 g_18 g_78.f0 g_91.f1 g_99 g_100 g_191.f3 g_142 g_81 g_464 g_470 g_4 g_117 g_138 g_271 g_838 g_849 g_499
 */
static int32_t * func_65(int32_t * const  p_66)
{ /* block id: 19 */
    uint16_t **l_474 = &g_154[1];
    int32_t l_476 = (-1L);
    int32_t l_490 = 0x64D1E5ECL;
    int32_t l_503[6];
    int32_t ***l_551 = &g_99;
    int32_t **** const l_550 = &l_551;
    int32_t *l_588 = &l_503[4];
    union U3 **l_622 = &g_256;
    const int32_t l_640[5] = {0x09DC056DL,0x09DC056DL,0x09DC056DL,0x09DC056DL,0x09DC056DL};
    uint8_t l_642 = 0UL;
    int64_t l_649 = (-1L);
    uint8_t l_650 = 8UL;
    int32_t *l_701 = &l_503[2];
    uint8_t *l_708 = &g_117;
    struct S1 *l_726 = &g_81[3][6][0];
    uint32_t l_751 = 0x6C5962AFL;
    const volatile struct S0 * const  volatile * volatile * volatile * volatile l_759 = &g_756;/* VOLATILE GLOBAL l_759 */
    uint32_t l_760 = 4294967295UL;
    uint8_t l_787[1][4] = {{0x5BL,0x5BL,0x5BL,0x5BL}};
    uint8_t l_816 = 7UL;
    union U3 ***l_848 = &l_622;
    union U3 ****l_847[5];
    union U3 ***l_850 = &l_622;
    uint16_t *l_857 = &g_464;
    struct S2 l_858 = {18446744073709551608UL};
    struct S0 *l_859 = (void*)0;
    int i, j;
    for (i = 0; i < 6; i++)
        l_503[i] = 0xCB034B31L;
    for (i = 0; i < 5; i++)
        l_847[i] = &l_848;
    for (g_47 = 8; (g_47 >= (-17)); g_47 = safe_sub_func_int32_t_s_s(g_47, 6))
    { /* block id: 22 */
        const uint32_t l_75 = 0x7E2544EDL;
        struct S2 l_76 = {0x0A3B3E62BA66F70DLL};
        int32_t *l_77[6] = {&g_10,&g_13[3],&g_10,&g_10,&g_13[3],&g_10};
        struct S1 *l_473 = &g_81[2][4][3];
        struct S1 **l_472 = &l_473;
        uint16_t ***l_475[6][10][3] = {{{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{(void*)0,&l_474,(void*)0},{(void*)0,&l_474,(void*)0},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474}},{{&l_474,&l_474,&l_474},{(void*)0,&l_474,&l_474},{(void*)0,&l_474,(void*)0},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,(void*)0,&l_474},{&l_474,&l_474,&l_474},{&l_474,(void*)0,(void*)0},{&l_474,&l_474,&l_474},{(void*)0,&l_474,&l_474}},{{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,(void*)0},{&l_474,&l_474,(void*)0},{(void*)0,&l_474,&l_474},{&l_474,(void*)0,&l_474},{&l_474,&l_474,&l_474},{&l_474,(void*)0,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474}},{{&l_474,&l_474,&l_474},{(void*)0,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,(void*)0},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{(void*)0,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474}},{{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{(void*)0,&l_474,(void*)0},{(void*)0,&l_474,(void*)0},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{(void*)0,&l_474,&l_474},{(void*)0,&l_474,(void*)0}},{{&l_474,&l_474,&l_474},{&l_474,&l_474,&l_474},{&l_474,(void*)0,&l_474},{&l_474,&l_474,&l_474},{&l_474,(void*)0,(void*)0},{&l_474,&l_474,&l_474},{(void*)0,(void*)0,&l_474},{&l_474,(void*)0,&l_474},{&l_474,&l_474,&l_474},{&l_474,(void*)0,&l_474}}};
        struct S1 *l_524 = &g_81[3][6][0];
        int64_t l_658 = 0xDEC1D1CF0B1ED638LL;
        uint8_t l_674 = 6UL;
        int32_t **l_678 = &l_77[4];
        int32_t *l_702[9] = {&l_503[4],&g_499[4].f3,&l_503[4],&l_503[4],&g_499[4].f3,&l_503[4],&l_503[4],&g_499[4].f3,&l_503[4]};
        int i, j, k;
        (*l_472) = func_70((l_75 , l_76), l_77[4], g_78, g_4[2][6][0]);
        (**g_222) ^= ((l_474 = l_474) != (void*)0);
    }
lbl_805:
    l_759 = g_755;
    if (((0x3BL == (((p_66 == (*g_420)) != l_760) , 0UL)) ^ ((safe_add_func_uint8_t_u_u((safe_mul_func_uint32_t_u_u((*l_701), ((safe_rshift_func_int8_t_s_u(((void*)0 == &g_464), 3)) <= (0x7BA9FB65B6784AC3LL < 18446744073709551606UL)))), (*l_701))) , (*l_701))))
    { /* block id: 455 */
        uint8_t l_767 = 1UL;
        struct S1 *l_785 = &g_81[3][6][0];
        int32_t *l_791 = &l_490;
        for (g_117 = 0; (g_117 <= 8); g_117 += 1)
        { /* block id: 458 */
            int8_t l_786 = 0L;
            struct S1 l_790 = {6,-55};
            for (g_470 = 8; (g_470 >= 0); g_470 -= 1)
            { /* block id: 461 */
                struct S1 l_784 = {2,-94};
                int i;
                ++l_767;
                if (((((safe_mul_func_int16_t_s_s((safe_sub_func_uint32_t_u_u((safe_mod_func_int64_t_s_s(((safe_sub_func_int16_t_s_s(((&l_708 == g_778[0][0][0]) <= g_528.f6), ((&l_551 == (((safe_rshift_func_uint32_t_u_u((((safe_lshift_func_uint64_t_u_s(((l_726 != (l_784 , l_785)) == (((((l_767 , (l_784.f0 != 65528UL)) , l_767) < (-6L)) <= l_767) != l_786)), l_784.f0)) , l_784.f0) == l_786), g_731.f0)) > l_784.f0) , (void*)0)) > (*l_701)))) && (*l_588)), l_784.f0)), (*p_66))), 1L)) | g_527.f6) > 0x2E047117BA2FEE53LL) , 0x678D5102L))
                { /* block id: 463 */
                    const uint32_t l_788[9] = {0x7B184F36L,0x0DE3F223L,0x7B184F36L,0x7B184F36L,0x0DE3F223L,0x7B184F36L,0x7B184F36L,0x0DE3F223L,0x7B184F36L};
                    struct S2 l_789 = {18446744073709551610UL};
                    int i;
                    (*g_164) = l_790;
                }
                else
                { /* block id: 465 */
                    int32_t *l_792 = &g_13[1];
                    (*l_726) = (*l_726);
                    return l_792;
                }
            }
        }
    }
    else
    { /* block id: 471 */
        struct S1 **l_797 = &l_726;
        int32_t l_808 = 0xB89AA5E2L;
        int32_t l_809 = 0x05FBD6C4L;
        int32_t l_810[7][1] = {{(-9L)},{0x9B375576L},{0x9B375576L},{(-9L)},{0x9B375576L},{0x9B375576L},{(-9L)}};
        int32_t l_811 = 0x96BA243AL;
        int i, j;
        for (g_138 = 20; (g_138 < 7); --g_138)
        { /* block id: 474 */
            uint32_t l_799 = 0x9AAA497AL;
            int32_t *l_827 = &l_503[4];
            for (g_78.f0 = 0; (g_78.f0 <= 13); g_78.f0 = safe_add_func_uint8_t_u_u(g_78.f0, 6))
            { /* block id: 477 */
                struct S1 ***l_798 = &l_797;
                struct S1 ** const l_800 = &l_726;
                uint64_t l_812[6] = {0UL,0UL,0xC7B8C48EF49CEFC2LL,0UL,0UL,0xC7B8C48EF49CEFC2LL};
                int32_t l_815 = 0x2439D0CEL;
                int i;
                if ((((*l_798) = l_797) != ((*l_588) , (l_799 , l_800))))
                { /* block id: 479 */
                    struct S1 *l_803 = (void*)0;
                    for (g_271 = 0; (g_271 != (-6)); g_271 = safe_sub_func_uint32_t_u_u(g_271, 6))
                    { /* block id: 482 */
                        (*l_588) = (0x182809F4L < (l_803 == ((*l_800) = (g_804[5] , &g_81[2][2][0]))));
                        if (g_16)
                            goto lbl_805;
                    }
                }
                else
                { /* block id: 487 */
                    int32_t *l_806 = &g_191.f3;
                    int32_t *l_807[1];
                    int64_t *l_837 = &g_47;
                    int i;
                    for (i = 0; i < 1; i++)
                        l_807[i] = (void*)0;
                    ++l_812[3];
                    ++l_816;
                    (*l_827) = (((safe_lshift_func_uint8_t_u_u(((safe_lshift_func_uint32_t_u_s(g_499[4].f6, ((safe_add_func_int16_t_s_s(l_799, (((((((*l_708) = ((safe_mod_func_int32_t_s_s((l_827 == ((g_838 = ((*p_66) || (safe_rshift_func_int64_t_s_s(((*l_837) |= (safe_mod_func_uint64_t_u_u((g_832[1][3][1] != &g_833), ((g_527.f5 || ((safe_sub_func_uint64_t_u_u(g_353.f5, (-1L))) | (*l_827))) | 0L)))), g_191.f2)))) , (void*)0)), g_527.f5)) > l_809)) >= 0x6DL) && l_809) , l_827) != p_66) | 1L))) == (*l_827)))) && 65535UL), (*l_827))) != 0x11ACL) > (*l_827));
                    return (*g_525);
                }
            }
            return (*g_525);
        }
    }
    (*g_860) = func_84(l_726, ((((*l_726) , (safe_sub_func_uint16_t_u_u(((**l_474) = (**g_153)), ((*l_857) = (g_527.f1 >= ((safe_lshift_func_uint8_t_u_s((((safe_div_func_uint8_t_u_u(((g_849 = &l_622) == l_850), (safe_mod_func_int8_t_s_s((safe_div_func_uint16_t_u_u((safe_add_func_int64_t_s_s(g_248[3], g_527.f3)), (*l_588))), 0x6CL)))) != (-4L)) | g_736.f0), 3)) <= g_248[7])))))) < (*l_588)) , l_858));
    return (*g_525);
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_18 g_2 g_10 g_99 g_323 g_153 g_154 g_138 g_100 g_13 g_191.f3 g_142 g_164 g_81 g_420 g_191.f2 g_464 g_16 g_470 g_255 g_256
 * writes: g_18 g_78.f0 g_91.f1 g_99 g_100 g_191.f3 g_142 g_81 g_464 g_470
 */
static struct S1 * func_70(struct S2  p_71, int32_t * p_72, union U4  p_73, uint8_t  p_74)
{ /* block id: 23 */
    struct S1 *l_80 = &g_81[3][6][0];
    uint16_t *l_463 = &g_464;
    int32_t l_467 = 0x5036E2BDL;
    int32_t *l_468 = (void*)0;
    int32_t *l_469[5];
    union U3 *l_471[4];
    int i;
    for (i = 0; i < 5; i++)
        l_469[i] = &g_456[6][6][0].f1;
    for (i = 0; i < 4; i++)
        l_471[i] = (void*)0;
    g_470 &= (~((((void*)0 != l_80) , func_82(l_80)) != (g_191.f2 , ((g_323[0][1].f2 && (((*l_463) ^= p_74) <= (((safe_div_func_int32_t_s_s(l_467, g_16)) , l_80) == (void*)0))) , (void*)0))));
    l_471[3] = (*g_255);
    return &g_81[1][6][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_18 g_2 g_10 g_99 g_323 g_153 g_154 g_138 g_100 g_13 g_191.f3 g_142 g_164 g_81 g_420
 * writes: g_18 g_78.f0 g_91.f1 g_99 g_100 g_191.f3 g_142 g_81
 */
static int32_t * func_82(struct S1 * p_83)
{ /* block id: 24 */
    struct S1 *l_87 = &g_81[0][6][1];
    struct S2 l_88 = {0xA40C5FB2365D8C47LL};
    int32_t ***l_327[6][2][3] = {{{&g_99,&g_99,(void*)0},{&g_99,&g_99,&g_99}},{{&g_99,&g_99,&g_99},{&g_99,&g_99,&g_99}},{{&g_99,&g_99,&g_99},{&g_99,&g_99,&g_99}},{{&g_99,&g_99,&g_99},{(void*)0,&g_99,&g_99}},{{&g_99,&g_99,(void*)0},{(void*)0,&g_99,&g_99}},{{&g_99,&g_99,&g_99},{(void*)0,&g_99,&g_99}}};
    int32_t ****l_326 = &l_327[5][1][0];
    int32_t ***l_328 = &g_99;
    uint8_t *l_360 = &g_117;
    int64_t l_423 = 1L;
    uint16_t **l_460[5][4] = {{&g_154[4],(void*)0,&g_154[4],&g_154[4]},{&g_154[4],&g_154[4],&g_154[4],&g_154[4]},{(void*)0,(void*)0,&g_154[4],&g_154[4]},{&g_154[4],&g_154[4],&g_154[4],&g_154[4]},{(void*)0,&g_154[4],&g_154[4],&g_154[4]}};
    uint16_t *** const l_459 = &l_460[3][1];
    uint32_t l_462 = 0xD23EFF9CL;
    int i, j, k;
    g_191.f3 ^= (func_84(l_87, l_88) , ((((void*)0 != &g_39) >= l_88.f0) | (((safe_sub_func_uint8_t_u_u((((*l_326) = &g_99) != l_328), (((-10L) == (**g_153)) & (*g_100)))) ^ l_88.f0) < l_88.f0)));
    for (g_142 = 0; (g_142 <= 6); g_142 += 1)
    { /* block id: 185 */
        int32_t *l_329 = (void*)0;
        int64_t *l_338[9];
        int16_t l_339[4];
        int32_t l_343 = 0L;
        int32_t l_345 = (-1L);
        int32_t l_346 = 1L;
        int32_t l_347 = 9L;
        int32_t l_348 = 0x3DD1F67BL;
        uint8_t l_350 = 249UL;
        uint16_t **l_397 = (void*)0;
        uint16_t ***l_396 = &l_397;
        const struct S2 l_439 = {0x87B141C56444DE7ELL};
        int i;
        for (i = 0; i < 9; i++)
            l_338[i] = (void*)0;
        for (i = 0; i < 4; i++)
            l_339[i] = 0x7351L;
        g_100 = func_32(l_329);
        (*p_83) = (*g_164);
    }
    return (*g_420);
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_18 g_2 g_10 g_99 g_323
 * writes: g_18 g_78.f0 g_91.f1 g_99 g_100
 */
static struct S0  func_84(struct S1 * p_85, struct S2  p_86)
{ /* block id: 25 */
    int32_t *l_89 = &g_18;
    union U3 *l_90 = &g_91;
    union U3 **l_92 = &l_90;
    int32_t ***l_322[5][9] = {{&g_99,&g_99,&g_99,&g_99,(void*)0,&g_99,&g_99,(void*)0,&g_99},{&g_99,(void*)0,&g_99,&g_99,(void*)0,&g_99,&g_99,&g_99,&g_99},{&g_99,(void*)0,&g_99,&g_99,(void*)0,&g_99,&g_99,(void*)0,&g_99},{&g_99,(void*)0,&g_99,&g_99,&g_99,&g_99,&g_99,(void*)0,&g_99},{&g_99,(void*)0,&g_99,&g_99,(void*)0,&g_99,&g_99,(void*)0,&g_99}};
    int i, j;
    (*l_89) &= g_9;
    (*l_92) = l_90;
    g_99 = func_93(p_86.f0);
    g_100 = func_32(&g_13[1]);
    return g_323[0][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_10 g_99
 * writes: g_78.f0 g_91.f1
 */
static int32_t ** func_93(int32_t  p_94)
{ /* block id: 28 */
    int32_t *l_96 = &g_10;
    int32_t **l_95 = &l_96;
    union U3 *l_120 = (void*)0;
    union U3 *l_135 = &g_91;
    struct S2 l_221 = {0xBA4C5310A4CB2ACALL};
    int32_t l_235[1];
    int16_t l_238 = 0xDD76L;
    uint16_t l_252 = 65532UL;
    int64_t l_272[8];
    uint64_t l_273[5];
    uint32_t l_279 = 0UL;
    int32_t l_285 = (-1L);
    int i;
    for (i = 0; i < 1; i++)
        l_235[i] = 0L;
    for (i = 0; i < 8; i++)
        l_272[i] = 0L;
    for (i = 0; i < 5; i++)
        l_273[i] = 0x59529E8AC78EAE60LL;
    for (p_94 = 0; (p_94 <= 6); p_94 += 1)
    { /* block id: 31 */
        int32_t **l_98 = &l_96;
        int32_t l_108 = 0xFFFA2150L;
        int32_t l_109 = 1L;
        struct S0 *l_190 = &g_191;
        union U4 l_196[1][8] = {{{0L},{0xC5AE53D3FBEF0643LL},{0L},{0L},{0xC5AE53D3FBEF0643LL},{0L},{0L},{0xC5AE53D3FBEF0643LL}}};
        int32_t l_236 = 7L;
        int32_t l_243 = 0x0D41EA75L;
        int32_t l_245 = (-1L);
        int32_t l_246 = 0xCEDC222EL;
        int32_t l_247 = 0x5341E847L;
        int32_t l_250[2];
        uint64_t l_306 = 7UL;
        uint8_t *l_311 = &g_117;
        uint32_t l_316 = 4UL;
        int i, j;
        for (i = 0; i < 2; i++)
            l_250[i] = 0L;
        if (g_2[0][7])
            break;
        if (g_10)
        { /* block id: 33 */
            int32_t **l_97 = &l_96;
            return g_99;
        }
        else
        { /* block id: 35 */
            if (p_94)
                break;
        }
        for (g_78.f0 = 5; (g_78.f0 >= 0); g_78.f0 -= 1)
        { /* block id: 40 */
            uint32_t l_110 = 0xD53C59B4L;
            uint8_t *l_115 = (void*)0;
            uint8_t *l_116 = &g_117;
            union U3 *l_121 = &g_91;
            union U3 **l_136 = &l_121;
            uint16_t *l_137 = &g_138;
            uint64_t *l_139 = &g_140;
            uint32_t *l_141 = &g_142;
            int16_t *l_188[8];
            int32_t l_193 = (-1L);
            uint8_t l_239 = 0x66L;
            int32_t l_242[9] = {0L,0xEA964835L,0xEA964835L,0L,0xEA964835L,0xEA964835L,0L,0xEA964835L,0xEA964835L};
            uint16_t l_274 = 9UL;
            uint64_t l_276 = 0x79AD63CF4FC4743CLL;
            struct S2 l_277 = {7UL};
            int32_t *l_278[3];
            int32_t ***l_307[6][8][3] = {{{&l_98,&l_98,&l_95},{&l_95,&g_99,(void*)0},{&l_98,&l_98,&l_98},{&l_98,&g_99,&g_99},{&l_98,&l_98,&l_98},{&g_99,(void*)0,(void*)0},{(void*)0,&l_95,&l_95},{&l_95,(void*)0,&l_98}},{{&l_98,&g_99,&l_95},{&l_98,&l_95,&l_95},{&l_98,(void*)0,&l_98},{&l_95,&g_99,&l_95},{(void*)0,&l_98,&l_98},{&l_98,&l_95,&l_98},{&l_98,&l_98,&g_99},{&g_99,&l_95,&l_95}},{{&l_98,&l_98,&l_95},{&l_98,&l_95,&l_95},{(void*)0,&l_98,&g_99},{&l_95,&g_99,&l_98},{&l_98,&l_98,&l_95},{(void*)0,(void*)0,&l_98},{(void*)0,&g_99,&g_99},{&l_98,&l_98,&l_95}},{{&l_95,&l_98,&l_95},{&l_95,(void*)0,&l_95},{&l_95,&l_98,&g_99},{&l_95,(void*)0,&l_98},{&l_98,&l_98,&l_98},{(void*)0,&l_98,&l_95},{&l_98,&g_99,&l_98},{&l_98,(void*)0,&g_99}},{{&l_98,&l_98,&g_99},{&l_98,&g_99,(void*)0},{&l_98,&l_98,&l_98},{(void*)0,&l_95,&l_95},{&l_98,&l_98,&l_98},{&l_95,&l_95,&g_99},{&l_95,&l_98,&l_98},{&l_95,&l_95,&l_95}},{{&l_95,&l_98,&l_98},{&l_98,&g_99,(void*)0},{(void*)0,(void*)0,&g_99},{(void*)0,(void*)0,&g_99},{&l_98,(void*)0,&l_98},{&l_95,&g_99,&l_95},{(void*)0,&l_98,&l_98},{&l_98,&l_95,&l_98}}};
            uint32_t l_318[10] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
            int i, j, k;
            for (i = 0; i < 8; i++)
                l_188[i] = (void*)0;
            for (i = 0; i < 3; i++)
                l_278[i] = &l_235[0];
            for (g_91.f1 = 4; (g_91.f1 >= 0); g_91.f1 -= 1)
            { /* block id: 43 */
                int32_t l_101 = 0x72823ACEL;
                int32_t *l_102 = &l_101;
                int32_t *l_103 = &g_18;
                int32_t *l_104 = &l_101;
                int32_t *l_105 = &g_18;
                int32_t *l_106[10] = {(void*)0,&g_13[1],(void*)0,(void*)0,&g_13[1],(void*)0,&g_13[1],(void*)0,(void*)0,&g_13[1]};
                int i, j, k;
                l_110--;
            }
        }
        for (l_279 = 0; (l_279 <= 6); l_279 += 1)
        { /* block id: 173 */
            int32_t **l_321 = (void*)0;
            return l_321;
        }
    }
    return &g_100;
}


/* ------------------------------------------ */
/* 
 * reads : g_142 g_99 g_138 g_91.f1 g_81 g_18 g_2 g_164 g_165
 * writes: g_142 g_107 g_138 g_91.f1 g_100 g_18 g_2 g_153 g_81 g_165
 */
static struct S1  func_122(uint32_t  p_123, uint8_t  p_124, struct S2  p_125)
{ /* block id: 51 */
    uint32_t l_160 = 4294967295UL;
    volatile struct S0 *l_166 = (void*)0;
    int32_t l_168 = (-6L);
    int32_t *l_169 = (void*)0;
    int32_t l_170 = (-6L);
    int32_t *l_171 = &g_91.f1;
    int32_t l_172 = 0x29E822CCL;
    int32_t *l_173 = (void*)0;
    int32_t *l_174 = &g_91.f1;
    int32_t *l_175 = &g_91.f1;
    int32_t l_176 = 0x5873EBDBL;
    int32_t l_177 = (-9L);
    int32_t l_178 = (-7L);
    int32_t *l_179 = &l_178;
    int32_t l_180 = 7L;
    int32_t *l_181[9][2][2] = {{{&g_18,&g_91.f1},{&g_18,&l_177}},{{&l_177,&g_91.f1},{&l_177,&l_177}},{{&g_18,&g_91.f1},{&g_18,&l_177}},{{&l_177,&g_91.f1},{&l_177,&l_177}},{{&g_18,&g_91.f1},{&g_18,&l_177}},{{&l_177,&g_91.f1},{&l_177,&l_177}},{{&g_18,&g_91.f1},{&g_18,&l_177}},{{&l_177,&g_91.f1},{&l_177,&l_177}},{{&g_18,&g_91.f1},{&g_18,&l_177}}};
    int8_t l_182 = 0x47L;
    int64_t l_183 = 0x2911801D432DFEADLL;
    uint32_t l_184 = 1UL;
    struct S1 l_187 = {7,228};
    int i, j, k;
    for (g_142 = 0; (g_142 >= 30); g_142++)
    { /* block id: 54 */
        uint8_t *l_146 = &g_117;
        int32_t l_156 = 0L;
        struct S1 l_163 = {6,231};
        for (p_123 = 0; (p_123 <= 6); p_123 += 1)
        { /* block id: 57 */
            int i;
            g_107[p_123] = ((l_146 != &g_117) == 0x871E9C7BL);
            for (g_138 = 0; (g_138 <= 3); g_138 += 1)
            { /* block id: 61 */
                for (g_91.f1 = 6; (g_91.f1 >= 2); g_91.f1 -= 1)
                { /* block id: 64 */
                    int i, j, k;
                    (*g_99) = (void*)0;
                    return g_81[(g_138 + 1)][g_91.f1][g_138];
                }
            }
        }
        for (g_138 = 0; (g_138 > 42); g_138 = safe_add_func_uint32_t_u_u(g_138, 9))
        { /* block id: 72 */
            uint32_t l_157 = 0x4EC45E47L;
            for (g_18 = 3; (g_18 >= 0); g_18 -= 1)
            { /* block id: 75 */
                volatile int32_t *l_149 = &g_2[g_18][(g_18 + 4)];
                const uint16_t ***l_150 = (void*)0;
                uint16_t **l_152 = (void*)0;
                uint16_t ***l_151[3][4] = {{&l_152,&l_152,&l_152,&l_152},{&l_152,&l_152,&l_152,&l_152},{&l_152,&l_152,&l_152,&l_152}};
                int32_t *l_155[4][3][8] = {{{&g_13[1],&g_5,&g_13[1],&g_91.f1,&g_91.f1,&g_13[1],&g_5,&g_13[1]},{&g_91.f1,&g_13[1],&g_5,&g_13[1],&g_91.f1,&g_91.f1,&g_13[1],&g_5},{&g_91.f1,&g_91.f1,&g_13[1],&g_5,&g_13[1],&g_91.f1,&g_91.f1,&g_13[1]}},{{&g_5,&g_13[1],&g_13[1],&g_5,&g_91.f1,&g_5,&g_13[1],&g_13[1]},{&g_13[1],&g_91.f1,&g_5,&g_5,&g_91.f1,&g_13[1],&g_91.f1,&g_5},{&g_5,&g_91.f1,&g_5,&g_13[1],&g_13[1],&g_5,&g_91.f1,&g_5}},{{&g_91.f1,&g_13[1],&g_5,&g_13[1],&g_91.f1,&g_91.f1,&g_13[1],&g_5},{&g_91.f1,&g_91.f1,&g_13[1],&g_5,&g_13[1],&g_91.f1,&g_91.f1,&g_13[1]},{&g_5,&g_13[1],&g_13[1],&g_5,&g_91.f1,&g_5,&g_13[1],&g_13[1]}},{{&g_13[1],&g_91.f1,&g_5,&g_5,&g_91.f1,&g_13[1],&g_91.f1,&g_5},{&g_5,&g_91.f1,&g_5,&g_13[1],&g_13[1],&g_5,&g_91.f1,&g_5},{&g_91.f1,&g_13[1],&g_5,&g_13[1],&g_91.f1,&g_91.f1,&g_13[1],&g_5}}};
                int i, j, k;
                (*l_149) = g_2[g_18][(g_18 + 4)];
                g_153 = (void*)0;
                ++l_157;
                ++l_160;
            }
            if (p_125.f0)
                continue;
            if (p_123)
                break;
            (*g_164) = l_163;
        }
    }
    g_165 = g_165;
    ++l_184;
    return l_187;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_2[i][j], "g_2[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_4[i][j][k], "g_4[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_9, "g_9", print_hash_value);
    transparent_crc(g_10, "g_10", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_13[i], "g_13[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    transparent_crc(g_39, "g_39", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_50, "g_50", print_hash_value);
    transparent_crc(g_78.f0, "g_78.f0", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_81[i][j][k].f0, "g_81[i][j][k].f0", print_hash_value);
                transparent_crc(g_81[i][j][k].f1, "g_81[i][j][k].f1", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_91.f0.f0, "g_91.f0.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_107[i], "g_107[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_138, "g_138", print_hash_value);
    transparent_crc(g_140, "g_140", print_hash_value);
    transparent_crc(g_142, "g_142", print_hash_value);
    transparent_crc(g_143.f0, "g_143.f0", print_hash_value);
    transparent_crc(g_165.f0, "g_165.f0", print_hash_value);
    transparent_crc(g_165.f1, "g_165.f1", print_hash_value);
    transparent_crc(g_165.f2, "g_165.f2", print_hash_value);
    transparent_crc(g_165.f3, "g_165.f3", print_hash_value);
    transparent_crc(g_165.f4, "g_165.f4", print_hash_value);
    transparent_crc(g_165.f5, "g_165.f5", print_hash_value);
    transparent_crc(g_165.f6, "g_165.f6", print_hash_value);
    transparent_crc(g_189, "g_189", print_hash_value);
    transparent_crc(g_191.f0, "g_191.f0", print_hash_value);
    transparent_crc(g_191.f1, "g_191.f1", print_hash_value);
    transparent_crc(g_191.f2, "g_191.f2", print_hash_value);
    transparent_crc(g_191.f3, "g_191.f3", print_hash_value);
    transparent_crc(g_191.f4, "g_191.f4", print_hash_value);
    transparent_crc(g_191.f5, "g_191.f5", print_hash_value);
    transparent_crc(g_191.f6, "g_191.f6", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_248[i], "g_248[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_249, "g_249", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_251[i], "g_251[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_271, "g_271", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_323[i][j].f0, "g_323[i][j].f0", print_hash_value);
            transparent_crc(g_323[i][j].f1, "g_323[i][j].f1", print_hash_value);
            transparent_crc(g_323[i][j].f2, "g_323[i][j].f2", print_hash_value);
            transparent_crc(g_323[i][j].f3, "g_323[i][j].f3", print_hash_value);
            transparent_crc(g_323[i][j].f4, "g_323[i][j].f4", print_hash_value);
            transparent_crc(g_323[i][j].f5, "g_323[i][j].f5", print_hash_value);
            transparent_crc(g_323[i][j].f6, "g_323[i][j].f6", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_353.f0, "g_353.f0", print_hash_value);
    transparent_crc(g_353.f1, "g_353.f1", print_hash_value);
    transparent_crc(g_353.f2, "g_353.f2", print_hash_value);
    transparent_crc(g_353.f3, "g_353.f3", print_hash_value);
    transparent_crc(g_353.f4, "g_353.f4", print_hash_value);
    transparent_crc(g_353.f5, "g_353.f5", print_hash_value);
    transparent_crc(g_353.f6, "g_353.f6", print_hash_value);
    transparent_crc(g_444.f0, "g_444.f0", print_hash_value);
    transparent_crc(g_445, "g_445", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_456[i][j][k].f0.f0, "g_456[i][j][k].f0.f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_464, "g_464", print_hash_value);
    transparent_crc(g_470, "g_470", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_499[i].f0, "g_499[i].f0", print_hash_value);
        transparent_crc(g_499[i].f1, "g_499[i].f1", print_hash_value);
        transparent_crc(g_499[i].f2, "g_499[i].f2", print_hash_value);
        transparent_crc(g_499[i].f3, "g_499[i].f3", print_hash_value);
        transparent_crc(g_499[i].f4, "g_499[i].f4", print_hash_value);
        transparent_crc(g_499[i].f5, "g_499[i].f5", print_hash_value);
        transparent_crc(g_499[i].f6, "g_499[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_527.f0, "g_527.f0", print_hash_value);
    transparent_crc(g_527.f1, "g_527.f1", print_hash_value);
    transparent_crc(g_527.f2, "g_527.f2", print_hash_value);
    transparent_crc(g_527.f3, "g_527.f3", print_hash_value);
    transparent_crc(g_527.f4, "g_527.f4", print_hash_value);
    transparent_crc(g_527.f5, "g_527.f5", print_hash_value);
    transparent_crc(g_527.f6, "g_527.f6", print_hash_value);
    transparent_crc(g_528.f0, "g_528.f0", print_hash_value);
    transparent_crc(g_528.f1, "g_528.f1", print_hash_value);
    transparent_crc(g_528.f2, "g_528.f2", print_hash_value);
    transparent_crc(g_528.f3, "g_528.f3", print_hash_value);
    transparent_crc(g_528.f4, "g_528.f4", print_hash_value);
    transparent_crc(g_528.f5, "g_528.f5", print_hash_value);
    transparent_crc(g_528.f6, "g_528.f6", print_hash_value);
    transparent_crc(g_616, "g_616", print_hash_value);
    transparent_crc(g_634, "g_634", print_hash_value);
    transparent_crc(g_644, "g_644", print_hash_value);
    transparent_crc(g_731.f0, "g_731.f0", print_hash_value);
    transparent_crc(g_736.f0, "g_736.f0", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_804[i].f0, "g_804[i].f0", print_hash_value);
        transparent_crc(g_804[i].f1, "g_804[i].f1", print_hash_value);
        transparent_crc(g_804[i].f2, "g_804[i].f2", print_hash_value);
        transparent_crc(g_804[i].f3, "g_804[i].f3", print_hash_value);
        transparent_crc(g_804[i].f4, "g_804[i].f4", print_hash_value);
        transparent_crc(g_804[i].f5, "g_804[i].f5", print_hash_value);
        transparent_crc(g_804[i].f6, "g_804[i].f6", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_834[i], "g_834[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_838, "g_838", print_hash_value);
    transparent_crc(g_915, "g_915", print_hash_value);
    transparent_crc(g_922.f0.f0, "g_922.f0.f0", print_hash_value);
    transparent_crc(g_929.f0.f0, "g_929.f0.f0", print_hash_value);
    transparent_crc(g_950, "g_950", print_hash_value);
    transparent_crc(g_955.f0.f0, "g_955.f0.f0", print_hash_value);
    transparent_crc(g_964.f0.f0, "g_964.f0.f0", print_hash_value);
    transparent_crc(g_991.f0.f0, "g_991.f0.f0", print_hash_value);
    transparent_crc(g_1017, "g_1017", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_1049[i][j][k].f0, "g_1049[i][j][k].f0", print_hash_value);
                transparent_crc(g_1049[i][j][k].f1, "g_1049[i][j][k].f1", print_hash_value);
                transparent_crc(g_1049[i][j][k].f2, "g_1049[i][j][k].f2", print_hash_value);
                transparent_crc(g_1049[i][j][k].f3, "g_1049[i][j][k].f3", print_hash_value);
                transparent_crc(g_1049[i][j][k].f4, "g_1049[i][j][k].f4", print_hash_value);
                transparent_crc(g_1049[i][j][k].f5, "g_1049[i][j][k].f5", print_hash_value);
                transparent_crc(g_1049[i][j][k].f6, "g_1049[i][j][k].f6", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1080.f0.f0, "g_1080.f0.f0", print_hash_value);
    transparent_crc(g_1162.f0, "g_1162.f0", print_hash_value);
    transparent_crc(g_1162.f1, "g_1162.f1", print_hash_value);
    transparent_crc(g_1162.f2, "g_1162.f2", print_hash_value);
    transparent_crc(g_1162.f3, "g_1162.f3", print_hash_value);
    transparent_crc(g_1162.f4, "g_1162.f4", print_hash_value);
    transparent_crc(g_1162.f5, "g_1162.f5", print_hash_value);
    transparent_crc(g_1162.f6, "g_1162.f6", print_hash_value);
    transparent_crc(g_1214.f0, "g_1214.f0", print_hash_value);
    transparent_crc(g_1214.f1, "g_1214.f1", print_hash_value);
    transparent_crc(g_1214.f2, "g_1214.f2", print_hash_value);
    transparent_crc(g_1214.f3, "g_1214.f3", print_hash_value);
    transparent_crc(g_1214.f4, "g_1214.f4", print_hash_value);
    transparent_crc(g_1214.f5, "g_1214.f5", print_hash_value);
    transparent_crc(g_1214.f6, "g_1214.f6", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1287[i], "g_1287[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_1296[i], "g_1296[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1309, "g_1309", print_hash_value);
    transparent_crc(g_1311, "g_1311", print_hash_value);
    transparent_crc(g_1353.f0, "g_1353.f0", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1374[i][j], "g_1374[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1395, "g_1395", print_hash_value);
    transparent_crc(g_1396.f0.f0, "g_1396.f0.f0", print_hash_value);
    transparent_crc(g_1416.f0, "g_1416.f0", print_hash_value);
    transparent_crc(g_1416.f1, "g_1416.f1", print_hash_value);
    transparent_crc(g_1416.f2, "g_1416.f2", print_hash_value);
    transparent_crc(g_1416.f3, "g_1416.f3", print_hash_value);
    transparent_crc(g_1416.f4, "g_1416.f4", print_hash_value);
    transparent_crc(g_1416.f5, "g_1416.f5", print_hash_value);
    transparent_crc(g_1416.f6, "g_1416.f6", print_hash_value);
    transparent_crc(g_1437.f0.f0, "g_1437.f0.f0", print_hash_value);
    transparent_crc(g_1439.f0.f0, "g_1439.f0.f0", print_hash_value);
    transparent_crc(g_1447, "g_1447", print_hash_value);
    transparent_crc(g_1454.f0, "g_1454.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 467
   depth: 1, occurrence: 42
XXX total union variables: 15

XXX non-zero bitfields defined in structs: 7
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 2
XXX structs with bitfields in the program: 51
breakdown:
   indirect level: 0, occurrence: 25
   indirect level: 1, occurrence: 14
   indirect level: 2, occurrence: 6
   indirect level: 3, occurrence: 2
   indirect level: 4, occurrence: 4
XXX full-bitfields structs in the program: 15
breakdown:
   indirect level: 0, occurrence: 15
XXX times a bitfields struct's address is taken: 55
XXX times a bitfields struct on LHS: 4
XXX times a bitfields struct on RHS: 34
XXX times a single bitfield on LHS: 5
XXX times a single bitfield on RHS: 89

XXX max expression depth: 36
breakdown:
   depth: 1, occurrence: 139
   depth: 2, occurrence: 42
   depth: 3, occurrence: 3
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 3
   depth: 14, occurrence: 1
   depth: 16, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 28, occurrence: 2
   depth: 29, occurrence: 1
   depth: 33, occurrence: 1
   depth: 36, occurrence: 1

XXX total number of pointers: 336

XXX times a variable address is taken: 1225
XXX times a pointer is dereferenced on RHS: 147
breakdown:
   depth: 1, occurrence: 121
   depth: 2, occurrence: 26
XXX times a pointer is dereferenced on LHS: 175
breakdown:
   depth: 1, occurrence: 164
   depth: 2, occurrence: 11
XXX times a pointer is compared with null: 20
XXX times a pointer is compared with address of another variable: 10
XXX times a pointer is compared with another pointer: 10
XXX times a pointer is qualified to be dereferenced: 7577

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 686
   level: 2, occurrence: 200
   level: 3, occurrence: 70
   level: 4, occurrence: 27
XXX number of pointers point to pointers: 111
XXX number of pointers point to scalars: 168
XXX number of pointers point to structs: 48
XXX percent of pointers has null in alias set: 33.6
XXX average alias set size: 1.48

XXX times a non-volatile is read: 968
XXX times a non-volatile is write: 533
XXX times a volatile is read: 113
XXX    times read thru a pointer: 28
XXX times a volatile is write: 59
XXX    times written thru a pointer: 26
XXX times a volatile is available for access: 4.84e+03
XXX percentage of non-volatile access: 89.7

XXX forward jumps: 1
XXX backward jumps: 1

XXX stmts: 140
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 23
   depth: 2, occurrence: 27
   depth: 3, occurrence: 20
   depth: 4, occurrence: 23
   depth: 5, occurrence: 13

XXX percentage a fresh-made variable is used: 18.7
XXX percentage an existing variable is used: 81.3
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
XXX total OOB instances added: 0
********************* end of statistics **********************/


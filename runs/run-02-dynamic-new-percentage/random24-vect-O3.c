/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      1243699593
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_3 = 7L;
static int32_t g_4 = 0xBF5CD53BL;
static volatile int32_t g_13 = 0x4D1454BCL;/* VOLATILE GLOBAL g_13 */
static volatile int32_t g_14 = (-8L);/* VOLATILE GLOBAL g_14 */
static volatile int32_t g_15 = 0xF44401D1L;/* VOLATILE GLOBAL g_15 */
static volatile int32_t g_16 = 0L;/* VOLATILE GLOBAL g_16 */
static int32_t g_17 = (-4L);
static volatile int32_t g_20 = 0xCD4431FAL;/* VOLATILE GLOBAL g_20 */
static volatile int32_t g_21[4] = {(-1L),(-1L),(-1L),(-1L)};
static volatile int32_t g_22 = 0x017779A7L;/* VOLATILE GLOBAL g_22 */
static int32_t g_23 = 0x5ACEE617L;
static int64_t g_47 = (-5L);
static uint32_t g_51 = 4294967290UL;
static uint32_t g_54 = 0x4ADE726FL;
static int32_t * volatile g_60 = &g_23;/* VOLATILE GLOBAL g_60 */
static uint16_t g_83[3] = {0x336FL,0x336FL,0x336FL};
static uint8_t g_91 = 1UL;
static uint8_t g_100 = 0x96L;
static int16_t g_119 = (-1L);
static int32_t g_128 = 0x1179EF6EL;
static uint32_t g_129 = 1UL;
static uint8_t g_143[7][3] = {{0x87L,0x87L,0x87L},{0x87L,0x87L,0x87L},{0x87L,0x87L,0x87L},{0x87L,0x87L,0x87L},{0x87L,0x87L,0x87L},{0x87L,0x87L,0x87L},{0x87L,0x87L,0x87L}};
static int32_t g_149 = 0x347E9BEEL;
static int32_t g_151 = 0xA53AF947L;
static uint32_t g_152 = 0x665D24B4L;
static int64_t g_155 = 0x61C4191E15B83574LL;
static int32_t g_203 = 2L;
static uint16_t g_205 = 5UL;
static uint8_t g_218 = 0xF8L;
static const int64_t * const  volatile g_236 = &g_47;/* VOLATILE GLOBAL g_236 */
static const int64_t * const  volatile *g_235 = &g_236;
static int32_t *g_257 = (void*)0;
static uint64_t g_261 = 0x4A546A7A22F57355LL;
static int64_t **g_330 = (void*)0;
static int64_t ***g_329 = &g_330;
static int64_t ****g_328 = &g_329;
static int16_t g_355 = 0xEA6BL;
static uint64_t g_356 = 18446744073709551610UL;
static const volatile uint16_t *g_360 = (void*)0;
static const volatile uint16_t * const *g_359 = &g_360;
static int8_t g_445[9] = {5L,5L,5L,5L,5L,5L,5L,5L,5L};
static int32_t g_478[5][6][7] = {{{0x6C7770FDL,0x6DFAE1C6L,(-1L),0x27B2C724L,0x5C62689EL,(-1L),7L},{0x6DFAE1C6L,7L,0L,(-1L),0x6C7770FDL,0x6C7770FDL,(-1L)},{7L,7L,7L,0x6C7770FDL,0x4D9ACF18L,7L,(-1L)},{0x6DFAE1C6L,(-1L),0x27B2C724L,0x5C62689EL,(-1L),7L,(-4L)},{(-1L),7L,7L,(-1L),(-1L),7L,7L},{(-4L),8L,0xE358FFBCL,(-1L),(-1L),0x6C7770FDL,(-3L)}},{{7L,0x6DFAE1C6L,8L,(-4L),(-1L),0x27B2C724L,0xE358FFBCL},{0x5C62689EL,0x4D9ACF18L,(-1L),(-1L),0x4D9ACF18L,0x5C62689EL,7L},{8L,0xE358FFBCL,(-1L),(-1L),0x6C7770FDL,(-3L),0x4D9ACF18L},{(-3L),(-1L),8L,0x5C62689EL,0L,0x4D9ACF18L,0L},{0x6C7770FDL,0xE358FFBCL,0xE358FFBCL,0x6C7770FDL,0x6DFAE1C6L,(-1L),0x27B2C724L},{0x6C7770FDL,0x4D9ACF18L,7L,(-1L),(-4L),(-4L),(-1L)}},{{(-3L),0x6DFAE1C6L,0x27B2C724L,7L,8L,7L,0x27B2C724L},{8L,8L,7L,1L,(-4L),7L,0L},{0x5C62689EL,7L,0L,0xE358FFBCL,(-3L),(-4L),0x4D9ACF18L},{7L,(-1L),(-4L),0L,(-4L),(-1L),7L},{(-4L),7L,0x6C7770FDL,0L,8L,0x4D9ACF18L,0xE358FFBCL},{(-1L),7L,0x6DFAE1C6L,0xE358FFBCL,(-4L),(-3L),(-3L)}},{{0x6DFAE1C6L,1L,0x6C7770FDL,1L,0x6DFAE1C6L,0x5C62689EL,7L},{7L,1L,(-4L),7L,0L,0x27B2C724L,(-4L)},{1L,7L,0L,(-1L),0x6C7770FDL,0x6C7770FDL,(-1L)},{7L,7L,7L,0x6C7770FDL,0x4D9ACF18L,7L,(-1L)},{0x6DFAE1C6L,(-1L),0x27B2C724L,0x5C62689EL,(-1L),7L,(-4L)},{(-1L),7L,7L,(-1L),(-1L),7L,7L}},{{(-4L),8L,0xE358FFBCL,(-1L),(-1L),0x6C7770FDL,(-3L)},{7L,0x6DFAE1C6L,8L,(-4L),(-1L),0x27B2C724L,0xE358FFBCL},{0x5C62689EL,0x4D9ACF18L,(-1L),(-1L),0x4D9ACF18L,0x5C62689EL,7L},{8L,0xE358FFBCL,(-1L),(-1L),0x6C7770FDL,(-3L),0x4D9ACF18L},{(-3L),(-1L),8L,0x5C62689EL,(-1L),(-1L),(-1L)},{7L,8L,8L,7L,1L,(-4L),7L}}};
static int32_t g_490 = (-1L);
static uint8_t *g_610 = &g_143[3][0];
static uint8_t **g_609 = &g_610;
static int32_t **g_626 = &g_257;
static volatile int32_t g_641[1][9][1] = {{{0xF6B82971L},{0L},{0xF6B82971L},{0L},{0xF6B82971L},{0L},{0xF6B82971L},{0L},{0xF6B82971L}}};
static volatile int32_t * const g_640 = &g_641[0][1][0];
static volatile int32_t * const *g_639[9] = {&g_640,&g_640,&g_640,&g_640,&g_640,&g_640,&g_640,&g_640,&g_640};
static const int32_t g_714 = 6L;
static uint32_t g_728 = 0xF136E3B2L;
static int16_t g_730[9][1] = {{0x5083L},{0x5083L},{0x5083L},{0x5083L},{0x5083L},{0x5083L},{0x5083L},{0x5083L},{0x5083L}};
static const int8_t *g_755[6] = {&g_445[2],&g_445[2],&g_445[2],&g_445[2],&g_445[2],&g_445[2]};
static const int8_t **g_754 = &g_755[4];
static uint64_t g_902 = 0xA377404C6B9BD186LL;
static volatile uint32_t g_990 = 0x73CF053CL;/* VOLATILE GLOBAL g_990 */
static volatile uint32_t *g_989 = &g_990;
static volatile uint32_t **g_988 = &g_989;
static const uint32_t **g_991 = (void*)0;
static int32_t g_1035[3][1] = {{0x63ACCAE7L},{0x63ACCAE7L},{0x63ACCAE7L}};
static volatile int8_t *** volatile g_1070 = (void*)0;/* VOLATILE GLOBAL g_1070 */
static volatile int8_t *** const  volatile * volatile g_1069 = &g_1070;/* VOLATILE GLOBAL g_1069 */
static volatile int8_t *** const  volatile * volatile *g_1068 = &g_1069;
static int32_t * volatile *g_1089 = (void*)0;
static int32_t * volatile ** const g_1088 = &g_1089;
static uint8_t g_1148 = 0UL;
static int8_t **g_1321 = (void*)0;
static int16_t *g_1327[5] = {&g_730[8][0],&g_730[8][0],&g_730[8][0],&g_730[8][0],&g_730[8][0]};
static int32_t * volatile g_1352 = &g_1035[1][0];/* VOLATILE GLOBAL g_1352 */
static uint8_t g_1370 = 255UL;
static uint8_t g_1404 = 0x03L;
static const int32_t *g_1446 = &g_490;
static const int32_t **g_1445[8][10] = {{&g_1446,&g_1446,&g_1446,&g_1446,(void*)0,&g_1446,&g_1446,&g_1446,&g_1446,(void*)0},{&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,(void*)0,&g_1446,&g_1446,&g_1446,&g_1446},{(void*)0,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,(void*)0,(void*)0,(void*)0},{&g_1446,&g_1446,&g_1446,(void*)0,(void*)0,&g_1446,&g_1446,&g_1446,(void*)0,&g_1446},{&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,(void*)0},{&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,&g_1446,(void*)0},{(void*)0,&g_1446,&g_1446,&g_1446,&g_1446,(void*)0,&g_1446,(void*)0,&g_1446,&g_1446},{&g_1446,&g_1446,&g_1446,&g_1446,(void*)0,&g_1446,&g_1446,&g_1446,&g_1446,(void*)0}};
static volatile uint16_t g_1457 = 1UL;/* VOLATILE GLOBAL g_1457 */
static uint32_t *g_1504 = &g_54;
static int32_t g_1507 = (-5L);
static volatile int16_t * const g_1559 = (void*)0;
static volatile int16_t * const *g_1558 = &g_1559;
static volatile int16_t * const * volatile *g_1557 = &g_1558;
static int64_t ** const *g_1563 = (void*)0;
static int64_t ** const **g_1562 = &g_1563;
static int64_t ** const ***g_1561[5][2][6] = {{{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562},{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562}},{{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562},{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562}},{{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562},{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562}},{{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562},{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562}},{{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562},{&g_1562,&g_1562,&g_1562,&g_1562,&g_1562,&g_1562}}};
static const int64_t **g_1593 = (void*)0;
static const int64_t ***g_1592 = &g_1593;
static const int64_t ****g_1591 = &g_1592;
static volatile int8_t g_1618 = (-1L);/* VOLATILE GLOBAL g_1618 */
static volatile int8_t *g_1617 = &g_1618;
static volatile int8_t **g_1616[9] = {&g_1617,&g_1617,&g_1617,&g_1617,&g_1617,&g_1617,&g_1617,&g_1617,&g_1617};
static volatile int8_t ** volatile *g_1615 = &g_1616[5];
static volatile int8_t ** volatile ** const g_1614 = &g_1615;
static int8_t g_1648 = 0L;
static int64_t g_1695[2][4] = {{(-1L),0xCFDE8317EA56A364LL,0xCFDE8317EA56A364LL,(-1L)},{0xCFDE8317EA56A364LL,(-1L),0xCFDE8317EA56A364LL,0xCFDE8317EA56A364LL}};
static volatile uint32_t g_1707 = 0xA8D0B4F2L;/* VOLATILE GLOBAL g_1707 */
static volatile int32_t g_1710 = 0xD3F5AAF7L;/* VOLATILE GLOBAL g_1710 */
static const int64_t g_1762 = 0xDF6E516797CFA448LL;
static uint32_t g_1799 = 0xF3748051L;
static int32_t * const  volatile g_1834 = (void*)0;/* VOLATILE GLOBAL g_1834 */
static int32_t * volatile g_1835[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int16_t **g_1846 = &g_1327[4];
static int32_t * volatile g_1880 = &g_1507;/* VOLATILE GLOBAL g_1880 */
static const int16_t g_1903 = 0x7482L;
static const int16_t g_1905 = 0xCA23L;
static const int16_t *g_1904[8][5][4] = {{{&g_1905,&g_1903,&g_1905,&g_1903},{(void*)0,(void*)0,&g_1905,(void*)0},{(void*)0,&g_1905,&g_1903,(void*)0},{&g_1905,&g_1903,&g_1903,&g_1905},{(void*)0,&g_1903,&g_1905,&g_1905}},{{(void*)0,(void*)0,&g_1905,&g_1905},{&g_1905,&g_1905,(void*)0,&g_1903},{&g_1903,(void*)0,(void*)0,(void*)0},{&g_1903,(void*)0,(void*)0,&g_1903},{(void*)0,&g_1905,&g_1903,&g_1905}},{{&g_1903,(void*)0,(void*)0,&g_1905},{&g_1903,&g_1903,&g_1903,&g_1905},{&g_1905,&g_1903,(void*)0,(void*)0},{&g_1905,&g_1905,&g_1903,(void*)0},{&g_1903,(void*)0,(void*)0,&g_1903}},{{&g_1903,&g_1903,&g_1903,&g_1903},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_1903,&g_1905,(void*)0,(void*)0},{&g_1903,(void*)0,(void*)0,&g_1903},{&g_1905,&g_1903,&g_1905,&g_1903}},{{(void*)0,(void*)0,&g_1905,(void*)0},{(void*)0,&g_1905,&g_1903,(void*)0},{&g_1905,&g_1903,&g_1903,&g_1905},{(void*)0,&g_1903,&g_1905,&g_1905},{(void*)0,(void*)0,&g_1905,&g_1905}},{{&g_1905,&g_1905,(void*)0,&g_1903},{&g_1903,(void*)0,(void*)0,(void*)0},{&g_1903,(void*)0,(void*)0,&g_1903},{(void*)0,&g_1905,&g_1903,&g_1905},{&g_1903,(void*)0,(void*)0,&g_1905}},{{&g_1903,&g_1903,&g_1903,&g_1905},{&g_1905,&g_1903,(void*)0,(void*)0},{&g_1905,&g_1905,&g_1903,(void*)0},{&g_1903,(void*)0,(void*)0,&g_1903},{&g_1903,&g_1903,&g_1903,&g_1903}},{{(void*)0,(void*)0,(void*)0,(void*)0},{&g_1903,&g_1905,(void*)0,(void*)0},{&g_1903,(void*)0,(void*)0,&g_1903},{&g_1905,&g_1903,&g_1905,&g_1903},{(void*)0,(void*)0,&g_1905,(void*)0}}};
static volatile int64_t g_1921 = 0xADA0AAAC1FD2017DLL;/* VOLATILE GLOBAL g_1921 */
static volatile int64_t * const  volatile g_1920 = &g_1921;/* VOLATILE GLOBAL g_1920 */
static volatile int64_t * const  volatile *g_1919 = &g_1920;
static volatile int64_t * const  volatile ** volatile g_1918 = &g_1919;/* VOLATILE GLOBAL g_1918 */
static uint32_t g_1931 = 0xE02E650EL;
static int16_t ***g_1999[6][2] = {{&g_1846,&g_1846},{&g_1846,&g_1846},{&g_1846,&g_1846},{&g_1846,&g_1846},{&g_1846,&g_1846},{&g_1846,&g_1846}};
static const volatile uint32_t g_2000 = 0xED748848L;/* VOLATILE GLOBAL g_2000 */
static int8_t * const g_2061 = (void*)0;
static int8_t * const *g_2060 = &g_2061;
static int8_t * const **g_2059[8][4][5] = {{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}},{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}},{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}},{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}},{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}},{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}},{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}},{{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060},{&g_2060,&g_2060,&g_2060,&g_2060,&g_2060}}};
static int8_t * const ***g_2058 = &g_2059[7][0][4];
static uint32_t *g_2101 = &g_1931;
static uint32_t **g_2100[3][8][3] = {{{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,(void*)0},{(void*)0,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{(void*)0,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,(void*)0}},{{(void*)0,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{(void*)0,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101}},{{&g_2101,&g_2101,&g_2101},{(void*)0,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{&g_2101,&g_2101,&g_2101},{(void*)0,&g_2101,&g_2101}}};
static uint32_t * volatile * volatile g_2103[7] = {&g_2101,&g_2101,&g_2101,&g_2101,&g_2101,&g_2101,&g_2101};
static int32_t *g_2122 = &g_478[3][3][0];
static int32_t ** volatile g_2121 = &g_2122;/* VOLATILE GLOBAL g_2121 */
static int32_t g_2126 = 0x491951F5L;
static int32_t g_2138 = 0x68152002L;
static volatile uint32_t * volatile * const * volatile * volatile *g_2139 = (void*)0;
static int32_t g_2169 = 0x85886D2AL;
static int64_t g_2261 = 0x7A5A2D37D2AA42C4LL;
static uint64_t *g_2300[1][10] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static const volatile uint8_t g_2318[7][3][2] = {{{248UL,1UL},{0UL,0x48L},{0UL,1UL}},{{248UL,4UL},{1UL,0xC4L},{246UL,0x47L}},{{6UL,1UL},{1UL,248UL},{0x3DL,248UL}},{{1UL,1UL},{6UL,0x47L},{246UL,0xC4L}},{{1UL,4UL},{248UL,1UL},{0UL,0x48L}},{{0UL,1UL},{248UL,4UL},{1UL,0xC4L}},{{246UL,0x47L},{6UL,1UL},{1UL,248UL}}};
static volatile int64_t g_2340 = 0xC99C4DB73D5EAA97LL;/* VOLATILE GLOBAL g_2340 */
static int32_t * volatile g_2349 = &g_1507;/* VOLATILE GLOBAL g_2349 */
static int32_t * volatile g_2399 = (void*)0;/* VOLATILE GLOBAL g_2399 */
static int64_t *****g_2511 = (void*)0;
static int64_t g_2531 = 1L;
static const volatile uint32_t g_2570 = 0x0A0EE0BAL;/* VOLATILE GLOBAL g_2570 */
static int16_t g_2595[9][4] = {{(-1L),(-1L),5L,0x3B65L},{(-1L),(-1L),(-1L),(-1L)},{0L,0x3B65L,0x6C75L,(-1L)},{0x3B65L,(-1L),0x164AL,0x4E15L},{0x91DAL,0L,(-1L),0x4E15L},{0x67A6L,(-1L),0x67A6L,(-1L)},{(-1L),0x3B65L,0x4C13L,(-1L)},{0x91DAL,(-1L),0x6C75L,0x3B65L},{(-1L),(-1L),0x6C75L,0x6C75L}};
static uint16_t *g_2624[7] = {&g_83[2],&g_83[2],&g_83[2],&g_83[2],&g_83[2],&g_83[2],&g_83[2]};
static uint16_t **g_2623 = &g_2624[6];
static uint16_t ***g_2622 = &g_2623;
static uint16_t **** volatile g_2621[3][2][3] = {{{&g_2622,&g_2622,&g_2622},{&g_2622,&g_2622,&g_2622}},{{&g_2622,&g_2622,&g_2622},{&g_2622,&g_2622,&g_2622}},{{&g_2622,&g_2622,&g_2622},{&g_2622,&g_2622,&g_2622}}};
static uint16_t **** volatile g_2625 = &g_2622;/* VOLATILE GLOBAL g_2625 */
static int32_t ** const  volatile g_2683 = &g_257;/* VOLATILE GLOBAL g_2683 */
static volatile int64_t g_2714 = (-8L);/* VOLATILE GLOBAL g_2714 */
static volatile uint32_t ***g_2734 = &g_988;
static volatile uint32_t ****g_2733[2] = {&g_2734,&g_2734};
static int32_t ***g_2773 = (void*)0;
static int32_t ****g_2772[6][9] = {{&g_2773,&g_2773,&g_2773,&g_2773,&g_2773,(void*)0,&g_2773,&g_2773,(void*)0},{&g_2773,(void*)0,&g_2773,(void*)0,(void*)0,&g_2773,&g_2773,&g_2773,&g_2773},{(void*)0,&g_2773,(void*)0,&g_2773,&g_2773,(void*)0,&g_2773,&g_2773,(void*)0},{&g_2773,&g_2773,&g_2773,(void*)0,&g_2773,&g_2773,(void*)0,(void*)0,(void*)0},{&g_2773,(void*)0,&g_2773,&g_2773,(void*)0,&g_2773,&g_2773,&g_2773,(void*)0},{&g_2773,&g_2773,&g_2773,&g_2773,&g_2773,(void*)0,&g_2773,&g_2773,&g_2773}};
static int32_t *****g_2771 = &g_2772[0][8];
static uint16_t g_2827[1][4][1] = {{{4UL},{4UL},{4UL},{4UL}}};
static volatile int64_t g_2885 = 8L;/* VOLATILE GLOBAL g_2885 */
static int32_t *g_2987[2][5] = {{&g_2126,&g_128,&g_128,&g_2126,&g_128},{&g_2126,&g_2126,&g_2138,&g_2126,&g_2126}};
static int32_t **g_2986 = &g_2987[0][2];
static volatile uint16_t g_3001 = 0xC737L;/* VOLATILE GLOBAL g_3001 */
static uint8_t g_3336 = 1UL;
static const int32_t g_3350 = 0L;
static const int32_t *g_3349 = &g_3350;
static const uint32_t g_3374 = 0UL;
static volatile int64_t g_3381 = (-1L);/* VOLATILE GLOBAL g_3381 */
static int8_t *g_3428 = &g_1648;
static int32_t ***g_3434 = &g_2986;
static int32_t ****g_3433 = &g_3434;
static int32_t *****g_3432 = &g_3433;
static volatile int32_t g_3449[8] = {0x31BB6663L,0x31BB6663L,0x31BB6663L,0x31BB6663L,0x31BB6663L,0x31BB6663L,0x31BB6663L,0x31BB6663L};


/* --- FORWARD DECLARATIONS --- */
static uint8_t  func_1(void);
static int16_t  func_11(const uint32_t  p_12);
static int64_t  func_30(uint32_t  p_31);
static int32_t  func_34(const int8_t  p_35, int32_t * p_36);
static int8_t  func_37(const int64_t  p_38, int64_t  p_39, uint8_t  p_40, const uint32_t  p_41, uint64_t  p_42);
static int8_t  func_61(uint32_t  p_62, uint32_t * p_63, uint64_t  p_64, int32_t  p_65);
static uint32_t * func_66(int32_t  p_67);
static int8_t  func_70(int32_t  p_71, int8_t  p_72, int32_t * const  p_73);
static int64_t  func_92(int16_t  p_93, uint8_t * p_94, int32_t * p_95, uint8_t  p_96, int16_t  p_97);
static int16_t  func_102(const int16_t * p_103, uint16_t * p_104, uint32_t * p_105, uint8_t  p_106, uint32_t  p_107);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_14 g_2122 g_17 g_2623 g_2624 g_83 g_1504 g_54 g_626 g_2622 g_3336 g_261 g_754 g_755 g_445 g_359 g_360 g_1069 g_1070 g_2734 g_988 g_989 g_1846 g_1327 g_151 g_3381 g_609 g_610 g_143 g_1352 g_1035 g_4 g_23 g_478 g_1799 g_2531 g_47 g_990 g_1919 g_1920 g_1921 g_2349 g_730
 * writes: g_3 g_4 g_17 g_23 g_22 g_478 g_257 g_54 g_83 g_261 g_155 g_3349 g_730 g_151 g_128 g_610 g_47 g_1507 g_3428 g_3432
 */
static uint8_t  func_1(void)
{ /* block id: 0 */
    int8_t l_2[5][4][2] = {{{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)}},{{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)}},{{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)}},{{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)}},{{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)},{(-2L),(-2L)}}};
    int32_t l_3316 = 0x843DB907L;
    int32_t l_3317 = (-4L);
    int64_t l_3379 = 0x67136A6D655E1C4ELL;
    int32_t *****l_3380 = &g_2772[4][4];
    uint32_t l_3403 = 0x299BFFD2L;
    int64_t l_3406 = 0x69AA31D30C6EDD0ELL;
    int8_t ***l_3421 = &g_1321;
    int8_t **** const l_3420 = &l_3421;
    int8_t **** const *l_3419 = &l_3420;
    int8_t *l_3427 = &g_1648;
    int32_t ***l_3431[9][6][2] = {{{&g_2986,&g_2986},{&g_2986,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_2986},{&g_2986,(void*)0},{&g_2986,&g_2986}},{{&g_2986,(void*)0},{&g_2986,&g_2986},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_2986,&g_2986},{&g_2986,&g_2986}},{{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986},{(void*)0,(void*)0},{&g_2986,&g_2986},{&g_2986,&g_2986}},{{&g_2986,(void*)0},{(void*)0,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986}},{{&g_2986,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_2986},{&g_2986,(void*)0},{&g_2986,&g_2986},{&g_2986,(void*)0}},{{&g_2986,&g_2986},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986}},{{&g_2986,&g_2986},{&g_2986,&g_2986},{(void*)0,(void*)0},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,(void*)0}},{{(void*)0,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,(void*)0}},{{(void*)0,(void*)0},{&g_2986,(void*)0},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,&g_2986},{&g_2986,(void*)0}}};
    int32_t ****l_3430[10][10] = {{&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1]},{&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0]},{&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0]},{&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1]},{&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0]},{&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0]},{&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1]},{&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0],&l_3431[0][2][0],&l_3431[7][5][1],&l_3431[0][2][0]},{&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0]},{&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1],&l_3431[0][2][0],&l_3431[4][2][1],&l_3431[4][2][1]}};
    int32_t *****l_3429 = &l_3430[7][1];
    uint16_t l_3440 = 0xE597L;
    int32_t l_3448 = 0xBE4145AAL;
    uint16_t l_3450 = 65535UL;
    uint32_t l_3466 = 0x4C107828L;
    int i, j, k;
    for (g_3 = 0; (g_3 <= 1); g_3 += 1)
    { /* block id: 3 */
        uint32_t l_3311 = 4294967292UL;
        int32_t ***l_3314 = &g_626;
        int32_t l_3318[7] = {0x6AB70EBDL,0x6AB70EBDL,0x6AB70EBDL,0x6AB70EBDL,0x6AB70EBDL,0x6AB70EBDL,0x6AB70EBDL};
        int64_t l_3338 = 0x320E37EB56013F57LL;
        uint16_t l_3405[8][7] = {{65534UL,65535UL,3UL,0x55A7L,3UL,65535UL,65534UL},{0x668BL,65534UL,0x7BEAL,0x7BEAL,65534UL,0x668BL,0x7BEAL},{0x81F2L,0x55A7L,65534UL,0x55A7L,0x81F2L,0xC6B5L,0x81F2L},{65534UL,0x7BEAL,0x7BEAL,65534UL,0x668BL,0x7BEAL,8UL},{3UL,0x55A7L,3UL,65535UL,65534UL,65535UL,3UL},{65534UL,65534UL,0xDEE9L,8UL,65534UL,65528UL,8UL},{0x81F2L,65535UL,0x088BL,0x55A7L,0x088BL,65535UL,0x81F2L},{0x668BL,8UL,0x7BEAL,0x668BL,65534UL,0x7BEAL,0x7BEAL}};
        int i, j;
        for (g_4 = 0; (g_4 <= 1); g_4 += 1)
        { /* block id: 6 */
            const int32_t l_3310 = 0xFD684D95L;
            int32_t ***l_3313 = &g_626;
            int32_t ****l_3315 = &l_3314;
            int32_t *l_3351 = &g_151;
            int64_t l_3376 = 0x57950176294C396BLL;
            l_3317 &= (safe_add_func_int16_t_s_s((l_2[3][1][1] ^ (safe_lshift_func_int16_t_s_s((safe_lshift_func_int16_t_s_u(func_11(l_2[0][3][1]), 1)), 10))), (safe_sub_func_uint32_t_u_u(((safe_add_func_int32_t_s_s((~(safe_mul_func_int16_t_s_s(((safe_mul_func_int32_t_s_s((!(l_3316 = ((safe_rshift_func_int16_t_s_s(((safe_mod_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u(((((safe_div_func_int32_t_s_s((safe_div_func_int8_t_s_s((safe_div_func_uint32_t_u_u(1UL, (safe_lshift_func_int64_t_s_s(l_3310, l_3311)))), (l_2[4][3][1] & (~(l_3313 != ((*l_3315) = l_3314)))))), l_2[3][1][1])) != l_2[3][3][1]) , l_3311) < l_2[4][0][1]), 1)), l_3311)) <= (**g_2623)), 9)) <= 0xCBL))), (*g_1504))) ^ l_2[0][3][0]), l_2[4][1][0]))), l_2[4][0][0])) && l_3310), 2UL))));
            (**l_3314) = func_66(l_3317);
            l_3317 = (l_3318[2] &= (l_3316 != 250UL));
            for (g_23 = 0; (g_23 <= 1); g_23 += 1)
            { /* block id: 1526 */
                int16_t l_3321 = (-8L);
                uint64_t *l_3339 = &g_261;
                int64_t *l_3347 = &g_155;
                int64_t *l_3348[5];
                uint8_t *l_3384 = &g_3336;
                int32_t l_3395 = 0x4F9C78D1L;
                int32_t l_3404[10][3] = {{(-2L),0x5AB3DD3CL,9L},{0x5AB3DD3CL,(-2L),9L},{1L,1L,9L},{(-2L),0x5AB3DD3CL,9L},{0x5AB3DD3CL,(-2L),9L},{1L,1L,9L},{0xB4E21B1AL,(-10L),1L},{(-10L),0xB4E21B1AL,1L},{0x0769F688L,0x0769F688L,1L},{0xB4E21B1AL,(-10L),1L}};
                int i, j, k;
                for (i = 0; i < 5; i++)
                    l_3348[i] = (void*)0;
                if (((safe_lshift_func_int32_t_s_s(l_3321, 2)) || (((safe_mul_func_int32_t_s_s((safe_lshift_func_int64_t_s_u((safe_div_func_int16_t_s_s((safe_add_func_int32_t_s_s((safe_mod_func_uint32_t_u_u((((((((*g_1504)--) , (g_3349 = func_66((l_3318[2] = ((safe_mod_func_uint16_t_u_u(((***g_2622) |= l_3321), (((g_3336 | (((*l_3339) ^= (l_3317 == (l_3338 = (safe_unary_minus_func_int8_t_s(0xFAL))))) > (safe_rshift_func_int32_t_s_s((safe_lshift_func_int64_t_s_u((+(0x4EL <= (**g_754))), (safe_div_func_int64_t_s_s((l_3316 &= ((*l_3347) = (((*g_2623) != (*g_359)) & l_3321))), l_3321)))), 8)))) , 0x5629C612L) ^ l_3321))) | 18446744073709551608UL))))) != l_3351) < 0xBEL) , l_3321) & l_2[2][2][0]), l_3317)), 0x68A15712L)), 0x855EL)), 52)), 6L)) , 0x05901D88L) | l_3311)))
                { /* block id: 1535 */
                    int8_t l_3372[8][3];
                    const uint32_t *l_3373 = &g_3374;
                    int i, j;
                    for (i = 0; i < 8; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_3372[i][j] = (-9L);
                    }
                    (*l_3351) = (((!(safe_sub_func_uint16_t_u_u((safe_div_func_uint16_t_u_u((((safe_mul_func_uint8_t_u_u(4UL, (safe_rshift_func_int32_t_s_s((!(safe_sub_func_int64_t_s_s(((void*)0 == (*g_1069)), (safe_lshift_func_uint8_t_u_u((((safe_add_func_uint8_t_u_u(0x00L, ((**g_2734) == (l_3373 = func_66((((safe_mul_func_int16_t_s_s(((**g_1846) = ((9L || (safe_mul_func_int16_t_s_s(((*g_988) == (*g_988)), l_3372[6][2]))) || 1UL)), l_3321)) && l_3321) && l_3311)))))) > l_3321) & l_3317), 2))))), (*l_3351))))) ^ l_3317) , 0xDF5EL), l_3321)), l_3321))) , 0L) , 1L);
                    for (g_128 = 4; (g_128 >= 0); g_128 -= 1)
                    { /* block id: 1541 */
                        (*l_3351) &= ((*g_2122) = 0x90A47620L);
                    }
                }
                else
                { /* block id: 1545 */
                    uint16_t l_3375 = 65535UL;
                    (*g_2122) = ((l_3375 , 0xAB10CE612AEC9498LL) & ((l_3376 | 1UL) >= ((safe_mul_func_uint16_t_u_u((((((l_3379 <= (((((-1L) <= (((*l_3351) = (&g_2772[1][1] == (l_3380 = &g_2772[1][0]))) > ((g_3381 || 0UL) == (***g_2622)))) & l_3321) || l_3321) , l_3321)) >= 0x88L) & 0x57L) ^ 0x536C672359D28F4ALL) < (**g_609)), l_3375)) , 0UL)));
                }
                (*g_2122) = (((((safe_add_func_int16_t_s_s((((*g_609) = l_3384) == (((safe_lshift_func_uint8_t_u_u(((safe_rshift_func_uint32_t_u_u((l_3404[6][0] |= (safe_lshift_func_uint64_t_u_u((safe_rshift_func_int8_t_s_s(((safe_mod_func_int8_t_s_s(l_3395, ((&g_2733[1] == (void*)0) & 18446744073709551615UL))) & (safe_add_func_int32_t_s_s((*g_1352), (safe_mod_func_uint16_t_u_u(((l_2[(g_4 + 3)][(g_3 + 2)][g_23] = (-9L)) & (((*g_2122) >= (safe_div_func_int32_t_s_s((((~(l_3318[2] , (*l_3351))) ^ l_3321) >= l_3403), (*g_2122)))) , g_1799)), 0x55A5L))))), l_3395)), l_3395))), 15)) > 0UL), 7)) < l_3405[0][2]) , &g_100)), 0UL)) == 1L) >= g_2531) , l_2[(g_4 + 3)][(g_3 + 2)][g_23]) >= l_3406);
            }
        }
    }
    for (g_47 = (-25); (g_47 == 13); g_47 = safe_add_func_uint16_t_u_u(g_47, 7))
    { /* block id: 1559 */
        int32_t l_3418[10] = {9L,9L,9L,9L,9L,9L,9L,9L,9L,9L};
        int8_t *l_3426 = (void*)0;
        int i;
        for (g_1507 = 0; (g_1507 != 22); g_1507 = safe_add_func_uint64_t_u_u(g_1507, 2))
        { /* block id: 1562 */
            int64_t l_3415[4] = {1L,1L,1L,1L};
            int8_t ****l_3423 = &l_3421;
            int8_t *****l_3422[10] = {&l_3423,(void*)0,(void*)0,&l_3423,(void*)0,&l_3423,(void*)0,(void*)0,&l_3423,(void*)0};
            int32_t ***l_3425 = &g_2986;
            int32_t ****l_3424 = &l_3425;
            int i;
            (*g_626) = func_66((safe_rshift_func_uint64_t_u_s((safe_sub_func_uint32_t_u_u(((((l_3415[0] <= (safe_add_func_uint64_t_u_u((l_3418[4] , ((((l_3419 != l_3422[6]) , ((*l_3424) = &g_2986)) == (void*)0) & 0x1FL)), (l_3426 != (g_3428 = (l_3427 = l_3426)))))) != l_3415[0]) & 0x22L) > 0x70BBD237L), l_3415[0])), 31)));
        }
    }
    g_3432 = l_3429;
    for (g_151 = 0; (g_151 > (-11)); g_151--)
    { /* block id: 1572 */
        uint16_t l_3443[9][3] = {{0x707DL,65535UL,0UL},{0x2CBBL,0x2CBBL,0x9605L},{0x707DL,65535UL,0UL},{0x2CBBL,0x2CBBL,0x9605L},{0x707DL,65535UL,0UL},{0x2CBBL,0x2CBBL,0x9605L},{0x707DL,65535UL,0UL},{0x2CBBL,0x2CBBL,0x9605L},{0x707DL,65535UL,0UL}};
        int8_t l_3444 = 0xD4L;
        int8_t l_3445 = 1L;
        int32_t l_3446 = 0x2A4637D1L;
        int32_t *l_3447[10] = {&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0],&g_478[3][3][0]};
        const uint32_t l_3459[6] = {5UL,5UL,5UL,5UL,5UL,5UL};
        int32_t l_3474 = 0xE285CF6AL;
        uint32_t l_3475[2];
        const int64_t l_3476 = 0xC5D06A92357FA4FCLL;
        int32_t l_3477 = (-2L);
        int8_t * const l_3478 = &l_2[4][0][0];
        int8_t *l_3479 = &l_2[4][3][1];
        int i, j;
        for (i = 0; i < 2; i++)
            l_3475[i] = 4294967290UL;
        (*g_2349) = ((((((+(((*g_610) || ((safe_mod_func_uint32_t_u_u(l_3406, (***g_2734))) <= l_3440)) , ((((**g_1919) || ((0x694FL | (safe_add_func_int32_t_s_s(((&g_2100[0][1][0] != &g_2100[1][0][1]) > (((*g_2122) = l_3443[7][1]) >= (*g_1504))), (-8L)))) >= l_3443[2][2])) != l_3443[7][1]) == (*g_1504)))) < l_3444) , l_3443[5][0]) <= 0x6730426BL) , 8UL) < 0x7B8AL);
        l_3446 &= l_3445;
        ++l_3450;
        l_3317 &= (((l_3379 < ((((l_3440 != ((safe_lshift_func_int32_t_s_u(((safe_lshift_func_int8_t_s_u(((safe_rshift_func_uint16_t_u_s(l_3459[1], l_3403)) | (safe_add_func_int32_t_s_s(((*g_2122) |= ((safe_div_func_uint16_t_u_u((l_3466 = (((***g_2622) = l_3403) <= (~(+250UL)))), ((**g_1846) = (safe_lshift_func_uint16_t_u_u(((0x16L > (safe_add_func_int8_t_s_s((((((safe_lshift_func_int64_t_s_s((!l_3450), ((**g_1846) , l_3474))) | 0x67A3514DL) < (*g_989)) || l_3475[0]) | l_3403), 251UL))) , 0xFDD4L), 7))))) || l_3476)), l_3406))), 7)) ^ 0x52CDC923L), (*g_1504))) > l_3477)) < l_3316) , l_3478) == l_3479)) & 0x99L) != 0x9CC8L);
    }
    return l_3448;
}


/* ------------------------------------------ */
/* 
 * reads : g_17 g_14 g_2122
 * writes: g_17 g_23 g_22 g_478
 */
static int16_t  func_11(const uint32_t  p_12)
{ /* block id: 7 */
    int32_t *l_2456 = &g_17;
    for (g_17 = 19; (g_17 > 13); g_17 = safe_sub_func_int16_t_s_s(g_17, 3))
    { /* block id: 10 */
        uint8_t l_26 = 0xD5L;
        volatile int32_t *l_27[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
        int64_t *l_45 = (void*)0;
        int64_t *l_46[6] = {&g_47,&g_47,&g_47,&g_47,&g_47,&g_47};
        int64_t *l_48 = &g_47;
        uint32_t *l_49 = (void*)0;
        uint32_t *l_50 = &g_51;
        int i;
        for (g_23 = (-14); (g_23 == 28); g_23 = safe_add_func_uint16_t_u_u(g_23, 2))
        { /* block id: 13 */
            l_26 ^= (p_12 == g_14);
        }
        g_22 = g_14;
    }
    (*g_2122) = 7L;
    return (*l_2456);
}


/* ------------------------------------------ */
/* 
 * reads : g_2734 g_988 g_989 g_990 g_261 g_3001 g_609 g_610 g_143 g_1068 g_1069 g_119 g_2771 g_2772 g_1352 g_1035 g_2625 g_2622 g_2623 g_60 g_23 g_1920 g_1921 g_235 g_236 g_47 g_626 g_2349 g_1507 g_490 g_100 g_1614 g_1615 g_1616 g_1617 g_1618 g_129 g_1880 g_356 g_728 g_2624 g_359 g_360 g_218 g_730 g_2138 g_152 g_1557 g_1558 g_1559
 * writes: g_2986 g_261 g_143 g_119 g_356 g_478 g_257 g_54 g_1069 g_490 g_1999 g_100 g_205 g_129 g_728 g_445 g_2261 g_23 g_218 g_730 g_2138 g_152
 */
static int64_t  func_30(uint32_t  p_31)
{ /* block id: 1351 */
    int64_t l_2969[3][3] = {{0xA0AEBFB471F320A0LL,0xA0AEBFB471F320A0LL,0xA0AEBFB471F320A0LL},{0x39C4EE148E099297LL,0x39C4EE148E099297LL,0x39C4EE148E099297LL},{0xA0AEBFB471F320A0LL,0xA0AEBFB471F320A0LL,0xA0AEBFB471F320A0LL}};
    int8_t l_2970 = 0L;
    int32_t * const l_2973 = (void*)0;
    int32_t **l_2988 = &g_2987[0][2];
    int32_t **l_2989 = &g_2987[1][4];
    const int8_t *l_2999 = &g_445[2];
    int32_t l_3003[9] = {(-1L),0xB50C1EABL,(-1L),0xB50C1EABL,(-1L),0xB50C1EABL,(-1L),0xB50C1EABL,(-1L)};
    int32_t *l_3043 = (void*)0;
    uint8_t l_3125 = 0xBFL;
    int64_t ***l_3128[3][5][9] = {{{&g_330,&g_330,&g_330,(void*)0,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,(void*)0},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330}},{{&g_330,(void*)0,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,(void*)0,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{(void*)0,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330}},{{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,(void*)0,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,(void*)0,&g_330,&g_330,&g_330},{&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330,&g_330}}};
    int32_t l_3268[5][1][1];
    int i, j, k;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
                l_3268[i][j][k] = 0xC78FF621L;
        }
    }
lbl_3285:
    if ((safe_rshift_func_int8_t_s_u((((safe_rshift_func_uint64_t_u_s((safe_div_func_uint16_t_u_u((safe_add_func_int64_t_s_s(p_31, (((p_31 ^ ((l_2969[2][2] , (((l_2970 && (p_31 < (((((safe_mul_func_uint32_t_u_u(((void*)0 == l_2973), ((+((+(~5UL)) != l_2969[1][1])) , l_2970))) && p_31) ^ (-8L)) < p_31) == p_31))) >= (-1L)) & 0x11B8L)) , (***g_2734))) & 0x3EL) != l_2969[0][2]))), 0x7986L)), p_31)) < l_2969[2][2]) <= p_31), l_2969[2][2])))
    { /* block id: 1352 */
        int32_t *l_2984 = &g_2138;
        int32_t **l_2983 = &l_2984;
        int32_t ***l_2985[5][9][5] = {{{&l_2983,&l_2983,(void*)0,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,(void*)0,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983}},{{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,(void*)0,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,(void*)0,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983}},{{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,(void*)0,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,(void*)0,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983}},{{&l_2983,&l_2983,&l_2983,&l_2983,(void*)0},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,(void*)0,&l_2983},{&l_2983,&l_2983,&l_2983,(void*)0,&l_2983},{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{(void*)0,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983}},{{&l_2983,(void*)0,&l_2983,&l_2983,&l_2983},{(void*)0,&l_2983,&l_2983,&l_2983,(void*)0},{&l_2983,&l_2983,(void*)0,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,(void*)0,&l_2983},{(void*)0,&l_2983,&l_2983,(void*)0,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,(void*)0},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,&l_2983,&l_2983},{&l_2983,&l_2983,&l_2983,(void*)0,&l_2983}}};
        uint64_t *l_2992 = &g_261;
        uint64_t l_3000 = 0x22A116D278A62843LL;
        int32_t l_3002[6][6] = {{0x2ECF8930L,0x2D9B26BDL,0x2D9B26BDL,0x2ECF8930L,0L,9L},{0L,0x2ECF8930L,0x2D9B26BDL,0x2D9B26BDL,0x2ECF8930L,0L},{0L,0x2D9B26BDL,9L,0x2ECF8930L,0x2ECF8930L,9L},{0x2ECF8930L,0x2ECF8930L,9L,0x2D9B26BDL,0L,0L},{0x2ECF8930L,0x2D9B26BDL,0x2D9B26BDL,0x2ECF8930L,0L,9L},{0L,0x2ECF8930L,0x2D9B26BDL,0x2D9B26BDL,0x2ECF8930L,0L}};
        int64_t *****l_3005 = &g_328;
        uint16_t **l_3013 = &g_2624[6];
        int i, j, k;
        l_3003[5] ^= (safe_lshift_func_int32_t_s_u((((safe_rshift_func_int32_t_s_u((safe_rshift_func_int32_t_s_s((((g_2986 = l_2983) != (l_2989 = (l_2988 = l_2988))) == (l_2969[2][2] == (l_2970 || ((safe_rshift_func_uint64_t_u_u((p_31 , (++(*l_2992))), 43)) , p_31)))), (l_3002[1][2] &= (((0xFBD50065L & (safe_lshift_func_int8_t_s_u(((safe_mul_func_uint64_t_u_u((l_2999 != &l_2970), 0x268248AE00EF360BLL)) && p_31), 3))) | l_3000) >= g_3001)))), l_2970)) && p_31) & 247UL), 12));
        if ((~18446744073709551615UL))
        { /* block id: 1359 */
            int32_t l_3014 = 0L;
            int32_t *l_3015 = (void*)0;
            int32_t *l_3016 = &l_3003[5];
            (*l_3016) ^= (l_3000 >= (((void*)0 == l_3005) , ((safe_div_func_uint32_t_u_u((((safe_lshift_func_int64_t_s_s((((((**g_609) = (+(-2L))) | (((0x5E85C805L >= (((safe_lshift_func_uint32_t_u_u(((void*)0 != l_3013), (((*l_2992) &= (((0x14DF7CD7L <= l_2969[2][2]) != l_3002[4][4]) , 18446744073709551615UL)) ^ l_3014))) , 0x49EBL) & l_2969[0][1])) || 0x268AFCD21F5AB5A4LL) & (-6L))) || l_3014) > p_31), l_3000)) == 0UL) , 0UL), p_31)) , p_31)));
            l_3002[1][4] &= (safe_add_func_uint64_t_u_u(((*l_2992)++), (safe_lshift_func_int16_t_s_s(((**g_609) || (!(safe_lshift_func_uint64_t_u_u((*l_3016), 44)))), 6))));
        }
        else
        { /* block id: 1365 */
            uint64_t l_3030 = 0xB5C52BA4FD479AE4LL;
            int8_t ***l_3036 = &g_1321;
            int8_t ****l_3035 = &l_3036;
            l_3002[1][2] &= (p_31 >= (((0L ^ p_31) > (safe_lshift_func_int16_t_s_s((safe_mul_func_int16_t_s_s((l_3030 > p_31), (safe_div_func_int64_t_s_s((safe_rshift_func_uint32_t_u_u((((l_3035 != (*g_1068)) != (safe_div_func_int8_t_s_s((safe_div_func_int8_t_s_s(((65535UL == (((safe_unary_minus_func_uint32_t_u(l_3030)) > l_2969[2][2]) ^ p_31)) | p_31), l_3030)), 0x04L))) && 0L), 12)), l_3030)))), p_31))) > p_31));
        }
        return p_31;
    }
    else
    { /* block id: 1369 */
        int32_t *l_3042 = &l_3003[1];
        l_3043 = l_3042;
    }
    for (g_119 = 0; (g_119 >= (-30)); g_119 = safe_sub_func_uint32_t_u_u(g_119, 1))
    { /* block id: 1374 */
        uint32_t l_3056 = 0x7558ED0DL;
        int32_t l_3057 = 1L;
        int32_t l_3103 = 0x3392607CL;
        int32_t l_3107 = 0x886716F6L;
        int16_t l_3141[7];
        uint64_t l_3154 = 0x83132A2E041C5F60LL;
        int64_t l_3179 = 0xE91FCF01DA8A898ELL;
        int16_t l_3256 = 0x793CL;
        int i;
        for (i = 0; i < 7; i++)
            l_3141[i] = (-6L);
        if ((p_31 , (*l_3043)))
        { /* block id: 1375 */
            const int32_t l_3053 = 0x6E19D791L;
            (*l_3043) = ((safe_lshift_func_int64_t_s_u(((safe_rshift_func_uint32_t_u_u(0xBF784E54L, 13)) , (safe_add_func_int8_t_s_s((safe_unary_minus_func_int8_t_s((l_3053 > ((*g_2771) == (*g_2771))))), l_3053))), (safe_add_func_int32_t_s_s(p_31, l_3053)))) != l_3056);
            l_3057 = ((*g_610) > 0x3DL);
            if (l_3057)
                continue;
            if ((*g_1352))
                break;
        }
        else
        { /* block id: 1380 */
            uint16_t l_3085 = 1UL;
            int32_t l_3104 = 0x46B9CFEAL;
            int32_t l_3105 = 0xDB484F8DL;
            int32_t l_3150 = 0x36DD1236L;
            int16_t ***l_3160[10][2][6] = {{{&g_1846,(void*)0,&g_1846,(void*)0,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,&g_1846}},{{&g_1846,(void*)0,&g_1846,&g_1846,&g_1846,(void*)0},{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,(void*)0}},{{&g_1846,(void*)0,&g_1846,&g_1846,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,(void*)0,&g_1846}},{{&g_1846,(void*)0,&g_1846,&g_1846,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,&g_1846}},{{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,(void*)0}},{{&g_1846,(void*)0,&g_1846,&g_1846,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,(void*)0,&g_1846}},{{&g_1846,&g_1846,(void*)0,&g_1846,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,&g_1846}},{{&g_1846,(void*)0,(void*)0,&g_1846,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,&g_1846}},{{&g_1846,(void*)0,&g_1846,&g_1846,&g_1846,&g_1846},{(void*)0,&g_1846,&g_1846,(void*)0,&g_1846,&g_1846}},{{&g_1846,(void*)0,&g_1846,&g_1846,&g_1846,&g_1846},{&g_1846,&g_1846,&g_1846,&g_1846,&g_1846,&g_1846}}};
            uint16_t *l_3178 = &g_205;
            int i, j, k;
            if (((l_3057 &= 0x04L) <= (safe_div_func_int32_t_s_s(0x91B2B5A9L, 4294967291UL))))
            { /* block id: 1382 */
                int16_t l_3089 = 1L;
                int32_t l_3090 = 0x28B806A2L;
                int32_t l_3101 = (-4L);
                int32_t l_3106[1];
                int32_t *l_3111 = (void*)0;
                uint16_t ****l_3124 = &g_2622;
                uint16_t *****l_3123 = &l_3124;
                int i;
                for (i = 0; i < 1; i++)
                    l_3106[i] = 0x3CB0354FL;
                for (l_2970 = 16; (l_2970 < 25); l_2970 = safe_add_func_uint16_t_u_u(l_2970, 8))
                { /* block id: 1385 */
                    uint64_t *l_3086 = (void*)0;
                    uint64_t *l_3087 = &g_356;
                    uint64_t l_3088 = 0x35D431EE3173404BLL;
                    int32_t l_3102[2][2][6];
                    uint8_t l_3108 = 251UL;
                    uint16_t *****l_3122 = (void*)0;
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 2; j++)
                        {
                            for (k = 0; k < 6; k++)
                                l_3102[i][j][k] = 0L;
                        }
                    }
                    if (((((*g_610) < (safe_mod_func_int16_t_s_s(((safe_mul_func_uint16_t_u_u((((safe_add_func_uint64_t_u_u(((safe_lshift_func_int32_t_s_s((((safe_rshift_func_int16_t_s_s(0x7C59L, 6)) , (safe_rshift_func_uint8_t_u_s((p_31 > ((safe_sub_func_uint64_t_u_u((l_3057 = ((*l_3087) = (safe_sub_func_uint16_t_u_u(((safe_unary_minus_func_int16_t_s((safe_lshift_func_int64_t_s_u(((**g_2625) == (*g_2622)), 55)))) , ((((1L || ((safe_mod_func_int64_t_s_s((safe_div_func_uint32_t_u_u(p_31, 1L)), l_3085)) ^ p_31)) | l_3057) && p_31) , 0x7584L)), 0xE7E0L)))), p_31)) , 1L)), 0))) >= 2UL), 7)) == 2L), 0L)) < 0xDB82F874L) ^ p_31), (*l_3043))) <= 0x0A4BBF48F468D926LL), l_3088))) <= l_3056) & 0x8DL))
                    { /* block id: 1388 */
                        int32_t *l_3091 = (void*)0;
                        int32_t *l_3092 = &l_3090;
                        int32_t *l_3093 = (void*)0;
                        int32_t *l_3094 = &g_1035[0][0];
                        int32_t *l_3095 = &g_203;
                        int32_t *l_3096 = &g_478[3][3][0];
                        int32_t *l_3097 = &l_3090;
                        int32_t *l_3098 = &l_3003[5];
                        int32_t *l_3099 = &l_3057;
                        int32_t *l_3100[6];
                        int i;
                        for (i = 0; i < 6; i++)
                            l_3100[i] = &g_478[3][3][0];
                        (*l_3043) = (*g_60);
                        ++l_3108;
                        if (l_3107)
                            break;
                    }
                    else
                    { /* block id: 1392 */
                        int32_t l_3119 = 7L;
                        l_3111 = func_66(p_31);
                        (*l_3043) |= (*g_1352);
                        l_3125 ^= (safe_add_func_uint64_t_u_u((((l_3085 && (((((((safe_sub_func_uint16_t_u_u((l_3056 & ((*g_1920) >= ((p_31 & (safe_rshift_func_uint32_t_u_u(p_31, (((~((*l_3087) = 18446744073709551609UL)) | ((l_3119 , (((safe_add_func_int8_t_s_s(((0xC4EDBAABBA8D5F49LL >= ((l_3103 = (l_3122 == l_3123)) | p_31)) == 0L), p_31)) == p_31) == p_31)) == p_31)) && 0L)))) != p_31))), l_3119)) > l_3085) & p_31) & (-9L)) , (-1L)) || 0x466258AEL) < 65535UL)) < p_31) , p_31), (*l_3043)));
                        return (**g_235);
                    }
                    (*g_626) = &l_3101;
                }
                (*l_3043) = ((p_31 ^ ((l_3104 = 0xCBL) > p_31)) , (safe_add_func_int32_t_s_s((l_3128[0][0][4] == (void*)0), (l_3107 ^= (safe_lshift_func_uint32_t_u_s((g_54 = (safe_lshift_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_u((p_31 ^ (safe_add_func_uint32_t_u_u(((l_3105 >= p_31) , (safe_sub_func_uint16_t_u_u(p_31, (*l_3043)))), (*l_3043)))), 4)) > 0x9D5E3FDC2F22456BLL), l_3085))), (*g_2349)))))));
            }
            else
            { /* block id: 1406 */
                int8_t *l_3140 = &l_2970;
                uint8_t *l_3149 = &l_3125;
                int32_t l_3151 = 0x1DAF0E84L;
                if ((+(((((l_3140 != (void*)0) >= ((((l_3141[3] | (safe_add_func_uint64_t_u_u(p_31, ((safe_div_func_uint8_t_u_u((**g_609), ((((*l_3149) = (+(safe_lshift_func_uint64_t_u_u(l_3104, 27)))) <= ((l_3150 , (0x451EL != p_31)) , 0L)) | 5L))) , l_3151)))) && p_31) | 0x311C155DB682317ALL) || 0x0CA5L)) , 0x5DL) & l_3151) >= (-1L))))
                { /* block id: 1408 */
                    int32_t l_3157[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_3157[i] = (-1L);
                    l_3157[2] = (safe_add_func_uint8_t_u_u(((*g_610) |= (l_3154 , 1UL)), (++(*l_3149))));
                }
                else
                { /* block id: 1412 */
                    int32_t *l_3175 = &l_3104;
                    (*g_1068) = (*g_1068);
                    for (g_490 = 26; (g_490 >= (-28)); g_490--)
                    { /* block id: 1416 */
                        int16_t ****l_3161 = &g_1999[4][1];
                        (*l_3161) = l_3160[5][0][0];
                        if (p_31)
                            break;
                    }
                    if (p_31)
                        continue;
                    for (g_100 = (-29); (g_100 == 24); ++g_100)
                    { /* block id: 1423 */
                        int64_t l_3174[8][8] = {{0x501754FFFCA747C5LL,0xC92D2A66C338EAEFLL,0L,0xC92D2A66C338EAEFLL,0x501754FFFCA747C5LL,0xB072A07727DCF7A5LL,0xB072A07727DCF7A5LL,0x501754FFFCA747C5LL},{0xC92D2A66C338EAEFLL,(-1L),(-1L),0xC92D2A66C338EAEFLL,1L,0x501754FFFCA747C5LL,1L,0xC92D2A66C338EAEFLL},{(-1L),1L,(-1L),0xB072A07727DCF7A5LL,0L,0L,0xB072A07727DCF7A5LL,(-1L)},{1L,1L,0L,0x501754FFFCA747C5LL,1L,0x501754FFFCA747C5LL,0L,1L},{1L,(-1L),0xB072A07727DCF7A5LL,0L,0L,0xB072A07727DCF7A5LL,(-1L),1L},{(-1L),0xC92D2A66C338EAEFLL,1L,0x501754FFFCA747C5LL,1L,0xC92D2A66C338EAEFLL,(-1L),(-1L)},{0xC92D2A66C338EAEFLL,0x501754FFFCA747C5LL,0xB072A07727DCF7A5LL,0xB072A07727DCF7A5LL,0x501754FFFCA747C5LL,0xC92D2A66C338EAEFLL,0L,0xC92D2A66C338EAEFLL},{0x501754FFFCA747C5LL,0xC92D2A66C338EAEFLL,0L,0xC92D2A66C338EAEFLL,0x501754FFFCA747C5LL,0xB072A07727DCF7A5LL,0xB072A07727DCF7A5LL,0x501754FFFCA747C5LL}};
                        int i, j;
                        (*l_3043) = ((safe_sub_func_int64_t_s_s(((((((((*l_3043) , (((safe_mul_func_uint64_t_u_u(0x87F88CF0F68AA26BLL, (safe_sub_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u((0x92L >= l_3105), p_31)) && (safe_mul_func_int16_t_s_s((l_3104 = (p_31 , ((p_31 ^ l_3105) >= (1L || p_31)))), p_31))), 0x1BL)))) | p_31) <= (*l_3043))) < (**g_609)) && (*l_3043)) || (****g_1614)) != l_3174[5][7]) & p_31) == (**g_609)), (-7L))) == p_31);
                        l_3175 = &l_3151;
                        if (p_31)
                            break;
                    }
                }
            }
            l_3179 &= (safe_lshift_func_uint16_t_u_u(((*l_3178) = (l_3085 , (0x7CF3F6BB0F5B7DFALL != (l_3103 = p_31)))), 3));
            if ((*l_3043))
                break;
            l_3105 = ((*l_3043) = 0L);
        }
        for (g_129 = (-25); (g_129 == 49); g_129 = safe_add_func_uint16_t_u_u(g_129, 9))
        { /* block id: 1440 */
            uint32_t l_3202 = 0x529DAF1EL;
            int32_t l_3214 = 0xB72F0612L;
            int32_t l_3280[9][8] = {{(-5L),0x6BDEAB3AL,3L,0xD80FD86EL,(-5L),0L,1L,(-4L)},{1L,0x8524D5FBL,1L,(-1L),0x8524D5FBL,(-1L),0xF7D682A2L,0xFE81780DL},{(-4L),0L,0x77E94A75L,(-9L),0xF7D682A2L,(-9L),0x77E94A75L,0L},{(-5L),0x77E94A75L,0L,0x8524D5FBL,(-1L),(-1L),0xD80FD86EL,(-1L)},{(-1L),0xFE81780DL,0x31F20AB1L,(-1L),(-5L),(-1L),0xD80FD86EL,1L},{0x77E94A75L,(-1L),0L,(-1L),0x67CA0F6CL,0x45469491L,0x77E94A75L,0xF7D682A2L},{0x67CA0F6CL,0x45469491L,0x77E94A75L,0xF7D682A2L,0xF77C2A00L,0xF77C2A00L,0xF7D682A2L,0x77E94A75L},{0xFE81780DL,0xFE81780DL,1L,(-5L),0L,0x88CBE56DL,1L,0xD80FD86EL},{0L,(-1L),3L,0x77E94A75L,0x67CA0F6CL,(-9L),(-1L),0xD80FD86EL}};
            int i, j;
            l_3103 = (*g_1880);
            for (l_2970 = (-4); (l_2970 >= 6); l_2970 = safe_add_func_int64_t_s_s(l_2970, 9))
            { /* block id: 1444 */
                uint16_t ***l_3194[1][4] = {{&g_2623,&g_2623,&g_2623,&g_2623}};
                int32_t l_3196 = 0x2BEE1975L;
                int32_t l_3218 = 0xF2F0CECBL;
                int32_t l_3281[4];
                int i, j;
                for (i = 0; i < 4; i++)
                    l_3281[i] = 0xA928CE3DL;
                for (g_356 = 0; (g_356 >= 8); ++g_356)
                { /* block id: 1447 */
                    uint16_t ***l_3195 = &g_2623;
                    int32_t l_3199 = 1L;
                    for (g_490 = 0; (g_490 > (-5)); --g_490)
                    { /* block id: 1450 */
                        uint32_t *l_3190 = (void*)0;
                        uint32_t *l_3191 = &g_728;
                        int8_t *l_3215 = &g_445[7];
                        uint64_t *l_3216 = &g_261;
                        int16_t *l_3217 = &l_3141[3];
                        (*l_3043) = p_31;
                        l_3196 = (((*l_3191)--) | (0xE1D6L > (l_3194[0][3] != l_3195)));
                        l_3218 &= (((((*l_3191)--) & l_3199) , (safe_mod_func_int32_t_s_s(l_3202, ((*l_3191) = (safe_mul_func_int16_t_s_s(((*l_3217) |= (((((*l_3216) &= ((safe_lshift_func_int64_t_s_s(0xE3D7DBDA8D8B26C8LL, 27)) , (safe_mod_func_int64_t_s_s((0UL && ((l_3103 = (safe_add_func_int16_t_s_s(p_31, p_31))) & (65534UL != (p_31 == ((~(safe_mod_func_uint8_t_u_u((((((*l_3215) = (l_3214 &= (p_31 != (*l_3043)))) == 0xF4L) & l_3196) , 0x68L), 0x86L))) , l_3196))))), p_31)))) , l_3154) ^ 4294967287UL) , (-6L))), l_3199)))))) ^ 1UL);
                    }
                    if (p_31)
                        break;
                    for (g_2261 = 0; g_2261 < 9; g_2261 += 1)
                    {
                        l_3003[g_2261] = 0L;
                    }
                }
                if (p_31)
                    continue;
                if ((safe_sub_func_int32_t_s_s(p_31, l_3196)))
                { /* block id: 1467 */
                    int64_t l_3223 = (-4L);
                    uint64_t l_3230 = 0x9B9ABEC008FE5B2DLL;
                    uint16_t l_3243[4] = {0x8B3AL,0x8B3AL,0x8B3AL,0x8B3AL};
                    uint64_t *l_3251[4];
                    int32_t l_3252 = 0x308409B3L;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_3251[i] = (void*)0;
                    for (g_23 = 0; (g_23 != 16); ++g_23)
                    { /* block id: 1470 */
                        return l_3223;
                    }
                    (*l_3043) = (((0x22BAL >= (l_3218 = (safe_sub_func_int8_t_s_s((safe_mul_func_int8_t_s_s((((l_3107 = p_31) == (p_31 | ((((*g_610) = (**g_609)) == ((safe_rshift_func_int8_t_s_s(p_31, l_3230)) > l_3218)) | p_31))) < (0x84CB2907L && l_3202)), l_3214)), p_31)))) | 0x1B02L) , 8L);
                    if ((safe_lshift_func_int8_t_s_u(((((*l_3043) , ((safe_mul_func_int32_t_s_s((*g_2349), ((void*)0 == &l_3141[1]))) & (l_3252 = ((*l_3043) = (safe_mul_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((safe_rshift_func_uint64_t_u_u((l_3214 = l_3179), (safe_add_func_uint16_t_u_u((l_3243[2]++), l_3223)))), 14)), (safe_mod_func_int64_t_s_s(((safe_unary_minus_func_uint64_t_u((((-4L) || (safe_add_func_uint16_t_u_u(((**g_2622) != (*g_359)), l_3202))) != p_31))) & 4294967290UL), 0x0062EE69E206B3C1LL)))))))) > l_3196) > l_3107), (**g_609))))
                    { /* block id: 1481 */
                        if (p_31)
                            break;
                    }
                    else
                    { /* block id: 1483 */
                        int64_t l_3255 = 1L;
                        int16_t *l_3267 = &g_730[8][0];
                        l_3196 |= ((*l_3043) = p_31);
                        l_3196 = (l_3230 , 0x4DFBD77AL);
                        l_3218 ^= (safe_mul_func_int16_t_s_s(((*l_3267) &= ((((0x1CL ^ ((l_3107 &= 0x76F77CCD713B87E1LL) == (l_3196 = l_3255))) , p_31) > l_3256) , (((safe_add_func_int32_t_s_s(1L, (safe_add_func_uint64_t_u_u((((safe_lshift_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_u((g_218 |= (*g_610)), (safe_lshift_func_uint8_t_u_s(0x55L, 4)))), 4)) , ((p_31 == 0x1D25L) & (**g_988))) == p_31), 0xC12C273B3B3619B6LL)))) > p_31) >= l_3103))), p_31));
                    }
                }
                else
                { /* block id: 1493 */
                    uint8_t l_3271 = 255UL;
                    l_3268[2][0][0] = p_31;
                    for (g_2138 = 0; (g_2138 == 10); g_2138 = safe_add_func_uint64_t_u_u(g_2138, 6))
                    { /* block id: 1497 */
                        l_3271++;
                    }
                    for (g_152 = 0; (g_152 != 55); g_152++)
                    { /* block id: 1502 */
                        int32_t *l_3276 = &g_149;
                        int32_t *l_3277 = &g_203;
                        int32_t *l_3278 = &l_3003[1];
                        int32_t *l_3279[1];
                        uint64_t l_3282 = 0UL;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_3279[i] = &l_3003[5];
                        l_3196 &= ((**g_1557) == (void*)0);
                        if ((*g_1352))
                            break;
                        (*l_3043) |= (-8L);
                        ++l_3282;
                    }
                }
            }
        }
    }
    if (g_490)
        goto lbl_3285;
    return p_31;
}


/* ------------------------------------------ */
/* 
 * reads : g_100 g_490 g_23 g_17 g_2126 g_205 g_2595 g_1614 g_1615 g_1616 g_1617 g_1618 g_2622 g_2623 g_203 g_149 g_1148 g_261 g_1880 g_1507 g_610 g_143
 * writes: g_100 g_490 g_23 g_129 g_2261 g_1148 g_2126 g_149 g_205 g_2531 g_2624 g_261 g_1507
 */
static int32_t  func_34(const int8_t  p_35, int32_t * p_36)
{ /* block id: 1128 */
    int32_t l_2485[4][1][10] = {{{0x94311D74L,4L,4L,0x94311D74L,0x94311D74L,4L,4L,0x94311D74L,0x94311D74L,4L}},{{0x94311D74L,0x94311D74L,4L,4L,0x94311D74L,0x94311D74L,4L,4L,0x94311D74L,0x94311D74L}},{{0x94311D74L,4L,4L,0x94311D74L,0x94311D74L,4L,4L,0x94311D74L,0x94311D74L,4L}},{{0x94311D74L,0x94311D74L,4L,4L,0x94311D74L,0x94311D74L,4L,4L,0x94311D74L,0x94311D74L}}};
    int64_t *****l_2510[1][1];
    int16_t l_2543 = (-10L);
    int32_t l_2569[2][9] = {{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}};
    int8_t l_2575 = 0x7EL;
    uint32_t l_2579 = 18446744073709551614UL;
    int32_t ***l_2589 = &g_626;
    int16_t l_2678 = 2L;
    int32_t * const l_2682 = &g_149;
    int64_t l_2705 = 0x45813B9A40909F6FLL;
    int64_t l_2712 = 0x473A0A9F0DDAB6FALL;
    uint16_t l_2806 = 0xD028L;
    const int32_t *l_2825 = &g_203;
    const int32_t **l_2824 = &l_2825;
    const int32_t ***l_2823 = &l_2824;
    const int32_t ****l_2822 = &l_2823;
    const int32_t *****l_2821 = &l_2822;
    int32_t *l_2851 = (void*)0;
    int8_t *l_2859 = &g_445[2];
    int8_t **l_2858[7] = {&l_2859,(void*)0,(void*)0,&l_2859,(void*)0,(void*)0,&l_2859};
    int8_t ** const *l_2909 = &l_2858[4];
    int8_t ** const **l_2908 = &l_2909;
    int8_t ** const ***l_2907[5] = {&l_2908,&l_2908,&l_2908,&l_2908,&l_2908};
    int32_t *l_2937 = (void*)0;
    int32_t **l_2936[7][7] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2937,&l_2937,&l_2937,&l_2937,&l_2937,&l_2937,&l_2937},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2937,&l_2937,&l_2937,&l_2937,&l_2937,&l_2937,&l_2937},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_2937,&l_2937,&l_2937,&l_2937,&l_2937,&l_2937,&l_2937},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
            l_2510[i][j] = &g_328;
    }
    for (g_100 = 0; (g_100 == 36); g_100++)
    { /* block id: 1131 */
        int32_t l_2482 = (-8L);
        int32_t l_2483 = (-8L);
        int32_t ***l_2523 = &g_626;
        int32_t l_2603 = 1L;
        int32_t l_2604[9][7] = {{0x0EF1B261L,7L,0x0EF1B261L,(-10L),0x9A662498L,(-10L),0x0EF1B261L},{(-1L),(-1L),0xE6EC5058L,(-1L),(-1L),0xE6EC5058L,(-1L)},{0x9A662498L,(-10L),0x0EF1B261L,7L,0x0EF1B261L,(-10L),0x9A662498L},{0x4DDF6A90L,(-1L),0x4DDF6A90L,0x4DDF6A90L,(-1L),0x4DDF6A90L,0x4DDF6A90L},{0x9A662498L,7L,(-1L),7L,0x9A662498L,0x70156E42L,0x9A662498L},{(-1L),0x4DDF6A90L,0x4DDF6A90L,(-1L),0x4DDF6A90L,0x4DDF6A90L,(-1L)},{0x0EF1B261L,7L,0x0EF1B261L,(-10L),0x9A662498L,(-10L),0x0EF1B261L},{(-1L),(-1L),0xE6EC5058L,(-1L),(-1L),0xE6EC5058L,(-1L)},{0x9A662498L,(-10L),0x0EF1B261L,7L,0x0EF1B261L,(-10L),0x9A662498L}};
        uint64_t *l_2610[1];
        uint8_t l_2614 = 252UL;
        uint16_t *l_2620 = &g_83[2];
        uint16_t **l_2619 = &l_2620;
        uint16_t ***l_2618 = &l_2619;
        int16_t ***l_2673 = &g_1846;
        uint8_t l_2681 = 0xE8L;
        uint32_t l_2702[5];
        int32_t *l_2728 = &l_2482;
        int32_t **l_2727 = &l_2728;
        int32_t ***l_2726[10];
        uint32_t l_2749 = 0UL;
        uint8_t **l_2775 = &g_610;
        uint16_t l_2789 = 65535UL;
        int8_t ***l_2793 = &g_1321;
        int i, j;
        for (i = 0; i < 1; i++)
            l_2610[i] = &g_902;
        for (i = 0; i < 5; i++)
            l_2702[i] = 0UL;
        for (i = 0; i < 10; i++)
            l_2726[i] = &l_2727;
        for (g_490 = 5; (g_490 >= 0); g_490 -= 1)
        { /* block id: 1134 */
            uint32_t l_2484[10][10] = {{0xEBE3635EL,0xF8759F25L,0UL,0xAEAA367CL,0x428253F6L,4294967286UL,0xC8EBE96EL,0xB8BE2EDBL,0xC6A1E28AL,0xAEAA367CL},{4294967293UL,0x9EEEF5E0L,4294967287UL,0xF8759F25L,0x428253F6L,0UL,1UL,4294967290UL,0x4E106F96L,1UL},{0x428253F6L,0xC8EBE96EL,0xA4DF9118L,0x392462F0L,0xC8EBE96EL,0xC6A1E28AL,1UL,4294967287UL,0UL,0xB8BE2EDBL},{4294967295UL,0xB8BE2EDBL,0xAEAA367CL,0x6119E419L,4294967290UL,0x6119E419L,0xAEAA367CL,0xB8BE2EDBL,4294967295UL,0xC6A1E28AL},{0x9EEEF5E0L,0x392462F0L,0xA9B3E444L,1UL,4294967291UL,0x572028F8L,0x428253F6L,4294967291UL,4294967290UL,0xA63E400FL},{0x428253F6L,0xC6A1E28AL,0x6119E419L,1UL,0UL,0xA4DF9118L,0x9EEEF5E0L,0xEAD33181L,4294967295UL,0x392462F0L},{0xAEAA367CL,4294967291UL,4294967287UL,0x6119E419L,4294967293UL,4294967286UL,4294967295UL,0x428253F6L,0UL,0UL},{1UL,0x428253F6L,1UL,0x392462F0L,0x392462F0L,1UL,0x428253F6L,1UL,0x4E106F96L,1UL},{1UL,0xA4DF9118L,0xC6A1E28AL,0xF8759F25L,0xAEAA367CL,4294967295UL,4294967293UL,4294967295UL,0xC6A1E28AL,0x392462F0L},{0xC8EBE96EL,0xB8BE2EDBL,0xC6A1E28AL,0xAEAA367CL,0xA63E400FL,4294967295UL,0xEBE3635EL,1UL,4294967295UL,0UL}};
            uint32_t *l_2486 = (void*)0;
            uint32_t *l_2487 = (void*)0;
            uint32_t *l_2488 = &g_54;
            int64_t *l_2489 = &g_155;
            uint8_t *l_2490 = (void*)0;
            uint8_t *l_2491[5] = {&g_218,&g_218,&g_218,&g_218,&g_218};
            int32_t l_2492 = (-1L);
            uint32_t l_2541 = 8UL;
            int32_t *l_2542[10][5][5] = {{{&g_3,&g_17,&l_2492,&l_2485[3][0][6],(void*)0},{&g_203,&g_3,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_2492,&g_151,(void*)0},{&l_2483,&g_3,&g_1035[2][0],&g_478[3][3][0],&g_203},{&g_151,&g_1035[2][0],(void*)0,(void*)0,&l_2485[1][0][2]}},{{&g_151,&g_3,&g_4,&g_151,(void*)0},{&l_2485[3][0][6],(void*)0,&g_478[3][3][0],&l_2492,&g_17},{&g_151,&g_3,&l_2485[1][0][2],&g_3,(void*)0},{&l_2485[0][0][7],&g_17,(void*)0,&g_3,&g_151},{(void*)0,&g_203,&l_2492,&l_2492,&g_203}},{{&g_203,&g_17,(void*)0,&g_151,&l_2492},{&l_2485[3][0][6],(void*)0,&g_1035[1][0],(void*)0,&g_203},{&g_478[3][3][0],&g_203,&g_478[3][3][0],&g_478[3][3][0],(void*)0},{&l_2485[3][0][6],&l_2492,&l_2492,&g_151,&g_1035[2][0]},{&g_203,&l_2485[3][0][6],&g_478[1][0][5],(void*)0,&g_203}},{{(void*)0,(void*)0,&g_1035[2][0],&l_2485[3][0][6],(void*)0},{&l_2485[0][0][7],(void*)0,&g_1035[2][0],&g_23,&g_478[3][3][0]},{&g_151,&g_3,&l_2485[0][0][7],&g_3,&l_2492},{&g_478[0][0][6],&g_17,&g_478[1][0][5],&l_2492,&g_1035[2][0]},{(void*)0,&l_2492,&l_2492,&g_478[3][3][0],&g_4}},{{&l_2492,&l_2485[3][0][6],&g_17,&g_17,&g_1035[2][0]},{&g_1035[1][0],&g_478[3][3][0],&l_2492,&g_478[3][3][0],&l_2492},{&g_1035[2][0],&l_2492,&g_4,&g_478[3][3][0],&l_2492},{&g_478[3][3][0],&l_2492,(void*)0,&l_2492,(void*)0},{&l_2483,&l_2492,&l_2492,&g_478[3][3][0],&l_2483}},{{(void*)0,&l_2492,&l_2492,(void*)0,&g_478[3][3][0]},{&l_2485[3][0][6],&g_478[3][3][0],&g_203,(void*)0,&g_3},{&l_2492,&l_2485[3][0][6],(void*)0,&l_2492,&l_2483},{&g_1035[2][0],&l_2492,&g_478[3][3][0],(void*)0,&g_478[1][0][5]},{&l_2485[1][0][2],&g_17,&l_2485[3][0][6],(void*)0,&g_478[3][3][0]}},{{&l_2492,&g_3,(void*)0,&g_478[3][3][0],&l_2492},{(void*)0,&g_478[3][3][0],&g_478[1][0][5],&l_2492,&l_2492},{(void*)0,&g_1035[2][0],&g_23,&g_478[3][3][0],&g_478[3][3][0]},{&l_2492,&l_2485[3][0][6],&l_2492,&g_478[3][3][0],&g_1035[2][0]},{&l_2485[1][0][2],&g_4,&l_2492,&g_17,&l_2492}},{{&g_1035[2][0],&g_478[3][3][0],&g_203,&g_478[3][3][0],&g_478[3][3][0]},{&l_2492,&l_2492,&l_2492,&l_2492,(void*)0},{&l_2485[3][0][6],&g_4,&l_2492,&g_3,(void*)0},{(void*)0,&g_478[3][3][0],&g_23,(void*)0,&g_3},{&l_2483,&g_478[3][3][0],&g_478[1][0][5],&g_478[0][0][6],&g_3}},{{&g_478[3][3][0],&l_2483,(void*)0,&g_1035[2][0],(void*)0},{&g_1035[2][0],&g_1035[2][0],&l_2485[3][0][6],(void*)0,(void*)0},{&g_1035[1][0],&g_17,&g_478[3][3][0],(void*)0,&g_478[3][3][0]},{&l_2492,&g_478[3][3][0],(void*)0,&g_3,&l_2492},{(void*)0,&g_17,&g_203,&l_2492,&g_1035[2][0]}},{{&g_478[0][0][6],&g_1035[2][0],&l_2492,&g_4,&g_478[3][3][0]},{&l_2492,&l_2483,&l_2492,&g_17,&l_2492},{&l_2485[1][0][2],&g_478[3][3][0],(void*)0,&g_17,&l_2492},{&l_2492,&g_478[3][3][0],&g_4,&g_4,&g_478[3][3][0]},{&g_478[3][3][0],&g_4,&l_2492,&l_2492,&g_478[1][0][5]}}};
            uint16_t l_2591 = 0x7B7EL;
            uint8_t l_2607 = 8UL;
            uint64_t **l_2611 = &g_2300[0][8];
            uint64_t l_2613 = 0xD1F93E2F61A40780LL;
            uint16_t l_2679 = 65527UL;
            uint32_t l_2716 = 0UL;
            int16_t l_2797 = 0x65BCL;
            int32_t l_2832 = 0x27ACBECBL;
            uint32_t l_2834 = 0UL;
            int i, j, k;
        }
    }
    for (g_23 = 0; (g_23 > 19); g_23 = safe_add_func_uint16_t_u_u(g_23, 5))
    { /* block id: 1301 */
        int8_t **l_2860 = &l_2859;
        int32_t l_2865 = 0x980547E5L;
        int32_t l_2877 = 0xC8D68A4EL;
        int16_t l_2882 = 0xCF46L;
        int32_t l_2889[4][1] = {{3L},{0x15D3B6BBL},{3L},{0x15D3B6BBL}};
        uint64_t *l_2938 = &g_261;
        int64_t **l_2945 = (void*)0;
        uint16_t l_2960 = 0x2CB6L;
        int i, j;
        for (g_129 = 1; (g_129 <= 5); g_129 += 1)
        { /* block id: 1304 */
            uint16_t ****l_2861 = &g_2622;
            int32_t l_2866 = (-2L);
            int32_t l_2867 = 1L;
            int32_t l_2876 = 0L;
            int32_t l_2878 = 0xCB8CB155L;
            int32_t l_2879 = 0L;
            int16_t l_2881[5][4][5] = {{{(-9L),0xA642L,0L,0xB657L,(-7L)},{0x66D0L,0x0696L,0L,0x81CCL,0x81CCL},{(-3L),(-10L),(-3L),0xB657L,0x81CCL},{0x1A97L,1L,2L,0x4745L,(-7L)}},{{(-3L),1L,0x66D0L,0L,0x21E1L},{(-1L),0xFC17L,0L,0L,(-9L)},{9L,0L,0xD408L,(-3L),0x21E1L},{9L,9L,0x6132L,2L,0L}},{{(-1L),0L,0x6132L,0x66D0L,0x66D0L},{0xD408L,0xFC17L,0xD408L,2L,0x66D0L},{0x29C9L,0x6A23L,0L,(-3L),0L},{0xD408L,0x6A23L,(-1L),0L,0x21E1L}},{{(-1L),0xFC17L,0L,0L,(-9L)},{9L,0L,0xD408L,(-3L),0x21E1L},{9L,9L,0x6132L,2L,0L},{(-1L),0L,0x6132L,0x66D0L,0x66D0L}},{{0xD408L,0xFC17L,0xD408L,2L,0x66D0L},{0x29C9L,0x6A23L,0L,(-3L),0L},{0xD408L,0x6A23L,(-1L),0L,0x21E1L},{(-1L),0xFC17L,0L,0L,(-9L)}}};
            int32_t l_2883 = 0L;
            int32_t l_2884 = (-1L);
            int32_t l_2886 = (-1L);
            int32_t l_2888 = 0x230BA30EL;
            int32_t l_2891 = 5L;
            int32_t l_2892 = 0x13FEF8F7L;
            int i, j, k;
            for (g_2261 = 0; (g_2261 <= 5); g_2261 += 1)
            { /* block id: 1307 */
                return (*p_36);
            }
            for (g_1148 = 0; (g_1148 <= 2); g_1148 += 1)
            { /* block id: 1312 */
                int16_t l_2863 = 0x584FL;
                int32_t l_2887 = 0x72225D60L;
                int32_t l_2890 = 0x1C57E804L;
                int32_t l_2893 = 0x6F6F03E4L;
                int32_t l_2903 = (-1L);
                l_2851 = p_36;
                for (g_2126 = 0; (g_2126 <= 5); g_2126 += 1)
                { /* block id: 1316 */
                    uint16_t l_2864 = 65527UL;
                    int32_t l_2880 = 0xB463C978L;
                    int32_t *l_2897 = (void*)0;
                    int32_t *l_2898 = &l_2880;
                    int32_t *l_2899 = &l_2892;
                    int32_t *l_2900 = (void*)0;
                    int32_t *l_2901 = &l_2893;
                    int32_t *l_2902[8][10] = {{&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866},{&g_151,&g_151,&l_2887,&g_151,&g_151,&l_2887,&g_151,&g_151,&l_2887,&g_151},{&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866,&g_151},{&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866},{&g_151,&g_151,&l_2887,&g_151,&g_151,&l_2887,&g_151,&g_151,&l_2887,&g_151},{&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866,&g_151},{&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866,&g_151,&l_2866,&l_2866},{&g_151,&g_151,&l_2887,&g_151,&g_151,&l_2887,&g_151,&g_151,&l_2887,&g_151}};
                    uint64_t l_2904[7] = {8UL,8UL,8UL,8UL,8UL,8UL,8UL};
                    int i, j;
                    (*l_2682) = (*p_36);
                    for (g_205 = 0; (g_205 <= 3); g_205 += 1)
                    { /* block id: 1320 */
                        int i, j;
                        return g_2595[g_2126][g_205];
                    }
                    for (g_2531 = 2; (g_2531 >= 0); g_2531 -= 1)
                    { /* block id: 1325 */
                        uint16_t *l_2853 = &g_83[1];
                        const int32_t l_2862 = 0x883026C5L;
                        int32_t *l_2868 = &g_478[4][1][5];
                        int32_t *l_2869 = &l_2865;
                        int32_t l_2870 = 0x5E262EC2L;
                        int32_t *l_2871 = &l_2485[3][0][6];
                        int32_t *l_2872 = &g_478[2][5][1];
                        int32_t *l_2873 = &g_203;
                        int32_t *l_2874 = &l_2485[0][0][9];
                        int32_t *l_2875[2][8] = {{(void*)0,&g_1035[0][0],(void*)0,(void*)0,&g_1035[0][0],(void*)0,(void*)0,&g_1035[0][0]},{&g_1035[0][0],(void*)0,(void*)0,&g_1035[0][0],(void*)0,(void*)0,&g_1035[0][0],(void*)0}};
                        uint16_t l_2894[2];
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_2894[i] = 0xF3E9L;
                        (*l_2682) &= ((l_2864 = ((p_35 > (!((****g_1614) == ((((**g_2622) = (void*)0) != l_2853) , ((safe_mul_func_int16_t_s_s((safe_div_func_uint64_t_u_u(((-1L) != (((((l_2858[3] == l_2860) | 0UL) , (void*)0) == l_2861) < 0x7F20905EL)), l_2862)), l_2863)) | (****l_2822)))))) , p_35)) > l_2865);
                        l_2894[1]--;
                    }
                    --l_2904[0];
                }
                for (g_261 = 0; (g_261 <= 2); g_261 += 1)
                { /* block id: 1335 */
                    uint64_t *l_2925 = (void*)0;
                    uint64_t *l_2926[4];
                    uint64_t l_2927 = 0xC4A64A3286B59FCFLL;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_2926[i] = &g_356;
                }
            }
            if ((*p_36))
                continue;
        }
        (*g_1880) ^= 0L;
        l_2889[1][0] = ((safe_mod_func_int8_t_s_s((p_35 <= ((safe_rshift_func_int64_t_s_s(((safe_sub_func_uint8_t_u_u(0UL, ((safe_rshift_func_uint64_t_u_s((&g_1446 != l_2936[4][5]), 62)) != p_35))) >= (((*l_2938) = ((void*)0 == &g_2772[0][8])) == ((safe_lshift_func_int8_t_s_s((((safe_rshift_func_uint8_t_u_u(((p_35 | (safe_lshift_func_uint16_t_u_u((l_2945 != l_2945), p_35))) && 2L), 4)) | (*p_36)) , p_35), 4)) == 0xE38FL))), p_35)) , 4294967295UL)), 0x94L)) < 3UL);
        l_2889[1][0] &= (((safe_lshift_func_uint64_t_u_u((((****l_2822) , 0x55L) == (l_2865 = (0xB863L > (p_35 < (safe_mul_func_int64_t_s_s((safe_div_func_uint8_t_u_u((*g_610), p_35)), (safe_rshift_func_int64_t_s_u(((safe_mul_func_int32_t_s_s(((-1L) <= (safe_rshift_func_uint32_t_u_s((safe_mod_func_int16_t_s_s(0x0AE3L, (-8L))), 27))), (***l_2823))) ^ 0L), p_35)))))))), 60)) >= (*p_36)) == l_2960);
    }
    return (*p_36);
}


/* ------------------------------------------ */
/* 
 * reads : g_60 g_23 g_490 g_1035 g_218 g_151 g_754 g_755 g_445 g_478 g_610 g_143 g_356 g_149 g_22 g_83 g_626 g_257 g_989 g_990 g_730 g_609 g_47 g_235 g_236 g_20 g_203 g_100 g_91 g_1352 g_1370 g_988 g_1404 g_119 g_1445 g_1457 g_328 g_329 g_1507 g_1557 g_1504 g_205 g_355 g_1591 g_14 g_54 g_1068 g_1069 g_1614 g_728 g_1558 g_1321 g_1880 g_2000 g_1920 g_1921 g_1648 g_128 g_1846 g_1592 g_17 g_2100 g_2103 g_2121 g_2126 g_4 g_1919 g_2138 g_2139 g_1562 g_1563 g_2101 g_1931 g_2261 g_2318 g_3 g_2340 g_2349 g_1615 g_1616 g_1617 g_1618 g_261 g_2122
 * writes: g_23 g_47 g_91 g_149 g_490 g_478 g_152 g_356 g_257 g_143 g_155 g_83 g_54 g_1321 g_1327 g_119 g_730 g_100 g_445 g_1035 g_51 g_1445 g_1457 g_1504 g_1507 g_20 g_1561 g_205 g_728 g_1846 g_1618 g_1999 g_261 g_128 g_2058 g_2100 g_2122 g_2261 g_2300 g_1404
 */
static int8_t  func_37(const int64_t  p_38, int64_t  p_39, uint8_t  p_40, const uint32_t  p_41, uint64_t  p_42)
{ /* block id: 20 */
    uint8_t l_59 = 255UL;
    int32_t l_74 = 0L;
    int32_t *l_1153 = (void*)0;
    int32_t l_1160[3];
    int8_t l_1169 = 0x10L;
    uint8_t **l_1183 = (void*)0;
    int8_t *l_1220 = (void*)0;
    int8_t **l_1219 = &l_1220;
    int64_t l_1233[2][4] = {{0x0194EAE6E5E6547ELL,0x0194EAE6E5E6547ELL,0x0194EAE6E5E6547ELL,0x0194EAE6E5E6547ELL},{0x0194EAE6E5E6547ELL,0x0194EAE6E5E6547ELL,0x0194EAE6E5E6547ELL,0x0194EAE6E5E6547ELL}};
    uint64_t l_1312 = 8UL;
    uint16_t *l_1331[4][9][7] = {{{(void*)0,&g_83[0],(void*)0,&g_83[2],&g_83[0],&g_83[2],&g_205},{&g_83[2],(void*)0,&g_83[2],&g_205,&g_83[2],&g_205,&g_83[2]},{&g_205,&g_83[2],&g_83[0],&g_205,(void*)0,(void*)0,&g_83[2]},{&g_83[2],&g_83[0],&g_83[2],(void*)0,&g_83[2],(void*)0,&g_205},{(void*)0,(void*)0,&g_205,&g_83[2],&g_83[2],&g_83[0],&g_205},{(void*)0,&g_205,&g_83[2],&g_83[0],&g_83[2],&g_83[2],&g_205},{&g_83[2],(void*)0,&g_83[0],&g_83[0],&g_205,(void*)0,&g_205},{&g_205,&g_83[2],&g_83[2],&g_83[2],&g_83[2],&g_205,&g_83[2]},{&g_83[2],&g_83[2],&g_83[1],&g_83[2],&g_83[2],&g_205,&g_83[2]}},{{&g_83[1],&g_83[2],&g_205,&g_83[2],&g_205,(void*)0,(void*)0},{&g_83[2],(void*)0,&g_83[2],&g_83[2],&g_205,&g_83[2],(void*)0},{&g_83[2],&g_83[2],(void*)0,&g_205,(void*)0,&g_83[2],&g_83[2]},{&g_83[2],&g_83[0],&g_83[2],&g_83[2],(void*)0,&g_83[2],&g_205},{&g_83[2],&g_83[0],&g_205,&g_205,&g_83[2],&g_83[1],(void*)0},{&g_83[2],&g_83[0],&g_83[2],&g_83[2],&g_83[2],&g_83[1],&g_205},{&g_205,&g_83[1],(void*)0,&g_83[2],&g_83[0],&g_83[2],(void*)0},{&g_205,&g_83[2],&g_83[2],(void*)0,&g_83[2],&g_83[2],(void*)0},{(void*)0,(void*)0,&g_205,(void*)0,(void*)0,&g_83[1],&g_83[2]}},{{&g_83[2],&g_205,&g_83[1],(void*)0,(void*)0,&g_83[2],&g_83[2]},{&g_83[2],&g_83[2],&g_83[2],&g_83[2],&g_205,&g_205,(void*)0},{&g_83[2],(void*)0,&g_83[0],&g_83[1],&g_83[2],&g_83[2],(void*)0},{&g_83[2],&g_83[2],&g_83[2],&g_83[2],&g_83[2],&g_83[1],(void*)0},{&g_205,&g_83[2],&g_205,&g_83[0],&g_83[2],&g_205,&g_83[2]},{&g_83[2],&g_83[0],&g_83[2],&g_83[1],&g_83[2],&g_205,&g_83[2]},{&g_205,&g_83[2],&g_83[0],&g_205,&g_205,&g_83[2],&g_205},{&g_205,&g_83[0],&g_83[2],&g_83[2],(void*)0,(void*)0,&g_83[2]},{(void*)0,&g_205,&g_205,&g_83[2],(void*)0,&g_83[2],&g_83[2]}},{{&g_83[2],&g_205,&g_83[2],&g_83[2],&g_83[0],&g_83[2],(void*)0},{(void*)0,&g_205,&g_83[0],&g_205,&g_83[0],&g_83[1],&g_83[0]},{&g_205,(void*)0,(void*)0,&g_205,&g_83[1],(void*)0,&g_83[2]},{&g_83[0],&g_205,(void*)0,&g_83[0],(void*)0,&g_83[2],&g_205},{&g_83[2],(void*)0,&g_83[2],&g_83[2],&g_205,&g_83[2],&g_83[2]},{(void*)0,(void*)0,&g_83[0],&g_83[2],&g_83[2],&g_83[1],&g_83[0]},{(void*)0,(void*)0,&g_205,&g_83[0],&g_83[2],&g_205,(void*)0},{&g_83[2],&g_205,&g_205,&g_205,&g_83[2],&g_83[2],&g_205},{(void*)0,(void*)0,&g_83[0],&g_205,&g_83[0],&g_83[2],&g_83[2]}}};
    int64_t * const **l_1348 = (void*)0;
    uint16_t l_1356[1];
    const int64_t l_1366[9][3][6] = {{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)}},{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)}},{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)}},{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)}},{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)}},{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)}},{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)}},{{(-6L),0xA154F05E985FF5F8LL,(-6L),(-6L),0xA154F05E985FF5F8LL,(-6L)},{(-8L),(-6L),(-8L),(-8L),(-6L),(-8L)},{(-8L),(-6L),(-8L),(-8L),(-6L),(-8L)}},{{(-8L),(-6L),(-8L),(-8L),(-6L),(-8L)},{(-8L),(-6L),(-8L),(-8L),(-6L),(-8L)},{(-8L),(-6L),(-8L),(-8L),(-6L),(-8L)}}};
    uint16_t l_1367 = 0UL;
    int8_t *l_1368 = &g_445[8];
    int16_t *l_1369[3][8][8] = {{{&g_730[0][0],(void*)0,(void*)0,&g_730[0][0],&g_355,(void*)0,&g_355,&g_355},{&g_730[0][0],&g_730[0][0],&g_730[0][0],&g_355,&g_730[0][0],&g_355,&g_730[0][0],&g_730[0][0]},{(void*)0,&g_730[0][0],&g_355,&g_730[0][0],&g_730[0][0],(void*)0,(void*)0,(void*)0},{&g_355,(void*)0,(void*)0,(void*)0,(void*)0,&g_355,(void*)0,&g_730[0][0]},{&g_355,(void*)0,&g_355,(void*)0,&g_730[0][0],&g_730[0][0],&g_730[0][0],(void*)0},{&g_730[0][0],&g_730[0][0],&g_730[0][0],(void*)0,&g_355,(void*)0,&g_355,&g_730[0][0]},{(void*)0,&g_355,(void*)0,(void*)0,(void*)0,(void*)0,&g_355,(void*)0},{(void*)0,(void*)0,&g_730[0][0],&g_730[0][0],&g_355,&g_730[0][0],(void*)0,&g_730[0][0]}},{{&g_730[0][0],&g_355,&g_730[0][0],&g_355,&g_730[0][0],&g_730[0][0],&g_730[0][0],&g_355},{&g_355,(void*)0,&g_355,&g_730[0][0],(void*)0,(void*)0,&g_730[0][0],&g_355},{&g_355,&g_355,&g_355,(void*)0,&g_730[0][0],(void*)0,&g_730[0][0],&g_355},{(void*)0,&g_730[0][0],&g_730[0][0],&g_355,&g_730[0][0],&g_730[0][0],(void*)0,&g_355},{&g_730[0][0],(void*)0,&g_730[0][0],(void*)0,&g_355,&g_355,&g_355,&g_355},{&g_730[0][0],(void*)0,(void*)0,&g_730[0][0],&g_355,(void*)0,&g_355,&g_355},{&g_730[0][0],&g_730[0][0],&g_730[0][0],&g_355,&g_730[0][0],&g_355,&g_730[0][0],&g_730[0][0]},{(void*)0,&g_730[0][0],&g_355,&g_730[0][0],&g_730[0][0],(void*)0,(void*)0,(void*)0}},{{&g_355,(void*)0,(void*)0,(void*)0,(void*)0,&g_355,(void*)0,&g_730[0][0]},{&g_355,(void*)0,&g_355,(void*)0,&g_730[0][0],&g_730[0][0],&g_730[0][0],(void*)0},{&g_730[0][0],&g_730[0][0],&g_730[0][0],(void*)0,&g_355,(void*)0,&g_355,&g_730[0][0]},{(void*)0,&g_355,(void*)0,(void*)0,(void*)0,(void*)0,&g_355,(void*)0},{(void*)0,&g_730[0][0],&g_730[0][0],(void*)0,&g_355,&g_730[0][0],&g_730[0][0],&g_730[0][0]},{(void*)0,&g_730[0][0],&g_355,&g_730[0][0],(void*)0,&g_730[0][0],(void*)0,&g_355},{&g_730[0][0],&g_730[0][0],&g_730[0][0],&g_355,&g_730[0][0],&g_730[0][0],&g_355,&g_730[0][0]},{&g_355,&g_355,&g_730[0][0],(void*)0,&g_730[0][0],(void*)0,(void*)0,&g_730[0][0]}}};
    int8_t * const * const *l_1387 = (void*)0;
    int8_t * const * const **l_1386 = &l_1387;
    int64_t l_1461 = 0x6C3DAA25C4B10DB8LL;
    int16_t **l_1467 = &l_1369[1][2][7];
    int32_t ***l_1496 = &g_626;
    int32_t *** const *l_1495 = &l_1496;
    int8_t l_1555 = (-1L);
    uint32_t * const l_1798 = &g_1799;
    uint32_t * const *l_1797 = &l_1798;
    const int64_t l_1832 = 0x0D378275CCF080AELL;
    int8_t l_1936 = 0xB8L;
    int32_t *l_1970[9][8] = {{&g_128,&g_128,&g_128,(void*)0,&g_490,&g_128,(void*)0,(void*)0},{&g_128,&g_490,&g_128,&g_128,&g_490,&g_128,&g_490,&g_490},{&g_490,(void*)0,&g_128,&g_490,(void*)0,(void*)0,&g_128,&g_128},{&g_128,&g_490,&g_128,&g_490,&g_128,&g_490,&g_128,&g_490},{&g_490,&g_128,&g_128,&g_128,(void*)0,&g_490,&g_128,(void*)0},{&g_490,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_490},{&g_490,&g_490,&g_490,(void*)0,&g_128,(void*)0,&g_128,(void*)0},{&g_128,&g_128,&g_490,(void*)0,(void*)0,(void*)0,&g_490,&g_128},{&g_490,&g_490,(void*)0,&g_490,&g_490,(void*)0,&g_128,&g_490}};
    int32_t **l_1969[2][1];
    uint16_t l_2006 = 0x3D43L;
    uint8_t l_2010 = 249UL;
    int8_t l_2031 = 1L;
    uint32_t **l_2072 = &g_1504;
    uint32_t ***l_2071 = &l_2072;
    uint8_t l_2091 = 5UL;
    volatile uint32_t * volatile *l_2143[7][3];
    volatile uint32_t * volatile * const *l_2142 = &l_2143[1][0];
    volatile uint32_t * volatile * const * volatile *l_2141[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    volatile uint32_t * volatile * const * volatile * volatile *l_2140 = &l_2141[8];
    uint8_t l_2336 = 8UL;
    int64_t l_2450[3];
    uint32_t l_2453 = 1UL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_1160[i] = 1L;
    for (i = 0; i < 1; i++)
        l_1356[i] = 2UL;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_1969[i][j] = &l_1970[1][6];
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
            l_2143[i][j] = (void*)0;
    }
    for (i = 0; i < 3; i++)
        l_2450[i] = (-1L);
lbl_1198:
    (*g_60) &= (safe_lshift_func_int8_t_s_s((l_59 != p_38), 3));
    for (l_59 = 0; (l_59 <= 3); l_59 += 1)
    { /* block id: 24 */
        uint32_t l_75 = 0x646D9140L;
        int32_t l_1158 = 1L;
        uint8_t **l_1184[6][5] = {{&g_610,&g_610,&g_610,&g_610,&g_610},{(void*)0,&g_610,(void*)0,&g_610,(void*)0},{&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610},{&g_610,&g_610,&g_610,&g_610,&g_610},{(void*)0,&g_610,(void*)0,&g_610,(void*)0}};
        uint64_t l_1215 = 18446744073709551607UL;
        uint32_t l_1221 = 18446744073709551612UL;
        int32_t l_1227 = (-1L);
        int32_t l_1234 = (-7L);
        int32_t l_1235[6][5][7] = {{{0xAC63D804L,0L,(-9L),0xB14880B2L,0xB14880B2L,(-9L),0L},{1L,0L,0x3E6B1986L,(-1L),0xC2537A62L,0L,1L},{0x0095D6FEL,0L,0x0095D6FEL,0L,0L,0x0FBFAA5BL,0x0FBFAA5BL},{1L,(-1L),1L,(-1L),1L,0x52F1F05BL,0L},{0xB14880B2L,0x0FBFAA5BL,0L,0xB14880B2L,0L,0x0FBFAA5BL,0xB14880B2L}},{{0x3E6B1986L,1L,0x778639EBL,0L,0L,0L,0x778639EBL},{0xB14880B2L,0xB14880B2L,(-9L),0L,0xAC63D804L,0xB14880B2L,0L},{0x778639EBL,0x52F1F05BL,1L,0L,0x26811E93L,0x3D12E0CCL,0x778639EBL},{(-1L),0L,0xE69D6B64L,0xE69D6B64L,0L,(-1L),(-9L)},{0xC2537A62L,0L,0L,(-8L),0x3E6B1986L,1L,0x778639EBL}},{{0L,(-9L),(-1L),0L,0xE69D6B64L,0xE69D6B64L,0L},{(-1L),0L,(-1L),0xEF96F65FL,0x778639EBL,0x52F1F05BL,1L},{0x0FBFAA5BL,0L,0xB14880B2L,0L,0x0FBFAA5BL,0xB14880B2L,0x0095D6FEL},{0x3E6B1986L,0x52F1F05BL,0x26811E93L,(-8L),0x26811E93L,0x52F1F05BL,0x3E6B1986L},{(-1L),0x0095D6FEL,(-9L),0xE69D6B64L,0x0095D6FEL,0xE69D6B64L,(-9L)}},{{0x3E6B1986L,(-8L),0L,0L,0xC2537A62L,1L,0xC2537A62L},{0x0FBFAA5BL,(-9L),(-9L),0x0FBFAA5BL,0xE69D6B64L,(-1L),0x0FBFAA5BL},{(-1L),0L,0x26811E93L,0xEF96F65FL,0xC2537A62L,0x3D12E0CCL,1L},{0L,0x0FBFAA5BL,0xB14880B2L,0x0095D6FEL,0x0095D6FEL,0xB14880B2L,0x0FBFAA5BL},{0xC2537A62L,0x52F1F05BL,(-1L),0L,0x26811E93L,0xEF96F65FL,0xC2537A62L}},{{(-1L),0x0FBFAA5BL,(-1L),0xE69D6B64L,0x0FBFAA5BL,(-9L),(-9L)},{0x778639EBL,0L,0L,0L,0x778639EBL,1L,0x3E6B1986L},{0x0095D6FEL,(-9L),0xE69D6B64L,0x0095D6FEL,0xE69D6B64L,(-9L),0x0095D6FEL},{(-1L),(-8L),1L,0xEF96F65FL,0x3E6B1986L,0xEF96F65FL,1L},{0x0095D6FEL,0x0095D6FEL,0xB14880B2L,0x0FBFAA5BL,0L,0xB14880B2L,0L}},{{0x778639EBL,0x52F1F05BL,1L,0L,0x26811E93L,0x3D12E0CCL,0x778639EBL},{(-1L),0L,0xE69D6B64L,0xE69D6B64L,0L,(-1L),(-9L)},{0xC2537A62L,0L,0L,(-8L),0x3E6B1986L,1L,0x778639EBL},{0L,(-9L),(-1L),0L,0xE69D6B64L,0xE69D6B64L,0L},{(-1L),0L,(-1L),0xEF96F65FL,0x778639EBL,0x52F1F05BL,1L}}};
        uint64_t *l_1241[8][3] = {{&g_261,&l_1215,&l_1215},{&l_1215,&g_356,(void*)0},{&g_261,&g_356,&g_261},{&g_356,&l_1215,(void*)0},{&g_356,&g_356,&l_1215},{&g_261,&l_1215,&l_1215},{&l_1215,&g_356,(void*)0},{&g_261,&g_356,&g_261}};
        uint16_t * const l_1323[2][4] = {{&g_205,&g_205,&g_205,&g_205},{&g_83[2],&g_205,&g_205,&g_83[2]}};
        int16_t *l_1328[5][10][5] = {{{(void*)0,&g_119,&g_355,(void*)0,&g_119},{&g_355,&g_730[3][0],&g_119,&g_730[0][0],&g_730[0][0]},{&g_730[3][0],&g_119,&g_730[0][0],&g_730[0][0],&g_119},{&g_730[0][0],&g_119,&g_355,&g_355,&g_355},{&g_355,(void*)0,&g_730[7][0],&g_355,&g_355},{&g_119,(void*)0,&g_355,&g_355,&g_119},{&g_730[0][0],(void*)0,&g_730[0][0],&g_355,(void*)0},{&g_119,&g_730[0][0],&g_730[2][0],&g_355,&g_355},{&g_730[5][0],&g_355,&g_119,&g_355,&g_119},{&g_355,&g_119,&g_119,&g_730[0][0],&g_730[6][0]}},{{&g_730[8][0],&g_730[0][0],&g_730[0][0],&g_730[0][0],&g_730[8][0]},{&g_355,&g_119,&g_355,(void*)0,&g_730[0][0]},{&g_119,&g_730[4][0],&g_119,&g_355,&g_730[0][0]},{&g_730[0][0],&g_355,&g_730[6][0],&g_119,&g_730[0][0]},{&g_730[2][0],&g_355,&g_355,&g_119,&g_730[8][0]},{&g_730[0][0],(void*)0,&g_730[2][0],&g_355,&g_730[6][0]},{&g_730[0][0],&g_355,&g_730[0][0],&g_730[5][0],&g_119},{&g_730[0][0],&g_730[2][0],&g_119,&g_355,&g_355},{&g_355,(void*)0,(void*)0,&g_730[0][0],(void*)0},{&g_119,&g_730[5][0],(void*)0,(void*)0,&g_119}},{{&g_119,&g_119,&g_355,&g_355,&g_355},{&g_355,&g_730[0][0],&g_730[0][0],&g_730[8][0],&g_355},{&g_730[0][0],(void*)0,&g_355,(void*)0,&g_119},{&g_730[0][0],(void*)0,&g_730[0][0],&g_119,&g_730[0][0]},{&g_730[0][0],&g_730[0][0],(void*)0,&g_730[7][0],&g_119},{&g_730[2][0],&g_730[0][0],&g_355,&g_119,&g_730[0][0]},{&g_730[0][0],&g_355,(void*)0,&g_355,(void*)0},{&g_119,&g_730[0][0],(void*)0,&g_355,&g_355},{&g_355,&g_730[0][0],(void*)0,&g_730[2][0],&g_355},{&g_730[8][0],(void*)0,&g_355,&g_119,&g_119}},{{&g_355,(void*)0,&g_119,&g_730[0][0],&g_730[0][0]},{&g_730[5][0],&g_730[0][0],&g_119,(void*)0,(void*)0},{&g_119,&g_119,&g_730[3][0],&g_355,&g_730[0][0]},{&g_730[0][0],&g_730[5][0],&g_730[3][0],(void*)0,&g_730[7][0]},{&g_119,(void*)0,&g_119,(void*)0,&g_355},{&g_355,&g_730[2][0],&g_119,&g_730[3][0],&g_730[0][0]},{&g_730[0][0],&g_355,&g_355,&g_355,(void*)0},{&g_730[3][0],(void*)0,&g_119,&g_355,&g_119},{(void*)0,&g_119,&g_119,(void*)0,&g_730[2][0]},{&g_730[0][0],&g_119,&g_730[2][0],&g_730[2][0],&g_355}},{{(void*)0,&g_355,&g_119,(void*)0,&g_355},{&g_119,&g_730[0][0],&g_355,&g_355,&g_119},{&g_355,&g_119,&g_355,&g_355,&g_355},{&g_730[0][0],&g_355,&g_119,&g_119,&g_730[3][0]},{&g_355,&g_119,&g_355,(void*)0,&g_355},{&g_355,&g_730[3][0],&g_119,&g_119,(void*)0},{&g_730[6][0],&g_730[1][0],&g_730[5][0],&g_730[0][0],(void*)0},{&g_730[0][0],&g_355,(void*)0,&g_730[5][0],&g_355},{&g_730[0][0],&g_730[5][0],&g_355,&g_355,&g_730[3][0]},{&g_355,&g_355,&g_730[3][0],&g_730[0][0],&g_355}}};
        int32_t *l_1344 = &g_128;
        uint32_t l_1353 = 1UL;
        uint32_t *l_1357 = &g_51;
        int i, j, k;
        for (g_47 = 0; (g_47 <= 3); g_47 += 1)
        { /* block id: 27 */
            uint32_t l_1146 = 4294967289UL;
            uint8_t *l_1147 = &g_1148;
            int i;
        }
        for (g_91 = 0; (g_91 <= 3); g_91 += 1)
        { /* block id: 544 */
            int64_t l_1155 = 0x5A898E23519FE442LL;
            int32_t l_1159 = (-8L);
            uint16_t l_1214 = 0x7F27L;
            int32_t l_1224 = 0xBCB7FF62L;
            int32_t l_1228 = 0L;
            int32_t l_1229[4][10] = {{(-1L),0xF5566C52L,0xC45F13B3L,0xC45F13B3L,0xF5566C52L,(-1L),0xAB84985AL,(-1L),0xF5566C52L,0xC45F13B3L},{0x297EBC9BL,1L,0x297EBC9BL,0xC45F13B3L,0xAB84985AL,0xAB84985AL,0xC45F13B3L,0x297EBC9BL,1L,0x297EBC9BL},{0x297EBC9BL,(-1L),1L,0xF5566C52L,1L,(-1L),0x297EBC9BL,0x297EBC9BL,(-1L),1L},{(-1L),0x297EBC9BL,0x297EBC9BL,(-1L),1L,0xF5566C52L,1L,(-1L),0x297EBC9BL,0x297EBC9BL}};
            uint64_t *l_1240 = &g_261;
            int8_t *l_1271 = &l_1169;
            uint32_t l_1292[6][9] = {{4294967295UL,0x7857A4A9L,0x85F09F3AL,4294967289UL,3UL,1UL,4294967295UL,0xC368DF14L,0xB8C54556L},{4294967289UL,0x7857A4A9L,0x73A92B5BL,0UL,3UL,0x81AF7230L,4294967289UL,0xC368DF14L,0xC368DF14L},{0UL,0x7857A4A9L,3UL,4294967295UL,3UL,0x7857A4A9L,0UL,0xC368DF14L,1UL},{4294967295UL,0x7857A4A9L,0x85F09F3AL,4294967289UL,3UL,1UL,4294967295UL,0xC368DF14L,0xB8C54556L},{4294967289UL,0x7857A4A9L,0x73A92B5BL,0UL,3UL,0x81AF7230L,4294967289UL,0xC368DF14L,0xC368DF14L},{0UL,0x7857A4A9L,3UL,4294967295UL,3UL,0x7857A4A9L,0UL,0xC368DF14L,1UL}};
            int16_t l_1324 = (-2L);
            int8_t ***l_1347 = &g_1321;
            int i, j;
            for (g_149 = 0; (g_149 <= 3); g_149 += 1)
            { /* block id: 547 */
                int32_t l_1154[1][10] = {{2L,(-9L),(-9L),2L,(-9L),(-9L),2L,(-9L),(-9L),2L}};
                int32_t *l_1156 = &g_478[3][3][0];
                int32_t *l_1157[4];
                uint8_t l_1161 = 0xA7L;
                uint32_t l_1166 = 18446744073709551615UL;
                int8_t *l_1217 = (void*)0;
                int8_t **l_1216 = &l_1217;
                int16_t l_1225 = (-1L);
                int i, j;
                for (i = 0; i < 4; i++)
                    l_1157[i] = &l_1154[0][7];
                l_1161--;
                for (g_490 = 0; (g_490 <= 0); g_490 += 1)
                { /* block id: 551 */
                    int32_t l_1195 = 1L;
                    int i, j;
                    if ((safe_mod_func_int8_t_s_s(g_1035[(g_490 + 1)][g_490], l_1166)))
                    { /* block id: 552 */
                        uint64_t l_1192 = 0UL;
                        int8_t *l_1196 = (void*)0;
                        int8_t *l_1197 = &l_1169;
                        int i, j;
                        (*l_1156) = (l_1154[g_490][(g_149 + 6)] |= ((safe_sub_func_int16_t_s_s(((((l_1169 , (((safe_sub_func_int64_t_s_s(0L, (((((+0xF143L) <= p_38) && ((*l_1197) = ((((safe_lshift_func_int64_t_s_s((((l_1195 |= ((safe_lshift_func_int64_t_s_u(((safe_lshift_func_int16_t_s_u((safe_mod_func_int32_t_s_s((safe_sub_func_int32_t_s_s((((l_1184[4][4] = l_1183) != (void*)0) != (((safe_rshift_func_int64_t_s_u(((safe_sub_func_uint64_t_u_u(((safe_mul_func_int32_t_s_s(p_41, (((+((l_1192 == (((safe_add_func_int64_t_s_s(((0x82L <= p_39) >= p_38), p_38)) | 0x873EL) != 1L)) , g_218)) == p_38) < l_1192))) != p_38), l_1158)) , g_1035[(g_490 + 1)][g_490]), 59)) , p_40) ^ p_42)), 0UL)), p_42)), l_1158)) <= g_1035[(g_490 + 1)][g_490]), l_1159)) , g_151)) || l_1195) & l_75), 13)) && l_1158) , 0x151CL) , (**g_754)))) ^ (*l_1156)) & (*g_610)))) , 0xCF716E0EL) && p_39)) | l_1155) || (-7L)) <= g_356), p_38)) || (*g_60)));
                        return p_39;
                    }
                    else
                    { /* block id: 559 */
                        if (g_218)
                            goto lbl_1198;
                        (*l_1156) |= l_75;
                    }
                    for (g_152 = 0; (g_152 <= 2); g_152 += 1)
                    { /* block id: 565 */
                        (*l_1156) &= ((+0x3F3154B2L) , (((safe_rshift_func_uint16_t_u_s((((**g_754) , (safe_sub_func_int8_t_s_s((safe_mul_func_uint64_t_u_u((p_38 != (safe_mod_func_uint64_t_u_u(p_41, g_22))), p_40)), (((safe_div_func_int64_t_s_s((safe_add_func_int8_t_s_s(((safe_rshift_func_int16_t_s_u(0L, 3)) , (p_39 != 0x86B77E296376A878LL)), p_42)), p_41)) == l_75) || 0L)))) ^ l_1214), 14)) < g_83[2]) ^ l_1215));
                    }
                    for (p_42 = 0; (p_42 <= 2); p_42 += 1)
                    { /* block id: 570 */
                        int8_t ***l_1218[5][6][2] = {{{(void*)0,(void*)0},{&l_1216,&l_1216},{&l_1216,&l_1216},{(void*)0,&l_1216},{&l_1216,(void*)0},{(void*)0,(void*)0}},{{&l_1216,&l_1216},{(void*)0,&l_1216},{&l_1216,&l_1216},{&l_1216,(void*)0},{(void*)0,&l_1216},{&l_1216,&l_1216}},{{&l_1216,(void*)0},{(void*)0,&l_1216},{(void*)0,(void*)0},{&l_1216,(void*)0},{&l_1216,(void*)0},{(void*)0,(void*)0}},{{&l_1216,(void*)0},{(void*)0,(void*)0},{&l_1216,(void*)0},{&l_1216,(void*)0},{(void*)0,&l_1216},{(void*)0,(void*)0}},{{&l_1216,&l_1216},{&l_1216,(void*)0},{(void*)0,&l_1216},{(void*)0,(void*)0},{&l_1216,(void*)0},{&l_1216,(void*)0}}};
                        int32_t l_1226[10];
                        uint16_t l_1230 = 65531UL;
                        int i, j, k;
                        for (i = 0; i < 10; i++)
                            l_1226[i] = 0x97397E73L;
                        l_1219 = l_1216;
                        l_1221--;
                        l_1230++;
                    }
                    for (g_356 = 0; (g_356 <= 2); g_356 += 1)
                    { /* block id: 577 */
                        uint32_t l_1236 = 0x6B44DE4EL;
                        (*g_626) = (*g_626);
                        (*g_626) = &l_74;
                        --l_1236;
                        l_1157[1] = func_66((*g_257));
                    }
                }
            }
            if ((1UL && (safe_unary_minus_func_uint8_t_u((l_1229[0][6] == ((l_1240 == (l_1241[1][2] = &g_356)) == (((+(safe_add_func_int32_t_s_s((safe_sub_func_uint16_t_u_u((0xB19FL & (&p_41 != (p_38 , &p_41))), (safe_add_func_int64_t_s_s(p_40, 0x3AC10B431317E0B7LL)))), (*g_989)))) , (void*)0) == (void*)0)))))))
            { /* block id: 586 */
                uint32_t l_1258 = 1UL;
                int64_t *l_1261 = &g_155;
                uint16_t l_1262 = 0x4AE1L;
                (*g_60) = 0xA727D654L;
                l_1262 ^= (g_730[0][0] | ((((*l_1261) = (l_1229[0][6] <= ((p_41 < l_1159) && ((((**g_609) || (+((safe_rshift_func_uint64_t_u_s((safe_rshift_func_uint8_t_u_u(((**g_609) = ((safe_lshift_func_uint8_t_u_u((safe_div_func_int64_t_s_s(p_41, (-1L))), 7)) ^ l_1258)), (safe_add_func_uint32_t_u_u(0x7DAAF11FL, 1L)))), l_1235[2][3][4])) , l_1227))) & (*g_60)) != p_42)))) ^ p_41) | 1L));
            }
            else
            { /* block id: 591 */
                int64_t l_1263 = (-2L);
                int8_t ***l_1279 = &l_1219;
                int8_t ****l_1278 = &l_1279;
                int16_t *l_1330 = (void*)0;
                int32_t *l_1349 = &l_74;
                l_1263 &= l_1235[0][0][3];
                if ((*g_60))
                { /* block id: 593 */
                    if (g_47)
                        goto lbl_1198;
                }
                else
                { /* block id: 595 */
                    int32_t *l_1264 = &l_1160[0];
                    int32_t *l_1265 = &l_1234;
                    int32_t *l_1266 = (void*)0;
                    int32_t *l_1267[4][4] = {{&l_74,&l_74,&l_74,&l_74},{&l_74,&l_74,&l_74,&l_74},{&l_74,&l_74,&l_74,&l_74},{&l_74,&l_74,&l_74,&l_74}};
                    uint32_t l_1268[7] = {1UL,1UL,0x799A93A4L,1UL,1UL,0x799A93A4L,1UL};
                    uint16_t *l_1286 = (void*)0;
                    uint16_t *l_1287 = &g_83[0];
                    uint16_t *l_1288 = &l_1214;
                    const int8_t l_1297 = 0xD3L;
                    int8_t **l_1320 = &l_1220;
                    int16_t *l_1326 = &g_355;
                    int16_t **l_1325 = &l_1326;
                    int16_t **l_1329 = (void*)0;
                    int i, j;
                    --l_1268[1];
                    if ((l_1271 == (((*l_1288) = (safe_lshift_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((p_38 != (**g_235)), (l_1233[1][3] , ((l_1224 , ((*l_1287) = ((l_1263 , ((safe_rshift_func_int64_t_s_s(((l_1278 != (void*)0) | ((safe_add_func_int64_t_s_s((((l_1224 |= (safe_lshift_func_int32_t_s_u((safe_mod_func_uint64_t_u_u(((((1L || p_38) ^ 8UL) == l_1263) == 4UL), l_1159)), l_1214))) < p_41) <= l_1263), p_38)) > l_1229[3][4])), 43)) && l_1263)) != p_41))) , p_39)))), p_41))) , (void*)0)))
                    { /* block id: 600 */
                        uint64_t l_1289 = 0UL;
                        uint32_t *l_1298 = &g_54;
                        int64_t *l_1299 = &l_1233[1][3];
                        int32_t l_1300 = (-10L);
                        if ((*g_60))
                            break;
                        l_1300 = (((*g_610) == (l_1289 &= (p_42 ^ 0x317CAA49ADE30E52LL))) , ((p_42 , (((safe_sub_func_int8_t_s_s(l_1292[3][3], ((safe_mod_func_int64_t_s_s(((*l_1299) ^= (safe_add_func_uint32_t_u_u((l_1289 , (l_1297 , p_42)), ((*l_1298) = (((-1L) & (**g_754)) && g_990))))), 0xC56149ADE56A7B0CLL)) | l_1289))) > 0x2320L) < 0x7FA0L)) , p_39));
                        if (p_38)
                            continue;
                    }
                    else
                    { /* block id: 607 */
                        uint8_t l_1305 = 0x7FL;
                        uint16_t **l_1322 = &l_1286;
                        (*g_626) = func_66(((safe_sub_func_int32_t_s_s((safe_add_func_int64_t_s_s((l_1305 , ((safe_div_func_int8_t_s_s((**g_754), (safe_add_func_uint8_t_u_u(((l_1312 | (safe_sub_func_uint16_t_u_u((((((((*l_1322) = ((safe_unary_minus_func_int32_t_s(((*l_1264) = (((safe_add_func_uint16_t_u_u(((-9L) == l_1305), ((safe_add_func_int32_t_s_s((((g_1321 = l_1320) == (void*)0) <= (p_40 , p_39)), p_41)) <= p_39))) < p_42) > g_149)))) , &g_83[2])) == l_1323[1][0]) , &g_730[0][0]) != (void*)0) , g_20) == 0xC9585B67191621C6LL), g_203))) >= 0L), p_39)))) & p_40)), p_41)), 4294967286UL)) , 0xDEFFF004L));
                        if (l_1324)
                            continue;
                    }
                    l_1158 = ((((255UL == (((*l_1271) ^= ((g_1327[3] = ((*l_1325) = (void*)0)) != (l_1330 = l_1328[3][0][4]))) | (*g_610))) , (l_1330 != l_1331[1][5][3])) != 1UL) & ((g_730[4][0] = (g_119 = 0xFE65L)) <= p_42));
                }
                for (g_100 = 0; (g_100 <= 3); g_100 += 1)
                { /* block id: 624 */
                    uint8_t l_1338 = 251UL;
                    int32_t *l_1343 = &g_128;
                    int32_t ***l_1346 = &g_626;
                    int32_t ****l_1345 = &l_1346;
                    int i, j;
                    if (((-1L) || (safe_mul_func_int64_t_s_s((safe_sub_func_int8_t_s_s(0x77L, ((*l_1271) = (safe_mod_func_uint8_t_u_u(l_1338, l_1158))))), (safe_mod_func_int32_t_s_s((&l_1155 != (void*)0), (l_1229[g_100][(g_91 + 2)] = (((((0x70L & (&g_1088 != ((safe_rshift_func_int8_t_s_s(((g_730[7][0] , l_1343) == l_1344), p_42)) , l_1345))) , 0xF401ED96L) , l_1347) != (void*)0) | p_41))))))))
                    { /* block id: 627 */
                        l_1349 = func_66((((g_445[2] = ((void*)0 != l_1348)) <= ((*l_1271) = ((void*)0 != &g_1069))) , p_42));
                    }
                    else
                    { /* block id: 631 */
                        if (p_41)
                            goto lbl_1198;
                        if (p_39)
                            continue;
                    }
                    if (p_41)
                        continue;
                }
            }
            for (l_1215 = (-22); (l_1215 >= 7); l_1215 = safe_add_func_uint8_t_u_u(l_1215, 8))
            { /* block id: 640 */
                (*g_1352) ^= p_41;
            }
        }
        (*g_60) = (l_1353 , (safe_mod_func_uint32_t_u_u(l_1356[0], ((*l_1357) = 0xC369789AL))));
    }
    if (((((p_41 | (l_1160[1] = (safe_mul_func_uint8_t_u_u(((**g_609) &= p_40), (safe_add_func_uint64_t_u_u((safe_rshift_func_int8_t_s_s(((*l_1368) = ((((*g_1352) = (*g_60)) >= (((((0xC203F941L < ((safe_mod_func_int32_t_s_s((l_1183 == l_1183), (((p_38 & (l_74 , ((1L < p_41) > p_42))) || l_1356[0]) ^ p_39))) <= p_40)) <= 255UL) != 0x9DL) < 0xE936994EF5F3B5F8LL) | l_1366[4][0][0])) && l_1367)), 4)), 0x23622564F78A3383LL)))))) , 6L) , 9L) > g_1370))
    { /* block id: 651 */
        int8_t l_1371 = 0xD9L;
        int64_t *l_1374 = &g_47;
        int32_t *l_1377 = &l_1160[0];
        int32_t ***l_1385 = &g_626;
        int32_t ****l_1384 = &l_1385;
        int8_t ***l_1389 = &l_1219;
        int8_t ****l_1388[1];
        uint32_t l_1392 = 0x618DD8D7L;
        int16_t l_1418 = 0L;
        uint64_t l_1566[10][4][6] = {{{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0x095D8422FD1D7328LL},{1UL,18446744073709551615UL,0xA532FD39E9D43394LL,1UL,0xA532FD39E9D43394LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0x095D8422FD1D7328LL,18446744073709551615UL,0xA532FD39E9D43394LL,0xA532FD39E9D43394LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0x095D8422FD1D7328LL}},{{1UL,18446744073709551615UL,0xA532FD39E9D43394LL,1UL,0xA532FD39E9D43394LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0x095D8422FD1D7328LL,18446744073709551615UL,0xA532FD39E9D43394LL,0xA532FD39E9D43394LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0x095D8422FD1D7328LL},{1UL,18446744073709551615UL,0xA532FD39E9D43394LL,1UL,0xA532FD39E9D43394LL,18446744073709551615UL}},{{18446744073709551615UL,18446744073709551615UL,0x095D8422FD1D7328LL,18446744073709551615UL,0xA532FD39E9D43394LL,0xA532FD39E9D43394LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0x095D8422FD1D7328LL},{1UL,18446744073709551615UL,0xA532FD39E9D43394LL,1UL,0xA532FD39E9D43394LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0x095D8422FD1D7328LL,18446744073709551615UL,0xA532FD39E9D43394LL,0xA532FD39E9D43394LL}},{{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0x095D8422FD1D7328LL},{1UL,18446744073709551615UL,0xA532FD39E9D43394LL,1UL,0xA532FD39E9D43394LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0x095D8422FD1D7328LL,18446744073709551615UL,0xA532FD39E9D43394LL,0xA532FD39E9D43394LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0x095D8422FD1D7328LL}},{{1UL,18446744073709551615UL,0xA532FD39E9D43394LL,1UL,0xA532FD39E9D43394LL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,0x095D8422FD1D7328LL,18446744073709551615UL,0xA532FD39E9D43394LL,0xA532FD39E9D43394LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0x095D8422FD1D7328LL},{1UL,18446744073709551615UL,0xA532FD39E9D43394LL,1UL,0xA532FD39E9D43394LL,18446744073709551615UL}},{{18446744073709551615UL,18446744073709551615UL,0x095D8422FD1D7328LL,18446744073709551615UL,0xA532FD39E9D43394LL,0xA532FD39E9D43394LL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,0xA532FD39E9D43394LL,0xCB04133E05D7DDA4LL},{0x095D8422FD1D7328LL,2UL,0x968C92109DA60A1BLL,0x095D8422FD1D7328LL,0x968C92109DA60A1BLL,2UL},{0xA532FD39E9D43394LL,2UL,0xCB04133E05D7DDA4LL,0xA532FD39E9D43394LL,0x968C92109DA60A1BLL,0x968C92109DA60A1BLL}},{{18446744073709551615UL,2UL,2UL,18446744073709551615UL,0x968C92109DA60A1BLL,0xCB04133E05D7DDA4LL},{0x095D8422FD1D7328LL,2UL,0x968C92109DA60A1BLL,0x095D8422FD1D7328LL,0x968C92109DA60A1BLL,2UL},{0xA532FD39E9D43394LL,2UL,0xCB04133E05D7DDA4LL,0xA532FD39E9D43394LL,0x968C92109DA60A1BLL,0x968C92109DA60A1BLL},{18446744073709551615UL,2UL,2UL,18446744073709551615UL,0x968C92109DA60A1BLL,0xCB04133E05D7DDA4LL}},{{0x095D8422FD1D7328LL,2UL,0x968C92109DA60A1BLL,0x095D8422FD1D7328LL,0x968C92109DA60A1BLL,2UL},{0xA532FD39E9D43394LL,2UL,0xCB04133E05D7DDA4LL,0xA532FD39E9D43394LL,0x968C92109DA60A1BLL,0x968C92109DA60A1BLL},{18446744073709551615UL,2UL,2UL,18446744073709551615UL,0x968C92109DA60A1BLL,0xCB04133E05D7DDA4LL},{0x095D8422FD1D7328LL,2UL,0x968C92109DA60A1BLL,0x095D8422FD1D7328LL,0x968C92109DA60A1BLL,2UL}},{{0xA532FD39E9D43394LL,2UL,0xCB04133E05D7DDA4LL,0xA532FD39E9D43394LL,0x968C92109DA60A1BLL,0x968C92109DA60A1BLL},{18446744073709551615UL,2UL,2UL,18446744073709551615UL,0x968C92109DA60A1BLL,0xCB04133E05D7DDA4LL},{0x095D8422FD1D7328LL,2UL,0x968C92109DA60A1BLL,0x095D8422FD1D7328LL,0x968C92109DA60A1BLL,2UL},{0xA532FD39E9D43394LL,2UL,0xCB04133E05D7DDA4LL,0xA532FD39E9D43394LL,0x968C92109DA60A1BLL,0x968C92109DA60A1BLL}},{{18446744073709551615UL,2UL,2UL,18446744073709551615UL,0x968C92109DA60A1BLL,0xCB04133E05D7DDA4LL},{0x095D8422FD1D7328LL,2UL,0x968C92109DA60A1BLL,0x095D8422FD1D7328LL,0x968C92109DA60A1BLL,2UL},{0xA532FD39E9D43394LL,2UL,0xCB04133E05D7DDA4LL,0xA532FD39E9D43394LL,0x968C92109DA60A1BLL,0x968C92109DA60A1BLL},{18446744073709551615UL,2UL,2UL,18446744073709551615UL,0x968C92109DA60A1BLL,0xCB04133E05D7DDA4LL}}};
        const uint64_t l_1584[10][7][3] = {{{0UL,2UL,9UL},{1UL,1UL,0x683D00A0FA5B055CLL},{18446744073709551615UL,18446744073709551606UL,18446744073709551615UL},{0x7C54DA8A71B922D0LL,0x683D00A0FA5B055CLL,0UL},{1UL,0x5F5A930D05B822BDLL,18446744073709551613UL},{0x7C54DA8A71B922D0LL,0x3C6DFBB969A6877ALL,0xABC36DFD3B491848LL},{18446744073709551615UL,0x021E03CEAFB18A16LL,2UL}},{{1UL,0xF7A66481B5B79BE9LL,1UL},{0UL,0xFEF7DAFA97BA8798LL,0x021E03CEAFB18A16LL},{0x77D1F89A0BD42D40LL,0x33D2E8092C125813LL,1UL},{2UL,9UL,0x021E03CEAFB18A16LL},{1UL,1UL,1UL},{18446744073709551615UL,0xCC14DB39799780BELL,2UL},{6UL,1UL,0xABC36DFD3B491848LL}},{{18446744073709551609UL,0xB3B9F08DF02A148FLL,18446744073709551613UL},{0x39C37B75D98993AELL,0xE964BC49BA2F749BLL,0UL},{0x3922088E7A0475E8LL,0xB3B9F08DF02A148FLL,18446744073709551615UL},{0UL,1UL,0x683D00A0FA5B055CLL},{0x99C0ACB65673773CLL,0xCC14DB39799780BELL,9UL},{0xF8052E9D15B5F987LL,1UL,0x39C8D4242C7F1875LL},{18446744073709551606UL,9UL,0xEA9903413856358ELL}},{{0xC180C7D96CB48EEDLL,0x33D2E8092C125813LL,0x74FF50C704F37C73LL},{18446744073709551606UL,0xFEF7DAFA97BA8798LL,0UL},{0xF8052E9D15B5F987LL,0xF7A66481B5B79BE9LL,1UL},{0x99C0ACB65673773CLL,0x021E03CEAFB18A16LL,18446744073709551606UL},{0UL,0x3C6DFBB969A6877ALL,0xF7A66481B5B79BE9LL},{0x3922088E7A0475E8LL,0x5F5A930D05B822BDLL,0x6446D32830D13319LL},{0x39C37B75D98993AELL,0x88C98A449B322F41LL,0x1A45CDA154A82BE0LL}},{{0x91D15B74EFDA32F8LL,0UL,0UL},{0x74FF50C704F37C73LL,0UL,0x88D1D97EEC1D2929LL},{18446744073709551613UL,0xF15385DD57602531LL,18446744073709551606UL},{0x823B99997A5D8136LL,1UL,18446744073709551615UL},{0xCC14DB39799780BELL,18446744073709551615UL,2UL},{0x3C6DFBB969A6877ALL,1UL,7UL},{0x0BB35DC88846FF4BLL,0xF15385DD57602531LL,18446744073709551609UL}},{{1UL,0UL,0x88C98A449B322F41LL},{0UL,0UL,18446744073709551615UL},{0x683D00A0FA5B055CLL,0x88C98A449B322F41LL,0x9FCB4C140BEB822DLL},{0x951B81232C97DD44LL,18446744073709551611UL,0xC6564A8E78851F2DLL},{0x683D00A0FA5B055CLL,0x08B1A0E6556DAFB6LL,18446744073709551615UL},{0UL,0x43B19DE03B17D783LL,0xF15385DD57602531LL},{1UL,0x1A45CDA154A82BE0LL,0UL}},{{0x0BB35DC88846FF4BLL,0x9805E9FB0603CC17LL,0x43B19DE03B17D783LL},{0x3C6DFBB969A6877ALL,1UL,0UL},{0xCC14DB39799780BELL,18446744073709551609UL,0x43B19DE03B17D783LL},{0x823B99997A5D8136LL,0UL,0UL},{18446744073709551613UL,0xF66B3905D54208E6LL,0xF15385DD57602531LL},{0x74FF50C704F37C73LL,0x88D1D97EEC1D2929LL,18446744073709551615UL},{0x91D15B74EFDA32F8LL,0xEB47AC7D66F70B64LL,0xC6564A8E78851F2DLL}},{{1UL,0x28AD7FAC118ED01BLL,0x9FCB4C140BEB822DLL},{0xFEF7DAFA97BA8798LL,0xEB47AC7D66F70B64LL,18446744073709551615UL},{18446744073709551612UL,0x88D1D97EEC1D2929LL,0x88C98A449B322F41LL},{18446744073709551606UL,0xF66B3905D54208E6LL,18446744073709551609UL},{0xABC36DFD3B491848LL,0UL,7UL},{9UL,18446744073709551609UL,2UL},{0UL,1UL,18446744073709551615UL}},{{9UL,0x9805E9FB0603CC17LL,18446744073709551606UL},{0xABC36DFD3B491848LL,0x1A45CDA154A82BE0LL,0x88D1D97EEC1D2929LL},{18446744073709551606UL,0x43B19DE03B17D783LL,0UL},{18446744073709551612UL,0x08B1A0E6556DAFB6LL,0x1A45CDA154A82BE0LL},{0xFEF7DAFA97BA8798LL,18446744073709551611UL,0xF01FB98CB253500FLL},{1UL,0x88C98A449B322F41LL,0x1A45CDA154A82BE0LL},{0x91D15B74EFDA32F8LL,0UL,0UL}},{{0x74FF50C704F37C73LL,0UL,0x88D1D97EEC1D2929LL},{18446744073709551613UL,0xF15385DD57602531LL,18446744073709551606UL},{0x823B99997A5D8136LL,1UL,18446744073709551615UL},{0xCC14DB39799780BELL,18446744073709551615UL,2UL},{0x3C6DFBB969A6877ALL,1UL,7UL},{0x0BB35DC88846FF4BLL,0xF15385DD57602531LL,18446744073709551609UL},{1UL,0UL,0x88C98A449B322F41LL}}};
        int16_t l_1604 = 0L;
        int32_t l_1631 = (-4L);
        int32_t l_1634 = (-4L);
        int32_t l_1635 = 0xE8DF8B8AL;
        int32_t l_1636 = 0L;
        int32_t l_1637 = 0xBB1154E1L;
        int32_t l_1638 = 3L;
        int32_t l_1639 = (-9L);
        int8_t * const l_1647 = &g_1648;
        int8_t * const *l_1646[10] = {&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647,&l_1647};
        int8_t * const **l_1645 = &l_1646[5];
        int32_t l_1666 = 0xC1170F5EL;
        int32_t l_1668 = 0xEF7C9177L;
        int64_t ****l_1793 = &g_329;
        uint32_t * const l_1796 = (void*)0;
        uint32_t * const *l_1795 = &l_1796;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1388[i] = &l_1389;
        l_1371 ^= ((*g_1352) = 0xB92F0873L);
        if (((*l_1377) = (safe_lshift_func_int64_t_s_s(((*l_1374) = l_1371), (safe_mul_func_uint32_t_u_u((**g_988), 0x7FEDBC21L))))))
        { /* block id: 656 */
            int64_t *l_1393 = (void*)0;
            int64_t *l_1394 = &l_1233[1][0];
            int32_t l_1403 = 0x385EE044L;
            l_1403 = ((*l_1377) = ((safe_mod_func_uint32_t_u_u(0xA274139BL, (safe_rshift_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(((l_1384 == (void*)0) , 0x25321D44DC37F2A3LL), (l_1386 != l_1388[0]))), (((safe_lshift_func_int64_t_s_u(((*l_1374) = p_41), 35)) != (((*l_1394) |= l_1392) , (safe_lshift_func_int64_t_s_u((safe_sub_func_uint16_t_u_u(((safe_add_func_int16_t_s_s((((**g_609) = ((safe_add_func_uint64_t_u_u((p_42 = 4UL), p_39)) , p_39)) && p_40), p_38)) >= l_1403), 0x606AL)), g_445[5])))) ^ 0x96L))))) | g_1404));
        }
        else
        { /* block id: 663 */
            uint64_t l_1413[1][1][5];
            int32_t l_1478 = 0L;
            int16_t **l_1624 = &g_1327[3];
            int32_t l_1633[3];
            uint32_t l_1640 = 18446744073709551615UL;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 5; k++)
                        l_1413[i][j][k] = 0UL;
                }
            }
            for (i = 0; i < 3; i++)
                l_1633[i] = 0x3905582BL;
            if (((*l_1377) = (safe_add_func_int64_t_s_s(((0xF91867C5L == (((safe_div_func_int32_t_s_s(((safe_sub_func_uint8_t_u_u(((**g_609) ^= l_1413[0][0][0]), p_38)) && (((((safe_lshift_func_uint16_t_u_s((safe_sub_func_uint32_t_u_u((*g_989), (((((p_39 , func_66(l_1418)) == (void*)0) ^ ((**g_609) , l_1413[0][0][0])) != p_40) , p_38))), 0)) , (*g_989)) < p_40) , (-1L)) < p_40)), p_40)) < 0xA8L) <= p_39)) == l_1413[0][0][0]), p_41))))
            { /* block id: 666 */
lbl_1632:
                (*l_1377) = 0x9A30D140L;
            }
            else
            { /* block id: 668 */
                int8_t *****l_1423 = (void*)0;
                uint64_t *l_1424 = &g_356;
                int8_t l_1429[8][5][6] = {{{0x81L,0x1CL,(-6L),0xC3L,(-1L),0xC3L},{1L,0x8BL,1L,(-10L),0L,0x5DL},{(-6L),0x1CL,0x81L,0xEDL,0xDDL,1L},{5L,4L,0xC9L,0xEDL,0L,(-10L)},{(-6L),(-1L),(-8L),(-10L),(-8L),(-1L)}},{{1L,0x60L,0x09L,0xC3L,5L,(-10L)},{0x81L,0xC3L,0xBCL,(-1L),(-10L),1L},{0xC9L,0xC3L,(-1L),0xE8L,5L,0x5DL},{(-8L),0x60L,0xDDL,0x60L,0x81L,(-1L)},{(-1L),4L,1L,0x1CL,0x09L,0x5DL}},{{0x07L,(-10L),0xA8L,4L,(-10L),0x5DL},{1L,0x5DL,1L,(-1L),0L,(-1L)},{(-10L),0xE8L,(-10L),0x60L,0x69L,1L},{1L,0x5DL,1L,0xC3L,0x7BL,0xEDL},{0xA8L,(-10L),0x07L,0xC3L,(-8L),0x60L}},{{1L,4L,(-1L),0x60L,(-1L),4L},{(-10L),0x8BL,0x81L,(-1L),0xA8L,0x60L},{1L,(-1L),5L,4L,0xC9L,0xEDL},{0x07L,(-1L),0xDDL,0x1CL,0xA8L,1L},{(-1L),0x8BL,0x7BL,0x8BL,(-1L),(-1L)}},{{0x81L,4L,0x86L,0x1CL,(-8L),0x5DL},{5L,(-10L),0L,4L,0x7BL,0x5DL},{0xDDL,0x5DL,0x86L,(-1L),0x69L,(-1L)},{0x7BL,0xE8L,0x7BL,0x60L,0L,1L},{0x86L,0x5DL,0xDDL,0xC3L,(-10L),0xEDL}},{{0L,(-10L),5L,0xC3L,0x09L,0x60L},{0x86L,4L,0x81L,0x60L,0x81L,4L},{0x7BL,0x8BL,(-1L),(-1L),0L,0x60L},{0xDDL,(-1L),0x07L,4L,0xBCL,0xEDL},{5L,(-1L),1L,0x1CL,0L,1L}},{{0x81L,0x8BL,(-10L),0x8BL,0x81L,(-1L)},{(-1L),4L,1L,0x1CL,0x09L,0x5DL},{0x07L,(-10L),0xA8L,4L,(-10L),0x5DL},{1L,0x5DL,1L,(-1L),0L,(-1L)},{(-10L),0xE8L,(-10L),0x60L,0x69L,1L}},{{1L,0x5DL,1L,0xC3L,0x7BL,0xEDL},{0xA8L,(-10L),0x07L,0xC3L,(-8L),0x60L},{1L,4L,(-1L),0x60L,(-1L),4L},{(-10L),0x8BL,0x81L,(-1L),0xA8L,0x60L},{1L,(-1L),5L,4L,0xC9L,0xEDL}}};
                int32_t *l_1433 = &g_490;
                int32_t l_1436 = 0x24FDECE9L;
                int64_t l_1456 = 0x9A8A608E241596EALL;
                int32_t *l_1460[4][2][3] = {{{(void*)0,(void*)0,(void*)0},{&g_149,&g_149,&g_149}},{{(void*)0,(void*)0,(void*)0},{&g_149,&g_149,&g_149}},{{(void*)0,(void*)0,(void*)0},{&g_149,&g_149,&g_149}},{{(void*)0,(void*)0,(void*)0},{&g_149,&g_149,&g_149}}};
                uint16_t **l_1556 = (void*)0;
                int64_t ** const ** const l_1590 = (void*)0;
                const int64_t ****l_1595 = &g_1592;
                int16_t ***l_1603 = (void*)0;
                int16_t **l_1623 = &g_1327[3];
                int i, j, k;
                if (((*l_1377) = ((safe_sub_func_uint64_t_u_u((safe_add_func_uint64_t_u_u((l_1423 != (void*)0), ((*l_1424) = g_119))), (((l_1436 = ((safe_mul_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u(((p_42 , (l_1429[1][3][1] == p_42)) <= (+(safe_add_func_uint8_t_u_u((((*l_1433) &= l_1429[1][3][1]) , ((safe_sub_func_int64_t_s_s(l_1413[0][0][0], (((*l_1377) == 65531UL) >= 253UL))) != 0UL)), l_1413[0][0][4])))), 6)) || p_41), 0xE8L)) && l_1413[0][0][0])) & l_1413[0][0][4]) , (*l_1377)))) & p_42)))
                { /* block id: 673 */
                    int32_t **l_1444 = &l_1433;
                    int32_t ***l_1443 = &l_1444;
                    const int32_t ***l_1447 = &g_1445[2][3];
                    int32_t l_1450 = 0x380464B6L;
                    int64_t l_1497 = 0x88767BB8A1E8E358LL;
                    if (((~(safe_div_func_uint64_t_u_u(((*l_1424) = 18446744073709551609UL), (safe_unary_minus_func_uint32_t_u(((safe_lshift_func_int32_t_s_u((((((*l_1443) = &l_1433) != ((*l_1447) = g_1445[2][3])) >= ((*l_1374) = (safe_mod_func_uint16_t_u_u(l_1413[0][0][0], l_1429[1][3][1])))) != g_143[5][2]), 27)) > (l_1450 = 0x9EC4CBB5L))))))) , (safe_add_func_uint16_t_u_u((safe_sub_func_int32_t_s_s(((~(1UL < p_38)) != (*l_1377)), (**g_988))), l_1456))))
                    { /* block id: 679 */
                        g_1457--;
                    }
                    else
                    { /* block id: 681 */
                        int16_t ***l_1468 = (void*)0;
                        int16_t ***l_1469 = &l_1467;
                        int32_t l_1472[7] = {0x98B788F9L,0x98B788F9L,0x98B788F9L,0x98B788F9L,0x98B788F9L,0x98B788F9L,0x98B788F9L};
                        uint32_t *l_1473 = (void*)0;
                        uint32_t *l_1474 = (void*)0;
                        uint32_t *l_1475 = &l_1392;
                        int32_t *** const l_1479 = (void*)0;
                        uint32_t *l_1503 = (void*)0;
                        uint32_t **l_1502[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1502[i] = &l_1503;
                        (***l_1384) = l_1460[2][0][0];
                        (***l_1384) = ((((((l_1478 = (l_1461 || (safe_div_func_int8_t_s_s((((-1L) > ((safe_rshift_func_uint32_t_u_u((safe_unary_minus_func_int32_t_s((p_40 ^ ((((*l_1469) = l_1467) != (void*)0) || (l_1413[0][0][0] > 0x6E96AD681ACBE8A8LL))))), (safe_mul_func_uint16_t_u_u(((((*l_1374) ^= l_1472[0]) == (((*l_1475)++) , l_1413[0][0][3])) , p_41), p_40)))) ^ g_91)) | 65532UL), p_40)))) <= l_1450) , &g_639[3]) == l_1479) < 0UL) , &l_1472[0]);
                        (*g_626) = (p_39 , func_66((((safe_lshift_func_uint32_t_u_u(((((((((safe_lshift_func_uint64_t_u_u(0x5CC7EF1B3E31D2DCLL, (((((safe_sub_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u(0x5EL, 7)), (-1L))), (safe_rshift_func_uint32_t_u_u(((((void*)0 == &g_610) || l_1472[1]) && (p_39 = (!(safe_sub_func_int64_t_s_s(((*l_1374) = ((void*)0 != l_1495)), l_1450))))), 2)))) , (void*)0) != &l_1433) == l_1497) || l_1497))) == (****l_1384)) , 2L) > (-3L)) && p_41) ^ 0x1A47L) <= p_41) < p_42), 14)) , 18446744073709551606UL) <= (-1L))));
                        (*g_60) |= (safe_mul_func_uint32_t_u_u((6UL != (safe_add_func_int16_t_s_s((((g_1504 = func_66(p_40)) == (void*)0) , (-1L)), (g_445[2] <= 0x6422109140333D07LL)))), ((((g_1507 = (**g_754)) > (p_39 != 0x2AF47436C0982398LL)) | p_39) ^ (-1L))));
                    }
                    (***l_1495) = (*g_626);
                    for (g_54 = 0; g_54 < 2; g_54 += 1)
                    {
                        for (g_20 = 0; g_20 < 4; g_20 += 1)
                        {
                            l_1233[g_54][g_20] = 0xE0D2629196FC70D9LL;
                        }
                    }
                }
                else
                { /* block id: 697 */
                    uint16_t l_1510 = 9UL;
                    int64_t ***l_1520 = &g_330;
                    int16_t *l_1533[7][5] = {{&g_355,&g_355,&l_1418,&g_730[0][0],&g_119},{&g_355,&g_119,&g_119,&g_119,&g_119},{&g_119,&g_730[0][0],&g_355,&g_119,&g_119},{&g_730[0][0],&g_119,&l_1418,&g_119,&g_730[0][0]},{&g_355,&g_355,&g_730[0][0],&g_119,&g_355},{&g_730[0][0],&g_730[0][0],&g_730[0][0],&g_730[0][0],&g_119},{&g_119,&g_730[0][0],&l_1418,&g_355,&g_355}};
                    int16_t ***l_1560[6];
                    int32_t l_1569 = 0L;
                    int32_t l_1585 = 0x26C593DBL;
                    const int64_t *****l_1594[5][6][3] = {{{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,(void*)0,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,(void*)0}},{{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591}},{{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{(void*)0,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,(void*)0,&g_1591}},{{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591}},{{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,(void*)0},{&g_1591,&g_1591,&g_1591},{&g_1591,&g_1591,&g_1591}}};
                    uint16_t l_1596 = 1UL;
                    int32_t ***l_1602 = &g_626;
                    int i, j, k;
                    for (i = 0; i < 6; i++)
                        l_1560[i] = &l_1467;
                    if ((0xE7B2762CL <= (safe_mul_func_uint64_t_u_u((0x36L & (**g_609)), l_1413[0][0][2]))))
                    { /* block id: 698 */
                        uint32_t l_1513 = 4294967295UL;
                        int32_t l_1534 = 0x6D41FE87L;
                        ++l_1510;
                        (*l_1377) = l_1513;
                        (*l_1377) = (safe_div_func_uint16_t_u_u((((safe_lshift_func_int64_t_s_u(((**g_609) & ((((safe_mul_func_int16_t_s_s((((*g_328) != l_1520) , p_39), (safe_rshift_func_int32_t_s_u((safe_sub_func_int64_t_s_s(((safe_sub_func_uint64_t_u_u(g_203, ((**g_754) & p_39))) && p_40), ((safe_sub_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((safe_div_func_int64_t_s_s(((*l_1374) = 0x0789762D6C442DE2LL), 0x995C91B891888D35LL)) , 0xF649L), g_1507)), l_1513)) ^ p_38))), (*g_989))))) <= p_38) == 0L) , 0xF7L)), 23)) & l_1413[0][0][0]) && 0xE9D23901F5EAACAALL), 0x3B53L));
                        l_1534 |= (l_1533[0][3] == &g_119);
                    }
                    else
                    { /* block id: 704 */
                        int32_t l_1564 = 1L;
                        int32_t l_1565 = 0L;
                        (*l_1377) = (((safe_sub_func_int64_t_s_s((safe_div_func_int8_t_s_s(((p_42 ^ (p_40 < (l_1565 = (safe_rshift_func_uint64_t_u_u(l_1510, ((((safe_mod_func_uint64_t_u_u((((*l_1433) = (((((l_1564 &= (safe_add_func_int64_t_s_s((safe_unary_minus_func_int8_t_s((safe_rshift_func_int32_t_s_u((+((g_1561[1][1][5] = ((safe_rshift_func_uint16_t_u_u((((safe_add_func_uint32_t_u_u(((safe_lshift_func_uint64_t_u_s((l_1555 >= ((*l_1377) <= l_1510)), l_1478)) & (((((((void*)0 != l_1556) || 1L) , g_1557) == l_1560[0]) == (-1L)) && 0UL)), p_41)) >= p_41) , g_149), 5)) , (void*)0)) == &g_328)), 16)))), 3L))) & 0x4A7C4C8EL) , l_1510) > 0L) && p_41)) , 0xEDECEFDFE903C245LL), 18446744073709551612UL)) & 65535UL) != l_1413[0][0][0]) >= 253UL)))))) > (**g_988)), l_1566[7][2][2])), g_730[0][0])) , (void*)0) != &g_1558);
                        l_1569 = ((safe_add_func_int32_t_s_s((p_38 && l_1413[0][0][0]), (*g_1352))) > (-6L));
                    }
                    for (l_1569 = 0; (l_1569 < 17); l_1569 = safe_add_func_uint8_t_u_u(l_1569, 6))
                    { /* block id: 714 */
                        int64_t * const * const l_1577 = &l_1374;
                        l_1585 = (safe_rshift_func_uint8_t_u_s((safe_add_func_uint32_t_u_u(((*g_1504) = (safe_unary_minus_func_uint32_t_u((**g_988)))), (((((((void*)0 != l_1577) ^ g_1507) || ((safe_div_func_int32_t_s_s(p_39, (((((((*l_1377) = (g_205 |= 0xB92AL)) & ((safe_lshift_func_uint8_t_u_s(((safe_sub_func_int8_t_s_s((((g_83[2] = p_39) < ((p_41 != p_38) >= l_1584[6][4][0])) <= g_355), 0xA2L)) == 0x00CC5C66L), 1)) | p_42)) | p_42) < p_40) >= 65535UL) , 2L))) , p_41)) , 0xC2AABC15E23D6551LL) <= p_38) , p_42))), p_38));
                    }
                    (*l_1377) = ((g_100 == (safe_rshift_func_uint32_t_u_u(((((((g_205 = (safe_mod_func_int8_t_s_s(((l_1590 != (l_1595 = g_1591)) & (((((**g_235) && (g_14 < ((((*g_610) != l_1596) != (g_83[1] = (p_40 == (safe_lshift_func_uint16_t_u_s(((g_445[2] = (!(((safe_add_func_uint16_t_u_u(p_38, g_1507)) , (void*)0) != l_1602))) != p_40), p_38))))) && 65535UL))) ^ g_100) >= p_42) >= (*g_610))), 0x3CL))) < p_41) >= 1UL) != p_41) , l_1603) == &g_1558), (*g_1504)))) && l_1604);
                    for (l_1169 = 0; (l_1169 == (-16)); --l_1169)
                    { /* block id: 728 */
                        int64_t ** const **l_1628 = &g_1563;
                        uint8_t l_1629 = 1UL;
                        uint32_t *l_1630 = &g_728;
                        l_1631 ^= ((safe_sub_func_int64_t_s_s(((!((safe_lshift_func_int16_t_s_s(0L, 10)) , (((((safe_mod_func_uint16_t_u_u(p_38, (*l_1377))) , (*g_1068)) != g_1614) | (safe_rshift_func_uint32_t_u_u(((*l_1630) ^= (safe_rshift_func_uint64_t_u_s((((l_1624 = l_1623) == (void*)0) ^ ((+(*g_1504)) <= (safe_div_func_int8_t_s_s(((((g_1035[0][0] , l_1628) != l_1590) , p_38) && l_1629), p_41)))), 2))), (*l_1377)))) <= l_1629))) >= (-4L)), p_41)) == p_40);
                        if (l_1510)
                            goto lbl_1632;
                    }
                }
                l_1640--;
            }
        }
        for (l_1555 = 0; (l_1555 <= 0); l_1555 += 1)
        { /* block id: 740 */
            int8_t * const *l_1644 = &l_1220;
            int8_t * const **l_1643 = &l_1644;
            int32_t l_1649 = (-1L);
            int32_t l_1658 = 0L;
            int32_t l_1663[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
            int16_t l_1667 = 0x9E68L;
            uint16_t l_1669 = 0x1230L;
            const int32_t *l_1729 = &l_1666;
            const int32_t **l_1728 = &l_1729;
            uint16_t l_1817 = 65531UL;
            int i;
        }
    }
    else
    { /* block id: 835 */
        uint8_t l_1847 = 0xF9L;
        int64_t ** const **l_1907 = (void*)0;
        int32_t l_1909[8][1] = {{(-9L)},{0xA107B59CL},{0xA107B59CL},{(-9L)},{0xA107B59CL},{0xA107B59CL},{(-9L)},{0xA107B59CL}};
        uint32_t **l_1925 = &g_1504;
        uint32_t ***l_1924 = &l_1925;
        int8_t l_1948[2];
        int16_t ***l_1995 = &g_1846;
        int16_t ***l_1997[1];
        int8_t ***l_2021 = &g_1321;
        int8_t ****l_2020 = &l_2021;
        int8_t *****l_2019 = &l_2020;
        int16_t * const *l_2041 = (void*)0;
        uint8_t l_2097 = 255UL;
        int32_t l_2171 = 0x40C3C18BL;
        int32_t l_2176[5][1] = {{0L},{0xF6D15A64L},{0L},{0xF6D15A64L},{0L}};
        uint32_t l_2179 = 2UL;
        uint32_t **l_2186 = &g_2101;
        int32_t ****l_2277 = &l_1496;
        uint16_t *l_2348[9][8][3] = {{{&g_205,&g_205,&g_83[1]},{&l_1356[0],(void*)0,&g_83[0]},{(void*)0,&g_83[1],&l_1356[0]},{&g_83[2],(void*)0,(void*)0},{&l_1356[0],(void*)0,&l_1356[0]},{&g_205,&l_1356[0],&l_1356[0]},{&l_1356[0],&g_83[1],(void*)0},{&l_2006,&l_1356[0],(void*)0}},{{&g_205,&l_1356[0],&l_1356[0]},{&l_2006,&g_83[0],(void*)0},{&l_1356[0],&g_83[2],&g_83[1]},{&g_205,(void*)0,&l_2006},{(void*)0,(void*)0,&g_83[2]},{(void*)0,(void*)0,&l_1356[0]},{&g_205,&g_83[2],&g_83[1]},{&l_1356[0],&g_83[0],(void*)0}},{{&g_83[1],&l_1356[0],&g_83[2]},{(void*)0,&l_1356[0],(void*)0},{(void*)0,&g_83[1],&g_83[1]},{&g_83[2],&l_1356[0],&l_1356[0]},{&l_1356[0],&g_205,&g_83[2]},{&g_83[2],&l_2006,&l_2006},{&l_1356[0],&g_205,&g_83[1]},{&g_83[2],(void*)0,(void*)0}},{{(void*)0,&g_83[1],&l_1356[0]},{(void*)0,&l_1356[0],(void*)0},{&g_83[1],&g_83[1],(void*)0},{&l_1356[0],(void*)0,&l_1356[0]},{&g_205,&g_205,(void*)0},{(void*)0,&l_2006,(void*)0},{(void*)0,&g_205,(void*)0},{&g_205,&l_1356[0],&l_1356[0]}},{{&l_1356[0],&g_83[1],(void*)0},{&l_2006,&l_1356[0],(void*)0},{&g_205,&l_1356[0],&l_1356[0]},{&l_2006,&g_83[0],(void*)0},{&l_1356[0],&g_83[2],&g_83[1]},{&g_205,(void*)0,&l_2006},{(void*)0,(void*)0,&g_83[2]},{(void*)0,(void*)0,&l_1356[0]}},{{&g_205,&g_83[2],&g_83[1]},{&l_1356[0],&g_83[0],(void*)0},{&g_83[1],&l_1356[0],&g_83[2]},{(void*)0,&l_1356[0],(void*)0},{(void*)0,&g_83[1],&g_83[1]},{&g_83[2],&l_1356[0],&l_1356[0]},{&l_1356[0],&g_205,&g_83[2]},{&g_83[2],&l_2006,&l_2006}},{{&l_1356[0],&g_205,&g_83[1]},{&g_83[2],(void*)0,(void*)0},{(void*)0,&g_83[1],&l_1356[0]},{(void*)0,&l_1356[0],(void*)0},{&g_83[1],&g_83[1],(void*)0},{&l_1356[0],(void*)0,&l_1356[0]},{&g_205,&g_205,(void*)0},{(void*)0,&l_2006,(void*)0}},{{(void*)0,&g_205,(void*)0},{&g_205,&l_1356[0],&l_1356[0]},{&l_1356[0],&g_83[1],(void*)0},{&l_2006,&l_1356[0],(void*)0},{&g_205,&l_1356[0],&l_1356[0]},{&l_2006,&g_83[0],(void*)0},{&l_1356[0],&g_83[2],&g_83[1]},{&g_205,(void*)0,&l_2006}},{{(void*)0,(void*)0,&g_83[2]},{(void*)0,(void*)0,&l_1356[0]},{&g_205,&g_83[2],&g_83[1]},{&l_1356[0],&g_83[0],(void*)0},{&g_83[1],&l_1356[0],&l_1356[0]},{(void*)0,&g_205,(void*)0},{&g_83[1],&g_205,&g_83[2]},{(void*)0,&g_83[0],&l_1356[0]}}};
        uint64_t l_2397 = 18446744073709551615UL;
        int8_t l_2436 = (-7L);
        int64_t *l_2449 = &g_2261;
        int32_t *l_2451[8] = {&g_203,&g_203,&g_4,&g_203,&g_203,&g_4,&g_203,&g_203};
        int8_t l_2452 = (-9L);
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1948[i] = 0xFCL;
        for (i = 0; i < 1; i++)
            l_1997[i] = (void*)0;
        for (g_23 = 0; (g_23 <= 0); g_23 += 1)
        { /* block id: 838 */
            const int32_t * const *l_1829[8][1] = {{&g_1446},{&g_1446},{&g_1446},{&g_1446},{&g_1446},{&g_1446},{&g_1446},{&g_1446}};
            int32_t l_1833 = 0L;
            uint32_t l_1860 = 4294967286UL;
            const int32_t *l_1871 = &l_1160[0];
            const int32_t * const *l_1870 = &l_1871;
            const int32_t * const **l_1869 = &l_1870;
            uint32_t l_1898 = 0xDCD963CCL;
            int32_t l_1937 = 0xAA5A4580L;
            int32_t l_1938 = 0x95F67A87L;
            int32_t l_1939[5][6][1] = {{{0x5C87D8B1L},{0x197961BEL},{0L},{0x407EF8EBL},{0x96C6C7C6L},{0xDD2064F1L}},{{0L},{0L},{0xDD2064F1L},{0x96C6C7C6L},{0x407EF8EBL},{0L}},{{0x197961BEL},{0x5C87D8B1L},{0x197961BEL},{0L},{0x407EF8EBL},{0x96C6C7C6L}},{{0xDD2064F1L},{0L},{0L},{0xDD2064F1L},{0x96C6C7C6L},{0x407EF8EBL}},{{0L},{0x197961BEL},{0x5C87D8B1L},{0x197961BEL},{0L},{0x407EF8EBL}}};
            uint64_t l_1942[4];
            uint16_t l_1949[6][8] = {{0xDCFEL,9UL,9UL,0xDCFEL,9UL,9UL,0xDCFEL,9UL},{0xDCFEL,0xDCFEL,0x3BD8L,0xDCFEL,0xDCFEL,0x3BD8L,0xDCFEL,0xDCFEL},{9UL,0xDCFEL,9UL,9UL,0xDCFEL,9UL,9UL,0xDCFEL},{0xDCFEL,9UL,9UL,0xDCFEL,9UL,9UL,0xDCFEL,9UL},{0xDCFEL,0xDCFEL,0x3BD8L,0xDCFEL,0xDCFEL,0x3BD8L,0xDCFEL,0xDCFEL},{9UL,0xDCFEL,9UL,9UL,0xDCFEL,9UL,9UL,0xDCFEL}};
            int8_t l_2026 = 9L;
            int16_t * const *l_2040 = (void*)0;
            uint32_t **l_2042 = &g_1504;
            uint16_t *l_2067 = &g_205;
            uint32_t l_2077 = 0x1CD61BEDL;
            int32_t l_2090 = 1L;
            uint32_t l_2183 = 0UL;
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_1942[i] = 18446744073709551615UL;
            (*g_1352) = ((((*g_235) == &p_39) && ((**g_609) , (((((safe_unary_minus_func_int64_t_s((safe_lshift_func_int64_t_s_u(((*g_989) > (safe_add_func_uint64_t_u_u((safe_mul_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(p_42, (safe_lshift_func_uint16_t_u_u(((void*)0 != l_1829[6][0]), 4)))), (safe_rshift_func_int16_t_s_u(l_1832, 12)))), (l_1833 && p_42)))), p_40)))) <= p_40) == p_42) , l_1833) && 0x99C3DA7A55F774CELL))) , l_1833);
            for (g_149 = 0; (g_149 <= 0); g_149 += 1)
            { /* block id: 842 */
                int16_t **l_1844 = &l_1369[0][7][0];
                int16_t ***l_1845[8];
                int32_t l_1850 = 0x191B8495L;
                uint32_t l_1877[7][1] = {{8UL},{4294967294UL},{8UL},{8UL},{4294967294UL},{8UL},{8UL}};
                int32_t l_1878[7];
                uint8_t l_1879[9][4][7] = {{{8UL,0x3CL,254UL,0x14L,0x0CL,255UL,0UL},{1UL,0x68L,0x3DL,246UL,0x26L,0UL,1UL},{0UL,0xFDL,0xDCL,0xB9L,0UL,0UL,0x0BL},{0xF1L,0x3CL,0xF4L,0xDCL,246UL,249UL,0x13L}},{{1UL,0x25L,0x6DL,246UL,0x68L,255UL,0UL},{6UL,0UL,0x5BL,246UL,0x76L,8UL,0x0CL},{0xFDL,1UL,254UL,0xDCL,0x0BL,0x13L,0x56L},{0xB2L,248UL,250UL,0xB9L,1UL,0x7BL,1UL}},{{246UL,0xF1L,0xF1L,246UL,0xFDL,0xEDL,1UL},{5UL,1UL,255UL,0x14L,246UL,0x13L,0x5AL},{0UL,1UL,0x87L,0x68L,6UL,250UL,1UL},{246UL,0x5BL,0UL,1UL,1UL,0x5BL,0x5AL}},{{0x5BL,255UL,1UL,3UL,0UL,0x87L,0x3DL},{255UL,0x14L,0x26L,0UL,0xDCL,248UL,0UL},{0xDCL,1UL,0xBCL,8UL,250UL,0xBCL,255UL},{0xF4L,0xF4L,0xBCL,255UL,0xEDL,250UL,1UL}},{{0UL,5UL,0x26L,0x13L,0xFDL,1UL,0UL},{0xF1L,0x74L,1UL,255UL,0x3DL,0x3DL,255UL},{0x02L,0UL,0x02L,250UL,0x5BL,0x6DL,0xF4L},{5UL,254UL,246UL,0UL,0xFDL,1UL,0x76L}},{{0UL,255UL,0xCAL,5UL,0x7BL,0x6DL,0xDCL},{250UL,1UL,6UL,0x02L,8UL,0x3DL,0x6DL},{255UL,0x56L,0x25L,0xF1L,0xDCL,1UL,0x76L},{0x13L,0x3DL,3UL,0UL,0UL,250UL,249UL}},{{255UL,0xF1L,1UL,0xF4L,0xF4L,0xBCL,255UL},{8UL,0xF1L,0UL,0xDCL,1UL,248UL,0UL},{0UL,0x3DL,1UL,255UL,0xF4L,0x87L,0x56L},{3UL,0x56L,1UL,0x5BL,0xF1L,0x5BL,1UL}},{{1UL,1UL,1UL,0UL,0x5AL,0x25L,0UL},{0xF1L,255UL,3UL,255UL,6UL,1UL,0x5BL},{0x1DL,254UL,0x74L,0x6DL,0x5AL,0UL,255UL},{249UL,0UL,0xAFL,249UL,0xF1L,0x68L,0x14L}},{{0xDCL,0x74L,0xCAL,0UL,0xF4L,1UL,0UL},{1UL,5UL,255UL,0xBCL,1UL,0x74L,1UL},{0UL,0xF4L,0xE4L,1UL,0xF4L,6UL,0x5AL},{0UL,1UL,1UL,5UL,0UL,0x87L,0x5BL}}};
                uint32_t **l_1923 = &g_1504;
                uint32_t ***l_1922 = &l_1923;
                int32_t *l_1946 = &l_1878[6];
                int i, j, k;
                for (i = 0; i < 8; i++)
                    l_1845[i] = &l_1467;
                for (i = 0; i < 7; i++)
                    l_1878[i] = 0L;
                if (((safe_rshift_func_int16_t_s_u((safe_rshift_func_int64_t_s_s((safe_div_func_uint64_t_u_u(18446744073709551610UL, (safe_add_func_uint32_t_u_u((0UL | ((g_1846 = l_1844) == (*g_1557))), p_39)))), 9)), 15)) > 0x17L))
                { /* block id: 844 */
                    int8_t ***l_1861 = &l_1219;
                    int8_t ***l_1862 = &g_1321;
                    int i, j;
                    ++l_1847;
                    if (l_1850)
                        continue;
                    l_1850 = (safe_lshift_func_uint16_t_u_u((((((safe_rshift_func_int8_t_s_s(0x56L, (l_1833 >= (safe_mul_func_int8_t_s_s(7L, ((~(0x81ADL & ((p_39 || (((*g_1504) <= (safe_lshift_func_int16_t_s_u(l_1860, 13))) | 255UL)) <= (((*l_1862) = g_1321) == (void*)0)))) <= g_143[0][1])))))) == 0xAE87FDFA7C0BB77FLL) > 0L) >= 0x40F8L) , p_41), 15));
                }
                else
                { /* block id: 849 */
                    for (l_1555 = 0; (l_1555 >= 0); l_1555 -= 1)
                    { /* block id: 852 */
                        int i, j;
                        g_1035[(l_1555 + 1)][g_149] = (safe_mul_func_int32_t_s_s(((g_1035[(g_149 + 1)][l_1555] , l_1847) < 0UL), p_40));
                    }
                    (*g_1880) = ((safe_sub_func_uint64_t_u_u(((((p_40 != (safe_mod_func_int16_t_s_s((&g_1089 == (l_1833 , l_1869)), l_1850))) , ((*l_1368) |= (0x1C6DC38CL && (((((((safe_rshift_func_int64_t_s_u((safe_unary_minus_func_uint64_t_u(1UL)), (((***l_1869) >= ((safe_mod_func_uint64_t_u_u(g_478[0][0][0], l_1877[2][0])) | p_42)) == l_1878[6]))) | 8L) && p_41) != l_1879[8][1][2]) , p_38) && (***l_1869)) , (*l_1871))))) < (*g_610)) < p_39), l_1850)) > (*g_1352));
                }
                for (p_39 = 0; (p_39 <= 0); p_39 += 1)
                { /* block id: 860 */
                    uint64_t l_1908 = 3UL;
                    uint8_t *l_1926 = (void*)0;
                    int32_t l_1932 = 0L;
                    int32_t l_1934 = 0L;
                    int32_t l_1935 = 0xDB8B9ABAL;
                    int32_t l_1940 = 0x7FB10B67L;
                    int32_t l_1941[9][10][1] = {{{1L},{0xC536A4BBL},{(-1L)},{1L},{0xF22175F0L},{0xF22175F0L},{1L},{(-1L)},{0xC536A4BBL},{1L}},{{0xBC0AFC99L},{1L},{0xC536A4BBL},{(-1L)},{1L},{0xF22175F0L},{0xF22175F0L},{1L},{1L},{0xBC0AFC99L}},{{0x322DB86FL},{0x2AC4A2ECL},{0x322DB86FL},{0xBC0AFC99L},{1L},{1L},{1L},{1L},{1L},{1L}},{{0xBC0AFC99L},{0x322DB86FL},{0x2AC4A2ECL},{0x322DB86FL},{0xBC0AFC99L},{1L},{1L},{1L},{1L},{1L}},{{1L},{0xBC0AFC99L},{0x322DB86FL},{0x2AC4A2ECL},{0x322DB86FL},{0xBC0AFC99L},{1L},{1L},{1L},{1L}},{{1L},{1L},{0xBC0AFC99L},{0x322DB86FL},{0x2AC4A2ECL},{0x322DB86FL},{0xBC0AFC99L},{1L},{1L},{1L}},{{1L},{1L},{1L},{0xBC0AFC99L},{0x322DB86FL},{0x2AC4A2ECL},{0x322DB86FL},{0xBC0AFC99L},{1L},{1L}},{{1L},{1L},{1L},{1L},{0xBC0AFC99L},{0x322DB86FL},{0x2AC4A2ECL},{0x322DB86FL},{0xBC0AFC99L},{1L}},{{1L},{1L},{1L},{1L},{1L},{0xBC0AFC99L},{0x322DB86FL},{0x2AC4A2ECL},{0x322DB86FL},{0xBC0AFC99L}}};
                    int i, j, k;
                }
                l_1946 = (void*)0;
                for (g_47 = 0; (g_47 <= 0); g_47 += 1)
                { /* block id: 891 */
                    int32_t *l_1947[6][7] = {{&g_1507,&l_1937,(void*)0,&l_1939[2][1][0],&l_1850,&g_4,&l_1850},{(void*)0,&l_1909[2][0],&l_1909[2][0],(void*)0,&g_203,&l_1938,&g_4},{(void*)0,&g_4,(void*)0,&l_1937,&l_1878[2],&l_1850,&l_1909[2][0]},{&g_1507,&g_203,&g_4,&l_1909[1][0],(void*)0,&l_1909[1][0],&g_4},{(void*)0,(void*)0,&l_1937,(void*)0,&l_1850,&l_1909[1][0],&l_1850},{&l_1939[2][1][0],&l_1937,&l_1850,&l_1909[2][0],&g_1507,&l_1850,&g_4}};
                    int8_t l_1979[8] = {0xCFL,0xCFL,0xCFL,0xCFL,0xCFL,0xCFL,0xCFL,0xCFL};
                    int i, j;
                    l_1949[1][3]--;
                    if (p_42)
                        continue;
                    if ((p_39 > p_39))
                    { /* block id: 894 */
                        (**l_1496) = func_66(p_41);
                    }
                    else
                    { /* block id: 896 */
                        uint64_t l_1956 = 0x8489588B41092200LL;
                        int16_t ** const l_1967 = (void*)0;
                        int16_t ** const *l_1966[6][5] = {{&l_1967,&l_1967,&l_1967,&l_1967,&l_1967},{&l_1967,&l_1967,&l_1967,&l_1967,&l_1967},{&l_1967,&l_1967,&l_1967,&l_1967,&l_1967},{&l_1967,&l_1967,&l_1967,&l_1967,&l_1967},{&l_1967,&l_1967,&l_1967,&l_1967,&l_1967},{&l_1967,&l_1967,&l_1967,&l_1967,&l_1967}};
                        int16_t ** const **l_1965 = &l_1966[4][3];
                        uint64_t *l_1968 = &l_1942[0];
                        int64_t *l_1971 = &g_155;
                        int32_t l_1972 = (-4L);
                        int i, j;
                        l_1956 = (((safe_sub_func_uint16_t_u_u(p_40, p_41)) < p_41) || (safe_sub_func_int8_t_s_s((p_42 | (g_478[3][2][1] , p_41)), p_39)));
                        l_1972 = ((safe_rshift_func_uint32_t_u_u(((+((*l_1971) = ((((safe_mod_func_int16_t_s_s(((((((((~((safe_sub_func_int16_t_s_s(0xF621L, p_40)) > (((p_40 , ((((*l_1965) = ((g_83[2] = l_1956) , &g_1846)) == &l_1467) , l_1956)) && (&g_1446 != (((*l_1968) = (((**g_988) && 0xA90D4202L) < g_23)) , l_1969[0][0]))) > p_41))) <= l_1956) & p_38) & p_41) , 6L) != (*l_1871)) & p_39) , 0x3C73L), p_41)) , p_40) | 0x8DBCL) <= 0xA7126B0BL))) || (**g_235)), (*l_1871))) != 0x88A14F6DL);
                    }
                    for (g_100 = 0; (g_100 <= 0); g_100 += 1)
                    { /* block id: 906 */
                        l_1937 &= ((0x59C6E70FEDD0F63FLL <= (((safe_mul_func_uint8_t_u_u((((*g_989) > l_1948[1]) < p_39), ((**g_609) = (safe_add_func_int16_t_s_s(0L, (l_1909[2][0] = (((((safe_mul_func_int16_t_s_s(p_40, p_40)) | p_41) == g_1404) == p_41) , (*l_1871)))))))) , l_1909[6][0]) | p_38)) ^ l_1979[6]);
                        return p_42;
                    }
                }
            }
            for (l_1312 = 0; (l_1312 <= 0); l_1312 += 1)
            { /* block id: 916 */
                int16_t ***l_1998 = &l_1467;
                int32_t l_2008[1][2][3] = {{{0xBFA87C1AL,0xBFA87C1AL,0xBFA87C1AL},{0xF1895336L,0xF1895336L,0xF1895336L}}};
                int32_t l_2009[7][10][3] = {{{(-2L),0xA8CF477AL,0xA8CF477AL},{0xD229ADA5L,0x95596419L,0x95596419L},{(-2L),0xA8CF477AL,0xA8CF477AL},{0xD229ADA5L,0x95596419L,0x95596419L},{(-2L),0xA8CF477AL,0xA8CF477AL},{0xD229ADA5L,0x95596419L,0x95596419L},{(-2L),0xA8CF477AL,0xA8CF477AL},{0xD229ADA5L,0x95596419L,0x95596419L},{(-2L),0xA8CF477AL,0xA8CF477AL},{0xD229ADA5L,0x95596419L,0x95596419L}},{{(-2L),0xA8CF477AL,0xA8CF477AL},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL}},{{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL}},{{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL}},{{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL}},{{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL}},{{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL},{0xA8CF477AL,1L,1L},{0x95596419L,0x8BBED39AL,0x8BBED39AL}}};
                int64_t l_2030 = 0x7D6E22D30DAB77AELL;
                int i, j, k;
                for (g_1618 = 0; g_1618 < 2; g_1618 += 1)
                {
                    l_1948[g_1618] = (-5L);
                }
                for (l_1936 = 0; (l_1936 <= 0); l_1936 += 1)
                { /* block id: 920 */
                    uint32_t l_1990 = 18446744073709551615UL;
                    int16_t ****l_1996 = &l_1995;
                    int32_t * const l_2003 = (void*)0;
                    int32_t * const *l_2002 = &l_2003;
                    int32_t * const **l_2001 = &l_2002;
                    int32_t *l_2007[2][4][8] = {{{&l_1938,&l_1939[2][5][0],&g_478[3][3][0],&g_17,&g_1035[0][0],&l_1833,&g_478[3][3][0],(void*)0},{&l_1833,&l_1939[2][5][0],&g_203,&g_1507,(void*)0,&g_17,&g_1507,&g_478[3][3][0]},{&g_1507,&l_1909[2][0],&g_1035[2][0],&l_1939[0][2][0],&g_17,&l_1939[0][2][0],&g_1035[2][0],&l_1909[2][0]},{&g_3,&l_1160[2],(void*)0,&g_17,(void*)0,&g_1507,&g_203,&l_1939[2][5][0]}},{{&l_1939[0][2][0],(void*)0,&l_1160[2],&l_1938,&g_3,&l_1833,&g_203,&g_478[3][3][0]},{(void*)0,&l_1938,(void*)0,&g_1507,(void*)0,&l_1939[4][0][0],&g_1035[2][0],&g_1035[2][0]},{(void*)0,&l_1939[4][0][0],&g_1035[2][0],&g_1035[2][0],&l_1939[4][0][0],(void*)0,&g_1507,(void*)0},{&l_1909[2][0],&g_478[3][3][0],&g_478[3][3][0],&g_203,&l_1833,&g_3,&l_1938,&l_1160[2]}}};
                    int64_t *l_2027 = &l_1233[0][0];
                    int64_t *l_2028 = (void*)0;
                    int64_t *l_2029[2][10][4] = {{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0}}};
                    int i, j, k;
                    (*g_626) = func_66((l_1948[1] != ((safe_mul_func_int16_t_s_s(p_42, ((***l_1869) , 2UL))) < ((safe_mod_func_int16_t_s_s((safe_rshift_func_int64_t_s_s((safe_lshift_func_uint8_t_u_s((safe_add_func_uint64_t_u_u((p_40 , 1UL), p_40)), (p_38 , l_1990))), l_1990)), p_38)) , 65532UL))));
                    l_2006 = ((safe_mod_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u(p_38, ((((*l_1996) = l_1995) == (g_1999[4][1] = (l_1998 = l_1997[0]))) | g_2000))) && (((*l_2001) = (void*)0) != (void*)0)), (safe_lshift_func_uint32_t_u_u((p_39 || 1L), 9)))) , (-1L));
                    --l_2010;
                    if ((p_39 , (safe_rshift_func_int16_t_s_s(((safe_mod_func_int64_t_s_s((safe_div_func_int64_t_s_s((l_2030 &= ((l_2008[0][0][0] | ((*l_2027) &= (((((p_39 ^ p_39) , ((void*)0 != l_2019)) && (((safe_div_func_uint8_t_u_u((**g_609), (((safe_sub_func_uint32_t_u_u(l_2026, ((p_40 = 0xC8L) && (-1L)))) > p_42) , (-7L)))) | p_41) ^ 0UL)) == p_38) > (**g_235)))) , (*g_1920))), g_1648)), 4UL)) , l_2031), l_1948[1]))))
                    { /* block id: 931 */
                        (**l_1496) = func_66(l_2008[0][0][1]);
                        (**l_1496) = func_66(((*l_1871) || (safe_lshift_func_int64_t_s_u(1L, 12))));
                    }
                    else
                    { /* block id: 934 */
                        const int32_t **l_2034 = &l_1871;
                        (*l_2034) = (**l_1869);
                        return p_42;
                    }
                }
                for (g_261 = 0; (g_261 <= 0); g_261 += 1)
                { /* block id: 941 */
                    (***l_1495) = &l_2008[0][0][0];
                    (*g_626) = (void*)0;
                }
            }
            for (g_490 = 0; (g_490 >= 0); g_490 -= 1)
            { /* block id: 948 */
                uint64_t l_2035[9][5][2] = {{{18446744073709551608UL,4UL},{5UL,0x33CD58CA855E8B84LL},{18446744073709551615UL,18446744073709551615UL},{18446744073709551614UL,18446744073709551615UL},{18446744073709551615UL,0x33CD58CA855E8B84LL}},{{5UL,4UL},{18446744073709551608UL,4UL},{0x7B175338ECDBE9CFLL,18446744073709551606UL},{18446744073709551615UL,0x59E4A63D703AA403LL},{0x13307829D8836718LL,5UL}},{{18446744073709551615UL,0xB244332472B83D68LL},{0x9CB7FE9E02C86ABBLL,0xD715E16EF5781B80LL},{0x97FC30BAF3D57F60LL,0x97FC30BAF3D57F60LL},{0UL,0x629229CB4ACE9034LL},{0xD715E16EF5781B80LL,0x13307829D8836718LL}},{{4UL,1UL},{0x9F586B071A8EDBBCLL,4UL},{0x33CD58CA855E8B84LL,18446744073709551615UL},{0x33CD58CA855E8B84LL,4UL},{0x9F586B071A8EDBBCLL,1UL}},{{4UL,0x13307829D8836718LL},{0xD715E16EF5781B80LL,0x629229CB4ACE9034LL},{0UL,0x97FC30BAF3D57F60LL},{0x97FC30BAF3D57F60LL,0xD715E16EF5781B80LL},{0x9CB7FE9E02C86ABBLL,0xB244332472B83D68LL}},{{18446744073709551615UL,5UL},{0x13307829D8836718LL,0x59E4A63D703AA403LL},{18446744073709551615UL,18446744073709551606UL},{0x7B175338ECDBE9CFLL,4UL},{18446744073709551608UL,4UL}},{{18446744073709551614UL,0xB244332472B83D68LL},{4UL,0x7B175338ECDBE9CFLL},{18446744073709551615UL,0x7B175338ECDBE9CFLL},{4UL,0xB244332472B83D68LL},{18446744073709551614UL,0x33CD58CA855E8B84LL}},{{0xD715E16EF5781B80LL,4UL},{0xB665CE183440ECD7LL,0x13307829D8836718LL},{0x59E4A63D703AA403LL,0x9F837DBC734BCC6FLL},{4UL,18446744073709551614UL},{0x7B175338ECDBE9CFLL,0x629229CB4ACE9034LL}},{{1UL,18446744073709551615UL},{18446744073709551610UL,18446744073709551610UL},{18446744073709551608UL,0xC67C7F50F406116FLL},{18446744073709551615UL,4UL},{0x9F586B071A8EDBBCLL,18446744073709551615UL}}};
                int32_t l_2080 = 0L;
                int32_t l_2081 = 1L;
                int32_t l_2082 = (-1L);
                int32_t l_2083 = 6L;
                int32_t l_2084 = 0x49C14702L;
                int32_t l_2085 = 1L;
                int32_t l_2088[8][8] = {{1L,0x8E1DCDD3L,0x04CF7E37L,0L,0xAA001735L,1L,0x231DA135L,1L},{0x8E1DCDD3L,(-1L),0xA6E84ECDL,(-1L),0x8E1DCDD3L,0x03BFB678L,0xA6CC3C16L,1L},{0xA6E84ECDL,0x7C943832L,1L,0xAA001735L,(-6L),0L,0x2653E4DCL,(-1L)},{0x2653E4DCL,0xA6CC3C16L,1L,(-8L),(-8L),1L,0xA6CC3C16L,0x2653E4DCL},{(-6L),1L,0xA6E84ECDL,1L,0x04CF7E37L,0x7C943832L,0x231DA135L,1L},{0x4DD67ECCL,0xAA001735L,0x04CF7E37L,0x03BFB678L,1L,0x7C943832L,1L,0x03BFB678L},{0xA6CC3C16L,1L,0xA6CC3C16L,0xA6E84ECDL,0x7C943832L,1L,0xAA001735L,(-6L)},{0x03BFB678L,0xA6CC3C16L,1L,0x231DA135L,0x4DD67ECCL,0L,0x7C943832L,0x7C943832L}};
                int i, j, k;
                for (g_54 = 0; (g_54 <= 0); g_54 += 1)
                { /* block id: 951 */
                    l_2035[7][1][1] = l_1909[2][0];
                }
                for (p_40 = 0; (p_40 <= 0); p_40 += 1)
                { /* block id: 956 */
                    uint64_t l_2045 = 0x9A4DF44C9FB5DF13LL;
                    int32_t l_2046 = 1L;
                    int8_t * const l_2057 = (void*)0;
                    int8_t * const *l_2056 = &l_2057;
                    int8_t * const **l_2055[2];
                    int8_t * const ***l_2054 = &l_2055[0];
                    int8_t * const ****l_2053[10] = {&l_2054,(void*)0,(void*)0,&l_2054,&l_2054,&l_2054,(void*)0,(void*)0,&l_2054,&l_2054};
                    uint32_t ****l_2073 = &l_2071;
                    int32_t l_2086 = (-5L);
                    int32_t l_2087 = 5L;
                    int32_t l_2089[6] = {0xC5109AB7L,0xC5109AB7L,0xC5109AB7L,0xC5109AB7L,0xC5109AB7L,0xC5109AB7L};
                    int i;
                    for (i = 0; i < 2; i++)
                        l_2055[i] = &l_2056;
                    l_2046 = ((safe_add_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(((((((g_128 |= l_2035[7][1][1]) , l_2040) != (l_2041 = (*l_1995))) , (((void*)0 == l_2042) != 0x948DL)) == p_38) , 0xC815393FL), (safe_mul_func_int32_t_s_s((p_39 <= l_2045), (*g_1880))))), p_41)) , (-1L));
                    if ((safe_sub_func_uint64_t_u_u(((safe_rshift_func_int32_t_s_u((l_2045 , (((safe_add_func_uint32_t_u_u((((g_2058 = (void*)0) != (void*)0) , (!(safe_lshift_func_uint32_t_u_u(((safe_mul_func_uint32_t_u_u(((l_2067 = (void*)0) != (void*)0), (0x202F53F5L != (~(**g_609))))) , (1L && (safe_lshift_func_uint32_t_u_s((((((((*l_2073) = l_2071) != &g_991) | 0x00L) != p_40) , (*g_1591)) == (void*)0), 26)))), 0)))), 0x32AE2C0EL)) || g_17) == 0x8B8002B189BB77EDLL)), 11)) && (**l_1870)), 0x9AC8D137A0CDAD1DLL)))
                    { /* block id: 963 */
                        int32_t *l_2074 = &g_203;
                        int32_t *l_2075[9][7][3] = {{{&l_1939[0][2][0],&l_74,(void*)0},{&g_203,&g_3,(void*)0},{&g_23,&g_23,(void*)0},{(void*)0,&l_74,(void*)0},{&g_4,&l_1939[0][2][0],(void*)0},{&g_149,&l_1938,(void*)0},{&l_1939[0][2][0],&g_4,(void*)0}},{{(void*)0,&g_3,(void*)0},{&g_23,&g_149,(void*)0},{&l_1939[0][2][0],&l_74,(void*)0},{&g_4,&l_1939[0][2][0],(void*)0},{&g_203,&l_1938,(void*)0},{&l_1939[0][2][0],&l_74,(void*)0},{&g_203,&g_3,(void*)0}},{{&g_23,&g_23,(void*)0},{(void*)0,&l_74,(void*)0},{&g_4,&l_1939[0][2][0],(void*)0},{&g_149,&l_1938,(void*)0},{&l_1939[0][2][0],&g_4,(void*)0},{(void*)0,&g_3,(void*)0},{&g_23,&g_149,(void*)0}},{{&l_1939[0][2][0],&l_74,(void*)0},{&g_4,&l_1939[0][2][0],(void*)0},{&g_203,&l_1938,(void*)0},{&l_1939[0][2][0],&l_74,(void*)0},{&g_203,&g_3,(void*)0},{&g_23,&g_23,(void*)0},{(void*)0,(void*)0,&g_203}},{{&g_3,&l_1160[0],&g_149},{(void*)0,&g_17,&g_149},{&l_1937,&g_3,&g_149},{&g_3,(void*)0,&g_203},{&l_1160[2],&l_1909[2][0],&g_149},{&g_478[3][3][1],(void*)0,&g_149},{&g_3,&l_1937,&g_149}},{{(void*)0,&g_17,&g_203},{&l_1937,(void*)0,&g_149},{&g_151,(void*)0,&g_149},{&l_1160[2],&l_1160[2],&g_149},{(void*)0,(void*)0,&g_203},{&g_3,&l_1160[0],&g_149},{(void*)0,&g_17,&g_149}},{{&l_1937,&g_3,&g_149},{&g_3,(void*)0,&g_203},{&l_1160[2],&l_1909[2][0],&g_149},{&g_478[3][3][1],(void*)0,&g_149},{&g_3,&l_1937,&g_149},{(void*)0,&g_17,&g_203},{&l_1937,(void*)0,&g_149}},{{&g_151,(void*)0,&g_149},{&l_1160[2],&l_1160[2],&g_149},{(void*)0,(void*)0,&g_203},{&g_3,&l_1160[0],&g_149},{(void*)0,&g_17,&g_149},{&l_1937,&g_3,&g_149},{&g_3,(void*)0,&g_203}},{{&l_1160[2],&l_1909[2][0],&g_149},{&g_478[3][3][1],(void*)0,&g_149},{&g_3,&l_1937,&g_149},{(void*)0,&g_17,&g_203},{&l_1937,(void*)0,&g_149},{&g_151,(void*)0,&g_149},{&l_1160[2],&l_1160[2],&g_149}}};
                        int32_t l_2076 = 0x7C8EFCFDL;
                        int i, j, k;
                        l_2077++;
                        if ((*g_1352))
                            break;
                        ++l_2091;
                    }
                    else
                    { /* block id: 967 */
                        int32_t *l_2094 = &l_2088[1][0];
                        int32_t *l_2095 = (void*)0;
                        int32_t *l_2096[5] = {&g_4,&g_4,&g_4,&g_4,&g_4};
                        int i;
                        l_2097--;
                    }
                    if (p_38)
                        break;
                }
            }
            for (g_205 = 0; (g_205 <= 0); g_205 += 1)
            { /* block id: 975 */
                uint32_t ***l_2102 = &g_2100[1][1][0];
                int8_t l_2104[3][8] = {{1L,0L,1L,1L,0L,1L,1L,0L},{0L,1L,1L,0L,1L,1L,0L,1L},{0L,0L,0x87L,0L,0L,0x87L,0L,0L}};
                int32_t l_2112 = 0x59D4288AL;
                int32_t l_2114 = 0L;
                int32_t l_2116 = 5L;
                uint64_t l_2146 = 18446744073709551615UL;
                int32_t l_2170 = 2L;
                int32_t l_2172 = 0x1DA42614L;
                int32_t l_2173 = 5L;
                int32_t l_2174 = 0x60565D52L;
                int32_t l_2177 = 0xC09F92FEL;
                int32_t *l_2182[5];
                int i, j;
                for (i = 0; i < 5; i++)
                    l_2182[i] = &l_2116;
                if ((((*l_2102) = g_2100[0][5][1]) != g_2103[2]))
                { /* block id: 977 */
                    int16_t l_2113 = 0x6485L;
                    int32_t l_2115 = 0xB84825BBL;
                    int32_t l_2117 = (-2L);
                    if (l_1948[1])
                    { /* block id: 978 */
                        int32_t *l_2105 = (void*)0;
                        int32_t *l_2106 = &l_1937;
                        int32_t *l_2107 = &g_1035[1][0];
                        int32_t *l_2108 = &l_1909[2][0];
                        int32_t *l_2109 = &l_2090;
                        int32_t *l_2110 = &l_1833;
                        int32_t *l_2111[10] = {(void*)0,&l_2090,&l_1160[0],&l_2090,(void*)0,(void*)0,&l_2090,&l_1160[0],&l_2090,(void*)0};
                        uint16_t l_2118 = 0x9C26L;
                        int i;
                        (*g_1352) = l_2104[0][4];
                        l_2118--;
                        (*g_2121) = ((**l_1496) = func_66(p_39));
                    }
                    else
                    { /* block id: 983 */
                        uint32_t l_2123[3][2] = {{4294967286UL,4294967286UL},{4294967286UL,4294967286UL},{4294967286UL,4294967286UL}};
                        int i, j;
                        (***l_1495) = func_66(l_1847);
                        l_2090 &= (l_2123[0][1] = ((**g_988) <= l_2104[0][4]));
                        if (p_39)
                            continue;
                        l_2115 |= (((safe_mul_func_int16_t_s_s((((g_2126 | (safe_div_func_uint8_t_u_u((g_4 ^ (1L == (((*g_626) == &l_1909[1][0]) && (((void*)0 != (*g_1919)) , (((safe_add_func_uint8_t_u_u((((**g_754) != (((safe_unary_minus_func_int8_t_s(((safe_rshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u((safe_div_func_int64_t_s_s(((*g_989) == 0x00FE01D4L), p_41)), 2)), 5)) != p_39))) >= p_40) , (**g_609))) , (**g_609)), g_2138)) < l_2097) | 0xAC177F83FB139E6CLL))))), p_38))) , (*l_1871)) , p_42), 5L)) , l_2123[1][1]) , l_2113);
                    }
                    l_1871 = &l_1909[4][0];
                    for (g_490 = 0; (g_490 <= 0); g_490 += 1)
                    { /* block id: 993 */
                        l_2140 = g_2139;
                    }
                }
                else
                { /* block id: 996 */
                    int16_t l_2151[8][2][4] = {{{(-1L),0L,(-5L),0xAAACL},{0x09B0L,0L,0x09B0L,0x103FL}},{{0L,(-1L),1L,0L},{0L,0xAAACL,(-1L),(-1L)}},{{(-1L),(-1L),(-1L),(-1L)},{0L,0x103FL,1L,0x09B0L}},{{0L,0xAD8FL,0x09B0L,7L},{0x09B0L,7L,(-5L),7L}},{{(-1L),0xAD8FL,0xAAACL,0x09B0L},{(-8L),0xAD8FL,0L,0xAAACL}},{{7L,(-1L),(-1L),(-1L)},{7L,7L,0L,0x103FL}},{{0x09B0L,(-1L),7L,0xAD8FL},{(-1L),0L,(-8L),7L}},{{(-1L),0L,(-1L),0xAD8FL},{0L,(-1L),(-5L),0x103FL}}};
                    int16_t ****l_2159 = &l_1997[0];
                    int32_t l_2168 = (-1L);
                    int32_t l_2175 = 0L;
                    int32_t l_2178 = (-10L);
                    int i, j, k;
                    if (((*g_610) || (((((safe_add_func_uint32_t_u_u(p_39, (l_2146 & (((safe_add_func_uint32_t_u_u(((safe_mod_func_uint16_t_u_u((((l_2151[5][0][0] == p_39) >= (safe_lshift_func_uint8_t_u_s(((safe_mul_func_int8_t_s_s((safe_rshift_func_int8_t_s_u((~((**g_609) <= p_39)), 2)), (((*l_2159) = (g_1999[4][1] = &g_1846)) == ((l_2151[5][0][0] == g_203) , &l_2040)))) , (**g_609)), p_38))) || p_39), p_42)) & p_40), (-1L))) , (*g_610)) , l_2151[5][0][0])))) && 0x2C381E44L) , p_39) , 0x0855BDCAC4ED92A3LL) >= 0xE15261812E462F45LL)))
                    { /* block id: 999 */
                        int32_t *l_2160 = &l_2116;
                        int32_t *l_2161 = &g_478[3][2][5];
                        int32_t *l_2162 = &l_1938;
                        int32_t *l_2163 = (void*)0;
                        int32_t *l_2164 = &l_1939[4][3][0];
                        int32_t *l_2165 = &l_74;
                        int32_t *l_2166 = &g_1507;
                        int32_t *l_2167[4] = {&l_2114,&l_2114,&l_2114,&l_2114};
                        int i;
                        l_2179--;
                    }
                    else
                    { /* block id: 1001 */
                        if (p_38)
                            break;
                    }
                }
                l_2183--;
                for (l_2146 = 0; (l_2146 <= 0); l_2146 += 1)
                { /* block id: 1008 */
                    int32_t l_2187 = 0x0D2627AEL;
                    l_2186 = (g_2100[0][5][1] = l_2186);
                    if ((***l_1869))
                        continue;
                    l_2187 ^= (*g_1352);
                    for (l_2010 = 0; (l_2010 <= 0); l_2010 += 1)
                    { /* block id: 1015 */
                        int8_t l_2190 = 0xFEL;
                        l_1938 = (safe_add_func_uint8_t_u_u(p_41, ((*g_610) = l_2179)));
                        if (l_2190)
                            continue;
                        (*g_2121) = ((***l_1495) = func_66(((*g_328) == (*g_1562))));
                        return p_39;
                    }
                }
            }
        }
        if (((void*)0 == (**l_1495)))
        { /* block id: 1026 */
            uint64_t l_2197 = 0x052FAE5B788B1782LL;
            int32_t l_2198[4] = {0xF6690478L,0xF6690478L,0xF6690478L,0xF6690478L};
            int32_t l_2216[4][6][5] = {{{0x7D859FE9L,0x363EB2E9L,0L,0x97A8EB52L,9L},{0x0C18285BL,0x526177A6L,0x4F789C9CL,0x8894447EL,0x4903599DL},{0x7D859FE9L,0xDDFE9F69L,0xDE5BA78BL,1L,0x8894447EL},{1L,0xA9B4688EL,1L,0x97A8EB52L,(-1L)},{0xDE5BA78BL,0x4903599DL,0L,0x45CC3380L,(-1L)},{0x363EB2E9L,0x0C18285BL,0x0C18285BL,0x363EB2E9L,0x8894447EL}},{{9L,0x363EB2E9L,(-8L),(-1L),0x4903599DL},{0xBDAE915DL,0x7D859FE9L,0x8894447EL,0L,9L},{0x363EB2E9L,0x45CC3380L,0xDE5BA78BL,(-1L),0x0C18285BL},{0x586A7B1FL,6L,0L,0x363EB2E9L,6L},{0x8894447EL,0x7D859FE9L,0xBDAE915DL,0x45CC3380L,0x526177A6L},{0x7D859FE9L,0x4F789C9CL,0xBDAE915DL,0x97A8EB52L,0xBDAE915DL}},{{0x526177A6L,0x526177A6L,0L,1L,0x4903599DL},{0L,0x4903599DL,0xDE5BA78BL,0x8894447EL,1L},{1L,0xAAE7FF06L,0x8894447EL,0x97A8EB52L,0x8598E27EL},{0x586A7B1FL,0x4903599DL,(-8L),6L,(-1L)},{0x4F789C9CL,0x526177A6L,0x0C18285BL,0x4F789C9CL,1L},{9L,0x4F789C9CL,0L,(-1L),0xDDFE9F69L}},{{9L,0x7D859FE9L,1L,0x7D859FE9L,9L},{0x4F789C9CL,6L,0xDE5BA78BL,0x8598E27EL,0x526177A6L},{0x586A7B1FL,0x45CC3380L,0x4F789C9CL,0x363EB2E9L,0x45CC3380L},{1L,0x7D859FE9L,0L,6L,0x526177A6L},{0L,0x363EB2E9L,0xBDAE915DL,9L,9L},{0x526177A6L,0x0C18285BL,0x4F789C9CL,1L,0xDDFE9F69L}}};
            uint32_t ***l_2278 = &l_2186;
            uint64_t *l_2299[8] = {&l_1312,&l_1312,&l_1312,&l_1312,&l_1312,&l_1312,&l_1312,&l_1312};
            int32_t ***l_2339 = &g_626;
            uint16_t l_2373 = 0xF938L;
            uint8_t l_2406 = 0x48L;
            uint8_t **l_2413 = &g_610;
            int i, j, k;
lbl_2418:
            l_2216[3][5][4] = (safe_add_func_int16_t_s_s((safe_mul_func_uint32_t_u_u(0xC0B34C67L, ((safe_mod_func_int64_t_s_s(((*g_2101) , ((l_2176[0][0] = (l_2197 | l_2197)) ^ ((l_2198[2] |= 0x3D08L) | (((0x2CL <= (safe_lshift_func_int32_t_s_s((((safe_lshift_func_int8_t_s_u((safe_div_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u((((safe_div_func_uint8_t_u_u(((((safe_lshift_func_uint32_t_u_s(((+(-3L)) < ((safe_mul_func_uint32_t_u_u(((safe_mod_func_uint32_t_u_u(p_38, (p_41 & 0xF968L))) , p_41), p_41)) || l_2197)), 18)) , 1L) ^ 1L) & p_38), p_42)) | (**g_754)) & p_39), 0x0FA0L)), (*g_236))), 6)) | p_38) < l_2097), 6))) , 0x10L) < l_1948[1])))), l_2171)) > p_38))), g_23));
            if ((safe_mod_func_int16_t_s_s(l_2097, ((((safe_sub_func_uint32_t_u_u(((safe_lshift_func_int64_t_s_s(((l_2198[2] | l_2171) <= p_39), p_40)) , ((**l_2072) &= (safe_mod_func_int16_t_s_s(((*l_1467) != (*l_1467)), (safe_unary_minus_func_uint32_t_u((safe_div_func_int16_t_s_s(((((*l_1368) = (safe_mod_func_int8_t_s_s(p_38, (*g_610)))) >= p_41) > l_1909[2][0]), 65530UL)))))))), 2L)) || 0x40955037L) || p_41) , 65528UL))))
            { /* block id: 1032 */
                int32_t l_2255 = 0xB1C341DFL;
                uint8_t *l_2258 = &g_91;
                int32_t l_2259 = (-1L);
                int32_t l_2260[4][6][6] = {{{(-1L),1L,0x9B9B2BEDL,0x9B9B2BEDL,1L,(-1L)},{0L,0x9B9B2BEDL,1L,(-1L),(-1L),0xA599E86DL},{1L,1L,0x92822C4BL,(-6L),(-1L),(-1L)},{1L,0xAF79EF41L,(-6L),(-1L),0L,(-1L)},{0L,(-1L),0xAF79EF41L,0x9B9B2BEDL,1L,(-1L)},{(-1L),0x9B9B2BEDL,0xA599E86DL,1L,1L,0xA599E86DL}},{{(-1L),(-1L),(-1L),(-6L),0L,(-1L)},{1L,0xAF79EF41L,6L,1L,(-1L),(-1L)},{(-1L),1L,6L,0x9B9B2BEDL,(-1L),(-1L)},{0L,0x9B9B2BEDL,(-1L),(-1L),1L,0xA599E86DL},{(-1L),1L,0xA599E86DL,(-6L),(-1L),(-1L)},{(-1L),0xAF79EF41L,0xAF79EF41L,(-1L),0L,(-1L)}},{{0L,(-1L),(-6L),0x9B9B2BEDL,(-1L),(-1L)},{(-1L),0x9B9B2BEDL,0x92822C4BL,1L,(-1L),0xA599E86DL},{1L,(-1L),1L,(-6L),0L,(-1L)},{(-1L),0xAF79EF41L,0x9B9B2BEDL,1L,(-1L),(-1L)},{(-1L),1L,0x9B9B2BEDL,0x9B9B2BEDL,1L,(-1L)},{0L,0x9B9B2BEDL,1L,(-1L),(-1L),0xA599E86DL}},{{1L,1L,0x92822C4BL,(-6L),(-1L),(-1L)},{1L,0xAF79EF41L,(-6L),(-1L),0L,(-1L)},{0L,(-1L),0xAF79EF41L,0x9B9B2BEDL,1L,(-1L)},{(-1L),0x9B9B2BEDL,0xA599E86DL,1L,1L,0xA599E86DL},{(-1L),(-1L),(-1L),(-6L),0L,(-1L)},{1L,0xAF79EF41L,6L,1L,(-1L),(-1L)}}};
                int i, j, k;
                l_2260[3][4][2] |= (((safe_lshift_func_int32_t_s_s(0x59B0ED3BL, 27)) , (safe_mod_func_int16_t_s_s((safe_rshift_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u(p_42, (safe_add_func_int32_t_s_s(((l_2198[2] = ((safe_mul_func_uint32_t_u_u(((safe_sub_func_uint8_t_u_u((**g_609), (safe_mul_func_int16_t_s_s((safe_mod_func_uint64_t_u_u((((-9L) & (&g_989 != ((safe_lshift_func_uint32_t_u_s((safe_rshift_func_uint16_t_u_s((+(safe_rshift_func_uint8_t_u_u(0x97L, l_2255))), 0)), 9)) , (*l_1924)))) && ((safe_rshift_func_int16_t_s_u((l_2258 != &p_40), 0)) , l_2216[3][5][4])), 0x7704571A55278F5ALL)), (-1L))))) > 0xC998C99887186031LL), 6UL)) , 0x830267EFL)) , l_2216[0][5][1]), l_2259)))), l_2259)), 65535UL))) ^ 1L);
                l_2260[1][1][5] |= (g_2261 ^= (*g_1352));
                (*g_1352) = l_1948[1];
            }
            else
            { /* block id: 1038 */
                uint8_t l_2274 = 1UL;
                int8_t l_2288 = 0xCBL;
                uint32_t ****l_2291[6][5] = {{&l_2071,(void*)0,&l_2071,&l_2071,(void*)0},{(void*)0,&l_2071,&l_2071,(void*)0,&l_2071},{(void*)0,(void*)0,&l_1924,(void*)0,(void*)0},{&l_2071,(void*)0,&l_2071,&l_2071,(void*)0},{(void*)0,&l_2071,&l_2071,(void*)0,&l_2071},{(void*)0,(void*)0,&l_1924,(void*)0,(void*)0}};
                uint32_t ****l_2295 = &l_1924;
                int64_t l_2320 = 0L;
                int32_t l_2321 = 6L;
                int32_t l_2328 = 0x496F14D2L;
                int32_t l_2330 = 0L;
                int32_t l_2332 = 0x9AEDA1E1L;
                int32_t l_2333 = 0xAB66969EL;
                int32_t l_2334 = 0xFD77138BL;
                int32_t l_2335[2];
                int i, j;
                for (i = 0; i < 2; i++)
                    l_2335[i] = 6L;
                for (g_51 = 28; (g_51 < 60); ++g_51)
                { /* block id: 1041 */
                    int8_t l_2270 = (-1L);
                    int8_t l_2329 = 0x39L;
                    int32_t l_2331[5][3][6] = {{{0x20A6FF60L,(-5L),0xE356EC7EL,(-1L),0x5290CE96L,(-1L)},{0x837182ABL,(-1L),0L,0xCF257925L,0xED890FC5L,(-1L)},{0xCF257925L,0xED890FC5L,(-1L),1L,1L,0L}},{{0x49BC00EAL,0xD5D08818L,0x34688073L,0L,3L,0x0F2FDAD5L},{3L,0x8F9ED02EL,(-1L),0x0C8D4675L,0xD5D08818L,0x0C8D4675L},{1L,0x0F2FDAD5L,1L,0xB2E4145CL,0x9D585ED0L,0xED890FC5L}},{{0x0C8D4675L,0x47D1AFD0L,0x49BC00EAL,0x5290CE96L,0xA34C865BL,0xCF257925L},{0xBB9CF7D1L,1L,(-7L),0L,0xE356EC7EL,(-7L)},{(-4L),(-1L),0x837182ABL,(-7L),0x6FBD10CAL,1L}},{{(-1L),0xED890FC5L,0x8F9ED02EL,(-4L),0x5290CE96L,(-1L)},{(-1L),1L,1L,0xB2E4145CL,(-7L),0x0C8D4675L},{0x20A6FF60L,1L,1L,0x6FBD10CAL,0xD81479B6L,0xD81479B6L}},{{1L,(-4L),(-4L),1L,(-1L),0x20A6FF60L},{1L,1L,0xE356EC7EL,0x89EC0021L,0x47D1AFD0L,0x34392F20L},{1L,(-7L),1L,0xA34C865BL,0x47D1AFD0L,0L}}};
                    uint32_t *l_2345[5][3] = {{(void*)0,&g_1931,(void*)0},{&l_2179,(void*)0,&l_2179},{(void*)0,&g_1931,(void*)0},{&l_2179,(void*)0,&l_2179},{(void*)0,&g_1931,(void*)0}};
                    int i, j, k;
                    if ((safe_rshift_func_uint64_t_u_u(g_478[3][3][0], (safe_sub_func_uint8_t_u_u(0UL, (&l_2186 == ((((*g_989) & (p_40 || (l_2270 == (safe_lshift_func_int32_t_s_u(1L, 8))))) | ((((+(5L || (((l_2274--) , &l_1496) != l_2277))) | 255UL) > p_39) , p_39)) , l_2278)))))))
                    { /* block id: 1043 */
                        uint32_t *l_2287[10][10] = {{&g_728,&g_129,&g_51,&g_728,&g_728,(void*)0,&g_129,&g_129,&g_152,&g_51},{&g_152,&g_152,&g_129,&g_152,(void*)0,&g_728,&g_51,&g_728,&g_129,(void*)0},{&g_152,&g_152,&g_129,&g_129,&g_129,&g_129,&g_129,&g_152,&g_152,&g_152},{&g_152,&g_129,(void*)0,&g_51,&g_728,&g_728,&g_51,(void*)0,&g_728,&g_129},{&g_129,(void*)0,&g_728,&g_51,&g_129,&g_728,&g_129,&g_51,&g_152,&g_51},{&g_129,(void*)0,(void*)0,&g_129,(void*)0,&g_152,&g_728,&g_129,&g_129,&g_728},{&g_129,&g_129,&g_152,&g_152,&g_129,&g_129,&g_152,&g_728,&g_152,&g_728},{&g_728,&g_152,&g_51,&g_728,&g_728,&g_152,&g_152,&g_51,&g_51,(void*)0},{&g_728,&g_51,(void*)0,&g_152,&g_51,&g_129,(void*)0,&g_152,&g_728,&g_129},{&g_129,(void*)0,&g_152,&g_728,&g_129,&g_152,(void*)0,&g_152,&g_129,&g_728}};
                        int32_t l_2289 = 0xC7AF745BL;
                        uint32_t *****l_2292 = (void*)0;
                        uint32_t *****l_2293 = (void*)0;
                        uint32_t *****l_2294 = &l_2291[0][1];
                        int32_t l_2296[10] = {0xF3956906L,0xF3956906L,0x30137BA2L,0xF3956906L,0xF3956906L,0x30137BA2L,0xF3956906L,0xF3956906L,0x30137BA2L,0xF3956906L};
                        int i, j;
                        l_2176[0][0] = 1L;
                        (*g_60) = ((safe_mod_func_uint8_t_u_u(p_40, (safe_lshift_func_int8_t_s_u(((safe_sub_func_int32_t_s_s(((l_2288 ^= (p_39 , (++(***l_1924)))) <= (p_40 && l_2216[3][5][4])), ((((((l_2289 &= ((void*)0 == &g_1446)) > ((p_40 ^ ((safe_unary_minus_func_int8_t_s((((*l_2294) = l_2291[0][1]) != (l_2198[3] , l_2295)))) , 1UL)) && l_2296[8])) || p_39) != l_2274) | l_2216[3][5][4]) == p_38))) == p_41), l_2296[8])))) >= l_2296[8]);
                    }
                    else
                    { /* block id: 1050 */
                        return p_39;
                    }
                    for (g_356 = (-26); (g_356 >= 21); g_356 = safe_add_func_uint32_t_u_u(g_356, 1))
                    { /* block id: 1055 */
                        int16_t l_2317 = 7L;
                        int32_t l_2319[3][9][7] = {{{1L,0L,0x1BB19579L,0x194A6413L,8L,8L,0x194A6413L},{0x06B970F5L,8L,0x06B970F5L,(-1L),(-4L),1L,0L},{0x06B970F5L,(-1L),0x194A6413L,0L,0x982518C3L,8L,(-1L)},{(-1L),0x982518C3L,0x06B970F5L,0x06B970F5L,0x982518C3L,(-1L),8L},{0x982518C3L,1L,(-8L),0x06B970F5L,(-10L),(-1L),8L},{0xC6576B69L,(-1L),0x1BB19579L,1L,0x194A6413L,1L,0x1BB19579L},{1L,1L,0L,0x3A912ACCL,0x06B970F5L,(-4L),0x1BB19579L},{8L,0x982518C3L,0x3A912ACCL,1L,0x1BB19579L,8L,8L},{0x06B970F5L,0x194A6413L,0xC6576B69L,0x194A6413L,0x06B970F5L,1L,8L}},{{(-1L),0L,0xC6576B69L,0x4FD6A55FL,0x194A6413L,(-10L),(-1L)},{0x4FD6A55FL,1L,0x3A912ACCL,(-10L),(-10L),0x3A912ACCL,1L},{(-1L),0x4FD6A55FL,0L,8L,0x982518C3L,0x3A912ACCL,1L},{0x06B970F5L,(-4L),0x1BB19579L,0xC6576B69L,(-8L),(-10L),(-8L)},{8L,(-8L),(-8L),8L,8L,1L,0xC6576B69L},{1L,(-8L),0x06B970F5L,(-10L),(-1L),8L,0x194A6413L},{0xC6576B69L,(-4L),1L,0x4FD6A55FL,1L,(-4L),0xC6576B69L},{0x982518C3L,0x4FD6A55FL,(-4L),0x194A6413L,1L,1L,(-8L)},{(-1L),1L,(-10L),1L,(-1L),(-1L),1L}},{{(-4L),0L,(-4L),0x3A912ACCL,8L,(-1L),1L},{(-4L),0x194A6413L,1L,1L,(-8L),0L,(-1L)},{(-1L),0x982518C3L,0x06B970F5L,0x06B970F5L,0x982518C3L,(-1L),8L},{0x982518C3L,1L,(-8L),0x06B970F5L,(-10L),(-1L),8L},{0xC6576B69L,(-1L),0x1BB19579L,1L,0x194A6413L,1L,0x1BB19579L},{1L,1L,0L,0x3A912ACCL,0x06B970F5L,(-4L),0x1BB19579L},{8L,0x982518C3L,0x3A912ACCL,1L,0x1BB19579L,8L,8L},{0x06B970F5L,0x194A6413L,0xC6576B69L,0x194A6413L,0x06B970F5L,1L,8L},{(-1L),0L,0xC6576B69L,0x4FD6A55FL,0x194A6413L,(-10L),(-1L)}}};
                        int32_t *l_2322 = &l_2216[3][5][4];
                        int32_t *l_2323 = &l_2171;
                        int32_t *l_2324 = &l_2216[3][5][4];
                        int32_t *l_2325 = &g_23;
                        int32_t *l_2326 = &l_2176[3][0];
                        int32_t *l_2327[2];
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                            l_2327[i] = (void*)0;
                        l_2321 = (((((l_2299[7] = &g_356) != (g_2300[0][6] = &g_902)) < (safe_mul_func_int64_t_s_s(((!(safe_mod_func_int16_t_s_s((l_2319[1][6][0] = ((safe_div_func_uint32_t_u_u(((**l_1925) = ((*g_610) , ((((safe_add_func_uint16_t_u_u(0x53CEL, 0xE55EL)) ^ ((l_2197 , ((((l_2317 = (((safe_mod_func_int32_t_s_s((((safe_mul_func_int32_t_s_s(((+l_2270) > (((l_1909[0][0] >= ((safe_mod_func_int8_t_s_s(0x37L, p_40)) & p_39)) == 8L) && p_38)), p_38)) == l_2288) , (-5L)), (*g_1504))) ^ p_41) && 8L)) , 0x8A6692B2456BD3ECLL) | 18446744073709551615UL) == 0x96F12A48189F01FALL)) ^ l_2270)) <= p_42) > g_2318[4][1][1]))), g_3)) == 0x4592727690803D6DLL)), p_41))) , l_2320), g_1507))) < l_2198[2]) == 254UL);
                        l_2336++;
                        (*l_2322) |= ((*l_2325) = ((void*)0 != l_2339));
                        l_2333 = g_2340;
                    }
                    (*g_2349) = (safe_rshift_func_int32_t_s_u(0x127C80B1L, (((safe_mod_func_uint64_t_u_u((l_2330 = ((**l_2278) != l_2345[4][2])), p_40)) , p_38) , (safe_mod_func_uint64_t_u_u(0x3FD4231368DE677DLL, (0xC01D5BC3FA0E8981LL || ((l_2348[4][0][2] == (void*)0) || 0UL)))))));
                }
                for (l_2333 = 0; (l_2333 != (-8)); --l_2333)
                { /* block id: 1072 */
                    int32_t l_2352 = 1L;
                    return l_2352;
                }
                for (g_1404 = 0; (g_1404 <= 0); g_1404 += 1)
                { /* block id: 1077 */
                    uint16_t l_2371 = 0x1AFCL;
                    int32_t l_2372 = (-5L);
                    int32_t l_2374[4][5][3] = {{{0x8ED21FDFL,0L,0x0CA82D28L},{0x60787B12L,0x58BFB5A6L,0x60787B12L},{(-7L),0x8ED21FDFL,0x0CA82D28L},{0x0EE8D33EL,0x0CD882E3L,0xB4E2C4F7L},{0xE11CF2C3L,0x8ED21FDFL,0x8ED21FDFL}},{{0xB4E2C4F7L,0x58BFB5A6L,(-2L)},{0xE11CF2C3L,0L,0xE11CF2C3L},{0x0EE8D33EL,5L,0x5C8199CCL},{0x8ED21FDFL,0x8ED21FDFL,0xE11CF2C3L},{(-2L),5L,0x60787B12L}},{{0xE11CF2C3L,(-7L),0L},{(-2L),0x0CD882E3L,(-2L)},{0x8ED21FDFL,0xE11CF2C3L,0L},{0xB4E2C4F7L,0L,0x60787B12L},{0x0CA82D28L,0xE11CF2C3L,0xE11CF2C3L}},{{0x60787B12L,0x0CD882E3L,0x5C8199CCL},{0x0CA82D28L,(-7L),0x0CA82D28L},{0xB4E2C4F7L,5L,0x5C8199CCL},{0x8ED21FDFL,0x8ED21FDFL,0xE11CF2C3L},{(-2L),5L,0x60787B12L}}};
                    int i, j, k;
                    if ((safe_mul_func_int8_t_s_s(((l_2374[1][4][2] = (l_2335[0] = (safe_mod_func_uint32_t_u_u((l_2373 ^= (safe_lshift_func_int32_t_s_u((*g_1880), (l_2328 = ((***l_1924) = (((p_39 == ((safe_mul_func_uint32_t_u_u((0x00L <= (safe_add_func_uint32_t_u_u((*g_1504), (safe_lshift_func_uint32_t_u_u((safe_mul_func_uint32_t_u_u(((safe_lshift_func_int32_t_s_s(p_40, 3)) , ((l_2372 = (safe_mod_func_int64_t_s_s((l_2176[2][0] |= l_2371), (l_2330 = l_2328)))) , (l_2372 |= (p_41 <= (l_2274 == g_83[0]))))), p_39)), l_2333))))), l_2371)) <= 0L)) , p_40) > p_38)))))), 0x3742197FL)))) | 0x0831L), l_2334)))
                    { /* block id: 1087 */
                        int8_t l_2382[8];
                        int32_t l_2398 = 0L;
                        int32_t *l_2400 = &g_23;
                        const int32_t l_2414 = 1L;
                        int i;
                        for (i = 0; i < 8; i++)
                            l_2382[i] = 0L;
                        if (l_2372)
                            break;
                        l_2374[2][0][0] = (safe_add_func_int32_t_s_s((safe_lshift_func_uint64_t_u_u(((p_42 , ((safe_lshift_func_int32_t_s_u((((((+p_41) == l_2382[2]) , ((*l_1368) = p_39)) <= (safe_sub_func_int32_t_s_s(((safe_rshift_func_int16_t_s_s((safe_sub_func_int8_t_s_s(l_2372, (((l_2398 |= (safe_sub_func_uint32_t_u_u(p_41, (safe_mul_func_int8_t_s_s((&g_2100[1][7][1] == ((safe_add_func_int16_t_s_s(((0x8C39L < (safe_lshift_func_uint8_t_u_u(((&l_1925 == &l_1925) <= p_41), 6))) == l_2374[0][4][0]), 7UL)) , &g_2100[2][4][0])), l_2397))))) == (**g_609)) , (*g_610)))), p_40)) , (*g_60)), (*g_1504)))) || l_2335[1]), 25)) > p_38)) > 0x0DL), 54)), l_2382[7]));
                        (*l_2400) = l_2382[6];
                        (*g_2349) ^= ((safe_rshift_func_int8_t_s_s(((!((((p_41 > (safe_div_func_int64_t_s_s(((((((**g_609) | ((*l_1368) = ((l_2406 || (l_2334 == (safe_lshift_func_uint32_t_u_s((safe_add_func_int8_t_s_s((l_2328 = (l_2374[1][0][0] == (safe_div_func_uint32_t_u_u((((0xF6DE51AEL < (((p_40 || l_2321) == (l_2413 != l_1183)) , l_2288)) < 1UL) < p_38), 0x5639792CL)))), (-4L))), 7)))) , p_41))) , &g_1562) != &l_1907) , p_40) < 0xB72E676B47F3D201LL), l_2414))) && p_41) > p_42) == 1L)) || 1L), 4)) >= 0xA04EL);
                    }
                    else
                    { /* block id: 1096 */
                        if (p_39)
                            break;
                    }
                    for (l_2197 = 0; (l_2197 <= 0); l_2197 += 1)
                    { /* block id: 1101 */
                        int32_t *l_2415 = &l_2330;
                        (*l_2415) = 1L;
                    }
                }
                for (l_2336 = 0; (l_2336 == 17); l_2336++)
                { /* block id: 1107 */
                    if (g_149)
                        goto lbl_2418;
                }
            }
        }
        else
        { /* block id: 1111 */
            for (g_54 = 0; (g_54 > 11); g_54 = safe_add_func_uint32_t_u_u(g_54, 3))
            { /* block id: 1114 */
                return p_42;
            }
        }
        (*g_60) = ((safe_lshift_func_uint16_t_u_u((safe_add_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(0x22FEL, (safe_div_func_uint32_t_u_u(((p_40 & ((safe_sub_func_uint8_t_u_u(((**g_609) = (safe_unary_minus_func_uint64_t_u((((((*l_1368) = (safe_mul_func_uint32_t_u_u((safe_mod_func_int8_t_s_s(l_2436, (safe_mod_func_uint32_t_u_u((safe_sub_func_int8_t_s_s((****g_1614), ((safe_rshift_func_int64_t_s_u(((*l_2449) = (p_39 = (safe_add_func_uint32_t_u_u((*g_1504), ((g_83[0] = (safe_sub_func_int64_t_s_s((safe_sub_func_uint16_t_u_u(((*l_2071) == &g_989), (&g_360 == ((((void*)0 == &g_1088) | g_261) , &g_360)))), 0UL))) || 0x9A02L))))), 52)) <= 9UL))), 0xCCCD2540L)))), p_42))) <= 0xF6L) < 0L) && 4294967295UL)))), l_2450[1])) && l_1948[1])) <= p_38), p_38)))), p_41)), g_23)) , p_41);
        l_2453--;
    }
    (**l_1496) = (*g_2121);
    return p_39;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_61(uint32_t  p_62, uint32_t * p_63, uint64_t  p_64, int32_t  p_65)
{ /* block id: 537 */
    uint64_t l_1152 = 1UL;
    return l_1152;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_478
 */
static uint32_t * func_66(int32_t  p_67)
{ /* block id: 534 */
    int32_t *l_1149 = (void*)0;
    int32_t *l_1150 = (void*)0;
    int32_t *l_1151 = &g_478[3][3][0];
    (*l_1151) = (-10L);
    return l_1149;
}


/* ------------------------------------------ */
/* 
 * reads : g_3 g_91 g_100 g_83 g_23 g_47 g_129 g_143 g_119 g_17 g_149 g_151 g_152 g_4 g_218 g_356 g_445 g_355 g_54 g_328 g_329 g_205 g_609 g_610 g_257 g_155 g_478 g_626 g_639 g_261 g_714 g_728 g_754 g_755 g_128 g_359 g_360 g_51 g_730 g_490 g_902 g_988 g_991 g_1068 g_989 g_1088
 * writes: g_83 g_91 g_129 g_23 g_143 g_119 g_149 g_151 g_152 g_155 g_356 g_261 g_445 g_355 g_257 g_626 g_639 g_54 g_203 g_51 g_478 g_610 g_730 g_902 g_991 g_728
 */
static int8_t  func_70(int32_t  p_71, int8_t  p_72, int32_t * const  p_73)
{ /* block id: 29 */
    uint16_t *l_82 = &g_83[2];
    uint32_t *l_108 = (void*)0;
    uint8_t *l_146[6];
    int32_t l_147 = 0xA9C7313BL;
    int64_t *l_179 = &g_47;
    int64_t **l_178 = &l_179;
    int32_t l_201 = 0x3C716A15L;
    int32_t l_217[2][3] = {{0x8352772FL,0x8352772FL,0x8352772FL},{0x24362DD0L,0x24362DD0L,0x24362DD0L}};
    const int64_t l_347 = 0x3B058BA22E2F3D00LL;
    int32_t l_416 = 0L;
    int32_t l_417 = (-8L);
    uint16_t l_456[5];
    int32_t **l_459[1][6];
    int64_t ****l_506 = (void*)0;
    uint16_t l_507[7][5][7] = {{{0x0170L,8UL,1UL,8UL,0x0170L,4UL,0x24BCL},{0x27A9L,0x6C95L,3UL,1UL,65527UL,65528UL,0xFE7EL},{0x24BCL,0UL,65529UL,1UL,65535UL,65535UL,3UL},{0x27A9L,1UL,0x35EAL,0UL,0x847EL,0x27A9L,65527UL},{0x0170L,0x336EL,8UL,0xF208L,65532UL,65535UL,0x7A31L}},{{4UL,4UL,0x3E4FL,65535UL,0x7D20L,0UL,65527UL},{0x8942L,65535UL,0xB003L,65535UL,65531UL,0x615EL,0x6DFAL},{0x35EAL,0x847EL,0xCA62L,0xCC6BL,65526UL,65535UL,65526UL},{0x615EL,0x24BCL,0x24BCL,0x615EL,0x323DL,1UL,65535UL},{0x0C19L,0xA923L,65527UL,0x3E4FL,1UL,0x3D8CL,0xA923L}},{{0x0696L,8UL,1UL,0x3EEEL,0x160AL,0xE071L,65535UL},{0x2007L,0xFE7EL,1UL,1UL,65535UL,65528UL,65526UL},{0x9E73L,65535UL,0x5DD3L,0x8B79L,0UL,0x3EEEL,0x6DFAL},{7UL,65535UL,0x35EAL,1UL,0x6C95L,1UL,65527UL},{0UL,4UL,2UL,65529UL,0x3EEEL,0UL,0x7A31L}},{{0UL,0x35EAL,65526UL,65527UL,0xA923L,0xA923L,65527UL},{1UL,0x8613L,1UL,65535UL,1UL,0xB003L,3UL},{1UL,0xA3DCL,0UL,0x232AL,1UL,0x35EAL,0xFE7EL},{0x615EL,0x3EEEL,0x9E73L,1UL,0UL,0xB003L,0x24BCL},{0xF298L,0UL,65535UL,65529UL,1UL,0xA923L,65535UL}},{{1UL,65532UL,0xE320L,0UL,0x0696L,0UL,65532UL},{5UL,1UL,0xCA62L,0UL,1UL,65526UL,0x0C19L},{1UL,5UL,65530UL,0x8613L,0UL,0x615EL,0xE071L},{0x3E4FL,0x2007L,0xD5B6L,65529UL,0xA923L,65529UL,0xD5B6L},{65535UL,65535UL,0x0C69L,65530UL,0xB003L,8UL,0UL}},{{1UL,0UL,65526UL,7UL,0x35EAL,1UL,0x1FDAL},{0xE320L,0x160AL,0xAA57L,1UL,0xB003L,1UL,65535UL},{65528UL,0x7D20L,0x847EL,0xFE7EL,0xA923L,0x27A9L,4UL},{0xAA57L,0x0C69L,1UL,0x5DD3L,0UL,0UL,0x0375L},{65535UL,0x35EAL,1UL,0x6C95L,1UL,65527UL,65535UL}},{{0x24BCL,0x8422L,0x5156L,0x0C69L,0x3EEEL,65531UL,0x8942L},{0xABB7L,0UL,0x3E4FL,65528UL,65528UL,0x3E4FL,0UL},{0xC35AL,5UL,3UL,65535UL,0xE071L,0x8942L,1UL},{0xCA62L,0xD5B6L,65528UL,0xABB7L,0x3D8CL,65529UL,1UL},{0UL,65531UL,0x0375L,65535UL,1UL,2UL,65529UL}}};
    uint32_t l_532 = 0xD810C863L;
    int64_t ***l_563 = (void*)0;
    int16_t *l_569 = &g_119;
    uint16_t * const l_602 = (void*)0;
    uint8_t l_616[2][3] = {{1UL,1UL,0xFCL},{1UL,1UL,0xFCL}};
    int16_t *l_663 = &g_119;
    const int8_t **l_757 = &g_755[0];
    int8_t *l_833 = &g_445[2];
    int8_t **l_832 = &l_833;
    int8_t ***l_831 = &l_832;
    int8_t ****l_830 = &l_831;
    const int16_t * const l_843 = &g_730[8][0];
    int32_t l_883 = 1L;
    uint16_t l_891 = 0xFD74L;
    uint64_t l_970 = 1UL;
    uint64_t l_1031 = 2UL;
    uint8_t l_1072 = 250UL;
    uint8_t l_1124 = 255UL;
    int32_t l_1128 = 0xC9DDD189L;
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_146[i] = &g_91;
    for (i = 0; i < 5; i++)
        l_456[i] = 8UL;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 6; j++)
            l_459[i][j] = (void*)0;
    }
lbl_1047:
    if ((safe_mul_func_uint16_t_u_u((safe_div_func_int16_t_s_s(g_3, p_71)), ((*l_82) = (safe_rshift_func_int8_t_s_u(p_72, 6))))))
    { /* block id: 31 */
        uint32_t *l_86 = (void*)0;
        int32_t l_89[1];
        const uint8_t l_172 = 0x48L;
        int64_t l_238 = 1L;
        uint16_t *l_258 = (void*)0;
        int64_t ***l_325 = &l_178;
        int64_t ****l_324 = &l_325;
        uint8_t *l_401 = &g_143[3][2];
        int i;
        for (i = 0; i < 1; i++)
            l_89[i] = 0xB0D72699L;
        for (p_72 = 14; (p_72 != 23); p_72 = safe_add_func_int64_t_s_s(p_72, 9))
        { /* block id: 34 */
            uint8_t *l_90 = &g_91;
            int16_t *l_101[5];
            int32_t l_109 = (-1L);
            int32_t *l_150[5][5] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_151,&g_151,&g_151,&g_151,&g_151},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_151,&g_151,&g_151,&g_151,&g_151},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
            int i, j;
            for (i = 0; i < 5; i++)
                l_101[i] = (void*)0;
            g_152 |= (g_151 |= ((p_73 != l_86) , (safe_mul_func_int64_t_s_s(p_72, ((0xB8L | ((*l_90) ^= l_89[0])) && func_92((safe_sub_func_uint64_t_u_u(((g_119 = ((((l_89[0] = g_100) >= func_102(l_82, &g_83[2], l_108, l_109, p_72)) == g_100) || p_71)) <= 0UL), 0x5AD9A11D30D066B3LL)), l_146[5], l_86, g_17, l_147))))));
            g_149 &= (safe_rshift_func_uint8_t_u_s(0x36L, (g_155 = l_89[0])));
            l_147 = (safe_add_func_int16_t_s_s(g_83[2], 0x288AL));
        }
    }
    else
    { /* block id: 203 */
        int64_t l_460 = 0x96ADE0BD541151AALL;
        uint64_t *l_471[7] = {&g_261,&g_261,&g_261,&g_261,&g_261,&g_261,&g_261};
        int32_t l_473 = 0x43E40C70L;
        int32_t l_475[4] = {0x3C77F458L,0x3C77F458L,0x3C77F458L,0x3C77F458L};
        uint32_t l_491 = 0x0DA58D73L;
        int32_t l_527[7] = {(-7L),(-7L),(-3L),(-7L),(-7L),(-3L),(-7L)};
        int64_t ***l_533 = &g_330;
        const int64_t l_567 = 0xECCE271C434F8EBALL;
        uint32_t l_588 = 0x07F35D76L;
        const uint16_t l_669 = 0x8184L;
        const int32_t *l_713 = &g_714;
        const int32_t **l_712 = &l_713;
        int8_t *l_753 = (void*)0;
        int8_t **l_752 = &l_753;
        int64_t **l_829 = (void*)0;
        int16_t l_861 = 0xF0B2L;
        int i;
        l_460 = (((void*)0 != l_459[0][2]) >= g_4);
        if ((safe_div_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((l_473 = (((safe_sub_func_uint16_t_u_u(65529UL, (0UL ^ (g_218 == (&p_72 != (void*)0))))) | (safe_add_func_int8_t_s_s(l_460, ((g_261 = (g_356 |= g_151)) >= (+(*p_73)))))) && ((p_71 , 0x916C4743E299DC2FLL) < p_71))), g_83[2])), 65530UL)))
        { /* block id: 208 */
            int8_t l_474 = 3L;
            int32_t l_476 = 0x118E6898L;
            int32_t l_477 = 0x873EBF1AL;
            int32_t l_479 = 0x6F66ED17L;
            int32_t l_480 = 0x886A3D9CL;
            int32_t l_481 = 1L;
            int32_t l_482 = 9L;
            int32_t l_483 = 0x1B7049EEL;
            int32_t l_484 = 0x27BD1524L;
            int32_t l_485 = 0x3FFCD047L;
            int32_t l_486 = 0x21BDBE2DL;
            int32_t l_487 = 0xEBA7E83DL;
            int32_t l_488[10];
            int32_t l_489 = 0x449F7745L;
            int8_t *l_501 = &g_445[2];
            int8_t *l_502 = &l_474;
            int16_t l_528 = 1L;
            uint32_t l_534 = 18446744073709551608UL;
            int64_t * const l_604 = &g_47;
            int i;
            for (i = 0; i < 10; i++)
                l_488[i] = 9L;
            ++l_491;
            if ((((((safe_mod_func_uint32_t_u_u(0x340233AAL, g_47)) , &g_329) != ((safe_unary_minus_func_int8_t_s((safe_rshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_u(((*l_502) ^= ((*l_501) |= 7L)), (+((safe_sub_func_uint16_t_u_u((((void*)0 != &g_490) == ((-1L) & p_71)), 0x363DL)) | p_71)))), p_71)))) , l_506)) & g_100) && p_71))
            { /* block id: 212 */
                l_487 ^= (1L < p_71);
                l_482 &= (-6L);
            }
            else
            { /* block id: 215 */
                int16_t *l_514[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int32_t l_517[9][9] = {{0x6C778D27L,1L,(-1L),0x6C778D27L,2L,0x74573FBBL,5L,0x5F126BD7L,0x3E60865BL},{5L,1L,0x884F2D46L,5L,2L,0x5F126BD7L,0xC264D355L,0x5F126BD7L,2L},{0xC264D355L,1L,1L,0xC264D355L,2L,(-2L),0x6C778D27L,0x5F126BD7L,5L},{0x6C778D27L,1L,(-1L),0x6C778D27L,2L,0x74573FBBL,5L,0x77EB65D1L,0x04E420CFL},{1L,0x2EFB3D1CL,0x1581D6A7L,1L,(-6L),0x77EB65D1L,0x884F2D46L,0x77EB65D1L,(-6L)},{0x884F2D46L,0x2EFB3D1CL,0x2EFB3D1CL,0x884F2D46L,(-6L),0x8383386BL,(-1L),0x77EB65D1L,(-4L)},{(-1L),0x2EFB3D1CL,1L,(-1L),(-6L),(-1L),1L,0x77EB65D1L,0x04E420CFL},{1L,0x2EFB3D1CL,0x1581D6A7L,1L,(-6L),0x77EB65D1L,0x884F2D46L,0x77EB65D1L,(-6L)},{0x884F2D46L,0x2EFB3D1CL,0x2EFB3D1CL,0x884F2D46L,(-6L),0x8383386BL,(-1L),0x77EB65D1L,(-4L)}};
                const int64_t l_535 = 0xFFD0442BE3EC5E41LL;
                int64_t * const *l_565 = &l_179;
                int64_t * const **l_564 = &l_565;
                int64_t *l_595 = &g_155;
                int i, j;
                l_475[2] ^= ((g_119 > ((((*l_82) |= p_71) && l_507[6][2][2]) < (safe_add_func_int64_t_s_s((safe_rshift_func_uint32_t_u_u((g_17 == (safe_rshift_func_uint16_t_u_s((((g_355 &= 0x1862L) || ((-8L) ^ ((p_71 , (safe_add_func_uint16_t_u_u(p_71, 0UL))) | (*p_73)))) , g_54), 13))), l_477)), p_71)))) , l_517[0][1]);
                g_23 |= ((g_100 , ((((safe_mod_func_uint64_t_u_u(p_71, (safe_add_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((+(p_71 | (safe_sub_func_int32_t_s_s((g_129 > (l_527[4] > ((p_71 <= l_528) , ((+((((safe_sub_func_uint16_t_u_u((l_486 || (l_532 | 0x5CAAA569281A904CLL)), 0x7E18L)) && 65535UL) , l_533) == (*g_328))) | g_83[1])))), 0UL)))), l_534)), 0x07L)))) | 8L) | l_535) != 255UL)) == g_205);
                if ((safe_rshift_func_int16_t_s_u(g_100, 11)))
                { /* block id: 220 */
                    int64_t **l_553[2][4] = {{&l_179,&l_179,&l_179,&l_179},{&l_179,&l_179,&l_179,&l_179}};
                    int32_t l_554 = 0x6D62F496L;
                    int32_t l_568 = 0x21D1FC7FL;
                    int32_t l_587 = 0x21084F0CL;
                    int i, j;
                    for (g_119 = (-17); (g_119 < 6); g_119 = safe_add_func_uint16_t_u_u(g_119, 7))
                    { /* block id: 223 */
                        uint64_t l_540 = 0x7B05189CCAD6D783LL;
                        int64_t * const ***l_566 = &l_564;
                        uint32_t *l_572 = &g_129;
                        uint8_t *l_575 = (void*)0;
                        uint16_t **l_586 = &l_82;
                        g_149 = ((void*)0 == &g_478[3][3][0]);
                        --l_540;
                    }
                    ++l_588;
                    for (l_491 = 4; (l_491 < 32); ++l_491)
                    { /* block id: 239 */
                        int64_t *l_603 = &l_460;
                        l_475[2] |= (0xDCL < ((safe_div_func_int64_t_s_s(((0xF9L & (((*l_178) = l_595) != &l_567)) > (((safe_sub_func_int64_t_s_s((safe_mul_func_int32_t_s_s((l_517[0][1] = (safe_lshift_func_uint16_t_u_u((((g_205 , (l_602 == (void*)0)) , (p_72 && (l_603 == l_604))) >= l_517[0][1]), g_445[2]))), (*p_73))), 0x26D177BDA8505505LL)) != (*p_73)) < l_487)), p_72)) != 0xC8L));
                        if (g_17)
                            goto lbl_642;
                        return p_71;
                    }
                }
                else
                { /* block id: 245 */
                    int64_t ****l_615 = &g_329;
                    int32_t l_636 = 0x554DDED7L;
                    g_257 = &g_151;
                    l_517[3][8] = ((*g_257) = (((((safe_mul_func_uint8_t_u_u((safe_add_func_int8_t_s_s(((((void*)0 == g_609) || (((((void*)0 == l_471[3]) >= ((*l_595) = ((safe_add_func_uint64_t_u_u((((safe_add_func_uint32_t_u_u((p_72 & (((&l_563 != l_615) & (**g_609)) , (g_83[0] , l_616[1][1]))), (*g_257))) , p_71) | g_91), p_72)) && g_83[2]))) , g_155) , p_71)) >= p_72), l_473)), p_71)) , 0xACL) >= (**g_609)) , (void*)0) != (void*)0));
                    for (l_481 = 0; (l_481 >= 0); l_481 -= 1)
                    { /* block id: 252 */
                        int8_t **l_617 = &l_502;
                        int32_t ***l_625[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_625[i] = &l_459[0][2];
                        l_473 ^= ((((&g_445[2] != ((*l_617) = &p_72)) <= (safe_div_func_uint8_t_u_u((safe_mul_func_uint64_t_u_u(0x1EBE811E0EE7DC0FLL, ((+(safe_mod_func_uint32_t_u_u(((((&p_73 == (g_626 = &g_257)) , ((safe_div_func_uint8_t_u_u((safe_add_func_int64_t_s_s((!0x395BL), (g_3 < (safe_mul_func_int64_t_s_s(((safe_lshift_func_int32_t_s_u((g_143[5][2] && g_445[2]), g_129)) && 255UL), l_636))))), g_478[2][2][1])) > g_54)) , l_527[4]) >= (*p_73)), (*p_73)))) || 0xF20DL))), p_72))) , 0xDE4FL) & l_535);
                        (**g_626) = (*p_73);
                        (*g_626) = (*g_626);
                    }
                }
                for (l_479 = 0; (l_479 >= (-15)); --l_479)
                { /* block id: 262 */
                    return p_71;
                }
            }
            g_23 = (((void*)0 == &p_73) , l_482);
        }
        else
        { /* block id: 267 */
            return p_72;
        }
lbl_642:
        g_639[2] = g_639[2];
        if ((safe_add_func_int8_t_s_s(0L, ((g_54 >= (safe_mul_func_int32_t_s_s(((safe_add_func_int8_t_s_s(((0xF3L & (((safe_lshift_func_uint16_t_u_u((((safe_unary_minus_func_uint32_t_u(l_588)) && (safe_div_func_uint32_t_u_u((p_72 != (((0xC973C285B16D08ABLL >= (((((((safe_mul_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s(p_71, (!((safe_div_func_uint32_t_u_u(((((void*)0 != l_663) , (*p_73)) > l_567), (*p_73))) ^ (*p_73))))), p_71)) ^ p_72) , p_72) && l_527[2]) >= p_72) >= 1UL) , p_72)) , (*p_73)) < (*p_73))), l_527[6]))) , 65534UL), g_3)) , p_71) ^ g_4)) & g_155), p_72)) & 0x9227A84DL), 3L))) | p_72))))
        { /* block id: 272 */
            int64_t ****l_680 = (void*)0;
            int32_t l_684 = (-1L);
            const int32_t *l_710[2][6][7] = {{{&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2]},{&l_473,&l_473,&l_473,&l_473,&l_473,&l_473,&l_473},{&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2]},{&l_473,&l_473,&l_473,&l_473,&l_473,&l_473,&l_473},{&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2]},{&l_473,&l_473,&l_473,&l_473,&l_473,&l_473,&l_473}},{{&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2]},{&l_473,&l_473,&l_473,&l_473,&l_473,&l_473,&l_473},{&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2]},{&l_473,&l_473,&l_473,&l_473,&l_473,&l_473,&l_473},{&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2],&l_475[2]},{&l_473,&l_473,&l_473,&l_473,&l_473,&l_473,&l_473}}};
            const int32_t **l_709 = &l_710[1][5][1];
            const int32_t * const l_729[5][8] = {{&g_3,&l_475[2],&l_475[2],&g_3,&g_4,&g_3,&l_475[2],&l_475[2]},{&l_475[2],&g_4,&g_151,&g_151,&g_4,&l_475[2],&g_4,&g_151},{&g_3,&g_4,&g_3,&l_475[2],&l_475[2],&g_3,&g_4,&g_3},{&l_473,&l_475[2],&g_151,&l_475[2],&l_473,&l_473,&l_475[2],&g_151},{&l_473,&l_473,&l_475[2],&g_151,&l_475[2],&l_473,&l_473,&l_475[2]}};
            uint32_t l_731 = 0x5D10B8F7L;
            int32_t l_791 = 7L;
            int8_t ***l_804 = &l_752;
            uint64_t *l_862 = &g_356;
            uint32_t *l_863 = (void*)0;
            uint32_t *l_864 = &g_54;
            int i, j, k;
            (*g_626) = (*g_626);
            if ((p_71 <= 0xE485803BL))
            { /* block id: 274 */
                uint16_t l_685 = 1UL;
                const int64_t *l_692 = &g_155;
                const int64_t **l_691 = &l_692;
                const int64_t ***l_690 = &l_691;
                const int64_t ****l_689 = &l_690;
                const int64_t *****l_688 = &l_689;
                int32_t l_715 = 0x01F5FD3CL;
                for (g_54 = 11; (g_54 == 60); g_54 = safe_add_func_int8_t_s_s(g_54, 5))
                { /* block id: 277 */
                    int64_t * const *l_683 = &l_179;
                    int64_t * const **l_682 = &l_683;
                    int64_t * const ***l_681 = &l_682;
                    const int32_t l_707[3][10] = {{0L,0x0EA4C669L,(-1L),0L,(-1L),0x0EA4C669L,0L,0xE9862A53L,0xE9862A53L,0L},{0xE9862A53L,0xEB896647L,(-2L),(-2L),(-1L),(-3L),6L,(-1L),6L,(-3L)},{0x0EA4C669L,(-1L),0L,(-1L),0x0EA4C669L,0L,0xE9862A53L,0xE9862A53L,0L,0x0EA4C669L}};
                    int32_t l_727[1][9][10] = {{{(-1L),0x3AD30772L,0x3AD30772L,(-1L),0L,0x3405303FL,(-1L),0x3AD30772L,0x3405303FL,0x0010F954L},{0x3405303FL,(-1L),0x3AD30772L,0x3405303FL,0x0010F954L,0x3405303FL,0x3AD30772L,(-1L),0x3405303FL,0L},{(-1L),(-1L),0x95722CF2L,(-1L),0x0010F954L,0xC32580C4L,(-1L),(-1L),0xC32580C4L,0x0010F954L},{(-1L),0x3AD30772L,0x3AD30772L,(-1L),0L,0x3405303FL,(-1L),0x3AD30772L,0x3405303FL,0x0010F954L},{0x3405303FL,(-1L),0x3AD30772L,0x3405303FL,0x0010F954L,0x3405303FL,0x3AD30772L,(-1L),0x3405303FL,0L},{(-1L),(-1L),0x95722CF2L,(-1L),0x0010F954L,0xC32580C4L,(-1L),(-1L),0xC32580C4L,0x0010F954L},{(-1L),0x3AD30772L,0x3AD30772L,(-1L),0L,0x3405303FL,(-1L),0x3AD30772L,0x3405303FL,0x0010F954L},{0x3405303FL,(-1L),0x3AD30772L,0x3405303FL,0x0010F954L,0x3405303FL,0x3AD30772L,(-1L),0x3405303FL,0L},{(-1L),(-1L),0x95722CF2L,(-1L),0x0010F954L,0xC32580C4L,(-1L),(-1L),0xC32580C4L,0x0010F954L}}};
                    int i, j, k;
                    if ((0xDAD2A46112E54424LL ^ (((((((safe_add_func_uint32_t_u_u((+0xF2BC2556L), l_669)) == (((g_203 = (*p_73)) || (safe_rshift_func_uint16_t_u_u((safe_mul_func_int64_t_s_s((safe_add_func_uint32_t_u_u((g_51 = ((+0xD1F0L) > g_218)), g_17)), ((safe_lshift_func_uint32_t_u_s((~(l_680 != l_681)), 16)) | 0x2F014713L))), 12))) && l_684)) && p_71) > l_685) | 0xF44AL) | p_71) | 18446744073709551615UL)))
                    { /* block id: 280 */
                        uint32_t *l_708 = &l_491;
                        (*g_626) = ((((*g_610) = ((safe_add_func_uint8_t_u_u(0UL, (&l_680 != l_688))) , 248UL)) & (((*l_708) ^= ((((1L || 0xF399L) , (*p_73)) | (safe_lshift_func_int64_t_s_s((safe_add_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u((((safe_sub_func_int32_t_s_s((safe_rshift_func_uint32_t_u_u(g_4, 24)), (safe_rshift_func_uint8_t_u_u((safe_sub_func_int32_t_s_s((l_707[1][5] & l_685), 0xFDC7A60BL)), p_72)))) || 1UL) , l_684), 1L)), 4294967293UL)), p_71))) | l_567)) != 0xD2FB89A0L)) , (void*)0);
                        if ((*p_73))
                            break;
                        return g_478[3][3][0];
                    }
                    else
                    { /* block id: 286 */
                        const int32_t ***l_711[1];
                        int32_t l_726 = (-1L);
                        int i;
                        for (i = 0; i < 1; i++)
                            l_711[i] = &l_709;
                        g_23 = (l_727[0][6][6] = (((&p_73 != (((*g_610) , p_71) , (l_712 = (g_17 , l_709)))) && ((l_726 ^= ((++g_261) >= (safe_rshift_func_uint32_t_u_s((g_129 | (255UL <= ((*l_713) <= (safe_rshift_func_uint8_t_u_u((safe_add_func_int64_t_s_s((p_72 < 255UL), l_707[1][5])), 3))))), 2)))) , 0x51E93DF4B974B94BLL)) == l_707[1][5]));
                        return g_728;
                    }
                }
                (*l_709) = l_729[0][5];
            }
            else
            { /* block id: 296 */
                int32_t *l_745 = &g_478[4][1][2];
                const int8_t ***l_756[8][5] = {{(void*)0,&g_754,&g_754,(void*)0,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,(void*)0,&g_754,&g_754,(void*)0},{&g_754,&g_754,&g_754,&g_754,&g_754},{(void*)0,(void*)0,&g_754,(void*)0,(void*)0},{&g_754,&g_754,&g_754,&g_754,&g_754},{(void*)0,&g_754,&g_754,(void*)0,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754}};
                uint32_t *l_771 = &g_54;
                int64_t *l_776 = (void*)0;
                int64_t *l_777[1][7];
                uint8_t *l_778 = &g_91;
                int32_t l_790[3];
                uint16_t *l_860 = &l_507[6][2][2];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 7; j++)
                        l_777[i][j] = &l_460;
                }
                for (i = 0; i < 3; i++)
                    l_790[i] = 0x1FC630D4L;
                for (p_71 = 0; (p_71 <= 4); p_71 += 1)
                { /* block id: 299 */
                    int32_t l_744 = 0x04B16348L;
                    l_731++;
                    for (l_147 = 0; (l_147 <= 4); l_147 += 1)
                    { /* block id: 303 */
                        int i;
                    }
                }
                if (((safe_lshift_func_int8_t_s_s((p_72 ^= 0x5CL), 0)) != (((safe_unary_minus_func_int64_t_s((safe_mul_func_uint32_t_u_u(((+0x5CC61A1428331507LL) || (l_752 == (l_757 = g_754))), ((((safe_rshift_func_uint16_t_u_s(g_83[2], 4)) && ((*l_713) <= ((*l_745) &= (safe_sub_func_uint8_t_u_u((!((l_473 = ((safe_rshift_func_uint32_t_u_s((safe_add_func_int8_t_s_s(((((safe_add_func_int8_t_s_s((safe_sub_func_uint8_t_u_u((((*l_771)++) , (**l_712)), ((safe_mul_func_uint64_t_u_u(g_445[0], (p_71 ^ 1UL))) || 0xBBD3B958L))), (**l_712))) || p_71) && p_71) != p_71), (**g_754))), (*p_73))) | g_714)) >= (-1L))), p_71))))) , (void*)0) != l_778))))) == (**g_754)) == 0x2A180EC5L)))
                { /* block id: 315 */
                    int16_t l_798 = (-1L);
                    int64_t ****l_807 = &g_329;
                    if ((safe_unary_minus_func_uint8_t_u((safe_rshift_func_int16_t_s_u(((safe_mod_func_uint16_t_u_u((((void*)0 != &g_730[5][0]) && (safe_add_func_uint16_t_u_u(p_72, (+(((~l_790[2]) == (l_791 &= ((l_684 &= (**l_709)) != (0x5A7DL || p_72)))) < ((((safe_rshift_func_int32_t_s_s((((**l_709) && ((safe_div_func_int32_t_s_s((safe_rshift_func_int64_t_s_s(l_798, (**l_712))), g_54)) | p_72)) < g_728), (*l_713))) , p_71) , l_179) == l_471[6])))))), g_119)) ^ 18446744073709551610UL), 4)))))
                    { /* block id: 318 */
                        uint32_t l_799[7];
                        int8_t **l_824 = &l_753;
                        int i;
                        for (i = 0; i < 7; i++)
                            l_799[i] = 5UL;
                        ++l_799[2];
                        (*l_712) = (*g_626);
                        l_475[2] ^= ((safe_lshift_func_uint8_t_u_u((l_804 == &g_754), 7)) && (safe_sub_func_int16_t_s_s((l_799[2] , (((*l_663) = 2L) <= (l_680 != l_807))), (safe_mod_func_int64_t_s_s((~(--(*l_82))), (safe_div_func_uint64_t_u_u((safe_sub_func_uint32_t_u_u(0x18F6575DL, (safe_mul_func_int64_t_s_s((!(((safe_lshift_func_uint32_t_u_u(g_218, (safe_rshift_func_uint64_t_u_s(((g_261 = ((g_356 <= (*p_73)) , g_100)) > 0xE87BD38CA76E619BLL), 7)))) > 1L) & 65532UL)), 0x37F4BC9070CCD320LL)))), p_72)))))));
                        (*l_745) = (((*l_745) < (((((p_71 <= 1L) , &p_72) != ((**l_804) = &g_445[2])) , (g_128 != (((&g_755[4] != l_824) <= (safe_mul_func_int32_t_s_s((safe_mul_func_uint8_t_u_u(p_71, p_71)), 0x5F48701CL))) != p_71))) && (-3L))) >= p_72);
                    }
                    else
                    { /* block id: 327 */
                        int8_t *****l_834 = &l_830;
                        int8_t ****l_836[5];
                        int8_t *****l_835 = &l_836[4];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_836[i] = &l_831;
                        (*g_626) = (*g_626);
                        (*l_709) = ((*g_626) = &l_791);
                        (*l_745) ^= ((void*)0 == l_829);
                        (*l_835) = ((*l_834) = l_830);
                    }
                    return l_798;
                }
                else
                { /* block id: 336 */
                    uint32_t l_859 = 0x7FF5CF69L;
                    if ((safe_mul_func_uint8_t_u_u((safe_add_func_int32_t_s_s((l_475[0] = ((((g_155 = ((((safe_rshift_func_uint16_t_u_u((((*l_771) = g_155) ^ ((*l_745) = ((g_155 , l_843) != (void*)0))), (g_91 != p_71))) && (p_72 >= (**l_712))) , &g_610) != (void*)0)) , g_23) , 0x558D3E14L) & 0x31F23D9DL)), g_356)), 255UL)))
                    { /* block id: 341 */
                        uint32_t *l_854 = &l_731;
                        l_791 = (safe_add_func_uint32_t_u_u(((*p_73) <= ((*l_771) = p_72)), (g_51 = ((((((((*l_569) |= (0xA995AB087DCBC0F0LL && (p_72 , (safe_div_func_uint16_t_u_u((safe_mul_func_uint32_t_u_u((((safe_rshift_func_int64_t_s_u(((safe_sub_func_uint32_t_u_u((--(*l_854)), ((safe_sub_func_uint64_t_u_u((g_261 = l_859), ((**g_754) == (l_860 != (*g_359))))) || (*l_745)))) , l_859), 38)) , p_71) || l_859), 0UL)), p_71))))) < l_859) && g_152) >= (*l_713)) || p_71) != g_51) < l_861))));
                        (*l_745) &= (l_862 != (p_72 , &g_356));
                    }
                    else
                    { /* block id: 349 */
                        return l_859;
                    }
                    g_151 = ((*l_745) = 0x29686BFAL);
                }
            }
            l_791 = ((*g_328) == ((++(*l_864)) , &l_829));
        }
        else
        { /* block id: 358 */
            uint32_t l_867 = 0xB665618AL;
            int32_t **l_868 = &g_257;
            l_527[4] ^= l_867;
            (*l_868) = p_73;
            for (l_417 = 0; (l_417 == 17); l_417 = safe_add_func_uint64_t_u_u(l_417, 1))
            { /* block id: 363 */
                g_149 = (&g_640 == (g_143[5][2] , &g_640));
            }
        }
    }
    if ((g_149 ^= ((safe_mul_func_int64_t_s_s((((((safe_lshift_func_uint16_t_u_s((safe_mod_func_uint16_t_u_u((safe_mod_func_int16_t_s_s((safe_lshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_s(((((l_883 = (((*g_609) = &l_616[1][1]) != &l_616[1][1])) >= (((((safe_sub_func_int32_t_s_s(((!0x1E34FA0F4ED0ADF4LL) ^ (p_71 && ((safe_div_func_int32_t_s_s((g_23 ^= ((g_730[0][0] &= ((void*)0 != &g_261)) && g_490)), (((safe_rshift_func_int8_t_s_s(l_891, ((safe_sub_func_int16_t_s_s(((*l_569) = (1L > p_72)), p_71)) && 0L))) < (*p_73)) , g_143[5][2]))) ^ 0x21FD1647DF03A9D2LL))), p_72)) == 0xB7L) <= (*p_73)) | (*p_73)) && p_71)) ^ g_490) || g_47), p_72)), g_143[5][2])), g_355)), p_72)), 12)) <= g_143[1][0]) ^ (**g_754)) < p_71) , p_71), g_478[4][5][0])) != p_72)))
    { /* block id: 374 */
        int32_t l_900 = 0x58270C9BL;
        uint8_t *l_901[3];
        int32_t l_903 = 0x0EE9BCD4L;
        int i;
        for (i = 0; i < 3; i++)
            l_901[i] = &g_91;
        for (l_417 = (-8); (l_417 <= (-17)); --l_417)
        { /* block id: 377 */
            for (l_891 = (-13); (l_891 == 13); l_891 = safe_add_func_int64_t_s_s(l_891, 4))
            { /* block id: 380 */
                (*g_626) = (*g_626);
                (*g_626) = (*g_626);
                return p_71;
            }
        }
        l_900 = (safe_sub_func_int32_t_s_s((-9L), (*p_73)));
        l_903 = (g_902 |= (&l_616[1][0] != (l_901[1] = &l_616[0][1])));
    }
    else
    { /* block id: 390 */
        int64_t l_924[7][9] = {{(-9L),0L,1L,1L,0L,(-9L),0x2B4B705E7C5DC705LL,5L,3L},{(-9L),0L,0x7D4DE743C36174A9LL,(-9L),0xFFD076AC0AB3F643LL,3L,3L,5L,0x7D4DE743C36174A9LL},{3L,0xFFD076AC0AB3F643LL,0x2B4B705E7C5DC705LL,3L,0xDC69299EC717A66BLL,3L,0x2B4B705E7C5DC705LL,0xFFD076AC0AB3F643LL,3L},{0x7D4DE743C36174A9LL,5L,3L,3L,0xFFD076AC0AB3F643LL,(-9L),0x7D4DE743C36174A9LL,0L,(-9L)},{3L,5L,0x2B4B705E7C5DC705LL,(-9L),0L,1L,0xE0B60085E999D3D3LL,1L,0x1303F84910EC29FCLL},{0x895BEF4098D3B589LL,0x8A6C35032A731440LL,0x895BEF4098D3B589LL,0xE0B60085E999D3D3LL,3L,(-1L),0xE0B60085E999D3D3LL,3L,0x3E806C713C962E1CLL},{0x3E806C713C962E1CLL,3L,0xE0B60085E999D3D3LL,(-1L),3L,0xE0B60085E999D3D3LL,0x895BEF4098D3B589LL,0x8A6C35032A731440LL,0x895BEF4098D3B589LL}};
        int64_t **l_926 = &l_179;
        int32_t l_928 = 0x94548417L;
        uint32_t *l_966 = &g_54;
        uint32_t *l_968[5];
        uint8_t *l_975 = (void*)0;
        const uint32_t l_993 = 1UL;
        int32_t l_994 = 0L;
        int32_t l_995 = (-1L);
        int64_t *****l_1030 = &l_506;
        int32_t l_1039[5][4] = {{0xBE05449CL,0xBE05449CL,0xBE05449CL,0xBE05449CL},{0xBE05449CL,0xBE05449CL,0xBE05449CL,0xBE05449CL},{0xBE05449CL,0xBE05449CL,0xBE05449CL,0xBE05449CL},{0xBE05449CL,0xBE05449CL,0xBE05449CL,0xBE05449CL},{0xBE05449CL,0xBE05449CL,0xBE05449CL,0xBE05449CL}};
        int i, j;
        for (i = 0; i < 5; i++)
            l_968[i] = (void*)0;
lbl_996:
        for (g_91 = 1; (g_91 <= 5); g_91 += 1)
        { /* block id: 393 */
            uint16_t l_915 = 65535UL;
            int32_t l_925 = 0x24F0BE1FL;
            int32_t l_933 = 0x845ED9D0L;
            int32_t l_938 = 0xA9E14352L;
            int32_t *l_958 = &l_217[0][1];
            uint32_t **l_967 = &l_966;
            uint64_t l_969 = 0UL;
            const uint32_t ***l_992 = &g_991;
            int i;
            for (g_203 = 4; (g_203 >= 0); g_203 -= 1)
            { /* block id: 396 */
                int32_t l_910 = 0xA9494990L;
                int32_t l_927 = 0x6C2FFE97L;
                int32_t *l_929[8] = {&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4,&g_4};
                const uint16_t *l_944 = (void*)0;
                const uint16_t **l_943 = &l_944;
                int i;
                l_928 = (l_927 &= (safe_rshift_func_uint8_t_u_s(p_72, ((((safe_div_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((((l_910 | (((l_925 ^= ((((((g_730[0][0] = ((safe_sub_func_uint8_t_u_u(((safe_lshift_func_uint8_t_u_s(l_915, 0)) >= ((((((!((safe_mul_func_int32_t_s_s((((*p_73) != (safe_mod_func_uint8_t_u_u(((*l_830) != (void*)0), p_72))) , (safe_mod_func_uint64_t_u_u((((!p_72) <= ((l_924[1][5] <= (*p_73)) , l_910)) ^ (*p_73)), 0x6FD045ADBE8EFE86LL))), (*p_73))) ^ 0xA1E37E79L)) ^ 0x1587L) || p_72) , p_71) & l_910) <= l_915)), l_915)) && l_924[1][5])) ^ l_910) , (-2L)) == 0x63CC4087D62E2FAALL) != 0xFB256AEDL) == 3L)) & g_51) < l_924[1][5])) , (void*)0) == l_926), p_71)), l_910)) <= (*p_73)) <= l_910) , (**g_754)))));
                for (g_51 = 1; (g_51 <= 5); g_51 += 1)
                { /* block id: 403 */
                    uint16_t l_932 = 0x41D4L;
                    int8_t l_941 = 7L;
                    int32_t l_942 = 0xE7D2FCCCL;
                    l_929[1] = p_73;
                    if (g_152)
                        goto lbl_996;
                    for (g_902 = 1; (g_902 <= 5); g_902 += 1)
                    { /* block id: 407 */
                        int i;
                        l_938 &= ((((safe_add_func_int32_t_s_s(l_915, 0x869EEC35L)) , (p_72 & ((l_933 ^= (l_925 == l_932)) , ((*g_328) != (void*)0)))) & (((safe_rshift_func_uint8_t_u_s((safe_lshift_func_uint8_t_u_u(0x0CL, (**g_609))), p_71)) > g_83[2]) > 0UL)) <= g_478[3][3][0]);
                        l_941 |= (safe_rshift_func_int8_t_s_u(0xA5L, 7));
                    }
                    l_942 = (*p_73);
                }
                l_933 = (l_938 = ((((((l_924[1][5] < g_119) , ((0x73254C0656C47F5ELL && (((((((*l_943) = &l_507[2][4][6]) != &l_507[0][4][0]) > ((((l_928 = (safe_mul_func_int16_t_s_s(0x3B18L, ((safe_div_func_int16_t_s_s(((*l_663) = (2UL || ((g_356 , (*g_610)) == 0x3BL))), p_71)) != l_938)))) , 0x6152L) , (*p_73)) | 4294967291UL)) != 0xA0L) != l_924[6][5]) < 0x18CA361EL)) | 0x91L)) == p_72) | g_47) || p_71) || 0UL));
            }
            g_151 &= ((safe_unary_minus_func_int8_t_s((((safe_div_func_uint16_t_u_u(((safe_mul_func_uint32_t_u_u((l_970 = ((safe_mul_func_int8_t_s_s(p_72, (safe_div_func_int8_t_s_s(p_71, (((((((((((l_958 == (void*)0) || g_149) || (safe_mul_func_uint32_t_u_u(((safe_mul_func_uint8_t_u_u((safe_lshift_func_uint32_t_u_s((safe_unary_minus_func_int64_t_s((((((*l_967) = l_966) != (l_968[1] = l_108)) != ((0UL != (p_71 ^ 0xB7L)) >= 0x8923635062B2BDA7LL)) <= l_915))), 24)), 0x4AL)) ^ 0x9D591D504E9FD194LL), (-1L)))) <= p_71) , g_205) & p_71) , l_928) < (*g_610)) , l_969) | p_71) | l_915))))) > (**g_609))), (*p_73))) >= p_72), 3UL)) && l_915) & p_71))) > g_445[2]);
            l_928 = l_925;
            l_925 ^= (safe_add_func_int16_t_s_s((safe_add_func_int32_t_s_s((l_975 != (l_146[g_91] = (void*)0)), p_72)), (9L > (safe_div_func_int32_t_s_s((safe_div_func_int16_t_s_s((p_71 <= (safe_mul_func_uint16_t_u_u((l_995 = ((*l_82) = (safe_mul_func_int8_t_s_s((l_915 > (l_994 = (((((l_928 = (safe_add_func_int8_t_s_s(0xF8L, (safe_mul_func_uint64_t_u_u((g_988 != ((*l_992) = g_991)), 1L))))) != 0UL) <= l_993) > p_71) | p_72))), 0x3AL)))), 9L))), g_100)), p_71)))));
        }
        for (l_970 = 0; (l_970 != 27); l_970++)
        { /* block id: 436 */
            const uint32_t l_1020 = 0x46260688L;
            int32_t l_1034 = 5L;
            int32_t l_1036 = (-8L);
            int32_t l_1037 = 0L;
            int32_t l_1038[3];
            uint32_t l_1040 = 18446744073709551615UL;
            int i;
            for (i = 0; i < 3; i++)
                l_1038[i] = 0xC0930D16L;
            for (l_147 = (-24); (l_147 < 14); l_147 = safe_add_func_uint8_t_u_u(l_147, 1))
            { /* block id: 439 */
                int64_t l_1015 = 0x44567157B418FB25LL;
                for (g_51 = 0; (g_51 >= 8); ++g_51)
                { /* block id: 442 */
                    uint16_t l_1007 = 0x6E7AL;
                    int32_t l_1008 = 0x91DDADF7L;
                    for (l_891 = 0; (l_891 >= 6); ++l_891)
                    { /* block id: 445 */
                        int32_t ***l_1006 = &g_626;
                        int32_t ****l_1005 = &l_1006;
                        l_1008 = (l_1007 = (g_151 &= (((*l_1005) = &g_626) == (void*)0)));
                    }
                    (*g_626) = (*g_626);
                    g_151 = l_994;
                    l_1008 = (*p_73);
                }
                if (((safe_mod_func_uint64_t_u_u(((p_71 , p_72) || p_72), g_91)) | (((((*l_82) ^= ((((safe_lshift_func_uint64_t_u_u((safe_div_func_int8_t_s_s((((****l_830) = l_1015) > (safe_sub_func_int8_t_s_s((safe_add_func_int32_t_s_s(l_1020, (((l_1020 <= ((safe_div_func_uint8_t_u_u((!(safe_mul_func_uint8_t_u_u((p_71 , (((p_71 > p_72) < 0x80B2C8F56127E163LL) , p_72)), 255UL))), 0xEDL)) > l_1020)) & p_71) && l_1020))), 0UL))), p_72)), g_218)) & p_72) , 0xA8A96CE6L) && (*p_73))) , g_17) <= p_71) , p_71)))
                { /* block id: 457 */
                    int32_t l_1026 = 0x43D549C4L;
                    g_478[2][2][3] |= l_1026;
                    if (g_100)
                        goto lbl_996;
                }
                else
                { /* block id: 460 */
                    uint32_t l_1027[3][8][5] = {{{0x4E6E069AL,4294967286UL,0xA3B1CA64L,0x9A008143L,1UL},{8UL,0x42137958L,0x44688818L,0x7EEA8D2BL,4294967289UL},{1UL,0UL,0x0EEAE67DL,0x626CD87CL,0x0EEAE67DL},{1UL,1UL,0x3CE112FBL,4294967286UL,0x199C52BCL},{8UL,0xB610609DL,8UL,0x0EEAE67DL,0xB610609DL},{0x4E6E069AL,0x0AE00A12L,0x46AB05C8L,0xB610609DL,0x0EEAE67DL},{0x42137958L,0xB610609DL,0xA3B1CA64L,0x199C52BCL,4294967286UL},{0x44688818L,1UL,0UL,0x0EEAE67DL,0x626CD87CL}},{{4294967289UL,0UL,0UL,4294967289UL,0x7EEA8D2BL},{0x199C52BCL,0x42137958L,0xA3B1CA64L,1UL,0x9A008143L},{8UL,4294967286UL,0x46AB05C8L,0x7EEA8D2BL,0x42137958L},{0x9A008143L,0UL,8UL,1UL,0x0EEAE67DL},{0x626CD87CL,0x9A008143L,0x3CE112FBL,4294967289UL,0x4E6E069AL},{8UL,0x4E6E069AL,0x0EEAE67DL,0x0EEAE67DL,0x4E6E069AL},{0xB610609DL,0x0AE00A12L,0x44688818L,0x199C52BCL,0x0EEAE67DL},{4294967286UL,0x199C52BCL,0xA3B1CA64L,0xB610609DL,0x42137958L}},{{0x44688818L,0x626CD87CL,0x0AE00A12L,0x0EEAE67DL,0x9A008143L},{4294967286UL,0UL,0UL,4294967286UL,0x7EEA8D2BL},{0xB610609DL,4294967289UL,0xA3B1CA64L,0x626CD87CL,0x626CD87CL},{8UL,4294967289UL,8UL,0x7EEA8D2BL,4294967286UL},{0x626CD87CL,0UL,0x7EEA8D2BL,0x9A008143L,0x0EEAE67DL},{0x9A008143L,0x626CD87CL,0x3CE112FBL,0x42137958L,0xB610609DL},{8UL,0x199C52BCL,0x7EEA8D2BL,0x0EEAE67DL,0x199C52BCL},{0x199C52BCL,0x0AE00A12L,8UL,0x4E6E069AL,0x0EEAE67DL}}};
                    int i, j, k;
                    if ((*p_73))
                        break;
                    if (l_1027[0][0][1])
                    { /* block id: 462 */
                        l_928 = ((safe_sub_func_int16_t_s_s(p_71, p_71)) < p_71);
                        l_1030 = &g_328;
                        g_149 = ((void*)0 == &p_71);
                    }
                    else
                    { /* block id: 466 */
                        if (g_490)
                            goto lbl_996;
                        return l_1031;
                    }
                }
                l_1034 = (safe_mod_func_uint32_t_u_u((g_152 = p_72), p_71));
            }
            --l_1040;
            if ((*p_73))
                continue;
        }
    }
    for (g_261 = 0; (g_261 == 43); g_261++)
    { /* block id: 480 */
        uint16_t l_1065[8][2] = {{65534UL,65534UL},{0UL,65534UL},{65534UL,0UL},{65534UL,65534UL},{0UL,65534UL},{65534UL,0UL},{65534UL,65534UL},{0UL,65534UL}};
        int32_t l_1066 = 0x3BD308C3L;
        int8_t **** const *l_1067 = &l_830;
        int8_t l_1071 = (-2L);
        uint32_t *l_1073[2];
        int32_t l_1074 = 0L;
        int i, j;
        for (i = 0; i < 2; i++)
            l_1073[i] = &g_152;
        for (l_891 = 0; (l_891 > 6); l_891 = safe_add_func_int8_t_s_s(l_891, 9))
        { /* block id: 483 */
            if (g_128)
                goto lbl_1047;
        }
        g_23 = (&g_755[3] == ((safe_rshift_func_int8_t_s_u((-8L), (p_71 != (l_1074 ^= ((safe_div_func_int16_t_s_s((g_355 &= ((((0x2F2E59CD9E56701CLL || ((safe_lshift_func_uint16_t_u_s((((((safe_add_func_uint16_t_u_u((((+(safe_sub_func_uint8_t_u_u((((((l_1066 = ((((((g_478[1][1][4] ^= (*p_73)) , (!g_54)) | 4294967294UL) ^ ((*l_82) |= (~(safe_add_func_uint64_t_u_u((((*l_569) = (((((safe_div_func_int64_t_s_s((((0x6C0DL <= l_1065[0][0]) , (-1L)) , 1L), p_72)) < (**g_609)) | 0x517CL) & g_205) > 0xFDEFL)) || 1L), p_71))))) & p_71) || 0x8730L)) & l_1065[0][0]) <= p_72) == 0x0BL) <= (-5L)), p_72))) <= 0UL) & p_71), 6L)) <= 0x73821EF3L) , l_1067) != g_1068) > l_1065[0][0]), 3)) & l_1065[3][0])) != l_1065[2][1]) && l_1071) & 0x05L)), l_1072)) >= l_1065[0][0]))))) , (void*)0));
    }
    for (g_155 = 0; (g_155 >= 0); g_155 -= 1)
    { /* block id: 496 */
        int64_t l_1087[5][2] = {{0xFCCAE40CB3B1CE87LL,0xFCCAE40CB3B1CE87LL},{0xFCCAE40CB3B1CE87LL,0xFCCAE40CB3B1CE87LL},{0xFCCAE40CB3B1CE87LL,0xFCCAE40CB3B1CE87LL},{0xFCCAE40CB3B1CE87LL,0xFCCAE40CB3B1CE87LL},{0xFCCAE40CB3B1CE87LL,0xFCCAE40CB3B1CE87LL}};
        uint16_t l_1091 = 65535UL;
        int32_t **l_1096 = (void*)0;
        int32_t ***l_1097 = &l_1096;
        int32_t l_1104 = 0x50FD679AL;
        uint64_t *l_1105 = &l_970;
        uint32_t **l_1115 = (void*)0;
        uint32_t ***l_1116 = (void*)0;
        uint32_t ***l_1117 = &l_1115;
        uint32_t *l_1125 = &g_54;
        int16_t *l_1126 = (void*)0;
        int16_t *l_1127 = &g_355;
        int i, j;
        for (g_149 = 8; (g_149 >= 2); g_149 -= 1)
        { /* block id: 499 */
            int8_t *l_1081 = &g_445[2];
            const uint64_t l_1082 = 18446744073709551614UL;
            int32_t ***l_1090 = &l_459[0][0];
            int i, j;
            if ((safe_mod_func_uint64_t_u_u(((l_217[(g_155 + 1)][g_155] | (0xD1B5CC9081C03BF1LL || (safe_mul_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(((((((&p_72 == ((**l_831) = l_1081)) == g_143[(g_155 + 4)][(g_155 + 2)]) == (((((((void*)0 != (*g_988)) || (l_1082 | (safe_mod_func_uint64_t_u_u((((safe_mod_func_uint32_t_u_u(g_149, l_1087[0][0])) && p_72) ^ 0UL), p_71)))) ^ p_72) == 65535UL) && p_71) , 1L)) , g_1088) == l_1090) > l_1091), l_1087[0][0])), 0x98L)))) < 0x2548EEB6L), p_72)))
            { /* block id: 501 */
                return p_72;
            }
            else
            { /* block id: 503 */
                int32_t l_1092 = 0xBE55E3EAL;
                return l_1092;
            }
        }
        l_1104 = (safe_div_func_int64_t_s_s((l_1091 | (safe_unary_minus_func_uint32_t_u(((p_72 , ((*l_1097) = l_1096)) != ((safe_mod_func_int32_t_s_s((safe_rshift_func_int32_t_s_u(((safe_rshift_func_int8_t_s_s((g_445[(g_155 + 2)] = (l_1091 > ((void*)0 != &l_179))), 2)) , (&g_626 == &l_459[0][2])), 22)), 4294967289UL)) , (void*)0))))), g_490));
        (*g_626) = ((((*l_1105) = g_356) < (safe_sub_func_uint8_t_u_u(((-2L) & (0x7D56C788L < (safe_mod_func_uint8_t_u_u(((~((*l_1127) = (g_730[0][0] = (l_1104 &= ((safe_mod_func_uint32_t_u_u(((*l_1125) &= (safe_mod_func_uint8_t_u_u(((**g_609) = ((((*l_1117) = l_1115) == (void*)0) && (safe_add_func_uint64_t_u_u(g_129, (safe_mul_func_uint32_t_u_u(((p_71 < (((*l_663) = ((safe_div_func_int64_t_s_s(l_1091, p_72)) < l_1124)) != p_72)) != 0xF2609C0BL), p_71)))))), 0xD5L))), 1L)) >= g_445[(g_155 + 2)]))))) , (**g_609)), l_1087[0][0])))), l_1128))) , (void*)0);
        for (g_728 = 0; (g_728 <= 1); g_728 += 1)
        { /* block id: 521 */
            uint16_t l_1129[5][9][2] = {{{0x4CADL,0x97FCL},{0UL,0x97FCL},{0x4CADL,0x7DF1L},{65534UL,0x16E8L},{0x1B17L,0UL},{0x1B17L,0x16E8L},{65534UL,0x7DF1L},{0x4CADL,0x97FCL},{0UL,0x97FCL}},{{0x4CADL,0x7DF1L},{65534UL,0x16E8L},{0x1B17L,0UL},{0x1B17L,0x16E8L},{65534UL,0x7DF1L},{0x4CADL,0x97FCL},{0UL,0x97FCL},{0x4CADL,0x7DF1L},{65534UL,0x16E8L}},{{0x1B17L,0UL},{0x1B17L,0x16E8L},{65534UL,0x7DF1L},{0x4CADL,0x97FCL},{0UL,0x97FCL},{0x4CADL,0x7DF1L},{65534UL,0x16E8L},{0x1B17L,0UL},{0x1B17L,0x16E8L}},{{65534UL,0x7DF1L},{0x4CADL,0x97FCL},{0UL,0x97FCL},{0x4CADL,0x7DF1L},{65534UL,0x16E8L},{0x1B17L,0UL},{0x1B17L,0x16E8L},{65534UL,0x7DF1L},{0x4CADL,0x97FCL}},{{0UL,0x97FCL},{0x4CADL,0x7DF1L},{65534UL,0x16E8L},{0x1B17L,0UL},{0x1B17L,0x16E8L},{65534UL,0x7DF1L},{0x4CADL,0x97FCL},{0UL,0x97FCL},{0x4CADL,0x7DF1L}}};
            uint32_t l_1130 = 2UL;
            int i, j, k;
            for (g_119 = 1; (g_119 >= 0); g_119 -= 1)
            { /* block id: 524 */
                int i, j;
                return l_217[(g_155 + 1)][(g_728 + 1)];
            }
            if (l_1129[0][3][1])
                break;
            if ((*p_73))
                break;
            l_1130++;
        }
    }
    return p_72;
}


/* ------------------------------------------ */
/* 
 * reads : g_23 g_149 g_129
 * writes: g_149
 */
static int64_t  func_92(int16_t  p_93, uint8_t * p_94, int32_t * p_95, uint8_t  p_96, int16_t  p_97)
{ /* block id: 51 */
    int32_t *l_148[5];
    int i;
    for (i = 0; i < 5; i++)
        l_148[i] = &g_149;
    g_149 |= g_23;
    g_149 = 0xF28406A8L;
    return g_129;
}


/* ------------------------------------------ */
/* 
 * reads : g_83 g_23 g_47 g_129 g_143 g_119
 * writes: g_129 g_23 g_143
 */
static int16_t  func_102(const int16_t * p_103, uint16_t * p_104, uint32_t * p_105, uint8_t  p_106, uint32_t  p_107)
{ /* block id: 37 */
    int16_t *l_118 = &g_119;
    int32_t l_122 = 5L;
    int32_t l_127 = 1L;
    if ((((safe_rshift_func_uint8_t_u_s(((safe_add_func_int32_t_s_s((g_83[2] < g_23), ((safe_sub_func_uint8_t_u_u((0UL & 255UL), (g_83[0] & ((safe_sub_func_uint32_t_u_u(((l_118 != l_118) == p_106), (safe_mul_func_int32_t_s_s(l_122, g_23)))) , g_47)))) | l_122))) >= 0x860F42E9L), 1)) ^ 0xF106BFD3L) >= p_106))
    { /* block id: 38 */
        int16_t l_126 = 0x5D78L;
        int32_t l_142 = 0x99F59CAFL;
        for (l_122 = 7; (l_122 < (-22)); l_122 = safe_sub_func_uint64_t_u_u(l_122, 4))
        { /* block id: 41 */
            int32_t *l_125[8][5][5] = {{{&g_3,(void*)0,&g_3,&g_3,(void*)0},{(void*)0,(void*)0,&g_3,&g_17,&g_4},{&g_23,&g_17,&g_3,&g_17,(void*)0},{(void*)0,(void*)0,&g_17,(void*)0,&g_17},{&g_23,&g_3,&g_4,&g_3,&g_4}},{{(void*)0,&g_23,&g_17,&g_23,(void*)0},{&g_3,&g_23,&g_4,&g_3,&g_3},{&l_122,&g_23,&g_4,&g_3,&g_17},{&g_17,&g_17,&g_17,&g_17,&g_17},{&g_3,&g_17,&g_4,&g_3,&g_23}},{{&g_3,&g_3,&g_17,&g_4,&g_3},{&g_17,&g_23,&g_3,&g_17,&g_23},{&g_4,&g_4,&g_3,&g_23,&g_17},{&g_23,&g_17,&g_3,&g_4,&g_17},{(void*)0,&g_17,&g_3,&g_17,&g_3}},{{&l_122,&g_17,&g_3,&g_4,(void*)0},{&g_3,&g_17,&g_17,&g_3,&g_4},{&g_23,&g_4,&g_4,&g_3,&g_17},{&g_17,&g_23,&g_3,&g_3,(void*)0},{&g_23,&g_4,&g_4,&g_23,&g_17}},{{&g_3,(void*)0,&g_23,&g_3,(void*)0},{&g_17,&l_122,(void*)0,&g_4,&g_3},{&g_23,&g_17,&g_17,&g_3,&g_4},{&g_23,&g_4,&g_3,&g_17,&g_17},{&g_17,&g_3,&g_17,&g_17,&g_4}},{{&g_3,(void*)0,&g_4,(void*)0,&g_17},{&g_4,(void*)0,(void*)0,&g_17,(void*)0},{&g_23,&g_4,&g_4,&g_17,&g_3},{&g_3,&g_4,&g_17,&l_122,(void*)0},{&g_3,(void*)0,&g_3,&g_3,&l_122}},{{&g_17,&g_17,&g_17,(void*)0,&l_122},{(void*)0,&g_17,(void*)0,&g_4,(void*)0},{&g_4,&g_4,&g_23,&g_23,&g_3},{&g_3,(void*)0,&g_4,&g_3,(void*)0},{&g_3,&g_4,(void*)0,&l_122,&g_17}},{{&g_23,(void*)0,(void*)0,&g_3,&g_4},{(void*)0,&g_4,&g_23,(void*)0,&g_17},{&g_3,&g_17,&g_17,(void*)0,&g_4},{&g_3,&g_17,&g_17,(void*)0,&g_3},{&g_4,(void*)0,(void*)0,(void*)0,(void*)0}}};
            int i, j, k;
            ++g_129;
            g_23 |= ((safe_add_func_int32_t_s_s((safe_div_func_uint8_t_u_u(l_126, (safe_sub_func_int16_t_s_s((safe_lshift_func_int32_t_s_u((g_47 , p_107), ((((g_83[2] | (safe_div_func_uint8_t_u_u(p_106, 0x9DL))) >= l_126) | (p_103 != (void*)0)) , 0xA879AF5BL))), g_129)))), p_106)) != 18446744073709551606UL);
            g_143[5][2]++;
        }
    }
    else
    { /* block id: 46 */
        return g_119;
    }
    return g_143[4][0];
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_4, "g_4", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    transparent_crc(g_14, "g_14", print_hash_value);
    transparent_crc(g_15, "g_15", print_hash_value);
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_17, "g_17", print_hash_value);
    transparent_crc(g_20, "g_20", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_21[i], "g_21[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_22, "g_22", print_hash_value);
    transparent_crc(g_23, "g_23", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_51, "g_51", print_hash_value);
    transparent_crc(g_54, "g_54", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_83[i], "g_83[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_91, "g_91", print_hash_value);
    transparent_crc(g_100, "g_100", print_hash_value);
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc(g_128, "g_128", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_143[i][j], "g_143[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_149, "g_149", print_hash_value);
    transparent_crc(g_151, "g_151", print_hash_value);
    transparent_crc(g_152, "g_152", print_hash_value);
    transparent_crc(g_155, "g_155", print_hash_value);
    transparent_crc(g_203, "g_203", print_hash_value);
    transparent_crc(g_205, "g_205", print_hash_value);
    transparent_crc(g_218, "g_218", print_hash_value);
    transparent_crc(g_261, "g_261", print_hash_value);
    transparent_crc(g_355, "g_355", print_hash_value);
    transparent_crc(g_356, "g_356", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_445[i], "g_445[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_478[i][j][k], "g_478[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_490, "g_490", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_641[i][j][k], "g_641[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_714, "g_714", print_hash_value);
    transparent_crc(g_728, "g_728", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_730[i][j], "g_730[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_902, "g_902", print_hash_value);
    transparent_crc(g_990, "g_990", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_1035[i][j], "g_1035[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1148, "g_1148", print_hash_value);
    transparent_crc(g_1370, "g_1370", print_hash_value);
    transparent_crc(g_1404, "g_1404", print_hash_value);
    transparent_crc(g_1457, "g_1457", print_hash_value);
    transparent_crc(g_1507, "g_1507", print_hash_value);
    transparent_crc(g_1618, "g_1618", print_hash_value);
    transparent_crc(g_1648, "g_1648", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1695[i][j], "g_1695[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1707, "g_1707", print_hash_value);
    transparent_crc(g_1710, "g_1710", print_hash_value);
    transparent_crc(g_1762, "g_1762", print_hash_value);
    transparent_crc(g_1799, "g_1799", print_hash_value);
    transparent_crc(g_1903, "g_1903", print_hash_value);
    transparent_crc(g_1905, "g_1905", print_hash_value);
    transparent_crc(g_1921, "g_1921", print_hash_value);
    transparent_crc(g_1931, "g_1931", print_hash_value);
    transparent_crc(g_2000, "g_2000", print_hash_value);
    transparent_crc(g_2126, "g_2126", print_hash_value);
    transparent_crc(g_2138, "g_2138", print_hash_value);
    transparent_crc(g_2169, "g_2169", print_hash_value);
    transparent_crc(g_2261, "g_2261", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_2318[i][j][k], "g_2318[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2340, "g_2340", print_hash_value);
    transparent_crc(g_2531, "g_2531", print_hash_value);
    transparent_crc(g_2570, "g_2570", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2595[i][j], "g_2595[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2714, "g_2714", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_2827[i][j][k], "g_2827[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2885, "g_2885", print_hash_value);
    transparent_crc(g_3001, "g_3001", print_hash_value);
    transparent_crc(g_3336, "g_3336", print_hash_value);
    transparent_crc(g_3350, "g_3350", print_hash_value);
    transparent_crc(g_3374, "g_3374", print_hash_value);
    transparent_crc(g_3381, "g_3381", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_3449[i], "g_3449[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 837
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 48
breakdown:
   depth: 1, occurrence: 415
   depth: 2, occurrence: 126
   depth: 3, occurrence: 14
   depth: 4, occurrence: 9
   depth: 5, occurrence: 5
   depth: 6, occurrence: 2
   depth: 7, occurrence: 1
   depth: 8, occurrence: 1
   depth: 9, occurrence: 2
   depth: 11, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 4
   depth: 15, occurrence: 4
   depth: 16, occurrence: 4
   depth: 17, occurrence: 4
   depth: 18, occurrence: 8
   depth: 19, occurrence: 5
   depth: 20, occurrence: 5
   depth: 22, occurrence: 6
   depth: 23, occurrence: 7
   depth: 24, occurrence: 8
   depth: 25, occurrence: 5
   depth: 26, occurrence: 5
   depth: 27, occurrence: 2
   depth: 28, occurrence: 6
   depth: 29, occurrence: 3
   depth: 30, occurrence: 6
   depth: 31, occurrence: 1
   depth: 32, occurrence: 4
   depth: 33, occurrence: 2
   depth: 34, occurrence: 1
   depth: 37, occurrence: 2
   depth: 39, occurrence: 1
   depth: 40, occurrence: 2
   depth: 46, occurrence: 1
   depth: 48, occurrence: 1

XXX total number of pointers: 663

XXX times a variable address is taken: 1579
XXX times a pointer is dereferenced on RHS: 412
breakdown:
   depth: 1, occurrence: 284
   depth: 2, occurrence: 95
   depth: 3, occurrence: 23
   depth: 4, occurrence: 9
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 347
breakdown:
   depth: 1, occurrence: 299
   depth: 2, occurrence: 33
   depth: 3, occurrence: 14
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 72
XXX times a pointer is compared with address of another variable: 27
XXX times a pointer is compared with another pointer: 28
XXX times a pointer is qualified to be dereferenced: 11231

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1452
   level: 2, occurrence: 512
   level: 3, occurrence: 410
   level: 4, occurrence: 202
   level: 5, occurrence: 50
XXX number of pointers point to pointers: 302
XXX number of pointers point to scalars: 361
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 32.4
XXX average alias set size: 1.47

XXX times a non-volatile is read: 2863
XXX times a non-volatile is write: 1268
XXX times a volatile is read: 127
XXX    times read thru a pointer: 69
XXX times a volatile is write: 34
XXX    times written thru a pointer: 1
XXX times a volatile is available for access: 1.79e+03
XXX percentage of non-volatile access: 96.2

XXX forward jumps: 2
XXX backward jumps: 11

XXX stmts: 447
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 47
   depth: 2, occurrence: 65
   depth: 3, occurrence: 66
   depth: 4, occurrence: 100
   depth: 5, occurrence: 136

XXX percentage a fresh-made variable is used: 15.4
XXX percentage an existing variable is used: 84.6
XXX total OOB instances added: 0
********************* end of statistics **********************/


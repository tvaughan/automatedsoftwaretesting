/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.4.0
 * Git version: 92069e4
 * Options:   (none)
 * Seed:      4140884741
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   unsigned f0 : 1;
   const volatile signed f1 : 20;
   volatile unsigned f2 : 7;
   uint32_t  f3;
   unsigned f4 : 15;
   volatile unsigned f5 : 1;
};

union U1 {
   const int64_t  f0;
   const uint8_t  f1;
   volatile int32_t  f2;
   const uint32_t  f3;
   volatile int32_t  f4;
};

union U2 {
   unsigned f0 : 13;
   volatile int64_t  f1;
   uint8_t  f2;
   volatile uint16_t  f3;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 0x86BD085CL;
static int32_t g_5 = (-1L);
static int32_t g_51 = 0L;
static int32_t *g_76 = (void*)0;
static uint8_t g_78 = 255UL;
static uint32_t g_95 = 18446744073709551614UL;
static int32_t *g_101[10] = {&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2};
static int32_t ** const g_100 = &g_101[2];
static int8_t g_105 = 0x5FL;
static struct S0 g_131 = {0,-39,3,0xC78F562DL,91,0};/* VOLATILE GLOBAL g_131 */
static union U2 g_135 = {4294967286UL};/* VOLATILE GLOBAL g_135 */
static union U2 * const  volatile g_134[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static union U2 *g_137 = &g_135;
static union U2 * volatile * volatile g_136 = &g_137;/* VOLATILE GLOBAL g_136 */
static const union U2 g_141[3][9][6] = {{{{4294967295UL},{0UL},{0xBC179A00L},{0x5F59DF16L},{0UL},{0x29BBF80EL}},{{0xFFD37754L},{0x29BBF80EL},{0xBC179A00L},{4294967295UL},{0x7E61D7D1L},{0x7E61D7D1L}},{{0x07974F8CL},{0x29BBF80EL},{0x29BBF80EL},{0x07974F8CL},{0UL},{4294967291UL}},{{0x07974F8CL},{0UL},{4294967291UL},{4294967295UL},{0x29BBF80EL},{4294967291UL}},{{0xFFD37754L},{0x7E61D7D1L},{4294967295UL},{0UL},{4294967295UL},{4294967295UL}},{{4294967291UL},{0x5FA6A51FL},{4294967289UL},{0UL},{0x5FA6A51FL},{4294967295UL}},{{0x29BBF80EL},{4294967295UL},{4294967289UL},{4294967291UL},{4294967295UL},{4294967295UL}},{{0xBC179A00L},{4294967295UL},{4294967295UL},{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L}},{{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L},{4294967291UL},{4294967295UL},{0xB49123F8L}}},{{{0x29BBF80EL},{4294967295UL},{4294967295UL},{0UL},{4294967295UL},{4294967295UL}},{{4294967291UL},{0x5FA6A51FL},{4294967289UL},{0UL},{0x5FA6A51FL},{4294967295UL}},{{0x29BBF80EL},{4294967295UL},{4294967289UL},{4294967291UL},{4294967295UL},{4294967295UL}},{{0xBC179A00L},{4294967295UL},{4294967295UL},{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L}},{{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L},{4294967291UL},{4294967295UL},{0xB49123F8L}},{{0x29BBF80EL},{4294967295UL},{4294967295UL},{0UL},{4294967295UL},{4294967295UL}},{{4294967291UL},{0x5FA6A51FL},{4294967289UL},{0UL},{0x5FA6A51FL},{4294967295UL}},{{0x29BBF80EL},{4294967295UL},{4294967289UL},{4294967291UL},{4294967295UL},{4294967295UL}},{{0xBC179A00L},{4294967295UL},{4294967295UL},{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L}}},{{{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L},{4294967291UL},{4294967295UL},{0xB49123F8L}},{{0x29BBF80EL},{4294967295UL},{4294967295UL},{0UL},{4294967295UL},{4294967295UL}},{{4294967291UL},{0x5FA6A51FL},{4294967289UL},{0UL},{0x5FA6A51FL},{4294967295UL}},{{0x29BBF80EL},{4294967295UL},{4294967289UL},{4294967291UL},{4294967295UL},{4294967295UL}},{{0xBC179A00L},{4294967295UL},{4294967295UL},{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L}},{{0xBC179A00L},{0x5FA6A51FL},{0xB49123F8L},{4294967291UL},{4294967295UL},{0xB49123F8L}},{{0x29BBF80EL},{4294967295UL},{4294967295UL},{0UL},{4294967295UL},{4294967295UL}},{{4294967291UL},{0x5FA6A51FL},{4294967289UL},{0UL},{0x5FA6A51FL},{4294967295UL}},{{0x29BBF80EL},{4294967295UL},{4294967289UL},{4294967291UL},{4294967295UL},{4294967295UL}}}};
static volatile union U2 g_160 = {4294967287UL};/* VOLATILE GLOBAL g_160 */
static uint32_t g_179 = 4294967288UL;
static union U1 g_186 = {-10L};/* VOLATILE GLOBAL g_186 */
static union U1 *g_185[6] = {&g_186,&g_186,&g_186,&g_186,&g_186,&g_186};
static union U1 *g_189[6] = {&g_186,&g_186,&g_186,&g_186,&g_186,&g_186};
static union U1 ** volatile g_188[4] = {&g_189[4],&g_189[4],&g_189[4],&g_189[4]};
static union U1 g_191 = {0x0DC9CCB98C823E52LL};/* VOLATILE GLOBAL g_191 */
static uint32_t *g_217 = &g_179;
static uint64_t g_282 = 0UL;
static int64_t g_288[4][9] = {{(-8L),0x8B85D11EB6188040LL,0x4190088FAD8E1CA1LL,0x5E4DA2ECC26A7BBDLL,0x6784C590CC90E365LL,(-1L),0x6784C590CC90E365LL,0x5E4DA2ECC26A7BBDLL,0x4190088FAD8E1CA1LL},{0xC59ECB9753820251LL,0xC59ECB9753820251LL,(-5L),1L,(-8L),0x8B85D11EB6188040LL,0x4190088FAD8E1CA1LL,0x5E4DA2ECC26A7BBDLL,0x6784C590CC90E365LL},{2L,0x31240E19525CCEA2LL,(-1L),0xC59ECB9753820251LL,7L,7L,0xC59ECB9753820251LL,(-1L),0x31240E19525CCEA2LL},{7L,0L,(-5L),0xFC61834C53088611LL,(-7L),1L,0xC59ECB9753820251LL,0x4190088FAD8E1CA1LL,0xED3EB0AC4D877492LL}};
static uint32_t * const g_312 = &g_179;
static uint64_t g_350 = 1UL;
static int32_t **g_406[4][1] = {{&g_101[4]},{&g_101[4]},{&g_101[4]},{&g_101[4]}};
static int32_t ***g_405 = &g_406[0][0];
static volatile uint32_t g_440 = 0xE91322AAL;/* VOLATILE GLOBAL g_440 */
static volatile int8_t g_467 = 0x85L;/* VOLATILE GLOBAL g_467 */
static volatile int8_t * volatile g_466 = &g_467;/* VOLATILE GLOBAL g_466 */
static volatile int8_t * volatile *g_465 = &g_466;
static uint16_t g_475 = 0x3659L;
static uint16_t *g_474 = &g_475;
static int8_t *g_494 = &g_105;
static int8_t **g_493[10][9] = {{(void*)0,&g_494,&g_494,&g_494,(void*)0,&g_494,&g_494,(void*)0,&g_494},{&g_494,(void*)0,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494,&g_494,(void*)0,&g_494,&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494,&g_494,(void*)0,&g_494,&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494,(void*)0,&g_494,&g_494,&g_494,(void*)0,&g_494},{&g_494,&g_494,&g_494,&g_494,(void*)0,&g_494,&g_494,&g_494,&g_494},{&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494},{&g_494,(void*)0,&g_494,&g_494,&g_494,&g_494,&g_494,&g_494,(void*)0},{(void*)0,&g_494,&g_494,&g_494,(void*)0,&g_494,&g_494,&g_494,(void*)0}};
static int8_t ***g_492 = &g_493[3][1];
static int8_t **** volatile g_491 = &g_492;/* VOLATILE GLOBAL g_491 */
static union U1 ** volatile * const g_537 = (void*)0;
static union U1 ** volatile * const *g_536 = &g_537;
static volatile struct S0 g_541 = {0,722,3,18446744073709551607UL,73,0};/* VOLATILE GLOBAL g_541 */
static uint8_t g_563 = 0xCFL;
static volatile union U1 g_569[1] = {{-3L}};
static const volatile struct S0 g_575 = {0,400,3,0x50C76F8EL,1,0};/* VOLATILE GLOBAL g_575 */
static struct S0 ** volatile g_577 = (void*)0;/* VOLATILE GLOBAL g_577 */
static const struct S0 **g_585 = (void*)0;
static const struct S0 g_589 = {0,67,6,0xF7F26F6AL,95,0};/* VOLATILE GLOBAL g_589 */
static uint16_t **g_606[3] = {&g_474,&g_474,&g_474};
static const uint16_t g_612 = 1UL;
static int32_t * volatile g_623 = &g_51;/* VOLATILE GLOBAL g_623 */
static volatile struct S0 g_645 = {0,222,8,0xFA684FB2L,64,0};/* VOLATILE GLOBAL g_645 */
static union U2 g_697 = {0xFAB40475L};/* VOLATILE GLOBAL g_697 */
static union U2 g_699 = {0xF0A214A7L};/* VOLATILE GLOBAL g_699 */
static union U2 g_700 = {0x9E32046CL};/* VOLATILE GLOBAL g_700 */
static volatile union U2 *g_726 = (void*)0;
static volatile union U2 * volatile *g_725 = &g_726;
static volatile union U2 * volatile **g_724 = &g_725;
static volatile union U2 * volatile *** volatile g_723 = &g_724;/* VOLATILE GLOBAL g_723 */
static uint64_t g_771 = 0x3708A0ADDFEFD538LL;
static volatile struct S0 g_793 = {0,-92,2,0xD8D8B709L,142,0};/* VOLATILE GLOBAL g_793 */
static volatile uint64_t * volatile *g_799 = (void*)0;
static union U2 g_871 = {0x5B10A549L};/* VOLATILE GLOBAL g_871 */
static volatile union U2 g_872 = {0x20DED892L};/* VOLATILE GLOBAL g_872 */
static uint64_t *g_875 = &g_350;
static uint64_t **g_874[9] = {&g_875,&g_875,&g_875,&g_875,&g_875,&g_875,&g_875,&g_875,&g_875};
static uint64_t ***g_873 = &g_874[2];
static volatile uint32_t g_886[9][10] = {{0x5DD52F3AL,5UL,6UL,0UL,6UL,5UL,0x5DD52F3AL,5UL,6UL,0UL},{1UL,0UL,1UL,5UL,0xCA83207FL,5UL,1UL,0UL,1UL,5UL},{0x5DD52F3AL,0UL,18446744073709551612UL,0UL,0x5DD52F3AL,18446744073709551612UL,0x5DD52F3AL,0UL,18446744073709551612UL,0UL},{0xCA83207FL,5UL,1UL,0UL,1UL,5UL,0xCA83207FL,5UL,1UL,0UL},{6UL,0UL,6UL,5UL,0x5DD52F3AL,5UL,6UL,0UL,6UL,5UL},{0xCA83207FL,0UL,8UL,0UL,0xCA83207FL,18446744073709551612UL,0xCA83207FL,0UL,8UL,0UL},{0x5DD52F3AL,5UL,6UL,0UL,6UL,5UL,0x5DD52F3AL,5UL,6UL,0UL},{1UL,0UL,1UL,5UL,0xCA83207FL,5UL,1UL,0UL,1UL,5UL},{0x5DD52F3AL,0UL,18446744073709551612UL,0UL,0x5DD52F3AL,18446744073709551612UL,6UL,5UL,0x5DD52F3AL,5UL}};
static union U2 **g_896 = (void*)0;
static union U2 ***g_895 = &g_896;
static union U2 ****g_894 = &g_895;
static union U2 g_905 = {0xF4F593ADL};/* VOLATILE GLOBAL g_905 */
static volatile union U1 g_919 = {-5L};/* VOLATILE GLOBAL g_919 */
static union U2 g_923 = {0UL};/* VOLATILE GLOBAL g_923 */
static const int16_t g_934 = (-1L);
static uint32_t g_940 = 0x03D51C13L;
static volatile union U2 g_985 = {0x76F562EEL};/* VOLATILE GLOBAL g_985 */
static struct S0 g_1011 = {0,-913,6,0x15B35985L,90,0};/* VOLATILE GLOBAL g_1011 */
static union U1 g_1014 = {0x46326F72286C7051LL};/* VOLATILE GLOBAL g_1014 */
static uint8_t *g_1068 = (void*)0;
static uint8_t **g_1067 = &g_1068;
static int32_t ** volatile g_1103[5] = {&g_76,&g_76,&g_76,&g_76,&g_76};
static union U1 g_1125 = {0L};/* VOLATILE GLOBAL g_1125 */
static union U2 g_1139 = {0xF518CB93L};/* VOLATILE GLOBAL g_1139 */
static uint32_t **g_1154 = &g_217;
static uint32_t ***g_1153 = &g_1154;
static uint32_t **** volatile g_1152 = &g_1153;/* VOLATILE GLOBAL g_1152 */
static union U2 g_1176 = {0UL};/* VOLATILE GLOBAL g_1176 */
static volatile struct S0 g_1180[1] = {{0,903,0,18446744073709551606UL,152,0}};
static uint32_t g_1186 = 9UL;
static int32_t g_1198 = (-1L);
static union U2 g_1227 = {0x7CF8F904L};/* VOLATILE GLOBAL g_1227 */
static union U1 g_1235 = {5L};/* VOLATILE GLOBAL g_1235 */
static union U2 g_1246 = {0x68BE9178L};/* VOLATILE GLOBAL g_1246 */
static struct S0 *g_1258 = &g_131;
static struct S0 ** volatile g_1257 = &g_1258;/* VOLATILE GLOBAL g_1257 */
static const uint32_t g_1264 = 0xD6C24996L;
static const uint32_t *g_1263 = &g_1264;
static const uint32_t **g_1262 = &g_1263;
static const uint32_t **g_1265 = (void*)0;
static volatile int8_t g_1275 = 0xB4L;/* VOLATILE GLOBAL g_1275 */
static volatile int32_t g_1277 = 0L;/* VOLATILE GLOBAL g_1277 */
static uint32_t g_1278 = 0x43AACCE0L;
static const int32_t *g_1282 = &g_51;
static uint64_t ***g_1322 = &g_874[2];
static volatile union U2 g_1376[5][2] = {{{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL}},{{4294967295UL},{4294967295UL}}};
static volatile union U1 g_1377 = {0L};/* VOLATILE GLOBAL g_1377 */
static const volatile union U2 g_1396 = {0x26FA9D4DL};/* VOLATILE GLOBAL g_1396 */
static union U1 g_1487 = {0xE72C3350821306F1LL};/* VOLATILE GLOBAL g_1487 */
static volatile struct S0 g_1517 = {0,238,8,0x8BA7FDE5L,59,0};/* VOLATILE GLOBAL g_1517 */
static const union U2 g_1529 = {0xBDBED9FDL};/* VOLATILE GLOBAL g_1529 */
static int32_t g_1532 = 0x5722A66AL;
static volatile union U2 g_1583[9] = {{2UL},{2UL},{2UL},{2UL},{2UL},{2UL},{2UL},{2UL},{2UL}};
static volatile union U1 g_1594[8] = {{0xEBBBEBD294DBCA1CLL},{0xEBBBEBD294DBCA1CLL},{0xEBBBEBD294DBCA1CLL},{0xEBBBEBD294DBCA1CLL},{0xEBBBEBD294DBCA1CLL},{0xEBBBEBD294DBCA1CLL},{0xEBBBEBD294DBCA1CLL},{0xEBBBEBD294DBCA1CLL}};
static volatile union U1 g_1597 = {1L};/* VOLATILE GLOBAL g_1597 */
static union U1 g_1603 = {9L};/* VOLATILE GLOBAL g_1603 */
static volatile union U2 g_1712 = {4294967288UL};/* VOLATILE GLOBAL g_1712 */
static union U1 g_1751 = {-1L};/* VOLATILE GLOBAL g_1751 */
static union U1 g_1803 = {1L};/* VOLATILE GLOBAL g_1803 */
static int32_t * volatile g_1805 = (void*)0;/* VOLATILE GLOBAL g_1805 */
static int32_t * const  volatile g_1807 = &g_51;/* VOLATILE GLOBAL g_1807 */
static volatile union U2 g_1822[7][5][5] = {{{{1UL},{0xA86C274BL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{4294967295UL},{6UL},{6UL}},{{0x8D6D5488L},{0x318039E4L},{1UL},{1UL},{0x318039E4L}},{{6UL},{0UL},{1UL},{6UL},{4294967288UL}},{{0x4D592E0DL},{0x318039E4L},{0UL},{0x318039E4L},{0x4D592E0DL}}},{{{1UL},{1UL},{0UL},{4294967288UL},{1UL}},{{0x4D592E0DL},{0xA86C274BL},{0xA86C274BL},{0x4D592E0DL},{1UL}},{{6UL},{6UL},{4294967295UL},{1UL},{1UL}},{{0x8D6D5488L},{0x4D592E0DL},{0x8D6D5488L},{1UL},{0x4D592E0DL}},{{1UL},{0UL},{4294967288UL},{1UL},{4294967288UL}}},{{{1UL},{1UL},{0UL},{0x4D592E0DL},{0x318039E4L}},{{1UL},{6UL},{4294967288UL},{4294967288UL},{6UL}},{{0x318039E4L},{0xA86C274BL},{0x8D6D5488L},{0x318039E4L},{1UL}},{{6UL},{6UL},{4294967295UL},{6UL},{6UL}},{{0x8D6D5488L},{1UL},{0xA86C274BL},{1UL},{1UL}}},{{{6UL},{0UL},{0UL},{6UL},{4294967288UL}},{{0x318039E4L},{0x4D592E0DL},{0UL},{1UL},{1UL}},{{1UL},{6UL},{1UL},{4294967288UL},{6UL}},{{1UL},{0xA86C274BL},{1UL},{1UL},{1UL}},{{1UL},{1UL},{4294967295UL},{6UL},{6UL}}},{{{0x8D6D5488L},{0x318039E4L},{1UL},{1UL},{0x318039E4L}},{{6UL},{0UL},{1UL},{6UL},{4294967288UL}},{{0x4D592E0DL},{0x318039E4L},{0UL},{0x318039E4L},{0x4D592E0DL}},{{1UL},{1UL},{0UL},{4294967288UL},{1UL}},{{0x4D592E0DL},{0xA86C274BL},{0xA86C274BL},{0x4D592E0DL},{1UL}}},{{{6UL},{6UL},{4294967295UL},{1UL},{1UL}},{{0x8D6D5488L},{0x4D592E0DL},{0x8D6D5488L},{1UL},{0x4D592E0DL}},{{1UL},{0UL},{4294967288UL},{1UL},{4294967288UL}},{{1UL},{1UL},{0UL},{0x4D592E0DL},{0x318039E4L}},{{1UL},{6UL},{4294967288UL},{4294967288UL},{6UL}}},{{{0x318039E4L},{0xA86C274BL},{0x8D6D5488L},{0x318039E4L},{0xC3D92AFAL}},{{0UL},{4294967288UL},{1UL},{4294967288UL},{0UL}},{{4294967295UL},{0x8D6D5488L},{0UL},{0xC3D92AFAL},{0x8D6D5488L}},{{0UL},{4294967295UL},{4294967295UL},{0UL},{0x7FCA54CAL}},{{1UL},{0xA86C274BL},{1UL},{0x8D6D5488L},{0x8D6D5488L}}}};
static int32_t * volatile g_1878 = &g_51;/* VOLATILE GLOBAL g_1878 */
static union U2 g_1934 = {0x2C8411DDL};/* VOLATILE GLOBAL g_1934 */
static int32_t ***** volatile g_1957 = (void*)0;/* VOLATILE GLOBAL g_1957 */
static int32_t ****g_1960 = &g_405;
static int32_t ***** volatile g_1959 = &g_1960;/* VOLATILE GLOBAL g_1959 */
static struct S0 ** const  volatile g_1962 = &g_1258;/* VOLATILE GLOBAL g_1962 */
static const uint32_t ***g_1990[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static const uint32_t ****g_1989 = &g_1990[3];
static int8_t g_2072 = 0x2DL;
static volatile union U2 g_2094 = {0xCC379967L};/* VOLATILE GLOBAL g_2094 */
static const int32_t **g_2107 = (void*)0;
static const int32_t *** const g_2106 = &g_2107;
static const int32_t *** const *g_2105[3][10] = {{(void*)0,&g_2106,(void*)0,(void*)0,&g_2106,(void*)0,&g_2106,(void*)0,(void*)0,&g_2106},{(void*)0,&g_2106,(void*)0,(void*)0,&g_2106,(void*)0,&g_2106,(void*)0,(void*)0,&g_2106},{(void*)0,&g_2106,(void*)0,(void*)0,&g_2106,(void*)0,&g_2106,(void*)0,(void*)0,&g_2106}};
static const int32_t *** const **g_2104[8] = {&g_2105[2][3],&g_2105[2][3],&g_2105[2][3],&g_2105[2][3],&g_2105[2][3],&g_2105[2][3],&g_2105[2][3],&g_2105[2][3]};


/* --- FORWARD DECLARATIONS --- */
static const int8_t  func_1(void);
static union U1  func_11(int32_t  p_12);
static int16_t  func_20(uint32_t  p_21);
static int32_t  func_24(uint16_t  p_25, uint32_t  p_26, uint8_t  p_27, uint32_t  p_28);
static int16_t  func_38(int32_t  p_39, uint32_t  p_40, int64_t  p_41, int32_t  p_42);
static uint8_t  func_44(int16_t  p_45, uint32_t  p_46, int64_t  p_47);
static union U2  func_55(uint32_t  p_56, int32_t  p_57, int32_t * const  p_58);
static uint64_t  func_59(uint32_t  p_60, uint16_t  p_61, int32_t  p_62);
static int32_t * func_71(int32_t * p_72);
static int8_t  func_91(uint64_t  p_92, int32_t * p_93);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_5 g_1532 g_494 g_105
 * writes: g_2 g_5 g_1532
 */
static const int8_t  func_1(void)
{ /* block id: 0 */
    const int8_t l_10 = 0xE2L;
    uint64_t l_19[1];
    union U2 ***l_1783 = &g_896;
    const uint32_t l_1784 = 0x438CAD06L;
    const uint64_t l_1823 = 18446744073709551615UL;
    const int16_t l_1833 = 0L;
    int32_t l_1836 = 0x6EC4F6E4L;
    int64_t l_1857 = 3L;
    uint64_t l_1893[7][8] = {{0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL,0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL},{0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL,0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL},{0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL,0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL},{0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL,0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL},{0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL,0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL},{0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL,0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL},{0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL,0xF10154BAC1B039DDLL,18446744073709551608UL,6UL,18446744073709551608UL}};
    int16_t l_1904 = (-10L);
    union U1 **l_1923 = &g_189[0];
    union U1 ** const * const l_1922 = &l_1923;
    uint16_t *l_1952 = &g_475;
    int32_t l_1972[8] = {0xE6B316DCL,0xE6B316DCL,0x4841AE78L,0xE6B316DCL,0xE6B316DCL,0x4841AE78L,0xE6B316DCL,0xE6B316DCL};
    uint32_t l_1973 = 1UL;
    uint64_t l_2025[9] = {18446744073709551615UL,0x49BAFDF464A64677LL,18446744073709551615UL,18446744073709551615UL,0x49BAFDF464A64677LL,18446744073709551615UL,18446744073709551615UL,0x49BAFDF464A64677LL,18446744073709551615UL};
    uint32_t l_2028 = 0UL;
    int i, j;
    for (i = 0; i < 1; i++)
        l_19[i] = 1UL;
    for (g_2 = 20; (g_2 <= (-17)); g_2--)
    { /* block id: 3 */
        union U2 ***l_1782 = &g_896;
        int32_t l_1804[8] = {0x3827394FL,0x3827394FL,0x3827394FL,0x3827394FL,0x3827394FL,0x3827394FL,0x3827394FL,0x3827394FL};
        int32_t l_1827 = 0L;
        const int32_t l_1834 = 0xB4F3E437L;
        int8_t ** const *l_1855 = &g_493[8][1];
        int8_t ** const **l_1854 = &l_1855;
        uint16_t l_1877 = 2UL;
        int8_t l_1976 = 0x27L;
        const uint8_t *l_2045 = (void*)0;
        const uint8_t **l_2044 = &l_2045;
        const int32_t ***l_2103 = (void*)0;
        const int32_t *** const *l_2102 = &l_2103;
        const int32_t *** const **l_2101 = &l_2102;
        int32_t *l_2109[6] = {&l_1804[2],(void*)0,(void*)0,&l_1804[2],(void*)0,(void*)0};
        uint64_t l_2110 = 0xEF6C40B6F1AA829DLL;
        int i;
        for (g_5 = 0; (g_5 < 27); g_5++)
        { /* block id: 6 */
            int32_t *l_1806 = (void*)0;
            uint64_t l_1813[9][2] = {{0xBEE5135899D0E04CLL,0xBEE5135899D0E04CLL},{0xBEE5135899D0E04CLL,1UL},{0xBEE5135899D0E04CLL,0xBEE5135899D0E04CLL},{0xBEE5135899D0E04CLL,1UL},{0xBEE5135899D0E04CLL,0xBEE5135899D0E04CLL},{0xBEE5135899D0E04CLL,1UL},{0xBEE5135899D0E04CLL,0xBEE5135899D0E04CLL},{0xBEE5135899D0E04CLL,1UL},{0xBEE5135899D0E04CLL,0xBEE5135899D0E04CLL}};
            uint64_t **l_1828 = (void*)0;
            uint32_t l_1831 = 1UL;
            int16_t l_1832 = (-1L);
            const uint64_t l_1835 = 18446744073709551608UL;
            int i, j;
        }
        l_1836 |= 1L;
        for (g_1532 = (-17); (g_1532 > (-10)); g_1532++)
        { /* block id: 844 */
            uint32_t l_1849 = 18446744073709551606UL;
            int32_t l_1892 = 0x062FF419L;
            int32_t l_1899 = 0x4D848013L;
            int32_t l_1906[1];
            int32_t l_1988 = 0xFEF2D188L;
            struct S0 ** const l_1997 = &g_1258;
            int32_t l_2010 = (-1L);
            uint16_t **l_2090 = &g_474;
            int i;
            for (i = 0; i < 1; i++)
                l_1906[i] = 1L;
            l_1836 = 1L;
        }
        ++l_2110;
    }
    return (*g_494);
}


/* ------------------------------------------ */
/* 
 * reads : g_697.f2 g_1803
 * writes: g_697.f2
 */
static union U1  func_11(int32_t  p_12)
{ /* block id: 819 */
    int32_t l_1789 = (-9L);
    int32_t l_1790 = 1L;
    int32_t l_1791 = 0xEED6508BL;
    int32_t l_1793 = (-1L);
    int32_t l_1794 = 0x03F1B6D9L;
    int32_t l_1795 = (-1L);
    int32_t l_1796 = 0L;
    int32_t l_1797 = 0L;
    int32_t l_1798 = 8L;
    uint64_t l_1799 = 0x726E5E8FAA5000D9LL;
    int32_t *l_1802 = &g_51;
    for (g_697.f2 = 0; (g_697.f2 == 54); g_697.f2 = safe_add_func_uint32_t_u_u(g_697.f2, 9))
    { /* block id: 822 */
        int32_t *l_1787 = &g_51;
        int32_t *l_1788[4] = {&g_51,&g_51,&g_51,&g_51};
        int8_t l_1792 = (-1L);
        int i;
        l_1799++;
    }
    l_1802 = &l_1796;
    return g_1803;
}


/* ------------------------------------------ */
/* 
 * reads : g_697.f0 g_1153 g_1154 g_217 g_179 g_1376 g_1377 g_645.f5 g_465 g_466 g_467 g_288 g_1139.f0 g_474 g_475 g_699.f0 g_405 g_406 g_2 g_623 g_51 g_101 g_100 g_1751 g_873 g_874 g_875 g_350 g_78 g_5 g_95 g_491 g_492 g_493 g_1264 g_141.f0 g_494
 * writes: g_51 g_131.f3 g_179 g_288 g_101 g_350 g_76 g_78 g_95 g_105
 */
static int16_t  func_20(uint32_t  p_21)
{ /* block id: 7 */
    uint32_t l_31 = 5UL;
    int16_t l_1736 = (-1L);
    const union U2 **** const l_1739 = (void*)0;
    int16_t l_1742 = 0xBCB5L;
    uint8_t *l_1752 = &g_78;
    int32_t l_1777 = 0x86B2A1C2L;
    int32_t *l_1778 = &l_1777;
    int64_t l_1779 = 0x857B0F97E6334656LL;
    for (p_21 = (-15); (p_21 != 56); p_21++)
    { /* block id: 10 */
        uint32_t l_43 = 4294967289UL;
        uint32_t *l_1367 = &g_131.f3;
        uint32_t l_1368 = 1UL;
        int32_t l_1753 = (-1L);
        if (func_24((safe_mul_func_int8_t_s_s(p_21, ((((((l_31 < (((safe_lshift_func_int64_t_s_u((safe_rshift_func_int8_t_s_u(l_31, 6)), 58)) | (safe_mul_func_int8_t_s_s(((p_21 , (l_31 || func_38(p_21, ((*l_1367) = (l_43 , (0xBADBEFC7127B1AA4LL && (func_44(l_43, p_21, p_21) <= p_21)))), l_43, l_1368))) >= 0xB2BF49A2L), p_21))) <= p_21)) <= g_1139.f0) | (-1L)) <= (*g_474)) | p_21) <= p_21))), g_699.f0, l_31, p_21))
        { /* block id: 794 */
            uint32_t l_1735 = 0xFF8E9697L;
            (*g_100) = (((8UL && (l_1735 >= l_31)) && (*g_623)) , (**g_405));
        }
        else
        { /* block id: 796 */
            int16_t *l_1737 = &l_1736;
            int16_t **l_1738 = &l_1737;
            int32_t l_1743 = (-7L);
            int32_t *l_1744[1];
            int i;
            for (i = 0; i < 1; i++)
                l_1744[i] = &g_51;
            l_1736 = p_21;
            (*g_623) = ((l_1736 & (((p_21 || (((*l_1738) = l_1737) != (void*)0)) <= (l_1739 != l_1739)) & (l_1743 = (safe_rshift_func_int32_t_s_u(((g_288[0][4] || (4UL || ((0xDFL > 0xE7L) == (-1L)))) || 0xBF1742C0L), l_1742))))) && l_31);
            l_1753 = (safe_div_func_uint16_t_u_u((p_21 >= (l_1368 < ((l_43 , ((***g_873) ^= (safe_mul_func_int16_t_s_s((((g_1751 , 0xC130909BL) != ((p_21 , (l_1752 != l_1752)) | (p_21 >= 0xD702C86950D9A9BFLL))) && 8L), p_21)))) < p_21))), 0xC180L));
        }
        for (g_179 = 0; (g_179 >= 28); g_179 = safe_add_func_uint8_t_u_u(g_179, 4))
        { /* block id: 806 */
            uint32_t **l_1770 = &l_1367;
            uint32_t *l_1772[10] = {&l_1368,&l_1368,&l_1368,&l_1368,&l_1368,&l_1368,&l_1368,&l_1368,&l_1368,&l_1368};
            uint32_t **l_1771 = &l_1772[4];
            int i;
            l_1777 &= (+((safe_mul_func_int8_t_s_s((((((p_21 <= (safe_rshift_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((((safe_mul_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u(((((*l_1771) = ((*l_1770) = func_71(((safe_unary_minus_func_uint32_t_u(l_1742)) , (void*)0)))) == &l_1368) ^ ((void*)0 != (**g_491))), ((safe_rshift_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((((***g_873) ^= l_1742) && p_21), (-1L))), l_1736)) , g_350))) & 0L), l_1753)) == p_21) , p_21), g_1264)), 6))) && l_1742) & 0xB413E79688562D1FLL) | 4L) < l_1753), g_141[2][6][4].f0)) , l_1368));
        }
        return p_21;
    }
    (*l_1778) = (((((*g_494) = l_1777) , &g_934) != (void*)0) ^ l_1736);
    (*l_1778) = l_1779;
    (*l_1778) = (safe_lshift_func_uint16_t_u_u(0x0ACDL, 0));
    return p_21;
}


/* ------------------------------------------ */
/* 
 * reads : g_405 g_406 g_2
 * writes: g_101
 */
static int32_t  func_24(uint16_t  p_25, uint32_t  p_26, uint8_t  p_27, uint32_t  p_28)
{ /* block id: 791 */
    int32_t *l_1734 = &g_2;
    (**g_405) = l_1734;
    return (*l_1734);
}


/* ------------------------------------------ */
/* 
 * reads : g_1153 g_1154 g_217 g_179 g_1376 g_1377 g_645.f5 g_465 g_466 g_467 g_288
 * writes: g_179 g_288
 */
static int16_t  func_38(int32_t  p_39, uint32_t  p_40, int64_t  p_41, int32_t  p_42)
{ /* block id: 626 */
    int16_t l_1371[2][1][5] = {{{(-10L),(-10L),8L,(-10L),(-10L)}},{{0L,(-10L),8L,8L,0L}}};
    int64_t *l_1392 = &g_288[1][5];
    int32_t l_1393 = 0x783640FCL;
    int16_t *l_1394 = (void*)0;
    int32_t l_1395 = 0xB2E9F6B2L;
    int32_t *l_1413 = &l_1395;
    int64_t l_1414 = 6L;
    int32_t *l_1415 = &g_51;
    union U1 ** const *l_1475 = (void*)0;
    union U1 ** const **l_1474 = &l_1475;
    union U1 ** const *** const l_1473 = &l_1474;
    uint32_t l_1497 = 0UL;
    int8_t **l_1518[6];
    int32_t l_1563 = 1L;
    int32_t l_1565 = 5L;
    int32_t l_1566 = 0xAC95C8B1L;
    int32_t l_1570[4];
    union U2 * const l_1613[10][5] = {{&g_1227,&g_699,&g_699,&g_1227,&g_699},{&g_1227,&g_1227,&g_905,&g_1227,&g_1227},{&g_699,&g_1227,&g_699,&g_699,&g_1227},{&g_1227,&g_699,&g_699,&g_1227,&g_699},{&g_1227,&g_1227,&g_905,&g_1227,&g_1227},{&g_699,&g_1227,&g_699,&g_699,&g_1227},{&g_1227,&g_699,&g_699,&g_1227,&g_699},{&g_1227,&g_1227,&g_905,&g_1227,&g_1227},{&g_699,&g_1227,&g_699,&g_699,&g_1227},{&g_1227,&g_699,&g_699,&g_1227,&g_699}};
    const struct S0 *l_1662 = (void*)0;
    const struct S0 **l_1661 = &l_1662;
    int8_t ****l_1688 = &g_492;
    int32_t *l_1690 = &g_2;
    uint64_t l_1693 = 0xD75C7E01D93DAD6ALL;
    uint32_t l_1701[1][1][9] = {{{18446744073709551607UL,18446744073709551607UL,18446744073709551615UL,18446744073709551607UL,18446744073709551607UL,18446744073709551615UL,18446744073709551607UL,18446744073709551607UL,18446744073709551615UL}}};
    uint8_t l_1708 = 0x51L;
    uint64_t ****l_1731[1];
    int i, j, k;
    for (i = 0; i < 6; i++)
        l_1518[i] = &g_494;
    for (i = 0; i < 4; i++)
        l_1570[i] = 0x0A3CA1A9L;
    for (i = 0; i < 1; i++)
        l_1731[i] = (void*)0;
    l_1395 ^= (((((l_1371[0][0][1] = ((--(***g_1153)) != p_41)) , (safe_add_func_uint8_t_u_u((safe_mod_func_int8_t_s_s(((g_1376[1][1] , g_1377) , (safe_add_func_int8_t_s_s((safe_add_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u(l_1371[0][0][1], (((safe_mul_func_int64_t_s_s(((g_645.f5 , (1L && ((safe_mod_func_int64_t_s_s(((void*)0 == &g_723), ((*l_1392) &= (safe_add_func_uint16_t_u_u(((((((safe_sub_func_int64_t_s_s(1L, p_42)) & p_40) == p_42) != (**g_465)) , l_1371[0][0][1]) != 0xA2L), (-6L)))))) , p_40))) == 0x32L), 0xE360503C8A340046LL)) | 5UL) , l_1371[0][0][1]))), 6UL)), l_1371[0][0][1]))), p_39)), 5L))) & l_1393) , &l_1371[0][0][1]) != l_1394);
    return p_42;
}


/* ------------------------------------------ */
/* 
 * reads : g_697.f0
 * writes: g_51
 */
static uint8_t  func_44(int16_t  p_45, uint32_t  p_46, int64_t  p_47)
{ /* block id: 11 */
    int32_t l_52 = 0xCD8A6CE2L;
    const int32_t *l_65 = (void*)0;
    const int32_t l_1247 = 0L;
    uint32_t l_1248 = 0x8586BFF0L;
    union U1 ** const l_1252 = &g_185[4];
    union U1 **l_1254 = &g_185[4];
    union U1 ***l_1253 = &l_1254;
    int32_t l_1273 = (-1L);
    int32_t l_1274[9][7][4] = {{{0xE228AD17L,0x633F7EE2L,0xDABCF6FCL,0xC6CD9CC9L},{0x8F2E2858L,0xEA05B4A1L,0xBC3C5A89L,8L},{0x88E1AC05L,0x87F27819L,2L,(-1L)},{9L,3L,9L,1L},{(-6L),0x1847E929L,1L,0x4883A2DFL},{0xE228AD17L,2L,(-6L),0x1847E929L},{0x1847E929L,0xADFE4090L,(-6L),8L}},{{0xE228AD17L,0x88E1AC05L,1L,(-6L)},{(-6L),0xEA05B4A1L,9L,0x8F2E2858L},{9L,0x8F2E2858L,2L,0L},{0x88E1AC05L,2L,0xBC3C5A89L,1L},{0x8F2E2858L,0x978AB44FL,0xDABCF6FCL,0xDABCF6FCL},{0xE228AD17L,0xE228AD17L,0xC6CD9CC9L,(-1L)},{0L,0xADFE4090L,(-1L),0x8F2E2858L}},{{0x1250EF80L,0x633F7EE2L,1L,(-1L)},{0x87F27819L,0x633F7EE2L,0xBC3C5A89L,0x8F2E2858L},{0x633F7EE2L,0xADFE4090L,0xBD552E8BL,(-1L)},{0x88E1AC05L,0xE228AD17L,0x633F7EE2L,0xDABCF6FCL},{(-6L),0x978AB44FL,0x4883A2DFL,1L},{0x1250EF80L,2L,0xC6CD9CC9L,0L},{(-1L),0x8F2E2858L,(-6L),0x8F2E2858L}},{{2L,0xEA05B4A1L,0xDABCF6FCL,(-6L)},{0x87F27819L,0x88E1AC05L,0x633F7EE2L,8L},{9L,0xADFE4090L,0x1250EF80L,0x1847E929L},{9L,2L,0x633F7EE2L,0x4883A2DFL},{0x87F27819L,0x1847E929L,0xDABCF6FCL,1L},{2L,3L,(-6L),(-1L)},{(-1L),0x87F27819L,0xC6CD9CC9L,8L}},{{0x1250EF80L,0xEA05B4A1L,0x4883A2DFL,0xC6CD9CC9L},{(-6L),0x633F7EE2L,0x633F7EE2L,(-6L)},{0x88E1AC05L,0x8F2E2858L,0xBD552E8BL,0x1847E929L},{0x633F7EE2L,3L,0xBC3C5A89L,0xDABCF6FCL},{0x87F27819L,1L,(-1L),0x0E5BE727L},{7L,(-7L),0xBD552E8BL,0x4883A2DFL},{0xDABCF6FCL,9L,0x1250EF80L,0xBC3C5A89L}},{{0L,0L,0x0E5BE727L,0x1250EF80L},{9L,(-1L),0x978AB44FL,0xEA05B4A1L},{0x1847E929L,0x633F7EE2L,(-1L),(-4L)},{(-1L),(-7L),(-1L),(-1L)},{0xBC3C5A89L,0x4883A2DFL,(-1L),(-6L)},{0L,(-1L),3L,0x4883A2DFL},{0x4883A2DFL,0x88E1AC05L,3L,0xEA05B4A1L}},{{0L,0x1847E929L,(-1L),3L},{0xBC3C5A89L,(-1L),(-1L),9L},{(-1L),9L,(-1L),0xDABCF6FCL},{0x1847E929L,(-1L),0x978AB44FL,(-1L)},{9L,0x1E09E1D2L,0x0E5BE727L,0x0E5BE727L},{0L,0L,0x1250EF80L,(-4L)},{0xDABCF6FCL,0x88E1AC05L,0xBD552E8BL,9L}},{{7L,0L,(-1L),0xBD552E8BL},{0x633F7EE2L,0L,0x978AB44FL,9L},{0L,0x88E1AC05L,0xE48E4FA5L,(-4L)},{0x1847E929L,0L,0L,0x0E5BE727L},{0xBC3C5A89L,0x1E09E1D2L,(-6L),(-1L)},{7L,(-1L),0x1250EF80L,0xDABCF6FCL},{1L,9L,3L,9L}},{{(-1L),(-1L),0x0E5BE727L,3L},{0x633F7EE2L,0x1847E929L,0L,0xEA05B4A1L},{(-1L),0x88E1AC05L,7L,0x4883A2DFL},{(-1L),(-1L),0L,(-6L)},{0x633F7EE2L,0x4883A2DFL,0x0E5BE727L,(-1L)},{(-1L),(-7L),3L,(-4L)},{1L,0x633F7EE2L,0x1250EF80L,0xEA05B4A1L}}};
    int16_t l_1276[2][2][8] = {{{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{2L,(-1L),2L,2L,(-1L),2L,2L,(-1L)}},{{(-1L),2L,2L,(-1L),2L,2L,(-1L),2L},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}}};
    uint64_t ***l_1320 = &g_874[3];
    uint64_t * const *l_1327 = (void*)0;
    uint64_t * const **l_1326 = &l_1327;
    int32_t l_1333[5][8][6] = {{{(-3L),0x39FEE4A9L,0x2630BB4EL,0x2630BB4EL,0x39FEE4A9L,(-3L)},{(-6L),(-9L),0x9EF818AFL,0L,0x7F7F5033L,(-5L)},{0x2630BB4EL,0x872ED102L,0x2742F788L,1L,1L,0xCF94340DL},{0x2630BB4EL,(-1L),1L,0L,1L,(-1L)},{(-6L),0x7F7F5033L,1L,0x2630BB4EL,0xB69F348CL,0x8F761AF9L},{(-3L),(-3L),0xCF94340DL,(-1L),0x45569C25L,0xE8BF67D5L},{0x6E37D8B5L,0x2742F788L,1L,0x6E37D8B5L,0x9786CC3BL,0x07C612C0L},{0xE8BF67D5L,(-5L),(-1L),0x71CFE978L,(-1L),0L}},{{(-1L),0L,0x9CD16FA0L,8L,(-1L),8L},{1L,(-5L),1L,0xEB3236A2L,0x9786CC3BL,0x338E6A62L},{0x07C612C0L,0x2742F788L,(-10L),0L,0x02034A49L,8L},{0x71CFE978L,(-3L),0xEB3236A2L,0L,0x2630BB4EL,0xEB3236A2L},{0x07C612C0L,1L,(-1L),0xEB3236A2L,0x9EF818AFL,1L},{1L,(-1L),0x07C612C0L,8L,0x2742F788L,(-1L)},{(-1L),1L,0x07C612C0L,0x71CFE978L,1L,1L},{0xE8BF67D5L,(-7L),(-1L),0x6E37D8B5L,1L,0xEB3236A2L}},{{0x6E37D8B5L,1L,0xEB3236A2L,0x07C612C0L,0xCF94340DL,8L},{0x338E6A62L,1L,(-10L),(-10L),1L,0x338E6A62L},{(-9L),(-7L),1L,0L,1L,8L},{(-10L),1L,0x9CD16FA0L,(-1L),0x2742F788L,0L},{(-10L),(-1L),(-1L),0L,0x9EF818AFL,0x07C612C0L},{(-9L),1L,1L,(-10L),0x2630BB4EL,0xE8BF67D5L},{0x338E6A62L,(-3L),0L,0x07C612C0L,0x02034A49L,0xE8BF67D5L},{0x6E37D8B5L,0x2742F788L,1L,0x6E37D8B5L,0x9786CC3BL,0x07C612C0L}},{{0xE8BF67D5L,(-5L),(-1L),0x71CFE978L,(-1L),0L},{(-1L),0L,0x9CD16FA0L,8L,(-1L),8L},{1L,(-5L),1L,0xEB3236A2L,0x9786CC3BL,0x338E6A62L},{0x07C612C0L,0x2742F788L,(-10L),0L,0x02034A49L,8L},{0x71CFE978L,(-3L),0xEB3236A2L,0L,0x2630BB4EL,0xEB3236A2L},{0x07C612C0L,1L,(-1L),0xEB3236A2L,0x9EF818AFL,1L},{1L,(-1L),0x07C612C0L,8L,0x2742F788L,(-1L)},{(-1L),1L,0x07C612C0L,0x71CFE978L,1L,1L}},{{0xE8BF67D5L,(-7L),(-1L),0x6E37D8B5L,1L,0xEB3236A2L},{0x6E37D8B5L,1L,0xEB3236A2L,0x07C612C0L,0xCF94340DL,8L},{0x338E6A62L,1L,(-10L),(-10L),1L,0x338E6A62L},{(-9L),(-7L),1L,0L,1L,8L},{(-10L),1L,0x9CD16FA0L,(-1L),0x2742F788L,0L},{(-10L),(-1L),(-1L),0L,1L,(-9L)},{(-1L),(-1L),0xB69F348CL,0xFD526EAFL,(-10L),0x39FEE4A9L},{0x7F7F5033L,0x338E6A62L,0x872ED102L,(-9L),0xEB3236A2L,0x39FEE4A9L}}};
    struct S0 **l_1342 = &g_1258;
    struct S0 ***l_1341 = &l_1342;
    int32_t *l_1354 = (void*)0;
    int32_t *l_1355 = &g_51;
    int32_t *l_1356 = &l_1333[1][4][1];
    int32_t *l_1357 = &l_1333[2][4][4];
    int32_t *l_1358 = &l_1333[2][0][5];
    int32_t *l_1359 = &l_1333[3][2][3];
    int32_t *l_1360 = (void*)0;
    int32_t *l_1361 = &l_1274[4][4][3];
    int32_t *l_1362 = &l_52;
    int32_t *l_1363[8][10][3] = {{{(void*)0,(void*)0,&l_1333[3][2][3]},{&l_52,&l_52,&l_1274[3][6][0]},{&l_1273,&l_1333[1][5][2],&l_1273},{&l_1274[3][6][0],&l_52,&l_52},{&l_1333[3][2][3],(void*)0,(void*)0},{&l_1274[6][4][2],(void*)0,&l_1333[4][1][0]},{&l_1274[8][4][0],&l_1274[8][4][0],(void*)0},{&l_1274[6][4][2],&l_1274[3][6][0],(void*)0},{&l_1333[3][2][3],(void*)0,&l_1274[3][6][0]},{&l_1274[3][6][0],&l_1274[4][2][0],&l_1274[4][2][0]}},{{&l_1273,&l_1333[3][2][3],&l_1274[3][6][0]},{&l_52,&g_51,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_1333[4][1][0],&l_1274[6][5][2],&l_1333[4][1][0]},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_51,&l_52},{&l_1274[3][6][0],&l_1333[3][2][3],&l_1273},{&l_1274[4][2][0],&l_1274[4][2][0],&l_1274[3][6][0]},{&l_1274[3][6][0],(void*)0,&l_1333[3][2][3]},{(void*)0,&l_1274[3][6][0],&l_1274[6][4][2]}},{{(void*)0,&l_1274[8][4][0],&l_1274[8][4][0]},{&l_1333[4][1][0],(void*)0,&l_1274[6][4][2]},{(void*)0,(void*)0,&l_1333[3][2][3]},{&l_52,&l_52,&l_1274[3][6][0]},{&l_1273,&l_1333[1][5][2],&l_1273},{&l_1274[3][6][0],&l_52,&l_52},{&l_1333[3][2][3],(void*)0,(void*)0},{&l_1274[6][4][2],(void*)0,&l_1333[4][1][0]},{&l_1274[8][4][0],&l_1274[8][4][0],(void*)0},{&l_1274[6][4][2],&l_1274[3][6][0],(void*)0}},{{&l_1333[3][2][3],(void*)0,&l_1274[3][6][0]},{&l_1274[3][6][0],&l_1274[4][2][0],&l_1274[4][2][0]},{&l_1273,&l_1333[3][2][3],&l_1274[3][6][0]},{&l_52,&g_51,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_1333[4][1][0],&l_1274[6][5][2],&l_1333[4][1][0]},{(void*)0,(void*)0,(void*)0},{(void*)0,&g_51,&l_52},{&l_1274[3][6][0],&l_1333[3][2][3],&l_1273},{&l_1274[4][2][0],&l_1274[4][2][0],&l_1274[3][6][0]}},{{&l_1274[3][6][0],(void*)0,&l_1333[3][2][3]},{(void*)0,&l_1274[3][6][0],&l_1274[6][4][2]},{(void*)0,&l_1274[8][4][0],&l_1274[8][4][0]},{&l_1333[4][1][0],(void*)0,&l_1274[6][4][2]},{(void*)0,(void*)0,&l_1333[3][2][3]},{&l_52,&l_52,&l_1274[3][6][0]},{&l_1273,&l_1333[1][5][2],&l_1273},{&l_1274[3][6][0],&l_52,&l_52},{&l_1333[3][2][3],(void*)0,(void*)0},{&l_1274[6][4][2],(void*)0,&l_1333[4][1][0]}},{{&l_1274[8][4][0],&l_1274[8][4][0],(void*)0},{&l_1274[6][4][2],&l_1274[3][6][0],(void*)0},{&l_1333[3][2][3],(void*)0,&l_1274[3][6][0]},{&l_1274[3][6][0],&l_1274[4][2][0],&l_1274[4][2][0]},{&l_1273,&l_1333[3][2][3],&l_1274[3][6][0]},{&l_52,&g_51,(void*)0},{(void*)0,(void*)0,(void*)0},{&l_1333[4][1][0],&l_1274[6][5][2],&l_1333[4][1][0]},{(void*)0,(void*)0,(void*)0},{&l_1274[6][5][2],&l_1274[6][4][2],&l_1333[4][1][0]}},{{(void*)0,&l_1333[1][5][2],(void*)0},{&g_51,&g_51,&l_1274[4][2][0]},{(void*)0,&l_1274[8][4][0],&l_1333[1][5][2]},{&l_1274[6][5][2],&l_1274[4][2][0],&l_52},{&l_1274[8][4][0],(void*)0,(void*)0},{&l_52,&l_1274[6][5][2],&l_52},{&l_1273,&l_1274[3][6][0],&l_1333[1][5][2]},{&l_1333[4][1][0],&l_1274[3][6][0],&l_1274[4][2][0]},{(void*)0,&g_5,(void*)0},{&l_1274[4][2][0],&l_1274[3][6][0],&l_1333[4][1][0]}},{{&l_1333[1][5][2],&l_1274[3][6][0],&l_1273},{&l_52,&l_1274[6][5][2],&l_52},{(void*)0,(void*)0,&l_1274[8][4][0]},{&l_52,&l_1274[4][2][0],&l_1274[6][5][2]},{&l_1333[1][5][2],&l_1274[8][4][0],(void*)0},{&l_1274[4][2][0],&g_51,&g_51},{(void*)0,&l_1333[1][5][2],(void*)0},{&l_1333[4][1][0],&l_1274[6][4][2],&l_1274[6][5][2]},{&l_1273,(void*)0,&l_1274[8][4][0]},{&l_52,(void*)0,&l_52}}};
    uint32_t l_1364 = 0x40D6F409L;
    int i, j, k;
    for (p_45 = (-2); (p_45 < (-24)); --p_45)
    { /* block id: 14 */
        int32_t *l_50 = &g_51;
        (*l_50) = (-2L);
        if (l_52)
            continue;
    }
    return g_697.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_700.f2 g_1176 g_1180 g_873 g_874 g_875 g_350 g_563 g_494 g_105 g_1186 g_697.f2 g_589.f0 g_475 g_1227 g_288 g_923.f0 g_474 g_78 g_51 g_312 g_179 g_1235 g_131.f5 g_1246
 * writes: g_700.f2 g_940 g_105 g_1186 g_697.f2 g_475 g_78 g_51
 */
static union U2  func_55(uint32_t  p_56, int32_t  p_57, int32_t * const  p_58)
{ /* block id: 525 */
    int32_t l_1177 = 0xB0D10789L;
    int32_t l_1191 = 0xA2F30CD1L;
    int32_t l_1193 = (-2L);
    int32_t l_1194 = (-1L);
    int32_t l_1195 = 0xE5EB0D82L;
    int32_t l_1196[4][10][6] = {{{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL}},{{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL}},{{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL}},{{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL},{9L,0x822654DCL,9L,0x822654DCL,9L,0x822654DCL}}};
    int i, j, k;
    for (g_700.f2 = 0; (g_700.f2 <= 5); g_700.f2 += 1)
    { /* block id: 528 */
        int8_t ****l_1179[6] = {&g_492,&g_492,&g_492,&g_492,&g_492,&g_492};
        int32_t l_1188 = 0x2FD3E970L;
        int32_t l_1192 = 0x40070F8EL;
        int32_t l_1197[7][9][4] = {{{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL}},{{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L}},{{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL}},{{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L}},{{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL}},{{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L}},{{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL},{0x7D3C969CL,0x8949E15EL,0x7D3C969CL,0x10CA0E91L},{0x7D3C969CL,0x10CA0E91L,0x7D3C969CL,0x8949E15EL}}};
        int32_t l_1202 = 0xC2C4DB71L;
        union U2 ***l_1226[2];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_1226[i] = &g_896;
        for (g_940 = 1; (g_940 <= 5); g_940 += 1)
        { /* block id: 531 */
            return g_1176;
        }
        g_1186 &= (l_1177 <= (((*g_494) &= (((((*p_58) > (!(p_56 , (l_1179[4] != l_1179[5])))) | 8L) , (g_1180[0] , (safe_mul_func_uint64_t_u_u(5UL, ((safe_div_func_int32_t_s_s((+0x4BB41B84L), 0xC83F3F1DL)) , (***g_873)))))) != g_563)) > p_56));
        for (g_697.f2 = 1; (g_697.f2 <= 5); g_697.f2 += 1)
        { /* block id: 538 */
            int16_t l_1199 = 0xB0E3L;
            int32_t l_1200 = 1L;
            int32_t l_1201[4][10][6] = {{{0x7CFE24D3L,1L,0L,0L,1L,0x7CFE24D3L},{0xB17FD459L,0xFB030689L,0L,0x42147077L,0L,2L},{0xC59F8B72L,0xB17FD459L,(-1L),0xE8362950L,(-1L),0xC883FCE5L},{0xC59F8B72L,0L,0xE8362950L,0x1C2A3499L,0x3243F7BAL,0xAF837979L},{0xC8331A03L,0x5BCF0283L,0xC59F8B72L,1L,(-9L),0xC59F8B72L},{0xE8362950L,0x5BCF0283L,(-10L),0xC8331A03L,0x3243F7BAL,1L},{0xC883FCE5L,0L,0x1C2A3499L,(-10L),(-1L),(-10L)},{0x1C2A3499L,0xB17FD459L,0x1C2A3499L,0xAF837979L,0x5BCF0283L,1L},{0x5ECC633CL,0xE21CAAF5L,(-10L),(-2L),1L,0xC59F8B72L},{(-2L),1L,0xC59F8B72L,(-2L),1L,0xAF837979L}},{{0x5ECC633CL,(-9L),0xE8362950L,0xAF837979L,0L,0xC883FCE5L},{0x1C2A3499L,1L,(-1L),(-10L),0L,2L},{0xC883FCE5L,(-9L),0x37A8ACF9L,0xC8331A03L,1L,0xE8362950L},{0xE8362950L,1L,1L,1L,1L,0xE8362950L},{0xC8331A03L,0xE21CAAF5L,0x37A8ACF9L,0x1C2A3499L,0x5BCF0283L,2L},{0xC59F8B72L,0xB17FD459L,(-1L),0xE8362950L,(-1L),0xC883FCE5L},{0xC59F8B72L,0L,0xE8362950L,0x1C2A3499L,0x3243F7BAL,0xAF837979L},{0xC8331A03L,0x5BCF0283L,0xC59F8B72L,1L,(-9L),0xC59F8B72L},{0xE8362950L,0x5BCF0283L,(-10L),0xC8331A03L,0x3243F7BAL,1L},{0xC883FCE5L,0L,0x1C2A3499L,(-10L),(-1L),(-10L)}},{{0x1C2A3499L,0xB17FD459L,0x1C2A3499L,0xAF837979L,0x5BCF0283L,1L},{0x5ECC633CL,0xE21CAAF5L,(-10L),(-2L),1L,0xC59F8B72L},{(-2L),1L,0xC59F8B72L,(-2L),1L,0xAF837979L},{0x5ECC633CL,(-9L),0xE8362950L,0xAF837979L,0L,0xC883FCE5L},{0x1C2A3499L,1L,(-1L),(-10L),0L,2L},{0xC883FCE5L,(-9L),0x37A8ACF9L,0xC8331A03L,1L,0xE8362950L},{0xE8362950L,1L,1L,1L,1L,0xE8362950L},{0xC8331A03L,0xE21CAAF5L,0x37A8ACF9L,0x1C2A3499L,0x5BCF0283L,2L},{0xC59F8B72L,0xB17FD459L,(-1L),0xE8362950L,(-1L),0xC883FCE5L},{0xC59F8B72L,0L,0xE8362950L,0x1C2A3499L,0x3243F7BAL,0xAF837979L}},{{0xC8331A03L,0x5BCF0283L,0xC59F8B72L,1L,(-9L),0xC59F8B72L},{0xE8362950L,0x5BCF0283L,(-10L),0xC8331A03L,0x3243F7BAL,1L},{0xC883FCE5L,0L,0x1C2A3499L,(-10L),(-1L),(-10L)},{0x1C2A3499L,0xB17FD459L,0x1C2A3499L,0xFF70DD79L,(-10L),0x9485D5CEL},{(-9L),2L,(-2L),(-9L),(-2L),0L},{(-9L),(-2L),0L,(-9L),0xC883FCE5L,0xFF70DD79L},{(-9L),0xC59F8B72L,(-1L),0xFF70DD79L,1L,0xFB030689L},{1L,0xC883FCE5L,0L,(-2L),1L,(-1L)},{0xFB030689L,0xC59F8B72L,(-7L),0xDF4C35E8L,0xC883FCE5L,(-1L)},{(-1L),(-2L),0x9485D5CEL,0x9485D5CEL,(-2L),(-1L)}}};
            int16_t l_1245[2];
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1245[i] = 0x75CFL;
            for (l_1177 = 0; (l_1177 <= 9); l_1177 += 1)
            { /* block id: 541 */
                int32_t *l_1187 = (void*)0;
                int32_t *l_1189 = &l_1188;
                int32_t *l_1190[6] = {&g_5,&g_5,&g_5,&g_5,&g_5,&g_5};
                uint8_t l_1203[4][8] = {{255UL,255UL,0x73L,0x1BL,0x73L,255UL,255UL,0x73L},{0x44L,0x73L,0x73L,0x44L,247UL,0x44L,0x73L,0x73L},{0x73L,247UL,0x1BL,0x1BL,247UL,0x73L,247UL,0x1BL},{0x44L,247UL,0x44L,0x73L,0x73L,0x44L,247UL,0x44L}};
                uint8_t *l_1228 = (void*)0;
                uint8_t *l_1229 = &g_78;
                int i, j;
                --l_1203[2][4];
                (*l_1189) = (((0x17440B22L == (safe_div_func_int32_t_s_s((((*g_494) |= p_56) && ((*l_1229) |= ((((*g_474) = (safe_div_func_int64_t_s_s(((safe_sub_func_int32_t_s_s(((g_589.f0 | (safe_rshift_func_int8_t_s_s((((safe_div_func_uint64_t_u_u((((safe_add_func_uint64_t_u_u((((((*p_58) = (safe_sub_func_uint8_t_u_u((safe_div_func_int8_t_s_s((g_475 != (safe_lshift_func_int8_t_s_s(l_1191, (((&g_799 != &g_799) , l_1226[1]) != (void*)0)))), (*l_1189))), p_57))) || p_57) && (*g_875)) < l_1196[3][0][5]), p_57)) , g_1227) , 0xFC53786EB06AA814LL), g_288[1][6])) || p_56) > 0UL), p_56))) || l_1201[1][8][0]), l_1192)) , p_56), g_923.f0))) > g_1186) != p_57))), l_1201[3][6][1]))) == 0x0E47L) || 0xA7AA52C0B92D1A1ALL);
                (*p_58) = (*p_58);
                for (g_51 = 1; (g_51 <= 5); g_51 += 1)
                { /* block id: 551 */
                    uint16_t l_1238 = 1UL;
                    int i;
                    (*l_1189) ^= (safe_rshift_func_int64_t_s_s((!((*g_312) ^ ((safe_sub_func_uint64_t_u_u(p_57, ((65530UL >= (((g_1235 , ((((((safe_div_func_uint64_t_u_u((l_1238 && (((safe_sub_func_uint32_t_u_u(p_57, (safe_mod_func_uint64_t_u_u((((safe_div_func_uint16_t_u_u(((*g_474) = l_1195), p_56)) > (0xCC2FL == p_57)) , p_57), (***g_873))))) , p_56) | 0UL)), 18446744073709551611UL)) != 1L) || p_56) && l_1200) && 0x3A70L) >= 0x7C4C698DAE473935LL)) & 248UL) , g_131.f5)) != p_56))) && p_56))), l_1245[0]));
                }
            }
        }
    }
    (*p_58) = l_1191;
    return g_1246;
}


/* ------------------------------------------ */
/* 
 * reads : g_51 g_78 g_5 g_95 g_100 g_101 g_475 g_645.f1 g_612 g_186.f0 g_131.f4 g_217 g_179 g_474 g_191.f0 g_282 g_312 g_569.f0 g_467 g_793 g_466 g_799 g_131.f3 g_465 g_76 g_2 g_645 g_589 g_575.f1 g_350 g_189 g_871 g_872 g_873 g_494 g_105 g_886 g_894 g_905 g_919 g_923 g_131.f0 g_135.f0 g_569 g_934 g_405 g_406 g_895 g_896 g_563 g_575 g_874 g_875 g_985 g_771 g_1011 g_131 g_623 g_1014 g_536 g_537 g_700.f2 g_940 g_699.f2 g_1067 g_186 g_606 g_1125 g_1139 g_1152 g_137 g_1154
 * writes: g_51 g_76 g_78 g_95 g_105 g_475 g_771 g_282 g_217 g_288 g_350 g_563 g_189 g_873 g_179 g_886 g_894 g_940 g_101 g_131.f3 g_905.f2 g_700.f2 g_699.f2 g_1067 g_1153 g_1139.f2
 */
static uint64_t  func_59(uint32_t  p_60, uint16_t  p_61, int32_t  p_62)
{ /* block id: 18 */
    int32_t *l_66 = (void*)0;
    int32_t *l_67 = &g_51;
    int32_t **l_96 = &g_76;
    int32_t **l_99 = &l_66;
    int32_t ***l_98 = &l_99;
    int8_t *l_104 = &g_105;
    int32_t l_116 = 5L;
    int32_t l_119[9] = {1L,(-3L),1L,(-3L),1L,(-3L),1L,(-3L),1L};
    int32_t *l_156 = &g_2;
    int8_t l_158 = 9L;
    int32_t *l_159 = &l_116;
    union U2 **l_240 = &g_137;
    union U2 ***l_239 = &l_240;
    uint16_t l_259 = 0xCC53L;
    uint8_t l_322 = 0xECL;
    int16_t l_335 = 5L;
    int8_t l_352 = 0x75L;
    union U1 **l_369 = &g_189[4];
    int16_t l_407 = 0xD87EL;
    int8_t **l_490 = (void*)0;
    int8_t ***l_489 = &l_490;
    int8_t l_511[1];
    struct S0 *l_576 = (void*)0;
    int64_t l_591 = 0L;
    const uint16_t *l_611 = &g_612;
    int32_t l_622 = 0xDEFC6456L;
    const int16_t l_659 = 0x90D7L;
    int16_t l_661 = 0L;
    uint16_t l_664 = 0UL;
    uint8_t l_746 = 0xC4L;
    uint32_t *l_785[10] = {(void*)0,&g_179,&g_179,&g_179,&g_179,(void*)0,&g_179,&g_179,&g_179,&g_179};
    int32_t l_822 = 8L;
    int16_t l_831 = 0x9B93L;
    union U2 ****l_901[10];
    const union U1 ****l_1020 = (void*)0;
    int32_t *** const *l_1060 = (void*)0;
    int32_t *** const **l_1059 = &l_1060;
    uint16_t ***l_1063 = &g_606[2];
    uint32_t l_1140 = 0x27182D0BL;
    int64_t l_1148 = 0xE90C59BD98374423LL;
    int32_t l_1166 = (-1L);
    int i;
    for (i = 0; i < 1; i++)
        l_511[i] = (-8L);
    for (i = 0; i < 10; i++)
        l_901[i] = &l_239;
lbl_68:
    (*l_67) = g_51;
    (*l_67) = 0x59CADFD6L;
lbl_106:
    if (g_51)
        goto lbl_68;
    if (((((*l_96) = func_71(l_66)) == (void*)0) != (!(((*l_104) = (((((*l_98) = &l_67) == g_100) | (((*g_100) != ((safe_add_func_int32_t_s_s(p_61, 0x35EE06F6L)) , (*g_100))) , p_60)) == p_60)) > 0xC1L))))
    { /* block id: 34 */
        uint32_t l_109[10][1] = {{0xE62B9925L},{0x1F9A2085L},{0x1F9A2085L},{0xE62B9925L},{0x1F9A2085L},{0x1F9A2085L},{0xE62B9925L},{0x1F9A2085L},{0x1F9A2085L},{0xE62B9925L}};
        int32_t ***l_112 = &l_99;
        int32_t l_120 = 0L;
        int32_t l_121 = 0xBB8370F7L;
        int32_t l_122 = (-1L);
        int32_t l_123 = 0x5AB6CAC8L;
        union U1 *l_190 = &g_191;
        int32_t * const *l_241 = &g_76;
        int8_t l_257 = 0x4DL;
        int32_t l_295 = 0xE375CAF8L;
        int32_t l_296 = 0x726A70D8L;
        int8_t **l_327 = &l_104;
        int32_t l_360 = 0x04FE3442L;
        int32_t l_361[6] = {0x6B277BD7L,0x6B277BD7L,0x6B277BD7L,0x6B277BD7L,0x6B277BD7L,0x6B277BD7L};
        union U1 ***l_383 = &l_369;
        union U1 ***l_385 = &l_369;
        uint64_t *l_399[4][5] = {{&g_282,&g_282,&g_282,&g_282,&g_282},{&g_350,&g_282,&g_350,&g_350,&g_282},{&g_282,&g_350,&g_350,&g_282,&g_350},{&g_282,&g_282,&g_282,&g_282,&g_282}};
        uint16_t *l_472 = (void*)0;
        int16_t *l_480 = &l_407;
        union U2 *l_698[4] = {&g_699,&g_699,&g_699,&g_699};
        uint16_t ***l_701 = &g_606[1];
        int i, j;
        if (p_60)
            goto lbl_106;
    }
    else
    { /* block id: 340 */
        uint32_t l_769 = 6UL;
        int32_t l_776 = 0xB4760392L;
        uint64_t *l_798 = &g_771;
        uint64_t **l_797 = &l_798;
        uint32_t **l_814[10][5][5] = {{{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]}},{{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]}},{{&l_785[4],&l_785[6],&g_217,&g_217,&l_785[6]},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217}},{{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217}},{{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217}},{{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217}},{{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217}},{{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217}},{{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217}},{{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&g_217,&l_785[6],&l_785[6],&g_217},{&l_785[4],&l_785[4],&g_217,&g_217,&l_785[4]}}};
        int32_t l_820 = 2L;
        int32_t l_821[5] = {0xE52B807AL,0xE52B807AL,0xE52B807AL,0xE52B807AL,0xE52B807AL};
        const int8_t ****l_927 = (void*)0;
        int8_t *** const *l_930 = &g_492;
        const union U1 *l_992 = &g_191;
        uint8_t *l_1015 = &l_322;
        struct S0 **l_1017 = &l_576;
        struct S0 ***l_1016 = &l_1017;
        int16_t *l_1029 = (void*)0;
        int16_t *l_1030[7] = {&l_831,&l_831,&l_661,&l_831,&l_831,&l_661,&l_831};
        const int16_t l_1031 = (-8L);
        int32_t *l_1032 = &l_822;
        int8_t *l_1033 = (void*)0;
        int32_t *** const *l_1058 = (void*)0;
        int32_t *** const **l_1057 = &l_1058;
        uint32_t l_1064 = 0x527A2B41L;
        union U2 *l_1175 = &g_699;
        int i, j, k;
        for (g_475 = 0; (g_475 <= 37); g_475++)
        { /* block id: 343 */
            int64_t l_751 = 0x26C9F509BFBB7E6DLL;
            int16_t *l_770 = (void*)0;
            uint64_t *l_772 = &g_282;
            int32_t *l_773 = (void*)0;
            int32_t *l_774 = (void*)0;
            int32_t *l_775 = &l_119[2];
            int32_t ****l_817[7];
            uint8_t l_823 = 251UL;
            int32_t l_879[10][7][3] = {{{(-4L),(-3L),(-3L)},{9L,1L,0L},{(-4L),0xF6052FF1L,(-4L)},{1L,0x60914D1BL,0L},{0x45CC67B1L,0x45CC67B1L,(-3L)},{(-10L),0x60914D1BL,9L},{(-3L),0xF6052FF1L,(-8L)}},{{(-10L),1L,(-10L)},{0x45CC67B1L,(-3L),(-8L)},{1L,(-4L),9L},{(-4L),(-3L),(-3L)},{9L,1L,0L},{(-4L),0xF6052FF1L,(-4L)},{1L,0x60914D1BL,0L}},{{0x45CC67B1L,0x45CC67B1L,(-3L)},{(-10L),0x60914D1BL,9L},{(-3L),0xF6052FF1L,(-8L)},{(-10L),1L,(-10L)},{0x45CC67B1L,(-3L),(-8L)},{1L,(-4L),9L},{(-4L),(-3L),(-3L)}},{{9L,1L,0L},{(-4L),0xF6052FF1L,(-4L)},{1L,0x60914D1BL,0L},{0x45CC67B1L,0x45CC67B1L,(-3L)},{(-10L),0x60914D1BL,9L},{(-3L),0xF6052FF1L,(-8L)},{(-10L),1L,(-10L)}},{{0x45CC67B1L,(-3L),(-8L)},{1L,(-4L),9L},{(-4L),(-3L),(-3L)},{9L,1L,0L},{(-4L),0xF6052FF1L,(-4L)},{1L,0x60914D1BL,0L},{0x45CC67B1L,0x45CC67B1L,(-3L)}},{{(-10L),0x60914D1BL,9L},{(-3L),0xF6052FF1L,(-8L)},{(-10L),1L,(-10L)},{0x45CC67B1L,(-3L),(-8L)},{1L,(-4L),9L},{(-4L),(-3L),(-3L)},{9L,1L,0L}},{{(-4L),0xF6052FF1L,(-4L)},{1L,0x60914D1BL,0L},{0x45CC67B1L,0x45CC67B1L,(-3L)},{(-10L),0x60914D1BL,9L},{(-3L),0xF6052FF1L,(-8L)},{(-10L),1L,(-10L)},{0x45CC67B1L,(-3L),(-8L)}},{{1L,(-4L),9L},{(-4L),(-3L),(-3L)},{9L,1L,0L},{(-4L),0xF6052FF1L,(-4L)},{1L,0x60914D1BL,0L},{0x45CC67B1L,0x45CC67B1L,(-3L)},{(-10L),0x60914D1BL,9L}},{{(-3L),0xF6052FF1L,(-8L)},{(-10L),1L,(-10L)},{0x45CC67B1L,(-4L),0xF6052FF1L},{9L,0x60914D1BL,(-10L)},{(-8L),(-4L),(-4L)},{(-10L),(-4L),0x2AA31E2AL},{(-8L),0x45CC67B1L,(-8L)}},{{9L,9L,0x2AA31E2AL},{(-3L),(-3L),(-4L)},{0L,9L,(-10L)},{(-4L),0x45CC67B1L,0xF6052FF1L},{0L,(-4L),0L},{(-3L),(-4L),0xF6052FF1L},{9L,0x60914D1BL,(-10L)}}};
            union U2 ****l_898 = &g_895;
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_817[i] = (void*)0;
            (*l_67) |= (l_751 = p_61);
            if (((*l_775) = ((*l_159) = (safe_add_func_uint64_t_u_u(g_645.f1, ((*l_772) |= ((((safe_mul_func_int8_t_s_s((safe_mod_func_int64_t_s_s(((safe_unary_minus_func_uint64_t_u(((((((g_612 , (((safe_mul_func_uint16_t_u_u(((((*l_67) = (safe_rshift_func_uint32_t_u_u(l_751, ((safe_sub_func_int16_t_s_s((g_771 = (((g_5 || l_751) & (g_186.f0 < (safe_div_func_uint32_t_u_u((1L ^ ((l_769 , g_131.f4) > p_61)), (*g_217))))) & p_61)), (*l_159))) ^ g_131.f4)))) == l_769) , (*g_474)), 0x701CL)) , 18446744073709551615UL) != 0UL)) <= l_751) <= g_78) || (-1L)) || 4L) == p_61))) , 0xC8D698D323FC18A0LL), g_5)), 3UL)) && p_62) > g_191.f0) , p_61)))))))
            { /* block id: 351 */
                int8_t l_800 = 0xDDL;
                int32_t l_802 = 0L;
                int32_t l_819[9][5][5] = {{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}},{{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L},{1L,1L,1L,1L,1L}}};
                int i, j, k;
                for (g_771 = 0; (g_771 <= 0); g_771 += 1)
                { /* block id: 354 */
                    int16_t l_780 = (-1L);
                    uint16_t *l_781 = &l_664;
                    uint32_t **l_784 = &g_217;
                    uint8_t *l_786 = &l_746;
                    uint64_t ***l_796 = (void*)0;
                    int64_t *l_801 = &g_288[0][5];
                    int32_t ****l_811 = &g_405;
                    l_776 = (p_61 && (l_769 == (0x91L <= (l_776 > (safe_rshift_func_uint64_t_u_s((((((+(*g_312)) | l_776) != ((((*l_781) = l_780) >= ((0xEB26L && (*g_474)) , g_569[0].f0)) || g_467)) < l_780) > p_60), 7))))));
                    l_802 |= (safe_div_func_uint64_t_u_u((p_62 < (((*l_786) |= (((*l_784) = func_71(&l_776)) == l_785[4])) != (safe_rshift_func_int64_t_s_s(((*l_801) = (safe_div_func_uint16_t_u_u(((((safe_rshift_func_int64_t_s_u((((*l_772) ^= (g_793 , ((safe_lshift_func_int64_t_s_u(((p_62 >= (*g_466)) && (((l_797 = (void*)0) != g_799) <= 65529UL)), 46)) >= 1UL))) & g_131.f3), 31)) || l_800) , (**g_465)) , 0x150AL), l_780))), 56)))), g_191.f0));
                    for (g_350 = 0; (g_350 <= 0); g_350 += 1)
                    { /* block id: 365 */
                        uint32_t l_815 = 0x342BA4BBL;
                        int32_t l_816[6][4] = {{1L,0xDE77FB6EL,0xDE77FB6EL,1L},{0xDE77FB6EL,1L,0xDE77FB6EL,0xDE77FB6EL},{1L,1L,0x153DAF7FL,1L},{1L,0xDE77FB6EL,0xDE77FB6EL,1L},{0xDE77FB6EL,1L,0xDE77FB6EL,0xDE77FB6EL},{1L,1L,0x153DAF7FL,1L}};
                        int32_t *****l_818 = &l_817[6];
                        int i, j;
                        if ((*g_76))
                            break;
                        l_802 &= l_800;
                        (**l_99) |= (safe_div_func_int32_t_s_s(((p_62 >= 0xD991874E82B0F3FCLL) & ((l_780 ^ 0xB8L) | ((safe_div_func_int32_t_s_s(0x730C591EL, (safe_sub_func_int32_t_s_s((safe_lshift_func_uint32_t_u_u(((p_62 | (l_811 == ((*l_818) = (((safe_add_func_uint8_t_u_u((((((l_814[8][0][1] == &g_217) && p_60) || p_62) <= l_815) , l_816[5][1]), p_62)) , (*l_159)) , l_817[6])))) , l_800), 14)), p_62)))) , p_60))), 4UL));
                    }
                }
                ++l_823;
                for (g_563 = 23; (g_563 > 52); g_563++)
                { /* block id: 375 */
                    uint32_t l_828 = 4294967295UL;
                    int16_t *l_848 = (void*)0;
                    int16_t *l_849 = &l_335;
                    uint8_t *l_850 = &g_78;
                    (*l_775) |= (l_828 != ((safe_mod_func_uint32_t_u_u((((p_61 = l_831) && l_819[4][3][3]) , (safe_rshift_func_uint8_t_u_u(((safe_lshift_func_uint64_t_u_u(((l_776 < (g_645 , ((*l_850) = (safe_mod_func_int64_t_s_s((safe_rshift_func_int32_t_s_s((safe_div_func_int16_t_s_s(((*l_849) = (((((void*)0 != &l_369) > (0L & (((safe_mul_func_int64_t_s_s((safe_div_func_int16_t_s_s((g_589 , g_575.f1), p_62)), 1UL)) >= g_2) == p_60))) , p_61) < 2UL)), 1L)), p_62)), g_5))))) ^ (*l_159)), g_350)) == l_828), g_2))), 0xE5460478L)) , l_800));
                }
            }
            else
            { /* block id: 381 */
                (***l_98) ^= l_821[3];
                (*l_369) = (*l_369);
            }
            (**l_99) = (-2L);
            for (g_51 = 0; (g_51 != 16); ++g_51)
            { /* block id: 388 */
                uint64_t l_858[5] = {0x493EBA0C57CB7EE8LL,0x493EBA0C57CB7EE8LL,0x493EBA0C57CB7EE8LL,0x493EBA0C57CB7EE8LL,0x493EBA0C57CB7EE8LL};
                int32_t l_880 = 0x3869A9C3L;
                int32_t l_882[9] = {1L,1L,4L,1L,1L,4L,1L,1L,4L};
                int8_t **l_891[3][1];
                int16_t l_958 = 0x9303L;
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_891[i][j] = &l_104;
                }
                for (l_591 = 0; (l_591 != 19); ++l_591)
                { /* block id: 391 */
                    uint8_t l_877 = 0xCEL;
                    int32_t l_881 = 0x5BE56C3AL;
                    int32_t l_883 = 0x3C9A10BDL;
                    int32_t l_884 = (-1L);
                    int16_t l_885 = 0x1D70L;
                    union U2 *****l_897 = &g_894;
                    union U2 *****l_899 = (void*)0;
                    union U2 *****l_900 = &l_898;
                    int16_t *l_922 = &l_407;
                    uint8_t l_941[6][1][5] = {{{0x94L,0x94L,0xACL,0x94L,0x94L}},{{0x5AL,0x94L,0x5AL,0x5AL,0x94L}},{{0x94L,0x5AL,0x5AL,0x94L,0x5AL}},{{0x94L,0x94L,0xACL,0x94L,0x94L}},{{0x5AL,0x94L,0x5AL,0x5AL,0x94L}},{{0x94L,0x5AL,0x5AL,0x94L,0x5AL}}};
                    uint32_t *l_942 = &l_769;
                    int32_t l_945 = 0x28FF5793L;
                    int32_t l_946 = 0x6698AEDBL;
                    int32_t l_947[4][2] = {{0xBD3E356AL,0xBD3E356AL},{0xBD3E356AL,0xBD3E356AL},{0xBD3E356AL,0xBD3E356AL},{0xBD3E356AL,0xBD3E356AL}};
                    uint16_t l_959 = 1UL;
                    int i, j, k;
                    for (g_105 = 0; (g_105 > (-10)); g_105 = safe_sub_func_uint8_t_u_u(g_105, 7))
                    { /* block id: 394 */
                        int32_t l_857 = 0L;
                        const uint64_t *l_870 = &g_282;
                        const uint64_t **l_869[6] = {&l_870,&l_870,&l_870,&l_870,&l_870,&l_870};
                        const uint64_t *** const l_868[6][2][7] = {{{&l_869[3],&l_869[3],&l_869[3],&l_869[1],(void*)0,(void*)0,&l_869[1]},{&l_869[3],&l_869[3],&l_869[3],&l_869[1],(void*)0,(void*)0,&l_869[1]}},{{&l_869[3],&l_869[3],&l_869[3],&l_869[1],(void*)0,(void*)0,&l_869[1]},{&l_869[3],&l_869[3],&l_869[3],&l_869[1],(void*)0,(void*)0,&l_869[1]}},{{&l_869[3],&l_869[3],&l_869[3],&l_869[1],(void*)0,(void*)0,&l_869[1]},{&l_869[3],&l_869[3],&l_869[3],&l_869[1],&l_869[3],&l_869[3],(void*)0}},{{(void*)0,&l_869[0],(void*)0,(void*)0,&l_869[3],&l_869[3],(void*)0},{(void*)0,&l_869[0],(void*)0,(void*)0,&l_869[3],&l_869[3],(void*)0}},{{(void*)0,&l_869[0],(void*)0,(void*)0,&l_869[3],&l_869[3],(void*)0},{(void*)0,&l_869[0],(void*)0,(void*)0,&l_869[3],&l_869[3],(void*)0}},{{(void*)0,&l_869[0],(void*)0,(void*)0,&l_869[3],&l_869[3],(void*)0},{(void*)0,&l_869[0],(void*)0,(void*)0,&l_869[3],&l_869[3],(void*)0}}};
                        int32_t l_876 = 0xA3208E13L;
                        int32_t l_878[5] = {3L,3L,3L,3L,3L};
                        int i, j, k;
                        l_858[1]++;
                        l_877 |= ((safe_div_func_uint64_t_u_u(((((**g_465) , (((p_62 , (safe_mul_func_int8_t_s_s(p_60, (l_876 = (safe_add_func_uint32_t_u_u(((*g_312) &= (+(l_868[1][0][0] == ((g_871 , g_872) , (g_873 = g_873))))), (((((((g_131.f3 | 18446744073709551610UL) != 0xD5L) < p_60) == (*g_494)) || 0x0531L) && 18446744073709551609UL) | 0xB7L))))))) , 0x88301D4C45C5A03ALL) && 4UL)) >= l_821[3]) ^ 249UL), 0x6078B55706FD1C21LL)) >= 3UL);
                        g_886[4][9]--;
                    }
                    if (((*l_775) = (safe_div_func_uint16_t_u_u((p_60 && (l_821[3] = (l_891[1][0] == l_891[1][0]))), (safe_add_func_int64_t_s_s((((*l_897) = g_894) != (l_901[3] = ((*l_900) = l_898))), ((*l_772) = (((safe_unary_minus_func_uint64_t_u(((safe_mod_func_uint16_t_u_u((g_905 , ((~(*l_775)) || ((safe_mod_func_int32_t_s_s((safe_mod_func_int64_t_s_s((safe_sub_func_int64_t_s_s((safe_sub_func_uint32_t_u_u(((*g_312) ^= (((safe_mod_func_int16_t_s_s((((l_820 |= (((safe_mul_func_uint16_t_u_u((g_919 , (safe_add_func_int16_t_s_s(((*l_922) = l_882[8]), g_95))), 0x0560L)) , p_60) != 0x8FF9L)) , g_923) , g_131.f0), (-6L))) , l_882[5]) && (*l_156))), 0x477C7F1BL)), 4L)), p_60)), (**l_99))) <= 0x4E55L))), (*g_474))) , 18446744073709551615UL))) & l_769) , p_60))))))))
                    { /* block id: 411 */
                        const int8_t *****l_928 = (void*)0;
                        const int8_t *****l_929 = &l_927;
                        uint32_t *l_943 = &l_769;
                        int32_t l_944 = 6L;
                        int32_t l_948 = 0L;
                        int32_t l_949 = 0L;
                        int32_t l_950 = 0x17DE5F6FL;
                        int32_t l_951 = 0L;
                        int32_t l_952 = 0xAE8A659EL;
                        int32_t l_953 = (-9L);
                        int32_t l_954 = 0x5248070FL;
                        int64_t l_955 = 0xE9BD21BEB54CF8ABLL;
                        int32_t l_956 = 0x278B4A1AL;
                        int32_t l_957 = (-1L);
                        (*l_775) ^= (((((safe_add_func_int16_t_s_s((((!(g_135.f0 | (((*l_772) = ((((*l_929) = l_927) == (g_569[0] , l_930)) & 65535UL)) , ((~((safe_lshift_func_uint8_t_u_u(g_934, 1)) | ((g_940 = (safe_mod_func_uint32_t_u_u((!g_645.f4), (safe_rshift_func_int8_t_s_s(l_821[3], 3))))) <= l_941[4][0][4]))) , (-1L))))) & g_131.f4) && p_62), p_61)) != p_62) >= p_61) < 0x6A0FC1DFL) == 0xCA9574C6L);
                        (*l_159) = (l_942 != l_943);
                        --l_959;
                    }
                    else
                    { /* block id: 418 */
                        return l_883;
                    }
                    for (p_60 = 0; (p_60 <= 0); p_60 += 1)
                    { /* block id: 423 */
                        int32_t l_964 = 0L;
                        uint8_t *l_965[4];
                        int64_t *l_978 = &g_288[3][1];
                        int i, j;
                        for (i = 0; i < 4; i++)
                            l_965[i] = &l_746;
                        (**g_405) = &l_882[5];
                        (*l_775) = (p_62 , (safe_mul_func_int32_t_s_s((l_964 && ((void*)0 == (**g_894))), (((g_563 |= (++g_78)) >= (((*l_942) = (safe_rshift_func_int16_t_s_u(((safe_rshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((safe_add_func_int64_t_s_s((g_575 , ((*l_978) = (((p_61 & (l_882[5] | ((((((*l_922) = ((((***g_873) |= ((safe_rshift_func_uint32_t_u_u(p_60, (0xF960L ^ 5L))) < g_645.f5)) == l_821[3]) <= l_880)) || p_62) && p_61) & p_62) | 0xA32141AEL))) ^ p_61) == (*l_775)))), p_60)) , (**g_465)), 6)), 1)) , (-1L)), 11))) >= 0xB651A0F7L)) <= (*g_217)))));
                        l_820 ^= ((((safe_sub_func_uint8_t_u_u(0UL, (safe_sub_func_int8_t_s_s(((void*)0 == &g_895), (safe_rshift_func_uint32_t_u_u((((*l_775) &= l_964) <= (g_985 , (safe_mul_func_uint8_t_u_u((l_883 = ((safe_mod_func_uint64_t_u_u(((p_61 |= (((*l_922) = (safe_sub_func_int32_t_s_s(((l_992 != (void*)0) , (((((safe_div_func_int8_t_s_s(((safe_rshift_func_uint32_t_u_u((0x50D7A415L | ((g_563 = l_821[2]) | 0x78L)), 14)) || l_882[6]), p_60)) > l_964) < 1L) , (void*)0) != (*g_465))), l_964))) >= 0UL)) >= (*l_156)), 0xB3FBE4B47CE2326CLL)) <= 0x9FAEL)), 0L)))), (*g_217))))))) , g_5) , g_771) && l_821[1]);
                    }
                }
                for (l_322 = 0; (l_322 >= 24); l_322++)
                { /* block id: 442 */
                    uint64_t l_1001 = 18446744073709551613UL;
                    int32_t **l_1004[3][2] = {{&g_101[2],&g_101[2]},{&g_101[2],&g_101[2]},{&g_101[2],&g_101[2]}};
                    int i, j;
                    l_882[5] &= (((safe_div_func_uint64_t_u_u((((*l_159) ^ (((l_1001 , ((safe_lshift_func_int64_t_s_s(((l_1004[2][1] == (*l_98)) < (safe_mul_func_int8_t_s_s(p_61, (safe_add_func_int64_t_s_s((&g_874[3] != &g_874[2]), (((safe_add_func_uint32_t_u_u(((((g_575.f0 , g_1011) , p_61) != l_958) || 0xFF5968F4L), (*l_775))) & p_62) , p_60)))))), 13)) , (void*)0)) != (void*)0) & (*l_159))) || l_858[2]), 0x3BE9C695342F40F7LL)) , p_61) > 0xA123DAB6L);
                    return p_62;
                }
                for (g_131.f3 = 0; (g_131.f3 <= 0); g_131.f3 += 1)
                { /* block id: 448 */
                    uint8_t *l_1013 = &g_563;
                    uint8_t **l_1012 = &l_1013;
                    int i, j;
                    (*l_159) ^= (g_131 , (l_821[(g_131.f3 + 2)] = l_821[(g_131.f3 + 2)]));
                    for (g_905.f2 = 0; (g_905.f2 <= 0); g_905.f2 += 1)
                    { /* block id: 453 */
                        (*l_159) |= (**l_99);
                        if (l_880)
                            continue;
                        if ((*g_623))
                            break;
                    }
                    l_821[(g_131.f3 + 2)] = ((((*l_1012) = &l_823) != (g_1014 , l_1015)) , 0xD2458441L);
                }
            }
        }
        if (((((((*l_1032) &= ((&g_577 != l_1016) <= (safe_mul_func_uint8_t_u_u(((l_1020 != l_1020) > 0xC50B7BACL), ((safe_mul_func_int16_t_s_s(p_60, ((safe_sub_func_int64_t_s_s((safe_div_func_int64_t_s_s(((safe_lshift_func_uint64_t_u_u(p_60, ((*g_875) = ((((*l_67) = (0xB1C28DD83A7BF5A2LL && ((*g_536) != (*g_536)))) , p_60) || (*g_875))))) != p_62), p_60)), l_1031)) < l_821[3]))) , 3UL))))) , l_820) == 1UL) , l_1033) != (void*)0))
        { /* block id: 466 */
            int8_t *l_1040 = &l_511[0];
            uint64_t **l_1045 = &g_875;
            for (g_700.f2 = 10; (g_700.f2 >= 38); g_700.f2++)
            { /* block id: 469 */
                (*l_159) = ((((((--(*l_798)) < (safe_rshift_func_uint8_t_u_s(((void*)0 != l_1040), 0))) && ((&g_179 == (void*)0) , ((safe_lshift_func_int32_t_s_u((((*l_67) = (safe_mul_func_int64_t_s_s((((void*)0 == l_1045) && (safe_lshift_func_int8_t_s_u((-1L), 1))), (18446744073709551606UL && g_940)))) < 1L), p_60)) <= 0x4A69L))) | 0x6BE5E60CL) , &g_491) != &l_927);
            }
            return p_61;
        }
        else
        { /* block id: 475 */
            uint16_t l_1056[7] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL};
            int32_t l_1099 = 0x83C90A36L;
            int8_t l_1105 = 9L;
            int32_t l_1109 = (-1L);
            int32_t l_1110[2];
            uint32_t l_1120 = 0x5B53105BL;
            int i;
            for (i = 0; i < 2; i++)
                l_1110[i] = (-1L);
            (**l_99) = ((p_61 ^ (((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u((p_61 && (**l_99)), ((safe_sub_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u(((l_1056[6] <= l_1056[6]) , ((l_1059 = l_1057) != (p_62 , (void*)0))), 4)), ((safe_lshift_func_uint64_t_u_u(((*l_798) = p_62), 35)) , g_191.f0))) > (*g_623)))), p_60)) , l_1063) == l_1063)) , l_1064);
            for (g_699.f2 = 0; (g_699.f2 > 27); g_699.f2++)
            { /* block id: 481 */
                int32_t l_1089 = 0x89F474A2L;
                int32_t l_1108[2][4];
                int8_t l_1134 = 0x99L;
                uint32_t l_1149 = 1UL;
                uint16_t l_1170[7][4][5] = {{{1UL,6UL,65528UL,65535UL,0x0E58L},{65535UL,0x3A60L,1UL,1UL,0x3A60L},{0UL,0xD644L,0UL,6UL,0x0E58L},{0x2233L,1UL,1UL,3UL,1UL}},{{0x0E58L,0x0E58L,1UL,0UL,65535UL},{0x2233L,8UL,65535UL,0x57AEL,0x57AEL},{0UL,2UL,0UL,0UL,65535UL},{65535UL,8UL,0x2233L,0x3A60L,0x371AL}},{{1UL,0x0E58L,0x0E58L,1UL,0UL},{1UL,1UL,0x2233L,0x371AL,8UL},{0UL,0xD644L,0UL,0xD644L,0UL},{1UL,0x3A60L,65535UL,0x371AL,65535UL}},{{65528UL,6UL,1UL,1UL,6UL},{0x57AEL,0x2233L,1UL,0x3A60L,65535UL},{0xD644L,1UL,0UL,0UL,0UL},{65535UL,65535UL,1UL,0x57AEL,8UL}},{{0xD644L,65535UL,65528UL,0UL,0UL},{0x57AEL,0x6372L,0x57AEL,3UL,0x371AL},{65528UL,65535UL,0xD644L,6UL,65535UL},{1UL,65535UL,65535UL,1UL,0x57AEL}},{{0UL,1UL,0xD644L,65535UL,65535UL},{1UL,0x2233L,0x57AEL,0x2233L,1UL},{1UL,6UL,65528UL,65535UL,0x0E58L},{65535UL,0x3A60L,1UL,1UL,0x3A60L}},{{0UL,0xD644L,0UL,6UL,0x0E58L},{0x2233L,1UL,1UL,0x2233L,65535UL},{0UL,0UL,0x0E58L,6UL,0UL},{0x6372L,1UL,0x57AEL,0x3A60L,0x3A60L}}};
                int i, j, k;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_1108[i][j] = (-1L);
                }
                if (((*l_67) = (-7L)))
                { /* block id: 483 */
                    uint8_t ***l_1069 = &g_1067;
                    uint8_t **l_1071 = &g_1068;
                    uint8_t ***l_1070 = &l_1071;
                    (*l_1070) = ((*l_1069) = g_1067);
                }
                else
                { /* block id: 486 */
                    const int8_t l_1100 = 0xDEL;
                    int32_t l_1101 = (-1L);
                    int32_t *l_1104 = &l_821[3];
                    int32_t l_1111 = 0xE7257207L;
                    int32_t l_1112 = 4L;
                    int32_t l_1113 = 0x8A35EFBFL;
                    int32_t l_1114 = 1L;
                    int32_t l_1115 = 9L;
                    int32_t l_1116 = (-2L);
                    int32_t l_1117 = 0xD4F7EFB6L;
                    int32_t l_1118 = 1L;
                    int32_t l_1119 = (-4L);
                    int32_t *l_1143 = (void*)0;
                    int32_t *l_1144 = &l_1112;
                    int32_t *l_1145 = &l_1118;
                    int32_t *l_1146 = &l_119[2];
                    int32_t *l_1147[6][9] = {{&l_1110[1],&l_1099,(void*)0,&l_1099,&l_1110[1],&l_1110[1],&l_1099,(void*)0,&l_1099},{&l_1099,&l_1112,(void*)0,(void*)0,&l_1112,&l_1099,&l_1112,(void*)0,(void*)0},{&l_1110[1],&l_1110[1],&l_1099,(void*)0,&l_1099,&l_1110[1],&l_1110[1],&l_1099,(void*)0},{&l_119[2],&l_1112,&l_119[2],&l_1099,&l_1099,&l_119[2],&l_1112,&l_119[2],&l_1099},{&l_119[2],&l_1099,&l_1099,&l_119[2],&l_1112,&l_119[2],&l_1099,&l_1099,&l_119[2]},{&l_1110[1],&l_1099,(void*)0,&l_1099,&l_1110[1],&l_1110[1],&l_1099,(void*)0,&l_1099}};
                    int i, j;
                    if (((safe_lshift_func_int8_t_s_s(((safe_mod_func_uint64_t_u_u(((p_62 ^ (g_186 , ((*l_1015)--))) >= ((safe_mul_func_uint8_t_u_u(p_61, (*g_494))) ^ (safe_rshift_func_int16_t_s_s(((safe_sub_func_uint32_t_u_u(((+(safe_div_func_uint16_t_u_u((((safe_sub_func_uint8_t_u_u(l_1089, (g_282 <= (l_1099 = (0L <= (safe_mod_func_int64_t_s_s((safe_unary_minus_func_uint64_t_u((((*g_494) || (safe_lshift_func_int16_t_s_s((safe_mod_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(((((***l_1063) = ((p_60 , l_1032) != &p_62)) || 0xE78DL) || p_60), (-9L))), p_62)), 10))) < p_61))), p_61))))))) & p_60) >= 1L), (-1L)))) != l_1100), p_61)) , l_1099), l_1101)))), 7UL)) > (***l_98)), l_1056[5])) > p_62))
                    { /* block id: 490 */
                        return p_61;
                    }
                    else
                    { /* block id: 492 */
                        int32_t * const l_1102 = &l_820;
                        int32_t *l_1106 = &l_116;
                        int32_t *l_1107[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_1107[i] = &l_776;
                        l_1104 = l_1102;
                        (**g_405) = &l_1099;
                        l_1120++;
                        (*l_159) |= (0xBE2299A1684EE013LL ^ ((safe_add_func_uint32_t_u_u((g_1125 , (((safe_sub_func_uint16_t_u_u(((safe_mod_func_int64_t_s_s((safe_rshift_func_int16_t_s_u(((safe_unary_minus_func_uint8_t_u((safe_unary_minus_func_int16_t_s(l_1134)))) == (p_61 ^ (safe_rshift_func_int8_t_s_s(0L, (safe_lshift_func_uint8_t_u_u((((((g_1139 , func_71(func_71(func_71((g_1011.f3 , &l_1110[0]))))) != (void*)0) > 0UL) != p_62) ^ (*g_312)), 4)))))), 3)), (*g_875))) ^ l_1110[0]), p_60)) == p_62) != l_1140)), (*l_1104))) == 0UL));
                    }
                    (***l_98) ^= ((*l_1104) = ((-8L) | (--(*g_312))));
                    l_1149--;
                }
                (*g_1152) = &l_814[8][0][1];
                for (l_1120 = 0; (l_1120 <= 5); l_1120 += 1)
                { /* block id: 506 */
                    uint16_t l_1157 = 0x6396L;
                    int32_t l_1160 = 0xC0586C84L;
                    int32_t l_1161 = (-2L);
                    int32_t l_1162 = 1L;
                    int32_t l_1163 = (-1L);
                    int32_t l_1164 = 0L;
                    int32_t l_1165 = 0x351D6C23L;
                    int32_t l_1167 = 0L;
                    int32_t l_1168 = 7L;
                    int32_t l_1169 = 0xB6C85CBAL;
                    for (g_1139.f2 = 0; (g_1139.f2 <= 5); g_1139.f2 += 1)
                    { /* block id: 509 */
                        int8_t l_1155 = 0L;
                        int32_t *l_1156[10] = {&l_1109,(void*)0,&l_1109,(void*)0,&l_1109,(void*)0,&l_1109,(void*)0,&l_1109,(void*)0};
                        int i;
                        ++l_1157;
                        l_1170[3][0][4]++;
                    }
                    if (p_60)
                        break;
                    if (p_60)
                        break;
                    if (p_60)
                        continue;
                }
            }
            (**l_99) = p_62;
        }
        (*l_67) |= ((safe_div_func_uint32_t_u_u(((l_1175 = (**l_239)) == (void*)0), (**g_1154))) < p_62);
        return p_62;
    }
    return p_62;
}


/* ------------------------------------------ */
/* 
 * reads : g_78 g_5 g_51 g_95
 * writes: g_51 g_76 g_78 g_95
 */
static int32_t * func_71(int32_t * p_72)
{ /* block id: 22 */
    int32_t *l_73 = &g_51;
    int32_t *l_74 = &g_2;
    int32_t **l_75 = &l_74;
    uint8_t *l_77[2];
    int32_t l_79 = 1L;
    int32_t l_80 = 1L;
    int32_t l_81 = (-5L);
    int32_t l_82 = 0x3FFCD553L;
    int32_t l_83 = 0x38AD7B59L;
    int32_t l_84 = 0x359A1248L;
    int32_t l_85 = (-1L);
    int32_t l_86 = 0L;
    int32_t l_87 = 0x7032CD4BL;
    int32_t l_88 = 8L;
    int i;
    for (i = 0; i < 2; i++)
        l_77[i] = &g_78;
    (*l_73) = 0xDFC7A8F6L;
    (*l_75) = l_74;
    g_76 = (*l_75);
    g_95 &= ((--g_78) >= func_91((0x89E30C92L & ((g_5 == 255UL) != (*l_73))), &g_2));
    return p_72;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_91(uint64_t  p_92, int32_t * p_93)
{ /* block id: 27 */
    int32_t l_94 = 0xAEBC0183L;
    return l_94;
}




/* ---------------------------------------- */
int main (int argc, char* argv[])
{
    int i, j, k;
    int print_hash_value = 0;
    if (argc == 2 && strcmp(argv[1], "1") == 0) print_hash_value = 1;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_51, "g_51", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    transparent_crc(g_131.f0, "g_131.f0", print_hash_value);
    transparent_crc(g_131.f1, "g_131.f1", print_hash_value);
    transparent_crc(g_131.f2, "g_131.f2", print_hash_value);
    transparent_crc(g_131.f3, "g_131.f3", print_hash_value);
    transparent_crc(g_131.f4, "g_131.f4", print_hash_value);
    transparent_crc(g_131.f5, "g_131.f5", print_hash_value);
    transparent_crc(g_135.f0, "g_135.f0", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_141[i][j][k].f0, "g_141[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_160.f0, "g_160.f0", print_hash_value);
    transparent_crc(g_179, "g_179", print_hash_value);
    transparent_crc(g_186.f0, "g_186.f0", print_hash_value);
    transparent_crc(g_191.f0, "g_191.f0", print_hash_value);
    transparent_crc(g_282, "g_282", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_288[i][j], "g_288[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_350, "g_350", print_hash_value);
    transparent_crc(g_440, "g_440", print_hash_value);
    transparent_crc(g_467, "g_467", print_hash_value);
    transparent_crc(g_475, "g_475", print_hash_value);
    transparent_crc(g_541.f0, "g_541.f0", print_hash_value);
    transparent_crc(g_541.f1, "g_541.f1", print_hash_value);
    transparent_crc(g_541.f2, "g_541.f2", print_hash_value);
    transparent_crc(g_541.f3, "g_541.f3", print_hash_value);
    transparent_crc(g_541.f4, "g_541.f4", print_hash_value);
    transparent_crc(g_541.f5, "g_541.f5", print_hash_value);
    transparent_crc(g_563, "g_563", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_569[i].f0, "g_569[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_575.f0, "g_575.f0", print_hash_value);
    transparent_crc(g_575.f1, "g_575.f1", print_hash_value);
    transparent_crc(g_575.f2, "g_575.f2", print_hash_value);
    transparent_crc(g_575.f3, "g_575.f3", print_hash_value);
    transparent_crc(g_575.f4, "g_575.f4", print_hash_value);
    transparent_crc(g_575.f5, "g_575.f5", print_hash_value);
    transparent_crc(g_589.f0, "g_589.f0", print_hash_value);
    transparent_crc(g_589.f1, "g_589.f1", print_hash_value);
    transparent_crc(g_589.f2, "g_589.f2", print_hash_value);
    transparent_crc(g_589.f3, "g_589.f3", print_hash_value);
    transparent_crc(g_589.f4, "g_589.f4", print_hash_value);
    transparent_crc(g_589.f5, "g_589.f5", print_hash_value);
    transparent_crc(g_612, "g_612", print_hash_value);
    transparent_crc(g_645.f0, "g_645.f0", print_hash_value);
    transparent_crc(g_645.f1, "g_645.f1", print_hash_value);
    transparent_crc(g_645.f2, "g_645.f2", print_hash_value);
    transparent_crc(g_645.f3, "g_645.f3", print_hash_value);
    transparent_crc(g_645.f4, "g_645.f4", print_hash_value);
    transparent_crc(g_645.f5, "g_645.f5", print_hash_value);
    transparent_crc(g_697.f0, "g_697.f0", print_hash_value);
    transparent_crc(g_699.f0, "g_699.f0", print_hash_value);
    transparent_crc(g_700.f0, "g_700.f0", print_hash_value);
    transparent_crc(g_771, "g_771", print_hash_value);
    transparent_crc(g_793.f0, "g_793.f0", print_hash_value);
    transparent_crc(g_793.f1, "g_793.f1", print_hash_value);
    transparent_crc(g_793.f2, "g_793.f2", print_hash_value);
    transparent_crc(g_793.f3, "g_793.f3", print_hash_value);
    transparent_crc(g_793.f4, "g_793.f4", print_hash_value);
    transparent_crc(g_793.f5, "g_793.f5", print_hash_value);
    transparent_crc(g_871.f0, "g_871.f0", print_hash_value);
    transparent_crc(g_872.f0, "g_872.f0", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_886[i][j], "g_886[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_905.f0, "g_905.f0", print_hash_value);
    transparent_crc(g_919.f0, "g_919.f0", print_hash_value);
    transparent_crc(g_923.f0, "g_923.f0", print_hash_value);
    transparent_crc(g_934, "g_934", print_hash_value);
    transparent_crc(g_940, "g_940", print_hash_value);
    transparent_crc(g_985.f0, "g_985.f0", print_hash_value);
    transparent_crc(g_1011.f0, "g_1011.f0", print_hash_value);
    transparent_crc(g_1011.f1, "g_1011.f1", print_hash_value);
    transparent_crc(g_1011.f2, "g_1011.f2", print_hash_value);
    transparent_crc(g_1011.f3, "g_1011.f3", print_hash_value);
    transparent_crc(g_1011.f4, "g_1011.f4", print_hash_value);
    transparent_crc(g_1011.f5, "g_1011.f5", print_hash_value);
    transparent_crc(g_1014.f0, "g_1014.f0", print_hash_value);
    transparent_crc(g_1125.f0, "g_1125.f0", print_hash_value);
    transparent_crc(g_1139.f0, "g_1139.f0", print_hash_value);
    transparent_crc(g_1176.f0, "g_1176.f0", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1180[i].f0, "g_1180[i].f0", print_hash_value);
        transparent_crc(g_1180[i].f1, "g_1180[i].f1", print_hash_value);
        transparent_crc(g_1180[i].f2, "g_1180[i].f2", print_hash_value);
        transparent_crc(g_1180[i].f3, "g_1180[i].f3", print_hash_value);
        transparent_crc(g_1180[i].f4, "g_1180[i].f4", print_hash_value);
        transparent_crc(g_1180[i].f5, "g_1180[i].f5", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1186, "g_1186", print_hash_value);
    transparent_crc(g_1198, "g_1198", print_hash_value);
    transparent_crc(g_1227.f0, "g_1227.f0", print_hash_value);
    transparent_crc(g_1235.f0, "g_1235.f0", print_hash_value);
    transparent_crc(g_1246.f0, "g_1246.f0", print_hash_value);
    transparent_crc(g_1264, "g_1264", print_hash_value);
    transparent_crc(g_1275, "g_1275", print_hash_value);
    transparent_crc(g_1277, "g_1277", print_hash_value);
    transparent_crc(g_1278, "g_1278", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1376[i][j].f0, "g_1376[i][j].f0", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1377.f0, "g_1377.f0", print_hash_value);
    transparent_crc(g_1396.f0, "g_1396.f0", print_hash_value);
    transparent_crc(g_1487.f0, "g_1487.f0", print_hash_value);
    transparent_crc(g_1517.f0, "g_1517.f0", print_hash_value);
    transparent_crc(g_1517.f1, "g_1517.f1", print_hash_value);
    transparent_crc(g_1517.f2, "g_1517.f2", print_hash_value);
    transparent_crc(g_1517.f3, "g_1517.f3", print_hash_value);
    transparent_crc(g_1517.f4, "g_1517.f4", print_hash_value);
    transparent_crc(g_1517.f5, "g_1517.f5", print_hash_value);
    transparent_crc(g_1529.f0, "g_1529.f0", print_hash_value);
    transparent_crc(g_1532, "g_1532", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1583[i].f0, "g_1583[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1594[i].f0, "g_1594[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1597.f0, "g_1597.f0", print_hash_value);
    transparent_crc(g_1603.f0, "g_1603.f0", print_hash_value);
    transparent_crc(g_1712.f0, "g_1712.f0", print_hash_value);
    transparent_crc(g_1751.f0, "g_1751.f0", print_hash_value);
    transparent_crc(g_1803.f0, "g_1803.f0", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1822[i][j][k].f0, "g_1822[i][j][k].f0", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1934.f0, "g_1934.f0", print_hash_value);
    transparent_crc(g_2072, "g_2072", print_hash_value);
    transparent_crc(g_2094.f0, "g_2094.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 557
   depth: 1, occurrence: 8
XXX total union variables: 30

XXX non-zero bitfields defined in structs: 6
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 3
XXX structs with bitfields in the program: 64
breakdown:
   indirect level: 0, occurrence: 26
   indirect level: 1, occurrence: 14
   indirect level: 2, occurrence: 11
   indirect level: 3, occurrence: 8
   indirect level: 4, occurrence: 5
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 54
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 39
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 70

XXX max expression depth: 36
breakdown:
   depth: 1, occurrence: 126
   depth: 2, occurrence: 32
   depth: 3, occurrence: 1
   depth: 4, occurrence: 1
   depth: 5, occurrence: 4
   depth: 8, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 3
   depth: 16, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 22, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 3
   depth: 25, occurrence: 1
   depth: 26, occurrence: 1
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 2
   depth: 33, occurrence: 1
   depth: 34, occurrence: 2
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1

XXX total number of pointers: 476

XXX times a variable address is taken: 1158
XXX times a pointer is dereferenced on RHS: 195
breakdown:
   depth: 1, occurrence: 147
   depth: 2, occurrence: 26
   depth: 3, occurrence: 22
XXX times a pointer is dereferenced on LHS: 285
breakdown:
   depth: 1, occurrence: 235
   depth: 2, occurrence: 26
   depth: 3, occurrence: 24
XXX times a pointer is compared with null: 36
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 12
XXX times a pointer is qualified to be dereferenced: 6954

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1050
   level: 2, occurrence: 260
   level: 3, occurrence: 215
   level: 4, occurrence: 25
   level: 5, occurrence: 13
XXX number of pointers point to pointers: 218
XXX number of pointers point to scalars: 232
XXX number of pointers point to structs: 8
XXX percent of pointers has null in alias set: 28.8
XXX average alias set size: 1.45

XXX times a non-volatile is read: 1588
XXX times a non-volatile is write: 880
XXX times a volatile is read: 103
XXX    times read thru a pointer: 18
XXX times a volatile is write: 17
XXX    times written thru a pointer: 1
XXX times a volatile is available for access: 5.38e+03
XXX percentage of non-volatile access: 95.4

XXX forward jumps: 1
XXX backward jumps: 11

XXX stmts: 125
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 18
   depth: 2, occurrence: 17
   depth: 3, occurrence: 16
   depth: 4, occurrence: 21
   depth: 5, occurrence: 23

XXX percentage a fresh-made variable is used: 18.9
XXX percentage an existing variable is used: 81.1
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
XXX total OOB instances added: 0
********************* end of statistics **********************/


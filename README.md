# Finding missed optimization

## Getting started

It requires Python >= 3.10.
To install dependencies and git hooks
```
python -m pip install -r requirements.txt
pre-commit install
```

To install Creduce
```
sudo apt-get update
sudo apt-get install creduce
```

To install Csmith
```
git clone https://github.com/csmith-project/csmith.git
cd csmith
sudo apt install g++ cmake m4
cmake -DCMAKE_INSTALL_PREFIX=<INSTALL-PREFIX> .
make && make install
```

## Checkers
Checkers are in `checkers.py`, all function with the `@Scorer.checker` are automatically registered to the scorer.

## Running
The static checker perform better with unlinked binary, for GCC/Clang this means running with `-c`.
The dynamic checker *needs* *non* position independent, linked binaries, this means `-no-pie`.
The dynamic checker also require a copy of [Intel Pin](https://www.intel.com/content/www/us/en/developer/articles/tool/pin-a-dynamic-binary-instrumentation-tool.html), the path to Intel Pin must be set using the `PIN_PATH` environment variable, for more information on the use of Intel Pin in this project, refer to [pintool/README.md](pintool/README.md).

The binary analyzer is launched using `main.py`.

Due to the way GCC inject utility functions it may needs to be run with `-O` > 0.
For vectorization we must use the following flags > -mavx -ftree-vectorize

## How to test our artefact
Once all the dependencies are installed you can test out our artifact doing the following
### Generate the data
In the automatedsoftwaretesting/csmith folder 
```
remove_data.sh
generate_data.sh
```
### Reducing the test cases
In the automatedsoftwaretesting/creduce folder
```
run.sh
```
### Output
The outputs will be stored in the creduce folder in the format
```
random$i-$compilerflag-$instruction
```

## Our Results
We have all the results in our runs in `/runs`, it also contains some early analysis of why this is or not a missed optimization.

## References

[Finding Missed Compiler Optimizations by Differential Testing](https://raw.githubusercontent.com/gergo-/missed-optimizations/master/missed_optimizations_preprint.pdf) for the main idea
[A new algorithm for identifying loops in decompilation](https://dl.acm.org/doi/10.5555/2391451.2391464) to compute the loop nesting tree

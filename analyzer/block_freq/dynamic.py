from os import getenv, path
import subprocess
import sys
from tempfile import TemporaryDirectory
from typing import Dict
from typing import Dict

from angr.knowledge_plugins.cfg.cfg_node import CFGNode

from .block_freq import BlockFrequencyEstimator

if getenv("PIN_DIR") is not None:
    PIN_PATH = getenv("PIN_DIR")
else:
    PIN_PATH = path.realpath(path.join(path.dirname(path.realpath(
        sys.argv[0])), '../pin-3.27-98718-gbeaa5d51e-gcc-linux'))

PINTOOL = path.realpath(
    path.join(path.dirname(__file__), '../../pintool/bb_freq.so'))


class DynamicBlockFrequencyEstimator(BlockFrequencyEstimator):
    def frequencies(self, entry_node: CFGNode) -> Dict[int, int]:
        bb = ','.join(list([str(b.addr) for b in self._cfg.model.nodes()]))
        with TemporaryDirectory() as output_dir:
            output_file = path.join(output_dir, 'block_freqs.out')

            try:
                proc = subprocess.run([
                    f'{PIN_PATH}/pin',
                    '-t', PINTOOL,
                    '-bb', bb,
                    '-o', output_file,
                    '--',
                    self._binary
                ], cwd=output_dir, stdout=subprocess.DEVNULL, timeout=30)
            except subprocess.TimeoutExpired:
                pass

            # proc.check_returncode()

            block_freqs = {}
            with open(output_file, mode='r') as output:
                while True:
                    line = output.readline()
                    if not line:
                        break
                    l = line.rstrip()
                    items = l.split(',')
                    assert len(items) == 2, "pin tool produced invalid file"
                    items = [int(item) for item in items]
                    block_freqs[items[0]] = items[1]

            output.close()

        return block_freqs

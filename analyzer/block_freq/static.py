from typing import Dict

from angr.knowledge_plugins.cfg.cfg_node import CFGNode

from .block_freq import BlockFrequencyEstimator

from ..cfg_utils import compute_loop_nesting, display_graph, get_instr


class StaticBlockFrequencyEstimator(BlockFrequencyEstimator):
    def frequencies(self, entry_node: CFGNode) -> Dict[int, int]:
        loop_data = compute_loop_nesting(self._cfg.graph, entry_node)
        return {addr: (8**data.loop_depth if data.loop_depth is not None else 0) for (addr, data) in loop_data.items().items()}

import abc
from typing import Dict

import angr
from angr.knowledge_plugins.cfg.cfg_node import CFGNode


class BlockFrequencyEstimator():
    __metaclass__ = abc.ABCMeta

    _p: angr.Project
    _cfg: angr.analyses.cfg.cfg_base.CFGBase
    # Path to the binary to analyze
    _binary: str

    def __init__(self, p: angr.Project, cfg: angr.analyses.cfg.cfg_base.CFGBase, binary: str):
        self._p = p
        self._cfg = cfg
        self._binary = binary

    @abc.abstractmethod
    def frequencies(self, entry_node: CFGNode) -> Dict[int, int]:
        return {}

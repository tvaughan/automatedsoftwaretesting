import angr
from .scorer import Scorer


@Scorer.checker
def instructions(instr: angr.analyses.disassembly.Instruction):
    return 1


@Scorer.checker
def loads(instr: angr.analyses.disassembly.Instruction):
    # Number of memory loads
    if (instr.insn.mnemonic == 'mov'):
        return 1
    else:
        return 0


@Scorer.checker
def calls(instr: angr.analyses.disassembly.Instruction):
    # Number of memory loads
    if (instr.insn.mnemonic == 'call'):
        return 1
    else:
        return 0


@Scorer.checker
def divide(instr: angr.analyses.disassembly.Instruction):
    # Number of memory loads
    div_mnemonics = ['div', 'idiv', 'fdiv', 'fdivr', 'fdivp',
                     'fdivrp', 'fidiv', 'fidivr', 'divps', 'divss', 'divpd', 'divsd']
    if instr.insn.mnemonic in div_mnemonics:
        return 1
    else:
        return 0


@Scorer.checker
def vect(instr: angr.analyses.disassembly.Instruction):
    if instr.insn.mnemonic[0] == 'v':
        return 1
    else:
        return 0

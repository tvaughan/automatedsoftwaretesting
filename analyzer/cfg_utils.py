import angr
from angr import Project
from angr.analyses.cfg.cfg_base import CFGBase
from angr.analyses.cfg.cfg_fast import CFGFast
import networkx as nx
from networkx import NetworkXError, graph
import matplotlib.pyplot as plt
import typing
from angr.knowledge_plugins.cfg.cfg_node import CFGNode
import logging

l = logging.getLogger(__name__)


class LoopData:
    traversed: bool = False
    DFSP_pos = 0
    iloop_header: typing.Union[CFGNode, None] = None
    is_loop_header = False
    is_reentry = False
    loop_irreducible = False
    # Nesting depth in loops
    loop_depth: typing.Union[int, None] = None

    def __init__(self, node: CFGNode):
        self.node = node

    def __str__(self) -> str:
        out = []
        if self.traversed:
            out.append('traversed')
        if self.is_loop_header:
            out.append('loop_header')
        if self.is_reentry:
            out.append('reentry')
        if self.loop_irreducible:
            out.append('irreducible')
        if self.iloop_header is not None:
            out.append(hex(self.iloop_header.addr))

        return f"{','.join(out)} ({self.DFSP_pos})"


NodeT = typing.TypeVar('NodeT', bound=typing.Hashable)
DataT = typing.TypeVar('DataT')


class GraphData(typing.Generic[NodeT, DataT]):
    """GraphData contains additional data for a graph."""

    _data: typing.Dict[int, DataT] = {}

    def items(self):
        return self._data.items()

    def __setitem__(self, node: NodeT, data: DataT):
        self._data[hash(node)] = data

    def __getitem__(self, node: NodeT) -> DataT:
        return self._data[hash(node)]


class LoopDatas(GraphData[CFGNode, LoopData]):
    """LoopDatas contains information about the loops in a CFG."""

    def items(self):
        return {data.node.addr: data for data in self._data.values()}

    def __repr__(self) -> str:
        out = ""
        for addr, item in self._data.items():
            out += f"{hex(addr)}: {item}, \n"

        return out


def tag_lhead(loop_data: LoopDatas, b: CFGNode, h: CFGNode):
    # last case is not in the paper, however not taking care of that
    # makes self loop go into infinite loops.
    # And I'm not the only one to think that way apparently
    # https://github.com/EnSoftCorp/jimple-toolbox-commons/blob/2a6b36942c1cc63ec27dac6f02b39fa3416d388f/com.ensoftcorp.open.jimple.commons/src/com/ensoftcorp/open/jimple/commons/loops/DecompiledLoopIdentification.java#L406
    if b == None or h == None or hash(h) == hash(b):
        return
    cur1 = b
    cur2 = h
    while loop_data[cur1].iloop_header is not None:
        ih = loop_data[cur1].iloop_header
        # Type checker can't infert that ih can't be None
        assert (ih is not None)
        if hash(ih) == hash(cur2):
            return
        if loop_data[ih].DFSP_pos < loop_data[cur2].DFSP_pos:
            loop_data[cur1].iloop_header = cur2
            cur1 = cur2
            cur2 = ih
        else:
            cur1 = ih
    loop_data[cur1].iloop_header = cur2


def traverse_loops_DFS(loop_data: LoopDatas, b0: CFGNode, dfsp_pos: int) -> CFGNode | None:
    loop_data[b0].traversed = True
    loop_data[b0].DFSP_pos = dfsp_pos
    for b in b0.successors:
        # if hash(b) == hash(b0):
        #      continue
        # We know that the loop analysis fields have been setup
        if not loop_data[b].traversed:
            nh = traverse_loops_DFS(loop_data, b, dfsp_pos+1)
            if nh is not None:
                tag_lhead(loop_data, b0, nh)
        else:
            if loop_data[b].DFSP_pos > 0:
                # case B
                loop_data[b].is_loop_header = True
                tag_lhead(loop_data, b0, b)
            elif loop_data[b].iloop_header == None:
                pass  # Case C
            else:
                h = loop_data[b].iloop_header
                # Type checker can't infer that h is not None
                assert (h is not None)
                if loop_data[h].DFSP_pos > 0:  # h in DFSP(b0)
                    # Case D
                    tag_lhead(loop_data, b0, h)
                else:  # h not in DFSP(b0)
                    # Case E
                    loop_data[b].is_reentry = True
                    loop_data[h].loop_irreducible = True
                    while loop_data[h].iloop_header is not None:
                        h = loop_data[h].iloop_header
                        # Type checker can't infer that h is not None
                        assert (h is not None)
                        if loop_data[h].DFSP_pos > 0:
                            tag_lhead(loop_data, b0, h)
                            break
                        loop_data[h].loop_irreducible = True
    loop_data[b0].DFSP_pos = 0
    return loop_data[b0].iloop_header


def identifiy_loops(cfg: graph.Graph, start: CFGNode):
    loop_data = LoopDatas()
    for node in cfg.nodes:
        loop_data[node] = LoopData(node)
    traverse_loops_DFS(loop_data, start, 1)

    return loop_data


def mark_depth(loop_data: LoopDatas, start: CFGNode):
    """Register the depth (loop nesting) of the block inside a control flow graph
    ALTERS the loop_data

    :param loop_data: Loopdata, precomputed by identifiy_loops
    :type loop_data: LoopDatas
    :param start:
    :type start: CFGNode
    """
    header = loop_data[start].iloop_header
    if header is not None:
        parent_depth = loop_data[header].loop_depth
        assert (parent_depth is not None)
        depth = parent_depth
    else:
        depth = 0

    if loop_data[start].is_loop_header:
        depth += 1

    loop_data[start].loop_depth = depth
    for n in start.successors:
        if loop_data[n].loop_depth is None:
            mark_depth(loop_data, n)

    return loop_data


def compute_loop_nesting(cfg: graph.Graph, start: CFGNode) -> LoopDatas:
    """Find loop and compute the nesting

    :param cfg:
    :type cfg: graph.Graph
    :param start:
    :type start: CFGNode
    """
    loop_data = identifiy_loops(cfg, start)
    mark_depth(loop_data, start)

    return loop_data


def display_graph(cfg: graph.Graph, labels=None):
    if labels is None:
        labels = {node: hex(node.addr) if isinstance(
            node, CFGNode) else "INVALID" for node in cfg.nodes()}

    # labels = { node: get_instr(node.block).render(formatting={}, ascii_only=True) for node in cfg.graph.nodes() }
    # print(labels)
    # labels = {node: f"{hex(node.addr)}: {hex(loop_data[node].iloop_header.addr)} ({loop_data[node].loop_depth})" if loop_data[
    #    node].iloop_header is not None else f"{hex(node.addr)} ({loop_data[node].loop_depth})" for node in cfg.graph.nodes()}
    # labels = { node: hex(node.addr) for node in cfg.graph.nodes() }
    nx.draw_networkx(cfg, with_labels=True, labels=labels,
                     font_weight='bold', horizontalalignment='left', font_family='monospace')

    plt.show()


def get_instr(project: angr.Project, block: angr.block.Block | None):
    if block is None:
        return None
    addr = block.addr - 1 if block.thumb else block.addr
    instr: angr.analyses.disassembly.Disassembly = project.analyses.Disassembly(
        ranges=[(addr, addr + block.size)],
        thumb=block.thumb,
        block_bytes=block.bytes,
    )  # pyright: ignore

    return instr


def clean_alignment_nodes(p: Project, cfg: CFGFast, entry: CFGNode):
    """Remove the unreachable nodes that the compiler adds to align jump targets

    :param p:
    :type p: Project
    :param cfg:
    :type cfg: CFGFast
    :param entry:
    :type entry: CFGNode
    """
    to_remove = []
    for node in cfg.graph.nodes():
        assert (isinstance(node, CFGNode))
        if node == entry:
            continue
        if node.block is None:
            continue
        if len(node.predecessors) == 0:
            get_instr(p, node.block)
            for _, instr in get_instr(p, node.block).raw_result_map['instructions'].items():
                assert (type(instr) == angr.analyses.disassembly.Instruction)
                if instr.insn.mnemonic == 'nop':
                    to_remove.append(node)
                else:
                    l.info(
                        f"Orphan node: {get_instr(p, node.block).render(formatting={}, ascii_only=True)}")

    for node in to_remove:
        try:
            cfg._remove_node(node)
        except NetworkXError:
            # That's weird sometimes nodes we just collected will no longer
            # be in the graph
            pass

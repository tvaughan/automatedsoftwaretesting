#! env python3
import logging
from angr.analyses.cfg.cfg_base import CFGBase
import typing
import angr
from angr.knowledge_plugins.cfg.cfg_node import CFGNode
from .cfg_utils import get_instr

CheckerType = typing.Callable[[angr.analyses.disassembly.Instruction], int]
logger = logging.getLogger(__name__)


class Scorer:
    # The number of times instructions inside a loop are considered
    # (i.e. the estimation of how many time the loop will iterate)
    LOOP_MUTLIPLICATION_FACTOR = 8
    # The scorer is a singleton
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    _checkers: list[CheckerType] = list()

    @classmethod
    def checker(cls, func: CheckerType):
        # Ugly runtime sanity check to ensure we won't have override when scoring
        names = [checker.__name__ for checker in cls()._checkers]
        assert (func.__name__ not in names)
        cls()._checkers.append(func)

    def score(self, project: angr.Project, cfg: CFGBase, loop_data: dict[int, int]):
        totals = {checker.__name__: 0 for checker in self._checkers}
        for checker in self._checkers:
            checkerName = checker.__name__
            for node in cfg.graph.nodes():
                assert (isinstance(node, CFGNode))
                if (node.block is None):
                    logger.warn(f"node {hex(node.addr)} has no block")
                    continue

                if node.addr in loop_data:
                    freq = loop_data[node.addr]
                else:
                    freq = 0
                assert (freq is not None)
                instructions = get_instr(project, node.block)
                # we know that we have a block
                assert instructions is not None
                for addr, instr in instructions.raw_result_map['instructions'].items():
                    assert (type(instr) == angr.analyses.disassembly.Instruction)
                    totals[checkerName] += freq * checker(instr)
        return totals


# Register all checkers
from . import checkers  # pyright: ignore # NOQA

#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 PATH GCC_VERSION"
    exit 1
fi

CODE_PATH=$(realpath "${1}")
GCC_VERSION="${2}"

podman-remote run -it --rm --volume $CODE_PATH:/root/src:z docker.io/gcc:$GCC_VERSION bash -c "cd /root/src/ && gcc -O3 -no-pie -mavx -ftree-vectorize main.c -o GCC-${GCC_VERSION}" 

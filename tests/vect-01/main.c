#include <immintrin.h>
#include <stdio.h>
double * array_add ;
double * array_mult;
/*
void print_matrix(double* matrix, int n){
    int counter = 0 ;
    for(int i = 0 ; i < n*n; i++){
        printf("%f ", matrix[i]);
        counter++;
        if (counter % 4 == 0){
            printf("\n");
        }
    }
}
*/

int main(){
    __m256d a, b,c,d;
    double * array1 = (double*) malloc(4*4*sizeof(double));
    
    double * array2 = (double*) malloc(4*4*sizeof(double));
    //double * array_add = (double*) malloc(4*4*sizeof(double));
    //double * array_mult = (double*) malloc(4*4*sizeof(double));
    for(int i = 0 ; i < 16 ; i++){
        array1[i] = i*5;
        array2[i] = i * 2;
    }
    /*
    printf("Matrix 1 : \n");
    print_matrix(array1, 4);
    printf("Matrix 2 : \n");
    print_matrix(array2, 4);
    */

    for(int i = 0 ; i < 16-3; i+=4){
        // Load both our vectors with 4 elements of array1 and array2
        a = _mm256_loadu_pd(&array1[i]);
        b = _mm256_loadu_pd(&array2[i]);
        // Add vector a + b and store in c
        c = _mm256_add_pd(a,b);
        // store in the array
        _mm256_storeu_pd(&array_add[i], c);
        // Multiply a * b and store in d
        d = _mm256_mul_pd(a,b);
        // store in the array mult array
        _mm256_storeu_pd(&array_mult[i], d);
    

    }
    /*
     print_matrix(array_add, 4);
    printf("Matrix mult : \n");
    print_matrix(array_mult, 4);
*/
    
}
#include <stdio.h>

int x[64];

int fn1() {
  int a = -10;
  for (int i = 0; i < 64; i++) {
    if (i % 5 == 0) {
      i += 10;
    }
    a ^= x[i];
    x[i] += a;
  }
  return 0;
}

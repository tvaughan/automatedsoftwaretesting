	.file	"main.c"
	.text
	.globl	N
	.bss
	.align 4
	.type	N, @object
	.size	N, 4
N:
	.zero	4
	.text
	.globl	fn1
	.type	fn1, @function
fn1:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	$0, -16(%rbp)
	movw	$0, -2(%rbp)
	movl	$0, -48(%rbp)
	jmp	.L2
.L4:
	movzwl	-2(%rbp), %eax
	movl	%eax, %edx
	addl	$1, %edx
	movw	%dx, -2(%rbp)
	movswq	%ax, %rax
	subq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movl	%eax, %edx
	movl	-20(%rbp), %eax
	addl	%edx, %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
	movq	-16(%rbp), %rax
	movl	%eax, %edx
	movl	-24(%rbp), %eax
	addl	%edx, %eax
	addl	$2, %eax
	movl	%eax, -24(%rbp)
	movq	-16(%rbp), %rax
	movl	%eax, %edx
	movl	-28(%rbp), %eax
	addl	%edx, %eax
	addl	$3, %eax
	movl	%eax, -28(%rbp)
	movq	-16(%rbp), %rax
	movl	%eax, %edx
	movl	-32(%rbp), %eax
	addl	%edx, %eax
	addl	$4, %eax
	movl	%eax, -32(%rbp)
	movq	-16(%rbp), %rax
	movl	%eax, %edx
	movl	-36(%rbp), %eax
	addl	%edx, %eax
	addl	$5, %eax
	movl	%eax, -36(%rbp)
	movq	-16(%rbp), %rax
	movl	%eax, %edx
	movl	-40(%rbp), %eax
	addl	%edx, %eax
	addl	$6, %eax
	movl	%eax, -40(%rbp)
	movq	-16(%rbp), %rax
	movl	%eax, %edx
	movl	-44(%rbp), %eax
	addl	%edx, %eax
	addl	$6, %eax
	movl	%eax, -44(%rbp)
	movl	-20(%rbp), %eax
	addl	%eax, -44(%rbp)
	movl	-24(%rbp), %eax
	addl	%eax, -40(%rbp)
	movl	-28(%rbp), %eax
	addl	%eax, -36(%rbp)
	movl	-36(%rbp), %eax
	addl	%eax, -32(%rbp)
.L3:
	movswl	-2(%rbp), %edx
	movl	N(%rip), %eax
	cmpl	%eax, %edx
	jl	.L4
	movl	-44(%rbp), %eax
	subl	-40(%rbp), %eax
	movl	%eax, %edx
	movl	-36(%rbp), %eax
	addl	%edx, %eax
	subl	-32(%rbp), %eax
	cltq
	addq	%rax, -16(%rbp)
	addl	$1, -48(%rbp)
.L2:
	cmpl	$99, -48(%rbp)
	jle	.L3
	movq	-16(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	fn1, .-fn1
	.ident	"GCC: (GNU) 12.2.0"
	.section	.note.GNU-stack,"",@progbits

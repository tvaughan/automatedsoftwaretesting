#! env python3
import os
import sys
import angr
import argparse
from analyzer.block_freq.dynamic import DynamicBlockFrequencyEstimator
from analyzer.block_freq.static import StaticBlockFrequencyEstimator
from os import path
import json

from analyzer.cfg_utils import clean_alignment_nodes
from analyzer.scorer import Scorer
import logging

if __name__ == "__main__":
    logging.getLogger().setLevel(level=os.getenv('LOG_LEVEL', 'WARNING').upper())

    parser = argparse.ArgumentParser(
        prog='ProgramName',
        description='Estimate program cost')
    parser.add_argument(
        'filename', help="Binary to analyze, must NOT be linked (compile with -c)")
    parser.add_argument('-e', '--entry', default='main',
                        help="Name of the entry function, from where the analyses will start")
    parser.add_argument('-f', '--frequency-estimator', default='dynamic', choices=['dynamic', 'static'],
                        help="Which frequency estimator to use")

    args = parser.parse_args()
    filename = path.realpath(args.filename)
    entry_func_name = args.entry
    frequency_estimator = args.frequency_estimator

    if not os.path.isfile(filename):
        print(f"File {filename} not found")
        sys.exit(-1)

    p = angr.Project(filename, load_options={'auto_load_libs': False})

    cfg: angr.analyses.cfg.cfg_fast.CFGFast = p.analyses.CFGFast(
        normalize=True)

    entry_node = None
    for addr, fun in p.kb.functions.items():
        if fun.name == entry_func_name:
            entry_node = cfg.get_any_node(addr)

    if entry_node is None:
        print(f"Couldn't find entry \"{entry_func_name}\", specify it with -e")
        sys.exit(-1)

    logging.info(f"Entry: {hex(entry_node.addr)}")

    clean_alignment_nodes(p, cfg, entry_node)

    logging.info(f"Cleaned alignment nodes")

    if frequency_estimator == 'dynamic':
        frequency_estimator = DynamicBlockFrequencyEstimator(p, cfg, filename)
    elif frequency_estimator == 'static':
        frequency_estimator = StaticBlockFrequencyEstimator(p, cfg, filename)
    frequencies = frequency_estimator.frequencies(entry_node)
    # labels = {
    #     node: f"{hex(node.addr)}: {hex(loop_data[node].iloop_header.addr)} ({loop_data[node].loop_depth})" if loop_data[
    #         node].iloop_header is not None
    #     else f"{hex(node.addr)} ({loop_data[node].loop_depth})\n{get_instr(p, node.block)}"
    #     for node in cfg.graph.nodes()
    # }
    # print(labels)
    # display_graph(cfg.graph, labels)

    print(json.dumps(Scorer().score(p, cfg, frequencies)))

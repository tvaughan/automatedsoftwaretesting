# Dynamic basic block frequency estimation using Intel Pin

## Building
It does not build with the latest version of GCC, it works well with GCC 12.2.

You also need a copy of Intel Pin, set the path using `PIN_PATH` environment, it was tested using version 3.27.

If you have docker or podman set up, you can easily build it:

```
docker run -it --rm -v $PWD:/root/pintool:z -v $PWD/../../pin-3.27-98718-gbeaa5d51e-gcc-linux/:/root/pin:z --env=PIN_PATH=/root/pin/ docker.io/gcc:12.2 make -C /root/pintool
```

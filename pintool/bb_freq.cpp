/*
 * Copyright (C) 2004-2021 Intel Corporation.
 * SPDX-License-Identifier: MIT
 */

#include <iostream>
#include <fstream>
#include <map>
#include <types.h>
#include <utility>
#include "pin.H"
using std::cerr;
using std::endl;
using std::ios;
using std::ofstream;
using std::string;
using std::map;

ofstream OutFile;

// The running count of instructions is kept here
// make it static to help the compiler optimize docount
std::map<ADDRINT, int> bcount{};

// This function is called before every block
VOID docount(ADDRINT c) { 
  if (bcount.count(c) > 0) {
    bcount[c] += 1;
  }
}

// Pin calls this function every time a new basic block is encountered
// It inserts a call to docount
VOID Trace(TRACE trace, VOID* v)
{
    // Visit every basic block  in the trace
    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
    {
//        OutFile << BBL_Address(bbl) << endl;
        // Insert a call to docount before every bbl, passing the number of instructions
        BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)docount, IARG_UINT32, BBL_Address(bbl), IARG_END);
    }
}

KNOB< string > KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "inscount.out", "specify output file name");
KNOB< string > KnobBlocks(KNOB_MODE_WRITEONCE, "pintool", "bb", "", "list of comma separated addresses of basic block to look for");

// This function is called when the application exits
VOID Fini(INT32 code, VOID* v)
{
    // Write to a file since cout and cerr maybe closed by the application
    OutFile.setf(ios::showbase);


    for (auto const &pair: bcount) {
        OutFile << pair.first << "," << pair.second << "\n";
    }

    OutFile.close();
}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    cerr << "This tool counts the number of dynamic instructions executed" << endl;
    cerr << endl << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char* argv[])
{
    // Initialize pin
    if (PIN_Init(argc, argv)) return Usage();

    OutFile.open(KnobOutputFile.Value().c_str());
    std::basic_string<char> bb_str = KnobBlocks.Value();
    std::replace(bb_str.begin(), bb_str.end(), ',', ' ');  // replace ':' by ' '

    std::stringstream ss(bb_str);
    int temp;
    while (ss >> temp)
      bcount.insert(std::make_pair(temp, 0));

    // Register Instruction to be called to instrument instructions
    TRACE_AddInstrumentFunction(Trace, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);

    // Start the program, never returns
    PIN_StartProgram();

    return 0;
}

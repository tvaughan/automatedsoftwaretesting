## Checkers
- [ ] vectorization (Krishna)
- [x] call (Eva)
- [x] division (Eva)
- [x] for over each function on the cfg (Eva)
 - Actually angr already does that, the bug was that when linking the linker injected a ton of utility functions / libaries

## LLVM loop unrolling
I suspect that LLVM aggressively unrolls, which implies a worst score than GCC almost all the time..

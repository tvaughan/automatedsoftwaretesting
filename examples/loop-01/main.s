	.file	"main.c"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4,,15
	.globl	fn1
	.type	fn1, @function
fn1:
.LFB0:
	movl	N(%rip), %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L4
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L3:
	movswq	%dx, %rcx
	addl	$1, %edx
	subq	%rcx, %rax
	movswl	%dx, %ecx
	cmpl	%esi, %ecx
	jl	.L3
	rep; ret
.L4:
	rep; ret
.LFE0:
	.size	fn1, .-fn1
	.section	.text.unlikely
.LCOLDE0:
	.text
.LHOTE0:
	.comm	N,4,4
	.section	.eh_frame,"a",@progbits
.Lframe1:
	.long	.LECIE1-.LSCIE1
.LSCIE1:
	.long	0
	.byte	0x3
	.string	"zR"
	.uleb128 0x1
	.sleb128 -8
	.uleb128 0x10
	.uleb128 0x1
	.byte	0x3
	.byte	0xc
	.uleb128 0x7
	.uleb128 0x8
	.byte	0x90
	.uleb128 0x1
	.align 8
.LECIE1:
.LSFDE1:
	.long	.LEFDE1-.LASFDE1
.LASFDE1:
	.long	.LASFDE1-.Lframe1
	.long	.LFB0
	.long	.LFE0-.LFB0
	.uleb128 0
	.align 8
.LEFDE1:
	.ident	"GCC: (GNU) 5.1.0"
	.section	.note.GNU-stack,"",@progbits

#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 PATH CLANG_VERSION"
    exit 1
fi

CODE_PATH=$(realpath "${1}")
CLANG_VERSION="${2}"

podman-remote run -it --rm --volume $CODE_PATH:/root/src:z docker.io/silkeh/clang:$CLANG_VERSION bash -c "cd /root/src/ && clang -O3 -no-pie -mavx -ftree-vectorize main.c -o CLANG-${CLANG_VERSION}" 

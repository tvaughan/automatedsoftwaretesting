set -e

size=100

cd data
for i in $(seq 0 $size);
do
    rm -r random$i
done

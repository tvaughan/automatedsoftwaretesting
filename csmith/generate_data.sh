#!/bin/bash
set -e

FLAGS="--float"
size=100

cd data
for i in $(seq 0 $size);
do
    mkdir random$i
    cd random$i
    csmith $FLAGS > random$i-gcc.c
    cd ..
done
cd ..
